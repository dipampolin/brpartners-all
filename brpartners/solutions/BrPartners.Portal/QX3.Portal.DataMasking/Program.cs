﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace QX3.Portal.DataMasking
{
    class Program
    {
        //static void Main(string[] args)
        //{
        //    if (Environment.UserInteractive)
        //    {
        //        var service = new DataMaskingService();
        //        service.Start(args);
        //    }
        //    else
        //    {
        //        ServiceBase[] ServicesToRun;
        //        ServicesToRun = new ServiceBase[]
        //        {
        //            new DataMaskingService()
        //        };

        //        ServiceBase.Run(ServicesToRun);
        //    }
        //}

        static void Main(string[] args)
        {
            if ((!Environment.UserInteractive))
            {
                Program.RunAsAService();
            }
            else
            {
                if (args != null && args.Length > 0)
                {
                    if (args[0].Equals("-i", StringComparison.OrdinalIgnoreCase))
                    {
                        SelfInstaller.InstallMe();
                    }
                    else
                    {
                        if (args[0].Equals("-u", StringComparison.OrdinalIgnoreCase))
                        {
                            SelfInstaller.UninstallMe();
                        }
                        else
                        {
                            Console.WriteLine("Invalid argument!");
                        }
                    }
                }
                else
                {
                    Program.RunAsAConsole();
                }
            }
        }

        static void RunAsAConsole()
        {
            DataMaskingService service = new DataMaskingService();
            service.Start(null);
        }

        static void RunAsAService()
        {
            ServiceBase[] servicesToRun = new ServiceBase[]
           {
                new DataMaskingService()
           };
            ServiceBase.Run(servicesToRun);
        }
    }
}
