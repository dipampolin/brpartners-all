﻿using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;

namespace QX3.Portal.DataMasking
{
    [RunInstaller(true)]
    public class DataMaskingInstaller : Installer
    {
        public DataMaskingInstaller()
        {
            ServiceProcessInstaller serviceProcessInstaller = new ServiceProcessInstaller();
            ServiceInstaller serviceInstaller = new ServiceInstaller();

            // Setup the Service Account type per your requirement
            serviceProcessInstaller.Account = ServiceAccount.LocalSystem;
            serviceProcessInstaller.Username = null;
            serviceProcessInstaller.Password = null;

            serviceInstaller.ServiceName = "DataMasking";
            serviceInstaller.DisplayName = "DataMasking Service";
            serviceInstaller.StartType = ServiceStartMode.Automatic;
            serviceInstaller.Description = "Bot responsável por coletar dados do ambiente de produção e mascará-los para serem usados em ambiente de dev/teste.";

            this.Installers.Add(serviceProcessInstaller);
            this.Installers.Add(serviceInstaller);
        }
    }
}
