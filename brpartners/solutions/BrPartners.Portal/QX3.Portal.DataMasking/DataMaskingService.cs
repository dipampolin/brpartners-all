﻿using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.Services.Business;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace QX3.Portal.DataMasking
{
    public partial class DataMaskingService : ServiceBase
    {
        private string PortalEndpoint = ConfigurationManager.AppSettings["Portal.Endpoint"];
        private string PortalRHEndpoint = ConfigurationManager.AppSettings["PortalRH.Endpoint"];

        private string PortalDataMasking = ConfigurationManager.AppSettings["Portal.DataMasking"];
        private string PortalRHDataMasking = ConfigurationManager.AppSettings["PortalRH.DataMasking"];

        private string DebugMode = ConfigurationManager.AppSettings["Debug"];

        private System.Timers.Timer timer;

        public DataMaskingService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            WriteMessage("O serviço está no Windows Services.");

            timer = new System.Timers.Timer(60000);
            timer.AutoReset = false; // true;
            timer.Elapsed += new System.Timers.ElapsedEventHandler(this.OnTimer);
            timer.Start();
        }

        public void Start(string[] args)
        {
            WriteMessage("O serviço está no modo manual.");

            Execute();

            //while (true)
            //{
            //    Execute();
            //    Thread.Sleep(3600000); // 1h
            //}
        }

        private void Execute()
        {
            try
            {
                WriteMessage("O serviço será executado.");

                if (PortalRHDataMasking == "1")
                {
                    WriteMessage("Mascarando dados do Portal RH.");

                    var url = PortalRHEndpoint + "Service/DataMasking";

                    var request = WebRequest.Create(url);
                    request.Method = "POST";
                    request.ContentType = "application/json";
                    request.ContentLength = 0;

                    using (var response = request.GetResponse())
                    {
                        var encoding = ASCIIEncoding.ASCII;
                        using (var reader = new StreamReader(response.GetResponseStream(), encoding))
                        {
                            string responseText = reader.ReadToEnd();
                            WriteMessage("Resposta do serviço: " + responseText);
                        }
                    }
                }

                if (PortalDataMasking == "1")
                {
                    WriteMessage("Mascarando dados do Portal.");

                    var url = PortalEndpoint + "Service/DataMasking";

                    var request = WebRequest.Create(url);
                    request.Method = "POST";
                    request.ContentType = "application/json";
                    request.ContentLength = 0;

                    using (var response = request.GetResponse())
                    {
                        var encoding = ASCIIEncoding.ASCII;
                        using (var reader = new StreamReader(response.GetResponseStream(), encoding))
                        {
                            string responseText = reader.ReadToEnd();
                            WriteMessage("Resposta do serviço: " + responseText);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string error = string.Format("{0} - {1} - {2}",
                   ex.Message,
                   ex.InnerException != null ? ex.InnerException.ToString() : "",
                   ex.StackTrace);

                WriteError(error);
            }
        }

        //private List<SuitabilityClients> GetClients()
        //{
        //    List<SuitabilityClients> clients = null;

        //    SuitabilityMethods service = new SuitabilityMethods();

        //    var response = service.SelecionaClientesSuitability2(null, null, null, null, null, null);
        //    clients = response.Clients;

        //    return clients;
        //}

        public void OnTimer(object sender, System.Timers.ElapsedEventArgs args)
        {
            Execute();
        }

        protected override void OnStop()
        {
            WriteMessage("O serviço foi parado.");

            timer.Stop();
        }

        private void WriteMessage(string message)
        {
            if (DebugMode == "1")
            {
                string file = GetFilePath();

                message = string.Format("{0:G}: {1}{2}", DateTime.Now, message, Environment.NewLine);
                File.AppendAllText(file, message);
            }
        }

        private void WriteError(string error)
        {
            error = "Error: " + error;
            WriteMessage(error);
        }

        private string GetFilePath()
        {
            string a = AppDomain.CurrentDomain.BaseDirectory;
            string path = string.Empty;
            path = a + "\\Log\\";

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            path = path + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";

            return path;
        }
    }

    partial class DataMaskingService
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            this.ServiceName = "DataMasking";
        }

        #endregion
    }
}
