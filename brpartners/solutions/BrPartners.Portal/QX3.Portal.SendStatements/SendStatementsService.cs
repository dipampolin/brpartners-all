﻿using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.ServiceProcess;
using System.Text;
using System.Threading;

namespace QX3.Portal.SendStatements
{
    public partial class SendStatementsService : ServiceBase
    {
        private string PortalEndpoint = ConfigurationManager.AppSettings["Send.Endpoint"];
        private string Schedule = ConfigurationManager.AppSettings["Expiry.Schedule"];
        private string DebugMode = ConfigurationManager.AppSettings["Debug"];

        private System.Timers.Timer timer;

        public SendStatementsService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            WriteMessage("O serviço está no Windows Services.");

            timer = new System.Timers.Timer(60000);
            timer.AutoReset = true;
            timer.Elapsed += new System.Timers.ElapsedEventHandler(this.OnTimer);
            timer.Start();
        }

        public void Start(string[] args)
        {
            WriteMessage("O serviço está no modo manual.");
            WriteMessage("O serviço será executado no último dia do mês.");

            while (true)
            {
                Execute();
                Thread.Sleep(60000);
            }
        }

        private void Execute()
        {
            try
            {
                int lastDayOfMonth = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);

                if (DateTime.Now.Day == lastDayOfMonth)
                {
                    int scheduleHour = Convert.ToInt32(Schedule.Split(':')[0]);
                    int scheduleMinute = Convert.ToInt32(Schedule.Split(':')[1]);

                    WriteMessage("O serviço está configurado para o horário de " + Schedule + ".");

                    DateTime now = DateTime.Now;

                    if (now.Hour == scheduleHour && now.Minute == scheduleMinute)
                    {
                        WriteMessage("Está na hora de enviar os extratos.");

                        var request = WebRequest.Create(PortalEndpoint + "Statement/SendStatements");
                        request.Method = "POST";
                        request.ContentType = "application/json";
                        request.ContentLength = 0;

                        using (var response = request.GetResponse())
                        {
                            var encoding = ASCIIEncoding.ASCII;
                            using (var reader = new StreamReader(response.GetResponseStream(), encoding))
                            {
                                string responseText = reader.ReadToEnd();
                                WriteMessage("Resposta do serviço: " + responseText);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string error = string.Format("{0} - {1} - {2}",
                   ex.Message,
                   ex.InnerException != null ? ex.InnerException.ToString() : "",
                   ex.StackTrace);

                WriteError(error);
            }
        }

        public void OnTimer(object sender, System.Timers.ElapsedEventArgs args)
        {
            Execute();
        }

        protected override void OnStop()
        {
            WriteMessage("O serviço foi parado.");

            timer.Stop();
        }

        private void WriteMessage(string message)
        {
            if (DebugMode == "1")
            {
                string file = GetFilePath();

                message = string.Format("{0:G}: {1}{2}", DateTime.Now, message, Environment.NewLine);
                File.AppendAllText(file, message);
            }
        }

        private void WriteError(string error)
        {
            error = "Error: " + error;
            WriteMessage(error);
        }

        private string GetFilePath()
        {
            string a = AppDomain.CurrentDomain.BaseDirectory;
            string path = string.Empty;
            path = a + "\\Log\\";

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            path = path + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";

            return path;
        }
    }

    partial class SendStatementsService
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            this.ServiceName = "SendStatements";
        }

        #endregion
    }
}
