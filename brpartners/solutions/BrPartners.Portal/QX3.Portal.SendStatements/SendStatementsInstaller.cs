﻿using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;

namespace QX3.Portal.SendStatements
{
    [RunInstaller(true)]
    public class SendStatementsInstaller : Installer
    {
        public SendStatementsInstaller()
        {
            ServiceProcessInstaller serviceProcessInstaller = new ServiceProcessInstaller();
            ServiceInstaller serviceInstaller = new ServiceInstaller();

            // Setup the Service Account type per your requirement
            serviceProcessInstaller.Account = ServiceAccount.LocalSystem;
            serviceProcessInstaller.Username = null;
            serviceProcessInstaller.Password = null;

            serviceInstaller.ServiceName = "SendStatements";
            serviceInstaller.DisplayName = "SendStatements Service";
            serviceInstaller.StartType = ServiceStartMode.Automatic;
            serviceInstaller.Description = "";

            this.Installers.Add(serviceProcessInstaller);
            this.Installers.Add(serviceInstaller);
        }
    }
}
