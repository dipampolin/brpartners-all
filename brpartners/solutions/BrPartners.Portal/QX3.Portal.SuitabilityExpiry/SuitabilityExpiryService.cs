﻿using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.Services.Business;
using QX3.Portal.Services.Business.SolutionTech;
using QX3.Spinnex.Common.Services.Mail;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Threading;

namespace QX3.Portal.SuitabilityExpiry
{
    public partial class SuitabilityExpiryService : ServiceBase
    {
        private const int REMAKE_STATUS_INTEGER = 3;
        private string Schedule = ConfigurationManager.AppSettings["Expiry.Schedule"];
        private string ExpiryDays = ConfigurationManager.AppSettings["Expiry.Days"];
        private string MailSubject = ConfigurationManager.AppSettings["Mail.Subject"];
        private string MailToCompliance = ConfigurationManager.AppSettings["Mail.To"];
        private string MailMessageToExpire = ConfigurationManager.AppSettings["Mail.Message.ToExpire"];
        private string MailMessageExpired = ConfigurationManager.AppSettings["Mail.Message.Expired"];
        private bool MailActive
        {
            get {
                string config = ConfigurationManager.AppSettings["Mail.Active"];

                if (!string.IsNullOrEmpty(config) && config == "true")
                    return true;

                return false;
            }
        } 
        private string EnvironmentMode = ConfigurationManager.AppSettings["Environment"];
        private string DebugMode = ConfigurationManager.AppSettings["Debug"];
        private string DaysUntilRemakeStatus = ConfigurationManager.AppSettings["Expiry.Days.Remake"];

        private System.Timers.Timer timer;

        public SuitabilityExpiryService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            WriteMessage("O serviço está no Windows Services.");

            timer = new System.Timers.Timer(60000);
            timer.AutoReset = true;
            timer.Elapsed += new System.Timers.ElapsedEventHandler(this.OnTimer);
            timer.Start();
        }

        public void Start(string[] args)
        {
            WriteMessage("O serviço está no modo manual.");

            while (true)
            {
                Execute();
                Thread.Sleep(60000);
            }
        }

        private void Execute()
        {
            try
            {
                int scheduleHour = Convert.ToInt32(Schedule.Split(':')[0]);
                int scheduleMinute = Convert.ToInt32(Schedule.Split(':')[1]);

                WriteMessage("O serviço está configurado para o horário de " + Schedule + ".");

                DateTime now = DateTime.Now;

                //if (now.Hour == scheduleHour && now.Minute == scheduleMinute && now.DayOfWeek != DayOfWeek.Saturday && now.DayOfWeek != DayOfWeek.Sunday)
                if (now.Hour == scheduleHour && now.Minute == scheduleMinute)
                {
                    WriteMessage("Está na hora de buscar a expiração do suitability.");
                    Search();
                }
            }
            catch (Exception ex)
            {
                string error = string.Format("{0} - {1} - {2}",
                   ex.Message,
                   ex.InnerException != null ? ex.InnerException.ToString() : "",
                   ex.StackTrace);

                WriteError(error);
            }
        }

        public void OnTimer(object sender, System.Timers.ElapsedEventArgs args)
        {
            Execute();
        }

        protected override void OnStop()
        {
            WriteMessage("O serviço foi parado.");

            timer.Stop();
        }

        public void Search()
        {
            WriteMessage("Procurando os prazos...");

            var clients = GetClients();
            if (clients != null && clients.Count > 0)
            {
                List<SuitabilityClients> expired = new List<SuitabilityClients>();
                List<SuitabilityClients> closeToExpire = new List<SuitabilityClients>();

                var daysToExpire = GetDaysToExpire();
                var daysToRemake = Convert.ToInt32(DaysUntilRemakeStatus);
                bool sendMailIsActive = MailActive;

                foreach (var client in clients)
                {
                    WriteMessage($"Analisando cliente {client.ClientName}, de IdCliente {client.IdCliente} de data suitability: {client.CompletionDate} com Status {client.StatusDesc} ...");

                    if (client.Status != 2)
                    {
                        WriteMessage("Cliente ainda não preencheu um suitability, não o refez ou não é elegível!");
                        continue;
                    }

                    if (client.Blocked)
                    {
                        WriteMessage("Cliente bloqueado!");
                        continue;
                    }

                    WriteMessage("Verificando necessidade de atualização de status...");
                    SetToRemakeStatusType(client, daysToRemake);

                    if (sendMailIsActive)
                        CalculateDeadlines(client, daysToExpire, expired, closeToExpire);
                }

                if (sendMailIsActive)
                {
                    SendExpired(expired);
                    SendCloseToExpire(closeToExpire);
                }

                WriteMessage("Fim da busca neste prazo.");
            }
        }

        private string GetMessage(string model, DateTime date, string clientName)
        {
            return model = clientName + ", <br><br>" + model.Replace("xx/xx/xxxx", date.ToString("dd/MM/yyyy"));
        }

        private List<int> GetDaysToExpire()
        {
            List<int> days = new List<int>();

            ////if (string.IsNullOrEmpty(ExpiryDays) || !int.TryParse(ExpiryDays, out days))
            ////    WriteMessage("A configuração da chave Expiry.Days está errada.");

            string[] arr = ExpiryDays.Split(';');
            foreach (var item in arr)
                days.Add(Convert.ToInt32(item));

            return days;
        }

        private List<SuitabilityClients> GetClients()
        {
            List<SuitabilityClients> clients = null;

            SuitabilityMethods service = new SuitabilityMethods();

            WriteMessage("Buscando clientes...");

            var response = service.SelecionaClientesSuitability(null, null, null, null, null, null);
            clients = response.Clients;

            WriteMessage("Busca de clientes concluída!");
            return clients;
        }

        private List<SuitabilityClients> GetHistoric(int clientId)
        {
            List<SuitabilityClients> clients = null;

            SuitabilityMethods service = new SuitabilityMethods();

            clients = service.HistoricoClientesSuitability(clientId, null, null);
            if (clients != null)
                clients = clients.Where(d => d.CompletionDate != DateTime.MinValue).ToList();

            return clients;
        }

        private void CalculateDeadlines(SuitabilityClients client, List<int> daysToExpire, List<SuitabilityClients> expired, List<SuitabilityClients> closeToExpire)
        {
            if (client.InvestorKind == 0 || client.PersonType == "F")
            {
                if (client.CompletionDate != DateTime.MinValue)
                {
                    foreach (var item in daysToExpire)
                    {
                        if (client.CompletionDate.AddMonths(24) > DateTime.Now.Date
                            && client.CompletionDate.AddMonths(24) == DateTime.Now.Date.AddDays(item))
                        {
                            client.CompletionDate = DateTime.Now.AddDays(item);
                            closeToExpire.Add(client);
                        }
                    }

                    if (client.CompletionDate.AddMonths(24) == DateTime.Now.Date.AddDays(-1))
                    {
                        client.CompletionDate = client.CompletionDate.AddMonths(24);
                        expired.Add(client);
                    }
                }
            }
        }

        private void SendExpired(List<SuitabilityClients> expired)
        {
            if (expired.Count > 0)
            {
                WriteMessage("Enviando o e-mail para os expirados...");  // Enviar p/ cliente, cópia compliance

                foreach (var client in expired)
                    SendMail(GetMessage(MailMessageExpired, client.CompletionDate, client.ClientName), client.SendSuitabilityEmail ? client.Email : "", MailToCompliance);
            }
            else
                WriteMessage("Nennhum cliente com o prazo expirado.");
        }

        private void SendCloseToExpire(List<SuitabilityClients> closeToExpire)
        {
            if (closeToExpire.Count > 0)
            {
                WriteMessage("Enviando o e-mail para aqueles próximo de expirar..."); // Enviar p/ cliente, cópia compliance

                foreach (var client in closeToExpire)
                    SendMail(GetMessage(MailMessageToExpire, client.CompletionDate, client.ClientName), client.SendSuitabilityEmail ? client.Email : "", MailToCompliance);
            }
            else
                WriteMessage("Nennhum cliente próximo de expirar o prazo.");
        }

        private void SendMail(string message, string mailTo, string mailCopy)
        {
            SendMail sendEmail = new SendMail();
            sendEmail.Subject = MailSubject;
            sendEmail.EmailTo = EnvironmentMode == "PRD" && !string.IsNullOrEmpty(mailTo) ? mailTo : MailToCompliance;

            if (!string.IsNullOrEmpty(mailCopy))
                sendEmail.CC = mailCopy.Replace(';',',');

            sendEmail.Body = message;
            sendEmail.IsBodyHtml = true;
            sendEmail.Send();
        }

        private void WriteMessage(string message)
        {
            if (DebugMode == "1")
            {
                string file = GetFilePath();

                message = string.Format("{0:G}: {1}{2}", DateTime.Now, message, Environment.NewLine);
                File.AppendAllText(file, message);
            }
        }

        private void WriteError(string error)
        {
            error = "Error: " + error;
            WriteMessage(error);
        }

        private string GetFilePath()
        {
            string a = AppDomain.CurrentDomain.BaseDirectory;
            string path = string.Empty;
            path = a + "\\Log\\";

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            path = path + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";

            return path;
        }

        /// <summary>
        /// Tests if you arrived 'x' days before the suitability expires - and changes the client status to 'Remake' on the chosen date, if so.
        /// </summary>
        /// <param name="suitabilityClient">Client selected to test whether to change status.</param>
        /// <param name="daysUntilExpiry">Chosen days before expiration.</param>
        private void SetToRemakeStatusType(SuitabilityClients suitabilityClient, int daysUntilExpiry)
        {
            var hasHistoric = suitabilityClient.CompletionDate != DateTime.MinValue;

            WriteMessage($"Tem histórico? {hasHistoric}");

            if (hasHistoric)
            {
                var suitabilityDateAfterTwoYears = suitabilityClient.CompletionDate.AddMonths(24);
                var dateDaysUntilExpiry = DateTime.Now.Date.AddDays(daysUntilExpiry);

                var dayUntilItExpiresHasArrived = suitabilityDateAfterTwoYears.Date > DateTime.Now.Date
                    && suitabilityDateAfterTwoYears.Date == dateDaysUntilExpiry.Date;

                WriteMessage($"Expirou? {dayUntilItExpiresHasArrived}. Data do Suitability: {suitabilityClient.CompletionDate}, Data em 2 anos: {suitabilityDateAfterTwoYears}, Data de vencimento: {dateDaysUntilExpiry}.");

                if (dayUntilItExpiresHasArrived)
                {
                    SuitabilityMethods suitabilityMethods = new SuitabilityMethods();
                    SolutionTechMethods solutionTechMethods = new SolutionTechMethods();

                    suitabilityMethods.AtualizaStatusSuitability(suitabilityClient.IdCliente, REMAKE_STATUS_INTEGER); // Changing customer status to 'Remake'

                    var codigoBolsa = suitabilityClient.CodigosBolsa.FirstOrDefault();
                    if (codigoBolsa > 0) // Não usei operador de diferença pois daria 'true' nos casos de nullable int.
                    {
                        solutionTechMethods.UpdatedSolutionTech(codigoBolsa, suitabilityClient.CompletionDate, false);
                    }
                }
            }          
        }
    }

    partial class SuitabilityExpiryService
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            this.ServiceName = "SuitabilityExpiry";
        }
    }
}
