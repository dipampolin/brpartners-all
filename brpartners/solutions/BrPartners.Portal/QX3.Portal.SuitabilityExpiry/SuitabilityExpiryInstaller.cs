﻿using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;

namespace QX3.Portal.SuitabilityExpiry
{
    [RunInstaller(true)]
    public class SuitabilityExpiryInstaller : Installer
    {
        public SuitabilityExpiryInstaller()
        {
            ServiceProcessInstaller serviceProcessInstaller = new ServiceProcessInstaller();
            ServiceInstaller serviceInstaller = new ServiceInstaller();

            // Setup the Service Account type per your requirement
            serviceProcessInstaller.Account = ServiceAccount.LocalSystem;
            serviceProcessInstaller.Username = null;
            serviceProcessInstaller.Password = null;

            serviceInstaller.ServiceName = "Suitability-Expiry";
            serviceInstaller.DisplayName = "Suitability-Expiry Service";
            serviceInstaller.StartType = ServiceStartMode.Automatic;
            serviceInstaller.Description = "Bot responsável por atualizar o Status do suitability para 'Refazer' quando necessário e avisa o cliente quando o suitability estiver perto de expirar (com o Compliance em cópia).";

            this.Installers.Add(serviceProcessInstaller);
            this.Installers.Add(serviceInstaller);
        }
    }
}
