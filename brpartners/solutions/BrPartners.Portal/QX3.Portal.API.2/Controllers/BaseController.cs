﻿using QX3.Portal.API._2.Models;
using QX3.Portal.API._2.Util;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace QX3.Portal.API._2.Controllers
{
    [ApiAuthorize]
    public class BaseController : ApiController
    {
        protected HttpResponseMessage Response<T>(Response<T> response, HttpStatusCode? statusCode = null)
        {
            if (response.Status == ResponseStatus.BusinessError)
                return Request.CreateResponse(HttpStatusCode.NotAcceptable, response); // (HttpStatusCode.BadRequest, new HttpError(response.ErrorMessage));
            else if (response.Status == ResponseStatus.SystemError)
                return Request.CreateResponse(HttpStatusCode.InternalServerError, response); // new HttpError(response.Message)
            else if (response.Status == ResponseStatus.Unauthorized)
                return Request.CreateResponse(HttpStatusCode.Unauthorized, response);
            else
                return Request.CreateResponse(statusCode.HasValue ? statusCode.Value : HttpStatusCode.OK, response);
        }
    }
}
