﻿using QX3.Portal.API._2.Helper;
using QX3.Portal.API._2.Models;
using QX3.Portal.API._2.Util;
using QX3.Portal.Services.Business;
using QX3.Portal.Services.Business.AccessControl;
using QX3.Spinnex.Common.Services.Mail;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Web.Http;
using System.Web.Http.Description;

namespace QX3.Portal.API._2.Controllers
{
    public class SuitabilityController : BaseController
    {
        private SuitabilityMethods suitability;
        private SuitabilityAdminMethods suitabilityAdmin;

        #region Private Methods

        private List<InvestorType> GetInvestorType(int profileId = 0)
        {
            List<InvestorType> obj = new List<InvestorType>();

            var result = suitability.GetInvestorTypeXProduct();
            if (result != null && result.Count > 0)
            {
                if (profileId != 0)
                    result = result.Where(x => x.Id == profileId).ToList();
                else
                    result = result.OrderBy(x => x.Id).ToList();

                int currentId = 0;

                foreach (var item in result)
                {
                    if (item.Id != currentId)
                    {
                        currentId = item.Id;

                        InvestorType it = new InvestorType();
                        it.Id = item.Id;
                        it.Name = item.Type;

                        it.Products = new List<Product>();

                        var products = result.Where(x => x.Id == currentId);
                        if (products != null && products.Count() > 0)
                        {
                            foreach (var j in products)
                            {
                                Product p = new Product();
                                p.Code = j.ProductCode;
                                p.Name = j.ProductDescription;

                                it.Products.Add(p);
                            }
                        }

                        obj.Add(it);
                    }

                }
            }

            return obj;
        }

        private void SendSuitabilityResult(string suitabilityId, out string error)
        {
            error = "";

            try
            {
                string mailTo = ConfigurationManager.AppSettings["Suitability.PDF.Mail.To"];

                if (!string.IsNullOrEmpty(mailTo))
                {
                    var sc = suitability.SelecionaResultadoSuitability(suitabilityId);

                    var html = ViewRenderer.RenderView("~/Views/PDF/SuitabilityResult.cshtml", sc);
                    var pdfBytes = PDFConverter.HtmlToPDF(html);

                    string client = string.Format("{0} ({1})", sc.Name, CommonHelper.FormatCpfCnpj(sc.CPFCNPJ));
                    string body = string.Format("Segue o último questionário de Suitability preenchido pelo cliente '{0}', na API de Suitability.", client);
                    string subject = string.Format("Resultado do Suitability - {0}", client);

                    var mail = new SendMail
                    {
                        EmailTo = mailTo,
                        Body = body,
                        Subject = subject
                    };

                    string fileName = string.Format("Suitability_{0}_{1}.{2}", suitabilityId, DateTime.Now.ToString("ddMMyyyymmss"), "pdf");

                    Attachment attachment = new Attachment(new MemoryStream(pdfBytes), fileName, "application/pdf");
                    mail.Attachments.Add(attachment);

                    mail.Send();
                }
            }
            catch (Exception ex)
            {
                error = string.Format("SMTP: {0}-{1}", ex.Message, ex.ToString());
                // TODO: Log.
            }
        }

        private int InsertClient(long cpfCnpj, string name, string personType, string email)
        {
            var user = new Contracts.DataContracts.ACUser();

            user.CpfCnpj = cpfCnpj;
            user.Name = name;
            user.Email = email;
            user.Login = user.Email;

            user.Profile = new Contracts.DataContracts.ACProfile();
            user.Profile.ID = 1;

            user.Type = "E"; // Externo
            user.ActiveStatus = 1;

            AccessControl service = new AccessControl();
            return  service.InsertUser(user);
        }

        private Response<bool> ValidateClient(SuitabilityInput input, Response<bool> obj)
        {
            if (input.CpfCnpj == 0 || string.IsNullOrEmpty(input.Name)
                                    || string.IsNullOrEmpty(input.PersonType) || string.IsNullOrEmpty(input.Email))
            {
                ResponseMessages.BusinessError(obj, "Nenhum cliente encontrado. Para cadastrar, envie: CPF/CNPJ, nome, tipo de pessoa e o e-mail.");
                return obj;
            }

            var treatedCpfCnpj = input.CpfCnpj.ToString();
            var validCpfCnpj = BrazilianDocumentsValidatorHelper.IsCPF(treatedCpfCnpj) || BrazilianDocumentsValidatorHelper.IsCNPJ(treatedCpfCnpj);

            if (!validCpfCnpj)
            {
                ResponseMessages.BusinessError(obj, "O CPF/CNPJ inserido é inválido.");
                return obj;
            }

            var validEmail = EmailValidatorHelper.IsEmail(input.Email);
            if (!validEmail)
            {
                ResponseMessages.BusinessError(obj, "O email inserido é inválido.");
                return obj;
            }

            return obj;
        }

        #endregion

        public SuitabilityController()
        {
            suitability = new SuitabilityMethods();
            suitabilityAdmin = new SuitabilityAdminMethods();
        }

        /// <summary>
        /// Retorna uma lista de Perfil x Produtos.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(Response<List<InvestorType>>))]
        [Route("api/Suitability/Profiles/Products")]
        [HttpGet]
        public HttpResponseMessage GetProfileXProducts()
        {
            Response<List<InvestorType>> obj = new Response<List<InvestorType>>();

            try
            {
                obj.Result = GetInvestorType();
            }
            catch (Exception ex)
            {
                ResponseMessages.SystemError(obj, ex);
            }

            return Response(obj);
        }

        /// <summary>
        /// Retorna um Perfil x Produtos.
        /// </summary>
        /// <param name="id">Id do perfil</param>
        /// <returns></returns>
        [ResponseType(typeof(Response<List<InvestorType>>))]
        [Route("api/Suitability/Profiles/{id}/Products")]
        [HttpGet]
        public HttpResponseMessage GetProfileXProducts(int id)
        {
            Response<List<InvestorType>> obj = new Response<List<InvestorType>>();

            try
            {
                List<InvestorType> i = GetInvestorType(id);
                if (i != null && i.Count > 0)
                    obj.Result = i;
                else
                    ResponseMessages.BusinessError(obj, "Nenhum perfil encontrado.");
            }
            catch (Exception ex)
            {
                ResponseMessages.SystemError(obj, ex);
            }

            return Response(obj);
        }

        /// <summary>
        /// Retorna o Perfil do Cliente.
        /// </summary>
        /// <param name="cpfCnpj">CPF/CNPJ do cliente</param>
        /// <returns></returns>
        [ResponseType(typeof(Response<SuitabilityClient>))]
        [Route("api/Suitability/Clients/{cpfCnpj}")]
        [HttpGet]
        public HttpResponseMessage GetClient(long cpfCnpj)
        {
            Response<SuitabilityClient> obj = new Response<SuitabilityClient>();

            try
            {
                var result = suitability.GetClient(cpfCnpj);
                if (result != null && result.CPFCNPJ > 0)
                {
                    SuitabilityClient i = new SuitabilityClient();
                    i.CpfCnpj = result.CPFCNPJ;
                    i.Name = result.ClientName;
                    i.Status = result.StatusDesc;
                    // i.StatusId = result.Status;
                    i.Unframed = result.Unframed;
                    i.PersonType = result.PersonType;
                    i.InvestorType = result.InvestorTypeDesc;
                    i.InvestorKind = result.InvestorKindDesc;
                    i.CompletionDate = result.CompletionDate > DateTime.MinValue ? result.CompletionDate.ToString("dd/MM/yyyy") : "";

                    obj.Result = i;
                }
                else
                    ResponseMessages.BusinessError(obj, "Nenhum cliente encontrado.");
            }
            catch (Exception ex)
            {
                ResponseMessages.SystemError(obj, ex);
            }

            return Response(obj);
        }

        /// <summary>
        /// Retorna os Formulários disponíveis.
        /// </summary>
        /// <param name="personType">Tipo de Pessoa</param>
        /// <param name="status">Status do Formulário</param>
        /// <returns></returns>
        [ResponseType(typeof(Response<List<SuitabilityCard>>))]
        [Route("api/Suitability/Forms")]
        [HttpGet]
        public HttpResponseMessage GetSuitabilityForms(string personType, string status)
        {
            Response<List<SuitabilityCard>> obj = new Response<List<SuitabilityCard>>();

            try
            {
                var s = suitabilityAdmin.GetForms(personType, status);
                if (s != null && s.Count > 0)
                {
                    List<SuitabilityCard> lst = new List<SuitabilityCard>();

                    foreach (var item in s)
                    {
                        SuitabilityCard sc = new SuitabilityCard();

                        sc.Id = item.Id;
                        sc.Description = item.Descricao;
                        sc.Status = item.Status;
                        sc.StatusDescription = item.DescricaoStatus;
                        sc.Type = item.Tipo;

                        lst.Add(sc);
                    }

                    obj.Result = lst;
                }
                else
                    ResponseMessages.BusinessError(obj, "Nenhum formulário encontrado.");
            }
            catch (Exception ex)
            {
                ResponseMessages.SystemError(obj, ex);
            }

            return Response(obj);
        }

        /// <summary>
        /// Retorna um Formulário.
        /// </summary>
        /// <param name="id">Id do formulário</param>
        /// <returns></returns>
        [ResponseType(typeof(Response<SuitabilityForm>))]
        [Route("api/Suitability/Forms/{id}")]
        [HttpGet]
        public HttpResponseMessage GetSuitabilityForm(int id)
        {
            Response<SuitabilityForm> obj = new Response<SuitabilityForm>();

            try
            {
                var s = suitabilityAdmin.GetForm(id);
                if (s != null && s.Formulario != null && s.Formulario.Id > 0)
                {
                    SuitabilityForm f = new SuitabilityForm();

                    f.Form.Id = s.Formulario.Id;
                    f.Form.Description = s.Formulario.Descricao;
                    f.Form.Status = s.Formulario.Status;
                    f.Form.StatusDescription = s.Formulario.DescricaoStatus;
                    f.Form.Type = s.Formulario.Tipo;

                    foreach (var q in s.Questoes)
                    {
                        if (!q.QuestaoAtiva)
                            continue;

                        SuitabilityQuestion sq = new SuitabilityQuestion();

                        sq.Active = q.QuestaoAtiva;
                        sq.Hidden = q.Oculta;
                        sq.Id = q.Id;
                        sq.MinimumPercentage = q.PercentualMinimo;
                        sq.Order = q.Ordem;
                        sq.PercentageDescription = q.DescricaoPercentual;
                        sq.Text = q.Enunciado;
                        sq.Type = q.Tipo;
                        sq.Weight = q.Peso;

                        if (q.Alternativas != null && q.Alternativas.Count > 0)
                        {
                            foreach (var a in q.Alternativas)
                            {
                                if (!a.Ativa)
                                    continue;

                                SuitabilityAlternative sa = new SuitabilityAlternative();

                                sa.Active = a.Ativa;
                                sa.Id = a.Id;
                                sa.Order = a.Ordem;
                                sa.Points = a.Pontos;
                                sa.Text = a.Texto;

                                sq.Alternatives.Add(sa);
                            }
                        }

                        if (q.Colunas != null && q.Colunas.Count > 0)
                        {
                            foreach (var c in q.Colunas)
                            {
                                if (!c.Ativa)
                                    continue;

                                SuitabilityColumn sc = new SuitabilityColumn();

                                sc.Active = c.Ativa;
                                sc.Description = c.Descricao;
                                sc.Id = c.Id;
                                sc.Order = c.Ordem;

                                sq.Columns.Add(sc);
                            }
                        }

                        f.Questions.Add(sq);
                    }

                    //foreach (var item in s.Pontuacao)
                    //{
                    //    var p = s.Perfis.Where(x => x.Id == item.IdPerfil).FirstOrDefault();

                    //    f.Scores.Add(new SuitabilityScore {
                    //        Type = p.Nome,
                    //        Min = HasValueOrSetMin(item.Minimo),
                    //        Max = HasValueOrSetMax(item.Maximo)
                    //    });
                    //}

                    obj.Result = f;
                }
                else
                    ResponseMessages.BusinessError(obj, "Nenhum formulário encontrado.");
            }
            catch (Exception ex)
            {
                ResponseMessages.SystemError(obj, ex);
            }

            return Response(obj);
        }

        /// <summary>
        /// Recebe as respostas de um Formulário.
        /// </summary>
        /// <param name="id">Id do formulário</param>
        /// <param name="input">Formulário</param>
        /// <returns></returns>
        [ResponseType(typeof(Response<bool>))]
        [Route("api/Suitability/Forms/{id}")]
        [HttpPost]
        public HttpResponseMessage PostSuitabilityForm(int id, SuitabilityInput input)
        {
            Response<bool> obj = new Response<bool>();

            try
            {
                var form = suitabilityAdmin.GetForm(id);

                #region Validation

                if (form == null || form.Formulario == null || form.Formulario.Id == 0)
                {
                    ResponseMessages.BusinessError(obj, "Nenhum formulário encontrado.");
                    return Response(obj);
                }

                int clientId = 0;

                var client = suitability.GetClient(input.CpfCnpj);
                if (client == null || client.CPFCNPJ == 0)
                {
                    obj = ValidateClient(input, obj);

                    if (obj.Status != ResponseStatus.Success)
                        return Response(obj);

                    clientId = InsertClient(input.CpfCnpj, input.Name, input.PersonType, input.Email);
                }
                else
                    clientId = client.IdCliente;

                #endregion

                // Objeto para salvar no banco.
                QX3.Portal.Contracts.DataContracts.SuitabilityInput si = new QX3.Portal.Contracts.DataContracts.SuitabilityInput();
                si.Items = new List<Contracts.DataContracts.SuitabilityInputItem>();

                string error = string.Empty;
                decimal points = 0;

                foreach (var question in input.Questions)
                {
                    var q = form.Questoes.SingleOrDefault(x => x.Id == question.Id);

                    #region Validation

                    if (q == null)
                    {
                        error = string.Format("Questão {0}: Questão não encontrada no formulário.", question.Id);
                        break;
                    }

                    if (q.Tipo == "D") // Distribuição
                    {
                        decimal total = question.Answers.Sum(x => x.Percentage);
                        if (total != 100)
                        {
                            error = string.Format("Questão {0}: O total deve somar 100%.", question.Id);
                            break;
                        }
                    }

                    if (q.Tipo == "R" || q.Tipo == "C") // Resposta simples ou múltipla
                    {
                        var total = question.Answers.Length;
                        if (total < 1)
                        {
                            string m = q.Tipo == "R" ? "Selecione uma opção." : "Selecione ao menos uma opção.";
                            error = string.Format("Questão {0}: {1}", question.Id, m);
                            break;
                        }
                    }

                    if (q.Tipo == "M") // Matriz
                    {
                        var total = question.Answers.Length;
                        var isChecked = false;
                        for (var i = 0; i < total; i++)
                        {
                            var a = question.Answers[i];
                            var md = a.MatrixData;
                            for (var j = 0; j < md.Length; j++)
                            {
                                if (md[j] != null)
                                {
                                    isChecked = true;
                                    break;
                                }

                                continue;
                            }
                        }

                        if (isChecked == false)
                            error = string.Format("Questão {0}: {1}", question.Id, "Selecione ao menos uma opção.");
                    }

                    #endregion

                    foreach (var answer in question.Answers)
                    {
                        var a = q.Alternativas.Where(x => x.Id == answer.Id).SingleOrDefault();

                        #region Validation

                        if (a == null || a.Id == 0)
                        {
                            error = string.Format("Questão {0}, Resposta {1}: Resposta não encontrada no formulário.", question.Id, answer.Id);
                            break;
                        }

                        #endregion

                        Contracts.DataContracts.SuitabilityInputItem sii = new Contracts.DataContracts.SuitabilityInputItem();
                        sii.QuestionId = question.Id;
                        sii.AnswerId = answer.Id;
                        sii.Percentage = Convert.ToDouble(answer.Percentage);
                        sii.IsCorrect = true;

                        if (q.Tipo == "M") // Matriz
                        {
                            sii.MatrixData = new List<Contracts.DataContracts.SuitabilityMatrixAnswer>();

                            foreach (var md in answer.MatrixData)
                            {
                                sii.MatrixData.Add(new Contracts.DataContracts.SuitabilityMatrixAnswer()
                                {
                                    ID = md.Id,
                                    IsChecked = true
                                });
                            }

                            var available = q.Colunas.Where(x => x.Ativa).Count();
                            var answered = answer.MatrixData.Count();

                            if (available == answered)
                                points += q.Peso * a.Pontos;
                            else
                                sii.IsCorrect = false;
                        }
                        else if (q.Tipo == "D") // Distribuição
                        {
                            if (q.PercentualMinimo.HasValue && (answer.Percentage > q.PercentualMinimo.Value))
                                points += q.Peso * a.Pontos;
                            else
                                sii.IsCorrect = false;
                        }
                        else
                            points += q.Peso * a.Pontos;

                        si.Items.Add(sii);
                    }

                    if (!string.IsNullOrEmpty(error))
                        break;
                }

                if (!string.IsNullOrEmpty(error))
                {
                    ResponseMessages.BusinessError(obj, error);
                    return Response(obj);
                }

                // Perfil
                var profile = form.Pontuacao.Find(score => (points >= CommonHelper.HasValueOrSetMin(score.Minimo)) && (points <= CommonHelper.HasValueOrSetMax(score.Maximo)));
                if (profile == null)
                    error = "Pontuação fora do range cadastrado: " + points;
                else
                {
                    try
                    {
                        int suitabilityId = suitability.GravarSuitability(si, string.Empty, 0, clientId, 2, profile.IdPerfil, (int)points, id);
                        if (suitabilityId > 0)
                            SendSuitabilityResult(suitabilityId.ToString(), out error);
                        else
                            error = "Ocorreu um erro ao salvar o suitability.";
                    }
                    catch (Exception ex)
                    {
                        error = "Ocorreu uma falha ao salvar o suitability.";
                    }
                }

                if (string.IsNullOrEmpty(error) || error.Contains("SMTP"))
                {
                    obj.Result = true;
                    obj.Message = error;
                }
                else
                {
                    ResponseMessages.BusinessError(obj, error);
                }
            }
            catch (Exception ex)
            {
                ResponseMessages.SystemError(obj, ex);
            }

            return Response(obj, HttpStatusCode.Created);
        }

        /// <summary>
        /// Retorna as respostas do último Formulário preenchido.
        /// </summary>
        /// <param name="cpfCnpj">CPF/CNPJ do cliente</param>
        /// <returns></returns>
        [ResponseType(typeof(Response<SuitabilityInput>))]
        [Route("api/Suitability/Clients/{cpfCnpj}/Last")]
        [HttpGet]
        public HttpResponseMessage GetLastSuitability(long cpfCnpj)
        {
            Response<SuitabilityInput> obj = new Response<SuitabilityInput>();

            try
            {
                var client = suitability.GetClient(cpfCnpj);

                #region Validation

                if (client == null || client.CPFCNPJ == 0)
                {
                    ResponseMessages.BusinessError(obj, "Nenhum cliente encontrado.");
                    return Response(obj);
                }

                if (client.Id == 0)
                {
                    ResponseMessages.BusinessError(obj, "O cliente não possui suitability preenchido.");
                    return Response(obj);
                }

                #endregion

                var s = suitability.SelecionaResultadoSuitability(client.Id.ToString());

                SuitabilityInput si = new SuitabilityInput();
                si.CpfCnpj = cpfCnpj;

                List<SuitabilityQuestionInput> lstQuestions = new List<SuitabilityQuestionInput>();

                foreach (var item in s.Answers.ToLookup(x => x.Id))
                {
                    SuitabilityQuestionInput sqi = new SuitabilityQuestionInput();
                    sqi.Id = item.Key;

                    List<SuitabilityAnswerInput> lstAnswers = new List<SuitabilityAnswerInput>();

                    foreach (var answer in item)
                    {
                        SuitabilityAnswerInput sai = new SuitabilityAnswerInput();
                        sai.Id = answer.IdAnswer;

                        if (answer.QuestionType == Contracts.DataContracts.QuestionType.Distribution)
                            sai.Percentage = answer.Percentage;

                        if (answer.QuestionType == Contracts.DataContracts.QuestionType.Matrix)
                        {
                            if (answer.MatrixData != null)
                            {
                                var o = answer.MatrixData.Where(x => x.IsChecked);
                                if (o.Count() == 0)
                                    continue;

                                sai.MatrixData = o.Select(x => new SuitabilityMatrixAnswer { Id = x.Id }).ToArray();
                            }
                        }

                        lstAnswers.Add(sai);
                    }

                    sqi.Answers = lstAnswers.ToArray();

                    lstQuestions.Add(sqi);
                }

                si.Questions = lstQuestions.ToArray();
                obj.Result = si;
            }
            catch (Exception ex)
            {
                ResponseMessages.SystemError(obj, ex);
            }

            return Response(obj);
        }
    }
}
