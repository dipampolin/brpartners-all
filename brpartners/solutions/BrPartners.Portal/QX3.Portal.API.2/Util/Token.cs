﻿using System;
using System.Configuration;
using System.Web;

namespace QX3.Portal.API._2.Util
{
    public static class Token
    {
        private const string TOKEN_HEADERS_KEY = "X-BRPartners-Token";

        public static string GetFromHeader()
        {
            return HttpContext.Current.Request.Headers[TOKEN_HEADERS_KEY];
        }

        public static string Generate()
        {
            Guid g = Guid.NewGuid();
            string token = Convert.ToBase64String(g.ToByteArray());
            return token.Replace("=", string.Empty).Replace("+", string.Empty).Replace("/", string.Empty);
        }

        public static bool Validate(string request)
        {
            string token = ConfigurationManager.AppSettings["API.Token"];

            return token == request;
        }
    }
}