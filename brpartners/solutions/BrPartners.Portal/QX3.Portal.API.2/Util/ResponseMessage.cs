﻿using QX3.Portal.API._2.Models;
using System;

namespace QX3.Portal.API._2.Util
{
    public static class ResponseMessages
    {
        public static Response<T> BusinessError<T>(Response<T> response, string message)
        {
            response.Message = message;
            response.Status = ResponseStatus.BusinessError;

            return response;
        }

        public static Response<T> SystemError<T>(Response<T> response, Exception ex, string message = null)
        {
            response.Message = "Erro: " + ex.Message;
            response.Status = ResponseStatus.SystemError;

            return response;
        }

        public static Response<T> Unauthorized<T>(Response<T> response, string message = null)
        {
            response.Message = "Erro: " + (string.IsNullOrEmpty(message) ? "Não autorizado." : message);
            response.Status = ResponseStatus.Unauthorized;

            return response;
        }
    }
}