﻿using System;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace QX3.Portal.API._2.Util
{
    public class ApiAuthorizeAttribute : AuthorizeAttribute
    {
        private bool UseAuthorize
        {
            get
            {
                string useAuthorize = ConfigurationManager.AppSettings["API.Authorize"];
                if (!string.IsNullOrEmpty(useAuthorize) && useAuthorize == "false")
                    return false;

                return true;
            }
        }

        private bool Authorize(HttpActionContext actionContext)
        {
            if (!UseAuthorize)
                return true;

            // var appUrl = actionContext.RequestContext.VirtualPathRoot.ToLower() == "/" ? "" : actionContext.RequestContext.VirtualPathRoot.ToLower();
            // string path = actionContext.Request.RequestUri.AbsolutePath.ToLower();

            //if (path.EndsWith("api/login/facebook/"))
            //    return true;

            try
            {
                var token = Token.GetFromHeader();
                if (token == null)
                    return false;

                return Token.Validate(token);

                // HttpContext.Current.Items.Add("UserContext", userToken);
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            var challengeMessage = new HttpResponseMessage(HttpStatusCode.Unauthorized);
            throw new HttpResponseException(challengeMessage);
        }

        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if (Authorize(actionContext))
                return;

            HandleUnauthorizedRequest(actionContext);
        }
    }
}