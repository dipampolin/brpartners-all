﻿using System;
using System.Reflection;

namespace QX3.Portal.API._2.Helper
{
    public static class CommonHelper
    {
        private static string apiVersion;

        public static string GetAPIVersion()
        {
            if (apiVersion != null)
                return apiVersion;

            Assembly asm = typeof(CommonHelper).Assembly;
            var version = asm.GetName().Version;

            string appVersion = version.ToString();

            apiVersion = string.Format("{0} - {1:d}", appVersion, GetBuildDate(asm));
            return apiVersion;
        }

        private static DateTime GetBuildDate(Assembly asm)
        {
            const int peHeaderOffset = 60;
            const int linkerTimestampOffset = 8;
            var b = new byte[2048];
            System.IO.FileStream s = null;

            try
            {
                s = new System.IO.FileStream(asm.Location, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                s.Read(b, 0, 2048);
            }
            finally
            {
                if (s != null)
                    s.Close();
            }

            var dt = new System.DateTime(1970, 1, 1, 0, 0, 0).AddSeconds(System.BitConverter.ToInt32(b, System.BitConverter.ToInt32(b, peHeaderOffset) + linkerTimestampOffset));
            return dt.AddHours(System.TimeZone.CurrentTimeZone.GetUtcOffset(dt).Hours);
        }

        public static string FormatCpfCnpj(long value)
        {
            if (value >= 100000000000)
                return value.ToString(@"00\.000\.000\/0000\-00");
            else
                return value.ToString(@"000\.000\.000\-00");
        }

        public static string FormatCpfCnpj(string value)
        {
            value = value.Replace(".", "").Replace("-", "").Replace("/", "");

            long v = Convert.ToInt64(value);
            return FormatCpfCnpj(v);
        }

        public static decimal HasValueOrSetMin(decimal? value)
        {
            return value.HasValue ? value.Value : 0;
        }

        public static decimal HasValueOrSetMax(decimal? value)
        {
            return value.HasValue ? value.Value : Int32.MaxValue;
        }
    }
}