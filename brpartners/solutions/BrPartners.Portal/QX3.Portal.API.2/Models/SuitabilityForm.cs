﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QX3.Portal.API._2.Models
{
    public class SuitabilityForm
    {
        public SuitabilityForm()
        {
            Form = new SuitabilityCard();
            Questions = new List<SuitabilityQuestion>();
            // Scores = new List<SuitabilityScore>();
        }

        public SuitabilityCard Form { get; set; }

        public List<SuitabilityQuestion> Questions { get; set; }

        // public List<SuitabilityScore> Scores { get; set; }
    }

    public class SuitabilityScore
    {
        public string Type { get; set; }
        public decimal Min { get; set; }
        public decimal Max { get; set; }
    }

    public class SuitabilityCard
    {
        public int Id { get; set; }

        public string Description { get; set; }

        public string Type { get; set; }

        public string Status { get; set; }

        public string StatusDescription { get; set; }
    }

    public class SuitabilityQuestion
    {
        public SuitabilityQuestion()
        {
            Alternatives = new List<SuitabilityAlternative>();
            Columns = new List<SuitabilityColumn>();
        }

        public int Id { get; set; }

        // public Guid UID { get; set; }

        public int Order { get; set; }

        // public int? NumeroSubQuestao { get; set; }

        public string Text { get; set; }

        public bool Active { get; set; }

        public int Weight { get; set; }

        public string Type { get; set; }

        public string TypeDescription
        {
            get {
                string t = string.Empty;
                switch (Type)
                {
                    case "R":
                        t = "Simples";
                        break;
                    case "C":
                        t = "Múltipla";
                        break;
                    case "D":
                        t = "Distribuição";
                        break;
                    case "M":
                        t = "Matriz";
                        break;
                    default:
                        break;
                }

                return t;
            }
        }

        public decimal? MinimumPercentage { get; set; }

        public string PercentageDescription { get; set; }

        public bool Hidden { get; set; }

        public List<SuitabilityAlternative> Alternatives { get; set; }

        public List<SuitabilityColumn> Columns { get; set; }
    }

    public class SuitabilityAlternative
    {
        public int Id { get; set; }

        public string Text { get; set; }

        public bool Active { get; set; }

        public decimal Points { get; set; }

        public int Order { get; set; }

        // public int? IdProximaQuestao { get; set; }

        // public Guid? UIDProximaQuestao { get; set; }
    }

    public class SuitabilityColumn
    {
        public int Id { get; set; }

        public int Order { get; set; }

        public string Description { get; set; }

        public bool Active { get; set; }
    }
}