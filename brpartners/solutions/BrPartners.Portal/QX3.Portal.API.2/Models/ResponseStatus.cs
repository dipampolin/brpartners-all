﻿namespace QX3.Portal.API._2.Models
{
    public enum ResponseStatus
    {
        BusinessError = 0,

        Success = 1,

        SystemError = 2,

        Unauthorized = 3
    }
}