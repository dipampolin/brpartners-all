﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QX3.Portal.API._2.Models
{
    public class SuitabilityInput
    {
        public long CpfCnpj { get; set; }
        public string Name { get; set; }
        public string PersonType { get; set; }
        public string Email { get; set; }
        public SuitabilityQuestionInput[] Questions { get; set; }
    }

    public class SuitabilityQuestionInput
    {
        public int Id { get; set; }
        public SuitabilityAnswerInput[] Answers { get; set; }
    }

    public class SuitabilityAnswerInput
    {
        public int Id { get; set; }
        public decimal Percentage { get; set; }
        public SuitabilityMatrixAnswer[] MatrixData { get; set; }
    }

    public class SuitabilityMatrixAnswer
    {
        public int Id { get; set; }
        // public bool IsChecked { get; set; }
    }
}