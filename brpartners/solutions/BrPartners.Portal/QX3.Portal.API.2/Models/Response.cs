﻿namespace QX3.Portal.API._2.Models
{
    public class Response<ResultType>
    {
        public Response()
        {
            Status = ResponseStatus.Success;
        }

        public string Message { get; set; }
        public ResultType Result { get; set; }
        public ResponseStatus Status { get; set; }
    }
}