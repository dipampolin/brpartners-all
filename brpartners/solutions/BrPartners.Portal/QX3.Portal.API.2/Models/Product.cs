﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QX3.Portal.API._2.Models
{
    public class InvestorType
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public List<Product> Products { get; set; }
    }

    public class Product
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}