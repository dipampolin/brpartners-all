﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QX3.Portal.API._2.Models
{
    public class SuitabilityClient
    {
        public long CpfCnpj { get; set; }

        public string Name { get; set; }

        public string InvestorType { get; set; }

        public string Status { get; set; }

        // public int StatusId { get; set; }

        public string Unframed { get; set; }

        public string CompletionDate { get; set; }

        public string InvestorKind { get; set; }

        public string PersonType { get; set; }
    }
}