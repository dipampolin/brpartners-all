﻿using System.Web.Http;
using System.Web.Http.Cors;

namespace QX3.Portal.API._2.App_Start
{
    public class CorsConfig
    {
        public static void RegisterCors(HttpConfiguration httpConfig)
        {
            var corsAttr = new EnableCorsAttribute("*", "*", "*");
            httpConfig.EnableCors(corsAttr);
        }
    }
}