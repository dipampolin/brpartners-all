﻿using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;

namespace QX3.Portal.Notifications
{
    [RunInstaller(true)]
    public class NotificationsInstaller : Installer
    {
        public NotificationsInstaller()
        {
            ServiceProcessInstaller serviceProcessInstaller = new ServiceProcessInstaller();
            ServiceInstaller serviceInstaller = new ServiceInstaller();

            // Setup the Service Account type per your requirement
            serviceProcessInstaller.Account = ServiceAccount.LocalSystem;
            serviceProcessInstaller.Username = null;
            serviceProcessInstaller.Password = null;

            serviceInstaller.ServiceName = "Notifications";
            serviceInstaller.DisplayName = "Notifications Service";
            serviceInstaller.StartType = ServiceStartMode.Automatic;
            serviceInstaller.Description = "Bot responsável por enviar notificações do Portal RH.";

            this.Installers.Add(serviceProcessInstaller);
            this.Installers.Add(serviceInstaller);
        }
    }
}
