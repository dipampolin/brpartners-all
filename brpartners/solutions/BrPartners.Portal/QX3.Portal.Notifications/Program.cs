﻿using System;
using System.ServiceProcess;

namespace QX3.Portal.Notifications
{
    //static class Program
    //{
    //    static void Main(string[] args)
    //    {
    //        if (Environment.UserInteractive)
    //        {
    //            var service = new NotificationsService();
    //            service.Start(args);
    //        }
    //        else
    //        {
    //            ServiceBase[] ServicesToRun;
    //            ServicesToRun = new ServiceBase[]
    //            {
    //                new NotificationsService()
    //            };

    //            ServiceBase.Run(ServicesToRun);
    //        }
    //    }
    //}

    class Program
    {
        static void Main(string[] args)
        {
            if ((!Environment.UserInteractive))
            {
                Program.RunAsAService();
            }
            else
            {
                if (args != null && args.Length > 0)
                {
                    if (args[0].Equals("-i", StringComparison.OrdinalIgnoreCase))
                    {
                        SelfInstaller.InstallMe();
                    }
                    else
                    {
                        if (args[0].Equals("-u", StringComparison.OrdinalIgnoreCase))
                        {
                            SelfInstaller.UninstallMe();
                        }
                        else
                        {
                            Console.WriteLine("Invalid argument!");
                        }
                    }
                }
                else
                {
                    Program.RunAsAConsole();
                }
            }
        }

        static void RunAsAConsole()
        {
            NotificationsService service = new NotificationsService();
            service.Start(null);
        }

        static void RunAsAService()
        {
            ServiceBase[] servicesToRun = new ServiceBase[]
           {
                new NotificationsService()
           };
            ServiceBase.Run(servicesToRun);
        }


    }
}
