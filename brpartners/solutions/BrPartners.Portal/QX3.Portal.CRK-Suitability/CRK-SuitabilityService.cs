﻿using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.Services.Business;
using QX3.Portal.Services.Business.AccessControl;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.ServiceProcess;
using System.Threading;

namespace QX3.Portal.CRK_Suitability
{
    public partial class CRKSuitabilityService : ServiceBase
    {
        private string DebugMode = ConfigurationManager.AppSettings["Debug"];

        private System.Timers.Timer timer;

        public CRKSuitabilityService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            WriteMessage("O serviço está no Windows Services.");

            timer = new System.Timers.Timer(60000);
            timer.AutoReset = true;
            timer.Elapsed += new System.Timers.ElapsedEventHandler(this.OnTimer);
            timer.Start();
        }

        public void Start(string[] args)
        {
            WriteMessage("O serviço está no modo manual.");

            while (true)
            {
                Execute();
                Thread.Sleep(3600000); // 1h
            }
        }

        private void Execute()
        {
            try
            {
                WriteMessage("O serviço está ativo de 9h às 20h.");

                if (DateTime.Now.Hour >= 9 && DateTime.Now.Hour <= 20)
                {
                    var clients = GetClients();

                    var crkUsers = GetCrkUsers();
                    foreach (var crk in crkUsers)
                    {
                        var c = clients.Find(x => x.CPFCNPJ == crk.CPFCNPJ);
                        if (c == null)
                        {
                            if (crk.Active)
                            {
                                // Insere

                                ACUser user = new ACUser();
                                user.ID = 0;
                                user.ChaveAD = GenerateADKey();

                                UpdateUser(user, crk);
                            }
                        }
                        else
                        {
                            // Atualiza

                            ACUser user = new ACUser();
                            user.ID = c.Id;

                            UpdateUser(user, crk);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string error = string.Format("{0} - {1} - {2}",
                   ex.Message,
                   ex.InnerException != null ? ex.InnerException.ToString() : "",
                   ex.StackTrace);

                WriteError(error);
            }
        }

        private void UpdateUser(ACUser user, SuitabilityClients crk)
        {
            // user.ID = c.Id;
            user.Name = crk.ClientName;
            user.Email = crk.Email;
            user.Login = crk.Login;

            user.Profile = new ACProfile();
            user.Profile.ID = crk.ProfileId;

            user.Type = crk.UserType;
            user.ActiveStatus = crk.Status;

            user.CpfCnpj = crk.CPFCNPJ;
            user.CdCliente = crk.CodigosBolsa[0];
            user.ActiveStatus = crk.Active ? 1 : 0;
            user.InvestorKind = crk.InvestorKind;
            user.SendSuitabilityEmail = crk.SendSuitabilityEmail;

            AccessControl service = new AccessControl();
            var a = service.InsertUser(user);
        }

        private List<SuitabilityClients> GetClients()
        {
            List<SuitabilityClients> clients = null;

            SuitabilityMethods service = new SuitabilityMethods();

            var response = service.SelecionaClientesSuitability2(null, null, null, null, null, null);
            clients = response.Clients;

            return clients;
        }

        private List<SuitabilityClients> GetCrkUsers()
        {
            List<SuitabilityClients> clients = null;

            SuitabilityMethods service = new SuitabilityMethods();

            clients = service.GetCrkClients();

            return clients;
        }

        private string GenerateADKey()
        {
            Random random = new Random();
            string key = string.Empty;

            string letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            while (key.Length < 3)
            {
                char letra = letters[(int)(random.NextDouble() * letters.Length)];
                key = key.Contains(letra.ToString()) ? string.Empty : key + letra;
            }

            string numbers = "0123456789";
            key = key + numbers[(int)(random.NextDouble() * numbers.Length)];

            return key.ToString().ToLower();
        }

        public void OnTimer(object sender, System.Timers.ElapsedEventArgs args)
        {
            Execute();
        }

        protected override void OnStop()
        {
            WriteMessage("O serviço foi parado.");

            timer.Stop();
        }

        private void WriteMessage(string message)
        {
            if (DebugMode == "1")
            {
                string file = GetFilePath();

                message = string.Format("{0:G}: {1}{2}", DateTime.Now, message, Environment.NewLine);
                File.AppendAllText(file, message);
            }
        }

        private void WriteError(string error)
        {
            error = "Error: " + error;
            WriteMessage(error);
        }

        private string GetFilePath()
        {
            string a = AppDomain.CurrentDomain.BaseDirectory;
            string path = string.Empty;
            path = a + "\\Log\\";

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            path = path + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";

            return path;
        }
    }

    partial class CRKSuitabilityService
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            this.ServiceName = "CRKSuitability";
        }

        #endregion
    }
}
