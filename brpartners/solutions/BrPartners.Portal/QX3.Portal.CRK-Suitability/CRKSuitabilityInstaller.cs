﻿using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;

namespace QX3.Portal.CRK_Suitability
{
    [RunInstaller(true)]
    public class CRKSuitabilityInstaller : Installer
    {
        public CRKSuitabilityInstaller()
        {
            ServiceProcessInstaller serviceProcessInstaller = new ServiceProcessInstaller();
            ServiceInstaller serviceInstaller = new ServiceInstaller();

            // Setup the Service Account type per your requirement
            serviceProcessInstaller.Account = ServiceAccount.LocalSystem;
            serviceProcessInstaller.Username = null;
            serviceProcessInstaller.Password = null;

            serviceInstaller.ServiceName = "CRK-Suitability";
            serviceInstaller.DisplayName = "CRK-Suitability Service";
            serviceInstaller.StartType = ServiceStartMode.Automatic;
            serviceInstaller.Description = "Bot responsável por atualizar a base do Portal com dados da CRK.";

            this.Installers.Add(serviceProcessInstaller);
            this.Installers.Add(serviceInstaller);
        }
    }
}
