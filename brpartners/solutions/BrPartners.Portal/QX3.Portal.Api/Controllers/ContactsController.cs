﻿using QX3.Portal.Api.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Repository;

namespace QX3.Portal.Api.Controllers
{
    public class ContactsController : Controller
    {
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IContactRepository _contactRepository;
        private readonly IEmployeeCostCenterRepository _employeeCostCenterRepository;

        public ContactsController(IEmployeeRepository employeeRepository, IContactRepository contactRepository, IEmployeeCostCenterRepository employeeCostCenterRepository)
        {
            this._employeeRepository = employeeRepository;
            this._contactRepository = contactRepository;
            this._employeeCostCenterRepository = employeeCostCenterRepository;
        }

        #region Private Methods

        private ContactModel EntityToDto(Employee entity, IEnumerable<EmployeeContact> contacts, EmployeeCostCenter costCenter)
        {
            ContactModel dto = new ContactModel();
            dto.Id = entity.Id;
            dto.Name = entity.Name;
            dto.Telephone = entity.MainPhone;

            if (contacts != null && contacts.Count() > 0)
            {
                foreach (var x in contacts)
                {
                    if (x.Type == "E-mail")
                        dto.Email = x.Value;

                    if (x.Type == "Celular Corporativo")
                        dto.CellPhone = x.Value;
                }
            }

            dto.CostCenter = costCenter.CostCenter.Name;

            return dto;
        }

        private List<ContactModel> Contacts(int employeeId = 0)
        {
            var result = new List<ContactModel>();

            List<Employee> data = null;

            if (employeeId == 0)
                data = _employeeRepository.All().Where(x => x.Active == true).OrderBy(x => x.Name).ToList();
            else
                data = _employeeRepository.SelectBy(x => x.Id == employeeId);

            if (data != null && data.Count() > 0)
            {
                var contacts = _contactRepository.SelectBy(e => e.DeletedDate == null && (e.Type == "E-mail" || e.Type == "Celular Corporativo"));
                var costCenters = _employeeCostCenterRepository.Query(e => e.DeletedDate == null).Include(c => c.CostCenter).ToList();

                foreach (var item in data)
                {
                    var employeeContacts = contacts.Where(x => x.EmployeeId == item.Id);
                    var currentCostCenter = costCenters.Where(x => x.EmployeeId == item.Id).OrderByDescending(x => x.StartDate).FirstOrDefault();

                    result.Add(EntityToDto(item, employeeContacts, currentCostCenter));
                }
            }

            return result;
        }

        #endregion

        [HttpGet]
        public JsonResult Employee(int id)
        {
            var result = Contacts(id);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult All()
        {
            var result = Contacts();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult Filtered(string costCenter)
        {
            var result = Contacts();

            if (!string.IsNullOrEmpty(costCenter))
            {
                costCenter = costCenter.Trim().ToLower();

                result = result.Where(x => x.CostCenter.ToLower() == costCenter).ToList();
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}
