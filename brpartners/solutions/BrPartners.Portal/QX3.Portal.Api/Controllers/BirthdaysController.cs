﻿using QX3.Portal.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Repository;

namespace QX3.Portal.Api.Controllers
{
    public class BirthdaysController : Controller
    {
        private readonly IEmployeeRepository _employeeRepository;

        public BirthdaysController(IEmployeeRepository employeeRepository)
        {
            this._employeeRepository = employeeRepository;
        }

        #region Private Methods

        private Expression<Func<Employee, BirthdayModel>> _convert = x =>
             new BirthdayModel()
             {
                 Id = x.Id,
                 Name = x.Name,
                 Birthdate = x.Birthdate.ToShortDateString()
             };

        private BirthdayModel EntityToDto(Employee entity)
        {
            BirthdayModel dto = new BirthdayModel();
            dto.Id = entity.Id;
            dto.Name = entity.Name;
            dto.Birthdate = entity.Birthdate.ToShortDateString();

            return dto;
        }

        #endregion

        [HttpGet]
        public JsonResult Employee(int id)
        {
            BirthdayModel result = new BirthdayModel();

            var data = _employeeRepository.SelectBy(x => x.Id == id).Single();
            if (data != null)
                result = EntityToDto(data);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult All()
        {
            var result = new List<BirthdayModel>();

            var data = _employeeRepository.All().Where(x => x.Active == true).OrderBy(x => x.Birthdate).ThenBy(x => x.Name);

            if (data != null && data.Count() > 0)
                foreach (var item in data)
                    result.Add(EntityToDto(item));

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult Filtered(int month, string orderColumn)
        {
            var result = new List<BirthdayModel>();

            var data = _employeeRepository.All().Where(x => x.Active == true);

            if (month != 0)
                data = data.Where(x => x.Birthdate.Month == month);

            if (data != null && data.Count() > 0)
            {
                if (!string.IsNullOrEmpty(orderColumn))
                {
                    orderColumn = orderColumn.Trim().ToLower();

                    if (orderColumn == "name")
                        data = data.OrderBy(x => x.Name);

                    if (orderColumn == "birthdate")
                        data = data.OrderBy(x => x.Birthdate);
                }

                foreach (var item in data)
                    result.Add(EntityToDto(item));
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}
