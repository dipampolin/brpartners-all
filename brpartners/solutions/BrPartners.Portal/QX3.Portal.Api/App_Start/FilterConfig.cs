﻿using System.Web;
using System.Web.Mvc;

namespace QX3.Portal.Api
{
    public class FilterConfig
    {
        public void FixEfProviderServicesProblem()
        {
            var instance = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }

        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        //public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        //{
        //    filters.Add(new JsonHandleErrorAttribute());
        //}
    }
}
