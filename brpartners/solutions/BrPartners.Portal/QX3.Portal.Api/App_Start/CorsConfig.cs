﻿using System.Web.Http;
using System.Web.Http.Cors;

namespace QX3.Portal.Api
{
    public class CorsConfig
    {
        public static void RegisterCors(HttpConfiguration httpConfig)
        {
            var corsAttr = new EnableCorsAttribute("*", "*", "*");
            httpConfig.EnableCors(corsAttr);
        }
    }
}