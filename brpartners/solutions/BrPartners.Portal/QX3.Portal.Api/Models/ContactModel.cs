﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QX3.Portal.Api.Models
{
    public class ContactModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string CostCenter { get; set; }

        public string Email { get; set; }

        public string Telephone { get; set; }

        public string CellPhone { get; set; }
    }
}