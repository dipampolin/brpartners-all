﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QX3.Portal.Api.Models
{
    public class BirthdayFilter
    {
        public int Month { get; set; }
        public string OrderColumn { get; set; }
        public string OrderDirection { get; set; }
    }
}