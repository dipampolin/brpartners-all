﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QX3.Portal.Api.Models
{
    public class BirthdayModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Birthdate { get; set; }

        // public string Picture { get; set; }
    }
}