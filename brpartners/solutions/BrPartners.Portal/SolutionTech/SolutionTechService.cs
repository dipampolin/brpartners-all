﻿using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.Services.Business;
using QX3.Portal.Services.Business.SolutionTech;
using QX3.Spinnex.Common.Services.Mail;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SolutionTech
{
    public partial class SolutionTechService : ServiceBase
    {
        private const int DEFAULT_MINUTES_BETWEEN_CYCLES = 10;
        private const int YEARS_UNTIL_EXPIRE_SUITABILITY = 2;
        private string MinutesBetweenCycles = ConfigurationManager.AppSettings["Minutes.Between.Bot.Cycles"];
        private string MailSubject = ConfigurationManager.AppSettings["Mail.Subject"];
        private string MailToBRP = ConfigurationManager.AppSettings["Mail.To"];
        private string DebugMode = ConfigurationManager.AppSettings["Debug"];

        private System.Timers.Timer timer;

        private List<SuitabilityToMail> suitabilityToMailList = new List<SuitabilityToMail>();
        private DateTime lastSendedDate;

        private SolutionTechMethods solutionTechMethods;
        private SuitabilityMethods suitabilityMethods;

        public SolutionTechService()
        {
            suitabilityMethods = new SuitabilityMethods();
            solutionTechMethods = new SolutionTechMethods();
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            WriteMessage("O serviço está no Windows Services. (Suitability-SolutionTech)");

            int minutesInMilliseconds = CalculateMinutesBetweenCycles();

            timer = new System.Timers.Timer(minutesInMilliseconds);
            timer.AutoReset = true;
            timer.Elapsed += new System.Timers.ElapsedEventHandler(this.OnTimer);
            timer.Start();
        }

        public void Start(string[] args)
        {
            WriteMessage("O serviço está no modo manual. (Suitability-SolutionTech)");

            int minutesInMilliseconds = CalculateMinutesBetweenCycles();

            while (true)
            {
                Execute();
                Thread.Sleep(minutesInMilliseconds);
            }
        }

        private bool IsSuitabilityExpired(DateTime suitabilityCompletionDate)
        {
            DateTime expiryDate = suitabilityCompletionDate.Date.AddYears(YEARS_UNTIL_EXPIRE_SUITABILITY);

            return expiryDate < DateTime.Today;
        }

        private int CalculateMinutesBetweenCycles()
        {
            int minutesBetweenCycles = int.TryParse(MinutesBetweenCycles, out minutesBetweenCycles) ? minutesBetweenCycles : DEFAULT_MINUTES_BETWEEN_CYCLES;
            int minutesInMilliseconds = minutesBetweenCycles * 60000;
            return minutesInMilliseconds;
        }

        //private async void Teste()
        //{
        //    try
        //    {
        //        DateTime testeInsertionDate = new DateTime(2020, 02, 21, 01, 22, 01, 000);
        //        var profile = new PerfilSuitability()
        //        {
        //            codBolsa = 100240,
        //            perfil = "MODERADO",
        //            totalPts = 0,
        //            CompletionDate = testeInsertionDate,
        //            NotifiedToBRP = false,
        //            UpdatedInSolutionTech = false
        //        };

        //        //var xmlStringMessage = XMLSerializer<PerfilSuitability>.Serialize(profile);
        //        //var content = new StringContent(xmlStringMessage, Encoding.UTF8, "application/xml");
        //        //var stringContent = content.ReadAsStringAsync().Result;
        //        //var servico = new SolutionTechSOAP.CadastroSimpleSoapClient();
        //        //Console.WriteLine("Enviando para SolutionTech");
        //        //var response = servico.AlterarPerfilSuitability(stringContent);
        //        //Console.WriteLine("Recebido!");
        //        //var incomingXml = XElement.Parse(response);

        //        XElement incomingXml = new XElement("Root",
        //            new XAttribute("Status", "200"),
        //            new XElement("Alert",
        //                new XElement("Message", "Sou uma mensagem")
        //            )
        //        );

        //        Console.WriteLine("Tratando XML de resposta");
        //        var profileResponse = GetResponseAttributes(incomingXml);
        //        Console.WriteLine("Gravando log no banco!");
        //        solutionTechMethods.RecordLog(profile.codBolsa, profile.perfil, profile.totalPts, profileResponse.ResponseStatus, profileResponse.MessageStatusTag, profileResponse.Message, profile.CompletionDate);
        //        Console.WriteLine("Adicionando a lista de email!");
        //        UpdateInSolutionTechOrAddToNotifyList(profile, profileResponse);
        //        Console.WriteLine("Enviando email!");
        //        NotifyByMailBRP();
        //    }
        //    catch (Exception ex)
        //    {
        //        string error = string.Format("{0} - {1} - {2}",
        //           ex.Message,
        //           ex.InnerException != null ? ex.InnerException.ToString() : "",
        //           ex.StackTrace);

        //        WriteError(error);
        //    }
        //    finally
        //    {
        //        lastSendedDate = DateTime.Today;
        //    }
        //}

        private void Execute()
        {
            try
            {
                WriteMessage("Está na hora de enviar resultados suitability para SolutionTech.");
                IntegrateInSolutionTech();

                //Teste();
            }
            catch (Exception ex)
            {
                string error = string.Format("{0} - {1} - {2}",
                   ex.Message,
                   ex.InnerException != null ? ex.InnerException.ToString() : "",
                   ex.StackTrace);

                WriteError(error);
            }
        }

        public void OnTimer(object sender, System.Timers.ElapsedEventArgs args)
        {
            Execute();
        }

        protected override void OnStop()
        {
            WriteMessage("O serviço foi parado. (Suitability-SolutionTech)");

            timer.Stop();
        }

        public async void IntegrateInSolutionTech()
        {
            try
            {
                //WriteMessage("Selecionando perfis e último histórico para envio...");
                var profilesList = GetSuitabilityProfiles();

                //WriteMessage("Enviando perfis, salvando log e selecionando cada um para atualização ou notificação...");
                foreach (var profile in profilesList)
                {
                    var profileResponse = await SendSuitabilityProfileToSolutionTech(profile);

                    solutionTechMethods.RecordLog(profile.codBolsa, profile.perfil, profile.totalPts, profileResponse.ResponseStatus, profileResponse.MessageStatusTag, profileResponse.Message, profile.CompletionDate);

                    UpdateInSolutionTechOrAddToNotifyList(profile, profileResponse);
                }

                NotifyByMailBRP();

                lastSendedDate = DateTime.Today;
            }
            catch (Exception ex)
            {
                string error = string.Format("{0} - {1} - {2}",
                   ex.Message,
                   ex.InnerException != null ? ex.InnerException.ToString() : "",
                   ex.StackTrace);

                WriteError(error);
            }
        }

        private void NotifyByMailBRP()
        {
            try
            {
                WriteMessage("Verificando necessidade de notificação por e-mail...");

                if (!suitabilityToMailList.Any())
                {
                    WriteMessage("A lista de registros necessários para notificação via email está vazia! Nenhum email será enviado à BRPartners.");
                    return;
                }
                else if (lastSendedDate.Date == DateTime.Today)
                {
                    WriteMessage("Já houve notificação para BRPartners no dia de hoje!");
                    suitabilityToMailList.Clear();
                    return;
                }

                WriteMessage($"Notificação necessária! Há {suitabilityToMailList.Count} registros a serem enviados.");

                var message = new StringBuilder($"Segue-se a lista de usuários não cadastrados na SolutionTech: \n\n\n");

                foreach (var suitabilityProfileAndResponse in suitabilityToMailList)
                {
                    message.Append($"Código Bolsa: {suitabilityProfileAndResponse.ProfileSuitability.codBolsa} \n");
                    message.Append($"Perfil de Investidor: {suitabilityProfileAndResponse.ProfileSuitability.perfil} \n");
                    message.Append($"Pontuação: {suitabilityProfileAndResponse.ProfileSuitability.totalPts} \n");
                    message.Append($"Data de preenchimento de Suitability: {suitabilityProfileAndResponse.ProfileSuitability.CompletionDate} \n");
                    message.Append($"Status Code recebido: {suitabilityProfileAndResponse.SolutionTechResponse.ResponseStatus} \n");
                    message.Append($"Mensagem recebida: {suitabilityProfileAndResponse.SolutionTechResponse.Message} \n\n");
                }

                string messageToSend = message.ToString();

                WriteMessage(messageToSend);

                SendMail(messageToSend, MailToBRP, null);

                foreach (var suitabilityProfileAndResponse in suitabilityToMailList)
                {
                    //WriteMessage($"Alterando status de NotificadoBRP do cliente de código bolsa: {suitabilityProfileAndResponse.ProfileSuitability.codBolsa}");
                    solutionTechMethods.NotifiedBRP(suitabilityProfileAndResponse.ProfileSuitability.codBolsa, suitabilityProfileAndResponse.ProfileSuitability.CompletionDate, true);
                }

                WriteMessage("Limpando lista de registros de notificação.");
            }
            catch (Exception ex)
            {
                string error = string.Format("{0} - {1} - {2}",
                   ex.Message,
                   ex.InnerException != null ? ex.InnerException.ToString() : "",
                   ex.StackTrace);

                WriteError(error);
            }
            finally
            {
                suitabilityToMailList.Clear();
            }  
        }

        private void UpdateInSolutionTechOrAddToNotifyList(PerfilSuitability profile, SolutionTechResponse profileResponse)
        {
            //WriteMessage($"Verificando se cliente de código bolsa {profile.codBolsa} será enviado à SolutionTech ou adicionado à lista para notificação.");

            var notRegisteredWithOkStatusCode = profileResponse.MessageStatusTag == "Alert" && profileResponse.ResponseStatus == 200;

            if (profileResponse.MessageStatusTag == "Info" && profileResponse.ResponseStatus == 200)
            {
                //WriteMessage("Cliente recebeu resposta com tag INFO e status code 200");
                solutionTechMethods.UpdatedSolutionTech(profile.codBolsa, profile.CompletionDate, true);
                return;
            }
            else if (notRegisteredWithOkStatusCode)
            {
                //WriteMessage("Cliente recebeu resposta com tag ALERT e status code 200");

                var suitabilityToMail = new SuitabilityToMail(profile, profileResponse);
                suitabilityToMailList.Add(suitabilityToMail);
                return;
            }
            else if (profileResponse.ResponseStatus == 500)
            {
                //WriteMessage("Cliente recebeu resposta com tag ERROR e status code 500");

                var logList = solutionTechMethods.GetLogsSolutionTech(profile.codBolsa);

                var firstAttemptOfTheLastSuitability =
                    (from log in logList
                    where log.SuitabilityDate == profile.CompletionDate
                    orderby log.SendDate ascending
                    select log)
                    .ToList()
                    .First();

                //var listOfAttempts = new List<SolutionTechLog>();

                //foreach (var log in logList)
                //{
                //    if (log.SuitabilityDate == profile.CompletionDate)
                //    {
                //        listOfAttempts.Add(log);
                //    }

                //    continue;
                //}

                //var firstAttempt = listOfAttempts.First();
                var oneDayAfterFirstAttempt = firstAttemptOfTheLastSuitability.SendDate.AddDays(1);

                var exceededOneDayOfTrying = oneDayAfterFirstAttempt <= DateTime.Now;

                if (!exceededOneDayOfTrying)
                {
                    return;
                }

                var suitabilityToMail = new SuitabilityToMail(profile, profileResponse);
                suitabilityToMailList.Add(suitabilityToMail);
                return;
            }

            throw new NotImplementedException("A SolutionTech enviou como resposta tag não reconhecida/implementada.");
        }

        private List<PerfilSuitability> GetSuitabilityProfiles()
        {
            List<PerfilSuitability> profilesList = new List<PerfilSuitability>();

            SuitabilityClientsResponse inputWithClientsList = suitabilityMethods.SelecionaClientesSuitability(null, null, null, null, null, null);
            var clientsList = inputWithClientsList.Clients;

            foreach (var client in clientsList)
            {
                //WriteMessage($"Selecionando cliente: {client.ClientName}");
                var codBolsa = client.CodigosBolsa.FirstOrDefault();

                if (codBolsa < 1000)
                {
                    continue;
                }

                var lastHistoric = GetLastSuitabilityHistoric(client);

                if (lastHistoric == null || lastHistoric.UpdatedSolutionTech)
                {
                    continue;
                }
                else if (IsSuitabilityExpired(lastHistoric.CompletionDate))
                {
                    continue;
                }

                var suitabilityProfile = new PerfilSuitability()
                {
                    codBolsa = codBolsa,
                    perfil = lastHistoric.InvestorTypeDesc,
                    totalPts = 0, // TO DO: EM BREVE ATRIBUIR PONTUAÇÃO QUANDO GUARDAR PONTUAÇÕES PARA PLATAFORMA DIGITAL
                    CompletionDate = lastHistoric.CompletionDate,
                    NotifiedToBRP = lastHistoric.NotifiedBRP,
                    UpdatedInSolutionTech = lastHistoric.UpdatedSolutionTech
                };

                profilesList.Add(suitabilityProfile);
            }

            return profilesList;
        }

        private SuitabilityClients GetLastSuitabilityHistoric(SuitabilityClients client)
        {
            //WriteMessage($"Selecionando histórico do cliente: {client.ClientName}");

            var stringCdBolsa = Convert.ToString(client.CodigosBolsa.First());

            var historicList = suitabilityMethods.HistoricoClientesSuitability(client.IdCliente, stringCdBolsa, client.Login);

            var lastHistoric = historicList.FirstOrDefault(); // A procedure devolve o histórico em ordem decrescente, portanto a primeira da lista é a mais atual.

            return lastHistoric;
        }

        private async Task<SolutionTechResponse> SendSuitabilityProfileToSolutionTech(PerfilSuitability profile)
        {
            //WriteMessage($"Enviando requisição para SolutionTech (Código Bolsa: {profile.codBolsa})");

            var xmlStringMessage = XMLSerializer<PerfilSuitability>.Serialize(profile);

            var content = new StringContent(xmlStringMessage, Encoding.UTF8, "application/xml");

            var stringContent = await content.ReadAsStringAsync();

            var servico = new SolutionTechSOAP.CadastroSimpleSoapClient();

            var response = servico.AlterarPerfilSuitability(stringContent);

            var incomingXml = XElement.Parse(response);

            var responseAttributes = GetResponseAttributes(incomingXml);

            return responseAttributes;
        }

        private SolutionTechResponse GetResponseAttributes(XElement incomingXml)
        {
            //WriteMessage("Coletando atributos da resposta XML...");

            var response = new SolutionTechResponse();

            var tagAndMessageNode = incomingXml.FirstNode;

            var tagAndMessageXElement = XElement.Parse(tagAndMessageNode.ToString());

            IEnumerable<XElement> elementsList = tagAndMessageXElement.DescendantsAndSelf();

            if (!elementsList.Any() || elementsList == null)
            {
                throw new Exception("Resposta vazia vinda da SolutionTech!");
            }

            var messageTypeTagName = elementsList.FirstOrDefault().Name.ToString();

            response.MessageStatusTag = messageTypeTagName;

            response.Message = incomingXml.Value;
            response.ResponseStatus = Convert.ToInt32(incomingXml.Attribute(response.StatusAttributeTag).Value);

            if (String.IsNullOrEmpty(response.MessageStatusTag))
            {
                response.Message = "A XML de resposta da SolutionTech não devolveu nenhuma das tag's pré-definidas na documentação que carregam a mensagem de resposta.";
            }

            return response;
        }

        private void SendMail(string message, string mailTo, string mailCopy)
        {
            SendMail sendEmail = new SendMail();
            sendEmail.Subject = MailSubject;
            sendEmail.EmailTo = mailTo;

            if (!string.IsNullOrEmpty(mailCopy))
                sendEmail.CC = mailCopy.Replace(';', ',');

            sendEmail.Body = message;
            sendEmail.IsBodyHtml = false;
            sendEmail.Send();
        }

        private void WriteMessage(string message)
        {
            if (DebugMode == "1")
            {
                string file = GetFilePath();

                message = string.Format("{0:G}: {1}{2}", DateTime.Now, message, Environment.NewLine);
                File.AppendAllText(file, message);
            }
        }

        private void WriteError(string error)
        {
            error = "Error: " + error;
            WriteMessage(error);
        }

        private string GetFilePath()
        {
            string a = AppDomain.CurrentDomain.BaseDirectory;
            string path = string.Empty;
            path = a + "\\Log\\";

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            path = path + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";

            return path;
        }
    }

    partial class SolutionTechService
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            this.ServiceName = "SolutionTechService";
        }
    }
}
