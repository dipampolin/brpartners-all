﻿using System;
using System.ServiceProcess;

namespace SolutionTech
{
    static class Program
    {
        static void Main(string[] args)
        {
            if ((!Environment.UserInteractive))
            {
                Program.RunAsAService();
            }
            else
            {
                if (args != null && args.Length > 0)
                {
                    if (args[0].Equals("-i", StringComparison.OrdinalIgnoreCase))
                    {
                        SelfInstaller.InstallMe();
                    }
                    else
                    {
                        if (args[0].Equals("-u", StringComparison.OrdinalIgnoreCase))
                        {
                            SelfInstaller.UninstallMe();
                        }
                        else
                        {
                            Console.WriteLine("Invalid argument!");
                        }
                    }
                }
                else
                {
                    Program.RunAsAConsole();
                }
            }
        }

        static void RunAsAConsole() // Executa como console app.
        {
            SolutionTechService solutionTechService = new SolutionTechService();
            solutionTechService.Start(null);
        }

        static void RunAsAService() // Executa como windows service.
        {
            ServiceBase[] servicesToRun = new ServiceBase[]
           {
                new SolutionTechService()
           };
            ServiceBase.Run(servicesToRun);
        }
    }
}
