﻿using CodeEngine.Framework.QueryBuilder;
using CodeEngine.Framework.QueryBuilder.Enums;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Representations;

namespace BrPartners.Portal.Test
{
    [TestFixture]
    public class ReportControllerTest
    {
        [Test]
        public void SouldCreateQuery()
        {
            var reportItem = new List<ReportItem>
            {
                // new ReportItem
                // {
                //    Id = 1,
                //    Field = "Name",
                //    FieldAlias = "Funcionário",
                //    Table = "Employee",
                //    TableAlias = "Funcionário",
                //    ReportId = 1
                // },
                // new ReportItem
                // {
                //    Id = 1,
                //    Field = "CostCenterDescription",
                //    FieldAlias = "Centro de Custo",
                //    Table = "Payslip",
                //    TableAlias = "Holerite",
                //    ReportId = 1
                // },
                //new ReportItem
                //{
                //    Id = 2,
                //    Field = "Code",
                //    FieldAlias = "Código",
                //    Table = "Company",
                //    TableAlias = "Empresa",
                //    ReportId = 1
                //},
                //new ReportItem
                //{
                //    Id = 2,
                //    Field = "Name",
                //    FieldAlias = "Nome",
                //    Table = "Company",
                //    TableAlias = "Empresa",
                //    ReportId = 1
                //}
                //,
                //new ReportItem
                //{
                //    Id = 2,
                //    Field = "Deadline",
                //    FieldAlias = "Dead Line",
                //    Table = "CourseEmployee",
                //    TableAlias = "Funcionário Curso",
                //    ReportId = 1
                //},
                //new ReportItem
                //{
                //    Id = 2,
                //    Field = "Name",
                //    FieldAlias = "Curso",
                //    Table = "Course",
                //    TableAlias = "Curso",
                //    ReportId = 1
                //}
                //,
                //new ReportItem
                //{
                //    Id = 2,
                //    Field = "Rubric",
                //    FieldAlias = "Rubrica",
                //    Table = "PayslipItem",
                //    TableAlias = "Itens do Holerite",
                //    ReportId = 1
                //}
                //,
                //new ReportItem
                //{
                //    Id = 2,
                //    Field = "Model",
                //    FieldAlias = "Modelo",
                //    Table = "Vehicle",
                //    TableAlias = "Veículo",
                //    ReportId = 1
                //}
                //,
                //new ReportItem
                //{
                //    Id = 2,
                //    Field = "Name",
                //    FieldAlias = "Nome CC",
                //    Table = "CostCenter",
                //    TableAlias = "Centro de Custo",
                //    ReportId = 1
                //}
                new ReportItem
                {
                    Id = 2,
                    Field = "Code",
                    FieldAlias = "Empresa-Código",
                    Table = "Company",
                    TableAlias = "Empresa",
                    ReportId = 1
                }
                //,
                //new ReportItem
                //{
                //    Id = 2,
                //    Field = "Code",
                //    FieldAlias = "Funcionário-Código",
                //    Table = "Employee",
                //    TableAlias = "Funcionário",
                //    ReportId = 1,
                //    Value = "6666"
                //}
            };


            //string test = CreateFilters(reportItem);

            //Console.WriteLine(test);

            //    var report = new Report
            //    {
            //        Id = 1,
            //        Description = "Relatório de funcionários",
            //        Name = "Funcionários",
            //        Items = reportItem
            //    };
            //    var query = GenerateFields(report);

            //    var test = TTTTT(report.Items.Select(c => c.Table).Distinct(), query);

            var t = NotificationRole.GetEnumDescription("EVERY_THREE_DAYS");
            Console.WriteLine(t);
        }

        private string CreateFilters(List<ReportItem> reportItem)
        {
            string query = "SELECT Employee.Code as 'Funcionário-Código', Employee.Name as 'Funcionário-Nome', Company.Code as 'Empresa-Código', Company.Name as 'Empresa-Nome' FROM rh.Employee INNER JOIN rh.Company ON Employee.CompanyId = rh.Company.Id WHERE Company.Code=3 AND Employee.Code=12";

            if (query.Contains("where") || query.Contains("WHERE"))
            {
                query = query.Remove(query.LastIndexOf("where", StringComparison.CurrentCultureIgnoreCase));
            }
            string filters = "";

            reportItem.ForEach(c =>
            {
                if (filters.Length == 0)
                    filters += "WHERE ";
                filters += string.Format("{0}.{1}={2} AND ", c.Table, c.Field);

            });

            if (filters.Trim().LastIndexOf("and ") > 0 || filters.Trim().LastIndexOf("AND ") > 0)
            {
                return string.Format("{0} {1}", query, filters.Remove(filters.LastIndexOf("and", StringComparison.CurrentCultureIgnoreCase)));
            }

            return string.Format("{0} {1}", query, filters.Length > 0 ? filters.Remove(filters.LastIndexOf("and", StringComparison.CurrentCultureIgnoreCase)) : "");
        }

        public string TTTTT(IEnumerable<string> table, string fields)
        {
            SelectQueryBuilder query = new SelectQueryBuilder();
            var selectedTable = table.OrderByDescending(c => c.Equals("Employee")).FirstOrDefault();
            query.SelectFromTable(string.Format("rh.{0}", selectedTable));
            var fieldList = fields.Split(',').ToList();

            query.SelectColumns(fields.Split(','));

            if (table.Contains("Employee") && table.Contains("Company"))
            {
                query.AddJoin(JoinType.InnerJoin,
                    "rh.Company", "Id",
                    Comparison.Equals,
                    "Employee", "CompanyId");
            }

            if (table.Contains("Employee") && table.Contains("Payslip"))
            {
                query.AddJoin(JoinType.InnerJoin,
                    "rh.Payslip", "EmployeeId",
                    Comparison.Equals,
                    "Employee", "Id");

                if (table.Contains("PayslipItem"))
                {
                    query.AddJoin(JoinType.InnerJoin,
                 "rh.PayslipItem", "PayslipId",
                 Comparison.Equals,
                 "Payslip", "Id");
                }

                if (table.Contains("CostCenter"))
                {
                    query.AddJoin(JoinType.InnerJoin,
                       "rh.CostCenter", "Id",
                       Comparison.Equals,
                       "Payslip", "CostCenterId");
                }
            }

            if (table.Contains("Employee") && table.Contains("CourseEmployee"))
            {
                query.AddJoin(JoinType.LeftJoin,
                    "rh.CourseEmployee", "EmployeeId",
                    Comparison.Equals,
                    "Employee", "Id");

                if (table.Contains("Course"))
                {

                    query.AddJoin(JoinType.LeftJoin,
                         "rh.Course", "Id",
                         Comparison.Equals,
                         "CourseEmployee", "CourseId");
                }
            }

            if (table.Contains("Employee") && table.Contains("Vehicle"))
            {
                query.AddJoin(JoinType.LeftJoin,
                    "rh.Vehicle", "EmployeeId",
                    Comparison.Equals,
                    "Employee", "Id");
            }
            var q = query.BuildQuery();
            return query.BuildQuery();
        }

        public string GenerateFields(Report report)
        {
            string query = "";
            foreach (var item in report.Items.OrderByDescending(c => c.Table.Equals("Employee")))
            {
                query += string.Format(" {0}.{1} as '{2}'{3}", item.Table, item.Field, item.FieldAlias, ",");
            }

            return query;
        }

        private string InsertJoin(ICollection<ReportItem> items, string fields)
        {
            var tables = items.Select(c => new { c.Table });
            var Joins = new List<String>();
            foreach (var item in tables)
            {
                Joins.Add(item.Table);
            }

            List<Type> objectType = (from asm in AppDomain.CurrentDomain.GetAssemblies()
                                     from type in asm.GetTypes()
                                     where type.IsClass && Joins.Contains(type.Name)
                                     select type).ToList();





            var objectLit = new List<object>();
            objectType.ForEach(c =>
            {
                objectLit.Add(Activator.CreateInstance(c));
            });

            var query = string.Format("SELECT {0} {1}", fields, GetJoins(fields));
            return "";
        }

        private static string GetJoins(string fields)
        {
            return string.Format("{0} {1} {2} {3} {4} {5} {6} {7}",
                    "SELECT",
                     fields,
                    "FROM rh.Employee Employee",
                    "LEFT JOIN rh.Company Company ON Employee.CompanyId = Company.Id",
                    "LEFT JOIN rh.Payslip Payslip ON Payslip.EmployeeId = Employee.Id",
                    "LEFT JOIN rh.CourseEmployee CourseEmployee ON CourseEmployee.EmployeeId = Employee.Id",
                    "LEFT JOIN rh.Course Course ON Course.Id = CourseEmployee.CourseId",
                    "LEFT JOIN rh.CostCenter CostCenter ON CostCenter.Id = Payslip.CostCenterId");
        }
    }
}
