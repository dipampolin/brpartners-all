﻿using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;

namespace Suitability_DigitalPlatform
{
    [RunInstaller(true)]
    public class SuitabilityDigitalPlatformInstaller : Installer
    {
        public SuitabilityDigitalPlatformInstaller()
        {
            ServiceProcessInstaller serviceProcessInstaller = new ServiceProcessInstaller();
            ServiceInstaller serviceInstaller = new ServiceInstaller();

            // Setup the Service Account type per your requirement
            serviceProcessInstaller.Account = ServiceAccount.LocalSystem;
            serviceProcessInstaller.Username = null;
            serviceProcessInstaller.Password = null;

            serviceInstaller.ServiceName = "Suitability-DigitalPlatform";
            serviceInstaller.DisplayName = "Suitability-DigitalPlatform Service";
            serviceInstaller.StartType = ServiceStartMode.Automatic;
            serviceInstaller.Description = "Serviço responsável por integrar todas novas mudanças de estado dos suitabilitys com o a API da Plataforma Digital.";

            this.Installers.Add(serviceProcessInstaller);
            this.Installers.Add(serviceInstaller);
        }
    }
}
