﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Suitability_DigitalPlatform
{
    public class NotEligibleRequest : SuitabilityDigitalPlatformRequest, ISuitabilityDigitalPlatformRequest
    {
        public override string statusProfile { get; set; }

        [JsonIgnore]
        public override string result { get; set; }
        [JsonIgnore]
        public override string expirationDate { get; set; }
        [JsonIgnore]
        public override int score { get; set; }

        public override string ToString()
        {
            return "NAO_ELEGIVEL";
        }
    }
}
