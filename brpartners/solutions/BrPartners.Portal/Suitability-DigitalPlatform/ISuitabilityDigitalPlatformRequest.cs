﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Suitability_DigitalPlatform
{
    public interface ISuitabilityDigitalPlatformRequest
    {
        string statusProfile { get; set; }

        string result { get; set; }

        string expirationDate { get; set; }

        int score { get; set; }

    }
}
