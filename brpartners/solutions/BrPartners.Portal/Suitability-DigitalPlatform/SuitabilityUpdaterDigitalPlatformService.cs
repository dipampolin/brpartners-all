﻿using Newtonsoft.Json;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.Services.Business;
using QX3.Portal.Services.Business.DigitalPlatform;
using QX3.Spinnex.Common.Services.Mail;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Suitability_DigitalPlatform
{
    public partial class SuitabilityUpdaterDigitalPlatformService : ServiceBase
    {
        private const int DEFAULT_MINUTES_BETWEEN_CYCLES = 10;
        private string MinutesBetweenCycles = ConfigurationManager.AppSettings["Minutes.Between.Bot.Cycles"];
        private string MailSubject = ConfigurationManager.AppSettings["Mail.Subject"];
        private string MailToBRP = ConfigurationManager.AppSettings["Mail.To"];
        private string TokenHeaderName = ConfigurationManager.AppSettings["API.Token.Header.Name"];
        private string TokenValue = ConfigurationManager.AppSettings["API.Token.Value"];
        private string DocumentHeaderName = ConfigurationManager.AppSettings["API.Document.Header.Name"];
        private string UserAgentValue = ConfigurationManager.AppSettings["API.User.Agent.Value"];
        private string EndpointUrl = ConfigurationManager.AppSettings["API.Endpoint.Url"];
        private string DebugMode = ConfigurationManager.AppSettings["Debug"];

        private System.Timers.Timer timer;

        private List<SuitabilityClients> overTryingSuitabilitys = new List<SuitabilityClients>();
        private DateTime lastMailSendedDate;

        private HttpClient _httpClient = new HttpClient();
        private SuitabilityMethods _suitabilityMethods = new SuitabilityMethods();
        private SuitabilityDigitalPlatformRequestFactory _requestFactory = new SuitabilityDigitalPlatformRequestFactory();
        private DigitalPlatformMethods _digitalPlatformMethods = new DigitalPlatformMethods();

        public SuitabilityUpdaterDigitalPlatformService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            WriteMessage("O serviço está no Windows Services.");

            int minutesInMilliseconds = CalculateMinutesBetweenCycles();

            timer = new System.Timers.Timer(minutesInMilliseconds);
            timer.AutoReset = true;
            timer.Elapsed += new System.Timers.ElapsedEventHandler(this.OnTimer);
            timer.Start();
        }

        public void Start(string[] args)
        {
            WriteMessage("O serviço está no modo manual.");

            int minutesInMilliseconds = CalculateMinutesBetweenCycles();

            while (true)
            {
                Execute();
                Thread.Sleep(minutesInMilliseconds);
            }
        }

        private int CalculateMinutesBetweenCycles()
        {
            int minutesBetweenCycles = int.TryParse(MinutesBetweenCycles, out minutesBetweenCycles) ? minutesBetweenCycles : DEFAULT_MINUTES_BETWEEN_CYCLES;
            int minutesInMilliseconds = minutesBetweenCycles * 60000;
            return minutesInMilliseconds;
        }

        private void Execute()
        {
            try
            {
                WriteMessage("Está na hora de enviar resultados suitability para Plataforma Digital.");
                IntegrateInDigitalPlatform();
            }
            catch (Exception ex)
            {
                string error = string.Format("{0} - {1} - {2}",
                   ex.Message,
                   ex.InnerException != null ? ex.InnerException.ToString() : "",
                   ex.StackTrace);

                WriteError(error);
            }
        }

        public async void IntegrateInDigitalPlatform()
        {
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12; // TALVEZ SEJA MELHOR COLOCAR NA CLASSE 'PROGRAM'
                List<SuitabilityClients> clients = GetClients();

                var clientsWithSuitabilityDate = GetLastSuitabilityHistoric(clients);

                if (!clientsWithSuitabilityDate.Any())
                {
                    WriteMessage("Não há atualizações para envio à Plataforma Digital!");
                    return;
                }

                WriteMessage($"Envio necessário! Há {clientsWithSuitabilityDate.Count} atualização(ões) de Suitability para a Plataforma Digital.");

                foreach (var client in clientsWithSuitabilityDate)
                {
                    ISuitabilityDigitalPlatformRequest request = _requestFactory.MakeRequest(client);

                    var response = await PostSuitabilityRequest(request, client);

                    if (response.IsSuccessStatusCode)
                    {
                        _digitalPlatformMethods.SetNeedToUpdateDigitalPlatform(client.IdCliente, false);
                        _digitalPlatformMethods.SetSendDateDigitalPlatform(client.IdCliente, client.CompletionDate);
                        continue;
                    }

                    if (!client.SendDateDigitalPlatform.HasValue)
                    {
                        _digitalPlatformMethods.SetSendDateDigitalPlatform(client.IdCliente, client.CompletionDate);
                        continue;
                    }

                    bool atLeastOneDayOfTrying = client.SendDateDigitalPlatform.Value.AddDays(1) <= DateTime.Now;

                    if (atLeastOneDayOfTrying)
                    {
                        overTryingSuitabilitys.Add(client);
                    }
                }

                NotifyByMailBRP();

                lastMailSendedDate = DateTime.Today;
            }
            catch (Exception ex)
            {
                string error = string.Format("{0} - {1} - {2}",
                   ex.Message,
                   ex.InnerException != null ? ex.InnerException.ToString() : "",
                   ex.StackTrace);

                WriteError(error);
            }
        }

        private void NotifyByMailBRP()
        {
            try
            {
                if (!overTryingSuitabilitys.Any())
                {
                    WriteMessage("A lista de registros necessários para notificação via email está vazia! Nenhum e-mail será enviado à BRPartners.");
                    return;
                }
                else if (lastMailSendedDate.Date == DateTime.Today)
                {
                    WriteMessage("Já houve notificação para BRPartners no dia de hoje!");
                    overTryingSuitabilitys.Clear();
                    return;
                }

                var message = new StringBuilder($"Segue-se lista de Suitabilitys de clientes que não conseguiram atualização na Plataforma Digital após 1 (um) dia inteiro de tentativas: \n\n\n");

                foreach (var client in overTryingSuitabilitys)
                {
                    message.Append($"ID Cliente: {client.IdCliente} \n");
                    message.Append($"Status do Suitability: {client.StatusDesc} \n");
                    message.Append($"Perfil de Investidor: {client.InvestorTypeDesc} \n");
                    message.Append($"Pontuação: {client.Score} \n");
                    message.Append($"Data de preenchimento de Suitability: {client.CompletionDate} \n\n\n");
                    //message.Append($"Status Code recebido: {response.StatusCode} \n");
                }

                string messageInString = message.ToString();

                WriteMessage(messageInString);

                SendMail(messageInString, MailToBRP, null);

                WriteMessage("Limpando lista de registros de notificação.");
            }
            catch (Exception ex)
            {
                string error = string.Format("{0} - {1} - {2}",
                   ex.Message,
                   ex.InnerException != null ? ex.InnerException.ToString() : "",
                   ex.StackTrace);

                WriteError(error);
            }
            finally
            {
                overTryingSuitabilitys.Clear();
            }  
        }

        public async Task<HttpResponseMessage> PostSuitabilityRequest(ISuitabilityDigitalPlatformRequest suitabilityRequest, SuitabilityClients client)
        {
            string cpfCnpjString = client.CPFCNPJ.ToString();

            var stringContent = ConvertRequestToJsonString(suitabilityRequest);
            stringContent.Headers.Add(TokenHeaderName, TokenValue);
            stringContent.Headers.Add(DocumentHeaderName, cpfCnpjString);

            _httpClient.DefaultRequestHeaders.UserAgent.ParseAdd(UserAgentValue);

            var response = await _httpClient
                .PostAsync(requestUri: EndpointUrl, stringContent);

            return response;
        }

        private StringContent ConvertRequestToJsonString(ISuitabilityDigitalPlatformRequest suitabilityRequest)
        {
            var json = JsonConvert.SerializeObject(suitabilityRequest);
            var jsonStringContent = new StringContent(json, Encoding.UTF8, mediaType: "application/json");

            return jsonStringContent;
        }

        private List<SuitabilityClients> GetClients()
        {
            List<SuitabilityClients> clients = null;

            SuitabilityMethods service = new SuitabilityMethods();

            var response = service.SelecionaClientesSuitability(null, null, null, null, null, null);
            clients = response.Clients;

            WriteMessage("Busca de clientes concluída!");
            return clients;
        }

        private List<SuitabilityClients> GetLastSuitabilityHistoric(IEnumerable<SuitabilityClients> clients)
        {
            List<SuitabilityClients> clientsWithLastSuitability = new List<SuitabilityClients>();
            foreach (var client in clients)
            {
                var historicList = _suitabilityMethods.HistoricoClientesSuitability(client.IdCliente, null, client.Login);
                var lastHistoric = historicList.FirstOrDefault(); // A procedure devolve o histórico em ordem decrescente, portanto a primeira da lista é a mais atual.

                if (lastHistoric == null)
                {
                    continue;
                }
                else if (!lastHistoric.NeedUpdateDigitalPlatform)
                {
                    continue;
                }

                client.Status = lastHistoric.Status;
                client.InvestorType = lastHistoric.InvestorType;
                client.CompletionDate = lastHistoric.CompletionDate;
                client.Score = lastHistoric.Score;
                client.NeedUpdateDigitalPlatform = lastHistoric.NeedUpdateDigitalPlatform;
                client.SendDateDigitalPlatform = lastHistoric.SendDateDigitalPlatform;

                clientsWithLastSuitability.Add(client);
            }

            WriteMessage("Busca e atribuição dos suitabilitys concluída!");
            return clientsWithLastSuitability;
        }

        public void OnTimer(object sender, System.Timers.ElapsedEventArgs args)
        {
            Execute();
        }

        protected override void OnStop()
        {
            WriteMessage("O serviço foi parado.");

            timer.Stop();
        }

        private void SendMail(string message, string mailTo, string mailCopy)
        {
            SendMail sendEmail = new SendMail();
            sendEmail.Subject = MailSubject;
            sendEmail.EmailTo = mailTo;

            if (!string.IsNullOrEmpty(mailCopy))
                sendEmail.CC = mailCopy.Replace(';', ',');

            sendEmail.Body = message;
            sendEmail.IsBodyHtml = false;
            sendEmail.Send();
        }

        private void WriteMessage(string message)
        {
            if (DebugMode == "1")
            {
                string file = GetFilePath();

                message = string.Format("{0:G}: {1}{2}", DateTime.Now, message, Environment.NewLine);
                File.AppendAllText(file, message);
            }
        }

        private void WriteError(string error)
        {
            error = "Error: " + error;
            WriteMessage(error);
        }

        private string GetFilePath()
        {
            string a = AppDomain.CurrentDomain.BaseDirectory;
            string path = string.Empty;
            path = a + "\\Log\\";

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            path = path + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";

            return path;
        }       
    }

    partial class SuitabilityUpdaterDigitalPlatformService
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            this.ServiceName = "Suitability-DigitalPlatform";
        }
    }
}