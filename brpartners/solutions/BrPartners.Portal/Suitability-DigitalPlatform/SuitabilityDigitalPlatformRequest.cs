﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Suitability_DigitalPlatform
{
    public abstract class SuitabilityDigitalPlatformRequest : ISuitabilityDigitalPlatformRequest
    {
        public abstract string statusProfile { get; set; }
        public abstract string result { get; set; }
        public abstract string expirationDate { get; set; }
        public abstract int score { get; set; }

        public abstract override string ToString();
    }
}
