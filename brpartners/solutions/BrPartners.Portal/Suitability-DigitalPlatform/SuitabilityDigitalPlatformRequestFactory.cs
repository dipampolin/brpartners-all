﻿using QX3.Portal.Contracts.DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static QX3.Portal.Services.Globals;

namespace Suitability_DigitalPlatform
{
    public class SuitabilityDigitalPlatformRequestFactory
    {
        private static IDictionary<int, ISuitabilityDigitalPlatformRequest> requests =
            new Dictionary<int, ISuitabilityDigitalPlatformRequest>()
            {
                { 2, new FilledInRequest() },
                { 3, new RemakeRequest() },
                { 4, new NotEligibleRequest() }
            };

        private ISuitabilityDigitalPlatformRequest ChooseRequestBasedInStatusSuitability(int clientSuitabilityStatus)
        {
            return requests[clientSuitabilityStatus];
        }

        private string GetExpirationSuitabilityDateInISOStandard(DateTime suitabilityCompletionDate)
        {
            string expirationDate = suitabilityCompletionDate
                .AddYears(YEARS_UNTIL_EXPIRE_SUITABILITY)
                .ToUniversalTime()
                .ToString("O");

            return expirationDate;
        }

        public ISuitabilityDigitalPlatformRequest MakeRequest(SuitabilityClients client)
        {
            ISuitabilityDigitalPlatformRequest request = ChooseRequestBasedInStatusSuitability(client.Status);

            request.statusProfile = request.ToString();
            request.result = client.InvestorTypeDesc.ToUpperInvariant();
            request.expirationDate = GetExpirationSuitabilityDateInISOStandard(client.CompletionDate);
            request.score = client.Score 
                ?? throw new ArgumentNullException("Score do Suitability de cliente não deve ser nulo ao gerar requisição para Plataforma Digital!");

            return request;
        }
    }
}
