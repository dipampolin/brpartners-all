﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Suitability_DigitalPlatform
{
    public class RemakeRequest : SuitabilityDigitalPlatformRequest, ISuitabilityDigitalPlatformRequest
    {
        public override string statusProfile { get; set; }
        public override string result { get; set; }
        public override string expirationDate { get; set; }
        public override int score { get; set; }

        public override string ToString()
        {
            return "REFAZER";
        }
    }
}
