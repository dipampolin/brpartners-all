﻿using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.Services.Business;
using QX3.Spinnex.Common.Services.Mail;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.ServiceProcess;
using System.Threading;

namespace SuitabilityMismatch
{
    public partial class SuitabilityMismatchService : ServiceBase
    {
        private string Schedule = ConfigurationManager.AppSettings["Suitability.Schedule"];
        private string VerifyAll = ConfigurationManager.AppSettings["Suitability.All"];
        private string MailSubject = ConfigurationManager.AppSettings["Mail.Subject"];
        private string MailTo = ConfigurationManager.AppSettings["Mail.To"];
        private string DebugMode = ConfigurationManager.AppSettings["Debug"];

        private System.Timers.Timer timer;

        public SuitabilityMismatchService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            WriteMessage("O serviço está no Windows Services.");

            timer = new System.Timers.Timer(60000);
            timer.AutoReset = true;
            timer.Elapsed += new System.Timers.ElapsedEventHandler(this.OnTimer);
            timer.Start();
        }

        public void Start(string[] args)
        {
            WriteMessage("O serviço está no modo manual.");

            while (true)
            {
                Execute();
                Thread.Sleep(60000);
            }
        }

        private void Execute()
        {
            try
            {
                // WriteMessage("Início da execução.");

                int scheduleHour = Convert.ToInt32(Schedule.Split(':')[0]);
                int scheduleMinute = Convert.ToInt32(Schedule.Split(':')[1]);

                WriteMessage("O serviço está configurado para o horário de " + Schedule + ".");

                DateTime now = DateTime.Now;

                if (now.Hour == scheduleHour && now.Minute == scheduleMinute && now.DayOfWeek != DayOfWeek.Saturday && now.DayOfWeek != DayOfWeek.Sunday)
                {
                    WriteMessage("Está na hora de buscar desenquadramentos no suitability.");
                    Search();
                }

                // WriteMessage("Fim da execução.");
            }
            catch (Exception ex)
            {
                string error = string.Format("{0} - {1} - {2}", 
                    ex.Message, 
                    ex.InnerException != null ? ex.InnerException.ToString() : "",
                    ex.StackTrace);

                WriteError(error);
            }
        }

        public void OnTimer(object sender, System.Timers.ElapsedEventArgs args)
        {
            Execute();
        }

        protected override void OnStop()
        {
            WriteMessage("O serviço foi parado.");

            timer.Stop();
        }

        public void Search()
        {
            WriteMessage("Procurando desenquadramentos...");

            int count = 0;
            string unframedContent = "";

            var clients = GetClients();
            if (clients != null && clients.Count > 0)
            {
                DateTime? day = GetDate();

                foreach (var client in clients)
                {
                    if (client.InvestorKind == 0 || client.PersonType == "F")
                    {
                        var mismatch = VerifyMismatch(client.IdCliente, day, day);
                        if (mismatch != null && mismatch.Length > 0)
                        {
                            unframedContent += FormatMail(client, mismatch);
                            count = count + 1;
                        }
                    }
                }
            }

            string message = count == 0 ? "Não houve desenquadramentos." : "Total de desenquadramentos: " + count.ToString();
            WriteMessage(message);

            SendMail(unframedContent);
        }

        private DateTime? GetDate()
        {
            if (VerifyAll == "0")
            {
                DateTime date = DateTime.Now;

                if (DateTime.Now.DayOfWeek == DayOfWeek.Monday)
                   date = date.AddDays(-3);
                else
                    date = date.AddDays(-1);

                WriteMessage("Data buscada: " + date.ToString("dd/MM/yyyy") + ".");

                return date;
            }
            else
                WriteMessage("Data buscada: todas.");

            return null;
        }

        private List<SuitabilityClients> GetClients()
        {
            List<SuitabilityClients> clients = null;

            SuitabilityMethods service = new SuitabilityMethods();

            var response = service.SelecionaClientesSuitability(null, null, null, null, null, null);
            clients = response.Clients;

            return clients;
        }

        private SuitabilityUnframedResponse[] VerifyMismatch(int clientId, DateTime? start, DateTime? end)
        {
            SuitabilityUnframedResponse[] unframed = null;

            SuitabilityMethods service = new SuitabilityMethods();

            var response = service.VerificaNovoDesenquadramento(clientId, start, end);

            unframed = response.ToArray();

            return unframed;
        }

        private string FormatMail(SuitabilityClients client, SuitabilityUnframedResponse[] unframed)
        {
            string format = string.Format("<b>{0} - {1}</b><br><br>", client.CPFCNPJ, client.ClientName);

            foreach (var item in unframed)
            {
                string list = "";

                foreach (var u in item.Unframeds)
                    list += string.Format("<li>{0} - {1}</li>", u.CdMercado, u.DtDatOrd.ToString("dd/MM/yyyy"));

                format += string.Format("<b>{0} - {1}</b><ul>{2}</ul><br>", item.InvestorTypeDesc, item.CompletionDate.ToString("d"), list);
            }

            return format + "<hr>";
        }

        private void SendMail(string body)
        {
            WriteMessage("Enviando o e-mail...");

            string message = "Resultado para a verificação do suitability ";

            if (VerifyAll == "0")
                message += "de " + DateTime.Now.AddDays(-1).ToShortDateString();

            if (string.IsNullOrEmpty(body))
                body = "Não houve desenquadros.";

            message = message + "<br><br><br>" + body;

            SendMail sendEmail = new SendMail();
            sendEmail.Subject = MailSubject;
            sendEmail.EmailTo = MailTo;
            sendEmail.Body = message;
            sendEmail.IsBodyHtml = true;
            sendEmail.Send();
        }

        private void WriteMessage(string message)
        {
            if (DebugMode == "1")
            {
                string file = GetFilePath();

                message = string.Format("{0:G}: {1}{2}", DateTime.Now, message, Environment.NewLine);
                File.AppendAllText(file, message);
            }
        }

        private string GetFilePath()
        {
            string a = AppDomain.CurrentDomain.BaseDirectory;

            string path = string.Empty;
            path = a + "\\Log\\";

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            path = path + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";

            return path;
        }

        private void WriteError(string error)
        {
            error = "Error: " + error;
            WriteMessage(error);
        }
    }

    partial class SuitabilityMismatchService
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            this.ServiceName = "SuitabilityMismatch";
        }

        #endregion
    }
}
