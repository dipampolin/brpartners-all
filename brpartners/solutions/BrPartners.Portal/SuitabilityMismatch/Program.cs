﻿using System;
using System.ServiceProcess;

namespace SuitabilityMismatch
{
    //static class Program
    //{
    //    static void Main(string[] args)
    //    {
    //        if (Environment.UserInteractive)
    //        {
    //            var service = new SuitabilityMismatchService();
    //            service.Start(args);
    //        }
    //        else
    //        {
    //            ServiceBase[] ServicesToRun;
    //            ServicesToRun = new ServiceBase[]
    //            {
    //                new SuitabilityMismatchService()
    //            };

    //            ServiceBase.Run(ServicesToRun);
    //        }
    //    }
    //}

    class Program
    {
        static void Main(string[] args)
        {
            if ((!Environment.UserInteractive))
            {
                Program.RunAsAService();
            }
            else
            {
                if (args != null && args.Length > 0)
                {
                    if (args[0].Equals("-i", StringComparison.OrdinalIgnoreCase))
                    {
                        SelfInstaller.InstallMe();
                    }
                    else
                    {
                        if (args[0].Equals("-u", StringComparison.OrdinalIgnoreCase))
                        {
                            SelfInstaller.UninstallMe();
                        }
                        else
                        {
                            Console.WriteLine("Invalid argument!");
                        }
                    }
                }
                else
                {
                    Program.RunAsAConsole();
                }
            }
        }

        static void RunAsAConsole()
        {
            SuitabilityMismatchService service = new SuitabilityMismatchService();
            service.Start(null);
        }

        static void RunAsAService()
        {
            ServiceBase[] servicesToRun = new ServiceBase[]
           {
                new SuitabilityMismatchService()
           };
            ServiceBase.Run(servicesToRun);
        }


    }
}
