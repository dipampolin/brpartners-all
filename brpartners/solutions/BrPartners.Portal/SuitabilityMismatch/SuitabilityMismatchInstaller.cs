﻿using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;

namespace SuitabilityMismatch
{
    [RunInstaller(true)]
    public class SuitabilityMismatchInstaller : Installer
    {
        public SuitabilityMismatchInstaller()
        {
            ServiceProcessInstaller serviceProcessInstaller = new ServiceProcessInstaller();
            ServiceInstaller serviceInstaller = new ServiceInstaller();

            // Setup the Service Account type per your requirement
            serviceProcessInstaller.Account = ServiceAccount.LocalSystem;
            serviceProcessInstaller.Username = null;
            serviceProcessInstaller.Password = null;

            serviceInstaller.ServiceName = "Suitability-Mismatch";
            serviceInstaller.DisplayName = "Suitability-Mismatch Service";
            serviceInstaller.StartType = ServiceStartMode.Automatic;
            serviceInstaller.Description = "Bot responsável por avisar os desenquadramentos no suitability.";

            this.Installers.Add(serviceProcessInstaller);
            this.Installers.Add(serviceInstaller);
        }
    }
}
