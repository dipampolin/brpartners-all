﻿using System;
using System.Xml;
namespace QX3.Spinnex.Trace.Services.Data
{
	public class TraceData
	{
		#region Propriedades

		public int ApplicationID { get; set; }
		public Guid TransactionID { get; set; }
		public Guid? ParentTransactionID { get; set; }
		public DateTime TimeStamp { get; set; }
		public int TraceType { get; set; }
		public string ModuleCodeName { get; set; }
		public string ServerIpAddress { get; set; }
		public string ClientIpAddress { get; set; }
		public string UserName { get; set; }
		public string UserID { get; set; }
		public string SessionID { get; set; }
		public string UserDisplayName { get; set; }
		public string ErrorMessage { get; set; }

		public XmlDocument XmlData { get; set; }

		#endregion
	}
}
