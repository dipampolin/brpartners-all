﻿using System;
using System.Configuration;
namespace QX3.Spinnex.Trace.Services.Data.Providers

{
	public abstract class TraceProviderBase
	{
		#region Atributos e Enumerados

		public enum TraceProviderType
		{
			MSSQL, Oracle
		}

		#endregion

		#region Construtores e Destrutores
		#endregion

		#region Sub-Classes e Estruturas
		#endregion

		#region Propriedades
		#endregion

		#region Métodos

		public abstract TraceApplication[] ListApplications();

		public static TraceProviderType GetCurrentProviderType()
		{

			string str = ConfigurationManager.AppSettings["CurrentTraceProvider"];

			if (str == null)
				throw new ConfigurationErrorsException("Parâmetro de configuração ausente: CurrentTraceProvider");

			TraceProviderType provider = (TraceProviderType) Enum.Parse(typeof(TraceProviderType), str);

			return provider;


		}

		public static TraceProviderBase CreateProvider()
		{
			TraceProviderType provider = GetCurrentProviderType();

			switch (provider)
			{
				case TraceProviderType.MSSQL:
					return new MSSQLTraceProvider();
				case TraceProviderType.Oracle:
					return new OracleTraceProvider();
				default:
					return null;
			}
		}
	
		public abstract void Connect();

		public abstract void Close();

		public abstract void InsertMinimalTrace(TraceData data);

		public abstract void InsertTraceData(TraceData data);

		public abstract TraceSearchResult[] Search(TraceSearchParameters searchParams);

		public abstract TraceModule[] ListModules(int applicationId);

		public abstract System.Xml.XmlDocument LoadTraceData(Guid transactionID);
		
		public abstract RelatedTrace[] LoadRelatedTraces(Guid parentTransactionID);
		
		#endregion

		
	}
}
