﻿using System.Data.SqlClient;
using System.Data.OracleClient;
using System.Configuration;
using System;
using System.IO;
using System.Xml;
using System.Text;
using System.Collections.Generic;
namespace QX3.Spinnex.Trace.Services.Data.Providers
{
    public class MSSQLTraceProvider : TraceProviderBase
    {
        #region Atributos e Enumerados
        private SqlConnection connection;
        #endregion

        #region Propriedades
        #endregion

        #region Métodos

        public override void Connect()
        {
            connection = new SqlConnection(ConfigurationManager.ConnectionStrings["CRK"].ConnectionString);
            connection.Open();
        }

        public SqlParameter CreateParam(string name, object value)
        {
            if (value == null)
                value = DBNull.Value;
            else if (typeof(Guid) == value.GetType())
                value = value.ToString();
            var p = new SqlParameter(name, value);

            return p;
        }

        public override TraceApplication[] ListApplications()
        {
            string sql = "SELECT * FROM APPLICATION";
            List<TraceApplication> list = new List<TraceApplication>();

            var cmd = connection.CreateCommand();
            cmd.CommandText = sql;
            var reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                TraceApplication app = new TraceApplication()
                {
                    ApplicationID = reader.GetInt32(0),
                    ApplicationCodeName = reader.GetString(1).Trim(),
                    Name = reader.GetString(2).Trim()
                };
                list.Add(app);
            }
            reader.Close();

            return list.ToArray();

        }

        public override void InsertMinimalTrace(TraceData data)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(data.XmlData.OuterXml);

            XmlNodeList requestNodes = doc.SelectNodes("log/request/server-variables/item");
            var clientIp = "";
            foreach (XmlNode item in requestNodes)
            {
                switch (item.FirstChild.FirstChild.Value)
                {
                    case "HTTPS":
                    case "LOCAL_ADDR":
                        continue;
                    case "REMOTE_ADDR":
                    case "HTTP_X_FORWARDED_FOR":
                        clientIp = item.FirstChild.FirstChild.Value;
                        continue;
                    default:
                        item.ParentNode.RemoveChild(item);
                        break;
                }
            }

            requestNodes = doc.SelectNodes("log/session/item");
            foreach (XmlNode item in requestNodes)
            {
                if (item.FirstChild.FirstChild.Value != "MACSYSTOKEN")
                    item.ParentNode.RemoveChild(item);
            }

            string sql = "INSERT INTO TRACE VALUES (:TIMESTAMP, :TRANSACTIONID, :PARENTTRANSACTIONID, :APPLICATIONID, :MODULENAME, :SESSIONID, :SERVERIP, :CLIENTIP, :TRACETYPE, :USERID, :USERNAME, :DISPLAYNAME, :ERRORMSG) ";
            var cmd = connection.CreateCommand();
            cmd.CommandText = sql;

            if (string.IsNullOrEmpty(clientIp))
                clientIp = data.ClientIpAddress;

            cmd.Parameters.Add(CreateParam("TIMESTAMP", data.TimeStamp));
            cmd.Parameters.Add(CreateParam("TRANSACTIONID", data.TransactionID));
            cmd.Parameters.Add(CreateParam("PARENTTRANSACTIONID", data.ParentTransactionID));
            cmd.Parameters.Add(CreateParam("APPLICATIONID", data.ApplicationID));
            cmd.Parameters.Add(CreateParam("MODULENAME", data.ModuleCodeName));
            cmd.Parameters.Add(CreateParam("SESSIONID", data.SessionID));
            cmd.Parameters.Add(CreateParam("SERVERIP", data.ServerIpAddress));
            cmd.Parameters.Add(CreateParam("CLIENTIP", clientIp));
            cmd.Parameters.Add(CreateParam("TRACETYPE", data.TraceType));
            cmd.Parameters.Add(CreateParam("USERID", data.UserID));
            cmd.Parameters.Add(CreateParam("USERNAME", data.UserName));
            cmd.Parameters.Add(CreateParam("DISPLAYNAME", data.UserDisplayName));
            cmd.Parameters.Add(CreateParam("ERRORMSG", data.ErrorMessage));

            cmd.ExecuteNonQuery();

            cmd = connection.CreateCommand();
            cmd.CommandText = "INSERT INTO TRACEDATA  VALUES (:a, :b, :c)";
            cmd.Parameters.Add(CreateParam("a", data.TimeStamp));
            cmd.Parameters.Add(CreateParam("b", data.TransactionID));
            cmd.Parameters.Add(CreateParam("c", doc.OuterXml));

            cmd.ExecuteNonQuery();
        }

        public override void InsertTraceData(TraceData data)
        {
            string sql = "INSERT INTO TRACE VALUES (:TIMESTAMP, :TRANSACTIONID, :PARENTTRANSACTIONID, :APPLICATIONID, :MODULENAME, :SESSIONID, :SERVERIP, :CLIENTIP, :TRACETYPE, :USERID, :USERNAME, :DISPLAYNAME, :ERRORMSG) ";
            var cmd = connection.CreateCommand();
            cmd.CommandText = sql;

            cmd.Parameters.Add(CreateParam("TIMESTAMP", data.TimeStamp));
            cmd.Parameters.Add(CreateParam("TRANSACTIONID", data.TransactionID));
            cmd.Parameters.Add(CreateParam("PARENTTRANSACTIONID", data.ParentTransactionID));
            cmd.Parameters.Add(CreateParam("APPLICATIONID", data.ApplicationID));
            cmd.Parameters.Add(CreateParam("MODULENAME", data.ModuleCodeName));
            cmd.Parameters.Add(CreateParam("SESSIONID", data.SessionID));
            cmd.Parameters.Add(CreateParam("SERVERIP", data.ServerIpAddress));
            cmd.Parameters.Add(CreateParam("CLIENTIP", data.ClientIpAddress));
            cmd.Parameters.Add(CreateParam("TRACETYPE", data.TraceType));
            cmd.Parameters.Add(CreateParam("USERID", data.UserID));
            cmd.Parameters.Add(CreateParam("USERNAME", data.UserName));
            cmd.Parameters.Add(CreateParam("DISPLAYNAME", data.UserDisplayName));
            cmd.Parameters.Add(CreateParam("ERRORMSG", data.ErrorMessage));

            cmd.ExecuteNonQuery();

            cmd = connection.CreateCommand();
            cmd.CommandText = "INSERT INTO TRACEDATA  VALUES (:a, :b, :c)";
            cmd.Parameters.Add(CreateParam("a", data.TimeStamp));
            cmd.Parameters.Add(CreateParam("b", data.TransactionID));
            cmd.Parameters.Add(CreateParam("c", data.XmlData.OuterXml));

            cmd.ExecuteNonQuery();
        }

        public override void Close()
        {
            if (connection != null)
                connection.Close();
            connection = null;
        }

        public override TraceSearchResult[] Search(TraceSearchParameters searchParams)
        {

            string sql = @"SELECT TRANSACTIONID, TIMESTAMP, B.NAME AS MODULECODENAME, A.USERID, A.USERNAME, A.USERDISPLAYNAME FROM TRACE A
                            JOIN MODULE B ON A.MODULECODENAME = B.MODULECODENAME
                            WHERE	A.APPLICATIONID = :APPLICATIONID 
                            AND		(:STARTDATE IS NULL OR TIMESTAMP >= :STARTDATE) AND (:ENDDATE IS NULL OR TIMESTAMP <= (:ENDDATE))
                            AND		(:USERID IS NULL OR USERID = :USERID)
                            AND		(:USERNAME IS NULL OR USERNAME = :USERNAME)
                            AND		(:MODULECODENAME IS NULL OR RTRIM(A.MODULECODENAME) = RTRIM(:MODULECODENAME))";

            if (searchParams.StartDate == null && searchParams.EndDate == null)
                sql += " AND ROWNUM <= 100 ";

            sql += "ORDER BY TIMESTAMP DESC";

            var cmd = connection.CreateCommand();
            cmd.CommandText = sql;
            cmd.Parameters.Add(CreateParam("APPLICATIONID", searchParams.ApplicationID));
            cmd.Parameters.Add(CreateParam("STARTDATE", searchParams.StartDate));
            cmd.Parameters.Add(CreateParam("ENDDATE", searchParams.EndDate));
            cmd.Parameters.Add(CreateParam("USERID", searchParams.UserID));
            cmd.Parameters.Add(CreateParam("USERNAME", searchParams.UserName));
            cmd.Parameters.Add(CreateParam("MODULECODENAME", searchParams.ModuleCodeName));

            var reader = cmd.ExecuteReader();
            List<TraceSearchResult> list = new List<TraceSearchResult>();
            while (reader.Read())
            {
                TraceSearchResult item = new TraceSearchResult()
                {
                    TransactionID = new Guid(reader.GetString(0)),
                    TimeStamp = reader.GetDateTime(1),
                    ModuleName = reader.GetString(2),
                    UserID = reader["USERID"].ToString().Trim(),
                    UserName = reader["USERNAME"].ToString().Trim(),
                    DisplayName = reader["USERDISPLAYNAME"].ToString().Trim()

                };

                list.Add(item);

            }
            reader.Close();

            return list.ToArray();
        }

        public override TraceModule[] ListModules(int applicationId)
        {
            List<TraceModule> list = new List<TraceModule>();

            var cmd = connection.CreateCommand();
            cmd.CommandText = @"SELECT MODULECODENAME, NAME FROM MODULE WHERE APPLICATIONID = :APPLICATIONID";
            cmd.Parameters.Add(CreateParam("APPLICATIONID", applicationId));

            var reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                var item = new TraceModule()

                {
                    ApplicationID = applicationId,
                    ModuleCodeName = reader.GetString(0).Trim(),
                    Name = reader.GetString(1).Trim()
                };
                list.Add(item);
            }
            return list.ToArray();

        }

        public override XmlDocument LoadTraceData(Guid transactionID)
        {
            string sql = @"SELECT DATA FROM TRACEDATA WHERE TRANSACTIONID = :TRANSACTIONID";

            var cmd = connection.CreateCommand();
            cmd.CommandText = sql;
            cmd.Parameters.Add(CreateParam("TRANSACTIONID", transactionID.ToString()));

            var reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                string data = reader.GetString(0);
                XmlDocument document = new XmlDocument();
                document.LoadXml(data);
                return document;
            }
            return null;
        }

        public override RelatedTrace[] LoadRelatedTraces(Guid parentTransactionID)
        {
            List<RelatedTrace> list = new List<RelatedTrace>();

            string sql = "SELECT A.TRANSACTIONID, B.NAME AS MODULENAME, C.NAME AS APPLICATIONNAME FROM TRACE A JOIN MODULE B ON A.MODULECODENAME = B.MODULECODENAME JOIN APPLICATION C ON A.APPLICATIONID = C.APPLICATIONID WHERE RTRIM(A.PARENTTRANSACTIONID) = RTRIM(:GUID) ORDER BY TIMESTAMP ASC";
            var cmd = connection.CreateCommand();
            cmd.CommandText = sql;
            cmd.Parameters.Add(CreateParam("GUID", parentTransactionID.ToString()));

            var reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                var item = new RelatedTrace()
                {
                    TransactionID = new Guid(reader.GetString(0)),
                    ModuleName = reader.GetString(1),
                    ApplicationName = reader.GetString(2)
                };
                list.Add(item);
            }
            return list.ToArray();
        }

        #endregion

    }
}
