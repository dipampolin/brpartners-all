﻿using System;
namespace QX3.Spinnex.Trace.Services.Data
{
	public class RelatedTrace
	{
		#region Atributos e Enumerados
		#endregion

		#region Construtores e Destrutores
		#endregion

		#region Sub-Classes e Estruturas
		#endregion

		#region Propriedades


		public Guid TransactionID { get; set; }
		public string ModuleName { get; set; }
		public string ApplicationName { get; set; }

		#endregion

		#region Métodos
		#endregion
	}
}
