﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QX3.Spinnex.Trace.Services.Data
{

	public class TraceModuleHierarchy
	{
		public TraceModuleCollection Modules { get; set; }
		public TraceModule[] OrphanModules { get; set; }

		public static TraceModuleHierarchy Create(IQueryable<TraceModule> data)
		{
			data = data.ToArray().AsQueryable();
			TraceModuleHierarchy tmh = new TraceModuleHierarchy();
			tmh.Modules = TraceModuleCollection.Create(data, null);

			tmh.OrphanModules = FindOrphanedModules(data);

			return tmh;
		}

		private static TraceModule[] FindOrphanedModules(IQueryable<TraceModule> data)
		{
			var orphans = (from m in data
						   where m.ParentModuleCodeName !=null &&
							   (from mParent in data where m.ParentModuleCodeName == mParent.ModuleCodeName select mParent).Count() == 0
						   select m);
			return orphans.ToArray();
		}

	}


	public class TraceModuleCollection
	{
		public class TraceModuleCollectionItem
		{
			public TraceModule Value { get; set; }
			public TraceModuleCollection Children { get; set; }
		}




		public TraceModuleCollectionItem[] Items { get; set; }

		
		public  static TraceModuleCollection Create(IQueryable<TraceModule> data, string parentModule)
		{
			TraceModuleCollection tmc = new TraceModuleCollection();
			tmc.Items = (from m in data where m.ParentModuleCodeName == parentModule select new TraceModuleCollectionItem() { Value = m }).ToArray();

			foreach (TraceModuleCollectionItem item in tmc.Items)
			{
				item.Children = Create(data, item.Value.ModuleCodeName);
			}

			
			return tmc;
		}

		
		
	}
}
