﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QX3.Spinnex.Trace.Services.Data
{
	public enum ChildTransactionHandlingMode
	{
		Default, Include, Exclude
	}
	public class TraceSearchParameters
	{

		

		public int ApplicationID { get; set; }
		public string ModuleCodeName { get; set; }
		public string SessionID { get; set; }
		public Guid? TransactionID { get; set; }
		public Guid? ParentTransactionID { get; set; }
		public DateTime? StartDate { get; set; }
		public DateTime? EndDate { get; set; }
		public int? TraceTypeID { get; set; }
		public string ErrorMessage { get; set; }
		public string ServerIpAddress { get; set; }
		public string ClientIpAddress { get; set; }
		public string UserID { get; set; }
		public string UserName { get; set; }
		public ChildTransactionHandlingMode ChildTransactionHandling { get; set; }
	}

	public class TraceSearchResult
	{
		public Guid TransactionID { get; set; }
		public DateTime TimeStamp { get; set; }
		public string TraceTypeDescription { get; set; }
		public string ServerName { get; set; }
		public string ClientIpAddress { get; set; }
		public string UserID { get; set; }
		public string UserName { get; set; }
		public string DisplayName { get; set; }
		public string ErrorMessage { get; set; }
		public string ModuleName { get; set; }

		
	}
}
