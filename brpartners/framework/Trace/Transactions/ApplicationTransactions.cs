﻿using QX3.Spinnex.Trace.Services.Data;
using QX3.Spinnex.Trace.Services.Data.Providers;
namespace QX3.Spinnex.Trace.Services.Transactions
{
	public class ApplicationTransactions
	{
		#region Atributos e Enumerados
		#endregion

		#region Construtores e Destrutores
		#endregion

		#region Sub-Classes e Estruturas
		#endregion

		#region Propriedades
		#endregion

		#region Métodos

		public TraceApplication[] ListApplications()
		{
			TraceProviderBase provider = TraceProviderBase.CreateProvider();
			try
			{
				provider.Connect();
				return provider.ListApplications();
			}
			finally
			{
				provider.Close();
			}
		}

		#endregion
	}
}
