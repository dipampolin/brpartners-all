﻿using QX3.Spinnex.Trace.Services.Data;
using QX3.Spinnex.Trace.Services.Data.Providers;
namespace QX3.Spinnex.Trace.Services.Transactions
{
	public class ModuleTransactions
	{
		

		#region Métodos

		public TraceModule[] ListModules(int applicationId)
		{
			TraceProviderBase provider = TraceProviderBase.CreateProvider();
			try
			{
				provider.Connect();
				return provider.ListModules(applicationId);
			}
			finally
			{
				provider.Close();
			}
		}

		#endregion
	}
}
