﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml;
using System.IO;
using QX3.Spinnex.Trace.Services.Data;
using QX3.Spinnex.Trace.Services.Data.Providers;
using System.Web;
using QX3.Spinnex.Common.Services.Logging;

namespace QX3.Spinnex.Trace.Services.Transactions
{
	public partial class TraceTransactions 
	{
		public void CreateTransaction(XmlDocument data)
		{
			Guid transactionID = Guid.Empty;
			Guid? parentTransactionID = null;
			DateTime timestamp = DateTime.Now;
			string sessionID = null;
			string userID = null;
			string username = null;
			string userDisplayName = null;
			string errorMessage = null;

			string serverIpAddress = null;
			string clientIpAddress = null;

			string applicationID = null;
			string moduleName = null;

			var logNode = data.DocumentElement.SelectSingleNode("/log");

			foreach (XmlAttribute att in logNode.Attributes)
			{
				switch (att.Name)
				{
					case "applicationId": applicationID = att.Value; break;
					case "module": moduleName = att.Value; break;
				}
			}

			foreach (XmlNode node in logNode.ChildNodes)
			{
				switch (node.Name)
				{
					case "transaction-id": transactionID = new Guid(node.FirstChild.Value); break;
					case "parent-transaction-id":
						parentTransactionID = new Guid(node.FirstChild.Value);
						if (parentTransactionID == Guid.Empty)
							parentTransactionID = null;
						break;
					case "timestamp": timestamp = DateTime.Parse(node.FirstChild.Value); break;
					case "session": sessionID = ReadSessionID(node); break;
					case "user-information": ReadUserInformation(node, ref userID, ref username, ref userDisplayName); break;
					case "error": ReadError(node, ref errorMessage); break;
					case "request":
						ReadRequest(node, ref serverIpAddress, ref clientIpAddress); break;
				}
			}

			bool hasErrors = data.SelectNodes("/log/error-message").Count > 0;

			TraceData traceData = new TraceData()
			{
				ClientIpAddress = clientIpAddress,
				ErrorMessage = errorMessage,
				ModuleCodeName = moduleName,
				ParentTransactionID = parentTransactionID,
				ServerIpAddress = serverIpAddress,
				SessionID = sessionID,
				TimeStamp = timestamp,
				TraceType = hasErrors ? 2 : 1,
				TransactionID = transactionID,
				UserDisplayName = userDisplayName,
				UserID = userID,
				UserName = username,
				XmlData = data
			};

			TraceProviderBase provider = TraceProviderBase.CreateProvider();

			try
			{
				provider.Connect();
				string cachekey = "Applications";
				var applications = (TraceApplication[])HttpContext.Current.Cache[cachekey];
				if (applications == null)
				{
					applications = provider.ListApplications();
					HttpContext.Current.Cache.Add(cachekey, applications, null, DateTime.MaxValue, TimeSpan.FromMinutes(5), System.Web.Caching.CacheItemPriority.Normal, null);
				}

				traceData.ApplicationID = (from a in applications.AsQueryable() where a.ApplicationCodeName == applicationID select a.ApplicationID).First();

				string traceMode = System.Configuration.ConfigurationManager.AppSettings["TraceMode"];
				switch (traceMode)
				{
					case "MINIMAL":
						provider.InsertMinimalTrace(traceData);
						break;
					default:
						provider.InsertTraceData(traceData);
						break;
				}
			}
			catch (InvalidOperationException)
			{

			}
			catch (Exception ex)
			{
				LogManager.WriteError(ex);
				throw new Exception("Erro ao registrar trace", ex);
			}
			finally
			{
				provider.Close();

			}
		}

		private void ReadRequest(XmlNode requestNode, ref string serverIpAddress, ref string clientIpAddress)
		{
			foreach (XmlNode node in requestNode.SelectNodes("server-variables/item"))
			{
				switch (node.FirstChild.FirstChild.Value)
				{
					case "LOCAL_ADDR":
						serverIpAddress = node.LastChild.FirstChild.Value; break;
					case "REMOTE_ADDR":
						clientIpAddress = node.LastChild.FirstChild.Value; break;
				}
			}
		}

		private void ReadError(XmlNode parent, ref string errorMessage)
		{
			errorMessage = "";
			XmlNodeList xl = parent.SelectNodes("exception/message");
			foreach (XmlNode node in xl)
				errorMessage += node.FirstChild.Value + "\n\n";
		}

		private void ReadUserInformation(XmlNode parent, ref string userID, ref string username, ref string userDisplayName)
		{
			foreach (XmlNode node in parent.ChildNodes)
				if (node.HasChildNodes)
				{
					switch (node.Name)
					{
						case "user-id": userID = node.FirstChild.Value; break;
						case "user-name": username = node.FirstChild.Value; break;
						case "display-name": userDisplayName = node.FirstChild.Value; break;
					}
				}
		}

		private string ReadSessionID(XmlNode parent)
		{
			foreach (XmlNode node in parent.ChildNodes)
				switch (node.Name)
				{
					case "session-id": return node.FirstChild.Value;
				}
			return null;
		}

		public TraceSearchResult[] Search(TraceSearchParameters searchParams)
		{
			TraceProviderBase provider = TraceProviderBase.CreateProvider();
			try
			{
				provider.Connect();
				return provider.Search(searchParams);
			}
			finally
			{
				provider.Close();
			}
		}

		public XmlDocument LoadTraceData(Guid transactionID)
		{
			TraceProviderBase provider = TraceProviderBase.CreateProvider();
			try
			{
				provider.Connect();
				return provider.LoadTraceData(transactionID);
			}
			finally
			{
				provider.Close();
			}
		}

		public RelatedTrace[] LoadRelatedTraces(Guid transactionID)
		{
			TraceProviderBase provider = TraceProviderBase.CreateProvider();
			try
			{
				provider.Connect();
				return provider.LoadRelatedTraces(transactionID);
			}
			finally
			{
				provider.Close();
			}

		}
	}
}
