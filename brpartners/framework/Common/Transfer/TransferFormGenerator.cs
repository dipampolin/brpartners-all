﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace QX3.Spinnex.Common.Services.Transfer
{
    #region Atributos e Enumerados
    /// <summary>
    /// Enum com os tipos de comportamentos da janela do homebroker.
    /// </summary>
    public enum FormSubmissionType
    {
        Default, Popup, ST4
    }
    #endregion

    public class TransferFormGenerator
    {
        #region Propriedades
        /// <summary>
        /// Largura da janela Popup
        /// </summary>
        public string PopupWidth { get; set; }
        /// <summary>
        /// Altura da janela Popup
        /// </summary>
        public string PopupHeight { get; set; }
        /// <summary>
        /// Nome da instância da janela Popup
        /// </summary>
        public string PopupName { get; set; }
        /// <summary>
        /// Habilita a opção fullscreen(ie)
        /// </summary>
        public bool PopupFullscreen { get; set; }
        /// <summary>
        /// Habilita o redimensionamento da janela
        /// </summary>
        public bool Resizable { get; set; }
		public bool ScrollBars { get; set; }


		public string PopupBlockedUrl { get; set; }



        #endregion

        #region Métodos
        public string GenerateForm(string token, string url, FormSubmissionType type)
        {
            string template = "";

            switch (type)
            {
                case FormSubmissionType.Default:
                    template = TransferFormGeneratorResources.TransferFormTemplate;
                    template = template.Replace("$ACTION$", url);
                    template = template.Replace("$TOKEN$", token);
                    break;
                case FormSubmissionType.Popup:
                    template = TransferFormGeneratorResources.TransferFormTemplatePopup;
                    template = template.Replace("$POPUPWIDTH$", PopupWidth);
                    template = template.Replace("$POPUPHEIGHT$", PopupHeight);
					template = template.Replace("$POPUPNAME$", string.Concat(PopupName,DateTime.Now.ToFileTime()));
					template = template.Replace("$FULLSCREEN$", PopupFullscreen ? "1" : "0");
					template = template.Replace("$SCROLLBARS$", ScrollBars ? "1" : "0");
                    template = template.Replace("$RESIZABLE$", Resizable ? "yes" : "no");
					template = template.Replace("$POPUPBLOCKEDURL", PopupBlockedUrl);
                    template = template.Replace("$ACTION$", url);
                    template = template.Replace("$TOKEN$", token);
                    break;
                case FormSubmissionType.ST4:
                    template = TransferFormGeneratorResources.TransferFormTemplateST4;
                    template = template.Replace("$POPUPWIDTH$", PopupWidth);
                    template = template.Replace("$POPUPHEIGHT$", PopupHeight);
                    template = template.Replace("$POPUPNAME$", string.Concat(PopupName, DateTime.Now.ToFileTime()));
                    template = template.Replace("$FULLSCREEN$", PopupFullscreen ? "1" : "0");
                    template = template.Replace("$SCROLLBARS$", ScrollBars ? "1" : "0");
                    template = template.Replace("$RESIZABLE$", Resizable ? "yes" : "no");
                    template = template.Replace("$POPUPBLOCKEDURL", PopupBlockedUrl);
                    template = template.Replace("$ACTION$", url);
                    template = template.Replace("$P1$", token.Split('-')[0]);
                    template = template.Replace("$P2$", token.Split('-')[1]);
                    break;

            }

            

            return template;
        }
        #endregion
    }
}
