﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QX3.Spinnex.Common.Services.Transfer
{

    public class TransferData : Dictionary<string, string>
    {
    }

    public class TransferFormater
    {
        #region Atributos e Enumerados
        private StringBuilder buffer;
        #endregion

        #region Construtores e Destrutores
        public TransferFormater()
        {
            buffer = new StringBuilder();
        }
        #endregion

        #region Métodos
        public void Write(string key, object value)
        {
            if (value == null)
                return;
            buffer.AppendFormat("{0}{1}{2}{3}", key, (char)1, value.ToString(), (char)2);
        }
        public TransferData ReadData(string str)
        {
            str = Encoding.Default.GetString(Convert.FromBase64String(str));
            buffer.Append(str);
            TransferData data = new TransferData();
            string[] items = str.Split((char)2);
            foreach (string s in items)
            {
                if (s == "")
                    continue;
                string[] keyvalue = s.Split((char)1);
                string key = keyvalue[0];
                string value = keyvalue[1];
                if (data.ContainsKey(key))
                    data.Remove(key);
                data.Add(key, value);
            }
            return data;
        }
        public override string ToString()
        {

            return Convert.ToBase64String(Encoding.Default.GetBytes(buffer.ToString()));
        }
        #endregion
    }
}
