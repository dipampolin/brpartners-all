﻿using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Reflection;
using System.Text;
using System.Web.Services.Description;
using System.Web.Services.Protocols;
using System.Xml;
using System.Xml.Serialization;


namespace QX3.Spinnex.Common.Services.Webservice
{
    public class WSInvoke
    {

        private Assembly webServiceAssembly;
        private List<string> services;
        Dictionary<string, Type> availableTypes;

        /// <summary>
        /// Método criado para chamar ws desconhecido
        /// </summary>
        /// <param name="webServiceUri"></param>
        /// <param name="methodName"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public string WebServiceInvoker(Uri webServiceUri, string methodName, object[] parameters)
        {
            object result = null;
            string xml = "";

            this.services = new List<string>(); // available services
            this.availableTypes = new Dictionary<string, Type>(); // available types

            // create an assembly from the web service description
            this.webServiceAssembly = BuildAssemblyFromWSDL(webServiceUri);

            // see what service types are available
            Type[] types = this.webServiceAssembly.GetExportedTypes();

            // and save them
            foreach (Type type in types)
            {
                if (typeof(HttpWebClientProtocol).IsAssignableFrom(type))
                {
                    HttpWebClientProtocol proxy = (HttpWebClientProtocol)Activator.CreateInstance(type);

                    foreach (MethodInfo info in type.GetMethods())
                    {
                        if (IsWebMethod(info) && info.Name.ToLower() == methodName.ToLower())
                        {
                            System.Type declaringType = info.DeclaringType;
                            object[] _parameters = parameters;
                            result = info.Invoke(proxy, BindingFlags.Public, null, _parameters, null);

                            XmlSerializer x = new XmlSerializer(result.GetType());

                            using (MemoryStream stream = new MemoryStream())
                            {
                                XmlTextWriter xtw = new XmlTextWriter(stream, Encoding.UTF8);
                                x.Serialize(xtw, result);

                                xml = Encoding.UTF8.GetString(stream.ToArray());
                            }
                        }
                    }
                }
            }


            return xml;
        }

        /// <summary>
        /// Builds an assembly from a web service description.
        /// The assembly can be used to execute the web service methods.
        /// </summary>
        /// <param name="webServiceUri">Location of WSDL.</param>
        /// <returns>A web service assembly.</returns>
        private Assembly BuildAssemblyFromWSDL(Uri webServiceUri)
        {
            if (String.IsNullOrEmpty(webServiceUri.ToString()))
                throw new Exception("Web Service Not Found");

            //This following code does not work for NAV Web services so I replaced it
            //XmlTextReader xmlreader = new XmlTextReader(webServiceUri.ToString() + "?wsdl");
            // Start of new code -->
            HttpWebRequest l_http = (HttpWebRequest)WebRequest.Create(webServiceUri.ToString() + "?WSDL");

            l_http.Timeout = 100000;
            l_http.UseDefaultCredentials = true;

            HttpWebResponse l_response = (HttpWebResponse)l_http.GetResponse();

            Encoding enc = Encoding.GetEncoding(1252); // Windows default Code Page

            StreamReader l_responseStream = new StreamReader(l_response.GetResponseStream(), enc);

            XmlTextReader xmlreader = new XmlTextReader(l_responseStream);
            // End of new code <--

            ServiceDescriptionImporter descriptionImporter = BuildServiceDescriptionImporter(xmlreader);

            return CompileAssembly(descriptionImporter);
        }

        public static bool IsWebMethod(MethodInfo method)
        {
            object[] customAttributes = method.GetCustomAttributes(typeof(SoapRpcMethodAttribute), true);
            if ((customAttributes != null) && (customAttributes.Length > 0))
            {
                return true;
            }
            customAttributes = method.GetCustomAttributes(typeof(SoapDocumentMethodAttribute), true);
            if ((customAttributes != null) && (customAttributes.Length > 0))
            {
                return true;
            }
            customAttributes = method.GetCustomAttributes(typeof(HttpMethodAttribute), true);
            return ((customAttributes != null) && (customAttributes.Length > 0));
        }

        /// <summary>
        /// Compiles an assembly from the proxy class provided by the ServiceDescriptionImporter.
        /// </summary>
        /// <param name="descriptionImporter"></param>
        /// <returns>An assembly that can be used to execute the web service methods.</returns>
        private Assembly CompileAssembly(ServiceDescriptionImporter descriptionImporter)
        {
            // a namespace and compile unit are needed by importer
            CodeNamespace codeNamespace = new CodeNamespace();
            CodeCompileUnit codeUnit = new CodeCompileUnit();

            codeUnit.Namespaces.Add(codeNamespace);

            ServiceDescriptionImportWarnings importWarnings = descriptionImporter.Import(codeNamespace, codeUnit);

            if (importWarnings == 0) // no warnings
            {
                // create a c# compiler
                CodeDomProvider compiler = CodeDomProvider.CreateProvider("CSharp");

                // include the assembly references needed to compile
                string[] references = new string[2] { "System.Web.Services.dll", "System.Xml.dll" };

                CompilerParameters parameters = new CompilerParameters(references);

                // compile into assembly
                CompilerResults results = compiler.CompileAssemblyFromDom(parameters, codeUnit);

                foreach (CompilerError oops in results.Errors)
                {
                    // trap these errors and make them available to exception object
                    throw new Exception("Compilation Error Creating Assembly");
                }

                // all done....
                return results.CompiledAssembly;
            }
            else
            {
                // warnings issued from importers, something wrong with WSDL
                throw new Exception("Invalid WSDL");
            }
        }

        /// <summary>
        /// Builds the web service description importer, which allows us to generate a proxy class based on the 
        /// content of the WSDL described by the XmlTextReader.
        /// </summary>
        /// <param name="xmlreader">The WSDL content, described by XML.</param>
        /// <returns>A ServiceDescriptionImporter that can be used to create a proxy class.</returns>
        private ServiceDescriptionImporter BuildServiceDescriptionImporter(XmlTextReader xmlreader)
        {
            // make sure xml describes a valid wsdl
            if (!ServiceDescription.CanRead(xmlreader))
                throw new Exception("Invalid Web Service Description");

            // parse wsdl
            ServiceDescription serviceDescription = ServiceDescription.Read(xmlreader);

            // build an importer, that assumes the SOAP protocol, client binding, and generates properties
            ServiceDescriptionImporter descriptionImporter = new ServiceDescriptionImporter();
            descriptionImporter.ProtocolName = "Soap";
            descriptionImporter.AddServiceDescription(serviceDescription, null, null);
            descriptionImporter.Style = ServiceDescriptionImportStyle.Client;
            descriptionImporter.CodeGenerationOptions = System.Xml.Serialization.CodeGenerationOptions.GenerateProperties;

            return descriptionImporter;
        }

       

    }
}
