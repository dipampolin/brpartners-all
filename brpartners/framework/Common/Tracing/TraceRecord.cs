using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Collections.Specialized;

namespace QX3.Spinnex.Common.Services.Tracing
{
	public class TraceRecord
	{
		#region Atributos e Enumerados
		private string sessionID;
		private List<TraceRecordItem> queryString = new List<TraceRecordItem>();
		private List<TraceRecordItem> formData = new List<TraceRecordItem>();
		private List<TraceRecordItem> sessionDatq = new List<TraceRecordItem>();
		private string url;
		private string userName;
		private List<TraceExceptionInfo> exceptions = new List<TraceExceptionInfo>();
		private List<TraceRecordItem> userData = new List<TraceRecordItem>();
		private List<TraceRecordItem> soapParams = new List<TraceRecordItem>();
		#endregion

		#region Propriedades
		public List<TraceRecordItem> QueryString
		{
			get { return queryString; }
			set { queryString = value; }
		}
		public List<TraceRecordItem> FormData
		{
			get { return formData; }
			set { formData = value; }
		}
		public List<TraceRecordItem> SessionData
		{
			get { return sessionDatq; }
			set { sessionDatq = value; }
		}
		public string SessionID
		{
			get { return sessionID; }
			set { sessionID = value; }
		}
		public string Url
		{
			get { return url; }
			set { url = value; }
		}
		public string UserName
		{
			get { return userName; }
			set { userName = value; }
		}
		public List<TraceExceptionInfo> Exceptions
		{
			get { return exceptions; }
			set { exceptions = value; }
		}
		public List<TraceRecordItem> UserData
		{
			get { return userData; }
			set { userData = value; }
		}
		public List<TraceRecordItem> MethodParameters
		{
			get { return soapParams; }
			set { soapParams = value; }
		}
		#endregion

		#region Sub-Classes e Estruturas
		public class TraceRecordItem
		{
			#region Atributos e Enumerados
			private string key;
			private object value;
			#endregion

			#region Propriedades
			public string Key
			{
				get { return key; }
				set { key = value; }
			}
			public object Value
			{
				get { return value; }
				set { this.value = value; }
			}
			#endregion

			#region Construtores e Destrutores
			public TraceRecordItem()
			{
			}
			public TraceRecordItem(string k, object v)
			{
				this.key = k;
				this.value = v;
			}
			#endregion
		}
		public class TraceExceptionInfo
		{
			#region Atributos e Enumerados
			private string message;
			private string source;
			private string stackTrace;
			private string targetSite;
			#endregion

			#region Propriedades
			public string Message
			{
				get { return message; }
				set { message = value; }
			}
			public string Source
			{
				get { return source; }
				set { source = value; }
			}
			public string StackTrace
			{
				get { return stackTrace; }
				set { stackTrace = value; }
			}
			public string TargetSite
			{
				get { return targetSite; }
				set { targetSite = value; }
			}
			#endregion
		}
		#endregion
	}
}
