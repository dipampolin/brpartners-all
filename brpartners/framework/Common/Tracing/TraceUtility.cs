using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Xml.Serialization;
using System.Xml;
using System.Configuration;
using System.IO;
using System.Diagnostics;

namespace QX3.Spinnex.Common.Services.Tracing
{
	public static class TraceUtility
	{
		#region M�todos
		public static void HandleAspxError()
		{

			HttpContext ctx = HttpContext.Current;
			Exception ex = ctx.Server.GetLastError();
			if (ex == null)
				ex = new ApplicationException("Ocorreu um erro desconhecido. Server.GetLastError() retornou null...");
			else
			{
				if (ex is HttpException)
					ex = ex.InnerException;
			}
			HandleException(ctx, ex);

			//ctx.Server.ClearError();
		}
		public static void HandleException(HttpContext ctx, Exception ex, params KeyValuePair<string, object>[] userData)
		{

			ctx.Trace.Warn("QXTrace", ex.ToString());

			TraceRecord record = CreateRecord(ctx, ex, userData);
			

			string filename = ConfigurationManager.AppSettings["TracePath"];
			if(filename == null)
				filename = "trace";
			filename = Path.Combine(filename,  DateTime.Now.ToString("yyyyMMdd"));
			DirectoryInfo di = new DirectoryInfo(filename);
			if(!di.Exists)
				di.Create();
			filename+=@"\" + DateTime.Now.ToString("HHmmss") + ".xml";

			XmlSerializer xs = new XmlSerializer(typeof(TraceRecord));
			using (FileStream fs = new FileStream(filename, FileMode.OpenOrCreate, FileAccess.Write))
			{

				try
				{
					xs.Serialize(fs, record);
					

				}
				//catch (Exception)
				//{
				//    record.MethodParameters.Clear();
				//    xs.Serialize(fs, record);
				//}
				finally
				{
					fs.Close();
				}
			}
		}
		private static TraceRecord CreateRecord(HttpContext ctx, Exception ex, params KeyValuePair<string, object>[] userData)
		{
			TraceRecord record = new TraceRecord();
			if (ctx != null)
			{
				if (ctx.Session != null)
					record.SessionID = ctx.Session.SessionID;

				record.Url = ctx.Request.Url.ToString();
				if (ctx.User.Identity != null)
					record.UserName = ctx.User.Identity.Name;
				if (ctx.Session != null)
					foreach (string key in ctx.Session.Keys)
						record.SessionData.Add(new TraceRecord.TraceRecordItem(key, Convert.ToString(ctx.Session[key])));


				foreach (string key in ctx.Request.QueryString.Keys)
					record.QueryString.Add(new TraceRecord.TraceRecordItem(key, Convert.ToString(ctx.Request.QueryString[key])));


				foreach (string key in ctx.Request.Form.Keys)
					record.FormData.Add(new TraceRecord.TraceRecordItem(key, Convert.ToString(ctx.Request.Form[key])));

				foreach (KeyValuePair<string, object> kv in userData)
					record.UserData.Add(new TraceRecord.TraceRecordItem(kv.Key, kv.Value == null ? null : kv.Value.ToString()));


				KeyValuePair<string, object>[] arr = (KeyValuePair<string, object>[]) ctx.Items["SoapParams"];

				if (arr != null)
				{
					foreach (KeyValuePair<string, object> kv in arr)
					{
						record.MethodParameters.Add(new TraceRecord.TraceRecordItem(kv.Key, kv.Value));
					}
				}

			}
			while (ex != null)
			{
				TraceRecord.TraceExceptionInfo info = new TraceRecord.TraceExceptionInfo();
				info.Message = ex.Message;
				info.StackTrace = ex.StackTrace;
				info.Source = ex.Source;
				if(ex.TargetSite!=null)
					info.TargetSite = ex.TargetSite.ToString();
				record.Exceptions.Add(info);
				ex = ex.InnerException;
			}
			return record;
		}
		#endregion
	}

	internal class TraceParameter
	{
		#region Atributos e Enumerados
		private string m_DataType;
		private object m_Value;
		private string m_Key;
		#endregion

		#region Propriedades
		public string DataType
		{
			get { return m_DataType; }
			set { m_DataType = value; }
		}
		public object Value
		{
			get { return m_Value; }
			set { m_Value = value; }
		}
		public string Key
		{
			get { return m_Key; }
			set { m_Key = value; }
		}
		#endregion

		#region Construtores e Destrutores
		public TraceParameter()
		{
		}
		public TraceParameter(Type type, string key, object obj)
		{
			this.Key = key;
			this.Value = obj;
			this.DataType = type.FullName;
		}
		#endregion
	}
}
