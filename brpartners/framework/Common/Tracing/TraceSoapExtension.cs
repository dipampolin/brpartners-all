using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Services.Protocols;
using System.Web;
using System.Reflection;
using System.Xml;
using System.Data;

namespace QX3.Spinnex.Common.Services.Tracing
{
	public class TraceSoapExtension : SoapExtension
	{
		#region M�todos
		public override object GetInitializer(Type serviceType)
		{
			return null;
		}

		public override object GetInitializer(LogicalMethodInfo methodInfo, SoapExtensionAttribute attribute)
		{
			return null;
		}

		public override void Initialize(object initializer)
		{

		}

		public override void ProcessMessage(SoapMessage message)
		{
			switch (message.Stage)
			{
				case SoapMessageStage.BeforeSerialize:

					if (message.Exception != null)
					{
						TraceUtility.HandleException(HttpContext.Current, message.Exception.InnerException);
					}

					break;
				case SoapMessageStage.AfterDeserialize:
					if (HttpContext.Current.Items["SoapParams"] == null)
					{
						List<KeyValuePair<string, object>> list = new List<KeyValuePair<string, object>>();

						for (int i = 0; i < message.MethodInfo.Parameters.Length; ++i)
						{
							ParameterInfo info = message.MethodInfo.Parameters[i];
							if (!info.IsOut)
							{
								object obj = message.GetInParameterValue(i);
								if (obj != null)
								{
									List<TraceParameter> memberList = new List<TraceParameter>();
									
									DecomposeObject(info.Name, obj, memberList, 1);

									foreach (TraceParameter tp in memberList)
										list.Add(new KeyValuePair<string, object>(tp.Key, tp.Value));


								}
								else
									list.Add(new KeyValuePair<string, object>(info.Name, obj));
							}
						}
						HttpContext.Current.Items["SoapParams"] = list.ToArray();
					}
					break;
			}
		}

		private void DecomposeObject(string name, object obj, List<TraceParameter> list, int recursionLevel)
		{
			if (obj == null)
				return;

			if (obj is DataSet)
			{
				DataSet ds = (DataSet)obj;
				obj = ds.GetXml();
			}

			if (recursionLevel > 10)
				return;

			Type type = obj.GetType();

            bool isSimple = type.IsPrimitive;

			//isSimple = true;

            if (!isSimple)
            {
                switch (type.FullName)
                {
                    case "System.DateTime":
                    case "System.TimeSpan":
                    case "System.String":
                    case "System.Guid":
                        isSimple = true;
                        break;
                }
            }


			if (isSimple)
				list.Add(new TraceParameter(type, name, obj));
			else
			{
				foreach (PropertyInfo p in type.GetProperties())
				{
					if (p.GetIndexParameters().Length == 0 && p.CanRead)
					{
						object pobj = p.GetValue(obj, null);
						if (obj != pobj)
						{
							DecomposeObject(string.Format("{0}_{1}", name, p.Name), pobj, list, recursionLevel+1);
						}
					}
				}
			}
		}
	
		public override System.IO.Stream ChainStream(System.IO.Stream stream)
		{
			return base.ChainStream(stream);
		}
		#endregion
	}

	public class TraceSoapExtensionAttribute : SoapExtensionAttribute
	{
		#region Atributos e Enumerados
		private int m_Priority;
		#endregion

		#region Propriedades
		public override Type ExtensionType
		{
			get { return typeof(TraceSoapExtension); }
		}
		public override int Priority
		{
			get
			{
				return m_Priority;
			}
			set
			{
				m_Priority = value;
			}
		}
		#endregion
	}
}
