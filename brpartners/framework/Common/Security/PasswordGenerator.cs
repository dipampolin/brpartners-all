using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;
using QX3.Spinnex.Common.Services.Security.Cryptography;
using System.IO;
using System.Configuration;

namespace QX3.Spinnex.Common.Services.Security
{
    public class PasswordGenerator
    {
        #region Atributos e Enumerados
        public enum PasswordDomain
        {
            AlphaNumeric, Alpha, Numeric
        }
        const string ALPHA = "abcdefghjkmnpqrstuvwxyz";
        const string ALPHA_UPPER = "ABCDEFGHJKMNPQRSTUVWXYZ";
        const string NUMBERS = "123456789";
        const string ALPHANUMERIC = ALPHA + NUMBERS + ALPHA_UPPER;
        #endregion

        #region M�todos
        public string Generate(int length, PasswordDomain passwordDomain)
        {

            if (length < 1) throw new ArgumentException("Comprimento da senha deve ser >= 1", "length");

            string domain;
            switch (passwordDomain)
            {
                case PasswordDomain.Alpha:
                    domain = ALPHA; break;
                case PasswordDomain.AlphaNumeric:
                    domain = ALPHANUMERIC; break;
                case PasswordDomain.Numeric:
                    domain = NUMBERS; break;
                default:
                    throw new ArgumentException("Unsupported password domain", "passwordDomain");
            }



            if (domain == null) throw new ArgumentException("Caracteres de dom�nio da senha n�o pode ser null", "domain");
            if (domain.Length == 0) throw new ArgumentException("Caracteres de dom�nio da senha n�o pode ser \"\"", "domain");

            char[] chars = new char[length];
            Random rand = new Random();
            for (int i = 0; i < length; ++i)
            {
                chars[i] = domain[rand.Next(0, domain.Length)];
            }

            if (domain == ALPHANUMERIC)
            {
                int i = 0;
                int t;
                string pass = new string(chars);
                foreach (char p in chars)
                {
                    if (Int32.TryParse(Convert.ToString(p), out t))
                        i++;                    
                }

                if (i == 0)
                    pass = pass.Substring(0, length - 1) + "5";

                return pass;
            }

            return new string(chars);

        }
        public string Generate(int length)
        {
            return Generate(length, PasswordDomain.AlphaNumeric);
        }

        /// <summary>
        /// Este m�todo demonstra uma codifica��o de senha usando SHA1 e codificando o resultado em uma string Base64.
        /// N�o necessariamente os m�todos de gera��o de senha precisam ficar amarrados a esta criptografia!!!!
        /// </summary>
        /// <param name="clearText">Texto a ser criptografado</param>
        /// <returns>Texto criptografado.</returns>
        public string EncodePassword(string clearText)
        {
            //Se este atributo n�o configurado, a senha do sistema n�o far� diferen�a entre mai�sculas e min�sculas
            if (ConfigurationManager.AppSettings["ActivateSensitivePassword"] == null || ConfigurationManager.AppSettings["ActivateSensitivePassword"].ToString().ToUpper() == "False")
                clearText = clearText.ToLower();

            byte[] bytes = Encoding.Unicode.GetBytes(clearText);
            byte[] hash;

            MemoryStream stream = new MemoryStream();
            stream.WriteByte(0);

            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            hash = md5.ComputeHash(bytes);


            stream.Write(hash, 0, hash.Length);
            stream.WriteByte(0);

            SHA1Managed sha1 = new SHA1Managed();
            hash = sha1.ComputeHash(bytes);

            stream.Write(hash, 0, hash.Length);


            bytes = stream.ToArray();
            string result = Convert.ToBase64String(bytes);

            return result;

        }

        public string EncodePassword(string MD5str, string SH1str)
        {
            if (string.IsNullOrEmpty(MD5str) || string.IsNullOrEmpty(SH1str))
            {
                return String.Empty;
            }
            char[] md5c = MD5str.ToCharArray();
            char[] sh1c = SH1str.ToCharArray();

            byte[] md5 = Convert.FromBase64CharArray(md5c, 0, md5c.Length);
            byte[] sh1 = Convert.FromBase64CharArray(sh1c, 0, sh1c.Length);


            MemoryStream stream = new MemoryStream();
            stream.WriteByte(0);

            stream.Write(md5, 0, md5.Length);
            stream.WriteByte(0);

            stream.Write(sh1, 0, sh1.Length);


            byte[] bytes = stream.ToArray();
            string result = Convert.ToBase64String(bytes);

            return result;

        }

        #endregion
    }
}
