﻿using QX3.Spinnex.Common.Services.Generics;
using System.Collections.Specialized;
using System.Collections.Generic;
using System;
using System.Web;
using System.Configuration;
using QX3.Spinnex.Common.Services.Logging;
using System.Web.Caching;
namespace QX3.Spinnex.Common.Services.Security
{
    /// <summary>
    /// Classe que permite às aplicações controlarem o acesso do usuário. O objetivo é impedir que o cliente super-utilize recursos como streamer de cotações e outros abrindo várias janelas ou passando seus dados de acesso para outros indivíduos.
    /// Este controle é feito associando um username (que pode ser um ID, o login ou o que o desenvolvedor da aplicação quiser) a seu sessionId, de forma que se ele abrir outra janela do navegador terá outra sessão e a aplicação poderá tratar esta situação de acordo.
    /// </summary>
    public class SingleSessionController : Singleton<SingleSessionController>
    {
        #region Atributos e Enumerados

        /// <summary>
        /// Coleção das aplicações controladas. As aplicações são cadastradas automaticamente quando as sessões são registradas.
        /// </summary>
        private ApplicationCollection applications;

        #endregion

        #region Construtores e Destrutores
        /// <summary>
        /// Código de inicialização. Joga dados de aplicações no Cache.
        /// </summary>
        public SingleSessionController()
        {
            applications = (ApplicationCollection)HttpContext.Current.Cache["SessionControl"];

            if (applications == null)
            {
                applications = new ApplicationCollection();
                CreateCache(applications);
            }
        }
        internal static void CreateCache(ApplicationCollection control)
        {
            HttpContext.Current.Cache.Add("SessionControl", control, null, DateTime.MaxValue, TimeSpan.Zero, System.Web.Caching.CacheItemPriority.NotRemovable,
                    new CacheItemRemovedCallback(delegate(string key, object obj, CacheItemRemovedReason reason)
                        {
                            if (reason != CacheItemRemovedReason.Removed)
                                CreateCache((ApplicationCollection)obj);
                        })
                    );

        }


        #endregion

        #region Sub-Classes e Estruturas
        /// <summary>
        /// Classe que mantém os dados da sessão.
        /// </summary>
        public class SessionControlData
        {
            /// <summary>
            /// SessionID na aplicação cliente.
            /// </summary>
            public string SessionId { get; set; }
            /// <summary>
            /// Data e hora usada no expurgo automático.
            /// </summary>
            public DateTime LastUpdateTime { get; set; }
            /// <summary>
            /// Atualiza a data de ultima atualizacao.
            /// </summary>
            public void Touch()
            {
                this.LastUpdateTime = DateTime.Now;
            }
        }
        /// <summary>
        /// Classe que guarda as sessões de uma aplicação.
        /// </summary>
        internal class SessionCollection : Dictionary<string, SessionControlData> { }
        /// <summary>
        /// Classe que guarda as aplicações controladas pelo sistema.
        /// </summary>
        internal class ApplicationCollection : Dictionary<string, SessionCollection> { }

        #endregion

        #region Propriedades
        #endregion

        #region Métodos

        /// <summary>
        /// Recupera variável de configuração CleanupTimeout, definida em minutos.
        /// </summary>
        /// <returns></returns>
        private TimeSpan GetCleanupTimeout()
        {
            string str = ConfigurationManager.AppSettings["SessionControl.CleanupTimeout"];
            if (str == null)
                throw new ConfigurationErrorsException("Parâmetro de configuração não encontrado: SessionControl.CleanupTimeout");
            try
            {
                int timeout = Int32.Parse(str);
                TimeSpan timeSpan = TimeSpan.FromMinutes(timeout);
                return timeSpan;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Erro ao converter timeout de sessão", ex);
            }
        }
        /// <summary>
        /// Limpa sessões muito antigas.
        /// </summary>
        /// <param name="app">aplicação a ser limpa.</param>
        private void Cleanup(SessionCollection app)
        {
            if (app == null)
                throw new ArgumentException("Aplicação não encontrada.", "applicationId");
            TimeSpan cleanupTimeout = GetCleanupTimeout();
            DateTime minimumDateTime = DateTime.Now.Subtract(cleanupTimeout);
            List<string> keysToRemove = new List<string>();
            foreach (var sessionKeyValue in app)
            {
                if (sessionKeyValue.Value.LastUpdateTime < minimumDateTime)
                {
                    keysToRemove.Add(sessionKeyValue.Key);
                    LogManager.WriteLog("Removendo sessão inativa: {0} -> {1}", sessionKeyValue.Key, sessionKeyValue.Value.LastUpdateTime);
                }
            }
            foreach (string key in keysToRemove)
                app.Remove(key);

        }
        /// <summary>
        /// Registra a SessionID que vale para o usuário corrente.
        /// </summary>
        /// <param name="applicationId">Aplicação a ser manipulada.</param>
        /// <param name="username">Identificador do usuário na aplicação cliente.</param>
        /// <param name="sessionId">SessionID na aplicação cliente.</param>
        public void RegisterSession(string applicationId, string username, string sessionId)
        {
            lock (this)
            {
                SessionCollection applicationSessions = null;

                if (!this.applications.ContainsKey(applicationId))
                {
                    applicationSessions = new SessionCollection();
                    this.applications.Add(applicationId, applicationSessions);
                }
                else
                {
                    applicationSessions = this.applications[applicationId];
                    Cleanup(applicationSessions);
                }
                SessionControlData sessionData;
                if (applicationSessions.ContainsKey(username))
                {
                    sessionData = applicationSessions[username];
                    applicationSessions.Remove(username);
                }
                sessionData = new SessionControlData() { SessionId = sessionId };
                applicationSessions.Add(username, sessionData);

                sessionData.Touch();
            }

        }

        /// <summary>
        /// Registra a SessionID que vale para o usuário corrente.
        /// </summary>
        /// <param name="applicationId">Aplicação a ser manipulada.</param>
        /// <param name="username">Identificador do usuário na aplicação cliente.</param>
        /// <param name="sessionId">SessionID na aplicação cliente.</param>
        public void RegisterSession(string applicationId, string username, string sessionId, bool validateLogin)
        {
            lock (this)
            {
                SessionCollection applicationSessions = null;

                if (!this.applications.ContainsKey(applicationId))
                {
                    applicationSessions = new SessionCollection();
                    this.applications.Add(applicationId, applicationSessions);
                }
                else
                {
                    applicationSessions = this.applications[applicationId];
                    Cleanup(applicationSessions);
                }
                SessionControlData sessionData;
                if (applicationSessions.ContainsKey(username))
                {
                    sessionData = applicationSessions[username];
                    applicationSessions.Remove(username);
                }
                sessionData = new SessionControlData() { SessionId = sessionId };
                applicationSessions.Add(username, sessionData);

                sessionData.Touch();
            }

        }
        /// <summary>
        /// Valida se a sessão na aplicação cliente é válida.
        /// </summary>
        /// <param name="applicationId">Aplicação a ser manipulada.</param>
        /// <param name="username">Identificador do usuário na aplicação cliente.</param>
        /// <param name="sessionId">SessionID na aplicação cliente.</param>
        /// <returns>true se o usuário existir e a sessionID confere, senao false.</returns>
        public bool ValidateSession(string applicationId, string username, string sessionId, string ip, bool deleteSession)
        {
            bool validate = true;

            lock (this)
            {
                SessionCollection applicationSessions = new SessionCollection();
                SessionControlData sessionData;

                if (this.applications.Count > 0)
                {
                    var app = CleanupSessions(this.applications);

                    foreach (var i in app)
                    {
                        foreach (var j in app[i.Key])
                        {
                            if (j.Key == username)
                            {
                                if (deleteSession)
                                {
                                    app.Remove(i.Key);
                                    return true;
                                }
                                else
                                {

                                    if (!app.ContainsKey(ip))
                                    {
                                        return false;
                                    }
                                    else
                                    {
                                        app.Remove(i.Key);

                                        this.applications.Add(ip, applicationSessions);

                                        sessionData = new SessionControlData() { SessionId = sessionId };
                                        applicationSessions.Add(username, sessionData);

                                        sessionData.Touch();

                                        return true;
                                    }

                                }

                            }

                        }
                    }
                }
                else
                {
                    this.applications.Add(ip, applicationSessions);

                    sessionData = new SessionControlData() { SessionId = sessionId };
                    applicationSessions.Add(username, sessionData);

                    sessionData.Touch();
                }

            }

            return validate;
        }

        /// <summary>
        /// Limpa sessões muito antigas.
        /// </summary>
        /// <param name="app">aplicação a ser limpa.</param>
        private ApplicationCollection CleanupSessions(ApplicationCollection app)
        {
            TimeSpan cleanupTimeout = GetCleanupTimeout();
            DateTime minimumDateTime = DateTime.Now.Subtract(cleanupTimeout);

            ApplicationCollection app2 = app;

            foreach (var i in app)
            {
                foreach (var j in app[i.Key])
                {
                    if (j.Value.LastUpdateTime < minimumDateTime)
                    {
                        app2.Remove(i.Key);
                    }

                }
            }

            return app2;

        }

        /// <summary>
        /// Valida se a sessão na aplicação cliente é válida.
        /// </summary>
        /// <param name="applicationId">Aplicação a ser manipulada.</param>
        /// <param name="username">Identificador do usuário na aplicação cliente.</param>
        /// <param name="sessionId">SessionID na aplicação cliente.</param>
        /// <returns>true se o usuário existir e a sessionID confere, senao false.</returns>
        public bool ValidateSession(string applicationId, string username, string sessionId)
        {
            if (!this.applications.ContainsKey(applicationId))
            {
                LogManager.WriteLog("Aplicação não encontrada: {0}", applicationId);
                return false;
            }
            var applicationSessions = this.applications[applicationId];
            if (!applicationSessions.ContainsKey(username))
            {
                LogManager.WriteLog("Usuário não encontrado: {0}", username);
                return false;
            }
            var sessionData = applicationSessions[username];
            sessionData.Touch();
            var sameSession = sessionData.SessionId.Equals(sessionId);
            if (!sameSession)
                LogManager.WriteLog("Sessão diferente ({0}) Param: {1} Corrente: {2}", username, sessionId, sessionData.SessionId);
            return sameSession;
        }

        /// <summary>
        /// Lista as sessões de uma aplicação. Usado para deputação / testes.
        /// </summary>
        /// <param name="applicationId">Aplicação a ser consultada.</param>
        /// <returns></returns>
        public SessionControlData[] GetSessions(string applicationId)
        {
            if (!this.applications.ContainsKey(applicationId))
                throw new ArgumentException("Aplicação não existe: " + applicationId);
            var applicationSessions = this.applications[applicationId];

            var values = applicationSessions.Values;

            SessionControlData[] array = new SessionControlData[values.Count];
            values.CopyTo(array, 0);

            return array;

        }




        #endregion
    }
}
