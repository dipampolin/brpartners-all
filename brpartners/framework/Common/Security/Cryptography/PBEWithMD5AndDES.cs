using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Security.Cryptography;
namespace QX3.Spinnex.Common.Services.Security.Cryptography
{
    public class PBEWithMD5AndDES
    {
		/*
			Fonte: http://www.codeproject.com/useritems/pbe.asp
		 *  uso:
		 *  PBEWithMD5AndDES kp = new PBEWithMD5AndDES();
			byte[] salt = Encoding.UTF8.GetBytes("cedrofin"); #salt
			int count = 20; #contador;
			string skey = "talarico"; # chave

			FromBase64Transform b64 = new FromBase64Transform();
			ICryptoTransform crypt = kp.Generate(
			"talarico",
			salt,// salt
			20, // iterations of MD5 hashing
			1);// number of 16-byte segments to create. 
			  // 1 to mimic Java behaviour.
			 // ICryptoTransform ct = crypt.Encryptor;
			String command = "cedro1";
			byte[] newbytes = crypt.TransformFinalBlock(Encoding.UTF8.GetBytes(command), 0, command.Length);
			Response.Write("mhhOcQLrHRo%3D <br />");
			Response.Write( System.Web.HttpUtility.UrlEncode(Convert.ToBase64String(newbytes)));
			//System.Web.HttpUtility.UrlEncode ( 

		 * 
		 */
		#region Atributos e Enumerados
		private byte[] key = new byte[8], iv = new byte[8];
        private DESCryptoServiceProvider des = new DESCryptoServiceProvider();
		#endregion

		#region Propriedades
		public byte[] Key
        {
            get
            {
                return key;
            }
        }
        public byte[] IV
        {
            get
            {
                return IV;
            }
        }
        public ICryptoTransform Encryptor
        {
            get
            {
                return des.CreateEncryptor(key, iv);
            }
		}
		#endregion

		#region Construtores e Destrutores
		public PBEWithMD5AndDES() { }
        public PBEWithMD5AndDES(String keystring,
                                byte[] salt,
                                int md5iterations,
                                int segments)
        {
            Generate(keystring, salt, md5iterations, segments);
		}
		#endregion

		#region M�todos
		public ICryptoTransform Generate(String keystring,
                                        byte[] salt,
                                        int md5iterations,
                                        int segments)
        {
            int HASHLENGTH = 16;	//MD5 bytes
            byte[] keymaterial = new byte[HASHLENGTH * segments];
            //to store contatenated Mi hashed results
            // --- get secret password bytes ----
            byte[] psbytes;
            psbytes = Encoding.UTF8.GetBytes(keystring);
            // --- contatenate salt and pswd bytes into fixed data array ---
            byte[] data00 = new byte[psbytes.Length + salt.Length];
            Array.Copy(psbytes, data00, psbytes.Length);		//copy the pswd bytes
            Array.Copy(salt, 0, data00, psbytes.Length, salt.Length);	//concatenate the salt bytes
            // ---- do multi-hashing and contatenate results  D1, D2 ...  into keymaterial bytes ----
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] result = null;
            byte[] hashtarget = new byte[HASHLENGTH + data00.Length];//fixed length initial hashtarget

            for (int j = 0; j < segments; j++)
            {
                // ----  Now hash consecutively for md5iterations times ------
                if (j == 0) result = data00;   	//initialize
                else
                {
                    Array.Copy(result, hashtarget, result.Length);
                    Array.Copy(data00, 0, hashtarget, result.Length, data00.Length);
                    result = hashtarget;
                }
                for (int i = 0; i < md5iterations; i++)
                    result = md5.ComputeHash(result);

                Array.Copy(result, 0, keymaterial, j * HASHLENGTH, result.Length);  //contatenate to keymaterial
            }

            Array.Copy(keymaterial, 0, key, 0, 8);
            Array.Copy(keymaterial, 8, iv, 0, 8);
            return Encryptor;
        }
        public ICryptoTransform Generate(string key, string p, byte[] salt, int p_4, int p_5)
        {
            throw new Exception("The method or operation is not implemented.");
		}
		#endregion
	}
}
