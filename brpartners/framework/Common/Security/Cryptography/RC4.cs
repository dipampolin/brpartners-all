using System;
using System.Collections.Generic;
using System.Text;

namespace QX3.Spinnex.Common.Services.Security.Cryptography
{
	/// <summary>
	/// Vers�o C# da QxCriptografia usada no Spinnex 2.2 (ASP).
	/// Os m�todos Encrypt e Decrypt s�o iguais, pois no RC4 a descriptografia � feita criptografando-se com a mesma chave.
	/// Os m�todo HexEncrypt criptografa a string e retorna a mesma em hexadecimal de dois digitos.
	/// Para descriptografar a string gerada pela HexEncrypt chame a HexDecrypt que retorna a string original.
	/// </summary>
	[Obsolete("O algoritmo da classe RC4 portado da QXCriptografia n�o funciona direito em .NET. Se voc� desejar usa-lo mesmo assim, use a DLL Criptografia.dll")]
	public class RC4
	{
		#region M�todos
		public string Encrypt(string content, string key)
		{
			return EnDeCrypt(content, key);
		}
		public string Decrypt(string content, string key)
		{
			return EnDeCrypt(content, key);
		}
		private byte[] RC4Initialize(byte[] arrPwd)
		{
			byte tempSwap, a, b;
			byte[] sbox = new byte[255];
			byte[] key = new byte[255];
			int intLength = arrPwd.Length;
			for (a = 0; a < 255; a++)
			{
				key[a] = arrPwd[a % intLength]; // asc(mid(strpwd, (a mod intLength)+1, 1))
				sbox[a] = a;
			}
			b = 0;
			for (a = 0; a < 255; ++a)
			{
				b = (byte)( (b + sbox[a] + key[a]) % 255);
				tempSwap = sbox[a];
				sbox[a] = sbox[b];
				sbox[b] = tempSwap;
			}
			return sbox;
		}

		protected string EnDeCrypt(string plaintxt, string psw)
		{
			return Encoding.Default.GetString(EnDeCrypt(Encoding.Default.GetBytes(plaintxt), Encoding.Default.GetBytes(psw)));
		}

		private byte[] EnDeCrypt(byte[] plaintxt, byte[] psw)
		{
			byte temp, a, i, j, k, cipherby;
			List<byte> cipher = new List<byte>();
			byte[] sbox;
			i = 0; j = 0;
			sbox = RC4Initialize(psw);
			for (a = 0; a < plaintxt.Length; ++a)
			{
				i =  (byte) ((i + 1) % 256);
				j =  (byte)((j + sbox[i]) % 256);
				temp = sbox[i];
				sbox[i] = sbox[j];
				sbox[j] = temp;
				k = sbox[(sbox[i] + sbox[j]) % 256];
				cipherby = (byte) (plaintxt[a] ^ k);  //Asc(Mid(plaintxt, a, 1)) Xor k
				cipher.Add(cipherby);
			}
			return cipher.ToArray();
		}
		public string HexDecrypt(string phex, string ppwd)
		{
			string str_crypt = ""; int x;
			for (x = 0; x < (phex.Length); x += 2)
				str_crypt = str_crypt + (char)Int32.Parse(phex.Substring(x, 2), System.Globalization.NumberStyles.HexNumber); //chr(cint("&H"&mid(phex,x,2)))

			return Encrypt(str_crypt, ppwd);
		}
		public string HexEncrypt(string content, string pwd)
		{
			string str_crypt = "", str_hex = ""; int x;
			str_crypt = Encrypt(content, pwd);
			for (x = 0; x < (str_crypt.Length); ++x)
				str_hex = str_hex + string.Format("{0:X2}",(int) str_crypt[x]); // right(string(2,"0") & hex(asc(mid(str_crypt, x, 1))),2)

			return str_hex;
		}
		#endregion
	}
}
