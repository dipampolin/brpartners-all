﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using QX3.Spinnex.Common.Services.Logging;

namespace QX3.Spinnex.Common.Services.Security
{
	public class SecureWebServiceModule : IHttpModule
	{
		#region Métodos
		#region IHttpModule Members

		public void Dispose()
		{
			
		}

		public void Init(HttpApplication context)
		{
			context.BeginRequest += new EventHandler(context_BeginRequest);
			
		}

		void context_BeginRequest(object sender, EventArgs e)
		{
			HttpContext ctx = HttpContext.Current;
			HttpRequest request = ctx.Request;

			if (request.HttpMethod == "POST")
			{

				var cookie = request.Cookies["TRANSACTIONID"];
				if (cookie == null)
				{
					throw new Exception("NÃO FOI PASSADO COOKIE 'TRANSACTIONID'! VOCÊ NÃO PODE CHAMAR ESTE WEB SERVICE!");
				}

				Guid decodedTransactionID = LogManager.DecodeTransactionID(cookie.Value);
			}
		}

		#endregion
		#endregion
	}
}
