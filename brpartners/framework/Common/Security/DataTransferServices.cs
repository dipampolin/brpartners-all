﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace QX3.Spinnex.Common.Services.Security
{
	public class DataTransferServices
	{
		#region Métodos
		public string CreateToken(string data)
		{
			MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
			byte[] dataBytes = Encoding.Default.GetBytes(data);
			byte[] hashedBytes = md5.ComputeHash(dataBytes);

			Guid guid = Guid.NewGuid();
			string base64Hash = Convert.ToBase64String(hashedBytes);

			string token = string.Format("{0}@{1}", guid, base64Hash);
			return token;
		}
		#endregion
	}
}
