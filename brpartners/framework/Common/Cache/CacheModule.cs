using System;
using System.Web;

namespace QX3.Spinnex.Common.Services.Cache
{
	public class CacheModule : IHttpModule
	{
		#region M�todos
		public void Dispose()
		{
		}

		public void Init(HttpApplication context)
		{
			context.BeginRequest += new EventHandler(OnBeginRequest);
		}

		void OnBeginRequest(object sender, EventArgs e)
		{
			HttpResponse response = HttpContext.Current.Response;
			response.Expires = -1;
			response.CacheControl = "Private";
            response.AddHeader("CacheModule-Enabled", "1");
			//response.Headers.Add("CacheModule-Enabled", "1");
		}
		#endregion
	}
}
