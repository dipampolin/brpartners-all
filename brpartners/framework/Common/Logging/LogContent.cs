﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Web;
using System.IO;

namespace QX3.Spinnex.Common.Services.Logging
{
	public class LogContent
	{
		#region Atributos e Enumerados
		private XDocument xml;
		private bool wirteToDisk = false;
		private DateTime requestStartDateTime;
		private DateTime lastLogDateTime;
		private string moduleName;
		#endregion

		#region Propriedades
		public bool IsImportant
		{
			get
			{
				return wirteToDisk;
			}
		}
		public Guid TransactionID { get; set; }
		//public string ModuleName { get; set; }
		internal bool ContainsErrors { get; set; }
		#endregion

		#region Construtores e Destrutores

		public LogContent()
		{
			requestStartDateTime = DateTime.Now;
			lastLogDateTime = requestStartDateTime;
			TransactionID = LogManager.GetTransactionID();


			Guid parentTransactionId = Guid.Empty;

			if (HttpContext.Current != null)
			{

				var cookie = HttpContext.Current.Request.Cookies["TRANSACTIONID"];
				if (cookie != null)
					parentTransactionId = LogManager.DecodeTransactionID(cookie.Value);
			}
			xml = new XDocument(
			new XElement("log",
				new XAttribute("version", "1"),
				new XAttribute("applicationId", LogManager.Config.AppName),
				new XAttribute("module", LogManager.Config.DefaultModuleName),
				new XElement("transaction-id", TransactionID),
				new XElement("parent-transaction-id", parentTransactionId),
				new XElement("timestamp", requestStartDateTime),
				this.GetRequestData(),
				GetSessionData(),
				GetAuthData()
			));

			this.moduleName = LogManager.Config.DefaultModuleName;

			//AddRawRequest();
			AddUserInformation();


		}
		#endregion

		#region Métodos
		internal void SetModuleName(string moduleName)
		{
			this.moduleName = moduleName;
			var logElement = this.xml.Root;
			var att = logElement.Attribute("module");
			if (att == null)
			{
				att = new XAttribute("module", moduleName);
				logElement.Add(att);
			}
			else
				att.Value = moduleName;
		}

		private void AddUserInformation()
		{
			if (LogManager.Config.UserInformationProviderType != null)
			{
				IUserInformationProvider provider = (IUserInformationProvider)Activator.CreateInstance(LogManager.Config.UserInformationProviderType);
				xml.Descendants().First().Add(new XElement("user-information",
					new XElement("user-id", provider.UserID),
					new XElement("user-name", provider.UserName),
					new XElement("display-name", provider.DisplayName),
					new XElement("properties",
						(from kvp in provider.Properties
						 select new XElement("property", new XElement("name", kvp.Key), new XElement("value", kvp.Value))).ToArray())));

			}
		}

		private void AddRawRequest()
		{
			Stream stm = HttpContext.Current.Request.InputStream;
			StreamReader sr = null;
			stm.Position = 0;
			try
			{
				sr = new StreamReader(stm, true);
				this.xml.Descendants().First().AddFirst(new XElement("raw-request",
					new XCData(sr.ReadToEnd())));
			}
			finally
			{
				if (sr != null)
					sr.Close();
			}
		}

		public XDocument GetLogData()
		{
			return this.xml;
		}

		private XElement GetRequestData()
		{
			HttpContext ctx = HttpContext.Current;

			if (ctx == null)
				return new XElement("empty-request");

			HttpRequest req = ctx.Request;

			return new XElement("request",
				new XElement("server-variables",
					(from sv in HttpContext.Current.Request.ServerVariables.AllKeys
					 where sv != "ALL_RAW" && sv != "ALL_HTTP" && HttpContext.Current.Request.ServerVariables[sv] != ""
					 select new XElement("item", new XElement("name", sv), new XElement("value", HttpContext.Current.Request.ServerVariables[sv]))).ToArray()),
				new XElement("cookies",
					(from c in HttpContext.Current.Request.Cookies.AllKeys
					 select new XElement("cookie", new XElement("name", c), new XElement("value", HttpContext.Current.Request.Cookies[c].Value)))),

				new XElement("url", req.Url.OriginalString),
				new XElement("method", req.RequestType),
				new XElement("query-string",
					(from key in req.QueryString.AllKeys
					 select new XElement("item", new XElement("key", key), new XElement("value", req.QueryString[key]))).ToArray()),
					 new XElement("form",
						 (from key in req.Form.AllKeys
						  where MaskFormField(key)
						  select new XElement("item", new XElement("key", key), new XElement("value", req.Form[key]))).ToArray())
						 );
		}

		private bool MaskFormField(string key)
		{
			if (key != null)
			{
				key = key.ToUpper();

				if (key.IndexOf("PASSWORD") != -1 || key.IndexOf("SENHA") != -1 || key.IndexOf("PWD") != -1 || key.IndexOf("ASSINATURA") != -1 || key.IndexOf("ELECTRONICSIGNATURE") != -1)
				{
					return false;
				}
			}
			return true;
		}

		private XElement GetAuthData()
		{
			if (HttpContext.Current == null)
				return new XElement("empty-authentication");

			return new XElement("authentication",
				new XElement("is-authenticated",
					HttpContext.Current.Request.IsAuthenticated),
					(HttpContext.Current.Request.IsAuthenticated ? new XElement("identity", HttpContext.Current.User.Identity.Name) : null)
					);
		}

		private XElement GetSessionData()
		{
			if (HttpContext.Current == null)
			{
				return new XElement("no-sessionstate");
			}

			if (HttpContext.Current.Session == null)
			{
				return null;
			}

			XElement[] session = new XElement[HttpContext.Current.Session.Count];
			int j = 0;
			for (int i = 0; i < session.Length; ++i)
				if (MaskFormField(HttpContext.Current.Session.Keys[i]))
					session[j++] = new XElement("item", new XElement("key", HttpContext.Current.Session.Keys[i]), new XElement("value", Convert.ToString(HttpContext.Current.Session[i])));

			return new XElement("session",
				new XElement("session-id", HttpContext.Current.Session.SessionID),
				session);

		}

		private void IncrementTimes(out TimeSpan requestTime, out TimeSpan lastLogTime)
		{
			requestTime = DateTime.Now.Subtract(requestStartDateTime);
			lastLogTime = DateTime.Now.Subtract(lastLogDateTime);
			lastLogDateTime = DateTime.Now;
		}

		public void AddLog(string message)
		{

			TimeSpan requestTime, lastLogTime;
			IncrementTimes(out requestTime, out lastLogTime);

			wirteToDisk = true;
			this.xml.Descendants().First().Add(new XElement("log-message",
				new XAttribute("time", requestTime.TotalSeconds),
				new XAttribute("last-log-time", lastLogTime.TotalSeconds),
				new XText(message)));
		}

		public void AddLog(XElement element)
		{
			TimeSpan requestTime, lastLogTime;
			IncrementTimes(out requestTime, out lastLogTime);

			element.Add(new XAttribute("time", requestTime.TotalSeconds));
			element.Add(new XAttribute("last-log-time", lastLogTime.TotalSeconds));

			wirteToDisk = true;
			this.xml.Descendants().First().Add(element);
		}

		public void AddError(Exception ex)
		{

			TimeSpan requestTime, lastLogTime;
			IncrementTimes(out requestTime, out lastLogTime);


			wirteToDisk = true;
			this.ContainsErrors = true;
			this.xml.Descendants().First().Add(
						new XElement("error",
							new XAttribute("time", requestTime.TotalSeconds),
							new XAttribute("last-log-time", lastLogTime.TotalSeconds),
						GetExceptionDetails(ex)
						)
						);
		}

		private XElement GetExceptionDetails(Exception ex)
		{
			if (ex == null)
				return null;

			return new XElement("exception",
					new XElement("type", ex.GetType().ToString()),
					new XElement("message", ex.Message),
					new XElement("stack-trace", ex.StackTrace),
					GetExceptionDetails(ex.InnerException)
					);
		}

		internal void SetTotalRequestTime()
		{
			TimeSpan totalRequestTime = DateTime.Now.Subtract(requestStartDateTime);

			this.xml.Root.Add(new XAttribute("total-time", totalRequestTime.TotalSeconds));

		}
		internal string GetModuleName()
		{
			return this.moduleName;
		}
		#endregion


	}
}
