﻿using System;
using System.Web;

namespace QX3.Spinnex.Common.Services.Logging
{
	public class LogModule : IHttpModule
	{
		#region Propriedades
		HttpContext CurrentContext
		{
			get
			{
				return HttpContext.Current;
			}
		}
		#endregion

		#region Métodos
		public void Dispose()
		{
		}
		public void Init(HttpApplication context)
		{
			context.BeginRequest += new EventHandler(OnBeginRequest);
			context.EndRequest += new EventHandler(OnEndRequest);
			context.Error += new EventHandler(OnError);
		}
		void OnBeginRequest(object sender, EventArgs e)
		{
			LogManager.SuspendRecoding();
		}
		void OnError(object sender, EventArgs e)
		{
			Exception ex = CurrentContext.Server.GetLastError();
			LogManager.WriteError(ex);
			CurrentContext.Cache["LastError"] = ex;
			//CurrentContext.Server.ClearError();
			//OnEndRequest(sender, e);
		}
		void OnEndRequest(object sender, EventArgs e)
		{
			LogManager.PersistLog();
			LogManager.ResumeRecording();
		}
		#endregion
	}
}
