﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Xml.Linq;
using System.Web;
using System.Web.Services.Protocols;
using System.IO;
using System.Xml;
using System.Configuration;
using System.Reflection;
using System.Security.Cryptography;
using System.Runtime.InteropServices;
using System.Threading;
using System.Data.Common;

namespace QX3.Spinnex.Common.Services.Logging
{
	#region Atributos e Enumerados
	public enum LogMode
	{
		All, Logs, Errors
	}
	#endregion

	/// <summary>
	/// Responsável por gerenciar a geração de logs.
	/// </summary>
	public static class LogManager
	{
		#region Atributos e Enumerados


		

		private static Thread recordingThread;
		private static Queue<LogContent> logQueue;
		private static LogContent _logContent;
		#endregion

		#region Propriedades
		static internal LogConfig Config { get; set; }
		internal static LogContent CurrentLog
		{
			get
			{
				if (HttpContext.Current == null)
				{
					return _logContent;
				}
				else
				{
					LogContent content = (LogContent)HttpContext.Current.Items["LogContent"];

					return content;
				}
			}
			set
			{
				if (HttpContext.Current == null)
					_logContent = value;
				else
					HttpContext.Current.Items["LogContent"] = value;
			}
		}
		#endregion

		#region Construtores e Destrutores
		static LogManager()
		{
			Config = (LogConfig)ConfigurationManager.GetSection("qx3.log");
			if (Config != null)
			{
				logQueue = new Queue<LogContent>();
				recordingThread = new Thread(new ThreadStart(RecordLogs));
				recordingThread.Priority = ThreadPriority.Lowest;
				recordingThread.Name = "Logging Thread";
				//recordingThread.Start();
			}
		}
		#endregion

		#region Métodos
		public static void InitLog(string moduleName)
		{
			if (HttpContext.Current == null)
			{
				_transactionID = Guid.Empty;
				_logContent = null;
			}


			if (Config == null)
				return;
			EnsureLog();

			
			CurrentLog.SetModuleName(moduleName);
		}

		#region Metodos que manipulam TransactionID

		private static Guid _transactionID;

		public static Guid GetTransactionID()
		{

			string key = "___LogManager.TransactionID";

			if (HttpContext.Current == null)
			{
				if (_transactionID == Guid.Empty)
					_transactionID = Guid.NewGuid();
				return _transactionID;
			}

			if (HttpContext.Current.Items[key] == null)
			{
				Guid guid = Guid.NewGuid();
				HttpContext.Current.Items[key] = guid;
				return guid;
			}

			return (Guid)HttpContext.Current.Items[key];
		}

		internal static string EncodeTransactionID(Guid transactionID)
		{
			byte[] bArr = transactionID.ToByteArray();
			SHA1 sha1 = SHA1.Create();
			byte[] bHash = sha1.ComputeHash(bArr);

			byte[] byteKey = new byte[bArr.Length + bHash.Length];
			Array.Copy(bArr, byteKey, bArr.Length);
			Array.Copy(bHash, 0, byteKey, 16, bHash.Length);

			string encodedString = Convert.ToBase64String(byteKey);
			return encodedString;

		}
		internal static Guid DecodeTransactionID(string str)
		{
			byte[] bytes = Convert.FromBase64String(str);
			byte[] bGuid = new byte[16];
			Array.Copy(bytes, bGuid, 16);
			byte[] bHash = new byte[bytes.Length - 16];
			Array.Copy(bytes, 16, bHash, 0, bHash.Length);

			SHA1 sha1 = SHA1.Create();
			byte[] computedHash = sha1.ComputeHash(bGuid);
			if (computedHash.Length != bHash.Length)
				throw new ArgumentException("TransactionID inválido!");

			for (int i = 0; i < computedHash.Length; ++i)
			{
				byte a = bHash[i];
				byte b = computedHash[i];
				if (a != b)
					throw new ArgumentException("TransactionID inválido!");
			}

			Guid guid = new Guid(bGuid);

			return guid;
		}
		#endregion

		#region Metodos que manipulam a thread e a fila de gravação

		internal static void SuspendRecoding()
		{
			//if (recordingThread != null)
			//{
			//    if (recordingThread.ThreadState == System.Threading.ThreadState.Running)
			//        recordingThread.Suspend();
			//}
		}
		internal static void ResumeRecording()
		{
			//if (recordingThread != null)
			//{
			//    if (recordingThread.ThreadState == System.Threading.ThreadState.Suspended)
			//        recordingThread.Resume();
			//}
		}
		private static void RecordLogs()
		{
			LogContent log = null;
			while (logQueue != null)
			{
				log = null;
				if (logQueue.Count > 0)
				{
					log = logQueue.Dequeue();
					_PersistLog(log);
				}
				else
					Thread.Sleep(10000);

			}
		}
		#endregion

		#region Métodos de escrita de Log

		public static void WriteLog(string format, params object[] arguments)
		{
			if (LogManager.Config == null)
				return;

			string str = string.Format(format, arguments);
			WriteLog(str);
		}

		public static void WriteLog(string message)
		{
			System.Diagnostics.Trace.WriteLine(message);
			EnsureLog();

			if (LogManager.Config == null)
				return;

			if (HttpContext.Current != null)
			{
				HttpContext.Current.Trace.Write("LOG: " + message);
			}
			CurrentLog.AddLog(message);
		}

		public static void WriteLog(XElement element)
		{
			if (LogManager.Config == null)
				return;


			CurrentLog.AddLog(element);
		}

		public static void WriteObjectToLog(object obj)
		{

			if (LogManager.Config == null)
				return;


			XElement element = new XElement("object",
				ListProperties(obj)
				);
			WriteLog(element);
		}
		public static void DumpCommand(DbCommand cmd)
		{
			StringBuilder sb = new StringBuilder(200);
			sb.AppendFormat("{0}\n-----------------------------------------------------\n", cmd.CommandText);
			foreach (DbParameter p in cmd.Parameters)
				sb.AppendFormat("{0} = \"{1}\" ({2})\n", p.ParameterName, p.Value, p.DbType);

			WriteLog(sb.ToString());
		}

		public static void WriteError(Exception ex)
		{
			System.Diagnostics.Trace.WriteLine(ex.ToString());

			EnsureLog();
			if (LogManager.Config == null)
				return;
			if (HttpContext.Current != null)
				HttpContext.Current.Trace.Write("ERRO: " + ex.ToString());

			CurrentLog.AddError(ex);
		}
		#endregion

		#region Métodos para serializar objetos no Log

		private static XElement[] ListProperties(object obj)
		{
			if (obj == null) return new XElement[0];
			Type t = obj.GetType();

			return (from p in t.GetProperties()
					where p.PropertyType.IsPrimitive || p.PropertyType.Name == "System.String"
					select new XElement(p.Name, GetPropertyValue(obj, p))).Union(
				   from p in t.GetProperties()
				   where !p.PropertyType.IsPrimitive && p.PropertyType.Name != "System.String"
				   select new XElement(p.Name, ListProperties(p.GetValue(obj, null)))
				   ).ToArray();

		}

		private static object GetPropertyValue(object source, PropertyInfo p)
		{
			try
			{
				return p.GetValue(source, null);
			}
			catch (Exception)
			{

			}
			return "NOT AVAILABLE";
		}
		#endregion

		private static void EnsureLog()
		{
			if (LogManager.Config == null)
				return;

			LogContent content = CurrentLog;

			if (content == null)
			{
				CurrentLog = new LogContent();
			}
		}

		internal static void PersistLog()
		{
			if (LogManager.Config == null)
				return;

			if (CurrentLog != null)
			{
				var log = CurrentLog;
				log.SetTotalRequestTime();
				if (log.IsImportant)
				{
					//ThreadPool.QueueUserWorkItem(new WaitCallback(LogManager._PersistLog), log);
					//_PersistLog(log);
					//logQueue.Enqueue(log);
					_PersistLog(log);
				}
				CurrentLog = null;
			}
		}

		private static void _PersistLog(LogContent log)
		{
			foreach (LogWriterConfig loggerConfig in Config.LogWriterTypes)
			{
				if (CheckLogCondition(loggerConfig, log))
				{
					if (!loggerConfig.ShouldLog(log.GetModuleName()))
					{
						log.AddLog("Passando " + loggerConfig.LoggerType.Name);
						continue;
					}
					ILogger logger = (ILogger)Activator.CreateInstance(loggerConfig.LoggerType);
					logger.Init(loggerConfig.LogWriterParams);
					bool success = logger.PersistLog(log);
					
				}
			}
		}

		private static bool CheckLogCondition(LogWriterConfig config, LogContent content)
		{

			

			if (config.Mode == LogMode.All)
				return true;

			if (content.ContainsErrors && config.Mode == LogMode.Errors)
				return true;
			if (!content.ContainsErrors && config.Mode == LogMode.Logs)
				return true;
			return false;
		}
		

		public static void ClearLog()
		{
			CurrentLog = null;

		}
		#endregion
	}

	public interface IUserInformationProvider
	{
		#region Propriedades
		string UserID { get; }
		string UserName { get; }
		string DisplayName { get; }
		KeyValuePair<string, string>[] Properties { get; }
		#endregion
	}

	public interface ILogger
	{
		#region Métodos
		void Init(string config);
		bool PersistLog(LogContent content);
		#endregion
	}
}
