﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Diagnostics;
using System.Web;

namespace QX3.Spinnex.Common.Services.Logging
{
	public class WebServiceLogger : ILogger
	{
		#region Atributos e Enumerados
		WS.QX3.Trace.TraceWebService svc;
		#endregion

		#region Métodos
		#region ILogger Members

		public void Init(string config)
		{


            try
            {
                svc = new QX3.Spinnex.Common.Services.WS.QX3.Trace.TraceWebService();
				if (!String.IsNullOrEmpty(config))
					svc.Url = config;
            }
            catch (Exception ex)
            {
				LogManager.WriteError(ex);
                if (HttpContext.Current != null)
                    HttpContext.Current.Trace.Warn(ex.ToString());

            }

		}

		public bool PersistLog(LogContent content)
		{
            if (svc == null)
                return false;


			if (content == null)
				return false;

			try
			{
				var data = content.GetLogData();
				if (data == null)
					return false;

				var doc = new XmlDocument();
				MemoryStream ms = new MemoryStream();
				XmlWriter xw = new XmlTextWriter(ms, Encoding.Default);
				data.Save(xw);
				xw.Flush();
				ms.Position = 0;
				doc.Load(ms);
				xw.Close();
				ms.Close();

				svc.CreateTraceRecord(doc.DocumentElement);
				return true;
			}
			catch (Exception ex)
			{
				content.AddError(ex);
				return false;
			}

		}

		#endregion
		#endregion
	}
}
