﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.IO;

namespace QX3.Spinnex.Common.Services.Logging
{
	public class FileLogger : ILogger
	{
		#region Propriedades
		public string FolderName { get; set; }
		#endregion

		#region Métodos
		public void Init(string config)
		{
			FolderName = config;

			if (!Directory.Exists(FolderName))
				Directory.CreateDirectory(FolderName);
		}

		public bool PersistLog(LogContent content)
		{
			if (content == null)
				return false;

			try
			{
				XDocument xml = content.GetLogData();
				string filename = Path.Combine(FolderName, string.Format("{0}_{1}_{2}.xml", DateTime.Now.ToString("yyyyMMddHHmmss"), content.GetModuleName(), Guid.NewGuid().ToString("P")));
				xml.Save(filename);
				return true;
			}
			catch (Exception ex)
			{
				LogManager.WriteError(ex);
				return false;
			}
		}
		#endregion
	}
}
