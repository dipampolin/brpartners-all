﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace QX3.Spinnex.Common.Services.Logging
{
	public class LogConfig
	{
		#region Atributos e Enumerados
		public Type UserInformationProviderType;
		#endregion

		#region Propriedades
		public LogWriterConfig[] LogWriterTypes { get; set; }
		public string AppName { get; set; }
		public string DefaultModuleName { get; set; }
		#endregion
	}

	public class LogWriterConfig
	{
		#region Propriedades
		public Type LoggerType { get; set; }
		public string LogWriterParams { get; set; }
		public LogMode Mode { get; set; }



		public List<string> logRestrictions { get; set; }
		public bool ShouldLog(string moduleName)
		{
			bool shouldLog;
			if (this.logRestrictions == null)
				shouldLog = true;
			else
				shouldLog = logRestrictions.Contains(moduleName);
			
			return shouldLog;
		}

		#endregion
	}

	public class LogConfigHandler : System.Configuration.IConfigurationSectionHandler
	{
		#region Métodos
		public object Create(object parent, object configContext, XmlNode section)
		{
			LogConfig config = new LogConfig();


			XmlAttribute att = section.Attributes["userInformationProvider"];
			if (att != null)
			{
				config.UserInformationProviderType = Type.GetType(att.Value);
			}
			att = section.Attributes["applicationId"];
			if (att != null)
				config.AppName = att.Value;
			//else
			//    throw new Exception("LOG: Revise a configuração do sistema de log! Parâmetro applicationId não informado!");

			att = section.Attributes["defaultModule"];
			if (att == null)
				config.DefaultModuleName = "SITE";
			else
				config.DefaultModuleName = att.Value;

			XmlNodeList list = section.SelectNodes("loggers/logger");
			config.LogWriterTypes = new LogWriterConfig[list.Count];

			int i = 0;

			foreach (XmlNode node in list)
			{
				Type type = Type.GetType(node.Attributes["type"].Value);
				XmlAttribute attrib = node.Attributes["param-string"];

				string paramString = null;
				if (attrib != null)
					paramString = attrib.Value;


				LogMode mode = LogMode.All;
				attrib = node.Attributes["mode"];
				if (attrib != null)
					mode = (LogMode)Enum.Parse(typeof(LogMode), attrib.Value);

				LogWriterConfig logger = new LogWriterConfig();
				logger.LogWriterParams = paramString;
				logger.LoggerType = type;
				logger.Mode = mode;
				config.LogWriterTypes[i++] = logger;

				HandleLogConfigChildren(node.FirstChild, logger);

			}

			return config;


		}

		private void HandleLogConfigChildren(XmlNode node, LogWriterConfig config)
		{
			for (; node != null; node = node.NextSibling)
			{
				switch (node.Name)
				{
					case "restrictions":
						HandleLogRestrictions(node.FirstChild, config);
						break;
				}
			}
		}
		private void HandleLogRestrictions(XmlNode node, LogWriterConfig config)
		{
			config.logRestrictions = new List<string>();
			for (; node != null; node = node.NextSibling)
			{
				switch (node.Name)
				{
					case "clear":
						config.logRestrictions.Clear();
						break;
					case "add":
						config.logRestrictions.Add(node.Attributes["name"].Value);
						break;
				}
			}
		}

		#endregion
	}
}
