using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace QX3.Spinnex.Common.Services.Formatting
{
	/// <summary>
	/// Classe est�tica com m�todos de formata��o comuns.
	/// </summary>
	public class GeneralFormatter
	{
		#region M�todos

		/// <summary>
		/// Coloca zeros � direita para completar um CPF ou CNPJ.
		/// </summary>
		/// <param name="pstrCPF">CPF</param>
		/// <returns></returns>
		public static string FormatCPF(string pstrCPF)
		{
			if (pstrCPF.Length < 11) // cpf
				pstrCPF = pstrCPF.PadLeft(11, '0');
			else if (pstrCPF.Length > 11) // cnpj
				pstrCPF = pstrCPF.PadLeft(14, '0');

			return pstrCPF;
		}
		/// <summary>
		/// Formata n�meros adicionando os sufixos K (mil), M (milh�o) e B (bilh�o).
		/// O par�metro decPlaces informa a quantidade de casas decimais.
		/// </summary>
		/// <example>2500 => 2.50K
		/// 1667830 => 1.66M
		/// 35487256740 => 35.49B
		/// </example>
		/// <param name="number">N�mero a ser formatado</param>
		/// <param name="decPlaces">Quantidade de casas decimais.</param>
		/// <returns></returns>
		public static string FormatKMB(decimal number, int decPlaces)
		{
			string sufix;
			if (number > 1000000000)
			{
				number /= 1000000000;
				sufix = "B";
			}
			else if (number > 1000000)
			{
				number /= 1000000;
				sufix = "M";
			}
			else if (number >= 1000)
			{
				number /= 1000;
				sufix = "K";
			}
			else
				sufix = "";
			string format = "{0:N" + decPlaces + "}{1}";

			return string.Format(format, number, sufix);

		}
		/// <summary>
		/// Formata n�meros adicionando os sufixos K (mil), M (milh�o) e B (bilh�o).
		/// O par�metro decPlaces informa a quantidade de casas decimais.
		/// </summary>
		/// <example>2500 => 2.500
		/// 1667830 => 1.667K
		/// 35487256740 => 35.487M
		/// </example>
		/// <remarks>
		/// Esta vers�o formata em valores de mil.</remarks>
		/// <param name="number">N�mero a ser formatado</param>
		/// <param name="decPlaces">Quantidade de casas decimais.</param>
		/// <returns></returns>
		public static string FormatKMBLarge(decimal number, int decPlaces)
		{
			string sufix;
			if (number > 1000000000000)
			{
				number /= 1000000000;
				sufix = "B";
			}
			else if (number > 1000000000)
			{
				number /= 1000000;
				sufix = "M";
			}
			else if (number >= 1000000)
			{
				number /= 1000;
				sufix = "K";
			}
			else
				sufix = "";
			string format = "{0:N" + decPlaces + "}{1}";

			return string.Format(format, number, sufix);

		}

		public static char RemoveAccents(char value)
		{
			return RemoveAccents(value.ToString())[0];
		}

		public static string RemoveAccents(string value)
		{
			string strNewValue = value;
			strNewValue = Regex.Replace(strNewValue, "[����]", "a");
			strNewValue = Regex.Replace(strNewValue, "[����]", "A");
			strNewValue = Regex.Replace(strNewValue, "[���]", "e");
			strNewValue = Regex.Replace(strNewValue, "[���]", "e");
			strNewValue = Regex.Replace(strNewValue, "[���]", "i");
			strNewValue = Regex.Replace(strNewValue, "[���]", "I");
			strNewValue = Regex.Replace(strNewValue, "[�����]", "o");
			strNewValue = Regex.Replace(strNewValue, "[����]", "O");
			strNewValue = Regex.Replace(strNewValue, "[���]", "u");
			strNewValue = Regex.Replace(strNewValue, "[���]", "U");
			strNewValue = Regex.Replace(strNewValue, "[�]", "c");
			strNewValue = Regex.Replace(strNewValue, "[�]", "C");
			return strNewValue;
		}

		#endregion
	}
}
