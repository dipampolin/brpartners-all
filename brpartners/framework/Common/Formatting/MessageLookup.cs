//Namespaces .NET Framework
using System;
using System.Configuration;
using System.Globalization;
using System.Web;
using System.Web.Caching;
using System.Xml;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Threading;
using System.Text;

namespace QX3.Spinnex.Common.Services.Formatting
{
	/// <summary>
	/// Classe que implementa o sistema de mensagens QX3.
	/// </summary>
	/// <see cref="http://wiki.qx3.com.br/mediawiki/index.php/QX3.Spinnex.Common:_Sistema_de_Mensagens"/>
	public static class MessageLookup
	{
		#region M�todos
		/// <summary>
		/// Carrega documento XML contendo as mensagens.
		/// </summary>
		/// <remarks>
		/// <list>
		/// <item>Se o documento XML j� estiver armazenado no Cache do ASP.NET, retorna documento do cache.</item>
		/// <item>Se o arquivo em disco for modificado, o cache perder� validade na pr�xima chamada ao m�todo.</item>
		/// </list>
		/// </remarks>
		/// <returns></returns>
		private static XmlDocument GetMessagesXml()
		{
			lock (typeof(MessageLookup))
			{
				XmlDocument document = null;

				if (HttpContext.Current != null)
				{
					document = (XmlDocument)HttpContext.Current.Cache["ErrorMessages"];
				}
				else
				{
					document = new XmlDocument();  
					document.Load(ConfigurationManager.AppSettings["MessagesPath"]);
				}

				if (document != null)
					return document;

				document = new XmlDocument();
				string path = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["MessagesPath"]);
				document.Load(path);
				HttpContext.Current.Cache.Add("ErrorMessages", document, new CacheDependency(path), DateTime.MaxValue, TimeSpan.Zero, CacheItemPriority.High, null);

				return document;
			}
		}
		/// <summary>
		/// Recupera mensagens na cultura corrente.
		/// </summary>
		/// <param name="messageCode">C�digo de mensagem.</param>
		/// <returns>Mensagem</returns>
		public static string GetMessage(string messageCode)
		{
			CultureInfo ci = CultureInfo.CurrentUICulture;
			if (HttpContext.Current == null)
				ci = Thread.CurrentThread.CurrentCulture;
			string message = GetMessage(messageCode, ci);
			return message;
		}

		/// <summary>
		/// Recupera mensagem na cultura especificada.
		/// </summary>
		/// <param name="messageCode">C�digo da mensagem</param>
		/// <param name="culture">Cultura</param>
		/// <returns>Mensagem</returns>
		public static string GetMessage(string messageCode, CultureInfo culture)
		{
			XmlDocument document = GetMessagesXml();
			string query = string.Format(@"/messages/culture[@code='{0}']/message[@code='{1}']", culture.Name, messageCode);
			XmlNodeList xl = document.SelectNodes(query);

			if (xl.Count == 0)
				return "Mensagem n�o encontrada: " + messageCode;
			XmlNode node = xl[0];
			return node.FirstChild.Value;
		}


        public static IDictionary<string, string> FilterMessages(string text)
        {
            CultureInfo ci = CultureInfo.CurrentUICulture;
            return FilterMessages(text, ci);
        }

        public static IDictionary<string, string> FilterMessages(string text, CultureInfo ci)
        {
            
            var list = GetMessagesXml().SelectNodes(string.Format("/messages/culture[@code='{0}']/message[contains(@code, '" + text + "')]", ci.Name, text));

            var dct = new Dictionary<string, string>(list.Count);
            
            
            foreach (XmlNode node in list)
            {
                string code = node.Attributes["code"].Value;
                string message = node.FirstChild.Value;
                dct.Add(code, message);
            }
            return dct;
        }


		public static Dictionary<string, string> GetMessages()
		{
			return GetMessages(CultureInfo.CurrentUICulture);
		}
		public static Dictionary<string, string> GetMessages(CultureInfo culture)
		{
			XmlDocument document = GetMessagesXml();
			string query = string.Format(@"/messages/culture[@code='{0}']/message", culture.Name);
			XmlNodeList xl = document.SelectNodes(query);

			Dictionary<string, string> messages = new Dictionary<string, string>(xl.Count);
			foreach (XmlNode node in xl)
			{
				messages.Add(node.Attributes["code"].Value, node.FirstChild.Value);
			}
			return messages;
		}


        public static string ConcatenateMessages(string prefix, string sufixes)
        {
            StringBuilder sb = new StringBuilder();
            string[] items = sufixes.Split(',');
            int length = items.Length;
            for(int i = 0; i < length; ++i)
            {
                string key = prefix+items[i].Trim();
                string message = GetMessage(key);
                if(i>0)
                    sb.Append(", ");
                sb.Append(message);
            }
            return sb.ToString();
        }


		#endregion
	}
}
