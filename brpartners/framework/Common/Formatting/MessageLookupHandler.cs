﻿// Namespaces .NET Framework
using System;
using System.Text;
using System.Web;

namespace QX3.Spinnex.Common.Services.Formatting
{
	/// <summary>
	/// Interface JavaScript para o sistema de mensagens.
	/// </summary>
	public class MessageLookupHandler : IHttpHandler
	{

		#region Propriedades

		/// <summary>
		/// Propriedade IsReusable da interface IHttpHandler.
		/// </summary>
		public bool IsReusable
		{
			get { return true; }
		}
		#endregion
		#region Métodos
		/// <summary>
		/// Printa uma função que traduz códigos para mensagens.
		/// </summary>
		/// <param name="context">Contexto HTTP atual.</param>
		public void ProcessRequest(HttpContext context)
		{
			var response = context.Response;
			response.ContentType = "text/javascript";

			StringBuilder sb = new StringBuilder();
			sb.Append("function lookupMessage(code)\n{\n\tswitch(code)\n\t{\n");

			foreach (var item in MessageLookup.GetMessages())
				sb.AppendFormat("\t\tcase '{0}': return '{1}';\n", item.Key, item.Value);
			sb.Append("\t\tdefault: return 'Mensagem não encontrada: ' + code + '.';\n");
			sb.Append("\t}\n}");

			response.Write(sb.ToString());
		}
		#endregion
	}
}
