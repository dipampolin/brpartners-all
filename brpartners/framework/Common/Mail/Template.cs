using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Web;

namespace QX3.Spinnex.Common.Services.Mail
{
	public class Template
	{
		#region Atributos e Enumerados
		private StringBuilder sb = new StringBuilder();
		private string m_VariableFormat;
		#endregion

		#region Propriedades
		public string VariableFormat
		{
			get { string s = m_VariableFormat; if (s == null) s = "#({0})#"; return s; }
			set { m_VariableFormat = value; }
		}


		public string Body
		{
			get { return sb.ToString(); }
			set
			{
				sb.Remove(0, sb.Length);
				sb.Append(value);
			}
		}
		#endregion

		#region Construtores e Destrutores
		public Template()
		{
		}
		public Template(string filename)
		{
			Load(filename);
		}
		#endregion

		#region M�todos
		public void Load(string filename)
		{
			StreamReader sr = null;
			try
			{
				sr = new StreamReader(filename);
				sb.Append(sr.ReadToEnd());
			}
			finally
			{
				if (sr != null) sr.Close();
			}
		}
		public void ReplaceVars(params KeyValuePair<string, object>[] variaveis)
		{

			List<KeyValuePair<string, object>> list = new List<KeyValuePair<string, object>>();
			list.AddRange(variaveis);
			list.Add(new KeyValuePair<string, object>("LongDate", DateTime.Now.ToLongDateString()));
			list.Add(new KeyValuePair<string, object>("DateTime", DateTime.Now.ToString()));


			foreach (KeyValuePair<string, object> pair in list)
				sb.Replace(string.Format(VariableFormat, pair.Key), pair.Value.ToString());
		}
		public string ApplyTemplate(string filename, params KeyValuePair<string, object>[] variaveis)
		{
			Load(filename);
			ReplaceVars(variaveis);

			return Body;
		}
		public override string ToString()
		{
			return Body;
		}
		#endregion
	}
}
