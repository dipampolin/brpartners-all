using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net.Mail;
using System.Web;
using System.ComponentModel;
using System.Configuration;
using QX3.Spinnex.Common.Services.Generics;

using QX3.Spinnex.Common.Services.Security;
using System.Net;
using System.Xml;


namespace QX3.Spinnex.Common.Services.Mail
{
    public class SendMail : QX3.Spinnex.Common.Services.Mail.ISendMail
    {
        #region Atributos e Enumerados
        private string m_EmailTo;
        private string m_EmailFrom;
        private Template m_Template = new Template();
        private KeyValuePair<string, object>[] m_Variables = new KeyValuePair<string, object>[0];
        private bool m_IsBodyHtml = true;
        private string m_Subject;
        #endregion

        #region Propriedades



        /// <summary>
        /// Permite definir op��es de notifica��o de entrega da mensagem, a serem passadas para a classe System.Net.MailMessage.
        /// </summary>
        /// <seealso cref="http://msdn.microsoft.com/en-us/library/system.net.mail.deliverynotificationoptions.aspx"/>
        public DeliveryNotificationOptions DeliveryNotifications { get; set; }

        public MailPriority Priority { get; set; }


        public string EmailTo
        {
            get { return m_EmailTo; }
            set { m_EmailTo = value; }
        }

        public string CC { get; set; }
        public string BCC { get; set; }

        public string EmailFrom
        {
            get { return m_EmailFrom; }
            set { m_EmailFrom = value; }
        }
        public string Body
        {
            get { return m_Template.Body; }
            set { m_Template.Body = value; }
        }
        public KeyValuePair<string, object>[] Variables
        {
            get { return m_Variables; }
            set { m_Variables = value; }
        }
        public string VariableFormat
        {
            get
            {
                return m_Template.VariableFormat;
            }
            set
            {
                m_Template.VariableFormat = value;
            }
        }
        public bool IsBodyHtml
        {
            get { return m_IsBodyHtml; }
            set { m_IsBodyHtml = value; }
        }
        public string Subject
        {
            get { return m_Subject; }
            set { m_Subject = value; }
        }
        public List<Attachment> Attachments { get; set; }
        #endregion

        #region M�todos
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        public void LoadTemplate(string filename)
        {
            this.m_Template.Load(filename);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="fileStream"></param>
        public void AttachFile(string filename, Stream fileStream)
        {
            this.Attachments.Add(new Attachment(fileStream, filename));
        }
        /// <summary>
        /// 
        /// </summary>
        public SendMail()
        {
            Attachments = new List<Attachment>();
        }
        /// <summary>
        /// 
        /// </summary>
        public void Send()
        {
            if (EmailTo == null)
                throw new ArgumentException("Informe o destinat�rio!");
            if (Body == null && m_Template == null)
                throw new ArgumentException("Corpo do email vazio!");

            Template template = this.m_Template;
            template.ReplaceVars(this.Variables);
            string corpo = template.Body;

            MailMessage msg = new MailMessage();

            foreach (string s in EmailTo.Split(';'))
                if (s.Length > 0)
                    msg.To.Add(s);

            if (EmailFrom != null)
                msg.From = new MailAddress(EmailFrom);

            msg.Subject = this.Subject;
            if (CC != null)
                foreach (string email in CC.Split(','))
                    msg.CC.Add(email.Trim());
            if (BCC != null)
                foreach (string email in BCC.Split(','))
                    msg.Bcc.Add(email.Trim());


            msg.Body = corpo;
            msg.Priority = this.Priority;
            msg.IsBodyHtml = this.IsBodyHtml;
            msg.DeliveryNotificationOptions = DeliveryNotifications;

            foreach (var attachment in Attachments)
                msg.Attachments.Add(attachment);

            SmtpClient client = new SmtpClient();
            
            bool quietErrors = ConfigurationManager.AppSettings["SendMail.QuietErrors"] == "1";
            try
            {
                client.Send(msg);
            }
            catch (Exception ex)
            {
                if (!quietErrors)
                    throw new Exception(ex.Message, ex);
            }
        }

        [Obsolete("Esta classe n�o � mais est�tica. Prefira instancia-la ao inv�s de usar este m�todo. A classe instanciada cont�m v�rias op��es que n�o est�o presentes aqui.", true)]
        public static void Send(string email_to, string assunto, string caminho_corpo, params KeyValuePair<string, object>[] variaveis)
        {
            Template template = new Template(caminho_corpo);
            template.VariableFormat = "#({0})#";
            template.ReplaceVars(variaveis);
            string corpo = template.Body;

            MailMessage msg = new MailMessage();
            foreach (string s in email_to.Split(';'))
                if (s.Length > 0)
                    msg.To.Add(s);
            msg.Subject = assunto;
            msg.Body = corpo;
            msg.IsBodyHtml = true;

            SmtpClient client = new SmtpClient();

            



            client.Send(msg);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idUser"></param>
        /// <param name="FileName"></param>
        /// <param name="email_to"></param>
        /// <param name="assunto"></param>
        /// <param name="caminho_corpo"></param>
        /// <param name="email_from"></param>
        /// <param name="variaveis"></param>
        public static void SendEmailWithAttachment(string idUser, string FileName, string email_to, string copyEmail_to, string assunto, string caminho_corpo, string email_from, params KeyValuePair<string, object>[] variaveis)
        {
            try
            {

                Template template = new Template(caminho_corpo);

                template.ReplaceVars(variaveis);
                string corpo = template.Body;

                MailAddress from = new MailAddress(email_from);
                MailAddress To = new MailAddress(email_to);
                MailAddress Cc = new MailAddress(copyEmail_to);
                MailMessage msg = new MailMessage();

                msg.From = from;
                foreach (string s in email_to.Split(';'))
                {
                    int i = msg.To.IndexOf(new MailAddress(s));
                    if ((s.Length > 0) && (i == -1))
                        msg.To.Add(s);
                }
                foreach (string s in copyEmail_to.Split(';'))
                {
                    int i = msg.CC.IndexOf(new MailAddress(s));
                    if ((s.Length > 0) && (i == -1))
                        msg.CC.Add(s);
                }

                msg.Subject = assunto;
                msg.Body = corpo;
                msg.IsBodyHtml = true;

                string staticUrl = ConfigurationManager.AppSettings["StorageUrl"].ToString();
                string dir = "/Documents/Email/Temp/UserID_" + idUser;
                string path = staticUrl + dir;
                string url = path + "/" + FileName;

                //n�o foi poss�vel anexar no email o arquivo remoto. //baixando o arquivo para pasta Temp do cliente.
                WebClient wc = new WebClient();
                byte[] inputData = wc.DownloadData(url);
                string tempPath = Path.GetTempPath();
                var fs = new FileStream(tempPath + FileName, FileMode.OpenOrCreate, FileAccess.Write);
                fs.Write(inputData, 0, inputData.Length);
                fs.Flush();
                fs.Close();


                //anexando fs ao email
                /*System.Net.Mail.Attachment attachment;
                  attachment = new System.Net.Mail.Attachment(fs.Name);*/
                Attachment attachment = new Attachment(fs.Name);
                msg.Attachments.Add(attachment);


                //deletando o arquivo remoto
                string HandlerPath = ConfigurationManager.AppSettings["StorageHandlerPath"];
                string urlDel = staticUrl + HandlerPath + "?filename=" + dir + "/" + FileName + "&action=delete";
                var resp = wc.DownloadData(urlDel);
                XmlDocument doc = new XmlDocument();
                MemoryStream ms = new MemoryStream(resp);
                doc.Load(ms);


                //enviando o email
                SmtpClient client = new SmtpClient();
                client.Send(msg);
                msg.Dispose();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }

        }


        public static void SendWithFrom(string email_to, string assunto, string caminho_corpo, string email_from, params KeyValuePair<string, object>[] variaveis)
        {
            Template template = new Template(caminho_corpo);
            template.ReplaceVars(variaveis);
            string corpo = template.Body;

            MailAddress from = new MailAddress(email_from);
            MailAddress To = new MailAddress(email_to);
            MailMessage msg = new MailMessage();

            msg.From = from;
            bool isBodyHtml = true;

            if (caminho_corpo.ToLower().EndsWith(".txt"))
            {
                isBodyHtml = false;
                AlternateView av = AlternateView.CreateAlternateViewFromString(corpo);
                av.ContentType = new System.Net.Mime.ContentType("text/plain");
                msg.AlternateViews.Add(av);
                msg.BodyEncoding = Encoding.UTF8;
                msg.SubjectEncoding = Encoding.UTF8;
            }

            foreach (string s in email_to.Split(';'))
            {
                int i = msg.To.IndexOf(new MailAddress(s));
                if ((s.Length > 0) && (i == -1))
                    msg.To.Add(s);
            }
            msg.Subject = assunto;
            msg.Body = corpo;
            msg.IsBodyHtml = isBodyHtml;



            SmtpClient client = new SmtpClient();

            client.Send(msg);
        }

        public static void SendWithFrom(string email_to, string assunto, string caminho_corpo, string email_from, bool isBodyHtml, params KeyValuePair<string, object>[] variaveis)
        {
            Template template = new Template(caminho_corpo);
            template.ReplaceVars(variaveis);
            string corpo = template.Body;

            MailAddress from = new MailAddress(email_from);
            MailAddress To = new MailAddress(email_to);
            MailMessage msg = new MailMessage();

            msg.From = from;

            if (caminho_corpo.ToLower().EndsWith(".txt"))
            {
                isBodyHtml = false;
                AlternateView av = AlternateView.CreateAlternateViewFromString(corpo);
                av.ContentType = new System.Net.Mime.ContentType("text/plain");
                msg.AlternateViews.Add(av);
                msg.BodyEncoding = Encoding.UTF8;
                msg.SubjectEncoding = Encoding.UTF8;
            }

            foreach (string s in email_to.Split(';'))
            {
                int i = msg.To.IndexOf(new MailAddress(s));
                if ((s.Length > 0) && (i == -1))
                    msg.To.Add(s);
            }
            msg.Subject = assunto;
            msg.Body = corpo;
            msg.IsBodyHtml = isBodyHtml;



            SmtpClient client = new SmtpClient();

            client.Send(msg);
        }

        public static void SendWithFromReplyTo(string email_to, string assunto, string caminho_corpo, string email_from, string email_reply, params KeyValuePair<string, object>[] variaveis)
        {
            Template template = new Template(caminho_corpo);
            template.ReplaceVars(variaveis);
            string corpo = template.Body;

            MailAddress from = new MailAddress(email_from);
            MailAddress To = new MailAddress(email_to);
            MailAddress replyTo = new MailAddress(email_reply);
            MailMessage msg = new MailMessage();
            //MailMessage msg = new MailMessage(from, To);
            //antigo ok
            //foreach (string s in email_to.Split(';'))
            //{                
            //    if (s.Length > 0)
            //        msg.To.Add(s);
            //}
            msg.From = from;
            msg.ReplyTo = replyTo;
            foreach (string s in email_to.Split(';'))
            {
                int i = msg.To.IndexOf(new MailAddress(s));
                if ((s.Length > 0) && (i == -1))
                    msg.To.Add(s);
            }
            msg.Subject = assunto;
            msg.Body = corpo;
            msg.IsBodyHtml = true;


            SmtpClient client = new SmtpClient();


            client.Send(msg);

        }

        public static string BuidSiteUrl(string caminhoVirtual)
        {
            HttpContext ctx = HttpContext.Current;
            string url = "http://" + ctx.Request.ServerVariables["SERVER_NAME"];
            string port = ctx.Request.ServerVariables["SERVER_PORT"];
            if (port != "80")
                url += ":" + port;
            url += caminhoVirtual;
            return url;
        }
        #endregion
    }
}
