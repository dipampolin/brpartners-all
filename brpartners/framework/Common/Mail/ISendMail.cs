﻿using System;
namespace QX3.Spinnex.Common.Services.Mail
{
    /// <summary>
    /// Interface da classe SendMail.
    /// </summary>
	public interface ISendMail
	{
        /// <summary>
        /// Anexa um arquivo na mensagem.
        /// </summary>
        /// <param name="filename">Filename do anexo dentro do email</param>
        /// <param name="fileStream">Dados do anexo</param>
		void AttachFile(string filename, System.IO.Stream fileStream);
        /// <summary>
        /// Acessa a lista de anexos.
        /// </summary>
		System.Collections.Generic.List<System.Net.Mail.Attachment> Attachments { get; set; }
        /// <summary>
        /// Cópia oculta
        /// </summary>
		string BCC { get; set; }
        /// <summary>
        /// Corpo da mensagem (carregado de um template ou criado programaticamente)
        /// </summary>
		string Body { get; set; }
        /// <summary>
        /// Endereços para cópia.
        /// </summary>
		string CC { get; set; }
        /// <summary>
        /// Flags de entrega do email.
        /// </summary>
		System.Net.Mail.DeliveryNotificationOptions DeliveryNotifications { get; set; }
        /// <summary>
        /// E-mail do remetente.
        /// </summary>
		string EmailFrom { get; set; }
        /// <summary>
        /// E-mail do destinatário. Pode ser separado por vírgula.
        /// </summary>
		string EmailTo { get; set; }
        /// <summary>
        /// Indica se o body do email é HTML
        /// </summary>
		bool IsBodyHtml { get; set; }
        /// <summary>
        /// Carrega um template a partir do arquivo
        /// </summary>
        /// <param name="filename">Caminho físico do template</param>
		void LoadTemplate(string filename);
        /// <summary>
        /// Prioridade de entrega do e-mail.
        /// </summary>
		System.Net.Mail.MailPriority Priority { get; set; }
        /// <summary>
        /// Envia a mensagem preparada.
        /// </summary>
		void Send();
        /// <summary>
        /// Assunto do email
        /// </summary>
		string Subject { get; set; }
        /// <summary>
        /// Formato de variáveis no corpo do email
        /// </summary>
		string VariableFormat { get; set; }
        /// <summary>
        /// Lista de variáveis no corpo do email.
        /// </summary>
		System.Collections.Generic.KeyValuePair<string, object>[] Variables { get; set; }
	}
}
