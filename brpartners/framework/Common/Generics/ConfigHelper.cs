﻿using System;
using System.Configuration;
namespace QX3.Spinnex.Common.Services.Generics
{
    /// <summary>
    /// Helper para acesso às AppSettings da aplicação.
    /// </summary>
	public class ConfigHelper
	{
        /// <summary>
        /// Recupera uma AppSetting.
        /// </summary>
        /// <param name="key">Chave na seção AppSettings do arquivo de configuração.</param>
        /// <returns>Valor da AppSetting</returns>
        /// <exception cref="ApplicationException">Caso key não seja a chave de uma AppSetting.</exception>
        public static string GetAppSetting(string key)
        {
            string value = ConfigurationManager.AppSettings[key];
            if (value == null)
                throw new ApplicationException(string.Format("Parâmetro de configuração faltando: \"{0}\"", key));
            return value;
        }
        /// <summary>
        /// Recupera uma AppSetting e converte para o tipo especificado em T.
        /// </summary>
        /// <typeparam name="T">Tipo para o qual o valor do parâmetro será convertido.</typeparam>
        /// <param name="key">Chave na seção AppSettings do arquivo de configuração.</param>
        /// <returns>Valor da AppSetting</returns>
		public static T GetAppSetting<T>(string key) where T : IConvertible
		{
            string value = GetAppSetting(key);
			try
			{
				return (T)Convert.ChangeType(value, typeof(T));
			}
			catch (Exception)
			{
				throw new ApplicationException(string.Format("Parâmetro \"{0}\" inválido! ({1})", key, typeof(T).Name));
			}
		}

	}
}
