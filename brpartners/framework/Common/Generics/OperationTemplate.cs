﻿
using QX3.Spinnex.Common.Services.Data;
using System;
namespace QX3.Spinnex.Common.Services.Generics
{
	public enum OpResultType { OK, ERROR, WARNING }

	public class OperationResponse<ResultType>
	{
		#region Propriedades
		public ResultType Result { get; set; }
		public string ErrorMessage { get; set; }
		public string ValidationMessage { get; set; }
        public OperationResponse()
        {

        }
		#endregion
	}

	public class OperationResponse<ResultType, DataType> : OperationResponse<ResultType>
	{
		#region Propriedades
		public DataType Data { get; set; }
		#endregion
	}

	public class OperationRequest<DataType>
	{
		#region Propriedades
		public DataType Data { get; set; }
		#endregion
	}

	public class OperationRequest<OperationType, DataType> : OperationRequest<DataType>
	{
		#region Propriedades
		public OperationType Operation { get; set; }
		#endregion
	}

	public class CrudOperationRequest<DataType> : OperationRequest<CrudOp, DataType>
	{
	}
}
