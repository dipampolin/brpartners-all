﻿using System;
using System.Web.Services.Protocols;
using System.Web;
using QX3.Spinnex.Common.Services.Logging;
using System.ServiceModel;

namespace QX3.Spinnex.Common.Services.Generics
{
	public static class SoapClientFactory
	{
		#region Métodos
		public static T CreateWsClient<T>() where T : SoapHttpClientProtocol, new()
		{
			T proto = new T();

			//LogManager.WriteLog("SoapClientFactory: Criando Web Service {0} ({1})", typeof(T).Name, proto.Url);

			proto.CookieContainer = new System.Net.CookieContainer();
			
			var cookie = new System.Net.Cookie();
			cookie.Name = "TRANSACTIONID";
			cookie.Domain = new Uri(proto.Url).Host;

			Guid gTransactionID = LogManager.GetTransactionID();

			string hash = LogManager.EncodeTransactionID(gTransactionID);

			cookie.Value = hash;// HttpContext.Current.Items["___LogManager.TransactionID"].ToString();

			proto.CookieContainer.Add(cookie);


			

			return proto;
		}

		public static TClient CreateWsClient<TClient, TSoap>() 
			where TClient : ClientBase<TSoap>, new() where TSoap : class
		{
			var service = new TClient();
			var channel = service.InnerChannel;
			var binding = (BasicHttpBinding) service.Endpoint.Binding;
			
			return service;
		}

		#endregion
	}
	
}
