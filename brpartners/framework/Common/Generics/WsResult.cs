using System.Data.Odbc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;
using System.Web;
using QX3.Spinnex.Common.Services.Tracing;

namespace QX3.Spinnex.Common.Services.Generics
{
	public class WsResult<T>
	{
		#region Atributos e Enumerados
		private int m_ErrorCode;
		private T m_Result;
		private string m_ErrorMessage;
		private string m_StackTrace;
		private List<DbErrorItem> m_DbErrors = new List<DbErrorItem>();
		#endregion

		#region Propriedades
		/// <summary>
		/// Um c�digo de erro num�rico que pode ser retornado para a aplica��o.
		/// </summary>
		public int ErrorCode
		{
			get { return m_ErrorCode; }
			set { m_ErrorCode = value; }
		}
		public bool Success
		{
			get
			{
				return this.ErrorMessage == null;
			}
			set
			{

			}
		}
		public T Result
		{
			get { return m_Result; }
			set { m_Result = value; }
		}
		public string ErrorMessage
		{
			get { return m_ErrorMessage; }
			set { m_ErrorMessage = value; }
		}
		public string StackTrace
		{
			get { return m_StackTrace; }
			set { m_StackTrace = value; }
		}
		public List<DbErrorItem> DbErrors
		{
			get { return m_DbErrors; }
			set { m_DbErrors = value; }
		}
		#endregion

		#region Construtores e Destrutores
		public WsResult(Exception ex, int errorCode) : this(ex)
		{
			this.ErrorCode = errorCode;
		}
		public WsResult(Exception ex, params KeyValuePair<string, object>[] userData)
		{
			TraceUtility.HandleException(HttpContext.Current, ex, userData);
			this.ErrorMessage = ex.Message;
			this.StackTrace = ex.StackTrace;

			if (ex.GetType() == typeof(OdbcException))
			{
				OdbcException odbcex = (OdbcException)ex;
				if (odbcex.Errors.Count > 0)
				{
					this.ErrorCode = odbcex.Errors[0].NativeError;
					for (int i = 0; i < odbcex.Errors.Count; ++i)
					{
						OdbcError err = odbcex.Errors[i];
						this.DbErrors.Add(new DbErrorItem(err.NativeError, err.Message));
					}
				}
				else
					this.ErrorCode = odbcex.ErrorCode;
			}

			else if (ex is DbException)
			{
				DbException dbex = (DbException)ex;
				this.ErrorCode = dbex.ErrorCode;
			}
		}
		public WsResult(string message)
		{
			this.ErrorMessage = message;
		}
		public WsResult(T result)
		{
			this.Result = result;
		}
		public WsResult()
		{
		}
		#endregion

		#region Sub-classes e Estruturas
		public class DbErrorItem
		{
			#region Atributos e Enumerados
			private string m_Message;
			private int m_ErrorCode;
			#endregion

			#region Propriedades
			public string Message
			{
				get { return m_Message; }
				set { m_Message = value; }
			}
			public int ErrorCode
			{
				get { return m_ErrorCode; }
				set { m_ErrorCode = value; }
			}
			#endregion

			#region Construtores e Destrutores
			public DbErrorItem(int code, string msg)
			{
				this.ErrorCode = code;
				this.Message = msg;
			}
			public DbErrorItem()
			{

			}
			#endregion
		}
		#endregion
	}
}
