﻿using System;
using System.Reflection;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Diagnostics;
using System.Web;

namespace QX3.Spinnex.Common.Services.Generics
{
	public static class TypeSerializer
	{
		#region Métodos
		public static byte[] Serialize(object obj)
		{
#if DEBUG
			DateTime startTime = DateTime.Now;
#endif
			XmlSerializer xs = new XmlSerializer(obj.GetType());

			MemoryStream ms = new MemoryStream();

			xs.Serialize(ms, obj);


	

			


			//XmlDocument document = new XmlDocument();
			//document.LoadXml(Encoding.Default.GetString(ms.ToArray()));

#if DEBUG
			DateTime endTime = DateTime.Now;
			TimeSpan ts = endTime.Subtract(startTime);
			HttpContext.Current.Trace.Warn("Serialização levou " + (int)ts.TotalMilliseconds + "ms");
#endif
			return ms.ToArray();
			//return document;

		}

		public static T Deserialize<T>(byte[] bArray)
		{

#if DEBUG
			DateTime startTime = DateTime.Now;
#endif

			MemoryStream ms = new MemoryStream();
			//XmlWriter xw = new XmlTextWriter(ms, Encoding.Default);
			//node.WriteTo(xw);
			//xw.Flush();
			ms.Write(bArray, 0, bArray.Length);
			ms.Seek(0, SeekOrigin.Begin);

			XmlSerializer xs = new XmlSerializer(typeof(T));
			T content = (T)xs.Deserialize(ms);

#if DEBUG
			DateTime endTime = DateTime.Now;
			TimeSpan ts = endTime.Subtract(startTime);
			HttpContext.Current.Trace.Warn("Desserialização levou " + (int)ts.TotalMilliseconds + "ms");
#endif

			return content;
		}

        public static object Serialize()
        {
            throw new NotImplementedException();
		}
		#endregion
	}
}
