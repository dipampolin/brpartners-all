using System;
using System.Collections;
using System.Collections.Generic;

namespace QX3.Spinnex.Common.Services.Generics
{
	public class PagedCollection<T>
	{
		#region Atributos e Enumerados
		private List<T> m_List;
		private int m_PageSize;
		private int m_PageIndex;
		private T[] m_Result;
		#endregion

		#region Propriedades
		public int PageSize
		{
			get { return m_PageSize; }
			set { m_PageSize = value; }
		}
		public int PageIndex
		{
			get { return m_PageIndex; }
			set { m_PageIndex = value; }
		}
		public int PageCount
		{
			get
			{
				return (int)Math.Ceiling(((float)this.m_List.Count) / PageSize);
			}

			set { }
		}
		public int RecordCount
		{
			get
			{
				return m_List.Count;
			}
			set { }
		}
		public T[] Items
		{
			get
			{
				return m_Result;
			}
			set
			{

			}
		}
		#endregion

		#region Construtores e Destrutores
		public PagedCollection()
		{
		}
		public PagedCollection(ICollection<T> collection, int pageSize, int pageIndex)
		{
			this.m_List = new List<T>(collection);
			this.PageSize = pageSize;
			m_PageIndex = pageIndex;

			GenerateResult();
		}
		#endregion

		#region M�todos
		private void GenerateResult()
		{
			T[] result = null;
			if (RecordCount == 0)
			{
				result = new T[0];
			}
			else
			{
				if (PageIndex > PageCount)
				{
					PageIndex = PageCount;
				}
				int lBound = PageSize * (PageIndex - 1);
				//int uBound = pageSize * pageNumber;
				int length = Math.Min(lBound + PageSize, m_List.Count) - lBound;
				T[] arr = m_List.ToArray();
				result = new T[length];
				Array.Copy(arr, lBound, result, 0, length);
			}
			m_Result = result;

		}
		#endregion
	}
}
