﻿namespace QX3.Spinnex.Common.Services.Generics
{
    /// <summary>
    /// Uma classe base para implementação de Singletons.
    /// </summary>
    /// <typeparam name="DerivedType">Tipo do Singleton.</typeparam>
	public class Singleton<DerivedType> where DerivedType : new()
	{
		#region Atributos e Enumerados

		static DerivedType instance;

		#endregion

		#region Construtores e Destrutores
		#endregion

		#region Sub-Classes e Estruturas
		#endregion

		#region Propriedades
		#endregion

		#region Métodos
        /// <summary>
        /// Cria instância do Singleton no primeiro uso e retorna a instância criada nas chamadas seguintes.
        /// </summary>
        /// <returns>Objeto singleton.</returns>
		public static DerivedType Get()
		{
			lock (typeof(Singleton<DerivedType>))
			{
				if (instance == null)
					instance = new DerivedType();
			}
			return instance;
		}
		#endregion
	}
}
