﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QX3.Spinnex.Common.Services.Logging;

namespace QX3.Spinnex.Common.Services.Generics
{
    public class OperationBehaviors
    {
        public delegate OperationResponse<T> OperationDelegate<T>();
        public static OperationResponse< T> WebServiceOperation<T>(string logModuleName, OperationDelegate<T> method)
        {
            LogManager.InitLog(logModuleName);
            try
            {
                return method();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return new OperationResponse<T>() { ErrorMessage = ex.ToString() };
            }
        }
    }
}
