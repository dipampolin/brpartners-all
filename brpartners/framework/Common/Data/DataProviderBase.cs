﻿using System;
namespace QX3.Spinnex.Common.Services.Data
{
    /// <summary>
    /// Classe básica que encapsula a classe DBInterface. Pode ser usada como ponto de partida para um provedor de dados.
    /// </summary>
	public class DataProviderBase
	{
	
		protected DBInterface dbi;

        /// <summary>
        /// Inicializa usando ConnectionString padrão....
        /// </summary>
		public DataProviderBase() : this("ConnectionString")
		{
			
		}
        /// <summary>
        /// Inicializa usando a Connection String especificada.
        /// </summary>
        /// <param name="connStr">O nome de uma ConnectionString no arquivo de configuração.</param>
        public DataProviderBase(string connStr)
        {
            dbi = new DBInterface(connStr);
        }
        /// <summary>
        /// Abre conexão
        /// </summary>
		public void Connect()
		{
			dbi.Connection.Open();
		}
        /// <summary>
        /// Fecha conexão
        /// </summary>
		public void Close()
		{
			dbi.Connection.Close();
		}
        /// <summary>
        /// Inicia uma nova transação
        /// </summary>
		public void BeginTransaction()
		{
			dbi.BeginTransaction();
		}
        /// <summary>
        /// Conclui uma trnasação
        /// </summary>
		public void Commit()
		{
			dbi.CommitTransaction();
		}
        /// <summary>
        /// Desfaz uma transação.
        /// </summary>
		public void Rollback()
		{
			dbi.RollbackTransaction();
		}
	}
}
