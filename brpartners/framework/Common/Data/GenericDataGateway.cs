﻿using System;
using System.Data.Linq;
using System.Data.Common;
using System.Data;

namespace QX3.Spinnex.Common.Services.Data
{
	#region Atributos e Enumerados
	public enum CrudOp
	{
		Insert,Update,Delete
	}
	public enum CrudResult
	{
		Success, Error
	}
	#endregion

	public class GenericDataGateway
	{
		#region Atributos e Enumerados
		private DataContext db;
		private bool openedConnection;
		#endregion

		#region Propriedades
		protected DataContext Context
		{
			get
			{
				return db;
			}
		}
		public DbTransaction Transaction
		{
			get
			{
				return db.Transaction;
			}
			set
			{
				db.Transaction = value;
			}
		}
		#endregion

		#region Métodos
		public GenericDataGateway(DataContext context)
		{
			db = context;
		}

		public void SubmitChanges()
		{
			db.SubmitChanges();
		}

		public void BeginTransaction()
		{
			if (db.Connection.State != ConnectionState.Open)
			{
				db.Connection.Open();
				openedConnection = true;
			}
			db.Transaction = db.Connection.BeginTransaction();
		}

		public void CommitTransaction()
		{
			if (db.Transaction == null)
				throw new ApplicationException("Nenhuma transação para executar Commit()!");
			db.Transaction.Commit();
			db.Transaction = null;

			CloseIfOpened();
		}
		
		public void RollbackTransaction()
		{
			if (db.Transaction == null)
				throw new ApplicationException("Nenhuma transação para executar Rollback()!");
			db.Transaction.Rollback();
			db.Transaction = null;

			CloseIfOpened();
		}

		private void CloseIfOpened()
		{
			if (openedConnection)
			{
				db.Connection.Close();
				openedConnection = false;
			}
		}
		#endregion
	}
}
