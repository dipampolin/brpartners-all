using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Web;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Collections;

namespace QX3.Spinnex.Common.Services.Data
{

    public static class DBInterfaceExtensions
    {
        public static void CreateParam(this DbCommand cmd, string paramName, object paramValue)
        {
            var p = cmd.CreateParameter();
            p.ParameterName = paramName;
            p.Value = paramValue;
            cmd.Parameters.Add(p);
        }
        /// <summary>
        /// Cria um novo par�metro em um comando ADO.NET do tipo Curso Oracle
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="name"></param>
        public static void CreateOracleCursor(this DbCommand cmd, string name)
        {
            if (cmd == null)
                throw new ArgumentNullException("cmd");

            System.Data.OracleClient.OracleParameter prm = new System.Data.OracleClient.OracleParameter();
            prm.OracleType = System.Data.OracleClient.OracleType.Cursor;
            prm.Direction = ParameterDirection.Output;
            prm.ParameterName = name;

            cmd.Parameters.Add(prm);
        }

    }


    /// <summary>
    /// Componente de acesso a dados que facilita o gerenciamento de conex�es e comandos ADO.NET.
    /// </summary>
    public class DBInterface : IDisposable
    {
        #region Atributos e Enumerados

        /// <summary>
        /// F�brica de providers do ADO.NET. Cria objetos do provider especificado na ConnectionString.
        /// </summary>
        private DbProviderFactory factory;
        /// <summary>
        /// Conex�o ADO.NET criada pelo provider ADO.NET.
        /// </summary>
        private DbConnection conn;
        /// <summary>
        /// Nome do provider ADO.NET.
        /// </summary>
        private string providerName;
        public string ProviderName{get{return this.providerName;}}
        /// <summary>
        /// Transa��o ADO.NET. Quando este campo aponta para um objeto isto significa que h� uma transa��o em andamento.
        /// </summary>
        private DbTransaction m_Transaction;

        #endregion
        #region SubClasses e Estruturas
        /// <summary>
        /// Tipo de exce��o gerado pela classe DBInterface.
        /// </summary>
        public class DBIException : ApplicationException
        {
            #region Construtores e Destrutor
            /// <summary>
            /// Construtor
            /// </summary>
            /// <param name="message">Mensagem da Exce��o.</param>
            public DBIException(string message)
                : base(message)
            {

            }
            /// <summary>
            /// Construtor
            /// </summary>
            /// <param name="message">Mensagem da exce��o</param>
            /// <param name="inner">Exce��o capturada</param>
            public DBIException(string message, Exception inner)
                : base(message, inner)
            {

            }
            #endregion
        }
        /// <summary>
        /// Permite que a aplica��o instancie a conex�o seguindo algum padr�o especifico.
        /// </summary>
        public class DBInterfaceCreationEventArgs : EventArgs
        {

            #region Campos
            /// <summary>
            /// Nome da ConnectionString que est� sendo instanciada.
            /// </summary>
            private string m_ConnectionStringName;

            #endregion
            #region Construtores e Destrutor

            /// <summary>
            /// Construtor
            /// </summary>
            /// <param name="connectionStringName">Nome da connectionString no arquivo de configura��o (web.config ou app.config)</param>
            public DBInterfaceCreationEventArgs(string connectionStringName)
            {
                this.m_ConnectionStringName = connectionStringName;
            }
            #endregion
            #region Propriedades
            /// <summary>
            /// Nome da ConnectionString que est� sendo instanciada.
            /// </summary>
            public string ConnectionStringName
            {
                get { return m_ConnectionStringName; }

            }
            /// <summary>
            /// Conex�o instanciada pela aplica��o. Se este campo n�o for mexido, a conex�o ser� criada pela DBInterface normalmente.
            /// </summary>
            public DbConnection Connection
            {
                get;
                set;
            }
            /// <summary>
            /// F�brica de objetos ADO.NET. Deve ser instanciado junto com a Connection.
            /// </summary>
            public DbProviderFactory Factory
            {
                get;
                set;
            }
            /// <summary>
            /// Nome do provider instanciado pela aplica��o
            /// </summary>
            public string ProviderName
            {
                get;
                set;
            }
            #endregion

        }


        #endregion
        #region Construtores e Destrutor
       
        public DBInterface(string connectionStringName)
        {

            if (BeforeCreate != null)
            {
                DBInterfaceCreationEventArgs args = new DBInterfaceCreationEventArgs(connectionStringName);
                BeforeCreate(this, args);
                if (args.Connection != null)
                {
                    this.conn = args.Connection;
                    this.factory = args.Factory;
                    providerName = args.ProviderName;
                    return;
                }
            }
            
            ConnectionStringTokenizer csTokenizer = new ConnectionStringTokenizer();
            ConnectionStringSettings connStr = csTokenizer.LoadConnectionString(connectionStringName);
           
            if (connStr == null)
                throw new ArgumentException("String de conex�o n�o encontrada: " + connectionStringName);

            providerName = connStr.ProviderName;
            factory = DbProviderFactories.GetFactory(connStr.ProviderName);
            conn = factory.CreateConnection();
            conn.ConnectionString = connStr.ConnectionString;

        }

        public DBInterface(DbConnection connection)
        {
            conn = connection;
        }


        #endregion
        #region Propriedades
        /// <summary>
        /// Recupera a conex�o em uso.
        /// </summary>
        public DbConnection Connection
        {
            get
            {
                return conn;
            }
        }

        #endregion
        #region Eventos
        /// <summary>
        /// Declara evento de cria��o da conex�o.
        /// </summary>
        /// <param name="sender">objeto que cria a conex�o.</param>
        /// <param name="e">Permite que a aplica��o crie a conex�o.</param>
        public delegate void DBInterfaceCreationEventHandler(object sender, DBInterfaceCreationEventArgs e);
        /// <summary>
        /// Ocorre antes da conex�o ser criada. Se a conex�o for criada durante o evento esta conex�o ser� usada.
        /// </summary>
        public static event DBInterfaceCreationEventHandler BeforeCreate;
        #endregion
        #region M�todos

        private DbCommand AutoCreateCommand(string sql, params object[] parameters)
        {
            DbCommand cmd = CreateCommand(sql, CommandType.Text);
            for (int i = 0; i < parameters.Length; ++i)
            {
                object obj = parameters[i];
                if (obj == null)
                    obj = DBNull.Value;
                if (obj.GetType() == typeof(Decimal))
                    cmd.Parameters.Add(CreateParam("@P" + i, DbType.Single, obj));
                cmd.Parameters.Add(CreateParam("@P" + i, obj));
            }
            return cmd;
        }


        public void PreFill(DbCommand cmd)
        {
            switch (this.providerName)
            {
                case "System.Data.SqlClient":
                    // remove "?" e coloca nome do parametro...
                    if (cmd.CommandType == CommandType.Text && cmd.CommandText.IndexOf("?") > 0)
                    {
                        TransformQuestionMarks(cmd);
                    }
                    break;
            }
        }

        public static void TransformQuestionMarks(DbCommand cmd)
        {
            string text = cmd.CommandText;
            int i, pIndex = 0;
            do
            {
                i = text.IndexOf("?");
                if (i == -1)
                    break;
                DbParameter p = cmd.Parameters[pIndex];
                text = text.Substring(0, i) + p.ParameterName + text.Substring(i + 1, text.Length - (i + 1));
                pIndex++;

            } while (true);
            cmd.CommandText = text;


        }





        public virtual DbTransaction BeginTransaction()
        {
            if (this.m_Transaction == null)
                m_Transaction = this.Connection.BeginTransaction();
            else
                throw new DBIException("Transa��o em andamento!");
            return m_Transaction;
        }
        public virtual void CommitTransaction()
        {
            if (this.m_Transaction != null)
                this.m_Transaction.Commit();
            else
                throw new DBIException("Nenhuma transa��o em andamento. Verifique se voc� j� n�o chamou CommitTransaction ou Rollback antes.");
            this.m_Transaction = null;
        }
        public virtual void RollbackTransaction()
        {
            if (this.m_Transaction != null)
                this.m_Transaction.Rollback();
            else
                System.Diagnostics.Trace.WriteLine("Nenhuma transa��o em andamento. Verifique se voc� j� n�o chamou CommitTransaction ou Rollback antes.");
            this.m_Transaction = null;
        }

        public DataSet Fill(DbCommand cmd)
        {
            return Fill(cmd, new DataSet(), "table");
        }

        public DataSet Fill(DbCommand cmd, DataSet ds, string tableName)
        {
            PreFill(cmd);
            DbDataAdapter adap = factory.CreateDataAdapter();
            adap.SelectCommand = cmd;
            try
            {
                adap.Fill(ds, tableName);
            }
            catch
            {
                TraceCommand("DBInterface", cmd);
                throw;
            }

            return ds;
        }

        private object ValidateParameter(object obj)
        {
            if (obj == null)
                return DBNull.Value;
            else
                return obj;
        }

        public DbParameter CreateReturnValueParam()
        {
            var p = CreateParam("return_value", DbType.Int32, 0);
            p.Direction = ParameterDirection.ReturnValue;
            return p;
        }
        /// <summary>
        /// L� valor de retorno de um comando. Funciona para SQL Server.
        /// </summary>
        /// <param name="cmd">Comando com par�metro de retorno.</param>
        /// <returns></returns>
        public int ReadReturnValue(DbCommand cmd)
        {
            return (int)cmd.Parameters[FormatParamName("return_value")].Value;
        }

        public DbParameter CreateParam(string name, object value)
        {
            DbParameter prm = factory.CreateParameter();
        
            prm.Value = ValidateParameter(value);
            prm.ParameterName = FormatParamName(name);
            return prm;
        }
        public DbParameter CreateOutputParam(string name, DbType type)
        {
            return CreateOutputParam(name, type, 0);
        }
        public DbParameter CreateOutputParam(string name, DbType type, int size)
        {
            DbParameter prm = factory.CreateParameter();
            prm.DbType = type;
            prm.Direction = ParameterDirection.Output;
            prm.ParameterName = FormatParamName(name);
            if (size > 0)
                prm.Size = size;
            return prm;

        }
        public DbParameter CreateParam(string name, DbType dbType, object value)
        {
            DbParameter prm = factory.CreateParameter();
            prm.DbType = dbType;

            prm.Value = ValidateParameter(value);
            prm.ParameterName = FormatParamName(name);
            return prm;
        }
        public string FormatParamName(string name)
        {
            if (Connection is SqlConnection)
            {
                if (name.Substring(0, 1) == "@")
                    return name;
                else if (name.Substring(0, 1) == ":")
                    return "@" + name.Substring(1);
                else
                    return "@" + name;
            }
            else if (conn is System.Data.OracleClient.OracleConnection)
            {
               
                if (name.Substring(0, 1) == ":")
                    return name;
                else if (name.Substring(0, 1) == "@")
                    return ":" + name.Substring(1);
                else
                    return ":" + name;
            }
            else
                return name;
        }

        public string FormatCreateCommand(string command)
        {
            if (Connection is SqlConnection)
                return command.Replace(":", "@");
            else if (conn is System.Data.OracleClient.OracleConnection)
                return command.Replace("@", ":");
            else
                return command;
        }

        public DbCommand CreateCommandWithQuery(string query, CommandType type)
        {
            DbCommand cmd = conn.CreateCommand();
            cmd.CommandText = query;
            cmd.CommandType = type;
            cmd.Transaction = this.m_Transaction;
            return cmd;
        }

        public DbCommand CreateCommand(string cmdText)
        {
            return CreateCommand(FormatCreateCommand(cmdText), CommandType.Text);
        }

        public DbCommand CreateCommand(string cmdText, CommandType type)
        {
            DbCommand cmd = conn.CreateCommand();
            cmd.CommandText = FormatCreateCommand(cmdText);
            cmd.CommandType = type;
            cmd.Transaction = this.m_Transaction;
            return cmd;
        }

        public DbDataAdapter CreateDataAdapter()
        {
            return factory.CreateDataAdapter();
        }
        public DbProviderFactory GetFactory()
        {
            return this.factory;
        }


        public DataSet Execute(string sql, params object[] parameters)
        {
            DbCommand cmd = AutoCreateCommand(sql, parameters);
            return Fill(cmd);
        }
        public DbDataReader ExecuteReader(string sql, params object[] parameters)
        {
            DbCommand cmd = AutoCreateCommand(sql, parameters);
            return cmd.ExecuteReader();
        }
        public int ExecuteNonQuery(string sql, params object[] p)
        {
            var cmd = CreateCommand(sql);
            for (int i = 0; i < p.Length; ++i)
                cmd.Parameters.Add(CreateParam("p" + i, p[i]));
            return cmd.ExecuteNonQuery();
        }

        public static void TraceCommand(string caption, DbCommand cmd)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("Command Text: {0}\n", cmd.CommandText);
            sb.AppendFormat("Parameters:\n");
            foreach (DbParameter p in cmd.Parameters)
                sb.AppendFormat("{0} = {1}\n", p.ParameterName, p.Value);

            HttpContext.Current.Trace.Warn("TraceCommand", caption + "\n" + sb.ToString());


            KeyValuePair<string, object>[] soapParams = (KeyValuePair<string, object>[])HttpContext.Current.Items["SoapParams"];

            List<KeyValuePair<string, object>> list = null;

            if (soapParams != null)
                list = new List<KeyValuePair<string, object>>(soapParams);
            else
                list = new List<KeyValuePair<string, object>>();
            list.Add(new KeyValuePair<string, object>("DB Command:", sb.ToString()));
            HttpContext.Current.Items["SoapParams"] = list.ToArray();
        }
        public static void TraceDataSet(string caption, DataSet ds)
        {
            string xml = ds.GetXml();
            HttpContext.Current.Trace.Warn("TraceDataSet", caption + "\n" + xml);
        }


        public void Dispose()
        {
            if (this.Connection != null && this.Connection.State == ConnectionState.Open)
                this.Connection.Close();
        }

        #endregion
    }


    public class DBInterfaceManager
    {
        public enum DBProviderType { ORACLE, SQLSERVER, SYBASE, ACCESS, SQLITE, MYSQL };

        private Hashtable providers = new Hashtable();
        private DBInterface dbInterface;
        public DBProviderType ProviderType
        {
            get
            {
                DBProviderType dbProvider = DBProviderType.SQLSERVER;
                switch (dbInterface.ProviderName)
                {
                    case "System.Data.OracleClient":
                        dbProvider = DBProviderType.ORACLE;
                        break;
                }
                return dbProvider;
            }
        }
        private string connectionName;
        public DBInterfaceManager(string connectionName)
        {
            this.connectionName = connectionName;
            dbInterface = new DBInterface(connectionName);
        }
        public DBInterface Db
        {
            get
            {
                dbInterface = dbInterface ?? new DBInterface(connectionName);

                return dbInterface;
            }
        }
        public void SetQueries(DBProviderType type, List<KeyValuePair<string, string>> queries)
        {
            if (type == this.ProviderType)
                this.providers[type] = queries;
        }
        public void AddQuery(string key, string query)
        {
            AddQuery(new KeyValuePair<string, string>(key, query));
        }
        public void AddQuery(KeyValuePair<string, string> query)
        {
            if (providers[ProviderType] == null)
            {
                providers[ProviderType] = new List<KeyValuePair<string, string>>();
            }
            (providers[ProviderType] as List<KeyValuePair<string, string>>).Add(query);
        }
        public DbCommand AddParameters(DbCommand cmd, params KeyValuePair<string, object>[] parameters)
        {
            string joker = "@";
            switch (this.ProviderType)
            {
                case DBProviderType.ORACLE:
                    joker = ":";
                    if (cmd.CommandType == CommandType.StoredProcedure)
                        joker = "";
                    break;
                case DBProviderType.MYSQL:
                case DBProviderType.SYBASE:
                    joker = "?";
                    break;
                case DBProviderType.ACCESS:
                case DBProviderType.SQLSERVER:
                case DBProviderType.SQLITE:
                    joker = "@";
                    break;
            }
            foreach (KeyValuePair<string, object> p in parameters)
            {
                if (cmd.CommandType == CommandType.StoredProcedure && this.ProviderType == DBProviderType.ORACLE)
                {
                    DbParameter param = cmd.CreateParameter();
                    param.ParameterName = string.Concat(joker, p.Key);
                    param.Value = p.Value;
                    cmd.Parameters.Add(param);
                }
                else
                    cmd.Parameters.Add(dbInterface.CreateParam(string.Concat(joker, p.Key), p.Value));
            }
            return cmd;
        }
        public DbCommand this[string queryName]
        {
            get
            {
                DbCommand cmd = null;
                string query = null;
                List<KeyValuePair<string, string>> queries = providers[this.ProviderType] as List<KeyValuePair<string, string>>;
                if (queries != null)
                {
                    KeyValuePair<string, string> current_query = queries.Find(new Predicate<KeyValuePair<string, string>>(delegate(KeyValuePair<string, string> q)
                    {
                        return (q.Key == queryName);
                    }));
                    query = current_query.Value;
                }
                Open();
                cmd = dbInterface.CreateCommand(query);
                return cmd;
            }
        }
        public void Close()
        {
            if (this.Db != null)
            {// && this.Db.Connection.State != ConnectionState.Closed)

                try
                {
                    this.Db.Connection.Close();
                    //this.Db.Connection.Dispose();
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }

            }
        }
        public void Open()
        {
            if (this.Db.Connection.State != ConnectionState.Open)
                this.Db.Connection.Open();
        }
    }

}