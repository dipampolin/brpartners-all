﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace QX3.Spinnex.Common.Services.Data
{
	/// <summary>
	/// Classe responsável por criptografar e descriptografar ConnectionStrings.
	/// <remarks>TO-DO: usar classe DbConnectionStringBuilder ao invés de parsear na mão.</remarks>
	/// </summary>
	public class ConnectionStringTokenizer
	{
		private string encryptKey = ",ALa(";
		private Dictionary<string, string> DecodeConnectionString(string connStr)
		{
			if (connStr == null)
				throw new ArgumentNullException("connStr");
			Dictionary<string, string> d = new Dictionary<string, string>();
			string[] pieces = connStr.Split(';');

			foreach (var piece in pieces)
			{
				int idx = piece.IndexOf('=');
				if (idx > 0)
				{
					string key = piece.Substring(0, idx);
					string value;
					if (idx == piece.Length - 1)
						value = "";
					else
						value = piece.Substring(idx + 1);
					d.Add(key.Trim(), value.Trim());
				}
			}

			return d;
		}

		private string RecodeConnectionString(Dictionary<string, string> keyValues)
		{
			StringBuilder sb = new StringBuilder();

			foreach (var item in keyValues)
			{
				string key = item.Key;
				string value = item.Value;
				sb.AppendFormat("{0}={1};", key, value);
			}

			return sb.ToString();
		}
		private string EncryptValue(string original)
		{
			byte[] bytes = Encoding.UTF8.GetBytes(original);
			RC4.Process(bytes, Encoding.UTF8.GetBytes(encryptKey));

			StringBuilder sb = new StringBuilder();

			for (int i = 0; i < bytes.Length; i++)
			{
				byte b = bytes[i];
				sb.AppendFormat("{0:X2}", b);
			}

			return sb.ToString();
		}
		private string DecryptValue(string encrypted)
		{
			int realLength = encrypted.Length / 2;
			byte[] bytes = new byte[realLength];

			for (int i = 0; i < realLength; i++)
			{
				string byteStr = encrypted.Substring(2 * i, 2);
				byte b = Byte.Parse(byteStr, System.Globalization.NumberStyles.HexNumber);
				bytes[i] = b;
			}

			RC4.Process(bytes, Encoding.UTF8.GetBytes(encryptKey));

			return Encoding.UTF8.GetString(bytes);
		}
		/// <summary>
		/// Codifica uma ConnectionString.
		/// </summary>
		/// <param name="originalConnectionString">Texto da ConnectionString.</param>
		/// <returns></returns>
		public string Encode(string originalConnectionString)
		{
			Dictionary<string, string> source = DecodeConnectionString(originalConnectionString);
			Dictionary<string, string> dest = new Dictionary<string, string>();
			foreach (var item in source)
			{
				string k = item.Key;
				string v = item.Value;
				switch (k.ToLower())
				{
					case "pwd":
					case "password":
						k = "Encoded_" + k;
						v = EncryptValue(v);
						break;
				}
				dest.Add(k, v);
			}
			string encodedConnectionString = RecodeConnectionString(dest);
			return encodedConnectionString;
		}
		/// <summary>
		/// Decodifica uma Connection String
		/// </summary>
		/// <param name="encodedConnectionString">Connection String codificada.</param>
		/// <returns></returns>
		public string Decode(string encodedConnectionString)
		{
			Dictionary<string, string> source = DecodeConnectionString(encodedConnectionString);
			Dictionary<string, string> dest = new Dictionary<string, string>();
			foreach (var item in source)
			{
				string k = item.Key;
				string v = item.Value;
				if (k.ToLower().StartsWith("encoded_"))
				{
					k = k.Substring(8);
					v = DecryptValue(v);
				}
				dest.Add(k, v);
			}
			string decodedConnectionString = RecodeConnectionString(dest);
			return decodedConnectionString;
		}
		/// <summary>
		/// Carrega e decodifica uma ConnectionString do arquivo .config.
		/// </summary>
		/// <param name="name">Nome da Connection String no Web.config ou App.config.</param>
		/// <returns></returns>
		public ConnectionStringSettings LoadConnectionString(string name)
		{
			if (string.IsNullOrEmpty(name))
				throw new ArgumentNullException("name");
			var cs = ConfigurationManager.ConnectionStrings[name];
			string decoded = Decode(cs.ConnectionString);
			var copy = new ConnectionStringSettings(cs.Name, decoded, cs.ProviderName);
			return copy;
		}
	}
}
