﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using System.Collections;

namespace System.Data.Objects.DataClasses
{
    public static class EntityObjectExtension
    {
        /// <summary>
        /// Atualiza todas as propriedades do objeto original com o segundo
        /// </summary>
        /// <param name="primary"></param>
        /// <param name="secondary"></param>
        public static void UpdateEntityWith(this EntityObject primary, EntityObject secondary)
        {
            var entityProperties = typeof(EntityObject).GetProperties();
            var primaryProperties = primary.GetType().GetProperties().Where(p => entityProperties.Count(ett => ett.Name == p.Name) == 0);
            foreach (var pi in primaryProperties)
            {
                if (primary.EntityKey.EntityKeyValues.Count(k => k.Key == pi.Name) == 0 &&
                    !(pi.PropertyType.IsGenericType && pi.PropertyType.GetGenericTypeDefinition() == typeof(EntityObject)) &&
                    !(pi.PropertyType.IsGenericType && pi.PropertyType.GetGenericTypeDefinition() == typeof(EntityCollection<>)))
                {
                    var secValue = pi.GetGetMethod().Invoke(secondary, null);
                    pi.GetSetMethod().Invoke(primary, new object[] { secValue });
                }
            }
        }
    }
}