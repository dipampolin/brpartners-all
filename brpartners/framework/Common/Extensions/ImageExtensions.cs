﻿using System.Drawing;

namespace System.Drawing
{
    [Obsolete("Funcionalidade duplicada: usar Assembly QX3.Spinnex.Common.Imaging", false)]
	public static class ImageExtensions
	{
		#region Métodos

		public static Image GetThumbnailImageResized(this Image img, int thumbWidth, int thumbHeight, Image.GetThumbnailImageAbort callback, System.IntPtr callbackData)
		{	
			int oWidth = img.Width; // largura original
			int oHeight = img.Height; // altura original

			int nWidth = thumbWidth;
			int nHeight = thumbWidth;

			// redimensiona se necessario
			if (oWidth > nWidth || oHeight > nHeight)
			{

				if (oWidth > oHeight)
				{
					// imagem horizontal
					nHeight = (oHeight * nWidth) / oWidth;
				}
				else
				{
					// imagem vertical
					nWidth = (oWidth * nHeight) / oHeight;
				}
			}

			// cria a copia da imagem
			return img.GetThumbnailImage(nWidth, nHeight, null, new System.IntPtr(0));
		}

		#endregion
	}
}
