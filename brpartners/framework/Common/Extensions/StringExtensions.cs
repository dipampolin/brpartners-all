﻿namespace System
{
    /// <summary>
    /// .NET 3.5 Language Extensions para facilitar a formatação de entrada de usuário para inclusão em banco de dados.
    /// </summary>
	public static class StringExtensions
	{
		#region Métodos
        /// <summary>
        /// Transforma string vazia em null
        /// </summary>
        /// <param name="str">String de entrada</param>
        /// <returns>String original ou null se a String for "".</returns>
		public static string ForDB(this string str)
		{
			if (str == null)
				return str;
			str = str.Trim();
			if (str == "")
				return null;
			return str;
		}
        /// <summary>
        /// Converte uma string em um ValueType (Int16, Int32, Int64...). Este método é sensível à cultura da aplicação para tratar entradas formatadas como 1.000,00 (neste caaso remove o ".") e 1,000.00 (neste caso remove a ",")
        /// </summary>
        /// <typeparam name="T">Um tipo que possa ser convertido a partir de uma String.</typeparam>
        /// <param name="str">Entrada do usuário.</param>
        /// <returns>null se a string for vazia ou o tipo</returns>
		public static T? As<T>(this string str) where T : struct, IConvertible 
		{
			if (str == null)
				return null;
			str = str.Trim();
			if (str == "")
				return null;

			Type t = typeof(T);
			if (t == typeof(Int64) || t == typeof(Int32) || t == typeof(Int16))
				str = str.Replace(System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberGroupSeparator, "");


			return (T) Convert.ChangeType(str, typeof(T));
		}
        /// <summary>
        /// Coloca a última letra da String maiuscula.....
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
		public static string Capiralize(this string str)
		{
			if (str == null)
				return "";
			if (str.Length == 0)
				return "";
			if (Char.IsUpper(str[0]))
				return str;
			char[] chars = str.ToCharArray();
			chars[0] = Char.ToUpper(chars[0]);
			return new string(chars);
		}
		#endregion
	}
}
