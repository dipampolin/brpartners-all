﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System
{
    public static class ArrayExtensions
    {
        public static bool CompareToArray<T>(this T[] a, T[] b) where T : IComparable
        {
            if (a.Length != b.Length)
                return false;
            for (int i = 0; i < a.Length; ++i)
                if (a[i].CompareTo(b[i])!=0)
                    return false;
            return true;
        }
    }
}
