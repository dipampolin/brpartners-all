﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System
{
    public static class ObjectExtensions
    {
        /// <summary>
        /// Atualiza todas as propriedades do objeto original com o segundo
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="primary"></param>
        /// <param name="secondary"></param>
        public static void UpdateWith<T>(this T primary, T secondary)
        {
            foreach (var pi in typeof(T).GetProperties())
            {
                var priValue = pi.GetGetMethod().Invoke(primary, null);
                var secValue = pi.GetGetMethod().Invoke(secondary, null);
                pi.GetSetMethod().Invoke(primary, new object[] { secValue });
            }
        }

        /// <summary>
        /// Atualiza todas as propriedades nulas do objeto original com o segundo
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="primary"></param>
        /// <param name="secondary"></param>
        public static void MergeWith<T>(this T primary, T secondary)
        {
            foreach (var pi in typeof(T).GetProperties())
            {
                var priValue = pi.GetGetMethod().Invoke(primary, null);
                var secValue = pi.GetGetMethod().Invoke(secondary, null);
                if (priValue == null || (pi.PropertyType.IsValueType && priValue == Activator.CreateInstance(pi.PropertyType)))
                {
                    pi.GetSetMethod().Invoke(primary, new object[] { secValue });
                }
            }
        }

        /// <summary>
        /// Clona o objeto original
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        public static T Clone<T>(this T source)
        {
            var dcs = new System.Runtime.Serialization.DataContractSerializer(typeof(T));
            using (var ms = new System.IO.MemoryStream())
            {
                dcs.WriteObject(ms, source);
                ms.Seek(0, System.IO.SeekOrigin.Begin);
                return (T)dcs.ReadObject(ms);
            }
        }
    }
}
