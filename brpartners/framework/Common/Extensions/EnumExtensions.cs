﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Reflection;

namespace System
{
	public static class EnumExtensions
	{
		private const char ENUM_SEPARATOR_CHARACTER = ',';

		public static string GetDescription(this Enum value)
		{
			// Check for Enum that is marked with FlagAttribute
			var entries = value.ToString().Split(ENUM_SEPARATOR_CHARACTER);
			var description = new string[entries.Length];

			for (var i = 0; i < entries.Length; i++)
			{
				var fieldInfo = value.GetType().GetField(entries[i].Trim());

				var attributes = (DescriptionAttribute[])fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);
				description[i] = (attributes.Length > 0) ? attributes[0].Description : string.Empty;
			}

			return String.Join(", ", description);
		}

		public static T GetValue<T>(this Enum value)
		{
			return (T)value.GetType().GetField(value.ToString()).GetRawConstantValue();
		}

	}
}
