﻿using System.Data.Common;

namespace System.Data
{
    /// <summary>
    /// Language extensions para ADO.NET
    /// </summary>
	public static class ADOExtensions
	{
		#region Métodos
		public static string GetDBString(this DbDataReader dr, int index)
		{
			if (dr.IsDBNull(index))
				return null;
			
			return (string)dr[index];
		}

		public static string GetDBString(this DbDataReader dr, string name)
		{
			return dr.GetDBString(dr.GetOrdinal(name));
		}

		public static T? GetDBValue<T>(this DbDataReader dr, int index) where T : struct
		{
			if (dr.IsDBNull(index))
				return null;
			
			return (T) Convert.ChangeType(dr[index], typeof(T));
		}

		public static T? GetDBValue<T>(this DbDataReader dr, string name) where T : struct
		{
			return dr.GetDBValue<T>(dr.GetOrdinal(name));
		}

		public static T GetValue<T>(this DbDataReader dr, string name) where T : struct
		{
			return (T)Convert.ChangeType(dr[dr.GetOrdinal(name)], typeof(T));
		}

		#endregion
	}
}
