-----------------------------------------------------
-- Export file for user BRPPORTAL                  --
-- Created by fabio.gusmao on 20/08/2014, 10:00:02 --
-----------------------------------------------------

spool log

prompt
prompt Creating table PLAN_TABLE
prompt =========================
prompt
create table PLAN_TABLE
(
  STATEMENT_ID    VARCHAR2(30),
  TIMESTAMP       DATE,
  REMARKS         VARCHAR2(80),
  OPERATION       VARCHAR2(30),
  OPTIONS         VARCHAR2(30),
  OBJECT_NODE     VARCHAR2(128),
  OBJECT_OWNER    VARCHAR2(30),
  OBJECT_NAME     VARCHAR2(30),
  OBJECT_INSTANCE NUMBER(38),
  OBJECT_TYPE     VARCHAR2(30),
  OPTIMIZER       VARCHAR2(255),
  SEARCH_COLUMNS  NUMBER,
  ID              NUMBER(38),
  PARENT_ID       NUMBER(38),
  POSITION        NUMBER(38),
  COST            NUMBER(38),
  CARDINALITY     NUMBER(38),
  BYTES           NUMBER(38),
  OTHER_TAG       VARCHAR2(255),
  PARTITION_START VARCHAR2(255),
  PARTITION_STOP  VARCHAR2(255),
  PARTITION_ID    NUMBER(38),
  OTHER           LONG,
  DISTRIBUTION    VARCHAR2(30)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table PLSQL_PROFILER_RUNS
prompt ==================================
prompt
create table PLSQL_PROFILER_RUNS
(
  RUNID           NUMBER not null,
  RELATED_RUN     NUMBER,
  RUN_OWNER       VARCHAR2(32),
  RUN_DATE        DATE,
  RUN_COMMENT     VARCHAR2(2047),
  RUN_TOTAL_TIME  NUMBER,
  RUN_SYSTEM_INFO VARCHAR2(2047),
  RUN_COMMENT1    VARCHAR2(2047),
  SPARE1          VARCHAR2(256)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
comment on table PLSQL_PROFILER_RUNS
  is 'Run-specific information for the PL/SQL profiler';
alter table PLSQL_PROFILER_RUNS
  add primary key (RUNID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table PLSQL_PROFILER_UNITS
prompt ===================================
prompt
create table PLSQL_PROFILER_UNITS
(
  RUNID          NUMBER not null,
  UNIT_NUMBER    NUMBER not null,
  UNIT_TYPE      VARCHAR2(32),
  UNIT_OWNER     VARCHAR2(32),
  UNIT_NAME      VARCHAR2(32),
  UNIT_TIMESTAMP DATE,
  TOTAL_TIME     NUMBER default 0 not null,
  SPARE1         NUMBER,
  SPARE2         NUMBER
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
comment on table PLSQL_PROFILER_UNITS
  is 'Information about each library unit in a run';
alter table PLSQL_PROFILER_UNITS
  add primary key (RUNID, UNIT_NUMBER)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table PLSQL_PROFILER_UNITS
  add foreign key (RUNID)
  references PLSQL_PROFILER_RUNS (RUNID);

prompt
prompt Creating table PLSQL_PROFILER_DATA
prompt ==================================
prompt
create table PLSQL_PROFILER_DATA
(
  RUNID       NUMBER not null,
  UNIT_NUMBER NUMBER not null,
  LINE#       NUMBER not null,
  TOTAL_OCCUR NUMBER,
  TOTAL_TIME  NUMBER,
  MIN_TIME    NUMBER,
  MAX_TIME    NUMBER,
  SPARE1      NUMBER,
  SPARE2      NUMBER,
  SPARE3      NUMBER,
  SPARE4      NUMBER
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 128K
    minextents 1
    maxextents unlimited
  );
comment on table PLSQL_PROFILER_DATA
  is 'Accumulated data from all profiler runs';
alter table PLSQL_PROFILER_DATA
  add primary key (RUNID, UNIT_NUMBER, LINE#)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table PLSQL_PROFILER_DATA
  add foreign key (RUNID, UNIT_NUMBER)
  references PLSQL_PROFILER_UNITS (RUNID, UNIT_NUMBER);

prompt
prompt Creating table SUITABILITY
prompt ==========================
prompt
create table SUITABILITY
(
  ID           NUMBER not null,
  IDQUESTION   NUMBER not null,
  QUESTION     NVARCHAR2(255) not null,
  INSERT_DATE  DATE default SYSDATE not null,
  STATUS       CHAR(1) default 'A' not null,
  ACCEPTRESULT NUMBER(1)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table SUITABILITY
  add primary key (ID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table SUITABILITY_ANSWERS
prompt ==================================
prompt
create table SUITABILITY_ANSWERS
(
  ID               NUMBER not null,
  IDQUESTION       NUMBER not null,
  ANSWER           NVARCHAR2(255) not null,
  STATUS           CHAR(1) default 'A' not null,
  ID_TYPE_INVESTOR NUMBER(1) not null,
  POINT            NUMBER not null,
  IDANSWER         NUMBER default 1
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table SUITABILITY_ANSWERS
  add primary key (ID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table SUITABILITY_ANSWERS
  add constraint SUITABILITY_ANSWERS_FK1 foreign key (IDQUESTION)
  references SUITABILITY (ID);

prompt
prompt Creating table SUITABILITY_STATUS
prompt =================================
prompt
create table SUITABILITY_STATUS
(
  ID          NUMBER not null,
  DESCRIPTION VARCHAR2(100) not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table SUITABILITY_STATUS
  add primary key (ID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table SUITABILITY_TYPES
prompt ================================
prompt
create table SUITABILITY_TYPES
(
  ID          NUMBER not null,
  DESCRIPTION NVARCHAR2(30) not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table SUITABILITY_TYPES
  add primary key (ID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table SUITABILITY_USER_ANSW
prompt ====================================
prompt
create table SUITABILITY_USER_ANSW
(
  ID          NUMBER not null,
  REGID       NUMBER not null,
  IDQUESTION  NUMBER not null,
  IDANSWER    NUMBER not null,
  INSERT_DATE DATE default SYSDATE not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table SUITABILITY_USER_ANSW
  add constraint SUITABILITY_USER_ANSW_FK1 foreign key (IDQUESTION)
  references SUITABILITY (ID);
alter table SUITABILITY_USER_ANSW
  add constraint SUITABILITY_USER_ANSW_FK2 foreign key (IDANSWER)
  references SUITABILITY_ANSWERS (ID);

prompt
prompt Creating table SUITABILITY_USER_STATUS_TYPE
prompt ===========================================
prompt
create table SUITABILITY_USER_STATUS_TYPE
(
  ID          NUMBER not null,
  REGID       NUMBER,
  IDSTATUS    NUMBER not null,
  IDTYPE      NUMBER not null,
  INSERT_DATE DATE default SYSDATE not null,
  COUNTSUIT   CHAR(1) default 'N'
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table SUITABILITY_USER_STATUS_TYPE
  add constraint SUITABILITY_STATUS_TYPE_FK1 foreign key (IDSTATUS)
  references SUITABILITY_STATUS (ID);
alter table SUITABILITY_USER_STATUS_TYPE
  add constraint SUITABILITY_USER_TYPE_FK1 foreign key (IDTYPE)
  references SUITABILITY_TYPES (ID);

prompt
prompt Creating table TBIDENTIFICACAODEMANDA
prompt =====================================
prompt
create table TBIDENTIFICACAODEMANDA
(
  CDIDENTIFICACAODEMANDA      INTEGER not null,
  NMIDENTIFICACAODEMANDA      VARCHAR2(100) not null,
  STIDENTIFICACAODEMANDAATIVA CHAR(1) not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table TBIDENTIFICACAODEMANDA
  add constraint PNUNTBIDENTIFICACAODEMANDA primary key (CDIDENTIFICACAODEMANDA)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TBIDENTIFICACAOSUBDEMANDA
prompt ========================================
prompt
create table TBIDENTIFICACAOSUBDEMANDA
(
  CDIDENTIFICACAODEMANDA         INTEGER not null,
  CDIDENTIFICACAOSUBDEMANDA      INTEGER not null,
  NMIDENTIFICACAOSUBDEMANDA      VARCHAR2(100) not null,
  STIDENTIFICACAOSUBDEMANDAATIVA CHAR(1) not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table TBIDENTIFICACAOSUBDEMANDA
  add constraint PNUNTBIDENTIFICACAOSUBDEMANDA primary key (CDIDENTIFICACAODEMANDA, CDIDENTIFICACAOSUBDEMANDA)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table TBIDENTIFICACAOSUBDEMANDA
  add constraint FKTBDNTFCCDMNDTBIDNTFCCSBDMNDA foreign key (CDIDENTIFICACAODEMANDA)
  references TBIDENTIFICACAODEMANDA (CDIDENTIFICACAODEMANDA);

prompt
prompt Creating table TBPARAMETROSISTEMA
prompt =================================
prompt
create table TBPARAMETROSISTEMA
(
  CDIDENTIFICACAODEMANDA    INTEGER not null,
  CDIDENTIFICACAOSUBDEMANDA INTEGER not null,
  CDPARAMETROSISTEMA        INTEGER not null,
  NMPARAMETROSISTEMA        VARCHAR2(100) not null,
  VLPARAMETROSISTEMA        VARCHAR2(255) not null,
  TPPARAMETROSISTEMA        CHAR(1) not null,
  STPARAMETROSISTEMAATIVO   CHAR(1) not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table TBPARAMETROSISTEMA
  add constraint PNUNTBPARAMETROSISTEMA primary key (CDIDENTIFICACAODEMANDA, CDIDENTIFICACAOSUBDEMANDA, CDPARAMETROSISTEMA)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table TBPARAMETROSISTEMA
  add constraint FKTBIDENTFCCSBDMNDTBPRMTRSSTMA foreign key (CDIDENTIFICACAODEMANDA, CDIDENTIFICACAOSUBDEMANDA)
  references TBIDENTIFICACAOSUBDEMANDA (CDIDENTIFICACAODEMANDA, CDIDENTIFICACAOSUBDEMANDA);

prompt
prompt Creating table TB_ASSESSOR_GRUPO
prompt ================================
prompt
create table TB_ASSESSOR_GRUPO
(
  CD_ASSESSOR NUMBER
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 22M
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_CA_GRUPO_ASSESSORES
prompt =====================================
prompt
create table TB_CA_GRUPO_ASSESSORES
(
  ID         NUMBER(4) not null,
  NOME       VARCHAR2(30) not null,
  ASSESSORES VARCHAR2(100)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table TB_CA_GRUPO_ASSESSORES
  add constraint PK_CA_ASSESSOR primary key (ID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_CA_ASSESSOR_USUARIO
prompt =====================================
prompt
create table TB_CA_ASSESSOR_USUARIO
(
  IDUSUARIO       NUMBER(4) not null,
  IDGRUPOASSESSOR NUMBER(4) not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table TB_CA_ASSESSOR_USUARIO
  add constraint UQ_CA_USU_ASSES unique (IDGRUPOASSESSOR, IDUSUARIO)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table TB_CA_ASSESSOR_USUARIO
  add constraint FK_CA_GASSESSOR foreign key (IDGRUPOASSESSOR)
  references TB_CA_GRUPO_ASSESSORES (ID);

prompt
prompt Creating table TB_CA_CLIENTES_EXCLUSAO
prompt ======================================
prompt
create table TB_CA_CLIENTES_EXCLUSAO
(
  ID_GRUPO   NUMBER(4) not null,
  CD_CLIENTE NUMBER(7) not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table TB_CA_CLIENTES_EXCLUSAO
  add constraint PK_CA_CLIENTE_EXCLUSAO primary key (ID_GRUPO, CD_CLIENTE)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table TB_CA_CLIENTES_EXCLUSAO
  add constraint FK_CA_CLIENTE_EXCLUSAO foreign key (ID_GRUPO)
  references TB_CA_GRUPO_ASSESSORES (ID) on delete cascade;

prompt
prompt Creating table TB_CA_CLIENTES_INCLUSAO
prompt ======================================
prompt
create table TB_CA_CLIENTES_INCLUSAO
(
  ID_GRUPO   NUMBER(4) not null,
  CD_CLIENTE NUMBER(7) not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table TB_CA_CLIENTES_INCLUSAO
  add constraint PK_CA_CLIENTE_INCLUSAO primary key (ID_GRUPO, CD_CLIENTE)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table TB_CA_CLIENTES_INCLUSAO
  add constraint FK_CA_CLIENTE_INCLUSAO foreign key (ID_GRUPO)
  references TB_CA_GRUPO_ASSESSORES (ID) on delete cascade;

prompt
prompt Creating table TB_CA_ELEMENTO
prompt =============================
prompt
create table TB_CA_ELEMENTO
(
  ID      NUMBER(4) not null,
  NOME    VARCHAR2(150) not null,
  APELIDO VARCHAR2(150) not null,
  IDPAI   NUMBER(4),
  PAGINA  NUMBER(1) not null,
  ATIVO   NUMBER(1) not null,
  VISIVEL NUMBER(1) default 1 not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table TB_CA_ELEMENTO
  add constraint PK_CA_ELEMENTO primary key (ID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_CA_ELEMENTO_2
prompt ===============================
prompt
create table TB_CA_ELEMENTO_2
(
  ID      NUMBER(4) not null,
  NOME    VARCHAR2(150) not null,
  APELIDO VARCHAR2(150) not null,
  IDPAI   NUMBER(4),
  PAGINA  NUMBER(1) not null,
  ATIVO   NUMBER(1) not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_CA_ELEMENTO_PENDENCIA
prompt =======================================
prompt
create table TB_CA_ELEMENTO_PENDENCIA
(
  ID_ELEMENTO    NUMBER(4) not null,
  STATUS         VARCHAR2(5),
  GERA_PENDENCIA NUMBER(1) default 0 not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
comment on column TB_CA_ELEMENTO_PENDENCIA.GERA_PENDENCIA
  is 'Se 0, elemento n�o gera pendencia, 1 em caso contr�rio';
alter table TB_CA_ELEMENTO_PENDENCIA
  add constraint PK_ID_ELEMENTO_PENDENCIA primary key (ID_ELEMENTO)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table TB_CA_ELEMENTO_PENDENCIA
  add constraint FK_ID_ELEMENTO_PENDENCIA foreign key (ID_ELEMENTO)
  references TB_CA_ELEMENTO (ID)
  disable;

prompt
prompt Creating table TB_CA_PERFIL
prompt ===========================
prompt
create table TB_CA_PERFIL
(
  ID   NUMBER(4) not null,
  NOME VARCHAR2(30)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table TB_CA_PERFIL
  add constraint PK_CA_PERFIL primary key (ID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_CA_ELEMENTO_PERFIL
prompt ====================================
prompt
create table TB_CA_ELEMENTO_PERFIL
(
  IDELEMENTO NUMBER(4) not null,
  IDPERFIL   NUMBER(4) not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table TB_CA_ELEMENTO_PERFIL
  add constraint UQ_CA_ELEM_PERF unique (IDPERFIL, IDELEMENTO)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table TB_CA_ELEMENTO_PERFIL
  add constraint FK_CA_ELEMENTO foreign key (IDELEMENTO)
  references TB_CA_ELEMENTO (ID)
  disable;
alter table TB_CA_ELEMENTO_PERFIL
  add constraint FK_CA_PERFIL foreign key (IDPERFIL)
  references TB_CA_PERFIL (ID);

prompt
prompt Creating table TB_CA_ENVIO_LINK
prompt ===============================
prompt
create table TB_CA_ENVIO_LINK
(
  ID           NUMBER(4) not null,
  EMAIL        VARCHAR2(150) not null,
  LINK         RAW(32) not null,
  DATA_CRIACAO DATE default to_date(to_char(sysdate, 'dd/mm/yyyy'), 'dd/mm/yyyy') not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table TB_CA_ENVIO_LINK
  add constraint PK_ENVIO_LINK primary key (ID, LINK)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_CA_SENHAS_ANTIGAS
prompt ===================================
prompt
create table TB_CA_SENHAS_ANTIGAS
(
  ID           NUMBER not null,
  SENHA        VARCHAR2(150) not null,
  DATA_CRIACAO DATE default to_date(to_char(sysdate, 'dd/mm/yyyy hh:mi:ss'), 'dd/mm/yyyy hh:mi:ss') not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_CA_USUARIO
prompt ============================
prompt
create table TB_CA_USUARIO
(
  ID                   NUMBER(4) not null,
  NOME                 VARCHAR2(150) not null,
  EMAIL                VARCHAR2(150) not null,
  IDASSESSOR           NUMBER(8,2),
  IDPERFIL             NUMBER(4),
  TIPOUSUARIO          CHAR(1) not null,
  ATIVO                NUMBER(1) not null,
  LOGIN                VARCHAR2(20) not null,
  SENHA                VARCHAR2(150),
  TENTATIVAS_RESTANTES NUMBER(1) default 5 not null,
  BLOQUEADO            NUMBER(1) default 0 not null,
  DATA_SENHA           DATE default to_date(to_char(sysdate, 'dd/mm/yyyy'), 'dd/mm/yyyy'),
  CHAVE_AD             VARCHAR2(12),
  CDBOLSA              NUMBER(7),
  SUITABILITY_STATUS   NUMBER default 4,
  INVESTIDOR           NUMBER default 0,
  DESENQUADRADO        VARCHAR2(1) default 'N'
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
comment on column TB_CA_USUARIO.TENTATIVAS_RESTANTES
  is 'Numero de tentativas restantes';
alter table TB_CA_USUARIO
  add constraint PK_TB_CA_USUARIO primary key (LOGIN)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table TB_CA_USUARIO
  add constraint FK_CA_USUARIO_PERFIL foreign key (IDPERFIL)
  references TB_CA_PERFIL (ID);
create unique index PK_CA_USUARIO on TB_CA_USUARIO (ID, EMAIL)
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_CF_CLIENTE
prompt ============================
prompt
create table TB_CF_CLIENTE
(
  CD_CLIENTE NUMBER(7) not null,
  NM_CLIENTE VARCHAR2(60)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table TB_CF_CLIENTE
  add constraint PK_CF_CLIENTE primary key (CD_CLIENTE)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_CF_FUNCIONALIDADE
prompt ===================================
prompt
create table TB_CF_FUNCIONALIDADE
(
  ID_FUNCIONALIDADE NUMBER(5) not null,
  NM_FUNCIONALIDADE VARCHAR2(60) not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table TB_CF_FUNCIONALIDADE
  add constraint PK_CF_FUNCIONALIDADE primary key (ID_FUNCIONALIDADE)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_CF_CLIENTE_FUNC
prompt =================================
prompt
create table TB_CF_CLIENTE_FUNC
(
  ID_REL_CLI_FUNC   NUMBER not null,
  CD_CLIENTE        NUMBER(7) not null,
  ID_FUNCIONALIDADE NUMBER(5) not null,
  FILETYPE          CHAR(1) default 'P' not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
comment on column TB_CF_CLIENTE_FUNC.FILETYPE
  is 'H - Html, P- PDF';
alter table TB_CF_CLIENTE_FUNC
  add constraint PK_REL_CLI_FUNC primary key (ID_REL_CLI_FUNC)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table TB_CF_CLIENTE_FUNC
  add constraint UNQ_CLIENTE_FUNC unique (CD_CLIENTE, ID_FUNCIONALIDADE)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table TB_CF_CLIENTE_FUNC
  add constraint FK_CLIENTE foreign key (CD_CLIENTE)
  references TB_CF_CLIENTE (CD_CLIENTE) on delete cascade;
alter table TB_CF_CLIENTE_FUNC
  add constraint FK_FUNCIONALIDADE foreign key (ID_FUNCIONALIDADE)
  references TB_CF_FUNCIONALIDADE (ID_FUNCIONALIDADE) on delete cascade;

prompt
prompt Creating table TB_CF_EMAIL
prompt ==========================
prompt
create table TB_CF_EMAIL
(
  ID_EMAIL        NUMBER not null,
  NM_EMAIL        VARCHAR2(80) not null,
  ID_REL_CLI_FUNC NUMBER not null,
  EMAILTYPE       CHAR(1) default 'P' not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
comment on column TB_CF_EMAIL.EMAILTYPE
  is 'P - Principal, C - Conta';
alter table TB_CF_EMAIL
  add constraint PK_CF_EMAIL primary key (ID_EMAIL)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table TB_CF_EMAIL
  add constraint UNQ_EMAIL unique (ID_EMAIL, NM_EMAIL, ID_REL_CLI_FUNC)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table TB_CF_EMAIL
  add constraint FK_ID_REL_CLI_FUNC foreign key (ID_REL_CLI_FUNC)
  references TB_CF_CLIENTE_FUNC (ID_REL_CLI_FUNC) on delete cascade;

prompt
prompt Creating table TB_CLIENTES_OPERADORES
prompt =====================================
prompt
create table TB_CLIENTES_OPERADORES
(
  CD_CLIENTE  NUMBER(7) not null,
  CD_OPERADOR NUMBER(5) not null,
  TP_CLIENTE  NUMBER(1) not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table TB_CLIENTES_OPERADORES
  add constraint PK_TB_CLIENTES_OPERADORES primary key (CD_CLIENTE, TP_CLIENTE, CD_OPERADOR)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_CLIENTE_COMPLEMENTO
prompt =====================================
prompt
create table TB_CLIENTE_COMPLEMENTO
(
  CD_CLIENTE            NUMBER(7) not null,
  NM_CLIENTE            VARCHAR2(60),
  CD_CLIENTE_SECURITIES NUMBER(7),
  NM_ENDERECO           VARCHAR2(100),
  NM_EMAIL              VARCHAR2(80),
  NR_TELEFONE           NUMBER(10),
  NR_FAX                NUMBER(10),
  PC_REPASSE            NUMBER(6,3),
  NR_DDD_TEL            NUMBER(7),
  NR_DDD_FAX            NUMBER(7)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
comment on table TB_CLIENTE_COMPLEMENTO
  is 'Cont�m os dados complementares dos clientes';
comment on column TB_CLIENTE_COMPLEMENTO.CD_CLIENTE
  is 'C�digo de cliente Sinacor';
comment on column TB_CLIENTE_COMPLEMENTO.NM_CLIENTE
  is 'Nome do cliente (complemento)';
comment on column TB_CLIENTE_COMPLEMENTO.CD_CLIENTE_SECURITIES
  is 'C�digo de cliente no Securities';
comment on column TB_CLIENTE_COMPLEMENTO.NM_ENDERECO
  is 'Endere�o do cliente  no Securities';
comment on column TB_CLIENTE_COMPLEMENTO.NM_EMAIL
  is 'Email do cliente  no Securities';
comment on column TB_CLIENTE_COMPLEMENTO.NR_TELEFONE
  is 'Telefone do cliente  no Securities';
comment on column TB_CLIENTE_COMPLEMENTO.NR_FAX
  is 'Fax  do cliente  no Securities';
comment on column TB_CLIENTE_COMPLEMENTO.PC_REPASSE
  is '% de Repasse do cliente';
comment on column TB_CLIENTE_COMPLEMENTO.NR_DDD_TEL
  is 'DDD do telefone';
comment on column TB_CLIENTE_COMPLEMENTO.NR_DDD_FAX
  is 'DDD do fax';
alter table TB_CLIENTE_COMPLEMENTO
  add constraint PK_TB_CLIENTE_COMPLEMENTO primary key (CD_CLIENTE)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_COL_AUXILIA_POSICAO
prompt =====================================
prompt
create table TB_COL_AUXILIA_POSICAO
(
  CD_CLIENTE      NUMBER(7) not null,
  DT_NEGOCIO      DATE not null,
  NR_NEGOCIO      NUMBER(9) not null,
  ID_LINHA        NUMBER(15) not null,
  CD_NATOPE       CHAR(1) not null,
  CD_NEGOCIO      VARCHAR2(12) not null,
  QT_POSICAO_UTIL NUMBER(15) default 0,
  PU_COMPRA       NUMBER(17,8) default 0,
  TP_APURACAO     NUMBER(1) not null,
  PU_BRUTO        NUMBER(17,8) default 0,
  DT_POSICAO      DATE,
  PU_VENDA        NUMBER(17,8),
  VL_TAXAS        NUMBER(17,2) default 0,
  IN_LIQUIDA      CHAR(1) default 'N'
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
comment on column TB_COL_AUXILIA_POSICAO.TP_APURACAO
  is 'Tipos 2 - FIFO, 3 - LIFO e 9 - Day Trade';
comment on column TB_COL_AUXILIA_POSICAO.PU_BRUTO
  is 'Pu bruto da opera��o';
comment on column TB_COL_AUXILIA_POSICAO.DT_POSICAO
  is 'Data de posi��o';
alter table TB_COL_AUXILIA_POSICAO
  add constraint PK_TB_COL_AUXILIA_POSICAO primary key (CD_CLIENTE, DT_NEGOCIO, NR_NEGOCIO, ID_LINHA, CD_NATOPE, CD_NEGOCIO, TP_APURACAO)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_COL_CLIENTE
prompt =============================
prompt
create table TB_COL_CLIENTE
(
  CD_CLIENTE    NUMBER(7) not null,
  TP_APURACAO   NUMBER(1),
  IN_ENVIA_HIST CHAR(1) default 'N',
  IN_ENVIA_PDF  CHAR(1) default 'S',
  DS_CONTATO    VARCHAR2(200),
  DT_INICIO     DATE,
  VL_INICIO     NUMBER(18,2)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
comment on table TB_COL_CLIENTE
  is 'Armazena os par�metros dos clientes que ser�o acompanhados pelo sistema';
comment on column TB_COL_CLIENTE.CD_CLIENTE
  is 'C�digo de cliente';
comment on column TB_COL_CLIENTE.TP_APURACAO
  is 'Identifica qual o tipo de apura��o ser� utilizado pelo cliente';
comment on column TB_COL_CLIENTE.IN_ENVIA_HIST
  is 'Indica se envia Hist�rico';
comment on column TB_COL_CLIENTE.IN_ENVIA_PDF
  is 'Indica se envia em formato PDF("S") ou Excel ("N")';
comment on column TB_COL_CLIENTE.DS_CONTATO
  is 'Contatos inseridos pelo operador';
comment on column TB_COL_CLIENTE.DT_INICIO
  is 'Data de in�cio da carteira';
comment on column TB_COL_CLIENTE.VL_INICIO
  is 'Valor inicial da carteira';
alter table TB_COL_CLIENTE
  add constraint PK_TB_COL_CLIENTE primary key (CD_CLIENTE)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_COL_CLIENTE_OPERADOR
prompt ======================================
prompt
create table TB_COL_CLIENTE_OPERADOR
(
  CD_CLIENTE NUMBER(7) not null,
  ID_USUARIO NUMBER(7) not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table TB_COL_CLIENTE_OPERADOR
  add constraint PK_COL_CLIENTE_OPERADOR primary key (CD_CLIENTE, ID_USUARIO)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table TB_COL_CLIENTE_OPERADOR
  add constraint FK_COL_CLIENTE_CD_CLIENTE foreign key (CD_CLIENTE)
  references TB_COL_CLIENTE (CD_CLIENTE);

prompt
prompt Creating table TB_COL_COTACAO
prompt =============================
prompt
create table TB_COL_COTACAO
(
  CD_NEGOCIO       VARCHAR2(12) not null,
  VL_COTACAO       NUMBER(17,6) default 0,
  VL_MENOR_COTACAO NUMBER(17,6) default 0,
  VL_MAIOR_COTACAO NUMBER(17,6) default 0,
  DT_COTACAO       DATE
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
comment on column TB_COL_COTACAO.CD_NEGOCIO
  is 'C�digo de negocia��o';
comment on column TB_COL_COTACAO.VL_COTACAO
  is '�ltima cota��o para o mercado';
comment on column TB_COL_COTACAO.VL_MENOR_COTACAO
  is 'Menor cota��o para o preg�o';
comment on column TB_COL_COTACAO.VL_MAIOR_COTACAO
  is 'Maior cota��o para o preg�o';
comment on column TB_COL_COTACAO.DT_COTACAO
  is 'Data/Hora da cota��o';
alter table TB_COL_COTACAO
  add constraint PK_TB_COL_COTACAO primary key (CD_NEGOCIO)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_COL_DAY_TRADE
prompt ===============================
prompt
create table TB_COL_DAY_TRADE
(
  CD_CLIENTE        NUMBER(7),
  NR_NEGOCIO_C      NUMBER(9),
  NR_NEGOCIO_V      NUMBER(9),
  CD_NEGOCIO        VARCHAR2(12),
  DT_NEGOCIO        DATE,
  QT_COMPRA         NUMBER(15),
  QT_VENDA          NUMBER(15),
  QT_DAYTRADE       NUMBER(15),
  QT_SALDO          NUMBER(15),
  PU_COMPRA_LIQUIDO NUMBER(17,8),
  PU_COMPRA_BRUTO   NUMBER(17,8),
  PU_VENDA_LIQUIDO  NUMBER(17,8),
  PU_VENDA_BRUTO    NUMBER(17,8),
  TP_MERCADO        VARCHAR2(3),
  DT_VENCIMENTO     DATE
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
create index NU1_TB_COL_RESUMO_DIA on TB_COL_DAY_TRADE (CD_CLIENTE, NR_NEGOCIO_C, NR_NEGOCIO_V, CD_NEGOCIO, DT_NEGOCIO)
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_COL_NEGOCIOS_DIA
prompt ==================================
prompt
create table TB_COL_NEGOCIOS_DIA
(
  CD_CLIENTE       NUMBER(7) not null,
  DT_NEGOCIO       DATE not null,
  NR_NEGOCIO       NUMBER(9) not null,
  CD_NATOPE        CHAR(1) not null,
  CD_NEGOCIO       VARCHAR2(12) not null,
  QT_NEGOCIO       NUMBER(15) default 0,
  PU_NEGOCIO       NUMBER(17,8) default 0,
  QT_POSICAO_UTIL  NUMBER(15) default 0,
  PU_POSICAO       NUMBER(17,8) default 0,
  IN_DAYTRADE      CHAR(1) default 'N',
  QT_DAYTRADE      NUMBER(15) default 0,
  VL_RESULTADO_POS NUMBER(17,2) default 0,
  VL_RESULTADO_DT  NUMBER(17,2) default 0,
  IN_POSICAO       CHAR(1) default 'N',
  TP_MERCADO       VARCHAR2(3) not null,
  FT_VALORIZACAO   NUMBER(15,5),
  PU_BRUTO         NUMBER(17,8) default 0,
  DT_POSICAO       DATE,
  TP_APURACAO      NUMBER(1),
  DT_VENCIMENTO    DATE,
  PU_VENDA         NUMBER(17,8) default 0,
  IN_LIQUIDA       CHAR(1) default 'N',
  QT_COMPRA        NUMBER(15) default 0,
  QT_VENDA         NUMBER(15) default 0
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
comment on column TB_COL_NEGOCIOS_DIA.CD_CLIENTE
  is 'Codigo de cliente';
comment on column TB_COL_NEGOCIOS_DIA.DT_NEGOCIO
  is 'Data do neg�cio';
comment on column TB_COL_NEGOCIOS_DIA.NR_NEGOCIO
  is 'N�mero do neg�cio';
comment on column TB_COL_NEGOCIOS_DIA.CD_NATOPE
  is 'Compra ou Venda';
comment on column TB_COL_NEGOCIOS_DIA.CD_NEGOCIO
  is 'C�digo de negocia��o';
comment on column TB_COL_NEGOCIOS_DIA.QT_NEGOCIO
  is 'Quantidade de neg�cio';
comment on column TB_COL_NEGOCIOS_DIA.PU_NEGOCIO
  is 'Pu do neg�cio + financeiro Taxas/Emolumentos/Corretagem';
comment on column TB_COL_NEGOCIOS_DIA.QT_POSICAO_UTIL
  is 'Quantidade utilizada da posi��o';
comment on column TB_COL_NEGOCIOS_DIA.PU_POSICAO
  is 'Pu da posi��o + financeiro Taxas/Emolumentos/Corretagem';
comment on column TB_COL_NEGOCIOS_DIA.IN_DAYTRADE
  is 'Indica se houve Dau Trade';
comment on column TB_COL_NEGOCIOS_DIA.QT_DAYTRADE
  is 'Quantidade de Day Trade';
comment on column TB_COL_NEGOCIOS_DIA.VL_RESULTADO_POS
  is 'Resultado Neg�cio contra posi��o';
comment on column TB_COL_NEGOCIOS_DIA.VL_RESULTADO_DT
  is 'Resultado Day Trade';
comment on column TB_COL_NEGOCIOS_DIA.IN_POSICAO
  is 'Indica se o neg�cio virou posi��o';
comment on column TB_COL_NEGOCIOS_DIA.TP_MERCADO
  is 'Tipo de mercado';
comment on column TB_COL_NEGOCIOS_DIA.FT_VALORIZACAO
  is 'Fator de cota��o';
comment on column TB_COL_NEGOCIOS_DIA.PU_BRUTO
  is 'Pre�o de custo bruto';
comment on column TB_COL_NEGOCIOS_DIA.DT_POSICAO
  is 'Data da posi��o';
comment on column TB_COL_NEGOCIOS_DIA.TP_APURACAO
  is 'Tipo de apura��o';
comment on column TB_COL_NEGOCIOS_DIA.DT_VENCIMENTO
  is 'Data de vencimento do Termo';
alter table TB_COL_NEGOCIOS_DIA
  add constraint PK_TB_COL_NEGOCIOS_DIA primary key (CD_CLIENTE, DT_NEGOCIO, NR_NEGOCIO, CD_NATOPE, CD_NEGOCIO, TP_MERCADO)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_COL_OPERADOR
prompt ==============================
prompt
create table TB_COL_OPERADOR
(
  CD_OPERA_MEGA NUMBER(6) not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
comment on table TB_COL_OPERADOR
  is 'Armazena os operadores da mesa';
comment on column TB_COL_OPERADOR.CD_OPERA_MEGA
  is 'C�digo de operador MEGA';
alter table TB_COL_OPERADOR
  add constraint PK_TB_COL_OPERADOR primary key (CD_OPERA_MEGA)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_COL_PARAMETRO
prompt ===============================
prompt
create table TB_COL_PARAMETRO
(
  DT_MOVIMENTO           DATE,
  TP_MOVIMENTO           CHAR(1),
  DT_ATUALIZACAO_SISTEMA DATE,
  DT_IBOV                DATE,
  VL_ULTIMO_IBOV_ANO     NUMBER(17,8),
  VL_IBOV_D_1            NUMBER(17,8),
  HR_ZERAGEM_OPCAO       VARCHAR2(8),
  DT_HORA_ULTIMA_EXEC    DATE
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
comment on table TB_COL_PARAMETRO
  is 'Tabela de par�metros de sistema';
comment on column TB_COL_PARAMETRO.DT_MOVIMENTO
  is 'Data em que o sistema est� aberto';
comment on column TB_COL_PARAMETRO.TP_MOVIMENTO
  is 'Indica se o movimento est� Aberto "A" ou Fechado "F"';
comment on column TB_COL_PARAMETRO.DT_IBOV
  is 'Data do �ltimo IBOV do �ltimo ano';
comment on column TB_COL_PARAMETRO.VL_ULTIMO_IBOV_ANO
  is 'Valor do IBOV de D-1';
comment on column TB_COL_PARAMETRO.HR_ZERAGEM_OPCAO
  is 'Hora de zeragem de op��es';
comment on column TB_COL_PARAMETRO.DT_HORA_ULTIMA_EXEC
  is 'Ultima execu�a� do rob� de apura��o ';

prompt
prompt Creating table TB_COL_POSICAO
prompt =============================
prompt
create table TB_COL_POSICAO
(
  CD_CLIENTE     NUMBER(7),
  DT_NEGOCIO     DATE,
  CD_NEGOCIO     VARCHAR2(12),
  TP_MERCADO     VARCHAR2(3),
  NM_ESPECI      VARCHAR2(3),
  CD_CARTEIRA    NUMBER(5),
  QT_QTDESP      NUMBER(15),
  PU_CUSTO       NUMBER(17,8),
  DT_POSICAO     DATE,
  IN_POSICAO     CHAR(1) default 'S',
  CD_NATOPE      CHAR(1),
  FT_VALORIZACAO NUMBER(15,5),
  ID_LINHA       NUMBER(15) not null,
  QT_SALDO       NUMBER(15) default 0,
  PU_BRUTO       NUMBER(17,8) default 0,
  DT_VENCIMENTO  DATE,
  DT_ROLAGEM     DATE,
  NM_USUARIO     VARCHAR2(100),
  NR_NEGOCIO     NUMBER(9),
  IN_LONG_SHORT  CHAR(1) default 'N',
  PU_MEDIO       NUMBER(17,8) default 0,
  IN_LIQUIDADO   CHAR(1) default 'N'
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
comment on column TB_COL_POSICAO.CD_CLIENTE
  is 'C�digo de Cliente';
comment on column TB_COL_POSICAO.DT_NEGOCIO
  is 'Dat� do Neg�cio';
comment on column TB_COL_POSICAO.CD_NEGOCIO
  is 'C�digo do Neg�cio';
comment on column TB_COL_POSICAO.TP_MERCADO
  is 'Tipo de Mercado';
comment on column TB_COL_POSICAO.CD_CARTEIRA
  is 'C�digo da Carteira';
comment on column TB_COL_POSICAO.QT_QTDESP
  is 'Qtd. Dispon�vel';
comment on column TB_COL_POSICAO.PU_CUSTO
  is 'Pre�o d Custo liquido';
comment on column TB_COL_POSICAO.DT_POSICAO
  is 'Data da apura��o da Posi��o';
comment on column TB_COL_POSICAO.IN_POSICAO
  is 'Indica se � posi��o de abertura "S" ou posi��o aberta no dia "N"';
comment on column TB_COL_POSICAO.CD_NATOPE
  is 'Compra ou Venda';
comment on column TB_COL_POSICAO.FT_VALORIZACAO
  is 'Fator de cota��o';
comment on column TB_COL_POSICAO.ID_LINHA
  is 'Identificador da linha';
comment on column TB_COL_POSICAO.QT_SALDO
  is 'Saldo de posi��o (n�o utilizada)';
comment on column TB_COL_POSICAO.PU_BRUTO
  is 'PU Bruto';
comment on column TB_COL_POSICAO.DT_VENCIMENTO
  is 'Data de vencimento do Termo';
comment on column TB_COL_POSICAO.DT_ROLAGEM
  is 'Data de Rolagem do Termo';
comment on column TB_COL_POSICAO.NM_USUARIO
  is 'Nome do usu�rio que esta mantendo o registro';
comment on column TB_COL_POSICAO.NR_NEGOCIO
  is 'Numero do neg�cio -  utilizar para liquidar o Termo';
comment on column TB_COL_POSICAO.IN_LONG_SHORT
  is 'Indica se a opera��o � Long Short';
comment on column TB_COL_POSICAO.IN_LIQUIDADO
  is 'Indica se o termo foi liquidado';
alter table TB_COL_POSICAO
  add constraint PK_TB_COL_POSICAO primary key (ID_LINHA)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_COL_POSICAO_ABERTURA
prompt ======================================
prompt
create table TB_COL_POSICAO_ABERTURA
(
  CD_CLIENTE        NUMBER(7),
  CD_NEGOCIO        VARCHAR2(12),
  DT_POSICAO        DATE,
  QT_COMPRA         NUMBER(15),
  QT_VENDA          NUMBER(15),
  QT_UTILIZADO      NUMBER(15),
  QT_SALDO          NUMBER(15),
  PU_COMPRA_LIQUIDO NUMBER(17,8),
  PU_COMPRA_BRUTO   NUMBER(17,8),
  PU_VENDA_LIQUIDO  NUMBER(17,8),
  PU_VENDA_BRUTO    NUMBER(17,8),
  TP_MERCADO        VARCHAR2(3)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_COL_POSICAO_DIA
prompt =================================
prompt
create table TB_COL_POSICAO_DIA
(
  CD_CLIENTE        NUMBER(7),
  CD_NEGOCIO        VARCHAR2(12),
  DT_POSICAO        DATE,
  NR_NEGOCIO        NUMBER(9),
  QT_COMPRA         NUMBER(15),
  QT_VENDA          NUMBER(15),
  QT_UTILIZADO      NUMBER(15),
  QT_SALDO          NUMBER(15),
  PU_COMPRA_LIQUIDO NUMBER(17,8),
  PU_COMPRA_BRUTO   NUMBER(17,8),
  PU_VENDA_LIQUIDO  NUMBER(17,8),
  PU_VENDA_BRUTO    NUMBER(17,8),
  TP_MERCADO        VARCHAR2(3),
  DT_VENCIMENTO     DATE
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_COL_POSICAO_TEMP
prompt ==================================
prompt
create table TB_COL_POSICAO_TEMP
(
  CD_CLIENTE     NUMBER(7),
  DT_NEGOCIO     DATE,
  CD_NEGOCIO     VARCHAR2(12),
  TP_MERCADO     VARCHAR2(3),
  NM_ESPECI      VARCHAR2(3),
  CD_CARTEIRA    NUMBER(5),
  QT_QTDESP      NUMBER(15),
  PU_CUSTO       NUMBER(17,8),
  DT_POSICAO     DATE,
  IN_POSICAO     CHAR(1),
  CD_NATOPE      CHAR(1),
  FT_VALORIZACAO NUMBER(15,5),
  ID_LINHA       NUMBER(15) not null,
  QT_SALDO       NUMBER(15),
  PU_BRUTO       NUMBER(17,8),
  DT_VENCIMENTO  DATE,
  DT_ROLAGEM     DATE,
  NM_USUARIO     VARCHAR2(100),
  NR_NEGOCIO     NUMBER(9),
  IN_LONG_SHORT  CHAR(1),
  PU_MEDIO       NUMBER(17,8)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_COL_REGISTRO_CONTATO
prompt ======================================
prompt
create table TB_COL_REGISTRO_CONTATO
(
  CD_CLIENTE          NUMBER(7),
  DT_CONTATO          DATE,
  DT_REGISTRO_CONTATO DATE,
  DS_CONTATO          VARCHAR2(4000),
  ID_USUARIO          NUMBER(5)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
create index NU_TB_COL_REGISTRO_CONTATO on TB_COL_REGISTRO_CONTATO (CD_CLIENTE, DT_CONTATO)
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_COL_RESULTADO_DIA
prompt ===================================
prompt
create table TB_COL_RESULTADO_DIA
(
  CD_CLIENTE        NUMBER(7),
  IN_POSICAO        VARCHAR2(2),
  DS_POSICAO        VARCHAR2(50),
  CD_NEGOCIO        VARCHAR2(12),
  DT_NEGOCIO        DATE,
  TP_MERCADO        VARCHAR2(3),
  QT_CONSIDERADA    NUMBER(15) default 0,
  VL_DIFERENCA      NUMBER(17,2) default 0,
  VL_LUCRO_PREJUIZO NUMBER(17,2) default 0,
  PU_NEGOCIO        NUMBER(17,8) default 0,
  PU_POSICAO        NUMBER(17,8) default 0,
  PU_COMPRA_BRUTO   NUMBER(17,8) default 0,
  PU_MERCADO_BRUTO  NUMBER(17,8) default 0,
  FT_COTACAO        NUMBER(15,5),
  DS_SETOR          VARCHAR2(50),
  DT_MOVIMENTO      DATE,
  CD_NATOPE         CHAR(1),
  DT_VENCIMENTO     DATE,
  NR_NEGOCIO        NUMBER(9),
  IN_STATUS_OPCAO   CHAR(1) default 'N'
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
create index NU1_TB_COL_RESULTADO_DIA on TB_COL_RESULTADO_DIA (CD_CLIENTE, CD_NEGOCIO, DT_NEGOCIO, TP_MERCADO)
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_COL_RESULTADO_HISTORICO
prompt =========================================
prompt
create table TB_COL_RESULTADO_HISTORICO
(
  CD_CLIENTE        NUMBER(7),
  IN_POSICAO        CHAR(1),
  CD_NEGOCIO        VARCHAR2(12),
  DT_NEGOCIO        DATE,
  TP_MERCADO        VARCHAR2(3),
  QT_QTDESP         NUMBER(15),
  PU_COMPRA         NUMBER(17,8),
  VL_COMPRA_LIQUIDO NUMBER(17,2),
  PU_MERCADO        NUMBER(17,8),
  VL_VENDA_LIQUIDO  NUMBER(17,2),
  FT_COTACAO        NUMBER(15,5),
  VL_LUCRO_PREJUIZO NUMBER(17,2),
  IN_DAYTRADE       CHAR(1),
  PC_LUCRO_PREJUIZO NUMBER(8,3),
  DT_MOVIMENTO      DATE,
  DT_VENCIMENTO     DATE
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
create index NU1_TB_COL_RESULTADO_HISTORICO on TB_COL_RESULTADO_HISTORICO (CD_CLIENTE, CD_NEGOCIO, DT_NEGOCIO, TP_MERCADO)
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_COL_TIPO_APURACAO
prompt ===================================
prompt
create table TB_COL_TIPO_APURACAO
(
  TP_APURACAO NUMBER(1) not null,
  DS_APURACAO VARCHAR2(25)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table TB_COL_TIPO_APURACAO
  add constraint PK_TB_COL_TIPO_APURACAO primary key (TP_APURACAO)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_CONTROLA_LIQUIDACAO
prompt =====================================
prompt
create table TB_CONTROLA_LIQUIDACAO
(
  DT_ULTIMA_LIQUIDACAO DATE
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
comment on table TB_CONTROLA_LIQUIDACAO
  is 'Contem data de controle da liquidacao de repasse';
comment on column TB_CONTROLA_LIQUIDACAO.DT_ULTIMA_LIQUIDACAO
  is 'Data da �ltima liquidacao de repasse';

prompt
prompt Creating table TB_CONTROL_SESSION
prompt =================================
prompt
create table TB_CONTROL_SESSION
(
  USERNAME   VARCHAR2(100),
  INSERTDATE DATE,
  SESSIONID  VARCHAR2(100),
  IP         VARCHAR2(100)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_TIPO
prompt ======================
prompt
create table TB_TIPO
(
  ID            NUMBER not null,
  DESCRICAO     NVARCHAR2(80) not null,
  IDELEMENTO    NUMBER not null,
  ESPECIFICACAO NVARCHAR2(256) not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table TB_TIPO
  add constraint TB_CA_TIPO_PK primary key (ID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table TB_TIPO
  add constraint TB_CA_TIPO_ELEMENTO_FK foreign key (IDELEMENTO)
  references TB_CA_ELEMENTO (ID)
  disable;

prompt
prompt Creating table TB_CORRECAO_ORDEM
prompt ================================
prompt
create table TB_CORRECAO_ORDEM
(
  ID               NUMBER not null,
  IDTIPO           NUMBER not null,
  IDCLIENTEORIGEM  NUMBER not null,
  IDCLIENTEDESTINO NUMBER,
  IDDESCRICAO      NUMBER not null,
  DATASOLICITACAO  DATE not null,
  OBSERVACAO       VARCHAR2(150) not null,
  IDSTATUS         NUMBER not null,
  IDUSUARIO        NUMBER not null,
  D1               NUMBER(1) not null,
  IDASSESSOR       NUMBER not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table TB_CORRECAO_ORDEM
  add constraint CO_PK primary key (ID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table TB_CORRECAO_ORDEM
  add constraint CO_DESCRICAO_FK foreign key (IDDESCRICAO)
  references TB_TIPO (ID);
alter table TB_CORRECAO_ORDEM
  add constraint CO_STATUS_FK foreign key (IDSTATUS)
  references TB_TIPO (ID);
alter table TB_CORRECAO_ORDEM
  add constraint CO_TIPO_FK foreign key (IDTIPO)
  references TB_TIPO (ID);

prompt
prompt Creating table TB_CORRECAO_ORDEM_DETALHE
prompt ========================================
prompt
create table TB_CORRECAO_ORDEM_DETALHE
(
  IDCORRECAOORDEM NUMBER not null,
  PAPEL           VARCHAR2(12) not null,
  QUANTIDADE      NUMBER(9) not null,
  PRECO           NUMBER(8,4)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_CUSTOMER_LEDGER
prompt =================================
prompt
create table TB_CUSTOMER_LEDGER
(
  CD_CLIENTE        NUMBER(7) not null,
  NR_NEGOCIO        NUMBER(9) not null,
  NM_EMPRESA        VARCHAR2(60),
  DT_NEGOCIO        DATE not null,
  DT_LIQUIDACAO     DATE not null,
  CD_NATOPE         CHAR(1) not null,
  VL_NEGOCIO        NUMBER(17,2),
  QT_QTDESP         NUMBER(15),
  VL_LIQUIDADO      NUMBER(17,2),
  NM_MOEDA          VARCHAR2(3),
  DT_LIQUIDACAO_EFE DATE,
  QT_FALHA_EFETIVA  NUMBER(15),
  VL_FALHA_EFETIVA  NUMBER(17,2),
  QT_FALHA          NUMBER(15),
  VL_FALHA          NUMBER(17,2),
  CD_NEGOCIO        VARCHAR2(12) not null,
  DT_GERACAO        DATE not null,
  DT_ANO_MES        NUMBER(6) not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
comment on table TB_CUSTOMER_LEDGER
  is 'Armazena os dados do relat�rio Customer Ledger';
comment on column TB_CUSTOMER_LEDGER.CD_CLIENTE
  is 'C�digo de cliente';
comment on column TB_CUSTOMER_LEDGER.NR_NEGOCIO
  is 'N�mero do neg�cio';
comment on column TB_CUSTOMER_LEDGER.NM_EMPRESA
  is 'Nome da empresa';
comment on column TB_CUSTOMER_LEDGER.DT_NEGOCIO
  is 'Data do neg�cio';
comment on column TB_CUSTOMER_LEDGER.DT_LIQUIDACAO
  is 'Data de liquida��o';
comment on column TB_CUSTOMER_LEDGER.CD_NATOPE
  is 'Natureza da opera��o';
comment on column TB_CUSTOMER_LEDGER.VL_NEGOCIO
  is 'Valor do neg�cio';
comment on column TB_CUSTOMER_LEDGER.QT_QTDESP
  is 'Quantidade do neg�cio';
comment on column TB_CUSTOMER_LEDGER.VL_LIQUIDADO
  is 'Valor l�quido';
comment on column TB_CUSTOMER_LEDGER.NM_MOEDA
  is 'Nome da moeda';
comment on column TB_CUSTOMER_LEDGER.DT_LIQUIDACAO_EFE
  is 'Data de liquida��o efetiva';
comment on column TB_CUSTOMER_LEDGER.QT_FALHA_EFETIVA
  is 'Quantidade de falha efetiva';
comment on column TB_CUSTOMER_LEDGER.VL_FALHA_EFETIVA
  is 'Valor de falha efetivo';
comment on column TB_CUSTOMER_LEDGER.QT_FALHA
  is 'Quantidade de falha';
comment on column TB_CUSTOMER_LEDGER.VL_FALHA
  is 'Valor de falha';
comment on column TB_CUSTOMER_LEDGER.CD_NEGOCIO
  is 'C�digo do neg�cio';
comment on column TB_CUSTOMER_LEDGER.DT_GERACAO
  is 'Data de gera��o do relat�rio';
comment on column TB_CUSTOMER_LEDGER.DT_ANO_MES
  is 'M�s e ano de refer�ncia';
alter table TB_CUSTOMER_LEDGER
  add constraint PK_TB_CUSTOMER_LEDGER primary key (CD_CLIENTE, NR_NEGOCIO, DT_NEGOCIO, DT_LIQUIDACAO, CD_NATOPE, CD_NEGOCIO, DT_GERACAO, DT_ANO_MES)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_CUSTOMER_STATEMENT
prompt ====================================
prompt
create table TB_CUSTOMER_STATEMENT
(
  CD_CLIENTE         NUMBER(7) not null,
  NM_EMPRESA         VARCHAR2(12),
  DT_NEGOCIO         DATE not null,
  CD_NATOPE          CHAR(1) not null,
  VL_PRECO_MEDIO     NUMBER(17,2),
  NM_MOEDA           VARCHAR2(3),
  QT_QTDESP          NUMBER(15),
  VL_LIQUIDO         NUMBER(17,2),
  DT_LIQUIDACAO      DATE not null,
  VL_LIQUIDO_EFETIVO NUMBER(17,2),
  QT_LIQUIDO_FALHA   NUMBER(15),
  VL_LIQUIDO_FALHA   NUMBER(17,2),
  CD_NEGOCIO         VARCHAR2(12) not null,
  DT_GERACAO         DATE not null,
  DT_ANO_MES         NUMBER(6) not null,
  CD_AGENTE          NUMBER(5)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 128K
    minextents 1
    maxextents unlimited
  );
comment on table TB_CUSTOMER_STATEMENT
  is 'Armazena relat�rio Customer Statement';
comment on column TB_CUSTOMER_STATEMENT.CD_CLIENTE
  is 'C�digo de cliente';
comment on column TB_CUSTOMER_STATEMENT.NM_EMPRESA
  is 'Nome da empresa';
comment on column TB_CUSTOMER_STATEMENT.DT_NEGOCIO
  is 'Data de neg�cio';
comment on column TB_CUSTOMER_STATEMENT.CD_NATOPE
  is 'Natureza da opera��o';
comment on column TB_CUSTOMER_STATEMENT.VL_PRECO_MEDIO
  is 'Valor do pre�o m�dio';
comment on column TB_CUSTOMER_STATEMENT.NM_MOEDA
  is 'Nome da moeda';
comment on column TB_CUSTOMER_STATEMENT.QT_QTDESP
  is 'Quantidade do neg�cio';
comment on column TB_CUSTOMER_STATEMENT.VL_LIQUIDO
  is 'Valor l�quido';
comment on column TB_CUSTOMER_STATEMENT.DT_LIQUIDACAO
  is 'Data da Liquida��o';
comment on column TB_CUSTOMER_STATEMENT.VL_LIQUIDO_EFETIVO
  is 'Valor liquido efetivo';
comment on column TB_CUSTOMER_STATEMENT.QT_LIQUIDO_FALHA
  is 'Quantidade liquida da falha';
comment on column TB_CUSTOMER_STATEMENT.VL_LIQUIDO_FALHA
  is 'Valor liquido da falha';
comment on column TB_CUSTOMER_STATEMENT.CD_NEGOCIO
  is 'C�digo do neg�cio';
comment on column TB_CUSTOMER_STATEMENT.DT_GERACAO
  is 'Data de gera��o';
comment on column TB_CUSTOMER_STATEMENT.DT_ANO_MES
  is 'M�s e ano de refer�ncia';
alter table TB_CUSTOMER_STATEMENT
  add constraint PK_TB_CUSTOMER_STATEMENT primary key (CD_CLIENTE, DT_NEGOCIO, CD_NATOPE, DT_LIQUIDACAO, CD_NEGOCIO, DT_GERACAO, DT_ANO_MES)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 128K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_DESENQUADRAMENTO
prompt ==================================
prompt
create table TB_DESENQUADRAMENTO
(
  ID         NUMBER not null,
  PERFIL     VARCHAR2(20) not null,
  CD_MERCADO VARCHAR2(5) not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_DISCLAIMER
prompt ============================
prompt
create table TB_DISCLAIMER
(
  ID_RELATORIO  NUMBER(3) not null,
  DS_DISCLAIMER CLOB
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
comment on column TB_DISCLAIMER.ID_RELATORIO
  is 'Identificador do relat�rio que ser� exibido';
comment on column TB_DISCLAIMER.DS_DISCLAIMER
  is 'Informa�ies que ser�o exibidas no relat�rio';
alter table TB_DISCLAIMER
  add constraint PK_TB_DISCLAIMER primary key (ID_RELATORIO)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_EXPIRACAO_SENHA
prompt =================================
prompt
create table TB_EXPIRACAO_SENHA
(
  DIAS_EXPIRACAO NUMBER not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_FAIL_RECIEVE_DELIVER
prompt ======================================
prompt
create table TB_FAIL_RECIEVE_DELIVER
(
  CD_CLIENTE      NUMBER(7) not null,
  CD_NEGOCIO      VARCHAR2(60) not null,
  NM_CLIENTE      VARCHAR2(60),
  NR_NEGOCIO      NUMBER(9) not null,
  CD_NATOPE       CHAR(1) not null,
  NM_EMPRESA      VARCHAR2(12),
  DT_NEGOCIO      DATE not null,
  DT_LIQUIDACAO   DATE not null,
  QT_FALHA        NUMBER(15),
  VL_FALHA        NUMBER(17,2),
  PR_MERCADO      NUMBER(17,2),
  VL_MERCADO      NUMBER(17,2),
  DT_GERACAO      DATE not null,
  IN_TP_RELATORIO CHAR(1) not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
comment on table TB_FAIL_RECIEVE_DELIVER
  is 'Armazena os relat�rios Fail Ledger';
comment on column TB_FAIL_RECIEVE_DELIVER.CD_CLIENTE
  is 'C�digo do cliente';
comment on column TB_FAIL_RECIEVE_DELIVER.CD_NEGOCIO
  is 'C�digo do neg�cio';
comment on column TB_FAIL_RECIEVE_DELIVER.NM_CLIENTE
  is 'Nome do cliente';
comment on column TB_FAIL_RECIEVE_DELIVER.NR_NEGOCIO
  is 'N�mero do neg�cio';
comment on column TB_FAIL_RECIEVE_DELIVER.CD_NATOPE
  is 'Natureza da opera��o';
comment on column TB_FAIL_RECIEVE_DELIVER.NM_EMPRESA
  is 'Nome da empresa';
comment on column TB_FAIL_RECIEVE_DELIVER.DT_NEGOCIO
  is 'Data do neg�cio';
comment on column TB_FAIL_RECIEVE_DELIVER.DT_LIQUIDACAO
  is 'Data de liquida��o';
comment on column TB_FAIL_RECIEVE_DELIVER.QT_FALHA
  is 'Quantidade de falha';
comment on column TB_FAIL_RECIEVE_DELIVER.VL_FALHA
  is 'Valor da falha';
comment on column TB_FAIL_RECIEVE_DELIVER.PR_MERCADO
  is 'Pre�o de mercado';
comment on column TB_FAIL_RECIEVE_DELIVER.VL_MERCADO
  is 'Valor de mercado (qtd x pre�o mercado)';
comment on column TB_FAIL_RECIEVE_DELIVER.DT_GERACAO
  is 'Data de Gera��o';
comment on column TB_FAIL_RECIEVE_DELIVER.IN_TP_RELATORIO
  is 'Tipo de relat�rio';
alter table TB_FAIL_RECIEVE_DELIVER
  add constraint PK_TB_FAIL_RECIEVE_DELIVER primary key (CD_CLIENTE, CD_NEGOCIO, NR_NEGOCIO, CD_NATOPE, DT_NEGOCIO, DT_LIQUIDACAO, DT_GERACAO, IN_TP_RELATORIO)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_FALHA
prompt =======================
prompt
create table TB_FALHA
(
  CD_CLIENTE     NUMBER(7) not null,
  DT_NEGOCIO     DATE not null,
  CD_NEGOCIO     VARCHAR2(12) not null,
  CD_NATOPE      CHAR(1) not null,
  NR_NEGOCIO     NUMBER(9) not null,
  QT_FALHA       NUMBER(15),
  VL_FALHA       NUMBER(17,2),
  NM_USUARIO     VARCHAR2(60),
  DT_ATUALIZACAO DATE,
  IN_LIQUIDADO   CHAR(1) default 'N',
  QT_LIQUIDADO   NUMBER(15) default 0,
  VL_LIQUIDADO   NUMBER(17,2) default 0,
  DT_LIQUIDACAO  DATE
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
comment on table TB_FALHA
  is 'Armazena os registros de falha';
comment on column TB_FALHA.CD_CLIENTE
  is 'C�digo de cliente no Sinacor';
comment on column TB_FALHA.DT_NEGOCIO
  is 'Data do neg�cio';
comment on column TB_FALHA.CD_NEGOCIO
  is 'C�digo do neg�cio';
comment on column TB_FALHA.CD_NATOPE
  is 'Natureza da opera��o';
comment on column TB_FALHA.NR_NEGOCIO
  is 'N�mero do neg�cio';
comment on column TB_FALHA.QT_FALHA
  is 'Quantidade de falha';
comment on column TB_FALHA.VL_FALHA
  is 'Valor financeiro da falha';
comment on column TB_FALHA.NM_USUARIO
  is 'Nome do usu�rio que registrou a falha';
comment on column TB_FALHA.DT_ATUALIZACAO
  is 'Data da ultima atualiza��o da falha';
comment on column TB_FALHA.IN_LIQUIDADO
  is 'Indica se o cliente liquidou a falha';
comment on column TB_FALHA.QT_LIQUIDADO
  is 'Quantidade liquidada da falha';
comment on column TB_FALHA.VL_LIQUIDADO
  is 'Valor liquidado da falha';
comment on column TB_FALHA.DT_LIQUIDACAO
  is 'Data de liquida��o da falha';
alter table TB_FALHA
  add constraint PK_TB_FALHA primary key (CD_CLIENTE, DT_NEGOCIO, CD_NEGOCIO, CD_NATOPE, NR_NEGOCIO)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_GARANTIA
prompt ==========================
prompt
create table TB_GARANTIA
(
  ID              NUMBER(9) not null,
  CDCLIENTE       NUMBER(7) not null,
  INRETIRADA      NUMBER(1) not null,
  INBOVESPA       NUMBER(1) not null,
  ATIVO           VARCHAR2(146) not null,
  QUANTIDADE      NUMBER(15) default 0,
  VALOR           NUMBER(17,2) default 0,
  IDSTATUS        NUMBER not null,
  DATASOLICITACAO DATE not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table TB_GARANTIA
  add constraint TB_GARANTIA_PK primary key (ID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table TB_GARANTIA
  add constraint TB_GATANTIA_TIPO_FK foreign key (IDSTATUS)
  references TB_TIPO (ID);

prompt
prompt Creating table TB_HISTORICO
prompt ===========================
prompt
create table TB_HISTORICO
(
  NM_MODULO VARCHAR2(255) not null,
  DATA      DATE not null,
  DADOS     CLOB
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 4M
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_INTERFACE_EXTRATO
prompt ===================================
prompt
create table TB_INTERFACE_EXTRATO
(
  CD_CLIENTE   NUMBER(7),
  CD_CON_DEP   NUMBER(2),
  CD_CPFCGC    NUMBER(15),
  DS_LINHA     VARCHAR2(1641),
  DT_ANO_MES   NUMBER(7),
  DT_NASC_FUND DATE,
  ID_LINHA     NUMBER(2),
  ID_ORDEM     NUMBER(15)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
create index NU_TB_INTERFACE_EXTRATO on TB_INTERFACE_EXTRATO (ID_ORDEM)
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_IR_POSICAO
prompt ============================
prompt
create table TB_IR_POSICAO
(
  CD_CLIENTE    NUMBER(7),
  CD_CPFCGC     NUMBER(15),
  DT_NASC_FUND  DATE,
  CD_CON_DEP    NUMBER(2),
  ID_LINHA      NUMBER(2),
  CD_NEGOCIO    VARCHAR2(12),
  QT_DISPONIVEL NUMBER(15),
  PR_MEDIO      NUMBER(17,2),
  VL_VOLUME     NUMBER(17,2),
  DT_VENCIMENTO DATE,
  TP_MERCADO    VARCHAR2(4),
  TP_CONTRATO   VARCHAR2(7),
  VL_TAXAS      NUMBER(17,2),
  DT_ANO_MES    NUMBER(7),
  DT_ABERTURA   DATE
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_IR_POSICAO_BMF
prompt ================================
prompt
create table TB_IR_POSICAO_BMF
(
  CD_CLIENTE    NUMBER(7) not null,
  CD_CPFCGC     NUMBER(15) not null,
  DT_NASC_FUND  DATE not null,
  CD_CON_DEP    NUMBER(2) not null,
  ID_LINHA      VARCHAR2(2),
  CD_COMMOD     VARCHAR2(8),
  TP_MERCADO    VARCHAR2(3) not null,
  DT_VENCIMENTO DATE,
  PR_EXERCICIO  NUMBER(12,2),
  QT_ATUAL      NUMBER(15),
  IN_NATATU     CHAR(1),
  DT_ANO_MES    NUMBER(7)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_IR_POSICAO_OURO
prompt =================================
prompt
create table TB_IR_POSICAO_OURO
(
  CD_CLIENTE      NUMBER(7) not null,
  CD_CPFCGC       NUMBER(15) not null,
  DT_NASC_FUND    DATE not null,
  CD_CON_DEP      NUMBER(2) not null,
  ID_LINHA        CHAR(2),
  QT_DIA_ATU      NUMBER(12,4) not null,
  QT_DIA_ATU_CONT NUMBER,
  VL_COTMENOR     NUMBER,
  VL_VOLUME       NUMBER,
  DT_ANO_MES      NUMBER(7)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_IR_POSICAO_SWP_FLX
prompt ====================================
prompt
create table TB_IR_POSICAO_SWP_FLX
(
  CD_CLIENTE   NUMBER(7) not null,
  CD_CPFCGC    NUMBER(15) not null,
  DT_NASC_FUND DATE not null,
  CD_CON_DEP   NUMBER(2) not null,
  ID_LINHA     CHAR(2),
  CD_COMMOD    VARCHAR2(3) not null,
  NUMBMF       NUMBER(12) not null,
  TAMBAS       NUMBER,
  DATVCT       DATE,
  DATREG       DATE,
  VOL          NUMBER,
  DT_ANO_MES   NUMBER(7)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_IR_PROVENTOS_PAGOS
prompt ====================================
prompt
create table TB_IR_PROVENTOS_PAGOS
(
  CD_CLIENTE      NUMBER(7) not null,
  CD_CPFCGC       NUMBER(15) not null,
  DT_NASC_FUND    DATE not null,
  CD_CON_DEP      NUMBER(2) not null,
  ID_LINHA        CHAR(2),
  CD_NEGOCIO      VARCHAR2(12),
  DS_PROVENTO     VARCHAR2(60),
  QT_PROVENTO     NUMBER(19,4),
  VL_PROVENTO     NUMBER(18,2),
  PC_IR_DIVIDENDO NUMBER(7,4),
  VL_IR_DIVIDENDO NUMBER,
  VL_PROVENTO_LIQ NUMBER,
  DT_PAGAMENTO    DATE,
  DT_ANO_MES      NUMBER(7)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_IR_SALDO
prompt ==========================
prompt
create table TB_IR_SALDO
(
  CD_CLIENTE    NUMBER(7) not null,
  CD_CPFCGC     NUMBER(15) not null,
  DT_NASC_FUND  DATE not null,
  CD_CON_DEP    NUMBER(2) not null,
  ID_LINHA      CHAR(2),
  VL_DISPONIVEL NUMBER(15,2) not null,
  DT_ANO_MES    NUMBER(7)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_LISTA_ASSESSORES
prompt ==================================
prompt
create global temporary table TB_LISTA_ASSESSORES
(
  CD_ASSESSOR NUMBER(5)
)
on commit preserve rows;

prompt
prompt Creating table TB_LISTA_CLIENTES
prompt ================================
prompt
create global temporary table TB_LISTA_CLIENTES
(
  CD_CLIENTE NUMBER(7) not null
)
on commit preserve rows;
alter table TB_LISTA_CLIENTES
  add constraint PK_TB_LISTA_CLIENTES primary key (CD_CLIENTE);

prompt
prompt Creating table TB_LISTA_CONTRATOS
prompt =================================
prompt
create global temporary table TB_LISTA_CONTRATOS
(
  NR_CONTRATO NUMBER(9)
)
on commit preserve rows;

prompt
prompt Creating table TB_OPERADORES
prompt ============================
prompt
create table TB_OPERADORES
(
  CD_OPERADOR    NUMBER(5) not null,
  NM_OPERADOR    VARCHAR2(100) not null,
  CD_OPERA_MEGA  NUMBER(5) not null,
  TP_CLIENTE     NUMBER(1) not null,
  CD_OPERA_MEGA2 NUMBER(5)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table TB_OPERADORES
  add constraint PK_TB_OPERADORES primary key (CD_OPERADOR)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_MESA_OPERACAO_OPERADORES
prompt ==========================================
prompt
create table TB_MESA_OPERACAO_OPERADORES
(
  CD_MESA_OPERACAO  NUMBER(5) not null,
  CD_OPERADOR       NUMBER(5) not null,
  CD_MESA_OPERACAO2 NUMBER(5)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table TB_MESA_OPERACAO_OPERADORES
  add constraint PK_TB_MESA_OPERACAO_OPERADORES primary key (CD_MESA_OPERACAO, CD_OPERADOR)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table TB_MESA_OPERACAO_OPERADORES
  add constraint FKTB_OPERADORETB_MS_PRC_PRDRES foreign key (CD_OPERADOR)
  references TB_OPERADORES (CD_OPERADOR);

prompt
prompt Creating table TB_MOCK_FAIXA_BMF
prompt ================================
prompt
create table TB_MOCK_FAIXA_BMF
(
  CD_ATIVO      VARCHAR2(7),
  CD_CONTRATO   NUMBER,
  CD_MERCADO    VARCHAR2(3),
  DT_VENCIMENTO DATE,
  CD_FAIXA      VARCHAR2(20),
  PC_NORMAL     NUMBER,
  PC_DAY_TRADE  NUMBER
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_MODELO_EMAIL
prompt ==============================
prompt
create table TB_MODELO_EMAIL
(
  ID_MODELO_EMAIL   NUMBER not null,
  ID_FUNCIONALIDADE NUMBER not null,
  REMETENTE         VARCHAR2(100) not null,
  EMAIL_REPLY       VARCHAR2(100) not null,
  ASSUNTO           VARCHAR2(150) not null,
  CONTEUDO          CLOB,
  CD_ASSESSOR       NUMBER
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
comment on column TB_MODELO_EMAIL.CD_ASSESSOR
  is 'Nulo se modelo � padr�o para todos os assessores.';
alter table TB_MODELO_EMAIL
  add constraint PK_MODELO_EMAIL primary key (ID_MODELO_EMAIL)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table TB_MODELO_EMAIL
  add constraint FK_MODELO_FUNCIONALIDADE foreign key (ID_FUNCIONALIDADE)
  references TB_CF_FUNCIONALIDADE (ID_FUNCIONALIDADE);

prompt
prompt Creating table TB_NOTIFICACAO_OPERACOES
prompt =======================================
prompt
create table TB_NOTIFICACAO_OPERACOES
(
  DT_NEGOCIO DATE not null,
  CD_CLIENTE NUMBER(7) not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table TB_NOTIFICACAO_OPERACOES
  add constraint PK_NOTIFICACAO_OPERACOES primary key (DT_NEGOCIO, CD_CLIENTE)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_PRT_CADASTRO_SUBSCRICAO
prompt =========================================
prompt
create table TB_PRT_CADASTRO_SUBSCRICAO
(
  ID_SUBSCRICAO      NUMBER(9) not null,
  DS_EMPRESA         VARCHAR2(100),
  CD_ISIN_PAPEL      VARCHAR2(12) not null,
  CD_ISIN_SUBSCRICAO VARCHAR2(12) not null,
  VL_DIREITO         NUMBER(17,2),
  DT_INICIO          DATE,
  DT_COM             DATE,
  DT_CORRETORA       DATE,
  HH_CORRETORA       VARCHAR2(8),
  DT_BOLSA           DATE,
  DT_EMPRESA         DATE,
  IN_STATUS          CHAR(1) default 'P',
  DS_DESCRICAO       VARCHAR2(500),
  DS_ASSUNTO_EMAIL   VARCHAR2(100),
  DS_CORPO_EMAIL     VARCHAR2(1000),
  DS_PROSPECTO       BLOB,
  PC_DIREITO         NUMBER(13,10)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
comment on column TB_PRT_CADASTRO_SUBSCRICAO.IN_STATUS
  is '"P" Para aprovar "A" Aprovada "S" Subscrevendo "F" finalizado "R" Reprovado';
alter table TB_PRT_CADASTRO_SUBSCRICAO
  add constraint PK_TB_PRT_CADASTRO_SUBSCRICAO primary key (ID_SUBSCRICAO, CD_ISIN_PAPEL, CD_ISIN_SUBSCRICAO)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_PRT_CLIENTE_SUBSCRICAO
prompt ========================================
prompt
create table TB_PRT_CLIENTE_SUBSCRICAO
(
  CD_CLIENTE           NUMBER(7) not null,
  ID_SUBSCRICAO        NUMBER(9) not null,
  QT_SOLICITADA        NUMBER(15),
  IN_SOBRAS            CHAR(1),
  IN_STATUS            CHAR(1) not null,
  IN_AVISO             CHAR(1) not null,
  DT_SOLICITACAO       DATE,
  CD_NEGOCIO_DIR       VARCHAR2(12),
  NM_ESPECIFICACAO_DIR VARCHAR2(10),
  ID_SUBS_SOLICI       NUMBER
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table TB_PRT_CLIENTE_SUBSCRICAO
  add constraint PK_TB_CLIENTE_SUBSCRICAO primary key (CD_CLIENTE, ID_SUBSCRICAO, IN_STATUS, IN_AVISO)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_PRT_DESCONTO
prompt ==============================
prompt
create table TB_PRT_DESCONTO
(
  ID_SOLICITACAO NUMBER(9) not null,
  CD_CLIENTE     NUMBER(7) not null,
  CD_BOLSA       NUMBER(1),
  CD_MERCADO     VARCHAR2(3),
  IN_NORMAL      VARCHAR2(20),
  IN_VIGENCIA    CHAR(1),
  DT_VIGENCIA    DATE,
  IN_TODOS       CHAR(1),
  CD_NEGOCIO     VARCHAR2(12),
  QT_QTDESP      NUMBER(15),
  PC_DESCONTO    NUMBER(12,8) default 0,
  VL_DESCONTO    NUMBER(17,2) default 0,
  DS_OBSERVACAO  VARCHAR2(200),
  IN_STATUS      CHAR(1),
  DT_SOLICITACAO DATE
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
comment on column TB_PRT_DESCONTO.ID_SOLICITACAO
  is 'Identificador da solicita��o';
comment on column TB_PRT_DESCONTO.CD_CLIENTE
  is 'C�digo de Cliente';
comment on column TB_PRT_DESCONTO.CD_BOLSA
  is 'C�digo da Bolsa 1 - Bovespa 2 - BMF';
comment on column TB_PRT_DESCONTO.CD_MERCADO
  is 'C�digo de Mercado';
comment on column TB_PRT_DESCONTO.IN_NORMAL
  is 'Indica se � para opera��o normal "S" ou Day Trade "N"';
comment on column TB_PRT_DESCONTO.IN_VIGENCIA
  is 'Indica o prazo de Vig�ncia "S" D0, "N" D1 e "P" permanente';
comment on column TB_PRT_DESCONTO.DT_VIGENCIA
  is 'Data de vig�ncia';
comment on column TB_PRT_DESCONTO.IN_TODOS
  is 'Desconto para todos as opera��es ';
comment on column TB_PRT_DESCONTO.CD_NEGOCIO
  is 'C�digo de negocio';
comment on column TB_PRT_DESCONTO.QT_QTDESP
  is 'Quantidade de neg�cio';
comment on column TB_PRT_DESCONTO.PC_DESCONTO
  is '% de desconto solicitado';
comment on column TB_PRT_DESCONTO.VL_DESCONTO
  is 'Valor do desconto solicitado';
comment on column TB_PRT_DESCONTO.DS_OBSERVACAO
  is 'Observa��o';
comment on column TB_PRT_DESCONTO.IN_STATUS
  is 'Status da solicita��o ''A'',''Aprovar'', ''F'', ''Para Efetuar'', ''E'', ''Efetuado'' e ''R'', ''Reprovado''';
comment on column TB_PRT_DESCONTO.DT_SOLICITACAO
  is 'Data da solicitacao';
alter table TB_PRT_DESCONTO
  add constraint PK_TB_PRT_DESCONTO primary key (ID_SOLICITACAO, CD_CLIENTE)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_PRT_NOTIFICACAO_SUBSCRICAO
prompt ============================================
prompt
create table TB_PRT_NOTIFICACAO_SUBSCRICAO
(
  CD_CLIENTE    NUMBER(7) not null,
  ID_SUBSCRICAO NUMBER(9) not null,
  IN_AVISO      CHAR(1)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table TB_PRT_NOTIFICACAO_SUBSCRICAO
  add constraint PK_INFORMACAO_SUBSCRICAO primary key (CD_CLIENTE, ID_SUBSCRICAO)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_PRT_REPASSE_ENVIADO
prompt =====================================
prompt
create table TB_PRT_REPASSE_ENVIADO
(
  NR_VINCULO   NUMBER(8) not null,
  CD_CORRETORA NUMBER(6) not null,
  CD_CLIENTE   NUMBER(7) not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table TB_PRT_REPASSE_ENVIADO
  add constraint PK_TB_PRT_REPASSE_ENVIADO primary key (NR_VINCULO, CD_CORRETORA, CD_CLIENTE)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table TB_PRT_REPASSE_ENVIADO
  add constraint UNQ_TB_PRT_REPASSE unique (CD_CORRETORA, NR_VINCULO)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_PRT_RESGATE
prompt =============================
prompt
create table TB_PRT_RESGATE
(
  ID_SOLICITACAO NUMBER(9) not null,
  CD_CLIENTE     NUMBER(7) not null,
  VL_RESGATE     NUMBER(17,2),
  DT_SOLICITACAO DATE,
  CD_BANCO       VARCHAR2(3),
  CD_AGENCIA     VARCHAR2(5),
  DV_AGENCIA     VARCHAR2(2),
  NR_CONTA       VARCHAR2(13),
  DV_CONTA       VARCHAR2(2),
  TP_CONTA       VARCHAR2(3),
  IN_SITUACAO    NUMBER,
  DT_LIQUIDACAO  DATE,
  DS_OBSERVACAO  VARCHAR2(60)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table TB_PRT_RESGATE
  add constraint PK_TB_PRT_RESGATE primary key (ID_SOLICITACAO, CD_CLIENTE)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table TB_PRT_RESGATE
  add constraint FK_PRT_RESGATE_IN_SITUACAO_FK foreign key (IN_SITUACAO)
  references TB_TIPO (ID);

prompt
prompt Creating table TB_PRT_TRANSFERENCIA
prompt ===================================
prompt
create table TB_PRT_TRANSFERENCIA
(
  ID_SOLICITACAO       NUMBER(9) not null,
  CD_CLIENTE           NUMBER(7),
  CD_NEGOCIO           VARCHAR2(12),
  CD_CARTEIRA_ORIGEM   NUMBER(5),
  CD_CORRETORA_DESTINO NUMBER(3) default 21,
  CD_CARTEIRA_DESTINO  NUMBER(5),
  DT_SOLICITACAO       DATE,
  CD_CLIENTE_DESTINO   NUMBER(7),
  PU_POSICAO           NUMBER(15,5),
  QT_TRASFERIR         NUMBER(15),
  IN_STATUS            CHAR(1) default 'A',
  DS_OBSERVACAO        VARCHAR2(200)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
comment on column TB_PRT_TRANSFERENCIA.ID_SOLICITACAO
  is 'Id da Solicita��o';
comment on column TB_PRT_TRANSFERENCIA.CD_CLIENTE
  is 'C�digo de Cliente';
comment on column TB_PRT_TRANSFERENCIA.CD_NEGOCIO
  is 'C�digo de Neg�cio';
comment on column TB_PRT_TRANSFERENCIA.CD_CARTEIRA_ORIGEM
  is 'Carteira de Origem';
comment on column TB_PRT_TRANSFERENCIA.CD_CORRETORA_DESTINO
  is 'Corretora de destino';
comment on column TB_PRT_TRANSFERENCIA.CD_CARTEIRA_DESTINO
  is 'Carteira de destino';
comment on column TB_PRT_TRANSFERENCIA.DT_SOLICITACAO
  is 'Data de solicita��o';
comment on column TB_PRT_TRANSFERENCIA.CD_CLIENTE_DESTINO
  is 'C�digo de Cliente na corretora de destino';
comment on column TB_PRT_TRANSFERENCIA.PU_POSICAO
  is 'Pu posi��o a ser trasferida';
comment on column TB_PRT_TRANSFERENCIA.QT_TRASFERIR
  is 'Quantidade a transferir';
comment on column TB_PRT_TRANSFERENCIA.IN_STATUS
  is 'A - Para Efetuar, E - Efetuada, T - Transferencia Extrerna.';
alter table TB_PRT_TRANSFERENCIA
  add constraint PK_TB_PRT_TRANSFERENCIA primary key (ID_SOLICITACAO)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_PTAX
prompt ======================
prompt
create table TB_PTAX
(
  DT_REFERENCIA DATE not null,
  VL_PTAX       NUMBER(9,6)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
comment on table TB_PTAX
  is 'Cont�m o valor da taxa PTAX';
comment on column TB_PTAX.DT_REFERENCIA
  is 'Data de referencia da taxa';
comment on column TB_PTAX.VL_PTAX
  is 'Valor da taxa';
alter table TB_PTAX
  add constraint PK_TB_PTAX primary key (DT_REFERENCIA)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_RESULTADO_REPASSE
prompt ===================================
prompt
create table TB_RESULTADO_REPASSE
(
  CD_CLIENTE            NUMBER(7) not null,
  NR_NEGOCIO            NUMBER(9) not null,
  DT_NEGOCIO            DATE not null,
  DT_LIQUIDACAO         DATE,
  CD_ISIN               VARCHAR2(12),
  CD_NEGOCIO            VARCHAR2(12) not null,
  NM_NOMRES             VARCHAR2(12),
  QT_QTDESP             NUMBER(16,4),
  VL_CORTOT             NUMBER(17,2),
  VL_REPASSE            NUMBER(17,2),
  PC_REPASSE            NUMBER(6,3),
  VL_CONVERTIDO_PTAX    NUMBER(17,2),
  VL_NEGOCIO_COMPRA     NUMBER(17,2),
  VL_NEGOCIO_VENDA      NUMBER(17,2),
  VL_LIQUIDO_COMPRA     NUMBER(17,2),
  VL_LIQUIDO_VENDA      NUMBER(17,2),
  IN_LIQUIDADO          CHAR(1) default 'N',
  DT_LIQUIDACAO_REPASSE DATE,
  NM_USUARIO_LIQUIDOU   VARCHAR2(60)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 128K
    minextents 1
    maxextents unlimited
  );
comment on table TB_RESULTADO_REPASSE
  is 'Armazena o resultado da apuracao do repasse';
comment on column TB_RESULTADO_REPASSE.CD_CLIENTE
  is 'COdigo de cliente no Sinacor';
comment on column TB_RESULTADO_REPASSE.NR_NEGOCIO
  is 'Numero do negOcio';
comment on column TB_RESULTADO_REPASSE.DT_NEGOCIO
  is 'Data do negocio';
comment on column TB_RESULTADO_REPASSE.DT_LIQUIDACAO
  is 'Data de liquidacao';
comment on column TB_RESULTADO_REPASSE.CD_ISIN
  is 'Codigo Isin';
comment on column TB_RESULTADO_REPASSE.CD_NEGOCIO
  is 'Codigo de negociacaoo';
comment on column TB_RESULTADO_REPASSE.NM_NOMRES
  is 'Nome da empresa';
comment on column TB_RESULTADO_REPASSE.QT_QTDESP
  is 'Quantidade do negocio';
comment on column TB_RESULTADO_REPASSE.VL_CORTOT
  is 'Valor da corretagem ';
comment on column TB_RESULTADO_REPASSE.VL_REPASSE
  is 'Valor do repasse apurado';
comment on column TB_RESULTADO_REPASSE.PC_REPASSE
  is 'Armazena o percentual de repasse utilizado para clculo';
comment on column TB_RESULTADO_REPASSE.VL_CONVERTIDO_PTAX
  is 'Valor do repasse convertido pelo PTAX';
comment on column TB_RESULTADO_REPASSE.VL_NEGOCIO_COMPRA
  is 'Valor do neg�cio de compra';
comment on column TB_RESULTADO_REPASSE.VL_NEGOCIO_VENDA
  is 'Valor do neg�cio de venda';
comment on column TB_RESULTADO_REPASSE.VL_LIQUIDO_COMPRA
  is 'Valor l�quido da compra';
comment on column TB_RESULTADO_REPASSE.VL_LIQUIDO_VENDA
  is 'Valor l�quido da venda';
comment on column TB_RESULTADO_REPASSE.IN_LIQUIDADO
  is 'Indica se o repasse foi liquidado';
comment on column TB_RESULTADO_REPASSE.DT_LIQUIDACAO_REPASSE
  is 'Data que foi efetivada a liquidacao pelo sistema';
comment on column TB_RESULTADO_REPASSE.NM_USUARIO_LIQUIDOU
  is 'Usu�rio que realizou a liquidacaoo';
alter table TB_RESULTADO_REPASSE
  add constraint PK_TB_RESULTADO_REPASSE primary key (NR_NEGOCIO, DT_NEGOCIO, CD_NEGOCIO, CD_CLIENTE)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_RETORNO_PENDENTES
prompt ===================================
prompt
create table TB_RETORNO_PENDENTES
(
  NOME        VARCHAR2(150),
  APELIDO     VARCHAR2(50),
  LINK        VARCHAR2(150),
  STATUSID    VARCHAR2(3),
  NOME_STATUS VARCHAR2(50),
  CONTADOR    NUMBER
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_STOCK_RECORD
prompt ==============================
prompt
create table TB_STOCK_RECORD
(
  CD_CLIENTE        NUMBER(7) not null,
  NR_NEGOCIO        NUMBER(9) not null,
  CD_NEGOCIO        VARCHAR2(12) not null,
  CD_NATOPE         CHAR(1) not null,
  DT_NEGOCIO        DATE not null,
  DT_LIQUIDACAO     DATE,
  QT_NEGOCIO_COMPRA NUMBER(15),
  QT_NEGOCIO_VENDA  NUMBER(15),
  NM_EMPRESA        VARCHAR2(12),
  DT_GERACAO        DATE not null,
  DT_ANO_MES        NUMBER(6) not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 3M
    minextents 1
    maxextents unlimited
  );
comment on table TB_STOCK_RECORD
  is 'Armazena os dados gerados para o relat�rio Stock Record';
comment on column TB_STOCK_RECORD.CD_CLIENTE
  is 'C�digo de Cliente';
comment on column TB_STOCK_RECORD.NR_NEGOCIO
  is 'N�mero do Neg�cio';
comment on column TB_STOCK_RECORD.CD_NEGOCIO
  is 'C�digo do Neg�cio';
comment on column TB_STOCK_RECORD.CD_NATOPE
  is 'Natureza da opera��o';
comment on column TB_STOCK_RECORD.DT_NEGOCIO
  is 'Data do neg�cio';
comment on column TB_STOCK_RECORD.DT_LIQUIDACAO
  is 'Data de liquida��o';
comment on column TB_STOCK_RECORD.QT_NEGOCIO_COMPRA
  is 'Quantidade de compra';
comment on column TB_STOCK_RECORD.QT_NEGOCIO_VENDA
  is 'Quantidade de venda';
comment on column TB_STOCK_RECORD.NM_EMPRESA
  is 'Nome da empresa';
comment on column TB_STOCK_RECORD.DT_GERACAO
  is 'Data de gera��o do relat�rio';
comment on column TB_STOCK_RECORD.DT_ANO_MES
  is 'M�s e ano de refer�ncia';
alter table TB_STOCK_RECORD
  add constraint PK_TB_STOCK_RECORD primary key (CD_CLIENTE, NR_NEGOCIO, CD_NEGOCIO, CD_NATOPE, DT_NEGOCIO, DT_GERACAO, DT_ANO_MES)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 4M
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_TAG_EMAIL
prompt ===========================
prompt
create table TB_TAG_EMAIL
(
  ID_TAG         NUMBER not null,
  NM_TAG         VARCHAR2(30) not null,
  NM_DESCRIPTION VARCHAR2(30)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table TB_TAG_EMAIL
  add constraint PK_TAG primary key (ID_TAG)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_TERMO
prompt =======================
prompt
create table TB_TERMO
(
  ID                 NUMBER not null,
  CDCLIENTE          NUMBER(7) not null,
  CDNEGOCIO          VARCHAR2(12) not null,
  EMPRESA            VARCHAR2(12) not null,
  NUMEROCONTRATO     NUMBER(9) not null,
  QUANTIDADETOTAL    NUMBER(15) not null,
  DATALIQUIDACAO     DATE not null,
  IDTIPO             NUMBER not null,
  IDSTATUS           NUMBER not null,
  DATASOLICITACAO    DATE not null,
  QUANTIDADELIQUIDAR NUMBER(15) not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table TB_TERMO
  add constraint TB_TERMO_ID_PK primary key (ID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table TB_TERMO
  add constraint TB_TIPO_IDSTATUS_FK foreign key (IDSTATUS)
  references TB_TIPO (ID);
alter table TB_TERMO
  add constraint TB_TIPO_IDTIPO_FK foreign key (IDTIPO)
  references TB_TIPO (ID);

prompt
prompt Creating table TB_TIPO_GARANTIA
prompt ===============================
prompt
create table TB_TIPO_GARANTIA
(
  TIPO_GARANTIA VARCHAR2(40) not null,
  QUANTIDADE    NUMBER(1) not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_TIPO_RELATORIO
prompt ================================
prompt
create table TB_TIPO_RELATORIO
(
  ID_RELATORIO NUMBER(3) not null,
  DS_RELATORIO VARCHAR2(30)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
comment on table TB_TIPO_RELATORIO
  is 'Tipos de relat�rios';
comment on column TB_TIPO_RELATORIO.ID_RELATORIO
  is 'Identificador do relat�rio';
comment on column TB_TIPO_RELATORIO.DS_RELATORIO
  is 'Descri��o do relat�rio';
alter table TB_TIPO_RELATORIO
  add constraint PK_TB_TIPO_RELATORIO primary key (ID_RELATORIO)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table TB_TRADE_BLOTTER
prompt ===============================
prompt
create table TB_TRADE_BLOTTER
(
  CD_CLIENTE        NUMBER(7) not null,
  CD_NATOPE         CHAR(1) not null,
  NR_NEGOCIO        NUMBER(9) not null,
  DT_NEGOCIO        DATE not null,
  DT_LIQUIDACAO     DATE not null,
  CD_ISIN           VARCHAR2(12) not null,
  NM_EMPRESA        VARCHAR2(12),
  QT_NEGOCIADA      NUMBER(15),
  VL_NEGOCIO        NUMBER(17,2),
  VL_VOLUME_COMPRA  NUMBER(17,2),
  VL_LIQUIDO_COMPRA NUMBER(17,2),
  VL_VOLUME_VENDA   NUMBER(17,2),
  VL_LIQUIDO_VENDA  NUMBER(17,2),
  NM_MOEDA          VARCHAR2(3),
  DT_GERACAO        DATE not null,
  CD_NEGOCIO        VARCHAR2(12)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 320K
    minextents 1
    maxextents unlimited
  );
comment on table TB_TRADE_BLOTTER
  is 'Armazena os relat�rios Trade Blotter';
comment on column TB_TRADE_BLOTTER.CD_CLIENTE
  is 'C�digo do Cliente';
comment on column TB_TRADE_BLOTTER.CD_NATOPE
  is 'Natureza da Opera��o''';
comment on column TB_TRADE_BLOTTER.NR_NEGOCIO
  is 'N�mero do neg�cio';
comment on column TB_TRADE_BLOTTER.DT_NEGOCIO
  is 'Data do neg�cio';
comment on column TB_TRADE_BLOTTER.DT_LIQUIDACAO
  is 'Data de liquida��o';
comment on column TB_TRADE_BLOTTER.CD_ISIN
  is 'C�digo Isin';
comment on column TB_TRADE_BLOTTER.NM_EMPRESA
  is 'Nome da Empresa';
comment on column TB_TRADE_BLOTTER.QT_NEGOCIADA
  is 'Quantidade negociada';
comment on column TB_TRADE_BLOTTER.VL_NEGOCIO
  is 'Valor do neg�cio';
comment on column TB_TRADE_BLOTTER.VL_VOLUME_COMPRA
  is 'Valor Volume Compra';
comment on column TB_TRADE_BLOTTER.VL_LIQUIDO_COMPRA
  is 'Valor Liquido da Compra';
comment on column TB_TRADE_BLOTTER.VL_VOLUME_VENDA
  is 'Valor Volume Venda';
comment on column TB_TRADE_BLOTTER.VL_LIQUIDO_VENDA
  is 'Valor Liquido Venda';
comment on column TB_TRADE_BLOTTER.NM_MOEDA
  is 'Nome da moeda';
comment on column TB_TRADE_BLOTTER.DT_GERACAO
  is 'Data de gera��o do relat�rio';
alter table TB_TRADE_BLOTTER
  add constraint PK_TB_TRADE_BLOTTER primary key (NR_NEGOCIO, DT_NEGOCIO, DT_LIQUIDACAO, CD_ISIN, DT_GERACAO, CD_CLIENTE, CD_NATOPE)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 384K
    minextents 1
    maxextents unlimited
  );


spool off
