
-- D, H ou P
:setvar Ambiente H
-- Nome dos databases. Pode mudar em outros ambientes
:setvar db_eGuardian  CRKeGUARDIAN
:setvar db_CRKCOE CRK_COE
:setvar db_Segregada Segregada
:setvar db_Change CHANGE_BRP_H_001




use SUITABILITY_BRP_$(Ambiente)_001
go

alter PROC [dbo].[PR_VERIFICAR_DESENQUADRAMENTO]
	@CD_CPFCNPJ DECIMAL(20,0),
	@PRODUTOS T_PRODUTO READONLY,
	@DT_INICIO SMALLDATETIME,
	@DT_FIM SMALLDATETIME
 AS
	-- Integra��o CRK (Renda Fixa e SWAP)
	-- Aplica��o em PROGRESS
	select b.cd_produto, a.dt_movimenta as dt_operacao
	from $(db_eGuardian).[dbo].[CRK_INTEG_MOVFIN] a
	join @produtos b on (B.CD_PRODUTO = 'CRKSWP' AND a.CD_IDENTIFICACAO LIKE  'CRKSWP%') OR (B.CD_PRODUTO = 'CRK_CRI' AND A.CD_PRODUTO LIKE  ' 09 %')
	where a.CD_CLIENTE = @CD_CPFCNPJ
	and a.dt_movimenta BETWEEN coalesce( @DT_INICIO, '1900-01-01') AND coalesce( @DT_FIM, getdate())
	GROUP BY b.cd_produto, a.dt_movimenta  


	UNION ALL
	--Os tipos de derivativos estruturados est�o nesta tabela:
	--	select * from COE_DOM_TP_ESTRUTURA
	
	select b.cd_produto, a.dt_emissao  as dt_operacao
	from $(db_CRKCOE)..COE_VW_OPERACOES a
	join @produtos b on (b.cd_produto = 'CRKCOE_PROT' AND DS_MODALIDADE = 'Protegido') OR (B.CD_PRODUTO = 'CRKCOE_RISCO' AND DS_MODALIDADE = 'Risco')
	INNER JOIN $(db_CRKCOE)..CRK_VW_COR_CLIENTE   C			(NOLOCK) ON C.ID_CLIENTE			= a.ID_CLIENTE
	where c.ds_cnpj_cpf = @cd_cpfcnpj	
	and a.dt_emissao BETWEEN coalesce( @DT_INICIO, '1900-01-01') AND coalesce( @DT_FIM, getdate())
	GROUP BY b.cd_produto, a.dt_emissao  
	
	
	UNION ALL
	-- Op��es Flex�veis. � poss�vel separar as opera��es por tipo (Compra / Venda etc) usando o switch que existe dentro da view pelo tipo de contrato.
	select b.cd_produto, a.dt_movimenta    as dt_operacao
	from $(db_Segregada)..OPF_VIEW_INTEG_MOVFIN_AUX A
	join @produtos b on b.cd_produto = 'OPF'
	where a.cd_cliente = @cd_cpfcnpj
	and a.dt_movimenta BETWEEN coalesce( @DT_INICIO, '1900-01-01') AND coalesce( @DT_FIM, getdate())
	GROUP BY b.cd_produto, a.dt_movimenta  	
	
	UNION ALL
	
	select b.cd_produto, a.dt_movimenta   	 as dt_operacao
	from CRK_BMF..BMF_VIEW_INTEG_MOVFIN_AUX  a
	join @produtos b on b.cd_produto = 'FUT'
	where a.cd_cliente = @cd_cpfcnpj
	and a.dt_movimenta BETWEEN coalesce( @DT_INICIO, '1900-01-01') AND coalesce( @DT_FIM, getdate())
	GROUP BY b.cd_produto, a.dt_movimenta  	
	
	UNION ALL
	
	select b.cd_produto, a.dt_movimenta   	 as dt_operacao
	from $(db_Change).dbo.VIEW_INTEG_MOVFIN_AUX  a
	join @produtos b on b.cd_produto = 'CHG'
	where a.cd_cliente = @cd_cpfcnpj	
	and a.dt_movimenta BETWEEN coalesce( @DT_INICIO, '1900-01-01') AND coalesce( @DT_FIM, getdate())
	and a.CPF_CNPJ <> 0
	and a.CD_PRODUTO not in ('Ban-Venda','Ban-Compra')
	GROUP BY b.cd_produto, a.dt_movimenta  	
	
	UNION ALL
	
	select b.cd_produto, a.dt_movimenta   	 as dt_operacao
	from $(db_Change).dbo.VIEW_INTEG_MOVFIN_ME_AUX  a
	join @produtos b on b.cd_produto = 'CHG_ME'
	where a.cd_cliente = @cd_cpfcnpj
	and a.dt_movimenta BETWEEN coalesce( @DT_INICIO, '1900-01-01') AND coalesce( @DT_FIM, getdate())
	and a.CD_PRODUTO not in ('Ban-Venda','Ban-Compra')
	GROUP BY b.cd_produto, a.dt_movimenta  	
GO
declare @t T_PRODUTO

-- C�digos atualmente suportados pela procedure.

insert into @t select 'CRK_CRI'			-- Renda Fixa (M�dio)
insert into @t select 'CRK_CRA'			-- Renda Fixa (M�dio)
insert into @t select 'CRKSWP'			-- SWAP (M�dio)
insert into @t select 'CRKCOE_PROT'		-- Derivativos Estruturados Protegidos (M�dio)
insert into @t select 'CRKCOE_RISCO'	-- Derivativos Estruturados em Risco (Alto)
insert into @t select 'OPF'				-- Op��es Flex�veis (M�dio)
insert into @t select 'FUT'				-- BM&F (M�dio)
insert into @t select 'CHG'				-- CAMBIO
insert into @t select 'CHG_ME'			-- CAMBIO

exec [dbo].[PR_VERIFICAR_DESENQUADRAMENTO] /*1444583140*/13220493000117, @t, null, null

