CREATE PROCEDURE PR_CLIENTE_ONLINE_INSERIR_ASSINATURA(
    @idClienteOnline INT,
    @nome            VARCHAR(200),
    @extensao        VARCHAR(200),
    @data            VARBINARY(MAX)
)
AS
BEGIN
    INSERT INTO TB_CLIENTE_ONLINE_ASSINATURA VALUES (@idClienteOnline, @nome, @extensao, @data)
END