CREATE PROCEDURE PR_DOCUMENTO_INSERIR
(
    @idDoc              INT NULL,
    @descricaoArquivo   VARCHAR(200),
    @nomeArquivo        VARCHAR(200),
    @instrucao          VARCHAR(300) NULL,
    @obrigatorio        BIT,
    @idDocValidade      INT,
    @dataValidade       DATE,
    @pessoaFisica       BIT NULL,
    @pessoaJuridica     BIT NULL,
    @fundos             BIT NULL,
    @extensao           VARCHAR(200),
    @arquivo            VARBINARY(MAX),
    @status             INT
)
AS
BEGIN

    DECLARE @id INT

    INSERT INTO 
        TB_DOCUMENTO 
    VALUES 
        (
            @nomeArquivo, @instrucao, @obrigatorio, @idDocValidade, @dataValidade, @pessoaFisica, 
            @pessoaJuridica, @fundos, @descricaoArquivo, @extensao, @arquivo, @status
        )

    SET @id = CAST(SCOPE_IDENTITY() AS INT)
    RETURN @id
END
GO


CREATE PROCEDURE PR_DOCUMENTO_INSERIR_PRODUTO
(
    @idDocumento    INT,
    @idProduto      INT
)
AS
BEGIN
    INSERT INTO TB_DOCUMENTO_PRODUTO VALUES (@idDocumento, @idProduto)
END
GO


CREATE PROCEDURE PR_DOCUMENTO_ATUALIZAR
(
    @idDoc              INT NULL,
    @descricaoArquivo   VARCHAR(200),
    @nomeArquivo        VARCHAR(200),
    @instrucao          VARCHAR(300) NULL,
    @obrigatorio        BIT,
    @idDocValidade      INT,
    @dataValidade       DATE,
    @pessoaFisica       BIT NULL,
    @pessoaJuridica     BIT NULL,
    @fundos             BIT NULL,
    @extensao           VARCHAR(200),
    @arquivo            VARBINARY(MAX),
    @status             INT
)
AS
BEGIN
    UPDATE 
        TB_DOCUMENTO 
    SET 
        NOME_ARQUIVO = @nomeArquivo, INSTRUCAO_USO = @instrucao, ID_DOCUMENTO_VALIDADE = @idDocValidade,
        OBRIGATORIO = @obrigatorio, DATA_VALIDADE = @datavalidade, PESSOA_FISICA = @pessoaFisica,
        PESSOA_JURIDICA = @pessoaJuridica, FUNDOS = @fundos, DESCRICAO_ARQUIVO = @descricaoArquivo,
        EXTENSAO = @extensao, ARQUIVO = @arquivo, STATUS = @status
    WHERE
        ID = @idDoc
END
GO


CREATE PROCEDURE PR_DOCUMENTO_EXCLUIR
(
    @idDocumento    INT
)
AS
BEGIN
    DELETE TB_DOCUMENTO_PRODUTO WHERE ID_DOCUMENTO = @idDocumento
END
GO 


CREATE PROCEDURE PR_DOCUMENTO_OBTER
(
    @idDocumento INT
)
AS
BEGIN
    SELECT * FROM TB_DOCUMENTO WHERE ID = @idDocumento
END
GO


CREATE PROCEDURE PR_DOCUMENTO_PRODUTO_OBTER
(
    @idDocumento INT
)
AS
BEGIN
    SELECT ID_PRODUTO FROM TB_DOCUMENTO_PRODUTO WHERE ID_DOCUMENTO = @idDocumento
END
GO


CREATE PROCEDURE PR_DOCUMENTO_LISTAR
AS
BEGIN
    SELECT * FROM TB_DOCUMENTO WHERE [STATUS] = 1
END
GO