CREATE PROCEDURE [dbo].[PR_CLIENTE_ONLINE_INSERIR_PODER](
    @idClienteOnline        INT,
    @idDocumento            INT,
    @idDocumentType         INT,
    @inicioValidade         DATE = NULL,
    @fimValidade            DATE = NULL,
    @idRepresentante        INT,
    @numeroDocumento        VARCHAR(200),
    @idQualificacao         INT,
    @idStatus               INT   
)
AS
BEGIN
    BEGIN
        INSERT INTO 
            TB_CLIENTE_ONLINE_PODER 
        VALUES 
            (
                @idClienteOnline, @idDocumento, @idDocumentType, @inicioValidade, @fimValidade, @idRepresentante, 
                @numeroDocumento, @idQualificacao, @idStatus
            )
    END
END