SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

alter PROC [dbo].[PR_INSERIR_CLIENTE_ONLINE]  
                                 @pNome      as  varchar(150),
                                 @pEmail     as  varchar(150),
                                 @pIdPerfil  as  NUMERIC,
                                 @pTipo      as  varchar(100),
                                 @pAtivo     as  NUMERIC,
                                 @pLogin     as  varchar(100),
                                 @pChaveAD   as  varchar(100),
                                 @pCdCpfCnpj as  NUMERIC,
                                 @pCdBolsa   as  NUMERIC,
                                 @pPassword  as  varchar(150),
                                 @pTelefone  as  varchar(50),
                                 @tipoPessoa as  varchar(1),
                                 @suit       as  int,
                                 @invest     as  int,
                                 @desenq     as VARCHAR(1)
AS
BEGIN
    declare @vIdUsuario INT
  
        INSERT INTO TB_CLIENTES VALUES (@pCdCpfCnpj, GETDATE(), @tipoPessoa, @suit, @invest, @desenq, @pTelefone);
        SET @vIdUsuario = CAST(SCOPE_IDENTITY() AS INT)

        INSERT INTO TB_CA_USUARIO 
            (ID_CLIENTE, NOME, EMAIL, IDPERFIL, TIPOUSUARIO, ATIVO, LOGIN, SENHA, CHAVE_AD, CDBOLSA, SUITABILITY_STATUS)
        VALUES 
            (
                @vIdUsuario, @pNome, @pEmail, @pIdPerfil, @pTipo, @pAtivo, @pLogin, @pPassword,
                @pChaveAD, @pCdBolsa, 1
            )

        INSERT INTO TB_CLIENTE_ONLINE_STATUS_CADASTRO VALUES (@vIdUsuario, 1);
        
        RETURN @vIdUsuario
END