CREATE PROCEDURE INSERE_ENDERECO(
    @idClienteOnline INT,
    @tipoEndereco    VARCHAR(200) = NULL,
    @finalidade      VARCHAR(200) = NULL,
    @tipoLogradouro  VARCHAR(200) = NULL,
    @logradouro      VARCHAR(200) = NULL,
    @numero          VARCHAR(200) = NULL,
    @estado          VARCHAR(200) = NULL,
    @cidade          VARCHAR(200) = NULL,
    @pais            VARCHAR(200) = NULL
)
AS
BEGIN
    INSERT INTO TB_CLIENTE_ONLINE_ENDERECO VALUES
    (
        @idClienteOnline,
        @tipoEndereco,
        @finalidade,
        @tipoLogradouro,
        @logradouro,
        @numero,
        @estado,
        @cidade,
        @pais
    )
END