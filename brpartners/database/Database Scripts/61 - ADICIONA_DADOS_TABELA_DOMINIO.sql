INSERT INTO TB_CLIENTE_ONLINE_STATUS VALUES
(1, 'Em preenchimento'),
(2, 'Em analise'),
(3, 'Aprovado'),
(4, 'Reprovado')
GO

INSERT INTO TB_TIPO_CLIENTE VALUES
(1, 'Instituição Financeira'),
(444, 'Investidores Institucionais'),
(445, 'Pessoa Jurídica'),
(446, 'Pessoa Física'),
(447, 'IF Assemelhadas'),
(448, 'INR - Doc. Simplificada'),
(449, 'INR - Doc. Completa'),
(450, 'Fundos')
GO

INSERT INTO TB_PORTE VALUES
(1, 'Micro'),
(2, 'Pequena'),
(3, 'Média'),
(4, 'Grande'),
(5, 'Sem Rendimento'),
(6, 'Até 1 salário mínimo'),
(7, 'Mais de 1 a 2 salários mínimos'),
(8, 'Mais de 2 a 3 salários mínimos'),
(9, 'Mais de 3 a 5 salários mínimos'),
(10, 'Mais de 5 a 10 salários mínimos'),
(11, 'Mais de 10 a 20 salários mínimos'),
(12, 'Acima de 20 salários mínimos')
GO

INSERT INTO TB_CATEGORIA VALUES
(1, 'Coligada'),
(3, 'Fundo'),
(8, 'Outros')
GO

INSERT INTO TB_ESTADO_CIVIL VALUES
(1, 'Solteiro'),
(2, 'Casado'),
(3, 'Divorciado'),
(4, 'Viuvo'),
(5, 'Desquitado'),
(6, 'Marital'),
(7, 'Casado(a) com brasileiro(a) nato(a)'),
(8, 'Casado(a) com brasileiro(a) naturalizado'),
(9, 'Casado(a) com estrangeiro(a)'),
(10, 'Uniao Estavel'),
(11, 'Separado'),
(12, 'Casado(a) comunhao universal de bens'),
(13, 'Casado(a) comunhao parcial de bens'),
(14, 'Casado(a) separacao de bens'),
(15, 'Outros'),
(99, 'A Informar')
GO

INSERT INTO TB_REGIME_CASAMENTO VALUES
(1, 'Comunhão Total'),
(2, 'Comunhão Parcial'),
(3, 'Separação Total')
GO

INSERT INTO TB_CLASSIFICACAO VALUES
(41, 'Pessoa Fisica'),
(42, 'Outras'),
(37, 'Industria'),
(38, 'Comercio'),
(39, 'Serviços'),
(40, 'Inst. Financeira'),
(43, 'Outras')
GO

INSERT INTO TB_TIPO_DOCUMENTO VALUES
(1, 'Documento de identificação'),
(2, 'CPF'),
(3, 'Ficha cadastral'),
(4, 'Contrato de intermediação'),
(5, 'Questionário de suitability'),
(6, 'Banco de títulos CBLC - Termo  '),
(7, 'Procuração'),
(8, 'Ficha complementar banco'),
(9, 'Ficha complementar CTVM'),
(10, 'Comprovante residencial'),
(11, 'Contrato/Estatuto social'),
(12, 'Lista de emissores de ordem'),
(13, 'Ata de eleição de diretoria'),
(14, 'Balanço patrimonial'),
(15, 'Questionário AML'),
(16, 'Certidão de casamento'),
(17, 'Cartão de assinatura'),
(18, 'Contrato de rep legal e fiscal'),
(19, 'Financial Statements'),
(20, 'By laws'),
(21, 'Invest Management Agreement'),
(22, 'Private Offerring Memorandum'),
(23, 'Directors Election'),
(24, 'Certificate of register'),
(25, 'Tab de participação societária'),
(26, 'Organograma do grupo'),
(27, 'KYC - Conheça seu cliente'),
(28, 'Ficha de representante legal'),
(29, 'Regulamento CVM - Fundo'),
(30, 'Ficha complementar gestão'),
(31, 'CGD - Contrato legal de deriva'),
(32, 'CGD - Apêndice'),
(33, 'Ficha avalista'),
(34, 'Waivers'),
(35, 'E-mails'),
(36, 'SCR/SISBACEN'),
(37, 'Contrato de repasse'),
(38, 'IRPF + Aplicações financeiras'),
(38, 'Contrato de parceria comercial'),
(40, 'Assinatura'),
(41, 'Cont. COE')
GO

INSERT INTO TB_QUALIFICACAO VALUES
(1, 'Opção1'),
(2, 'Opção2'),
(3, 'Opção3')
GO

INSERT INTO TB_TIPO_DOCUMENTO_PODER VALUES
(1, 'Publico'),
(2, 'Particular')
GO

INSERT INTO TB_DOCUMENTO_VALIDADE VALUES
(1, '6'),
(2, '12'),
(3, '18'),
(4, '24')
GO

INSERT INTO TB_TIPO_ENDERECO VALUES 
(4, 'Residencial'), 
(5, 'Comercial'), 
(6, 'Outros'),
(7, 'Caixa Postal'),
(8, 'Matriz'),
(9, 'Filial'),
(10, 'E-mail')
GO

INSERT INTO TB_FINALIDADE VALUES 
(1, 'Finalidade 1'), 
(2, 'Finalidade 2'), 
(3, 'Finalidade 3'),
(4, 'Finalidade 4'),
(5, 'Finalidade 5'),
(6, 'Finalidade 6'),
(7, 'Finalidade 7')
GO

INSERT INTO TB_FORMA_REPRESENTACAO VALUES 
(1, 'Procurador'), 
(2, 'Diretor'), 
(3, 'Sócio-Administrador'),
(4, 'Avalista'),
(5, 'Emissor de Ordem')
GO

INSERT INTO TB_BANCO VALUES 
(1,'BANCO DO BRASIL S.A.'), 
(204,'BANCO BRADESCO CARTOES S.A.'), 
(356,'BANCO ABN AMRO REAL S.A.'),
(654,'BANCO A.J. RENNER S.A.'), 
(2,'BANCO CENTRAL DO BRASIL'), 
(3,'BANCO DA AMAZONIA S.A.'),
(4,'BANCO DO NORDESTE DO BRASIL S.A.'), 
(7,'BANCO NACIONAL DO DESENVOLVIMENTO ECONOM'), 
(8,'BANCO DO ESTADO DE SAO PAULO S.A. - BANE'),
(20,'BANCO DO ESTADO DE ALAGOAS S.A.'),
(21,'BANESTES S.A BANCO DO ESTADO DO ESPIRITO'), 
(22,'CREDIREAL -EM ABSORCAO'), 
(24,'BANCO DE PERNAMBUCO S.A.-BANDEPE'),
(25,'BANCO ALFA S/A'), 
(26,'BANCO DO ESTADO DO ACRE S.A.'), 
(27,'BANCO DO ESTADO DE SANTA CATARINA S.A.'),
(28,'BANEB-EM ABSORCAO'), 
(29,'BANCO BANERJ S.A.'), 
(30,'PARAIBAN-BANCO DA PARAIBA S.A.'),
(31,'BANCO BEG S.A.'),
(32,'BANCO DO ESTADO DO MATO GROSSO S.A.'), 
(33,'BANCO SANTANDER (BRASIL) S.A.'), 
(34,'BANCO BEA S.A.'),
(35,'BANCO BEC S.A.'), 
(36,'BANCO BRADESCO BBI S.A'), 
(37,'BANCO DO ESTADO DO PARA S.A.'),
(38,'BANCO BANESTADO S.A.'), 
(39,'BANCO DO ESTADO DO PIAUI S.A.-BEP'), 
(40,'BANCO CARGILL S.A'),
(41,'BANCO DO ESTADO DO RIO GRANDE DO SUL S.A'),
(42,'BANCO J. SAFRA S.A'), 
(44,'BANCO BVA SA'), 
(45,'BANCO OPPORTUNITY S.A.'),
(47,'BANCO DO ESTADO DE SERGIPE S.A.'), 
(48,'BANCO BEMGE S.A.'), 
(59,'BANCO DO ESTADO DE RONDONIA S.A.'),
(61,'BANCO ABB SA'), 
(62,'HIPERCARD BANCO MULTIPLO S.A'), 
(63,'BANCO IBI S.A - BANCO MULTIPLO'),
(64,'GOLDMAN SACHS DO BRASIL-BANCO MULTIPLO S'),
(65,'BANCO BRACCE S.A.'), 
(66,'BANCO MORGAN STANLEY DEAN WITTER S.A'), 
(67,'BANCO BANEB SA'),
(68,'BANCO BEA S.A'), 
(70,'BRB - BANCO DE BRASILIA S.A.'), 
(72,'BANCO RURAL MAIS S.A'),
(73,'BB BANCO POPULAR DO BRASIL'), 
(74,'BANCO J. SAFRA S.A.'), 
(96,'BANCO BM&F DE SERV. DE LIQUIDA00O E CUST'),
(104,'CAIXA ECONOMICA FEDERAL'),
(106,'BANCO ITABANCO S.A.'), 
(107,'BANCO BBM S.A'), 
(109,'CREDIBANCO S.A.'),
(116,'BANCO UNICO S.A.'), 
(148,'BANK OF AMERICA - BRASIL S.A. (BANCO MUL'), 
(151,'BANCO NOSSA CAIXA S.A'),
(153,'CAIXA ECONOMICA ESTADUAL DO RIO GRANDE D'), 
(165,'BANCO NORCHEM S.A.'), 
(166,'BANCO INTER-ATLANTICO S.A.'),
(168,'HSBC INVESTMENT BANK BRASIL S.A.-BANCO M'),
(175,'BANCO FINASA S.A.'), 
(184,'BANCO ITAU BBA S.A'), 
(199,'BANCO CAIXA GERAL - BRASIL S.A.'),
(200,'BANCO FICRISA AXELRUD S.A.'), 
(201,'BANCO AXIAL S.A.'), 
(205,'BANCO SUL AMERICA S.A.'),
(206,'BANCO MARTINELLI S.A.'), 
(208,'BANCO BTG PACTUAL S.A.'), 
(210,'DRESDNER BANK LATEINAMERIKA AKTIENGESELL'),
(211,'BANCO SISTEMA S.A.'),
(212,'BANCO MATONE S.A.'), 
(213,'BANCO ARBI S.A.'), 
(214,'BANCO DIBENS S.A.'),
(215,'BANCO COMERCIAL E DE INVESTIMENTO SUDAME'), 
(216,'BANCO REGIONAL MALCON S.A.'), 
(217,'BANCO JOHN DEERE S.A.'),
(218,'BANCO BONSUCESSO S.A.'), 
(219,'BANCO ZOGBI S.A'), 
(220,'BANCO CREFISUL S.A.'),
(221,'BANCO CHASE FLEMING S.A.'),
(222,'BANCO CREDIT AGRICOLE BRASIL S.A.'), 
(224,'BANCO FIBRA S.A.'), 
(225,'BANCO BRASCAN S.A.'),
(228,'BANCO ICATU S.A.'), 
(229,'BANCO CRUZEIRO DO SUL S.A.'), 
(230,'UNICARD BANCO MULTIPLO S.A'),
(231,'BANCO BOAVISTA INTERATLANTICO S.A-EM ABS'), 
(232,'BANCO INTERPART S.A.'), 
(233,'BANCO GE CAPITAL S.A'),
(234,'BANCO LAVRA S.A.'),
(235,'BANK OF AMERICA - LIBERAL S.A'), 
(236,'BANCO CAMBIAL SA'), 
(237,'BANCO BRADESCO S.A.'),
(239,'BANCO BANCRED S.A.'), 
(240,'BANCO DE CREDITO REAL DE MINAS GERAIS S.'), 
(241,'BANCO CLASSICO S.A.'),
(242,'BANCO EUROINVEST S.A. EUROBANCO'), 
(243,'BANCO MAXIMA S.A.'), 
(244,'BANCO CIDADE S.A.'),
(245,'BANCO EMPRESARIAL S.A.'),
(246,'BANCO ABC BRASIL S.A.'), 
(247,'BANCO UBS S.A'), 
(248,'BANCO BOA VISTA INTERATLANTICO S.A'),
(249,'BANCO INVESTCRED UNIBANCO S.A'), 
(250,'BANCO SCHAHIN S.A'), 
(252,'BANCO FININVEST S.A.'),
(254,'PARANA BANCO S.A.'), 
(255,'MILBANCO S.A.'), 
(256,'BANCO GULFINVEST S.A.'),
(258,'BANCO INDUSCRED S.A. '),
(262,'BANCO BOREAL S.A.'), 
(263,'BANCO CACIQUE S.A.'), 
(265,'BANCO FATOR S.A.'),
(266,'BANCO CEDULA S.A.'), 
(267,'BANCO BBM-COM.C.IMOB.CFI S.A.'), 
(275,'BANCO REAL S.A.'),
(277,'BANCO PLANIBANC S.A.'), 
(282,'BANCO BRASILEIRO COMERCIAL S.A.- BBC'), 
(291,'BANCO BCN S.A.'),
(294,'BCR/EM ABSORCAO'),
(300,'BANCO DE LA NACION ARGENTINA'), 
(302,'BANCO DO PROGRESSO S.A.'), 
(303,'BANCO HNF S.A.'),
(304,'BANCO PONTUAL S.A.'), 
(318,'BANCO BMG S.A.'), 
(320,'BANCO INDUSTRIAL E COMERCIAL S.A.'),
(341,'ITAU UNIBANCO S.A.'), 
(346,'BANCO BFB _ EM ABSORCAO'), 
(347,'BANCO SUDAMERIS DO BRASIL S.A'),
(351,'BANCO SANTANDER S.A.'),
(353,'BANCO SANTANDER BRASIL S.A.'), 
(366,'BANCO SOCIETE GENERALE BRASIL S.A'), 
(369,'BANCO DIGIBANCO S.A.'),
(370,'BANCO WESTLB DO BRASIL  S.A.'), 
(372,'BANCO ITAMARATI S/A'), 
(375,'BANCO FENICIA S.A.'),
(376,'BANCO J.P. MORGAN S.A.'), 
(388,'BANCO BMD S.A.'), 
(389,'BANCO MERCANTIL DO BRASIL S.A.'),
(392,'BANCO MERCANTIL DE SAO PAULO S.A.'),
(394,'BANCO BRADESCO FINANCIAMENTOS S.A.'), 
(399,'HSBC BANK BRASIL S.A.-BANCO MULTIPLO'), 
(409,'UNIBANCO - UNIAO DE BANCOS BRASILEIROS S'),
(412,'BANCO CAPITAL S.A.'), 
(415,'BANCO NACIONAL S.A.'), 
(420,'BANORTE - BANCO NACIONAL DO NORTE S.A.'),
(422,'BANCO SAFRA S.A.'), 
(424,'BANCO SANTANDER NOROESTE S.A'), 
(434,'BANFORT - BANCO FORTALEZA S.A.'),
(453,'BANCO RURAL S.A.'),
(456,'BANCO DE TOKYO MITSUBISHI UFJ BRASIL S.A'), 
(464,'BANCO SUMITOMO MITSUI BRASILEIRO S.A.'), 
(472,'LLOYDS TSB BANK PLC'),
(473,'BANCO CAIXA GERAL - BRASIL S.A.'), 
(477,'CITIBANK N.A.'), 
(479,'BANCO ITAUBANK S.A.'),
(480,'BANCO WACHOVIA S.A.'), 
(487,'DEUTSCHE BANK S. A. - BANCO ALEMAO'), 
(488,'JPMORGAN CHASE BANK'),
(489,'BANCO FRANCES INTERNACIONAL-BRASIL S.A'),
(492,'ING BANK N.V.'), 
(493,'BANCO UNION - BRASIL S.A'), 
(494,'BANCO DE LA REPUBLICA ORIENTAL DEL URUGU'),
(495,'BANCO DE LA PROVINCIA DE BUENOS AIRES'), 
(496,'BANCO UNO-E BRASIL S.A'), 
(498,'CENTRO HISPANO BANCO'),
(499,'BANCO IOCHPE S.A.'), 
(501,'BANCO BRASILEIRO IRAQUIANO S.A.'), 
(502,'BANCO SANTANDER DE NEGOCIOS S.A.'),
(504,'BANCO MULTIPLIC S.A.'),
(505,'BANCO CREDIT SUISSE (BRASIL) S.A.'), 
(600,'BANCO LUSO BRASILEIRO S.A.'), 
(602,'BANCO PATENTE S.A.'),
(604,'BANCO INDUSTRIAL DO BRASIL S.A.'), 
(607,'BANCO SANTOS NEVES S.A.'), 
(610,'BANCO VR S.A.'),
(611,'BANCO PAULISTA S.A.'), 
(612,'BANCO GUANABARA S.A.'), 
(613,'BANCO PECUNIA S.A.'),
(618,'BANCO TENDENCIA S.A.'),
(621,'BANCO APLICAP S.A.'), 
(623,'BANCO PANAMERICANO S.A.'), 
(624,'BANCO GENERAL MOTORS S.A'),
(625,'BANCO ARAUCARIA S.A.'), 
(626,'BANCO FICSA S.A.'), 
(627,'BANCO DESTAK S.A.'),
(628,'BANCO CRITERIUM S.A.'), 
(630,'BANCO INTERCAP S.A.'), 
(633,'BANCO RENDIMENTO S.A.'),
(634,'BANCO TRIANGULO S.A.'),
(635,'BANCO DO ESTADO AMAPA S.A.'), 
(637,'BANCO SOFISA S.A.'), 
(638,'BANCO PROSPER S.A.'),
(640,'BANCO CREDITO METROPOLITANO S/A'), 
(641,'BANCO ALVORADA S.A.'), 
(643,'BANCO PINE S.A.'),
(645,'BANCO DO ESTADO DE RORAIMA S.A.'), 
(647,'BANCO MARKA S.A.'), 
(649,'BANCO DIMENSAO S.A.'),
(650,'BANCO PEBB S.A.'),
(652,'BANCO ITAU HOLDING FINANCEIRA S.A'), 
(653,'BANCO INDUSVAL S.A.'), 
(655,'BANCO VOTORANTIM S.A.'),
(656,'BANCO MATRIX S.A.'), 
(657,'BANCO TECNICORP S.A.'), 
(658,'BANCO PORTO REAL S.A.'),
(702,'BANCO SANTOS S.A.'), 
(707,'BANCO DAYCOVAL S.A.'), 
(711,'BANCO VETOR S.A.'),
(715,'BANCO VEGA S.A. '),
(718,'BANCO OPERADOR S.A.'), 
(719,'BANIF-BANCO INTERNACIONAL DO FUNCHAL (BR'), 
(720,'BANCO MAXINVEST S.A.'),
(721,'BANCO CREDIBEL S.A.'), 
(722,'BANCO INTERIOR DE SAO PAULO S.A.'), 
(725,'BANCO FINANSINOS S. A.'),
(728,'BANCO FITAL S.A.'), 
(729,'BANCO FONTE CINDAM S.A.'), 
(732,'BANCO MINAS S.A.'),
(733,'BANCO DAS NACOES S.A.'),
(734,'BANCO GERDAU S.A.'), 
(735,'BANCO POTTENCIAL S.A.'), 
(737,'BANCO THECA S.A.'),
(738,'BANCO MORADA S.A.'), 
(739,'BANCO BGN S.A.'), 
(740,'BANCO BARCLAYS S.A.'),
(741,'BANCO RIBEIRAO PRETO S.A.'), 
(742,'BANCO EQUATORIAL S.A.'), 
(743,'BANCO SEMEAR S.A.'),
(744,'BANKBOSTON N.A.'),
(745,'BANCO CITIBANK S.A.'), 
(746,'BANCO MODAL S.A.'), 
(747,'BANCO RABOBANK INTERNATIONAL BRASIL S.A.'),
(748,'BANCO COOPERATIVO SICREDI S.A.'), 
(749,'BANCO SIMPLES S.A.'), 
(750,'HSBC REPUBLIC BANK BRASIL S.A-BANCO MULT'),
(751,'DRESDNER BANK BRASIL S.A. BANCO MULTIPLO'), 
(752,'BANCO BNP PARIBAS BRASIL S.A'), 
(753,'NBC BANK BRASIL S.A.- BANCO MULTIPLO'),
(755,'BANK OF AMERICA MERRILL LYNCH BANCO MULT'),
(756,'BANCO COOPERATIVO DO BRASIL S.A.'), 
(757,'BANCO KEB DO BRASIL S.A.'), 
(800,'BCR BANCO DE CREDITO REAL S.A'),
(100,'Banco bahia'), 
(5555,'JOSE BANK'), 
(801,'Real Banco'),
(69,'BPN BRASIL BANCO MULTIPLO S.A'), 
(75,'BANCO CR2 S.A'), 
(76,'BANCO KDB DO BRASIL S.A'),
(123,'Invest'),
(3652,'FRT - BANCO ALFA'),
(12,'BANCO STANDARD DE INVESTIMENTOS S.A.'), 
(14,'NATIXIS BRASIL S.A. - BANCO M0LTIPLO'), 
(19,'BANCO AZTECA DO BRASIL S.A.'),
(77,'BANCO INTERMEDIUM S.A.'), 
(78,'BES INVESTIMENTO DO BRASIL SA - BANCO DE'), 
(79,'JBS BANCO S.A.'),
(81,'CONCORDIA BANCO S.A.'),
(82,'BANCO TOP0ZIO S.A.'), 
(83,'BANCO DA CHINA BRASIL S.A.'), 
(84,'CC UNICRED NORTE DO PARANA'),
(85,'COOPERATIVA CENTRAL DE CREDITO URBANO'), 
(86,'OBOE CREDITO, FINANCIAMENTO E INVESTIMEN'), 
(87,'UNICRED CENTRAL SANTA CATARINA'),
(88,'BANCO RANDON S.A.'), 
(89,'COOPERATIVA DE CREDITO RURAL DA REGIAO D'), 
(90,'COOPERATIVA CENTRAL DE CREDITO DO ESTADO'),
(91,'UNICRED CENTRAL RS - CENTRAL DE COOP ECO'),
(92,'BRICKELL S A CREDITO, FINANCIAMENTO E IN'), 
(94,'BANCO PETRA S.A.'), 
(97,'COOPERATIVA CENTRAL DE CREDITO NOROESTE'),
(98,'CREDICOROL COOPERATIVA DE CREDITO RURAL'), 
(99,'COOPERATIVA CENTRAL ECONOMIA E CREDITO M'), 
(11,'Banco do Brasil SA'),
(102,'XP INVESTIMENTOS CCTVM S/A'), 
(114,'BANCO DO BRASIL SA'), 
(17,'BNY MELLON BANCO SA'),
(126,'BR PARTNERS BI'),
(135,'Gradual')
GO

INSERT INTO TB_TIPO_REGISTRO VALUES 
(1, 'Cliente'), 
(2, 'BrPartners')
GO


INSERT INTO TB_ESTADO VALUES (1, 'Opcao1'), (2, 'Opcao2'), (3, 'Opcao3')
GO
INSERT INTO TB_PAIS VALUES (1, 'Opcao1'), (2, 'Opcao2'), (3, 'Opcao3')
GO

INSERT INTO TB_ATIVIDADE VALUES (1, 'Opcao1'), (2, 'Opcao2'), (3, 'Opcao3')
GO
INSERT INTO TB_OCUPACAO VALUES (1, 'Opcao1'), (2, 'Opcao2'), (3, 'Opcao3')
GO
INSERT INTO TB_PROFISSAO VALUES (1, 'Opcao1'), (2, 'Opcao2'), (3, 'Opcao3')

GO
INSERT INTO TB_SEXO VALUES (1, 'Opcao1'), (2, 'Opcao2'), (3, 'Opcao3')
GO
INSERT INTO TB_STATUS VALUES (1, 'Opcao1'), (2, 'Opcao2'), (3, 'Opcao3')
GO
INSERT INTO TB_GRUPO_ECONOMICO VALUES (1, 'Opcao1'), (2, 'Opcao2'), (3, 'Opcao3')
GO
INSERT INTO TB_ATIVIDADE_ECONOMICA VALUES (1, 'Opcao1'), (2, 'Opcao2'), (3, 'Opcao3')

GO
INSERT INTO TB_GERENTE VALUES (1, 'Opcao1'), (2, 'Opcao2'), (3, 'Opcao3')
GO
INSERT INTO TB_NATURALIDADE VALUES (1, 'Opcao1'), (2, 'Opcao2'), (3, 'Opcao3')
GO
INSERT INTO TB_NACIONALIDADE VALUES (1, 'Opcao1'), (2, 'Opcao2'), (3, 'Opcao3')
GO
INSERT INTO TB_TIPO_TELEFONE VALUES (1, 'Opcao1'), (2, 'Opcao2'), (3, 'Opcao3')
GO
INSERT INTO TB_PRODUTO VALUES (1, 'Aplicações Financeiras'), (2, 'Câmbio'), (3, 'Derivativos'), (4, 'Operações de Crédito')
GO
INSERT INTO TB_NATUREZA_FISCAL VALUES (1, 'Natureza1'), (2, 'Natureza2'), (3, 'Natureza3')
GO
INSERT INTO TB_FINALIDADE_CONTA VALUES (1, 'Conta Principal'), (2, 'Finalidade2'), (3, 'Finalidade3')
GO
INSERT INTO TB_UTILIZACAO_CONTA VALUES (1, 'Emite Nota'), (2, 'Envia Extrato'), (3, 'Emite Aviso')
GO
INSERT INTO TB_TIPO_CONTA VALUES (1, 'Individual'), (2, 'Tipo2'), (3, 'Tipo3')
GO
INSERT INTO TB_TIPO_CONTA_BANCO VALUES (1, 'Corrente'), (2, 'Investimento'), (3, 'Poupança')

GO
INSERT INTO TB_PERFIL_REPRESENTANTE VALUES (1, 'Perfil1'), (2, 'Perfil2'), (3, 'Perfil3')
GO
INSERT INTO TB_ESCOLHA VALUES (1, 'Sim'), (2, 'Não')



-- drop table TB_TIPO_CLIENTE
-- drop table TB_CATEGORIA
-- drop table TB_ESTADO_CIVIL
-- drop table TB_REGIME_CASAMENTO
-- drop table TB_CLASSIFICACAO
-- DROP TABLE TB_TIPO_REGISTRO