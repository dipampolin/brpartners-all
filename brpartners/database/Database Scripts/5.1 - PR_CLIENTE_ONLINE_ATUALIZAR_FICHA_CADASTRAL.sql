CREATE PROCEDURE PR_CLIENTE_ONLINE_ATUALIZAR_FICHA_CADASTRAL(
    @idClienteOnline        INT,
    @grupoEconomico         VARCHAR(200) = NULL,
    @classificacao          VARCHAR(200) = NULL,
    @grupoAtividade         VARCHAR(200) = NULL,
    @tipoCadastro           VARCHAR(200) = NULL,
    @tipoCliente            VARCHAR(200) = NULL,
    @categoria              VARCHAR(200) = NULL,
    @atividadePrincipal     VARCHAR(200) = NULL,
    @naturezaOperacao       VARCHAR(200) = NULL,
    @porte                  VARCHAR(200) = NULL,
    @nome                   VARCHAR(200) = NULL,
    @cpf                    VARCHAR(20) = null,
    @profissao              VARCHAR(200) = NULL,
    @genero                 VARCHAR(50) = NULL,
    @dataNasc               DATE = NULL,
    @estadoCivil            VARCHAR(20) = NULL,
    @regimeCasamento        VARCHAR(200) = NULL,
    @uniaoEstavel           VARCHAR(50) = NULL,
    @estrangeiro            VARCHAR(50) = NULL,
    @nomePai                VARCHAR(200) = NULL,
    @nomeMae                VARCHAR(200) = NULL,
    @emailPrincipal         VARCHAR(200) = NULL,
    @status                 VARCHAR(200) = NULL,
    @observacao             VARCHAR(200) = NULL,
    @paisNascimento         VARCHAR(200) = NULL,
    @estadoNascimento       VARCHAR(200) = NULL,
    @naturalidade           VARCHAR(200) = NULL,
    @nacionalidade          VARCHAR(200) = NULL,
    @nomeConjugue           VARCHAR(200) = NULL,
    @cpfConjugue            VARCHAR(200) = NULL,
    @nascimentoConjugue     DATE = NULL,
    @tipoDocumento          VARCHAR(200) = NULL,
    @numeroDocumento        VARCHAR(200) = NULL,
    @orgaoEmissor           VARCHAR(200) = NULL,
    @dataVencimento         DATE = NULL,
    @dataEmissao            DATE = NULL,
    @estadoEmissao          VARCHAR(200) = NULL,
    @nomeEmpresa            VARCHAR(200) = NULL,
    @dataAdmissao           DATE = NULL,
    @endereco               VARCHAR(200) = NULL,
    @gerente                VARCHAR(200) = NULL,
    @dataInicio             DATE = NULL,
    @dataFinal              DATE = NULL,
    @cargoPublico           VARCHAR(200) = NULL,
    @funcaoPublica          VARCHAR(200) = NULL,
    @empresaPublica         VARCHAR(200) = NULL,
    @dataInicioEP           DATE = NULL,
    @dataFinalEP            DATE = NULL,
    @parenteCargoPublico    VARCHAR(200) = NULL,
    @nomeParente            VARCHAR(200) = NULL,
    @tipoParente            VARCHAR(200) = NULL,
    @funcaoPublicaParente   VARCHAR(200) = NULL,
    @possuiRepresentante    VARCHAR(50) = NULL,
    @relacaoBrPartners      VARCHAR(50) = NULL,
    @paisEstrangeiro        VARCHAR(50) = NULL,
    @estadoEstrangeiro      VARCHAR(50) = NULL,
    @cidadeEstrangeiro      VARCHAR(50) = NULL          
)
AS
BEGIN
            DECLARE @idUsuario int

            UPDATE 
                TB_CLIENTE_ONLINE_FICHA_CADASTRAL 
            SET 
                GRUPO_ECONOMICO = @grupoEconomico, CLASSIFICACAO = @classificacao,
                GRUPO_ATIVIDADE = @grupoAtividade, TIPO_CADASTRO = @tipoCadastro, TIPO_CLIENTE = @tipoCliente,
                CATEGORIA = @categoria, ATIVIDADE_PRINCIPAL = @atividadePrincipal, NATUREZA_OPERACAO = @naturezaOperacao,
                PORTE = @porte, DATA_NASC = @dataNasc, PROFISSAO = @profissao, NACIONALIDADE = @nacionalidade, 
                GENERO = @genero, REGIME_CASAMENTO = @regimeCasamento, UNIAO_ESTAVEL = @uniaoEstavel, 
                ESTADO_CIVIL = @estadoCivil, NOME_PAI = @nomePai, NOME_MAE = @nomeMae, NOME_CONJUGUE = @nomeConjugue,
                CPF_CONJUGUE = @cpfConjugue, NASC_CONJUGUE = @nascimentoConjugue, ESTRANGEIRO = @estrangeiro,
                OBS = @observacao, PAIS_NASCIMENTO = @paisNascimento, ESTADO_NASCIMENTO = @estadoNascimento,
                NATURALIDADE = @naturalidade, TIPO_DOC = @tipoDocumento, NUMERO_DOC = @numeroDocumento, 
                ORGAO_EMISSOR_DOC = @orgaoEmissor, DATA_VENCIMENTO_DOC = @dataVencimento, DATA_EMISSAO_DOC = @dataEmissao,
                ESTADO_EMISSAO_DOC = @estadoEmissao, NOME_EMPRESA = @nomeEmpresa, DATA_ADMISSAO = @dataAdmissao, 
                ENDERECO_EMPRESA = @endereco, GERENTE_REL = @gerente, DATA_INICIO_REL = @dataInicio, DATA_FIM_REL = @dataFinal,
                CARGO_PUBL = @cargoPublico, NOME_CARGO_PUBL = @funcaoPublica, NOME_EMPRESA_PUBL = @empresaPublica,
                DATA_INICIO_PUBL = @dataInicioEP, DATA_FIM_PUBL = @dataFinalEP, PARENTE_EMP_PUBL = @parenteCargoPublico,
                NOME_PARENTE = @nomeParente, TP_PARENTE = @tipoParente, CARGO_PARENTE_PUBL = @funcaoPublicaParente,
                TEM_REPRESENTANTE = @possuiRepresentante, ADMINISTRADOR = @relacaoBrPartners, PAIS_NASC_ESTR = @paisEstrangeiro, 
                ESTADO_NASC_ESTR = @estadoEstrangeiro, NATURALIDADE_ESTR = @cidadeEstrangeiro,
                [STATUS] = @status, DATA_ALTERACAO = GETDATE()
            WHERE 
                ID_CLIENTE_ONLINE = @idClienteOnline

            UPDATE TB_CLIENTES SET CD_CPFCNPJ = @cpf WHERE ID_CLIENTE = @idClienteOnline

            UPDATE TB_CA_USUARIO SET NOME = @nome, EMAIL = @emailPrincipal WHERE ID_CLIENTE = @idClienteOnline
END