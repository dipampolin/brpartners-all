CREATE PROCEDURE INSERE_PRODUTO_BANCARIO(
    @idClienteOnline        INT,
    @idProduto              INT
)
AS
BEGIN
    INSERT INTO TB_CLIENTE_ONLINE_FINANCEIRO_PRODUTO VALUES 
    (
        @idClienteOnline, @idProduto
    )
END