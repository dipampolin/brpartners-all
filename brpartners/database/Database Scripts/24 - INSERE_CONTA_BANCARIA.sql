CREATE PROCEDURE INSERE_CONTA_BANCARIA(
    @idClienteOnline    INT,
    @IdFinalidade       INT = 0,
    @idUtilizacao       INT = 0,
    @idTipoConta        INT = 0,
    @idTipoContaBanco   INT = 0,
    @idBanco            INT = 0,
    @agencia            VARCHAR(200),
    @numeroConta        VARCHAR(200)
)
AS
BEGIN
    INSERT INTO TB_CLIENTE_ONLINE_FINANCEIRO_CONTA 
    VALUES 
    (
        @idClienteOnline, @IdFinalidade, @idUtilizacao, @idTipoConta, @idTipoContaBanco, @idBanco, @agencia, @numeroConta
    )
END