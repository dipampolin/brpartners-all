CREATE PROCEDURE PR_CLIENTE_ONLINE_ATUALIZAR_FINANCEIRO(
    @idClienteOnline        INT,
    @rendaMensal            VARCHAR(200) = NULL,
    @rendaFamilia           VARCHAR(200) = NULL,
    @outrasRendas           VARCHAR(200) = NULL,
    @imovelPatrimonio       VARCHAR(200) = NULL,
    @veiculoPatrimonio      VARCHAR(200) = NULL,
    @aplicacaoPatrimonio    VARCHAR(200) = NULL,
    @aplicacaoOrigem        BIT,
    @indenizacao            BIT,
    @premio                 BIT,
    @rescisao               BIT,
    @salario                BIT,
    @venda                  BIT,
    @joias                  BIT,
    @aluguel                BIT,
    @doacao                 BIT,
    @heranca                BIT,
    @automovel              BIT,
    @seguro                 BIT,
    @producao               BIT,
    @imovel                 BIT,
    @outros                 BIT,
    @outrosDescricao        VARCHAR(200) = NULL,
    @idNatureza             INT
)
AS
BEGIN
    UPDATE 
        TB_CLIENTE_ONLINE_FINANCEIRO SET RENDA_MENSAL = @rendaMensal, RENDA_MENSAL_FAMILIAR = @rendaFamilia, OUTRAS_RENDAS = @outrasRendas, 
        IMOVEL_PATRIMONIO = @imovelPatrimonio, VEICULO_PATRIMONIO = @veiculoPatrimonio, APLICACAO_PATRIMONIO = @aplicacaoPatrimonio,
        APLICACAO_ORIGEM = @aplicacaoOrigem, INDENIZACAO_ORIGEM = @indenizacao, PREMIO_ORIGEM = @premio, RESCISAO_ORIGEM = @rescisao, SALARIO_ORIGEM = @salario,
        VENDA_ORIGEM = @venda, JOIAS_ORIGEM = @joias, ALUGUEL_ORIGEM = @aluguel, DOACAO_ORIGEM = @doacao, HERANCA_ORIGEM = @heranca, AUTOMOVEL_ORIGEM = @automovel,
        INDENIZACAO_SEG_ORIGEM = @seguro, PRODUCAO_AGRI_ORIGEM = @producao, IMOVEL_ORIGEM = @imovel, OUTROS_ORIGEM = @outros, OUTROS_DESCRICAO = @outrosDescricao,
        ID_NATUREZA_FISCAL = @idNatureza
    WHERE
        ID_CLIENTE_ONLINE = @idClienteOnline
END