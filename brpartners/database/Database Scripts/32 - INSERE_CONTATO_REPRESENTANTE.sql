CREATE PROCEDURE INSERE_CONTATO_REPRESENTANTE(
    @idClienteOnline INT,
    @tipo            VARCHAR(200) = NULL,
    @ddi             VARCHAR(200) = NULL,
    @ddd             VARCHAR(200) = NULL,
    @numero          VARCHAR(200) = NULL,
    @ramal           VARCHAR(200) = NULL,
    @emailContato    VARCHAR(200) = NULL
)
AS
BEGIN
    INSERT INTO TB_CLIENTE_ONLINE_REPRESENTANTE_CONTATO VALUES
    (
        @idClienteOnline,
        @tipo,
        @ddi,
        @ddd,
        @numero,
        @ramal,
        @emailContato
    )
END