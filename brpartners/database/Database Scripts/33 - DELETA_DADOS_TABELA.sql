CREATE PROCEDURE DELETA_DADOS_TABELA
(
    @idClientOnline varchar(300),
    @nomeTabela VARCHAR(300),
    @nomeColuna VARCHAR(300) 
)
AS
BEGIN 
    DECLARE @Sql NVARCHAR(MAX);
    SET @Sql = N'DELETE FROM ' + QUOTENAME(@nomeTabela) + ' WHERE '+ @nomeColuna +' = '+@idClientOnline+''

EXECUTE sp_executesql @Sql

END
GO