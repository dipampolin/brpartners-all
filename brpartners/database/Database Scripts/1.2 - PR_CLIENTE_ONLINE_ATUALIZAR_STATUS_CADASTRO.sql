CREATE PROCEDURE PR_CLIENTE_ONLINE_ATUALIZAR_STATUS_CADASTRO
(
    @idCliente   INT,
    @idStatus    INT
)
AS
UPDATE TB_CLIENTE_ONLINE_STATUS_CADASTRO SET ID_STATUS_CLIENTE_ONLINE = @idStatus WHERE ID_CLIENTE = @idCliente