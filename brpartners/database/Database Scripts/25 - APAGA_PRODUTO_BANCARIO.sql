CREATE PROCEDURE APAGA_PRODUTO_BANCARIO (
    @idClientOnline int
)
AS
BEGIN
    DELETE TB_CLIENTE_ONLINE_FINANCEIRO_PRODUTO WHERE ID_CLIENTE_ONLINE = @idClientOnline
END