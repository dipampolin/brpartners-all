USE PORTAL
BEGIN TRY
    BEGIN TRAN

        DECLARE @id             int
        DECLARE @idCadOnline    int
        DECLARE @idPerfil       int
        DECLARE @idPerfilCad    int

        SELECT @idPerfilCad = ID FROM TB_CA_PERFIL WHERE NOME = 'Cadastro'

        -- ADICIONA NOVA COLUNA
        IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'TELEFONE' AND Object_ID = Object_ID(N'TB_CLIENTES'))
        BEGIN
            ALTER TABLE TB_CLIENTES ADD TELEFONE VARCHAR(20)
        END

        -- ADICIONA PERFIL DE ACESSO
        IF EXISTS (SELECT * FROM TB_CA_PERFIL WHERE NOME = 'Pre-Cadastro') 
        BEGIN
            SELECT @idPerfil = ID FROM TB_CA_PERFIL WHERE NOME = 'Pre-Cadastro'
        END
        ELSE
            BEGIN
                INSERT INTO TB_CA_PERFIL VALUES ('Pre-Cadastro')
                SET @idPerfil = CAST(SCOPE_IDENTITY() AS INT)
            END


        -- ADICIONA ITEM PAI - CADASTRO ONLINE
        IF EXISTS (SELECT * FROM TB_CA_ELEMENTO WHERE NOME = 'CadastroOnline') 
        BEGIN
            SELECT @id = ID FROM TB_CA_ELEMENTO WHERE NOME = 'CadastroOnline'
            SELECT @idCadOnline = ID FROM TB_CA_ELEMENTO WHERE NOME = 'CadastroOnline'
        END
        ELSE
            BEGIN
                INSERT INTO TB_CA_ELEMENTO VALUES ('CadastroOnline', 'Cadastro Online', NULL, 1, 1, 1)
                SET @id = CAST(SCOPE_IDENTITY() AS INT)
                SET @idCadOnline = CAST(SCOPE_IDENTITY() AS INT)
            END

        IF NOT EXISTS (SELECT * FROM TB_CA_ELEMENTO_PERFIL WHERE IDELEMENTO = @id AND IDPERFIL = @idPerfil) 
        BEGIN
            INSERT INTO TB_CA_ELEMENTO_PERFIL VALUES (@id, @idPerfil)
        END

        IF NOT EXISTS (SELECT * FROM TB_CA_ELEMENTO_PERFIL WHERE IDELEMENTO = @id AND IDPERFIL = @idPerfilCad) 
        BEGIN
            INSERT INTO TB_CA_ELEMENTO_PERFIL VALUES (@id, @idPerfilCad)
        END
        

        -- ADICIONA ITEM - FICHA CADASTRAL
        IF EXISTS (SELECT * FROM TB_CA_ELEMENTO WHERE NOME = 'RH.Account.RegistrationForm') 
        BEGIN
            SELECT @id = ID FROM TB_CA_ELEMENTO WHERE NOME = 'RH.Account.RegistrationForm'
        END
        ELSE
            BEGIN
                INSERT INTO TB_CA_ELEMENTO VALUES ('RH.Account.RegistrationForm', 'Ficha Cadastral', @id, 1, 1, 1)
                SET @id = CAST(SCOPE_IDENTITY() AS INT)
            END

        IF NOT EXISTS (SELECT * FROM TB_CA_ELEMENTO_PERFIL WHERE IDELEMENTO = @id AND IDPERFIL = @idPerfil) 
        BEGIN
            INSERT INTO TB_CA_ELEMENTO_PERFIL VALUES (@id, @idPerfil)
        END

        IF NOT EXISTS (SELECT * FROM TB_CA_ELEMENTO_PERFIL WHERE IDELEMENTO = @id AND IDPERFIL = @idPerfilCad) 
        BEGIN
            INSERT INTO TB_CA_ELEMENTO_PERFIL VALUES (@id, @idPerfilCad)
        END


        -- ADICIONA ITEM - INFORMAÇÕES FINANCEIRAS
        IF EXISTS (SELECT * FROM TB_CA_ELEMENTO WHERE NOME = 'RH.Account.FinancialInformation') 
        BEGIN
            SELECT @id = ID FROM TB_CA_ELEMENTO WHERE NOME = 'RH.Account.FinancialInformation'
        END
        ELSE
            BEGIN
                INSERT INTO TB_CA_ELEMENTO VALUES ('RH.Account.FinancialInformation', 'Informações Financeiras', @id, 1, 1, 1)
                SET @id = CAST(SCOPE_IDENTITY() AS INT)
            END

        IF NOT EXISTS (SELECT * FROM TB_CA_ELEMENTO_PERFIL WHERE IDELEMENTO = @id AND IDPERFIL = @idPerfil) 
        BEGIN
            INSERT INTO TB_CA_ELEMENTO_PERFIL VALUES (@id, @idPerfil)
        END

        IF NOT EXISTS (SELECT * FROM TB_CA_ELEMENTO_PERFIL WHERE IDELEMENTO = @id AND IDPERFIL = @idPerfilCad) 
        BEGIN
            INSERT INTO TB_CA_ELEMENTO_PERFIL VALUES (@id, @idPerfilCad)
        END


        -- ADICIONA ITEM - DOCUMENTOS
        IF EXISTS (SELECT * FROM TB_CA_ELEMENTO WHERE NOME = 'RH.Account.Documents') 
        BEGIN
            SELECT @id = ID FROM TB_CA_ELEMENTO WHERE NOME = 'RH.Account.Documents'
        END
        ELSE
            BEGIN
                INSERT INTO TB_CA_ELEMENTO VALUES ('RH.Account.Documents', 'Documentos', @id, 1, 1, 1)
                SET @id = CAST(SCOPE_IDENTITY() AS INT)
            END

        IF NOT EXISTS (SELECT * FROM TB_CA_ELEMENTO_PERFIL WHERE IDELEMENTO = @id AND IDPERFIL = @idPerfil) 
        BEGIN
            INSERT INTO TB_CA_ELEMENTO_PERFIL VALUES (@id, @idPerfil)
        END

        IF NOT EXISTS (SELECT * FROM TB_CA_ELEMENTO_PERFIL WHERE IDELEMENTO = @id AND IDPERFIL = @idPerfilCad) 
        BEGIN
            INSERT INTO TB_CA_ELEMENTO_PERFIL VALUES (@id, @idPerfilCad)
        END

        -- ADICIONA ITEM - Representante
        IF EXISTS (SELECT * FROM TB_CA_ELEMENTO WHERE NOME = 'RH.Account.Representation') 
        BEGIN
            SELECT @id = ID FROM TB_CA_ELEMENTO WHERE NOME = 'RH.Account.Representation'
        END
        ELSE
            BEGIN
                INSERT INTO TB_CA_ELEMENTO VALUES ('RH.Account.Representation', 'Representante', @id, 1, 1, 1)
                SET @id = CAST(SCOPE_IDENTITY() AS INT)
            END

        IF NOT EXISTS (SELECT * FROM TB_CA_ELEMENTO_PERFIL WHERE IDELEMENTO = @id AND IDPERFIL = @idPerfil) 
        BEGIN
            INSERT INTO TB_CA_ELEMENTO_PERFIL VALUES (@id, @idPerfil)
        END

        IF NOT EXISTS (SELECT * FROM TB_CA_ELEMENTO_PERFIL WHERE IDELEMENTO = @id AND IDPERFIL = @idPerfilCad) 
        BEGIN
            INSERT INTO TB_CA_ELEMENTO_PERFIL VALUES (@id, @idPerfilCad)
        END


        -- ADICIONA ITEM - PODERES
        IF EXISTS (SELECT * FROM TB_CA_ELEMENTO WHERE NOME = 'RH.Account.Rules') 
        BEGIN
            SELECT @id = ID FROM TB_CA_ELEMENTO WHERE NOME = 'RH.Account.Rules'
        END
        ELSE
            BEGIN
                INSERT INTO TB_CA_ELEMENTO VALUES ('RH.Account.Rules', 'Poderes', @id, 1, 1, 1)
                SET @id = CAST(SCOPE_IDENTITY() AS INT)
            END

        IF NOT EXISTS (SELECT * FROM TB_CA_ELEMENTO_PERFIL WHERE IDELEMENTO = @id AND IDPERFIL = @idPerfil) 
        BEGIN
            INSERT INTO TB_CA_ELEMENTO_PERFIL VALUES (@id, @idPerfil)
        END

        IF NOT EXISTS (SELECT * FROM TB_CA_ELEMENTO_PERFIL WHERE IDELEMENTO = @id AND IDPERFIL = @idPerfilCad) 
        BEGIN
            INSERT INTO TB_CA_ELEMENTO_PERFIL VALUES (@id, @idPerfilCad)
        END

        -- ADICIONA ITEM - ENVIAR
        IF EXISTS (SELECT * FROM TB_CA_ELEMENTO WHERE NOME = 'RH.Account.FinishRegistration') 
        BEGIN
            SELECT @id = ID FROM TB_CA_ELEMENTO WHERE NOME = 'RH.Account.FinishRegistration'
        END
        ELSE
            BEGIN
                INSERT INTO TB_CA_ELEMENTO VALUES ('RH.Account.FinishRegistration', 'Finalizar', @id, 1, 1, 1)
                SET @id = CAST(SCOPE_IDENTITY() AS INT)
            END

        IF NOT EXISTS (SELECT * FROM TB_CA_ELEMENTO_PERFIL WHERE IDELEMENTO = @id AND IDPERFIL = @idPerfil) 
        BEGIN
            INSERT INTO TB_CA_ELEMENTO_PERFIL VALUES (@id, @idPerfil)
        END

        IF NOT EXISTS (SELECT * FROM TB_CA_ELEMENTO_PERFIL WHERE IDELEMENTO = @id AND IDPERFIL = @idPerfilCad) 
        BEGIN
            INSERT INTO TB_CA_ELEMENTO_PERFIL VALUES (@id, @idPerfilCad)
        END

        -- ADICIONA CADASTRO DOCUMENTO
        IF EXISTS (SELECT * FROM TB_CA_ELEMENTO WHERE NOME = 'ClientRegistration.ClientDocument') 
        BEGIN
            SELECT @id = ID FROM TB_CA_ELEMENTO WHERE NOME = 'ClientRegistration.ClientDocument'
        END
        ELSE
            BEGIN
                INSERT INTO TB_CA_ELEMENTO VALUES ('ClientRegistration.ClientDocument', 'Documento', @idCadOnline, 1, 1, 1)
                SET @id = CAST(SCOPE_IDENTITY() AS INT)
            END

        IF NOT EXISTS (SELECT * FROM TB_CA_ELEMENTO_PERFIL WHERE IDELEMENTO = @id AND IDPERFIL = @idPerfilCad) 
        BEGIN
            INSERT INTO TB_CA_ELEMENTO_PERFIL VALUES (@id, @idPerfilCad)
        END

        -- ADICIONA CLIENTE EM ANÁLISE
        IF EXISTS (SELECT * FROM TB_CA_ELEMENTO WHERE NOME = 'ClientRegistration.ClientAnalyze') 
        BEGIN
            SELECT @id = ID FROM TB_CA_ELEMENTO WHERE NOME = 'ClientRegistration.ClientAnalyze'
        END
        ELSE
            BEGIN
                INSERT INTO TB_CA_ELEMENTO VALUES ('ClientRegistration.ClientAnalyze', 'Em análise', @idCadOnline, 1, 1, 1)
                SET @id = CAST(SCOPE_IDENTITY() AS INT)
            END

        IF NOT EXISTS (SELECT * FROM TB_CA_ELEMENTO_PERFIL WHERE IDELEMENTO = @id AND IDPERFIL = @idPerfilCad) 
        BEGIN
            INSERT INTO TB_CA_ELEMENTO_PERFIL VALUES (@id, @idPerfilCad)
        END

        -- ADICIONA CLIENTE APROVADOS
        IF EXISTS (SELECT * FROM TB_CA_ELEMENTO WHERE NOME = 'ClientRegistration.ClientApproved') 
        BEGIN
            SELECT @id = ID FROM TB_CA_ELEMENTO WHERE NOME = 'ClientRegistration.ClientApproved'
        END
        ELSE
            BEGIN
                INSERT INTO TB_CA_ELEMENTO VALUES ('ClientRegistration.ClientApproved', 'Aprovados', @idCadOnline, 1, 1, 1)
                SET @id = CAST(SCOPE_IDENTITY() AS INT)
            END

        IF NOT EXISTS (SELECT * FROM TB_CA_ELEMENTO_PERFIL WHERE IDELEMENTO = @id AND IDPERFIL = @idPerfilCad) 
        BEGIN
            INSERT INTO TB_CA_ELEMENTO_PERFIL VALUES (@id, @idPerfilCad)
        END

        -- ADICIONA CLIENTE REPROVADOS
        IF EXISTS (SELECT * FROM TB_CA_ELEMENTO WHERE NOME = 'ClientRegistration.ClientDisapproved') 
        BEGIN
            SELECT @id = ID FROM TB_CA_ELEMENTO WHERE NOME = 'ClientRegistration.ClientDisapproved'
        END
        ELSE
            BEGIN
                INSERT INTO TB_CA_ELEMENTO VALUES ('ClientRegistration.ClientDisapproved', 'Reprovados', @idCadOnline, 1, 1, 1)
                SET @id = CAST(SCOPE_IDENTITY() AS INT)
            END

        IF NOT EXISTS (SELECT * FROM TB_CA_ELEMENTO_PERFIL WHERE IDELEMENTO = @id AND IDPERFIL = @idPerfilCad) 
        BEGIN
            INSERT INTO TB_CA_ELEMENTO_PERFIL VALUES (@id, @idPerfilCad)
        END

        -- ADICIONA CLIENTE EM PREENCHIMENTO
        IF EXISTS (SELECT * FROM TB_CA_ELEMENTO WHERE NOME = 'ClientRegistration.ClientFilling') 
        BEGIN
            SELECT @id = ID FROM TB_CA_ELEMENTO WHERE NOME = 'ClientRegistration.ClientFilling'
        END
        ELSE
            BEGIN
                INSERT INTO TB_CA_ELEMENTO VALUES ('ClientRegistration.ClientFilling', 'Em preenchimento', @idCadOnline, 1, 1, 1)
                SET @id = CAST(SCOPE_IDENTITY() AS INT)
            END


        IF NOT EXISTS (SELECT * FROM TB_CA_ELEMENTO_PERFIL WHERE IDELEMENTO = @id AND IDPERFIL = @idPerfilCad) 
        BEGIN
            INSERT INTO TB_CA_ELEMENTO_PERFIL VALUES (@id, @idPerfilCad)
        END

        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_CLIENTE_ONLINE_STATUS_CADASTRO')
        BEGIN
            CREATE TABLE TB_CLIENTE_ONLINE_STATUS_CADASTRO(
                ID_CLIENTE                  INT NOT NULL,
                ID_STATUS_CLIENTE_ONLINE    INT NOT NULL
        )
        END


        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_CLIENTE_ONLINE_STATUS')
        BEGIN
            CREATE TABLE TB_CLIENTE_ONLINE_STATUS(
                ID              INT NOT NULL,
                DESCRICAO       VARCHAR(200) NOT NULL
        )
        END
        

        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_CLIENTE_ONLINE_FICHA_CADASTRAL')
        BEGIN
            CREATE TABLE TB_CLIENTE_ONLINE_FICHA_CADASTRAL(
                ID                  INT IDENTITY(1,1),
                ID_CLIENTE_ONLINE   INT NOT NULL,
                GRUPO_ECONOMICO     VARCHAR(200),
                CLASSIFICACAO       VARCHAR(200),
                GRUPO_ATIVIDADE     VARCHAR(200),
                TIPO_CADASTRO       VARCHAR(200),
                TIPO_CLIENTE        VARCHAR(200),
                CATEGORIA           VARCHAR(200),
                ATIVIDADE_PRINCIPAL VARCHAR(200),
                NATUREZA_OPERACAO   VARCHAR(200),
                PORTE               VARCHAR(200),
                DATA_NASC           DATE NULL,
                PROFISSAO           VARCHAR(200),
                NACIONALIDADE       VARCHAR(200),
                GENERO              VARCHAR(20),
                REGIME_CASAMENTO    VARCHAR(200),
                UNIAO_ESTAVEL       VARCHAR(200),
                ESTADO_CIVIL        VARCHAR(50),
                NOME_PAI            VARCHAR(200),
                NOME_MAE            VARCHAR(200),
                NOME_CONJUGUE       VARCHAR(200),
                CPF_CONJUGUE        VARCHAR(20),
                NASC_CONJUGUE       DATE,
                ESTRANGEIRO         VARCHAR(200),
                OBS                 VARCHAR(200),
                PAIS_NASCIMENTO     VARCHAR(200),
                PAIS_NASC_ESTR      VARCHAR(200),            
                ESTADO_NASCIMENTO   VARCHAR(200),
                ESTADO_NASC_ESTR    VARCHAR(200),
                NATURALIDADE        VARCHAR(200),
                NATURALIDADE_ESTR   VARCHAR(200),
                TIPO_DOC            VARCHAR(200),
                NUMERO_DOC          VARCHAR(200),
                ORGAO_EMISSOR_DOC   VARCHAR(200),
                DATA_VENCIMENTO_DOC DATE,
                DATA_EMISSAO_DOC    DATE,
                ESTADO_EMISSAO_DOC  VARCHAR(200),
                NOME_EMPRESA        VARCHAR(200),
                DATA_ADMISSAO       DATE,
                ENDERECO_EMPRESA    VARCHAR(200),
                GERENTE_REL         VARCHAR(200),
                DATA_INICIO_REL     DATE,
                DATA_FIM_REL        DATE,
                CARGO_PUBL          VARCHAR(200),
                NOME_CARGO_PUBL     VARCHAR(200),
                NOME_EMPRESA_PUBL   VARCHAR(200),
                DATA_INICIO_PUBL    DATE,
                DATA_FIM_PUBL       DATE,
                PARENTE_EMP_PUBL    VARCHAR(200),
                NOME_PARENTE        VARCHAR(200),
                TP_PARENTE          VARCHAR(200),
                CARGO_PARENTE_PUBL  VARCHAR(200),
                TEM_REPRESENTANTE   VARCHAR(200),
                ADMINISTRADOR       VARCHAR(200),
                STATUS              VARCHAR(200),
                DATA_CRIACAO        DATETIME NOT NULL,
                DATA_ALTERACAO      DATETIME NOT NULL
            )
        END

        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_CLIENTE_ONLINE_ENDERECO')
        BEGIN
            CREATE TABLE TB_CLIENTE_ONLINE_ENDERECO(
                ID                  INT IDENTITY(1,1),
                ID_CLIENTE_ONLINE   INT NOT NULL,
                TIPO_ENDERECO       VARCHAR(200),
                FINALIDADE          VARCHAR(250),
                TIPO_LOGRADOURO     VARCHAR(100),
                LOGRADOURO          VARCHAR(100),
                NUMERO              VARCHAR(10),
                ESTADO              VARCHAR(100),
                CIDADE              VARCHAR(100),
                PAIS                VARCHAR(100)
            )
        END

        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_CLIENTE_ONLINE_CONTATO')
        BEGIN
            CREATE TABLE TB_CLIENTE_ONLINE_CONTATO(
                ID                  INT IDENTITY(1, 1),
                ID_CLIENTE_ONLINE   INT NOT NULL,
                TIPO                VARCHAR(200),
                DDI                 VARCHAR(200),
                DDD                 VARCHAR(200),
                NUMERO              VARCHAR(200),
                RAMAL               VARCHAR(200),
                EMAIL_CONTATO       VARCHAR(200)
            )
        END

        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_CLIENTE_ONLINE_EMAIL')
        BEGIN
            CREATE TABLE TB_CLIENTE_ONLINE_EMAIL(
                ID                  INT IDENTITY(1, 1),
                ID_CLIENTE_ONLINE   INT NOT NULL,
                EMAIL               VARCHAR(200)
            )
        END

        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_CLIENTE_ONLINE_ASSINATURA')
        BEGIN
            CREATE TABLE TB_CLIENTE_ONLINE_ASSINATURA(
                ID                  INT IDENTITY(1, 1),
                ID_CLIENTE_ONLINE   INT NOT NULL,
                NOME                VARCHAR(200),
                EXTENSAO            VARCHAR(200),
                ARQUIVO             VARBINARY(MAX)
            )
        END

        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_ESTADO')
        BEGIN
            CREATE TABLE TB_ESTADO(
                ID                  INT IDENTITY(1, 1),
                ID_EXTERNO          INT,
                DESCRICAO           VARCHAR(200) NOT NULL
            )
        END

        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_PAIS')
        BEGIN
            CREATE TABLE TB_PAIS(
                ID                  INT IDENTITY(1, 1),
                ID_EXTERNO          INT,
                DESCRICAO           VARCHAR(200) NOT NULL
            )
        END

        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_TIPO_ENDERECO')
        BEGIN
            CREATE TABLE TB_TIPO_ENDERECO(
                ID                  INT IDENTITY(1, 1),
                ID_EXTERNO          INT,
                DESCRICAO           VARCHAR(200) NOT NULL
            )
        END

        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_FINALIDADE')
        BEGIN
            CREATE TABLE TB_FINALIDADE(
                ID                  INT IDENTITY(1, 1),
                ID_EXTERNO          INT,
                DESCRICAO           VARCHAR(200) NOT NULL
            )
        END

        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_ATIVIDADE')
        BEGIN
            CREATE TABLE TB_ATIVIDADE(
                ID                  INT IDENTITY(1, 1),
                ID_EXTERNO          INT,
                DESCRICAO           VARCHAR(200) NOT NULL
            )
        END

        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_OCUPACAO')
        BEGIN
            CREATE TABLE TB_OCUPACAO(
                ID                  INT IDENTITY(1, 1),
                ID_EXTERNO          INT,
                DESCRICAO           VARCHAR(200) NOT NULL
            )
        END

        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_PORTE')
        BEGIN
            CREATE TABLE TB_PORTE(
                ID                  INT IDENTITY(1, 1),
                ID_EXTERNO          INT,
                DESCRICAO           VARCHAR(200) NOT NULL
            )
        END

        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_PROFISSAO')
        BEGIN
            CREATE TABLE TB_PROFISSAO(
                ID                  INT IDENTITY(1, 1),
                ID_EXTERNO          INT,
                DESCRICAO           VARCHAR(200) NOT NULL
            )
        END

        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_ESTADO_CIVIL')
        BEGIN
            CREATE TABLE TB_ESTADO_CIVIL(
                ID                  INT IDENTITY(1, 1),
                ID_EXTERNO          INT,
                DESCRICAO           VARCHAR(200) NOT NULL
            )
        END

        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_SEXO')
        BEGIN
            CREATE TABLE TB_SEXO(
                ID                  INT IDENTITY(1, 1),
                ID_EXTERNO          INT,
                DESCRICAO           VARCHAR(200) NOT NULL
            )
        END

        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_STATUS')
        BEGIN
            CREATE TABLE TB_STATUS(
                ID                  INT IDENTITY(1, 1),
                ID_EXTERNO          INT,
                DESCRICAO           VARCHAR(200) NOT NULL
            )
        END

        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_TIPO_DOCUMENTO')
        BEGIN
            CREATE TABLE TB_TIPO_DOCUMENTO(
                ID                  INT IDENTITY(1, 1),
                ID_EXTERNO          INT,
                DESCRICAO           VARCHAR(200) NOT NULL
            )
        END

        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_GRUPO_ECONOMICO')
        BEGIN
            CREATE TABLE TB_GRUPO_ECONOMICO(
                ID                  INT IDENTITY(1, 1),
                ID_EXTERNO          INT,
                DESCRICAO           VARCHAR(200) NOT NULL
            )
        END

        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_CLASSIFICACAO')
        BEGIN
            CREATE TABLE TB_CLASSIFICACAO(
                ID                  INT IDENTITY(1, 1),
                ID_EXTERNO          INT,
                DESCRICAO           VARCHAR(200) NOT NULL
            )
        END

        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_ATIVIDADE_ECONOMICA')
        BEGIN
            CREATE TABLE TB_ATIVIDADE_ECONOMICA(
                ID                  INT IDENTITY(1, 1),
                ID_EXTERNO          INT,
                DESCRICAO           VARCHAR(200) NOT NULL
            )
        END

        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_TIPO_REGISTRO')
        BEGIN
            CREATE TABLE TB_TIPO_REGISTRO(
                ID                  INT IDENTITY(1, 1),
                ID_EXTERNO          INT,
                DESCRICAO           VARCHAR(200) NOT NULL
            )
        END

        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_TIPO_CLIENTE')
        BEGIN
            CREATE TABLE TB_TIPO_CLIENTE(
                ID                  INT IDENTITY(1, 1),
                ID_EXTERNO          INT,
                DESCRICAO           VARCHAR(200) NOT NULL
            )
        END

        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_CATEGORIA')
        BEGIN
            CREATE TABLE TB_CATEGORIA(
                ID                  INT IDENTITY(1, 1),
                ID_EXTERNO          INT,
                DESCRICAO           VARCHAR(200) NOT NULL
            )
        END

        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_GERENTE')
        BEGIN
            CREATE TABLE TB_GERENTE(
                ID                  INT IDENTITY(1, 1),
                ID_EXTERNO          INT,
                DESCRICAO           VARCHAR(200) NOT NULL
            )
        END

        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_NATURALIDADE')
        BEGIN
            CREATE TABLE TB_NATURALIDADE(
                ID                  INT IDENTITY(1, 1),
                ID_EXTERNO          INT,
                DESCRICAO           VARCHAR(200) NOT NULL
            )
        END

        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_NACIONALIDADE')
        BEGIN
            CREATE TABLE TB_NACIONALIDADE(
                ID                  INT IDENTITY(1, 1),
                ID_EXTERNO          INT,
                DESCRICAO           VARCHAR(200) NOT NULL
            )
        END

        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_TIPO_TELEFONE')
        BEGIN
            CREATE TABLE TB_TIPO_TELEFONE(
                ID                  INT IDENTITY(1, 1),
                ID_EXTERNO          INT,
                DESCRICAO           VARCHAR(200) NOT NULL
            )
        END

        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_REGIME_CASAMENTO')
        BEGIN
            CREATE TABLE TB_REGIME_CASAMENTO(
                ID                  INT IDENTITY(1, 1),
                ID_EXTERNO          INT,
                DESCRICAO           VARCHAR(200) NOT NULL
            )
        END


        ----------------*************  DADOS FINANCEIROS ***************------------------------------------------

        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_CLIENTE_ONLINE_FINANCEIRO')
        BEGIN
            CREATE TABLE TB_CLIENTE_ONLINE_FINANCEIRO(
                ID                      INT IDENTITY(1, 1),
                ID_CLIENTE_ONLINE       INT NOT NULL,
                RENDA_MENSAL            VARCHAR(200),
                RENDA_MENSAL_FAMILIAR   VARCHAR(200),
                OUTRAS_RENDAS           VARCHAR(200),
                IMOVEL_PATRIMONIO       VARCHAR(200),
                VEICULO_PATRIMONIO      VARCHAR(200),
                APLICACAO_PATRIMONIO    VARCHAR(200),
                APLICACAO_ORIGEM        BIT,
                INDENIZACAO_ORIGEM      BIT,
                PREMIO_ORIGEM           BIT,
                RESCISAO_ORIGEM         BIT,
                SALARIO_ORIGEM          BIT,
                VENDA_ORIGEM            BIT,
                JOIAS_ORIGEM            BIT,
                ALUGUEL_ORIGEM          BIT,
                DOACAO_ORIGEM           BIT,
                HERANCA_ORIGEM          BIT,
                AUTOMOVEL_ORIGEM        BIT,
                INDENIZACAO_SEG_ORIGEM  BIT,
                PRODUCAO_AGRI_ORIGEM    BIT,
                IMOVEL_ORIGEM           BIT,
                OUTROS_ORIGEM           BIT,
                OUTROS_DESCRICAO        VARCHAR(200),
                ID_NATUREZA_FISCAL      INT
            )
        END

        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_CLIENTE_ONLINE_FINANCEIRO_CONTA')
        BEGIN
            CREATE TABLE TB_CLIENTE_ONLINE_FINANCEIRO_CONTA(
                ID_CLIENTE_ONLINE       INT NOT NULL,
                ID_FINALIDADE           INT,
                ID_UTILIZACAO           INT,
                ID_TIPO_CONTA           INT,
                ID_TIPO_CONTA_BANCO     INT,
                ID_BANCO                INT,
                AGENCIA                 VARCHAR(200),
                NUMEROCONTA             VARCHAR(200)
            )
        END

        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_CLIENTE_ONLINE_FINANCEIRO_PRODUTO')
        BEGIN
            CREATE TABLE TB_CLIENTE_ONLINE_FINANCEIRO_PRODUTO(
                ID_CLIENTE_ONLINE   INT NOT NULL,
                ID_PRODUTO          INT
            )
        END

        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_PRODUTO')
        BEGIN
            CREATE TABLE TB_PRODUTO(
                ID                  INT IDENTITY(1, 1),
                ID_EXTERNO          INT,
                DESCRICAO           VARCHAR(200) NOT NULL
            )
        END

        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_NATUREZA_FISCAL')
        BEGIN
            CREATE TABLE TB_NATUREZA_FISCAL(
                ID                  INT IDENTITY(1, 1),
                ID_EXTERNO          INT,
                DESCRICAO           VARCHAR(200) NOT NULL
            )
        END

        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_FINALIDADE_CONTA')
        BEGIN
            CREATE TABLE TB_FINALIDADE_CONTA(
                ID                  INT IDENTITY(1, 1),
                ID_EXTERNO          INT,
                DESCRICAO           VARCHAR(200) NOT NULL
            )
        END

        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_UTILIZACAO_CONTA')
        BEGIN
            CREATE TABLE TB_UTILIZACAO_CONTA(
                ID                  INT IDENTITY(1, 1),
                ID_EXTERNO          INT,
                DESCRICAO           VARCHAR(200) NOT NULL
            )
        END

        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_TIPO_CONTA')
        BEGIN
            CREATE TABLE TB_TIPO_CONTA(
                ID                  INT IDENTITY(1, 1),
                ID_EXTERNO          INT,
                DESCRICAO           VARCHAR(200) NOT NULL
            )
        END

        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_TIPO_CONTA_BANCO')
        BEGIN
            CREATE TABLE TB_TIPO_CONTA_BANCO(
                ID                  INT IDENTITY(1, 1),
                ID_EXTERNO          INT,
                DESCRICAO           VARCHAR(200) NOT NULL
            )
        END

        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_BANCO')
        BEGIN
            CREATE TABLE TB_BANCO(
                ID                  INT IDENTITY(1, 1),
                ID_EXTERNO          INT,
                DESCRICAO           VARCHAR(200) NOT NULL
            )
        END

        ----------------************* DOCUMENTOS ***************---------------------------------------------
        
        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_CLIENTE_ONLINE_DOCUMENTO')
        BEGIN
            CREATE TABLE TB_CLIENTE_ONLINE_DOCUMENTO(
                ID_CLIENTE_ONLINE       INT NOT NULL,
                ID_DOCUMENTO            INT NOT NULL,
                NOME                    VARCHAR(200) NOT NULL,
                EXTENSAO                VARCHAR(200) NOT NULL,
                ARQUIVO                 VARBINARY(MAX) NOT NULL,
                TEM_FISICO              BIT NOT NULL
            )
        END


        ----------------************* REPRESENTANTE ***************------------------------------------------

        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_FORMA_REPRESENTACAO')
        BEGIN
            CREATE TABLE TB_FORMA_REPRESENTACAO(
                ID                  INT IDENTITY(1, 1),
                ID_EXTERNO          INT,
                DESCRICAO           VARCHAR(200) NOT NULL
            )
        END

        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_PERFIL_REPRESENTANTE')
        BEGIN
            CREATE TABLE TB_PERFIL_REPRESENTANTE(
                ID                  INT IDENTITY(1, 1),
                ID_EXTERNO          INT,
                DESCRICAO           VARCHAR(200) NOT NULL
            )
        END

        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_CLIENTE_ONLINE_REPRESENTANTE')
        BEGIN
            CREATE TABLE TB_CLIENTE_ONLINE_REPRESENTANTE(
                ID                              INT IDENTITY(1, 1),
                ID_CLIENTE_ONLINE               INT NOT NULL,
                NOME                            VARCHAR(200),
                ID_FORMA_REPRESENTACAO          INT,
                ID_TIPO_DOCUMENTO               INT,
                NUMERO_DOCUMENTO                VARCHAR(40),
                ORGAO_EMISSOR                   VARCHAR(100),
                DATA_VALIDADE_DOCUMENTO         DATE,
                DATA_EMISSAO_DOCUMENTO          DATE,
                DATA_NASCIMENTO                 DATE,
                LOCAL_NASCIMENTO                VARCHAR(200),
                NACIONALIDADE                   VARCHAR(200),
                ID_ESTADO_CIVIL                 INT,
                ID_PROFISSAO                    INT,
                QUALIFICACAOCSS                 DATETIME,
                DATAVALIDADECSS                 DATETIME,
                FUNCAO_GOVERNO                  BIT,
                ID_PERFIL                       INT,
                NOME_CARGO_GOVERNO              VARCHAR(200),
                NOME_EMPRESA_GOVERNO            VARCHAR(200),
                INICIO_FUNCAO_GOVERNO           DATE,
                FIM_FUNCAO_GOVERNO              DATE,
                PARENTE_FUNCAO_GOVERNO          BIT,
                NOME_PARENTE                    VARCHAR(200),
                RELACIONAMENTO                  VARCHAR(100),
                NOME_FUNCAO_PARENTE_GOVERNO     VARCHAR(200),
                NOME_EMPRESA_PARENTE_GOVERNO    VARCHAR(200),
                VINCULO_BRPARTNERS              BIT,
                RESIDENCIA_FISCAL               BIT,
                NOME_PAIS_RESIDENCIA_FISCAL     VARCHAR(200),
                NOME_PAIS_RESIDENCIA            VARCHAR(200)                       
            )
        END

        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_CLIENTE_ONLINE_REPRESENTANTE_ENDERECO')
        BEGIN
            CREATE TABLE TB_CLIENTE_ONLINE_REPRESENTANTE_ENDERECO(
                ID_CLIENTE_ONLINE   INT NOT NULL,
                ID_TIPO_ENDERECO    INT,
                ID_FINALIDADE       INT,
                TIPO_LOGRADOURO     VARCHAR(100),
                LOGRADOURO          VARCHAR(100),
                NUMERO              VARCHAR(10),
                ESTADO              VARCHAR(100),
                CIDADE              VARCHAR(100),
                PAIS                VARCHAR(100)
            )
        END

        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_CLIENTE_ONLINE_REPRESENTANTE_CONTATO')
        BEGIN
            CREATE TABLE TB_CLIENTE_ONLINE_REPRESENTANTE_CONTATO(
                ID_CLIENTE_ONLINE   INT NOT NULL,
                ID_TIPO_CONTATO     INT,
                DDI                 VARCHAR(200),
                DDD                 VARCHAR(200),
                NUMERO              VARCHAR(200),
                RAMAL               VARCHAR(200),
                EMAIL_CONTATO       VARCHAR(200)
            )
        END

        --------------------------************* FATCA ***************------------------------------------------

        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_INFORMACAO_FISCAL_FATCA')
        BEGIN
            CREATE TABLE TB_INFORMACAO_FISCAL_FATCA(
                ID                  INT IDENTITY(1, 1),
                ID_EXTERNO          INT,
                DESCRICAO           VARCHAR(200) NOT NULL
            )
        END
        
        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_ESCOLHA')
        BEGIN
            CREATE TABLE TB_ESCOLHA(
                ID                  INT IDENTITY(1, 1),
                ID_EXTERNO          INT,
                DESCRICAO           VARCHAR(200) NOT NULL
            )
        END

        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_CLIENTE_ONLINE_FATCA')
        BEGIN
            CREATE TABLE TB_CLIENTE_ONLINE_FATCA(
                ID_CLIENTE_ONLINE       INT NOT NULL,
                FATCA                   VARCHAR(200),
                TIN                     VARCHAR(200),
                PAIS_EMISSOR            VARCHAR(200),
                ID_INFORMACAO_FISCAL    VARCHAR(200)
            )
        END

        --------------------------************* PODERES ***************------------------------------------------

        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_QUALIFICACAO')
        BEGIN
            CREATE TABLE TB_QUALIFICACAO(
                ID                  INT IDENTITY(1, 1),
                ID_EXTERNO          INT,
                DESCRICAO           VARCHAR(200) NOT NULL
            )
        END

        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_TIPO_DOCUMENTO_PODER')
        BEGIN
            CREATE TABLE TB_TIPO_DOCUMENTO_PODER(
                ID                  INT IDENTITY(1, 1),
                ID_EXTERNO          INT,
                DESCRICAO           VARCHAR(200) NOT NULL
            )
        END

        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_CLIENTE_ONLINE_PODER')
        BEGIN
            CREATE TABLE TB_CLIENTE_ONLINE_PODER(
                ID                      INT IDENTITY(1, 1),
                ID_CLIENTE_ONLINE       INT NOT NULL,
                ID_DOCUMENTO            INT NOT NULL,
                ID_TIPO                 INT NOT NULL,
                DATA_VALIDADE_INICIO    DATE,
                DATA_VALIDADE_FIM       DATE,
                ID_REPRESENTANTE        INT,
                NUMERO_DOCUMENTO        VARCHAR(200),
                ID_QUALIFICACAO         INT NOT NULL,
                ID_STATUS               INT NOT NULL
            )
        END

        --------------------------************* CADASTRO DE DOCUMENTOS ***************------------------------------------------
        
        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_DOCUMENTO')
        BEGIN
            CREATE TABLE TB_DOCUMENTO(
                ID                          INT IDENTITY(1, 1),
                NOME_ARQUIVO                VARCHAR(200) NOT NULL,
                INSTRUCAO_USO               VARCHAR(300) NULL,
                OBRIGATORIO                 BIT NOT NULL,
                ID_DOCUMENTO_VALIDADE       INT NOT NULL,
                DATA_VALIDADE               DATE NOT NULL,
                PESSOA_FISICA               BIT NULL,
                PESSOA_JURIDICA             BIT NULL,
                FUNDOS                      BIT NULL,
                DESCRICAO_ARQUIVO           VARCHAR(200) NOT NULL,
                EXTENSAO                    VARCHAR(200) NOT NULL,
                ARQUIVO                     VARBINARY(MAX) NOT NULL,
                STATUS                      INT NOT NULL
            )
        END

        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_DOCUMENTO_PRODUTO')
        BEGIN
            CREATE TABLE TB_DOCUMENTO_PRODUTO(
                ID_DOCUMENTO    INT NOT NULL,
                ID_PRODUTO      INT NOT NULL
            )
        END

        IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TB_DOCUMENTO_VALIDADE')
        BEGIN
            CREATE TABLE TB_DOCUMENTO_VALIDADE(
                ID                  INT IDENTITY(1, 1),
                ID_EXTERNO          INT,
                MESES           VARCHAR(200) NOT NULL
            )
        END

    COMMIT
END TRY
BEGIN CATCH
    ROLLBACK
END CATCH