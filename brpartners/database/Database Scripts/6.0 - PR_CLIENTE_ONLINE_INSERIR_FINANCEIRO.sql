CREATE PROCEDURE PR_CLIENTE_ONLINE_INSERIR_FINANCEIRO(
    @idClienteOnline        INT,
    @rendaMensal            VARCHAR(200) = NULL,
    @rendaFamilia           VARCHAR(200) = NULL,
    @outrasRendas           VARCHAR(200) = NULL,
    @imovelPatrimonio       VARCHAR(200) = NULL,
    @veiculoPatrimonio      VARCHAR(200) = NULL,
    @aplicacaoPatrimonio    VARCHAR(200) = NULL,
    @aplicacaoOrigem        BIT,
    @indenizacao            BIT,
    @premio                 BIT,
    @rescisao               BIT,
    @salario                BIT,
    @venda                  BIT,
    @joias                  BIT,
    @aluguel                BIT,
    @doacao                 BIT,
    @heranca                BIT,
    @automovel              BIT,
    @seguro                 BIT,
    @producao               BIT,
    @imovel                 BIT,
    @outros                 BIT,
    @outrosDescricao        VARCHAR(200) = NULL,
    @idNatureza             INT
)
AS
BEGIN
    INSERT INTO TB_CLIENTE_ONLINE_FINANCEIRO VALUES 
    (
        @idClienteOnline, @rendaMensal, @rendaFamilia, @outrasRendas, @imovelPatrimonio, @veiculoPatrimonio,
        @aplicacaoPatrimonio, @aplicacaoOrigem, @indenizacao, @premio, @rescisao, @salario, @venda, @joias,
        @aluguel, @doacao, @heranca, @automovel, @seguro, @producao, @imovel, @outros, @outrosDescricao, @idNatureza
    )
END