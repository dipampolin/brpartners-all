CREATE PROCEDURE PR_CLIENTE_ONLINE_INSERIR_ATUALIZAR_REPRESENTANTE(
    @id                         INT = null,
    @idClienteOnline            INT,
    @nome                       VARCHAR(200) = NULL,
    @idFormaRepresentacao       INT = 0,
    @idTipoDocumento            INT = 0,
    @numeroDocumento            VARCHAR(200) = NULL,
    @orgaoEmissor               VARCHAR(200) = NULL,
    @dataValidadeDoc            DATE = NULL,
    @dataEmissoaDoc             DATE = NULL,
    @dataNasc                   DATE = NULL,
    @localNasc                  VARCHAR(200) = NULL,
    @nacionalidade              VARCHAR(200) = NULL,
    @idEstadoCivil              INT = NULL,
    @idProfissao                INT = NULL,
    @qualificacaoCss            DATETIME = NULL,
    @dataValidadeCss            DATETIME = NULL,
    @funcaoGoverno              BIT = NULL,
    @idPerfil                   INT = NULL,
    @nomeCargoGoverno           VARCHAR(200) = NULL,
    @nomeEmpresaGoverno         VARCHAR(200) = NULL,
    @inicioFuncaoGoverno        DATE = NULL,
    @fimFuncaoGoverno           DATE = NULL,
    @parenteFuncaoGoverno       BIT = NULL,
    @nomeParente                VARCHAR(200) = NULL,
    @relacionamento             VARCHAR(200) = NULL,
    @nomeFuncaoParenteGov       VARCHAR(200) = NULL,
    @nomeEmpresaParenteGov      VARCHAR(200) = NULL,
    @vinculoBrPartners          BIT = NULL,
    @residenciaFiscal           BIT = NULL,
    @nomePaisResidencia         VARCHAR(200) = NULL,
    @nomePais                   VARCHAR(200) = NULL
)
AS
BEGIN
    IF(@id > 0)
    BEGIN
        UPDATE 
            TB_CLIENTE_ONLINE_REPRESENTANTE 
        SET 
            NOME = @nome, ID_FORMA_REPRESENTACAO = @idFormaRepresentacao, ID_TIPO_DOCUMENTO = @idTipoDocumento, NUMERO_DOCUMENTO = @numeroDocumento,
            ORGAO_EMISSOR = @orgaoEmissor, DATA_VALIDADE_DOCUMENTO = @dataValidadeDoc, DATA_NASCIMENTO = @dataNasc, LOCAL_NASCIMENTO = @localNasc,
            NACIONALIDADE = @nacionalidade, ID_ESTADO_CIVIL = @idEstadoCivil, ID_PROFISSAO = @idProfissao, QUALIFICACAOCSS = @qualificacaoCss,
            DATAVALIDADECSS = @dataValidadeCss, FUNCAO_GOVERNO = @funcaoGoverno, ID_PERFIL = @idPerfil, NOME_CARGO_GOVERNO = @nomeCargoGoverno,
            NOME_EMPRESA_GOVERNO = @nomeEmpresaGoverno, INICIO_FUNCAO_GOVERNO = @inicioFuncaoGoverno, FIM_FUNCAO_GOVERNO = @fimFuncaoGoverno,
            PARENTE_FUNCAO_GOVERNO = @parenteFuncaoGoverno, NOME_PARENTE = @nomeParente, RELACIONAMENTO = @relacionamento,
            NOME_FUNCAO_PARENTE_GOVERNO = @nomeFuncaoParenteGov, NOME_EMPRESA_PARENTE_GOVERNO = @nomeEmpresaParenteGov, 
            VINCULO_BRPARTNERS = @vinculoBrPartners, RESIDENCIA_FISCAL = @residenciaFiscal, NOME_PAIS_RESIDENCIA_FISCAL = @nomePaisResidencia,
            NOME_PAIS_RESIDENCIA = @nomePais, DATA_EMISSAO_DOCUMENTO = @dataEmissoaDoc
        WHERE
            ID = @id
    END
    ELSE
        BEGIN
            INSERT INTO 
                TB_CLIENTE_ONLINE_REPRESENTANTE
            VALUES 
            (
                @idClienteOnline, @nome, @idFormaRepresentacao, @idTipoDocumento, @numeroDocumento, @orgaoEmissor, @dataValidadeDoc, @dataEmissoaDoc, 
                @dataNasc, @localNasc, @nacionalidade, @idEstadoCivil, @idProfissao, @qualificacaoCss, @dataValidadeCss, @funcaoGoverno,
                @idPerfil, @nomeCargoGoverno, @nomeEmpresaGoverno, @inicioFuncaoGoverno, @fimFuncaoGoverno, @parenteFuncaoGoverno,
                @nomeParente, @relacionamento, @nomeFuncaoParenteGov, @nomeEmpresaParenteGov, @vinculoBrPartners, @residenciaFiscal, 
                @nomePaisResidencia, @nomePais
            )
        END
END