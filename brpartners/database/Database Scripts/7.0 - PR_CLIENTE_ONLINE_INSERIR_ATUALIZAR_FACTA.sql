CREATE PROCEDURE PR_CLIENTE_ONLINE_INSERIR_ATUALIZAR_FACTA(
    @idClienteOnline    INT,
    @facta              VARCHAR(200) = NULL,
    @tin                VARCHAR(200) = NULL,
    @paisEmissor        VARCHAR(200) = NULL,
    @idNatureza         INT 
)
AS
BEGIN

    IF EXISTS(SELECT * FROM TB_CLIENTE_ONLINE_FATCA WHERE ID_CLIENTE_ONLINE = @idClienteOnline)
    BEGIN
        UPDATE 
            TB_CLIENTE_ONLINE_FATCA 
        SET 
            FATCA = @facta, TIN = @tin, PAIS_EMISSOR = @paisEmissor, ID_INFORMACAO_FISCAL = @idNatureza
        WHERE
            ID_CLIENTE_ONLINE = @idClienteOnline
    END
    ELSE
        BEGIN
            INSERT INTO TB_CLIENTE_ONLINE_FATCA VALUES 
            (
                @idClienteOnline, @facta, @tin, @paisEmissor, @idNatureza
            )
        END
END