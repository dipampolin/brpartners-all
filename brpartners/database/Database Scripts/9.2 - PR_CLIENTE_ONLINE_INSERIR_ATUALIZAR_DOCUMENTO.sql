CREATE PROCEDURE PR_CLIENTE_ONLINE_INSERIR_ATUALIZAR_DOCUMENTO(
    @idClienteOnline INT,
    @idDoc           INT,
    @nome            VARCHAR(200) = NULL,
    @extensao        VARCHAR(200) = NULL,
    @data            VARBINARY(MAX) = NULL,
    @fisico          BIT
)
AS
BEGIN
    IF EXISTS (SELECT * FROM TB_CLIENTE_ONLINE_DOCUMENTO WHERE ID_DOCUMENTO = @idDoc AND ID_CLIENTE_ONLINE = @idClienteOnline)
    BEGIN
        IF(@data IS NULL)
        BEGIN
            UPDATE TB_CLIENTE_ONLINE_DOCUMENTO SET TEM_FISICO = @fisico
            WHERE ID_DOCUMENTO = @idDoc AND ID_CLIENTE_ONLINE = @idClienteOnline
        END
        ELSE
            BEGIN
                UPDATE TB_CLIENTE_ONLINE_DOCUMENTO SET NOME = @nome, EXTENSAO = @extensao, ARQUIVO = @data, TEM_FISICO = @fisico
                WHERE ID_DOCUMENTO = @idDoc AND ID_CLIENTE_ONLINE = @idClienteOnline
            END
    END
    ELSE
        BEGIN
            INSERT INTO TB_CLIENTE_ONLINE_DOCUMENTO VALUES (@idClienteOnline, @idDoc, @nome, @extensao, @data, @fisico)
        END
END