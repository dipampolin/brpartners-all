CREATE  TABLE FORMULARIO_SUITABILITY
  (
    ID_FORMULARIO NUMBER  NOT NULL PRIMARY KEY,
    DS_FORMULARIO      VARCHAR2 (50),
  TP_PESSOA       CHAR(1) NULL,
  IN_ATIVO      CHAR(1) DEFAULT 'N' NOT NULL
  ) ;


  
  CREATE TABLE TIPO_QUESTAO (
  CD_TIPO CHAR(1) PRIMARY KEY,
  DS_TIPO VARCHAR(20) NOT NULL
  );
  


  
alter table SUITABILITY 
  add (ID_FORMULARIO NUMBER null,
      IDSUBQUESTION number,
      VL_PESO NUMBER DEFAULT 1 NOT NULL ,
      CD_TIPO CHAR(1) null references TIPO_QUESTAO(CD_TIPO));

-- Create/Recreate primary, unique and foreign key constraints 
alter table SUITABILITY
  add constraint FK_FORMULARIO foreign key (ID_FORMULARIO)
  references FORMULARIO_SUITABILITY (ID_FORMULARIO);

  
ALTER TABLE SUITABILITY_ANSWERS
  ADD IDSUBQUESTION NUMBER NULL REFERENCES SUITABILITY(ID);
  
  
  
-- Add/modify columns 
alter table SUITABILITY_USER_STATUS_TYPE add ID_FORMULARIO number default 0 not null references FORMULARIO_SUITABILITY (ID_FORMULARIO);
-- Create/Recreate primary, unique and foreign key constraints 

alter table SUITABILITY_USER_ANSW
  add constraint PK_USER_ANSWERS primary key (ID, IDQUESTION, IDANSWER, INSERT_DATE);

  
  
ALTER TABLE SUITABILITY_USER_STATUS_TYPE ADD CONSTRAINT
SUITABILITY_USER_TYPE_PK PRIMARY KEY ( ID ) ;
  
ALTER TABLE SUITABILITY_USER_STATUS_TYPE
  ADD CD_LOGIN VARCHAR(20) NULL REFERENCES TB_CA_USUARIO;
  
-- Create table
create table PONTUACAO_FORMULARIO
(
  ID            number not null,
  ID_FORMULARIO number not null REFERENCES FORMULARIO_SUITABILITY(ID_FORMULARIO),
  VL_PONTOS_MIN number,
  VL_PONTOS_MAX number,
  ID_TYPE       number not null
)
;
-- Add comments to the columns 
comment on column PONTUACAO_FORMULARIO.ID
  is 'PK';
comment on column PONTUACAO_FORMULARIO.ID_FORMULARIO
  is 'Identificador do formulário';
comment on column PONTUACAO_FORMULARIO.VL_PONTOS_MIN
  is 'Pontuação minima ou NULL para Conservador';
comment on column PONTUACAO_FORMULARIO.VL_PONTOS_MAX
  is 'Pontuação máxima ou NULL para arrojado';
comment on column PONTUACAO_FORMULARIO.ID_TYPE
  is 'Perfil em que o cliente deve ser enquadrado.';
-- Create/Recreate primary, unique and foreign key constraints 
alter table PONTUACAO_FORMULARIO
  add constraint PK_PONTUACAO primary key (ID);
alter table PONTUACAO_FORMULARIO
  add constraint FK_SUITABILITY_TYPE foreign key (ID_TYPE)
  references suitability_types (ID) on delete cascade;
alter table PONTUACAO_FORMULARIO
  add constraint UK_PONTUACAO_MIN unique (ID, VL_PONTOS_MIN);
alter table PONTUACAO_FORMULARIO
  add constraint UK_PONTUACAO_MAX unique (ID, VL_PONTOS_MAX);

  
  
  
-- Create/Recreate primary, unique and foreign key constraints 
alter table SUITABILITY_USER_ANSW
  add constraint FK_USER_ANSWERS_STATUS foreign key (ID)
  references suitability_user_status_type (ID);

  
  



insert into FORMULARIO_SUITABILITY values (0, 'Formulário Legado', null, 'N'); 
insert into FORMULARIO_SUITABILITY values (1, 'Pessoa Física', 'F', 'S');
insert into FORMULARIO_SUITABILITY values (2, 'Pessoa Juridica', 'J', 'S');

  
insert into tipo_questao values ('C', 'Check Box');
insert into tipo_questao values ('R', 'Radio Button');


update suitability set id_formulario = 0, cd_tipo = 'R';

