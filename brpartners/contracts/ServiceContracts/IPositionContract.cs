﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using QX3.Portal.Contracts.DataContracts;

namespace QX3.Portal.Contracts.ServiceContracts
{
    [ServiceContract]
    public interface IPositionContract
    {
        [OperationContract]
        WcfResponse<CustodyPositionResponse> ListCustodyPosition(int cdCliente, string cdMercado);

        [OperationContract]
        WcfResponse<ConsolidatedPositionResponse2> ListConsolidatedPosition(int cdCliente);

        [OperationContract]
        WcfResponse<List<ConsolidatedPositionDetails>> ListConsolidatedPositionDetails(int cdCliente, string grupo);

        [OperationContract]
        WcfResponse<FinancialPositionResponse> FinancialPosition(int cdCliente);

        [OperationContract]
        WcfResponse<List<InvestmentsGroups>> ListInvestmentsGroups();

        [OperationContract]
        WcfResponse<List<ExtratoRendaDatails>> ListExtratoRendaDatails(string codEmpresa, string cpfCnpj, string DtRefDe, string DtRefAte);
    }

}

