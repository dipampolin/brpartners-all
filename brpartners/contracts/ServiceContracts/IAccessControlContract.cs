﻿using System.ServiceModel;
using QX3.Portal.Contracts.DataContracts;
using System.Collections.Generic;
using System;

namespace QX3.Portal.Contracts.ServiceContracts
{
	[ServiceContract]
	public interface IAccessControlContract
	{
		[OperationContract]
		WcfResponse<ACProfile> LoadUserProfile(string email);

		[OperationContract]
		WcfResponse<List<ACElement>> LoadACElements(bool active);

		[OperationContract]
		WcfResponse<Boolean> UpdateElementAlias(int elementID, string newAlias);

		[OperationContract]
		WcfResponse<Int32> InsertGroupOfAssessors(ACGroupOfAssessors group);

		[OperationContract]
		WcfResponse<Boolean> InsertUserGroupRelationship(int userID, int userId);

        [OperationContract]
        WcfResponse<Int32> InsertUser(ACUser user);

        [OperationContract]
        WcfResponse<Int32> InsertClientOnline(ACUser user);

        [OperationContract]
        WcfResponse<ACUser> LoadClient(string cpf);

        [OperationContract]
		WcfResponse<Boolean> ValidateGroupName(string groupName, int userId);

		[OperationContract]
		WcfResponse<Boolean> ValidateAssessorsList(string list, int userId);

        [OperationContract]
        WcfResponse<Boolean> ValidateAssessorsAndClientsList(int groupId, string list, string includeList, string excludeList);

		[OperationContract]
		WcfResponse<List<ACUser>> LoadGroupOfAssessorsFreeUsers(string nameFilter, string idsFilter);

		[OperationContract]
		WcfResponse<List<ACUser>> LoadUsers();

        [OperationContract]
        WcfResponse<ACUser> LoadUser(int id);

		[OperationContract]
		WcfResponse<ACUser> LoadUserByEmail(string email);

        [OperationContract]
        WcfResponse<List<ACProfile>> LoadProfiles();

        [OperationContract]
        WcfResponse<ACProfile> LoadProfile(int profileID);

        [OperationContract]
        WcfResponse<Int32> InsertProfile(ACProfile profile);

		[OperationContract]
		WcfResponse<List<ACGroupOfAssessors>> LoadGroupOfAssessors(bool minimum, int userId, string userName);

		[OperationContract]
		WcfResponse<List<ACGroupOfAssessors>> LoadGroupsOfAssessorsPerUser(long userId);

		[OperationContract]
		WcfResponse<Boolean> DeleteGroupOfAssessors(int userId);

        [OperationContract]
        WcfResponse<Boolean> DeleteUser(int userID);

        [OperationContract]
        WcfResponse<List<ACProfile>> LoadProfilesAndUserCount(int profileID, string userName, bool withoutUser, List<ACElement> elements);

        [OperationContract]
        WcfResponse<Boolean> DeleteProfile(int profileID);

        [OperationContract]
        WcfResponse<Boolean> ValidateProfileName(string profileName, int profileID);

        [OperationContract]
        WcfResponse<Boolean> ValidateProfileElements(List<ACElement> elements, int profileID);

        [OperationContract]
        WcfResponse<List<ACUser>> LoadUsersWithoutProfile();

        [OperationContract]
        WcfResponse<bool> CheckIfExistsInIncludedClients(int clientCode, int userId);

        [OperationContract]
        WcfResponse<bool> CheckIfExistsInExcludedClients(int clientCode, int userId);

        [OperationContract]
        WcfResponse<List<FuncionalityClient>, int> ListFuncionalityClients(FuncionalityClientFilter filter);

        [OperationContract]
        WcfResponse<bool> InsertClientForFuncionality(FuncionalityClient client);

        [OperationContract]
        WcfResponse<bool> DeleteClientForFuncionality(FuncionalityClient client);

        [OperationContract]
        WcfResponse<bool> UpdateClientForFuncionality(FuncionalityClient client);

        [OperationContract]
        WcfResponse<List<Funcionality>> ListFuncionalities();

        [OperationContract]
        WcfResponse<List<ACUser>> ListClientsSINACOR(string clientes);

        [OperationContract]
        WcfResponse<bool> Verify_SESSION_Login(string username);

        [OperationContract]
        WcfResponse<bool> Delete_SESSION_Login(string username);

        [OperationContract]
        WcfResponse<bool> Insert_SESSION(string username, string ipClient, DateTime dt, string sessionId);

        [OperationContract]
        WcfResponse<bool> Verify_SESSION_IP(string ipClient);

        [OperationContract]
        WcfResponse<bool> Verify_SESSION(string username, string ipClient);

        [OperationContract]
        WcfResponse<List<UserInfo>> ListUsers();

        [OperationContract]
        WcfResponse<bool> UpdateUsers(List<UserInfo> users);
    }
}
