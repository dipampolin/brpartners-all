﻿using QX3.Portal.Contracts.DataContracts;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using ADM = QX3.Portal.Contracts.DataContracts.SuitabilityAdmin;

namespace QX3.Portal.Contracts.ServiceContracts
{
    [ServiceContract]
    public interface ISuitabilityContract
    {
        #region Preenchimento

        [OperationContract]
        WcfResponse<SuitabilityForm> Suitability(int client);
        
        [OperationContract]
        WcfResponse<SuitabilityForm> TestarSuitability(int idFormulario);

        [OperationContract]
        WcfResponse<bool, int> GravarSuitability(SuitabilityInput userInput, string login, int cdCliente, int cdClienteGlobal, int status, int investidor, int? score);

        [OperationContract]
        WcfResponse<List<SuitabilityPerfilCliente>> PerfilClienteJustification(string cnpj, bool init);

        [OperationContract]
        WcfResponse<bool> GravarSuitabilityJustification(int suitabilityId, string message);

        [OperationContract]
        WcfResponse<SuitabilityClientsResponse> SelecionaClientesSuitability(int? idClienteGlobal, long? cpfcnpj, string cdCliente, string status, string investidor, string desenquadrado);

        [OperationContract]
        WcfResponse<List<SuitabilityClients>> HistoricoClientesSuitability(int? idCliente, string cdCliente, string login);

        [OperationContract]
        WcfResponse<SuitabilityAnswersResponse> SelecionaResultadoSuitability(string id);

        [OperationContract]
        WcfResponse<bool> AtualizaStatusSuitability(int cdCliente, int status);

        [OperationContract]
        WcfResponse<List<SuitabilityClients>> HistoricosSuitability(int? idCliente, string cdCliente, string dtInicio, string dtFim, string tipo);

        //[OperationContract]
        //WcfResponse<List<SuitabilityUnframed>> VerificaDesenquadramento(int idCliente, int idPerfil, string dtInicio, string dtFim, string tipo);

        [OperationContract]
        WcfResponse<List<SuitabilityUnframedResponse>> VerificaNovoDesenquadramento(int idCliente, DateTime? dtInicio, DateTime? dtFim);

        [OperationContract]
        WcfResponse<List<StatementClient>> GetClients(StatementClientFilter filter);

        [OperationContract]
        WcfResponse<List<FixedIncomeDetails>> GetFixedIncome(FixedIncomeFilter filter);

        [OperationContract]
        WcfResponse<List<NdfDetails>> GetNdf(NdfFilter filter);

        [OperationContract]
        WcfResponse<List<SwapDetails>> GetSwap(SwapFilter filter);

        [OperationContract]
        WcfResponse<StatementConfiguration> GetStatementConfiguration(int clientId);

        [OperationContract]
        WcfResponse<List<StatementLog>> GetStatementLog(StatementLog filter);

        [OperationContract]
        WcfResponse<int> InsertStatementConfiguration(StatementConfiguration obj);

        [OperationContract]
        WcfResponse<int> InsertStatementLog(StatementLog obj);

        #endregion

        #region Administrativo

        [OperationContract]
        List<ADM.FormularioSuitability> ListarFormularios(string tipoPessoa, string status);

        [OperationContract]
        ADM.EdicaoSuitability AbrirFormulario(int id);

        [OperationContract]
        ADM.EdicaoSuitability Salvar(ADM.RequisicaoSalvarSuitability requisicao);

        [OperationContract]
        bool DeletarFormulario(int id);

        #endregion
    }
}
