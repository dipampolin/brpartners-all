﻿using System.ServiceModel;
using QX3.Portal.Contracts.DataContracts;
using System.Collections.Generic;
using System;

namespace QX3.Portal.Contracts.ServiceContracts
{
	[ServiceContract]
	public interface ICommonContract
	{
		[OperationContract]
        WcfResponse<List<CommonType>> LoadTypes(string moduleName, string specification);

        [OperationContract]
        WcfResponse<List<WarningList>> GetWarningList(UserInformation user, string areaName, string assessors, string clients, int? userId);

        [OperationContract]
        WcfResponse<List<int>> GetFullListOfAssessors();

        [OperationContract]
        WcfResponse<String> LoadClientName(int code);

        [OperationContract]
        WcfResponse<MailModel> GetMailModel(int funcionalityId);

        [OperationContract]
        WcfResponse<List<TagModel>> GetTagModel();

        [OperationContract]
        WcfResponse<bool> UpdateMailModel(MailModel model);

        [OperationContract]
        WcfResponse<bool> InsertMailModel(MailModel model);

        [OperationContract]
        WcfResponse<bool> SendMailForUser(int funcionalityId, List<KeyValuePair<string, object>> variables, byte[] attach, FuncionalityClient client, string fileName);

        [OperationContract]
        WcfResponse<bool> SendMailForUserWithAttachment(
            List<KeyValuePair<string, object>> variables
            , List<KeyValuePair<string, byte[]>> attachments
            , string templateFileName
            , string subject
            , string emailTo
            , string emailReplyTo
            , List<string> emailCCList);

        [OperationContract]
        WcfResponse<RelatedPersons> RelatedPersons(DateTime dtStart, DateTime dtEnd);
	}
}
