﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using QX3.Portal.Contracts.DataContracts;

namespace QX3.Portal.Contracts.ServiceContracts
{
    [ServiceContract]
    public interface IBrokerageDiscountContract
    {
        [OperationContract]
        WcfResponse<List<BrokerageDiscount>> LoadBrokerageDiscounts(BrokerageDiscount broker, int? userId);

        [OperationContract]
        WcfResponse<List<BrokerageDiscount>, int> LoadBrokerageDiscountsPaginate(BrokerageDiscount brokerage, FilterOptions options, int? userId);

        [OperationContract]
        WcfResponse<bool> UpdateBrokerageDiscountStatus(List<BrokerageDiscount> requests);

        [OperationContract]
        WcfResponse<bool> DeleteBrokerageDiscount(List<BrokerageDiscount> requests);

        [OperationContract]
        WcfResponse<bool, int> BrokerageDiscountRequest(BrokerageDiscount request);

        [OperationContract]
        WcfResponse<DateTime> GetProjectedDate(DateTime? date);

        [OperationContract]
        WcfResponse<int> GetPendentBrokerageDiscounts(BrokerageDiscount filter, int? userId);

        [OperationContract]
        WcfResponse<decimal> GetActualDiscount(int clientCode);
    }
}
