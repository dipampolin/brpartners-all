﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using QX3.Portal.Contracts.DataContracts;

namespace QX3.Portal.Contracts.ServiceContracts
{
    [ServiceContract]
    public interface ICustodyTransferContract
    {
        [OperationContract]
        WcfResponse<List<CustodyTransfer>, int> LoadCustodyTransfers(CustodyTransferFilter filter, FilterOptions options, int? userId);

        [OperationContract]
        WcfResponse<bool, int> InsertCustodyTransfer(CustodyTransfer custody);

        [OperationContract]
        WcfResponse<bool> DeleteCustodyTransfer(List<CustodyTransfer> requestList);

        [OperationContract]
        WcfResponse<bool> UpdateCustodyTransferStatus(List<CustodyTransfer> requestList);

		[OperationContract]
		WcfResponse<CustodyTransferReport> LoadCustodyTransferReport(string fromDate, string toDate);

        [OperationContract]
        WcfResponse<List<CustodyTransferDetails>> LoadClientPosition(CustodyTransferFilter filter, int? userId);

        [OperationContract]
        WcfResponse<bool,int> EditCustodyTransfer(CustodyTransfer request);

        [OperationContract]
        WcfResponse<int> GetPendentCustodyTransfers(CustodyTransferFilter filter, int? userId);
    }
}
