﻿using System.ServiceModel;
using QX3.Portal.Contracts.DataContracts;
using System.Collections.Generic;
using System;

namespace QX3.Portal.Contracts.ServiceContracts
{
    [ServiceContract]
    public interface ITermContract
    {
        [OperationContract]
        WcfResponse<List<TermItem>> LoadTerms(TermFilter filter, int? userId);

        [OperationContract]
        WcfResponse<List<TermItem>> LoadLotTerms(string lstContracts);

        [OperationContract]
        WcfResponse<TermItem> LoadTerm(int termId);

        [OperationContract]
        WcfResponse<bool> SettleTerm(int termID);

        [OperationContract]
        WcfResponse<bool> ExcludeTerm(int termID);

        [OperationContract]
        WcfResponse<List<TermItem>> LoadClientTerms(TermFilter filter, int? userId);

        [OperationContract]
        WcfResponse<List<TermItem>> InsertTerms(List<TermItem> terms);

        [OperationContract]
        WcfResponse<bool> ChangeQuantityToSettle(int termID, Int64 quantity);

        [OperationContract]
        WcfResponse<int> GetNumberOfPending(string assessorsList);
    }
}
