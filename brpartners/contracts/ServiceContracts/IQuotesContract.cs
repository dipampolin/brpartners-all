﻿using System;
using System.ServiceModel;
using QX3.Spinnex.Trace.Services.Data;
using QX3.Portal.Contracts.DataContracts;
using System.Collections.Generic;
using System.Xml.Linq;


namespace QX3.Portal.Contracts.ServiceContracts
{
	[ServiceContract]

    public interface IQuotesContract
	{
		[OperationContract]
        WcfResponse<List<StockPrice>> LoadQuotes(string[] stocks);
	}
}
