﻿using System;
using System.ServiceModel;
using QX3.Spinnex.Trace.Services.Data;
using QX3.Portal.Contracts.DataContracts;
using System.Collections.Generic;
using System.Xml.Linq;


namespace QX3.Portal.Contracts.ServiceContracts
{
	[ServiceContract]

    public interface ILogContract
	{
        [OperationContract]
		void CreateTraceRecord(string data);

		[OperationContract]
		TraceApplication[] APP_ListApplications();

		[OperationContract]
		TraceServer[] SRV_ListServers(int applicationID);

		[OperationContract]
		TraceModule[] MOD_ListModules(int applicationID);

		[OperationContract]
		TraceModuleHierarchy MOD_ListHierarchy(int applicationID);

		[OperationContract]
		TraceSearchResult[] TRACE_Search(TraceSearchParameters searchParams);

		[OperationContract]
		string TRACE_GetDetails(Guid transactionID);

		[OperationContract]
		RelatedTrace[] TRACE_LoadRelatedTraces(Guid transactionID);

		[OperationContract]
		WcfResponse<List<HistoryData>> LoadHistory(HistoryFilter filter);

		[OperationContract]
		WcfResponse<Boolean> InsertHistory(HistoryLog log);
	}
}
