﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using QX3.Portal.Contracts.DataContracts;

namespace QX3.Portal.Contracts.ServiceContracts
{
    [ServiceContract]
    public interface INonResidentTradersServiceContract
	{
		#region Common

		[OperationContract]
		WcfResponse<String> LoadCustomerName(int code);

        [OperationContract]
        WcfResponse<String> GetCustomerName(int code, string assessors, int? userId);

		#endregion

		#region Registration

		[OperationContract]
		WcfResponse<List<NonResidentCustomer>> LoadCustomers(int code, int? userId);

        [OperationContract]
        WcfResponse<List<Broker>> LoadBrokers(string filter);

        [OperationContract]
        WcfResponse<List<Broker>> LoadBrokersTransfer(string filter);

        [OperationContract]
        WcfResponse<string> LoadClientEmail(int clientCode);

        [OperationContract]
        WcfResponse<List<CommonType>> LoadPortfolios(string filter);

		[OperationContract]
		WcfResponse<Boolean> InsertCustomer(NonResidentCustomer customer);

        [OperationContract]
        WcfResponse<SettlementDate> LoadLastSettlement();

		[OperationContract]
		WcfResponse<List<PTax>> LoadPTaxes(string date);

		[OperationContract]
		WcfResponse<Boolean> InsertPTax(PTax ptax);

		[OperationContract]
		WcfResponse<List<Failure>> LoadFailures(string date, int code, int? userId);

		[OperationContract]
		WcfResponse<Boolean> InsertFailure(Failure failure);

        [OperationContract]
        WcfResponse<List<ReportType>> LoadReportTypes();

        [OperationContract]
        WcfResponse<bool> InsertDisclaimer(Disclaimer disclaimer);

        [OperationContract]
        WcfResponse<Disclaimer> LoadDisclaimer(int reportId);

		[OperationContract]
		WcfResponse<List<Disclaimer>> LoadDisclaimers();
		#endregion

        #region Report

        [OperationContract]
		WcfResponse<Confirmation> LoadConfirmationReports(int code, DateTime trading);

        [OperationContract]
		WcfResponse<List<Customer>> LoadClients(int code, string consultType, DateTime tradingDate, int? userId);
                
        [OperationContract]
        WcfResponse<TradeBlotter> LoadTradeBlotterReports(DateTime tradingDate, int? userId);

        [OperationContract]
        WcfResponse<ReportHeader> LoadHeader();

        [OperationContract]
        WcfResponse<CustomerStatement> LoadCustomerStatementReports(int code, string date, int? userId);

		[OperationContract]
		WcfResponse<CustomerLedger> LoadCustomerLedgerReports(int code, string date);

		[OperationContract]
		WcfResponse<StockRecord> LoadStockRecordReports(string date, int? userId);

		[OperationContract]
		WcfResponse<FailLedger> LoadFailLedgerReports(int code, DateTime tradingDate, FailLedgerType type, int? userId);

        #endregion

		#region Brokerage

		[OperationContract]
		WcfResponse<Boolean> SettleBrokerageTransfers(string settlementDate, string settlementUsername);

		[OperationContract]
		WcfResponse<List<BrokerageTransfer>> LoadPendingBrokerageTransfers(int code, string date, int? userId);

		[OperationContract]
		WcfResponse<List<BrokerageTransfer>> LoadSettledBrokerageTransfers(int code, string startDate, string endDate, int? userId);

		#endregion
	}
}
