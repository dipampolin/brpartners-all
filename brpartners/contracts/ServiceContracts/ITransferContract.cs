﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using QX3.Portal.Contracts.DataContracts;

namespace QX3.Portal.Contracts.ServiceContracts
{
    [ServiceContract]
    public interface ITransferContract
    {
        [OperationContract]
        WcfResponse<List<TransferBovespaItem>> LoadTransfersBovespa(TransferBovespaFilter filter, FilterOptions options, int? userId, out int count);
    }
}
