﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QX3.Portal.Contracts.DataContracts;
using System.ServiceModel;

namespace QX3.Portal.Contracts.ServiceContracts
{
    [ServiceContract]
    public interface IBMFContract
    {
        [OperationContract]
        WcfResponse<List<BovespaBrokerageReport>, int> LoadBovespaBrokerageReport(BrokerageReportFilter filter);

        [OperationContract]
        WcfResponse<List<BMFBrokerageReport>, int> LoadBMFBrokerageReport(BrokerageReportFilter filter);

        [OperationContract]
        WcfResponse<BrokerageRange, int> LoadBrokerageRange(BrokerageRangeFilter filter, FilterOptions filterOptions, out bool hasAnterior, out bool hasPosterior);        
    }
}
