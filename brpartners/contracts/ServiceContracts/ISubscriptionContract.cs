﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using QX3.Portal.Contracts.DataContracts;

namespace QX3.Portal.Contracts.ServiceContracts
{
    [ServiceContract]
    public interface ISubscriptionContract
    {
        [OperationContract]
        WcfResponse<List<Subscription>, int> LoadSubscriptions(Subscription filter, FilterOptions options, bool getProspects);

        [OperationContract]
        WcfResponse<bool, int> SubscriptionRequest(Subscription request);

        [OperationContract]
        WcfResponse<bool, int> SubscriptionUpdate(Subscription request);

        [OperationContract]
        WcfResponse<bool, int> SubscriptionDelete(Subscription request);

        [OperationContract]
        WcfResponse<bool, int> SubscriptionUpdateStatus(Subscription request);

        [OperationContract]
        WcfResponse<List<SubscriptionRights>, int> LoadSubscriptionRights(SubscriptionRights filter, FilterOptions options, int? userId);

        [OperationContract]
        WcfResponse<bool> SubscriptionRightsRequest(SubscriptionRights request);

        [OperationContract]
        WcfResponse<bool> SubscriptionRightsUpdate(SubscriptionRights request);

        [OperationContract]
        WcfResponse<bool> SubscriptionRightsDelete(SubscriptionRights request);

        [OperationContract]
        WcfResponse<long> GetRequestedQuantity(SubscriptionRights request);

        [OperationContract]
        WcfResponse<long> GetRightQuantity(SubscriptionRights request);

        [OperationContract]
        WcfResponse<bool> RequestRightsNotify(SubscriptionRights request, Subscription subscription);

        [OperationContract]
        WcfResponse<List<SubscriptionNotification>> LoadNotifications(int subscriptionId);
    }
}
