﻿using QX3.Portal.Contracts.DataContracts;
using System.Collections.Generic;
using System.ServiceModel;

namespace QX3.Portal.Contracts.ServiceContracts
{
    [ServiceContract]
    public interface IClientRegistrationContract
    {
        [OperationContract]
        WcfResponse<List<Client>, int> LoadActiveAndInactiveClients(ClientFilter filter, FilterOptions options, int? userId);

		[OperationContract]
		WcfResponse<List<OverdueAndDue>, int> LoadOverdueAndDueRegistration(OverdueAndDueFilter filter, FilterOptions options, int? userId);

        [OperationContract]
        WcfResponse<List<TransferBovespaItem>> LoadTransfersBovespa(TransferBovespaFilter filter, FilterOptions options, int? userId, out int count);

        [OperationContract]
        WcfResponse<List<TransferBMFItem>, int> LoadTransfersBMF(TransferBMFFilter filter, FilterOptions options);

        [OperationContract]
        WcfResponse<bool> InsertClientBond(TransferBMFItem transfer);

        [OperationContract]
        WcfResponse<Client> CheckClientBMF(int clientCode);

        [OperationContract]
        WcfResponse<bool, int> CheckClientPerCPFCGC(string cpccgc);

        [OperationContract]
        WcfResponse<string> CheckClientType(string clientCodes, string assessors, int? userId);

        [OperationContract]
        WcfResponse<IndividualPerson> LoadIndividualPerson(int clientCode);

        [OperationContract]
        WcfResponse<List<BankAccount>> LoadBanks(string bankName);

        [OperationContract]
        WcfResponse<ClientOnline> LoadClientOnline(string idClient);

        [OperationContract]
        WcfResponse<ClientOnlineFinancial> LoadClientFinancial(int idClientOnline);

        [OperationContract]
        WcfResponse<ClientOnlineRepresentative> LoadRepresentative(int idClientOnline, int id);

        [OperationContract]
        WcfResponse<Representatives> LoadRepresentatives(int idClientOnline);

        [OperationContract]
        WcfResponse<ClientOnlineDocuments> LoadClientDocument(int idClientOnline);

        [OperationContract]
        WcfResponse<ClientOnlineRules> LoadDocumentsRepresentatives(int idDocument);

        [OperationContract]
        WcfResponse<ClientsOnlineStatus> LoadClientsInFilling();

        [OperationContract]
        WcfResponse<ClientsOnlineStatus> LoadClientsInAnalysis();

        [OperationContract]
        WcfResponse<ClientsOnlineStatus> LoadClientsApproved();

        [OperationContract]
        WcfResponse<ClientsOnlineStatus> LoadClientsDisapproved();

        [OperationContract]
        WcfResponse<Documents> LoadDocument();

        [OperationContract]
        WcfResponse<Document> LoadDocumentById(int idDocument);

        [OperationContract]
        WcfResponse<int> UpsertDocument(Document document);

        [OperationContract]
        WcfResponse<int> RemoveDocument(int idDocument);

        [OperationContract]
        WcfResponse<int> InsertUpdateRegistrationClient(ClientOnline client);

        [OperationContract]
        WcfResponse<int> UpsertDocumentRepresentative(ClientOnlineRules clientOnlineRules);

        [OperationContract]
        WcfResponse<int> SubmitClientOnline(int idClientOnline);

        [OperationContract]
        WcfResponse<int> ApproveClientOnline(int idClientOnline);

        [OperationContract]
        WcfResponse<int> DisapproveClientOnline(int idClientOnline);

        [OperationContract]
        WcfResponse<int> InsertUpdateClientFinancial(ClientOnlineFinancial clientFinancial);

        [OperationContract]
        WcfResponse<int> UpsertDocumentClientOnline(ClientOnlineDocuments documents);

        [OperationContract]
        WcfResponse<bool> DeleteAssignById(int idFile);

        [OperationContract]
        WcfResponse<bool> DeleteRepresentative(int idClientOnline, int idRepresentative);

        [OperationContract]
        WcfResponse<ClientAssign> GetAssignById(int id);

        [OperationContract]
        WcfResponse<ClientDocument> GetModelDocumentById(int id);

        [OperationContract]
        WcfResponse<ClientDocument> GetDocumentById(int id);

        [OperationContract]
        WcfResponse<int> UpsertRepresentative(ClientOnlineRepresentative representative);

        [OperationContract]
        WcfResponse<List<Parameters>> GetState();

        [OperationContract]
        WcfResponse<List<Parameters>> GetCountries();

        [OperationContract]
        WcfResponse<List<Parameters>> GetAddressTypes();

        [OperationContract]
        WcfResponse<List<Parameters>> GetFinalities();

        [OperationContract]
        WcfResponse<List<Parameters>> GetActivities();

        [OperationContract]
        WcfResponse<List<Parameters>> GetOccupations();

        [OperationContract]
        WcfResponse<List<Parameters>> GetSizes();

        [OperationContract]
        WcfResponse<List<Parameters>> GetProfessions();

        [OperationContract]
        WcfResponse<List<Parameters>> GetMaritalStatus();

        [OperationContract]
        WcfResponse<List<Parameters>> GetSex();

        [OperationContract]
        WcfResponse<List<Parameters>> GetStatus();

        [OperationContract]
        WcfResponse<List<Parameters>> GetDocumentTypes();

        [OperationContract]
        WcfResponse<List<Parameters>> GetEconomicGroups();

        [OperationContract]
        WcfResponse<List<Parameters>> GetClassifications();

        [OperationContract]
        WcfResponse<List<Parameters>> GetEconomicActivities();

        [OperationContract]
        WcfResponse<List<Parameters>> GetRegisterType();

        [OperationContract]
        WcfResponse<List<Parameters>> GetClientTypes();

        [OperationContract]
        WcfResponse<List<Parameters>> GetCategories();

        [OperationContract]
        WcfResponse<List<Parameters>> GetManagers();

        [OperationContract]
        WcfResponse<List<Parameters>> GetNaturalness();

        [OperationContract]
        WcfResponse<List<Parameters>> GetNacionality();

        [OperationContract]
        WcfResponse<List<Parameters>> GetTelephoneTypes();

        [OperationContract]
        WcfResponse<List<Parameters>> GetMatrimonialRegime();

        [OperationContract]
        WcfResponse<List<Parameters>> GetChoice();

        [OperationContract]
        WcfResponse<List<Parameters>> GetFinancialProduct();

        [OperationContract]
        WcfResponse<List<Parameters>> GetFiscalNature();

        [OperationContract]
        WcfResponse<List<Parameters>> GetAccountFinality();

        [OperationContract]
        WcfResponse<List<Parameters>> GetAccountUtility();

        [OperationContract]
        WcfResponse<List<Parameters>> GetAccountType();

        [OperationContract]
        WcfResponse<List<Parameters>> GetAccountTypeBank();

        [OperationContract]
        WcfResponse<List<Parameters>> GetBank();

        [OperationContract]
        WcfResponse<List<Parameters>> GetRepresentationStyle();

        [OperationContract]
        WcfResponse<List<Parameters>> GetRepresentationProfile();

        [OperationContract]
        WcfResponse<List<Parameters>> GetQualifications();

        [OperationContract]
        WcfResponse<List<Parameters>> GetRulesTypes();

        [OperationContract]
        WcfResponse<List<Parameters>> GetExpirationDateDocument();
    }
}
