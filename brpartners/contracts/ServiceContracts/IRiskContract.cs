﻿using System.ServiceModel;
using QX3.Portal.Contracts.DataContracts;
using System.Collections.Generic;
using System;

namespace QX3.Portal.Contracts.ServiceContracts
{
    [ServiceContract]
    public interface IRiskContract
    {
        [OperationContract]
        WcfResponse<List<Int32>> InsertGuarantee(Guarantee guarantee);

        [OperationContract]
        WcfResponse<bool> UpdateGuarantee(GuaranteeItem guaranteeItem);

        [OperationContract]
        WcfResponse<bool> DeleteGuarantee(int guaranteeID);

        [OperationContract]
        WcfResponse<bool> ChangeGuaranteeStatus(int guaranteeID, int statusID);

        [OperationContract]
        WcfResponse<List<Guarantee>> LoadGuarantees(GuaranteeFilter filter, int? userId);

        [OperationContract]
        WcfResponse<int> GetNumberOfPending(string assessorsList);

        [OperationContract]
        WcfResponse<List<GuaranteeItem>> LoadClientGuarantees(GuaranteeFilter filter, int? userId);
    }
}
