﻿using System.ServiceModel;
using QX3.Portal.Contracts.DataContracts;
using System.Collections.Generic;
using System;

namespace QX3.Portal.Contracts.ServiceContracts
{
	[ServiceContract]
	public interface IMarketContract
	{
		[OperationContract]
        WcfResponse<List<Proceeds>> LoadProceeds(ProceedsFilter filter, int? userId);

		[OperationContract]
		WcfResponse<List<Stock>> LoadStocks();

        [OperationContract]
        WcfResponse<String> LoadAssessorName(int assessorID);

        [OperationContract]
        WcfResponse<List<CommonType>> AssessorNameSuggest(string assessorName);

        [OperationContract]
        WcfResponse<String> GetNextBusinessDay(int numberOfDays, string targetDate);

        [OperationContract]
        WcfResponse<int> GetNumberOfPending(string tableName, int statusId, string assessorsList);

        [OperationContract]
        WcfResponse<StockExchanges> LoadClientExchanges(int clientCode);
	}
}
