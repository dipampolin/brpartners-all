﻿using System.ServiceModel;
using QX3.Portal.Contracts.DataContracts;
using System;
using QX3.Spinnex.Authentication.Services;


namespace QX3.Portal.Contracts.ServiceContracts
{
    [ServiceContract]
    public interface IAuthenticationContract
    {
        [OperationContract] //----------------------Login LDAP-------------------------TODO: Retirar este método quando não usar mais o LDAP
        WcfResponse<UserInformation> Authenticate(string userName, string password, bool loadPermissions);

        [OperationContract]
        WcfResponse<Boolean> CheckIfUserExists(string filter, LDAPSearchFilter filterType);

        [OperationContract]
        WcfResponse<UserInformation, StatusLogin> AuthenticateUser(string userName, string password, bool loadPermissions);

        [OperationContract]
        WcfResponse<UserInformation, StatusLogin> SelectUser(string userName);

        [OperationContract]
        WcfResponse<bool> ChangePassword(UserInformation userInfo);

        [OperationContract]
        WcfResponse<Boolean, string> VerifyChangePassword(string guid);

        [OperationContract]
        WcfResponse<Boolean, string> CreateGuidToSend(UserInformation userInfo);

		[OperationContract]
		WcfResponse<bool> SendRedefinePasswordEmail(string token, string email, string userName);

        [OperationContract]
        WcfResponse<bool> SendWelcomeEmail(string token, string email, string userName);

        //[OperationContract]
        //WcfResponse<bool> TestEmail(string token, string email, string userName);
    }
}
