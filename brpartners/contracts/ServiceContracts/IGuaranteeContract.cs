﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using QX3.Portal.Contracts.DataContracts;

namespace QX3.Portal.Contracts.ServiceContracts
{
    [ServiceContract]
    public interface IGuaranteeContract
    {
		[OperationContract]
		WcfResponse<GuaranteeClient, int> ListGuarantees(GuaranteeClientFilter filter, int? userId, out int pos);
    }
}
