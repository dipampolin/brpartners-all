﻿using QX3.Portal.Contracts.DataContracts;
using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace QX3.Portal.Contracts.ServiceContracts
{
    [ServiceContract]
    public interface IDemonstrativeContract
    {
        [OperationContract]
        WcfResponse<ExtractRendaResponse> Extract(string CodEmpresa, string Cnpjcpf, string DtRefDe, string DtRefAte);

        [OperationContract]
        WcfResponse<List<ExtractFunction>> ExtractFunction(string CodEmpresa, string DtRefDe, string DtRefAte, string CpfCnpj, string UserName);

        [OperationContract]
        WcfResponse<TradingSummaryResponse> ListTradingSummary(int cdCliente, string cdAtivo, DateTime dtInicial, DateTime dtFinal);

        [OperationContract]
        WcfResponse<ProceedsDemonstrativeResponse> ListProceeds(int cdCliente, string provisionados, DateTime dtInicial, DateTime dtFinal);

        [OperationContract]
        WcfResponse<ExtractNDFResponse> ListExtractNDF(string CodEmpresa, string Cnpjcpf, int Mes, int Ano);

        [OperationContract]
        WcfResponse<ExtractSWPResponse> ListExtractSWP(string CodEmpresa, string Cnpjcpf, int mes, int ano);

        [OperationContract]
        WcfResponse<BrokerageNoteResponse> ListBrokeragenotesDetails(int cdCliente, int nrNota, DateTime dtNegocio);

        [OperationContract]
        WcfResponse<BrokerageNoteResponse> ListBrokeragenotesNegociosDia(int cdCliente, DateTime dtNegocio, string tpNegocio);

        [OperationContract]
        WcfResponse<BrokerageNoteResponse> ListBrokeragenotesNegociosHist(int cdCliente, DateTime dtNegocio, string tpNegocio);
    }
}
