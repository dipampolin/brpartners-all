﻿using System.ServiceModel;
using QX3.Portal.Contracts.DataContracts;
using System.Collections.Generic;
using System;

namespace QX3.Portal.Contracts.ServiceContracts
{
    [ServiceContract]
    public interface IOnlinePortfolioContract
    {
        [OperationContract]
        WcfResponse<bool> InsertContact(int clientCode, PortfolioContactItem contact);

        [OperationContract]
        WcfResponse<PortfolioContact> LoadClientContacts(int clientCode);

        [OperationContract]
        WcfResponse<List<CommonType>> SearchClients(string nameFilter, long operatorId, int maxItems);

        [OperationContract]
        WcfResponse<PortfolioClient> LoadClientName(int clientId);

        [OperationContract]
        WcfResponse<bool> UpdateObservation(int clientId, string observation);

        [OperationContract]
        WcfResponse<string> LoadObservation(int clientId);

        [OperationContract]
        WcfResponse<List<Int32>> LoadOperatorClients(int operatorID, int filter);

        [OperationContract]
        WcfResponse<bool> InsertClient(PortfolioClient obj, bool isUpdate);

        [OperationContract]
        WcfResponse<bool> DeleteClient(string code);

        [OperationContract]
        WcfResponse<List<PortfolioClient>> LoadPortfolioClients(PortfolioClientFilter filter);

        [OperationContract]
        WcfResponse<List<CommonType>> LoadCalculationTypes();

        [OperationContract]
        WcfResponse<PortfolioPosition> LoadClientPosition(int clientCode, long operatorID);

        [OperationContract]
        WcfResponse<PortfolioClient> LoadPortfolioClient(string clientCode);

        [OperationContract]
        WcfResponse<PortfolioClientSearchClients> LoadSinacorClients(string term);

        [OperationContract]
        WcfResponse<PortfolioPosition> SearchClientPortfolioPosition(string clientCode, string operatorCode);

        [OperationContract]
        WcfResponse<PortfolioPosition> LoadClientHistory(int clientCode, long operatorID);

        [OperationContract]
        WcfResponse<bool> DeletePosition(string id);

        [OperationContract]
        WcfResponse<bool> InsertPosition(PortfolioPosition obj, string sourcePosition);

    }
}
