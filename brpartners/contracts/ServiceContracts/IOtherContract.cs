﻿using System.ServiceModel;
using QX3.Portal.Contracts.DataContracts;
using System.Collections.Generic;
using System;

namespace QX3.Portal.Contracts.ServiceContracts
{
    [ServiceContract]
    public interface IOtherContract
    {
        [OperationContract]
        WcfResponse<int> InsertOrderCorrection(OrdersCorrection correction);

        [OperationContract]
        WcfResponse<bool> DeleteOrderCorrection(int correctionID);

        [OperationContract]
        WcfResponse<bool> ChangeOrdersCorrectionStatus(int correctionId, int statusID);

        [OperationContract]
        WcfResponse<List<OrdersCorrection>> LoadOrdersCorrections(OrdersCorrectionFilter filter);

        [OperationContract]
        WcfResponse<OrdersCorrection> LoadOrdersCorrection(int correctionID);

        [OperationContract]
        WcfResponse<ConsolidatedPositionResponse> LoadConsolidatedPosition(string clientCode, string assessors, int? userId);

        [OperationContract]
        WcfResponse<List<OperationsMap>> LoadOperationsMap(OperationMapFilter filter);

        [OperationContract]
        WcfResponse<Dictionary<String, String>> GetTradingDates();

        [OperationContract]
        WcfResponse<List<Operator>> GetOperatorList(OperationMapFilter filter);

        [OperationContract]
        WcfResponse<CPClientInformation> LoadClientInformation(string clientCode, string assessors, int? userId);

        [OperationContract]
        WcfResponse<List<OperationClient>, int> LoadOperationClients(OperationClientFilter filter, FilterOptions filterOptions, int? userId);

		[OperationContract]
		WcfResponse<OperationReport> LoadOperationReport(OperationReport filter);

        [OperationContract]
        WcfResponse<bool> SinalizeMailSended(int clientCode, DateTime tradingDate);

        [OperationContract]
        WcfResponse<IncomeReport> LoadIncomeReport(IncomeReportFilter filter);

        [OperationContract]
        WcfResponse<List<Private>> LoadPrivates(DateTime initmov, DateTime endmov);

		[OperationContract]
		WcfResponse<BBRent> LoadBBRent(DateTime tradeDate);

        [OperationContract]
        WcfResponse<Dictionary<string, string>> GetISINs(int? groupID);

        [OperationContract]
        WcfResponse<List<VAMItem>> LoadVAM(int? groupID, string isin);
    }
}
