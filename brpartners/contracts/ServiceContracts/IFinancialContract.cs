﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using QX3.Portal.Contracts.DataContracts;

namespace QX3.Portal.Contracts.ServiceContracts
{
    [ServiceContract]
    public interface IFinancialContract
    {
        [OperationContract]
        WcfResponse<List<ProjectedReportItem>> LoadProjectedReport(ProjectedReportFilter filter, int? userId);

        [OperationContract]
        WcfResponse<List<RedemptionRequestItem>> LoadRedemptionRequest(RedemptionRequestFilter filter, int? userId);

        [OperationContract]
        WcfResponse<bool> DeleteRedemptionRequest(int id, string clientId);

        [OperationContract]
        WcfResponse<RedemptionRequestResult> InsertRedemptionRequest(RedemptionRequestItem item);

        [OperationContract]
        WcfResponse<RedemptionRequestBalance> LoadRedemptionRequestBalance(string clientCode, string assessor, int? userId);

        [OperationContract]
        WcfResponse<List<RedemptionRequestBankAccount>> LoadBankAccountInformation(string clientCode);

        [OperationContract]
        WcfResponse<bool> ChangeRedemptionRequestStatus(RedemptionRequestItem item);

        [OperationContract]
        WcfResponse<int> GetPendentRedemptionRequest();

        [OperationContract]
        WcfResponse<List<DailyFinancial>, int> LoadDailyFinancialReport(DailyFinancialFilter filter, FilterOptions options, int? userId);        
    }
}
