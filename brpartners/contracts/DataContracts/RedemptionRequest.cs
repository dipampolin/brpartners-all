﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace QX3.Portal.Contracts.DataContracts
{
    #region Redemption Request

    [Serializable][DataContract]
    public class RedemptionRequestItem
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public DateTime RequestDate { get; set; }
        [DataMember]
        public CommonType Client { get; set; }
        [DataMember]
        public RedemptionRequestBankAccount BankAccount { get; set; }
        [DataMember]
        public decimal RedemptionValue { get; set; }
        [DataMember]
        public CommonType Status { get; set; }
        [DataMember]
        public DateTime SettlementDate { get; set; }
        [DataMember]
        public string Observation { get; set; }
    }

	[Serializable]
	[DataContract]
	public class RedemptionRequestBalance
	{
		[DataMember]
		public decimal Available { get; set; }
		[DataMember]
		public decimal AvailableD1 { get; set; }
		[DataMember]
		public decimal AvailableD2 { get; set; }
		[DataMember]
		public decimal AvailableD3 { get; set; }
		[DataMember]
		public decimal Total { get; set; }
		[DataMember]
		public decimal Intended { get; set; }
	}

    [Serializable][DataContract]
    public class RedemptionRequestFilter
    {
        [DataMember]
        public string Assessors { get; set; }
        [DataMember]
        public string Clients { get; set; }
        [DataMember]
        public int StatusID { get; set; }
        [DataMember]
        public string StartDate { get; set; }
        [DataMember]
        public string EndDate { get; set; }
    }

    [Serializable]
    [DataContract]
    public class RedemptionRequestBankAccount
    {
        [DataMember]
        public string BankCode { get; set; }
        [DataMember]
        public string BankName { get; set; }
        [DataMember]
        public string AgencyNumber { get; set; }
        [DataMember]
        public string AgencyDigit { get; set; }
        [DataMember]
        public string AccountNumber { get; set; }
        [DataMember]
        public string AccountDigit { get; set; }
        [DataMember]
        public string AccountType { get; set; }
    }

    [Serializable]
    [DataContract]
    public class RedemptionRequestResult
    {
        [DataMember]
        public bool Result { get; set; }
        [DataMember]
        public int Id { get; set; }
    }

    #endregion

}
