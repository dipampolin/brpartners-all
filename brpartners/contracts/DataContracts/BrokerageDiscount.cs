﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace QX3.Portal.Contracts.DataContracts
{
    [Serializable]
    [DataContract]
    public class BrokerageDiscount
    {
        [DataMember]
        public int RequestId { get; set; }

        [DataMember]
        public int ClientCode { get; set; }

        [DataMember]
        public int DV { get; set; }

        [DataMember]
        public string ClientName { get; set; }

        [DataMember]
        public string Assessor { get; set; }

        [DataMember]
        public string ClientFilter { get; set; }

        [DataMember]
        public int StockExchange { get; set; }

        [DataMember]
        public string MarketType { get; set; }

        [DataMember]
        public string Operation { get; set; }

        [DataMember]
        public string Vigency { get; set; }

        [DataMember]
        public string StockCode { get; set; }

        [DataMember]
        public decimal Qtty { get; set; }

        [DataMember]
        public decimal? StockDiscountValue { get; set; }

        [DataMember]
        public decimal? StockDiscountMinorValue { get; set; }

        [DataMember]
        public decimal? StockDiscountMajorValue { get; set; }

        [DataMember]
        public decimal? StockDiscountPercentual { get; set; }

        [DataMember]
        public decimal? StockDiscountMinorPercentual { get; set; }

        [DataMember]
        public decimal? StockDiscountMajorPercentual { get; set; }

        [DataMember]
        public decimal? ActualDiscount { get; set; }

        [DataMember]
        public string Description { get; set; }
                
        [DataMember]
        public string Situation { get; set; }

        [DataMember]
        public DateTime RequestDate { get; set; }
    }

    [Serializable]
    [DataContract]
    public class FilterOptions 
    {
        [DataMember]
        public int InitRow { get; set; }
        [DataMember]
        public int FinalRow { get; set; }
        [DataMember]
        public int SortColumn { get; set; }
        [DataMember]
        public string SortDirection { get; set; }
        [DataMember]
        public bool AllRecords { get; set; }
    }

    //[DataContract]
    //[Serializable]
    //public enum StockExchange
    //{
    //    [EnumMember(Value="1")]
    //    Bovespa = 1,
    //    [EnumMember(Value="2")]
    //    BMF = 2,
    //    [EnumMember(Value = "3")]
    //    All = 3
    //}

    //[Serializable]
    //[DataContract]
    //public enum OperationType
    //{
    //    [EnumMember(Value = "1")]
    //    All= 1,
    //    [EnumMember(Value = "2")]
    //    Day,
    //    [EnumMember(Value = "3")]
    //    DayTrade
    //}

    //[Serializable]
    //[DataContract]
    //public enum VigencyType 
    //{
    //    [EnumMember(Value = "1")]
    //    D0 = 1,
    //    [EnumMember(Value = "2")]
    //    D1,
    //    [EnumMember(Value = "3")]
    //    Permanent,
    //    [EnumMember(Value = "4")]
    //    All
    //}

    //[Serializable]
    //[DataContract]
    //public enum SituationType {
    //    [EnumMember(Value = "1")]
    //    All = 1,
    //    [EnumMember(Value = "2")]
    //    Approve,
    //    [EnumMember(Value = "3")]
    //    Effect,
    //    [EnumMember(Value = "4")]
    //    Effected,
    //    [EnumMember(Value = "5")]
    //    Reproved }
}
