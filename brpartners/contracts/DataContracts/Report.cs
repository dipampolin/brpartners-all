﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace QX3.Portal.Contracts.DataContracts
{
	#region Commom
	[Serializable][DataContract]
	public class Trader
	{
		[DataMember]
		public Int32 Code { get; set; }
		[DataMember]
		public string Name { get; set; }
	}

	[Serializable][DataContract]
	public class ReportHeader
	{
		[DataMember]
		public string AddressLine1 { get; set; }
		[DataMember]
		public string Number { get; set; }
		[DataMember]
		public string AddressLine2 { get; set; }
		[DataMember]
		public string PostalCode { get; set; }
		[DataMember]
		public string PostalCodeExtension { get; set; }
		[DataMember]
		public string Quarter { get; set; }
		[DataMember]
		public string City { get; set; }
		[DataMember]
		public Int32 PhoneDDDNumber { get; set; }
		[DataMember]
		public Int64 PhoneNumber { get; set; }
		[DataMember]
		public Int32 FaxDDDNumber { get; set; }
		[DataMember]
		public Int64 FaxNumber { get; set; }
		[DataMember]
		public string InternetCode { get; set; }
		[DataMember]
		public string Email { get; set; }
	}

	[Serializable][DataContract]
	public class ReportFooter
	{
		[DataMember]
		public string Disclaimer { get; set; }
	}
	#endregion

	#region Confirmation
	[Serializable][DataContract]
	public class Confirmation
	{
		[DataMember]
		public ReportHeader Header { get; set; }
		[DataMember]
		public int ClientCode { get; set; }
		[DataMember]
		public string ClientName { get; set; }
		[DataMember]
		public string Address { get; set; }
		[DataMember]
		public string PhoneNumber { get; set; }
		[DataMember]
		public string FaxNumber { get; set; }
		[DataMember]
		public string Email { get; set; }
		[DataMember]
		public List<ConfirmationItem> Items { get; set; }
		[DataMember]
		public ReportFooter Footer { get; set; }
	}

	[Serializable][DataContract]
	public class ConfirmationItem
	{
		[DataMember]
		public string Symbol { get; set; }
		[DataMember]
		public Int32 AgentCode { get; set; }
		[DataMember]
		public string OperationType { get; set; }
		[DataMember]
		public Int64 TradeId { get; set; }
		[DataMember]
		public Decimal Quantity { get; set; }
		[DataMember]
		public string ISINCode { get; set; }
		[DataMember]
		public string CompanyName { get; set; }
		[DataMember]
		public string CurrencyName { get; set; }
		[DataMember]
		public Decimal TradeValue { get; set; }
		[DataMember]
		public DateTime TradeDate { get; set; }
		[DataMember]
		public DateTime SettlementDate { get; set; }
		[DataMember]
		public Decimal PrincipalValue { get; set; }
		[DataMember]
		public Decimal CommissionValue { get; set; }
		[DataMember]
		public Decimal TransactionFeeValue { get; set; }
		[DataMember]
		public Decimal NetAmmountValue { get; set; }
		[DataMember]
		public Int32 CustodyCode { get; set; }
	}
	#endregion

	#region Trade Blotter
	[Serializable][DataContract]
	public class TradeBlotter
	{
		[DataMember]
		public ReportHeader Header { get; set; }
		[DataMember]
		public List<TradeBlotterItem> Items { get; set; }
		[DataMember]
		public ReportFooter Footer { get; set; }
	}

	[Serializable][DataContract]
	public class TradeBlotterItem
	{
		[DataMember]
		public Int64 TradeId { get; set; }
		[DataMember]
		public DateTime TradeDate { get; set; }
		[DataMember]
		public DateTime SettlementDate { get; set; } //Data de liquidação
		[DataMember]
		public string ISINCode { get; set; }
		[DataMember]
		public string CompanyName { get; set; }
		[DataMember]
		public Int64 NumberOfShares { get; set; } //Quantidade negociada
		[DataMember]
		public Decimal SharePrice { get; set; } //Valor negócio
		[DataMember]
		public Decimal CustomerBuyAmount { get; set; } //Valor volume de compra
		[DataMember]
		public Decimal NetBuyAmount { get; set; } //Valor liquido de compra
		[DataMember]
		public Decimal CustomerSellAmount { get; set; } //Volume de venda
		[DataMember]
		public Decimal NetSellAmount { get; set; } //Valor liquido de venda
		[DataMember]
		public string CurrencyName { get; set; } //Nome da moeda
		[DataMember]
		public string ClientName { get; set; }
	}
	#endregion

	#region Customer Statement
	[Serializable][DataContract]
	public class CustomerStatement
	{
		[DataMember]
		public ReportHeader Header { get; set; }
		[DataMember]
		public int ClientCode { get; set; }
		[DataMember]
		public string ClientName { get; set; }
		[DataMember]
		public string Address { get; set; }
		[DataMember]
		public string PhoneNumber { get; set; }
		[DataMember]
		public string FaxNumber { get; set; }
		[DataMember]
		public string Email { get; set; }
		[DataMember]
		public DateTime BeginDate { get; set; }
		[DataMember]
		public DateTime EndDate { get; set; }
		[DataMember]
		public List<CustomerStatementItem> Items { get; set; }
		[DataMember]
		public ReportFooter Footer { get; set; }
	}

	[Serializable][DataContract]
	public class CustomerStatementItem
	{
		[DataMember]
		public string CompanyName { get; set; }
		[DataMember]
		public DateTime TradeDate { get; set; }
		[DataMember]
		public string OperationCode { get; set; }
		[DataMember]
		public Decimal SharePrice { get; set; }//Valor de preço medio
		[DataMember]
		public string CurrencyName { get; set; }
		[DataMember]
		public Int32 Shares { get; set; } //Qt_qtdesp 
		[DataMember]
		public Decimal NetValue { get; set; } //Valor liquidos
		[DataMember]
		public DateTime SettlementDate { get; set; } //Data de liquidação
		[DataMember]
		public Decimal SettleValue { get; set; } //Valor de liquidação
		[DataMember]
		public Int32 OutstandingShares { get; set; } //Quantidade liquido de falha
		[DataMember]
		public Decimal OutstandingValues { get; set; } //Valor liquido de falha
	}
	#endregion

	#region Customer Ledger

	[Serializable][DataContract]
	public class CustomerLedger
	{
		[DataMember]
		public ReportHeader Header { get; set; }
		[DataMember]
		public int ClientCode { get; set; }
		[DataMember]
		public string ClientName { get; set; }
		[DataMember]
		public DateTime StartDate { get; set; }
		[DataMember]
		public DateTime EndDate { get; set; }
		[DataMember]
		public List<CustomerLedgerItem> Items { get; set; }
		[DataMember]
		public ReportFooter Footer { get; set; }
	}

	[Serializable][DataContract]
	public class CustomerLedgerItem
	{
		[DataMember]
		public Int32 ConfirmID { get; set; }
		[DataMember]
		public string Security { get; set; }
		[DataMember]
		public DateTime TradeDate { get; set; }
		[DataMember]
		public DateTime SettleDate { get; set; }
		[DataMember]
		public string OperationCode { get; set; }
		[DataMember]
		public Decimal SharePrice { get; set; }
		[DataMember]
		public Int32 Shares { get; set; }
		[DataMember]
		public Decimal NetValue { get; set; }
		[DataMember]
		public string CurrencyName { get; set; }
		[DataMember]
		public Int32 SettledShares { get; set; }
		[DataMember]
		public Decimal SettledValue { get; set; }
		[DataMember]
		public Int32 OutstandingShares { get; set; }
		[DataMember]
		public Decimal OutstandingValues { get; set; }
	}
	#endregion

	#region Stock Record

	[Serializable][DataContract]
	public class StockRecord
	{
		[DataMember]
		public ReportHeader Header { get; set; }
		[DataMember]
		public DateTime StartDate { get; set; }
		[DataMember]
		public DateTime EndDate { get; set; }
		[DataMember]
		public List<StockRecordItem> Items { get; set; }
		[DataMember]
		public ReportFooter Footer { get; set; }
	}

	[Serializable][DataContract]
	public class StockRecordItem
	{
		[DataMember]
		public string CompanyName { get; set; }
		[DataMember]
		public List<StockRecordSubItem> SubItems { get; set; }
	}

	[Serializable][DataContract]
	public class StockRecordSubItem
	{
		[DataMember]
		public Int32 ConfirmID { get; set; }
		[DataMember]
		public string ClientBroker { get; set; }
		[DataMember]
		public DateTime DateSettled { get; set; }
		[DataMember]
		public Int64 Long { get; set; }
		[DataMember]
		public Int64 Short { get; set; }
	}

	public class PreStockRecordReport
	{
		public DateTime Start { get; set; }
		public DateTime End { get; set; }
		public Int32 ConfirmID { get; set; }
		public Int32 ClientCode { get; set; }
		public string ClientBroker { get; set; }
		public DateTime DateSettled { get; set; }
		public Int64 Long { get; set; }
		public Int64 Short { get; set; }
		public string CompanyName { get; set; }
	}

	#endregion

	#region Fail Ledger

	[Serializable][DataContract]
	public class FailLedger
	{
		[DataMember]
		public ReportHeader Header { get; set; }
		[DataMember]
		public List<FailLedgerItem> Items { get; set; }
		[DataMember]
		public FailLedgerType Type { get; set; }
	}

	[Serializable][DataContract]
	public class FailLedgerItem
	{
		[DataMember]
		public int ClientCode { get; set; }
		[DataMember]
		public string ClientName { get; set; }
		[DataMember]
		public Int32 ConfirmID { get; set; }
		[DataMember]
		public string OperationCode { get; set; }
		[DataMember]
		public string Security { get; set; }
		[DataMember]
		public DateTime TradeDate { get; set; }
		[DataMember]
		public DateTime SettleDate { get; set; }
		[DataMember]
		public Int64 Shares { get; set; }
		[DataMember]
		public Decimal NetAmount { get; set; }
		[DataMember]
		public Decimal MarketValuePerShare { get; set; }
		[DataMember]
		public Decimal MarketValueTotal { get; set; }
	}

	public enum FailLedgerType { FailToDeliver, FailToReceive, APCustomer, ARCustomer };

#endregion

}
