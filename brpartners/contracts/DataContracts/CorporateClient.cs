﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QX3.Portal.Contracts.DataContracts
{
    public class CorporateClient
    {
        /// <summary>
        /// CPF/CNPJ
        /// </summary>
        public long Document { get; set; }
        /// <summary>
        /// Nome do Cliente
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Códigos Bolsa / Banco
        /// </summary>
        public List<int> Codes { get; set; }
    }
}
