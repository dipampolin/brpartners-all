﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace QX3.Portal.Contracts.DataContracts
{

    [DataContract]
    public enum QuestionType
    {
        [EnumMember]
        Single = 'R',
        [EnumMember]
        Multiple = 'C',
        [EnumMember]
        Distribution = 'D',
        [EnumMember]
        Matrix = 'M'
    }


    [Serializable]
    [DataContract]
    public class Suitability
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int? SubQuestionId { get; set; }

        [DataMember]
        public int IdQuestion { get; set; }
        [DataMember]
        public string Question { get; set; }

        [DataMember]
        public QuestionType QuestionType { get; set; }

        [DataMember]
        public decimal Weight { get; set; }

        [DataMember]
        public decimal? Percentage { get; set; }

        [DataMember]
        public string PercentageDescription { get; set; }


        [DataMember]
        public List<SuitabilityAnswers> Answers { get; set; }
        [DataMember]
        public List<SuitabilityMatrixColumns> MatrixColumns { get; set; }
    }

    [DataContract]
    public class SuitabilityMatrixColumns
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public int QuestionID { get; set; }
        [DataMember]
        public int SortOrder { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public bool IsActive { get; set; }

    }

    [DataContract]
    public class SuitabilityForm
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public List<Suitability> Questions { get; set; }
        [DataMember]
        public List<SuitabilityScore> Scores { get; set; }
    }

    [Serializable]
    [DataContract]
    public class SuitabilityPerfilCliente
    {
        [DataMember]
        [System.ComponentModel.DisplayName("CPF/CNPJ")]
        public string CnpjCpf { get; set; }

        [DataMember]
        [System.ComponentModel.DisplayName("Data Cadastro")]
        public String DataCadastro { get; set; }

        [DataMember]
        [System.ComponentModel.DisplayName("Descrição")]
        public string Descricao { get; set; }

        [DataMember]
        [System.ComponentModel.DisplayName("Justificação")]
        public string Justificacao { get; set; }

        [DataMember]
        [System.ComponentModel.DisplayName("Desenquadramento")]
        public string Desenquadramento { get; set; }

        [DataMember]
        [System.ComponentModel.DisplayName("Id Cliente")]
        public int IdCliente { get; set; }
    }

    [DataContract]
    public class SuitabilityScore
    {
        [DataMember]
        public decimal Min { get; set; }
        [DataMember]
        public decimal Max { get; set; }
        [DataMember]
        public int TypeId { get; set; }
        [DataMember]
        public string Description { get; set; }
    }


    [DataContract]
    public class SuitabilityInput
    {
        [DataMember]
        public List<SuitabilityInputItem> Items { get; set; }
        [DataMember]
        public int InvestorTypeId { get; set; }

        [DataMember]
        public dynamic htmlBody { get; set; }
        
    }
    [DataContract]
    public class SuitabilityInputItem
    {
        [DataMember]
        public int QuestionId { get; set; }
        [DataMember]
        public int AnswerId { get; set; }
        [DataMember]
        public bool IsCorrect { get; set; }
        [DataMember]
        public double Percentage { get; set; }
        [DataMember]
        public List<SuitabilityMatrixAnswer> MatrixData { get; set; }

    }


    [Serializable]
    [DataContract]
    public class SuitabilityClientsResponse
    {
        [DataMember]
        public string CdBolsa { get; set; }
        [DataMember]
        public string InvestorType { get; set; }
        [DataMember]
        public string Status { get; set; }
        [DataMember]
        public string Unframed { get; set; }

        [DataMember]
        public List<SuitabilityClients> Clients { get; set; }
    }

    [Serializable]
    [DataContract]
    public class SuitabilityClients
    {
        [DataMember]
        public int Id { get; set; }


        [DataMember]
        public int IdCliente { get; set; }

        [DataMember]
        public long CPFCNPJ { get; set; }

        [DataMember]
        public int[] CodigosBolsa { get; set; }


        [DataMember]
        public int InvestorType { get; set; }
        [DataMember]
        public string InvestorTypeDesc
        {
            get
            {
                switch (this.InvestorType)
                {
                    case 0: return String.Empty;
                    case 1: return "Conservador";
                    case 2: return "Moderado";
                    case 3: return "Arrojado";
                    default: return String.Empty;
                }

            }
            set { }
        }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public string StatusDesc
        {
            get
            {
                switch (this.Status)
                {
                    case 0: return String.Empty;
                    case 1: return "Não preencheu";
                    case 2: return "Preencheu";
                    case 3: return "Refazer";
                    case 4: return "Não elegível";
                    default: return String.Empty;
                }

            }
            set { }
        }
        [DataMember]
        public string Unframed { get; set; }
        [DataMember]
        public string ClientName { get; set; }
        [DataMember]
        public DateTime CompletionDate { get; set; }
        [DataMember]
        public SuitabilityClientsUnframed Unframeds { get; set; }

        public string Situacao { get; set; }

        public string Email { get; set; }

        [DataMember]
        public int InvestorKind { get; set; }

        [DataMember]
        public string InvestorKindDesc
        {
            get
            {
                switch (this.InvestorKind)
                {
                    case 0: return String.Empty;
                    case 1: return "Qualificado";
                    case 2: return "Profissional";
                    default: return String.Empty;
                }

            }
            set { }
        }

        [DataMember]
        public string PersonType { get; set; }

        [DataMember]
        public string DueInfo { get; set; }

        [DataMember]
        public bool SendSuitabilityEmail { get; set; }

        [DataMember]
        public bool Blocked { get; set; }

        [DataMember]
        public bool Active { get; set; }

        [DataMember]
        public int ProfileId { get; set; }

        [DataMember]
        public string UserType { get; set; }

        [DataMember]
        public string Login { get; set; }

        [DataMember]
        public bool UpdatedSolutionTech { get; set; }

        [DataMember]
        public bool NotifiedBRP { get; set; }

        [DataMember]
        public int? Score { get; set; }

        [DataMember]
        public bool NeedUpdateDigitalPlatform { get; set; }

        [DataMember]
        public DateTime? SendDateDigitalPlatform { get; set; }
    }

    [Serializable]
    [DataContract]
    public class SuitabilityAnswersResponse
    {
        [DataMember]
        public string Name { get; set; }


        [DataMember]
        public string CPFCNPJ { get; set; }

        [DataMember]
        public DateTime Date { get; set; }
        [DataMember]
        public int IdTypeInvestor { get; set; }
        [DataMember]
        public string TypeInvestor
        {
            get
            {
                switch (this.IdTypeInvestor)
                {
                    case 0: return String.Empty;
                    case 1: return "Conservador";
                    case 2: return "Moderado";
                    case 3: return "Arrojado";
                    default: return String.Empty;
                }

            }
            set { }
        }
        [DataMember]
        public List<SuitabilityAnswers> Answers { get; set; }
        [DataMember]
        public string CdBolsa { get; set; }


        [DataMember]
        public int GlobalClientId { get; set; }

        [DataMember]
        public string Operator { get; set; }
    }

    [Serializable]
    [DataContract]
    public class SuitabilityAnswers
    {
        [DataMember]
        public int IdQuestion { get; set; }
        [DataMember]
        public string Question { get; set; }

        [DataMember]
        public int? SubQuestionId { get; set; }
        [DataMember]
        public string Answer { get; set; }
        [DataMember]
        public int IdTypeInvestor { get; set; }
        [DataMember]
        public decimal Point { get; set; }
        [DataMember]
        public int IdAnswer { get; set; }

        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string TypeInvestor
        {
            get
            {
                switch (this.IdTypeInvestor)
                {
                    case 0: return String.Empty;
                    case 1: return "Conservador";
                    case 2: return "Moderado";
                    case 3: return "Arrojado";
                    default: return String.Empty;
                }

            }
            set { }
        }
        [DataMember]
        public QuestionType QuestionType { get; set; }
        [DataMember]
        public decimal Percentage { get; set; }
        [DataMember]
        public bool IsCorrect { get; set; }
        [DataMember]
        public List<SuitabilityMatrixAnswer> MatrixAnswers { get; set; }

        [DataMember]
        public string PercentageDescription { get; set; }


        [DataMember]
        public List<MatrixResult> MatrixData { get; set; }

    }


    [Serializable]
    [DataContract]
    public class MatrixResult
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public bool IsChecked { get; set; }
    }

    [DataContract]
    public class SuitabilityMatrixAnswer
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public bool IsChecked { get; set; }
    }



    [Serializable]
    [DataContract]
    public enum InvestorType
    {
        [EnumMember]
        [Description("Sem perfil"), DefaultValue(0)]
        NotSet,
        [EnumMember]
        [Description("Conservador"), DefaultValue(1)]
        Conservative,
        [EnumMember]
        [Description("Moderado"), DefaultValue(2)]
        Moderate,
        [EnumMember]
        [Description("Agressivo"), DefaultValue(3)]
        Aggressive
    }

    [Serializable]
    [DataContract]
    public enum SuitabilityStatus
    {
        [EnumMember]
        [Description("Em branco"), DefaultValue(0)]
        Empty,
        [EnumMember]
        [Description("Sem status"), DefaultValue(1)]
        NotSet,
        [EnumMember]
        [Description("Respondido"), DefaultValue(2)]
        Answered,
        [EnumMember]
        [Description("Refazer"), DefaultValue(3)]
        AnswerAgain,
        [EnumMember]
        [Description("Não elegível"), DefaultValue(4)]
        NotElegible,
        [EnumMember]
        [Description("Optou não responder"), DefaultValue(5)]
        ChooseNotAnswer,
        [EnumMember]
        [Description("Definido corretora"), DefaultValue(6)]
        ProfileBrokerage
    }

    [Serializable]
    [DataContract]
    public class SuitabilityUnframed
    {
        [DataMember]
        public string CdMercado { get; set; }
        [DataMember]
        public DateTime DtDatOrd { get; set; }
    }

    [Serializable]
    [DataContract]
    public class SuitabilityUnframedResponse
    {
        [DataMember]
        public int InvestorType { get; set; }
        [DataMember]
        public string InvestorTypeDesc
        {
            get
            {
                switch (this.InvestorType)
                {
                    case 0: return String.Empty;
                    case 1: return "Conservador";
                    case 2: return "Moderado";
                    case 3: return "Arrojado";
                    default: return String.Empty;
                }

            }
            set { }
        }
        [DataMember]
        public DateTime CompletionDate { get; set; }
        [DataMember]
        public List<SuitabilityUnframed> Unframeds { get; set; }
    }

    [Serializable]
    [DataContract]
    public class SuitabilityClientsUnframed
    {
        [DataMember]
        public int IdCliente { get; set; }
        [DataMember]
        public List<SuitabilityUnframedResponse> Profiles { get; set; }

    }


    [DataContract]
    public class SuitabilityInvestorTypeXProduct
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Type { get; set; }

        [DataMember]
        public string ProductCode { get; set; }

        [DataMember]
        public string ProductDescription { get; set; }
    }

}
