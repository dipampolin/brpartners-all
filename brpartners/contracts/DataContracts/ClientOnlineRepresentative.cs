﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace QX3.Portal.Contracts.DataContracts
{
    [Serializable]
    [DataContract]
    public class ClientOnlineRepresentative : ClientOnline
    {
        [DataMember]
        public int IdClientOnline { get; set; }

        [DataMember]
        public int IdRepresentation { get; set; }

        [DataMember]
        public int IdRepresentationForm { get; set; }

        [DataMember]
        public string DescriptionRepresentationForm { get; set; }

        [DataMember]
        public string Birthplace { get; set; }

        [DataMember]
        public DateTime? CssQualification { get; set; }

        [DataMember]
        public DateTime? CssExpired { get; set; }

        [DataMember]
        public string JobNameAgencyRelative { get; set; }

        [DataMember]
        public int TaxResidence { get; set; }

        [DataMember]
        public string TaxCountry { get; set; }

        [DataMember]
        public string TaxNameResidence { get; set; }

        [DataMember]
        public int IdProfile { get; set; }
    }

    [Serializable]
    [DataContract]
    public class Representatives
    {
        public Representatives()
        {
            ClientRepresentatives = new List<ClientOnlineRepresentative>();
        }

        [DataMember]
        public List<ClientOnlineRepresentative> ClientRepresentatives { get; set; }
    }
}