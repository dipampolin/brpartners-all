﻿using System;
using System.Runtime.Serialization;
using QX3.Spinnex.Authentication.Services.UserGroupData;
using System.Collections.Generic;

namespace QX3.Portal.Contracts.DataContracts
{
    [Serializable][DataContract]
    public class UserInformation
    {
        [DataMember]
        public long ID { get; set; }
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string IdClient { get; set; }
        [DataMember]
		public string Email { get; set; }
		[DataMember]
		public int AssessorID { get; set; }
        [DataMember]
        public ACProfile Profile { get; set; }
		[DataMember]
		public List<Int32> Assessors { get; set; }
        [DataMember]
        public bool IsBackOffice { get; set; }
        [DataMember]
        public string Password { get; set; }
        [DataMember]
        public int OtherAttempts { get; set; }
        [DataMember]
        public bool IsBlocked { get; set; }
        [DataMember]
        public ACGroupOfAssessors GroupOfAssessor { get; set; }
        [DataMember]
        public string ADKey { get; set; }
        [DataMember]
        public string UserType { get; set; }
        [DataMember]
        public int CdBolsa { get; set; }
        [DataMember]
        public int InvestorType { get; set; }
        [DataMember]
        public string InvestorTypeDesc
        {
            get
            {
                    switch (this.InvestorType)
                    {
                        case 0: return String.Empty;
                        case 1: return "Perfil: Conservador";
                        case 2: return "Perfil: Moderado";
                        case 3: return "Perfil: Arrojado";
                        default: return String.Empty;
                    }
                
            }
            set { }
        }
        [DataMember]
        public int SuitabilityStatus { get; set; }
        [DataMember]
        public DateTime? SuitabilityDate { get; set; }


        /***************************************************************
         * 15/09/2014: agora temos o conceito de Cliente Global, 
         * ou seja os usuários externos estão associados a um Cliente.
         ***************************************************************/
       /// <summary>
       /// ID do cliente Global.
       /// </summary>
        [DataMember]
        public int? GlobalClientId { get; set; }
        /// <summary>
        /// CPF ou CNPJ do cliente. Usado para identificá-lo em outros sistemas.
        /// </summary>
        [DataMember]
        public long? CpfCnpj { get; set; }

        [DataMember]
        public int InvestorKind { get; set; }

        [DataMember]
        public string InvestorKindDesc
        {
            get
            {
                switch (this.InvestorKind)
                {
                    case 0: return String.Empty;
                    case 1: return "Qualificado";
                    case 2: return "Profissional";
                    default: return String.Empty;
                }

            }
            set { }
        }

        [DataMember]
        public string PersonType { get; set; }

        [DataMember]
        public bool SendSuitabilityEmail { get; set; }

    }

	[Serializable][DataContract]
	public class Assessor
	{
		[DataMember]
		public int ID { get; set; }
		[DataMember]
		public string Name { get; set; }
		[DataMember]
		public List<int> ClientCodes { get; set; }
	}

    [Serializable]
    [DataContract]
    public class StatusLogin 
    {
        [DataMember]
        public bool CanLogIn { get; set; }

        [DataMember]
        public int ErrorType { get; set; }

        [DataMember]
        public string ADKey { get; set; }
    }
}
