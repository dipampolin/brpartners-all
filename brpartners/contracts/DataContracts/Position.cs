﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace QX3.Portal.Contracts.DataContracts
{
    [Serializable]
    [DataContract]
    public class CustodyPositionResponse
    {
        [DataMember]
        public string TipoUsuario { get; set; }
        [DataMember]
        public int CdBolsa { get; set; }
        [DataMember]
        public string TipoMercado { get; set; }
        [DataMember]
        public string DescTipoMercado { get; set; }
        [DataMember]
        public List<CustodyPosition> CustodyPosition { get; set; }
    }

    [Serializable] [DataContract]
    public class CustodyPosition
    {
        [DataMember]
        public string Ativo { get; set; }
        [DataMember]
        public string NomeCarteira { get; set; }
        [DataMember]
        public int NumeroCarteira { get; set; }
        [DataMember]
        public int QtdDisponivel { get; set; }
        [DataMember]
        public int QtdTotal { get; set; }
        [DataMember]
        public int QtdOpLiquidar { get; set; }
        [DataMember]
        public int QtdBloqueada { get; set; }
        [DataMember]
        public int QtdComprasExec { get; set; }
        [DataMember]
        public int QtdComprasAbertas { get; set; }
        [DataMember]
        public int QtdVendasExec { get; set; }
        [DataMember]
        public int QtdVendasAbertas { get; set; }
        [DataMember]
        public decimal ValorAtual { get; set; }
    }

    [Serializable]
    [DataContract]
    public class ConsolidatedPositionResponse2
    {
        [DataMember]
        public string TipoUsuario { get; set; }
        [DataMember]
        public int CdBolsa { get; set; }
        [DataMember]
        public List<ConsolidatedPosition2> ConsolidatedPosition { get; set; }
    }

    [Serializable]
    [DataContract]
    public class ConsolidatedPosition2
    {
        [DataMember]
        public string DescTipoMercado { get; set; }
        [DataMember]
        public string TipoMercado { get; set; }  
        [DataMember]
        public decimal ValorBruto { get; set; }
        [DataMember]
        public List<ConsolidatedPositionDetails> ConsolidatedPositionDetails { get; set; }
    }

    [Serializable]
    [DataContract]
    public class ConsolidatedPositionDetails
    {
        [DataMember]
        public string Ativo { get; set; }
        [DataMember]
        public int QtdDisponivel { get; set; }
        [DataMember]
        public decimal Preco { get; set; }
        [DataMember]
        public decimal ValorAtual { get; set; }
        [DataMember]
        public string TipoMercado { get; set; }  
    }

    [Serializable]
    [DataContract]
    public class Balance
    {
        [DataMember]
        public decimal Inicial { get; set; }
        [DataMember]
        public decimal MovimentoDia { get; set; }
        [DataMember]
        public decimal Final { get; set; }
        
    }

    [Serializable]
    [DataContract]
    public class Available
    {
        [DataMember]
        public string Mercado { get; set; }
        [DataMember]
        public string DescMercado { get; set; }
        [DataMember]
        public decimal ValorDisponivel { get; set; }        
    }

    [Serializable]
    [DataContract]
    public class Projected
    {
        [DataMember]
        public decimal ProjetadoD1 { get; set; }
        [DataMember]
        public decimal ProjetadoD2 { get; set; }
        [DataMember]
        public decimal ProjetadoD3 { get; set; }
        [DataMember]
        public decimal ProjetadoTotal { get; set; }
    }

    [Serializable]
    [DataContract]
    public class Orders
    {
        [DataMember]
        public decimal ValorComprasExec { get; set; }
        [DataMember]
        public decimal ValorComprasEmAberto { get; set; }
        [DataMember]
        public decimal ValorVendasExec { get; set; }
        [DataMember]
        public decimal ValorVendasEmAberto { get; set; }
    }

    [Serializable]
    [DataContract]
    public class FinancialPositionResponse
    {
        [DataMember]
        public string TipoUsuario { get; set; }
        [DataMember]
        public int CdBolsa { get; set; }
        [DataMember]
        public FinancialPosition FinancialPosition { get; set; }
    }

    [Serializable]
    [DataContract]
    public class FinancialPosition
    {
        [DataMember]
        public Balance Saldo { get; set; }
        [DataMember]
        public List<Available> Disponivel { get; set; }
        [DataMember]
        public Orders Ordens { get; set; }
        [DataMember]
        public Projected Projetado { get; set; }
    }

    [Serializable]
    [DataContract]
    public class InvestmentsGroups
    {
        [DataMember]
        public string Group { get; set; }
        [DataMember]
        public string DescGroup{ get; set; }        
    }

    [Serializable]
    [DataContract]
    public class ExtratoRendaDatails
    {
        [DataMember]
        public string tp_registro { get; set; }
        [DataMember]
        public string dt_operacao { get; set; }
        [DataMember]
        public string dc_evento { get; set; }
        [DataMember]
        public string dc_grupo_papel { get; set; }
        [DataMember]
        public string dc_indexador { get; set; }
        [DataMember]
        public decimal tx_operacao { get; set; }
        [DataMember]
        public decimal tx_percentual { get; set; }
        [DataMember]
        public string dt_vencimento { get; set; }
        [DataMember]
        public decimal vl_operacao { get; set; }
        [DataMember]
        public decimal tx_ir { get; set; }
        [DataMember]
        public decimal tx_iof { get; set; }
        [DataMember]
        public decimal vl_bruto { get; set; }
        [DataMember]
        public decimal vl_liquido { get; set; }
    }
}
