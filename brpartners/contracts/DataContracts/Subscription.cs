﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace QX3.Portal.Contracts.DataContracts
{
    [DataContract]
    [Serializable]
    public class Subscription
    {
        [DataMember]
        public int RequestId { get; set; }

        [DataMember]
        public string Company { get; set; }

        [DataMember]
        public string StockCode { get; set; }

        [DataMember]
        public string ISINStock { get; set; }

        [DataMember]
        public string ISINSubscription { get; set; }

        [DataMember]
        public decimal? RightPercentual { get; set; }

        [DataMember]
        public decimal? RightValue { get; set; }

        [DataMember]
        public DateTime? InitialDate { get; set; }

        [DataMember]
        public DateTime? COMDate { get; set; }

        [DataMember]
        public DateTime? BrokerDate { get; set; }

        [DataMember]
        public DateTime? BrokerHour { get; set; }

        [DataMember]
        public DateTime? StockExchangeDate { get; set; }

        [DataMember]
        public DateTime? CompanyDate { get; set; }

        [DataMember]//"P" Para aprovar "A" Aprovada "S" Subscrevendo "F" finalizado
        public char? Status { get; set; }

        [DataMember]
        public string DescStatus { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public string SubjectMail { get; set; }

        [DataMember]
        public string BodyMail { get; set; }

        [DataMember]
        public byte[] Prospect { get; set; }

        [DataMember]
        public int Index { get; set; }
    }

    [DataContract]
    [Serializable]
    public class SubscriptionRights 
    {
        [DataMember]
        public int RequestId { get; set; }

		[DataMember]//a.nm_cliente
		public string ClientName { get; set; }

        [DataMember]//a.cod_cli 
        public int ClientCode { get; set; }

        [DataMember]//a.cod_cli 
        public string ClientCodeFilter { get; set; }

        [DataMember]
        public string AssessorFilter { get; set; }

		[DataMember]//a.nm_assessor
		public string AssessorName { get; set; }

		[DataMember]
		public int AssessorCode { get; set; }

        [DataMember]//a.Cod_Neg
        public string StockCode { get; set; }

        [DataMember]//a.nome_esp_pap 
        public string EspecificationName { get; set; }

		[DataMember]//a.qtde_direito
		public long? RightsQtty { get; set; }

        [DataMember]//a.qtde_disp
        public long? RequestedQtty { get; set; }

        [DataMember]//FUNC_RETORNA_SOLICITADO(a.cod_cli,b.Id_Subscricao) qt_solicitado
        public long? AvailableQtty { get; set; }

        [DataMember]//b.vl_direito
        public decimal? RightValue { get; set; }

        [DataMember]//(a.qtde_disp*b.Vl_Direito)    vl_total,
        public decimal? TotalValue { get; set; }

        [DataMember]//b.Dt_Bolsa - StockExchangeDate
        public DateTime? StockExchangeDate { get; set; }

        [DataMember]//in_sobras
        public char? Scraps { get; set; }

		[DataMember]
		public string DescScraps
		{
			get
			{
				if (this.Scraps.HasValue)
				{
					switch (this.Scraps.Value)
					{
						case 'S':
							return "Sim";
						case 'N':
							return "Não";
						default:
							return this.Scraps.HasValue ? this.Scraps.Value.ToString() : string.Empty;
					}
				}
				else
				{
					return this.Scraps.HasValue ? this.Scraps.Value.ToString() : string.Empty;
				}
			}
			set { }
		}

        [DataMember]//in_aviso
		public char? Warning { get; set; }

		[DataMember]
		public string DescWarning
		{
			get
			{
				if (this.Warning.HasValue)
				{
					switch (this.Warning.Value)
					{
						case 'N':
							return "Não Avisado";
						case 'E':
							return "Email";
						case 'T':
							return "Telefone";
						default:
							return this.Warning.HasValue ? this.Warning.Value.ToString() : string.Empty;
					}
				}
				else
				{
					return this.Warning.HasValue ? this.Warning.Value.ToString() : string.Empty;
				}
			}
			set { }
		}

        [DataMember]//in_status
        public char? Status { get; set; }

		[DataMember]
		public string DescStatus
		{
			get
			{
				if (this.Status.HasValue)
				{
					switch (this.Status.Value)
					{
						case 'A':
							return "Aberto";
                        case 'P':
                            return "Para Efetuar";
						case 'E':
							return "Efetuado";
						default:
							return this.Status.HasValue ? this.Status.Value.ToString() : string.Empty;
					}
				}
				else
				{
					return this.Status.HasValue ? this.Status.Value.ToString() : string.Empty;
				}
			}
			set { }
		}

        [DataMember]
        public DateTime? InitialDate { get; set; }

        [DataMember]
        public DateTime? FinalDate { get; set; }

        [DataMember]
        public string ClientEmail { get; set; }
    }

    [DataContract]
    [Serializable]
    public class SubscriptionNotification 
    {
        [DataMember]
        public int SubscriptionId { get; set; }

        [DataMember]
        public int ClientCode { get; set; }

        [DataMember]//in_aviso
        public string Warning { get; set; }

        [DataMember]
        public string DescWarning
        {
            get
            {
                if (!String.IsNullOrEmpty(this.Warning))
                {
                    switch (this.Warning)
                    {
                        case "N":
                            return "Não Avisado";
                        case "E":
                            return "Email";
                        case "T":
                            return "Telefone";
                        default:
                            return !String.IsNullOrEmpty(this.Warning) ? this.Warning : string.Empty;
                    }
                }
                else
                {
                    return !String.IsNullOrEmpty(this.Warning) ? this.Warning : string.Empty;
                }
            }
            set { }
        }
    }
}
