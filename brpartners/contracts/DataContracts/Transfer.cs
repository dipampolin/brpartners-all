﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace QX3.Portal.Contracts.DataContracts
{
    [Serializable]
    [DataContract]
    public class TransferBovespaItem
    {
        public TransferBovespaItem()
        {
            this.Client = new CommonType();
        }
        
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public CommonType Client { get; set; }
        [DataMember]
        public string CNPJ { get; set; }
    }

    [Serializable]
    [DataContract]
    public class TransferBovespaFilter
    {
        [DataMember]
        public string ClientCode { get; set; }
        [DataMember]
        public string CNPJ { get; set; }
    }

    [Serializable]
    [DataContract]
    public class TransferBMFItem
    {
        [DataMember]
        public int BondId { get; set; }

        [DataMember]
        public int BrokerId { get; set; }

        [DataMember]
        public string BrokerName { get; set; }

        [DataMember]
        public int ClientCode { get; set; }

        [DataMember]
        public string ClientName { get; set; }

        [DataMember]
        public string CPFCGC { get; set; }

        [DataMember]
        public string Type { get; set; }

        [DataMember]
        public string TypeDesc { get; set; }
    }

    [Serializable]
    [DataContract]
    public class TransferBMFFilter: TransferBMFItem
    {
        [DataMember]
        public string Situation { get; set; }

        [DataMember]
        public int? UserId { get; set; }
    }
       
}
