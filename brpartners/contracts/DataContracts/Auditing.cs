﻿using System;
using System.Runtime.Serialization;

namespace QX3.Portal.Contracts.DataContracts
{
	[Serializable][DataContract]
	public class Auditing
	{
		[DataMember]
		public string User { get; set; }
		[DataMember]
		public string IP { get; set; }
		[DataMember]
		public DateTime Timestamp { get; set; }
		[DataMember]
		public string Module { get; set; }
		[DataMember]
		public string Funtionality { get; set; }
		[DataMember]
		public string Parameters { get; set; }
	}
}
