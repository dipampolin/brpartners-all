﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace QX3.Portal.Contracts.DataContracts
{
    [Serializable]
    [DataContract]
	public class VAMItem
    {
        [DataMember]
        public int ClientCode { get; set; }

        [DataMember]
        public int ClientDigit { get; set; }

        [DataMember]
		public string ClientName { get; set; }

        [DataMember]
		public int AssessorCode { get; set; }

        [DataMember]
		public string AssessorName { get; set; }

        [DataMember]
		public string Market { get; set; }

        [DataMember]
		public string MarketDescription { get; set; }

        [DataMember]
		public int Portfolio { get; set; }

        [DataMember]
		public string PortfolioDescription { get; set; }

        [DataMember]
		public decimal Price { get; set; }

        [DataMember]
		public Int64 TotalQuantity { get; set; }

        [DataMember]
		public Int64 AvailableQuantity { get; set; }

        [DataMember]
		public Int64 BloquedQuantity { get; set; }

        [DataMember]
		public Int64 PendingQuantity { get; set; }

        [DataMember]
        public Int64 QuantityToSettle { get; set; }

        [DataMember]
        public Int64 NegotiableQuantity { get; set; }
        
        [DataMember]
        public Int64 D1Quantity { get; set; }

        [DataMember]
        public Int64 D2Quantity { get; set; }

        [DataMember]
        public Int64 D3Quantity { get; set; }

         [DataMember]
		public string DueDate { get; set; }
    }
}
