﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace QX3.Portal.Contracts.DataContracts
{
	[Serializable][DataContract]
	public class Proceeds
	{
		[DataMember]
		public string StockCode { get; set; }
		[DataMember]
		public string CompanyName { get; set; }
		[DataMember]
		public string ISIN { get; set; }
		[DataMember]
		public int AssessorCode { get; set; }
		[DataMember]
		public string AssessorName { get; set; }
		[DataMember]
		public int ClientCode { get; set; }
		[DataMember]
		public string ClientName { get; set; }
		[DataMember]
		public string Proceed { get; set; }
		[DataMember]
		public Int64 Quantity { get; set; }
		[DataMember]
		public Decimal GrossValue { get; set; }
		[DataMember]
		public Decimal IR { get; set; }
		[DataMember]
		public Decimal IRValue { get; set; }
		[DataMember]
		public Decimal NetValue { get; set; }
		[DataMember]
		public DateTime PaymentDate { get; set; }
		[DataMember]
		public int Portfolio { get; set; }
	}

	[Serializable][DataContract]
	public class ProceedsFilter
	{
		[DataMember]
		public int ClientCode { get; set; }
		[DataMember]
		public string Assessors { get; set; }
		[DataMember]
		public string StockCode { get; set; }
        [DataMember]
        public string CompanyName { get; set; }
		[DataMember]
		public string StartDate { get; set; }
		[DataMember]
		public string EndDate { get; set; }
        [DataMember]
        public char Type { get; set; }
        [DataMember]
        public string ClientStart { get; set; }
        [DataMember]
        public string ClientEnd { get; set; }
	}

}
