﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace QX3.Portal.Contracts.DataContracts
{
    [Serializable]
    [DataContract]
    public class ProjectedReportItem
    {
        [DataMember]
        public string ClientID { get; set; }
        [DataMember]
        public string ClientName { get; set; }
        [DataMember]
        public string Assessor { get; set; }
        [DataMember]
        public DateTime TradingDate { get; set; }
        [DataMember]
        public decimal Percentage { get; set; }
        [DataMember]
        public int Note { get; set; }
        [DataMember]
        public decimal Volume { get; set; }
        [DataMember]
        public decimal Brokerage { get; set; }
        [DataMember]
        public decimal Record { get; set; }
        [DataMember]
        public decimal Fees { get; set; }
        [DataMember]
        public DateTime SettlementDate { get; set; }
        [DataMember]
        public decimal ANA { get; set; }
        [DataMember]
        public decimal IRRF { get; set; }
        [DataMember]
        public decimal NetValue { get; set; }
    }

    [Serializable]
    [DataContract]
    public class ProjectedReportFilter
    {
        [DataMember]
        public string Assessors { get; set; }
        [DataMember]
        public string Clients { get; set; }
        [DataMember]
        public string TradingDate { get; set; }
        [DataMember]
        public string SettlementDate { get; set; }
        [DataMember]
        public char Type { get; set; }
    }
}
