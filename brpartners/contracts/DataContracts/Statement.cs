﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace QX3.Portal.Contracts.DataContracts
{
    #region Client

    [Serializable]
    [DataContract]
    public class StatementClient
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public long CpfCnpj { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public int ClientCode { get; set; } // Código Bolsa

        [DataMember]
        public string PersonType { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public bool? AutomaticSending { get; set; }

        [DataMember]
        public List<int> StatementIDs { get; set; }
    }

    [Serializable]
    [DataContract]
    public class StatementClientFilter
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public long? CpfCnpj { get; set; }

        [DataMember]
        public int? ClientId { get; set; }

        [DataMember]
        public int? ClientCode { get; set; }

        [DataMember]
        public int? AutomaticSending { get; set; }
    }

    #endregion

    #region Fixed Income

    [Serializable]
    [DataContract]
    public class FixedIncomeFilter
    {
        [DataMember]
        public long CpfCnpj { get; set; }

        [DataMember]
        public DateTime? Start { get; set; }

        [DataMember]
        public DateTime? End { get; set; }
    }

    [Serializable]
    [DataContract]
    public class FixedIncomeDetails
    {
        [DataMember]
        public string tp_registro { get; set; }

        [DataMember]
        public string dt_operacao { get; set; }

        [DataMember]
        public string dc_evento { get; set; }

        [DataMember]
        public string dc_grupo_papel { get; set; }

        [DataMember]
        public string dc_indexador { get; set; }

        [DataMember]
        public decimal tx_operacao { get; set; }

        [DataMember]
        public decimal tx_percentual { get; set; }

        [DataMember]
        public string dt_vencimento { get; set; }

        [DataMember]
        public decimal vl_operacao { get; set; }

        [DataMember]
        public decimal tx_ir { get; set; }

        [DataMember]
        public decimal tx_iof { get; set; }

        [DataMember]
        public decimal vl_bruto { get; set; }

        [DataMember]
        public decimal vl_liquido { get; set; }
    }

    #endregion

    #region NDF

    [Serializable]
    [DataContract]
    public class NdfFilter
    {
        [DataMember]
        public long CpfCnpj { get; set; }

        [DataMember]
        public int Month { get; set; }

        [DataMember]
        public int Year { get; set; }

        [DataMember]
        public NdfSwapType Type { get; set; }
    }

    [Serializable]
    [DataContract]
    public enum NdfSwapType
    {
        [EnumMember]
        CurvaContabil = 0,

        [EnumMember]
        CurvaMercado = 1
    }

    [Serializable]
    [DataContract]
    public class NdfDetails
    {
        [DataMember]
        public string tp_operacao { get; set; }

        [DataMember]
        public string dt_operacao { get; set; }

        [DataMember]
        public string dt_vencimento { get; set; }

        [DataMember]
        public string tp_moeda_ref { get; set; }

        [DataMember]
        public string tp_moeda_cot { get; set; }

        [DataMember]
        public string dc_criterio_termo { get; set; }

        [DataMember]
        public string dc_criterio_vencto { get; set; }

        [DataMember]
        public decimal vl_cot_termo_fwd { get; set; }

        [DataMember]
        public decimal vl_base { get; set; }

        [DataMember]
        public decimal vl_bruto { get; set; }

        [DataMember]
        public decimal vl_liquido { get; set; }

        [DataMember]
        public decimal vl_base_moeda_estr { get; set; }

        [DataMember]
        public decimal vl_ir { get; set; }

        [DataMember]
        public decimal vlr_resultado_mtm { get; set; }

    }

    #endregion

    #region SWAP

    [Serializable]
    [DataContract]
    public class SwapFilter
    {
        [DataMember]
        public long CpfCnpj { get; set; }

        [DataMember]
        public int Month { get; set; }

        [DataMember]
        public int Year { get; set; }

        [DataMember]
        public NdfSwapType Type { get; set; }
    }

    [Serializable]
    [DataContract]
    public class SwapDetails
    {
        [DataMember]
        public string tp_operacao { get; set; }

        [DataMember]
        public string dt_operacao { get; set; }

        [DataMember]
        public string dt_vencimento { get; set; }

        [DataMember]
        public decimal vl_base { get; set; }

        [DataMember]
        public string dc_index_dado { get; set; }

        [DataMember]
        public decimal vl_perc_index_dado { get; set; }

        [DataMember]
        public decimal vl_taxa_dado { get; set; }

        [DataMember]
        public string dc_index_tomado { get; set; }

        [DataMember]
        public decimal vl_perc_index_tomado { get; set; }

        [DataMember]
        public decimal vl_taxa_tomado { get; set; }

        [DataMember]
        public decimal vl_curva_dado { get; set; }

        [DataMember]
        public decimal vl_curva_tomado { get; set; }

        [DataMember]
        public decimal vl_bruto { get; set; }

        [DataMember]
        public decimal vl_ir { get; set; }

        [DataMember]
        public decimal vl_liquido { get; set; }

        [DataMember]
        public decimal vl_mtm_dado { get; set; }

        [DataMember]
        public decimal vl_mtm_tomado { get; set; }

        [DataMember]
        public decimal vlr_resultado_mtm { get; set; }

    }

    #endregion

    #region Configuration and Log

    [Serializable]
    [DataContract]
    public class StatementConfiguration
    {
        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public bool HasConfiguration { get; set; }

        [DataMember]
        public bool AutomaticSending { get; set; }

        [DataMember]
        public List<int> StatementIDs { get; set; }
    }

    [Serializable]
    [DataContract]
    public class StatementLog
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public long? CpfCnpj { get; set; }

        [DataMember]
        public int? ClientCode { get; set; }

        [DataMember]
        public int? ClientId { get; set; }

        [DataMember]
        public int Year { get; set; }

        [DataMember]
        public int Month { get; set; }

        [DataMember]
        public List<int> StatementIDs { get; set; }

        [DataMember]
        public DateTime SendDate { get; set; }
    }

    #endregion

    #region Statement types

    [Serializable]
    [DataContract]
    public enum StatementTypes
    {
        [EnumMember]
        [Description("Renda Fixa")]
        RendaFixa = 1,

        [EnumMember]
        [Description("NDF (Curva Contábil)")]
        NDFCurvaContábil = 2,

        [EnumMember]
        [Description("NDF (Curva Mercado)")]
        NDFCurvaMercado = 3,

        [EnumMember]
        [Description("Swap (Curva Contábil)")]
        SwapCurvaContábil = 4,

        [EnumMember]
        [Description("Swap (Curva Mercado)")]
        SwapCurvaMercado = 5,

        [EnumMember]
        [Description("CCB/Fiança")]
        CCBFiança = 6,

        [EnumMember]
        [Description("Opções Flexíveis")]
        OpçõesFlexíveis = 7,

        [EnumMember]
        [Description("Conta Cliente")]
        ContaCliente = 8,

        [EnumMember]
        [Description("Moeda Estrangeira")]
        MoedaEstrangeira = 9
    }

    #endregion
}


