﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace QX3.Portal.Contracts.DataContracts
{
    [Serializable]
    [DataContract]
    public class OrdersCorrection
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public CommonType Type { get; set; }
        [DataMember]
        public CommonType ClientFrom { get; set; }
        [DataMember]
        public CommonType ClientTo { get; set; }
        [DataMember]
        public CommonType Description { get; set; }
        [DataMember]
        public CommonType Responsible { get; set; }
        [DataMember]
        public string Comments { get; set; }
        [DataMember]
        public DateTime Date { get; set; }
        [DataMember]
        public CommonType Status { get; set; }
        [DataMember]
        public List<OrdersCorrectionDetail> Details { get; set; }
        [DataMember]
        public long UserID { get; set; }
        [DataMember]
        public bool D1 { get; set; }
    }

    [Serializable]
    [DataContract]
    public class OrdersCorrectionDetail
    {
        [DataMember]
        public string StockCode { get; set; }
        [DataMember]
        public int Quantity { get; set; }
        [DataMember]
        public decimal Value { get; set; }
    }

    [Serializable]
    [DataContract]
    public class OrdersCorrectionFilter
    {
        [DataMember]
        public string Assessors { get; set; }
        [DataMember]
        public string Clients { get; set; }
        [DataMember]
        public int StatusID { get; set; }
        [DataMember]
        public int TypeID { get; set; }
        [DataMember]
        public string StartDate { get; set; }
        [DataMember]
        public string EndDate { get; set; }
        [DataMember]
        public int? UserId { get; set; }
    }
}
