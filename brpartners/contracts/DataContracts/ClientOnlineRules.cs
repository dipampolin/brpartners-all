﻿using System;
using System.Collections.Generic;

namespace QX3.Portal.Contracts.DataContracts
{
    public class ClientOnlineRules
    {
        public ClientOnlineRules()
        {
            Representatives = new List<DocumentRepresentative>();
            DeleteRepresentatives = new List<string>();
        }

        public int IdClientOnline { get; set; }
        public int IdDocument { get; set; }
        public int IdDocumentType { get; set; }
        public DateTime? StartValidateDocument { get; set; }
        public DateTime? FinalValidateDocument { get; set; }

        public List<DocumentRepresentative> Representatives { get; set; }
        public List<string> DeleteRepresentatives { get; set; }
    }

    public class DocumentRepresentative
    {
        public int Id { get; set; }
        public int IdRepresentative { get; set; }
        public string NameRepresentative { get; set; }
        public string NumberDocument { get; set; }
        public int IdQualification { get; set; }
        public string DescriptionQualification { get; set; }
    }
}