﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel;

namespace QX3.Portal.Contracts.DataContracts
{
    [Serializable]
    [DataContract]
    public class IndividualPerson 
    {
        [DataMember]
        public bool IsRenew { get; set; }

        [DataMember]
        public string CPF { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public DateTime? BirthDate { get; set; }

        [DataMember]
        public string Nationality { get; set; }

        [DataMember]
        public string Naturality { get; set; }

        [DataMember]
        public Gender? Gender { get; set; }

        [DataMember]
        public string FatherName { get; set; }

        [DataMember]
        public string MotherName { get; set; }

        [DataMember]
        public bool? PPE { get; set; }

        [DataMember]
        public bool? QualifiedInvestor { get; set; }
        
        [DataMember]
        public string PersonalMail { get; set; }

        [DataMember]
        public string ProfessionalMail { get; set; }

        [DataMember]
        public string InstantMail { get; set; }
        
        [DataMember]
        public ContactType? PersonalChoiceContact{ get; set; }

        [DataMember]
        public AddressType? PersonalAddressType { get; set; }

        [DataMember]
        public List<PersonAddress> Addresses { get; set; }  

        [DataMember]
        public List<Phone> Phones{ get; set; }
        
        [DataMember]
        public Profession Occupation{ get; set; }

        [DataMember]
        public CivilState CivilState { get; set; }

        [DataMember]
        public DocumentInfo Document{ get; set; }

        [DataMember]
        public List<AuthorizedAccount> DepositAccounts { get; set; }

        [DataMember]
        public List<References> References { get; set; }

        [DataMember]
        public EstimatedPatrimony EstimatedPatrimony { get; set; }

        [DataMember]
        public EstimatedPatrimonyComposition EstimatedPatrimonyComposition { get; set; }

        [DataMember]
        public decimal? TotalPatrimony { get; set; }

        [DataMember]
        public List<PatrimonyOrigin> PatrimonyOrigins { get; set; }

        [DataMember]
        public string OtherOrigins { get; set; }

        [DataMember]
        public Investment Investment { get; set; }

        [DataMember]
        public Liquidity Liquidity { get; set; }

        [DataMember]
        public AnnualIncome AnnualIncome { get; set; }

        [DataMember]
        public Transaction Transaction { get; set; }

        [DataMember]
        public TransactionCTVM TransactionCTVM { get; set; }

        [DataMember]
        public string Administrator { get; set; }

        [DataMember]
        public bool? NonResidentInvestor { get; set; }

        [DataMember]
        public bool? OperatesOnItsOwn { get; set; }

        [DataMember]
        public bool? AuthorizesTransmissionOrders { get; set; }

        [DataMember]
        public bool? BrokerBound { get;set; }

        [DataMember]
        public List<ImobileGood> ImobileGoods { get; set; }

        [DataMember]
        public List<MobileGood> MobileGoods { get; set; }

        [DataMember]
        public List<MensalRendiment> MensalRendiments { get; set; }

        [DataMember]
        public List<AuthorizedAccount> AuthorizedAccounts { get; set; }

        [DataMember]
        public ApprovalTerm? ApprovalTerm { get; set; }

        [DataMember]
        public string CVM { get; set; }

        [DataMember]
        public string RDE { get; set; }

        [DataMember]
        public string OriginCountry { get; set; }

        [DataMember]
        public string Custodian { get; set; }

        [DataMember]
        public string LegalRepresentative { get; set; }

        [DataMember]
        public string TributeRepresentative { get; set; }

        [DataMember]
        public string CoLegalRepresentative { get; set; }

        [DataMember]
        public bool? CollectiveAccount { get; set; }

        [DataMember]
        public string CollectiveAccountTitular { get; set; }

        [DataMember]
        public List<AuthorizedMember> AuthorizedMembers { get; set; }
    }

    [Serializable]
    [DataContract]
    public class LegalPerson 
    {
        [DataMember]
        public bool IsRenew { get; set; }

        [DataMember]
        public string CNPJ { get; set; }

        [DataMember]
        public string CorporateName { get; set; }

        [DataMember]
        public string FancyName { get; set; }

        [DataMember]
        public string NIRE { get; set; }

        [DataMember]
        public string StateInscription { get; set; }

        [DataMember]
        public string PrincipalActivity { get; set; }

        [DataMember]
        public DateTime? ConstitutionDate { get; set; }

        [DataMember]
        public ConstitutionForm? ConstitutionForm { get; set; }

        [DataMember]
        public String ConstitutionFormOtherDescription { get; set; }

        [DataMember]
        public bool? OpenCapital { get; set; }

        [DataMember]
        public StockControlType? StockControlType { get; set; }

        [DataMember]
        public CapitalType? CapitalType { get; set; }

        [DataMember]
        public bool? QualifiedInvestor { get; set; }

        [DataMember]
        public PersonAddress Address { get; set; }

        [DataMember]
        public string Site { get; set; }

        [DataMember]
        public string Email1 { get; set; }

        [DataMember]
        public string Email2 { get; set; }

        [DataMember]
        public List<Phone> Contacts { get; set; }

        [DataMember]
        public DateTime? BaseData { get; set; }

        [DataMember]
        public Decimal? Capital { get; set; }

        [DataMember]
        public Decimal? IntegratedCapital { get; set; }

        [DataMember]
        public Decimal? NetWorth { get; set; }

        [DataMember]
        public List<GrossValue> GrossValues { get; set; }

        [DataMember]
        public string AccountingContactName { get; set; }

        [DataMember]
        public string AccountingContactPhone { get; set; }

        [DataMember]
        public List<StockControl> StockControls { get; set; }

        [DataMember]
        public List<ControlledLegalPerson> ControlledLegalPerson { get; set; }

        [DataMember]
        public List<AffiliatedLegalPerson> AffiliatedLegalPerson { get; set; }

        [DataMember]
        public List<AttorneyLegalPerson> AttorneyLegalPerson { get; set; }

        [DataMember]
        public List<AuthorizedAccount> ReferenceAccounts { get; set; }

        [DataMember]
        public List<References> References { get; set; }

        [DataMember]
        public List<AuthorizedAccount> AuthorizedAccountsSame { get; set; }

        [DataMember]
        public List<AuthorizedAccount> AuthorizedAccountsOthers { get; set; }

        [DataMember]
        public string Administrator { get; set; }

        [DataMember]
        public string CVM { get; set; }

        [DataMember]
        public string RDE { get; set; }

        [DataMember]
        public string OriginCountry { get; set; }

        [DataMember]
        public string Custodian { get; set; }

        [DataMember]
        public string LegalRepresentative { get; set; }

        [DataMember]
        public string TributeRepresentative { get; set; }

        [DataMember]
        public string CoLegalRepresentative { get; set; }

        [DataMember]
        public bool? CollectiveAccount { get; set; }

        [DataMember]
        public string CollectiveAccountTitular { get; set; }

        [DataMember]
        public bool? OperatesOnItsOwn { get; set; }

        [DataMember]
        public bool? AuthorizesTransmissionOrders { get; set; }

        [DataMember]
        public bool? BrokerBound { get; set; }

        [DataMember]
        public List<ImobileGood> ImobileGoods { get; set; }

        [DataMember]
        public List<MobileGood> MobileGoods { get; set; }

        [DataMember]
        public List<MensalRendiment> MensalRendiments { get; set; }

        [DataMember]
        public string FinancialPositionDate { get; set; }

        [DataMember]
        public Decimal? PL { get; set; }

        [DataMember]
        public Decimal? TotalPatrimony { get; set; }

        [DataMember]
        public ApprovalTerm? ApprovalTerm { get; set; }

        [DataMember]
        public List<AuthorizedMember> AuthorizedMembers { get; set; }

        [DataMember]
        public bool? PPE { get; set; }

        [DataMember]
        public List<AuthorizedMember> PoliticalMembers { get; set; }
    }

    #region CNPJ Types
    
    [Serializable]
    [DataContract]
    public enum ConstitutionForm 
    { 
        [EnumMember][Description("S/A")] SA, 
        [EnumMember][Description("LTDA")] LTDA, 
        [EnumMember][Description("Outros")] Other 
    }
    
    [Serializable]
    [DataContract]
    public enum StockControlType 
    { 
        [EnumMember][Description("Nacional")] National, 
        [EnumMember][Description("Estrangeiro")] Foreign, 
        [EnumMember][Description("Misto")] Mixed 
    }

    [Serializable]
    [DataContract]
    public enum CapitalType 
    { 
        [EnumMember][Description("Estatal")] StateOwner, 
        [EnumMember][Description("Privado")] PrivateOwner, 
        [EnumMember][Description("Misto")] Mixed 
    }

    [Serializable]
    [DataContract]
    public class GrossValue 
    {
        [DataMember]
        public int Year { get; set; }
        [DataMember]
        public Decimal Value { get; set; }
    }

    [Serializable]
    [DataContract]
    public class StockControl 
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string CPFCNPJ { get; set; }
        [DataMember]
        public DateTime? EntryDate { get; set; }
        [DataMember]
        public Decimal? Participation { get; set; }
    }

    [Serializable]
    [DataContract]
    public class BaseLegalPerson 
    {
        [DataMember]
        public string CorporateName { get; set; }

        [DataMember]
        public string CPFCNPJ { get; set; }
    }

    [Serializable]
    [DataContract]
    public class ControlledLegalPerson: BaseLegalPerson { }

    [Serializable]
    [DataContract]
    public class AffiliatedLegalPerson : BaseLegalPerson { }

    [Serializable]
    [DataContract]
    public class AttorneyLegalPerson : BaseLegalPerson 
    {
        [DataMember]
        public DateTime? BirthDate { get; set; }

        [DataMember]
        public string Role { get; set; }
    }

    [Serializable]
    [DataContract]
    public class List 
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string CPF { get; set; }
    }

    #endregion

    [Serializable]
    [DataContract]
    public enum Gender 
    { 
        [EnumMember][Description("Masculino")] Male, 
        [EnumMember][Description("Feminino")] Female 
    }
    
    [Serializable]
    [DataContract]
    public enum ContactType 
    { 
        [EnumMember][Description("Residencial")] ResidentialPhone,
        [EnumMember][Description("Comercial")] ComercialPhone,
        [EnumMember][Description("Celular")] CellularPhone,
        [EnumMember][Description("Fax")] FaxPhone,
        [EnumMember][Description("Pessoal")] PersonalMail,
        [EnumMember][Description("Profissional")] ProfessionalMail 
    }

    [Serializable]
    [DataContract]
    public enum AddressType 
    { 
        [EnumMember][Description("Residencial")] Residential, 
        [EnumMember][Description("Comercial")] Comercial, 
        [EnumMember][Description("Outros")] Other 
    }

    [Serializable]
    [DataContract]
    public class PersonAddress : Address 
    {
        [DataMember]
        public AddressType Type { get; set; }

        [DataMember]
        public State? State { get; set; }
    }

    [Serializable]
    [DataContract]
    public enum DocumentType 
    { 
        [EnumMember][Description("RG")] RG, 
        [EnumMember][Description("RNE")] RNE, 
        [EnumMember][Description("CNH")] CNH, 
        [EnumMember][Description("Passaporte")] Passport, 
        [EnumMember][Description("Registro Profissional")] Professional 
    }

    [Serializable]
    [DataContract]
    public class DocumentInfo 
    {
        [DataMember]
        public DocumentType? DocumentType { get; set; }
        [DataMember]
        public string DocumentNumber { get; set; }
        [DataMember]
        public DateTime? EmissorDate { get; set; }
        [DataMember]
        public string Emissor { get; set; }
        [DataMember]
        public State? DocumentState { get; set; }

    }

    /*[Serializable]
    [DataContract]
    public class State
    {
        //private string[,] states = { { "AC", "Acre" } };

        [DataMember]
        public string StateCode { get; set; }
        [DataMember]
        public string StateDescription { get; set; }
    }*/

    [Serializable]
    [DataContract]
    public enum CivilStateType 
    { 
        [EnumMember][Description("Solteiro")] Single,
        [EnumMember][Description("Casado")] Married,
        [EnumMember][Description("Divorciado")] Divorced,
        [EnumMember][Description("Separado")] Separated,
        [EnumMember][Description("Viúvo")] Widower,
        [EnumMember][Description("União estável")] StableUnion 
    }

    [Serializable]
    [DataContract]
    public enum PropertyRegime 
    { 
        [EnumMember][Description("Comunhão parcial")] PartialCommunion, 
        [EnumMember][Description("Comunhão universal")] UniversalCommunion, 
        [EnumMember][Description("Participação final")] FinalParticipation, 
        [EnumMember][Description("Separação total")] TotalSeparation 
    }

    [Serializable]
    [DataContract]
    public class CivilState 
    {
        [DataMember]
        public CivilStateType? CivilStateType { get; set; }
        [DataMember]
        public PropertyRegime? PropertyRegime { get; set; }
        [DataMember]
        public string SpouseName { get; set; }
        [DataMember]
        public string SpouseCPF { get; set; }
    }

    [Serializable]
    [DataContract]
    public class Phone 
    {
        [DataMember]
        public ContactType ContactType { get; set; }
        [DataMember]
        public int DDDCode { get; set; }
        [DataMember]
        public string PhoneNumber { get; set; }
    }

    [Serializable]
    [DataContract]
    public enum BestProfessionalOption 
    {
        [EnumMember][Description("Empregado de empresa do setor privado (exceto de Instituições Financeiras)")] PrivateCompanyFinancyException,
        [EnumMember][Description("Empregado de empresa pública ou de economia mista (exceto de Instituições Financeiras)")] PublicOrMixedCompanyFinancyException,
        [EnumMember][Description("Empregado de Instituições Financeiras públicas ou privadas")] FinancyCompany,
        [EnumMember][Description("Profissional Liberal ou Autônomo")] Autonomous,
        [EnumMember][Description("Proprietário de empresa ou de firma individual ou empregador-titular")] CompanyOwner,
        [EnumMember][Description("Capitalista, que auferiu rendimentos de capital, inclusive de aluguéis")] Capitalist,
        [EnumMember][Description("Aposentado, pensionista da previdência ou militar da reserva")] Retired,
        [EnumMember][Description("Outros")] Other
    }

    [Serializable]
    [DataContract]
    public class Profession 
    {
        [DataMember]
        public BestProfessionalOption? BestProfessionalOption { get; set; }
        [DataMember]
        public string Occupation { get; set; }
        [DataMember]
        public string Role { get; set; }
        [DataMember]
        public string Company { get; set; }
        [DataMember]
        public string PropertyCNPJ { get; set; }
    }

    [Serializable]
    [DataContract]
    public class References 
    {
        public string Name { get; set; }

        //public List<Phone> Phones { get; set; }

        public int PhoneCode { get; set; }
        public string PhoneNumber { get; set; }

        public AddressType Type { get; set; }

        public ReferenceType? ReferType { get; set; }
    }

    [Serializable]
    [DataContract]
    public enum ReferenceType
    {
        [EnumMember][Description("Pessoal")] Personal, [EnumMember][Description("Comercial")] Comercial
    }

    [Serializable]
    [DataContract]
    public enum Yield 
    {
        [EnumMember][Description("Até R$ 300.000")] Until300000,
        [EnumMember][Description("De R$ 300.000 a R$ 500.000")] Until500000,
        [EnumMember][Description("De R$ 500.000 a R$ 1.000.000")] Until1000000,
        [EnumMember][Description("De R$ 1.000.000 a R$ 2.000.000")] Until2000000,
        [EnumMember][Description("De R$ 2.000.000 a R$ 3.000.000")] Until3000000,
        [EnumMember][Description("Acima de R$ 3.000.000 ")] Above3000000
    }

    [Serializable]
    [DataContract]
    public class EstimatedPatrimony 
    {
        [DataMember]
        public Yield Yield { get; set; }
        [DataMember]
        public Decimal? Other { get; set; }
    }

    [Serializable]
    [DataContract]
    public class EstimatedPatrimonyComposition 
    {
        public decimal? ApplicationPerc { get; set; }

        public decimal? CorporatePerc { get; set; }

        public decimal? HabitationPerc { get; set; }

        public decimal? LandPerc { get; set; }

        public decimal? OtherPerc { get; set; }
    }

    [Serializable]
    [DataContract]
    public enum PatrimonyOrigin 
    {
        [EnumMember][Description("Atividade Profissional")] Profession,
        [EnumMember][Description("Investimento em imóveis")] Habitation,
        [EnumMember][Description("Atividade Produtiva (agrícola, pecuária, etc.)")] Commodities,
        [EnumMember][Description("Investimento em ações")] StockActions,
        [EnumMember][Description("Participação Societária")] CorporateParticipation,
        [EnumMember][Description("Herança")] Heritage,
        [EnumMember][Description("Outros")] Other
    }

    [Serializable]
    [DataContract]
    public enum LiquidityValues
    {
        [EnumMember][Description("Até R$ 100.000")] Until100000,
        [EnumMember][Description("De R$100.000 a R$ 300.000")] Until300000,
        [EnumMember][Description("De R$ 300.000 a R$ 500.000")] Until500000,
        [EnumMember][Description("De R$ 500.000 a R$ 1.000.000")] Until1000000,
        [EnumMember][Description("De R$ 1.000.000 a R$ 2.000.000")] Until2000000,
        [EnumMember][Description("Acima de R$ 2.000.000")] Above2000000
    }

    [Serializable]
    [DataContract]
    public class Liquidity
    {
        [DataMember]
        public LiquidityValues Value { get; set; }
        [DataMember]//somente se maior q 2000000
        public Decimal? Other { get; set; }
    }

    [Serializable]
    [DataContract]
    public enum AnnualIncomeValues
    {
        [EnumMember][Description("Até R$ 100.000")] Until100000,
        [EnumMember][Description("De R$ 100.000 a R$ 200.000")] Until200000,
        [EnumMember][Description("De R$ 200.000 a R$ 300.000")] Until300000,
        [EnumMember][Description("De R$ 300.000 a R$ 400.000")] Until400000,
        [EnumMember][Description("De R$ 400.000 a R$ 500.000")] Until500000,
        [EnumMember][Description("De R$ 500.000 a R$ 600.000")] Until600000,
        [EnumMember][Description("De R$ 600.000 a R$ 700.000")] Until700000,
        [EnumMember][Description("De R$ 700.000 a R$ 800.000")] Until800000,
        [EnumMember][Description("Acima de R$ 800.000")] Above800000
    }

    [Serializable]
    [DataContract]
    public class AnnualIncome
    {
        [DataMember]
        public AnnualIncomeValues Value { get; set; }
        [DataMember]
        public Decimal? Other { get; set; }
    }

    [Serializable]
    [DataContract]
    public enum InvestmentValues
    {
        [EnumMember]
        [Description("Até R$ 100.000")]
        Until100000,
        [EnumMember]
        [Description("De R$ 100.000,01 a R$ 300.000")]
        Until300000,
        [EnumMember]
        [Description("De R$ 300.000,01 a R$ 500.000")]
        Until500000,
        [EnumMember]
        [Description("De R$ 500.000,01 a R$ 1.000.000")]
        Until1000000,
        [EnumMember]
        [Description("De R$ 1.000.000,01 a R$ 2.000.000")]
        Until2000000,
        [EnumMember]
        [Description("Acima de R$ 2.000.000,01")]
        Above2000000
    }

    [Serializable]
    [DataContract]
    public class Investment
    {
        [DataMember]
        public InvestmentValues Value { get; set; }
        [DataMember]
        public Decimal? Other { get; set; }
    }

    [Serializable]
    [DataContract]
    public enum TransactionValues
    {
        [EnumMember][Description("Até R$ 100.000")] Until100000,
        [EnumMember][Description("Até R$ 500.000")] Until500000,
        [EnumMember][Description("Até R$ 1.000.000")] Until1000000,
        [EnumMember][Description("Até R$ 2.000.000")] Until2000000,
        [EnumMember][Description("Até R$ 3.000.000")] Until3000000,
        [EnumMember][Description("Acima de R$ 4.000.000")] Above3000000
    }

    [Serializable]
    [DataContract]
    public class Transaction
    {
        [DataMember]
        public TransactionValues Value { get; set; }
        [DataMember]
        public Decimal? Other { get; set; }
    }

    [Serializable]
    [DataContract]
    public enum TransactionCTVMValues
    {
        [EnumMember]
        [Description("Até R$ 100.000")]
        Until100000,
        [EnumMember]
        [Description("De R$ 100.000,01 a R$ 500.000")]
        Until500000,
        [EnumMember]
        [Description("De R$ 500.000,01 a R$ 1.000.000")]
        Until1000000,
        [EnumMember]
        [Description("De R$ 1.000.000,01 a R$ 2.000.000")]
        Until2000000,
        [EnumMember]
        [Description("De R$ 2.000.000,01 a R$ 3.000.000")]
        Until3000000,
        [EnumMember]
        [Description("Acima de R$ 3.000.000,01")]
        Above3000000
    }

    [Serializable]
    [DataContract]
    public class TransactionCTVM
    {
        [DataMember]
        public TransactionCTVMValues Value { get; set; }
        [DataMember]
        public Decimal? Other { get; set; }
    }

    [Serializable]
    [DataContract]
    public enum GoodKind 
    { 
        [EnumMember][Description("Bem")] Good,
        [EnumMember][Description("Direito")] Right 
    }

    [Serializable]
    [DataContract]
    public enum MensalRendimentType 
    {
        [EnumMember][Description("Salário")] Salary,
        [EnumMember][Description("Pró-Labore")] ProLabore,
        [EnumMember][Description("Outros")] Other
    }

    [Serializable]
    [DataContract]
    public class ImobileGood
    {
        [DataMember]
        public GoodKind? Type { get; set; }
        [DataMember]
        public Decimal? Value { get; set; }
        [DataMember]
        public PersonAddress Address { get; set; }
    }

    [Serializable]
    [DataContract]
    public class MobileGood 
    {
        [DataMember]
        public GoodKind? Type { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public Decimal? Value { get; set; }
    }

    [Serializable]
    [DataContract]
    public class MensalRendiment 
    {
        [DataMember]
        public MensalRendimentType Type { get; set; }

        [DataMember]
        public string PJType { get; set; }

        [DataMember]
        public decimal? Value { get; set; }

        [DataMember]
        public string Description { get; set; }
    }

    [Serializable]
    [DataContract]
    public class AuthorizedAccount 
    {
        [DataMember]
        public string TitularName { get; set; }
        [DataMember]
        public string CPF { get; set; }
        [DataMember]
        public string CoTitularName { get; set; }
        [DataMember]
        public string CoCPF { get; set; }
        [DataMember]
        public BankAccount Account { get; set; }
    }

    [Serializable]
    [DataContract]
    public class AuthorizedMember 
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string CPF { get; set; }
        [DataMember]
        public string RG { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public DateTime? BirthDate { get; set; }
    }

    [Serializable]
    [DataContract]
    public enum ApprovalTerm 
    { 
        [EnumMember][Description("Concordo")] Approve,
        [EnumMember][Description("Não Concordo")] NotApprove,
        [EnumMember][Description("Concordo sob consulta")] OnRequest 
    }

    [Serializable]
    [DataContract]
    public enum State 
    { 
        [EnumMember]AC,
        [EnumMember]AL,
        [EnumMember]AM,
        [EnumMember]AP,
        [EnumMember]BA,
        [EnumMember]CE,
        [EnumMember]DF,
        [EnumMember]ES,
        [EnumMember]GO,
        [EnumMember]MA,
        [EnumMember]MG,
        [EnumMember]MS,
        [EnumMember]MT,
        [EnumMember]PA,
        [EnumMember]PB,
        [EnumMember]PE,
        [EnumMember]PI,
        [EnumMember]PR,
        [EnumMember]RJ,
        [EnumMember]RN,
        [EnumMember]RO,
        [EnumMember]RR,
        [EnumMember]RS,
        [EnumMember]SC,
        [EnumMember]SE,
        [EnumMember]SP,
        [EnumMember]TO
    }
}
