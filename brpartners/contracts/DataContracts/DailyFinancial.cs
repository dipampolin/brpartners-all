﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace QX3.Portal.Contracts.DataContracts
{
    [DataContract]
    [Serializable]
    public class DailyFinancial
    {
        [DataMember]
        public int ClientID { get; set; }

        [DataMember]
        public int DV { get; set; }

        [DataMember]
        public string ClientName { get; set; }

        [DataMember]
        public int AssessorID { get; set; }

        [DataMember]
        public string AssessorName { get; set; }

        [DataMember]
        public decimal AccountBalance { get; set; }

        [DataMember]
        public decimal Credit { get; set; }

        [DataMember]
        public decimal Debt { get; set; }

        [DataMember]
        public decimal Finished { get; set; }

        [DataMember]
        public decimal LiquidResult { get; set; }

        [DataMember]
        public decimal FinalBalance { get; set; }

        [DataMember]
        public decimal ProjectedBalance { get; set; }

        [DataMember]
        public decimal AnteriorBalance { get; set; }

        [DataMember]
        public bool? CustodyTax { get; set; }
    }

    [DataContract]
    [Serializable]
    public class DailyFinancialFilter : DailyFinancial 
    {
        [DataMember]
        public char? Situation { get; set; }

        [DataMember]
        public bool D0 { get; set; }

        [DataMember]
        public string AssessorFilter { get; set; }

        [DataMember]
        public string ClientFilter { get; set; }

        [DataMember]
        public DateTime? MovementDate { get; set; }
    }
}   
