﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace QX3.Portal.Contracts.DataContracts
{

    [Serializable]
    [DataContract]
    public class ExtractRendaResponse
    {
        [DataMember]
        public string TipoUsuario { get; set; }
        [DataMember]
        public string CodEmpresa { get; set; }
        [DataMember]
        public string Cnpjcpf { get; set; }
        [DataMember]
        public string DtRefDe { get; set; }
        [DataMember]
        public string DtRefAte { get; set; }
        [DataMember]
        public List<ExtractRenda> Extrato { get; set; }
    }
    [Serializable]
    [DataContract]
    public class ExtractRenda
    {
        [DataMember]
        public string tp_registro { get; set; }
        [DataMember]
        public string dt_operacao { get; set; }
        [DataMember]
        public string dc_evento { get; set; }
        [DataMember]
        public string dc_grupo_papel { get; set; }
        [DataMember]
        public string dc_indexador { get; set; }
        [DataMember]
        public decimal tx_operacao { get; set; }
        [DataMember]
        public decimal tx_percentual { get; set; }
        [DataMember]
        public string dt_vencimento { get; set; }
        [DataMember]
        public decimal vl_operacao { get; set; }
        [DataMember]
        public decimal tx_ir { get; set; }
        [DataMember]
        public decimal tx_iof { get; set; }
        [DataMember]
        public decimal vl_bruto { get; set; }
        [DataMember]
        public decimal vl_liquido { get; set; }
    }

    [Serializable]
    [DataContract]
    public class ExtractNDFResponse
    {
        [DataMember]
        public string TipoUsuario { get; set; }
        [DataMember]
        public int Mes { get; set; }
        [DataMember]
        public int Ano { get; set; }
        [DataMember]
        public string Cnpjcpf { get; set; }
        [DataMember]
        public string CodEmpresa { get; set; }
        [DataMember]
        public List<ExtractNDF> ExtratoNDF { get; set; }
    }
    [Serializable]
    [DataContract]
    public class ExtractNDF
    {
        [DataMember]
        public string tp_operacao { get; set; }
        [DataMember]
        public string dt_operacao { get; set; }
        [DataMember]
        public string dt_vencimento { get; set; }
        [DataMember]
        public decimal vl_base_moeda_estr { get; set; }
        [DataMember]
        public string tp_moeda_ref { get; set; }
        [DataMember]
        public string tp_moeda_cot { get; set; }
        [DataMember]
        public string dc_criterio_termo { get; set; }
        [DataMember]
        public decimal vl_cotacao { get; set; }
        [DataMember]
        public decimal vl_bruto { get; set; }
        [DataMember]
        public decimal vl_ir { get; set; }
        [DataMember]
        public decimal vl_liquido { get; set; }
    }

    [Serializable]
    [DataContract]
    public class ExtractSWPResponse
    {
        [DataMember]
        public string TipoUsuario { get; set; }
        [DataMember]
        public string DtRef { get; set; }
        [DataMember]
        public string Cnpjcpf { get; set; }
        [DataMember]
        public string CodEmpresa { get; set; }
        [DataMember]
        public List<ExtractSWP> ExtratoSWP { get; set; }
        [DataMember]
        public int Mes { get; set; }
        [DataMember]
        public int Ano { get; set; }
    }
    [Serializable]
    [DataContract]
    public class ExtractSWP
    {
        [DataMember]
        public string tp_operacao { get; set; }
        [DataMember]
        public string dt_operacao { get; set; }
        [DataMember]
        public string dt_vencimento { get; set; }
        [DataMember]
        public decimal vl_base { get; set; }
        [DataMember]
        public string dc_index_dado { get; set; }
        [DataMember]
        public decimal vl_perc_index_data { get; set; }
        [DataMember]
        public decimal vl_taxa_dada { get; set; }
        [DataMember]
        public string dc_index_tomado { get; set; }
        [DataMember]
        public decimal vl_perc_index_tomado { get; set; }
        [DataMember]
        public decimal vl_taxa_tomado { get; set; }
        [DataMember]
        public decimal vl_curva_dado { get; set; }
        [DataMember]
        public decimal vl_curva_tomado { get; set; }
        [DataMember]
        public decimal vl_bruto { get; set; }
        [DataMember]
        public decimal vl_ir { get; set; }
        [DataMember]
        public decimal vl_liquido { get; set; }
    }

    [Serializable]
    [DataContract]
    public class Movimentation
    {
        [DataMember]
        public DateTime DtLiquidacao { get; set; }
        [DataMember]
        public DateTime DtMovimentacao { get; set; }
        [DataMember]
        public string DsLancamento { get; set; }
        [DataMember]
        public decimal VlLancamento { get; set; }
    }

    [Serializable]
    [DataContract]
    public class ExtractResponse
    {
        [DataMember]
        public string TipoUsuario { get; set; }
        [DataMember]
        public int CdBolsa { get; set; }
        [DataMember]
        public string Mes { get; set; }
        [DataMember]
        public string Ano { get; set; }
        [DataMember]
        public Extract Extrato { get; set; }
    }

    [Serializable]
    [DataContract]
    public class Extract
    {
        [DataMember]
        public decimal VlInicial { get; set; }
        [DataMember]
        public decimal VlFinal { get; set; }
        [DataMember]
        public List<Movimentation> Movimentacao { get; set; }
    }

    [Serializable]
    [DataContract]
    public class TradingSummaryResponse
    {
        [DataMember]
        public string TipoUsuario { get; set; }
        [DataMember]
        public int CdBolsa { get; set; }
        [DataMember]
        public string Ativo { get; set; }
        [DataMember]
        public string Mes { get; set; }
        [DataMember]
        public List<TradingSummary> ResumoNegociacao { get; set; }


    }

    [Serializable]
    [DataContract]
    public class TradingSummary
    {
        [DataMember]
        public DateTime Data { get; set; }
        [DataMember]
        public int QtdCompra { get; set; }
        [DataMember]
        public int QtdVenda { get; set; }
        [DataMember]
        public decimal Preco { get; set; }
        [DataMember]
        public decimal TotalFinCompra { get; set; }
        [DataMember]
        public decimal TotalFinVenda { get; set; }
        [DataMember]
        public decimal FinanceiroLiquido { get; set; }


    }

    [Serializable]
    [DataContract]
    public class ProceedsDemonstrativeResponse
    {
        [DataMember]
        public string TipoUsuario { get; set; }
        [DataMember]
        public int CdBolsa { get; set; }
        [DataMember]
        public string Tipo { get; set; }
        [DataMember]
        public DateTime dtInicial { get; set; }
        [DataMember]
        public DateTime dtFinal { get; set; }
        [DataMember]
        public List<ProceedsDemonstrative> Proventos { get; set; }
    }

    [Serializable]
    [DataContract]
    public class ProceedsDemonstrative
    {
        [DataMember]
        public DateTime Pagamento { get; set; }
        [DataMember]
        public string Ativo { get; set; }
        [DataMember]
        public string Empresa { get; set; }
        [DataMember]
        public string Provento { get; set; }
        [DataMember]
        public int Quantidade { get; set; }
        [DataMember]
        public decimal Valor { get; set; }
        [DataMember]
        public decimal ValorBruto { get; set; }
        [DataMember]
        public decimal PercIr { get; set; }
        [DataMember]
        public decimal ValorLiquido { get; set; }


    }

    [Serializable]
    [DataContract]
    public class BrokerageNoteResponse
    {
        [DataMember]
        public string TipoUsuario { get; set; }
        [DataMember]
        public int CdBolsa { get; set; }
        [DataMember]
        public DateTime dtInicial { get; set; }
        [DataMember]
        public DateTime dtFinal { get; set; }
        [DataMember]
        public List<BrokerageNote> NotasCorretagem { get; set; }

        [DataMember]
        public BrokerageNoteHeader Cabecalho { get; set; }

        [DataMember]
        public BrokerageNoteSummary Resumo { get; set; }

        [DataMember]
        public List<BrokerageNoteBody> Corpo { get; set; }

    }

    [Serializable]
    [DataContract]
    public class BrokerageNote
    {
        [DataMember]
        public DateTime DtNota { get; set; }
        [DataMember]
        public int NrNota { get; set; }

        [DataMember]
        public List<BrokerageNoteBody> Corpo { get; set; }

    }

    [Serializable]
    [DataContract]
    public class BrokerageNoteHeader
    {
        [DataMember]
        public string NomeCliente { get; set; }
        [DataMember]
        public string Logradouro { get; set; }
        [DataMember]
        public string NrLogradouro { get; set; }
        [DataMember]
        public string Complemento { get; set; }
        [DataMember]
        public string Bairro { get; set; }
        [DataMember]
        public string Cep { get; set; }
        [DataMember]
        public string Cidade { get; set; }
        [DataMember]
        public string Uf { get; set; }
        [DataMember]
        public string Cid { get; set; }
        [DataMember]
        public decimal Cpf { get; set; }
        [DataMember]
        public int CdCliente { get; set; }

        [DataMember]
        public int NrNota { get; set; }
        [DataMember]
        public DateTime DtPregao { get; set; }

    }

    [Serializable]
    [DataContract]
    public class BrokerageNoteSummary
    {
        [DataMember]
        public decimal VlDebentures { get; set; }
        [DataMember]
        public decimal Logradouro { get; set; }
        [DataMember]
        public decimal VlVendasVista { get; set; }
        [DataMember]
        public decimal VlComprasVista { get; set; }
        [DataMember]
        public decimal VlVendasOpc { get; set; }
        [DataMember]
        public decimal VlComprasOpc { get; set; }
        [DataMember]
        public decimal VlTermo { get; set; }
        [DataMember]
        public decimal VlTitulos { get; set; }
        [DataMember]
        public decimal VlOperacoes { get; set; }

        [DataMember]
        public decimal VlLiquidoOperacoes { get; set; }
        [DataMember]
        public string VlLiquidoOperacoesDC { get; set; }
        [DataMember]
        public decimal TaxaLiquidacao { get; set; }
        [DataMember]
        public string TaxaLiquidacaoDC { get; set; }
        [DataMember]
        public decimal TaxaRegistro { get; set; }
        [DataMember]
        public string TaxaRegistroDC { get; set; }
        [DataMember]
        public decimal TotalCBLC { get; set; }
        [DataMember]
        public string TotalCBLCDC { get; set; }

        [DataMember]
        public decimal TaxaTermoOpc { get; set; }
        [DataMember]
        public decimal TaxaANA { get; set; }
        [DataMember]
        public decimal Emolumentos { get; set; }
        [DataMember]
        public decimal TotalBovespaSoma { get; set; }

        [DataMember]
        public decimal Corretagem { get; set; }
        [DataMember]
        public decimal ISS { get; set; }
        [DataMember]
        public decimal IRRF { get; set; }
        [DataMember]
        public decimal VlBaseIR { get; set; }
        [DataMember]
        public decimal Outras { get; set; }
        [DataMember]
        public decimal TotalCorretagem { get; set; }
        [DataMember]
        public decimal Liquido { get; set; }
        [DataMember]
        public string LiquidoDC { get; set; }
        [DataMember]
        public string LiquidoDt { get; set; }

        [DataMember]
        public string TpNegocio { get; set; }

        [DataMember]
        public decimal VlBaseIrDT { get; set; }
        [DataMember]
        public decimal VlBaseIrOper { get; set; }

        [DataMember]
        public string EspecDivEstorno { get; set; }



    }

    [Serializable]
    [DataContract]
    public class BrokerageNoteBody
    {
        [DataMember]
        public string Qualificado { get; set; }
        [DataMember]
        public string Negociacao { get; set; }
        [DataMember]
        public string NatOp { get; set; }
        [DataMember]
        public string TipoMercado { get; set; }
        [DataMember]
        public string Especificacao { get; set; }
        [DataMember]
        public string Obs { get; set; }
        [DataMember]
        public int Quatidade { get; set; }
        [DataMember]
        public decimal Preco { get; set; }
        [DataMember]
        public decimal Valor { get; set; }
        [DataMember]
        public string DC { get; set; }
    }

    [Serializable]
    [DataContract]
    public class ExtractFunction
    {
        [DataMember]
        public string OPDTBASE { get; set; }
        [DataMember]
        public string PADTVCTO { get; set; }
        [DataMember]
        public string PANROPER { get; set; }
        [DataMember]
        public int OPBASECLC { get; set; }
        [DataMember]
        public string OPQTDDPARC_PANRPARC { get; set; }
        [DataMember]
        public string CLNOMECLI { get; set; }
        [DataMember]
        public string PATXJRAP { get; set; }
        [DataMember]
        public string PAVLRPR { get; set; }
        [DataMember]
        public string OPCODMD_MDSIMB { get; set; }
        [DataMember]
        public string SLDDATA { get; set; }
        [DataMember]
        public string VLRFINAL { get; set; }
        [DataMember]
        public string SLDMORA { get; set; }
        [DataMember]
        public string SLDMULTA { get; set; }
        [DataMember]
        public string SLDIOF { get; set; }
        [DataMember]
        public string SLDDESPTAR { get; set; }
        [DataMember]
        public string SLDDEV { get; set; }
    }
}
