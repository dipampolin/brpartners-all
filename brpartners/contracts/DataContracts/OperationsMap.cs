﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace QX3.Portal.Contracts.DataContracts
{
    [DataContract]
    [Serializable]
    public class OperationsMap
    {
        [DataMember]
        public int Line { get; set; }

        [DataMember]
        public int OperatorCode { get; set; }

        [DataMember]
        public string Operator { get; set; }

        [DataMember]
        public int ClientCode { get; set; }

        [DataMember]
        public DateTime? TradingDate { get; set; }

        [DataMember]
        public char? Operation { get; set; }

        [DataMember]
        public string OperationDesc { 
            get
            {
                if (this.Operation.HasValue)
                {
                    switch (this.Operation.Value)
                    {
                        case 'C': return "Compra";
                        case 'V': return "Venda";
                        default: return String.Empty;
                    }
                }
                else return String.Empty;
            }
            set{} 
        }

        [DataMember]
        public string StockCode { get; set; }

        [DataMember]
        public int? TradingQtty { get; set; }

        [DataMember]
        public decimal? TradingValue { get; set; }

        [DataMember]
        public decimal? Volume { get; set; }

        [DataMember]
        public decimal? BrokerageValue { get; set; }

    }

    [DataContract]
    [Serializable]
    public class OperationMapFilter : OperationsMap 
    {
        [DataMember]
        public string AssessorFilter { get; set; }

        [DataMember]
        public string ClientFilter { get; set; }

        [DataMember]
        public string Login { get; set; }

        [DataMember]
        public bool Master { get; set; }

        [DataMember]
        public int? UserId { get; set; }
    }

    [DataContract]
    [Serializable]
    public class Operator 
    {
        [DataMember]
        public int Position { get; set; }
        [DataMember]
        public int OperatorCode { get; set; }
        [DataMember]
        public string OperatorName { get; set; }
        [DataMember]
        public decimal? BrokerageValue { get; set; }
    }

    [DataContract]
    [Serializable]
    public class OperationClient : Client 
    {
        [DataMember]
        public DateTime? TradingDate { get; set; }

        [DataMember]
        public string Status { get; set; }

        [DataMember]
        public string DescStatus { get;set;}
    }

    [DataContract]
    [Serializable]
    public class OperationClientFilter : OperationClient
    {
        [DataMember]
        public string AssessorFilter { get; set; }

        [DataMember]
        public string ClientFilter { get; set; }
    }

	[DataContract]
	[Serializable]
	public class OperationReport
	{
		[DataMember]
		public int ClientCode { get; set; }

		[DataMember]
		public string ClientName { get; set; }

		[DataMember]
		public DateTime TradeDate { get; set; }

		[DataMember]
		public List<OperationReportDetails> ReportDetails { get; set; }

		[DataMember]
		public List<OperationReportFinancial> ReportFinancial { get; set; }
	}

	[DataContract]
	[Serializable]
	public class OperationReportDetails
	{
		[DataMember]
		public string TradeType { get; set; }

		[DataMember]
		public string OperationType { get; set; }

        [DataMember]
        public string OperationTypeDesc { 
            get 
            {
                switch (this.OperationType) 
                {
                    case "C": return "Compra";
                    case "V": return "Venda";
                    default:
                        return "-";
                }
            } 
            set { }
        }

		[DataMember]
		public string MarketName { get; set; }

		[DataMember]
		public string TradeCode { get; set; }

		[DataMember]
		public string MarketType { get; set; }

		[DataMember]
		public string MarketTypeDesc
		{
			get
			{
				switch (this.MarketType)
				{
					case "TER": return "Termo";
					case "VIS": return "A Vista";
					case "OPC": return "Opção";
					default:
						return "-";
				}
			}
			set { }
		}

		[DataMember]
		public int TermDueDateType { get; set; }

		[DataMember]
		public string CompanyName { get; set; }

		[DataMember]
		public string SpecificationCode { get; set; }

		[DataMember]
		public string IsinCode { get; set; }

		[DataMember]
		public string Header { get; set; }

		[DataMember]
		public int AvailableQuantity { get; set; }

		[DataMember]
		public decimal TradeValue { get; set; }

		[DataMember]
		public DateTime SettlementDate { get; set; }

		[DataMember]
		public decimal TotalTradeValue { get; set; }

		[DataMember]
		public decimal TotalNet { get; set; }

		[DataMember]
		public decimal BrokerageValue { get; set; }
	}

	[DataContract]
	[Serializable]
	public class OperationReportFinancial
	{
		[DataMember]
		public string MarketType { get; set; }

		[DataMember]
		public string TradeType { get; set; }

		[DataMember]
		public int TotalBuying { get; set; }

		[DataMember]
		public int TotalSelling { get; set; }

		[DataMember]
		public int TotalTrade { get; set; }

		[DataMember]
		public decimal BrokerageValue { get; set; }

		[DataMember]
		public decimal OperationalTaxCBLC { get; set; }

		[DataMember]
		public decimal OperationalTaxBovespa { get; set; }

		[DataMember]
		public decimal OperationalTax { get; set; }

		[DataMember]
		public decimal OtherExpenses { get; set; }

		[DataMember]
		public decimal IRRFDaytrade { get; set; }

		[DataMember]
		public decimal IRRFOperations { get; set; }

		[DataMember]
		public decimal TotalNet { get; set; }

		[DataMember]
		public DateTime SettlementDate { get; set; }
	}
}
