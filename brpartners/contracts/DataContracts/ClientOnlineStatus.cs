﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace QX3.Portal.Contracts.DataContracts
{
    [Serializable]
    [DataContract]
    public class ClientOnlineStatus
    {
        [DataMember]
        public int IdClient { get; set; }

        [DataMember]
        public string ClientName { get; set; }

        [DataMember]
        public string CpfCnpj { get; set; }

        [DataMember]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Birthdate { get; set; }

        [DataMember]
        public string Telephone { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string Gender { get; set; }
    }

    public class ClientsOnlineStatus
    {
        public ClientsOnlineStatus()
        {
            Clients = new List<ClientOnlineStatus>();
        }

        public List<ClientOnlineStatus> Clients { get; set; }
    }
}