﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace QX3.Portal.Contracts.DataContracts
{
	[Serializable][DataContract]
	public class BrokerageTransfer
	{
		[DataMember]
		public int ClientBrokerCode { get; set; }
		[DataMember]
		public int ClientCode { get; set; }
		[DataMember]
		public string ClientName { get; set; }
		[DataMember]
		public DateTime BrokerageDate { get; set; }
		[DataMember]
		public Decimal BrokerageValue { get; set; }
		[DataMember]
		public Decimal TransferPercentage { get; set; }
		[DataMember]
		public Decimal TransferValue { get; set; }
		[DataMember]
		public Decimal TransferValueToDolar { get; set; }
		[DataMember]
		public DateTime? SettlementDate { get; set; }
		[DataMember]
		public string UserName { get; set; }
		[DataMember]
		public BrokerageTransferStatus Status { get; set; }
	}

	public enum BrokerageTransferStatus { Pending, Settled };

	[Serializable]
	[DataContract]
	public class BrokerageRange
	{
		[DataMember]
		public int ClientCode { get; set; }
		[DataMember]
		public string ClientName { get; set; }
		[DataMember]
		public int AssessorCode { get; set; }
        [DataMember]
        public string AssessorName { get; set; }
		[DataMember]
		public List<BrokerageBovespa> Bovespa { get; set; }
		[DataMember]
		public List<BrokerageBMF> BMF { get; set; }

        public BrokerageRange() { this.Bovespa = new List<BrokerageBovespa>(); this.BMF = new List<BrokerageBMF>(); }
	}

    [Serializable]
    [DataContract]
    public class BrokerageRangeFilter 
    {
        [DataMember]
        public string ClientFilter { get; set; }

        [DataMember]
        public string AssessorFilter { get; set; }

        [DataMember]
        public string Brokerage { get; set; }

        [DataMember]
        public int? UserID { get; set; }
    }

	[Serializable]
	[DataContract]
	public class BrokerageBovespa
	{
		[DataMember]
		public string MarketType { get; set; }
		[DataMember]
		public decimal? NormalOperationRate { get; set; }
		[DataMember]
		public decimal? DayTradeRate { get; set; }
	}

	[Serializable]
	[DataContract]
	public class BrokerageBMF
	{
		[DataMember]
		public string Stock { get; set; }
		[DataMember]
		public decimal? Quantity { get; set; }
		[DataMember]
		public string MarketType { get; set; }
		[DataMember]
		public decimal? DueMonthStart { get; set; }
		[DataMember]
		public decimal? DueMonthFinish { get; set; }
		[DataMember]
		public decimal? RangeContractStart { get; set; }
		[DataMember]
		public decimal? RangeContractFinish { get; set; }
		[DataMember]
		public decimal? NormalOperationRate { get; set; }
		[DataMember]
		public decimal? DayTradeRate { get; set; }
	}
}
