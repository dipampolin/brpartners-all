﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace QX3.Portal.Contracts.DataContracts
{
    [Serializable]
    [DataContract]
    public class Private
    {
        [DataMember]
        public string CPFCGC { get; set; }

        [DataMember]
        public string ClientName { get; set; }

        [DataMember]
        public string Product { get; set; }

        [DataMember]
        public decimal? BrokerageValue { get; set; }

        [DataMember]
        public decimal? PositionValue { get; set; }

        [DataMember]
        public decimal? AvailableValue { get; set; }

        [DataMember]
        public decimal? CaptationValue { get; set; }

        [DataMember]
        public decimal? ApplicationValue { get; set; }

        [DataMember]
        public decimal? RedemptionValue { get; set; }
    }
}
