﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace QX3.Portal.Contracts.DataContracts
{
    [Serializable]
    [DataContract]
    public class PortfolioPosition
    {
        [DataMember]
        public CommonType Client { get; set; }
        [DataMember]
        public CommonType Assessor { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string AssessorMail { get; set; }
        [DataMember]
        public string Phone { get; set; }
        [DataMember]
        public DateTime StartDate { get; set; }
        [DataMember]
        public decimal StartValue { get; set; }
        [DataMember]
        public string Bound { get; set; }
        [DataMember]
        public string Comments { get; set; }
        [DataMember]
        public decimal Discount { get; set; }
        [DataMember]
        public decimal DayTradeDiscount { get; set; }
        [DataMember]
        public decimal AvailableValue { get; set; }
        [DataMember]
        public decimal ProjectedValueD1 { get; set; }
        [DataMember]
        public decimal ProjectedValueD2 { get; set; }
        [DataMember]
        public decimal ProjectedValueD3 { get; set; }
        [DataMember]
        public decimal TradesValue { get; set; }
        [DataMember]
        public decimal IbovValue { get; set; }
        [DataMember]
        public DateTime IbovDate { get; set; }
        [DataMember]
        public decimal IbovValueD1 { get; set; }
        [DataMember]
        public bool SendHistory { get; set; }
        [DataMember]
        public bool SendPdf { get; set; }
        [DataMember]
        public List<PortfolioPositionItem> Details { get; set; }
    }

    [Serializable]
    [DataContract]
    public class PortfolioPositionItem
    {
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public string MarketGroup { get; set; }
        [DataMember]
        public string MarketType { get; set; }
        [DataMember]
        public string StockCode { get; set; }
        [DataMember]
        public string TermStockCode { get; set; }
        [DataMember]
        public string BuyDate { get; set; }
        [DataMember]
        public string DueDate { get; set; }
        [DataMember]
        public string RollOverDate { get; set; }
        [DataMember]
        public Int64 Quantity { get; set; }
        [DataMember]
        public decimal BuyValue { get; set; }
        [DataMember]
        public decimal SellNetValue { get; set; }
        [DataMember]
        public decimal BuyNetValue { get; set; }
        [DataMember]
        public decimal Price { get; set; }
        [DataMember]
        public decimal SellValue { get; set; }
        [DataMember]
        public decimal ProfitLoss { get; set; }
        [DataMember]
        public decimal Percentage { get; set; }
        [DataMember]
        public string Sector { get; set; }
        [DataMember]
        public decimal Quotation { get; set; }
        [DataMember]
        public decimal AdditionalValue { get; set; }
        [DataMember]
        public decimal IbovFee { get; set; }
        [DataMember]
        public decimal CblcFee { get; set; }
        [DataMember]
        public decimal Rate { get; set; }
        [DataMember]
        public DateTime HistoryDate { get; set; }
        [DataMember]
        public Int32 PortfolioNumber { get; set; }
        [DataMember]
        public Decimal NetPrice { get; set; }
        [DataMember]
        public DateTime PositionDate { get; set; }
        [DataMember]
        public Int32 Id { get; set; }
        [DataMember]
        public bool LongShort { get; set; }
        [DataMember]
        public string Operation { get; set; }
        [DataMember]
        public Decimal Financial { get; set; }
        [DataMember]
        public char TradeType { get; set; }
    }

    [Serializable]
    [DataContract]
    public class PortofioFilter
    {
        [DataMember]
        public string Client { get; set; }
        [DataMember]
        public string Company { get; set; }
        [DataMember]
        public int SectorId { get; set; }
    }

    [Serializable]
    [DataContract]
    public class PortfolioContact
    {
        [DataMember]
        public CommonType Client { get; set; }
        [DataMember]
        public string ClientEmail { get; set; }
        [DataMember]
        public string ClientPhone { get; set; }
        [DataMember]
        public List<PortfolioContactItem> Contacts { get; set; }
    }

    [Serializable]
    [DataContract]
    public class PortfolioContactItem
    {
        [DataMember]
        public string ContactDate { get; set; }
        [DataMember]
        public string InsertDate { get; set; }
        [DataMember]
        public string Contact { get; set; }
        [DataMember]
        public CommonType Operator { get; set; }
    }

    [Serializable]
    [DataContract]
    public enum PortfolioOperatorFilter
    {
        None, Contact, Position, History
    }


    [Serializable]
    [DataContract]
    public class PortfolioOperator
    {
        [DataMember]
        public Int32 Code { get; set; }
        [DataMember]
        public string Name { get; set; }
    }

    [Serializable]
    [DataContract]
    public class PortfolioClient
    {
        [DataMember]
        public CommonType Client { get; set; }
        [DataMember]
        public List<PortfolioOperator> Operators { get; set; }
        [DataMember]
        public CommonType Calculation { get; set; }
        [DataMember]
        public DateTime StartDate { get; set; }
        [DataMember]
        public decimal InitialValue { get; set; }
        [DataMember]
        public bool SendHistory { get; set; }
        [DataMember]
        public string SendAttachType { get; set; }
    }

    [Serializable]
    [DataContract]
    public class PortfolioClientFilter
    {
        [DataMember]
        public string Client { get; set; }
        [DataMember]
        public string Operator { get; set; }
        [DataMember]
        public string Calculation { get; set; }
        [DataMember]
        public string SendHistory { get; set; }
        [DataMember]
        public string SendAttachType { get; set; }
    }

    [Serializable]
    [DataContract]
    public class PortfolioClientSearchClients
    {
        [DataMember]
        public bool Exist { get; set; }
        [DataMember]
        public List<CommonType> Clients { get; set; }
    }

}
