﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace QX3.Portal.Contracts.DataContracts
{
    [DataContract]
    [Serializable]
    public class BovespaBrokerageReport
    {
        [DataMember]
        public int ClientCode{ get; set; }

        [DataMember]
        public decimal? GrossBrokerage { get; set; }

        [DataMember]
        public decimal? NetBrokerage { get; set; }

        [DataMember]
        public decimal? DiscountValue { get; set; }

    }

    [DataContract]
    [Serializable]
    public class BrokerageReportFilter : BovespaBrokerageReport
    {
        [DataMember]
        public string AssessorFilter { get; set; }

        [DataMember]
        public string ClientFilter { get; set; }

        [DataMember]
        public DateTime? InitialRequest { get; set; }

        [DataMember]
        public DateTime? FinalRequest { get; set; }

        [DataMember]
        public Int32? UserID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class BovespaBrokerageReportFilter : BovespaBrokerageReport
    {
        [DataMember]
        public string AssessorFilter { get; set; }

        [DataMember]
        public string ClientFilter { get; set; }

        [DataMember]
        public DateTime? InitialRequest { get; set; }

        [DataMember]
        public DateTime? FinalRequest { get; set; }

        [DataMember]
        public Int32? UserID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class BMFBrokerageReport
    {
        [DataMember]
        public int ClientCode { get; set; }

        [DataMember]
        public string Broker { get; set; }
        
        [DataMember]
        public string Product { get; set; }

        [DataMember]
        public decimal? GrossBrokerage { get; set; }

        [DataMember]
        public decimal? TransferValue { get; set; }

        [DataMember]
        public decimal? TransferPercentual { get; set; }

        [DataMember]
        public decimal? Revenue { get; set; }

    }

    [DataContract]
    [Serializable]
    public class BMFBrokerageReportFilter : BMFBrokerageReport
    {
        [DataMember]
        public string AssessorFilter { get; set; }

        [DataMember]
        public string ClientFilter { get; set; }

        [DataMember]
        public DateTime? DateRequest { get; set; }

        [DataMember]
        public Int32? UserID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class BrokerageTransferInfo 
    {
        [DataMember]
        public List<BovespaBrokerageReport> BovespaInfo { get; set; }
        
        [DataMember]
        public List<BMFBrokerageReport> BMFInfo { get; set; }
    }
}
