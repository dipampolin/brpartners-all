﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace QX3.Portal.Contracts.DataContracts
{
    [Serializable]
    [DataContract]
    public class ClientOnlineParameters
    {
        public ClientOnlineParameters()
        {
            TipoPessoa = new Dictionary<string, string>();
            TipoPessoa.Add("Pessoa Física", "1");
            TipoPessoa.Add("Pessoa Juridica", "2");
        }

        public Dictionary<string, string> TipoPessoa { get; set; }
    }
}
