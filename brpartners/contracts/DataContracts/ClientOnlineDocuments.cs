﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace QX3.Portal.Contracts.DataContracts
{
    [Serializable]
    [DataContract]
    public class ClientOnlineDocuments
    {
        public ClientOnlineDocuments()
        {
            Documents = new List<ClientDocument>();
        }

        [DataMember]
        public int IdClientOnline { get; set; }

        [DataMember]
        public List<ClientDocument> Documents { get; set; }
    }

    [Serializable]
    [DataContract]
    public class ClientDocument : ClientAssign
    {

        [DataMember]
        public int IdDocClient { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DocumentExpiration { get; set; }

        [DataMember]
        public bool HasPysical { get; set; }
    }
}