﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace QX3.Portal.Contracts.DataContracts
{
	[Serializable][DataContract]
    public class Guarantee
	{
		[DataMember]
		public CommonType Client { get; set; }
		[DataMember]
		public CommonType Assessor { get; set; }
		[DataMember]
		public List<GuaranteeItem> Items { get; set; }
		[DataMember]
		public GuaranteeBalance Balance { get; set; }
	}

	[Serializable][DataContract]
    public class GuaranteeItem
	{
		[DataMember]
		public int ID { get; set; }
		[DataMember]
        public bool IsWithdrawal { get; set; }
        [DataMember]
        public string Guarantee { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public bool IsBovespa { get; set; }
		[DataMember]
		public DateTime UpdateDate { get; set; }
		[DataMember]
		public Int64 Quantity { get; set; }
        [DataMember]
        public decimal UnitPrice { get; set; }
        [DataMember]
        public decimal Total { get; set; }
        [DataMember]
        public decimal Discount { get; set; }
        [DataMember]
        public decimal RequestedValue { get; set; }
        [DataMember]
        public CommonType Status { get; set; }
        [DataMember]
        public bool IsQuantity { get; set; }
	}

    [Serializable]
    [DataContract]
    public class GuaranteeBalance
    {
        [DataMember]
        public decimal Total { get; set; }
        [DataMember]
        public decimal Used { get; set; }
        [DataMember]
        public decimal Free { get; set; }
        [DataMember]
        public decimal AfterRealize { get; set; }
    }

    [Serializable]
    [DataContract]
    public class GuaranteeFilter
    {
        [DataMember]
        public string Assessors { get; set; }
        [DataMember]
        public string Clients { get; set; }
        [DataMember]
        public int StatusID { get; set; }
        [DataMember]
        public string StartDate { get; set; }
        [DataMember]
        public string EndDate { get; set; }
        [DataMember]
        public bool Bovespa { get; set; }
    }

	#region Garantias do Cliente

	[Serializable]
	[DataContract]
	public class GuaranteeClient
	{
		public GuaranteeClient()
		{
			Activits = new List<GuaranteeActivit>();
			Stocks = new List<GuaranteeStock>();
		}
		
		[DataMember]
		public int ClientCode { get; set; }
		[DataMember]
		public string ClientName { get; set; }
		[DataMember]
		public int AssessorCode { get; set; }
		[DataMember]
		public string AssessorName { get; set; }
		[DataMember]
		public int ActivitType { get; set; }
		[DataMember]
		public string ActivitTypeDesc
		{
			get
			{
				switch (ActivitType)
				{
					case 1:
						return "Bovespa";
					case 2:
						return "BM&F";
					default:
						return "-";
				}
			}
			set { }
		}
		[DataMember]
		public List<GuaranteeActivit> Activits { get; set; }
		[DataMember]
		public List<GuaranteeStock> Stocks { get; set; }
		[DataMember]
		public decimal TotalValue { get; set; }
		[DataMember]
		public decimal MarginCall { get; set; }
		[DataMember]
		public decimal BalanceValue { get; set; }
	}

	[Serializable]
	[DataContract]
	public class GuaranteeActivit
	{
		[DataMember]
		public string Type { get; set; }
		[DataMember]
		public DateTime DueDate { get; set; }
		[DataMember]
		public decimal TotalValue { get; set; }
	}

	[Serializable]
	[DataContract]
	public class GuaranteeStock
	{
		[DataMember]
		public string Stock { get; set; }
		[DataMember]
		public int Quantity { get; set; }
		[DataMember]
		public decimal Value { get; set; }
	}

	[Serializable]
	[DataContract]
	public class GuaranteeClientFilter : GuaranteeClient
	{
		[DataMember]
		public string AssessorFilterText { get; set; }
		[DataMember]
		public string ClientFilterText { get; set; }
		[DataMember]
		public int? Position { get; set; }
	}

	#endregion
}
