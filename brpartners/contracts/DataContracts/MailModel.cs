﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace QX3.Portal.Contracts.DataContracts
{
    [Serializable]
    [DataContract]
    public class MailModel
    {
        [DataMember]
        public int FuncionalityId { get; set; }

        [DataMember]
        public string ReplyName { get; set; }

        [DataMember]
        public string ReplyMail { get; set; }

        [DataMember]
        public string Subject { get; set; }

        [DataMember]
        public string Content { get; set; }
    }

    [Serializable]
    [DataContract]
    public class TagModel 
    {
        [DataMember]
        public int TagId { get; set; }

        [DataMember]
        public string TagName { get; set; }

        [DataMember]
        public string TagDescription { get; set; }
    }
}
