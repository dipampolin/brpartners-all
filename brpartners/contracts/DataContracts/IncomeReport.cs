﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace QX3.Portal.Contracts.DataContracts
{
    [Serializable]
    [DataContract]
    public class IncomeReportFilter 
    {
        [DataMember]
        public int ClientCode { get; set; }

        [DataMember]
        public int Year { get; set; }

        [DataMember]
        public int? UserId { get; set; }
    }

    [Serializable]
    [DataContract]
    public class IncomeReport
    {
        [DataMember]
        public decimal? Balance { get; set; }

        [DataMember]
        public CPClientInformation Client { get; set; }

		[DataMember]
		public int Year { get; set; }

        [DataMember]
        public DateTime? PositionDate { get; set; }

        [DataMember]
        public List<StockMarket> StockMarketList { get; set; }

        [DataMember]
        public List<StockRent> StockRentList { get; set; }

        [DataMember]
        public List<BMFOptions> OptionsList { get; set; }

        [DataMember]
        public List<BMFOptionsFuture> BMFOptionsFutureList { get; set; }

        [DataMember]
        public List<BMFSwap> BMFSwapList { get; set; }

        [DataMember]
        public List<BMFGold> BMFGoldList { get; set; }

        [DataMember]
        public List<IncomeProceeds> IncomeProceedsList { get; set; }

        [DataMember]
        public List<IncomeTerms> IncomeTermsList { get; set; }

        [DataMember]
        public List<IRPeriod> IRRFDayTradeList { get; set; }

        [DataMember]
        public List<IRPeriod> IRRFOperationsList { get; set; }

        public IncomeReport() 
        {
            this.StockMarketList = new List<StockMarket>();
            this.StockRentList = new List<StockRent>();
            this.OptionsList = new List<BMFOptions>();
            this.BMFOptionsFutureList = new List<BMFOptionsFuture>();
            this.BMFSwapList = new List<BMFSwap>();
            this.BMFGoldList = new List<BMFGold>();
            this.IncomeProceedsList = new List<IncomeProceeds>();
            this.IncomeTermsList = new List<IncomeTerms>();
            this.IRRFDayTradeList = new List<IRPeriod>();
            this.IRRFOperationsList = new List<IRPeriod>();
        }
    }

    [Serializable]
    [DataContract]
    public class StockMarket 
    {
        [DataMember]
        public string StockCode { get; set; }

        [DataMember]
        public int? AvailableQuantity { get; set; }

        [DataMember]
        public decimal? Price { get; set; }

        [DataMember]
        public decimal? TotalValue { get; set; }
    }

    [Serializable]
    [DataContract]
    public class StockRent
    {
        [DataMember]
        public string StockCode { get; set; }

        [DataMember]
        public string Type { get; set; }

        [DataMember]
        public DateTime? OpenDate { get; set; }

        [DataMember]
        public DateTime? PaymentDate { get; set; }

        [DataMember]
        public int? Quantity { get; set; }

        [DataMember]
        public decimal? Tax { get; set; }

        [DataMember]
        public decimal? Quote { get; set; }

        [DataMember]
        public decimal? TotalValue { get; set; }
    }

    [Serializable]
    [DataContract]
    public class BMFOptions 
    {
        [DataMember]
        public string StockCode { get; set; }

        [DataMember]
        public string OperationType { get; set; }

        [DataMember]
        public string Type { get; set; }

        [DataMember]
        public DateTime? PaymentDate { get; set; }

        [DataMember]
        public int? AvailableQtty { get; set; }

        [DataMember]
        public decimal? Price { get; set; }

        [DataMember]
        public decimal? Volume { get; set; }

        [DataMember]
        public decimal? VistaVolume { get; set; }
    }

    [Serializable]
    [DataContract]
    public class BMFOptionsFuture
    {
        [DataMember]
        public string StockCode { get; set; }

        [DataMember]
        public string Type { get; set; }

        [DataMember]
        public DateTime? PaymentDate { get; set; }

        [DataMember]
        public int? AvailableQuantity { get; set; }

        [DataMember]
        public decimal? Price { get; set; }

        [DataMember]
        public string CV { get; set; }

        [DataMember]
        public decimal? Position { get; set; }
    }

    [Serializable]
    [DataContract]
    public class BMFGold
    {
        [DataMember]
        public decimal? Pounds { get; set; }

        [DataMember]
        public int? DefaultQuantity { get; set; }

        [DataMember]
        public decimal? Quote { get; set; }
        
        [DataMember]
        public decimal? TotalValue { get; set; }
    }

    [Serializable]
    [DataContract]
    public class BMFSwap
    {
        [DataMember]
        public string Contract { get; set; }

        [DataMember]
        public int? Number { get; set; }

        [DataMember]
        public int? Base { get; set; }

        [DataMember]
        public DateTime? PaymentDate { get; set; }

        [DataMember]
        public DateTime? RegisterDate { get; set; }

        [DataMember]
        public decimal? NetTotal { get; set; }
    }

    [Serializable]
    [DataContract]
    public class IncomeProceeds
    {
        [DataMember]
        public string StockCode { get; set; }

        [DataMember]
        public string Type { get; set; }

        [DataMember]
        public int? AvailableQuantity { get; set; }

        [DataMember]
        public decimal? GrossValue { get; set; }

        [DataMember]
        public decimal? IRPerc { get; set; }

        [DataMember]
        public decimal? IRValue { get; set; }

        [DataMember]
        public decimal? NetValue { get; set; }

        [DataMember]
        public DateTime? PaymentDate { get; set; }
    }

    [Serializable]
    [DataContract]
    public class IncomeTerms
    {
        [DataMember]
        public string StockCode { get; set; }

        [DataMember]
        public int? AvailableQuantity { get; set; }

        [DataMember]
        public decimal? Price { get; set; }
                
        [DataMember]
        public DateTime? PaymentDate { get; set; }

        [DataMember]
        public decimal? Total { get; set; }
    }

    [Serializable]
    [DataContract]
    public class IR
    {
        [DataMember]
        public string Month { get; set; }
        
        [DataMember]
        public decimal? GrossValue { get; set; }

        [DataMember]
        public decimal? Retained { get; set; }
    }

    [Serializable]
    [DataContract]
    public class IRPeriod: IR
    {
        [DataMember]
        public DateTime? InitDate { get; set; }

        [DataMember]
        public DateTime? FinalDate { get; set; }
    }
}
