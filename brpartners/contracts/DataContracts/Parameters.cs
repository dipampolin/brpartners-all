﻿using System;
using System.Runtime.Serialization;

namespace QX3.Portal.Contracts.DataContracts
{
    [Serializable]
    [DataContract]
    public class Parameters
    {
        [DataMember]
        public string Descricao { get; set; }

        [DataMember]
        public string Valor { get; set; }
    }
}
