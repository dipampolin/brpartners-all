﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace QX3.Portal.Contracts.DataContracts
{
    [Serializable]
    [DataContract]
    public class UserInfo
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int? IdCliente { get; set; }

        [DataMember]
        public long? CpfCnpj { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Email { get; set; }
    }
}
