﻿using System.Runtime.Serialization;
using System;

namespace QX3.Portal.Contracts.DataContracts
{
	[Serializable][DataContract]
	public class Address
	{
		[DataMember]
		public string AddressLine1 { get; set; }
		[DataMember]
		public string Number { get; set; }
		[DataMember]
		public string AddressLine2 { get; set; }
		[DataMember]
		public string Quarter { get; set; }
        [DataMember]
        public string City { get; set; }
        [DataMember]
        public string StateCountry { get; set; }
        [DataMember]
        public string PostalCode { get; set; }
        [DataMember]
        public string Phone { get; set; }
        [DataMember]
        public string Fax { get; set; }
        [DataMember]
        public string Cell { get; set; }
        [DataMember]
        public string Neighborhood { get; set; }
    }

	[Serializable][DataContract]
	public class BankAccount
	{
        [DataMember]
        public string BankCode { get; set; }
        [DataMember]
        public string BankName { get; set; }
        [DataMember]
        public string AgencyCode { get; set; }
        [DataMember]
        public string AgencyDigit { get; set; }
        [DataMember]
        public string Account { get; set; }
        [DataMember]
        public string AccountDigit { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public bool IsPrincipal { get; set; }
        [DataMember]
        public bool IsInactive { get; set; }
        [DataMember]
        public bool IsJointAccount { get; set; }
        [DataMember]
        public string JointAccountName { get; set; }
	}

	[Serializable][DataContract]
	public class OrdersIssuer
	{
		[DataMember]
		public string Name { get; set; }
        [DataMember]
		public string Market { get; set; }
         [DataMember]
		public string Type { get; set; }
	}
}
