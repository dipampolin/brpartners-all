﻿using System.Runtime.Serialization;
using System;
using System.Collections.Generic;

namespace QX3.Portal.Contracts.DataContracts
{
	[Serializable][DataContract]
	public class Customer
	{
		 [DataMember]
		 public int Code { get; set; }
		 [DataMember]
		public string Name { get; set; }
	}

	[Serializable][DataContract]
	public class NonResidentCustomer : Customer
	{
		[DataMember]
		public int BrokerCode { get; set; }
		[DataMember]
		public Decimal TransferBrokerage { get; set; }
		[DataMember]
		public string Address { get; set; }
		[DataMember]
		public string Email { get; set; }
		[DataMember]
		public int PhoneDDDNumber { get; set; }
		[DataMember]
		public Int64 PhoneNumber { get; set; }
		[DataMember]
		public int FaxDDDNumber { get; set; }
		[DataMember]
		public Int64 FaxNumber { get; set; }
        [DataMember]
        public bool HasRegistration { get; set; }
	}
}
