﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace QX3.Portal.Contracts.DataContracts
{
	[Serializable]
	[DataContract]
	public class BBRent
	{
		public BBRent()
		{
			Settlements = new List<BBSettlement>();
			Transfers = new List<BBTransfer>();
			Proceeds = new List<BBProceeds>();
		}

		[DataMember]
		public List<BBSettlement> Settlements { get; set; }

		[DataMember]
		public List<BBTransfer> Transfers { get; set; }

		[DataMember]
		public List<BBProceeds> Proceeds { get; set; }
	}

    [Serializable]
    [DataContract]
	public class BBSettlement
    {
        [DataMember]
        public DateTime Date { get; set; }

        [DataMember]
        public int ClientCode { get; set; }

        [DataMember]
		public int? ByAccount { get; set; }

        [DataMember]
		public long Contract { get; set; }

        [DataMember]
        public string Stock { get; set; }

        [DataMember]
        public int Quantity { get; set; }

        [DataMember]
		public decimal Gross { get; set; }

        [DataMember]
        public decimal IR { get; set; }

        [DataMember]
        public decimal NetValue { get; set; }

		[DataMember]
		public decimal TotalCommission { get; set; }

		[DataMember]
		public decimal NetCommission { get; set; }

		[DataMember]
		public decimal Transfer { get; set; }
    }

	[Serializable]
	[DataContract]
	public class BBTransfer : BBSettlement
	{
		[DataMember]
		public decimal Transfers { get; set; }
	}

	[Serializable]
	[DataContract]
	public class BBProceeds
	{
		[DataMember]
		public DateTime Date { get; set; }

		[DataMember]
		public int ClientCode { get; set; }

		[DataMember]
		public int? ByAccount { get; set; }

		[DataMember]
		public string Stock { get; set; }

		[DataMember]
		public int Quantity { get; set; }

		[DataMember]
		public decimal NetValue { get; set; }

		[DataMember]
		public string Type { get; set; }
	}
}
