﻿using System.Runtime.Serialization;
using System;

namespace QX3.Portal.Contracts.DataContracts
{
	[Serializable][DataContract]
	public class WcfResponse<ResultType>
	{
		[DataMember]
		public ResultType Result { get; set; }
		[DataMember]
		public string Message { get; set; }

		public WcfResponse()
		{

		}
	}

	[Serializable][DataContract]
	public class WcfResponse<ResultType, DataType> : WcfResponse<ResultType>
	{
		[DataMember]
		public DataType Data { get; set; }
	}

    [Serializable]
    [DataContract]
    public class CommonType 
    {
        [DataMember]
        public string Value { get; set; }
        [DataMember]
        public string Description { get; set; }
    }

    [Serializable]
    [DataContract]
    public class WarningList 
    { 
        [DataMember] //NOME
        public string AreaName { get; set; }

        [DataMember] //Apelido
        public string PageName { get; set; }

        [DataMember] //LINK
        public string Link { get; set; }

        [DataMember] //STATUSID
        public string IdStatus { get; set; }

        [DataMember] //NOME_STATUS
        public string Action { get; set; }

        [DataMember] //CONTADOR
        public int Count { get; set; }
    }

    [Serializable]
    [DataContract]
    public class RelatedPersons
    {
        [DataMember] 
        public decimal QdtTotalTrading { get; set; }

        [DataMember] 
        public decimal QtdTradingRelatedPersons { get; set; }

        [DataMember]
        public decimal QtdTradingRelatedPersonsECP { get; set; }

        [DataMember] 
        public decimal QtdTradingRelatedPersonsCCP { get; set; }

        [DataMember]
        public decimal QtdTradingCP { get; set; }
    }

}
