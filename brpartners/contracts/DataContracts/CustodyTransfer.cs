﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace QX3.Portal.Contracts.DataContracts
{
    [Serializable]
    [DataContract]
    public class CustodyTransfer
    {
        [DataMember]
        public int RequestId { get; set; }

        [DataMember]
        public int ClientBrokerCode { get; set; }

        [DataMember]
        public int ClientCode { get; set; }

        [DataMember]
        public int DV { get; set; }

        [DataMember]
        public string ClientName { get; set; }

        [DataMember]
        public string Assessor { get; set; }

        [DataMember]
        public string OrigBroker { get; set; }

        [DataMember]
        public int OrigBrokerCode { get; set; }

        [DataMember]
        public int OrigPortfolio { get; set; }
        
        [DataMember]
        public string DestinyBroker { get; set; }

        [DataMember]
        public int DestinyBrokerCode { get; set; }

        [DataMember]
        public int DestinyClientCode { get; set; }

        [DataMember]
        public int DestinyPortfolio { get; set; }

        [DataMember]
        public string StockCode { get; set; }

        [DataMember]
        public decimal Qtty { get; set; }

        [DataMember] // Situação
        public string Status { get; set; }

        [DataMember]
        public string DescStatus { get; set; }

        [DataMember]
        public DateTime RequestDate { get; set; }

        [DataMember]
        public string Observations { get; set; }

        [DataMember]
        public int AssessorCode { get; set; }
    }

    [Serializable]
    [DataContract]
    public class CustodyTransferFilter : CustodyTransfer
    {
        [DataMember]
        public string ClientFilter { get; set; }

        [DataMember]
        public string CustodyTransferType{ get; set; }

        [DataMember]
        public DateTime? InitialRequest { get; set; }

        [DataMember]
        public DateTime? FinalRequest { get; set; }

    }

    [Serializable]
    [DataContract]
    public class CustodyTransferDetails
    {
        [DataMember]
        public int ClientCode { get; set; }

        [DataMember]
        public string StockCode { get; set; }

        [DataMember]
        public decimal StockQtty { get; set; }

        [DataMember]
        public decimal PortfolioCode { get; set; }

        [DataMember]
        public decimal StockValue { get; set; }
    }

    public enum CustodyTransferStatus 
    { 
        ToExecute = 1, Executed, External
    }

    public enum CustodyTransferType 
    {
        All = 1, Alienating, Transferee
    }

	[Serializable]
	[DataContract]
	public class CustodyTransferReport
	{
		public CustodyTransferReport()
		{
			BrokerInTop = new List<CustodyTransferTop>();
			BrokerOutTop = new List<CustodyTransferTop>();
			AssessorInTop = new List<CustodyTransferTop>();
			AssessorOutTop = new List<CustodyTransferTop>();
			ChartOrigin = new List<CustodyTransferChart>();
			ChartDestination = new List<CustodyTransferChart>();
		}

		[DataMember]
		public DateTime FromDate { get; set; }
		[DataMember]
		public DateTime ToDate { get; set; }

		[DataMember]
		public List<CustodyTransferTop> BrokerInTop { get; set; }
		[DataMember]
		public List<CustodyTransferTop> BrokerOutTop { get; set; }
		[DataMember]
		public List<CustodyTransferTop> AssessorInTop { get; set; }
		[DataMember]
		public List<CustodyTransferTop> AssessorOutTop { get; set; }

		[DataMember]
		public List<CustodyTransferChart> ChartOrigin { get; set; }
		[DataMember]
		public List<CustodyTransferChart> ChartDestination { get; set; }
	}

	[Serializable]
	[DataContract]
	public class CustodyTransferTop
	{
		[DataMember]
		public string Title { get; set; }
		[DataMember]
		public decimal Amount { get; set; }
		[DataMember]
		public decimal Percentage { get; set; }
	}

	[Serializable]
	[DataContract]
	public class CustodyTransferChart
	{
		[DataMember]
		public DateTime Date { get; set; }
		[DataMember]
		public decimal Amount { get; set; }
	}
}
