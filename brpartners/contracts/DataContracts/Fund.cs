﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace QX3.Portal.Contracts.DataContracts
{
    [Serializable]
    [DataContract]
    public class FinancialPositionDataItem
    {
        [DataMember]
        public string AccountType { get; set; }
        [DataMember]
        public decimal AskExecutedValue { get; set; }
        [DataMember]
        public decimal AskNotExecutedValue { get; set; }
        [DataMember]
        public decimal AskPendingValue { get; set; }
        [DataMember]
        public decimal AvailableBalance { get; set; }
        [DataMember]
        public decimal BidExecutedValue { get; set; }
        [DataMember]
        public decimal BidNotExecutedValue { get; set; }
        [DataMember]
        public decimal BidPendingValue { get; set; }
        [DataMember]
        public string ClientAccount { get; set; }
        [DataMember]
        public decimal CreditLimit { get; set; }
        [DataMember]
        public decimal CurrentAccountInitialBalance { get; set; }
        [DataMember]
        public decimal CurrentDateMovementBalance { get; set; }
        [DataMember]
        public decimal CurrentLimit { get; set; }
        [DataMember]
        public decimal DailyLimit { get; set; }
        [DataMember]
        public decimal Options { get; set; }
        [DataMember]
        public decimal ProjBalanceD1 { get; set; }
        [DataMember]
        public decimal ProjBalanceD2 { get; set; }
        [DataMember]
        public decimal ProjBalanceD3 { get; set; }
        [DataMember]
        public DateTime ProjDateD1 { get; set; }
        [DataMember]
        public DateTime ProjDateD2 { get; set; }
        [DataMember]
        public DateTime ProjDateD3 { get; set; }
        [DataMember]
        public decimal SettledBalanceToDate { get; set; }
        [DataMember]
        public decimal Stocks { get; set; }
        [DataMember]
        public decimal AvailableForWithDrawl { get; set; }
        [DataMember]
        public decimal AvailableForOptions { get; set; }
        [DataMember]
        public decimal AvailableForStocks { get; set; }
    }

    [Serializable]
    [DataContract]
    public class FundClient : Client
    {
        [DataMember]
        public DateTime? DateBalance { get; set; }

        /*[DataMember]
        public decimal? AccountBalance { get; set; }*/

        [DataMember]
        public decimal? AvailableBalance { get; set; }

        [DataMember]
        public decimal? BlockedBalance { get; set; }

        [DataMember]
        public decimal? PositionBalance { get; set; }

        [DataMember]
        public string ClientEmail { get; set; }

        [DataMember]
        public string FormattedClientCode { get; set; }
    }
}
