﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace QX3.Portal.Contracts.DataContracts
{
	[Serializable][DataContract]
	public class Stock
	{
		[DataMember]
		public string Symbol { get; set; }
		[DataMember]
		public string CompanyName { get; set; }
	}

    public class StockExchanges
    {
        [DataMember]
        public bool Bmf { get; set; }
        [DataMember]
        public bool Bovespa { get; set; }
    }
}
