﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace QX3.Portal.Contracts.DataContracts.SuitabilityAdmin
{
    [DataContract]
    public class ColunaMatriz
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int Ordem { get; set; }
        [DataMember]
        public string Descricao { get; set; }
        [DataMember]
        public bool Ativa { get; set; }
    }
}
