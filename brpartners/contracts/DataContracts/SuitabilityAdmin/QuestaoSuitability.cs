﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace QX3.Portal.Contracts.DataContracts.SuitabilityAdmin
{
    [DataContract]
    public class QuestaoSuitability
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public Guid UID { get; set; }

        [DataMember]
        public int Ordem { get; set; }


        [DataMember]
        public int? NumeroSubQuestao { get; set; }

        [DataMember]
        public string Enunciado { get; set; }
        [DataMember]
        public bool QuestaoAtiva { get; set; }
        [DataMember]
        public int Peso { get; set; }
        [DataMember]
        public string Tipo { get; set; }
        [DataMember]
        public decimal? PercentualMinimo { get; set; }
        [DataMember]
        public string DescricaoPercentual { get; set; }

        [DataMember]
        public bool Oculta { get; set; }


        [DataMember]
        public List<Alternativa> Alternativas { get; set; }
        [DataMember]
        public List<ColunaMatriz> Colunas { get; set; }
    }
}
