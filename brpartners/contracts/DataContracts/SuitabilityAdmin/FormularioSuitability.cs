﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace QX3.Portal.Contracts.DataContracts.SuitabilityAdmin
{
    [DataContract]
    public class FormularioSuitability
    {

        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Descricao { get; set; }
        [DataMember]
        public string Tipo { get; set; }
        [DataMember]
        public string Status { get; set; }
        [DataMember]
        public string DescricaoStatus { get; set; }

    }
}
