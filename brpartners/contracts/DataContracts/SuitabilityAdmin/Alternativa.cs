﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace QX3.Portal.Contracts.DataContracts.SuitabilityAdmin
{
    [DataContract]
    public class Alternativa
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Texto { get; set; }
        [DataMember]
        public bool Ativa { get; set; }
        [DataMember]
        public decimal Pontos { get; set; }
        [DataMember]
        public int Ordem { get; set; }
        [DataMember]
        public int? IdProximaQuestao { get; set; }
        [DataMember]
        public Guid? UIDProximaQuestao { get; set; }

    }
}
