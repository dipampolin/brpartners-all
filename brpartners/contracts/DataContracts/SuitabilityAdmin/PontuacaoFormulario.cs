﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace QX3.Portal.Contracts.DataContracts.SuitabilityAdmin
{
    [DataContract]
    public class PontuacaoFormulario
    {
        [DataMember]
        public int IdPerfil { get; set; }
        [DataMember]
        public int? Id { get; set; }
        [DataMember]
        public decimal? Minimo { get; set; }
        [DataMember]
        public decimal? Maximo { get; set; }

    }
}
