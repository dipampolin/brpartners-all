﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace QX3.Portal.Contracts.DataContracts.SuitabilityAdmin
{
    [DataContract]
    public class PerfilSuitability
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Nome { get; set; }
    }
}
