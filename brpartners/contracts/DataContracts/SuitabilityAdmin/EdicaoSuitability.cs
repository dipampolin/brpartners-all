﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace QX3.Portal.Contracts.DataContracts.SuitabilityAdmin
{
    [DataContract]
    public class EdicaoSuitability
    {
        [DataMember]public FormularioSuitability Formulario { get; set; }
        [DataMember]
        public List<QuestaoSuitability> Questoes { get; set; }


        [DataMember]
        public List<PontuacaoFormulario> Pontuacao { get; set; }

        [DataMember]
        public List<PerfilSuitability> Perfis { get; set; }

        [DataMember]
        public int QuantidadePreenchimentos { get; set; }
    }
}
