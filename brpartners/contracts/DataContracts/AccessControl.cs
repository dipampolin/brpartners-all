﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;


namespace QX3.Portal.Contracts.DataContracts
{
	[Serializable][DataContract]
	public class ACElement
	{
		[DataMember]
		public int ID { get; set; }
		[DataMember]
		public string Name { get; set; }
		[DataMember]
		public string Alias { get; set; }
		[DataMember]
		public int ParentID { get; set; }
		[DataMember]
		public bool Active { get; set; }
		[DataMember]
		public List<ACElement> Elements { get; set; }
		[DataMember]
		public bool IsPage { get; set; }
        [DataMember]
        public bool Visible { get; set; }
	}

	[Serializable][DataContract]
	public class ACProfile
	{
		[DataMember]
		public int ID { get; set; }
		[DataMember]
		public string Name { get; set; }
		[DataMember]
		public List<ACElement> Permissions { get; set; }
        [DataMember]
        public List<ACUser> Users { get; set; }
        [DataMember]
        public int UsersCount { get; set; }
	}

    [Serializable][DataContract]
    public class ACGroupOfAssessors
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Assessors { get; set; }
        [DataMember]
        public List<ACUser> Users { get; set; }
        [DataMember]
        public List<Client> ClientsIncluded { get; set; }
        [DataMember]
        public List<Client> ClientsExcluded { get; set; }
    }

    [Serializable]
    [DataContract]
    public class Client 
    {
        [DataMember]
        public int ClientCode { get; set; }
        [DataMember]
        public string ClientName { get; set; }

        [DataMember]
        public int AssessorCode { get; set; }

        [DataMember]
        public string AssessorName { get; set; }

        [DataMember]
        public long CPFCGC { get; set; }

        [DataMember]
        public bool? BovespaActive { get; set; }

        [DataMember]
        public bool? BMFActive { get; set; }

        [DataMember]
        public DateTime? AccessionDate { get; set; }
    }

    [Serializable]
    [DataContract]
    public class ClientFilter : Client 
    {
        [DataMember]
        public DateTime? InitAcessionDate { get; set; }

        [DataMember]
        public DateTime? FinalAcessionDate { get; set; }

        [DataMember]
        public string ClientFilterText { get; set; }

        [DataMember]
        public string AssessorFilterText { get; set; }
    }

    [Serializable]
    [DataContract]
    public class FuncionalityClient : Client
    {
        [DataMember]
        public List<Funcionality> Funcionalities { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ClientEmail 
    {
        [DataMember]
        public int EmailId { get; set; }

        [DataMember]
        public string Email { get; set; }
    }

    [DataContract]
    [Serializable]
    public class Funcionality
    {
        [DataMember]
        public int FuncClientMailId { get; set; }

        [DataMember]
        public int FuncId { get; set; }

        [DataMember]
        public string FuncionalityName { get; set; }

        [DataMember]
        public List<ClientEmail> Emails { get; set; }

        [DataMember]
        public List<ClientEmail> CopyTo { get; set; }

        [DataMember]
        public string SendFormat { get; set; }

        [DataMember]
        public string SendFormatDescription
        {
            get
            {
                switch (this.SendFormat)
                {
                    case "H": return "HTML";
                    case "P": return "PDF";
                    default: return "";
                }
            }
            set {  }
        }
    }

    [DataContract]
    [Serializable]
    public class FuncionalityClientFilter : FilterOptions
    {
        [DataMember]
        public string ClientCodes { get; set; }

        [DataMember]
        public string SendFormat { get; set; }

        [DataMember]
        public string Assessors { get; set; }

        [DataMember]
        public int UserID { get; set; }
    }

	[Serializable][DataContract]
	public class ACUser
	{
		[DataMember]
		public int ID { get; set; }
        [DataMember]
        public string Login { get; set; }
        [DataMember]
        public string Password { get; set; }
		[DataMember]
		public string Name { get; set; }
		[DataMember]
		public string Email { get; set; }
		[DataMember]
		public string ChaveAD { get; set; }
		[DataMember]
        public int AssessorID { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public string PersonType { get; set; }
        [DataMember]
		public ACProfile Profile { get; set; }
        [DataMember]
        public int ActiveStatus { get; set; }
		[DataMember]
		public List<ACGroupOfAssessors> AssessorsGroup { get; set; }
		[DataMember]
		public bool Blocked { get; set; }
        [DataMember]
        public int CdCliente { get; set; }
        [DataMember]
        public decimal? CdCpfCnpj { get; set; }
        [DataMember]
        public int InvestorType { get; set; }
        [DataMember]
        public int SuitabilityStatus { get; set; }
        [DataMember]
        public DateTime? SuitabilityDate { get; set; }
        [DataMember]
        public string Telephone { get; set; }


        [DataMember]
        public int? ClientId { get; set; }
        [DataMember]
        public long? CpfCnpj { get; set; }

        [DataMember]
        public int InvestorKind { get; set; }

        [DataMember]
        public bool SendSuitabilityEmail { get; set; }

    }

    [DataContract]
	[Serializable]
	public class OverdueAndDue : Client
	{
		[DataMember]
		public int Situation { get; set; }
		[DataMember][System.Diagnostics.DebuggerHidden]
		public string SituationDesc
		{
			get
			{
				switch (Situation)
				{
					case 1:
						return "A Vencer";
					case 2:
						return "Vencido";
					default:
						return "-";
				}
			}
			set { }
		}
		[DataMember]
		public string RegisterType { get; set; }
		[DataMember]
		public DateTime RegisterDate { get; set; }
		[DataMember]
		public string SinacorCode { get; set; }
		[DataMember]
		public string SinacorDesc
		{
			get
			{
				switch (SinacorCode)
				{
					case "AT":
						return "Ativo";
					case "EN":
						return "Inativo";
					case "BL":
						return "Bloqueado";
					default:
						return "-";
				}
			}
			set { }
		}
	}

	[DataContract]
	[Serializable]
	public class OverdueAndDueFilter : OverdueAndDue
	{
		[DataMember]
		public string AssessorFilterText { get; set; }
		[DataMember]
		public DateTime DueDateStart { get; set; }
		[DataMember]
		public DateTime DueDateEnd { get; set; }
		[DataMember]
		public Boolean SinacorActive { get; set; }
		[DataMember]
		public Boolean SinacorInactive { get; set; }
		[DataMember]
		public Boolean SinacorBlocked { get; set; }
	}
}
    