﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace QX3.Portal.Contracts.DataContracts
{
    [Serializable]
    [DataContract]
    public class Document
    {
        public Document()
        {
            Products = new List<DocumentProduct>();
        }

        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string NameFile { get; set; }

        [DataMember]
        public string Instruction { get; set; }

        [DataMember]
        public bool Required { get; set; }

        [DataMember]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime ExpirationDate { get; set; }

        [DataMember]
        public int IdValidation { get; set; }

        [DataMember]
        public bool PhysicalPerson { get; set; }

        [DataMember]
        public bool LegalPerson { get; set; }

        [DataMember]
        public bool Funds { get; set; }

        [DataMember]
        public byte[] Data { get; set; }

        [DataMember]
        public string DescriptionFile { get; set; }

        [DataMember]
        public string ContentType { get; set; }

        [DataMember]
        public List<DocumentProduct> Products { get; set; }

        [DataMember]
        public int Status { get; set; }
    }

    [Serializable]
    [DataContract]
    public class DocumentProduct
    {
        [DataMember]
        public int IdProduct { get; set; }
    }

    [Serializable]
    [DataContract]
    public class Documents
    {
        public Documents()
        {
            Docs = new List<Document>();
        }

        [DataMember]
        public List<Document> Docs { get; set; }
    }
}
