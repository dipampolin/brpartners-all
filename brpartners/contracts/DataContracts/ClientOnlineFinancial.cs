﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace QX3.Portal.Contracts.DataContracts
{
    [Serializable]
    [DataContract]
    public class ClientOnlineFinancial
    {
        public ClientOnlineFinancial()
        {
            Accounts = new List<FinancialAccount>();
            Products = new List<FinancialProduct>();
        }

        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int IdClientOnline { get; set; }

        [DataMember]
        public string MonthlyProfit { get; set; }

        [DataMember]
        public string FamilyMonthlyProfit { get; set; }

        [DataMember]
        public string AnotherProfit { get; set; }

        [DataMember]
        public string HousePatrimony{ get; set; }

        [DataMember]
        public string CarPatrimony { get; set; }

        [DataMember]
        public string ApplicationPatrimony { get; set; }

        [DataMember]
        public bool ApplicationOrigin { get; set; }

        [DataMember]
        public bool Indemnity { get; set; }

        [DataMember]
        public bool Award { get; set; }

        [DataMember]
        public bool Termination { get; set; }

        [DataMember]
        public bool Salary { get; set; }

        [DataMember]
        public bool Sell { get; set; }

        [DataMember]
        public bool Jewels { get; set; }

        [DataMember]
        public bool Rental { get; set; }

        [DataMember]
        public bool Donation { get; set; }

        [DataMember]
        public bool Heritage { get; set; }

        [DataMember]
        public bool Car { get; set; }

        [DataMember]
        public bool Insurance { get; set; }

        [DataMember]
        public bool ProductAgricultural { get; set; }

        [DataMember]
        public bool House { get; set; }

        [DataMember]
        public bool Another { get; set; }

        [DataMember]
        public string AnotherDescription { get; set; }

        [DataMember]
        public int IdFiscalNature { get; set; }

        [DataMember]
        public List<FinancialAccount> Accounts { get; set; }

        [DataMember]
        public List<FinancialProduct> Products { get; set; }
    }

    [Serializable]
    [DataContract]
    public class FinancialAccount
    {
        [DataMember]
        public int IdClientOnline { get; set; }

        [DataMember]
        public int IdFinality { get; set; }

        [DataMember]
        public string DescriptionFinality { get; set; }

        [DataMember]
        public int IdUtility { get; set; }

        [DataMember]
        public string DescriptionUtility { get; set; }

        [DataMember]
        public int IdAccountType { get; set; }

        [DataMember]
        public string DescriptionAccountType { get; set; }

        [DataMember]
        public int IdAccountTypeBank { get; set; }

        [DataMember]
        public string DescriptionAccountTypeBank { get; set; }

        [DataMember]
        public int IdBank { get; set; }

        [DataMember]
        public string DescriptionBank { get; set; }

        [DataMember]
        public string Agency { get; set; }

        [DataMember]
        public string NumberAccount { get; set; }
    }

    [Serializable]
    [DataContract]
    public class FinancialProduct
    {
        [DataMember]
        public int IdClientOnline { get; set; }

        [DataMember]
        public int IdProduct { get; set; }

        [DataMember]
        public string DescriptionProduct { get; set; }
    }
}