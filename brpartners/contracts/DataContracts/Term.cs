﻿using System;
using System.Runtime.Serialization;

namespace QX3.Portal.Contracts.DataContracts
{
    [Serializable]
    [DataContract]
    public class TermItem
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public CommonType Client { get; set; }
        [DataMember]
        public string Code { get; set; }
        [DataMember]
        public CommonType Assessor { get; set; }
        [DataMember]
        public string StockCode { get; set; }
        [DataMember]
        public CommonType Company { get; set; }
        [DataMember]
        public Int32 Contract { get; set; }
        [DataMember]
        public Int64 AvailableQuantity { get; set; }
        [DataMember]
        public Int64 SettleQuantity { get; set; }
        [DataMember]
        public Int64 TotalQuantity { get; set; }
        [DataMember]
        public CommonType Type { get; set; }
        [DataMember]
        public DateTime? SettleDate { get; set; }
        [DataMember]
        public DateTime? DueDate { get; set; }
        [DataMember]
        public DateTime? RolloverDate { get; set; }
        [DataMember]
        public CommonType Status { get; set; }
        [DataMember]
        public long UserID { get; set; }
        [DataMember]
        public bool D3 { get; set; }
    }

    [Serializable]
    [DataContract]
    public class TermFilter
    {
        [DataMember]
        public string Assessors { get; set; }
        [DataMember]
        public string Clients { get; set; }
        [DataMember]
        public CommonType Company { get; set; }
        [DataMember]
        public bool D1 { get; set; }
        [DataMember]
        public bool D2 { get; set; }
        [DataMember]
        public bool D3 { get; set; }
        [DataMember]
        public int StatusID { get; set; }
        [DataMember]
        public string StartDate { get; set; }
        [DataMember]
        public string EndDate { get; set; }
    }
}
