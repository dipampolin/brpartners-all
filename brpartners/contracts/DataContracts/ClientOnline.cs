﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace QX3.Portal.Contracts.DataContracts
{
    [Serializable]
    [DataContract]
    public class ClientOnline
    {
        public ClientOnline()
        {
            Address = new List<AddressClientOnline>();
            Emails = new List<string>();
            Contact = new List<ContactClientOnline>();
            Fatca = new FatcaClientOnline();
            ClientFiles = new List<ClientAssign>();
        }

        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int IdStatusRegistration { get; set; }

        [DataMember]
        public int IdFichaCadastral { get; set; }

        [DataMember]
        public string EconomicGroup { get; set; }

        [DataMember]
        public string Classification { get; set; }

        [DataMember]
        public string ActivityGroup { get; set; }

        [DataMember]
        public string RegistrationType { get; set; }

        [DataMember]
        public string ClientType { get; set; }

        [DataMember]
        public string Category { get; set; }

        [DataMember]
        public string AtividadePrincipal { get; set; }

        [DataMember]
        public string NaturezaOperacao { get; set; }

        [DataMember]
        public string Size { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Cpf { get; set; }

        [DataMember]
        public string Profession { get; set; }

        [DataMember]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MM-yy}")]
        public DateTime? Birthdate { get; set; }

        [DataMember]
        public string Sex { get; set; }

        [DataMember]
        public string MaritalStatus { get; set; }

        [DataMember]
        public string MatrimonialRegime { get; set; }

        [DataMember]
        public string CommonLaw { get; set; }

        [DataMember]
        public string Estrangeiro { get; set; }

        [DataMember]
        public string NomePai { get; set; }

        [DataMember]
        public string NomeMae { get; set; }

        [DataMember]
        public List<string> Emails { get; set; }

        [DataMember]
        public string EmailPrincipal { get; set; }

        [DataMember]
        public string Situation { get; set; }

        [DataMember]
        public DateTime? DataInclusao { get; set; }

        [DataMember]
        public DateTime? DataAlteracao { get; set; }

        [DataMember]
        public string Observacao { get; set; }

        [DataMember]
        public string PaisNascimento { get; set; }

        [DataMember]
        public string PaisNascimentoEstrangeiro { get; set; }

        [DataMember]
        public string EstadoNascimento { get; set; }

        [DataMember]
        public string EstadoNascimentoEstrangeiro { get; set; }

        [DataMember]
        public string Naturalness { get; set; }

        [DataMember]
        public string NaturalnessForeign { get; set; }

        [DataMember]
        public string Nacionality { get; set; }

        [DataMember]
        public string NomeConjugue { get; set; }

        [DataMember]
        public int? CpfConjugue { get; set; }

        [DataMember]
        public DateTime? DataNascimentoConjugue { get; set; }

        [DataMember]
        public string DocumentType { get; set; }

        [DataMember]
        public string NumberDocument { get; set; }

        [DataMember]
        public string IssuingAgency { get; set; }

        [DataMember]
        public DateTime? ValidateDocument { get; set; }

        [DataMember]
        public DateTime? IssuingDate { get; set; }

        [DataMember]
        public string EstadoEmissao { get; set; }

        [DataMember]
        public string NomeEmpresa { get; set; }

        [DataMember]
        public DateTime? DataAdmissao { get; set; }

        [DataMember]
        public string Endereco { get; set; }

        [DataMember]
        public string Manager { get; set; }

        [DataMember]
        public DateTime? DataInicio { get; set; }

        [DataMember]
        public DateTime? DataFinal { get; set; }

        [DataMember]
        public string WorkGovernment { get; set; }

        [DataMember]
        public string JobGovernmentName { get; set; }

        [DataMember]
        public string Agency { get; set; }

        [DataMember]
        public DateTime? StartDateJobGovernment { get; set; }

        [DataMember]
        public DateTime? FinalDateJobGovernment { get; set; }

        [DataMember]
        public string RelativeWorkGovernment { get; set; }

        [DataMember]
        public string RelativeName { get; set; }

        [DataMember]
        public string RelativeRelationship { get; set; }

        [DataMember]
        public string JobRelativeName { get; set; }

        [DataMember]
        public string PossuiRepresentante { get; set; }

        [DataMember]
        public string RelationshipBrPartners { get; set; }

        [DataMember]
        public List<string> Telefones { get; set; }

        [DataMember]
        public List<AddressClientOnline> Address { get; set; }

        [DataMember]
        public List<ContactClientOnline> Contact { get; set; }

        [DataMember]
        public FatcaClientOnline Fatca { get; set; }

        [DataMember]
        public List<ClientAssign> ClientFiles { get; set; }
    }

    [Serializable]
    [DataContract]
    public class FatcaClientOnline
    {
        [DataMember]
        public string Fatca { get; set; }

        [DataMember]
        public string Tin { get; set; }

        [DataMember]
        public string IssuingCountry { get; set; }

        [DataMember]
        public int IdFiscalInformation { get; set; }
    }

    [Serializable]
    [DataContract]
    public class AddressClientOnline
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string AddressType { get; set; }

        [DataMember]
        public string DescriptionAddressType { get; set; }

        [DataMember]
        public string Finality { get; set; }

        [DataMember]
        public string DescriptionFinality { get; set; }

        [DataMember]
        public string StreetType { get; set; }

        [DataMember]
        public string StreetName { get; set; }

        [DataMember]
        public string Number { get; set; }

        [DataMember]
        public string State { get; set; }

        [DataMember]
        public string DescriptionState { get; set; }

        [DataMember]
        public string City { get; set; }

        [DataMember]
        public string Country { get; set; }

        [DataMember]
        public string DescriptionCountry { get; set; }
    }

    [Serializable]
    [DataContract]
    public class ContactClientOnline
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Type { get; set; }

        [DataMember]
        public string DescriptionType { get; set; }

        [DataMember]
        public string Ddi { get; set; }

        [DataMember]
        public string Ddd { get; set; }

        [DataMember]
        public string Number { get; set; }

        [DataMember]
        public string Ramal { get; set; }

        [DataMember]
        public string EmailContact { get; set; }
    }

    [Serializable]
    [DataContract]
    public class ClientAssign
    {
        [DataMember]
        public string FileName { get; set; }

        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string ContentType { get; set; }

        [DataMember]
        public byte[] Data { get; set; }
    }
}