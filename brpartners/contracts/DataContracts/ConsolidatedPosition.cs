﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace QX3.Portal.Contracts.DataContracts
{
    [Serializable]
    [DataContract]
    public class ConsolidatedPosition
    {
        [DataMember]
        public CPClientInformation ClientInformation { get; set; }
        [DataMember]
        public List<CPResume> Resume { get; set; }
        [DataMember]
        public CPTotalStockModel TotalStock { get; set; }
        [DataMember]
        public List<CPRentDonor> RentDonor { get; set; }
        [DataMember]
        public List<CPRentTaker> RentTaker { get; set; }
        [DataMember]
        public List<CPCDBFinal> CDBFinal { get; set; }
        [DataMember]
        public List<CPCommitedCDB> CommitedCDB { get; set; }
        [DataMember]
        public List<CPClubs> Clubs { get; set; }
        [DataMember]
		public CPBMFGuarantees BMFGuarantees { get; set; }
        [DataMember]
        public List<CPBovespaGuarantees> BovespaGuarantees { get; set; }
        [DataMember]
        public List<CPOption> Option { get; set; }
        [DataMember]
        public List<CPGold> Gold { get; set; }
        [DataMember]
        public CPTerms Terms { get; set; }
        [DataMember]
        public List<CPSettledTerms> SettledTerms { get; set; }
        [DataMember]
        public List<CPSettledTermsToday> SettledTermsToday { get; set; }
        [DataMember]
        public List<CPPublicTitles> PublicTitles { get; set; }
        [DataMember]
        public CPBMFTotal BMF { get; set; }
		[DataMember]
		public CPTotalCheckingAccount TotalCheckingAccount { get; set; }
    }

    [Serializable]
    [DataContract]
    public class CPResume
    {
        [DataMember]
        public string Type { get; set; } // tipo = "TotalStocks"
        [DataMember]
        public string Label { get; set; } // label = "ações total"
        [DataMember]
        public decimal InvestmentAccount { get; set; }
        [DataMember]
        public decimal NormalAccount { get; set; }
        [DataMember]
        public decimal Total { get; set; }
        [DataMember]
        public int ProductCount { get; set; }
    }

	#region TotalStock

	[Serializable]
    [DataContract]
	public class CPTotalStock : QuantitativePositionStock
    {
	}

    [Serializable]
    [DataContract]
    public class CPOpeningStock 
    {
        [DataMember]
        public string Company { get; set; }
        [DataMember]
        public string OperationType { get; set; }
        [DataMember]
        public string MarketType { get; set; }
        [DataMember]
        public long Quantity { get; set; }
        [DataMember]
        public decimal Price { get; set; }
        [DataMember]
        public decimal NegotiationValue { get; set; }
        [DataMember]
        public decimal CurrentPrice { get; set; }
        [DataMember]
        public decimal ProfitGainLossValue { get; set; }
        [DataMember]
        public decimal ProfitGainLossPercentual { get; set; }
        [DataMember]
        public string Gate { get; set; }
        [DataMember]
        public string Operator { get; set; }
    }

    [Serializable]
    [DataContract]
    public class CPTotalStockModel 
    {
        [DataMember]
        public List<CPTotalStock> TotalStockList { get; set; }

        [DataMember]
        public List<CPOpeningStock> OpeningPositionList { get; set; }
    }

	#endregion

	#region Rent

	[Serializable]
    [DataContract]
    public class CPRent
    {
        [DataMember]
        public string Company { get; set; }
        [DataMember]
        public string ISIN { get; set; }
        [DataMember]
        public string Portfolio { get; set; }
        [DataMember]
		public string Origin { get; set; }
        [DataMember]
        public string OpeningDate { get; set; }
        [DataMember]
        public string DueDate { get; set; }
        [DataMember]
        public string LiquidationGraceDate { get; set; }
        [DataMember]
        public Int64 Quantity { get; set; }
        [DataMember]
        public decimal AveragePrice { get; set; }
        [DataMember]
        public decimal RemunerationRate { get; set; }
        [DataMember]
        public decimal CommisionRate { get; set; }
        [DataMember]
        public decimal NetValue { get; set; }
        [DataMember]
        public decimal CurrentPrice { get; set; }
        [DataMember]
        public decimal CurrentValue { get; set; }
    }

    [Serializable]
    [DataContract]
    public class CPRentDonor : CPRent
    {

    }

    [Serializable]
    [DataContract]
    public class CPRentTaker : CPRent
    {

    }

    #endregion

    #region BMF

    [Serializable]
    [DataContract]
    public class CPBMFOpening
    {
        [DataMember]
        public string Commodity { get; set; }
        [DataMember]
        public long Quantity { get; set; }
        [DataMember]
        public decimal Closing { get; set; }
        [DataMember]
        public decimal Current { get; set; }
        [DataMember]
        public decimal ProfitLoss { get; set; }
        [DataMember]
        public decimal ProfitLossPercentage { get; set; }
    }

    [Serializable]
    [DataContract]
    public class CPBMFCurrent
    {
        [DataMember]
        public string Commodity { get; set; }
        [DataMember]
        public string TradeNumber { get; set; }
        [DataMember]
        public string TradeHour { get; set; }
        [DataMember]
        public string OperationType { get; set; }
        [DataMember]
        public string AfterMarket { get; set; }
        [DataMember]
        public long Quantity { get; set; }
        [DataMember]
        public decimal Price { get; set; }
        [DataMember]
        public decimal TotalPrice { get; set; }
        [DataMember]
        public decimal CurrentPrice { get; set; }
        [DataMember]
        public decimal ProfitLoss { get; set; }
        [DataMember]
        public decimal ProfitLossPercentage { get; set; }
        [DataMember]
        public string Type { get; set; }
    }

	[Serializable]
	[DataContract]
	public class CPBMFConsolidated
	{
		[DataMember]
		public string Commodity { get; set; }
		[DataMember]
		public long Quantity { get; set; }
		[DataMember]
		public decimal ProfitLoss { get; set; }
	}

    [Serializable]
    [DataContract]
    public class CPBMFTotal
    {
        [DataMember]
        public List<CPBMFOpening> BMFOpenList { get; set; }
        [DataMember]
        public List<CPBMFCurrent> BMFCurrentList { get; set; }
		[DataMember]
		public List<CPBMFConsolidated> BMFConsolidated { get; set; }
    }

    #endregion

    #region CDB

    [Serializable]
    [DataContract]
    public class CPCDB
    {
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string IssueDate { get; set; }
        [DataMember]
        public string DueDate { get; set; }
        [DataMember]
        public decimal Ratio { get; set; }
        [DataMember]
        public string OperationDate { get; set; }
        [DataMember]
        public string RedemptionDate { get; set; }
        [DataMember]
        public Int64 Quantity { get; set; }
        [DataMember]
        public decimal IRIOF { get; set; }
        [DataMember]
        public decimal NetValue { get; set; }
        [DataMember]
        public decimal GrossValue { get; set; }
    }

    [Serializable]
    [DataContract]
    public class CPCDBFinal : CPCDB
    {
    }

    [Serializable]
    [DataContract]
    public class CPCommitedCDB : CPCDB
    {
    }

    #endregion

    [Serializable]
    [DataContract]
    public class CPClubs
    {
        [DataMember]
        public string ClubFund { get; set; }
        [DataMember]
        public string ApplicationDate { get; set; }
        [DataMember]
        public Int64 QuotaQuantity { get; set; }
        [DataMember]
        public decimal QuotaValue { get; set; }
        [DataMember]
        public decimal GrossValue { get; set; }
        [DataMember]
        public decimal IR { get; set; }
        [DataMember]
        public decimal IOF { get; set; }
        [DataMember]
        public decimal NetValue { get; set; }
    }

	#region BMFGuarantees

	[Serializable]
	[DataContract]
	public class CPBMFGuarantees
	{
		[DataMember]
		public List<CPBMFGuaranteesOpening> OpeningPosition { get; set; }
		[DataMember]
		public List<CPBMFGuaranteesComposition> GuaranteesComposition { get; set; }
	}

	[Serializable]
    [DataContract]
    public class CPBMFGuaranteesOpening
    {
        [DataMember]
		public decimal TotalGuarantees { get; set; }
        [DataMember]
		public decimal RequiredMargin { get; set; }
        [DataMember]
		public decimal OwnGuarantees { get; set; }
        [DataMember]
		public decimal ExternalGuarantees { get; set; }
        [DataMember]
        public decimal Money { get; set; }
        [DataMember]
		public decimal Assets { get; set; }
        [DataMember]
		public decimal Custody { get; set; }
        [DataMember]
		public decimal Stocks { get; set; }
    }

	[Serializable]
	[DataContract]
	public class CPBMFGuaranteesComposition
	{
		[DataMember]
		public string Stock { get; set; }
		[DataMember]
		public Int64 StockNumber { get; set; }
		[DataMember]
		public string DueDate { get; set; }
		[DataMember]
		public Int64 Quantity { get; set; }
		[DataMember]
		public string InstitutionIssuer { get; set; }
		[DataMember]
		public decimal Value { get; set; }
		[DataMember]
		public string Umbrella { get; set; }
	}

	#endregion

	[Serializable]
    [DataContract]
    public class CPBovespaGuarantees
    {
        [DataMember]
        public string Stock { get; set; }
        [DataMember]
        public string Bank { get; set; }
        [DataMember]
        public string DueDate { get; set; }
        [DataMember]
        public long Quantity { get; set; }
        [DataMember]
        public decimal CurrentPrice { get; set; }
        [DataMember]
        public decimal GuaranteeValue { get; set; }
        [DataMember]
		public string Origin { get; set; }
        [DataMember]
		public string Umbrella { get; set; }
        [DataMember]
        public string DepositDate { get; set; }
        [DataMember]
        public decimal TotalValue { get; set; }
    }

    [Serializable]
    [DataContract]
	public class CPOption : QuantitativePositionStock
    {
    }

    [Serializable]
    [DataContract]
    public class CPGold
    {
        [DataMember]
		public decimal Quantity { get; set; }
        [DataMember]
		public decimal DefaultQuantity { get; set; }
        [DataMember]
        public decimal CustodyValue { get; set; }
        [DataMember]
        public decimal ProvisionCustodyRatio { get; set; }
        [DataMember]
        public decimal ProvisionLastMonth { get; set; }
    }

	#region Term

	[Serializable]
	[DataContract]
	public class CPTerms
	{
		[DataMember]
		public List<CPTerm> Terms { get; set; }
		[DataMember]
		public List<CPRegisteredTerm> RegisteredTerms { get; set; }
	}

    [Serializable]
    [DataContract]
	public class CPTerm : QuantitativePositionStock
    {
    }

    [Serializable]
    [DataContract]
	public class CPRegisteredTerm
    {
        [DataMember]
        public string Operation { get; set; }
        [DataMember]
        public string Stock { get; set; }
        [DataMember]
        public string Contract { get; set; }
        [DataMember]
        public string Opening { get; set; }
        [DataMember]
        public string DueDate { get; set; }
        [DataMember]
        public long OriginalQuantity { get; set; }
        [DataMember]
        public decimal OriginalPrice { get; set; }
        [DataMember]
        public decimal OriginalValue { get; set; }
        [DataMember]
        public decimal CurrentPrice { get; set; }
        [DataMember]
        public decimal CurrentValue { get; set; }
        [DataMember]
        public decimal ProfitLossPercentage { get; set; }
        [DataMember]
        public decimal ProfitLossValue { get; set; }
    }

	#endregion

	#region SettledTerms

	[Serializable]
    [DataContract]
    public class CPSettledTerms
    {
        [DataMember]
        public string LiquidationDate { get; set; }
        [DataMember]
        public string Stock { get; set; }
        [DataMember]
        public string LiquidationType { get; set; }
        [DataMember]
        public Int64 BusinessNumber { get; set; }
        [DataMember]
        public string CV { get; set; }
        [DataMember]
        public Int64 Quantity { get; set; }
        [DataMember]
        public decimal BusinessPrice { get; set; }
        [DataMember]
        public decimal SettledContractValue { get; set; }
        [DataMember]
        public decimal SettledContractReversed { get; set; }
        [DataMember]
        public decimal ProfitLoss { get; set; }
    }

    [Serializable]
    [DataContract]
    public class CPSettledTermsToday : CPSettledTerms
    {
    }

    #endregion

    [Serializable]
    [DataContract]
    public class CPPublicTitles
    {
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public Int64 Quantity { get; set; }
        [DataMember]
        public string DueDate { get; set; }
        [DataMember]
        public decimal GrossValue { get; set; }
        [DataMember]
        public decimal NetValue { get; set; }
		[DataMember]
		public decimal IRIOF { get; set; }
		[DataMember]
		public decimal Rate { get; set; }
    }

	#region TotalCheckingAccount

	[Serializable]
	[DataContract]
	public class CPTotalCheckingAccount
	{
		[DataMember]
		public CPCheckingAccountBalance CheckingAccountBalance { get; set; }
		[DataMember]
		public CPDescriptionProjected DescriptionProjected { get; set; }
	}
	
	[Serializable]
	[DataContract]
	public class CPCheckingAccountBalance
	{
		[DataMember]
		public decimal AmountAvailable { get; set; }
		[DataMember]
		public decimal AmountProjected { get; set; }
		[DataMember]
		public decimal AmountTotal { get; set; }
		[DataMember]
		public decimal TradingProjected { get; set; }
		[DataMember]
		public decimal Total { get; set; }
		[DataMember]
		public Int64 DebtDaysMounth { get; set; }
		[DataMember]
		public Int64 DebtDaysTotal { get; set; }
	}

	[Serializable]
	[DataContract]
	public class CPDescriptionProjected
	{
		[DataMember]
		public string SettlementDate { get; set; }
		[DataMember]
		public decimal AmountProjected { get; set; }
		[DataMember]
		public decimal ProjectedAmountCurrentTrading { get; set; }
		[DataMember]
		public decimal Total { get; set; }
	}

	#endregion

	[Serializable]
    [DataContract]
    public class ConsolidatedPositionResponse
    {
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public ConsolidatedPosition Result { get; set; }
    }

    [Serializable]
    [DataContract]
    public class CPClientInformation
    {
        [DataMember]
        public int ClientCode { get; set; }
        [DataMember]
        public CommonType Client { get; set; }
        [DataMember]
        public string CPFCNPJ { get; set; }
        [DataMember]
        public string Birthdate { get; set; }
		[DataMember]
		public string AssessorCode { get; set; }
		[DataMember]
        public string AssessorName { get; set; }
        [DataMember]
        public string PositionDate { get; set; }
        [DataMember]
        public string PositionHour { get; set; }
		[DataMember]
		public string Email { get; set; }
		[DataMember]
        public string ClientName { get; set; }
        [DataMember]
        public string Occupation { get; set; }
        [DataMember]
        public string Age { get; set; }
        [DataMember]
        public string Suitability { get; set; }
		[DataMember]
		public string SuitabilityProfile { get; set; }
        [DataMember]
        public decimal NetWorth { get; set; }
        [DataMember]
        public List<CPMarketInfo> MarketInfo { get; set; }
        [DataMember]
        public Address Address { get; set; }
        [DataMember]
        public List<BankAccount> BankAccounts { get; set; }
        [DataMember]
        public List<OrdersIssuer> OrdersIssuers { get; set; }
		[DataMember]
		public string TradingSince { get; set; }
		[DataMember]
		public decimal ReturnBrokeragePercentage { get; set; }
		[DataMember]
		public string ClientDigit { get; set; }
    }

    [Serializable]
    public class CPMarketInfo 
    { 
        [DataMember]
        public string MarketName { get; set; }
        [DataMember]
        public string MarketType { get; set; }
        [DataMember]
        public string Counterparty { get; set; }
        [DataMember]
        public string Qualified { get; set; }
    }

	[Serializable]
	public class QuantitativePositionStock
	{
		[DataMember]
		public string Stock { get; set; }
		[DataMember]
		public string Portfolio { get; set; }
		[DataMember]
		public string Distribution { get; set; }
		[DataMember]
		public long AvailableQuantity { get; set; }
		[DataMember]
		public long PendingQuantity { get; set; }
		[DataMember]
		public long BlockedQuantity { get; set; }
		[DataMember]
		public long SettleInD0 { get; set; }
		[DataMember]
		public long BlockedD1 { get; set; }
		[DataMember]
		public long BlockedD2 { get; set; }
		[DataMember]
		public long BlockedD3 { get; set; }
		[DataMember]
		public long BalanceQuantity { get; set; }
		[DataMember]
		public decimal CurrentPrice { get; set; }
		[DataMember]
		public decimal CurrentValue { get; set; }
		[DataMember]
		public decimal DiscountPercentage { get; set; }
		[DataMember]
		public decimal DiscountValue { get; set; }
		[DataMember]
		public string DueDate { get; set; }
		[DataMember]
		public long VACQuantity { get; set; }
		[DataMember]
		public decimal VACValue { get; set; }
	}
}