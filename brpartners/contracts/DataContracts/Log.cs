﻿using System.Runtime.Serialization;
using System;
using System.Collections.Generic;
using System.Xml.Linq;

namespace QX3.Portal.Contracts.DataContracts
{
	[Serializable][DataContract]
	public class HistoryFilter
	{
		[DataMember]
		public string ModuleName { get; set; }
		[DataMember]
		public string StartDate { get; set; }
		[DataMember]
		public string EndDate { get; set; }
		[DataMember]
		public long ResponsibleID { get; set; }
		[DataMember]
		public string Action { get; set; }
        [DataMember]
        public int ID { get; set; }
		[DataMember]
		public int TargetID { get; set; }
		[DataMember]
		public string TargetText { get; set; }
        [DataMember]
        public string ExtraTargetText { get; set; }
	}

	[Serializable][DataContract]
	public class HistoryData
	{
		[DataMember]
		public DateTime Date { get; set; }
		[DataMember]
		public int ResponsibleID { get; set; }
		[DataMember]
		public string Responsible { get; set; }
		[DataMember]
		public string Action { get; set; }
        [DataMember]
        public int ID { get; set; }
		[DataMember]
		public int TargetID { get; set; }
		[DataMember]
		public string Target { get; set; }
		[DataMember]
		public string OldData { get; set; }
		[DataMember]
		public string NewData { get; set; }
        [DataMember]
        public string ExtraTarget { get; set; }
	}

	[Serializable][DataContract]
	public class HistoryLog
	{
		[DataMember]
		public string ModuleName { get; set; }
		[DataMember]
		public HistoryData Data { get; set; }
	}
}
