﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.IO;

namespace QX3.Portal.Contracts.DataContracts
{
    #region Failure Registration

    [Serializable][DataContract]
	public class Failure
	{
		[DataMember]
		public Int32 ClientCode { get; set; }
		[DataMember]
		public Int32 TradeNumber { get; set; }
		[DataMember]
		public string ISIN { get; set; }
		[DataMember]
		public string TradeCode { get; set; }
		[DataMember]
		public DateTime TradeDate { get; set; }
		[DataMember]
		public DateTime SettlementDate { get; set; }
		[DataMember]
		public Int32 Quantity { get; set; }
		[DataMember]
		public Decimal OperationValue { get; set; }
		[DataMember]
		public Decimal TotalValue { get; set; }
		[DataMember]
		public Int32 FailureQuantity { get; set; }
		[DataMember]
		public Decimal FailureValue { get; set; }
		[DataMember]
		public DateTime ChangeDate { get; set; }
		[DataMember]
		public char OperationType { get; set; }
		[DataMember]
		public string Username { get; set; }
	}
    #endregion

    #region Disclaimer  
    [Serializable][DataContract]
    public class Disclaimer
    {
		[DataMember]
		public ReportType ReportType { get; set; }
		[DataMember]
		public string Content { get; set; }
    }

    [Serializable][DataContract]
    public class ReportType 
    {
        [DataMember]
        public Int32 ReportId { get; set; }
        [DataMember]
        public string Description{ get; set; }
		[DataMember]
		public Boolean HasDisclaimer { get; set; }
    }

    #endregion

    #region PTAX
    [Serializable][DataContract]
    public class PTax
    {
        [DataMember]
        public Decimal Value { get; set; }
        [DataMember]
        public DateTime Date { get; set; }
    }

    [Serializable][DataContract]
    public class SettlementDate 
    {
        [DataMember]
        public DateTime LastSettlementDate { get; set; }
    }
    #endregion

    #region Broker
    [Serializable]
    [DataContract]
    public class Broker 
    {
        [DataMember]
        public int BrokerCode { get; set; }

        [DataMember]
        public string BrokerName { get; set; }
    }

    #endregion
}
