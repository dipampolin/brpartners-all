﻿using System.Runtime.Serialization;
using System;
using System.Collections.Generic;
using System.Xml.Linq;

namespace QX3.Portal.Contracts.DataContracts
{
	[Serializable][DataContract]
	public class StockPrice
	{
        [DataMember]
        public string Symbol { get; set; }
        [DataMember]
        public double Price { get; set; }
	}
}
