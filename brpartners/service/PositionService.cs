﻿using System;
using System.Collections.Generic;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.Contracts.ServiceContracts;
using System.ServiceModel.Activation;
using QX3.Portal.Services.Business;
using System.Net.Mail;
using System.IO;
using System.Configuration;
using System.Linq;
using QX3.Spinnex.Common.Services.Mail;
using QX3.Spinnex.Common.Services.Logging;

namespace QX3.Portal.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class PositionService : IPositionContract
    {
        private PositionMethods positionController;

        private PositionMethods PositionController
        {
            get
            {
                positionController = positionController ?? new PositionMethods();
                return positionController;
            }
        }

        public WcfResponse<CustodyPositionResponse> ListCustodyPosition(int cdCliente, string cdMercado)
        {
            try
            {
                var list = PositionController.ListCustodyPosition(cdCliente, cdMercado);
                return new WcfResponse<CustodyPositionResponse> { Result = list };
            }
            catch (Exception ex)
            {
                return new WcfResponse<CustodyPositionResponse> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<ConsolidatedPositionResponse2> ListConsolidatedPosition(int cdCliente)
        {
            try
            {
                var list = PositionController.ListConsolidatedPosition(cdCliente);
                return new WcfResponse<ConsolidatedPositionResponse2> { Result = list };
            }
            catch (Exception ex)
            {
                return new WcfResponse<ConsolidatedPositionResponse2> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<List<ConsolidatedPositionDetails>> ListConsolidatedPositionDetails(int cdCliente, string grupo)
        {
            try
            {
                var list = PositionController.ListConsolidatedPositionDetails(cdCliente, grupo);
                return new WcfResponse<List<ConsolidatedPositionDetails>> { Result = list };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<ConsolidatedPositionDetails>> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<FinancialPositionResponse> FinancialPosition(int cdCliente)
        {
            try
            {
                var res = PositionController.FinancialPosition(cdCliente);
                return new WcfResponse<FinancialPositionResponse> { Result = res };
            }
            catch (Exception ex)
            {
                return new WcfResponse<FinancialPositionResponse> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<List<InvestmentsGroups>> ListInvestmentsGroups()
        {
            try
            {
                var list = PositionController.ListInvestmentsGroups();
                return new WcfResponse<List<InvestmentsGroups>> { Result = list };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<InvestmentsGroups>> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<List<ExtratoRendaDatails>> ListExtratoRendaDatails(string codEmpresa, string cpfCnpj, string DtRefDe, string DtRefAte)
        {
            try
            {
                var list = PositionController.ListExtratoRendaDatails(codEmpresa, cpfCnpj, DtRefDe, DtRefAte);
                return new WcfResponse<List<ExtratoRendaDatails>> { Result = list };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<ExtratoRendaDatails>> { Result = null, Message = ex.Message };
            }
        }

    }
}
