﻿using System;
using System.Xml;
using QX3.Spinnex.Common.Services.Logging;
using QX3.Spinnex.Trace.Services.Transactions;
using QX3.Spinnex.Trace.Services.Data;
using QX3.Portal.Contracts.ServiceContracts;
using QX3.Portal.Services.Business.Log;
using System.ServiceModel.Activation;
using QX3.Portal.Contracts.DataContracts;
using System.Xml.Linq;
using System.Collections.Generic;

namespace QX3.Portal.Services
{
	[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
	public class LogService : ILogContract
	{
		private Log logController;

		private Log LogController
		{
			get
			{
				logController = logController ?? new Log();
				return logController;
			}
		}

		public void CreateTraceRecord(string data)
		{
			try
			{
				LogManager.WriteLog("Gravar transacao");
				TraceTransactions trans = new TraceTransactions();
				LogController.CreateTraceRecord(data);
			}
			catch (Exception ex)
			{
				LogManager.WriteError(ex);
			}
		}

		public TraceApplication[] APP_ListApplications()
		{
			LogManager.WriteLog("Lista aplicações");
			ApplicationTransactions trans = new ApplicationTransactions();
			return trans.ListApplications();
		}

		public TraceServer[] SRV_ListServers(int applicationID)
		{
			throw new Exception("Nao implementado");
		}

		public TraceModule[] MOD_ListModules(int applicationID)
		{
			ModuleTransactions trans = new ModuleTransactions();
			return trans.ListModules(applicationID);
		}

		public TraceModuleHierarchy MOD_ListHierarchy(int applicationID)
		{
			throw new Exception("Nao implementado");
		}

		public TraceSearchResult[] TRACE_Search(TraceSearchParameters searchParams)
		{
			TraceTransactions trans = new TraceTransactions();
			return trans.Search(searchParams);
		}

		public string TRACE_GetDetails(Guid transactionID)
		{
			try
			{
				return LogController.GetTraceDetails(transactionID);
			}
			catch (Exception ex)
			{
				LogManager.WriteError(ex);
			}
			return string.Empty;
		}

		public RelatedTrace[] TRACE_LoadRelatedTraces(Guid transactionID)
		{
			TraceTransactions trans = new TraceTransactions();
			return trans.LoadRelatedTraces(transactionID);
		}

		public WcfResponse<List<HistoryData>> LoadHistory(HistoryFilter filter)
		{
			try
			{
				return new WcfResponse<List<HistoryData>> { Result = LogController.LoadHistory(filter) };
			}
			catch (Exception ex)
			{
				return new WcfResponse<List<HistoryData>> { Result = null, Message = ex.Message };
			}
		}

		public WcfResponse<Boolean> InsertHistory(HistoryLog log)
		{
			try
			{
				return new WcfResponse<Boolean> { Result = LogController.InsertHistory(log) };
			}
			catch (Exception ex)
			{
				return new WcfResponse<Boolean> { Result = false, Message = ex.Message };
			}
		}
	}
}
