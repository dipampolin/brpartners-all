﻿using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.Contracts.ServiceContracts;
using QX3.Portal.Services.Business;
using System;
using System.Collections.Generic;
using System.ServiceModel.Activation;

namespace QX3.Portal.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class DemonstrativeService : IDemonstrativeContract
    {
        private DemonstrativeMethods demonstrativeController;

        private DemonstrativeMethods DemonstrativeController
        {
            get
            {
                demonstrativeController = demonstrativeController ?? new DemonstrativeMethods();
                return demonstrativeController;
            }
        }

        public WcfResponse<ExtractRendaResponse> Extract(string CodEmpresa, string Cnpjcpf, string DtRefDe, string DtRefAte)
        {
            try
            {
                var list = DemonstrativeController.Extract(CodEmpresa, Cnpjcpf, DtRefDe, DtRefAte);
                return new WcfResponse<ExtractRendaResponse> { Result = list };
            }
            catch (Exception ex)
            {
                return new WcfResponse<ExtractRendaResponse> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<List<ExtractFunction>> ExtractFunction(string CodEmpresa, string DtRefDe, string DtRefAte, string CpfCnpj, string UserName)
        {
            try
            {
                var list = DemonstrativeController.ExtractFunction(CodEmpresa, DtRefDe, DtRefAte, CpfCnpj, UserName);
                return new WcfResponse<List<ExtractFunction>> { Result = list };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<ExtractFunction>> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<TradingSummaryResponse> ListTradingSummary(int cdCliente, string cdAtivo, DateTime dtInicial, DateTime dtFinal)
        {
            try
            {
                var list = DemonstrativeController.ListTradingSummary(cdCliente, cdAtivo, dtInicial, dtFinal);
                return new WcfResponse<TradingSummaryResponse> { Result = list };
            }
            catch (Exception ex)
            {
                return new WcfResponse<TradingSummaryResponse> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<ProceedsDemonstrativeResponse> ListProceeds(int cdCliente, string provisionados, DateTime dtInicial, DateTime dtFinal)
        {
            try
            {
                var list = DemonstrativeController.ListProceeds(cdCliente, provisionados, dtInicial, dtFinal);
                return new WcfResponse<ProceedsDemonstrativeResponse> { Result = list };
            }
            catch (Exception ex)
            {
                return new WcfResponse<ProceedsDemonstrativeResponse> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<ExtractNDFResponse> ListExtractNDF(string CodEmpresa, string Cnpjcpf, int Mes, int Ano)
        {
            try
            {
                var list = DemonstrativeController.ListExtractNDF(CodEmpresa, Cnpjcpf, Mes, Ano);
                return new WcfResponse<ExtractNDFResponse> { Result = list };
            }
            catch (Exception ex)
            {
                return new WcfResponse<ExtractNDFResponse> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<ExtractSWPResponse> ListExtractSWP(string CodEmpresa, string Cnpjcpf, int mes, int ano)
        {
            try
            {
                var list = DemonstrativeController.ListExtractSWP(CodEmpresa, Cnpjcpf, mes, ano);
                return new WcfResponse<ExtractSWPResponse> { Result = list };
            }
            catch (Exception ex)
            {
                return new WcfResponse<ExtractSWPResponse> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<BrokerageNoteResponse> ListBrokeragenotesDetails(int cdCliente, int nrNota, DateTime dtNegocio)
        {
            try
            {
                var list = DemonstrativeController.ListBrokeragenotesDetails(cdCliente, nrNota, dtNegocio);
                return new WcfResponse<BrokerageNoteResponse> { Result = list };
            }
            catch (Exception ex)
            {
                return new WcfResponse<BrokerageNoteResponse> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<BrokerageNoteResponse> ListBrokeragenotesNegociosDia(int cdCliente, DateTime dtNegocio, string tpNegocio)
        {
            try
            {
                var list = DemonstrativeController.ListBrokeragenotesNegociosDia(cdCliente, dtNegocio, tpNegocio);
                return new WcfResponse<BrokerageNoteResponse> { Result = list };
            }
            catch (Exception ex)
            {
                return new WcfResponse<BrokerageNoteResponse> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<BrokerageNoteResponse> ListBrokeragenotesNegociosHist(int cdCliente, DateTime dtNegocio, string tpNegocio)
        {
            try
            {
                var list = DemonstrativeController.ListBrokeragenotesNegociosHist(cdCliente, dtNegocio, tpNegocio);
                return new WcfResponse<BrokerageNoteResponse> { Result = list };
            }
            catch (Exception ex)
            {
                return new WcfResponse<BrokerageNoteResponse> { Result = null, Message = ex.Message };
            }
        }
    }

}
