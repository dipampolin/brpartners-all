﻿using System;
using System.Collections.Generic;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.Services.Data;
using QX3.Spinnex.Common.Services.Logging;

namespace QX3.Portal.Services.Business
{
    public class PositionMethods
    {
        private PositionDAL provider;

        public PositionMethods()
		{
            provider = new PositionDAL();
		}

        public CustodyPositionResponse ListCustodyPosition(int cdCliente, string cdMercado)
		{
            LogManager.InitLog("Position.ListCustodyPosition");
            CustodyPositionResponse result = new CustodyPositionResponse();

			try
			{
				provider.Connect();
				result = provider.ListCustodyPosition(cdCliente, cdMercado);
			}
			catch (Exception ex)
			{
				LogManager.WriteError(ex);
				throw new Exception(ex.Message, ex);
			}
			finally
			{
				provider.Close();
			}

			return result;
		}

        public ConsolidatedPositionResponse2 ListConsolidatedPosition(int cdCliente)
        {
            LogManager.InitLog("Position.ListConsolidatedPosition");
            ConsolidatedPositionResponse2 result = new ConsolidatedPositionResponse2();

            try
            {
                provider.Connect();
                result = provider.ListConsolidatedPosition(cdCliente);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return result;
        }

        public List<ConsolidatedPositionDetails> ListConsolidatedPositionDetails(int cdCliente, string grupo)
        {
            LogManager.InitLog("Position.ListConsolidatedPositionDetails");
            List<ConsolidatedPositionDetails> result = new List<ConsolidatedPositionDetails>();

            try
            {
                provider.Connect();
                result = provider.ListConsolidatedPositionDetails(cdCliente, grupo);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return result;
        }

        public FinancialPositionResponse FinancialPosition(int cdCliente)
        {
            LogManager.InitLog("Position.FinancialPosition");
            FinancialPositionResponse result = new FinancialPositionResponse();

            try 
            {
                provider.Connect();
                result = provider.FinancialPosition(cdCliente);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return result;
        }

        public List<InvestmentsGroups> ListInvestmentsGroups()
        {
            LogManager.InitLog("Position.ListInvestmentsGroups");
            List<InvestmentsGroups> result = new List<InvestmentsGroups>();

            try
            {
                provider.Connect();
                result = provider.ListInvestmentsGroups();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return result;
        }

        public List<ExtratoRendaDatails> ListExtratoRendaDatails(string codEmpresa, string cpfCnpj, string DtRefDe, string DtRefAte)
        {
            LogManager.InitLog("Position.ListExtratoRendaDatails");
            List<ExtratoRendaDatails> result = new List<ExtratoRendaDatails>();

            try
            {
                provider.Connect();
                result = provider.ListExtratoRendaDatails(codEmpresa, cpfCnpj, DtRefDe, DtRefAte);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return result;
        }
    }
}
