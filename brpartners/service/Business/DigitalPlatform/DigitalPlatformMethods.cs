﻿using QX3.Portal.Services.Data.DigitalPlatform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QX3.Portal.Services.Business.DigitalPlatform
{
    public class DigitalPlatformMethods
    {
        private DigitalPlatformDAL provider;

        public DigitalPlatformMethods()
        {
            provider = new DigitalPlatformDAL();
        }

        public void SetNeedToUpdateDigitalPlatform(int clientId, bool needToBeUpdatedInDigitalPlatform)
        {
            provider.SetNeedToUpdateDigitalPlatform(clientId, needToBeUpdatedInDigitalPlatform);
        }

        public void SetSendDateDigitalPlatform(int clientId, DateTime suitabilityCompletionDate)
        {
            DateTime sendDateDigitalPlatform = DateTime.Now;

            provider.SetSendDateDigitalPlatform(clientId, suitabilityCompletionDate, sendDateDigitalPlatform);
        }
    }
}
