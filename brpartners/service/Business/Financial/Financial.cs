﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QX3.Portal.Services.Data.Financial;
using QX3.Portal.Contracts.DataContracts;
using QX3.Spinnex.Common.Services.Logging;

namespace QX3.Portal.Services.Business.Financial
{
    public class Financial
    {
        public FinancialDAL provider;

        public Financial()
		{
            provider = new FinancialDAL();
		}

        public List<ProjectedReportItem> LoadProjectedReport(ProjectedReportFilter filter, int? userId)
        {
            LogManager.InitLog("Financial.LoadProjectedReport");
            List<ProjectedReportItem> list;

            try
            {
                provider.Connect();
                list = provider.LoadProjectedReport(filter, userId);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return list;
        }

        public List<RedemptionRequestItem> LoadRedemptionRequest(RedemptionRequestFilter filter, int? userId)
        {
            LogManager.InitLog("Financial.LoadRedemptionRequest");
            List<RedemptionRequestItem> obj;

            try
            {
                provider.Connect();
                obj = provider.LoadRedemptionRequest(filter, userId);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return obj;
        }

        public bool DeleteRedemptionRequest(int id, string clientId)
        {
            LogManager.InitLog("Financial.DeleteRedemptionRequest");
            bool result;

            try
            {
                provider.Connect();
                result = provider.DeleteRedemptionRequest(id, clientId);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return result;
        }

        public RedemptionRequestResult InsertRedemptionRequest(RedemptionRequestItem item)
        {
            LogManager.InitLog("Financial.InsertRedemptionRequest");
            RedemptionRequestResult result;

            try
            {
                provider.Connect();
                result = provider.InsertRedemptionRequest(item);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return result;
        }

		public RedemptionRequestBalance LoadRedemptionRequestBalance(string clientCode, string assessor, int? userId)
		{
			LogManager.InitLog("Financial.LoadRedemptionRequestBalance");
			RedemptionRequestBalance balance;

			try
			{
				provider.Connect();
				balance = provider.LoadRedemptionRequestBalance(clientCode, assessor, userId);
			}
			catch (Exception ex)
			{
				LogManager.WriteError(ex);
				throw new Exception(ex.Message, ex);
			}
			finally
			{
				provider.Close();
			}

			return balance;
		}

        public List<RedemptionRequestBankAccount> LoadBankAccountInformation(string clientCode)
        {
            LogManager.InitLog("Financial.LoadBankAccountInformation");
            List<RedemptionRequestBankAccount> obj;

            try
            {
                provider.Connect();
                obj = provider.LoadBankAccountInformation(clientCode);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return obj;
        }

        public bool ChangeRedemptionRequestStatus(RedemptionRequestItem item)
        {
            LogManager.InitLog("Financial.ChangeRedemptionRequestStatus");
            bool result;

            try
            {
                provider.Connect();
                result = provider.ChangeRedemptionRequestStatus(item);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return result;
        }

        public int GetPendentRedemptionRequest()
        {
            LogManager.InitLog("Financial.GetPendentRedemptionRequest");
            int result;

            try
            {
                provider.Connect();
                result = provider.GetPendentRedemptionRequest();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return result;
        }

        public List<DailyFinancial> LoadDailyFinancialReport(DailyFinancialFilter filter, FilterOptions options, int? userId, out int count)
        {
            LogManager.InitLog("Financial.LoadProjectedReport");
            List<DailyFinancial> list;

            try
            {
                provider.Connect();
                list = provider.LoadDailyFinancialReport(filter, options, userId, out count);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return list;
        }

        
    }
}
