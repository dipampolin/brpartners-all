﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QX3.Portal.Services.Data;
using QX3.Portal.Contracts.DataContracts;
using QX3.Spinnex.Common.Services.Logging;

namespace QX3.Portal.Services.Business
{
    public class CustodyTransferMethods
    {
        private CustodyTransferDAL provider;

        public CustodyTransferMethods()
		{
            provider = new CustodyTransferDAL();
		}

        public List<CustodyTransfer> LoadCustodyTransfers(CustodyTransferFilter filter, FilterOptions options, int? userId, out int count)
        {
			LogManager.InitLog("Other.LoadCustodyTransfers");
            List<CustodyTransfer> list = null;
            count = 0;

            try
            {
                provider.Connect();
                list = provider.LoadCustodyTransfers(filter, options, userId, out count);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return list;
        }

        public bool UpdateCustodyTransferStatus(List<CustodyTransfer> requestList) 
        {
            LogManager.InitLog("BrokerageDiscounts.UpdateCustodyTransferStatus");
             var auxRes = true;

            try
            {
                provider.Connect();
                                                
                foreach (var req in requestList) 
                {
                    var result = provider.UpdateCustodyTransferStatus(req);

                    if (!result && auxRes)
                        auxRes = false;
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return auxRes;
        }

		public CustodyTransferReport LoadCustodyTransferReport(string fromDate, string toDate)
		{
			LogManager.InitLog("Other.LoadCustodyTransferReport");
			CustodyTransferReport report = null;

			try
			{
				provider.Connect();
				report = provider.LoadCustodyTransferReport(fromDate, toDate);
			}
			catch (Exception ex)
			{
				LogManager.WriteError(ex);
				throw new Exception(ex.Message, ex);
			}
			finally
			{
				provider.Close();
			}

			return report;
		}

        public bool DeleteCustodyTransfer(List<CustodyTransfer> requestList)
        {
            LogManager.InitLog("BrokerageDiscounts.DeleteCustodyTransfer");
            var auxRes = true;

            try
            {
                provider.Connect();

                foreach (var req in requestList)
                {
                    var result = provider.DeleteCustodyTransfer(req);

                    if (!result && auxRes)
                        auxRes = false;
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return auxRes;
        }

        public List<CustodyTransferDetails> LoadClientPosition(CustodyTransferFilter filter, int? userId) 
        {
            LogManager.InitLog("Other.LoadClientPosition");
            List<CustodyTransferDetails> list = null;

            try
            {
                provider.Connect();
                list = provider.LoadClientPosition(filter, userId);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return list;
        }

        public bool InsertCustodyTransfer(CustodyTransfer request, out int requestId)
        {
            LogManager.InitLog("Other.InsertCustodyTransfer");
            
            bool result = true;

            try
            {
                provider.Connect();
                result = provider.InsertCustodyTransfer(request, out requestId);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return result;
        }

        public bool EditCustodyTransfer(CustodyTransfer request) 
        {
            LogManager.InitLog("Other.EditCustodyTransfer");

            bool result = true;

            try
            {
                provider.Connect();
                result = provider.EditCustodyTransfer(request);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return result;
        }

        public int GetPendentCustodyTransfers(CustodyTransferFilter filter, int? userId) 
        {
            LogManager.InitLog("Other.LoadClientPosition");
            int count = 0;

            try
            {
                provider.Connect();
                count = provider.GetPendentCustodyTransfers(filter, userId);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return count;
        }
    }
}
