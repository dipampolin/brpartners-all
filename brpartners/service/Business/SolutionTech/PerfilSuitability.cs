﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace QX3.Portal.Services.Business.SolutionTech
{
    public class PerfilSuitability
    {
        public int codBolsa { get; set; }

        public string perfil { get; set; }

        public decimal totalPts { get; set; }

        public DateTime CompletionDate { get; set; }

        [XmlIgnore]
        public bool NotifiedToBRP { get; set; }

        [XmlIgnore]
        public bool UpdatedInSolutionTech { get; set; }
    }
}
