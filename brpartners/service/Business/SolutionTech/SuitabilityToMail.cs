﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QX3.Portal.Services.Business.SolutionTech
{
    public class SuitabilityToMail
    {
        public SuitabilityToMail(PerfilSuitability profileSuitability, SolutionTechResponse solutionTechResponse)
        {
            ProfileSuitability = profileSuitability ?? throw new ArgumentNullException(nameof(profileSuitability));
            SolutionTechResponse = solutionTechResponse ?? throw new ArgumentNullException(nameof(solutionTechResponse));
        }

        public PerfilSuitability ProfileSuitability { get; set; }
        public SolutionTechResponse SolutionTechResponse { get; set; }
    }
}
