﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QX3.Portal.Services.Business.SolutionTech
{
    public class SolutionTechResponse
    {
        public string StatusAttributeTag 
        { 
            get 
            {
                return "Status";
            }
        }
        public int ResponseStatus { get; set; }
        public string MessageStatusTag { get; set; }
        public string Message { get; set; }
        
        public enum ResponseAttributes
        {
            Info,
            Alert,
            Error
        }
    }
}
