﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QX3.Portal.Services.Business.SolutionTech
{
    public class SolutionTechLog
    {
        public int Id { get; set; }
        public int CdBolsa { get; set; }
        public string InvestorType { get; set; }
        public int ScoreSuitability { get; set; }
        public int StatusCode { get; set; }
        public string AttributeTag { get; set; }
        public string Message { get; set; }
        public DateTime SuitabilityDate { get; set; }
        public DateTime SendDate { get; set; }
    }
}
