﻿using QX3.Portal.Services.Data.SolutionTech;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QX3.Portal.Services.Business.SolutionTech
{
    public class SolutionTechMethods
    {
        private SolutionTechDAL provider;

        public SolutionTechMethods()
        {
            provider = new SolutionTechDAL();
        }

        public void RecordLog(decimal codBolsa, string tipoInvestidor, decimal scoreSuitability, int httpStatusCode, string attributeTag, string message, DateTime suitabilityCompletionDate)
        {
            provider.RecordLog(codBolsa, tipoInvestidor, scoreSuitability, httpStatusCode, attributeTag, message, suitabilityCompletionDate);
        }

        public void UpdatedSolutionTech(int cdBolsa, DateTime suitabilityDate, bool sendedToSolutionTech)
        {
            provider.UpdatedSolutionTech(cdBolsa, suitabilityDate, sendedToSolutionTech);
        }

        public void NotifiedBRP(int cdBolsa, DateTime suitabilityDate, bool brpEmailNotified)
        {
            provider.NotifiedBRP(cdBolsa, suitabilityDate, brpEmailNotified);
        }

        public List<SolutionTechLog> GetLogsSolutionTech(int? codBolsa)
        {
            return provider.GetLogsSolutionTech(codBolsa);
        }
    }
}
