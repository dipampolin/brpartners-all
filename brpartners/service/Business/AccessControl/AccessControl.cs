﻿using QX3.Portal.Services.Data.AccessControl;
using QX3.Spinnex.Authentication.Services.UserData;
using QX3.Spinnex.Common.Services.Logging;
using QX3.Portal.Contracts.DataContracts;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using QX3.Spinnex.Authentication.Services;

namespace QX3.Portal.Services.Business.AccessControl
{

    public class AccessControl
    {
        public AccessControlDAL provider;

        public AccessControl()
        {
            provider = new AccessControlDAL();
        }

        public ACProfile LoadUserProfile(string email)
        {
            LogManager.InitLog("AccessControl.LoadUserProfile");
            ACProfile profile = new ACProfile();

            try
            {
                //provider.Connect();
                profile = provider.LoadUserProfile(email);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }

            return profile;
        }

        public List<ACElement> LoadElements(bool active)
        {
            LogManager.InitLog("AccessControl.LoadElements");
            List<ACElement> elements = new List<ACElement>();

            try
            {
                //provider.Connect();
                elements = provider.LoadElements(active);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }

            return elements;
        }

        public ACProfile LoadProfile(int profileID)
        {
            LogManager.InitLog("AccessControl.LoadElements");
            ACProfile profile = new ACProfile();

            try
            {
                //provider.Connect();
                profile = provider.LoadProfile(profileID);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }

            return profile;
        }

        public int InsertProfile(ACProfile profile)
        {
            LogManager.InitLog("AccessControl.UpdateProfile");

            int profileID = 0;

            try
            {
                //provider.Connect();

                bool edit = (profile.ID > 0);

                profileID = (edit) ? provider.UpdateProfile(profile) : provider.InsertProfile(profile);

                if (profileID > 0)
                {
                    var profileObject = provider.LoadProfile(profile.Name);

                    var deleted = provider.DeleteProfileElementRelationship(profile.ID);
                    var updated = provider.UpdateOldUsersProfileToNull(profileID);

                    if (profile.Users != null && profile.Users.Count > 0)
                        foreach (ACUser user in profile.Users)
                            provider.UpdateUserProfile(profileID, user.ID);

                    if (profile.Permissions != null && profile.Permissions.Count > 0)
                        foreach (ACElement element in profile.Permissions)
                            provider.InsertProfileElementRelationship(profileObject.ID, element.ID);
                }

            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }

            return profileID;
        }

        public void LoadUserInfo(ref QX3.Portal.Contracts.DataContracts.UserInformation info)
        {
            LogManager.InitLog("AccessControl.LoadElements");

            if (info != null && !string.IsNullOrEmpty(info.Email))
            {
                try
                {
                    //provider.Connect();
                    var user = provider.LoadUser(info.Email);
                    if (user != null)
                    {
                        info.ID = user.ID;
                        info.Name = user.Name;
                        info.AssessorID = user.AssessorID;
                        info.Email = user.Email;

                        //Alteração para não buscar mais os assessores
                        info.Assessors = new List<int> { 0 };
                        info.IsBackOffice = false;
                    }
                    //if (info.ID > 0)
                    //{
                    //    ////Common.Common common = new Common.Common();
                    //    ////var fullListOfAssessors = common.GetFullListOfAssessors();
                    //    ////var fullClientListOfAssessors = provider.LoadUserListOfAssessors(info.ID);
                    //    ////info.Assessors = fullListOfAssessors == null ? fullClientListOfAssessors : fullListOfAssessors.Intersect(fullClientListOfAssessors).ToList();
                    //    ////info.IsBackOffice = info.Assessors.Contains(0);
                    //}
                }
                catch (Exception ex)
                {
                    LogManager.WriteError(ex);
                    throw new Exception(ex.Message, ex);
                }
                finally
                {
                    //provider.Close();
                }
            }
        }

        public bool UpdateElementAlias(int elementID, string newAlias)
        {
            LogManager.InitLog("AccessControl.UpdateElementAlias");

            bool changed = false;

            try
            {
                //provider.Connect();
                changed = provider.UpdateElementAlias(elementID, newAlias);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }

            return changed;
        }

        public int InsertGroupOfAssessors(ACGroupOfAssessors group)
        {
            LogManager.InitLog("AccessControl.InsertAssessorsGroup");
            int newGroupID = 0;

            try
            {
                //provider.Connect();
                group.Assessors = FormatAssessorsList(group.Assessors);

                bool edit = group.ID > 0;
                if (edit)
                {
                    provider.DeleteGroupOfAssessorsUsers(group.ID);

                    provider.RemoveAllExceptionClients(group.ID);
                }


                newGroupID = edit ? provider.UpdateGroupOfAssessors(group) : provider.InsertGroupOfAssessors(group);

                if (newGroupID > 0)
                {
                    if (!edit)
                        group.ID = newGroupID;

                    if (group.Users != null && group.Users.Count > 0)
                        foreach (ACUser user in group.Users)
                            provider.InsertUserGroupRelationship(user.ID, newGroupID);

                    if (group.ClientsIncluded != null && group.ClientsIncluded.Count > 0)
                        foreach (Client cli in group.ClientsIncluded)
                            provider.InsertClientOnGroup(group.ID, cli.ClientCode);

                    if (group.ClientsExcluded != null && group.ClientsExcluded.Count > 0)
                        foreach (Client cli in group.ClientsExcluded)
                            provider.RemoveClientFromGroup(group.ID, cli.ClientCode);
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }

            return newGroupID;
        }

        public bool InsertUserGroupRelationship(int userID, int groupID)
        {
            LogManager.InitLog("AccessControl.InsertUserGroupRelationship");
            bool ok = false;
            try
            {
                //provider.Connect();
                ok = provider.InsertUserGroupRelationship(userID, groupID);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }

            return ok;
        }

        public int InsertClientOnline(ACUser user)
        {
            int newUserID = 0;

            try
            {
                LogManager.InitLog("AccessControl.InsertClientOnline");
                newUserID = provider.InsertClientOnline(user);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(string.Concat(ex.Message, ex.StackTrace), ex);
            }

            return newUserID;
        }

        public ACUser LoadClient(string cpf)
        {
            LogManager.InitLog("AccessControl.LoadClientsOnline");
            var user = new ACUser();

            try
            {
                user = provider.LoadClient(cpf);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }

            return user;
        }

        public int InsertUser(ACUser user)
        {
            LogManager.InitLog("AccessControl.InsertUser");
            int newUserID = 0;
            try
            {
                //provider.Connect();

                bool edit = user.ID > 0;

                //if (edit)
                //    provider.DeleteUserGroupOfAssessor(user.ID);

                /*else
                {
                    if (!string.IsNullOrEmpty(user.ChaveAD) && !string.IsNullOrEmpty(user.Password))
                    {
                        Authentication.Authentication auth = new Authentication.Authentication();
						if (!auth.CreateLDAPUser(user))
						{
							provider.DeleteUser(user.ID);
							throw new Exception("Não foi possível inserir novo usuário no AD.");
						}
                    }
                }*/

                // user.Email = user.Email.ToLowerInvariant();

                newUserID = edit ? provider.UpdateUser(user) : provider.InsertUser(user);

                //if (newUserID > 0)
                //{
                //    if (user.AssessorsGroup != null && user.AssessorsGroup.Count > 0)
                //        foreach (var group in user.AssessorsGroup)
                //            provider.InsertUserGroupRelationship(newUserID, group.ID);
                //}

            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(string.Concat(ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                //provider.Close();
            }

            return newUserID;
        }

        public bool DeleteUser(int userID)
        {
            LogManager.InitLog("AccessControl.DeleteUser");

            try
            {
                //provider.Connect();

                ACUser user = provider.LoadUser(userID);
                bool deleted = provider.DeleteUser(userID);

                /*if (deleted)
                {
                    Authentication.Authentication auth = new Authentication.Authentication();
                    auth.DeleteUserLDAP(user.Email, LDAPSearchFilter.mail);
                }*/

                return deleted;
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }

        }

        public bool ValidateGroupName(string groupName, int groupID)
        {
            LogManager.InitLog("AccessControl.ValidateGroupName");
            bool valid = false;

            try
            {
                //provider.Connect();
                valid = provider.ValidateGroupName(groupName, groupID);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }

            return valid;
        }

        public bool ValidateAssessorsList(string list, int groupID)
        {
            LogManager.InitLog("AccessControl.ValidateAssessorsList");
            bool valid = false;

            try
            {
                //provider.Connect();
                string formattedList = FormatAssessorsList(list);
                valid = provider.ValidateAssessorsList(list, groupID);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }

            return valid;
        }

        public bool ValidateAssessorsAndClientsList(int groupId, string list, string includeList, string excludeList)
        {
            LogManager.InitLog("AccessControl.ValidateAssessorsList");
            bool valid = false;

            try
            {
                //provider.Connect();
                string formattedList = FormatAssessorsList(list);
                valid = provider.ValidateAssessorsAndClientsList(groupId, list, includeList, excludeList);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }

            return valid;
        }

        private string FormatAssessorsList(string list)
        {
            List<AssessorListItem> assessors = new List<AssessorListItem>();

            string formattedList = string.Empty;
            string[] items = list.Split(',');

            foreach (string item in items)
            {
                AssessorListItem listItem = new AssessorListItem();
                int assessorId = 0;

                string[] subItems = item.Split('-');
                if (subItems.Length > 1)
                    assessorId = Int32.Parse(subItems[0]);
                else
                    Int32.TryParse(item, out assessorId);

                listItem.StartNumber = assessorId;
                listItem.DisplayValue = item;
                assessors.Add(listItem);
            }

            assessors = (from a in assessors orderby a.StartNumber select a).ToList();

            for (int a = 0; a < assessors.Count; a++)
                formattedList += string.Concat(assessors[a].DisplayValue, (assessors.Count - 1) > a ? "," : "");

            return formattedList;
        }

        public List<ACUser> LoadGroupOfAssessorsFreeUsers(string nameFilter, string idsFilter)
        {
            LogManager.InitLog("AccessControl.LoadAssessorsGroupFreeUsers");
            List<ACUser> users = new List<ACUser>();

            try
            {
                //provider.Connect();
                users = provider.LoadGroupOfAssessorsFreeUsers(nameFilter, idsFilter);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }

            return users;
        }

        public List<ACUser> LoadUsers()
        {
            LogManager.InitLog("AccessControl.LoadUsers");
            List<ACUser> users = new List<ACUser>();

            try
            {
                //provider.Connect();
                users = provider.LoadUsers();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }

            return users;
        }

        public ACUser LoadUser(int id)
        {
            LogManager.InitLog("AccessControl.LoadUser");
            ACUser user = new ACUser();

            try
            {
                //provider.Connect();
                user = provider.LoadUser(id);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }

            return user;
        }

        public ACUser LoadUser(string email)
        {
            LogManager.InitLog("AccessControl.LoadUserByEmail");
            ACUser user = new ACUser();

            try
            {
                //provider.Connect();
                user = provider.LoadUser(email);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }

            return user;
        }

        public List<ACProfile> LoadProfiles()
        {
            LogManager.InitLog("AccessControl.LoadProfiles");
            List<ACProfile> profiles = new List<ACProfile>();

            try
            {
                //provider.Connect();
                profiles = provider.LoadProfiles();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }

            return profiles;
        }

        public List<ACGroupOfAssessors> LoadGroupOfAssessors(bool minimum, int groupID, string userName)
        {
            LogManager.InitLog("AccessControl.LoadGroupOfAssessors");
            List<ACGroupOfAssessors> groups = new List<ACGroupOfAssessors>();

            try
            {
                //provider.Connect();
                groups = minimum ? provider.LoadMinimumGroupsOfAssessors() : provider.LoadGroupsOfAssessors(groupID, userName);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }

            return groups;
        }

        public ACGroupOfAssessors LoadGroupOfAssessorsPerUser(long userId)
        {
            LogManager.InitLog("AccessControl.LoadGroupOfAssessors");
            ACGroupOfAssessors group = new ACGroupOfAssessors();

            try
            {
                //provider.Connect();
                group = provider.LoadGroupOfAssessorsPerUser(userId);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }

            return group;
        }

        public List<ACGroupOfAssessors> LoadGroupsOfAssessorsPerUser(long userId)
        {
            LogManager.InitLog("AccessControl.LoadGroupsOfAssessorsPerUser");
            List<ACGroupOfAssessors> group = new List<ACGroupOfAssessors>();

            try
            {
                //provider.Connect();
                group = provider.LoadGroupsOfAssessorsPerUser(userId);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }

            return group;
        }

        public bool DeleteGroupOfAssessors(int groupID)
        {
            LogManager.InitLog("AccessControl.DeleteGroupOfAssessors");

            bool changed = false;

            try
            {
                //provider.Connect();
                changed = provider.DeleteGroupOfAssessors(groupID);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }

            return changed;
        }

        public List<ACProfile> LoadProfilesAndUserCount(int profileID, string userName, bool withoutUser, List<ACElement> elements)
        {
            LogManager.InitLog("AccessControl.LoadProfilesAndUserCount");
            List<ACProfile> lstProfiles = new List<ACProfile>();
            List<ACProfile> lstResult = new List<ACProfile>();

            try
            {
                //provider.Connect();
                lstProfiles = provider.LoadProfilesAndUserCount(profileID, userName, withoutUser);

                if (elements != null && elements.Count > 0)
                {
                    foreach (ACProfile profile in lstProfiles)
                    {
                        List<ACElement> lstMatchCount = provider.LoadElementsIDByProfile(profile.ID);
                        int a = (from m in lstMatchCount where (from o in elements select o.ID).Contains(m.ID) select m).Count();

                        if (a == elements.Count)
                            lstResult.Add(profile);
                    }

                    lstProfiles = lstResult;
                }


            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }

            return lstProfiles;
        }

        public bool DeleteProfile(int profileID)
        {
            LogManager.InitLog("AccessControl.DeleteProfile");

            bool deleted = false;

            try
            {
                //provider.Connect();
                deleted = provider.DeleteProfile(profileID);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }

            return deleted;
        }

        public bool ValidateProfileName(string profileName, int profileID)
        {
            LogManager.InitLog("AccessControl.ValidateProfileName");
            bool valid = false;

            try
            {
                //provider.Connect();
                valid = provider.ValidateProfileName(profileName, profileID);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }

            return valid;
        }

        public bool ValidateProfileElements(List<ACElement> elements, int profileID)
        {
            LogManager.InitLog("AccessControl.ValidateProfileElements");
            bool valid = false;

            try
            {
                //provider.Connect();
                valid = provider.ValidateProfileElements(elements, profileID);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }

            return valid;
        }

        public List<ACUser> LoadUsersWithoutProfile()
        {
            LogManager.InitLog("AccessControl.LoadProfilesAndUserCount");
            List<ACUser> lstUsers = new List<ACUser>();

            try
            {
                //provider.Connect();
                lstUsers = provider.LoadUsersWithoutProfile();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }

            return lstUsers;
        }

        public List<Int32> LoadUserListOfAssessors(long userID)
        {
            LogManager.InitLog("AccessControl.LoadUserListOfAssessors");
            List<Int32> list = new List<Int32>();

            try
            {
                //provider.Connect();
                list = provider.LoadUserListOfAssessors(userID);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }

            return list;
        }

        public bool InsertClientOnGroup(int groupID, int clientCode)
        {
            LogManager.InitLog("AccessControl.LoadUserListOfAssessors");
            bool response = false;

            try
            {
                //provider.Connect();
                response = provider.InsertClientOnGroup(groupID, clientCode);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }

            return response;
        }

        public bool CancelClientOnGroup(int groupID, int clientCode)
        {
            LogManager.InitLog("AccessControl.LoadUserListOfAssessors");
            bool response = false;

            try
            {
                //provider.Connect();
                response = provider.CancelClientOnGroup(groupID, clientCode);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }

            return response;
        }

        public bool RemoveClientFromGroup(int groupID, int clientCode)
        {
            LogManager.InitLog("AccessControl.LoadUserListOfAssessors");
            bool response = false;

            try
            {
                //provider.Connect();
                response = provider.RemoveClientFromGroup(groupID, clientCode);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }

            return response;
        }

        public bool CancelRemovedClientFromGroup(int groupID, int clientCode)
        {
            LogManager.InitLog("AccessControl.LoadUserListOfAssessors");
            bool response = false;

            try
            {
                //provider.Connect();
                response = provider.CancelRemovedClientFromGroup(groupID, clientCode);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }

            return response;
        }

        public bool CheckIfExistsInIncludedClients(int clientCode, int groupId)
        {
            bool response = false;

            try
            {
                //provider.Connect();
                var group = new ACGroupOfAssessors() { ID = groupId };
                provider.LoadClientsIncluded(ref group);

                response = (group.ClientsIncluded == null)
                    ? false
                    : group.ClientsIncluded.Where(a => a.ClientCode == clientCode).FirstOrDefault() != null;
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }

            return response;
        }

        public bool CheckIfExistsInExcludedClients(int clientCode, int groupId)
        {
            bool response = false;

            try
            {
                //provider.Connect();
                var group = new ACGroupOfAssessors() { ID = groupId };
                provider.LoadClientsExcluded(ref group);

                response = (group.ClientsExcluded == null)
                    ? false
                    : group.ClientsExcluded.Where(a => a.ClientCode == clientCode).FirstOrDefault() != null;
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }

            return response;
        }

        public List<UserInfo> ListUsers()
        {
            List<UserInfo> list = null;
            try
            {
                list = provider.ListUsers();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
            }

            return list;
        }

        public bool UpdateUsers(List<UserInfo> users)
        {
            bool result = false;
            try
            {
                result = provider.UpdateUsers(users);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
            }

            return result;
        }

        #region Cadastro de clientes por funcionalidade

        public List<FuncionalityClient> ListFuncionalityClients(FuncionalityClientFilter filter, out int count)
        {
            List<FuncionalityClient> list = null;
            try
            {
                //provider.Connect();

                list = provider.ListFuncionalityClients(filter, out count);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                count = 0;
                throw new Exception(ex.Message, ex);

            }
            finally
            {
                //provider.Close();
            }

            return list;
        }

        public bool InsertClientForFuncionality(FuncionalityClient client)
        {
            bool result = true;
            try
            {
                //provider.Connect();

                result = provider.InsertClientForFuncionality(client);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);

            }
            finally
            {
                //provider.Close();
            }

            return result;
        }

        public bool DeleteClientForFuncionality(FuncionalityClient client)
        {
            bool result = true;
            try
            {
                //provider.Connect();

                result = provider.DeleteClientForFuncionality(client);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);

            }
            finally
            {
                //provider.Close();
            }

            return result;
        }

        public bool UpdateClientForFuncionality(FuncionalityClient client)
        {
            bool result = true;
            try
            {
                //provider.Connect();

                result = provider.UpdateClientForFuncionality(client);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);

            }
            finally
            {
                //provider.Close();
            }

            return result;
        }

        public List<Funcionality> ListFuncionalities()
        {
            List<Funcionality> list = null;
            try
            {
                //provider.Connect();

                list = provider.ListFuncionalities();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);

            }
            finally
            {
                //provider.Close();
            }

            return list;
        }

        public List<ACUser> ListClientsSINACOR(string clientes)
        {
            List<ACUser> list = null;
            try
            {
                //provider.Connect();

                list = provider.ListClientsSINACOR(clientes);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);

            }
            finally
            {
                //provider.Close();
            }

            return list;
        }

        public bool Verify_SESSION_Login(string username)
        {
            bool list = false;
            try
            {
                //provider.Connect();

                list = provider.Verify_SESSION_Login(username);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);

            }
            finally
            {
                //provider.Close();
            }

            return list;
        }

        public bool Delete_SESSION_Login(string username)
        {
            bool list = false;
            try
            {
                //provider.Connect();

                list = provider.Delete_SESSION_Login(username);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);

            }
            finally
            {
                //provider.Close();
            }

            return list;
        }

        public bool Insert_SESSION(string username, string ipClient, DateTime dt, string sessionId)
        {
            bool list = false;
            try
            {
                //provider.Connect();

                list = provider.Insert_SESSION(username, ipClient, dt, sessionId);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);

            }
            finally
            {
                //provider.Close();
            }

            return list;
        }

        public bool Verify_SESSION_IP(string ipClient)
        {
            bool list = false;
            try
            {
                //provider.Connect();

                list = provider.Verify_SESSION_IP(ipClient);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);

            }
            finally
            {
                //provider.Close();
            }

            return list;
        }

        public bool Verify_SESSION(string username, string ipClient)
        {
            bool list = false;
            try
            {
                //provider.Connect();

                list = provider.Verify_SESSION(username, ipClient);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);

            }
            finally
            {
                //provider.Close();
            }

            return list;
        }
        #endregion
    }

    public class AssessorListItem
    {
        public int StartNumber { get; set; }
        public string DisplayValue { get; set; }
    }
}
