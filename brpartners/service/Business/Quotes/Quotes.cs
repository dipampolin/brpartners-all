﻿using System;
using QX3.Spinnex.Trace.Services.Transactions;
using System.Xml;
using QX3.Spinnex.Common.Services.Logging;
using System.Collections.Generic;
using QX3.Portal.Services.Data.Log;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.Services.Data;
using QX3.Portal.Services.SolutionTechService;
using System.Configuration;
using System.Web;

namespace QX3.Portal.Services.Business.Other
{
	public class Quotes
	{
        public static string SolutionTechToken = ConfigurationManager.AppSettings["SolutionTech.Token"] ?? "";

        public List<StockPrice> LoadQuotes(string[] stocks)
        {
            LogManager.InitLog("Quotes.LoadQuotes");

            List<StockPrice> prices = new List<StockPrice>();
            string currentStock = "";
            try
            {
                lock (SolutionTechToken)
                {
                    var cache = HttpContext.Current.Cache;
                    List<string> askingSymbols = new List<string>();

                    foreach (string symbol in stocks)
                    {
                        currentStock = symbol;
                        string key = "SymbolLastPrice$" + symbol;

                        if (cache[key] != null)
                        {
                            StockPrice price = (StockPrice)cache[key];
                            prices.Add(price);
                        }
                        else
                        {
                            askingSymbols.Add(symbol);
                        }
                    }

                    using (ServiceQuoteClient client = new ServiceQuoteClient())
                    {
                        string[] arrSymbols = askingSymbols.ToArray();
                        double[] arrPrices = client.GetLastQuote(arrSymbols, SolutionTechToken);

                        for (int i = 0; i < arrSymbols.Length; i++)
                        {
                            string symbol = arrSymbols[i];
                            double price = arrPrices[i];
                            string key = "SymbolLastPrice$" + symbol;

                            currentStock = symbol;

                            StockPrice stockPrice = new StockPrice() { Price = price, Symbol = symbol };
                            prices.Add(stockPrice);

                            cache.Add(key, stockPrice, null, DateTime.MaxValue, TimeSpan.FromMinutes(1), System.Web.Caching.CacheItemPriority.Normal, null);
                        }

                    }
                }
            }
            catch (Exception ex)
            {

            }
            return prices;
        }
	}
}
