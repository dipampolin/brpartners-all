﻿using System;
using System.Collections.Generic;
using System.Xml;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.Services.Data;
using QX3.Portal.Services.Data.Log;
using QX3.Spinnex.Common.Services.Logging;
using QX3.Spinnex.Trace.Services.Transactions;

namespace QX3.Portal.Services.Business
{
    public class BrokerageDiscountMethods
    {
        private BrokerDiscountDAL provider;

        public BrokerageDiscountMethods()
		{
            provider = new BrokerDiscountDAL();
		}

        public List<BrokerageDiscount> LoadBrokerageDiscounts(BrokerageDiscount filter, int? userId)
        {
           
            LogManager.InitLog("BrokerageDiscounts.LoadBrokerageDiscounts");
            List<BrokerageDiscount> list = null;

            try
            {
                provider.Connect();
                list = provider.LoadBrokerageDiscounts(filter, userId);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return list;
        }

        public List<BrokerageDiscount> LoadBrokerageDiscountsPaginate(BrokerageDiscount filter, FilterOptions options, int? userId, out int count)
        {

            LogManager.InitLog("BrokerageDiscounts.LoadBrokerageDiscounts");
            List<BrokerageDiscount> list = null;

            try
            {
                provider.Connect();
                list = provider.LoadBrokerageDiscounts(filter, options, userId, out count);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return list;
        }

        public bool UpdateBrokerageDiscountStatus(List<BrokerageDiscount> requests)
        {
            LogManager.InitLog("BrokerageDiscounts.UpdateBrokerageDiscountStatus");

            var auxBool = true;


            provider.Connect();

            foreach (var req in requests)
            {
                var b = provider.UpdateBrokerageDiscountStatus(req);
                if (!b && auxBool)
                    auxBool = false;
            }
                
            provider.Close();
            
            return auxBool;
        }

        public bool DeleteBrokerageDiscount(List<BrokerageDiscount> requests)
        {
            LogManager.InitLog("BrokerageDiscounts.DeleteBrokerageDiscount");

            var auxBool = true;

            provider.Connect();

            foreach (var req in requests)
            {
                var b = provider.DeleteBrokerageDiscount(req);
                if (!b && auxBool)
                    auxBool = false;
            }

            provider.Close();

            return auxBool;
        }

        public bool BrokerageDiscountRequest(BrokerageDiscount request, out int requestId)
        {
            LogManager.InitLog("BrokerageDiscounts.BrokerageDiscountRequest");

            var auxBool = true;

            try
            {
                provider.Connect();

                auxBool = provider.BrokerageDiscountRequest(request, out requestId);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                auxBool = false;
                throw new Exception(ex.Message, ex);

            }
            finally {
                provider.Close();
            }            

            return auxBool;
        }

        public DateTime GetProjectedDate(DateTime? date) {
            LogManager.InitLog("BrokerageDiscounts.GetProjectedDate");

            var dateResult = DateTime.MinValue;

            try
            {
                provider.Connect();

                dateResult = provider.GetProjectedDate(date);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);

            }
            finally
            {
                provider.Close();
            }

            return dateResult;
        }

        public int GetPendentBrokerageDiscounts(BrokerageDiscount filter, int? userId)
        {
            LogManager.InitLog("BrokerageDiscounts.GetPendentBrokerageDiscounts");

            int value = 0;

            try
            {
                provider.Connect();

                value = provider.GetPendentBrokerageDiscounts(filter, userId);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);

            }
            finally
            {
                provider.Close();
            }

            return value;
        }

        public decimal GetActualDiscount(int clientCode) 
        {
            LogManager.InitLog("BrokerageDiscounts.GetPendentBrokerageDiscounts");

            decimal value = 0;

            try
            {
                provider.Connect();

                value = provider.GetActualDiscount(clientCode);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);

            }
            finally
            {
                provider.Close();
            }

            return value;
        }
    }
}
