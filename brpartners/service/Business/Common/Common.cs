﻿using System;
using QX3.Spinnex.Trace.Services.Transactions;
using System.Xml;
using QX3.Spinnex.Common.Services.Logging;
using System.Collections.Generic;
using QX3.Portal.Services.Data.Log;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.Services.Data;
using System.Configuration;
using System.Web;
using QX3.Spinnex.Common.Services.Mail;
using System.Net.Mail;
using System.IO;

namespace QX3.Portal.Services.Business.Common
{
    public class Common
    {
        public CommonDAL provider;

        public Common()
        {
            provider = new CommonDAL();
        }

        #region Proceeds

        public List<CommonType> LoadTypes(string moduleName, string specification)
        {
            LogManager.InitLog("Common.LoadTypes");
            List<CommonType> list = null;

            try
            {
                provider.Connect();
                list = provider.LoadTypes(moduleName, specification);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return list;
        }

        #endregion

        #region Pendencias

        public List<WarningList> GetWarningList(UserInformation user, string areaName, string assessors, string clients, int? userId)
        {
            LogManager.InitLog("Common.GetWarningList");
            List<WarningList> list = null;

            try
            {
                provider.Connect();
                list = provider.GetWarningList(user, areaName, assessors, clients, userId);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return list;
        }

        #endregion

        public List<int> GetFullListOfAssessors()
        {
            LogManager.InitLog("Common.GetFullListOfAssessors");
            List<int> list = null;

            try
            {
                provider.Connect();
                list = provider.GetFullListOfAssessors();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return list;
        }

        public string GetClientName(int code)
        {
            LogManager.InitLog("Common.GetClientName");

            string name = string.Empty;

            try
            {
                provider.Connect();
                name = provider.GetClientName(code);

                LogManager.WriteLog(string.Format("<in>{0}</in><out>{1}</out>", code, name));
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return name;
        }


        public RelatedPersons RelatedPersons(DateTime dtStart, DateTime dtEnd) {
            LogManager.InitLog("Common.RelatedPersons");

            RelatedPersons res = new RelatedPersons(); 

            try {
                provider.Connect();
                res = provider.RelatedPersons(dtStart, dtEnd);

                //LogManager.WriteLog(string.Format("<in>{0}</in><out>{1}</out>", code, name));
            }
            catch (Exception ex) {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally {
                provider.Close();
            }

            return res;
        }

        public MailModel GetMailModel(int funcionalityId)
        {
            LogManager.InitLog("Common.GetMailModel");

            MailModel model = null;

            try
            {
                provider.Connect();
                model = provider.GetMailModel(funcionalityId);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return model;
        }

        public List<TagModel> GetTagModel()
        {
            LogManager.InitLog("Common.GetTagModel");

            List<TagModel> model = null;

            try
            {
                provider.Connect();
                model = provider.GetTagModel();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return model;
        }

        public bool UpdateMailModel(MailModel model)
        {
            LogManager.InitLog("Common.UpdateMailModel");

            bool response = false;

            try
            {
                provider.Connect();
                response = provider.ManageMailModel(model, 'A');
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return response;
        }

        public bool InsertMailModel(MailModel model)
        {
            LogManager.InitLog("Common.InsertMailModel");

            bool response = false;

            try
            {
                provider.Connect();
                response = provider.ManageMailModel(model, 'I');
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return response;
        }

        public bool SendMailForUserWithAttachment(
            List<KeyValuePair<string, object>> variables
            , List<KeyValuePair<string, byte[]>> attachments
            , string templateFileName
            , string subject
            , string emailTo
            , string emailReplyTo
            , List<string> emailCCList
            )
        {

            variables.Add(new KeyValuePair<string, object>("strUrlSite", ConfigurationManager.AppSettings["Subscription.RequestRights.SiteUrl"]));

            emailTo = string.IsNullOrEmpty(ConfigurationManager.AppSettings["Subscription.RequestRights.EmailSendToMock"])
                ? emailTo
                : ConfigurationManager.AppSettings["Subscription.RequestRights.EmailSendToMock"];

            Template template = new Template(HttpContext.Current.Server.MapPath(templateFileName));
            template.ReplaceVars(variables.ToArray());
            string corpo = template.Body;

            MailAddress from = new MailAddress(emailReplyTo);
            MailAddress To = new MailAddress(emailTo);
            MailMessage msg = new MailMessage();

            msg.From = from;
            foreach (string s in emailTo.Split(';'))
            {
                int i = msg.To.IndexOf(new MailAddress(s));
                if ((s.Length > 0) && (i == -1))
                    msg.To.Add(s);
            }
            msg.Subject = subject;
            msg.Body = corpo;
            msg.IsBodyHtml = true;

            if (emailCCList != null)
                foreach (string cc in emailCCList)
                    msg.CC.Add(cc);

            foreach (var attach in attachments)
            {

                MemoryStream mst = new MemoryStream(attach.Value);

                msg.Attachments.Add(new Attachment(mst, attach.Key));
            }

            SmtpClient smtp = new SmtpClient();

            smtp.Send(msg);

            return true;
        }
    }
}
