﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QX3.Portal.Services.Data.BMF;
using QX3.Portal.Contracts.DataContracts;
using QX3.Spinnex.Common.Services.Logging;

namespace QX3.Portal.Services.Business
{
    public class BMFMethods
    {
        private BMFDAL provider;

        public BMFMethods()
		{
            provider = new BMFDAL();
		}

        public List<BovespaBrokerageReport> LoadBovespaBrokerageReport(BrokerageReportFilter filter)
        {
            LogManager.InitLog("BMF.LoadBovespaBrokerageTransfers");
            List<BovespaBrokerageReport> list = null;

            try
            {
                provider.Connect();
                list = provider.LoadBovespaBrokerageReport(filter);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return list;
        }

        public List<BMFBrokerageReport> LoadBMFBrokerageReport(BrokerageReportFilter filter)
        {
            LogManager.InitLog("BMF.LoadBMFBrokerageTransfers");
            List<BMFBrokerageReport> list = null;

            try
            {
                provider.Connect();
                list = provider.LoadBMFBrokerageReport(filter);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return list;
        }

        public BrokerageRange LoadBrokerageRange(BrokerageRangeFilter filter, FilterOptions filterOptions, out int count, out bool hasAnterior, out bool hasPosterior)
        {
            LogManager.InitLog("BMF.LoadBrokerageRange");
            BrokerageRange list = null;

            try
            {
                provider.Connect();
                list = provider.LoadBrokerageRange(filter, filterOptions, out count, out hasAnterior, out hasPosterior);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return list;
        }
    }
}
