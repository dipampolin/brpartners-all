﻿using System;
using System.Collections.Generic;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.Services.Data;
using QX3.Spinnex.Common.Services.Logging;
using System.ServiceModel;
using System.Linq;
using System.Diagnostics;
using QX3.Portal.Services.Business.DigitalPlatform;

namespace QX3.Portal.Services.Business
{
    public class SuitabilityMethods
    {
        private SuitabilityDAL provider;
        private DigitalPlatformMethods digitalPlatformMethods;

        public SuitabilityMethods()
        {
            provider = new SuitabilityDAL();
            digitalPlatformMethods = new DigitalPlatformMethods();
        }

        public SuitabilityForm Suitability(int client)
        {
            LogManager.InitLog("Suitability.Suitability");
            try
            {
                //provider.Connect();
                var result = provider.Suitability(client, null);
                return result;
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }
        }

        public List<SuitabilityPerfilCliente> PerfilClienteJustification(string cnpj, bool init)
        {
            LogManager.InitLog("Suitability.PerfilCliente");
            try
            {
                //provider.Connect();
                var result = provider.PerfilClienteJustification(cnpj, init);
                return result;
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }
        }

        public SuitabilityForm TestarSuitability(int idFormulario)
        {
            LogManager.InitLog("Suitability.Suitability");
            try
            {
                //provider.Connect();
                var result = provider.Suitability(0, idFormulario);
                return result;
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }
        }

        public int GravarSuitability(SuitabilityInput userInput, string login, int cdCliente, int idClienteGlobal, int status, int investidor, int? score, int formId = 0)
        {
            LogManager.InitLog("Suitability.GravarSuitability");
            int result = 0;

            try
            {
                //provider.Connect();
                //provider.BeginTransaction();
                result = provider.GravarSuitability(userInput, login, cdCliente, idClienteGlobal, status, investidor, score, formId);
                digitalPlatformMethods.SetNeedToUpdateDigitalPlatform(idClienteGlobal, true);

                //provider.Commit();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                //provider.Rollback();
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }

            return result;
        }

        public bool GravarSuitabilityJustification(int cuitabilityId, string mesage)
        {
            LogManager.InitLog("Suitability.GravarSuitabilityJustification");
            bool result = false;

            try
            {
                result = provider.GravarSuitabilityJustification(cuitabilityId, mesage);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                //provider.Rollback();
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }

            return result;

        }

        public SuitabilityClientsResponse SelecionaClientesSuitability(int? idClienteGlobal, long? cpfcnpj, string cdCliente, string status, string investidor, string desenquadrado)
        {
            LogManager.InitLog("Suitability.SelecionaClientesSuitability");
            SuitabilityClientsResponse result = new SuitabilityClientsResponse();

            try
            {
                //provider.Connect();
                result = provider.SelecionaClientesSuitability(idClienteGlobal, cpfcnpj, cdCliente, status, investidor, desenquadrado);

                // busca os nomes dos clientes banco no SQL Server

                if (result.Clients.Any())
                {

                    var clients = result.Clients.Where(c => c.CodigosBolsa == null || !c.CodigosBolsa.Any());
                    long[] documents = clients.Select(c => c.CPFCNPJ).Distinct().ToArray();

                    var corporateInfo = provider.GetCorporateInfo(documents);

                    foreach (var info in corporateInfo)
                    {
                        var infoClients = clients.Where(c => c.CPFCNPJ == info.Document);

                        Debug.Assert(infoClients.Any(), "Não encontrou o cliente relativo ao CNPJ " + info.Document);

                        foreach (var client in infoClients)
                        {
                            client.ClientName = info.Name;
                            client.CodigosBolsa = info.Codes.ToArray();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }

            return result;
        }

        public SuitabilityClientsResponse SelecionaClientesSuitability2(int? idClienteGlobal, long? cpfcnpj, string cdCliente, string status, string investidor, string desenquadrado)
        {
            LogManager.InitLog("Suitability.SelecionaClientesSuitability");
            SuitabilityClientsResponse result = new SuitabilityClientsResponse();

            try
            {
                //provider.Connect();
                result = provider.SelecionaClientesSuitability(idClienteGlobal, cpfcnpj, cdCliente, status, investidor, desenquadrado);

                // busca os nomes dos clientes banco no SQL Server

                //if (result.Clients.Any())
                //{

                //    var clients = result.Clients.Where(c => c.CodigosBolsa == null || !c.CodigosBolsa.Any());
                //    long[] documents = clients.Select(c => c.CPFCNPJ).Distinct().ToArray();


                //    var corporateInfo = provider.GetCorporateInfo(documents);

                //    foreach (var info in corporateInfo)
                //    {
                //        var infoClients = clients.Where(c => c.CPFCNPJ == info.Document);

                //        Debug.Assert(infoClients.Any(), "Não encontrou o cliente relativo ao CNPJ " + info.Document);

                //        foreach (var client in infoClients)
                //        {
                //            client.ClientName = info.Name;
                //            client.CodigosBolsa = info.Codes.ToArray();
                //        }
                //    }
                //}

            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }

            return result;
        }

        public List<SuitabilityClients> HistoricoClientesSuitability(int? idCliente, string cdCliente, string login)
        {
            LogManager.InitLog("Suitability.HistoricoClientesSuitability");
            List<SuitabilityClients> result = new List<SuitabilityClients>();

            try
            {
                //provider.Connect();
                result = provider.HistoricoClientesSuitability(idCliente, cdCliente, login);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }

            return result;
        }

        public SuitabilityAnswersResponse SelecionaResultadoSuitability(string id)
        {
            LogManager.InitLog("Suitability.SelecionaResultadoSuitability");
            SuitabilityAnswersResponse result = new SuitabilityAnswersResponse();

            try
            {
                //provider.Connect();
                result = provider.SelecionaResultadoSuitability(id);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }

            return result;
        }

        public bool AtualizaStatusSuitability(int cdCliente, int status)
        {
            LogManager.InitLog("Suitability.AtualizaStatusSuitability");
            bool result = false;

            try
            {
                //provider.Connect();
                result = provider.AtualizaStatusSuitability(cdCliente, status);
                digitalPlatformMethods.SetNeedToUpdateDigitalPlatform(cdCliente, result);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }

            return result;
        }

        public List<SuitabilityClients> HistoricosSuitability(int? idCliente, string cdCliente, string dtInicio, string dtFim, string tipo)
        {
            LogManager.InitLog("Suitability.HistoricosSuitability");
            List<SuitabilityClients> result = new List<SuitabilityClients>();

            try
            {
                //provider.Connect();
                result = provider.HistoricosSuitability(idCliente, cdCliente, dtInicio, dtFim, tipo);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }

            return result;
        }

        //public List<SuitabilityUnframedResponse> VerificaDesenquadramento(int idCliente, int idPerfil, string dtInicio, string dtFim, string tipo)
        //{
        //    LogManager.InitLog("Suitability.VerificaDesenquadramento");
        //    List<SuitabilityUnframedResponse> result = new List<SuitabilityUnframedResponse>();

        //    try
        //    {
        //        //provider.Connect();
        //        result = provider.VerificaNovoDesenquadramento(idCliente, DateTime.Parse(dtInicio), DateTime.Parse(dtFim));
        //    }
        //    catch (Exception ex)
        //    {
        //        LogManager.WriteError(ex);
        //        throw new Exception(ex.Message, ex);
        //    }
        //    finally
        //    {
        //        //provider.Close();
        //    }

        //    return result;
        //}

        public List<SuitabilityUnframedResponse> VerificaNovoDesenquadramento(int idCliente, DateTime? dtInicio, DateTime? dtFim)
        {
            try
            {
                //provider.Connect();
                return provider.VerificaNovoDesenquadramento(idCliente, dtInicio, dtFim);
            }
            finally
            {
                //provider.Close();
            }
        }

        public List<SuitabilityClients> GetCrkClients()
        {
            LogManager.InitLog("Suitability.GetCrkClients");
            List<SuitabilityClients> result = new List<SuitabilityClients>();

            try
            {
                //provider.Connect();
                result = provider.GetCrkClients();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }

            return result;
        }

        public List<StatementClient> GetClients(StatementClientFilter filter)
        {
            LogManager.InitLog("Statement.GetClients");
            List<StatementClient> result = new List<StatementClient>();

            try
            {
                //provider.Connect();
                result = provider.GetClients(filter);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }

            return result;
        }

        public List<FixedIncomeDetails> GetFixedIncome(FixedIncomeFilter filter)
        {
            LogManager.InitLog("Statement.GetFixedIncome");
            List<FixedIncomeDetails> result = new List<FixedIncomeDetails>();

            try
            {
                //provider.Connect();
                result = provider.GetFixedIncome(filter);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }

            return result;
        }

        public List<NdfDetails> GetNdf(NdfFilter filter)
        {
            LogManager.InitLog("Statement.GetNdf");
            List<NdfDetails> result = new List<NdfDetails>();

            try
            {
                //provider.Connect();
                result = provider.GetNdf(filter);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }

            return result;
        }

        public List<SwapDetails> GetSwap(SwapFilter filter)
        {
            LogManager.InitLog("Statement.GetSwap");
            List<SwapDetails> result = new List<SwapDetails>();

            try
            {
                //provider.Connect();
                result = provider.GetSwap(filter);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }

            return result;
        }

        public StatementConfiguration GetStatementConfiguration(int clientId)
        {
            LogManager.InitLog("Statement.GetStatementConfiguration");
            StatementConfiguration result = new StatementConfiguration();

            try
            {
                //provider.Connect();
                result = provider.GetStatementConfiguration(clientId);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }

            return result;
        }

        public List<StatementLog> GetStatementLog(StatementLog filter)
        {
            LogManager.InitLog("Statement.GetStatementLog");
            List<StatementLog> result = new List<StatementLog>();

            try
            {
                //provider.Connect();
                result = provider.GetStatementLog(filter);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }

            return result;
        }

        public int InsertStatementConfiguration(StatementConfiguration obj)
        {
            LogManager.InitLog("Statement.InsertStatementConfiguration");
            int result = 0;

            try
            {
                //provider.Connect();
                result = provider.InsertStatementConfiguration(obj);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }

            return result;
        }

        public int InsertStatementLog(StatementLog obj)
        {
            LogManager.InitLog("Statement.InsertStatementLog");
            int result = 0;

            try
            {
                //provider.Connect();
                result = provider.InsertStatementLog(obj);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }

            return result;
        }

        public List<SuitabilityInvestorTypeXProduct> GetInvestorTypeXProduct()
        {
            LogManager.InitLog("Statement.GetInvestorTypeXProduct");
            List<SuitabilityInvestorTypeXProduct> result = new List<SuitabilityInvestorTypeXProduct>();

            try
            {
                //provider.Connect();
                result = provider.GetInvestorTypeXProduct();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }

            return result;
        }

        public SuitabilityClients GetClient(long cpfCnpj)
        {
            LogManager.InitLog("Suitability.GetClient");
            SuitabilityClients result = new SuitabilityClients();

            try
            {
                //provider.Connect();
                result = provider.GetClient(cpfCnpj);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //provider.Close();
            }

            return result;
        }
    }

}