﻿using QX3.Portal.Contracts.DataContracts.SuitabilityAdmin;
using QX3.Portal.Services.Data.SuitabilityAdmin;
using QX3.Spinnex.Common.Services.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QX3.Portal.Services.Business
{
    public class SuitabilityAdminMethods
    {
        private SuitabilityAdminDAL provider;

        public SuitabilityAdminMethods()
        {
            provider = new SuitabilityAdminDAL();
        }

        public EdicaoSuitability GetForm(int id)
        {
            LogManager.InitLog("SuitabilityAdmin.GetForm");
            EdicaoSuitability result = new EdicaoSuitability();

            try
            {
                result = provider.AbrirFormulario(id);
            }
            catch (Exception ex)
            {
            }

            return result;
        }

        public List<FormularioSuitability> GetForms(string personType, string status)
        {
            LogManager.InitLog("SuitabilityAdmin.GetForms");
            List<FormularioSuitability> result = new List<FormularioSuitability>();

            try
            {
                result = provider.ListarFormularios(personType, status);
            }
            catch (Exception ex)
            {
            }

            return result;
        }
    }
}
