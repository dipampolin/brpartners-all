﻿using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.Services.Data;
using QX3.Portal.Services.Data.Parameters;
using QX3.Spinnex.Common.Services.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace QX3.Portal.Services.Business
{
    public class ClientRegistrationMethods
    {
        private ClientRegistrationDAL provider;

        public ClientRegistrationMethods()
        {
            provider = new ClientRegistrationDAL();
        }

        public List<Client> LoadActiveAndInactiveClients(ClientFilter filter, FilterOptions filterOptions, int? userId, out int count)
        {
            LogManager.InitLog("ClientRegistration.LoadActiveAndInactiveClients");
            List<Client> list = null;

            try
            {
                provider.Connect();
                list = provider.LoadActiveAndInactiveClients(filter, filterOptions, userId, out count);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return list;
        }

        public List<OverdueAndDue> LoadOverdueAndDueRegistration(OverdueAndDueFilter filter, FilterOptions filterOptions, int? userId, out int count)
        {
            LogManager.InitLog("ClientRegistration.LoadOverdueAndDueRegistration");
            List<OverdueAndDue> list = null;

            try
            {
                provider.Connect();
                list = provider.LoadOverdueAndDueRegistration(filter, filterOptions, userId, out count);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return list;
        }

        public List<TransferBovespaItem> LoadTransfersBovespa(TransferBovespaFilter filter, FilterOptions options, int? userId, out int count)
        {
            LogManager.InitLog("Transfer.LoadTransfersBovespa");
            List<TransferBovespaItem> list = null;

            try
            {
                provider.Connect();
                list = provider.LoadTransfersBovespa(filter, options, userId, out count);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return list;
        }

        public List<TransferBMFItem> LoadTransfersBMF(TransferBMFFilter filter, FilterOptions options, out int count)
        {
            LogManager.InitLog("Transfer.LoadTransfersBMF");
            List<TransferBMFItem> list = null;

            try
            {
                provider.Connect();
                list = provider.LoadTransfersBMF(filter, options, out count);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return list;
        }

        public bool InsertClientBond(TransferBMFItem transfer)
        {
            LogManager.InitLog("ClientRegistration.InsertClientBond");
            bool result;

            try
            {
                provider.Connect();
                result = provider.InsertClientBond(transfer);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return result;
        }

        public Client CheckClientBMF(int clientCode)
        {
            LogManager.InitLog("ClientRegistration.CheckClientBMF");
            Client result;

            try
            {
                provider.Connect();
                result = provider.CheckClientBMF(clientCode);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return result;
        }

        #region Cadastro
        public bool CheckClientPerCPFCGC(string cpfcgc, out int clientId)
        {
            LogManager.InitLog("ClientRegistration.CheckClientPerCPFCGC");
            bool result;

            try
            {
                provider.Connect();
                result = provider.CheckClientPerCPFCGC(cpfcgc, out clientId);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return result;
        }

        public string CheckClientType(string clientCodes, string assessors, int? userId)
        {
            LogManager.InitLog("ClientRegistration.CheckClientType");
            string result;

            try
            {
                provider.Connect();
                result = provider.CheckClientType(clientCodes, assessors, userId);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return result;
        }

        public IndividualPerson LoadIndividualPerson(int clientCode)
        {
            LogManager.InitLog("ClientRegistration.LoadIndividualPerson");
            IndividualPerson result;

            try
            {
                provider.Connect();
                result = provider.LoadIndividualPerson(clientCode);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return result;
        }

        public List<BankAccount> LoadBanks(string bankName)
        {
            LogManager.InitLog("ClientRegistration.LoadBanks");
            List<BankAccount> list = null;

            try
            {
                provider.Connect();
                list = provider.LoadBanks(bankName);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return list;
        }

        public ClientOnline LoadClientOnline(string idClient)
        {
            LogManager.InitLog("ClientRegistration.LoadClientOnline");
            var client = new ClientOnline();

            try
            {
                client = provider.LoadClientOnline(idClient);

                if(client.Id > 0 && client.Cpf.Length < 11)
                    client.Cpf = "0" + client.Cpf;
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {

            }

            return client;
        }

        public ClientOnlineFinancial LoadClientFinancial(int idClientOnline)
        {
            LogManager.InitLog("ClientRegistration.LoadClientOnline");
            var client = new ClientOnlineFinancial();

            try
            {
                client = provider.LoadClientFinancial(idClientOnline);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }

            return client;
        }

        public ClientOnlineRepresentative LoadRepresentative(int idClientOnline, int id)
        {
            LogManager.InitLog("ClientRegistration.LoadRepresentative");

            try
            {
                return provider.LoadClientRepresentative(idClientOnline, id);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
        }

        public Representatives LoadRepresentatives(int idClientOnline)
        {
            LogManager.InitLog("ClientRegistration.LoadRepresentatives");

            try
            {
                return provider.LoadRepresentatives(idClientOnline);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
        }

        public ClientOnlineDocuments LoadClientDocument(int idClientOnline)
        {
            LogManager.InitLog("ClientRegistration.LoadDocuments");

            try
            {
                return provider.LoadClientDocument(idClientOnline, "PESSOA_FISICA");
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
        }

        public ClientOnlineRules LoadDocumentsRepresentatives(int idDocument)
        {
            LogManager.InitLog("ClientRegistration.LoadDocumentsRepresentatives");

            try
            {
                return provider.LoadDocumentsRepresentatives(idDocument);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
        }

        public ClientsOnlineStatus LoadClientsInFilling()
        {
            LogManager.InitLog("ClientRegistration.LoadClientsInFilling");

            try
            {
                return provider.LoadClientByStatus(1);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
        }

        public ClientsOnlineStatus LoadClientsInAnalysis()
        {
            LogManager.InitLog("ClientRegistration.LoadClientsInAnalysis");

            try
            {
                return provider.LoadClientByStatus(2);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
        }

        public ClientsOnlineStatus LoadClientsApproved()
        {
            LogManager.InitLog("ClientRegistration.LoadClientsApproved");

            try
            {
                return provider.LoadClientByStatus(3);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
        }

        public ClientsOnlineStatus LoadClientsDisapproved()
        {
            LogManager.InitLog("ClientRegistration.LoadClientsDisapproved");

            try
            {
                return provider.LoadClientByStatus(4);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
        }

        public Documents LoadDocument()
        {
            LogManager.InitLog("ClientRegistration.LoadDocument");

            try
            {
                return provider.LoadDocument();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
        }

        public Document LoadDocumentById(int idDocument)
        {
            LogManager.InitLog("ClientRegistration.LoadDocumentById");

            try
            {
                return provider.LoadDocumentById(idDocument);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
        }

        public int UpsertDocument(Document document)
        {
            LogManager.InitLog("ClientRegistration.UpsertDocument");

            try
            {
                var parameter = new ParametersDAL();
                var validation = parameter.GetExpirationDateDocument();
                var info = validation.FirstOrDefault(x => x.Valor == Convert.ToString(document.IdValidation));

                if(info != null)
                    document.ExpirationDate = DateTime.Now.AddMonths(Convert.ToInt32(info.Descricao));

                if(document.Data == null)
                {
                    var result = provider.LoadDocumentById(document.Id);
                    document.Data = result.Data;
                    document.ContentType = result.ContentType;
                    document.DescriptionFile = result.DescriptionFile;
                }

                document.Status = 1;
                return provider.UpsertDocument(document);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
        }

        public int RemoveDocument(int idDocument)
        {
            LogManager.InitLog("ClientRegistration.RemoveDocument");

            try
            {
                var document = provider.LoadDocumentById(idDocument);
                document.Status = 2;
                return provider.UpsertDocument(document);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
        }

        public int InsertUpdateRegistrationClient(ClientOnline client)
        {
            LogManager.InitLog("ClientRegistration.InsertUpdateRegistrationClient");
            var response = 0;

            try
            {
                response = provider.UpsertRegistrationClient(client);
                provider.UpsertFatca(client);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }

            return response;
        }

        public int UpsertDocumentRepresentative(ClientOnlineRules clientOnlineRules)
        {
            LogManager.InitLog("ClientRegistration.UpsertDocumentRepresentative");

            try
            {
                return provider.UpsertDocumentRepresentative(clientOnlineRules);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
        }

        public int SubmitClientOnline(int idClientOnline)
        {
            LogManager.InitLog("ClientRegistration.UpdateStatusSubmitRegistration");

            try
            {
                return provider.UpdateStatusSubmitRegistration(idClientOnline, 2);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
        }

        public int ApproveClientOnline(int idClientOnline)
        {
            LogManager.InitLog("ClientRegistration.UpdateStatusSubmitRegistration");

            try
            {
                return provider.UpdateStatusSubmitRegistration(idClientOnline, 3);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
        }

        public int DisapproveClientOnline(int idClientOnline)
        {
            LogManager.InitLog("ClientRegistration.DisapproveClientOnline");

            try
            {
                return provider.UpdateStatusSubmitRegistration(idClientOnline, 4);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
        }

        public int InsertUpdateClientFinancial(ClientOnlineFinancial clientOnlineFinancial)
        {
            LogManager.InitLog("ClientRegistration.InsertClientFinancial");
            var response = 0;

            try
            {
                response = provider.UpsertClientFinancial(clientOnlineFinancial);
                provider.InsertBankAccount(clientOnlineFinancial);
                provider.InsertBankProduct(clientOnlineFinancial);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }

            return response;
        }

        public int UpsertDocumentClientOnline(ClientOnlineDocuments documents)
        {
            LogManager.InitLog("ClientRegistration.UpsertDocumentClientOnline");

            try
            {
                provider.UpsertDocumentClientOnline(documents);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }

            return 1;
        }

        public bool DeleteAssignById(int idFile)
        {
            LogManager.InitLog("ClientRegistration.DeleteAssign");

            try
            {
                return provider.DeleteAssign(idFile);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
        }

        public bool DeleteRepresentative(int idClientOnline, int idRepresentative)
        {
            LogManager.InitLog("ClientRegistration.DeleteRepresentative");

            try
            {
                return provider.DeleteRepresentative(idClientOnline, idRepresentative);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
        }

        public ClientAssign GetAssignById(int id)
        {
            LogManager.InitLog("ClientRegistration.InsertClientFinancial");

            try
            {
                return provider.GetAssignById(id);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
        }

        public ClientDocument GetModelDocumentById(int id)
        {
            LogManager.InitLog("ClientRegistration.GetModelDocumentById");

            try
            {
                return provider.GetModelDocumentById(id);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
        }

        public ClientDocument GetDocumentById(int id)
        {
            LogManager.InitLog("ClientRegistration.GetDocumentById");

            try
            {
                return provider.GetDocumentById(id);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
        }

        public int UpsertRepresentative(ClientOnlineRepresentative representative)
        {
            LogManager.InitLog("ClientRegistration.InsertClientFinancial");
            var response = 0;

            try
            {
                response = provider.UpsertRepresentative(representative);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }

            return response;
        }

        #endregion
    }
}
