﻿using System;
using System.Xml.Linq;
using QX3.Spinnex.Trace.Services.Transactions;
using System.Xml;
using QX3.Spinnex.Common.Services.Logging;
using System.Collections.Generic;
using QX3.Portal.Services.Data.Log;
using QX3.Portal.Contracts.DataContracts;

namespace QX3.Portal.Services.Business.Log
{
	public class Log
	{
		public LogDAL provider;

		public Log()
		{
			provider = new LogDAL();
		}

		public string GetTraceDetails(Guid transactionID)
		{
			TraceTransactions trans = new TraceTransactions();
			var document = trans.LoadTraceData(transactionID);
			return document.ToString();
		}

		public void CreateTraceRecord(string data)
		{
			TraceTransactions trans = new TraceTransactions();
			XmlDocument xmlData = new XmlDocument();
			xmlData.LoadXml(data);
			trans.CreateTransaction(xmlData);
		}

		public List<HistoryData> LoadHistory(HistoryFilter filter)
		{
			LogManager.InitLog("Log.LoadHistory");
			List<HistoryData> list = null;

			try
			{
				//provider.Connect();
				list = provider.LoadHistory(filter);
			}
			catch (Exception ex)
			{
				LogManager.WriteError(ex);
				throw new Exception(ex.Message, ex);
			}
			finally
			{
				//provider.Close();
			}

			return list;
		}

		public Boolean InsertHistory(HistoryLog log)
		{
			LogManager.InitLog("Log.InsertHistory");
			bool inserted = false;

			try
			{
				//provider.Connect();
				inserted = provider.InsertHistory(log);
			}
			catch (Exception ex)
			{
				LogManager.WriteError(ex);
				throw new Exception(ex.Message, ex);
			}
			finally
			{
				//provider.Close();
			}

			return inserted;
		}
	}
}
