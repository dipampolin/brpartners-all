﻿using System;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.Services.Data;
using QX3.Spinnex.Common.Services.Logging;

namespace QX3.Portal.Services.Business
{
    public class GuaranteeMethods
    {
        private GuaranteeDAL provider;

        public GuaranteeMethods()
		{
            provider = new GuaranteeDAL();
		}

		public GuaranteeClient ListGuarantees(GuaranteeClientFilter filter, int? userId, out int count, out int pos)
		{
			LogManager.InitLog("Guarantee.ListGuarantees");
			GuaranteeClient result = null;

			try
			{
				provider.Connect();
				result = provider.ListGuarantees(filter, userId, out count, out pos);
			}
			catch (Exception ex)
			{
				LogManager.WriteError(ex);
				throw new Exception(ex.Message, ex);
			}
			finally
			{
				provider.Close();
			}

			return result;
		}
    }
}
