﻿using QX3.Portal.Services.Data.Parameters;
using System;
using System.Collections.Generic;

namespace QX3.Portal.Services.Business.Parameters
{
    public class ParametersMethod
    {
        public ParametersDAL provider;

        public ParametersMethod()
        {
            provider = new ParametersDAL();
        }

        public List<Contracts.DataContracts.Parameters> GetState()
        {
            var data = new List<Contracts.DataContracts.Parameters>();

            try
            {
                data = provider.GetStates();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }

            return data;
        }

        public List<Contracts.DataContracts.Parameters> GetCountries()
        {
            var data = new List<Contracts.DataContracts.Parameters>();

            try
            {
                data = provider.GetCountries();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }

            return data;
        }

        public List<Contracts.DataContracts.Parameters> GetAddressTypes()
        {
            var data = new List<Contracts.DataContracts.Parameters>();

            try
            {
                data = provider.GetAddressTypes();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }

            return data;
        }

        public List<Contracts.DataContracts.Parameters> GetFinalities()
        {
            var data = new List<Contracts.DataContracts.Parameters>();

            try
            {
                data = provider.GetFinalities();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }

            return data;
        }

        public List<Contracts.DataContracts.Parameters> GetActivities()
        {
            var data = new List<Contracts.DataContracts.Parameters>();

            try
            {
                data = provider.GetActivities();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }

            return data;
        }

        public List<Contracts.DataContracts.Parameters> GetOccupations()
        {
            var data = new List<Contracts.DataContracts.Parameters>();

            try
            {
                data = provider.GetOccupation();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }

            return data;
        }

        public List<Contracts.DataContracts.Parameters> GetSizes()
        {
            var data = new List<Contracts.DataContracts.Parameters>();

            try
            {
                data = provider.GetSize();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }

            return data;
        }

        public List<Contracts.DataContracts.Parameters> GetProfessions()
        {
            var data = new List<Contracts.DataContracts.Parameters>();

            try
            {
                data = provider.GetProfession();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }

            return data;
        }

        public List<Contracts.DataContracts.Parameters> GetMaritalStatus()
        {
            var data = new List<Contracts.DataContracts.Parameters>();

            try
            {
                data = provider.GetMaritalStatus();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }

            return data;
        }

        public List<Contracts.DataContracts.Parameters> GetSex()
        {
            var data = new List<Contracts.DataContracts.Parameters>();

            try
            {
                data = provider.GetSex();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }

            return data;
        }

        public List<Contracts.DataContracts.Parameters> GetStatus()
        {
            var data = new List<Contracts.DataContracts.Parameters>();

            try
            {
                data = provider.GetStatus();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }

            return data;
        }

        public List<Contracts.DataContracts.Parameters> GetDocumentTypes()
        {
            var data = new List<Contracts.DataContracts.Parameters>();

            try
            {
                data = provider.GetDocumentTypes();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }

            return data;
        }

        public List<Contracts.DataContracts.Parameters> GetEconomicGroups()
        {
            var data = new List<Contracts.DataContracts.Parameters>();

            try
            {
                data = provider.GetEconomicGroup();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }

            return data;
        }

        public List<Contracts.DataContracts.Parameters> GetClassifications()
        {
            var data = new List<Contracts.DataContracts.Parameters>();

            try
            {
                data = provider.GetClassification();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }

            return data;
        }

        public List<Contracts.DataContracts.Parameters> GetEconomicActivities()
        {
            var data = new List<Contracts.DataContracts.Parameters>();

            try
            {
                data = provider.GetEconomicActivities();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }

            return data;
        }

        public List<Contracts.DataContracts.Parameters> GetRegisterType()
        {
            var data = new List<Contracts.DataContracts.Parameters>();

            try
            {
                data = provider.GetRegistrationTypes();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }

            return data;
        }

        public List<Contracts.DataContracts.Parameters> GetClientTypes()
        {
            var data = new List<Contracts.DataContracts.Parameters>();

            try
            {
                data = provider.GetClientTypes();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }

            return data;
        }

        public List<Contracts.DataContracts.Parameters> GetCategories()
        {
            var data = new List<Contracts.DataContracts.Parameters>();

            try
            {
                data = provider.GetCategories();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }

            return data;
        }

        public List<Contracts.DataContracts.Parameters> GetManagers()
        {
            var data = new List<Contracts.DataContracts.Parameters>();

            try
            {
                data = provider.GetManagers();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }

            return data;
        }

        public List<Contracts.DataContracts.Parameters> GetNaturalness()
        {
            var data = new List<Contracts.DataContracts.Parameters>();

            try
            {
                data = provider.GetNaturalness();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }

            return data;
        }

        public List<Contracts.DataContracts.Parameters> GetNacionality()
        {
            var data = new List<Contracts.DataContracts.Parameters>();

            try
            {
                data = provider.GetNacionalities();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }

            return data;
        }

        public List<Contracts.DataContracts.Parameters> GetTelephoneTypes()
        {
            var data = new List<Contracts.DataContracts.Parameters>();

            try
            {
                data = provider.GetTelephoneTypes();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }

            return data;
        }

        public List<Contracts.DataContracts.Parameters> GetMatrimonialRegime()
        {
            var data = new List<Contracts.DataContracts.Parameters>();

            try
            {
                data = provider.GetMatrimonialRegime();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }

            return data;
        }

        public List<Contracts.DataContracts.Parameters> GetChoice()
        {
            var data = new List<Contracts.DataContracts.Parameters>();

            try
            {
                data = provider.GetChoice();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }

            return data;
        }


        // ************************* DADOS FINANCEIROS *********************************

        public List<Contracts.DataContracts.Parameters> GetFinancialProduct()
        {
            var data = new List<Contracts.DataContracts.Parameters>();

            try
            {
                data = provider.GetFinancialProduct();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }

            return data;
        }

        public List<Contracts.DataContracts.Parameters> GetFiscalNature()
        {
            var data = new List<Contracts.DataContracts.Parameters>();

            try
            {
                data = provider.GetFiscalNature();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }

            return data;
        }

        public List<Contracts.DataContracts.Parameters> GetAccountFinality()
        {
            var data = new List<Contracts.DataContracts.Parameters>();

            try
            {
                data = provider.GetAccountFinality();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }

            return data;
        }

        public List<Contracts.DataContracts.Parameters> GetAccountUtility()
        {
            var data = new List<Contracts.DataContracts.Parameters>();

            try
            {
                data = provider.GetAccountUtility();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }

            return data;
        }

        public List<Contracts.DataContracts.Parameters> GetAccountType()
        {
            var data = new List<Contracts.DataContracts.Parameters>();

            try
            {
                data = provider.GetAccountType();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }

            return data;
        }

        public List<Contracts.DataContracts.Parameters> GetAccountTypeBank()
        {
            var data = new List<Contracts.DataContracts.Parameters>();

            try
            {
                data = provider.GetAccountTypeBank();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }

            return data;
        }

        public List<Contracts.DataContracts.Parameters> GetBank()
        {
            var data = new List<Contracts.DataContracts.Parameters>();

            try
            {
                data = provider.GetBank();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }

            return data;
        }

        public List<Contracts.DataContracts.Parameters> GetRepresentationStyle()
        {
            var data = new List<Contracts.DataContracts.Parameters>();

            try
            {
                data = provider.GetRepresentationStyle();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }

            return data;
        }

        public List<Contracts.DataContracts.Parameters> GetRepresentationProfile()
        {
            var data = new List<Contracts.DataContracts.Parameters>();

            try
            {
                data = provider.GetRepresentationProfile();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }

            return data;
        }

        public List<Contracts.DataContracts.Parameters> GetQualifications()
        {
            var data = new List<Contracts.DataContracts.Parameters>();

            try
            {
                data = provider.GetQualifications();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }

            return data;
        }

        public List<Contracts.DataContracts.Parameters> GetRulesTypes()
        {
            var data = new List<Contracts.DataContracts.Parameters>();

            try
            {
                data = provider.GetRulesTypes();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }

            return data;
        }

        public List<Contracts.DataContracts.Parameters> GetExpirationDateDocument()
        {
            var data = new List<Contracts.DataContracts.Parameters>();

            try
            {
                data = provider.GetExpirationDateDocument();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }

            return data;
        }
    }
}
