﻿using System;
using System.Collections.Generic;
using System.Xml;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.Services.Data;
using QX3.Portal.Services.Data.Log;
using QX3.Spinnex.Common.Services.Logging;
using QX3.Spinnex.Trace.Services.Transactions;
using System.Web;
using System.Configuration;
using QX3.Spinnex.Common.Services.Mail;


namespace QX3.Portal.Services.Business
{
    public class SubscriptionMethods
    {
        private SubscriptionDAL provider;

        public SubscriptionMethods() {
            provider = new SubscriptionDAL();
        }

        #region Subscrições
        public List<Subscription> LoadSubscriptions(Subscription filter, FilterOptions options, bool getProspects, out int count)
        {

            LogManager.InitLog("Subscription.LoadSubscriptions");
            List<Subscription> list = null;

            try
            {
                provider.Connect();
                list = provider.LoadSubscriptions(filter, options, getProspects, out count);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return list;
        }

        public bool SubscriptionRequest(Subscription request, out int requestId)
        {
            LogManager.InitLog("Subscription.SubscriptionRequest");
            bool response = false;
            
            try
            {
                provider.Connect();
                
                response = provider.ManageSubscription(request, 'I', out requestId);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return response;
        }

        public bool SubscriptionUpdate(Subscription request, out int requestId)
        {
            LogManager.InitLog("Subscription.SubscriptionUpdate");
            bool response = false;

            try
            {
                provider.Connect();
                response = provider.ManageSubscription(request, 'A', out requestId);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return response;
        }

        public bool SubscriptionDelete(Subscription request, out int requestId)
        {
            LogManager.InitLog("Subscription.SubscriptionDelete");
            bool response = false;

            try
            {
                provider.Connect();
                response = provider.ManageSubscription(request, 'E', out requestId);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return response;
        }

        public bool SubscriptionUpdateStatus(Subscription request, out int requestId)
        {
            return this.SubscriptionUpdate(request, out requestId);
        }

        #endregion

        #region Direitos
        
		public List<SubscriptionRights> LoadSubscriptionRights(SubscriptionRights filter, FilterOptions options, out int count, int? userId) 
        {
            LogManager.InitLog("Subscription.LoadSubscriptionRights");
            List<SubscriptionRights> list = null;

            try
            {
                provider.Connect();
                list = provider.LoadSubscriptionRights(filter, options, out count, userId);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return list;
        }

        public bool SubscriptionRightsRequest(SubscriptionRights request)
        {
            LogManager.InitLog("Subscription.SubscriptionRightsRequest");
            bool response = false;

            try
            {
                provider.Connect();
                response = provider.ManageSubscriptionRights(request, 'I');
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return response;
        }

        public bool SubscriptionRightsUpdate(SubscriptionRights request)
        {
            LogManager.InitLog("Subscription.SubscriptionRightsUpdate");
            bool response = false;

            try
            {
                provider.Connect();
                response = provider.ManageSubscriptionRights(request, 'A');
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return response;
        }

        public bool SubscriptionRightsDelete(SubscriptionRights request)
        {
            LogManager.InitLog("Subscription.SubscriptionRightsDelete");
            bool response = false;

            try
            {
                provider.Connect();
                response = provider.ManageSubscriptionRights(request, 'E');
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return response;
        }

        public long GetRequestedQuantity(SubscriptionRights request) 
        {
            LogManager.InitLog("Subscription.GetRequestedQuantity");
            long response = 0;

            try
            {
                provider.Connect();
                response = provider.GetRequestedQuantity(request);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return response;
        }

        public long GetRightQuantity(SubscriptionRights request)
        {
            LogManager.InitLog("Subscription.GetRightQuantity");
            long response = 0;

            try
            {
                provider.Connect();
                response = provider.GetRightQuantity(request);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return response;
        }

        public bool RequestRightsNotify(SubscriptionRights request, Subscription subscription)
        {
            LogManager.InitLog("Subscription.SubscriptionRightsNotify");
            bool response = false;

            try
            {
                // Send E-mail
                List<KeyValuePair<string, object>> messageValues = new List<KeyValuePair<string, object>>();
                messageValues.Add(new KeyValuePair<string, object>("strUrlSite", ConfigurationManager.AppSettings["Subscription.RequestRights.SiteUrl"]));
                messageValues.Add(new KeyValuePair<string, object>("strCompany", subscription.Company));
                messageValues.Add(new KeyValuePair<string, object>("strStockCode", subscription.StockCode));
                messageValues.Add(new KeyValuePair<string, object>("strInitialDate", subscription.InitialDate.Value.ToShortDateString()));
                messageValues.Add(new KeyValuePair<string, object>("strBrokerDate", subscription.BrokerDate.Value.ToShortDateString()));
                messageValues.Add(new KeyValuePair<string, object>("strRightsQuantity", request.RightsQtty.Value.ToString("N0")));
                messageValues.Add(new KeyValuePair<string, object>("strRightsValue", request.RightValue.Value.ToString("N2")));
                messageValues.Add(new KeyValuePair<string, object>("strBodyMail", HttpUtility.HtmlEncode(subscription.BodyMail)));

                string subject = subscription.SubjectMail;
                string emailTo = string.IsNullOrEmpty(ConfigurationManager.AppSettings["Subscription.RequestRights.EmailSendToMock"]) ? String.Format("{0}<{1}>", request.ClientName, request.ClientEmail) : ConfigurationManager.AppSettings["Subscription.RequestRights.EmailSendToMock"];
                string emailReplyTo = ConfigurationManager.AppSettings["Subscription.RequestRights.EmailReplyTo"];

                SendMail.SendWithFrom(emailTo, subject, HttpContext.Current.Server.MapPath("~/Template/RightRequestsNotification.html"), emailReplyTo, messageValues.ToArray());

                // Update Notification Status
                provider.Connect();
                response = provider.ManageSubscriptionRights(request, 'A');
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally {
                provider.Close();
            }
            

            return response;
        }

        public List<SubscriptionNotification> LoadNotifications(int subscriptionId) 
        {
            LogManager.InitLog("Subscription.LoadNotifications");
            List<SubscriptionNotification> response;

            try
            {
                provider.Connect();
                response = provider.LoadNotifications(subscriptionId);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return response;
        }

        #endregion
    }
}
