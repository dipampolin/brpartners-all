﻿using System;
using System.Collections.Generic;
using QX3.Portal.Services.Data;
using QX3.Spinnex.Common.Services.Logging;
using QX3.Portal.Contracts.DataContracts;

namespace QX3.Portal.Services.Business.Term
{
    public class Term
    {
        public TermDAL provider;

        public Term()
        {
            provider = new TermDAL();
        }

        public bool SettleTerm(int termID)
        {
            LogManager.InitLog("Term.SettleTerm");

            try
            {
                provider.Connect();
                return provider.SettleTerm(termID);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw ex;
            }
            finally
            {
                provider.Close();
            }
        }

        public bool ExcludeTerm(int termID)
        {
            LogManager.InitLog("Term.ExcludeTerm");

            try
            {
                provider.Connect();
                return provider.ExcludeTerm(termID);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw ex;
            }
            finally
            {
                provider.Close();
            }
        }

        public List<TermItem> LoadTerms(TermFilter filter, int? userId)
        {
            LogManager.InitLog("Other.LoadTerms");
            List<TermItem> list = null;

            try
            {
                provider.Connect();
                list = provider.LoadTerms(filter, userId);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return list;
        }

        public List<TermItem> LoadTerms(string lstContracts)
        {
            LogManager.InitLog("Other.LoadLotTerms");
            List<TermItem> list = null;

            try
            {
                provider.Connect();
                list = provider.LoadTerms(lstContracts);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return list;
        }

        public TermItem LoadTerm(int termId)
        {
            LogManager.InitLog("Other.LoadTerm");
            TermItem item = new TermItem();

            try
            {
                provider.Connect();
                item = provider.LoadTerm(termId);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return item;
        }

        public List<TermItem> LoadClientTerms(TermFilter filter, int? userId)
        {
            LogManager.InitLog("Term.LoadClientTerms");
            List<TermItem> list = null;

            try
            {
                provider.Connect();
                list = provider.LoadClientTerms(filter, userId);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return list;
        }

        public List<TermItem> InsertTerms(List<TermItem> terms)
        {
            LogManager.InitLog("Term.InsertTerms");
            List<TermItem> list = new List<TermItem>();

            try
            {
                provider.Connect();
                list = provider.InsertTerms(terms);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }
            return list;
        }

        public bool ChangeQuantityToSettle(int termID, Int64 quantity)
        {
            LogManager.InitLog("Term.ChangeQuantityToSettle");

            try
            {
                provider.Connect();
                return provider.ChangeQuantityToSettle(termID, quantity);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw ex;
            }
            finally
            {
                provider.Close();
            }
        }

        public int GetNumberOfPending(string assessorsList)
        {
            LogManager.InitLog("Term.GetNumberOfPending");
            int pending = 0;

            try
            {
                provider.Connect();
                pending = provider.GetNumberOfPending(assessorsList);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return pending;
        }

    }
}
