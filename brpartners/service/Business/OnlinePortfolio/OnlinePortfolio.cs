﻿using System;
using System.Collections.Generic;
using QX3.Portal.Services.Data;
using QX3.Spinnex.Common.Services.Logging;
using QX3.Portal.Contracts.DataContracts;
using System.Configuration;
using System.Net.Mail;
using QX3.Spinnex.Common.Services.Mail;
using System.Web;
using System.IO;

namespace QX3.Portal.Services.Business.OnlinePortfolio
{
    public class OnlinePortfolio
    {
        public OnlinePortfolioDAL provider;

        public OnlinePortfolio()
        {
            provider = new OnlinePortfolioDAL();
        }

        public bool InsertContact(int clientCode, PortfolioContactItem contact)
        {
            LogManager.InitLog("OnlinePortfolio.InsertContact");

            try
            {
                provider.Connect();
                return provider.InsertContact(clientCode, contact);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }
        }

        public PortfolioContact LoadClientContacts(int clientCode)
        {
            LogManager.InitLog("OnlinePortfolio.LoadClientContacts");
            PortfolioContact clientContacts = new PortfolioContact();

            try
            {
                provider.Connect();
                clientContacts = provider.LoadClientContacts(clientCode);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return clientContacts;
        }

        public List<PortfolioOperator> LoadPortfolioOperators(string code)
        {
            LogManager.InitLog("OnlinePortfolio.LoadPortfolioOperators");
            List<PortfolioOperator> operators = new List<PortfolioOperator>();

            try
            {
                provider.Connect();
                operators = provider.LoadPortfolioOperators(code);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return operators;
        }

        public List<PortfolioOperator> LoadOperators(string code)
        {
            LogManager.InitLog("OnlinePortfolio.LoadOperators");
            List<PortfolioOperator> operators = new List<PortfolioOperator>();

            try
            {
                provider.Connect();
                operators = provider.LoadOperators(code);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return operators;
        }

        public List<CommonType> SearchClients(string nameFilter, long operatorId, int maxItems)
        {
            LogManager.InitLog("OnlinePortfolio.LoadClientContacts");
            List<CommonType> clients = new List<CommonType>();

            try
            {
                provider.Connect();
                clients = provider.SearchClients(nameFilter, operatorId, maxItems);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return clients;
        }

        public PortfolioClient LoadClientName(int clientId)
        {
            LogManager.InitLog("OnlinePortfolio.LoadClientName");

            PortfolioClient operators = new PortfolioClient();

            try
            {
                provider.Connect();
                operators = provider.LoadClientName(clientId);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return operators;
        }

        public bool UpdateObservation(int clientId, string observation)
        {
            LogManager.InitLog("OnlinePortfolio.InsertObservation");
            bool inserted = false;

            try
            {
                provider.Connect();
                inserted = provider.UpdateObservation(clientId, observation);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return inserted;
        }

        public string LoadObservation(int clientId)
        {
            LogManager.InitLog("OnlinePortfolio.LoadObservation");
            

            try
            {
                provider.Connect();
                return provider.LoadObservation(clientId);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

        }

        public List<int> LoadOperatorClients(int operatorID, int filter)
        {
            LogManager.InitLog("OnlinePortfolio.LoadOperatorClients");
            List<int> clients = new List<int>();

            try
            {
                provider.Connect();
                clients = provider.LoadOperatorClients(operatorID, filter);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return clients;
        }

        public bool InsertClient(PortfolioClient obj, bool isUpdate)
        {
            LogManager.InitLog("OnlinePortfolio.InsertClient");

            try
            {
                provider.Connect();
                var insert = provider.InsertClient(obj, isUpdate);

                if (insert)
                    provider.InsertClientOperators(obj.Client.Value, obj.Operators);

                return insert;
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }
        }

        public bool DeleteClient(string code)
        {
            LogManager.InitLog("OnlinePortfolio.DeleteClient");

            try
            {
                provider.Connect();
                return provider.DeleteClient(code);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }
        }

        public List<PortfolioClient> LoadPortfolioClients(PortfolioClientFilter filter)
        {
            LogManager.InitLog("OnlinePortfolio.LoadPortfolioClients");
            List<PortfolioClient> clients = new List<PortfolioClient>();

            try
            {
                provider.Connect();
                clients = provider.LoadPortfolioClients(filter);

                foreach (PortfolioClient item in clients)
                    item.Operators = provider.LoadPortfolioOperators(item.Client.Value);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return clients;
        }

        public List<CommonType> LoadCalculationTypes()
        {
            LogManager.InitLog("OnlinePortfolio.LoadCalculationTypes");
            List<CommonType> calculationTypes = new List<CommonType>();

            try
            {
                provider.Connect();
                calculationTypes = provider.LoadCalculationTypes();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return calculationTypes;
        }

        public PortfolioPosition LoadClientPortfolioPosition(int clientCode, long operatorID, string isHistory)
        {
            LogManager.InitLog("OnlinePortfolio.LoadClientContacts");
            PortfolioPosition position = new PortfolioPosition();

            try
            {
                provider.Connect();
                position = provider.LoadClientPortfolioPosition(clientCode, operatorID, isHistory);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return position;
        }

        public PortfolioClient LoadPortfolioClient(string clientCode)
        {
            LogManager.InitLog("OnlinePortfolio.LoadPortfolioClient");
            PortfolioClient client = new PortfolioClient();

            try
            {
                provider.Connect();
                var result = provider.LoadPortfolioClients(new PortfolioClientFilter { Client = clientCode });

                if (result != null && result.Count > 0)
                {
                    client = result[0];
                    if (client.Client != null)
                        client.Operators = provider.LoadPortfolioOperators(client.Client.Value);
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return client;
        }

        public PortfolioClientSearchClients LoadSinacorClients(string term)
        {
            LogManager.InitLog("OnlinePortfolio.LoadSinacorClients");
            PortfolioClientSearchClients clients = new PortfolioClientSearchClients();
            
            try
            {
                provider.Connect();
                clients = provider.LoadSinacorClients(term);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return clients;
        }

        public PortfolioPosition SearchClientPortfolioPosition(string clientCode, string operatorCode)
        {
            LogManager.InitLog("OnlinePortfolio.SearchClientPortfolioPosition");
            PortfolioPosition position = new PortfolioPosition();

            

            try
            {
                provider.Connect();
                position = provider.SearchClientPortfolioPosition(clientCode, operatorCode);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return position;
        }

        public bool DeletePosition(string id)
        {
            LogManager.InitLog("OnlinePortfolio.DeletePosition");

            try
            {
                provider.Connect();
                return provider.DeletePosition(id);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }
        }

        public bool InsertPosition(PortfolioPosition obj, string sourcePosition)
        {
            LogManager.InitLog("OnlinePortfolio.InsertPosition");

            try
            {
                provider.Connect();
                var insert = provider.InsertPosition(obj, sourcePosition);

                if (insert && (!string.IsNullOrEmpty(sourcePosition) && !sourcePosition.Equals("0")))
                    provider.DeletePosition(sourcePosition);

                return insert;
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }
        }
   
    }
}
