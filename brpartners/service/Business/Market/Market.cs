﻿using System;
using QX3.Spinnex.Trace.Services.Transactions;
using System.Xml;
using QX3.Spinnex.Common.Services.Logging;
using System.Collections.Generic;
using QX3.Portal.Services.Data.Log;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.Services.Data;

namespace QX3.Portal.Services.Business.Market
{
    public class Market
    {
        public MarketDAL provider;

        public Market()
        {
            provider = new MarketDAL();
        }

        #region Common

        public List<Stock> LoadStocks()
        {
            LogManager.InitLog("Market.LoadStocks");
            List<Stock> list = null;

            try
            {
                provider.Connect();
                list = provider.LoadStocks();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return list;
        }

        public string GetAssessorName(int assessorID)
        {
            LogManager.InitLog("Market.GetAssessorName");

            string name = string.Empty;

            try
            {
                provider.Connect();
                name = provider.GetAssessorName(assessorID);

                LogManager.WriteLog(string.Format("<in>{0}</in><out>{1}</out>", assessorID, name));
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return name;
        }

        public List<CommonType> AssessorNameSuggest(string assessorName)
        {
            LogManager.InitLog("Market.AssessorNameSuggest");

            List<CommonType> assessor;

            try
            {
                provider.Connect();
                assessor = provider.AssessorNameSuggest(assessorName);

                LogManager.WriteLog(string.Format("<in>{0}</in><out>{1}</out>", assessorName, assessor));
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return assessor;
        }

        public string GetNextBusinessDay(int numberOfDays, string targetDate)
        {
            LogManager.InitLog("Market.GetNextBusinessDay");
            string businessDay = string.Empty;

            try
            {
                provider.Connect();
                businessDay = provider.GetNextBusinessDay(numberOfDays, targetDate);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return businessDay;
        }

        public StockExchanges LoadClientExchanges(int clientCode)
        {
            LogManager.InitLog("Market.LoadClientExchanges");
            StockExchanges exchanges = new StockExchanges();

            try
            {
                provider.Connect();
                exchanges = provider.LoadClientExchanges(clientCode);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return exchanges;
        }

        #endregion

        #region Proceeds

        public List<Proceeds> LoadProceeds(ProceedsFilter filter, int? userId)
        {
            LogManager.InitLog("Market.LoadProceeds");
            List<Proceeds> list = null;

            try
            {
                provider.Connect();
                list = provider.LoadProceeds(filter, userId);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return list;
        }

        public int GetNumberOfPending(string tableName, int statusId, string assessorsList)
        {
            LogManager.InitLog("Market.GetNumberOfPending");
            int pending = 0;

            try
            {
                provider.Connect();
                pending = provider.GetNumberOfPending(tableName, statusId, assessorsList);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return pending;
        }

        #endregion



    }
}
