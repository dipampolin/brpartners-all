﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using QX3.Portal.Services.Data;
using QX3.Spinnex.Common.Services.Logging;
using QX3.Portal.Contracts.DataContracts;


namespace QX3.Portal.Services.Business
{
	public class Brokerage
	{
		public BrokerageDAL provider;

		public Brokerage()
		{
			provider = new BrokerageDAL();
		}

		public bool SettleTransfer(string settlementDate, string settlementUsername)
		{
			bool changed;

			LogManager.InitLog("NonResidentTraders.SettleTransfer");

			try
			{
				provider.Connect();

				changed = provider.SettleBrokerageTransfer(settlementDate, settlementUsername);

				LogManager.WriteLog(string.Format("<in>{0}</in><in>{1}</in><out>{2}</out>", settlementDate, settlementUsername, changed));
			}
			catch (Exception ex)
			{
				LogManager.WriteError(ex);
				throw new Exception(ex.Message, ex);
			}
			finally
			{
				provider.Close();
			}

			return changed;
		}

		public List<BrokerageTransfer> LoadPendingTransfers(int code, string date, int? userId)
		{
			LogManager.InitLog("NonResidentTraders.PendingBrokerageTransfers");
			List<BrokerageTransfer> list = null;

			try
			{
				provider.Connect();
				list = provider.LoadPendingBrokerageTransfers(code, date, userId);
				LogManager.WriteLog(string.Format("<in>{0}</in><in>{1}</in><out>{2}</out>", code, date, JsonConvert.SerializeObject(list)));
			}
			catch (Exception ex)
			{
				LogManager.WriteError(ex);
				throw new Exception(ex.Message, ex);
			}
			finally
			{
				provider.Close();
			}

			return list;
		}

		public List<BrokerageTransfer> LoadSettledTransfers(int code, string startDate, string endDate, int? userId)
		{
			LogManager.InitLog("NonResidentTraders.BrokerageTransfers");
			List<BrokerageTransfer> list = null;

			try
			{
				provider.Connect();
				list = provider.LoadSettledBrokerageTransfers(code, startDate, endDate, userId);
				LogManager.WriteLog(string.Format("<in>{0}</in><in>{1}</in><in>{2}</in><out>{3}</out>", code, startDate, endDate, JsonConvert.SerializeObject(list)));
			}
			catch (Exception ex)
			{
				LogManager.WriteError(ex);
				throw new Exception(ex.Message, ex);
			}
			finally
			{
				provider.Close();
			}

			return list;
		}

		public decimal GetTransferDefaultValue()
		{
			decimal value = 0;

			LogManager.InitLog("NonResidentTraders.GetTransferBrokerageDefaultValue");

			try
			{
				provider.Connect();

				value = provider.GetTransferBrokerageDefaultValue();

				LogManager.WriteLog(string.Format("<out>{0}</out>", value));
			}
			catch (Exception ex)
			{
				LogManager.WriteError(ex);
				throw new Exception(ex.Message, ex);
			}
			finally
			{
				provider.Close();
			}

			return value;
		}
 
	}
}
