﻿using System;
using QX3.Portal.Services.Data;
using QX3.Spinnex.Common.Services.Logging;
using QX3.Portal.Contracts.DataContracts;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace QX3.Portal.Services.Business
{
	public class Registration
	{
		public RegistrationDAL provider;

		public Registration()
		{
			provider = new RegistrationDAL();
		}

		public string GetCustomerName(int code)
		{
			LogManager.InitLog("NonResidentTraders.GetCustomerName");

			string name = string.Empty;

			try
			{
				provider.Connect();
				name = provider.GetCustomerName(code);

				LogManager.WriteLog(string.Format("<in>{0}</in><out>{1}</out>", code, name));
			}
			catch (Exception ex)
			{
				LogManager.WriteError(ex);
				throw new Exception(ex.Message, ex);
			}
			finally
			{
				provider.Close();
			}

			return name;
		}

        public string GetCustomerName(int code, string assessors, int? userId)
        {
            LogManager.InitLog("NonResidentTraders.GetCustomerName");

            string name = string.Empty;

            try
            {
                provider.Connect();
                name = provider.GetCustomerName(code, assessors, userId);

                LogManager.WriteLog(string.Format("<in>{0}</in><out>{1}</out>", code, name));
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return name;
        }

        public List<Broker> LoadBrokers(string filter)
        {
            LogManager.InitLog("NonResidentTraders.LoadBrokers");

            List<Broker> list = null;

            try
            {
                provider.Connect();
                list = provider.LoadBrokers(filter);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return list;
        }

        public List<Broker> LoadBrokersTransfer(string filter)
        {
            LogManager.InitLog("NonResidentTraders.LoadBrokersTransfer");

            List<Broker> list = null;

            try
            {
                provider.Connect();
                list = provider.LoadBrokersTransfer(filter);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return list;
        }

        public string LoadClientEmail(int clientCode) 
        {
            LogManager.InitLog("NonResidentTraders.GetCustomerName");

            string email = string.Empty;

            try
            {
                provider.Connect();
                email = provider.LoadClientEmail(clientCode);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return email;
        }

        public List<CommonType> LoadPortfolios(string filter)
        {
            LogManager.InitLog("NonResidentTraders.GetCustomerName");

            List<CommonType> list = null;

            try
            {
                provider.Connect();
                list = provider.LoadPortfolios(filter);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return list;
        }

		public List<NonResidentCustomer> LoadCustomers(int code, int? userId)
		{
			List<NonResidentCustomer> customers = new List<NonResidentCustomer>();

			LogManager.InitLog("NonResidentTraders.LoadCustomers");

			try
			{
				provider.Connect();
				customers = provider.LoadCustomers(code, userId);

				LogManager.WriteLog(string.Format("<in>{0}</in><out>{1}</out>", code, JsonConvert.SerializeObject(customers)));
			}
			catch (Exception ex)
			{
				LogManager.WriteError(ex);
				throw new Exception(ex.Message, ex);
			}
			finally
			{
				provider.Close();
			}

			return customers;
		}

		public bool InsertCustomer(NonResidentCustomer customer)
		{
			bool changed;

			LogManager.InitLog("NonResidentTraders.InsertCustomer");

			try
			{
				if (customer.Code == 0)
					throw new Exception("Invalid Customer");

				provider.Connect();

				changed = provider.InsertCustomer(customer);

				LogManager.WriteLog(string.Format("<in>{0}</in><out>{1}</out>", JsonConvert.SerializeObject(customer), changed));
			}
			catch (Exception ex)
			{
				LogManager.WriteError(ex);
				throw new Exception(ex.Message, ex);
			}
			finally
			{
				provider.Close();
			}

			return changed;
		}

        public SettlementDate LoadLastSettlement()
        {
            LogManager.InitLog("NonResidentTraders.LoadLastSettlement");
            SettlementDate settlementDate = null;

            try
            {
                provider.Connect();
                settlementDate = provider.LoadLastSettlement();
                LogManager.WriteLog(string.Format("<out>{0}</out>", JsonConvert.SerializeObject(settlementDate)));
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return settlementDate;       
        }

		public List<PTax> LoadPTaxes(string date)
		{
			LogManager.InitLog("NonResidentTraders.LoadPTaxes");
			List<PTax> list = null;

			try
			{
				provider.Connect();
				list = provider.LoadPTaxes(date);
				LogManager.WriteLog(string.Format("<in>{0}</in><out>{1}</out>", date, JsonConvert.SerializeObject(list)));
			}
			catch (Exception ex)
			{
				LogManager.WriteError(ex);
				throw new Exception(ex.Message, ex);
			}
			finally
			{
				provider.Close();
			}

			return list;
		}

		public bool InsertPTax(PTax ptax)
		{
			bool changed;

			LogManager.InitLog("NonResidentTraders.InsertPTax");

			try
			{
				provider.Connect();

				Brokerage brokController = new Brokerage();
				var transferPercentage = brokController.GetTransferDefaultValue();

				changed = provider.InsertPTax(ptax, transferPercentage);

				LogManager.WriteLog(string.Format("<in>{0}</in><in>{1}</in><out>{2}</out>", JsonConvert.SerializeObject(ptax), transferPercentage, changed));
			}
			catch (Exception ex)
			{
				LogManager.WriteError(ex);
				throw new Exception(ex.Message, ex);
			}
			finally
			{
				provider.Close();
			}

			return changed;
		}

		public List<Failure> LoadFailures(string date, int code, int? userId)
		{
			LogManager.InitLog("NonResidentTraders.LoadFailures");
			List<Failure> list = null;

			try
			{
				provider.Connect();
				list = provider.LoadFailures(code, date, userId);
				LogManager.WriteLog(string.Format("<in>{0}</in><in>{1}</in><out>{2}</out>", date, code, JsonConvert.SerializeObject(list)));
			}
			catch (Exception ex)
			{
				LogManager.WriteError(ex);
				throw new Exception(ex.Message, ex);
			}
			finally
			{
				provider.Close();
			}

			return list;
		}

		public bool InsertFailure(Failure failure)
		{
			bool changed;

			LogManager.InitLog("NonResidentTraders.InsertFailure");

			try
			{
				provider.Connect();

				changed = provider.InsertFailure(failure);

				LogManager.WriteLog(string.Format("<in>{0}</in><out>{1}</out>", JsonConvert.SerializeObject(failure), changed));
			}
			catch (Exception ex)
			{
				LogManager.WriteError(ex);
				throw new Exception(ex.Message, ex);
			}
			finally
			{
				provider.Close();
			}

			return changed;
		}

        public List<ReportType> LoadReportTypes() 
        {
			List<ReportType> reportTypes = new List<ReportType>();
            
            LogManager.InitLog("NonResidentTraders.LoadReportTypes");

            try
            {
                provider.Connect();

                reportTypes = provider.LoadReportTypes();

                LogManager.WriteLog(string.Format("<out>{0}</out>", JsonConvert.SerializeObject(reportTypes)));
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return reportTypes; 
        }

        public bool InsertDisclaimer(Disclaimer disclaimer)
        {
            bool charged;

            LogManager.InitLog("NonResidentTraders.InsertDisclaimer");

            try
            {
                provider.Connect();

                charged = provider.InsertDisclaimer(disclaimer);

                LogManager.WriteLog(string.Format("<in>{0}</in><out>{1}</out>", JsonConvert.SerializeObject(disclaimer), charged));
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return charged;
        }

		public Disclaimer LoadDisclaimer(int reportId)
        {
			Disclaimer disclaimer = new Disclaimer();

            LogManager.InitLog("NonResidentTraders.LoadDisclaimer");

            try
            {
                provider.Connect();

                disclaimer = provider.LoadDisclaimer(reportId);

                LogManager.WriteLog(string.Format("<in>{0}</in><out>{1}</out>", reportId, disclaimer));
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return disclaimer;
        }

		public List<Disclaimer> LoadDisclaimers()
		{
			List<Disclaimer> disclaimers = new List<Disclaimer>();

			LogManager.InitLog("NonResidentTraders.LoadDisclaimers");

			try
			{
				provider.Connect();

				var reports = provider.LoadReportTypes();
				foreach (ReportType report in reports)
				{
					var disclaimer = provider.LoadDisclaimer(report.ReportId);
					disclaimers.Add(disclaimer);
				}

				LogManager.WriteLog(string.Format("<out>{0}</out>", JsonConvert.SerializeObject(disclaimers)));
			}
			catch (Exception ex)
			{
				LogManager.WriteError(ex);
				throw new Exception(ex.Message, ex);
			}
			finally
			{
				provider.Close();
			}

			return disclaimers;
		}
                
	}
}
