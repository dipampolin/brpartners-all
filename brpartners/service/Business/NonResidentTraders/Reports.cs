﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using QX3.Portal.Services.Data;
using QX3.Spinnex.Common.Services.Logging;
using QX3.Portal.Contracts.DataContracts;


namespace QX3.Portal.Services.Business
{
	public class Reports
	{
		public ReportDAL provider;

		public Reports()
		{
			provider = new ReportDAL();
		}

        public Confirmation LoadConfirmationReports(int code, DateTime trading) 
        {
            LogManager.InitLog("NonResidentTraders.LoadConfirmationReports");
            Confirmation report = null;

            try
            {
                provider.Connect();
				report = provider.LoadConfirmationReports(code, trading);

				if (report != null)
				{
					Registration regsController = new Registration();
					report.Footer = new ReportFooter { Disclaimer = regsController.LoadDisclaimer(1).Content };
				}

                LogManager.WriteLog(string.Format("<in>{0}</in><in>{1}</in><out>{2}</out>", code, trading, JsonConvert.SerializeObject(report)));
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally 
            {
                provider.Close();
            }

            return report;
        }

        public List<Customer> LoadCustomers(int code, string consultType, DateTime tradingDate, int? userId) 
        {
            LogManager.InitLog("NonResidentTraders.LoadCustomers");
            List<Customer> list = null;

            try
            {
                provider.Connect();
				list = provider.LoadClients(code, consultType, tradingDate, userId);
                LogManager.WriteLog(string.Format("<in>{0}</in><in>{1}</in><in>{2}</in><out>{3}</out>", code, consultType, tradingDate, JsonConvert.SerializeObject(list)));
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally 
            {
                provider.Close();
            }

            return list;
        }

		public TradeBlotter LoadTradeBlotterReports(DateTime tradingDate, int? userId) 
        {
            LogManager.InitLog("NonResidentTraders.LoadTradeBlotterReports");
            TradeBlotter tradeBlotter = null;

            try
            {
                provider.Connect();
                tradeBlotter = provider.LoadTradeBlotterReports(tradingDate, userId);
                LogManager.WriteLog(string.Format("<in>{0}</in><out>{1}</out>", tradingDate, JsonConvert.SerializeObject(tradeBlotter)));
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally 
            {
                provider.Close();
            }

            return tradeBlotter;
        }

        public ReportHeader LoadHeader() 
        {
            LogManager.InitLog("NonResidentTraders.LoadHeader");
            ReportHeader header = null;

            try
            {
                provider.Connect();
                header = provider.LoadHeader();
                LogManager.WriteLog(string.Format("<out>{0}</out>", JsonConvert.SerializeObject(header)));
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally 
            {
                provider.Close();
            }

            return header;
        }

        public CustomerStatement LoadCustomerStatementReports(int code, string date, int? userId)
        {
            LogManager.InitLog("NonResidentTraders.LoadHeader");
            CustomerStatement customerStatement = null;

            try
            {
                provider.Connect();
                customerStatement = provider.LoadCustomerStatementReports(code, date, userId);

				if (customerStatement != null)
				{
					Registration regsController = new Registration();
					customerStatement.Footer = new ReportFooter { Disclaimer = regsController.LoadDisclaimer(2).Content };
				}

                LogManager.WriteLog(string.Format("<in>{0}</in><in>{1}</in><out>{2}</out>", code, date, JsonConvert.SerializeObject(customerStatement)));
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally 
            {
                provider.Close();
            }

            return customerStatement;
        }

		public CustomerLedger LoadCustomerLedgerReports(int code, string date)
		{
			LogManager.InitLog("NonResidentTraders.LoadCustomerLedgerReports");
			CustomerLedger customerLedger = null;

			try
			{
				provider.Connect();
				customerLedger = provider.LoadCustomerLedgerReports(code, date);
				LogManager.WriteLog(string.Format("<in>{0}</in><in>{1}</in><out>{2}</out>", code, date, JsonConvert.SerializeObject(customerLedger)));
			}
			catch (Exception ex)
			{
				LogManager.WriteError(ex);
				throw new Exception(ex.Message, ex);
			}
			finally
			{
				provider.Close();
			}

			return customerLedger;
		}

        public StockRecord LoadStockRecordReports(string date, int? userId) 
        {
            LogManager.InitLog("NonResidentTraders.LoadPreStockRecordReports");
            StockRecord stockRecord = null;

            try
			{
				provider.Connect();
                
                //Report, including just header and footer
                stockRecord = provider.LoadStockRecordReports();
                
                //Items, it's the body
				var records = provider.LoadPreStockRecordReports(date, userId);
                List<StockRecordItem> items = new List<StockRecordItem>();

                var stocks = (from record in records select record.CompanyName).Distinct();
                bool first = true;

                foreach (string stock in stocks)
                {
                    var stockItems = (from record in records where record.CompanyName.Equals(stock) select record).ToList();
                    List<StockRecordSubItem> subItems = new List<StockRecordSubItem>();

                    foreach (PreStockRecordReport item in stockItems)
                        subItems.Add(new StockRecordSubItem { ClientBroker = item.ClientBroker, ConfirmID = item.ConfirmID, DateSettled = item.DateSettled, Long = item.Long, Short = item.Short });

                    items.Add(new StockRecordItem { CompanyName = stock, SubItems = subItems });

                    if (first)
                    {
                        stockRecord.StartDate = stockItems[0].Start;
                        stockRecord.EndDate = stockItems[0].End;
                        first = !first;
                    }
                }

                stockRecord.Items = items;
                LogManager.WriteLog(string.Format("<in>{0}</in><out>{1}</out>", date, JsonConvert.SerializeObject(stockRecord)));
			}
			catch (Exception ex)
			{
				LogManager.WriteError(ex);
				throw new Exception(ex.Message, ex);
			}
			finally
			{
				provider.Close();
			}

            return stockRecord;
        }

		public FailLedger LoadFailLedgerReports(int code, DateTime tradingDate, FailLedgerType type, int? userId)
		{
			LogManager.InitLog("NonResidentTraders.LoadFailLedgerReports");
			FailLedger failLedger = null;

			try
			{
				provider.Connect();
				failLedger = provider.LoadFailLedgerReports(code, tradingDate, type, userId);
				LogManager.WriteLog(string.Format("<in>{0}</in><in>{1}</in><in>{2}</in><out>{3}</out>", code, tradingDate, type, JsonConvert.SerializeObject(failLedger)));
			}
			catch (Exception ex)
			{
				LogManager.WriteError(ex);
				throw new Exception(ex.Message, ex);
			}
			finally
			{
				provider.Close();
			}

			return failLedger;
		}
	}
}
