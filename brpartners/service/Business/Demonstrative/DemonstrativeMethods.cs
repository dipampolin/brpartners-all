﻿using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.Services.Data;
using QX3.Spinnex.Common.Services.Logging;
using System;
using System.Collections.Generic;

namespace QX3.Portal.Services.Business
{
    public class DemonstrativeMethods
    {
        private DemonstrativeDAL provider;

        public DemonstrativeMethods()
        {
            provider = new DemonstrativeDAL();
        }

        public ExtractRendaResponse Extract(string CodEmpresa, string Cnpjcpf, string DtRefDe, string DtRefAte)
        {
            LogManager.InitLog("Demonstrative.Extract");
            ExtractRendaResponse result = new ExtractRendaResponse();

            try
            {
                result = provider.Extract(CodEmpresa, Cnpjcpf, DtRefDe, DtRefAte);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
            }

            return result;
        }

        public TradingSummaryResponse ListTradingSummary(int cdCliente, string cdAtivo, DateTime dtInicial, DateTime dtFinal)
        {
            LogManager.InitLog("Demonstrative.ListTradingSummary");
            TradingSummaryResponse result = new TradingSummaryResponse();

            try
            {
                provider.Connect();
                result = provider.ListTradingSummary(cdCliente, cdAtivo, dtInicial, dtFinal);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return result;
        }

        public List<ExtractFunction> ExtractFunction(string CodEmpresa, string DtRefDe, string DtRefAte, string CpfCnpj, string UserName)
        {
            LogManager.InitLog("Demonstrative.ListExtractFunction");

            var result = new List<ExtractFunction>();

            try
            {
                //provider.Connect();
                result = provider.ExtractFunction(CodEmpresa, DtRefDe, DtRefAte, CpfCnpj, UserName);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return result;
        }

        public ProceedsDemonstrativeResponse ListProceeds(int cdCliente, string provisionados, DateTime dtInicial, DateTime dtFinal)
        {
            LogManager.InitLog("Demonstrative.ListProceeds");
            ProceedsDemonstrativeResponse result = new ProceedsDemonstrativeResponse();

            try
            {
                provider.Connect();
                result = provider.ListProceeds(cdCliente, provisionados, dtInicial, dtFinal);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return result;
        }

        public ExtractNDFResponse ListExtractNDF(string CodEmpresa, string Cnpjcpf, int Mes, int Ano)
        {
            LogManager.InitLog("Demonstrative.ListExtractNDF");
            ExtractNDFResponse result = new ExtractNDFResponse();

            try
            {
                result = provider.ListExtractNDF(CodEmpresa, Cnpjcpf, Mes, Ano);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {

            }

            return result;
        }

        public ExtractSWPResponse ListExtractSWP(string CodEmpresa, string Cnpjcpf, int mes, int ano)
        {
            LogManager.InitLog("Demonstrative.ListExtractSWP");
            ExtractSWPResponse result = new ExtractSWPResponse();

            try
            {
                result = provider.ListExtractSWP(CodEmpresa, Cnpjcpf, mes, ano);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {

            }

            return result;
        }

        public BrokerageNoteResponse ListBrokeragenotesDetails(int cdCliente, int nrNota, DateTime dtNegocio)
        {
            LogManager.InitLog("Demonstrative.ListBrokeragenotesDetails");
            BrokerageNoteResponse result = new BrokerageNoteResponse();

            try
            {
                provider.Connect();
                result = provider.ListBrokeragenotesDetails(cdCliente, nrNota, dtNegocio);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return result;
        }

        public BrokerageNoteResponse ListBrokeragenotesNegociosDia(int cdCliente, DateTime dtNegocio, string tpNegocio)
        {
            LogManager.InitLog("Demonstrative.ListBrokeragenotesNegociosDia");
            BrokerageNoteResponse result = new BrokerageNoteResponse();

            try
            {
                provider.Connect();
                result = provider.ListBrokeragenotesNegociosDia(cdCliente, dtNegocio, tpNegocio);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return result;
        }

        public BrokerageNoteResponse ListBrokeragenotesNegociosHist(int cdCliente, DateTime dtNegocio, string tpNegocio)
        {
            LogManager.InitLog("Demonstrative.ListBrokeragenotesNegociosHist");
            BrokerageNoteResponse result = new BrokerageNoteResponse();

            try
            {
                provider.Connect();
                result = provider.ListBrokeragenotesNegociosHist(cdCliente, dtNegocio, tpNegocio);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return result;
        }
    }

}