﻿using QX3.Portal.Contracts.DataContracts;
using QX3.Spinnex.Authentication.Services;
using QX3.Spinnex.Common.Services.Logging;
using QX3.Spinnex.Common.Services.Mail;
using QX3.Spinnex.Common.Services.Security;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Mail;
using System.Web;
using DataContract = QX3.Portal.Contracts.DataContracts;
using FrameWork = QX3.Spinnex.Authentication.Services.UserData;

namespace QX3.Portal.Services.Business.Authentication
{
    public class Authentication
    {
        private QX3.Portal.Services.Data.Authentication.AuthenticationDAL prv;
        protected QX3.Portal.Services.Data.Authentication.AuthenticationDAL provider
        {
            get
            {
                if (prv == null)
                    prv = new Data.Authentication.AuthenticationDAL();
                return prv;
            }
        }

        private AuthenticationManager manager;
        private string _hostEvironment = System.Web.Hosting.HostingEnvironment.MapPath("~/XML/LdapConfig.xml");

        public Authentication()
        {
            manager = new AuthenticationManager(_hostEvironment);
        }
        //-------------------------LDAP----------------------------//TODO: Retirar este método quando não usar mais o LDAP
        public DataContract.UserInformation Authenticate(string userName, string password, bool loadPermissions)
        {
            userName = string.Concat(ConfigurationManager.AppSettings["Portalweb.LoginPrefix"], userName);

            FrameWork.UserInformation userInformation = manager.Authenticate(userName, password, loadPermissions);

            var data = new DataContract.UserInformation
            {
                Name = userInformation.Name,
                UserName = userInformation.UserName,
                Email = userInformation.Email
            };
            /*
            if (data != null && !string.IsNullOrEmpty(data.Email))
            {
                AccessControl.AccessControl acController = new AccessControl.AccessControl();
                data.Profile = acController.LoadUserProfile(data.Email);

                if (data.Profile == null || data.Profile.ID == 0)
                    throw new Exception("Este usuário não possui um perfil associado a ele.");
                acController.LoadUserInfo(ref data);
            }
            else
                throw new Exception("Este usuário não possui informações associadas a ele.");
            */
            return data;
        }

        //Login Banco
        public DataContract.UserInformation AuthenticateUser(string userName, string password, out string message, out bool logged, out int errorType, out string ADKey)
        {

            /*userName = string.Concat(ConfigurationManager.AppSettings["Portalweb.LoginPrefix"], userName);

            FrameWork.UserInformation userInformation = manager.Authenticate(userName, password, true);

            if (userInformation != null) {  */


            //var data = new QX3.Portal.Services.Data.Authentication.AuthenticationDAL().Authenticate(userName, password, out message, out logged, out errorType, out ADKey);

            var data = new QX3.Portal.Services.Data.Authentication.AuthenticationDAL().LoginSQLServer(userName, password, out message, out logged, out errorType, out ADKey);

            if (data != null && !string.IsNullOrEmpty(data.Email))
            {
                AccessControl.AccessControl acController = new AccessControl.AccessControl();
                data.Profile = acController.LoadUserProfile(data.Email);
                //data.GroupOfAssessor = acController.LoadGroupOfAssessorsPerUser(data.ID);

                if (data.Profile == null || data.Profile.ID == 0)
                    throw new Exception("Este usuário não possui um perfil associado a ele.");
                acController.LoadUserInfo(ref data);
            }

           

            return data;
        }

        public DataContract.UserInformation SelectUser(string userName, out string message, out bool logged, out int errorType)
        {

            var data = new QX3.Portal.Services.Data.Authentication.AuthenticationDAL().SelectUser(userName, out message, out logged, out errorType);

            if (data != null && !string.IsNullOrEmpty(data.Email))
            {
                AccessControl.AccessControl acController = new AccessControl.AccessControl();
                data.Profile = acController.LoadUserProfile(data.Email);
                data.GroupOfAssessor = acController.LoadGroupOfAssessorsPerUser(data.ID);

                acController.LoadUserInfo(ref data);
            }

            return data;
        }

        public bool CheckUserExists(string filter, LDAPSearchFilter filterType)
        {
            PasswordGenerator pg = new PasswordGenerator();
            if (filterType == LDAPSearchFilter.SAMAccountName)
                filter = string.Concat(ConfigurationManager.AppSettings["Portalweb.LoginPrefix"], filter);

            return manager.CheckIfUserExistsInLDAP(filter, filterType);
        }

        public bool CreateLDAPUser(ACUser user)
        {
            string username = string.Concat(ConfigurationManager.AppSettings["Portalweb.LoginPrefix"], user.ChaveAD);
            return manager.CreateUserLDAP(user.Name, username, user.Password, user.Email, false);
        }

        public bool DeleteUserLDAP(string filterValue, LDAPSearchFilter filterType)
        {
            return manager.DeleteUserLDAP(filterValue, filterType);
        }

        public bool ChangePassword(QX3.Portal.Contracts.DataContracts.UserInformation userInfo, out string message)
        {
            try
            {
                //var changed = manager.ChangePasswordLDAP(userInfo.Name, string.Concat(ConfigurationManager.AppSettings["Portalweb.LoginPrefix"], userInfo.UserName), "", userInfo.Password, true);
                //if (changed)
                return provider.ChangePassword(userInfo, out message);
                //else
                // {
            }
            catch (Exception ex)
            {
                message = ex.Message;
                //message = "Ocorreu um erro ao alterar senha no AD";
                return false;
            }
        }

        public bool VerifyChangePassword(string guid, out string message, out string email)
        {
            return provider.VerifyChangePassword(guid, out message, out email);
        }

        public bool CreateGuidToSend(UserInformation userInfo, out string guid, out string message)
        {
            return provider.CreateGuidToSend(userInfo, out guid, out message);
        }

        public bool SendRedefinePasswordEmail(string token, string email, string userName, bool welcomeEmail = false)
        {
            LogManager.InitLog("Authentication.SendRedefinePasswordEmail");
            bool response = false;

            MailMessage objEmail = new MailMessage();
            try
            {
                List<KeyValuePair<string, object>> messageValues = new List<KeyValuePair<string, object>>();
                messageValues.Add(new KeyValuePair<string, object>("strUrlSite", ConfigurationManager.AppSettings["Portal.Url"]));
                messageValues.Add(new KeyValuePair<string, object>("strUsuario", userName));
                messageValues.Add(new KeyValuePair<string, object>("strHorasDisponivel", ConfigurationManager.AppSettings["Mail.TokenDuration"]));
                messageValues.Add(new KeyValuePair<string, object>("strLink", string.Format("{0}/Account/RedefinePassword/?token={1}", ConfigurationManager.AppSettings["Portal.Url"], token)));

                string welcomeSubject = ConfigurationManager.AppSettings["Mail.Welcome"];
                string resetpasswordSubject = ConfigurationManager.AppSettings["Mail.ResetPassword"];

                string subject = welcomeEmail ? welcomeSubject : resetpasswordSubject;
                string templatePath = welcomeEmail ? "~/Template/WelcomeEmail.html" : "~/Template/RedefinePasswordEmail.html";

                Template template = new Template(HttpContext.Current.Server.MapPath(templatePath));
                template.VariableFormat = "#({0})#";
                template.ReplaceVars(messageValues.ToArray());
                string body = template.Body;

                var mail = new SendMail
                {
                    EmailTo = email,
                    Body = body,
                    IsBodyHtml = true,
                    Subject = subject
                };

                mail.Send();

                response = true;
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                objEmail.Dispose();
            }

            return response;
        }
    }
}
