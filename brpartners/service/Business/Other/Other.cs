﻿using System;
using QX3.Spinnex.Trace.Services.Transactions;
using System.Xml;
using QX3.Spinnex.Common.Services.Logging;
using System.Collections.Generic;
using QX3.Portal.Services.Data.Log;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.Services.Data;
using QX3.Portal.Services.GrTraderService;
using System.Configuration;

namespace QX3.Portal.Services.Business.Other
{
    public class Other
    {
        public OtherDAL provider;

        public Other()
        {
            provider = new OtherDAL();
        }

        public int InsertOrdersCorrection(OrdersCorrection correction)
        {
            LogManager.InitLog("Other.InsertOrdersCorrection");
            int correctionId = 0;

            try
            {
                provider.Connect();
                correctionId = provider.InsertOrdersCorrection(correction);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return correctionId;
        }

        public bool DeleteOrdersCorrection(int correctionId)
        {
            LogManager.InitLog("Other.DeleteOrdersCorrection");

            try
            {
                provider.Connect();
                return provider.DeleteOrdersCorrection(correctionId);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw ex;
            }
            finally
            {
                provider.Close();
            }
        }

        public bool ChangeOrdersCorrectionStatus(int correctionId, int statusID)
        {
            LogManager.InitLog("Other.ChangeOrdersCorrectionStatus");

            try
            {
                provider.Connect();
                return provider.ChangeOrdersCorrectionStatus(correctionId, statusID);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw ex;
            }
            finally
            {
                provider.Close();
            }
        }

        public List<OrdersCorrection> LoadOrdersCorrections(OrdersCorrectionFilter filter)
        {
            LogManager.InitLog("Other.LoadOrdersCorrection");
            List<OrdersCorrection> list = null;

            try
            {
                provider.Connect();
                list = provider.LoadOrdersCorrections(filter);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return list;
        }

        public OrdersCorrection LoadOrdersCorrection(int correctionID)
        {
            LogManager.InitLog("Other.LoadOrdersCorrection");
            OrdersCorrection correction = null;

            try
            {
                provider.Connect();
                correction = provider.LoadOrdersCorrection(correctionID);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return correction;
        }

        public ConsolidatedPositionResponse LoadConsolidatedPosition(string clientCode, string assessors, int? userId)
        {
            LogManager.InitLog("Other.LoadConsolidatedPosition");
            ConsolidatedPositionResponse obj = new ConsolidatedPositionResponse();

            try
            {
                provider.Connect();

                var clientInformation = provider.LoadClientInformation(clientCode, assessors, userId);
                if (clientInformation != null)
                {
                    var position = provider.LoadConsolidatedPosition(clientInformation);
                    if (position != null)
                        obj.Result = position;
                    else
                        obj.Message = string.Format("Posição do cliente {0} não encontrada.", clientCode);
                }
                else
                    obj.Message = string.Format("Cliente {0} não encontrado.", clientCode);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                obj.Message = string.Format("Falha ao carregar a Posição Consolidada do cliente {0}.", clientCode);
                //obj.Message = string.Format("Falha ao carregar a Posição Consolidada. Erro: {0}, {1}, {2}, {3}", ex.Message, ex.StackTrace, ex.InnerException, ex.Source);
            }
            finally
            {
                provider.Close();
            }

            return obj;
        }

        #region Mapa de Operações

        public List<OperationsMap> LoadOperationsMap(OperationMapFilter filter)
        {
            LogManager.InitLog("Other.LoadOperationMap");
            List<OperationsMap> list = null;

            try
            {
                provider.Connect();
                list = provider.LoadOperationsMap(filter);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return list;
        }

        public Dictionary<String, String> GetTradingDates() 
        {
            Dictionary<String, String> list = null;

            try
            {
                provider.Connect();
                list = provider.GetTradingDates();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return list;
        }

        public List<Operator> GetOperatorList(OperationMapFilter filter) 
        {
            LogManager.InitLog("Other.GetOperatorList");
            List<Operator> list = null;

            try
            {
                provider.Connect();
                list = provider.GetOperatorList(filter);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return list;
        }

        public CPClientInformation LoadClientInformation(string clientCode, string assessors, int? userId)
        {
            LogManager.InitLog("Other.GetClientInfo");
            CPClientInformation client = null;

            try
            {
                provider.Connect();
                client = provider.LoadClientInformation(clientCode, assessors, userId);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return client;
        }

        #endregion

        #region Operações

        public List<OperationClient> LoadOperationClients(OperationClientFilter filter, FilterOptions filterOptions, int? userId, out int count) 
        {
            LogManager.InitLog("Other.LoadOperationClients");

            var list = new List<OperationClient>();
            try
            {
                provider.Connect();
                list = provider.LoadOperationClients(filter, filterOptions, userId, out count);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return list;
        }

		public OperationReport LoadOperationReport(OperationReport filter)
		{
            LogManager.InitLog("Other.GetOperatorList");

			var item = new OperationReport();
			try
			{
				provider.Connect();
				item = provider.LoadOperationReport(filter);
			}
			catch (Exception ex)
			{
				LogManager.WriteError(ex);
				throw new Exception(ex.Message, ex);
			}
			finally
			{
				provider.Close();
			}

			return item;
		}

        public bool SinalizeMailSended(int clientCode, DateTime tradingDate) 
        {
            LogManager.InitLog("Other.SinalizeEmailSended");
            var response = false;
            try
            {
                provider.Connect();
                response = provider.SinalizeMailSended(clientCode, tradingDate);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return response;
        } 

        #endregion

        #region Informe de rendimentos

        public IncomeReport LoadIncomeReport(IncomeReportFilter filter) 
        {
            LogManager.InitLog("Other.LoadIncomeReport");
            IncomeReport item = null;
            try
            {
                provider.Connect();
                item = provider.LoadIncomeReport(filter);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return item;
        }

        #endregion

        #region Private
        public List<Private> LoadPrivates(DateTime initmov, DateTime endmov)
        {
            LogManager.InitLog("Other.LoadPrivates");

            List<Private> item = null;
            try
            {
                provider.Connect();
                item = provider.LoadPrivates(initmov, endmov);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return item;
        }
        #endregion

		#region BB Rent

		public BBRent LoadBBRent(DateTime tradeDate)
		{
			LogManager.InitLog("Other.LoadBBRent");

			BBRent item = null;
			try
			{
				provider.Connect();
				item = provider.LoadBBRent(tradeDate);
			}
			catch (Exception ex)
			{
				LogManager.WriteError(ex);
				throw new Exception(ex.Message, ex);
			}
			finally
			{
				provider.Close();
			}

			return item;
		}

		#endregion

        #region VAM

        public List<VAMItem> LoadVAM(int? groupID, string isin)
        {
            LogManager.InitLog("Other.LoadOrdersCorrection");
            List<VAMItem> list = null;

            try
            {
                provider.Connect();
                list = provider.LoadVAM(groupID, isin);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return list;
        }

        public Dictionary<string, string> GetISINs(int? groupID)
        {
            Dictionary<string, string> list = null;

            try
            {
                provider.Connect();
                list = provider.GetISINs(groupID);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return list;
        }

        #endregion
    }
}
