﻿using System;
using QX3.Spinnex.Trace.Services.Transactions;
using System.Xml;
using QX3.Spinnex.Common.Services.Logging;
using System.Collections.Generic;
using QX3.Portal.Services.Data.Log;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.Services.Data;

namespace QX3.Portal.Services.Business.Other
{
	public class Risk
	{
		public RiskDAL provider;

        public Risk()
		{
            provider = new RiskDAL();
		}

		public List<Int32> InsertGuarantee(Guarantee guarantee)
		{
            LogManager.InitLog("Risk.InsertGuarantee");
            List<Int32> ids = new List<Int32>();

			try
			{
				provider.Connect();
                ids = provider.InsertGuarantee(guarantee);
			}
			catch (Exception ex)
			{
				LogManager.WriteError(ex);
				throw new Exception(ex.Message, ex);
			}
			finally
			{
				provider.Close();
			}

            return ids;
		}

        public bool DeleteGuarantee(int guaranteeID)
        {
            LogManager.InitLog("Risk.DeleteGuarantee");

            try
            {
                provider.Connect();
                return provider.DeleteGuarantee(guaranteeID);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw ex;
            }
            finally
            {
                provider.Close();
            }
        }

        public bool ChangeGuaranteeStatus(int guaranteeID, int statusID)
        {
            LogManager.InitLog("Risk.ChangeGuaranteeStatus");

            try
            {
                provider.Connect();
                return provider.ChangeGuaranteeStatus(guaranteeID, statusID);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw ex;
            }
            finally
            {
                provider.Close();
            }
        }

        public bool UpdateGuarantee(GuaranteeItem guarantee)
        {
            LogManager.InitLog("Risk.UpdateGuarantee");
            bool update = false;

            try
            {
                provider.Connect();
                update = provider.UpdateGuarantee(guarantee);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return update;
        }

        public List<Guarantee> LoadGuarantees(GuaranteeFilter filter, int? userId)
        {
            LogManager.InitLog("Risk.LoadGuarantees");
            List<Guarantee> list = null;

            try
            {
                provider.Connect();
                list = provider.LoadGuarantees(filter, userId);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return list;
        }

        public int GetNumberOfPending(string assessorsList)
        {
            LogManager.InitLog("Risk.GetNumberOfPending");
            int pending = 0;

            try
            {
                provider.Connect();
                pending = provider.GetNumberOfPending(assessorsList);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return pending;
        }

        public List<GuaranteeItem> LoadClientGuarantees(GuaranteeFilter filter, int? userId)
        {
            LogManager.InitLog("Risk.LoadClientGuarantees");
            List<GuaranteeItem> list = null;

            try
            {
                provider.Connect();
                list = provider.LoadClientGuarantees(filter, userId);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                provider.Close();
            }

            return list;
        }

	}
}
