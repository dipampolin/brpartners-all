﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace QX3.Portal.Services.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class AccessControlResources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal AccessControlResources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("QX3.Portal.Services.Properties.AccessControlResources", typeof(AccessControlResources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT COUNT(ID) FROM TB_CA_GRUPO_ASSESSORES WHERE ASSESSORES = :List AND ID &lt;&gt; :GroupId.
        /// </summary>
        internal static string CountGroupByAssessorsList {
            get {
                return ResourceManager.GetString("CountGroupByAssessorsList", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT COUNT(ID) FROM TB_CA_GRUPO_ASSESSORES WHERE Lower(NOME) = Lower(:GroupName) AND ID &lt;&gt; :GroupId.
        /// </summary>
        internal static string CountGroupByName {
            get {
                return ResourceManager.GetString("CountGroupByName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT COUNT(ID) FROM TB_CA_PERFIL WHERE Lower(NOME) = Lower(:ProfileName) AND ID &lt;&gt; :ProfileId.
        /// </summary>
        internal static string CountProfileByName {
            get {
                return ResourceManager.GetString("CountProfileByName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to DELETE FROM TB_CA_ASSESSOR_USUARIO WHERE IDGRUPOASSESSOR = :GroupId.
        /// </summary>
        internal static string DeleteGroupOfAssessorsUsers {
            get {
                return ResourceManager.GetString("DeleteGroupOfAssessorsUsers", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to DELETE FROM TB_CA_ELEMENTO_PERFIL WHERE IDPERFIL = :ProfileID.
        /// </summary>
        internal static string DeleteProfileElementRelationship {
            get {
                return ResourceManager.GetString("DeleteProfileElementRelationship", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to DELETE FROM TB_CA_ASSESSOR_USUARIO WHERE IDUSUARIO = :UserId.
        /// </summary>
        internal static string DeleteUserGroupAssessor {
            get {
                return ResourceManager.GetString("DeleteUserGroupAssessor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 
        ///SELECT U.ID USERID, U.NOME USERNAME FROM TB_CA_USUARIO U WHERE U.ID NOT IN (:Ids) AND LOWER(U.NOME) LIKE &apos;%:Name%&apos;.
        /// </summary>
        internal static string GroupOfAssessorsFreeUsers {
            get {
                return ResourceManager.GetString("GroupOfAssessorsFreeUsers", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to INSERT INTO TB_CA_GRUPO_ASSESSORES
        ///	(
        ///	ID,
        ///	NOME,
        ///	ASSESSORES
        ///	)
        ///VALUES 
        ///	(
        ///	:Id,
        ///	:Name,
        ///	:Assessors
        ///	).
        /// </summary>
        internal static string InsertGroupOfAssessors {
            get {
                return ResourceManager.GetString("InsertGroupOfAssessors", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to INSERT INTO TB_CA_PERFIL (ID,NOME) VALUES (:ID,:Name).
        /// </summary>
        internal static string InsertProfile {
            get {
                return ResourceManager.GetString("InsertProfile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to INSERT INTO TB_CA_ELEMENTO_PERFIL (IDELEMENTO,IDPERFIL) VALUES (:ElementID,:ProfileID).
        /// </summary>
        internal static string InsertProfileElementRelationship {
            get {
                return ResourceManager.GetString("InsertProfileElementRelationship", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to INSERT INTO TB_CA_USUARIO
        ///	(
        ///	ID,
        ///	NOME,
        ///	EMAIL,
        ///	IDASSESSOR,
        ///	IDPERFIL,
        ///	TIPOUSUARIO,
        ///	ATIVO,
        ///	LOGIN
        ///	)
        ///VALUES 
        ///	(
        ///	:Id,
        ///	:Name,
        ///	:Email,
        ///	:AssessorId,
        ///	:ProfileId,
        ///	:Type,
        ///	:ActiveStatus,
        ///	:Login
        ///	).
        /// </summary>
        internal static string InsertUser {
            get {
                return ResourceManager.GetString("InsertUser", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to INSERT INTO TB_CA_ASSESSOR_USUARIO
        ///	(
        ///	IDUSUARIO,
        ///	IDGRUPOASSESSOR
        ///	)
        ///VALUES 
        ///	(
        ///	:UserId,
        ///	:GroupId
        ///	).
        /// </summary>
        internal static string InsertUserGroupOfAssessorsRelationShip {
            get {
                return ResourceManager.GetString("InsertUserGroupOfAssessorsRelationShip", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT		E.ID ELEMENTID, E.NOME ELEMENTNAME, E.APELIDO ALIAS, E.IDPAI PARENTID, E.PAGINA ISPAGE, E.ATIVO ACTIVE
        ///							FROM  TB_CA_ELEMENTO E 
        ///							WHERE		E.ATIVO = 1 	 
        ///							ORDER BY 	1.
        /// </summary>
        internal static string LoadActiveElements {
            get {
                return ResourceManager.GetString("LoadActiveElements", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT 
        ///U.ID USERID,
        ///U.LOGIN, 
        ///U.NOME USERNAME, 
        ///U.EMAIL, 
        ///U.IDASSESSOR ASSESSORID, 
        ///U.IDPERFIL PROFILEID, 
        ///P.NOME PROFILENAME, 
        ///U.TIPOUSUARIO TYPE, 
        ///U.ATIVO STATUS 
        ///FROM TB_CA_USUARIO U, TB_CA_PERFIL P WHERE U.IDPERFIL = P.ID(+)  AND U.ATIVO = 1 AND U.ID = :userID.
        /// </summary>
        internal static string LoadActiveUser {
            get {
                return ResourceManager.GetString("LoadActiveUser", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT U.ID USERID, U.NOME USERNAME, U.EMAIL, U.IDASSESSOR ASSESSORID, U.IDPERFIL PROFILEID, P.NOME PROFILENAME, U.TIPOUSUARIO TYPE, U.ATIVO STATUS, U.BLOQUEADO BLOCKED,  U.LOGIN 
        ///FROM TB_CA_USUARIO U, TB_CA_PERFIL P 
        ///WHERE U.IDPERFIL = P.ID(+) AND U.ATIVO = 1.
        /// </summary>
        internal static string LoadActiveUsers {
            get {
                return ResourceManager.GetString("LoadActiveUsers", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT		E.ID ELEMENTID, E.NOME ELEMENTNAME, E.APELIDO ALIAS, E.IDPAI PARENTID, E.PAGINA ISPAGE, E.ATIVO ACTIVE
        ///							FROM		TB_CA_ELEMENTO E 	 
        ///							ORDER BY 	1.
        /// </summary>
        internal static string LoadElements {
            get {
                return ResourceManager.GetString("LoadElements", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT		E.ID ELEMENTID, E.NOME ELEMENTNAME, E.APELIDO ALIAS, E.IDPAI PARENTID, E.PAGINA ISPAGE, E.ATIVO ACTIVE
        ///FROM  TB_CA_ELEMENTO E INNER JOIN TB_CA_ELEMENTO_PERFIL P ON P.IDELEMENTO = E.ID
        ///WHERE		E.ATIVO = 1 AND P.IDPERFIL = :PROFILEID
        ///ORDER BY 	1.
        /// </summary>
        internal static string LoadElementsByProfile {
            get {
                return ResourceManager.GetString("LoadElementsByProfile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT		E.ID ELEMENTID
        ///FROM  TB_CA_ELEMENTO E INNER JOIN TB_CA_ELEMENTO_PERFIL P ON P.IDELEMENTO = E.ID
        ///WHERE		E.ATIVO = 1 AND P.IDPERFIL = :ProfileID.
        /// </summary>
        internal static string LoadElementsIDByProfile {
            get {
                return ResourceManager.GetString("LoadElementsIDByProfile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT  DISTINCT 
        ///        A.ID GROUPID, 
        ///        A.NOME GROUPNAME, 
        ///        A.ASSESSORES ASSESSORS  
        ///FROM         TB_CA_GRUPO_ASSESSORES A LEFT OUTER JOIN TB_CA_ASSESSOR_USUARIO AU ON (A.ID = AU.IDGRUPOASSESSOR)
        ///JOIN    TB_CA_USUARIO U ON (AU.IDUSUARIO = U.ID)
        ///WHERE (U.ID = :ID)
        ///ORDER BY     2.
        /// </summary>
        internal static string LoadGroupOfAssessorsPerUser {
            get {
                return ResourceManager.GetString("LoadGroupOfAssessorsPerUser", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT 				U.ID USERID, 
        ///					U.NOME USERNAME, 
        ///					U.EMAIL, 
        ///					U.IDASSESSOR ASSESSORID, 
        ///					U.IDPERFIL PROFILEID, 
        ///					P.NOME PROFILENAME
        ///FROM 	 			TB_CA_ASSESSOR_USUARIO AU
        ///INNER JOIN 			TB_CA_GRUPO_ASSESSORES GA ON AU.IDGRUPOASSESSOR = GA.ID
        ///INNER JOIN			TB_CA_USUARIO U ON AU.IDUSUARIO = U.ID
        ///LEFT OUTER JOIN 	TB_CA_PERFIL P ON U.IDPERFIL = P.ID
        ///WHERE				GA.ID = :GroupId.
        /// </summary>
        internal static string LoadGroupOfAssessorsUsers {
            get {
                return ResourceManager.GetString("LoadGroupOfAssessorsUsers", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT 		DISTINCT A.ID GROUPID, 
        ///			A.NOME GROUPNAME, 
        ///			A.ASSESSORES ASSESSORS  
        ///FROM 		TB_CA_GRUPO_ASSESSORES A LEFT OUTER JOIN TB_CA_ASSESSOR_USUARIO AU ON (A.ID = AU.IDGRUPOASSESSOR)
        ///LEFT OUTER JOIN	TB_CA_USUARIO U ON (AU.IDUSUARIO = U.ID)
        ///WHERE	 	(:GroupId = 0 OR A.ID = :GroupId)
        ///AND			(:UserName IS NULL OR U.NOME = :UserName)
        ///ORDER BY 	2.
        /// </summary>
        internal static string LoadGroupsOfAssessors {
            get {
                return ResourceManager.GetString("LoadGroupsOfAssessors", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT g.* FROM TB_CA_GRUPO_ASSESSORES g JOIN TB_CA_ASSESSOR_USUARIO u on g.ID = u.IDGRUPOASSESSOR AND (u.IDUSUARIO = :userId).
        /// </summary>
        internal static string LoadGroupsOfAssessorsPerUser {
            get {
                return ResourceManager.GetString("LoadGroupsOfAssessorsPerUser", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT NOME PROFILENAME FROM TB_CA_PERFIL
        ///WHERE ID = :PROFILEID.
        /// </summary>
        internal static string LoadProfile {
            get {
                return ResourceManager.GetString("LoadProfile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT ID PROFILEID, NOME PROFILENAME FROM TB_CA_PERFIL.
        /// </summary>
        internal static string LoadProfiles {
            get {
                return ResourceManager.GetString("LoadProfiles", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT P.ID, COUNT(E.IDELEMENTO) AS ELEMENTOS
        ///FROM TB_CA_PERFIL P LEFT JOIN TB_CA_ELEMENTO_PERFIL E 
        ///ON E.IDPERFIL = P.ID WHERE P.ID &lt;&gt; :ProfileID
        ///GROUP BY P.ID.
        /// </summary>
        internal static string LoadProfilesAndElementCount {
            get {
                return ResourceManager.GetString("LoadProfilesAndElementCount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT P.ID, P.NOME, COUNT(U.ID) AS USUARIOS FROM TB_CA_PERFIL P
        ///LEFT JOIN TB_CA_USUARIO U ON U.IDPERFIL = P.ID
        ///WHERE (:ProfileId = 0 OR P.ID = :ProfileId) AND (:UserName IS NULL OR U.NOME = :UserName)
        ///GROUP BY P.ID, P.NOME {0} ORDER BY P.NOME.
        /// </summary>
        internal static string LoadProfilesAndUserCount {
            get {
                return ResourceManager.GetString("LoadProfilesAndUserCount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT U.ID USERID, U.NOME USERNAME, U.EMAIL, U.IDASSESSOR ASSESSORID, U.IDPERFIL PROFILEID, P.NOME PROFILENAME, U.TIPOUSUARIO &quot;TYPE&quot;, U.BLOQUEADO, U.LOGIN
        ///FROM TB_CA_USUARIO U, TB_CA_PERFIL P WHERE U.IDPERFIL = P.ID AND  U.EMAIL = :Email.
        /// </summary>
        internal static string LoadUserByEmail {
            get {
                return ResourceManager.GetString("LoadUserByEmail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT U.ID USERID, U.NOME NAME
        ///FROM TB_CA_USUARIO U WHERE  U.ATIVO = 1 AND U.IDPERFIL = :PROFILEID.
        /// </summary>
        internal static string LoadUserByProfile {
            get {
                return ResourceManager.GetString("LoadUserByProfile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT 	    TO_CHAR(U.IDASSESSOR) ASSESSORS
        ///FROM 		TB_CA_USUARIO U
        ///WHERE 		U.ID = :UserId
        ///UNION	
        ///SELECT 		GA.ASSESSORES ASSESSORS
        ///FROM		TB_CA_ASSESSOR_USUARIO AU
        ///INNER JOIN	TB_CA_GRUPO_ASSESSORES GA ON AU.IDGRUPOASSESSOR = GA.ID
        ///WHERE		AU.IDUSUARIO = :UserId.
        /// </summary>
        internal static string LoadUserListOfAssessors {
            get {
                return ResourceManager.GetString("LoadUserListOfAssessors", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT						P.ID PROFILEID, 
        ///							P.NOME PROFILENAME, 
        ///							E.ID ELEMENTID, 
        ///							E.NOME ELEMENTNAME, 
        ///							E.APELIDO ALIAS, 
        ///							E.IDPAI PARENTID, 
        ///							E.PAGINA ISPAGE, 
        ///							E.ATIVO ACTIVE
        ///							FROM		TB_CA_USUARIO U, TB_CA_PERFIL P, 
        ///										TB_CA_ELEMENTO_PERFIL EP, TB_CA_ELEMENTO E 
        ///							WHERE		U.EMAIL = :Email
        ///							AND			U.IDPERFIL = P.ID
        ///							AND 		P.ID = EP.IDPERFIL
        ///							AND			EP.IDELEMENTO = E.ID AND 		E.ATIVO = 1 	 
        ///							ORDER BY 	3.
        /// </summary>
        internal static string LoadUserProfile {
            get {
                return ResourceManager.GetString("LoadUserProfile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT ID, NOME FROM TB_CA_USUARIO WHERE IDPERFIL IS NULL.
        /// </summary>
        internal static string LoadUsersWithoutProfile {
            get {
                return ResourceManager.GetString("LoadUsersWithoutProfile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to UPDATE TB_CA_ELEMENTO SET APELIDO=:Alias WHERE ID=:Id.
        /// </summary>
        internal static string UpdateElementAlias {
            get {
                return ResourceManager.GetString("UpdateElementAlias", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to UPDATE TB_CA_GRUPO_ASSESSORES SET NOME = :GroupName, ASSESSORES = :Assessors 
        ///WHERE ID= :GroupId.
        /// </summary>
        internal static string UpdateGroupOfAssessors {
            get {
                return ResourceManager.GetString("UpdateGroupOfAssessors", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to UPDATE TB_CA_USUARIO SET IDPERFIL = :DBNull WHERE IDPERFIL = :ProfileID.
        /// </summary>
        internal static string UpdateOldUsersProfileToNull {
            get {
                return ResourceManager.GetString("UpdateOldUsersProfileToNull", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to UPDATE TB_CA_PERFIL SET NOME = :Name WHERE ID = :ID.
        /// </summary>
        internal static string UpdateProfile {
            get {
                return ResourceManager.GetString("UpdateProfile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to UPDATE TB_CA_USUARIO
        ///SET NOME = :Name, EMAIL = :Email, IDASSESSOR = :AssessorId,
        ///IDPERFIL = :ProfileId, TIPOUSUARIO = :Type, ATIVO = :ActiveStatus WHERE ID = :Id.
        /// </summary>
        internal static string UpdateUser {
            get {
                return ResourceManager.GetString("UpdateUser", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to UPDATE TB_CA_USUARIO SET IDPERFIL = :ProfileID WHERE ID = :UserID.
        /// </summary>
        internal static string UpdateUserProfile {
            get {
                return ResourceManager.GetString("UpdateUserProfile", resourceCulture);
            }
        }
    }
}
