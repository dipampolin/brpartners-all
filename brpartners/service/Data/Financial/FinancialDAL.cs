﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QX3.Spinnex.Common.Services.Data;
using QX3.Portal.Contracts.DataContracts;
using System.Data;
using System.Data.OracleClient;
using System.Data.Common;

namespace QX3.Portal.Services.Data.Financial
{
    public class FinancialDAL : DataProviderBase
    {
        public FinancialDAL() : base("PortalConnectionString") { }

        public List<ProjectedReportItem> LoadProjectedReport(ProjectedReportFilter filter, int? userId)
        {
            var list = new List<ProjectedReportItem>();
            var cmd = dbi.CreateCommand("PCK_PORTAL_PROJETADO.PROC_CONSULTA_PROJETADO", CommandType.StoredProcedure);

            if (string.IsNullOrEmpty(filter.Assessors))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTASSESSOR", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTASSESSOR", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.Assessors });
            if (string.IsNullOrEmpty(filter.Clients))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTCLIENTE", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTCLIENTE", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.Clients });

            if (userId.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = userId.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });

            if (string.IsNullOrEmpty(filter.TradingDate))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_NEGOCIO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_NEGOCIO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.TradingDate });
            if (string.IsNullOrEmpty(filter.SettlementDate))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_LIQUIDACAO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_LIQUIDACAO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.SettlementDate });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_NEGATIVO", OracleType = OracleType.Char, Direction = ParameterDirection.Input, Value = filter.Type });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSOR", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output, });

            using (var dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    var item = new ProjectedReportItem();

                    if (!(dr["CD_CLIENTE"] is DBNull))
                        item.ClientID = dr["CD_CLIENTE"].ToString();

                    if (!(dr["NM_CLIENTE"] is DBNull))
                        item.ClientName = dr["NM_CLIENTE"].ToString();

                    if (!(dr["NM_ASSESSOR"] is DBNull))
                        item.Assessor = dr["NM_ASSESSOR"].ToString();

                    if (!(dr["DT_NEGOCIO"] is DBNull))
                        item.TradingDate = Convert.ToDateTime(dr["DT_NEGOCIO"]);

                    if (!(dr["Pc_Corcor_Prin"] is DBNull))
                        item.Percentage = Convert.ToDecimal(dr["Pc_Corcor_Prin"]);

                    if (!(dr["NR_NOTA"] is DBNull))
                        item.Note = Convert.ToInt32(dr["NR_NOTA"]);

                    if (!(dr["VL_VOLUME"] is DBNull))
                        item.Volume = Convert.ToDecimal(dr["VL_VOLUME"]);

                    if (!(dr["VL_CORRETAGEM"] is DBNull))
                        item.Brokerage = Convert.ToDecimal(dr["VL_CORRETAGEM"]);

                    if (!(dr["VL_REGISTRO"] is DBNull))
                        item.Record = Convert.ToDecimal(dr["VL_REGISTRO"]);

                    if (!(dr["VL_EMOLUMENTOS"] is DBNull))
                        item.Fees = Convert.ToDecimal(dr["VL_EMOLUMENTOS"]);

                    if (!(dr["DT_LIQUIDACAO"] is DBNull))
                        item.SettlementDate = Convert.ToDateTime(dr["DT_LIQUIDACAO"]);

                    if (!(dr["VL_TAXANA"] is DBNull))
                        item.ANA = Convert.ToDecimal(dr["VL_TAXANA"]);

                    if (!(dr["VL_IRRF"] is DBNull))
                        item.IRRF = Convert.ToDecimal(dr["VL_IRRF"]);

                    if (!(dr["VL_LIQUIDO"] is DBNull))
                        item.NetValue = Convert.ToDecimal(dr["VL_LIQUIDO"]);

                    list.Add(item);
                }
            }

            return list;
        }

        #region Redemption Request
        public List<RedemptionRequestItem> LoadRedemptionRequest(RedemptionRequestFilter filter, int? userId)
        {
            var list = new List<RedemptionRequestItem>();
            var cmd = dbi.CreateCommand("PCK_PORTAL_RESGATE.PROC_CONSULTA_RESGATE", CommandType.StoredProcedure);

            #region Oracle Parameters

            if (filter.StatusID.Equals(0))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_SITUACAO", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_SITUACAO", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = filter.StatusID });

            if (string.IsNullOrEmpty(filter.StartDate))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_INICIO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_INICIO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.StartDate });

            if (string.IsNullOrEmpty(filter.EndDate))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_FIM", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_FIM", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.EndDate });

            if (string.IsNullOrEmpty(filter.Assessors))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTASSESSOR", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTASSESSOR", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.Assessors });

            if (string.IsNullOrEmpty(filter.Clients))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTCLIENTE", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTCLIENTE", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.Clients });

            if (userId.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = userId.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pRESGATE", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            #endregion

            var dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                    var item = new RedemptionRequestItem();

                    if (!(dr["ID_SOLICITACAO"] is DBNull))
                        item.ID = Convert.ToInt32(dr["ID_SOLICITACAO"]);

                    if (!(dr["CD_CLIENTE"] is DBNull) && !(dr["NM_CLIENTE"] is DBNull))
                        item.Client = new CommonType { Value = Convert.ToString(dr["CD_CLIENTE"]), Description = Convert.ToString(dr["NM_CLIENTE"]) };

                    if (!(dr["VL_RESGATE"] is DBNull))
                        item.RedemptionValue = Convert.ToDecimal(dr["VL_RESGATE"]);

                    if (!(dr["DT_SOLICITACAO"] is DBNull))
                        item.RequestDate = Convert.ToDateTime(dr["DT_SOLICITACAO"]);

                    RedemptionRequestBankAccount bankAccount = new RedemptionRequestBankAccount();

                    if (!(dr["CD_BANCO"] is DBNull))
                        bankAccount.BankCode = Convert.ToString(dr["CD_BANCO"]);

                    if (!(dr["NM_BANCO"] is DBNull))
                        bankAccount.BankName = Convert.ToString(dr["NM_BANCO"]);

                    if (!(dr["CD_AGENCIA"] is DBNull))
                        bankAccount.AgencyNumber = Convert.ToString(dr["CD_AGENCIA"]);

                    if (!(dr["DV_AGENCIA"] is DBNull))
                        bankAccount.AgencyDigit = Convert.ToString(dr["DV_AGENCIA"]);

                    if (!(dr["NR_CONTA"] is DBNull))
                        bankAccount.AccountNumber = Convert.ToString(dr["NR_CONTA"]);

                    if (!(dr["DV_CONTA"] is DBNull))
                        bankAccount.AccountDigit = Convert.ToString(dr["DV_CONTA"]);

                    if (!(dr["TP_CONTA"] is DBNull))
                        bankAccount.AccountType = Convert.ToString(dr["TP_CONTA"]);

                    if (!(dr["IN_SITUACAO"] is DBNull) && !(dr["SITUACAO_DESCRICAO"] is DBNull))
                        item.Status = new CommonType { Value = Convert.ToString(dr["IN_SITUACAO"]), Description = Convert.ToString(dr["SITUACAO_DESCRICAO"]) };

                    if (!(dr["DS_OBSERVACAO"] is DBNull))
                        item.Observation = dr["DS_OBSERVACAO"].ToString();

                    item.BankAccount = bankAccount;

                    list.Add(item);
            }
            
            return list;
        }

        public bool DeleteRedemptionRequest(int id, string clientId)
        {
            var list = new List<RedemptionRequestItem>();
            var cmd = dbi.CreateCommand("PCK_PORTAL_RESGATE.PROC_MANTEM_SOLICITACAO", CommandType.StoredProcedure);

            #region Oracle Parameters
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_SOLICITACAO", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = id });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = Convert.ToInt32(clientId) });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pVL_RESGATE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_BANCO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_AGENCIA", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pDV_AGENCIA", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pNR_CONTA", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pDV_CONTA", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pTP_CONTA", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_SITUACAO", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_SOLICITACAO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pDS_OBSERVACAO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pTP_OPERACAO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = "D" });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_SOLICITACAO_REQUEST", OracleType = OracleType.Number, Direction = ParameterDirection.Output });
            #endregion

            return cmd.ExecuteNonQuery() > 0;
        }

        public RedemptionRequestResult InsertRedemptionRequest(RedemptionRequestItem item)
        {
            var list = new List<RedemptionRequestItem>();
            var cmd = dbi.CreateCommand("PCK_PORTAL_RESGATE.PROC_MANTEM_SOLICITACAO", CommandType.StoredProcedure);

            #region Oracle Parameters

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_SOLICITACAO", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = item.ID });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = Convert.ToInt32(item.Client.Value) });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pVL_RESGATE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = item.RedemptionValue });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_BANCO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = item.BankAccount.BankCode });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_AGENCIA", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = item.BankAccount.AgencyNumber });

            if (string.IsNullOrEmpty(item.BankAccount.AgencyDigit))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDV_AGENCIA", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDV_AGENCIA", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = item.BankAccount.AgencyDigit });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pNR_CONTA", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = item.BankAccount.AccountNumber });
            
            if (string.IsNullOrEmpty(item.BankAccount.AccountDigit))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDV_CONTA", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDV_CONTA", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = item.BankAccount.AccountDigit });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pTP_CONTA", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = item.BankAccount.AccountType });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_SITUACAO", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = 17 }); // status = Para Aprovar
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_SOLICITACAO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DateTime.Now.ToString("dd/MM/yyyy HH:mm") });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pDS_OBSERVACAO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pTP_OPERACAO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = item.ID.Equals(0) ? "I" : "U" });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_SOLICITACAO_REQUEST", OracleType = OracleType.Number, Direction = ParameterDirection.Output });

            #endregion

            var r = cmd.ExecuteNonQuery() > 0;

            RedemptionRequestResult result = new RedemptionRequestResult();
            result.Result = r;
            result.Id = Convert.ToInt32(cmd.Parameters["pID_SOLICITACAO_REQUEST"].Value);

            return result;
        }

        public RedemptionRequestBalance LoadRedemptionRequestBalance(string clientCode, string assessor, int? userId)
        {
            RedemptionRequestBalance balance = null;
            DbCommand cmd = dbi.CreateCommand("PCK_PORTAL_RESGATE.PROC_CONSULTA_SALDO", CommandType.StoredProcedure);

            #region Oracle Parameters

            cmd.Parameters.Add(new OracleParameter
            {
                ParameterName = "pCD_CLIENTE",
                OracleType = OracleType.Number,
                Direction = ParameterDirection.Input,
                Value = Convert.ToInt32(clientCode)
            });

            if (String.IsNullOrEmpty(assessor))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTASSESSOR", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTASSESSOR", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = assessor });

            if (userId.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = userId.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });

            cmd.Parameters.Add(new OracleParameter
            {
                ParameterName = "pSALDO",
                OracleType = OracleType.Cursor,
                Direction = ParameterDirection.Output
            });

            cmd.Parameters.Add(new OracleParameter
            {
                ParameterName = "pPROJETADO",
                OracleType = OracleType.Cursor,
                Direction = ParameterDirection.Output
            });

            #endregion

            using (DbDataReader dr = cmd.ExecuteReader())
            {
                if (dr.Read())
                {
                    balance = new RedemptionRequestBalance();

                    if (!(dr["vl_disponivel_real"] is DBNull))
                        balance.Available = Convert.ToDecimal(dr["vl_disponivel_real"]);

                    if (!(dr["vl_projetado_d1"] is DBNull))
                        balance.AvailableD1 = Convert.ToDecimal(dr["vl_projetado_d1"]);

                    if (!(dr["vl_projetado_d2"] is DBNull))
                        balance.AvailableD2 = Convert.ToDecimal(dr["vl_projetado_d2"]);

                    if (!(dr["vl_projetado_d3"] is DBNull))
                        balance.AvailableD3 = Convert.ToDecimal(dr["vl_projetado_d3"]);

                    if (dr.NextResult())
                    {
                        if (dr.Read())
                        {
                            if (!(dr["vl_total"] is DBNull))
                                balance.Total = Convert.ToDecimal(dr["vl_total"]);

                            if (!(dr["vl_projetado"] is DBNull))
                                balance.Intended = Convert.ToDecimal(dr["vl_projetado"]);
                        }
                    }
                    dr.Close();
                }
                return balance;
            }
        }

        public List<RedemptionRequestBankAccount> LoadBankAccountInformation(string clientCode)
        {
            var list = new List<RedemptionRequestBankAccount>();
            var cmd = dbi.CreateCommand("PCK_PORTAL_RESGATE.PROC_BUSCA_CONTA_CORRENTE", CommandType.StoredProcedure);

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = Convert.ToInt32(clientCode) });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCONTACORRENTE", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            var dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                var item = new RedemptionRequestBankAccount();

                if (!(dr["CD_BANCO"] is DBNull))
                    item.BankCode = Convert.ToString(dr["CD_BANCO"]).Trim();

                if (!(dr["NM_BANCO"] is DBNull))
                    item.BankName = Convert.ToString(dr["NM_BANCO"]).Trim();

                if (!(dr["CD_AGENCIA"] is DBNull))
                    item.AgencyNumber = Convert.ToString(dr["CD_AGENCIA"]).Trim();

                if (!(dr["DV_AGENCIA"] is DBNull))
                    item.AgencyDigit = Convert.ToString(dr["DV_AGENCIA"]).Trim();

                if (!(dr["NR_CONTA"] is DBNull))
                    item.AccountNumber = Convert.ToString(dr["NR_CONTA"]).Trim();

                if (!(dr["DV_CONTA"] is DBNull))
                    item.AccountDigit = Convert.ToString(dr["DV_CONTA"]).Trim();

                if (!(dr["TP_CONTA"] is DBNull))
                    item.AccountType = Convert.ToString(dr["TP_CONTA"]).Trim();

                list.Add(item);
            }

            return list;
        }

        public bool ChangeRedemptionRequestStatus(RedemptionRequestItem item)
        {
            var list = new List<RedemptionRequestItem>();
            var cmd = dbi.CreateCommand("PCK_PORTAL_RESGATE.PROC_MANTEM_SOLICITACAO", CommandType.StoredProcedure);

            #region Oracle Parameters

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_SOLICITACAO", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = item.ID });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = Convert.ToInt32(item.Client.Value) });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pVL_RESGATE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_BANCO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_AGENCIA", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pDV_AGENCIA", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pNR_CONTA", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pDV_CONTA", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pTP_CONTA", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_SITUACAO", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = Convert.ToInt32(item.Status.Value) });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_SOLICITACAO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            if (string.IsNullOrEmpty(item.Observation))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDS_OBSERVACAO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDS_OBSERVACAO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = item.Observation });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pTP_OPERACAO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = "S" });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_SOLICITACAO_REQUEST", OracleType = OracleType.Number, Direction = ParameterDirection.Output });
            #endregion

            return cmd.ExecuteNonQuery() > 0;
        }

        public int GetPendentRedemptionRequest()
        {
            var list = new List<RedemptionRequestItem>();
            var cmd = dbi.CreateCommand("PCK_PORTAL_RESGATE.PROC_TOTAL_PENDENTES", CommandType.StoredProcedure);

            #region Oracle Parameters

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCount", OracleType = OracleType.Number, Direction = ParameterDirection.Output });

            #endregion

            cmd.ExecuteNonQuery();

            return Convert.ToInt32(cmd.Parameters["pCount"].Value);
        }
        #endregion

        public List<DailyFinancial> LoadDailyFinancialReport(DailyFinancialFilter filter, FilterOptions options, int? userId, out int count) 
        {
            List<DailyFinancial> result = null; count = 0;

            var cmd = dbi.CreateCommand("PCK_PORTAL_FINANCEIRO.PROC_SALDO_DEVEDOR", CommandType.StoredProcedure);
            cmd.Parameters.Clear();
                        

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_LIQUIDA_D0", IsNullable = true, OracleType = OracleType.Char, Direction = ParameterDirection.Input, Value = (filter.D0) ? 'S' : 'N' });

            if (filter.Situation.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_SITUACAO", IsNullable = true, OracleType = OracleType.Char, Direction = ParameterDirection.Input, Value = filter.Situation.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_SITUACAO", IsNullable = true, OracleType = OracleType.Char, Direction = ParameterDirection.Input, Value = DBNull.Value });

            if (filter.CustodyTax.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_TAXA_CUSTODIA", IsNullable = true, OracleType = OracleType.Char, Direction = ParameterDirection.Input, Value = (filter.CustodyTax.Value) ? 'S' : 'N' });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_TAXA_CUSTODIA", IsNullable = true, OracleType = OracleType.Char, Direction = ParameterDirection.Input, Value = DBNull.Value });
            
            if (String.IsNullOrEmpty(filter.AssessorFilter))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTASSESSOR", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTASSESSOR", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.AssessorFilter });

            if (filter.ClientFilter == null)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTCLIENTE", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTCLIENTE", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.ClientFilter });

            if (userId.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = userId.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });

            if (filter.MovementDate.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_MOVIMENTO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.MovementDate.Value.ToString("dd/MM/yyyy") });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_MOVIMENTO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pINITROW", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = options.InitRow });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pFINALROW", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = options.FinalRow });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pSORTCOLUMN", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = options.SortColumn });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pSORTDIR", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = options.SortDirection });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pTODOS", IsNullable = true, OracleType = OracleType.Int32, Direction = ParameterDirection.Input, Value = (options.AllRecords) ? 1 : 0 });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCount", OracleType = OracleType.Number, Direction = ParameterDirection.Output });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCursor", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    count = Convert.ToInt32(cmd.Parameters["pCount"].Value);

                    result = new List<DailyFinancial>();
                    while (dr.Read())
                    {
                        DailyFinancial daily = new DailyFinancial();

                        /*
                            rowData.Add(daily.AssessorName);
                            rowData.Add(daily.ClientID.ToString() + "-" + daily.DV.ToString());
                            rowData.Add(daily.ClientName);
                            rowData.Add(daily.AnteriorBalance.ToString("n")); 
                            rowData.Add(daily.AccountBalance.ToString("n"));
                            rowData.Add(daily.Credit.ToString("n"));
                            rowData.Add(daily.Debt.ToString("n"));
                            rowData.Add(daily.Finished.ToString("n"));
                            rowData.Add(daily.LiquidResult.ToString("n"));
                            rowData.Add(daily.ProjectedBalance.ToString("n"));
                            rowData.Add(daily.FinalBalance.ToString("n"));   
                         */

                        int counter = -1;

                        if (!dr.IsDBNull(++counter)) daily.ClientID = dr.GetInt32(counter);                                      //cd_cliente
                        if (!dr.IsDBNull(++counter)) daily.ClientName = dr.GetString(counter);                                   //Nm_Cliente
                        if (!dr.IsDBNull(++counter)) daily.AssessorName = dr.GetString(counter);                                 //Nm_Assessor
                        if (!dr.IsDBNull(++counter)) daily.AnteriorBalance = dr.GetDecimal(counter);                             //vl_saldo_Ant
                        if (!dr.IsDBNull(++counter)) daily.AccountBalance = dr.GetDecimal(counter);                              //vl_saldo
                        if (!dr.IsDBNull(++counter)) daily.Credit = dr.GetDecimal(counter);                                      //vl_gera_credito
                        if (!dr.IsDBNull(++counter)) daily.Debt = dr.GetDecimal(counter);                                        //vl_gera_debito
                        if (!dr.IsDBNull(++counter)) daily.Finished = dr.GetDecimal(counter);                                    //vl_liquidado
                        if (!dr.IsDBNull(++counter)) daily.LiquidResult = dr.GetDecimal(counter);                                //Vl_Resultado
                        if (!dr.IsDBNull(++counter)) daily.ProjectedBalance = dr.GetDecimal(counter);                            //vl_projetado
                        if (!dr.IsDBNull(++counter)) daily.FinalBalance = dr.GetDecimal(counter);                                //vl_saldo_final
                        if (!dr.IsDBNull(++counter)) daily.CustodyTax = (dr.GetString(counter) == "N") ? false : true;           //in_taxa_custodia
                        if (!dr.IsDBNull(++counter)) daily.DV = dr.GetInt32(counter);                                            //Dv_Cliente
                        if (!dr.IsDBNull(++counter)) daily.AssessorID = dr.GetInt32(counter);                                    //cd_assessor

                        result.Add(daily);
                    }
                }

                dr.Close();

            }

            return result;
        }
    }
}
