﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using QX3.Portal.Contracts.DataContracts;
using QX3.Spinnex.Common.Services.Data;
using System.Linq;
using System.Data.SqlClient;

namespace QX3.Portal.Services.Data
{
    public class PositionDAL : DataProviderBase
    {
        public PositionDAL() : base("PortalConnectionString") { }

        public CustodyPositionResponse ListCustodyPosition(int cdCliente, string cdMercado)
        {

            List<CustodyPosition> result = new List<CustodyPosition>();

            CustodyPosition cp = new CustodyPosition();

            var cmd = dbi.CreateCommand("PCK_POSICAO.PROC_POSICAO_CUSTODIA", CommandType.StoredProcedure);
            cmd.Parameters.Clear();


            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = cdCliente });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pMERCADO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = cdMercado == "TDO" ? "" : cdMercado });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCUSTODIA", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        cp = new CustodyPosition();

                        cp.Ativo = dr["cod_neg"] != DBNull.Value ? dr["cod_neg"].ToString() : "";
                        cp.NomeCarteira = dr["DESCRICAO_CART"] != DBNull.Value ? dr["DESCRICAO_CART"].ToString() : "";
                        cp.NumeroCarteira = dr["COD_CARTEIRA"] != DBNull.Value ? Convert.ToInt32(dr["COD_CARTEIRA"]) : 0;
                        cp.QtdDisponivel = dr["QTDE_DIPS"] != DBNull.Value ? Convert.ToInt32(dr["QTDE_DIPS"]) : 0;
                        cp.QtdTotal = dr["QTDE_TOTAL"] != DBNull.Value ? Convert.ToInt32(dr["QTDE_TOTAL"]) : 0;
                        cp.QtdOpLiquidar = dr["QTDE_PROJ"] != DBNull.Value ? Convert.ToInt32(dr["QTDE_PROJ"]) : 0;
                        cp.QtdBloqueada = dr["QTDE_BLOQ"] != DBNull.Value ? Convert.ToInt32(dr["QTDE_BLOQ"]) : 0;
                        cp.QtdComprasExec= dr["QTDE_EXEC_COMPRA"] != DBNull.Value ? Convert.ToInt32(dr["QTDE_EXEC_COMPRA"]) : 0;
                        cp.QtdComprasAbertas = dr["QTDE_ABERT_COMPRA"] != DBNull.Value ? Convert.ToInt32(dr["QTDE_ABERT_COMPRA"]) : 0;
                        cp.QtdVendasExec = dr["QTDE_EXEC_VENDA"] != DBNull.Value ? Convert.ToInt32(dr["QTDE_EXEC_VENDA"]) : 0;
                        cp.QtdVendasAbertas = dr["QTDE_ABERT_VENDA"] != DBNull.Value ? Convert.ToInt32(dr["QTDE_ABERT_VENDA"]) : 0;
                        cp.ValorAtual = dr["VALOR_POSICAO"] != DBNull.Value ? Convert.ToDecimal(dr["VALOR_POSICAO"]) : 0;

                        result.Add(cp);


                    }

                }
                dr.Close();
            }

            CustodyPositionResponse cr = new CustodyPositionResponse();

            cr.CustodyPosition = result;
            cr.TipoMercado = cdMercado;
            cr.CdBolsa = cdCliente;

            return cr;

        }

        public ConsolidatedPositionResponse2 ListConsolidatedPosition(int cdCliente)
        {

            List<ConsolidatedPosition2> result = new List<ConsolidatedPosition2>();
            ConsolidatedPosition2 cp = new ConsolidatedPosition2();

            ConsolidatedPositionResponse2 cpr = new ConsolidatedPositionResponse2();
            cpr.CdBolsa = cdCliente;

            var cmd = dbi.CreateCommand("PCK_POSICAO.PROC_POSICAO_CONSOLIDADA", CommandType.StoredProcedure);
            cmd.Parameters.Clear();


            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = cdCliente });            

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCUSTODIA", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        cp = new ConsolidatedPosition2();

                        cp.TipoMercado = dr["TIPO_MERC"] != DBNull.Value ? dr["TIPO_MERC"].ToString() : "";
                        cp.DescTipoMercado = TipoMercado(dr["TIPO_MERC"] != DBNull.Value ? dr["TIPO_MERC"].ToString() : "");
                        cp.ValorBruto = dr["VALOR_POSICAO"] != DBNull.Value ? Convert.ToDecimal(dr["VALOR_POSICAO"]) : 0;

                        result.Add(cp);


                    }

                }
                dr.Close();
            }

            cpr.ConsolidatedPosition = result;

            return cpr;

        }

        public List<ConsolidatedPositionDetails> ListConsolidatedPositionDetails(int cdCliente, string grupo)
        {

            List<ConsolidatedPositionDetails> result = new List<ConsolidatedPositionDetails>();

            ConsolidatedPositionDetails cp = new ConsolidatedPositionDetails();

            var cmd = dbi.CreateCommand("PCK_POSICAO.PROC_POSICAO_CONSOLIDADA_DET", CommandType.StoredProcedure);
            cmd.Parameters.Clear();


            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = cdCliente });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pTIPO_MERC", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = grupo });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCUSTODIA", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        cp = new ConsolidatedPositionDetails();

                        cp.Ativo = dr["COD_NEG"] != DBNull.Value ? dr["COD_NEG"].ToString() : "";
                        cp.TipoMercado = dr["TIPO_MERC"] != DBNull.Value ? dr["TIPO_MERC"].ToString() : "";
                        cp.QtdDisponivel = dr["QTDE_DISP"] != DBNull.Value ? Convert.ToInt32(dr["QTDE_DISP"]) : 0;
                        cp.Preco = dr["PRECO_ATIVO"] != DBNull.Value ? Convert.ToDecimal(dr["PRECO_ATIVO"]) : 0;
                        cp.ValorAtual = dr["VALOR_POSICAO"] != DBNull.Value ? Convert.ToDecimal(dr["VALOR_POSICAO"]) : 0;

                        if (cp.QtdDisponivel != 0)
                            result.Add(cp);
                    }

                }
                dr.Close();
            }


            return result;

        }

        public FinancialPositionResponse FinancialPosition(int cdCliente)
        {
            FinancialPositionResponse fpr = new FinancialPositionResponse();
            FinancialPosition fp = new FinancialPosition();
            var cmd = dbi.CreateCommand("PCK_POSICAO.PROC_POSICAO_FINANCEIRA", CommandType.StoredProcedure);
            cmd.Parameters.Clear();


            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = cdCliente });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pSALDO", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pDISPONIVEL", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pORDENS", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pPROJETADO", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            Balance b = new Balance();
            
            List<Available> la = new List<Available>();
            Available a = new Available();

            Orders o = new Orders();

            Projected p = new Projected();

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    // SALDO
                    if (dr.Read())
                    {
                        b.Inicial = dr["INICIAL"] != DBNull.Value ? Convert.ToDecimal(dr["INICIAL"]) : 0;
                        b.MovimentoDia = dr["MOVIMENTO_DIA"] != DBNull.Value ? Convert.ToDecimal(dr["MOVIMENTO_DIA"]) : 0;
                        b.Final = dr["FINAL"] != DBNull.Value ? Convert.ToDecimal(dr["FINAL"]) : 0;
                        
                    }

                    fp.Saldo = b;

                    //DISPONIVEL
                    if (dr.NextResult())
                    {
                        while (dr.Read())
                        {
                            a = new Available();

                            a.Mercado = dr["CD_TIPO_MERC"] != DBNull.Value ? dr["CD_TIPO_MERC"].ToString() : "";
                            a.DescMercado = TipoMercado(dr["CD_TIPO_MERC"] != DBNull.Value ? dr["CD_TIPO_MERC"].ToString() : "");
                            a.ValorDisponivel = dr["VALOR_POSICAO"] != DBNull.Value ? Convert.ToDecimal(dr["VALOR_POSICAO"]) : 0;

                            la.Add(a);
                        }
                    }

                    fp.Disponivel = la;

                    //ORDENS
                    if (dr.NextResult())
                    {
                        if (dr.Read())
                        {
                            o.ValorComprasExec = dr["VL_EXEC_COMPRA"] != DBNull.Value ? Convert.ToDecimal(dr["VL_EXEC_COMPRA"]) : 0;
                            o.ValorVendasEmAberto = dr["VL_ABERT_COMPRA"] != DBNull.Value ? Convert.ToDecimal(dr["VL_ABERT_COMPRA"]) : 0;
                            o.ValorVendasExec = dr["VL_EXEC_VENDA"] != DBNull.Value ? Convert.ToDecimal(dr["VL_EXEC_VENDA"]) : 0;
                            o.ValorVendasExec = dr["VL_ABERT_VENDA"] != DBNull.Value ? Convert.ToDecimal(dr["VL_ABERT_VENDA"]) : 0;

                        }

                    }

                    fp.Ordens = o;

                    //PROJETADO
                    if (dr.NextResult())
                    {
                        if (dr.Read())
                        {
                            p.ProjetadoD1 = dr["VL_SALDO_DATA_PROJ1"] != DBNull.Value ? Convert.ToDecimal(dr["VL_SALDO_DATA_PROJ1"]) : 0;
                            p.ProjetadoD2 = dr["VL_SALDO_DATA_PROJ2"] != DBNull.Value ? Convert.ToDecimal(dr["VL_SALDO_DATA_PROJ2"]) : 0;
                            p.ProjetadoD3 = dr["VL_SALDO_DATA_PROJ3"] != DBNull.Value ? Convert.ToDecimal(dr["VL_SALDO_DATA_PROJ3"]) : 0;
                            p.ProjetadoTotal = dr["VL_TOTAL_PROJETADO"] != DBNull.Value ? Convert.ToDecimal(dr["VL_TOTAL_PROJETADO"]) : 0;
                        }

                    }

                    fp.Projetado = p;
                }
                dr.Close();
            }

            fpr.FinancialPosition = fp;

            return fpr;

        }

        public List<InvestmentsGroups> ListInvestmentsGroups()
        {

            List<InvestmentsGroups> result = new List<InvestmentsGroups>();

            InvestmentsGroups cp = new InvestmentsGroups();

            var cmd = dbi.CreateCommand("PCK_POSICAO.PROC_GRUPOS_INVESTIMENTOS", CommandType.StoredProcedure);
            cmd.Parameters.Clear();

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pINVESTIMENTOS", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        cp = new InvestmentsGroups();

                        cp.Group = dr["tipo_grup"] != DBNull.Value ? dr["tipo_grup"].ToString() : "";
                        cp.DescGroup = dr["desc_tipo_grup"] != DBNull.Value ? dr["desc_tipo_grup"].ToString() : "";
                        

                        result.Add(cp);


                    }

                }
                dr.Close();
            }


            return result;

        }

        public List<ExtratoRendaDatails> ListExtratoRendaDatails(string codEmpresa, string cpfCnpj, string DtRefDe, string DtRefAte)
        {

            List<ExtratoRendaDatails> result = new List<ExtratoRendaDatails>();

            ExtratoRendaDatails cp = new ExtratoRendaDatails();

            DBInterface dbiCRK = new DBInterface("CRK");

            var cmd = (SqlCommand)dbiCRK.CreateCommand("SP_REN_EXTRATO", CommandType.StoredProcedure);

            cmd.Parameters.Clear();

            cmd.Parameters.Add(new SqlParameter { ParameterName = "@tp_codempresa", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input, Value = Convert.ToInt32(codEmpresa)});
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@cd_cpf_cnpj", SqlDbType = SqlDbType.Decimal, Direction = ParameterDirection.Input, Value = Convert.ToDecimal(cpfCnpj)});
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@dt_ref_de", SqlDbType = SqlDbType.Decimal, Direction = ParameterDirection.Input, Value = Convert.ToDateTime(DtRefDe) });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@dt_ref_ate", SqlDbType = SqlDbType.Decimal, Direction = ParameterDirection.Input, Value = Convert.ToDateTime(DtRefAte) });

            cmd.Connection.Open();

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        cp = new ExtratoRendaDatails();

                        cp.tp_registro = dr["tipo_grup"] != DBNull.Value ? dr["tp_registro"].ToString() : "";
                        cp.dt_operacao = dr["dt_operacao"] != DBNull.Value ? dr["dt_operacao"].ToString() : "";
                        cp.dc_evento = dr["dc_evento"] != DBNull.Value ? dr["dc_evento"].ToString() : "";
                        cp.dc_grupo_papel = dr["dc_grupo_papel"] != DBNull.Value ? dr["dc_grupo_papel"].ToString() : "";
                        cp.dc_indexador = dr["dc_indexador"] != DBNull.Value ? dr["dc_indexador"].ToString() : "";
                        cp.tx_operacao = dr["tx_operacao"] != DBNull.Value ? Convert.ToDecimal(dr["tx_operacao"]) : 0;
                        cp.tx_percentual = dr["tx_percentual"] != DBNull.Value ? Convert.ToDecimal(dr["tx_percentual"]) : 0;
                        cp.dt_vencimento = dr["dt_vencimento"] != DBNull.Value ? dr["dt_vencimento"].ToString() : "";
                        cp.vl_operacao = dr["vl_operacao"] != DBNull.Value ? Convert.ToDecimal(dr["vl_operacao"]) : 0;
                        cp.tx_ir = dr["tx_ir"] != DBNull.Value ? Convert.ToDecimal(dr["tx_ir"]) : 0;
                        cp.tx_iof = dr["tx_iof"] != DBNull.Value ? Convert.ToDecimal(dr["tx_iof"]) : 0;
                        cp.vl_bruto = dr["vl_bruto"] != DBNull.Value ? Convert.ToDecimal(dr["vl_bruto"]) : 0;
                        cp.vl_liquido = dr["vl_liquido"] != DBNull.Value ? Convert.ToDecimal(dr["vl_liquido"]) : 0;

                        result.Add(cp);
                    }

                }
                dr.Close();
            }

            cmd.Connection.Close();
            return result;

        }

        private string TipoMercado(string tipo)
        {
            switch (tipo)
            {
                case "OPV":
                    {
                        return "Opção de Venda";
                    }

                case "OPC":
                    {
                        return "Opção de Compra";
                    }
                case "TER":
                    {
                        return "Termo";
                    }
                case "VIS":
                    {
                        return "Ação";
                    }
            }

            return tipo;

        }


    }
}
