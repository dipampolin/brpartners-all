﻿using System;
using System.Collections.Generic;
using QX3.Spinnex.Common.Services.Data;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.Services.Properties;
using System.Data;
using System.Linq;
using System.Data.OracleClient;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Data.SqlClient;

namespace QX3.Portal.Services.Data.AccessControl
{
    public class AccessControlDAL : DataProviderBase
    {
        public AccessControlDAL() : base("PortalConnectionString") { }

        //public ACProfile LoadUserProfile(string email)
        //{
        //    ACProfile profile = new ACProfile();

        //    /*var cmd = dbi.CreateCommand(AccessControlResources.LoadUserProfile, CommandType.Text);
        //    cmd.Parameters.Add(dbi.CreateParam("Email", email));*/

        //    var cmd = dbi.CreateCommand("PCK_CONTROLE_ACESSO.PROC_CARREGAR_PERFIL_USUARIO", CommandType.StoredProcedure);
        //    cmd.Parameters.Add(new OracleParameter { ParameterName = "pEmail", OracleType = OracleType.VarChar, Value = email, Direction = ParameterDirection.Input });
        //    cmd.Parameters.Add(new OracleParameter { ParameterName = "pCursor", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

        //    bool first = true;
        //    List<ACElement> tempElements = new List<ACElement>();

        //    using (var dr = cmd.ExecuteReader())
        //    {
        //        while (dr.Read())
        //        {
        //            if (first)
        //            {
        //                if (!(dr["PROFILEID"] is DBNull))
        //                    profile.ID = Convert.ToInt32(dr["PROFILEID"]);
        //                if (!(dr["PROFILENAME"] is DBNull))
        //                    profile.Name = Convert.ToString(dr["PROFILENAME"]);
        //                first = false;
        //            }
        //            ACElement element = new ACElement();

        //            if (!(dr["ELEMENTID"] is DBNull))
        //                element.ID = Convert.ToInt32(dr["ELEMENTID"]);
        //            if (!(dr["ELEMENTNAME"] is DBNull))
        //                element.Name = Convert.ToString(dr["ELEMENTNAME"]);
        //            if (!(dr["ALIAS"] is DBNull))
        //                element.Alias = Convert.ToString(dr["ALIAS"]);
        //            if (!(dr["PARENTID"] is DBNull))
        //                element.ParentID = Convert.ToInt32(dr["PARENTID"]);
        //            if (!(dr["ISPAGE"] is DBNull))
        //                element.IsPage = Convert.ToBoolean(dr["ISPAGE"]);
        //            if (!(dr["ACTIVE"] is DBNull))
        //                element.Active = Convert.ToBoolean(dr["ACTIVE"]);
        //            if (!(dr["VISIVEL"] is DBNull))
        //                element.Visible = Convert.ToBoolean(dr["VISIVEL"]);

        //            tempElements.Add(element);
        //        }
        //        dr.Close();
        //    }
        //    profile.Permissions = tempElements;

        //    return profile;
        //}

        // LoadUserProfile SQLServer
        public ACProfile LoadUserProfile(string email)
        {
            ACProfile profile = new ACProfile();

            DBInterface dbiCRK = new DBInterface("PORTAL");

            var cmd = (SqlCommand)dbiCRK.CreateCommand("PROC_CARREGAR_PERFIL_USUARIO", CommandType.StoredProcedure);

            cmd.Parameters.Clear();
            cmd.Parameters.Add("@EMAIL", SqlDbType.VarChar).Value = (object)email ?? DBNull.Value;

            cmd.Connection.Open();

            bool first = true;
            List<ACElement> tempElements = new List<ACElement>();

            using (var dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    if (first)
                    {
                        if (!(dr["PROFILEID"] is DBNull))
                            profile.ID = Convert.ToInt32(dr["PROFILEID"]);
                        if (!(dr["PROFILENAME"] is DBNull))
                            profile.Name = Convert.ToString(dr["PROFILENAME"]);
                        first = false;
                    }
                    ACElement element = new ACElement();

                    if (!(dr["ELEMENTID"] is DBNull))
                        element.ID = Convert.ToInt32(dr["ELEMENTID"]);
                    if (!(dr["ELEMENTNAME"] is DBNull))
                        element.Name = Convert.ToString(dr["ELEMENTNAME"]);
                    if (!(dr["ALIAS"] is DBNull))
                        element.Alias = Convert.ToString(dr["ALIAS"]);
                    if (!(dr["PARENTID"] is DBNull))
                        element.ParentID = Convert.ToInt32(dr["PARENTID"]);
                    if (!(dr["ISPAGE"] is DBNull))
                        element.IsPage = Convert.ToBoolean(dr["ISPAGE"]);
                    if (!(dr["ACTIVE"] is DBNull))
                        element.Active = Convert.ToBoolean(dr["ACTIVE"]);
                    if (!(dr["VISIVEL"] is DBNull))
                        element.Visible = Convert.ToBoolean(dr["VISIVEL"]);

                    tempElements.Add(element);
                }
                dr.Close();
            }

            profile.Permissions = tempElements;

            cmd.Connection.Close();

            return profile;
        }

        //public ACUser LoadUser(string email)
        //{
        //    ACUser user = new ACUser();

        //    /*
        //    var cmd = dbi.CreateCommand(AccessControlResources.LoadUserByEmail, CommandType.Text);
        //    cmd.Parameters.Add(dbi.CreateParam("Email", email));*/

        //    var cmd = dbi.CreateCommand("PCK_CONTROLE_ACESSO.PROC_CARREGAR_PERFIL_POR_EMAIL", CommandType.StoredProcedure);
        //    cmd.Parameters.Add(new OracleParameter { ParameterName = "pEmail", OracleType = OracleType.VarChar, Value = email, Direction = ParameterDirection.Input });
        //    cmd.Parameters.Add(new OracleParameter { ParameterName = "pCursor", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

        //    using (var dr = cmd.ExecuteReader())
        //    {
        //        while (dr.Read())
        //        {
        //            if (!(dr["LOGIN"] is DBNull))
        //                user.Login = dr["LOGIN"].ToString();
        //            if (!(dr["USERID"] is DBNull))
        //                user.ID = Convert.ToInt32(dr["USERID"]);
        //            if (!(dr["USERNAME"] is DBNull))
        //                user.Name = Convert.ToString(dr["USERNAME"]);
        //            if (!(dr["EMAIL"] is DBNull))
        //                user.Email = Convert.ToString(dr["EMAIL"]);
        //            if (!(dr["PROFILEID"] is DBNull) && !(dr["PROFILENAME"] is DBNull))
        //                user.Profile = new ACProfile
        //                {
        //                    ID = Convert.ToInt32(dr["PROFILEID"]),
        //                    Name = Convert.ToString(dr["PROFILENAME"])
        //                };
        //            if (!(dr["ASSESSORID"] is DBNull))
        //                user.AssessorID = Convert.ToInt32(dr["ASSESSORID"]);
        //            if (!(dr["TYPE"] is DBNull))
        //                user.Type = dr["TYPE"].ToString();
        //            if (!(dr["BLOQUEADO"] is DBNull))
        //                user.Blocked = Convert.ToInt32(dr["BLOQUEADO"]).Equals(1);
        //            if (!(dr["Chave_Ad"] is DBNull))
        //                user.ChaveAD = dr["Chave_Ad"].ToString();
        //            if (!(dr["SUITABILITY_DATE"] is DBNull))
        //                user.SuitabilityDate = Convert.ToDateTime(dr["SUITABILITY_DATE"]);


        //            List<ACGroupOfAssessors> groups = new List<ACGroupOfAssessors>();

        //            var cmdGroup = dbi.CreateCommand(AccessControlResources.LoadGroupsOfAssessors, CommandType.Text);
        //            cmdGroup.Parameters.Add(dbi.CreateParam("GroupId", 0));
        //            cmdGroup.Parameters.Add(dbi.CreateParam("UserName", Convert.ToString(dr["USERNAME"])));


        //            using (var drGroup = cmdGroup.ExecuteReader())
        //            {
        //                while (drGroup.Read())
        //                {

        //                    ACGroupOfAssessors group = new ACGroupOfAssessors();

        //                    if (!(drGroup["GROUPID"] is DBNull))
        //                        group.ID = Convert.ToInt32(drGroup["GROUPID"]);
        //                    if (!(drGroup["GROUPNAME"] is DBNull))
        //                        group.Name = Convert.ToString(drGroup["GROUPNAME"]);
        //                    if (!(drGroup["ASSESSORS"] is DBNull))
        //                        group.Assessors = Convert.ToString(drGroup["ASSESSORS"]);

        //                    groups.Add(group);
        //                }
        //            }

        //            user.AssessorsGroup = groups;


        //        }
        //        dr.Close();
        //    }

        //    return user;
        //}

        //LoadUser SQLServer
        public ACUser LoadUser(string email)
        {
            ACUser user = new ACUser();

            DBInterface dbiCRK = new DBInterface("PORTAL");

            var cmd = (SqlCommand)dbiCRK.CreateCommand("PROC_CARREGAR_PERFIL_POR_EMAIL", CommandType.StoredProcedure);

            cmd.Parameters.Clear();
            cmd.Parameters.Add("@EMAIL", SqlDbType.VarChar).Value = (object)email ?? DBNull.Value;

            cmd.Connection.Open();

            using (var dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    if (!(dr["LOGIN"] is DBNull))
                        user.Login = dr["LOGIN"].ToString();
                    if (!(dr["USERID"] is DBNull))
                        user.ID = Convert.ToInt32(dr["USERID"]);
                    if (!(dr["USERNAME"] is DBNull))
                        user.Name = Convert.ToString(dr["USERNAME"]);
                    if (!(dr["EMAIL"] is DBNull))
                        user.Email = Convert.ToString(dr["EMAIL"]);
                    if (!(dr["PROFILEID"] is DBNull) && !(dr["PROFILENAME"] is DBNull))
                        user.Profile = new ACProfile
                        {
                            ID = Convert.ToInt32(dr["PROFILEID"]),
                            Name = Convert.ToString(dr["PROFILENAME"])
                        };
                    if (!(dr["ASSESSORID"] is DBNull))
                        user.AssessorID = Convert.ToInt32(dr["ASSESSORID"]);
                    if (!(dr["TYPE"] is DBNull))
                        user.Type = dr["TYPE"].ToString();
                    if (!(dr["BLOQUEADO"] is DBNull))
                        user.Blocked = Convert.ToInt32(dr["BLOQUEADO"]).Equals(1);
                    if (!(dr["Chave_Ad"] is DBNull))
                        user.ChaveAD = dr["Chave_Ad"].ToString();
                    if (!(dr["SUITABILITY_DATE"] is DBNull))
                        user.SuitabilityDate = Convert.ToDateTime(dr["SUITABILITY_DATE"]);

                    if (!(dr["ENVIAR_EMAIL_SUITABILITY"] is DBNull))
                        user.SendSuitabilityEmail = Convert.ToInt32(dr["ENVIAR_EMAIL_SUITABILITY"]) == 1 ? true : false;
                    else
                        user.SendSuitabilityEmail = true;

                    List<ACGroupOfAssessors> groups = new List<ACGroupOfAssessors>();
                    user.AssessorsGroup = null;
                }
                dr.Close();
            }

            cmd.Connection.Close();

            return user;
        }

        public List<ACElement> LoadElements(bool active)
        {
            List<ACElement> elements = new List<ACElement>();

            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PROC_CARREGAR_ELEMENTOS", CommandType.StoredProcedure);

            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pAtivo", SqlDbType = SqlDbType.Decimal, Value = (active ? 1 : 0), Direction = ParameterDirection.Input });

            cmd.Connection.Open();

            using (var dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    ACElement element = new ACElement();

                    if (!(dr["ELEMENTID"] is DBNull))
                        element.ID = Convert.ToInt32(dr["ELEMENTID"]);
                    if (!(dr["ELEMENTNAME"] is DBNull))
                        element.Name = Convert.ToString(dr["ELEMENTNAME"]);
                    if (!(dr["ALIAS"] is DBNull))
                        element.Alias = Convert.ToString(dr["ALIAS"]);
                    if (!(dr["PARENTID"] is DBNull))
                        element.ParentID = Convert.ToInt32(dr["PARENTID"]);
                    if (!(dr["ISPAGE"] is DBNull))
                        element.IsPage = Convert.ToBoolean(dr["ISPAGE"]);
                    if (!(dr["ACTIVE"] is DBNull))
                        element.Active = Convert.ToBoolean(dr["ACTIVE"]);

                    elements.Add(element);
                }
                dr.Close();
            }

            cmd.Connection.Close();

            return elements;
        }

        private List<ACElement> LoadElementsByProfile(int profileID)
        {
            List<ACElement> elements = new List<ACElement>();

            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PROC_CARREGAR_ELEMENTOS_PERFIL", CommandType.StoredProcedure);

            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pPerfil", SqlDbType = SqlDbType.Decimal, Value = profileID, Direction = ParameterDirection.Input });

            cmd.Connection.Open();

            using (var dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    ACElement element = new ACElement();

                    if (!(dr["ELEMENTID"] is DBNull))
                        element.ID = Convert.ToInt32(dr["ELEMENTID"]);
                    if (!(dr["ELEMENTNAME"] is DBNull))
                        element.Name = Convert.ToString(dr["ELEMENTNAME"]);
                    if (!(dr["ALIAS"] is DBNull))
                        element.Alias = Convert.ToString(dr["ALIAS"]);
                    if (!(dr["PARENTID"] is DBNull))
                        element.ParentID = Convert.ToInt32(dr["PARENTID"]);
                    if (!(dr["ISPAGE"] is DBNull))
                        element.IsPage = Convert.ToBoolean(dr["ISPAGE"]);
                    if (!(dr["ACTIVE"] is DBNull))
                        element.Active = Convert.ToBoolean(dr["ACTIVE"]);

                    elements.Add(element);
                }
                dr.Close();
            }

            cmd.Connection.Close();

            return elements;
        }

        private List<ACUser> LoadUsersByProfile(int profileID)
        {
            List<ACUser> users = new List<ACUser>();

            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PROC_CARREGAR_USER_POR_PERFIL", CommandType.StoredProcedure);

            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pPerfil", SqlDbType = SqlDbType.Decimal, Value = profileID, Direction = ParameterDirection.Input });

            cmd.Connection.Open();

            using (var dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    ACUser user = new ACUser();

                    if (!(dr["USERID"] is DBNull))
                        user.ID = Convert.ToInt32(dr["USERID"]);
                    if (!(dr["NAME"] is DBNull))
                        user.Name = Convert.ToString(dr["NAME"]);

                    users.Add(user);
                }
                dr.Close();
            }

            cmd.Connection.Close();

            return users;
        }

        public ACProfile LoadProfile(string profileName)
        {
            ACProfile profile = new ACProfile();

            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PR_PERFIL_CARREGAR_POR_NOME", CommandType.StoredProcedure);

            cmd.Parameters.Add(new SqlParameter { ParameterName = "pPerfilName", SqlDbType = SqlDbType.VarChar, Value = profileName, Direction = ParameterDirection.Input });

            cmd.Connection.Open();

            var dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                profile.ID = Convert.ToInt32(dr["ID"]);
                profile.Name = profileName;
                dr.Close();
            }

            cmd.Connection.Close();
            return profile;
        }

        public ACProfile LoadProfile(int profileID)
        {
            ACProfile profile = new ACProfile();

            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PROC_CARREGAR_PERFIL_ID", CommandType.StoredProcedure);

            cmd.Parameters.Add(new SqlParameter { ParameterName = "pPerfil", SqlDbType = SqlDbType.Decimal, Value = profileID, Direction = ParameterDirection.Input });

            cmd.Connection.Open();

            var dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                profile.ID = profileID;
                profile.Name = (!(dr["PROFILENAME"] is DBNull)) ? Convert.ToString(dr["PROFILENAME"]) : "";
            }

            dr.Close();

            if (!profile.ID.Equals(0))
            {
                profile.Permissions = LoadElementsByProfile(profileID);
                profile.Users = LoadUsersByProfile(profileID);
            }

            cmd.Connection.Close();

            return profile;
        }

        public ACUser LoadClient(string cpf)
        {
            var user = new ACUser();

            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PR_CLIENTE_ONLINE_CARREGAR_POR_CPF", CommandType.StoredProcedure);
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@cCpf", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = cpf });
            cmd.Connection.Open();

            var dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                user.CpfCnpj = Convert.ToInt64(dr["CPF"]);
                dr.Close();
            }

            cmd.Connection.Close();
            return user;
        }

        public List<ACProfile> LoadProfilesAndUserCount(int profileID, string userName, bool withoutUser)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PROC_CARREGAR_PERFIL_CONTADOR", CommandType.StoredProcedure);

            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pPerfil", SqlDbType = SqlDbType.Decimal, Value = profileID, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pUserName", SqlDbType = SqlDbType.VarChar, Value = userName, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pSemUsuario", SqlDbType = SqlDbType.Decimal, Value = withoutUser ? 1 : 0, Direction = ParameterDirection.Input });

            cmd.Connection.Open();

            var dr = cmd.ExecuteReader();

            List<ACProfile> lstProfiles = new List<ACProfile>();

            while (dr.Read())
            {
                ACProfile profile = new ACProfile();

                profile.ID = Convert.ToInt32(dr["ID"]);
                profile.Name = (!(dr["NOME"] is DBNull)) ? Convert.ToString(dr["NOME"]) : "";
                profile.UsersCount = Convert.ToInt32(dr["USUARIOS"]);

                lstProfiles.Add(profile);
            }

            dr.Close();

            cmd.Connection.Close();

            return lstProfiles;
        }

        public bool DeleteProfile(int profileID)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PROC_CA_DELETARPERFIL", CommandType.StoredProcedure);

            cmd.Parameters.Add(new SqlParameter { ParameterName = "id_perfil", SqlDbType = SqlDbType.Decimal, Value = profileID });
            cmd.Connection.Open();

            var ret = cmd.ExecuteNonQuery() > 0;
            cmd.Connection.Close();
            return ret;
        }

        public int InsertProfile(ACProfile profile)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PROC_INSERIR_PERFIL", CommandType.StoredProcedure);

            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pName", SqlDbType = SqlDbType.VarChar, Value = profile.Name });
            cmd.Connection.Open();

            var ret = cmd.ExecuteNonQuery();

            cmd.Connection.Close();

            return ret;
        }

        public int UpdateProfile(ACProfile profile)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PROC_ATUALIZAR_PERFIL", CommandType.StoredProcedure);


            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pIdPerfil", SqlDbType = SqlDbType.Decimal, Direction = ParameterDirection.Input, Value = profile.ID });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pName", SqlDbType = SqlDbType.VarChar, Value = profile.Name });

            cmd.Connection.Open();

            var ret = (cmd.ExecuteNonQuery() > 0) ? profile.ID : 0;

            cmd.Connection.Close();

            return ret;

        }

        public bool DeleteProfileElementRelationship(int profileID)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PROC_EXCLUIR_RELAC_PERFIL", CommandType.StoredProcedure);

            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pIdPerfil", SqlDbType = SqlDbType.Decimal, Direction = ParameterDirection.Input, Value = profileID });

            cmd.Connection.Open();

            var ret = cmd.ExecuteNonQuery() > 0;

            cmd.Connection.Close();

            return ret;
        }

        public bool InsertProfileElementRelationship(int profileID, int elementID)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PROC_INSERIR_RELAC_PERFIL", CommandType.StoredProcedure);

            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pIdPerfil", SqlDbType = SqlDbType.Decimal, Direction = ParameterDirection.Input, Value = profileID });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pIdElemento", SqlDbType = SqlDbType.Decimal, Direction = ParameterDirection.Input, Value = elementID });

            cmd.Connection.Open();

            var ret = (cmd.ExecuteNonQuery() > 0);

            cmd.Connection.Close();

            return ret;
        }

        public bool UpdateUserProfile(int profileID, int userID)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PROC_ATUALIZAR_USUARIO_PERFIL", CommandType.StoredProcedure);

            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pIdPerfil", SqlDbType = SqlDbType.Decimal, Direction = ParameterDirection.Input, Value = profileID });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pIdUsuario", SqlDbType = SqlDbType.Decimal, Direction = ParameterDirection.Input, Value = userID });
            cmd.Connection.Open();
            var ret = (cmd.ExecuteNonQuery() > 0);
            cmd.Connection.Close();
            return ret;
        }

        public bool UpdateOldUsersProfileToNull(int profileID)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PROC_ATUALIZAR_PERFIL_NULL", CommandType.StoredProcedure);

            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pIdPerfil", SqlDbType = SqlDbType.Decimal, Direction = ParameterDirection.Input, Value = profileID });
            cmd.Connection.Open();
            var ret = (cmd.ExecuteNonQuery() > 0);
            cmd.Connection.Close();
            return ret;
        }

        public bool UpdateElementAlias(int elementID, string newAlias)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PROC_ATUALIZAR_APELIDO", CommandType.StoredProcedure);

            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pApelido", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = newAlias });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pIdElemento", SqlDbType = SqlDbType.Decimal, Direction = ParameterDirection.Input, Value = elementID });
            cmd.Connection.Open();
            var ret = cmd.ExecuteNonQuery() > 0;
            cmd.Connection.Close();
            return ret;
        }

        public int InsertGroupOfAssessors(ACGroupOfAssessors group)
        {
            int newID = AdvanceSequence("TB_CA_GRUPO_ASSESSORES");

            /*var cmd = dbi.CreateCommand(AccessControlResources.InsertGroupOfAssessors, CommandType.Text);
            cmd.Parameters.Add(dbi.CreateParam("Id", newID));
            cmd.Parameters.Add(dbi.CreateParam("Name", group.Name));
            cmd.Parameters.Add(dbi.CreateParam("Assessors", group.Assessors));*/

            var cmd = dbi.CreateCommand("PCK_CONTROLE_ACESSO.PROC_INSERIR_GRUPO_ASSESSORES", CommandType.StoredProcedure);

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIdGrupo", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = newID });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pNome", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = group.Name });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pAssessores", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = group.Assessors });

            cmd.Connection.Open();

            var ret = cmd.ExecuteNonQuery();

            cmd.Connection.Close();

            if (ret > 0)
                return newID;
            return 0;
        }

        public bool InsertUserGroupRelationship(int userID, int groupID)
        {
            /*var cmd = dbi.CreateCommand(AccessControlResources.InsertUserGroupOfAssessorsRelationShip, CommandType.Text);
            cmd.Parameters.Add(dbi.CreateParam("UserId", userID));
            cmd.Parameters.Add(dbi.CreateParam("GroupId", groupID));*/

            var cmd = dbi.CreateCommand("PCK_CONTROLE_ACESSO.PROC_INS_REL_GRUPO_ASSESSORES", CommandType.StoredProcedure);

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIdGrupo", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = groupID });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIdUsuario", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = userID });

            cmd.Connection.Open();

            var ret = cmd.ExecuteNonQuery() > 0;

            cmd.Connection.Close();

            return ret;

        }

        private int AdvanceSequence(string tableName)
        {
            string sql = string.Format("SELECT {0}.NEXTVAL FROM DUAL", tableName.Replace("TB_", "SEQ_"));
            var cmd = dbi.CreateCommand(sql, CommandType.Text);
            return Convert.ToInt32(cmd.ExecuteScalar());
        }

        public bool ValidateGroupName(string groupName, int groupID)
        {
            /*ar cmd = dbi.CreateCommand(AccessControlResources.CountGroupByName, CommandType.Text);
            cmd.Parameters.Add(dbi.CreateParam("GroupName", groupName));
            cmd.Parameters.Add(dbi.CreateParam("GroupId", groupID));
            return Int32.Parse(cmd.ExecuteScalar().ToString()) == 0;
             */

            var cmd = dbi.CreateCommand("PCK_CONTROLE_ACESSO.FUNC_VALIDAR_GRUPO", CommandType.StoredProcedure);
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIdGrupo", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = groupID });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pNomeGrupo", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = groupName });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pReturn", OracleType = OracleType.Number, Direction = ParameterDirection.ReturnValue });

            cmd.ExecuteNonQuery();

            return Convert.ToInt32(cmd.Parameters["pReturn"].Value) == 0;
        }

        public bool ValidateAssessorsList(string list, int groupID)
        {
            /*var cmd = dbi.CreateCommand(AccessControlResources.CountGroupByAssessorsList, CommandType.Text);
            cmd.Parameters.Add(dbi.CreateParam("List", list));
            cmd.Parameters.Add(dbi.CreateParam("GroupId", groupID));
            return Int32.Parse(cmd.ExecuteScalar().ToString()) == 0;*/

            var cmd = dbi.CreateCommand("PCK_CONTROLE_ACESSO.FUNC_VALIDAR_GRUPO_ASSESSOR", CommandType.StoredProcedure);
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIdGrupo", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = groupID });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pLista", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = list });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pReturn", OracleType = OracleType.Number, Direction = ParameterDirection.ReturnValue });

            cmd.ExecuteNonQuery();

            return Convert.ToInt32(cmd.Parameters["pReturn"].Value) == 0;
        }

        public bool ValidateAssessorsAndClientsList(int groupId, string list, string includeList, string excludeList)
        {
            var cmd = dbi.CreateCommand("PCK_PORTALWEB.PROC_FILTRO_ASSESSOR_GRUPO", CommandType.StoredProcedure);

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIdGrupo", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = groupId });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pAssessor", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = list });

            if (includeList != string.Empty)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "lstclienteinclusao", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = includeList });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "lstclienteinclusao", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });

            if (excludeList != string.Empty)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "lstclienteexclusao", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = excludeList });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "lstclienteexclusao", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "result", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.ReturnValue });
            cmd.ExecuteNonQuery();
            return Convert.ToInt32(cmd.Parameters["result"].Value) == 0;
        }

        public List<ACUser> LoadGroupOfAssessorsFreeUsers(string nameFilter, string idsFilter)
        {
            List<ACUser> users = new List<ACUser>();
            /*
            var cmd = dbi.CreateCommand(AccessControlResources.GroupOfAssessorsFreeUsers, CommandType.Text);
            cmd.Parameters.Add(dbi.CreateParam("Ids", string.IsNullOrEmpty(idsFilter) ? "0" : idsFilter));
            cmd.Parameters.Add(dbi.CreateParam("Name", nameFilter.ToLower()));*/

            var cmd = dbi.CreateCommand("PCK_CONTROLE_ACESSO.PROC_GRUPO_USUARIO_LIVRE", CommandType.StoredProcedure);
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIds", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = idsFilter });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pNomeFiltro", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = nameFilter });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCursor", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    ACUser user = new ACUser();

                    if (!(dr["USERID"] is DBNull))
                        user.ID = Convert.ToInt32(dr["USERID"]);
                    if (!(dr["USERNAME"] is DBNull))
                        user.Name = Convert.ToString(dr["USERNAME"]);

                    users.Add(user);
                }
                dr.Close();
            }

            return users;
        }

        public void LoadClientsIncluded(ref ACGroupOfAssessors group)
        {
            var cmdGroup = dbi.CreateCommand(@"PCK_CONTROLE_ACESSO.PROC_CLIENTES_INCLUSAO", CommandType.StoredProcedure);

            cmdGroup.Parameters.Add(new OracleParameter { ParameterName = "pIdGrupo", Value = group.ID, OracleType = OracleType.Number, Direction = ParameterDirection.Input });
            cmdGroup.Parameters.Add(new OracleParameter { ParameterName = "pCursor", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var reader = cmdGroup.ExecuteReader())
            {
                while (reader.Read())
                {
                    if (group.ClientsIncluded == null)
                    {
                        group.ClientsIncluded = new List<Client>();
                    }
                    group.ClientsIncluded.Add(
                        new Client()
                        {
                            ClientCode = reader.GetInt32(0),
                            ClientName = reader.GetString(1)
                        });
                }
                reader.Close();
            }
        }

        public void LoadClientsExcluded(ref ACGroupOfAssessors group)
        {

            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PROC_CLIENTES_EXCLUSAO", CommandType.StoredProcedure);

            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pIdGrupo", Value = group.ID, SqlDbType = SqlDbType.Decimal, Direction = ParameterDirection.Input });

            cmd.Connection.Open();

            using (var reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    if (group.ClientsExcluded == null)
                    {
                        group.ClientsExcluded = new List<Client>();
                    }
                    group.ClientsExcluded.Add(
                        new Client()
                        {
                            ClientCode = reader.GetInt32(0),
                            ClientName = reader.GetString(1)
                        });
                }
                reader.Close();
            }
            cmd.Connection.Close();
        }

        public List<ACGroupOfAssessors> LoadGroupsOfAssessors(int groupID, string userName)
        {
            List<ACGroupOfAssessors> groups = new List<ACGroupOfAssessors>();

            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PROC_CARREGAR_GRUPO_ASSESSOR", CommandType.StoredProcedure);

            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pIdGrupo", Value = groupID, SqlDbType = SqlDbType.Decimal, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pUsuario", Value = userName, SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input });

            cmd.Connection.Open();

            using (var dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    ACGroupOfAssessors group = new ACGroupOfAssessors();

                    if (!(dr["GROUPID"] is DBNull))
                        group.ID = Convert.ToInt32(dr["GROUPID"]);
                    if (!(dr["GROUPNAME"] is DBNull))
                        group.Name = Convert.ToString(dr["GROUPNAME"]);
                    if (!(dr["ASSESSORS"] is DBNull))
                        group.Assessors = Convert.ToString(dr["ASSESSORS"]);

                    LoadClientsIncluded(ref group);
                    LoadClientsExcluded(ref group);

                    groups.Add(group);
                }
                dr.Close();
            }

            foreach (ACGroupOfAssessors g in groups)
                g.Users = LoadGroupsOfAssessorsUsers(g.ID);

            cmd.Connection.Close();

            return groups;
        }

        public List<ACGroupOfAssessors> LoadMinimumGroupsOfAssessors()
        {
            List<ACGroupOfAssessors> groups = new List<ACGroupOfAssessors>();


            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PROC_CARREGAR_GRUPO_ASSESSOR", CommandType.StoredProcedure);

            cmd.Parameters.Add(new SqlParameter { ParameterName = "pIdGrupo", Value = 0, SqlDbType = SqlDbType.Decimal, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "pUsuario", Value = string.Empty, SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input });

            cmd.Connection.Open();

            using (var dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    ACGroupOfAssessors group = new ACGroupOfAssessors();

                    if (!(dr["GROUPID"] is DBNull))
                        group.ID = Convert.ToInt32(dr["GROUPID"]);
                    if (!(dr["GROUPNAME"] is DBNull))
                        group.Name = Convert.ToString(dr["GROUPNAME"]);

                    groups.Add(group);
                }
                dr.Close();
            }

            cmd.Connection.Close();

            return groups;
        }

        public ACGroupOfAssessors LoadGroupOfAssessorsPerUser(long userId)
        {
            ACGroupOfAssessors group = null;


            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PROC_CARREGAR_GRUPO_USUARIO", CommandType.StoredProcedure);

            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pIdUsuario", Value = userId, SqlDbType = SqlDbType.Decimal, Direction = ParameterDirection.Input });

            cmd.Connection.Open();

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    group = new ACGroupOfAssessors();
                    if (dr.Read())
                    {
                        if (!(dr["GROUPID"] is DBNull))
                            group.ID = Convert.ToInt32(dr["GROUPID"]);
                        if (!(dr["GROUPNAME"] is DBNull))
                            group.Name = Convert.ToString(dr["GROUPNAME"]);
                        if (!(dr["ASSESSORS"] is DBNull))
                            group.Assessors = Convert.ToString(dr["ASSESSORS"]);

                        LoadClientsIncluded(ref group);
                        LoadClientsExcluded(ref group);

                        group.Users = LoadGroupsOfAssessorsUsers(group.ID);
                    }
                }
                dr.Close();
            }

            cmd.Connection.Close();

            return group;
        }

        public List<ACGroupOfAssessors> LoadGroupsOfAssessorsPerUser(long userId)
        {
            List<ACGroupOfAssessors> groups = new List<ACGroupOfAssessors>();


            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PROC_GRUPO_USUARIO_LISTA", CommandType.StoredProcedure);

            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pIdUsuario", Value = userId, SqlDbType = SqlDbType.Decimal, Direction = ParameterDirection.Input });

            cmd.Connection.Open();

            using (var dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    ACGroupOfAssessors group = new ACGroupOfAssessors();

                    if (!(dr["GROUPID"] is DBNull))
                        group.ID = Convert.ToInt32(dr["id"]);
                    if (!(dr["GROUPNAME"] is DBNull))
                        group.Name = Convert.ToString(dr["nome"]);
                    if (!(dr["ASSESSORS"] is DBNull))
                        group.Assessors = Convert.ToString(dr["assessores"]);

                    groups.Add(group);
                }
                dr.Close();
            }

            cmd.Connection.Close();

            return groups;
        }

        public List<ACUser> LoadGroupsOfAssessorsUsers(int groupID)
        {
            List<ACUser> users = new List<ACUser>();

            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PROC_LISTA_USUARIO_GRUPO", CommandType.StoredProcedure);

            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pIdGrupo", Value = groupID, SqlDbType = SqlDbType.Decimal, Direction = ParameterDirection.Input });

            cmd.Connection.Open();

            using (var dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    ACUser user = new ACUser();

                    if (!(dr["USERID"] is DBNull))
                        user.ID = Convert.ToInt32(dr["USERID"]);
                    if (!(dr["USERNAME"] is DBNull))
                        user.Name = Convert.ToString(dr["USERNAME"]);
                    if (!(dr["EMAIL"] is DBNull))
                        user.Email = Convert.ToString(dr["EMAIL"]);
                    if (!(dr["ASSESSORID"] is DBNull))
                        user.AssessorID = Convert.ToInt32(dr["ASSESSORID"]);
                    if (!(dr["PROFILEID"] is DBNull) && !(dr["PROFILENAME"] is DBNull))
                        user.Profile = new ACProfile { ID = Convert.ToInt32(dr["PROFILEID"]), Name = Convert.ToString(dr["PROFILENAME"]) };

                    users.Add(user);
                }
                dr.Close();
            }

            cmd.Connection.Close();

            return users;
        }

        public List<ACUser> LoadUsers()
        {
            List<ACUser> users = new List<ACUser>();

            DBInterface dbiCRK = new DBInterface("PORTAL");
            dbiCRK.Connection.Open();
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PROC_LISTA_USUARIOS_ATIVOS", CommandType.StoredProcedure);

            using (var dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    ACUser user = new ACUser();

                    if (!(dr["USERID"] is DBNull))
                        user.ID = Convert.ToInt32(dr["USERID"]);
                    if (!(dr["USERNAME"] is DBNull))
                        user.Name = Convert.ToString(dr["USERNAME"]);
                    if (!(dr["EMAIL"] is DBNull))
                        user.Email = Convert.ToString(dr["EMAIL"]);

                    user.Profile = !(dr["PROFILEID"] is DBNull) && !(dr["PROFILENAME"] is DBNull) ? new ACProfile { ID = Convert.ToInt32(dr["PROFILEID"]), Name = Convert.ToString(dr["PROFILENAME"]) } : new ACProfile { ID = 0 };

                    if (!(dr["ASSESSORID"] is DBNull))
                        user.AssessorID = Convert.ToInt32(dr["ASSESSORID"]);
                    else
                        user.AssessorID = -1;

                    if (!(dr["TYPE"] is DBNull))
                        user.Type = dr["TYPE"].ToString();
                    if (!(dr["STATUS"] is DBNull))
                        user.ActiveStatus = Convert.ToInt32(dr["STATUS"]);

                    if (!(dr["BLOCKED"] is DBNull))
                        user.Blocked = Convert.ToBoolean(dr["BLOCKED"]);

                    if (!(dr["LOGIN"] is DBNull))
                        user.Login = Convert.ToString(dr["LOGIN"]);

                    if (!(dr["CHAVE_AD"] is DBNull))
                        user.ChaveAD = Convert.ToString(dr["CHAVE_AD"]);

                    if (!(dr["CDBOLSA"] is DBNull))
                        user.CdCliente = Convert.ToInt32(dr["CDBOLSA"]);
                    else
                        user.CdCliente = 0;


                    object clientId = dr["ID_CLIENTE"];
                    object cpfcnpj = dr["CD_CPFCNPJ"];

                    if (!clientId.Equals(DBNull.Value))
                        user.ClientId = Convert.ToInt32(clientId);
                    if (!cpfcnpj.Equals(DBNull.Value))
                        user.CpfCnpj = Convert.ToInt64(cpfcnpj);

                    DBInterface dbiCRK2 = new DBInterface("PORTAL");
                    dbiCRK2.Connection.Open();
                    List<ACGroupOfAssessors> groups = new List<ACGroupOfAssessors>();

                    var cmdGroup = (SqlCommand)dbiCRK2.CreateCommand("PROC_CARREGAR_GRUPO_ASSESSOR", CommandType.StoredProcedure);

                    cmdGroup.Parameters.Add(new SqlParameter { ParameterName = "@pIdGrupo", Value = 0, SqlDbType = SqlDbType.Decimal, Direction = ParameterDirection.Input });
                    cmdGroup.Parameters.Add(new SqlParameter { ParameterName = "@pUsuario", Value = Convert.ToString(dr["USERNAME"]), SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input });

                    using (var drGroup = cmdGroup.ExecuteReader())
                    {
                        while (drGroup.Read())
                        {

                            ACGroupOfAssessors group = new ACGroupOfAssessors();

                            if (!(drGroup["GROUPID"] is DBNull))
                                group.ID = Convert.ToInt32(drGroup["GROUPID"]);
                            if (!(drGroup["GROUPNAME"] is DBNull))
                                group.Name = Convert.ToString(drGroup["GROUPNAME"]);
                            if (!(drGroup["ASSESSORS"] is DBNull))
                                group.Assessors = Convert.ToString(drGroup["ASSESSORS"]);

                            groups.Add(group);
                        }
                        drGroup.Close();
                    }


                    user.AssessorsGroup = groups;

                    users.Add(user);

                    dbiCRK2.Connection.Close();
                }
                dr.Close();
            }

            dbiCRK.Connection.Close();

            return users;
        }

        public ACUser LoadUser(int id)
        {
            ACUser user = new ACUser();

            DBInterface dbiCRK = new DBInterface("PORTAL");
            dbiCRK.Connection.Open();
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PROC_USUARIO_ATIVO", CommandType.StoredProcedure);

            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pIdUsuario", Value = id, SqlDbType = SqlDbType.Decimal, Direction = ParameterDirection.Input });

            using (var dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    if (!(dr["LOGIN"] is DBNull))
                        user.Login = dr["LOGIN"].ToString();

                    if (!(dr["USERID"] is DBNull))
                        user.ID = Convert.ToInt32(dr["USERID"]);
                    if (!(dr["USERNAME"] is DBNull))
                        user.Name = Convert.ToString(dr["USERNAME"]);
                    if (!(dr["EMAIL"] is DBNull))
                        user.Email = Convert.ToString(dr["EMAIL"]);
                    if (!(dr["PROFILEID"] is DBNull) && !(dr["PROFILENAME"] is DBNull))
                        user.Profile = new ACProfile
                        {
                            ID = Convert.ToInt32(dr["PROFILEID"]),
                            Name = Convert.ToString(dr["PROFILENAME"])
                        };

                    if (!(dr["ASSESSORID"] is DBNull))
                        user.AssessorID = Convert.ToInt32(dr["ASSESSORID"]);
                    else
                        user.AssessorID = -1;

                    if (!(dr["TYPE"] is DBNull))
                        user.Type = dr["TYPE"].ToString();
                    if (!(dr["STATUS"] is DBNull))
                        user.ActiveStatus = Convert.ToInt32(dr["STATUS"]);

                    if (!(dr["Cdbolsa"] is DBNull))
                        user.CdCliente = Convert.ToInt32(dr["Cdbolsa"]);

                    if (!(dr["BLOQUEADO"] is DBNull))
                        user.Blocked = Convert.ToInt32(dr["BLOQUEADO"]) == 1 ? true : false;

                    if (!(dr["Suitability_Status"] is DBNull))
                        user.SuitabilityStatus = Convert.ToInt32(dr["Suitability_Status"]);

                    if (!(dr["Investidor"] is DBNull))
                        user.InvestorType = Convert.ToInt32(dr["Investidor"]);

                    if (!(dr["SUITABILITY_DATE"] is DBNull))
                        user.SuitabilityDate = Convert.ToDateTime(dr["SUITABILITY_DATE"]);

                    if (!(dr["TIPO_INVESTIDOR"] is DBNull))
                        user.InvestorKind = Convert.ToInt32(dr["TIPO_INVESTIDOR"]);
                    else
                        user.InvestorKind = 0;

                    if (!(dr["ENVIAR_EMAIL_SUITABILITY"] is DBNull))
                        user.SendSuitabilityEmail = Convert.ToInt32(dr["ENVIAR_EMAIL_SUITABILITY"]) == 1 ? true : false;
                    else
                        user.SendSuitabilityEmail = true;

                    object clientId = dr["ID_CLIENTE"];
                    object cpfcnpj = dr["CD_CPFCNPJ"];

                    if (!clientId.Equals(DBNull.Value))
                        user.ClientId = Convert.ToInt32(clientId);
                    if (!cpfcnpj.Equals(DBNull.Value))
                        user.CpfCnpj = Convert.ToInt64(cpfcnpj);


                    List<ACGroupOfAssessors> groups = new List<ACGroupOfAssessors>();
                    DBInterface dbiCRK2 = new DBInterface("PORTAL");
                    dbiCRK2.Connection.Open();

                    var cmdGroup = (SqlCommand)dbiCRK2.CreateCommand("PROC_CARREGAR_GRUPO_ASSESSOR", CommandType.StoredProcedure);

                    cmdGroup.Parameters.Add(new SqlParameter { ParameterName = "@pIdGrupo", Value = 0, SqlDbType = SqlDbType.Decimal, Direction = ParameterDirection.Input });
                    cmdGroup.Parameters.Add(new SqlParameter { ParameterName = "@pUsuario", Value = Convert.ToString(dr["USERNAME"]), SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input });

                    using (var drGroup = cmdGroup.ExecuteReader())
                    {
                        while (drGroup.Read())
                        {

                            ACGroupOfAssessors group = new ACGroupOfAssessors();

                            if (!(drGroup["GROUPID"] is DBNull))
                                group.ID = Convert.ToInt32(drGroup["GROUPID"]);
                            if (!(drGroup["GROUPNAME"] is DBNull))
                                group.Name = Convert.ToString(drGroup["GROUPNAME"]);
                            if (!(drGroup["ASSESSORS"] is DBNull))
                                group.Assessors = Convert.ToString(drGroup["ASSESSORS"]);

                            groups.Add(group);
                        }
                    }

                    user.AssessorsGroup = groups;

                    dbiCRK2.Connection.Close();
                }
                dr.Close();
            }

            dbiCRK.Connection.Close();

            return user;
        }

        public List<ACProfile> LoadProfiles()
        {
            List<ACProfile> profiles = new List<ACProfile>();

            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PROC_CARREGAR_PERFIL", CommandType.StoredProcedure);

            cmd.Connection.Open();

            using (var dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    ACProfile profile = new ACProfile();
                    if (!(dr["PROFILEID"] is DBNull))
                        profile.ID = Convert.ToInt32(dr["PROFILEID"]);
                    if (!(dr["PROFILENAME"] is DBNull))
                        profile.Name = Convert.ToString(dr["PROFILENAME"]);

                    profiles.Add(profile);
                }
                dr.Close();
            }

            cmd.Connection.Close();

            return profiles;
        }

        public bool DeleteGroupOfAssessors(int groupID)
        {
            var cmd = dbi.CreateCommand("PCK_PORTALWEB.PROC_CA_DELETARGRUPOASSESSORES", CommandType.StoredProcedure);

            cmd.Parameters.Add(new OracleParameter { ParameterName = "id_grupo", OracleType = OracleType.Number, Value = groupID });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "erro", OracleType = OracleType.Number, Direction = ParameterDirection.Output });
            var dr = cmd.ExecuteReader();

            return Int32.Parse(cmd.Parameters["erro"].Value.ToString()) == 0;
        }

        public bool DeleteGroupOfAssessorsUsers(int groupID)
        {
            /*
            var cmd = dbi.CreateCommand(AccessControlResources.DeleteGroupOfAssessorsUsers, CommandType.Text);
            cmd.Parameters.Add(dbi.CreateParam("GroupId", groupID));*/

            var cmd = dbi.CreateCommand("PCK_CONTROLE_ACESSO.PROC_REMOVER_GRUPO_GR_ASR", CommandType.StoredProcedure);
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIdGrupo", OracleType = OracleType.Number, Value = groupID, Direction = ParameterDirection.Input });
            return cmd.ExecuteNonQuery() > 0;
        }

        public bool DeleteUserGroupOfAssessor(int userID)
        {
            /*var cmd = dbi.CreateCommand(AccessControlResources.DeleteUserGroupAssessor, CommandType.Text);
            cmd.Parameters.Add(dbi.CreateParam("UserId", userID));*/

            var cmd = dbi.CreateCommand("PCK_CONTROLE_ACESSO.PROC_REMOVER_USUARIO_GR_ASR", CommandType.StoredProcedure);
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIdUsuario", OracleType = OracleType.Number, Value = userID, Direction = ParameterDirection.Input });
            return cmd.ExecuteNonQuery() > 0;
        }

        public int UpdateGroupOfAssessors(ACGroupOfAssessors group)
        {
            /*var cmd = dbi.CreateCommand(AccessControlResources.UpdateGroupOfAssessors, CommandType.Text);
            cmd.Parameters.Add(dbi.CreateParam("GroupId", group.ID));
            cmd.Parameters.Add(dbi.CreateParam("GroupName", group.Name));
            cmd.Parameters.Add(dbi.CreateParam("Assessors", group.Assessors));*/

            /*procedure PROC_ATUALIZAR_GRUPO_ASSESSOR(
                 pIdGrupo                   in number,
                 pNomeGrupo                 in varchar2,
                 pAssessores                in varchar2
            )*/
            var cmd = dbi.CreateCommand("PCK_CONTROLE_ACESSO.PROC_ATUALIZAR_GRUPO_ASSESSOR", CommandType.StoredProcedure);
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIdGrupo", OracleType = OracleType.Number, Value = group.ID, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pNomeGrupo", OracleType = OracleType.VarChar, Value = group.Name, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pAssessores", OracleType = OracleType.VarChar, Value = group.Assessors, Direction = ParameterDirection.Input });

            return (cmd.ExecuteNonQuery() > 0) ? group.ID : 0;
        }

        public int InsertClientOnline(ACUser user)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PR_INSERIR_CLIENTE_ONLINE", CommandType.StoredProcedure);

            cmd.Connection.Open();

            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pNome", SqlDbType = SqlDbType.VarChar, Value = user.Name, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pEmail", SqlDbType = SqlDbType.VarChar, Value = user.Email, Direction = ParameterDirection.Input });

            if (user.Profile.ID == 0)
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@pIdPerfil", SqlDbType = SqlDbType.Decimal, Value = DBNull.Value, Direction = ParameterDirection.Input });
            else
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@pIdPerfil", SqlDbType = SqlDbType.Decimal, Value = user.Profile.ID, Direction = ParameterDirection.Input });

            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pTipo", SqlDbType = SqlDbType.VarChar, Value = user.Type, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pAtivo", SqlDbType = SqlDbType.Decimal, Value = user.ActiveStatus, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pLogin", SqlDbType = SqlDbType.VarChar, Value = user.Login, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pChaveAD", SqlDbType = SqlDbType.VarChar, Value = user.ChaveAD, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pCdCpfCnpj", SqlDbType = SqlDbType.Decimal, Value = (object)user.CpfCnpj ?? DBNull.Value, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pCdBolsa", SqlDbType = SqlDbType.Int, Value = 0, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pPassword", SqlDbType = SqlDbType.VarChar, Value = user.Password, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pTelefone", SqlDbType = SqlDbType.VarChar, Value = user.Telephone, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@tipoPessoa", SqlDbType = SqlDbType.VarChar, Value = "F", Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@suit", SqlDbType = SqlDbType.Int, Value = 1, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@invest", SqlDbType = SqlDbType.Int, Value = 0, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@desenq", SqlDbType = SqlDbType.VarChar, Value = "N", Direction = ParameterDirection.Input });

            var returnValue = cmd.Parameters.Add("@vIdUsuario", SqlDbType.Int);
            returnValue.Direction = ParameterDirection.ReturnValue;

            cmd.ExecuteNonQuery();
            cmd.Connection.Close();

            return Convert.ToInt32(returnValue.Value);
        }

        private string PersonType(string personType, long? cpfCnpj)
        {
            string r = string.Empty;

            if (!string.IsNullOrEmpty(personType))
                r = personType;
            else
            {
                if (cpfCnpj.HasValue)
                    r = cpfCnpj.Value.ToString().Length <= 11 ? "F" : "J";
            }

            return r;
        }

        public int InsertUser(ACUser user)
        {

            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PROC_INSERIR_USUARIO", CommandType.StoredProcedure);

            cmd.Connection.Open();

            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pNome", SqlDbType = SqlDbType.VarChar, Value = user.Name, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pEmail", SqlDbType = SqlDbType.VarChar, Value = user.Email, Direction = ParameterDirection.Input });

            if (user.Profile.ID == 0)
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@pIdPerfil", SqlDbType = SqlDbType.Decimal, Value = DBNull.Value, Direction = ParameterDirection.Input });
            else
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@pIdPerfil", SqlDbType = SqlDbType.Decimal, Value = user.Profile.ID, Direction = ParameterDirection.Input });

            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pTipo", SqlDbType = SqlDbType.VarChar, Value = user.Type, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pPessoa", SqlDbType = SqlDbType.VarChar, Value = PersonType(user.PersonType, user.CpfCnpj), Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pAtivo", SqlDbType = SqlDbType.Decimal, Value = user.ActiveStatus, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pLogin", SqlDbType = SqlDbType.VarChar, Value = user.Login, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pChaveAD", SqlDbType = SqlDbType.VarChar, Value = !string.IsNullOrEmpty(user.ChaveAD) ? user.ChaveAD : "", Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pCdCpfCnpj", SqlDbType = SqlDbType.Decimal, Value = (object)user.CpfCnpj ?? DBNull.Value, Direction = ParameterDirection.Input });

            if (user.CdCliente > 0)
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@pCdBolsa", SqlDbType = SqlDbType.Int, Value = user.CdCliente, Direction = ParameterDirection.Input });
            else
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@pCdBolsa", SqlDbType = SqlDbType.Int, Value = DBNull.Value, Direction = ParameterDirection.Input });

            if (user.InvestorKind == 0)
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@pIdInvestidor", SqlDbType = SqlDbType.Int, Value = DBNull.Value, Direction = ParameterDirection.Input });
            else
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@pIdInvestidor", SqlDbType = SqlDbType.Int, Value = user.InvestorKind, Direction = ParameterDirection.Input });

            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pEnviarEmailSuitability", SqlDbType = SqlDbType.Int, Value = user.SendSuitabilityEmail ? 1 : 0, Direction = ParameterDirection.Input });

            cmd.Parameters.Add("@pID_CLIENTE", SqlDbType.Int).Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            int id = Int32.Parse(cmd.Parameters["@pID_CLIENTE"].Value.ToString());

            cmd.Connection.Close();

            return id;
        }

        public int UpdateUser(ACUser user)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PROC_ALTERAR_USUARIO", CommandType.StoredProcedure);

            cmd.Connection.Open();

            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pIdUsuario", SqlDbType = SqlDbType.Decimal, Value = user.ID, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pNome", SqlDbType = SqlDbType.VarChar, Value = user.Name, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pEmail", SqlDbType = SqlDbType.VarChar, Value = user.Email, Direction = ParameterDirection.Input });
            if (user.AssessorID < 0)
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@pIdAssessor", SqlDbType = SqlDbType.Decimal, Value = DBNull.Value, Direction = ParameterDirection.Input });
            else
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@pIdAssessor", SqlDbType = SqlDbType.Decimal, Value = user.AssessorID, Direction = ParameterDirection.Input });

            if (user.Profile.ID == 0)
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@pIdPerfil", SqlDbType = SqlDbType.Decimal, Value = DBNull.Value, Direction = ParameterDirection.Input });
            else
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@pIdPerfil", SqlDbType = SqlDbType.Decimal, Value = user.Profile.ID, Direction = ParameterDirection.Input });

            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pTipo", SqlDbType = SqlDbType.VarChar, Value = user.Type, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pAtivo", SqlDbType = SqlDbType.Decimal, Value = user.ActiveStatus, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pCdCpfCnpj", SqlDbType = SqlDbType.Decimal, Value = (object)user.CpfCnpj ?? DBNull.Value, Direction = ParameterDirection.Input });

            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pCdBolsa", SqlDbType = SqlDbType.Decimal, Value = user.CdCliente, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pBloqueado", SqlDbType = SqlDbType.Decimal, Value = user.Blocked == true ? 1 : 0, Direction = ParameterDirection.Input });

            if (user.InvestorKind == 0)
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@pIdInvestidor", SqlDbType = SqlDbType.Int, Value = DBNull.Value, Direction = ParameterDirection.Input });
            else
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@pIdInvestidor", SqlDbType = SqlDbType.Int, Value = user.InvestorKind, Direction = ParameterDirection.Input });

            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pEnviarEmailSuitability", SqlDbType = SqlDbType.Int, Value = user.SendSuitabilityEmail ? 1 : 0, Direction = ParameterDirection.Input });

            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pLogin", SqlDbType = SqlDbType.VarChar, Value = user.Login, Direction = ParameterDirection.Input });

            bool ok = (cmd.ExecuteNonQuery() > 0);

            cmd.Connection.Close();

            if (!ok)
            {
                return 0;
            }
            else
            {
                return user.ID;
            }

        }

        public bool DeleteUser(int userID)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PROC_CA_DELETARUSUARIOS", CommandType.StoredProcedure);

            cmd.Parameters.Add(new SqlParameter { ParameterName = "@id_do_usuario", SqlDbType = SqlDbType.Decimal, Value = userID });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@erro", SqlDbType = SqlDbType.Decimal, Direction = ParameterDirection.Output });

            cmd.Connection.Open();

            cmd.ExecuteNonQuery();
            cmd.ExecuteReader();

            cmd.Connection.Close();

            return Int32.Parse(cmd.Parameters["@erro"].Value.ToString()) == 0;
        }

        public bool ValidateProfileName(string profileName, int profileID)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("FUNC_VALIDAR_PERFIL", CommandType.StoredProcedure);

            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pIdPerfil", Value = profileID, SqlDbType = SqlDbType.Decimal, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pNomePerfil", Value = profileName, SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pReturn", SqlDbType = SqlDbType.Decimal, Direction = ParameterDirection.ReturnValue });

            cmd.Connection.Open();

            cmd.ExecuteNonQuery();

            cmd.Connection.Close();

            return Convert.ToInt32(cmd.Parameters["@pReturn"].Value) == 0;
        }

        public bool ValidateProfileElements(List<ACElement> elements, int profileID)
        {
            int elementsCount = elements.Count;
            List<int> lstProfileID = new List<int>();


            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PROC_LISTA_PERFIL_ELEMENTO", CommandType.StoredProcedure);

            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pIdPerfil", Value = profileID, SqlDbType = SqlDbType.Decimal, Direction = ParameterDirection.Input });

            cmd.Connection.Open();

            var dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                if (Convert.ToInt32(dr["ELEMENTOS"]) == elementsCount)
                    lstProfileID.Add(Convert.ToInt32(dr["ID"]));
            }

            dr.Close();

            if (lstProfileID.Count > 0)
                foreach (int id in lstProfileID)
                {
                    List<ACElement> lstMatchCount = this.LoadElementsIDByProfile(id);
                    int a = (from m in lstMatchCount where !(from o in elements select o.ID).Contains(m.ID) select m).Count();

                    if (a == 0)
                        return false;
                }

            cmd.Connection.Close();

            return true;
        }

        public List<ACElement> LoadElementsIDByProfile(int profileID)
        {
            List<ACElement> elements = new List<ACElement>();

            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PROC_LISTA_IDELEMENTOS_PERFIL", CommandType.StoredProcedure);

            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pIdPerfil", Value = profileID, SqlDbType = SqlDbType.Decimal, Direction = ParameterDirection.Input });

            cmd.Connection.Open();

            using (var dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    ACElement element = new ACElement();

                    if (!(dr["ELEMENTID"] is DBNull))
                        element.ID = Convert.ToInt32(dr["ELEMENTID"]);

                    elements.Add(element);
                }
                dr.Close();
            }

            cmd.Connection.Close();

            return elements;
        }

        public List<ACUser> LoadUsersWithoutProfile()
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PROC_LISTA_USUARIOS_SEM_PERFIL", CommandType.StoredProcedure);

            cmd.Connection.Open();

            var dr = cmd.ExecuteReader();

            List<ACUser> lstUsers = new List<ACUser>();

            while (dr.Read())
            {
                ACUser user = new ACUser();

                user.ID = Convert.ToInt32(dr["ID"]);
                user.Name = (!(dr["NOME"] is DBNull)) ? Convert.ToString(dr["NOME"]) : "";

                lstUsers.Add(user);
            }

            dr.Close();

            cmd.Connection.Close();

            return lstUsers;
        }

        public List<Int32> LoadUserListOfAssessors(long userID)
        {
            List<Int32> assessors = new List<int>();

            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PROC_LISTA_ASR_USUARIO", CommandType.StoredProcedure);

            cmd.Connection.Open();

            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pIdUsuario", SqlDbType = SqlDbType.Decimal, Direction = ParameterDirection.Input, Value = userID });

            var dr = cmd.ExecuteReader();

            while (dr.Read())
                if (!(dr["ASSESSORS"] is DBNull))
                    assessors.AddRange(ConvertRangeStrToNumList(Convert.ToString(dr["ASSESSORS"])));

            dr.Close();

            cmd.Connection.Close();

            return assessors.Distinct().ToList();
        }

        private List<int> ConvertRangeStrToNumList(string rangeStr)
        {
            List<int> list = new List<int>();

            foreach (char c in rangeStr)
                if ("0123456789,-".IndexOf(c) < 0)
                    return null;

            if (string.IsNullOrEmpty(rangeStr))
                return null;

            string[] assParams = rangeStr.Split(',');
            foreach (string ap in assParams)
            {
                string[] assParams2 = ap.Split('-');
                if (assParams2.Length > 2)
                    return null;
                else
                {
                    int start = Int32.Parse(assParams2[0]);
                    list.Add(start);
                    if (assParams2.Length == 2)
                    {
                        int end = Int32.Parse(assParams2[1]);
                        for (int i = start + 1; i <= end; i++)
                            list.Add(i);
                    }
                }
            }

            return list;
        }

        public bool InsertClientOnGroup(int groupID, int clientCode)
        {
            /*
             pIDGRUPO          in number
            , pCD_CLIENTE       in number
            , tp_operacao       in varchar
             */
            var cmd = dbi.CreateCommand("PCK_USUARIOS.FUNC_CLIENTE_INCLUSAO", CommandType.StoredProcedure);

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIDGRUPO", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = groupID });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = clientCode });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "tp_operacao", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = "I", Size = 1 });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pResult", OracleType = OracleType.Number, Direction = ParameterDirection.ReturnValue });

            cmd.ExecuteNonQuery();

            return Convert.ToInt32(cmd.Parameters["pResult"].Value) > 0;
        }

        public bool CancelClientOnGroup(int groupID, int clientCode)
        {
            /*
             pIDGRUPO          in number
            , pCD_CLIENTE       in number
            , tp_operacao       in varchar
             */
            var cmd = dbi.CreateCommand("PCK_USUARIOS.FUNC_CLIENTE_INCLUSAO", CommandType.StoredProcedure);

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIDGRUPO", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = groupID });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = clientCode });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "tp_operacao", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = "E", Size = 1 });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pResult", OracleType = OracleType.Number, Direction = ParameterDirection.ReturnValue });

            cmd.ExecuteNonQuery();

            return Convert.ToInt32(cmd.Parameters["pResult"].Value) > 0;
        }

        public bool RemoveClientFromGroup(int groupID, int clientCode)
        {
            /*
             pIDGRUPO          in number
            , pCD_CLIENTE       in number
            , tp_operacao       in varchar
             */
            var cmd = dbi.CreateCommand("PCK_USUARIOS.FUNC_CLIENTE_EXCLUSAO", CommandType.StoredProcedure);

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIDGRUPO", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = groupID });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = clientCode });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "tp_operacao", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = "I", Size = 1 });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pResult", OracleType = OracleType.Number, Direction = ParameterDirection.ReturnValue });

            cmd.ExecuteNonQuery();

            return Convert.ToInt32(cmd.Parameters["pResult"].Value) > 0;
        }

        public bool CancelRemovedClientFromGroup(int groupID, int clientCode)
        {
            /*
             pIDGRUPO          in number
            , pCD_CLIENTE       in number
            , tp_operacao       in varchar
             */
            var cmd = dbi.CreateCommand("PCK_USUARIOS.FUNC_CLIENTE_EXCLUSAO", CommandType.StoredProcedure);

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIDGRUPO", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = groupID });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = clientCode });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "tp_operacao", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = "E", Size = 1 });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pResult", OracleType = OracleType.Number, Direction = ParameterDirection.ReturnValue });

            cmd.ExecuteNonQuery();

            return Convert.ToInt32(cmd.Parameters["pResult"].Value) > 0;
        }

        public bool RemoveAllExceptionClients(int groupId)
        {
            try
            {
                var cmd1 = dbi.CreateCommand("DELETE FROM TB_CA_CLIENTES_INCLUSAO WHERE ID_GRUPO = :ID_GRUPO", CommandType.Text);

                cmd1.Parameters.Add(new OracleParameter { ParameterName = "ID_GRUPO", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = groupId });

                int result1 = cmd1.ExecuteNonQuery();

                var cmd2 = dbi.CreateCommand("DELETE FROM TB_CA_CLIENTES_EXCLUSAO WHERE ID_GRUPO = :ID_GRUPO", CommandType.Text);

                cmd2.Parameters.Add(new OracleParameter { ParameterName = "ID_GRUPO", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = groupId });

                int result2 = cmd2.ExecuteNonQuery();

                return (result1 > 0 && result2 > 0);
            }
            catch { return false; }
        }

        #region Cadastro de clientes por funcionalidade

        public List<FuncionalityClient> ListFuncionalityClients(FuncionalityClientFilter filter, out int count)
        {
            var cmd = dbi.CreateCommand("PCK_CLIENTES_FUNCIONALIDADE.PROC_LISTA_CLIENTE_FUNC", CommandType.StoredProcedure);

            /*
              pCD_CLIENTE        in number,
               pFORMATO           in char,
               pINITROW           In NUMBER,
               pFINALROW          In NUMBER,
               pSORTCOLUMN        In NUMBER,
               pSORTDIR           In Varchar2,
               pTODOS             In Integer,
               pCount             Out NUMBER,
               pCURSOR            Out tCursor
             */
            if (filter.ClientCodes != "")
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLstCliente", IsNullable = true, OracleType = OracleType.VarChar, Value = filter.ClientCodes });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLstCliente", IsNullable = true, OracleType = OracleType.VarChar, Value = DBNull.Value });

            if (!String.IsNullOrEmpty(filter.SendFormat))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pFORMATO", IsNullable = true, OracleType = OracleType.Char, Value = filter.SendFormat });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pFORMATO", IsNullable = true, OracleType = OracleType.Char, Value = DBNull.Value });

            if (!String.IsNullOrEmpty(filter.Assessors))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTASSESSOR", IsNullable = true, OracleType = OracleType.Char, Value = filter.Assessors });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTASSESSOR", IsNullable = true, OracleType = OracleType.Char, Value = DBNull.Value });

            if (filter.UserID != 0)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_USUARIO", IsNullable = true, OracleType = OracleType.Number, Value = filter.UserID });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_USUARIO", IsNullable = true, OracleType = OracleType.Number, Value = DBNull.Value });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pINITROW", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = filter.InitRow });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pFINALROW", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = filter.FinalRow });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pSORTCOLUMN", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = filter.SortColumn });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pSORTDIR", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.SortDirection });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pTODOS", IsNullable = true, OracleType = OracleType.Int32, Direction = ParameterDirection.Input, Value = (filter.AllRecords) ? 1 : 0 });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCount", OracleType = OracleType.Number, Direction = ParameterDirection.Output });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCursor", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            List<FuncionalityClient> result = null;

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    result = new List<FuncionalityClient>();
                    FuncionalityClient obj = null;
                    Funcionality func = null;
                    var auxCliente = 0;
                    var auxFuncionality = 0;

                    while (dr.Read())
                    {

                        var clientId = dr.GetInt32(0);

                        if (auxCliente == 0 || auxCliente != clientId)
                        {
                            if (obj != null && func != null)
                                obj.Funcionalities.Add(func);

                            if (obj != null)
                                result.Add(obj);

                            obj = new FuncionalityClient();
                            auxCliente = clientId;

                            obj.ClientCode = clientId;
                            if (!dr.IsDBNull(1)) obj.ClientName = dr.GetString(1);

                            obj.Funcionalities = new List<Funcionality>();
                            auxFuncionality = 0;
                            func = null;
                        }

                        var funcId = dr.GetInt32(5);

                        if (auxFuncionality == 0 || auxFuncionality != funcId)
                        {
                            if (obj != null && func != null)
                                obj.Funcionalities.Add(func);

                            func = new Funcionality();
                            func.Emails = new List<ClientEmail>();
                            func.CopyTo = new List<ClientEmail>();

                            func.FuncId = funcId;
                            func.FuncionalityName = dr.GetString(4);
                            func.SendFormat = dr.GetString(3);

                            var type = dr.GetString(8);
                            if (type == "P")
                                func.Emails.Add(new ClientEmail() { Email = dr.GetString(2), EmailId = dr.GetInt32(7) });
                            else
                                func.CopyTo.Add(new ClientEmail() { Email = dr.GetString(2), EmailId = dr.GetInt32(7) });

                            func.FuncClientMailId = dr.GetInt32(6);

                            auxFuncionality = funcId;
                        }
                        else
                        {
                            var type = dr.GetString(8);
                            if (type == "P")
                                func.Emails.Add(new ClientEmail() { Email = dr.GetString(2), EmailId = dr.GetInt32(7) });
                            else
                                func.CopyTo.Add(new ClientEmail() { Email = dr.GetString(2), EmailId = dr.GetInt32(7) });
                        }


                    }

                    if (obj != null && func != null)
                        obj.Funcionalities.Add(func);

                    if (obj != null)
                        result.Add(obj);
                }

                count = Convert.ToInt32(cmd.Parameters["pCount"].Value);

                dr.Close();

            }

            return result;
        }

        public bool InsertClientForFuncionality(FuncionalityClient client)
        {
            /*PROC_CADASTRO_CLIENTE ( 
              pCD_CLIENTE                         in number,
              pFORMATO                            in char,
              pIdFuncionalidade                   in number,
              TP_OPERACAO                         in char,
              pCLIENTE_FUNC                       in out number*/

            var cmd = dbi.CreateCommand("PCK_CLIENTES_FUNCIONALIDADE.PROC_CADASTRO_CLIENTE", CommandType.StoredProcedure);
            var totalResult = true;

            foreach (var f in client.Funcionalities)
            {
                if (f.Emails == null || f.Emails.Count <= 0 || String.IsNullOrEmpty(f.SendFormat))
                    continue;

                cmd.Parameters.Clear();

                cmd.Parameters.Add(new OracleParameter { IsNullable = true, Direction = ParameterDirection.Input, ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Value = client.ClientCode });

                cmd.Parameters.Add(new OracleParameter { IsNullable = true, Direction = ParameterDirection.Input, ParameterName = "pFORMATO", OracleType = OracleType.Char, Value = f.SendFormat.ToCharArray()[0] });

                cmd.Parameters.Add(new OracleParameter { IsNullable = true, Direction = ParameterDirection.Input, ParameterName = "pIdFuncionalidade", OracleType = OracleType.Number, Value = f.FuncId });

                cmd.Parameters.Add(new OracleParameter { IsNullable = true, Direction = ParameterDirection.Input, ParameterName = "TP_OPERACAO", OracleType = OracleType.Char, Value = 'I' });

                cmd.Parameters.Add(new OracleParameter { IsNullable = true, Direction = ParameterDirection.InputOutput, ParameterName = "pCLIENTE_FUNC", OracleType = OracleType.Number, Size = 38 });

                var result = cmd.ExecuteNonQuery();

                if (result > 0)
                {
                    var aux = ManageEmailForFuncionality(client.ClientCode, f, 'I');
                    if (totalResult)
                        totalResult = aux;
                }
                else
                {
                    if (totalResult)
                        totalResult = false;
                }
            }

            return totalResult;
        }

        public bool DeleteClientForFuncionality(FuncionalityClient client)
        {
            /*PROC_CADASTRO_CLIENTE ( 
              pCD_CLIENTE                         in number,
              pFORMATO                            in char,
              pIdFuncionalidade                   in number,
              TP_OPERACAO                         in char,
              pCLIENTE_FUNC                       in out number*/

            var cmd = dbi.CreateCommand("PCK_CLIENTES_FUNCIONALIDADE.PROC_CADASTRO_CLIENTE", CommandType.StoredProcedure);

            cmd.Parameters.Clear();

            cmd.Parameters.Add(new OracleParameter { IsNullable = true, Direction = ParameterDirection.Input, ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Value = client.ClientCode });

            cmd.Parameters.Add(new OracleParameter { IsNullable = true, Direction = ParameterDirection.Input, ParameterName = "pFORMATO", OracleType = OracleType.Char, Value = DBNull.Value });

            cmd.Parameters.Add(new OracleParameter { IsNullable = true, Direction = ParameterDirection.Input, ParameterName = "pIdFuncionalidade", OracleType = OracleType.Number, Value = DBNull.Value });

            cmd.Parameters.Add(new OracleParameter { IsNullable = true, Direction = ParameterDirection.Input, ParameterName = "TP_OPERACAO", OracleType = OracleType.Char, Value = 'E' });

            cmd.Parameters.Add(new OracleParameter { IsNullable = true, Direction = ParameterDirection.InputOutput, ParameterName = "pCLIENTE_FUNC", OracleType = OracleType.Number, Size = 38 });

            var result = cmd.ExecuteNonQuery();

            return result > 0;
        }

        public bool UpdateClientForFuncionality(FuncionalityClient client)
        {

            var cmd = dbi.CreateCommand("PCK_CLIENTES_FUNCIONALIDADE.PROC_CADASTRO_CLIENTE", CommandType.StoredProcedure);
            var totalResult = true;


            //Excluir emails antigos
            var cmdEmail = dbi.CreateCommand("PCK_CLIENTES_FUNCIONALIDADE.PROC_CADASTRO_EMAIL", CommandType.StoredProcedure);

            cmdEmail.Parameters.Clear();

            cmdEmail.Parameters.Add(new OracleParameter { IsNullable = true, Direction = ParameterDirection.Input, ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Value = client.ClientCode });

            cmdEmail.Parameters.Add(new OracleParameter { IsNullable = true, Direction = ParameterDirection.Input, ParameterName = "pNM_EMAIL", OracleType = OracleType.VarChar, Value = DBNull.Value });

            cmdEmail.Parameters.Add(new OracleParameter { IsNullable = true, Direction = ParameterDirection.Input, ParameterName = "pFUNCIONALIDADE", OracleType = OracleType.Number, Value = DBNull.Value });

            cmdEmail.Parameters.Add(new OracleParameter { IsNullable = true, Direction = ParameterDirection.Input, ParameterName = "TP_OPERACAO", OracleType = OracleType.Char, Value = 'E' });

            cmdEmail.Parameters.Add(new OracleParameter { IsNullable = true, Direction = ParameterDirection.Input, ParameterName = "pTP_EMAIL", OracleType = OracleType.Char, Value = DBNull.Value });

            cmdEmail.Parameters.Add(new OracleParameter { IsNullable = true, Direction = ParameterDirection.InputOutput, ParameterName = "pID_EMAIL", OracleType = OracleType.Number, Size = 38 });

            var resultEmail = cmdEmail.ExecuteNonQuery();

            if (resultEmail <= 0 && totalResult)
                totalResult = false;

            foreach (var f in client.Funcionalities)
            {
                if (f.Emails == null || f.Emails.Count <= 0 || String.IsNullOrEmpty(f.SendFormat))
                    continue;

                //incluir novos emails
                cmd.Parameters.Clear();

                cmd.Parameters.Add(new OracleParameter { IsNullable = true, Direction = ParameterDirection.Input, ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Value = client.ClientCode });

                cmd.Parameters.Add(new OracleParameter { IsNullable = true, Direction = ParameterDirection.Input, ParameterName = "pFORMATO", OracleType = OracleType.Char, Value = f.SendFormat.ToCharArray()[0] });

                cmd.Parameters.Add(new OracleParameter { IsNullable = true, Direction = ParameterDirection.Input, ParameterName = "pIdFuncionalidade", OracleType = OracleType.Number, Value = f.FuncId });

                cmd.Parameters.Add(new OracleParameter { IsNullable = true, Direction = ParameterDirection.Input, ParameterName = "TP_OPERACAO", OracleType = OracleType.Char, Value = 'A' });

                cmdEmail.Parameters.Add(new OracleParameter { IsNullable = true, Direction = ParameterDirection.Input, ParameterName = "pTP_EMAIL", OracleType = OracleType.Char, Value = DBNull.Value });

                cmd.Parameters.Add(new OracleParameter { IsNullable = true, Direction = ParameterDirection.InputOutput, ParameterName = "pCLIENTE_FUNC", OracleType = OracleType.Number, Size = 38 });

                var result = cmd.ExecuteNonQuery();

                if (result > 0)
                {
                    var aux = ManageEmailForFuncionality(client.ClientCode, f, 'I');
                    if (totalResult)
                        totalResult = aux;
                }
                else
                {
                    if (totalResult)
                        totalResult = false;
                }
            }

            return totalResult;
        }

        private bool ManageEmailForFuncionality(int clientCode, Funcionality funcionality, char operation)
        {
            /*
                pCD_CLIENTE         in number,
                pNM_EMAIL           in varchar2,
                pFUNCIONALIDADE     in number,
                TP_OPERACAO         in char,
                pID_EMAIL           in out number
                */
            var cmdEmail = dbi.CreateCommand("PCK_CLIENTES_FUNCIONALIDADE.PROC_CADASTRO_EMAIL", CommandType.StoredProcedure);
            var result = true;

            foreach (var e in funcionality.Emails)
            {
                cmdEmail.Parameters.Clear();

                cmdEmail.Parameters.Add(new OracleParameter { IsNullable = true, Direction = ParameterDirection.Input, ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Value = clientCode });

                cmdEmail.Parameters.Add(new OracleParameter { IsNullable = true, Direction = ParameterDirection.Input, ParameterName = "pNM_EMAIL", OracleType = OracleType.VarChar, Value = e.Email });

                cmdEmail.Parameters.Add(new OracleParameter { IsNullable = true, Direction = ParameterDirection.Input, ParameterName = "pFUNCIONALIDADE", OracleType = OracleType.Number, Value = funcionality.FuncId });

                cmdEmail.Parameters.Add(new OracleParameter { IsNullable = true, Direction = ParameterDirection.Input, ParameterName = "TP_OPERACAO", OracleType = OracleType.Char, Value = operation });

                cmdEmail.Parameters.Add(new OracleParameter { IsNullable = true, Direction = ParameterDirection.Input, ParameterName = "pTP_EMAIL", OracleType = OracleType.Char, Value = 'P' });

                cmdEmail.Parameters.Add(new OracleParameter { IsNullable = true, Direction = ParameterDirection.InputOutput, ParameterName = "pID_EMAIL", OracleType = OracleType.Number, Size = 38 });

                var resultEmail = cmdEmail.ExecuteNonQuery();

                if (resultEmail <= 0 && result)
                    result = false;
            }

            foreach (var e in funcionality.CopyTo)
            {
                cmdEmail.Parameters.Clear();

                cmdEmail.Parameters.Add(new OracleParameter { IsNullable = true, Direction = ParameterDirection.Input, ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Value = clientCode });

                cmdEmail.Parameters.Add(new OracleParameter { IsNullable = true, Direction = ParameterDirection.Input, ParameterName = "pNM_EMAIL", OracleType = OracleType.VarChar, Value = e.Email });

                cmdEmail.Parameters.Add(new OracleParameter { IsNullable = true, Direction = ParameterDirection.Input, ParameterName = "pFUNCIONALIDADE", OracleType = OracleType.Number, Value = funcionality.FuncId });

                cmdEmail.Parameters.Add(new OracleParameter { IsNullable = true, Direction = ParameterDirection.Input, ParameterName = "TP_OPERACAO", OracleType = OracleType.Char, Value = operation });

                cmdEmail.Parameters.Add(new OracleParameter { IsNullable = true, Direction = ParameterDirection.Input, ParameterName = "pTP_EMAIL", OracleType = OracleType.Char, Value = 'C' });

                cmdEmail.Parameters.Add(new OracleParameter { IsNullable = true, Direction = ParameterDirection.InputOutput, ParameterName = "pID_EMAIL", OracleType = OracleType.Number, Size = 38 });

                var resultEmail = cmdEmail.ExecuteNonQuery();

                if (resultEmail <= 0 && result)
                    result = false;
            }

            return result;
        }

        public List<Funcionality> ListFuncionalities()
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PROC_LISTA_FUNCIONALIDADES", CommandType.StoredProcedure);
            cmd.Connection.Open();
            List<Funcionality> result = null;

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    result = new List<Funcionality>();
                    while (dr.Read())
                    {
                        Funcionality func = new Funcionality();
                        func.FuncId = dr.GetInt32(0);
                        func.FuncionalityName = dr.GetString(1);

                        result.Add(func);
                    }
                }

                dr.Close();

            }
            cmd.Connection.Close();
            return result;
        }

        #endregion

        public List<ACUser> ListClientsSINACOR(string clientes)
        {
            List<ACUser> elements = new List<ACUser>();

            /*var cmd = dbi.CreateCommand(AccessControlResources.LoadElementsIDByProfile, CommandType.Text);
            cmd.Parameters.Add(dbi.CreateParam("ProfileID", profileID));*/

            var cmd = dbi.CreateCommand(@"PCK_CONTROLE_ACESSO.PROC_LISTA_CLIENTES_SINACOR", CommandType.StoredProcedure);

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pClientes", Value = clientes, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pEmpresa", Value = Convert.ToInt32(ConfigurationManager.AppSettings["CD_EMPRESA"]), OracleType = OracleType.Number, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCursor", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    ACUser element = new ACUser();

                    if (!(dr["CD_CLIENTE"] is DBNull))
                        element.CdCliente = Convert.ToInt32(dr["CD_CLIENTE"]);

                    if (!(dr["NM_CLIENTE"] is DBNull))
                        element.Name = Convert.ToString(dr["NM_CLIENTE"]);

                    if (!(dr["NM_E_MAIL"] is DBNull))
                        element.Email = Convert.ToString(dr["NM_E_MAIL"]);

                    if (!(dr["CD_CPFCGC"] is DBNull))
                        element.CdCpfCnpj = Convert.ToDecimal(dr["CD_CPFCGC"]);

                    elements.Add(element);
                }
                dr.Close();
            }

            return elements;
        }

        //public bool Verify_SESSION_Login(string username)
        //{
        //    List<ACUser> elements = new List<ACUser>();

        //    var cmd = dbi.CreateCommand(@"SELECT count(*) FROM TB_CONTROL_SESSION WHERE USERNAME = :l_username", CommandType.Text);

        //    cmd.Parameters.Add(new OracleParameter { ParameterName = "l_username", Value = username, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input });

        //    int i = Convert.ToInt32(cmd.ExecuteScalar());

        //    if (i > 0)
        //        return true;
        //    else
        //        return false;
        //}

        public bool Verify_SESSION_Login(string username)
        {
            List<ACUser> elements = new List<ACUser>();

            DBInterface dbiCRK = new DBInterface("PORTAL");

            var cmd = (SqlCommand)dbiCRK.CreateCommand(@"SELECT count(*) FROM TB_CONTROL_SESSION WHERE USERNAME = :l_username", CommandType.Text);
            cmd.Parameters.Add(new SqlParameter { ParameterName = "l_username", Value = username, SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input });

            cmd.Connection.Open();

            int i = Convert.ToInt32(cmd.ExecuteScalar());

            if (i > 0)
                return true;
            else
                return false;
        }

        //public bool Delete_SESSION_Login(string username)
        //{
        //    List<ACUser> elements = new List<ACUser>();

        //    var cmd = dbi.CreateCommand(@"DELETE FROM TB_CONTROL_SESSION WHERE USERNAME = :l_username", CommandType.Text);

        //    cmd.Parameters.Add(new OracleParameter { ParameterName = "l_username", Value = username, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input });


        //    int i = cmd.ExecuteNonQuery();

        //    if (i > 0)
        //        return true;
        //    else
        //        return false;


        //}

        public bool Delete_SESSION_Login(string username)
        {
            List<ACUser> elements = new List<ACUser>();

            DBInterface dbiCRK = new DBInterface("PORTAL");

            var cmd = (SqlCommand)dbiCRK.CreateCommand(@"DELETE FROM TB_CONTROL_SESSION WHERE USERNAME = :l_username", CommandType.Text);

            cmd.Parameters.Add(new SqlParameter { ParameterName = "l_username", Value = username, SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input });

            cmd.Connection.Open();

            int i = cmd.ExecuteNonQuery();

            if (i > 0)
                return true;
            else
                return false;


        }

        //        public bool Insert_SESSION(string username, string ipClient, DateTime dt, string sessionId)
        //        {
        //            List<ACUser> elements = new List<ACUser>();

        //            var cmd = dbi.CreateCommand(@"INSERT INTO TB_CONTROL_SESSION (USERNAME, INSERTDATE, SESSIONID, IP)
        //                            VALUES (:l_username, :l_insertdate, :l_sessionid, :l_ip)", CommandType.Text);

        //            cmd.Parameters.Add(new OracleParameter { ParameterName = "l_username", Value = username, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input });
        //            cmd.Parameters.Add(new OracleParameter { ParameterName = "l_insertdate", Value = dt, OracleType = OracleType.DateTime, Direction = ParameterDirection.Input });
        //            cmd.Parameters.Add(new OracleParameter { ParameterName = "l_sessionid", Value = sessionId, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input });
        //            cmd.Parameters.Add(new OracleParameter { ParameterName = "l_ip", Value = ipClient, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input });
        //            int i = cmd.ExecuteNonQuery();

        //            if (i > 0)
        //                return true;
        //            else
        //                return false;

        //        }

        public bool Insert_SESSION(string username, string ipClient, DateTime dt, string sessionId)
        {
            List<ACUser> elements = new List<ACUser>();

            DBInterface dbiCRK = new DBInterface("PORTAL");

            var cmd = (SqlCommand)dbiCRK.CreateCommand(@"INSERT INTO TB_CONTROL_SESSION (USERNAME, INSERTDATE, SESSIONID, IP)
                            VALUES (:l_username, :l_insertdate, :l_sessionid, :l_ip)", CommandType.Text);


            cmd.Parameters.Add(new SqlParameter { ParameterName = "l_username", Value = username, SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "l_insertdate", Value = dt, SqlDbType = SqlDbType.DateTime, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "l_sessionid", Value = sessionId, SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "l_ip", Value = ipClient, SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input });

            cmd.Connection.Open();

            int i = cmd.ExecuteNonQuery();

            if (i > 0)
                return true;
            else
                return false;

        }

        //public bool Verify_SESSION(string username, string ipClient)
        //{
        //    List<ACUser> elements = new List<ACUser>();

        //    var cmd = dbi.CreateCommand(@"SELECT count(*) FROM TB_CONTROL_SESSION WHERE IP = :l_ip AND USERNAME = :l_username", CommandType.Text);

        //    cmd.Parameters.Add(new OracleParameter { ParameterName = "l_ip", Value = ipClient, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input });
        //    cmd.Parameters.Add(new OracleParameter { ParameterName = "l_username", Value = username, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input });

        //    int i = Convert.ToInt32(cmd.ExecuteScalar());

        //    if (i > 0)
        //        return true;
        //    else
        //        return false;



        //}

        public bool Verify_SESSION(string username, string ipClient)
        {
            List<ACUser> elements = new List<ACUser>();

            using (DBInterface dbiCRK = new DBInterface("PORTAL"))
            {

                var cmd = (SqlCommand)dbiCRK.CreateCommand(@"SELECT count(*) FROM TB_CONTROL_SESSION WHERE IP = :l_ip AND USERNAME = :l_username", CommandType.Text);

                cmd.Parameters.Add(new SqlParameter { ParameterName = "l_ip", Value = ipClient, SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "l_username", Value = username, SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input });

                cmd.Connection.Open();

                int i = Convert.ToInt32(cmd.ExecuteScalar());

                if (i > 0)
                    return true;
                else
                    return false;
            }
        }

        //public bool Verify_SESSION_IP(string ipClient)
        //{
        //    List<ACUser> elements = new List<ACUser>();

        //    var cmd = dbi.CreateCommand(@"SELECT count(*) FROM TB_CONTROL_SESSION WHERE IP = :l_ip", CommandType.Text);

        //    cmd.Parameters.Add(new OracleParameter { ParameterName = "l_ip", Value = ipClient, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input });

        //    int i = Convert.ToInt32(cmd.ExecuteScalar());

        //    if (i > 0)
        //        return true;
        //    else
        //        return false;



        //}

        public bool Verify_SESSION_IP(string ipClient)
        {
            List<ACUser> elements = new List<ACUser>();

            using (DBInterface dbiCRK = new DBInterface("PORTAL"))
            {

                var cmd = (SqlCommand)dbiCRK.CreateCommand(@"SELECT count(*) FROM TB_CONTROL_SESSION WHERE IP = :l_ip", CommandType.Text);

                cmd.Parameters.Add(new SqlParameter { ParameterName = "l_ip", Value = ipClient, SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input });

                cmd.Connection.Open();

                int i = Convert.ToInt32(cmd.ExecuteScalar());

                if (i > 0)
                    return true;
                else
                    return false;
            }
        }

        public List<UserInfo> ListUsers()
        {
            List<UserInfo> users = new List<UserInfo>();

            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PR_LISTAR_USUARIOS", CommandType.StoredProcedure);

            cmd.Connection.Open();

            using (var dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    UserInfo user = new UserInfo();

                    if (!(dr["USERID"] is DBNull))
                        user.Id = Convert.ToInt32(dr["USERID"]);
                    if (!(dr["NAME"] is DBNull))
                        user.Name = Convert.ToString(dr["NAME"]);
                    if (!(dr["EMAIL"] is DBNull))
                        user.Email = Convert.ToString(dr["EMAIL"]);
                    if (!(dr["ID_CLIENTE"] is DBNull))
                        user.IdCliente = Convert.ToInt32(dr["ID_CLIENTE"]);
                    if (!(dr["CD_CPFCNPJ"] is DBNull))
                        user.CpfCnpj = Convert.ToInt64(dr["CD_CPFCNPJ"]);

                    users.Add(user);
                }

                dr.Close();
            }

            cmd.Connection.Close();

            return users;
        }

        public bool UpdateUsers(List<UserInfo> users)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PR_ALTERAR_USUARIO", CommandType.StoredProcedure);
            var totalResult = true;

            foreach (var user in users)
            {
                cmd.Parameters.Clear();

                cmd.Parameters.Add(new SqlParameter { ParameterName = "@pIdUsuario", Value = user.Id, SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@pNome", Value = user.Name, SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@pEmail", Value = user.Email, SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input });

                if (user.IdCliente.HasValue)
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@pCdCliente", Value = user.IdCliente, SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input });
                else
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@pCdCliente", Value = DBNull.Value, SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input });

                if (user.CpfCnpj.HasValue)
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@pCdCpfCnpj", Value = user.CpfCnpj, SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Input });
                else
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@pCdCpfCnpj", Value = DBNull.Value, SqlDbType = SqlDbType.BigInt, Direction = ParameterDirection.Input });

                cmd.Connection.Open();

                var result = cmd.ExecuteNonQuery();

                cmd.Connection.Close();
            }

            return totalResult;
        }
    }
}
