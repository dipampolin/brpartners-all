﻿using System;
using System.Collections.Generic;
using QX3.Spinnex.Common.Services.Data;
using System.Data;
using System.Data.OracleClient;
using QX3.Portal.Contracts.DataContracts;
using System.Linq;

namespace QX3.Portal.Services.Data
{
    public class OnlinePortfolioDAL : DataProviderBase
    {
        public OnlinePortfolioDAL() : base("PortalConnectionString") { }

        public bool InsertContact(int clientCode, PortfolioContactItem contact)
        {
            var cmd = dbi.CreateCommand("PCK_COL_CADASTRO.PROC_REGISTRA_CONTATO", CommandType.StoredProcedure);

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = clientCode });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_CONTATO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = contact.ContactDate });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pDS_CONTATO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = contact.Contact });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_OPERADOR", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = contact.Operator.Value });

            return cmd.ExecuteNonQuery() > 0;
        }

        public PortfolioContact LoadClientContacts(int clientCode)
        {
            PortfolioContact client = new PortfolioContact();
            List<PortfolioContactItem> contacts = new List<PortfolioContactItem>();

            var cmd = dbi.CreateCommand("PCK_COL_CADASTRO.PROC_CONSULTA_CONTATO", CommandType.StoredProcedure);

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = clientCode });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_CONTATO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_OPERADOR", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSOR", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSOR2", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.Read())
                {
                    client.Client = new CommonType { Value = Convert.ToString(dr["CdCliente"]), Description = Convert.ToString(dr["NomeCliente"]) };
                    client.ClientEmail = Convert.ToString(dr["Email"]);
                    client.ClientPhone = Convert.ToString(dr["NumeroTelefone"]);
                    client.Contacts = new List<PortfolioContactItem>();

                    if (dr.NextResult())
                    {
                        client.Contacts = new List<PortfolioContactItem>();

                        while (dr.Read())
                        {
                            PortfolioContactItem contact = new PortfolioContactItem();
                            contact.Operator = new CommonType { Value = Convert.ToString(dr["CdOperador"]), Description = Convert.ToString(dr["NomeOperador"]) };
                            if (!(dr["DataContato"] is DBNull))
                                contact.ContactDate = Convert.ToDateTime(dr["DataContato"]).ToString("dd/MM/yyyy");
                            if (!(dr["DataRegistroContato"] is DBNull))
                                contact.InsertDate = Convert.ToString(dr["DataRegistroContato"]);
                            if (!(dr["Contato"] is DBNull))
                                contact.Contact = Convert.ToString(dr["Contato"]);
                            client.Contacts.Add(contact);
                        }
                    }
                }
                dr.Close();
            }

            return client;
        }

        public List<CommonType> SearchClients(string nameFilter, long operatorId, int maxItems)
        {
            List<CommonType> clients = new List<CommonType>();

            var cmd = dbi.CreateCommand("PCK_COL_CADASTRO.PROC_BUSCA_CLIENTE", CommandType.StoredProcedure);
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pNmCliente", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = nameFilter });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIdUsuario", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = operatorId });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pMaxItems", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = maxItems });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSOR", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            var dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                clients.Add(new CommonType { Value = Convert.ToString(dr["CdCliente"]), Description = Convert.ToString(dr["NomeCliente"]) });
            }
            dr.Close();

            return clients;
        }

        public List<PortfolioOperator> LoadOperators(string code)
        {
            List<PortfolioOperator> lst = new List<PortfolioOperator>();

            var cmd = dbi.CreateCommand("PCK_COL_CADASTRO.PROC_COMBO_OPERADOR", CommandType.StoredProcedure);

            if (string.IsNullOrEmpty(code))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_OPERADOR", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_OPERADOR", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = code });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSOR", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    PortfolioOperator obj = new PortfolioOperator();

                    obj.Code = Convert.ToInt32(dr["CD_OPERADOR"]);
                    obj.Name = Convert.ToString(dr["NM_OPERADOR"]);

                    lst.Add(obj);
                }

                dr.Close();
            }

            return lst;
        }

        public PortfolioClient LoadClientName(int clientId)
        {
            PortfolioClient obj = new PortfolioClient();

            var cmd = dbi.CreateCommand("PCK_COL_CADASTRO.PROC_CONSULTA_OPERADOR", CommandType.StoredProcedure);

            cmd.Parameters.Add(new OracleParameter
            {
                ParameterName = "pCD_OPERADOR",
                OracleType = OracleType.VarChar,
                Direction = ParameterDirection.Input,
                Value = clientId
            });

            using (var dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    obj.Client = new CommonType { Value = dr["CD_CLIENTE"].ToString(), Description = dr["NM_CLIENTE"].ToString() };
                }

                dr.Close();
            }

            return obj;
        }

        public bool UpdateObservation(int clientId, string Observation)
        {
            var cmd = dbi.CreateCommand("PCK_COL_APURACAO.PROC_MANTEM_CONTATO", CommandType.StoredProcedure);

            cmd.Parameters.Add(new OracleParameter
            {
                ParameterName = "pCD_CLIENTE",
                OracleType = OracleType.Number,
                Direction = ParameterDirection.Input,
                Value = clientId
            });

            cmd.Parameters.Add(new OracleParameter
            {
                ParameterName = "pDS_CONTATO",
                OracleType = OracleType.VarChar,
                Direction = ParameterDirection.Input,
                Value = Observation
            });

            return cmd.ExecuteNonQuery() > 0;
        }

        public string LoadObservation(int clientId)
        {
            string observation = string.Empty;
            var cmd = dbi.CreateCommand("PCK_COL_APURACAO.PROC_CARREGA_OBS", CommandType.StoredProcedure);

            cmd.Parameters.Add(new OracleParameter
            {
                ParameterName = "pCD_CLIENTE",
                OracleType = OracleType.Number,
                Direction = ParameterDirection.Input,
                Value = clientId
            });

            cmd.Parameters.Add(new OracleParameter
            {
                ParameterName = "pCURSOR",
                OracleType = OracleType.Cursor,
                Direction = ParameterDirection.Output
            });

            using (var dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    observation = dr["DS_CONTATO"].ToString();
                }

                dr.Close();
            }

            return observation;
        }

        public List<Int32> LoadOperatorClients(int operatorID, int filter)
        {
            List<Int32> list = new List<Int32>();

            var cmd = dbi.CreateCommand("PCK_COL_APURACAO.PROC_CARREGA_CLIENTES_OPERADOR", CommandType.StoredProcedure);
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_OPERADOR", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = operatorID });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_TIPO", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = filter });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSOR", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    list.Add(Convert.ToInt32(dr["CD_CLIENTE"]));
                }

                dr.Close();
            }

            return list;
        }

        public bool InsertClient(PortfolioClient obj, bool isUpdate)
        {
            var cmd = dbi.CreateCommand("PCK_COL_CADASTRO.PROC_MANTEM_CLIENTE", CommandType.StoredProcedure);

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = Convert.ToInt32(obj.Client.Value) });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pTP_APURACAO", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value =  Convert.ToInt32(obj.Calculation.Value) });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pTP_OPERACAO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = isUpdate ? "U" : "I" });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_ENVIA_HIST", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = obj.SendHistory ? "S" : "N" });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_ENVIA_PDF", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = obj.SendAttachType });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_INICIO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = obj.StartDate.ToString("dd/MM/yyyy") });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pVALOR_INICIAL", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = obj.InitialValue });

            return cmd.ExecuteNonQuery() > 0;
        }

        public bool DeleteClient(string code)
        {
            var cmd = dbi.CreateCommand("PCK_COL_CADASTRO.PROC_MANTEM_CLIENTE", CommandType.StoredProcedure);

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = Convert.ToInt32(code) });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pTP_APURACAO", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pTP_OPERACAO", OracleType = OracleType.Char, Direction = ParameterDirection.Input, Value = "D" });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_ENVIA_HIST", OracleType = OracleType.Char, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_ENVIA_PDF", OracleType = OracleType.Char, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_INICIO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pVALOR_INICIAL", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });

            return cmd.ExecuteNonQuery() > 0;
        }

        public List<PortfolioClient> LoadPortfolioClients(PortfolioClientFilter filter)
        {
            List<PortfolioClient> lst = new List<PortfolioClient>();

            var cmd = dbi.CreateCommand("PCK_COL_CADASTRO.PROC_CONSULTA_CLIENTE", CommandType.StoredProcedure);

            if (string.IsNullOrEmpty(filter.Operator))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_OPERADOR", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_OPERADOR", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = Convert.ToInt32(filter.Operator) });

            if (string.IsNullOrEmpty(filter.Client))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = Convert.ToInt32(filter.Client) });

            if (string.IsNullOrEmpty(filter.Calculation))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pTP_APURACAO", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pTP_APURACAO", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = Convert.ToInt32(filter.Calculation) });

            if (string.IsNullOrEmpty(filter.SendHistory))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_ENVIA_HIST", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_ENVIA_HIST", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.SendHistory });

            if (string.IsNullOrEmpty(filter.SendAttachType))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_ENVIA_PDF", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_ENVIA_PDF", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.SendAttachType });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSOR", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            var dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                PortfolioClient obj = new PortfolioClient();

                if (!(dr["CD_CLIENTE"] is DBNull) && !(dr["NM_CLIENTE"] is DBNull))
                    obj.Client = new CommonType { Value = Convert.ToString(dr["CD_CLIENTE"]), Description = Convert.ToString(dr["NM_CLIENTE"]) };
                if (!(dr["TP_APURACAO"] is DBNull) && !(dr["DS_APURACAO"] is DBNull))
                    obj.Calculation = new CommonType { Value = Convert.ToString(dr["TP_APURACAO"]), Description = Convert.ToString(dr["DS_APURACAO"]) };
                if (!(dr["IN_ENVIA_HIST"] is DBNull))
                    obj.SendHistory = Convert.ToString(dr["IN_ENVIA_HIST"]).Equals("S");
                if (!(dr["IN_ENVIA_PDF"] is DBNull))
                    obj.SendAttachType = Convert.ToString(dr["IN_ENVIA_PDF"]);
                if (!(dr["DT_INICIO_CARTEIRA"] is DBNull))
                    obj.StartDate = Convert.ToDateTime(Convert.ToString(dr["DT_INICIO_CARTEIRA"]));
                if (!(dr["VALOR_INICIAL"] is DBNull))
                    obj.InitialValue = Convert.ToDecimal(dr["VALOR_INICIAL"]);

                lst.Add(obj);
            }

            dr.Close();

            return lst;
        }

        public List<CommonType> LoadCalculationTypes()
        {
            List<CommonType> lst = new List<CommonType>();

            var cmd = dbi.CreateCommand("PCK_COL_CADASTRO.PROC_COMBO_TIPO_APURACAO", CommandType.StoredProcedure);

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pTP_APURACAO", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSOR", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                    lst.Add(new CommonType { Value = Convert.ToString(dr["TP_APURACAO"]), Description = Convert.ToString(dr["DS_APURACAO"]) });

                dr.Close();
            }

            return lst;

        }

        public PortfolioPosition LoadClientPortfolioPosition(int clientCode, long operatorID, string isHistory)
        {
            PortfolioPosition position = new PortfolioPosition();
            List<PortfolioContactItem> contacts = new List<PortfolioContactItem>();

            var cmd = dbi.CreateCommand("PCK_COL_APURACAO.PROC_CONSULTA_INTRA_DAY", CommandType.StoredProcedure);

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = clientCode });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_OPERADOR", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = operatorID.ToString() });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pHISTORICO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = isHistory });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSOR", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSOR2", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.Read())
                {

                    if (!(dr["CD_CLIENTE"] is DBNull) && !(dr["NM_CLIENTE"] is DBNull))
                        position.Client = new CommonType { Value = Convert.ToString(dr["CD_CLIENTE"]), Description = Convert.ToString(dr["NM_CLIENTE"]) };
                    if (!(dr["NR_TELEFONE"] is DBNull))
                        position.Phone = Convert.ToString(dr["NR_TELEFONE"]);
                    if (!(dr["NM_EMAIL"] is DBNull))
                        position.Email = Convert.ToString(dr["NM_EMAIL"]);
                    if (!(dr["DS_CONTATO"] is DBNull))
                        position.Comments = Convert.ToString(dr["DS_CONTATO"]);
                    if (!(dr["PC_DESCONTO"] is DBNull))
                        position.Discount = Convert.ToDecimal(dr["PC_DESCONTO"]);
                    if (!(dr["PC_DESCONTO_DT"] is DBNull))
                        position.DayTradeDiscount = Convert.ToDecimal(dr["PC_DESCONTO_DT"]);
                    if (!(dr["VL_DISPONIVEL"] is DBNull))
                        position.AvailableValue = Convert.ToDecimal(dr["VL_DISPONIVEL"]);
                    if (!(dr["VL_PROJETADO_D1"] is DBNull))
                        position.ProjectedValueD1 = Convert.ToDecimal(dr["VL_PROJETADO_D1"]);
                    if (!(dr["VL_PROJETADO_D3"] is DBNull))
                        position.ProjectedValueD2 = Convert.ToDecimal(dr["VL_PROJETADO_D2"]);
                    if (!(dr["VL_PROJETADO_D3"] is DBNull))
                        position.ProjectedValueD3 = Convert.ToDecimal(dr["VL_PROJETADO_D3"]);
                    if (!(dr["VL_NEGOCIOS_DIA"] is DBNull))
                        position.TradesValue = Convert.ToDecimal(dr["VL_NEGOCIOS_DIA"]);
                    if (!(dr["VL_ULTIMO_IBOV_ANO"] is DBNull))
                        position.IbovValue = Convert.ToDecimal(dr["VL_ULTIMO_IBOV_ANO"]);
                    if (!(dr["DT_ULTIMO_IBOV_ANO"] is DBNull))
                        position.IbovDate = Convert.ToDateTime(dr["DT_ULTIMO_IBOV_ANO"]);
                    if (!(dr["VL_IBOV_D_1"] is DBNull))
                        position.IbovValueD1 = Convert.ToDecimal(dr["VL_IBOV_D_1"]);
                    if (!(dr["In_Envia_Hist"] is DBNull))
                        position.SendHistory = Convert.ToString(dr["In_Envia_Hist"]) == "S";
                    if (!(dr["In_Envia_Pdf"] is DBNull))
                        position.SendPdf = Convert.ToString(dr["In_Envia_Pdf"]) == "S";
                    if (!(dr["DT_INICIO_CARTEIRA"] is DBNull))
                        position.StartDate = Convert.ToDateTime(dr["DT_INICIO_CARTEIRA"]);
                    if (!(dr["VL_INICIO"] is DBNull))
                        position.StartValue = Convert.ToDecimal(dr["VL_INICIO"]);
                    if (!(dr["in_vinculado"] is DBNull))
                        position.Bound = Convert.ToString(dr["in_vinculado"]);

                    if (dr.NextResult())
                    {
                        position.Details = new List<PortfolioPositionItem>();

                        while (dr.Read())
                        {
                            PortfolioPositionItem item = new PortfolioPositionItem();
                            if (!(dr["TP_MERCADO"] is DBNull))
                                item.MarketType = Convert.ToString(dr["TP_MERCADO"]);
                            if (!(dr["CD_NEGOCIO"] is DBNull))
                                item.StockCode = Convert.ToString(dr["CD_NEGOCIO"]);
                            if (!(dr["CD_ATIVO_OBJETO"] is DBNull))
                                item.TermStockCode = Convert.ToString(dr["CD_ATIVO_OBJETO"]);
                            if (!(dr["IN_POSICAO"] is DBNull))
                                item.Type = Convert.ToString(dr["IN_POSICAO"]);
                            if (!(dr["DT_NEGOCIO"] is DBNull))
                                item.BuyDate = Convert.ToString(dr["DT_NEGOCIO"]);
                            if (!(dr["DT_VENCIMENTO_TERMO"] is DBNull))
                                item.DueDate = Convert.ToString(dr["DT_VENCIMENTO_TERMO"]);
                            if (!(dr["DT_ROLAGEM_TERMO"] is DBNull))
                                item.RollOverDate = Convert.ToString(dr["DT_ROLAGEM_TERMO"]);
                            if (!(dr["QT_QTDESP"] is DBNull))
                                item.Quantity = Convert.ToInt64(dr["QT_QTDESP"]);
                            if (!(dr["PU_COMPRA"] is DBNull))
                                item.BuyValue = Convert.ToDecimal(dr["PU_COMPRA"]);
                            if (!(dr["VL_COMPRA_LIQUIDO"] is DBNull))
                                item.BuyNetValue = Convert.ToDecimal(dr["VL_COMPRA_LIQUIDO"]);
                            if (!(dr["VL_VENDA_LIQUIDO"] is DBNull))
                                item.SellNetValue = Convert.ToDecimal(dr["VL_VENDA_LIQUIDO"]);
                            if (!(dr["PU_MERCADO"] is DBNull))
                                item.Price = item.SellValue = Convert.ToDecimal(dr["PU_MERCADO"]);
                            if (!(dr["VL_LUCRO_PREJUIZO"] is DBNull))
                                item.ProfitLoss = Convert.ToDecimal(dr["VL_LUCRO_PREJUIZO"]);
                            if (!(dr["PC_LUCRO_PREJUIZO"] is DBNull))
                                item.Percentage = Convert.ToDecimal(dr["PC_LUCRO_PREJUIZO"]);
                            if (!(dr["DS_SETOR"] is DBNull))
                                item.Sector = Convert.ToString(dr["DS_SETOR"]);
                            if (!(dr["FT_COTACAO"] is DBNull))
                                item.Quotation = Convert.ToDecimal(dr["FT_COTACAO"]);
                            if (!(dr["DT_MOVIMENTO"] is DBNull))
                                item.HistoryDate = Convert.ToDateTime(dr["DT_MOVIMENTO"]);
                            if (!(dr["PC_ALIQUOTA"] is DBNull))
                                item.Rate = Convert.ToDecimal(dr["PC_ALIQUOTA"]);
                            if (!(dr["VL_ACRESCIMO"] is DBNull))
                                item.AdditionalValue = Convert.ToDecimal(dr["VL_ACRESCIMO"]);
                            if (!(dr["PC_EMOLUMENTO_BOVESPA"] is DBNull))
                                item.IbovFee = Convert.ToDecimal(dr["PC_EMOLUMENTO_BOVESPA"]);
                            if (!(dr["PC_EMOLUMENTO_CBLC"] is DBNull))
                                item.CblcFee = Convert.ToDecimal(dr["PC_EMOLUMENTO_CBLC"]);
                            item.MarketGroup = item.Type == "F" ? "Financiamento" : item.MarketType;
                            if (!(dr["CD_NATOPE"] is DBNull))
                                item.TradeType = Convert.ToChar(dr["CD_NATOPE"]);
                            position.Details.Add(item);
                        }
                    }

                }
            }


            return position;
        }

        public List<PortfolioOperator> LoadPortfolioOperators(string clientCode)
        {
            List<PortfolioOperator> lst = new List<PortfolioOperator>();

            var cmd = dbi.CreateCommand("PCK_COL_CADASTRO.PROC_CONSULTA_CLIENTE_OPERADOR", CommandType.StoredProcedure);

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = Convert.ToInt32(clientCode) });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSOR", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            var dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                PortfolioOperator obj = new PortfolioOperator();

                obj.Code = Convert.ToInt32(dr["ID"]);
                obj.Name = Convert.ToString(dr["NOME"]);

                lst.Add(obj);
            }

            dr.Close();

            return lst;
        }

        public bool InsertClientOperators(string clientCode, List<PortfolioOperator> operators)
        {
            string arrOperators = string.Empty;

            foreach (PortfolioOperator item in operators)
                arrOperators += string.Concat(item.Code, ";");

            if (!string.IsNullOrEmpty(arrOperators))
            {
                var cmd = dbi.CreateCommand("PCK_COL_CADASTRO.PROC_MANTEM_CLIENTE_OPERADOR", CommandType.StoredProcedure);

                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = Convert.ToInt32(clientCode) });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLst_OPERADORES", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = arrOperators });

                return cmd.ExecuteNonQuery() > 0;
            }

            return true;
        }

        public PortfolioClientSearchClients LoadSinacorClients(string term)
        {
            List<string> list = new List<string>();
            PortfolioClientSearchClients clients = new PortfolioClientSearchClients();

            int code = 0;
            bool tryParse = int.TryParse(term, out code);

            var search = dbi.CreateCommand("PCK_COL_CADASTRO.PROC_BUSCA_CLIENTE_SINACOR", CommandType.StoredProcedure);
            var verify = dbi.CreateCommand("PCK_COL_CADASTRO.PROC_VERIFICA_CLIENTE_EXISTE", CommandType.StoredProcedure);

            if (code.Equals(0))
            {
                verify.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
            }
            else
            {
                verify.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = Convert.ToInt32(term) });
            }

            verify.Parameters.Add(new OracleParameter { ParameterName = "pCURSOR", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = verify.ExecuteReader())
            {
                while (dr.Read())
                    list.Add(dr[0].ToString());
            }

            clients.Exist = list.Count > 0;

            if (code.Equals(0))
            {
                search.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
                search.Parameters.Add(new OracleParameter { ParameterName = "pNM_CLIENTE", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = term });
            }
            else
            {
                search.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = Convert.ToInt32(term) });
                search.Parameters.Add(new OracleParameter { ParameterName = "pNM_CLIENTE", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            }

            search.Parameters.Add(new OracleParameter { ParameterName = "pCURSOR", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            if (!clients.Exist)
            {
                using (var dr = search.ExecuteReader())
                {
                    clients.Clients = new List<CommonType>();
                    while (dr.Read())
                        clients.Clients.Add(new CommonType { Value = Convert.ToString(dr["CDCLIENTE"]), Description = Convert.ToString(dr["NOMECLIENTE"]) });
                }
            }

            return clients;
        }

        public bool VerifyClientExist(string term)
        {
            int code = 0;
            bool tryParse = int.TryParse(term, out code);
            List<string> lista = new List<string>();

            var cmd = dbi.CreateCommand("PCK_COL_CADASTRO.PROC_VERIFICA_CLIENTE_EXISTE", CommandType.StoredProcedure);

            if (code.Equals(0))
            {
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
            }
            else
            {
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = Convert.ToInt32(term) });
            }

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSOR", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                    lista.Add(Convert.ToString(dr[0]));

                dr.Close();
            }

            return lista.Count > 0;
        }

        public PortfolioPosition SearchClientPortfolioPosition(string clientCode, string operatorCode)
        {
            PortfolioPosition position = new PortfolioPosition();
            List<PortfolioContactItem> contacts = new List<PortfolioContactItem>();

            var cmd = dbi.CreateCommand("PCK_COL_CADASTRO.PROC_BUSCA_POSICAO_CLIENTE", CommandType.StoredProcedure);

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = Convert.ToInt32(clientCode) });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_OPERADOR", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = Convert.ToInt32(operatorCode) });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSOR", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSOR2", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.Read())
                {
                    position.Client = new CommonType { Value = Convert.ToString(dr["CD_CLIENTE"]), Description = Convert.ToString(dr["NM_CLIENTE"]) };
                    position.Phone = Convert.ToString(dr["NR_TELEFONE"]);
                    position.Email = Convert.ToString(dr["NM_EMAIL"]);

                    if (dr.NextResult())
                    {
                        position.Details = new List<PortfolioPositionItem>();

                        while (dr.Read())
                        {
                            PortfolioPositionItem item = new PortfolioPositionItem();

                            item.StockCode = Convert.ToString(dr["CD_NEGOCIO"]);
                            item.MarketType = Convert.ToString(dr["TP_MERCADO"]);
                            if (!(dr["CD_CARTEIRA"] is DBNull))
                                item.PortfolioNumber = Convert.ToInt32(dr["CD_CARTEIRA"]);
                            item.Quantity = Convert.ToInt64(dr["QT_QTDESP"]);
                            item.NetPrice = Convert.ToDecimal(dr["PU_CUSTO"]);
                            item.PositionDate = Convert.ToDateTime(dr["DT_POSICAO"]);
                            item.Operation = Convert.ToString(dr["CD_NATOPE"]);
                            item.Id = Convert.ToInt32(dr["ID_LINHA"]);
                            item.Price = Convert.ToDecimal(dr["PU_BRUTO"]);

                            if (!(dr["DT_VENCIMENTO"] is DBNull))
                                item.DueDate = Convert.ToDateTime(dr["DT_VENCIMENTO"]).ToShortDateString();
                            if (!(dr["DT_ROLAGEM"] is DBNull))
                                item.RollOverDate = Convert.ToDateTime(dr["DT_ROLAGEM"]).ToShortDateString();

                            item.LongShort = (!(dr["IN_LONG_SHORT"] is DBNull) && Convert.ToString(dr["IN_LONG_SHORT"]).Equals("S")) ? true : false;

                            position.Details.Add(item);
                        }
                    }

                }
            }


            return position;

        }

        public bool DeletePosition(string id)
        {
            var cmd = dbi.CreateCommand("PCK_COL_CADASTRO.PROC_MANTEM_POSICAO", CommandType.StoredProcedure);

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_NEGOCIO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pTP_MERCADO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CARTEIRA", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pQT_QTDESP", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pPU_CUSTO", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_POSICAO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_NATOPE", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_LINHA", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = id });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pPU_BRUTO", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_VENCIMENTO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_ROLAGEM", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_LONG_SHORT", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pTP_OPERACAO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = "D" });

            return cmd.ExecuteNonQuery() > 0;
        }

        public bool InsertPosition(PortfolioPosition obj, string sourcePosition)
        {
            var cmd = dbi.CreateCommand("PCK_COL_CADASTRO.PROC_MANTEM_POSICAO", CommandType.StoredProcedure);

            var allUpdated = true;

            foreach (PortfolioPositionItem item in obj.Details)
            {
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = Convert.ToInt32(obj.Client.Value) });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_NEGOCIO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = item.StockCode });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pTP_MERCADO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = item.MarketType });

                if (item.PortfolioNumber.Equals(0))
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CARTEIRA", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
                else
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CARTEIRA", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = Convert.ToInt32(item.PortfolioNumber) });

                cmd.Parameters.Add(new OracleParameter { ParameterName = "pQT_QTDESP", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = item.Quantity });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pPU_CUSTO", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = item.NetPrice });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_POSICAO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = item.PositionDate.ToString("dd/MM/yyyy") });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_NATOPE", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = item.Operation });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_LINHA", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = item.Id });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pPU_BRUTO", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = item.Price });

                if (item.MarketType.Equals("TER"))
                {
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_VENCIMENTO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = item.DueDate });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_ROLAGEM", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = item.RollOverDate });
                }
                else
                {
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_VENCIMENTO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_ROLAGEM", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
                }
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_LONG_SHORT", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = item.LongShort ? "S" : "N" });

                cmd.Parameters.Add(new OracleParameter { ParameterName = "pTP_OPERACAO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = item.Id.Equals(0) ? "I" : "U" });

                var result = cmd.ExecuteNonQuery() > 0;
                if (!result)
                    allUpdated = false;
            }

            return allUpdated;
        }
    }
}
