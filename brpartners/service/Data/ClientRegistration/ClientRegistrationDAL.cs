﻿using System;
using System.Collections.Generic;
using System.Data.OracleClient;
using QX3.Portal.Contracts.DataContracts;
using QX3.Spinnex.Common.Services.Data;
using System.Data;
using System.Data.SqlClient;

namespace QX3.Portal.Services.Data
{
    public class ClientRegistrationDAL : DataProviderBase
    {
        public ClientRegistrationDAL() : base("PortalConnectionString") { }

        public List<Client> LoadActiveAndInactiveClients(ClientFilter filter, FilterOptions filterOptions, int? userId, out int count)
        {
            List<Client> result = null; count = 0;

            var cmd = dbi.CreateCommand("PCK_CADASTRO.PROC_LISTA_CADASTRO_ATIVOS", CommandType.StoredProcedure);
            cmd.Parameters.Clear();

            if (filter.ClientCode == 0)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.ClientCode });

            if (String.IsNullOrEmpty(filter.AssessorFilterText))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTASSESSOR", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTASSESSOR", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.AssessorFilterText });

            if (filter.ClientFilterText == null)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTCLIENTE", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTCLIENTE", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.ClientFilterText });

            if (userId.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_USUARIO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = userId.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_USUARIO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });


            if (!filter.InitAcessionDate.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pdtinicio", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pdtinicio", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.InitAcessionDate.Value.ToString("dd/MM/yyyy") });

            if (!filter.FinalAcessionDate.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pdtfim", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pdtfim", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.FinalAcessionDate.Value.ToString("dd/MM/yyyy") });

            if (!filter.BMFActive.HasValue || !filter.BMFActive.Value)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pbmf", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pbmf", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = "D" });

            if (!filter.BovespaActive.HasValue || !filter.BovespaActive.Value)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pbovespa", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pbovespa", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = "D" });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pINITROW", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = filterOptions.InitRow });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pFINALROW", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = filterOptions.FinalRow });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pSORTCOLUMN", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = filterOptions.SortColumn });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pSORTDIR", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filterOptions.SortDirection });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pTODOS", IsNullable = true, OracleType = OracleType.Int32, Direction = ParameterDirection.Input, Value = (filterOptions.AllRecords) ? 1 : 0 });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCount", OracleType = OracleType.Number, Direction = ParameterDirection.Output });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCursor", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    count = Convert.ToInt32(cmd.Parameters["pCount"].Value);

                    result = new List<Client>();
                    while (dr.Read())
                    {
                        Client client = new Client();


                        if (!dr.IsDBNull(0)) client.AccessionDate = dr.GetDateTime(0); //DT_CRIACAO
                        if (!dr.IsDBNull(1)) client.ClientCode = dr.GetInt32(1); //CD_CLIENTE
                        if (!dr.IsDBNull(2)) client.ClientName = dr.GetString(2); //NM_CLIENTE
                        if (!dr.IsDBNull(3)) client.CPFCGC = dr.GetInt64(3); //CD_CPFCGC
                        if (!dr.IsDBNull(4)) client.AssessorName = dr.GetString(4); //CD_ASSESSOR
                        if (!dr.IsDBNull(5) && dr.GetString(5) != "N") client.BMFActive = dr.GetString(5) == "A"; //in_ATIVO_BMF
                        if (!dr.IsDBNull(6) && dr.GetString(6) != "N") client.BovespaActive = dr.GetString(6) == "A"; //in_ATIVO_BOVESPA

                        result.Add(client);
                    }
                }

                dr.Close();

            }

            return result;
        }

        public List<OverdueAndDue> LoadOverdueAndDueRegistration(OverdueAndDueFilter filter, FilterOptions filterOptions, int? userId, out int count)
        {
            List<OverdueAndDue> result = null; count = 0;

            var cmd = dbi.CreateCommand("PCK_CADASTRO.PROC_LISTA_CADASTRO_VENC", CommandType.StoredProcedure);
            cmd.Parameters.Clear();

            if (filter.ClientCode == 0)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.ClientCode });

            if (filter.Situation == 0)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pSITUACAO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pSITUACAO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.Situation });

            if (!filter.SinacorActive)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pBUSCA_ATIVO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pBUSCA_ATIVO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = "AT" });

            if (!filter.SinacorInactive)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pBUSCA_INATIVO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pBUSCA_INATIVO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = "EN" });

            if (!filter.SinacorBlocked)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pBUSCA_BLOQUEADO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pBUSCA_BLOQUEADO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = "BL" });

            if (filter.DueDateStart == DateTime.MinValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_VENC_DE", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_VENC_DE", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.DueDateStart.ToShortDateString() });

            if (filter.DueDateEnd == DateTime.MinValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_VENC_ATE", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_VENC_ATE", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.DueDateEnd.ToShortDateString() });

            if (String.IsNullOrEmpty(filter.AssessorFilterText))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTASSESSOR", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTASSESSOR", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.AssessorFilterText });

            //if (filter.ClientFilterText == null)
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTCLIENTE", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            //else
            //    cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTCLIENTE", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.ClientFilterText });

            if (userId.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_USUARIO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = userId.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_USUARIO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pINITROW", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = filterOptions.InitRow });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pFINALROW", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = filterOptions.FinalRow });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pSORTCOLUMN", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = filterOptions.SortColumn });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pSORTDIR", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filterOptions.SortDirection });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pTODOS", IsNullable = true, OracleType = OracleType.Int32, Direction = ParameterDirection.Input, Value = (filterOptions.AllRecords) ? 1 : 0 });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCount", OracleType = OracleType.Number, Direction = ParameterDirection.Output });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCursor", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    count = Convert.ToInt32(cmd.Parameters["pCount"].Value);
                    result = new List<OverdueAndDue>();
                    while (dr.Read())
                    {
                        OverdueAndDue client = new OverdueAndDue();
                        if (!dr.IsDBNull(0)) client.ClientCode = dr.GetInt32(0);        //Cd_Cliente
                        if (!dr.IsDBNull(1)) client.ClientName = dr.GetString(1);       //Nm_Cliente
                        if (!dr.IsDBNull(2)) client.CPFCGC = dr.GetInt64(2);            //CD_CPFCGC
                        if (!dr.IsDBNull(3)) client.AssessorCode = dr.GetInt32(3);      //Cd_Assessor
                        if (!dr.IsDBNull(4)) client.RegisterType = dr.GetString(4);     //tp_ficha
                        if (!dr.IsDBNull(5)) client.Situation = dr.GetInt32(5);         //in_situacao
                        if (!dr.IsDBNull(6)) client.RegisterDate = dr.GetDateTime(6);   //Dt_Fich_Cad
                        if (!dr.IsDBNull(7)) client.SinacorCode = dr.GetString(7);      //Tp_Situac
                        result.Add(client);
                    }
                }
                dr.Close();
            }
            return result;
        }

        #region Transfer Bovespa

        public List<TransferBovespaItem> LoadTransfersBovespa(TransferBovespaFilter filter, FilterOptions options, int? userId, out int count)
        {
            var items = new List<TransferBovespaItem>();

            var cmd = dbi.CreateCommand("PCK_CADASTRO.PROC_CONSULTA_REPASSE_BOVESPA", CommandType.StoredProcedure);
            cmd.Parameters.Clear();

            if (!string.IsNullOrEmpty(filter.ClientCode))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.ClientCode });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });

            if (!string.IsNullOrEmpty(filter.CNPJ))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCNPJ", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.CNPJ });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCNPJ", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });

            if (userId.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_USUARIO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = userId.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_USUARIO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pINITROW", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = options.InitRow });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pFINALROW", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = options.FinalRow });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pSORTCOLUMN", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = options.SortColumn });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pSORTDIR", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = options.SortDirection });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pTODOS", IsNullable = true, OracleType = OracleType.Int32, Direction = ParameterDirection.Input, Value = (options.AllRecords) ? 1 : 0 });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCount", OracleType = OracleType.Number, Direction = ParameterDirection.Output });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCursor", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    count = Convert.ToInt32(cmd.Parameters["pCount"].Value);
                    while (dr.Read())
                    {
                        TransferBovespaItem item = new TransferBovespaItem();

                        /*if (!(dr["id"] is DBNull))
                            item.ID = Convert.ToInt32(dr["id"]);*/
                        if (!(dr["Cd_Cliente"] is DBNull))
                            item.Client.Value = dr["Cd_Cliente"].ToString();
                        if (!(dr["Nm_Cliente"] is DBNull))
                            item.Client.Description = dr["Nm_Cliente"].ToString();
                        if (!(dr["Cd_Cpfcgc"] is DBNull))
                            item.CNPJ = dr["Cd_Cpfcgc"].ToString();
                        items.Add(item);
                    }
                }
                else
                    count = 0;
            }

            return items;
        }

        public List<TransferBMFItem> LoadTransfersBMF(TransferBMFFilter filter, FilterOptions options, out int count)
        {

            var items = new List<TransferBMFItem>();

            var cmd = dbi.CreateCommand("PCK_CADASTRO.PROC_CONSULTA_REPASSE_BMF", CommandType.StoredProcedure);
            cmd.Parameters.Clear();

            if (filter.ClientCode != 0)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.ClientCode.ToString() });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });

            if (filter.BrokerId != 0)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCORRETORA", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.BrokerId.ToString() });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCORRETORA", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });

            if (!String.IsNullOrEmpty(filter.Type))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pTIPO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.Type });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pTIPO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });

            if (!String.IsNullOrEmpty(filter.Situation))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pSITUACAO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.Situation });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pSITUACAO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });

            if (filter.UserId.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_USUARIO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = filter.UserId.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_USUARIO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pINITROW", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = options.InitRow });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pFINALROW", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = options.FinalRow });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pSORTCOLUMN", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = options.SortColumn });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pSORTDIR", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = options.SortDirection });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pTODOS", IsNullable = true, OracleType = OracleType.Int32, Direction = ParameterDirection.Input, Value = (options.AllRecords) ? 1 : 0 });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCount", OracleType = OracleType.Number, Direction = ParameterDirection.Output });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCursor", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    count = Convert.ToInt32(cmd.Parameters["pCount"].Value);
                    while (dr.Read())
                    {
                        TransferBMFItem item = new TransferBMFItem();

                        if (!dr.IsDBNull(0)) item.BondId = dr.GetInt32(0);//nr_vinc
                        if (!dr.IsDBNull(1)) item.BrokerName = dr.GetString(1);//nm_corret
                        if (!dr.IsDBNull(2)) item.ClientCode = Convert.ToInt32(dr.GetString(2));//cd_cliente
                        if (!dr.IsDBNull(3)) item.ClientName = dr.GetString(3);//nm_cliente
                        if (!dr.IsDBNull(4)) item.CPFCGC = dr.GetString(4);//cd_cpfcgc
                        if (!dr.IsDBNull(5)) item.TypeDesc = dr.GetString(5);//Tipo
                        if (!dr.IsDBNull(6)) item.BrokerId = dr.GetInt32(6);//Cd_Corret
                        if (!dr.IsDBNull(7)) item.Type = dr.GetString(7);//TP_RECENV

                        items.Add(item);
                    }
                }
                else
                    count = 0;
            }

            return items;
        }

        public bool InsertClientBond(TransferBMFItem transfer)
        {
            var cmd = dbi.CreateCommand("PCK_CADASTRO.PROC_INSERIR_VINCULO", CommandType.StoredProcedure);
            cmd.Parameters.Clear();

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_VINCULO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = transfer.BondId });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_CORRETORA", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = transfer.BrokerId });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_CLIENTE", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = transfer.ClientCode });

            return (cmd.ExecuteNonQuery() == 1);
        }

        /*public bool CheckClientBMF(int clientCode)
        {
            var cmd = dbi.CreateCommand("PCK_CADASTRO.FUNC_EXISTE_CLIENTE_BMF", CommandType.StoredProcedure);
            cmd.Parameters.Clear();

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = clientCode });
            //cmd.Parameters.Add(new OracleParameter { ParameterName = "pResult", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.ReturnValue, Size = 10 });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCursor", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            cmd.ExecuteNonQuery();

            return (Convert.ToInt32(cmd.Parameters["pResult"].Value) == 1);
        }*/

        public Client CheckClientBMF(int clientCode)
        {
            var cmd = dbi.CreateCommand("PCK_CADASTRO.PROC_EXISTE_CLIENTE_BMF", CommandType.StoredProcedure);
            cmd.Parameters.Clear();
            Client client = null;

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = clientCode });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCursor", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    if (dr.Read())
                    {
                        client = new Client();

                        client.ClientCode = dr.GetInt32(0);
                        client.ClientName = dr.GetString(1);
                        client.CPFCGC = dr.GetInt64(2);
                    }
                }
            }

            return client;
        }

        #endregion

        #region Cadastro

        public bool CheckClientPerCPFCGC(string cpfcgc, out int clientId)
        {
            var cmd = dbi.CreateCommand("PCK_CADASTRO.PROC_EXISTE_CLIENTE_CADASTRO", CommandType.StoredProcedure);
            cmd.Parameters.Clear();

            cmd.Parameters.Add(new OracleParameter { ParameterName = "cpfcgc", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = cpfcgc });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pClientId", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Output });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pReturn", OracleType = OracleType.VarChar, Direction = ParameterDirection.ReturnValue, Size = 1 });

            cmd.ExecuteNonQuery();

            if (Convert.ToInt32(cmd.Parameters["pReturn"].Value) == 1)
                clientId = Convert.ToInt32(cmd.Parameters["pClientId"].Value);
            else
                clientId = 0;

            return Convert.ToInt32(cmd.Parameters["pReturn"].Value) == 1;

        }

        public string CheckClientType(string clientCodes, string assessors, int? userId)
        {
            var cmd = dbi.CreateCommand("PCK_CADASTRO.PROC_DESCOBRE_TIPO_PESSOA", CommandType.StoredProcedure);
            cmd.Parameters.Clear();

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTCLIENTE", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = clientCodes });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTASSESSOR", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = assessors });
            if (userId.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_USUARIO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = userId });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_USUARIO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pReturn", OracleType = OracleType.VarChar, Direction = ParameterDirection.ReturnValue, Size = 1 });

            cmd.ExecuteNonQuery();

            return cmd.Parameters["pReturn"].Value != null ? cmd.Parameters["pReturn"].Value.ToString() : "N";

        }

        public IndividualPerson LoadIndividualPerson(int clientCode)
        {
            var cmd = dbi.CreateCommand("PCK_CADASTRO.PROC_CONSULTA_CADASTRO_PF", CommandType.StoredProcedure);
            cmd.Parameters.Clear();
            IndividualPerson model = null;

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = clientCode });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSORINFO", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSORENDERECO", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSOREMAIL", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSORCONTA", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    model = new IndividualPerson();
                    model.IsRenew = true;

                    if (dr.Read())
                    {
                        int counter = -1;

                        if (!dr.IsDBNull(++counter)) model.CPF = String.Format(@"{0:000\.000\.000\-00}", dr.GetInt64(counter));
                        if (!dr.IsDBNull(++counter)) model.Name = dr.GetString(counter);
                        if (!dr.IsDBNull(++counter)) model.BirthDate = dr.GetDateTime(counter);

                        if (!dr.IsDBNull(++counter))
                        {
                            var nacion = dr.GetInt32(counter);

                            switch (nacion)
                            {
                                case 1:
                                    model.Nationality = "BRASILEIRO NATO";
                                    break;
                                case 2:
                                    model.Nationality = "BRASILEIRO NATURALIZADO";
                                    break;
                                case 3:
                                    model.Nationality = "ESTRANGEIRO";
                                    break;
                                default:
                                    break;
                            }
                        }
                        if (!dr.IsDBNull(++counter)) model.Naturality = dr.GetString(counter);
                        if (!dr.IsDBNull(++counter)) model.Gender = dr.GetString(counter) == "F" ? Gender.Female : Gender.Male;
                        if (!dr.IsDBNull(++counter)) model.FatherName = dr.GetString(counter);
                        if (!dr.IsDBNull(++counter)) model.MotherName = dr.GetString(counter);
                        model.Document = new DocumentInfo();
                        if (!dr.IsDBNull(++counter))
                        {
                            var docType = dr.GetString(counter);
                            switch (docType)
                            {
                                case "RG":
                                    model.Document.DocumentType = DocumentType.RG;
                                    break;
                                case "RN":
                                    model.Document.DocumentType = DocumentType.RNE;
                                    break;
                                case "PP":
                                    model.Document.DocumentType = DocumentType.Passport;
                                    break;
                                case "CN":
                                    model.Document.DocumentType = DocumentType.CNH;
                                    break;
                                default:
                                    model.Document.DocumentType = DocumentType.Professional;
                                    break;
                            }
                        }
                        if (!dr.IsDBNull(++counter)) model.Document.DocumentNumber = dr.GetString(counter);
                        if (!dr.IsDBNull(++counter)) model.Document.EmissorDate = dr.GetDateTime(counter);
                        if (!dr.IsDBNull(++counter)) model.Document.Emissor = dr.GetString(counter);
                        if (!dr.IsDBNull(++counter)) model.Document.DocumentState = (State)System.Enum.Parse(typeof(State), dr.GetString(counter));

                        model.CivilState = new CivilState();

                        if (!dr.IsDBNull(++counter))
                        {
                            var civState = dr.GetInt32(counter);

                            switch (civState)
                            {
                                case 1:
                                    model.CivilState.CivilStateType = CivilStateType.Single;
                                    break;
                                case 2:
                                    model.CivilState.CivilStateType = CivilStateType.Separated;
                                    break;
                                case 3:
                                    model.CivilState.CivilStateType = CivilStateType.Widower;
                                    break;
                                case 4:
                                    model.CivilState.CivilStateType = CivilStateType.Divorced;
                                    break;
                                case 8:
                                    model.CivilState.CivilStateType = CivilStateType.StableUnion;
                                    break;
                                case 5:
                                case 6:
                                case 7:
                                    model.CivilState.CivilStateType = CivilStateType.Married;
                                    break;
                                default:
                                    break;
                            }
                        }

                        if (!dr.IsDBNull(++counter))
                        {
                            var regimeType = dr.GetInt32(counter);

                            switch (regimeType)
                            {
                                case 1:
                                    model.CivilState.PropertyRegime = PropertyRegime.UniversalCommunion;
                                    break;
                                case 2:
                                    model.CivilState.PropertyRegime = PropertyRegime.PartialCommunion;
                                    break;
                                case 3:
                                    model.CivilState.PropertyRegime = PropertyRegime.FinalParticipation;
                                    break;
                                case 4:
                                    model.CivilState.PropertyRegime = PropertyRegime.TotalSeparation;
                                    break;
                                default:
                                    break;
                            }
                        }

                        if (!dr.IsDBNull(++counter)) model.PPE = dr.GetString(counter) == "S";
                        if (!dr.IsDBNull(++counter)) model.QualifiedInvestor = dr.GetString(counter) == "S";
                    }

                    if (dr.NextResult())
                    {
                        model.Addresses = new List<PersonAddress>();
                        var counter = 0;

                        while (dr.Read())
                        {
                            counter = 0;
                            var address = new PersonAddress();
                            if (model.Phones == null) model.Phones = new List<Phone>();

                            if (!dr.IsDBNull(++counter)) address.AddressLine1 = dr.GetString(counter);
                            if (!dr.IsDBNull(++counter)) address.AddressLine1 += ", " + dr.GetString(counter);
                            ++counter;
                            if (!dr.IsDBNull(++counter)) address.Quarter = dr.GetString(counter);
                            if (!dr.IsDBNull(++counter)) address.City = dr.GetString(counter);
                            if (!dr.IsDBNull(++counter)) address.PostalCode = dr.GetString(counter);
                            if (!dr.IsDBNull(++counter)) address.PostalCode += "-" + dr.GetString(counter);
                            if (!dr.IsDBNull(++counter)) address.State = (State)System.Enum.Parse(typeof(State), dr.GetString(counter));
                            if (!dr.IsDBNull(++counter)) address.StateCountry += dr.GetString(counter);
                            var addressType = "";
                            if (!dr.IsDBNull(++counter))
                            {
                                addressType = dr.GetString(counter);
                                switch (addressType)
                                {
                                    case "R":
                                        address.Type = AddressType.Residential;
                                        break;

                                    case "C":
                                        address.Type = AddressType.Comercial;
                                        break;
                                    case "O":
                                        address.Type = AddressType.Other;
                                        break;
                                    default: break;
                                }
                            }
                            model.Addresses.Add(address);

                            if (!dr.IsDBNull(++counter))
                            {
                                var phone = new Phone() { DDDCode = dr.GetInt32(counter) };
                                if (!dr.IsDBNull(++counter)) phone.PhoneNumber = String.Format(@"{0:0000\-0000}", dr.GetInt64(counter));
                                if (addressType == "R") phone.ContactType = ContactType.ResidentialPhone;
                                else if (addressType == "C") phone.ContactType = ContactType.ComercialPhone;

                                model.Phones.Add(phone);
                            }

                            if (!dr.IsDBNull(++counter))
                            {
                                var phone = new Phone() { DDDCode = dr.GetInt32(counter) };
                                if (!dr.IsDBNull(++counter)) phone.PhoneNumber = String.Format(@"{0:0000\-0000}", dr.GetInt64(counter));
                                phone.ContactType = ContactType.CellularPhone;

                                model.Phones.Add(phone);
                            }
                        }
                    }

                    if (dr.NextResult())
                    {
                        //TODO: Pegarr emails
                    }

                    if (dr.NextResult())
                    {
                        while (dr.Read())
                        {
                            var counter = 0;

                            if (model.DepositAccounts == null) model.DepositAccounts = new List<AuthorizedAccount>();

                            AuthorizedAccount acc = new AuthorizedAccount();
                            acc.Account = new BankAccount();

                            if (!dr.IsDBNull(++counter)) acc.Account.BankCode = dr.GetString(counter);
                            if (!dr.IsDBNull(++counter)) acc.Account.AgencyCode = dr.GetString(counter);
                            if (!dr.IsDBNull(++counter)) acc.Account.AgencyDigit = dr.GetString(counter);
                            if (!dr.IsDBNull(++counter)) acc.Account.Account = dr.GetString(counter);
                            if (!dr.IsDBNull(++counter)) acc.Account.AccountDigit = dr.GetString(counter);

                            model.DepositAccounts.Add(acc);
                        }
                    }
                }
            }

            return model;
        }

        #endregion

        #region Bancos

        public List<BankAccount> LoadBanks(string bankName)
        {
            List<BankAccount> result = null;

            var cmd = dbi.CreateCommand("PCK_CADASTRO.PROC_CONSULTA_BANCO", CommandType.StoredProcedure);
            cmd.Parameters.Clear();

            if (String.IsNullOrEmpty(bankName))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pcd_banco", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pcd_banco", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = bankName });


            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCursor", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    result = new List<BankAccount>();
                    while (dr.Read())
                    {
                        BankAccount acc = new BankAccount();


                        if (!dr.IsDBNull(0)) acc.BankCode = dr.GetString(0);
                        if (!dr.IsDBNull(1)) acc.BankName = dr.GetString(1);

                        result.Add(acc);
                    }
                }

                dr.Close();

            }

            return result;
        }

        #endregion

        #region Cliente Online

        public ClientOnline LoadClientOnline(string idClient)
        {
            var client = new ClientOnline();
            DBInterface dbiCRK = new DBInterface("PORTAL");

            using (var cmd = (SqlCommand)dbiCRK.CreateCommand("PR_CLIENTE_ONLINE_OBTER_USUARIO_POR_LOGIN", CommandType.StoredProcedure))
            {
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@idCliente", Value = idClient, SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input });
                cmd.Connection.Open();

                using (var dr = cmd.ExecuteReader())
                {
                    if (dr.Read())
                    {
                        client.Id = Convert.ToInt32(dr["IDCLIENTE"]);
                        client.IdStatusRegistration = Convert.ToInt32(dr["STATUSREGISTRO"]);
                        client.IdFichaCadastral = dr["ID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ID"]);
                        client.Name = Convert.ToString(dr["NOME"]);
                        client.Cpf = Convert.ToString(dr["CD_CPFCNPJ"]);
                        client.EconomicGroup = Convert.ToString(dr["GRUPO_ECONOMICO"]);
                        client.Classification = Convert.ToString(dr["CLASSIFICACAO"]);
                        client.ActivityGroup = Convert.ToString(dr["GRUPO_ATIVIDADE"]);
                        client.RegistrationType = Convert.ToString(dr["TIPO_CADASTRO"]);
                        client.ClientType = Convert.ToString(dr["TIPO_CLIENTE"]);
                        client.Category = Convert.ToString(dr["CATEGORIA"]);
                        client.AtividadePrincipal = Convert.ToString(dr["ATIVIDADE_PRINCIPAL"]);
                        client.NaturezaOperacao = Convert.ToString(dr["NATUREZA_OPERACAO"]);
                        client.Size = Convert.ToString(dr["PORTE"]);
                        client.Profession = Convert.ToString(dr["PROFISSAO"]);
                        client.Birthdate = dr["DATA_NASC"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["DATA_NASC"]);
                        client.Sex = Convert.ToString(dr["GENERO"]);
                        client.MaritalStatus = Convert.ToString(dr["ESTADO_CIVIL"]);
                        client.MatrimonialRegime = Convert.ToString(dr["REGIME_CASAMENTO"]);
                        client.CommonLaw = Convert.ToString(dr["UNIAO_ESTAVEL"]);
                        client.Estrangeiro = Convert.ToString(dr["ESTRANGEIRO"]);
                        client.NomePai = Convert.ToString(dr["NOME_PAI"]);
                        client.NomeMae = Convert.ToString(dr["NOME_MAE"]);
                        client.EmailPrincipal = Convert.ToString(dr["EMAIL"]);
                        client.Nacionality = Convert.ToString(dr["NACIONALIDADE"]);
                        client.Situation = Convert.ToString(dr["STATUS"]);
                        client.DataInclusao = dr["DATA_CRIACAO"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["DATA_CRIACAO"]);
                        client.DataAlteracao = dr["DATA_ALTERACAO"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["DATA_ALTERACAO"]);
                        client.Observacao = Convert.ToString(dr["OBS"]);
                        client.PaisNascimento = Convert.ToString(dr["PAIS_NASCIMENTO"]);
                        client.EstadoNascimento = Convert.ToString(dr["ESTADO_NASCIMENTO"]);
                        client.Naturalness = Convert.ToString(dr["NATURALIDADE"]);
                        client.Nacionality = Convert.ToString(dr["NACIONALIDADE"]);
                        client.NomeConjugue = Convert.ToString(dr["NOME_CONJUGUE"]);
                        client.CpfConjugue = dr["CPF_CONJUGUE"] == DBNull.Value ? (int?)null : Convert.ToInt32(dr["CPF_CONJUGUE"]);
                        client.DataNascimentoConjugue = dr["NASC_CONJUGUE"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["NASC_CONJUGUE"]);
                        client.DocumentType = Convert.ToString(dr["TIPO_DOC"]);
                        client.NumberDocument = Convert.ToString(dr["NUMERO_DOC"]);
                        client.IssuingAgency = Convert.ToString(dr["ORGAO_EMISSOR_DOC"]);
                        client.ValidateDocument = dr["DATA_VENCIMENTO_DOC"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["DATA_VENCIMENTO_DOC"]);
                        client.IssuingDate = dr["DATA_EMISSAO_DOC"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["DATA_EMISSAO_DOC"]);
                        client.EstadoEmissao = Convert.ToString(dr["ESTADO_EMISSAO_DOC"]);
                        client.NomeEmpresa = Convert.ToString(dr["NOME_EMPRESA"]);
                        client.DataAdmissao = dr["DATA_ADMISSAO"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["DATA_ADMISSAO"]);
                        client.Endereco = Convert.ToString(dr["ENDERECO_EMPRESA"]);
                        client.Manager = Convert.ToString(dr["GERENTE_REL"]);
                        client.DataInicio = dr["DATA_INICIO_REL"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["DATA_INICIO_REL"]);
                        client.DataFinal = dr["DATA_FIM_REL"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["DATA_FIM_REL"]);
                        client.WorkGovernment = Convert.ToString(dr["CARGO_PUBL"]);
                        client.JobGovernmentName = Convert.ToString(dr["NOME_CARGO_PUBL"]);
                        client.Agency = Convert.ToString(dr["NOME_EMPRESA_PUBL"]);
                        client.StartDateJobGovernment = dr["DATA_INICIO_PUBL"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["DATA_INICIO_PUBL"]);
                        client.FinalDateJobGovernment = dr["DATA_FIM_PUBL"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["DATA_FIM_PUBL"]);
                        client.RelativeWorkGovernment = Convert.ToString(dr["PARENTE_EMP_PUBL"]);
                        client.RelativeName = Convert.ToString(dr["NOME_PARENTE"]);
                        client.RelativeRelationship = Convert.ToString(dr["TP_PARENTE"]);
                        client.JobRelativeName = Convert.ToString(dr["CARGO_PARENTE_PUBL"]);
                        client.PossuiRepresentante = Convert.ToString(dr["TEM_REPRESENTANTE"]);
                        client.RelationshipBrPartners = Convert.ToString(dr["ADMINISTRADOR"]);
                    }

                    dr.Close();
                }
            }

            GetEmailClientOnline(client);
            GetAddressClientOnline(client);
            GetContactClientOnline(client);
            GetAssignClientOnline(client);
            GetFatca(client);

            return client;
        }

        public ClientOnlineFinancial LoadClientFinancial(int idClientOnline)
        {
            var clientFinancial = new ClientOnlineFinancial();
            DBInterface dbiCRK = new DBInterface("PORTAL");

            using (var cmd = (SqlCommand)dbiCRK.CreateCommand("PR_CLIENTE_ONLINE_FINANCEIRO_OBTER", CommandType.StoredProcedure))
            {
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@idClientOnline", Value = idClientOnline, SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input });
                cmd.Connection.Open();

                using (var dr = cmd.ExecuteReader())
                {
                    if (dr.Read())
                    {
                        clientFinancial.Id = Convert.ToInt32(dr["ID"]);
                        clientFinancial.IdClientOnline = Convert.ToInt32(dr["ID_CLIENTE_ONLINE"]);
                        clientFinancial.MonthlyProfit = Convert.ToString(dr["RENDA_MENSAL"]);
                        clientFinancial.FamilyMonthlyProfit = Convert.ToString(dr["RENDA_MENSAL_FAMILIAR"]);
                        clientFinancial.AnotherProfit = Convert.ToString(dr["OUTRAS_RENDAS"]);
                        clientFinancial.HousePatrimony = Convert.ToString(dr["IMOVEL_PATRIMONIO"]);
                        clientFinancial.CarPatrimony = Convert.ToString(dr["VEICULO_PATRIMONIO"]);
                        clientFinancial.ApplicationPatrimony = Convert.ToString(dr["APLICACAO_PATRIMONIO"]);
                        clientFinancial.ApplicationOrigin = Convert.ToBoolean(dr["APLICACAO_ORIGEM"]);
                        clientFinancial.Indemnity = Convert.ToBoolean(dr["INDENIZACAO_ORIGEM"]);
                        clientFinancial.Award = Convert.ToBoolean(dr["PREMIO_ORIGEM"]);
                        clientFinancial.Termination = Convert.ToBoolean(dr["RESCISAO_ORIGEM"]);
                        clientFinancial.Salary = Convert.ToBoolean(dr["SALARIO_ORIGEM"]);
                        clientFinancial.Sell = Convert.ToBoolean(dr["VENDA_ORIGEM"]);
                        clientFinancial.Jewels = Convert.ToBoolean(dr["JOIAS_ORIGEM"]);
                        clientFinancial.Rental = Convert.ToBoolean(dr["ALUGUEL_ORIGEM"]);
                        clientFinancial.Donation = Convert.ToBoolean(dr["DOACAO_ORIGEM"]);
                        clientFinancial.Heritage = Convert.ToBoolean(dr["HERANCA_ORIGEM"]);
                        clientFinancial.Car = Convert.ToBoolean(dr["AUTOMOVEL_ORIGEM"]);
                        clientFinancial.Insurance = Convert.ToBoolean(dr["INDENIZACAO_SEG_ORIGEM"]);
                        clientFinancial.ProductAgricultural = Convert.ToBoolean(dr["PRODUCAO_AGRI_ORIGEM"]);
                        clientFinancial.House = Convert.ToBoolean(dr["IMOVEL_ORIGEM"]);
                        clientFinancial.Another = Convert.ToBoolean(dr["OUTROS_ORIGEM"]);
                        clientFinancial.AnotherDescription = Convert.ToString(dr["OUTROS_DESCRICAO"]);
                        clientFinancial.IdFiscalNature = Convert.ToInt32(dr["ID_NATUREZA_FISCAL"]);
                    }

                    dr.Close();
                }
            }

            clientFinancial.Accounts.AddRange(GetClientBankAccount(idClientOnline));
            clientFinancial.Products.AddRange(GetClientProductFinancial(idClientOnline));

            return clientFinancial;
        }

        public ClientOnlineRepresentative LoadClientRepresentative(int idClientOnline, int id)
        {
            var clientRepresentative = new ClientOnlineRepresentative();
            clientRepresentative.IdClientOnline = idClientOnline;
            DBInterface dbiCRK = new DBInterface("PORTAL");

            using (var cmd = (SqlCommand)dbiCRK.CreateCommand("PR_CLIENTE_ONLINE_OBTER_REPRESENTANTE", CommandType.StoredProcedure))
            {
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@IDCLIENTEONLINE", Value = idClientOnline, SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@ID", Value = id, SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input });
                cmd.Connection.Open();

                using (var dr = cmd.ExecuteReader())
                {
                    if (dr.Read())
                    {
                        clientRepresentative.IdRepresentation = Convert.ToInt32(dr["ID"]);
                        clientRepresentative.Name = Convert.ToString(dr["NOME"]);
                        clientRepresentative.IdRepresentationForm = Convert.ToInt32(dr["ID_FORMA_REPRESENTACAO"]);
                        clientRepresentative.DocumentType = Convert.ToString(dr["ID_TIPO_DOCUMENTO"]);
                        clientRepresentative.NumberDocument = Convert.ToString(dr["NUMERO_DOCUMENTO"]);
                        clientRepresentative.IssuingAgency = Convert.ToString(dr["ORGAO_EMISSOR"]);
                        clientRepresentative.IssuingDate = dr["DATA_EMISSAO_DOCUMENTO"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["DATA_EMISSAO_DOCUMENTO"]);
                        clientRepresentative.ValidateDocument = dr["DATA_VALIDADE_DOCUMENTO"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["DATA_VALIDADE_DOCUMENTO"]);
                        clientRepresentative.Birthdate = dr["DATA_NASCIMENTO"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["DATA_NASCIMENTO"]);
                        clientRepresentative.Birthplace = Convert.ToString(dr["LOCAL_NASCIMENTO"]);
                        clientRepresentative.Nacionality = Convert.ToString(dr["NACIONALIDADE"]);
                        clientRepresentative.MaritalStatus = Convert.ToString(dr["ID_ESTADO_CIVIL"]);
                        clientRepresentative.Profession = Convert.ToString(dr["ID_PROFISSAO"]);
                        clientRepresentative.CssQualification = dr["QUALIFICACAOCSS"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["QUALIFICACAOCSS"]);
                        clientRepresentative.CssExpired = dr["DATAVALIDADECSS"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["DATAVALIDADECSS"]);
                        clientRepresentative.WorkGovernment = Convert.ToBoolean(dr["FUNCAO_GOVERNO"]) == true ? "1" : "0";
                        clientRepresentative.IdProfile = Convert.ToInt32(dr["ID_PERFIL"]);
                        clientRepresentative.JobGovernmentName = Convert.ToString(dr["NOME_CARGO_GOVERNO"]);
                        clientRepresentative.Agency = Convert.ToString(dr["NOME_EMPRESA_GOVERNO"]);
                        clientRepresentative.StartDateJobGovernment = dr["INICIO_FUNCAO_GOVERNO"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["INICIO_FUNCAO_GOVERNO"]);
                        clientRepresentative.FinalDateJobGovernment = dr["FIM_FUNCAO_GOVERNO"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["FIM_FUNCAO_GOVERNO"]);
                        clientRepresentative.RelativeWorkGovernment = Convert.ToBoolean(dr["PARENTE_FUNCAO_GOVERNO"]) == true ? "1" : "0";
                        clientRepresentative.RelativeName = Convert.ToString(dr["NOME_PARENTE"]);
                        clientRepresentative.JobRelativeName = Convert.ToString(dr["NOME_FUNCAO_PARENTE_GOVERNO"]);
                        clientRepresentative.RelativeRelationship = Convert.ToString(dr["RELACIONAMENTO"]);
                        clientRepresentative.JobNameAgencyRelative = Convert.ToString(dr["NOME_EMPRESA_PARENTE_GOVERNO"]);
                        clientRepresentative.TaxResidence = Convert.ToBoolean(dr["RESIDENCIA_FISCAL"]) == true ? 1 : 0;
                        clientRepresentative.RelationshipBrPartners = Convert.ToBoolean(dr["VINCULO_BRPARTNERS"]) == true ? "1" : "0";
                        clientRepresentative.TaxCountry = Convert.ToString(dr["NOME_PAIS_RESIDENCIA_FISCAL"]);
                        clientRepresentative.TaxNameResidence = Convert.ToString(dr["NOME_PAIS_RESIDENCIA"]);
                    }

                    dr.Close();
                }
            }

            GetAddressRepresentative(clientRepresentative);
            GetContactRepresentative(clientRepresentative);

            return clientRepresentative;
        }

        public Representatives LoadRepresentatives(int idClientOnline)
        {
            var clientRepresentative = new Representatives();
            DBInterface dbiCRK = new DBInterface("PORTAL");

            using (var cmd = (SqlCommand)dbiCRK.CreateCommand("PR_CLIENTE_ONLINE_LISTAR_REPRESENTANTE", CommandType.StoredProcedure))
            {
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@IDCLIENTEONLINE", Value = idClientOnline, SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input });
                cmd.Connection.Open();

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        var representative = new ClientOnlineRepresentative();
                        representative.Name = Convert.ToString(dr["NOME"]);
                        representative.IdRepresentationForm = Convert.ToInt32(dr["ID_FORMA_REPRESENTACAO"]);
                        representative.IdRepresentation = Convert.ToInt32(dr["ID"]);
                        representative.NumberDocument = Convert.ToString(dr["NUMERO_DOCUMENTO"]);
                        representative.Birthdate = dr["DATA_NASCIMENTO"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["DATA_NASCIMENTO"]);
                        representative.DescriptionRepresentationForm = Convert.ToString(dr["DESCRICAO"]);

                        clientRepresentative.ClientRepresentatives.Add(representative);
                    }
                }
            }

            return clientRepresentative;
        }

        public ClientOnlineDocuments LoadClientDocument(int idClientOnline, string nomeColuna)
        {
            var documents = new ClientOnlineDocuments();
            DBInterface dbiCRK = new DBInterface("PORTAL");

            using (var cmd = (SqlCommand)dbiCRK.CreateCommand("PR_CLIENTE_ONLINE_LISTAR_DOCUMENTOS", CommandType.StoredProcedure))
            {
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@idClienteOnline", Value = idClientOnline, SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@nomecoluna", Value = nomeColuna, SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@valorColuna", Value = 1, SqlDbType = SqlDbType.Bit, Direction = ParameterDirection.Input });
                cmd.Connection.Open();

                using (var dr = cmd.ExecuteReader())
                {
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            var doc = new ClientDocument();
                            doc.Id = Convert.ToInt32(dr["ID"]);
                            doc.IdDocClient = dr["IDDOCCLIENTE"] == DBNull.Value ? 0 : Convert.ToInt32(dr["IDDOCCLIENTE"]);
                            doc.Description = Convert.ToString(dr["DESCRICAODOC"]);
                            doc.HasPysical = dr["TEM_FISICO"] == DBNull.Value ? false : Convert.ToBoolean(dr["TEM_FISICO"]);
                            doc.DocumentExpiration = Convert.ToDateTime(dr["DATA_VALIDADE"]);
                            documents.Documents.Add(doc);
                        }
                    }

                    dr.Close();
                }
            }

            return documents;
        }

        public ClientOnlineRules LoadDocumentsRepresentatives(int idDocument)
        {
            var documents = new ClientOnlineRules();
            DBInterface dbiCRK = new DBInterface("PORTAL");

            using (var cmd = (SqlCommand)dbiCRK.CreateCommand("PR_CLIENTE_ONLINE_LISTAR_PODERES", CommandType.StoredProcedure))
            {
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@idDocumento", Value = idDocument, SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input });
                cmd.Connection.Open();

                using (var dr = cmd.ExecuteReader())
                {
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            var doc = new DocumentRepresentative();
                            documents.IdClientOnline = Convert.ToInt32(dr["ID_CLIENTE_ONLINE"]);
                            documents.IdDocument = Convert.ToInt32(dr["ID_DOCUMENTO"]);
                            documents.IdDocumentType = Convert.ToInt32(dr["ID_TIPO"]);
                            documents.StartValidateDocument = Convert.ToDateTime(dr["DATA_VALIDADE_INICIO"]);
                            documents.FinalValidateDocument = Convert.ToDateTime(dr["DATA_VALIDADE_FIM"]);
                            doc.Id = Convert.ToInt32(dr["ID"]);
                            doc.IdRepresentative = Convert.ToInt32(dr["ID_REPRESENTANTE"]);
                            doc.NumberDocument = Convert.ToString(dr["NUMERO_DOCUMENTO"]);
                            doc.IdQualification = Convert.ToInt32(dr["ID_QUALIFICACAO"]);
                            doc.DescriptionQualification = Convert.ToString(dr["DESCRICAO_QUALIFICACAO"]);
                            doc.NameRepresentative = Convert.ToString(dr["NOME_REPRESENTANTE"]);
                            documents.Representatives.Add(doc);
                        }
                    }

                    dr.Close();
                }
            }

            return documents;
        }

        public ClientsOnlineStatus LoadClientByStatus(int idStatus)
        {
            var clients = new ClientsOnlineStatus();
            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PR_CLIENTE_ONLINE_LISTAR_POR_STATUS", CommandType.StoredProcedure);
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@idStatus", Value = idStatus, SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input });
            cmd.Connection.Open();

            using (var dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    var client = new ClientOnlineStatus();
                    client.IdClient = Convert.ToInt32(dr["ID_CLIENTE"]);
                    client.ClientName = Convert.ToString(dr["NOME"]);
                    client.CpfCnpj = Convert.ToString(dr["CD_CPFCNPJ"]);
                    client.Telephone = Convert.ToString(dr["TELEFONE"]);
                    client.Email = Convert.ToString(dr["EMAIL"]);
                    client.Gender = Convert.ToString(dr["SEXO"]);
                    client.Birthdate = dr["DATA_NASC"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["DATA_NASC"]);
                    clients.Clients.Add(client);
                }

                dr.Close();
            }

            cmd.Connection.Close();

            return clients;
        }

        public Documents LoadDocument()
        {
            var docLst = new Documents();
            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PR_DOCUMENTO_LISTAR", CommandType.StoredProcedure);
            cmd.Connection.Open();

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        var document = new Document
                        {
                            Id = Convert.ToInt32(dr["ID"]),
                            NameFile = Convert.ToString(dr["NOME_ARQUIVO"]),
                            Instruction = Convert.ToString(dr["INSTRUCAO_USO"]),
                            Required = Convert.ToBoolean(dr["OBRIGATORIO"]),
                            ExpirationDate = Convert.ToDateTime(dr["DATA_VALIDADE"]),
                            PhysicalPerson = Convert.ToBoolean(dr["PESSOA_FISICA"]),
                            LegalPerson = Convert.ToBoolean(dr["PESSOA_JURIDICA"]),
                            Funds = Convert.ToBoolean(dr["FUNDOS"])
                        };
                        docLst.Docs.Add(document);
                    }
                }

                dr.Close();
            }

            cmd.Connection.Close();

            return docLst;
        }

        public Document LoadDocumentById(int idDocument)
        {
            var document = new Document();
            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PR_DOCUMENTO_OBTER", CommandType.StoredProcedure);
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@idDocumento", Value = idDocument, SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input });
            cmd.Connection.Open();

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.Read())
                {
                    document.Id = Convert.ToInt32(dr["ID"]);
                    document.NameFile = Convert.ToString(dr["NOME_ARQUIVO"]);
                    document.Instruction = Convert.ToString(dr["INSTRUCAO_USO"]);
                    document.Required = Convert.ToBoolean(dr["OBRIGATORIO"]);
                    document.IdValidation = Convert.ToInt32(dr["ID_DOCUMENTO_VALIDADE"]);
                    document.PhysicalPerson = Convert.ToBoolean(dr["PESSOA_FISICA"]);
                    document.LegalPerson = Convert.ToBoolean(dr["PESSOA_JURIDICA"]);
                    document.Funds = Convert.ToBoolean(dr["FUNDOS"]);

                    document.DescriptionFile = Convert.ToString(dr["DESCRICAO_ARQUIVO"]);
                    document.ContentType = Convert.ToString(dr["EXTENSAO"]);
                    document.Data = (byte[])dr["ARQUIVO"];
                }

                dr.Close();
            }

            cmd.Connection.Close();

            return document;
        }

        public int UpsertDocument(Document document)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");
            SqlCommand cmd;

            if (document.Id == 0)
                cmd = (SqlCommand)dbiCRK.CreateCommand("PR_DOCUMENTO_INSERIR", CommandType.StoredProcedure);
            else
            {
                RemoveProductDocument(document.Id);
                cmd = (SqlCommand)dbiCRK.CreateCommand("PR_DOCUMENTO_ATUALIZAR", CommandType.StoredProcedure);
            }

            cmd.Connection.Open();
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@idDoc", SqlDbType = SqlDbType.Int, Value = document.Id, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@descricaoArquivo", SqlDbType = SqlDbType.VarChar, Value = document.DescriptionFile, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@nomeArquivo", SqlDbType = SqlDbType.VarChar, Value = document.NameFile, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@instrucao", SqlDbType = SqlDbType.VarChar, Value = document.Instruction, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@obrigatorio", SqlDbType = SqlDbType.Bit, Value = document.Required, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@idDocValidade", SqlDbType = SqlDbType.Int, Value = document.IdValidation, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@dataValidade", SqlDbType = SqlDbType.Date, Value = document.ExpirationDate, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pessoaFisica", SqlDbType = SqlDbType.Bit, Value = document.PhysicalPerson, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pessoaJuridica", SqlDbType = SqlDbType.Bit, Value = document.LegalPerson, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@fundos", SqlDbType = SqlDbType.Bit, Value = document.Funds, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@extensao", SqlDbType = SqlDbType.VarChar, Value = document.ContentType, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@arquivo", SqlDbType = SqlDbType.VarBinary, Value = document.Data, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@status", SqlDbType = SqlDbType.Int, Value = document.Status, Direction = ParameterDirection.Input });

            var returnValue = cmd.Parameters.Add("@id", SqlDbType.Int);
            returnValue.Direction = ParameterDirection.ReturnValue;

            cmd.ExecuteNonQuery();
            cmd.Connection.Close();

            InsertProductDocument(document, Convert.ToInt32(returnValue.Value));

            return 1;
        }

        private void InsertProductDocument(Document document, int idDocument)
        {
            foreach (var item in document.Products)
            {
                DBInterface dbiCRK = new DBInterface("PORTAL");
                var cmd = (SqlCommand)dbiCRK.CreateCommand("PR_DOCUMENTO_INSERIR_PRODUTO", CommandType.StoredProcedure);
                cmd.Connection.Open();

                cmd.Parameters.Add(new SqlParameter { ParameterName = "@idDocumento", SqlDbType = SqlDbType.Int, Value = idDocument, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@idProduto", SqlDbType = SqlDbType.Int, Value = item.IdProduct, Direction = ParameterDirection.Input });
                cmd.ExecuteNonQuery();

                cmd.Connection.Close();
            }
        }

        private void RemoveProductDocument(int idDocument)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PR_DOCUMENTO_EXCLUIR", CommandType.StoredProcedure);

            cmd.Connection.Open();
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@idDocumento", SqlDbType = SqlDbType.VarChar, Value = idDocument, Direction = ParameterDirection.Input });

            cmd.ExecuteNonQuery();
            cmd.Connection.Close();
        }

        public int UpsertRegistrationClient(ClientOnline client)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = new SqlCommand();

            if (client.IdFichaCadastral == 0)
                cmd = (SqlCommand)dbiCRK.CreateCommand("PR_CLIENTE_ONLINE_INSERIR_FICHA_CADASTRAL", CommandType.StoredProcedure);
            else
                cmd = (SqlCommand)dbiCRK.CreateCommand("PR_CLIENTE_ONLINE_ATUALIZAR_FICHA_CADASTRAL", CommandType.StoredProcedure);

            cmd.Connection.Open();
            AddParametersClientOnline(cmd, client);
            cmd.ExecuteNonQuery();
            cmd.Connection.Close();

            InsertEmail(client, "TB_CLIENTE_ONLINE_EMAIL", "ID_CLIENTE_ONLINE");
            InsertAddress(client.Address, client.Id, "INSERE_ENDERECO", "TB_CLIENTE_ONLINE_ENDERECO", "ID_CLIENTE_ONLINE");
            InsertContact(client.Contact, client.Id, "INSERE_CONTATO", "TB_CLIENTE_ONLINE_CONTATO", "ID_CLIENTE_ONLINE");
            InsertAssign(client, "TB_CLIENTE_ONLINE_ASSINATURA", "ID_CLIENTE_ONLINE");

            return 1;
        }

        public int UpsertRepresentative(ClientOnlineRepresentative representative)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");

            using (var cmd = (SqlCommand)dbiCRK.CreateCommand("PR_CLIENTE_ONLINE_INSERIR_ATUALIZAR_REPRESENTANTE", CommandType.StoredProcedure))
            {
                cmd.Connection.Open();

                cmd.Parameters.Add(new SqlParameter { ParameterName = "@id", SqlDbType = SqlDbType.Int, Value = representative.IdRepresentation, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@idClienteOnline", SqlDbType = SqlDbType.Int, Value = representative.IdClientOnline, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@nome", SqlDbType = SqlDbType.VarChar, Value = representative.Name, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@idFormaRepresentacao", SqlDbType = SqlDbType.Int, Value = representative.IdRepresentationForm, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@idTipoDocumento", SqlDbType = SqlDbType.Int, Value = Convert.ToInt32(representative.DocumentType), Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@numeroDocumento", SqlDbType = SqlDbType.VarChar, Value = representative.NumberDocument, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@orgaoEmissor", SqlDbType = SqlDbType.VarChar, Value = representative.IssuingAgency, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@dataValidadeDoc", SqlDbType = SqlDbType.Date, Value = representative.ValidateDocument, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@dataEmissoaDoc", SqlDbType = SqlDbType.Date, Value = representative.IssuingDate, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@dataNasc", SqlDbType = SqlDbType.Date, Value = representative.Birthdate, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@localNasc", SqlDbType = SqlDbType.VarChar, Value = representative.Birthplace, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@nacionalidade", SqlDbType = SqlDbType.VarChar, Value = representative.Nacionality, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@idEstadoCivil", SqlDbType = SqlDbType.Int, Value = Convert.ToInt32(representative.MaritalStatus), Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@idProfissao", SqlDbType = SqlDbType.Int, Value = Convert.ToInt32(representative.Profession), Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@qualificacaoCss", SqlDbType = SqlDbType.DateTime, Value = representative.CssQualification, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@dataValidadeCss", SqlDbType = SqlDbType.DateTime, Value = representative.CssExpired, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@funcaoGoverno", SqlDbType = SqlDbType.Bit, Value = representative.WorkGovernment == "1", Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@idPerfil", SqlDbType = SqlDbType.Int, Value = representative.IdProfile, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@nomeCargoGoverno", SqlDbType = SqlDbType.VarChar, Value = representative.JobGovernmentName, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@nomeEmpresaGoverno", SqlDbType = SqlDbType.VarChar, Value = representative.Agency, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@inicioFuncaoGoverno", SqlDbType = SqlDbType.Date, Value = representative.StartDateJobGovernment, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@fimFuncaoGoverno", SqlDbType = SqlDbType.Date, Value = representative.FinalDateJobGovernment, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@parenteFuncaoGoverno", SqlDbType = SqlDbType.Bit, Value = representative.RelativeWorkGovernment == "1", Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@nomeParente", SqlDbType = SqlDbType.VarChar, Value = representative.RelativeName, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@relacionamento", SqlDbType = SqlDbType.VarChar, Value = representative.RelativeRelationship, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@nomeFuncaoParenteGov", SqlDbType = SqlDbType.VarChar, Value = representative.JobRelativeName, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@nomeEmpresaParenteGov", SqlDbType = SqlDbType.VarChar, Value = representative.JobRelativeName, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@vinculoBrPartners", SqlDbType = SqlDbType.Bit, Value = representative.RelationshipBrPartners == "1", Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@residenciaFiscal", SqlDbType = SqlDbType.Bit, Value = representative.TaxResidence, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@nomePaisResidencia", SqlDbType = SqlDbType.VarChar, Value = representative.TaxNameResidence, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@nomePais", SqlDbType = SqlDbType.VarChar, Value = representative.TaxCountry, Direction = ParameterDirection.Input });

                cmd.ExecuteNonQuery();

                InsertAddress(representative.Address, representative.IdClientOnline, "INSERE_ENDERECO_REPRESENTANTE", "TB_CLIENTE_ONLINE_REPRESENTANTE_ENDERECO", "ID_CLIENTE_ONLINE");
                InsertContact(representative.Contact, representative.IdClientOnline, "INSERE_CONTATO_REPRESENTANTE", "TB_CLIENTE_ONLINE_REPRESENTANTE_CONTATO", "ID_CLIENTE_ONLINE");
            }

            return 1;
        }

        public int UpsertClientFinancial(ClientOnlineFinancial clientOnlineFinancial)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = new SqlCommand();

            if (clientOnlineFinancial.Id == 0)
                cmd = (SqlCommand)dbiCRK.CreateCommand("PR_CLIENTE_ONLINE_INSERIR_FINANCEIRO", CommandType.StoredProcedure);
            else
                cmd = (SqlCommand)dbiCRK.CreateCommand("PR_CLIENTE_ONLINE_ATUALIZAR_FINANCEIRO", CommandType.StoredProcedure);

            cmd.Connection.Open();
            AddParametersFinancial(cmd, clientOnlineFinancial);
            cmd.ExecuteNonQuery();
            cmd.Connection.Close();

            return 1;
        }

        public int UpsertDocumentClientOnline(ClientOnlineDocuments clientOnlineDocuments)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");

            foreach (var item in clientOnlineDocuments.Documents)
            {
                var cmd = (SqlCommand)dbiCRK.CreateCommand("PR_CLIENTE_ONLINE_INSERIR_ATUALIZAR_DOCUMENTO", CommandType.StoredProcedure);
                cmd.Connection.Open();

                cmd.Parameters.Add(new SqlParameter { ParameterName = "@idClienteOnline", SqlDbType = SqlDbType.Int, Value = clientOnlineDocuments.IdClientOnline, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@idDoc", SqlDbType = SqlDbType.Int, Value = item.IdDocClient, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@nome", SqlDbType = SqlDbType.VarChar, Value = item.FileName, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@extensao", SqlDbType = SqlDbType.VarChar, Value = item.ContentType, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@data", SqlDbType = SqlDbType.VarBinary, Value = item.Data, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@fisico", SqlDbType = SqlDbType.Bit, Value = item.HasPysical, Direction = ParameterDirection.Input });

                cmd.ExecuteNonQuery();
                cmd.Connection.Close();
            }

            return 1;
        }

        public int UpsertFatca(ClientOnline client)
        {
            if (client.Estrangeiro == "1")
            {
                DBInterface dbiCRK = new DBInterface("PORTAL");

                var cmd = (SqlCommand)dbiCRK.CreateCommand("PR_CLIENTE_ONLINE_INSERIR_ATUALIZAR_FACTA", CommandType.StoredProcedure);

                cmd.Parameters.Add(new SqlParameter { ParameterName = "@idClienteOnline", SqlDbType = SqlDbType.Int, Value = client.Id, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@facta", SqlDbType = SqlDbType.VarChar, Value = client.Fatca.Fatca, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@tin", SqlDbType = SqlDbType.VarChar, Value = client.Fatca.Tin, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@paisEmissor", SqlDbType = SqlDbType.VarChar, Value = client.Fatca.IssuingCountry, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@idNatureza", SqlDbType = SqlDbType.Int, Value = client.Fatca.IdFiscalInformation, Direction = ParameterDirection.Input });

                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                cmd.Connection.Close();
            }

            return 1;
        }

        public int UpsertDocumentRepresentative(ClientOnlineRules clientOnlineRules)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PR_CLIENTE_ONLINE_INSERIR_PODER", CommandType.StoredProcedure);
            cmd.Connection.Open();

            foreach (var item in clientOnlineRules.Representatives)
            {
                if (item.Id == 0)
                {
                    cmd.Parameters.Clear();
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@idClienteOnline", SqlDbType = SqlDbType.Int, Value = clientOnlineRules.IdClientOnline, Direction = ParameterDirection.Input });
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@idDocumento", SqlDbType = SqlDbType.Int, Value = clientOnlineRules.IdDocument, Direction = ParameterDirection.Input });
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@idDocumentType", SqlDbType = SqlDbType.Int, Value = clientOnlineRules.IdDocumentType, Direction = ParameterDirection.Input });
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@inicioValidade", SqlDbType = SqlDbType.Date, Value = clientOnlineRules.StartValidateDocument, Direction = ParameterDirection.Input });
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@fimValidade", SqlDbType = SqlDbType.Date, Value = clientOnlineRules.FinalValidateDocument, Direction = ParameterDirection.Input });
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@idRepresentante", SqlDbType = SqlDbType.Int, Value = item.IdRepresentative, Direction = ParameterDirection.Input });
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@numeroDocumento", SqlDbType = SqlDbType.VarChar, Value = item.NumberDocument, Direction = ParameterDirection.Input });
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@idQualificacao", SqlDbType = SqlDbType.Int, Value = item.IdQualification, Direction = ParameterDirection.Input });
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@idStatus", SqlDbType = SqlDbType.Int, Value = 1, Direction = ParameterDirection.Input });
                    cmd.ExecuteNonQuery();
                }
            }

            cmd.Connection.Close();

            DeleteDocumentsRepresentatives(clientOnlineRules.DeleteRepresentatives, clientOnlineRules.IdClientOnline);
            return 1;
        }

        public int UpdateStatusSubmitRegistration(int idClientOnline, int idStatusRegistration)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PR_CLIENTE_ONLINE_ATUALIZAR_STATUS_CADASTRO", CommandType.StoredProcedure);
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@idCliente", SqlDbType = SqlDbType.Int, Value = idClientOnline, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@idStatus", SqlDbType = SqlDbType.Int, Value = idStatusRegistration, Direction = ParameterDirection.Input });
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            cmd.Connection.Close();

            return 1;
        }

        public void InsertBankAccount(ClientOnlineFinancial clientOnlineFinancial)
        {
            ExecProcedureDeleteData("APAGA_CONTA_BANCARIA", clientOnlineFinancial.IdClientOnline);
            DBInterface dbiCRK = new DBInterface("PORTAL");

            foreach (var item in clientOnlineFinancial.Accounts)
            {
                var cmd = (SqlCommand)dbiCRK.CreateCommand("INSERE_CONTA_BANCARIA", CommandType.StoredProcedure);
                cmd.Connection.Open();
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@idClienteOnline", SqlDbType = SqlDbType.Int, Value = clientOnlineFinancial.IdClientOnline, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@IdFinalidade", SqlDbType = SqlDbType.Int, Value = item.IdFinality, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@idUtilizacao", SqlDbType = SqlDbType.Int, Value = item.IdUtility, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@idTipoConta", SqlDbType = SqlDbType.Int, Value = item.IdAccountType, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@idTipoContaBanco", SqlDbType = SqlDbType.Int, Value = item.IdAccountTypeBank, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@idBanco", SqlDbType = SqlDbType.Int, Value = item.IdBank, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@agencia", SqlDbType = SqlDbType.VarChar, Value = item.Agency, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@numeroConta", SqlDbType = SqlDbType.VarChar, Value = item.NumberAccount, Direction = ParameterDirection.Input });
                cmd.ExecuteNonQuery();
                cmd.Connection.Close();
            }
        }

        public void InsertBankProduct(ClientOnlineFinancial clientOnlineFinancial)
        {
            ExecProcedureDeleteData("APAGA_PRODUTO_BANCARIO", clientOnlineFinancial.IdClientOnline);
            DBInterface dbiCRK = new DBInterface("PORTAL");

            foreach (var item in clientOnlineFinancial.Products)
            {
                var cmd = (SqlCommand)dbiCRK.CreateCommand("INSERE_PRODUTO_BANCARIO", CommandType.StoredProcedure);
                cmd.Connection.Open();
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@idClienteOnline", SqlDbType = SqlDbType.Int, Value = item.IdClientOnline, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@idProduto", SqlDbType = SqlDbType.Int, Value = item.IdProduct, Direction = ParameterDirection.Input });
                cmd.ExecuteNonQuery();
                cmd.Connection.Close();
            }
        }

        public void InsertEmail(ClientOnline client, string table, string column)
        {
            DeletaDados(client.Id, table, column);
            DBInterface dbiCRK = new DBInterface("PORTAL");

            foreach (var item in client.Emails)
            {
                var cmd = (SqlCommand)dbiCRK.CreateCommand("INSERE_EMAIL_CLIENTE_ONLINE", CommandType.StoredProcedure);
                cmd.Connection.Open();

                cmd.Parameters.Add(new SqlParameter { ParameterName = "@idClienteOnline", SqlDbType = SqlDbType.Int, Value = client.Id, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@email", SqlDbType = SqlDbType.VarChar, Value = item, Direction = ParameterDirection.Input });

                cmd.ExecuteNonQuery();
                cmd.Connection.Close();
            }
        }

        public void InsertAddress(List<AddressClientOnline> client, int idClientOnline, string procName, string table, string column)
        {
            DeletaDados(idClientOnline, table, column);
            DBInterface dbiCRK = new DBInterface("PORTAL");

            foreach (var item in client)
            {
                var cmd = (SqlCommand)dbiCRK.CreateCommand(procName, CommandType.StoredProcedure);
                cmd.Connection.Open();

                cmd.Parameters.Add(new SqlParameter { ParameterName = "@idClienteOnline", SqlDbType = SqlDbType.Int, Value = idClientOnline, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@tipoEndereco", SqlDbType = SqlDbType.VarChar, Value = item.AddressType, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@finalidade", SqlDbType = SqlDbType.VarChar, Value = item.Finality, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@tipoLogradouro", SqlDbType = SqlDbType.VarChar, Value = item.StreetType, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@logradouro", SqlDbType = SqlDbType.VarChar, Value = item.StreetName, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@numero", SqlDbType = SqlDbType.VarChar, Value = item.Number, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@estado", SqlDbType = SqlDbType.VarChar, Value = item.State, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@cidade", SqlDbType = SqlDbType.VarChar, Value = item.City, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@pais", SqlDbType = SqlDbType.VarChar, Value = item.Country, Direction = ParameterDirection.Input });

                cmd.ExecuteNonQuery();
                cmd.Connection.Close();
            }
        }

        public void InsertContact(List<ContactClientOnline> client, int idClientOnline, string procName, string table, string column)
        {
            DeletaDados(idClientOnline, table, column);
            DBInterface dbiCRK = new DBInterface("PORTAL");

            foreach (var item in client)
            {
                var cmd = (SqlCommand)dbiCRK.CreateCommand(procName, CommandType.StoredProcedure);

                cmd.Connection.Open();

                cmd.Parameters.Add(new SqlParameter { ParameterName = "@idClienteOnline", SqlDbType = SqlDbType.Int, Value = idClientOnline, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@tipo", SqlDbType = SqlDbType.VarChar, Value = item.Type, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@ddi", SqlDbType = SqlDbType.VarChar, Value = item.Ddi, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@ddd", SqlDbType = SqlDbType.VarChar, Value = item.Ddd, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@numero", SqlDbType = SqlDbType.VarChar, Value = item.Number, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@ramal", SqlDbType = SqlDbType.VarChar, Value = item.Ramal, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@emailContato", SqlDbType = SqlDbType.VarChar, Value = item.EmailContact, Direction = ParameterDirection.Input });

                cmd.ExecuteNonQuery();
                cmd.Connection.Close();
            }
        }

        public void InsertAssign(ClientOnline client, string table, string column)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");

            foreach (var item in client.ClientFiles)
            {
                var cmd = (SqlCommand)dbiCRK.CreateCommand("PR_CLIENTE_ONLINE_INSERIR_ASSINATURA", CommandType.StoredProcedure);
                cmd.Connection.Open();

                cmd.Parameters.Add(new SqlParameter { ParameterName = "@idClienteOnline", SqlDbType = SqlDbType.Int, Value = client.Id, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@nome", SqlDbType = SqlDbType.VarChar, Value = item.FileName, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@extensao", SqlDbType = SqlDbType.VarChar, Value = item.ContentType, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@data", SqlDbType = SqlDbType.VarBinary, Value = item.Data, Direction = ParameterDirection.Input });

                cmd.ExecuteNonQuery();
                cmd.Connection.Close();
            }
        }

        public bool DeleteAssign(int idFile)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");

            using (var cmd = (SqlCommand)dbiCRK.CreateCommand("PR_CLIENTE_ONLINE_EXCLUIR_ASSINATURA_POR_ID", CommandType.StoredProcedure))
            {
                cmd.Connection.Open();
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@id", SqlDbType = SqlDbType.Int, Value = idFile, Direction = ParameterDirection.Input });
                return cmd.ExecuteNonQuery() == 1;
            }
        }

        public bool DeleteRepresentative(int idClientOnline, int idRepresentative)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");

            using (var cmd = (SqlCommand)dbiCRK.CreateCommand("PR_CLIENTE_ONLINE_EXCLUIR_REPRESENTANTE", CommandType.StoredProcedure))
            {
                cmd.Connection.Open();
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@id", SqlDbType = SqlDbType.Int, Value = idRepresentative, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@IDCLIENTEONLINE", SqlDbType = SqlDbType.Int, Value = idClientOnline, Direction = ParameterDirection.Input });
                return cmd.ExecuteNonQuery() == 1;
            }
        }

        public ClientAssign GetAssignById(int id)
        {
            var file = new ClientAssign();
            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PR_CLIENTE_ONLINE_OBTER_ASSINATURA_POR_ID", CommandType.StoredProcedure);
            cmd.Connection.Open();

            cmd.Parameters.Add(new SqlParameter { ParameterName = "@id", SqlDbType = SqlDbType.Int, Value = id, Direction = ParameterDirection.Input });

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.Read())
                {
                    file.FileName = Convert.ToString(dr["NOME"]);
                    file.Id = Convert.ToInt32(dr["ID"]);
                    file.ContentType = Convert.ToString(dr["EXTENSAO"]);
                    file.Data = (byte[])dr["ARQUIVO"];
                }

                dr.Close();
            }

            return file;
        }

        public ClientDocument GetModelDocumentById(int id)
        {
            var file = new ClientDocument();
            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PR_CLIENTE_ONLINE_OBTER_MODELO_DOCUMENTO", CommandType.StoredProcedure);
            cmd.Connection.Open();

            cmd.Parameters.Add(new SqlParameter { ParameterName = "@id", SqlDbType = SqlDbType.Int, Value = id, Direction = ParameterDirection.Input });

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.Read())
                {
                    file.FileName = Convert.ToString(dr["DESCRICAO_ARQUIVO"]);
                    file.Id = Convert.ToInt32(dr["ID"]);
                    file.ContentType = Convert.ToString(dr["EXTENSAO"]);
                    file.Data = (byte[])dr["ARQUIVO"];
                }

                dr.Close();
            }

            return file;
        }

        public ClientDocument GetDocumentById(int id)
        {
            var file = new ClientDocument();
            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PR_CLIENTE_ONLINE_OBTER_DOCUMENTO_POR_ID", CommandType.StoredProcedure);
            cmd.Connection.Open();

            cmd.Parameters.Add(new SqlParameter { ParameterName = "@id", SqlDbType = SqlDbType.Int, Value = id, Direction = ParameterDirection.Input });

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.Read())
                {
                    file.FileName = Convert.ToString(dr["NOME"]);
                    file.Id = Convert.ToInt32(dr["ID_DOCUMENTO"]);
                    file.ContentType = Convert.ToString(dr["EXTENSAO"]);
                    file.Data = (byte[])dr["ARQUIVO"];
                }

                dr.Close();
            }

            return file;
        }

        public void DeleteDocumentsRepresentatives(List<string> idDocuments, int idclientOnline)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");

            foreach (var item in idDocuments)
            {
                var cmd = (SqlCommand)dbiCRK.CreateCommand("PR_CLIENTE_ONLINE_EXCLUIR_PODER", CommandType.StoredProcedure);
                cmd.Connection.Open();
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@id", SqlDbType = SqlDbType.Int, Value = item, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@idClienteOnline", SqlDbType = SqlDbType.Int, Value = idclientOnline, Direction = ParameterDirection.Input });
                cmd.ExecuteNonQuery();
                cmd.Connection.Close();
            }
        }

        private void AddParametersClientOnline(SqlCommand cmd, ClientOnline client)
        {
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@idClienteOnline", SqlDbType = SqlDbType.Int, Value = client.Id, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@grupoEconomico", SqlDbType = SqlDbType.VarChar, Value = client.EconomicGroup, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@classificacao", SqlDbType = SqlDbType.VarChar, Value = client.Classification, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@grupoAtividade", SqlDbType = SqlDbType.VarChar, Value = client.ActivityGroup, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@tipoCadastro", SqlDbType = SqlDbType.VarChar, Value = client.RegistrationType, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@tipoCliente", SqlDbType = SqlDbType.VarChar, Value = client.ClientType, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@categoria", SqlDbType = SqlDbType.VarChar, Value = client.Category, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@atividadePrincipal", SqlDbType = SqlDbType.VarChar, Value = client.AtividadePrincipal, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@naturezaOperacao", SqlDbType = SqlDbType.VarChar, Value = client.NaturezaOperacao, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@porte", SqlDbType = SqlDbType.VarChar, Value = client.Size, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@nome", SqlDbType = SqlDbType.VarChar, Value = client.Name, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@cpf", SqlDbType = SqlDbType.VarChar, Value = client.Cpf, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@profissao", SqlDbType = SqlDbType.VarChar, Value = client.Profession, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@genero", SqlDbType = SqlDbType.VarChar, Value = client.Sex, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@dataNasc", SqlDbType = SqlDbType.DateTime, Value = client.Birthdate, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@estadoCivil", SqlDbType = SqlDbType.VarChar, Value = client.MaritalStatus, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@regimeCasamento", SqlDbType = SqlDbType.VarChar, Value = client.MatrimonialRegime, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@uniaoEstavel", SqlDbType = SqlDbType.VarChar, Value = client.CommonLaw, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@estrangeiro", SqlDbType = SqlDbType.VarChar, Value = client.Estrangeiro, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@nomePai", SqlDbType = SqlDbType.VarChar, Value = client.NomePai, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@nomeMae", SqlDbType = SqlDbType.VarChar, Value = client.NomeMae, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@emailPrincipal", SqlDbType = SqlDbType.VarChar, Value = client.EmailPrincipal, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@status", SqlDbType = SqlDbType.VarChar, Value = client.Situation, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@observacao", SqlDbType = SqlDbType.VarChar, Value = client.Observacao, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@paisNascimento", SqlDbType = SqlDbType.VarChar, Value = client.PaisNascimento, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@estadoNascimento", SqlDbType = SqlDbType.VarChar, Value = client.EstadoNascimento, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@naturalidade", SqlDbType = SqlDbType.VarChar, Value = client.Naturalness, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@nacionalidade", SqlDbType = SqlDbType.VarChar, Value = client.Nacionality, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@nomeConjugue", SqlDbType = SqlDbType.VarChar, Value = client.NomeConjugue, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@cpfConjugue", SqlDbType = SqlDbType.VarChar, Value = client.CpfConjugue, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@nascimentoConjugue", SqlDbType = SqlDbType.DateTime, Value = client.DataNascimentoConjugue, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@tipoDocumento", SqlDbType = SqlDbType.VarChar, Value = client.DocumentType, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@numeroDocumento", SqlDbType = SqlDbType.VarChar, Value = client.NumberDocument, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@orgaoEmissor", SqlDbType = SqlDbType.VarChar, Value = client.IssuingAgency, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@dataVencimento", SqlDbType = SqlDbType.DateTime, Value = client.ValidateDocument, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@dataEmissao", SqlDbType = SqlDbType.DateTime, Value = client.IssuingDate, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@estadoEmissao", SqlDbType = SqlDbType.VarChar, Value = client.EstadoEmissao, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@nomeEmpresa", SqlDbType = SqlDbType.VarChar, Value = client.NomeEmpresa, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@dataAdmissao", SqlDbType = SqlDbType.DateTime, Value = client.DataAdmissao, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@endereco", SqlDbType = SqlDbType.VarChar, Value = client.Endereco, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@gerente", SqlDbType = SqlDbType.VarChar, Value = client.Manager, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@dataInicio", SqlDbType = SqlDbType.DateTime, Value = client.DataInicio, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@dataFinal", SqlDbType = SqlDbType.DateTime, Value = client.DataFinal, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@cargoPublico", SqlDbType = SqlDbType.VarChar, Value = client.WorkGovernment, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@funcaoPublica", SqlDbType = SqlDbType.VarChar, Value = client.JobGovernmentName, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@empresaPublica", SqlDbType = SqlDbType.VarChar, Value = client.Agency, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@dataInicioEP", SqlDbType = SqlDbType.DateTime, Value = client.StartDateJobGovernment, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@dataFinalEP", SqlDbType = SqlDbType.DateTime, Value = client.FinalDateJobGovernment, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@parenteCargoPublico", SqlDbType = SqlDbType.VarChar, Value = client.RelativeWorkGovernment, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@nomeParente", SqlDbType = SqlDbType.VarChar, Value = client.RelativeName, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@tipoParente", SqlDbType = SqlDbType.VarChar, Value = client.RelativeRelationship, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@funcaoPublicaParente", SqlDbType = SqlDbType.VarChar, Value = client.JobRelativeName, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@possuiRepresentante", SqlDbType = SqlDbType.VarChar, Value = client.PossuiRepresentante, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@relacaoBrPartners", SqlDbType = SqlDbType.VarChar, Value = client.RelationshipBrPartners, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@paisEstrangeiro", SqlDbType = SqlDbType.VarChar, Value = client.PaisNascimentoEstrangeiro, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@estadoEstrangeiro", SqlDbType = SqlDbType.VarChar, Value = client.EstadoNascimentoEstrangeiro, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@cidadeEstrangeiro", SqlDbType = SqlDbType.VarChar, Value = client.NaturalnessForeign, Direction = ParameterDirection.Input });
        }

        private void AddParametersFinancial(SqlCommand cmd, ClientOnlineFinancial client)
        {
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@idClienteOnline", SqlDbType = SqlDbType.Int, Value = client.IdClientOnline, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@rendaMensal", SqlDbType = SqlDbType.VarChar, Value = client.MonthlyProfit, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@rendaFamilia", SqlDbType = SqlDbType.VarChar, Value = client.FamilyMonthlyProfit, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@outrasRendas", SqlDbType = SqlDbType.VarChar, Value = client.AnotherProfit, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@imovelPatrimonio", SqlDbType = SqlDbType.VarChar, Value = client.HousePatrimony, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@veiculoPatrimonio", SqlDbType = SqlDbType.VarChar, Value = client.CarPatrimony, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@aplicacaoPatrimonio", SqlDbType = SqlDbType.VarChar, Value = client.ApplicationPatrimony, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@aplicacaoOrigem", SqlDbType = SqlDbType.Bit, Value = client.ApplicationOrigin, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@indenizacao", SqlDbType = SqlDbType.Bit, Value = client.Indemnity, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@premio", SqlDbType = SqlDbType.Bit, Value = client.Award, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@rescisao", SqlDbType = SqlDbType.Bit, Value = client.Termination, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@salario", SqlDbType = SqlDbType.Bit, Value = client.Salary, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@venda", SqlDbType = SqlDbType.Bit, Value = client.Sell, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@joias", SqlDbType = SqlDbType.Bit, Value = client.Jewels, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@aluguel", SqlDbType = SqlDbType.Bit, Value = client.Rental, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@doacao", SqlDbType = SqlDbType.Bit, Value = client.Donation, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@heranca", SqlDbType = SqlDbType.Bit, Value = client.Heritage, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@automovel", SqlDbType = SqlDbType.Bit, Value = client.Car, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@seguro", SqlDbType = SqlDbType.Bit, Value = client.Insurance, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@producao", SqlDbType = SqlDbType.Bit, Value = client.ProductAgricultural, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@imovel", SqlDbType = SqlDbType.Bit, Value = client.House, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@outros", SqlDbType = SqlDbType.Bit, Value = client.Another, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@outrosDescricao", SqlDbType = SqlDbType.VarChar, Value = client.AnotherDescription, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@idNatureza", SqlDbType = SqlDbType.Int, Value = client.IdFiscalNature, Direction = ParameterDirection.Input });
        }

        private void DeletaDados(int idClientOnline, string table, string column)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");

            using (var cmd = (SqlCommand)dbiCRK.CreateCommand("DELETA_DADOS_TABELA", CommandType.StoredProcedure))
            {
                cmd.Connection.Open();
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@idClientOnline", SqlDbType = SqlDbType.VarChar, Value = idClientOnline, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@nomeTabela", SqlDbType = SqlDbType.VarChar, Value = table, Direction = ParameterDirection.Input });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@nomeColuna", SqlDbType = SqlDbType.VarChar, Value = column, Direction = ParameterDirection.Input });
                cmd.ExecuteNonQuery();
            }
        }

        private void ExecProcedureDeleteData(string procName, int idClientOnline)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");

            using (var cmd = (SqlCommand)dbiCRK.CreateCommand(procName, CommandType.StoredProcedure))
            {
                cmd.Connection.Open();
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@idClientOnline", SqlDbType = SqlDbType.Int, Value = idClientOnline, Direction = ParameterDirection.Input });
                cmd.ExecuteNonQuery();
            }

        }

        private void GetEmailClientOnline(ClientOnline clientOnline)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");

            using (var cmd = (SqlCommand)dbiCRK.CreateCommand("PROC_CARREGA_EMAIL_CLIENTE_ONLINE", CommandType.StoredProcedure))
            {
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@idClientOnline", Value = clientOnline.Id, SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input });
                cmd.Connection.Open();

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        clientOnline.Emails.Add(dr["EMAIL"].ToString());
                    }

                    dr.Close();
                }
            }
        }

        private void GetAddressClientOnline(ClientOnline clientOnline)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");

            using (var cmd = (SqlCommand)dbiCRK.CreateCommand("PR_CLIENTE_ONLINE_LISTAR_ENDERECO", CommandType.StoredProcedure))
            {
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@idClientOnline", Value = clientOnline.Id, SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input });
                cmd.Connection.Open();

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        var address = new AddressClientOnline
                        {
                            Id = Convert.ToInt32(dr["ID"]),
                            AddressType = Convert.ToString(dr["TIPO_ENDERECO"]),
                            DescriptionAddressType = Convert.ToString(dr["NOMETIPOENDERECO"]),
                            Finality = Convert.ToString(dr["FINALIDADE"]),
                            DescriptionFinality = Convert.ToString(dr["NOMEFINALIDADE"]),
                            StreetType = Convert.ToString(dr["TIPO_LOGRADOURO"]),
                            StreetName = Convert.ToString(dr["LOGRADOURO"]),
                            Number = Convert.ToString(dr["NUMERO"]),
                            State = Convert.ToString(dr["ESTADO"]),
                            DescriptionState = Convert.ToString(dr["NOMEESTADO"]),
                            City = Convert.ToString(dr["CIDADE"]),
                            DescriptionCountry = Convert.ToString(dr["NOMEPAIS"]),
                            Country = Convert.ToString(dr["PAIS"])
                        };

                        clientOnline.Address.Add(address);
                    }

                    dr.Close();
                }
            }
        }

        private void GetContactClientOnline(ClientOnline clientOnline)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");

            using (var cmd = (SqlCommand)dbiCRK.CreateCommand("PR_CLIENTE_ONLINE_LISTAR_CONTATO", CommandType.StoredProcedure))
            {
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@idClientOnline", Value = clientOnline.Id, SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input });
                cmd.Connection.Open();

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        var contact = new ContactClientOnline
                        {
                            Id = Convert.ToInt32(dr["ID"]),
                            Type = Convert.ToString(dr["TIPO"]),
                            DescriptionType = Convert.ToString(dr["NOMETIPO"]),
                            Ddi = Convert.ToString(dr["DDI"]),
                            Ddd = Convert.ToString(dr["DDD"]),
                            Number = Convert.ToString(dr["NUMERO"]),
                            Ramal = Convert.ToString(dr["RAMAL"]),
                            EmailContact = Convert.ToString(dr["EMAIL_CONTATO"])
                        };

                        clientOnline.Contact.Add(contact);
                    }

                    dr.Close();
                }
            }
        }

        private void GetAddressRepresentative(ClientOnlineRepresentative representative)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");

            using (var cmd = (SqlCommand)dbiCRK.CreateCommand("PR_CLIENTE_ONLINE_REPRESENTANTE_LISTAR_ENDERECO", CommandType.StoredProcedure))
            {
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@idClientOnline", Value = representative.IdClientOnline, SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input });
                cmd.Connection.Open();

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        var address = new AddressClientOnline
                        {
                            AddressType = Convert.ToString(dr["ID_TIPO_ENDERECO"]),
                            DescriptionAddressType = Convert.ToString(dr["NOMETIPOENDERECO"]),
                            Finality = Convert.ToString(dr["ID_FINALIDADE"]),
                            DescriptionFinality = Convert.ToString(dr["NOMEFINALIDADE"]),
                            StreetType = Convert.ToString(dr["TIPO_LOGRADOURO"]),
                            StreetName = Convert.ToString(dr["LOGRADOURO"]),
                            Number = Convert.ToString(dr["NUMERO"]),
                            State = Convert.ToString(dr["ESTADO"]),
                            DescriptionState = Convert.ToString(dr["NOMEESTADO"]),
                            City = Convert.ToString(dr["CIDADE"]),
                            DescriptionCountry = Convert.ToString(dr["NOMEPAIS"]),
                            Country = Convert.ToString(dr["PAIS"])
                        };

                        representative.Address.Add(address);
                    }

                    dr.Close();
                }
            }
        }

        private void GetContactRepresentative(ClientOnlineRepresentative representative)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");

            using (var cmd = (SqlCommand)dbiCRK.CreateCommand("PR_CLIENTE_ONLINE_REPRESENTANTE_LISTAR_CONTATO", CommandType.StoredProcedure))
            {
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@idClientOnline", Value = representative.IdClientOnline, SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input });
                cmd.Connection.Open();

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        var contact = new ContactClientOnline
                        {
                            Type = Convert.ToString(dr["ID_TIPO_CONTATO"]),
                            DescriptionType = Convert.ToString(dr["NOMETIPO"]),
                            Ddi = Convert.ToString(dr["DDI"]),
                            Ddd = Convert.ToString(dr["DDD"]),
                            Number = Convert.ToString(dr["NUMERO"]),
                            Ramal = Convert.ToString(dr["RAMAL"]),
                            EmailContact = Convert.ToString(dr["EMAIL_CONTATO"])
                        };

                        representative.Contact.Add(contact);
                    }

                    dr.Close();
                }
            }
        }

        private void GetAssignClientOnline(ClientOnline clientOnline)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");

            using (var cmd = (SqlCommand)dbiCRK.CreateCommand("PR_CLIENTE_ONLINE_LISTAR_ASSINATURAS", CommandType.StoredProcedure))
            {
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@idClientOnline", Value = clientOnline.Id, SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input });
                cmd.Connection.Open();

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        clientOnline.ClientFiles.Add(new ClientAssign
                        {
                            FileName = Convert.ToString(dr["NOME"]),
                            Id = Convert.ToInt32(dr["ID"])
                        });
                    }

                    dr.Close();
                }
            }
        }

        private void GetFatca(ClientOnline clientOnline)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");

            using (var cmd = (SqlCommand)dbiCRK.CreateCommand("PR_CLIENTE_ONLINE_LISTAR_FATCA", CommandType.StoredProcedure))
            {
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@idClientOnline", Value = clientOnline.Id, SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input });
                cmd.Connection.Open();

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        clientOnline.Fatca.Fatca = dr["FATCA"].ToString();
                        clientOnline.Fatca.Tin = dr["TIN"].ToString();
                        clientOnline.Fatca.IssuingCountry = dr["PAIS_EMISSOR"].ToString();
                        clientOnline.Fatca.IdFiscalInformation = Convert.ToInt32(dr["ID_INFORMACAO_FISCAL"]);
                    }

                    dr.Close();
                }
            }
        }

        private List<FinancialAccount> GetClientBankAccount(int idClientOnline)
        {
            var accounts = new List<FinancialAccount>();
            DBInterface dbiCRK = new DBInterface("PORTAL");

            using (var cmd = (SqlCommand)dbiCRK.CreateCommand("PROC_CARREGA_CLIENTE_FINANCEIRO_CONTA", CommandType.StoredProcedure))
            {
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@idClientOnline", Value = idClientOnline, SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input });
                cmd.Connection.Open();

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        var account = new FinancialAccount
                        {
                            IdFinality = Convert.ToInt32(dr["ID_FINALIDADE"]),
                            DescriptionFinality = Convert.ToString(dr["DESCRICAOFINALIDADE"]),
                            IdUtility = Convert.ToInt32(dr["ID_UTILIZACAO"]),
                            DescriptionUtility = Convert.ToString(dr["DESCRICAOUTILIZACAO"]),
                            IdAccountType = Convert.ToInt32(dr["ID_TIPO_CONTA"]),
                            DescriptionAccountType = Convert.ToString(dr["DESCRICAOTIPOCONTA"]),
                            IdAccountTypeBank = Convert.ToInt32(dr["ID_TIPO_CONTA_BANCO"]),
                            DescriptionAccountTypeBank = Convert.ToString(dr["DESCRICAOTIPOCONTABANCO"]),
                            IdBank = Convert.ToInt32(dr["ID_BANCO"]),
                            DescriptionBank = Convert.ToString(dr["DESCRICAOBANCO"]),
                            Agency = Convert.ToString(dr["AGENCIA"]),
                            NumberAccount = Convert.ToString(dr["NUMEROCONTA"])
                        };
                        accounts.Add(account);
                    }

                    dr.Close();
                }
            }

            return accounts;
        }

        private List<FinancialProduct> GetClientProductFinancial(int idClientOnline)
        {
            var accounts = new List<FinancialProduct>();
            DBInterface dbiCRK = new DBInterface("PORTAL");

            using (var cmd = (SqlCommand)dbiCRK.CreateCommand("PROC_CARREGA_CLIENTE_FINANCEIRO_PRODUTO", CommandType.StoredProcedure))
            {
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@idClientOnline", Value = idClientOnline, SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input });
                cmd.Connection.Open();

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        var account = new FinancialProduct
                        {
                            IdProduct = Convert.ToInt32(dr["ID_PRODUTO"]),
                            DescriptionProduct = Convert.ToString(dr["DESCRICAO"])
                        };
                        accounts.Add(account);
                    }

                    dr.Close();
                }
            }

            return accounts;
        }

        #endregion
    }
}