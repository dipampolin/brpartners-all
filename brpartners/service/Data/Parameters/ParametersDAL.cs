﻿using QX3.Spinnex.Common.Services.Data;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace QX3.Portal.Services.Data.Parameters
{
    public class ParametersDAL
    {
        public List<Contracts.DataContracts.Parameters> GetStates()
        {
            return GetParameters("TB_ESTADO");
        }

        public List<Contracts.DataContracts.Parameters> GetCountries()
        {
            return GetParameters("TB_PAIS");
        }

        public List<Contracts.DataContracts.Parameters> GetAddressTypes()
        {
            return GetParameters("TB_TIPO_ENDERECO");
        }

        public List<Contracts.DataContracts.Parameters> GetFinalities()
        {
            return GetParameters("TB_FINALIDADE");
        }

        public List<Contracts.DataContracts.Parameters> GetActivities()
        {
            return GetParameters("TB_ATIVIDADE");
        }

        public List<Contracts.DataContracts.Parameters> GetOccupation()
        {
            return GetParameters("TB_OCUPACAO");
        }

        public List<Contracts.DataContracts.Parameters> GetSize()
        {
            return GetParameters("TB_PORTE");
        }

        public List<Contracts.DataContracts.Parameters> GetProfession()
        {
            return GetParameters("TB_PROFISSAO");
        }

        public List<Contracts.DataContracts.Parameters> GetMaritalStatus()
        {
            return GetParameters("TB_ESTADO_CIVIL");
        }

        public List<Contracts.DataContracts.Parameters> GetSex()
        {
            return GetParameters("TB_SEXO");
        }

        public List<Contracts.DataContracts.Parameters> GetStatus()
        {
            return GetParameters("TB_STATUS");
        }

        public List<Contracts.DataContracts.Parameters> GetDocumentTypes()
        {
            return GetParameters("TB_TIPO_DOCUMENTO");
        }

        public List<Contracts.DataContracts.Parameters> GetEconomicGroup()
        {
            return GetParameters("TB_GRUPO_ECONOMICO");
        }

        public List<Contracts.DataContracts.Parameters> GetClassification()
        {
            return GetParameters("TB_CLASSIFICACAO");
        }

        public List<Contracts.DataContracts.Parameters> GetEconomicActivities()
        {
            return GetParameters("TB_ATIVIDADE_ECONOMICA");
        }

        public List<Contracts.DataContracts.Parameters> GetRegistrationTypes()
        {
            return GetParameters("TB_TIPO_REGISTRO");
        }

        public List<Contracts.DataContracts.Parameters> GetClientTypes()
        {
            return GetParameters("TB_TIPO_CLIENTE");
        }

        public List<Contracts.DataContracts.Parameters> GetCategories()
        {
            return GetParameters("TB_CATEGORIA");
        }

        public List<Contracts.DataContracts.Parameters> GetManagers()
        {
            return GetParameters("TB_GERENTE");
        }

        public List<Contracts.DataContracts.Parameters> GetNaturalness()
        {
            return GetParameters("TB_NATURALIDADE");
        }

        public List<Contracts.DataContracts.Parameters> GetNacionalities()
        {
            return GetParameters("TB_NACIONALIDADE");
        }

        public List<Contracts.DataContracts.Parameters> GetTelephoneTypes()
        {
            return GetParameters("TB_TIPO_TELEFONE");
        }

        public List<Contracts.DataContracts.Parameters> GetMatrimonialRegime()
        {
            return GetParameters("TB_REGIME_CASAMENTO");
        }

        public List<Contracts.DataContracts.Parameters> GetChoice()
        {
            return GetParameters("TB_ESCOLHA");
        }


        // ************************* DADOS FINANCEIROS *********************************

        public List<Contracts.DataContracts.Parameters> GetFinancialProduct()
        {
            return GetParameters("TB_PRODUTO");
        }

        public List<Contracts.DataContracts.Parameters> GetFiscalNature()
        {
            return GetParameters("TB_NATUREZA_FISCAL");
        }

        public List<Contracts.DataContracts.Parameters> GetAccountFinality()
        {
            return GetParameters("TB_FINALIDADE_CONTA");
        }

        public List<Contracts.DataContracts.Parameters> GetAccountUtility()
        {
            return GetParameters("TB_UTILIZACAO_CONTA");
        }

        public List<Contracts.DataContracts.Parameters> GetAccountType()
        {
            return GetParameters("TB_TIPO_CONTA");
        }

        public List<Contracts.DataContracts.Parameters> GetAccountTypeBank()
        {
            return GetParameters("TB_TIPO_CONTA_BANCO");
        }

        public List<Contracts.DataContracts.Parameters> GetBank()
        {
            return GetParameters("TB_BANCO");
        }


        // ************************* REPRESENTANTE *********************************

        public List<Contracts.DataContracts.Parameters> GetRepresentationStyle()
        {
            return GetParameters("TB_FORMA_REPRESENTACAO");
        }

        public List<Contracts.DataContracts.Parameters> GetRepresentationProfile()
        {
            return GetParameters("TB_PERFIL_REPRESENTANTE");
        }


        // ************************* PODERES *********************************


        public List<Contracts.DataContracts.Parameters> GetQualifications()
        {
            return GetParameters("TB_QUALIFICACAO");
        }

        public List<Contracts.DataContracts.Parameters> GetRulesTypes()
        {
            return GetParameters("TB_TIPO_DOCUMENTO_PODER");
        }

        public List<Contracts.DataContracts.Parameters> GetExpirationDateDocument()
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");

            using (var cmd = (SqlCommand)dbiCRK.CreateCommand("PROC_CARREGA_PARAMETROS", CommandType.StoredProcedure))
            {
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@nomeTabela", Value = "TB_DOCUMENTO_VALIDADE", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input });
                cmd.Connection.Open();

                var parametros = new List<Contracts.DataContracts.Parameters>();

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        var parametro = new Contracts.DataContracts.Parameters
                        {
                            Valor = dr["ID"].ToString(),
                            Descricao = dr["MESES"].ToString()
                        };

                        parametros.Add(parametro);
                    }

                    dr.Close();
                }

                return parametros;
            }
        }

        private List<Contracts.DataContracts.Parameters> GetParameters(string nameTable)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PROC_CARREGA_PARAMETROS", CommandType.StoredProcedure);
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@nomeTabela", Value = nameTable, SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input });
            cmd.Connection.Open();

            var parametros = new List<Contracts.DataContracts.Parameters>();

            using (var dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    var parametro = new Contracts.DataContracts.Parameters
                    {
                        Valor = dr["ID"].ToString(),
                        Descricao = dr["DESCRICAO"].ToString()
                    };

                    parametros.Add(parametro);
                }

                dr.Close();
            }

            cmd.Connection.Close();

            return parametros;
        }
    }
}
