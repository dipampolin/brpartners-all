﻿using QX3.Portal.Contracts.DataContracts.SuitabilityAdmin;
using QX3.Spinnex.Common.Services.Data;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data;
using System.Data.OracleClient;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace QX3.Portal.Services.Data.SuitabilityAdmin
{
    public class SuitabilityAdminDAL : DataProviderBase, IDisposable
    {

        private DbCommand cmdInserirForm, cmdAtualizarForm, cmdExcluirForm;
        private DbCommand cmdInserirQuestao, cmdAtualizarQuestao, cmdExcluirQuestao;
        private DbCommand cmdInserirAlternativa, cmdAtualizarAlternativa, cmdExcluirAlternativa;
        private DbCommand cmdInserirMatriz, cmdAtualizarMatriz, cmdExcluirMatriz;
        private DbCommand cmdInserirPontuacao, cmdAtualizarPontuacao;


        public SuitabilityAdminDAL()
            : base("PortalConnectionString")
        {
        }
        public void Dispose()
        {
            if (dbi != null)
                dbi.Dispose();
        }


        //public void PrepararComandosCrud()
        //{
        //    cmdInserirForm = dbi.CreateCommand("PCK_GESTAO_SUITABILITY.INCLUIR_FORMULARIO", System.Data.CommandType.StoredProcedure);
        //    cmdInserirForm.Parameters.Add(new OracleParameter("pDS_FORMULARIO", OracleType.VarChar, 50));
        //    cmdInserirForm.Parameters.Add(new OracleParameter("pTP_PESSOA", OracleType.VarChar, 1));
        //    cmdInserirForm.Parameters.Add(new OracleParameter("pCD_STATUS", OracleType.VarChar, 1));
        //    cmdInserirForm.Parameters.Add(new OracleParameter("oID_FORMULARIO", OracleType.Number, 0) { Direction = System.Data.ParameterDirection.Output });

        //    cmdAtualizarForm = dbi.CreateCommand("PCK_GESTAO_SUITABILITY.ATUALIZAR_FORMULARIO", System.Data.CommandType.StoredProcedure);
        //    cmdAtualizarForm.Parameters.Add(new OracleParameter("pID_FORMULARIO", OracleType.Number, 0));
        //    cmdAtualizarForm.Parameters.Add(new OracleParameter("pDS_FORMULARIO", OracleType.VarChar, 50));
        //    cmdAtualizarForm.Parameters.Add(new OracleParameter("pTP_PESSOA", OracleType.VarChar, 1));
        //    cmdAtualizarForm.Parameters.Add(new OracleParameter("pCD_STATUS", OracleType.VarChar, 1));


        //    cmdExcluirForm = dbi.CreateCommand("PCK_GESTAO_SUITABILITY.EXCLUIR_FORMULARIO", System.Data.CommandType.StoredProcedure);
        //    cmdExcluirForm.Parameters.Add(new OracleParameter("pID_FORMULARIO", OracleType.Number, 0));

        //    cmdInserirQuestao = dbi.CreateCommand("PCK_GESTAO_SUITABILITY.INCLUIR_QUESTAO", System.Data.CommandType.StoredProcedure);
        //    cmdInserirQuestao.Parameters.Add(new OracleParameter("pID_FORMULARIO", OracleType.Number, 0));
        //    cmdInserirQuestao.Parameters.Add(new OracleParameter("pNU_QUESTAO", OracleType.Number, 0));
        //    cmdInserirQuestao.Parameters.Add(new OracleParameter("pNU_SUBQUESTAO", OracleType.Number, 0));
        //    cmdInserirQuestao.Parameters.Add(new OracleParameter("pCD_TIPO", OracleType.VarChar, 1));
        //    cmdInserirQuestao.Parameters.Add(new OracleParameter("pIN_OCULTA", OracleType.Number, 0));
        //    cmdInserirQuestao.Parameters.Add(new OracleParameter("pDS_ENUNCIADO", OracleType.VarChar, 255));
        //    cmdInserirQuestao.Parameters.Add(new OracleParameter("pDS_PERCENTUAL", OracleType.VarChar, 100));
        //    cmdInserirQuestao.Parameters.Add(new OracleParameter("pVL_PESO", OracleType.Number, 0));
        //    cmdInserirQuestao.Parameters.Add(new OracleParameter("pPC_MINIMO", OracleType.Number, 0));
        //    cmdInserirQuestao.Parameters.Add(new OracleParameter("pIN_QUESTAO_ATIVA", OracleType.VarChar, 1));
        //    cmdInserirQuestao.Parameters.Add(new OracleParameter("oID_QUESTAO", OracleType.Number, 0) { Direction = System.Data.ParameterDirection.Output });

        //    cmdAtualizarQuestao = dbi.CreateCommand("PCK_GESTAO_SUITABILITY.ATUALIZAR_QUESTAO", System.Data.CommandType.StoredProcedure);
        //    cmdAtualizarQuestao.Parameters.Add(new OracleParameter("pID_QUESTAO", OracleType.Number, 0));
        //    cmdAtualizarQuestao.Parameters.Add(new OracleParameter("pNU_QUESTAO", OracleType.Number, 0));
        //    cmdAtualizarQuestao.Parameters.Add(new OracleParameter("pNU_SUBQUESTAO", OracleType.Number, 0));
        //    cmdAtualizarQuestao.Parameters.Add(new OracleParameter("pCD_TIPO", OracleType.VarChar, 1));
        //    cmdAtualizarQuestao.Parameters.Add(new OracleParameter("pIN_OCULTA", OracleType.Number, 0));
        //    cmdAtualizarQuestao.Parameters.Add(new OracleParameter("pDS_ENUNCIADO", OracleType.VarChar, 255));
        //    cmdAtualizarQuestao.Parameters.Add(new OracleParameter("pDS_PERCENTUAL", OracleType.VarChar, 100));
        //    cmdAtualizarQuestao.Parameters.Add(new OracleParameter("pVL_PESO", OracleType.Number, 0));
        //    cmdAtualizarQuestao.Parameters.Add(new OracleParameter("pPC_MINIMO", OracleType.Number, 0));
        //    cmdAtualizarQuestao.Parameters.Add(new OracleParameter("pIN_QUESTAO_ATIVA", OracleType.VarChar, 1));

        //    cmdExcluirQuestao = dbi.CreateCommand("PCK_GESTAO_SUITABILITY.EXCLUIR_QUESTAO", System.Data.CommandType.StoredProcedure);
        //    cmdExcluirQuestao.Parameters.Add(new OracleParameter("pID_QUESTAO", OracleType.Number, 0));

        //    cmdInserirAlternativa = dbi.CreateCommand("PCK_GESTAO_SUITABILITY.INCLUIR_ALTERNATIVA", System.Data.CommandType.StoredProcedure);
        //    cmdInserirAlternativa.Parameters.Add(new OracleParameter("pID_QUESTAO", OracleType.Number, 0));
        //    cmdInserirAlternativa.Parameters.Add(new OracleParameter("pNU_ORDEM", OracleType.Number, 0));
        //    cmdInserirAlternativa.Parameters.Add(new OracleParameter("pDS_ALTERNATIVA", OracleType.VarChar, 255));
        //    cmdInserirAlternativa.Parameters.Add(new OracleParameter("pID_PROXIMA_QUESTAO", OracleType.Number, 0));
        //    cmdInserirAlternativa.Parameters.Add(new OracleParameter("pVL_PONTOS", OracleType.Number, 0));
        //    cmdInserirAlternativa.Parameters.Add(new OracleParameter("pIN_ATIVA", OracleType.VarChar, 1));
        //    cmdInserirAlternativa.Parameters.Add(new OracleParameter("oID_ALTERNATIVA", OracleType.Number, 0) { Direction = System.Data.ParameterDirection.Output });

        //    cmdAtualizarAlternativa = dbi.CreateCommand("PCK_GESTAO_SUITABILITY.ATUALIZAR_ALTERNATIVA", System.Data.CommandType.StoredProcedure);
        //    cmdAtualizarAlternativa.Parameters.Add(new OracleParameter("pID_ALTERNATIVA", OracleType.Number, 0));
        //    cmdAtualizarAlternativa.Parameters.Add(new OracleParameter("pNU_ORDEM", OracleType.Number, 0));
        //    cmdAtualizarAlternativa.Parameters.Add(new OracleParameter("pDS_ALTERNATIVA", OracleType.VarChar, 255));
        //    cmdAtualizarAlternativa.Parameters.Add(new OracleParameter("pID_PROXIMA_QUESTAO", OracleType.Number, 0));
        //    cmdAtualizarAlternativa.Parameters.Add(new OracleParameter("pVL_PONTOS", OracleType.Number, 0));
        //    cmdAtualizarAlternativa.Parameters.Add(new OracleParameter("pIN_ATIVA", OracleType.VarChar, 1));

        //    cmdExcluirAlternativa = dbi.CreateCommand("PCK_GESTAO_SUITABILITY.EXCLUIR_ALTERNATIVA", System.Data.CommandType.StoredProcedure);
        //    cmdExcluirAlternativa.Parameters.Add(new OracleParameter("pID_ALTERNATIVA", OracleType.Number, 0));

        //    cmdInserirMatriz = dbi.CreateCommand("PCK_GESTAO_SUITABILITY.INCLUIR_COLUNA", System.Data.CommandType.StoredProcedure);
        //    cmdInserirMatriz.Parameters.Add(new OracleParameter("pID_QUESTAO", OracleType.Number, 0));
        //    cmdInserirMatriz.Parameters.Add(new OracleParameter("pIN_ATIVA", OracleType.VarChar, 1));
        //    cmdInserirMatriz.Parameters.Add(new OracleParameter("pDS_COLUNA", OracleType.VarChar, 255));
        //    cmdInserirMatriz.Parameters.Add(new OracleParameter("pNU_ORDEM", OracleType.Number, 0));
        //    cmdInserirMatriz.Parameters.Add(new OracleParameter("oID_COLUNA", OracleType.Number, 0) { Direction = System.Data.ParameterDirection.Output });

        //    cmdAtualizarMatriz = dbi.CreateCommand("PCK_GESTAO_SUITABILITY.ATUALIZAR_COLUNA", System.Data.CommandType.StoredProcedure);
        //    cmdAtualizarMatriz.Parameters.Add(new OracleParameter("pID_COLUNA", OracleType.Number, 0));
        //    cmdAtualizarMatriz.Parameters.Add(new OracleParameter("pIN_ATIVA", OracleType.VarChar, 1));
        //    cmdAtualizarMatriz.Parameters.Add(new OracleParameter("pDS_COLUNA", OracleType.VarChar, 255));
        //    cmdAtualizarMatriz.Parameters.Add(new OracleParameter("pNU_ORDEM", OracleType.Number, 0));

        //    cmdExcluirMatriz = dbi.CreateCommand("PCK_GESTAO_SUITABILITY.EXCLUIR_COLUNA", System.Data.CommandType.StoredProcedure);
        //    cmdExcluirMatriz.Parameters.Add(new OracleParameter("pID_COLUNA", OracleType.Number, 0));

        //    cmdInserirPontuacao = dbi.CreateCommand("PCK_GESTAO_SUITABILITY.INCLUIR_PONTUACAO", System.Data.CommandType.StoredProcedure);
        //    cmdInserirPontuacao.Parameters.Add(new OracleParameter("pID_FORMULARIO", OracleType.Number, 0));
        //    cmdInserirPontuacao.Parameters.Add(new OracleParameter("pVL_PONTOS_MIN", OracleType.Number, 0));
        //    cmdInserirPontuacao.Parameters.Add(new OracleParameter("pVL_PONTOS_MAX", OracleType.Number, 0));
        //    cmdInserirPontuacao.Parameters.Add(new OracleParameter("pID_PERFIL", OracleType.Number, 0));
        //    cmdInserirPontuacao.Parameters.Add(new OracleParameter("oID_PONTUACAO", OracleType.Number, 0) { Direction = System.Data.ParameterDirection.Output });

        //    cmdAtualizarPontuacao = dbi.CreateCommand("PCK_GESTAO_SUITABILITY.ATUALIZAR_PONTUACAO", System.Data.CommandType.StoredProcedure);
        //    cmdAtualizarPontuacao.Parameters.Add(new OracleParameter("pID_PONTUACAO", OracleType.Number, 0));
        //    cmdAtualizarPontuacao.Parameters.Add(new OracleParameter("pVL_PONTOS_MIN", OracleType.Number, 0));
        //    cmdAtualizarPontuacao.Parameters.Add(new OracleParameter("pVL_PONTOS_MAX", OracleType.Number, 0));

        //}

        public void PrepararComandosCrud()
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");

            cmdInserirForm = dbiCRK.CreateCommand("PR_GESTAO_SUITABILITY_INCLUIR_FORMULARIO", System.Data.CommandType.StoredProcedure);
            cmdInserirForm.Parameters.Add(new SqlParameter("@pDS_FORMULARIO", SqlDbType.VarChar, 50));
            cmdInserirForm.Parameters.Add(new SqlParameter("@pTP_PESSOA", SqlDbType.VarChar, 1));
            cmdInserirForm.Parameters.Add(new SqlParameter("@pCD_STATUS", SqlDbType.VarChar, 1));
            //cmdInserirForm.Parameters.Add(new SqlParameter("@oID_FORMULARIO", SqlDbType.Int, 0) { Direction = System.Data.ParameterDirection.Output });

            cmdAtualizarForm = dbiCRK.CreateCommand("PR_GESTAO_SUITABILITY_ATUALIZAR_FORMULARIO", System.Data.CommandType.StoredProcedure);
            cmdAtualizarForm.Parameters.Add(new SqlParameter("@pID_FORMULARIO", SqlDbType.Int, 0));
            cmdAtualizarForm.Parameters.Add(new SqlParameter("@pDS_FORMULARIO", SqlDbType.VarChar, 50));
            cmdAtualizarForm.Parameters.Add(new SqlParameter("@pTP_PESSOA", SqlDbType.VarChar, 1));
            cmdAtualizarForm.Parameters.Add(new SqlParameter("@pCD_STATUS", SqlDbType.VarChar, 1));

            cmdExcluirForm = dbiCRK.CreateCommand("PR_GESTAO_SUITABILITY_EXCLUIR_FORMULARIO", System.Data.CommandType.StoredProcedure);
            cmdExcluirForm.Parameters.Add(new SqlParameter("@pID_FORMULARIO", SqlDbType.Int, 0));

            cmdInserirQuestao = dbiCRK.CreateCommand("PR_GESTAO_SUITABILITY_INCLUIR_QUESTAO", System.Data.CommandType.StoredProcedure);
            cmdInserirQuestao.Parameters.Add(new SqlParameter("@pID_FORMULARIO", SqlDbType.Decimal, 0));
            cmdInserirQuestao.Parameters.Add(new SqlParameter("@pNU_QUESTAO", SqlDbType.Decimal, 0));
            cmdInserirQuestao.Parameters.Add(new SqlParameter("@pNU_SUBQUESTAO", SqlDbType.Decimal, 0));
            cmdInserirQuestao.Parameters.Add(new SqlParameter("@pCD_TIPO", SqlDbType.VarChar, 1));
            cmdInserirQuestao.Parameters.Add(new SqlParameter("@pIN_OCULTA", SqlDbType.Decimal, 0));
            cmdInserirQuestao.Parameters.Add(new SqlParameter("@pDS_ENUNCIADO", SqlDbType.VarChar, 255));
            cmdInserirQuestao.Parameters.Add(new SqlParameter("@pDS_PERCENTUAL", SqlDbType.VarChar, 100));
            cmdInserirQuestao.Parameters.Add(new SqlParameter("@pVL_PESO", SqlDbType.Decimal, 0));
            cmdInserirQuestao.Parameters.Add(new SqlParameter("@pPC_MINIMO", SqlDbType.Decimal, 0));
            cmdInserirQuestao.Parameters.Add(new SqlParameter("@pIN_QUESTAO_ATIVA", SqlDbType.VarChar, 1));
            cmdInserirQuestao.Parameters.Add(new SqlParameter("@oID_QUESTAO", SqlDbType.Int, 0) { Direction = System.Data.ParameterDirection.Output });

            cmdAtualizarQuestao = dbiCRK.CreateCommand("PR_GESTAO_SUITABILITY_ATUALIZAR_QUESTAO", System.Data.CommandType.StoredProcedure);
            cmdAtualizarQuestao.Parameters.Add(new SqlParameter("@pID_QUESTAO", SqlDbType.Decimal, 0));
            cmdAtualizarQuestao.Parameters.Add(new SqlParameter("@pNU_QUESTAO", SqlDbType.Decimal, 0));
            cmdAtualizarQuestao.Parameters.Add(new SqlParameter("@pNU_SUBQUESTAO", SqlDbType.Decimal, 0));
            cmdAtualizarQuestao.Parameters.Add(new SqlParameter("@pCD_TIPO", SqlDbType.VarChar, 1));
            cmdAtualizarQuestao.Parameters.Add(new SqlParameter("@pIN_OCULTA", SqlDbType.Decimal, 0));
            cmdAtualizarQuestao.Parameters.Add(new SqlParameter("@pDS_ENUNCIADO", SqlDbType.VarChar, 255));
            cmdAtualizarQuestao.Parameters.Add(new SqlParameter("@pDS_PERCENTUAL", SqlDbType.VarChar, 100));
            cmdAtualizarQuestao.Parameters.Add(new SqlParameter("@pVL_PESO", SqlDbType.Decimal, 0));
            cmdAtualizarQuestao.Parameters.Add(new SqlParameter("@pPC_MINIMO", SqlDbType.Decimal, 0));
            cmdAtualizarQuestao.Parameters.Add(new SqlParameter("@pIN_QUESTAO_ATIVA", SqlDbType.VarChar, 1));

            cmdExcluirQuestao = dbiCRK.CreateCommand("PR_GESTAO_SUITABILITY_EXCLUIR_QUESTAO", System.Data.CommandType.StoredProcedure);
            cmdExcluirQuestao.Parameters.Add(new SqlParameter("@pID_QUESTAO", SqlDbType.Decimal, 0));

            cmdInserirAlternativa = dbiCRK.CreateCommand("PR_GESTAO_SUITABILITY_INCLUIR_ALTERNATIVA", System.Data.CommandType.StoredProcedure);
            cmdInserirAlternativa.Parameters.Add(new SqlParameter("@pID_QUESTAO", SqlDbType.Decimal, 0));
            cmdInserirAlternativa.Parameters.Add(new SqlParameter("@pNU_ORDEM", SqlDbType.Decimal, 0));
            cmdInserirAlternativa.Parameters.Add(new SqlParameter("@pDS_ALTERNATIVA", SqlDbType.VarChar, 255));
            cmdInserirAlternativa.Parameters.Add(new SqlParameter("@pID_PROXIMA_QUESTAO", SqlDbType.Decimal, 0));
            cmdInserirAlternativa.Parameters.Add(new SqlParameter("@pVL_PONTOS", SqlDbType.Decimal, 0));
            cmdInserirAlternativa.Parameters.Add(new SqlParameter("@pIN_ATIVA", SqlDbType.VarChar, 1));
            cmdInserirAlternativa.Parameters.Add(new SqlParameter("@oID_ALTERNATIVA", SqlDbType.Int, 0) { Direction = System.Data.ParameterDirection.Output });

            cmdAtualizarAlternativa = dbiCRK.CreateCommand("PR_GESTAO_SUITABILITY_ATUALIZAR_ALTERNATIVA", System.Data.CommandType.StoredProcedure);
            cmdAtualizarAlternativa.Parameters.Add(new SqlParameter("@pID_ALTERNATIVA", SqlDbType.Decimal, 0));
            cmdAtualizarAlternativa.Parameters.Add(new SqlParameter("@pNU_ORDEM", SqlDbType.Decimal, 0));
            cmdAtualizarAlternativa.Parameters.Add(new SqlParameter("@pDS_ALTERNATIVA", SqlDbType.VarChar, 255));
            cmdAtualizarAlternativa.Parameters.Add(new SqlParameter("@pID_PROXIMA_QUESTAO", SqlDbType.Decimal, 0));
            cmdAtualizarAlternativa.Parameters.Add(new SqlParameter("@pVL_PONTOS", SqlDbType.Decimal, 0));
            cmdAtualizarAlternativa.Parameters.Add(new SqlParameter("@pIN_ATIVA", SqlDbType.VarChar, 1));

            cmdExcluirAlternativa = dbiCRK.CreateCommand("PR_GESTAO_SUITABILITY_EXCLUIR_ALTERNATIVA", System.Data.CommandType.StoredProcedure);
            cmdExcluirAlternativa.Parameters.Add(new SqlParameter("@pID_ALTERNATIVA", SqlDbType.Decimal, 0));

            cmdInserirMatriz = dbiCRK.CreateCommand("PR_GESTAO_SUITABILITY_INCLUIR_COLUNA", System.Data.CommandType.StoredProcedure);
            cmdInserirMatriz.Parameters.Add(new SqlParameter("@pID_QUESTAO", SqlDbType.Decimal, 0));
            cmdInserirMatriz.Parameters.Add(new SqlParameter("@pIN_ATIVA", SqlDbType.VarChar, 1));
            cmdInserirMatriz.Parameters.Add(new SqlParameter("@pDS_COLUNA", SqlDbType.VarChar, 255));
            cmdInserirMatriz.Parameters.Add(new SqlParameter("@pNU_ORDEM", SqlDbType.Decimal, 0));
            cmdInserirMatriz.Parameters.Add(new SqlParameter("@oID_COLUNA", SqlDbType.Int, 0) { Direction = System.Data.ParameterDirection.Output });

            cmdAtualizarMatriz = dbiCRK.CreateCommand("PR_GESTAO_SUITABILITY_ATUALIZAR_COLUNA", System.Data.CommandType.StoredProcedure);
            cmdAtualizarMatriz.Parameters.Add(new SqlParameter("@pID_COLUNA", SqlDbType.Decimal, 0));
            cmdAtualizarMatriz.Parameters.Add(new SqlParameter("@pIN_ATIVA", SqlDbType.VarChar, 1));
            cmdAtualizarMatriz.Parameters.Add(new SqlParameter("@pDS_COLUNA", SqlDbType.VarChar, 255));
            cmdAtualizarMatriz.Parameters.Add(new SqlParameter("@pNU_ORDEM", SqlDbType.Decimal, 0));

            cmdExcluirMatriz = dbiCRK.CreateCommand("PR_GESTAO_SUITABILITY_EXCLUIR_COLUNA", System.Data.CommandType.StoredProcedure);
            cmdExcluirMatriz.Parameters.Add(new SqlParameter("@pID_COLUNA", SqlDbType.Decimal, 0));

            cmdInserirPontuacao = dbiCRK.CreateCommand("PR_GESTAO_SUITABILITY_INCLUIR_PONTUACAO", System.Data.CommandType.StoredProcedure);
            cmdInserirPontuacao.Parameters.Add(new SqlParameter("@pID_FORMULARIO", SqlDbType.Decimal, 0));
            cmdInserirPontuacao.Parameters.Add(new SqlParameter("@pVL_PONTOS_MIN", SqlDbType.Decimal, 0));
            cmdInserirPontuacao.Parameters.Add(new SqlParameter("@pVL_PONTOS_MAX", SqlDbType.Decimal, 0));
            cmdInserirPontuacao.Parameters.Add(new SqlParameter("@pID_PERFIL", SqlDbType.Decimal, 0));
            cmdInserirPontuacao.Parameters.Add(new SqlParameter("@oID_PONTUACAO", SqlDbType.Int, 0) { Direction = System.Data.ParameterDirection.Output });

            cmdAtualizarPontuacao = dbiCRK.CreateCommand("PR_GESTAO_SUITABILITY_ATUALIZAR_PONTUACAO", System.Data.CommandType.StoredProcedure);
            cmdAtualizarPontuacao.Parameters.Add(new SqlParameter("@pID_PONTUACAO", SqlDbType.Int, 0));
            cmdAtualizarPontuacao.Parameters.Add(new SqlParameter("@pVL_PONTOS_MIN", SqlDbType.Decimal, 0));
            cmdAtualizarPontuacao.Parameters.Add(new SqlParameter("@pVL_PONTOS_MAX", SqlDbType.Decimal, 0));

        }

        //private FormularioSuitability LerFormulario(DbDataReader dr)
        //{
        //    FormularioSuitability form = new FormularioSuitability();
        //    form.Id = (int)(decimal)dr["ID_FORMULARIO"];
        //    form.Descricao = (string)dr["DS_FORMULARIO"];
        //    form.Tipo = dr["TP_PESSOA"] as string;
        //    form.Status = (string)dr["CD_STATUS"];
        //    form.DescricaoStatus = (string)dr["DS_STATUS"];
        //    return form;
        //}

        private FormularioSuitability LerFormulario(DbDataReader dr)
        {
            FormularioSuitability form = new FormularioSuitability();
            form.Id = (int)dr["ID_FORMULARIO"];
            form.Descricao = (string)dr["DS_FORMULARIO"];
            form.Tipo = dr["TP_PESSOA"] as string;
            form.Status = (string)dr["CD_STATUS"];
            form.DescricaoStatus = (string)dr["DS_STATUS"];
            return form;
        }

        //public List<FormularioSuitability> ListarFormularios(string tipo, string status)
        //{
        //    using (var cmd = dbi.CreateCommand("PCK_GESTAO_SUITABILITY.LISTAR_FORMULARIOS", System.Data.CommandType.StoredProcedure))
        //    {
        //        cmd.Parameters.Add(new OracleParameter("pTP_PESSOA", OracleType.VarChar, string.IsNullOrEmpty(tipo) ? 0 : tipo.Length) { Value = (object)tipo ?? DBNull.Value });
        //        cmd.Parameters.Add(new OracleParameter("pCD_STATUS", OracleType.VarChar, string.IsNullOrEmpty(status) ? 0 : status.Length) { Value = (object)status ?? DBNull.Value });
        //        cmd.CreateOracleCursor("pDADOS");


        //        using (var dr = cmd.ExecuteReader())
        //        {
        //            List<FormularioSuitability> list = new List<FormularioSuitability>();
        //            while (dr.Read())
        //            {
        //                var form = LerFormulario(dr);
        //                list.Add(form);
        //            }
        //            return list;
        //        }
        //    }
        //}

        public List<FormularioSuitability> ListarFormularios(string tipo, string status)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");
            using (var cmd = (SqlCommand)dbiCRK.CreateCommand("PR_GESTAO_SUITABILITY_LISTAR_FORMULARIOS", CommandType.StoredProcedure))
            {
                cmd.Parameters.Add(new SqlParameter("@pTP_PESSOA", string.IsNullOrEmpty(tipo) ? (object)DBNull.Value : tipo));
                cmd.Parameters.Add(new SqlParameter("@pCD_STATUS", string.IsNullOrEmpty(status) ? (object)DBNull.Value : status));

                cmd.Connection.Open();

                using (var dr = cmd.ExecuteReader())
                {
                    List<FormularioSuitability> list = new List<FormularioSuitability>();
                    while (dr.Read())
                    {
                        var form = LerFormulario(dr);
                        list.Add(form);
                    }
                    return list;
                }
            }
        }

        //public EdicaoSuitability AbrirFormulario(int id)
        //{
        //    using (var cmd = dbi.CreateCommand("PCK_GESTAO_SUITABILITY.ABRIR_FORMULARIO", System.Data.CommandType.StoredProcedure))
        //    {
        //        cmd.Parameters.Add(new OracleParameter("pID_FORMULARIO", OracleType.Int32) { Value = id });

        //        cmd.CreateOracleCursor("pFORMULARIO");
        //        cmd.CreateOracleCursor("pPERGUNTAS");
        //        cmd.CreateOracleCursor("pRESPOSTAS");
        //        cmd.CreateOracleCursor("pMATRIZES");
        //        cmd.CreateOracleCursor("pPONTUACAO");
        //        cmd.CreateOracleCursor("pPERFIS");

        //        cmd.Parameters.Add(new OracleParameter("pQTD_INVESTIDORES", OracleType.Int32) { Direction = System.Data.ParameterDirection.Output });


        //        using (var dr = cmd.ExecuteReader())
        //        {

        //            EdicaoSuitability edicao = new EdicaoSuitability();


        //            if (dr.Read())
        //            {
        //                edicao.Formulario = LerFormulario(dr);
        //                edicao.QuantidadePreenchimentos = Convert.ToInt32(cmd.Parameters["pQTD_INVESTIDORES"].Value);
        //            }

        //            if (dr.NextResult())
        //            {
        //                edicao.Questoes = new List<QuestaoSuitability>();
        //                while (dr.Read())
        //                {
        //                    QuestaoSuitability q = new QuestaoSuitability();
        //                    q.Id = (int)(decimal)dr["id"];
        //                    q.UID = Guid.NewGuid();
        //                    q.Ordem = (int)(decimal)dr["idquestion"];
        //                    q.NumeroSubQuestao = (int?)(dr["idsubquestion"] as decimal?);
        //                    q.Enunciado = (string)dr["question"];
        //                    q.QuestaoAtiva = dr["status"].Equals("A");
        //                    q.Peso = (int)(decimal)dr["vl_peso"];
        //                    q.Tipo = (string)dr["cd_tipo"];
        //                    q.PercentualMinimo = dr["pc_minimo"] as decimal?;
        //                    q.DescricaoPercentual = dr["ds_percentual"] as string;

        //                    q.Oculta = dr["IN_OCULTA"].Equals(1.0M);

        //                    q.Alternativas = new List<Alternativa>();
        //                    q.Colunas = new List<ColunaMatriz>();
        //                    edicao.Questoes.Add(q);
        //                }
        //            }

        //            if (dr.NextResult())
        //            {
        //                while (dr.Read())
        //                {
        //                    int idQuestao = (int)(decimal)dr["idquestion"];
        //                    var a = new Alternativa();
        //                    a.Id = (int)(decimal)dr["Id"];
        //                    a.Texto = (string)dr["answer"];
        //                    a.Ativa = dr["status"].Equals("A");
        //                    a.Pontos = (decimal)dr["Point"];
        //                    a.Ordem = (int)(decimal)dr["idanswer"];
        //                    a.IdProximaQuestao = (int?)(dr["idsubquestion"] as decimal?);

        //                    var questao = edicao.Questoes.Find(q => q.Id == idQuestao);
        //                    if (questao != null)
        //                    {
        //                        questao.Alternativas.Add(a);
        //                    }

        //                    if (a.IdProximaQuestao != null)
        //                    {
        //                        questao = edicao.Questoes.Find(q => q.Id == a.IdProximaQuestao.Value);
        //                        if (questao != null)
        //                            a.UIDProximaQuestao = questao.UID;
        //                    }

        //                }
        //            }

        //            if (dr.NextResult())
        //            {
        //                while (dr.Read())
        //                {
        //                    var coluna = new ColunaMatriz();
        //                    int idQuestao = (int)(decimal)dr["idQuestion"];

        //                    coluna.Id = (int)(decimal)dr["ID_COLUNA"];
        //                    coluna.Ordem = (int)(decimal)dr["nu_ordem"];
        //                    coluna.Descricao = (string)dr["ds_coluna"];
        //                    coluna.Ativa = ((decimal)dr["in_ativo"]) == 1.0M;

        //                    var questao = edicao.Questoes.Find(q => q.Id == idQuestao);

        //                    if (questao != null)
        //                    {
        //                        questao.Colunas.Add(coluna);
        //                    }

        //                }
        //            }

        //            if (dr.NextResult())
        //            {
        //                edicao.Pontuacao = new List<PontuacaoFormulario>();
        //                while (dr.Read())
        //                {
        //                    PontuacaoFormulario p = new PontuacaoFormulario();
        //                    p.Id = (int?)(dr["id"] as decimal?);
        //                    p.Minimo = (dr["VL_PONTOS_MIN"] as decimal?);
        //                    p.Maximo = (dr["VL_PONTOS_MAX"] as decimal?);
        //                    p.IdPerfil = (int)(decimal)dr["ID_TYPE"];

        //                    edicao.Pontuacao.Add(p);

        //                }
        //            }

        //            if (dr.NextResult())
        //            {
        //                edicao.Perfis = new List<PerfilSuitability>();
        //                while (dr.Read())
        //                {
        //                    PerfilSuitability p = new PerfilSuitability();
        //                    p.Id = (int)(decimal)dr["ID"];
        //                    p.Nome = (string)dr["description"];

        //                    edicao.Perfis.Add(p);
        //                }
        //            }

        //            return edicao;
        //        }
        //    }
        //}

        public EdicaoSuitability AbrirFormulario(int id)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");
            using (var cmd = (SqlCommand)dbiCRK.CreateCommand("PR_GESTAO_SUITABILITY_ABRIR_FORMULARIO", CommandType.StoredProcedure))
            {
                cmd.Parameters.Add("@pID_FORMULARIO", SqlDbType.Int).Value = Convert.ToInt32(id);

                cmd.Parameters.Add("@pQTD_INVESTIDORES", SqlDbType.Int).Direction = ParameterDirection.Output;

                cmd.Connection.Open();

                using (var dr = cmd.ExecuteReader())
                {

                    EdicaoSuitability edicao = new EdicaoSuitability();


                    if (dr.Read())
                    {
                        edicao.Formulario = LerFormulario(dr);
                        edicao.QuantidadePreenchimentos = Convert.ToInt32(cmd.Parameters["@pQTD_INVESTIDORES"].Value);
                    }

                    if (dr.NextResult())
                    {
                        edicao.Questoes = new List<QuestaoSuitability>();
                        while (dr.Read())
                        {
                            QuestaoSuitability q = new QuestaoSuitability();
                            q.Id = (int)dr["id"];
                            q.UID = Guid.NewGuid();
                            q.Ordem = (int)(decimal)dr["idquestion"];
                            q.NumeroSubQuestao = (int?)(dr["idsubquestion"] as decimal?);
                            q.Enunciado = (string)dr["question"];
                            q.QuestaoAtiva = dr["status"].Equals("A");
                            q.Peso = (int)(decimal)dr["vl_peso"];
                            q.Tipo = (string)dr["cd_tipo"];
                            q.PercentualMinimo = dr["pc_minimo"] as decimal?;
                            q.DescricaoPercentual = dr["ds_percentual"] as string;

                            q.Oculta = dr["IN_OCULTA"].Equals(1.0M);

                            q.Alternativas = new List<Alternativa>();
                            q.Colunas = new List<ColunaMatriz>();
                            edicao.Questoes.Add(q);
                        }
                    }

                    if (dr.NextResult())
                    {
                        while (dr.Read())
                        {
                            int idQuestao = (int)(decimal)dr["idquestion"];
                            var a = new Alternativa();
                            a.Id = (int)dr["Id"];
                            a.Texto = (string)dr["answer"];
                            a.Ativa = dr["status"].Equals("A");
                            a.Pontos = (decimal)dr["Point"];
                            a.Ordem = (int)(decimal)dr["idanswer"];
                            a.IdProximaQuestao = (int?)(dr["idsubquestion"] as decimal?);

                            var questao = edicao.Questoes.Find(q => q.Id == idQuestao);
                            if (questao != null)
                            {
                                questao.Alternativas.Add(a);
                            }

                            if (a.IdProximaQuestao != null)
                            {
                                questao = edicao.Questoes.Find(q => q.Id == a.IdProximaQuestao.Value);
                                if (questao != null)
                                    a.UIDProximaQuestao = questao.UID;
                            }

                        }
                    }

                    if (dr.NextResult())
                    {
                        while (dr.Read())
                        {
                            var coluna = new ColunaMatriz();
                            int idQuestao = (int)(decimal)dr["idQuestion"];

                            coluna.Id = (int)dr["ID_COLUNA"];
                            coluna.Ordem = (int)(decimal)dr["nu_ordem"];
                            coluna.Descricao = (string)dr["ds_coluna"];
                            coluna.Ativa = ((decimal)dr["in_ativo"]) == 1.0M;

                            var questao = edicao.Questoes.Find(q => q.Id == idQuestao);

                            if (questao != null)
                            {
                                questao.Colunas.Add(coluna);
                            }

                        }
                    }

                    if (dr.NextResult())
                    {
                        edicao.Pontuacao = new List<PontuacaoFormulario>();
                        while (dr.Read())
                        {
                            PontuacaoFormulario p = new PontuacaoFormulario();
                            p.Id = (dr["id"] as int?);
                            p.Minimo = (dr["VL_PONTOS_MIN"] as decimal?);
                            p.Maximo = (dr["VL_PONTOS_MAX"] as decimal?);
                            p.IdPerfil = (int)dr["ID_TYPE"];

                            edicao.Pontuacao.Add(p);

                        }
                    }

                    if (dr.NextResult())
                    {
                        edicao.Perfis = new List<PerfilSuitability>();
                        while (dr.Read())
                        {
                            PerfilSuitability p = new PerfilSuitability();
                            p.Id = (int)dr["ID"];
                            p.Nome = (string)dr["description"];

                            edicao.Perfis.Add(p);
                        }
                    }

                    return edicao;
                }
            }
        }

        //public int InserirFormulario(FormularioSuitability form)
        //{
        //    cmdInserirForm.Parameters["pDS_FORMULARIO"].Value = form.Descricao;
        //    cmdInserirForm.Parameters["pTP_PESSOA"].Value = (object)form.Tipo ?? DBNull.Value;
        //    cmdInserirForm.Parameters["pCD_STATUS"].Value = form.Status;

        //    cmdInserirForm.ExecuteNonQuery();

        //    int id = (int)(decimal)cmdInserirForm.Parameters["oID_FORMULARIO"].Value;

        //    return id;
        //}

        public int InserirFormulario(FormularioSuitability form)
        {
            cmdInserirForm.Parameters["@pDS_FORMULARIO"].Value = form.Descricao;
            cmdInserirForm.Parameters["@pTP_PESSOA"].Value = string.IsNullOrEmpty(form.Tipo) ? (object)DBNull.Value : form.Tipo;
            cmdInserirForm.Parameters["@pCD_STATUS"].Value = form.Status;

            cmdInserirForm.Connection.Open();

            int id = (int)cmdInserirForm.ExecuteScalar();

            cmdInserirForm.Connection.Close();

            return id;
        }

        //public void AtualizarFormulario(FormularioSuitability form)
        //{
        //    cmdAtualizarForm.Parameters["pID_FORMULARIO"].Value = form.Id;
        //    cmdAtualizarForm.Parameters["pDS_FORMULARIO"].Value = form.Descricao;
        //    cmdAtualizarForm.Parameters["pTP_PESSOA"].Value = (object)form.Tipo ?? DBNull.Value;
        //    cmdAtualizarForm.Parameters["pCD_STATUS"].Value = form.Status;

        //    cmdAtualizarForm.ExecuteNonQuery();
        //}

        public void AtualizarFormulario(FormularioSuitability form)
        {
            cmdAtualizarForm.Parameters["@pID_FORMULARIO"].Value = form.Id;
            cmdAtualizarForm.Parameters["@pDS_FORMULARIO"].Value = form.Descricao;
            cmdAtualizarForm.Parameters["@pTP_PESSOA"].Value = string.IsNullOrEmpty(form.Tipo) ? (object)DBNull.Value : form.Tipo;
            cmdAtualizarForm.Parameters["@pCD_STATUS"].Value = form.Status;



            cmdAtualizarForm.Connection.Open();

            cmdAtualizarForm.ExecuteNonQuery();

            cmdAtualizarForm.Connection.Close();
        }

        //public void ExcluirFormulario(int id)
        //{
        //    cmdExcluirForm.Parameters["pID_FORMULARIO"].Value = id;
        //    cmdExcluirForm.ExecuteNonQuery();
        //}

        public void ExcluirFormulario(int id)
        {
            cmdExcluirForm.Parameters["@pID_FORMULARIO"].Value = id;

            cmdExcluirForm.Connection.Open();

            cmdExcluirForm.ExecuteNonQuery();

            cmdExcluirForm.Connection.Close();
        }

        //public int InserirQuestao(int idFormulario, QuestaoSuitability q)
        //{
        //    cmdInserirQuestao.Parameters["pID_FORMULARIO"].Value = idFormulario;
        //    cmdInserirQuestao.Parameters["pNU_QUESTAO"].Value = q.Ordem;
        //    cmdInserirQuestao.Parameters["pNU_SUBQUESTAO"].Value = (object)q.NumeroSubQuestao ?? DBNull.Value;
        //    cmdInserirQuestao.Parameters["pCD_TIPO"].Value = q.Tipo;
        //    cmdInserirQuestao.Parameters["pIN_OCULTA"].Value = q.Oculta ? 1 : 0;
        //    cmdInserirQuestao.Parameters["pDS_ENUNCIADO"].Value = q.Enunciado;
        //    cmdInserirQuestao.Parameters["pDS_PERCENTUAL"].Value = (object)q.DescricaoPercentual ?? DBNull.Value;
        //    cmdInserirQuestao.Parameters["pVL_PESO"].Value = q.Peso;
        //    cmdInserirQuestao.Parameters["pPC_MINIMO"].Value = (object)q.PercentualMinimo ?? DBNull.Value;
        //    cmdInserirQuestao.Parameters["pIN_QUESTAO_ATIVA"].Value = q.QuestaoAtiva ? "A" : "I";

        //    cmdInserirQuestao.ExecuteNonQuery();
        //    int id = (int)(decimal)cmdInserirQuestao.Parameters["oID_QUESTAO"].Value;

        //    return id;
        //}

        public int InserirQuestao(int idFormulario, QuestaoSuitability q)
        {
            cmdInserirQuestao.Parameters["@pID_FORMULARIO"].Value = idFormulario;
            cmdInserirQuestao.Parameters["@pNU_QUESTAO"].Value = q.Ordem;
            cmdInserirQuestao.Parameters["@pNU_SUBQUESTAO"].Value = (object)q.NumeroSubQuestao ?? DBNull.Value;
            cmdInserirQuestao.Parameters["@pCD_TIPO"].Value = q.Tipo;
            cmdInserirQuestao.Parameters["@pIN_OCULTA"].Value = q.Oculta ? 1 : 0;
            cmdInserirQuestao.Parameters["@pDS_ENUNCIADO"].Value = q.Enunciado;
            cmdInserirQuestao.Parameters["@pDS_PERCENTUAL"].Value = (object)q.DescricaoPercentual ?? DBNull.Value;
            cmdInserirQuestao.Parameters["@pVL_PESO"].Value = q.Peso;
            cmdInserirQuestao.Parameters["@pPC_MINIMO"].Value = (object)q.PercentualMinimo ?? DBNull.Value;
            cmdInserirQuestao.Parameters["@pIN_QUESTAO_ATIVA"].Value = q.QuestaoAtiva ? "A" : "I";

            cmdInserirQuestao.Connection.Open();

            cmdInserirQuestao.ExecuteNonQuery();
            int id = (int)cmdInserirQuestao.Parameters["@oID_QUESTAO"].Value;

            cmdExcluirForm.Connection.Close();

            return id;
        }

        //public void AtualizarQuestao(QuestaoSuitability q)
        //{
        //    cmdAtualizarQuestao.Parameters["pID_QUESTAO"].Value = q.Id;
        //    cmdAtualizarQuestao.Parameters["pNU_QUESTAO"].Value = q.Ordem;
        //    cmdAtualizarQuestao.Parameters["pNU_SUBQUESTAO"].Value = (object)q.NumeroSubQuestao ?? DBNull.Value;
        //    cmdAtualizarQuestao.Parameters["pCD_TIPO"].Value = q.Tipo;
        //    cmdAtualizarQuestao.Parameters["pIN_OCULTA"].Value = q.Oculta ? 1 : 0;
        //    cmdAtualizarQuestao.Parameters["pDS_ENUNCIADO"].Value = q.Enunciado;
        //    cmdAtualizarQuestao.Parameters["pDS_PERCENTUAL"].Value = (object)q.DescricaoPercentual ?? DBNull.Value;
        //    cmdAtualizarQuestao.Parameters["pVL_PESO"].Value = q.Peso;
        //    cmdAtualizarQuestao.Parameters["pPC_MINIMO"].Value = (object)q.PercentualMinimo ?? DBNull.Value;
        //    cmdAtualizarQuestao.Parameters["pIN_QUESTAO_ATIVA"].Value = q.QuestaoAtiva ? "A" : "I";

        //    cmdAtualizarQuestao.ExecuteNonQuery();

        //}

        public void AtualizarQuestao(QuestaoSuitability q)
        {
            cmdAtualizarQuestao.Parameters["@pID_QUESTAO"].Value = q.Id;
            cmdAtualizarQuestao.Parameters["@pNU_QUESTAO"].Value = q.Ordem;
            cmdAtualizarQuestao.Parameters["@pNU_SUBQUESTAO"].Value = (object)q.NumeroSubQuestao ?? DBNull.Value;
            cmdAtualizarQuestao.Parameters["@pCD_TIPO"].Value = q.Tipo;
            cmdAtualizarQuestao.Parameters["@pIN_OCULTA"].Value = q.Oculta ? 1 : 0;
            cmdAtualizarQuestao.Parameters["@pDS_ENUNCIADO"].Value = q.Enunciado;
            cmdAtualizarQuestao.Parameters["@pDS_PERCENTUAL"].Value = (object)q.DescricaoPercentual ?? DBNull.Value;
            cmdAtualizarQuestao.Parameters["@pVL_PESO"].Value = q.Peso;
            cmdAtualizarQuestao.Parameters["@pPC_MINIMO"].Value = (object)q.PercentualMinimo ?? DBNull.Value;
            cmdAtualizarQuestao.Parameters["@pIN_QUESTAO_ATIVA"].Value = q.QuestaoAtiva ? "A" : "I";

            cmdAtualizarQuestao.Connection.Open();

            cmdAtualizarQuestao.ExecuteNonQuery();

            cmdAtualizarQuestao.Connection.Close();
        }

        //public void ExcluirQuestao(int id)
        //{
        //    cmdExcluirQuestao.Parameters["pID_QUESTAO"].Value = id;
        //    cmdExcluirQuestao.ExecuteNonQuery();

        //}

        public void ExcluirQuestao(int id)
        {
            cmdExcluirQuestao.Parameters["@pID_QUESTAO"].Value = id;

            cmdExcluirQuestao.Connection.Open();
            cmdExcluirQuestao.ExecuteNonQuery();
            cmdExcluirQuestao.Connection.Close();

        }

        //public int InserirAlternativa(int idQuestao, Alternativa a)
        //{
        //    cmdInserirAlternativa.Parameters["pID_QUESTAO"].Value = idQuestao;
        //    cmdInserirAlternativa.Parameters["pNU_ORDEM"].Value = a.Ordem;
        //    cmdInserirAlternativa.Parameters["pDS_ALTERNATIVA"].Value = a.Texto;
        //    cmdInserirAlternativa.Parameters["pID_PROXIMA_QUESTAO"].Value = (object)a.IdProximaQuestao ?? DBNull.Value;
        //    cmdInserirAlternativa.Parameters["pVL_PONTOS"].Value = a.Pontos;
        //    cmdInserirAlternativa.Parameters["pIN_ATIVA"].Value = a.Ativa ? "A" : "I";

        //    cmdInserirAlternativa.ExecuteNonQuery();
        //    int id = (int)(decimal)cmdInserirAlternativa.Parameters["oID_ALTERNATIVA"].Value;
        //    return id;
        //}

        public int InserirAlternativa(int idQuestao, Alternativa a)
        {
            cmdInserirAlternativa.Parameters["@pID_QUESTAO"].Value = idQuestao;
            cmdInserirAlternativa.Parameters["@pNU_ORDEM"].Value = a.Ordem;
            cmdInserirAlternativa.Parameters["@pDS_ALTERNATIVA"].Value = a.Texto;
            cmdInserirAlternativa.Parameters["@pID_PROXIMA_QUESTAO"].Value = (object)a.IdProximaQuestao ?? DBNull.Value;
            cmdInserirAlternativa.Parameters["@pVL_PONTOS"].Value = a.Pontos;
            cmdInserirAlternativa.Parameters["@pIN_ATIVA"].Value = a.Ativa ? "A" : "I";

            cmdInserirAlternativa.Connection.Open();

            cmdInserirAlternativa.ExecuteNonQuery();
            int id = (int)cmdInserirAlternativa.Parameters["@oID_ALTERNATIVA"].Value;

            cmdInserirAlternativa.Connection.Close();
            return id;
        }

        //public void AtualizarAlternativa(Alternativa a)
        //{
        //    cmdAtualizarAlternativa.Parameters["pID_ALTERNATIVA"].Value = a.Id;
        //    cmdAtualizarAlternativa.Parameters["pNU_ORDEM"].Value = a.Ordem;
        //    cmdAtualizarAlternativa.Parameters["pDS_ALTERNATIVA"].Value = a.Texto;
        //    cmdAtualizarAlternativa.Parameters["pID_PROXIMA_QUESTAO"].Value = (object)a.IdProximaQuestao ?? DBNull.Value;
        //    cmdAtualizarAlternativa.Parameters["pVL_PONTOS"].Value = a.Pontos;
        //    cmdAtualizarAlternativa.Parameters["pIN_ATIVA"].Value = a.Ativa ? "A" : "I";

        //    cmdAtualizarAlternativa.ExecuteNonQuery();
        //}

        public void AtualizarAlternativa(Alternativa a)
        {
            cmdAtualizarAlternativa.Parameters["@pID_ALTERNATIVA"].Value = a.Id;
            cmdAtualizarAlternativa.Parameters["@pNU_ORDEM"].Value = a.Ordem;
            cmdAtualizarAlternativa.Parameters["@pDS_ALTERNATIVA"].Value = a.Texto;
            cmdAtualizarAlternativa.Parameters["@pID_PROXIMA_QUESTAO"].Value = (object)a.IdProximaQuestao ?? DBNull.Value;
            cmdAtualizarAlternativa.Parameters["@pVL_PONTOS"].Value = a.Pontos;
            cmdAtualizarAlternativa.Parameters["@pIN_ATIVA"].Value = a.Ativa ? "A" : "I";

            cmdAtualizarAlternativa.Connection.Open();

            cmdAtualizarAlternativa.ExecuteNonQuery();

            cmdAtualizarAlternativa.Connection.Close();

        }

        //public void ExcluirAlternativa(int id)
        //{
        //    cmdExcluirAlternativa.Parameters["pID_ALTERNATIVA"].Value = id;
        //    cmdExcluirAlternativa.ExecuteNonQuery();
        //}

        public void ExcluirAlternativa(int id)
        {
            cmdExcluirAlternativa.Parameters["@pID_ALTERNATIVA"].Value = id;

            cmdExcluirAlternativa.Connection.Open();

            cmdExcluirAlternativa.ExecuteNonQuery();

            cmdExcluirAlternativa.Connection.Close();
        }

        //public int InserirColuna(int idQuestao, ColunaMatriz m)
        //{
        //    cmdInserirMatriz.Parameters["pID_QUESTAO"].Value = idQuestao;
        //    cmdInserirMatriz.Parameters["pIN_ATIVA"].Value = m.Ativa ? 1 : 0;
        //    cmdInserirMatriz.Parameters["pDS_COLUNA"].Value = m.Descricao;
        //    cmdInserirMatriz.Parameters["pNU_ORDEM"].Value = m.Ordem;

        //    cmdInserirMatriz.ExecuteNonQuery();
        //    int id = (int)(decimal)cmdInserirMatriz.Parameters["oID_COLUNA"].Value;
        //    return id;
        //}

        public int InserirColuna(int idQuestao, ColunaMatriz m)
        {
            cmdInserirMatriz.Parameters["@pID_QUESTAO"].Value = idQuestao;
            cmdInserirMatriz.Parameters["@pIN_ATIVA"].Value = m.Ativa ? 1 : 0;
            cmdInserirMatriz.Parameters["@pDS_COLUNA"].Value = m.Descricao;
            cmdInserirMatriz.Parameters["@pNU_ORDEM"].Value = m.Ordem;

            cmdInserirMatriz.Connection.Open();

            cmdInserirMatriz.ExecuteNonQuery();
            int id = (int)cmdInserirMatriz.Parameters["@oID_COLUNA"].Value;
            cmdInserirMatriz.Connection.Close();
            return id;
        }

        //public void AtualizarColuna(ColunaMatriz m)
        //{
        //    cmdAtualizarMatriz.Parameters["pID_COLUNA"].Value = m.Id;
        //    cmdAtualizarMatriz.Parameters["pIN_ATIVA"].Value = m.Ativa ? 1 : 0;
        //    cmdAtualizarMatriz.Parameters["pDS_COLUNA"].Value = m.Descricao;
        //    cmdAtualizarMatriz.Parameters["pNU_ORDEM"].Value = m.Ordem;

        //    cmdAtualizarMatriz.ExecuteNonQuery();
        //}

        public void AtualizarColuna(ColunaMatriz m)
        {
            cmdAtualizarMatriz.Parameters["@pID_COLUNA"].Value = m.Id;
            cmdAtualizarMatriz.Parameters["@pIN_ATIVA"].Value = m.Ativa ? 1 : 0;
            cmdAtualizarMatriz.Parameters["@pDS_COLUNA"].Value = m.Descricao;
            cmdAtualizarMatriz.Parameters["@pNU_ORDEM"].Value = m.Ordem;

            cmdAtualizarMatriz.Connection.Open();

            cmdAtualizarMatriz.ExecuteNonQuery();

            cmdAtualizarMatriz.Connection.Close();

        }


        //public void ExcluirColuna(int id)
        //{
        //    cmdExcluirMatriz.Parameters["pID_COLUNA"].Value = id;
        //    cmdExcluirMatriz.ExecuteNonQuery();
        //}

        public void ExcluirColuna(int id)
        {
            cmdExcluirMatriz.Parameters["@pID_COLUNA"].Value = id;

            cmdExcluirMatriz.Connection.Open();

            cmdExcluirMatriz.ExecuteNonQuery();

            cmdExcluirMatriz.Connection.Close();
        }

        //public int InserirPontuacao(int idFormulario, PontuacaoFormulario pontuacao)
        //{
        //    cmdInserirPontuacao.Parameters["pID_FORMULARIO"].Value = idFormulario;
        //    cmdInserirPontuacao.Parameters["pVL_PONTOS_MIN"].Value = (object)pontuacao.Minimo ?? DBNull.Value;
        //    cmdInserirPontuacao.Parameters["pVL_PONTOS_MAX"].Value = (object)pontuacao.Maximo ?? DBNull.Value;
        //    cmdInserirPontuacao.Parameters["pID_PERFIL"].Value = pontuacao.IdPerfil;

        //    cmdInserirPontuacao.ExecuteNonQuery();
        //    int id = (int)(decimal)cmdInserirPontuacao.Parameters["oID_PONTUACAO"].Value;
        //    return id;
        //}

        public int InserirPontuacao(int idFormulario, PontuacaoFormulario pontuacao)
        {
            cmdInserirPontuacao.Parameters["@pID_FORMULARIO"].Value = idFormulario;
            cmdInserirPontuacao.Parameters["@pVL_PONTOS_MIN"].Value = (object)pontuacao.Minimo ?? DBNull.Value;
            cmdInserirPontuacao.Parameters["@pVL_PONTOS_MAX"].Value = (object)pontuacao.Maximo ?? DBNull.Value;
            cmdInserirPontuacao.Parameters["@pID_PERFIL"].Value = pontuacao.IdPerfil;

            cmdInserirPontuacao.Connection.Open();

            cmdInserirPontuacao.ExecuteNonQuery();
            int id = (int)cmdInserirPontuacao.Parameters["@oID_PONTUACAO"].Value;

            cmdInserirPontuacao.Connection.Close();

            return id;
        }

        //public void AtualizarPontuacao(PontuacaoFormulario pontuacao)
        //{
        //    cmdAtualizarPontuacao.Parameters["pID_PONTUACAO"].Value = pontuacao.Id;
        //    cmdAtualizarPontuacao.Parameters["pVL_PONTOS_MIN"].Value = (object)pontuacao.Minimo ?? DBNull.Value;
        //    cmdAtualizarPontuacao.Parameters["pVL_PONTOS_MAX"].Value = (object)pontuacao.Maximo ?? DBNull.Value;
        //    cmdAtualizarPontuacao.ExecuteNonQuery();

        //}

        public void AtualizarPontuacao(PontuacaoFormulario pontuacao)
        {
            cmdAtualizarPontuacao.Parameters["@pID_PONTUACAO"].Value = pontuacao.Id;
            cmdAtualizarPontuacao.Parameters["@pVL_PONTOS_MIN"].Value = (object)pontuacao.Minimo ?? DBNull.Value;
            cmdAtualizarPontuacao.Parameters["@pVL_PONTOS_MAX"].Value = (object)pontuacao.Maximo ?? DBNull.Value;

            cmdAtualizarPontuacao.Connection.Open();

            cmdAtualizarPontuacao.ExecuteNonQuery();

            cmdAtualizarPontuacao.Connection.Close();

        }

        //public bool VerificarExclusao(int id)
        //{
        //    using (var cmd = dbi.CreateCommand("PCK_GESTAO_SUITABILITY.VERIFICAR_EXCLUSAO_FORMULARIO", System.Data.CommandType.StoredProcedure))
        //    {
        //        cmd.CreateParam("pID_FORMULARIO", id);
        //        cmd.Parameters.Add(new OracleParameter("oIN_OK", OracleType.Number) { Direction = System.Data.ParameterDirection.Output });
        //        cmd.ExecuteNonQuery();
        //        return cmd.Parameters["oIN_OK"].Value.Equals(0.0M);
        //    }
        //}

        public bool VerificarExclusao(int id)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");
            using (var cmd = (SqlCommand)dbiCRK.CreateCommand("PR_GESTAO_SUITABILITY_VERIFICAR_EXCLUSAO_FORMULARIO", CommandType.StoredProcedure))
            {
                cmd.CreateParam("@pID_FORMULARIO", id);
 
                cmd.Parameters.Add("@oIN_OK", SqlDbType.Int).Direction = ParameterDirection.Output;

                cmd.Connection.Open();

                cmd.ExecuteNonQuery();

                cmd.Connection.Close();

                return cmd.Parameters["@oIN_OK"].Value.Equals(0);
            }
        }
    }
}
