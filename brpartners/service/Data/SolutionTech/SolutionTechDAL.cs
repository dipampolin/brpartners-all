﻿using QX3.Portal.Services.Business.SolutionTech;
using QX3.Spinnex.Common.Services.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace QX3.Portal.Services.Data.SolutionTech
{
    public class SolutionTechDAL : DataProviderBase
    {
        public SolutionTechDAL() : base("PortalConnectionString") { }

        public void RecordLog(decimal codBolsa, string tipoInvestidor, decimal scoreSuitability, int httpStatusCode, string attributeTag, string message, DateTime suitabilityCompletionDate)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");

            DateTime sendDate = DateTime.Now;

            var cmd = (SqlCommand)dbiCRK.CreateCommand("InsertLog_SolutionTech", CommandType.StoredProcedure);
            cmd.Parameters.Clear();

            cmd.Parameters.Add("@cdbolsa", SqlDbType.Decimal).Value = codBolsa;

            if (string.IsNullOrEmpty(tipoInvestidor))
                cmd.Parameters.Add("@tipoInvestidor", SqlDbType.VarChar).Value = DBNull.Value;
            else
                cmd.Parameters.Add("@tipoInvestidor", SqlDbType.VarChar).Value = tipoInvestidor;

            
            cmd.Parameters.Add("@scoreSuitability", SqlDbType.Decimal).Value = scoreSuitability;
            cmd.Parameters.Add("@statusCode", SqlDbType.Int).Value = httpStatusCode;
            cmd.Parameters.Add("@attributeTag", SqlDbType.VarChar).Value = attributeTag;
            cmd.Parameters.Add("@message", SqlDbType.VarChar).Value = message;
            cmd.Parameters.Add("@suitabilityDate", SqlDbType.DateTime).Value = suitabilityCompletionDate;

            cmd.Parameters.Add("@sendDate", SqlDbType.DateTime).Value = sendDate;

            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            cmd.Connection.Close();
        }

        public void UpdatedSolutionTech(int clientId, DateTime suitabilityDate, bool sendedToSolutionTech)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");

            var cmd = (SqlCommand)dbiCRK.CreateCommand("Update_Updated_Status_SolutionTech", CommandType.StoredProcedure);
            cmd.Parameters.Clear();

            cmd.Parameters.Add("@cdBolsa", SqlDbType.Int).Value = clientId;

            cmd.Parameters.Add("@dataSuitability", SqlDbType.DateTime).Value = suitabilityDate;

            cmd.Parameters.Add("@updatedSolutionTech", SqlDbType.Bit).Value = sendedToSolutionTech;

            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            cmd.Connection.Close();
        }

        public void NotifiedBRP(int clientId, DateTime suitabilityDate, bool brpEmailNotified)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");

            var cmd = (SqlCommand)dbiCRK.CreateCommand("Update_NotifiedBRP_Status_SolutionTech", CommandType.StoredProcedure);
            cmd.Parameters.Clear();

            cmd.Parameters.Add("@cdBolsa", SqlDbType.Int).Value = clientId;

            cmd.Parameters.Add("@dataSuitability", SqlDbType.DateTime).Value = suitabilityDate;

            cmd.Parameters.Add("@notifiedBRP", SqlDbType.Bit).Value = brpEmailNotified;

            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            cmd.Connection.Close();
        }

        public List<SolutionTechLog> GetLogsSolutionTech(int? codBolsa)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");

            var cmd = (SqlCommand)dbiCRK.CreateCommand("GetLog_SolutionTech_By_CdBolsa", CommandType.StoredProcedure);

            SolutionTechLog log = new SolutionTechLog();
            List<SolutionTechLog> logList = new List<SolutionTechLog>();
            cmd.Parameters.Clear();

            if (codBolsa.HasValue)
                cmd.Parameters.Add("@cdBolsa", SqlDbType.Int).Value = codBolsa;
            else
                cmd.Parameters.Add("@cdBolsa", SqlDbType.Int).Value = DBNull.Value;

            cmd.Connection.Open();

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        log = new SolutionTechLog();
                        log.Id = dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0;
                        log.CdBolsa = dr["CdBolsa"] != DBNull.Value ? Convert.ToInt32(dr["CdBolsa"]) : 0;
                        log.InvestorType = dr["Tipo_Investidor"] != DBNull.Value ? Convert.ToString(dr["Tipo_Investidor"]) : String.Empty;
                        log.ScoreSuitability = dr["Score_Suitability"] != DBNull.Value ? Convert.ToInt32(dr["Score_Suitability"]) : 0;
                        log.StatusCode = dr["Status_Code"] != DBNull.Value ? Convert.ToInt32(dr["Status_Code"]) : 0;
                        log.AttributeTag = dr["Attribute_Tag"] != DBNull.Value ? Convert.ToString(dr["Attribute_Tag"]) : String.Empty;
                        log.Message = dr["Message"] != DBNull.Value ? Convert.ToString(dr["Message"]) : String.Empty;
                        log.SuitabilityDate = dr["Suitability_Date"] != DBNull.Value ? Convert.ToDateTime(dr["Suitability_Date"]) : DateTime.MinValue;
                        log.SendDate = dr["Send_Date"] != DBNull.Value ? Convert.ToDateTime(dr["Send_Date"]) : DateTime.MinValue;

                        logList.Add(log);
                    }
                }


                dr.Close();
            }

            cmd.Connection.Close();

            return logList;

        }
    }
}
