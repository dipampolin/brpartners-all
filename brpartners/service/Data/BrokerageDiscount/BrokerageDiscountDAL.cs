﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using QX3.Portal.Contracts.DataContracts;
using QX3.Spinnex.Common.Services.Data;

namespace QX3.Portal.Services.Data
{
    /// <summary>
    /// Classe que fará persistência na base. Será chamada pela classe dentro de Business
    /// </summary>
    public class BrokerDiscountDAL: DataProviderBase
    {
        public BrokerDiscountDAL() : base("PortalConnectionString") { }

        public List<BrokerageDiscount> LoadBrokerageDiscounts(BrokerageDiscount filter, int? userId)
        {
            List<BrokerageDiscount> result = null;

            var cmd = dbi.CreateCommand("PCK_PORTAL_DESCONTO.PROC_CONSULTA_DESCONTO", CommandType.StoredProcedure);
            cmd.Parameters.Clear();

            if (filter.StockExchange != 0)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_BOLSA", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = (int)filter.StockExchange });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_BOLSA", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });


            if (String.IsNullOrEmpty(filter.MarketType))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_MERCADO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_MERCADO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.MarketType });
            
            //string auxOperationType = (filter.OperationType == OperationType.All)
            //                            ? null
            //                            : (filter.OperationType == OperationType.Day)
            //                                ? "S"
            //                                : "N";
            if (String.IsNullOrEmpty(filter.Operation))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_NORMAL", IsNullable = true, OracleType = OracleType.Char, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_NORMAL", IsNullable = true, OracleType = OracleType.Char, Direction = ParameterDirection.Input, Value = filter.Operation.ToCharArray()[0] });

            //string auxvigencyType = (filter.VigencyType == VigencyType.All)
            //                            ? null
            //                            : (filter.VigencyType == VigencyType.D0)
            //                                ? "S"
            //                                : (filter.VigencyType == VigencyType.D1)
            //                                    ? "N"
            //                                    : "P";

            if (String.IsNullOrEmpty(filter.Vigency)) 
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_VIGENCIA", IsNullable = true, OracleType = OracleType.Char, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_VIGENCIA", IsNullable = true, OracleType = OracleType.Char, Direction = ParameterDirection.Input, Value = filter.Vigency.ToCharArray()[0] });

            //string auxSituationType = (filter.SituationType == SituationType.All)
            //                                ? null
            //                                : (filter.SituationType == SituationType.Approve)
            //                                    ? "A"
            //                                    : (filter.SituationType == SituationType.Reproved)
            //                                        ? "R"
            //                                        : (filter.SituationType == SituationType.Effect)
            //                                            ? "F"
            //                                            : "E";
            //if ()

            if (String.IsNullOrEmpty(filter.Situation))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_STATUS", IsNullable = true, OracleType = OracleType.Char, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_STATUS", IsNullable = true, OracleType = OracleType.Char, Direction = ParameterDirection.Input, Value = filter.Situation.ToCharArray()[0] });


            if (filter.StockDiscountMinorPercentual.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pPC_DESCONTOMENOR", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = filter.StockDiscountMinorPercentual.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pPC_DESCONTOMENOR", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });

            if (filter.StockDiscountMajorPercentual.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pPC_DESCONTOMAIOR", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = filter.StockDiscountMajorPercentual.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pPC_DESCONTOMAIOR", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });

            if (filter.StockDiscountMinorValue.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pVL_DESCONTOMENOR", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = filter.StockDiscountMinorValue.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pVL_DESCONTOMENOR", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });

            if (filter.StockDiscountMinorValue.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pVL_DESCONTOMAIOR", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = filter.StockDiscountMajorValue.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pVL_DESCONTOMAIOR", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });


            if (String.IsNullOrEmpty(filter.Assessor))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTASSESSOR", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTASSESSOR", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.Assessor });

            if (filter.ClientFilter == null)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTCLIENTE", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTCLIENTE", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.ClientFilter });

            if (userId.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = userId.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCursor", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    result = new List<BrokerageDiscount>();
                    while (dr.Read())
                    {
                        BrokerageDiscount broker = new BrokerageDiscount();

                        if (!dr.IsDBNull(0)) broker.RequestId = dr.GetInt32(0); //ID_SOLICITACAO
                        if (!dr.IsDBNull(1)) broker.ClientCode = dr.GetInt32(1); //CD_CLIENTE
                        if (!dr.IsDBNull(2)) broker.ClientName = dr.GetString(2); //NM_CLIENTE
                        if (!dr.IsDBNull(3)) broker.Assessor = dr.GetString(3); //NM_ASSESSOR

                        if (!dr.IsDBNull(4)) broker.StockExchange = dr.GetInt32(4); //CD_BOLSA

                        if (!dr.IsDBNull(5)) broker.MarketType = dr.GetString(5); //CD_MERCADO

                        if (!dr.IsDBNull(6)) broker.Operation = dr.GetString(6); //ds_operacao
                        if (!dr.IsDBNull(7)) broker.Vigency = dr.GetString(7); //ds_vigencia

                        if (!dr.IsDBNull(8)) broker.StockDiscountPercentual = dr.GetDecimal(8); //Pc_Desconto
                        if (!dr.IsDBNull(9)) broker.StockDiscountValue = dr.GetDecimal(9); //Vl_Desconto

                        if (!dr.IsDBNull(10)) broker.Description = dr.GetString(10); //ds_observacao
                        if (!dr.IsDBNull(11)) broker.Situation = dr.GetString(11); //Ds_status
                        if (!dr.IsDBNull(12)) broker.RequestDate = dr.GetDateTime(12); //dt_solicitacao
                        if (!dr.IsDBNull(13)) broker.ActualDiscount = dr.GetDecimal(13); //pc_desconto_atual

                        if (!dr.IsDBNull(14)) broker.StockCode = dr.GetString(14); //CD_NEGOCIO
                        if (!dr.IsDBNull(15)) broker.Qtty = dr.GetInt32(15); //QT_DESP
                        

                        result.Add(broker);
                    }
                }

                dr.Close();

            }

            return result;
        }

        public List<BrokerageDiscount> LoadBrokerageDiscounts(BrokerageDiscount filter, FilterOptions filterOptions, int? userId, out int count)
        {
            List<BrokerageDiscount> result = null; count = 0;

            var cmd = dbi.CreateCommand("PCK_PORTAL_DESCONTO.PROC_CONSULTA_DESCONTO_PAG", CommandType.StoredProcedure);
            cmd.Parameters.Clear();

            if (filter.StockExchange != 0)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_BOLSA", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = (int)filter.StockExchange });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_BOLSA", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });


            if (String.IsNullOrEmpty(filter.MarketType))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_MERCADO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_MERCADO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.MarketType });

            
            if (String.IsNullOrEmpty(filter.Operation))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_NORMAL", IsNullable = true, OracleType = OracleType.Char, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_NORMAL", IsNullable = true, OracleType = OracleType.Char, Direction = ParameterDirection.Input, Value = filter.Operation.ToCharArray()[0] });


            if (String.IsNullOrEmpty(filter.Vigency))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_VIGENCIA", IsNullable = true, OracleType = OracleType.Char, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_VIGENCIA", IsNullable = true, OracleType = OracleType.Char, Direction = ParameterDirection.Input, Value = filter.Vigency.ToCharArray()[0] });

            if (String.IsNullOrEmpty(filter.Situation))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_STATUS", IsNullable = true, OracleType = OracleType.Char, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_STATUS", IsNullable = true, OracleType = OracleType.Char, Direction = ParameterDirection.Input, Value = filter.Situation.ToCharArray()[0] });


            if (filter.StockDiscountMinorPercentual.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pPC_DESCONTOMENOR", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = filter.StockDiscountMinorPercentual.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pPC_DESCONTOMENOR", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });

            if (filter.StockDiscountMajorPercentual.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pPC_DESCONTOMAIOR", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = filter.StockDiscountMajorPercentual.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pPC_DESCONTOMAIOR", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });

            if (filter.StockDiscountMinorValue.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pVL_DESCONTOMENOR", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = filter.StockDiscountMinorValue.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pVL_DESCONTOMENOR", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });

            if (filter.StockDiscountMinorValue.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pVL_DESCONTOMAIOR", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = filter.StockDiscountMajorValue.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pVL_DESCONTOMAIOR", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });


            if (String.IsNullOrEmpty(filter.Assessor))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTASSESSOR", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTASSESSOR", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.Assessor });

            if (filter.ClientFilter == null)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTCLIENTE", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTCLIENTE", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.ClientFilter });

            if (userId.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = userId.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pINITROW", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = filterOptions.InitRow });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pFINALROW", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = filterOptions.FinalRow });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pSORTCOLUMN", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = filterOptions.SortColumn });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pSORTDIR", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filterOptions.SortDirection });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pTODOS", IsNullable = true, OracleType = OracleType.Int32, Direction = ParameterDirection.Input, Value = (filterOptions.AllRecords) ? 1 : 0 });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCount", OracleType = OracleType.Number, Direction = ParameterDirection.Output });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCursor", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    count = Convert.ToInt32(cmd.Parameters["pCount"].Value);

                    result = new List<BrokerageDiscount>();
                    while (dr.Read())
                    {
                        BrokerageDiscount broker = new BrokerageDiscount();

                        decimal auxBroker = 0;

                         //ID_SOLICITACAO
                        if (!dr.IsDBNull(0)) broker.ClientCode = dr.GetInt32(0); //CD_CLIENTE
                        if (!dr.IsDBNull(1)) broker.ClientName = dr.GetString(1); //NM_CLIENTE
                        if (!dr.IsDBNull(2)) broker.Assessor = dr.GetString(2); //NM_ASSESSOR

                        if (!dr.IsDBNull(3)) broker.StockExchange = dr.GetInt32(3); //CD_BOLSA

                        if (!dr.IsDBNull(4)) broker.MarketType = dr.GetString(4); //CD_MERCADO

                        if (!dr.IsDBNull(5)) broker.Operation = dr.GetString(5); //ds_operacao
                        if (!dr.IsDBNull(6)) broker.Vigency = dr.GetString(6); //ds_vigencia

                        if (!dr.IsDBNull(7)) broker.StockCode = dr.GetString(7); //CD_NEGOCIO
                        if (!dr.IsDBNull(8)) broker.Qtty = dr.GetInt32(8); //QT_DESP

                        if (!dr.IsDBNull(9)) auxBroker = dr.GetDecimal(9); //broker.StockDiscountPercentual = dr.GetDecimal(9); //Pc_Desconto
                        //if (!dr.IsDBNull(10)) broker.StockDiscountValue = dr.GetDecimal(10); //Vl_Desconto

                        if (!dr.IsDBNull(10)) broker.Description = dr.GetString(10); //ds_observacao
                        if (!dr.IsDBNull(11)) broker.Situation = dr.GetString(11); //Ds_status
                        if (!dr.IsDBNull(12)) broker.RequestDate = dr.GetDateTime(12); //dt_solicitacao
                        if (!dr.IsDBNull(13)) broker.ActualDiscount = dr.GetDecimal(13); //pc_desconto_atual

                        if (!dr.IsDBNull(14)) broker.RequestId = dr.GetInt32(14);
                        
                        string tipo = dr.GetString(15);
                        if (tipo == "V") broker.StockDiscountValue = auxBroker; else broker.StockDiscountPercentual = auxBroker;

                        if (!dr.IsDBNull(16)) broker.DV = dr.GetInt32(16);
                        result.Add(broker);
                    }
                }

                dr.Close();

            }

            return result;
        }

        public bool UpdateBrokerageDiscountStatus(BrokerageDiscount request) 
        {

            var cmd = dbi.CreateCommand("begin :result := PCK_PORTAL_DESCONTO.FUNC_ALTERA_STATUS_SOLICITACAO(:pID_SOLICITACAO, :pCD_CLIENTE, :pIN_STATUS); end;", CommandType.Text);
            cmd.Parameters.Clear();

            if (request.ClientCode == 0 || request.RequestId == 0 || (request.Situation != "A" && request.Situation != "F" && request.Situation != "E" && request.Situation != "R"))
                return false;

            try
            {
                if (String.IsNullOrEmpty(request.Situation))
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_STATUS", IsNullable = true, OracleType = OracleType.Char, Direction = ParameterDirection.Input, Value = DBNull.Value });
                else
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_STATUS", IsNullable = true, OracleType = OracleType.Char, Direction = ParameterDirection.Input, Value = request.Situation });

                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = request.ClientCode });

                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_SOLICITACAO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = request.RequestId });

                cmd.Parameters.Add(new OracleParameter { ParameterName = "result", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Output });

                cmd.ExecuteNonQuery();

                return Convert.ToInt32(cmd.Parameters["result"].Value) == 1;
            }
            catch (Exception ex)
            {
                QX3.Spinnex.Common.Services.Logging.LogManager.WriteError(ex);
                return false;
            }
        }

        public bool DeleteBrokerageDiscount(BrokerageDiscount request)
        {

            var cmd = dbi.CreateCommand("begin :result := PCK_PORTAL_DESCONTO.FUNC_EXCLUI_SOLICITACAO(:pID_SOLICITACAO, :pCD_CLIENTE); end;", CommandType.Text);
            cmd.Parameters.Clear();

            if (request.ClientCode == 0 || request.RequestId == 0)
                return false;

            try
            {

                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = request.ClientCode });

                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_SOLICITACAO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = request.RequestId });

                cmd.Parameters.Add(new OracleParameter { ParameterName = "result", IsNullable = false, OracleType = OracleType.Number, Direction = ParameterDirection.Output });

                cmd.ExecuteNonQuery();

                return Convert.ToInt32(cmd.Parameters["result"].Value) == 1;
            }
            catch (Exception ex)
            {
                QX3.Spinnex.Common.Services.Logging.LogManager.WriteError(ex);
                return false;
            }
        }

        public bool BrokerageDiscountRequest(BrokerageDiscount request, out int requestId) 
        {
            requestId = 0;
            var cmd = dbi.CreateCommand("PCK_PORTAL_DESCONTO.PROC_SOLICITA_DESCONTO", CommandType.StoredProcedure);
            cmd.Parameters.Clear();

            
            /*
            pID_SOLICITACAO NUMBER,
            pCD_CLIENTE     NUMBER,
            pCD_BOLSA       NUMBER,
            pCD_MERCADO     VARCHAR2,
            pIN_NORMAL      CHAR,
            pIN_VIGENCIA    CHAR,
            pIN_TODOS       CHAR,
            pCD_NEGOCIO     Varchar2,
            pQT_QTDESP      Number,
            pPC_DESCONTO    NUMBER,
            pVL_DESCONTO    NUMBER,
            pDS_OBSERVACAO  VARCHAR2,
            pIN_STATUS      CHAR,
            pTP_OPERACAO    CHAR 
                */

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_SOLICITACAO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
                
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = request.ClientCode });                

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pTP_OPERACAO", IsNullable = true, OracleType = OracleType.Char, Direction = ParameterDirection.Input, Value = 'I' });

            
            
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_STATUS", IsNullable = true, OracleType = OracleType.Char, Direction = ParameterDirection.Input, Value = 'A' });

            if (request.StockExchange == 0)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_BOLSA", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_BOLSA", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = request.StockExchange });

            if (String.IsNullOrEmpty(request.MarketType))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_MERCADO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_MERCADO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = request.MarketType });

            if (String.IsNullOrEmpty(request.Operation))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_NORMAL", IsNullable = true, OracleType = OracleType.Char, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_NORMAL", IsNullable = true, OracleType = OracleType.Char, Direction = ParameterDirection.Input, Value = request.Operation });

            if (String.IsNullOrEmpty(request.Vigency))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_VIGENCIA", IsNullable = true, OracleType = OracleType.Char, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_VIGENCIA", IsNullable = true, OracleType = OracleType.Char, Direction = ParameterDirection.Input, Value = request.Vigency });


            if (request.StockCode != "TODOS")
            {
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_TODOS", IsNullable = true, OracleType = OracleType.Char, Direction = ParameterDirection.Input, Value = 'N' });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_NEGOCIO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = request.StockCode });
            }
            else
            {
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_TODOS", IsNullable = true, OracleType = OracleType.Char, Direction = ParameterDirection.Input, Value = 'S' });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_NEGOCIO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            }                
                                               
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pQT_QTDESP", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = request.Qtty });

            if (request.StockDiscountPercentual.HasValue)
            {
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pPC_DESCONTO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = request.StockDiscountPercentual.Value });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pVL_DESCONTO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
            }
            else if (request.StockDiscountValue.HasValue)
            {
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pVL_DESCONTO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = request.StockDiscountValue.Value });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pPC_DESCONTO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
            }
                
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pDS_OBSERVACAO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = request.Description });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_SOLICITACAO_REQUEST", OracleType = OracleType.Number, Direction = ParameterDirection.Output });

            int exec = cmd.ExecuteNonQuery();

            requestId = Convert.ToInt32(cmd.Parameters["pID_SOLICITACAO_REQUEST"].Value);

            return (exec > 0);
            
        }

        public DateTime GetProjectedDate(DateTime? date)
        {
            DateTime returnDate = DateTime.MinValue;
            var cmd = dbi.CreateCommand("PCK_PORTAL_DESCONTO.PROC_CALCULA_DATA", CommandType.StoredProcedure);
            cmd.Parameters.Clear();

            if (!date.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_ATUAL", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DateTime.Now.ToString("dd/MM/yyyy") });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_ATUAL", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = date.Value.ToString("dd/MM/yyyy") });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_PROJETADA", OracleType = OracleType.VarChar, Size=10, Direction = ParameterDirection.Output });

            cmd.ExecuteNonQuery();
            
            returnDate = Convert.ToDateTime(cmd.Parameters["pDT_PROJETADA"].Value);

            return returnDate;
        }

        public int GetPendentBrokerageDiscounts(BrokerageDiscount filter, int? userId)
        {

            var cmd = dbi.CreateCommand("begin :pCount := PCK_PORTAL_DESCONTO.FUNC_NUMERO_DESCONTOS_PENDENTE(:pLSTASSESSOR, :pLSTCLIENTE, :pID_GRUPO); end;", CommandType.Text);
            cmd.Parameters.Clear();

            if (String.IsNullOrEmpty(filter.Assessor))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTASSESSOR", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTASSESSOR", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.Assessor });

            if (filter.ClientFilter == null)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTCLIENTE", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTCLIENTE", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.ClientFilter });

            if (userId.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = userId.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCount", OracleType = OracleType.Number, Direction = ParameterDirection.Output });

            cmd.ExecuteNonQuery();

            return (cmd.Parameters["pCount"].Value != null) ? Convert.ToInt32(cmd.Parameters["pCount"].Value) : 0;
        }

        public decimal GetActualDiscount(int clientCode) 
        {
            
            var cmd = dbi.CreateCommand("begin :pResult := PCK_PORTAL_DESCONTO.FUNC_DESCONTO_ATUAL(:pClient); end;", CommandType.Text);
            cmd.Parameters.Clear();

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pClient", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = clientCode });


            cmd.Parameters.Add(new OracleParameter { ParameterName = "pResult", OracleType = OracleType.Number, Size = 18, Direction = ParameterDirection.Output });

            cmd.ExecuteNonQuery();

            var discount = Convert.ToDecimal(cmd.Parameters["pResult"].Value);

            return discount;
        }
    }
}
