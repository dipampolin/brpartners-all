﻿using System;
using System.Data;
using System.Data.Common;
using QX3.Spinnex.Authentication.Services.UserData;
using QX3.Spinnex.Common.Services.Data;
using QX3.Spinnex.Common.Services.Formatting;
using QX3.Spinnex.Common.Services.Security;
using QX3.Spinnex.Authentication.Services.UserGroupData;
using System.Collections.Generic;
using QX3.Spinnex.Authentication.Services;
using System.Data.OracleClient;
using QX3.Portal.Contracts.DataContracts;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Data.SqlClient;

namespace QX3.Portal.Services.Data.Authentication
{
    public class AuthenticationDAL : DataProviderBase
    {
        public AuthenticationDAL() : base("PortalConnectionString") { }

        public QX3.Portal.Contracts.DataContracts.UserInformation Authenticate(string userName
            , string password
            , out string message
            , out bool logged
            , out int errorType
            , out string ADKey)
        {
            QX3.Portal.Contracts.DataContracts.UserInformation user = null;
            var cmd = dbi.CreateCommand("PCK_USUARIOS.PROC_LOGIN", CommandType.StoredProcedure);

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pUserName", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = userName });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pPassword", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = password });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCursor", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pOK", OracleType = OracleType.Number, Direction = ParameterDirection.Output });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pMessage", OracleType = OracleType.VarChar, Direction = ParameterDirection.Output, Size = 150 });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pChaveAD", OracleType = OracleType.VarChar, Direction = ParameterDirection.Output, Size = 12 });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pTipo", OracleType = OracleType.Number, Direction = ParameterDirection.Output });

            this.Connect();

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.Read())
                {
                    if (Convert.ToInt32(cmd.Parameters["pOK"].Value) > 0)
                    {
                        user = new QX3.Portal.Contracts.DataContracts.UserInformation();

                        int counter = 0;
                        user.ID = dr.GetInt32(counter);
                        user.Name = dr.GetString(++counter);
                        user.Email = dr.GetString(++counter);
                        if (!dr.IsDBNull(++counter)) user.AssessorID = dr.GetInt32(counter);
                        if (!dr.IsDBNull(++counter)) user.UserName = dr.GetString(counter);
                        if (!dr.IsDBNull(++counter)) user.OtherAttempts = dr.GetInt32(counter);
                        user.CdBolsa = dr["cdbolsa"] != DBNull.Value ? Convert.ToInt32(dr["cdbolsa"]) : 0;
                        user.UserType = dr["tipousuario"] != DBNull.Value ? Convert.ToString(dr["tipousuario"]) : "";
                        user.InvestorType = dr["investidor"] != DBNull.Value ? Convert.ToInt32(dr["investidor"]) : 0;
                        user.SuitabilityStatus = dr["suitability_status"] != DBNull.Value ? Convert.ToInt32(dr["suitability_status"]) : 1;

                        if (!(dr["SUITABILITY_DATE"] is DBNull))
                            user.SuitabilityDate = Convert.ToDateTime(dr["SUITABILITY_DATE"]);

                        if (!(dr["ENVIAR_EMAIL_SUITABILITY"] is DBNull))
                            user.SendSuitabilityEmail = Convert.ToInt32(dr["ENVIAR_EMAIL_SUITABILITY"]) == 1 ? true : false;
                        else
                            user.SendSuitabilityEmail = true;

                        user.IsBlocked = false;

                        object clientId = dr["ID_CLIENTE"];
                        if (!(clientId.Equals(DBNull.Value)))
                            user.GlobalClientId = Convert.ToInt32(clientId);

                        object cpfcnpj = dr["CD_CPFCNPJ"];
                        if (!(cpfcnpj.Equals(DBNull.Value)))
                            user.CpfCnpj = Convert.ToInt64(cpfcnpj);


                    }
                }

                message = cmd.Parameters["pMessage"].Value.ToString();

                dr.Close();
            }

            this.Close();

            logged = (Convert.ToInt32(cmd.Parameters["pOK"].Value) > 0);

            errorType = Convert.ToInt32(cmd.Parameters["pTipo"].Value);

            ADKey = cmd.Parameters["pChaveAD"].Value.ToString();

            return user;
        }


        public QX3.Portal.Contracts.DataContracts.UserInformation SelectUser(string userName
            , out string message
            , out bool logged
            , out int errorType)
        {
            QX3.Portal.Contracts.DataContracts.UserInformation user = null;
            var cmd = dbi.CreateCommand("PCK_USUARIOS.PROC_SELECIONA_USUARIO", CommandType.StoredProcedure);

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pUserName", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = userName });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCursor", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pOK", OracleType = OracleType.Number, Direction = ParameterDirection.Output });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pMessage", OracleType = OracleType.VarChar, Direction = ParameterDirection.Output, Size = 150 });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pTipo", OracleType = OracleType.Number, Direction = ParameterDirection.Output });

            this.Connect();

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.Read())
                {
                    if (Convert.ToInt32(cmd.Parameters["pOK"].Value) > 0)
                    {
                        user = new QX3.Portal.Contracts.DataContracts.UserInformation();

                        int counter = 0;
                        user.ID = dr.GetInt32(counter);
                        user.Name = dr.GetString(++counter);
                        user.Email = dr.GetString(++counter);
                        if (!dr.IsDBNull(++counter)) user.AssessorID = dr.GetInt32(counter);
                        if (!dr.IsDBNull(++counter)) user.UserName = dr.GetString(counter);
                        if (!dr.IsDBNull(++counter)) user.OtherAttempts = dr.GetInt32(counter);
                        user.CdBolsa = dr["cdbolsa"] != DBNull.Value ? Convert.ToInt32(dr["cdbolsa"]) : 0;
                        user.UserType = dr["tipousuario"] != DBNull.Value ? Convert.ToString(dr["tipousuario"]) : "";
                        user.InvestorType = dr["investidor"] != DBNull.Value ? Convert.ToInt32(dr["investidor"]) : 0;
                        user.SuitabilityStatus = dr["suitability_status"] != DBNull.Value ? Convert.ToInt32(dr["suitability_status"]) : 1;
                        user.IsBlocked = false;

                        object clientId = dr["ID_CLIENTE"];
                        object cpfcnpj = dr["CD_CPFCNPJ"];

                        if (!clientId.Equals(DBNull.Value))
                            user.GlobalClientId = Convert.ToInt32(clientId);
                        if (!cpfcnpj.Equals(DBNull.Value))
                            user.CpfCnpj = Convert.ToInt64(cpfcnpj);




                    }
                }

                message = cmd.Parameters["pMessage"].Value.ToString();

                dr.Close();
            }

            this.Close();

            logged = (Convert.ToInt32(cmd.Parameters["pOK"].Value) > 0);

            errorType = Convert.ToInt32(cmd.Parameters["pTipo"].Value);

            return user;
        }

        public bool ChangePassword(QX3.Portal.Contracts.DataContracts.UserInformation userInfo, out string message)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PROC_ALTERA_SENHA", CommandType.StoredProcedure);

            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pEmail", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = userInfo.Email });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pNovaSenha", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = userInfo.Password });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pResult", SqlDbType = SqlDbType.Decimal, Direction = ParameterDirection.Output });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pMessage", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Output, Size = 150 });

            cmd.Connection.Open();

            cmd.ExecuteNonQuery();

            bool response = Convert.ToInt32(cmd.Parameters["@pResult"].Value) > 0;
            message = cmd.Parameters["@pMessage"].Value.ToString();

            cmd.Connection.Close();

            return response;
        }

        public bool VerifyChangePassword(string guid, out string message, out string email)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("VERIFICA_GUID", CommandType.StoredProcedure);

            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pLink", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = guid });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pDuration", SqlDbType = SqlDbType.Decimal, Direction = ParameterDirection.Input, Value = Convert.ToInt32(ConfigurationManager.AppSettings["Mail.TokenDuration"]) });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@ret", SqlDbType = SqlDbType.Decimal, Direction = ParameterDirection.Output });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pMessage", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Output, Size = 150 });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pEmail", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Output, Value = DBNull.Value, Size = 150 });

            cmd.Connection.Open();

            cmd.ExecuteNonQuery();

            bool response = Convert.ToInt32(cmd.Parameters["@ret"].Value) > 0;
            message = cmd.Parameters["@pMessage"].Value.ToString();
            email = cmd.Parameters["@pEmail"].Value.ToString();

            cmd.Connection.Close();

            return response;
        }

        public bool CreateGuidToSend(QX3.Portal.Contracts.DataContracts.UserInformation userInfo, out string guid, out string message)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");

            var cmd = (SqlCommand)dbiCRK.CreateCommand("ENVIO_EMAIL", CommandType.StoredProcedure);

            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pEmail", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = userInfo.Email });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pLink", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Output, Size = 40 });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pResult", SqlDbType = SqlDbType.Decimal, Direction = ParameterDirection.Output, Size = 1 });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pMessage", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Output, Size = 150 });

            cmd.Connection.Open();

            cmd.ExecuteNonQuery();

            bool response = Convert.ToInt32(cmd.Parameters["@pResult"].Value) > 0;
            guid = Convert.ToString(cmd.Parameters["@pLink"].Value);
            message = cmd.Parameters["@pMessage"].Value.ToString();

            cmd.Connection.Close();

            return response;
        }

        public QX3.Portal.Contracts.DataContracts.UserInformation LoginSQLServer(string userName, string password, out string message, out bool logged, out int errorType, out string ADKey)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");

            var cmd = (SqlCommand)dbiCRK.CreateCommand("PROC_LOGIN", CommandType.StoredProcedure);

            cmd.Parameters.Clear();
            cmd.Parameters.Add("@LOGIN", SqlDbType.VarChar).Value = (object)userName ?? DBNull.Value;
            cmd.Parameters.Add("@SENHA", SqlDbType.VarChar).Value = (object)password ?? DBNull.Value;

            QX3.Portal.Contracts.DataContracts.UserInformation user = null;

            cmd.Connection.Open();

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    if (dr.Read())
                    {
                        user = new QX3.Portal.Contracts.DataContracts.UserInformation();

                        user.ID = dr["id"] != DBNull.Value ? Convert.ToInt32(dr["id"]) : 0;
                        user.Name = dr["nome"] != DBNull.Value ? Convert.ToString(dr["nome"]) : "";
                        user.IdClient = dr["id_cliente"] != DBNull.Value ? Convert.ToString(dr["id_cliente"]) : "";
                        user.Email = dr["email"] != DBNull.Value ? Convert.ToString(dr["email"]) : "";
                        user.AssessorID = dr["idassessor"] != DBNull.Value ? Convert.ToInt32(dr["idassessor"]) : 0;
                        user.UserName = dr["login"] != DBNull.Value ? Convert.ToString(dr["login"]) : "";
                        user.OtherAttempts = dr["tentativas_restantes"] != DBNull.Value ? Convert.ToInt32(dr["tentativas_restantes"]) : 0;
                        user.CdBolsa = dr["cdbolsa"] != DBNull.Value ? Convert.ToInt32(dr["cdbolsa"]) : 0;
                        user.UserType = dr["tipousuario"] != DBNull.Value ? Convert.ToString(dr["tipousuario"]) : "";
                        user.InvestorType = dr["investidor"] != DBNull.Value ? Convert.ToInt32(dr["investidor"]) : 0;
                        user.SuitabilityStatus = dr["suitability_status"] != DBNull.Value ? Convert.ToInt32(dr["suitability_status"]) : 1;

                        if (!(dr["SUITABILITY_DATE"] is DBNull))
                            user.SuitabilityDate = Convert.ToDateTime(dr["SUITABILITY_DATE"]);

                                                user.IsBlocked = false;

                        if (dr["id_cliente"] != DBNull.Value)
                            user.GlobalClientId = Convert.ToInt32(dr["id_cliente"]);

                        //user.GlobalClientId = 3;

                        object cpfcnpj = dr["CD_CPFCNPJ"];
                        if (!(cpfcnpj.Equals(DBNull.Value)))
                            user.CpfCnpj = Convert.ToInt64(cpfcnpj);

                        user.InvestorKind = dr["TIPO_INVESTIDOR"] != DBNull.Value ? Convert.ToInt32(dr["TIPO_INVESTIDOR"]) : 0;
                        user.PersonType = dr["TP_PESSOA"] != DBNull.Value ? Convert.ToString(dr["TP_PESSOA"]) : "";
                    }

                    message = "";
                    logged = true;
                    errorType = 0;
                    ADKey = "";
                }
                else
                {
                    message = cmd.Parameters["pMessage"].Value.ToString();
                    logged = false;
                    errorType = 1;
                    ADKey = "";
                }

                dr.Close();
            }
            cmd.Connection.Close();
            return user;
        }
    }
}
