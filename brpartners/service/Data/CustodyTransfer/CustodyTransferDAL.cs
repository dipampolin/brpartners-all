﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.OracleClient;
using QX3.Portal.Contracts.DataContracts;
using QX3.Spinnex.Common.Services.Data;

namespace QX3.Portal.Services.Data
{
    public class CustodyTransferDAL : DataProviderBase
    {
        public CustodyTransferDAL() : base("PortalConnectionString") { }

        public List<CustodyTransfer> LoadCustodyTransfers(CustodyTransferFilter filter, FilterOptions options, int? userId, out int count) 
        {
            count = 0;
            List<CustodyTransfer> result = null; count = 0;
            
            var cmd = dbi.CreateCommand("PCK_PORTAL_TRANSFERENCIAS.PROC_CONSULTA_SOLICITACAO", CommandType.StoredProcedure);
            cmd.Parameters.Clear();

            if (filter.InitialRequest.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "PINI_DATA_MOV", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.InitialRequest.Value.ToString("dd/MM/yyyy") });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "PINI_DATA_MOV", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });


            if (filter.FinalRequest.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "PFIM_DATA_MOV", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.FinalRequest.Value.ToString("dd/MM/yyyy") });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "PFIM_DATA_MOV", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });


            if (String.IsNullOrEmpty(filter.Status))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_STATUS", IsNullable = true, OracleType = OracleType.Char, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_STATUS", IsNullable = true, OracleType = OracleType.Char, Direction = ParameterDirection.Input, Value = filter.Status.ToCharArray()[0] });

            if (String.IsNullOrEmpty(filter.CustodyTransferType))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pTIPO_TRANSFERENCIA", IsNullable = true, OracleType = OracleType.Char, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pTIPO_TRANSFERENCIA", IsNullable = true, OracleType = OracleType.Char, Direction = ParameterDirection.Input, Value = filter.CustodyTransferType.ToCharArray()[0] });

            if (String.IsNullOrEmpty(filter.Assessor))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTASSESSOR", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTASSESSOR", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.Assessor });

            if (String.IsNullOrEmpty(filter.ClientFilter))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTCLIENTE", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTCLIENTE", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.ClientFilter });

            if (userId.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = userId.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pINITROW", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = options.InitRow });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pFINALROW", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = options.FinalRow });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pSORTCOLUMN", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = options.SortColumn });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pSORTDIR", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = options.SortDirection });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pTODOS", IsNullable = true, OracleType = OracleType.Int32, Direction = ParameterDirection.Input, Value = (options.AllRecords) ? 1 : 0 });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCount", OracleType = OracleType.Number, Direction = ParameterDirection.Output });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCursor", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    count = Convert.ToInt32(cmd.Parameters["pCount"].Value);

                    result = new List<CustodyTransfer>();
                    while (dr.Read())
                    {
                        CustodyTransfer transfer = new CustodyTransfer();

                        if (!dr.IsDBNull(0)) transfer.ClientCode = dr.GetInt32(0); //CD_CLIENTE
                        if (!dr.IsDBNull(1)) transfer.ClientName = dr.GetString(1); //NM_CLIENTE
                        if (!dr.IsDBNull(2)) transfer.Assessor = dr.GetString(2); //NM_ASSESSOR
                        if (!dr.IsDBNull(3)) transfer.StockCode = dr.GetString(3); //CD_NEGOCIO
                        if (!dr.IsDBNull(4)) transfer.Qtty = dr.GetDecimal(4); //CD_CLIENTE
                        if (!dr.IsDBNull(5)) transfer.OrigBroker = dr.GetString(5); //NM_LOCAL
                        if (!dr.IsDBNull(6)) transfer.OrigPortfolio = dr.GetInt32(6); //cd_carteira
                        if (!dr.IsDBNull(7)) transfer.DestinyBroker = dr.GetString(7); //nm_contraparte
                        if (!dr.IsDBNull(8)) transfer.DestinyPortfolio = dr.GetInt32(8); //NM_LOCAL
                        if (!dr.IsDBNull(9)) transfer.DescStatus = dr.GetString(9); //ds_historico
                        if (!dr.IsDBNull(10)) transfer.RequestDate = dr.GetDateTime(10); //dt_movimento
                        if (!dr.IsDBNull(11)) transfer.Observations = dr.GetString(11); //ds_observacao

                        if (!dr.IsDBNull(12)) transfer.Status = dr.GetString(12); //in_status
                        if (!dr.IsDBNull(13)) transfer.RequestId = dr.GetInt32(13); //id_solicitacao
                        if (!dr.IsDBNull(14)) transfer.AssessorCode = dr.GetInt32(14);

                        if (!dr.IsDBNull(15)) transfer.OrigBrokerCode = dr.GetInt32(15); //cd_local
                        if (!dr.IsDBNull(17)) transfer.DestinyBrokerCode = dr.GetInt32(17); //cd_Contraparte
                        if (!dr.IsDBNull(18)) transfer.DestinyClientCode = dr.GetInt32(18); //cd_cliente_contraparte
                        if (!dr.IsDBNull(19)) transfer.DV = dr.GetInt32(19); //DV

                        result.Add(transfer);
                    }
                }

                dr.Close();

            }

            
            return result;
        }

        public bool UpdateCustodyTransferStatus(CustodyTransfer request) 
        {
            /*
             Procedure PROC_MANTEM_SOLICITACAO ( pID_SOLICITACAO       in number,
                                      pCD_CLIENTE           in number,
                                      pCD_NEGOCIO           in Varchar2,
                                      pCD_CARTEIRA_ORIGEM   in number,
                                      pCD_CORRETORA_DESTINO in number,
                                      pCD_CARTEIRA_DESTINO  in number,
                                      pDT_SOLICITACAO       in Varchar2,
                                      pCD_CLIENTE_DESTINO   in number,
                                      pPU_POSICAO           In Number,
                                      pQT_TRANSFERIR        in Number,
                                      pDS_OBSERVACAO        In varchar2,
                                      pIN_STATUS            In char,
                                      pTP_OPERACAO          In char,
                                      pCount                Out number
                                       ) is
             */

            var cmd = dbi.CreateCommand("PCK_PORTAL_TRANSFERENCIAS.PROC_MANTEM_SOLICITACAO", CommandType.StoredProcedure);
            cmd.Parameters.Clear();

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_SOLICITACAO", IsNullable = false, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = request.RequestId });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", IsNullable = false, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = request.ClientCode });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_STATUS", IsNullable = false, OracleType = OracleType.Char, Direction = ParameterDirection.Input, Value = request.Status.ToCharArray()[0] });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pTP_OPERACAO", IsNullable = false, OracleType = OracleType.Char, Direction = ParameterDirection.Input, Value = 'A' });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_NEGOCIO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CARTEIRA_ORIGEM", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CORRETORA_DESTINO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CARTEIRA_DESTINO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_SOLICITACAO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE_DESTINO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pPU_POSICAO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pQT_TRANSFERIR", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pDS_OBSERVACAO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCount", OracleType = OracleType.Number, Direction = ParameterDirection.Output });

            var rest = cmd.ExecuteNonQuery();

            return Convert.ToInt32(rest) > 0;
        }

		public CustodyTransferReport LoadCustodyTransferReport(string fromDate, string toDate)
		{
			CustodyTransferReport report = new CustodyTransferReport();

			DbCommand cmd = dbi.CreateCommand("PCK_PORTAL_TRANSFERENCIAS.PROC_RELATORIO_TRANSFERENCIA", CommandType.StoredProcedure);
			cmd.Parameters.Clear();

			cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_INICIO", IsNullable = false, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = fromDate });
			cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_FIM", IsNullable = false, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = toDate });

			cmd.Parameters.Add(new OracleParameter { ParameterName = "pAPORTE_CORRETORA", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });
			cmd.Parameters.Add(new OracleParameter { ParameterName = "pAPORTE_ASSESSOR", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });
			cmd.Parameters.Add(new OracleParameter { ParameterName = "pSAIDA_CORRETORA", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });
			cmd.Parameters.Add(new OracleParameter { ParameterName = "pSAIDA_ASSESSOR", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });
			cmd.Parameters.Add(new OracleParameter { ParameterName = "pGRAFICO", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

			using (DbDataReader dr = cmd.ExecuteReader())
			{
				report.FromDate = Convert.ToDateTime(fromDate);
				report.ToDate = Convert.ToDateTime(toDate);

				if (dr.HasRows)
				{
					while (dr.Read())
					{
						report.BrokerInTop.Add(new CustodyTransferTop()
						{
							Amount = (dr["vl_negocio"] is DBNull) ? 0 : Convert.ToDecimal(dr["vl_negocio"]),
							Percentage = (dr["pc_total"] is DBNull) ? 0 : Convert.ToDecimal(dr["pc_total"]),
							Title = (dr["desc_loc"] is DBNull) ? string.Empty : Convert.ToString(dr["desc_loc"])
						});
					}

					if (dr.NextResult())
					{
						while (dr.Read())
						{
							report.AssessorInTop.Add(new CustodyTransferTop()
							{
								Amount = (dr["vl_negocio"] is DBNull) ? 0 : Convert.ToDecimal(dr["vl_negocio"]),
								Percentage = (dr["pc_total"] is DBNull) ? 0 : Convert.ToDecimal(dr["pc_total"]),
								Title = (dr["nm_assessor"] is DBNull) ? string.Empty : Convert.ToString(dr["nm_assessor"])
							});
						}
					}

					if (dr.NextResult())
					{
						while (dr.Read())
						{
							report.BrokerOutTop.Add(new CustodyTransferTop()
							{
								Amount = (dr["vl_negocio"] is DBNull) ? 0 : Convert.ToDecimal(dr["vl_negocio"]),
								Percentage = (dr["pc_total"] is DBNull) ? 0 : Convert.ToDecimal(dr["pc_total"]),
								Title = (dr["desc_loc"] is DBNull) ? string.Empty : Convert.ToString(dr["desc_loc"])
							});
						}
					}

					if (dr.NextResult())
					{
						while (dr.Read())
						{
							report.AssessorOutTop.Add(new CustodyTransferTop()
							{
								Amount = (dr["vl_negocio"] is DBNull) ? 0 : Convert.ToDecimal(dr["vl_negocio"]),
								Percentage = (dr["pc_total"] is DBNull) ? 0 : Convert.ToDecimal(dr["pc_total"]),
								Title = (dr["nm_assessor"] is DBNull) ? string.Empty : Convert.ToString(dr["nm_assessor"])
							});
						}
					}

					if (dr.NextResult())
					{
						while (dr.Read())
						{
							if (!(dr["in_aporte_saida"] is DBNull) && Convert.ToString(dr["in_aporte_saida"]).Equals("ENTRADA"))
							{
								report.ChartOrigin.Add(new CustodyTransferChart()
								{
									Amount = (dr["vl_negocio"] is DBNull) ? 0 : Convert.ToDecimal(dr["vl_negocio"]),
									Date = (dr["dt_movimento"] is DBNull) ? DateTime.MinValue : Convert.ToDateTime(dr["dt_movimento"])
								});
							}

							if (!(dr["in_aporte_saida"] is DBNull) && Convert.ToString(dr["in_aporte_saida"]).Equals("SAÍDA"))
							{
								report.ChartDestination.Add(new CustodyTransferChart()
								{
									Amount = (dr["vl_negocio"] is DBNull) ? 0 : Convert.ToDecimal(dr["vl_negocio"]),
									Date = (dr["dt_movimento"] is DBNull) ? DateTime.MinValue : Convert.ToDateTime(dr["dt_movimento"])
								});
							}
						}
					}
				}
				dr.Close();
			}
			return report;
		}

        public bool DeleteCustodyTransfer(CustodyTransfer request)
        {
            /*
             Procedure PROC_MANTEM_SOLICITACAO ( pID_SOLICITACAO       in number,
                                      pCD_CLIENTE           in number,
                                      pCD_NEGOCIO           in Varchar2,
                                      pCD_CARTEIRA_ORIGEM   in number,
                                      pCD_CORRETORA_DESTINO in number,
                                      pCD_CARTEIRA_DESTINO  in number,
                                      pDT_SOLICITACAO       in Varchar2,
                                      pCD_CLIENTE_DESTINO   in number,
                                      pPU_POSICAO           In Number,
                                      pQT_TRANSFERIR        in Number,
                                      pDS_OBSERVACAO        In varchar2,
                                      pIN_STATUS            In char,
                                      pTP_OPERACAO          In char,
                                      pCount                Out number
                                       ) is
             */

            var cmd = dbi.CreateCommand("PCK_PORTAL_TRANSFERENCIAS.PROC_MANTEM_SOLICITACAO", CommandType.StoredProcedure);
            cmd.Parameters.Clear();

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_SOLICITACAO", IsNullable = false, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = request.RequestId });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", IsNullable = false, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = request.ClientCode });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pTP_OPERACAO", IsNullable = false, OracleType = OracleType.Char, Direction = ParameterDirection.Input, Value = 'E' });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_STATUS", IsNullable = true, OracleType = OracleType.Char, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_NEGOCIO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CARTEIRA_ORIGEM", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CORRETORA_DESTINO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CARTEIRA_DESTINO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_SOLICITACAO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE_DESTINO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pPU_POSICAO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pQT_TRANSFERIR", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pDS_OBSERVACAO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCount", OracleType = OracleType.Number, Direction = ParameterDirection.Output });

            var rest = cmd.ExecuteNonQuery();

            return Convert.ToInt32(rest) > 0;
        }

        public List<CustodyTransferDetails> LoadClientPosition(CustodyTransferFilter filter, int? userId) 
        {
            List<CustodyTransferDetails> result = new List<CustodyTransferDetails>();
            var cmd = dbi.CreateCommand("PCK_PORTAL_TRANSFERENCIAS.PROC_CONSULTA_POSICAO", CommandType.StoredProcedure);
            cmd.Parameters.Clear();

            if (String.IsNullOrEmpty(filter.Assessor))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTASSESSOR", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTASSESSOR", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.Assessor });

            if (userId.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = userId.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", IsNullable = false, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = filter.ClientCode });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCursor", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {

                    result = new List<CustodyTransferDetails>();
                    while (dr.Read())
                    {
                        CustodyTransferDetails transfer = new CustodyTransferDetails();

                        if (!dr.IsDBNull(0)) transfer.ClientCode = dr.GetInt32(0); //cod_cli
                        if (!dr.IsDBNull(1)) transfer.StockCode = dr.GetString(1); //cod_neg
                        if (!dr.IsDBNull(2)) transfer.PortfolioCode = dr.GetInt32(2); //cod_cart
                        if (!dr.IsDBNull(3)) transfer.StockQtty = dr.GetDecimal(3); //qtde_disp
                        
                        result.Add(transfer);
                    }
                }

                dr.Close();

            }

            return result;
        }

        public bool InsertCustodyTransfer(CustodyTransfer request, out int requestId)
        {
            /*
             Procedure PROC_MANTEM_SOLICITACAO ( pID_SOLICITACAO       in number,
                                      pCD_CLIENTE           in number,
                                      pCD_NEGOCIO           in Varchar2,
                                      pCD_CARTEIRA_ORIGEM   in number,
                                      pCD_CORRETORA_DESTINO in number,
                                      pCD_CARTEIRA_DESTINO  in number,
                                      pDT_SOLICITACAO       in Varchar2,
                                      pCD_CLIENTE_DESTINO   in number,
                                      pPU_POSICAO           In Number,
                                      pQT_TRANSFERIR        in Number,
                                      pDS_OBSERVACAO        In varchar2,
                                      pIN_STATUS            In char,
                                      pTP_OPERACAO          In char,
                                      pCount                Out number
                                       ) is
             */

            var cmd = dbi.CreateCommand("PCK_PORTAL_TRANSFERENCIAS.PROC_MANTEM_SOLICITACAO", CommandType.StoredProcedure);
            cmd.Parameters.Clear();

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_SOLICITACAO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", IsNullable = false, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = request.ClientCode });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_STATUS", IsNullable = false, OracleType = OracleType.Char, Direction = ParameterDirection.Input, Value = 'A' });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pTP_OPERACAO", IsNullable = false, OracleType = OracleType.Char, Direction = ParameterDirection.Input, Value = 'I' });

            if (String.IsNullOrEmpty(request.StockCode))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_NEGOCIO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_NEGOCIO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = request.StockCode });

            if (request.OrigPortfolio <= 0)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CARTEIRA_ORIGEM", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CARTEIRA_ORIGEM", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = request.OrigPortfolio });

            if (request.DestinyBrokerCode <= 0)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CORRETORA_DESTINO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CORRETORA_DESTINO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = request.DestinyBrokerCode });

            if (request.DestinyPortfolio <= 0)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CARTEIRA_DESTINO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CARTEIRA_DESTINO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = request.DestinyPortfolio });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_SOLICITACAO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DateTime.Now.ToString("dd/MM/yyyy") });

            if (request.DestinyClientCode <= 0)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE_DESTINO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE_DESTINO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = request.DestinyClientCode });
                        
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pPU_POSICAO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
            
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pQT_TRANSFERIR", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = request.Qtty });

            
            if (!String.IsNullOrEmpty(request.Observations))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDS_OBSERVACAO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = request.Observations });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDS_OBSERVACAO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCount", OracleType = OracleType.Number, Direction = ParameterDirection.Output });

            var rest = cmd.ExecuteNonQuery();

            requestId = Convert.ToInt32(cmd.Parameters["pCount"].Value);

            return Convert.ToInt32(rest) > 0;
        }

        public bool EditCustodyTransfer(CustodyTransfer request)
        {
            
            var cmd = dbi.CreateCommand("PCK_PORTAL_TRANSFERENCIAS.PROC_MANTEM_SOLICITACAO", CommandType.StoredProcedure);
            cmd.Parameters.Clear();

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_SOLICITACAO", IsNullable = false, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = request.RequestId });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", IsNullable = false, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = request.ClientCode });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_STATUS", IsNullable = true, OracleType = OracleType.Char, Direction = ParameterDirection.Input, Value = DBNull.Value });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pTP_OPERACAO", IsNullable = false, OracleType = OracleType.Char, Direction = ParameterDirection.Input, Value = 'D' });

            if (String.IsNullOrEmpty(request.StockCode))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_NEGOCIO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_NEGOCIO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = request.StockCode });

            if (request.OrigPortfolio <= 0)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CARTEIRA_ORIGEM", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CARTEIRA_ORIGEM", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = request.OrigPortfolio });

            if (request.DestinyBrokerCode <= 0)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CORRETORA_DESTINO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CORRETORA_DESTINO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = request.DestinyBrokerCode });

            if (request.DestinyPortfolio <= 0)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CARTEIRA_DESTINO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CARTEIRA_DESTINO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = request.DestinyPortfolio });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_SOLICITACAO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });

            if (request.DestinyClientCode <= 0)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE_DESTINO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE_DESTINO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = request.DestinyClientCode });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pPU_POSICAO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pQT_TRANSFERIR", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = request.Qtty });


            if (!String.IsNullOrEmpty(request.Observations))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDS_OBSERVACAO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = request.Observations });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDS_OBSERVACAO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCount", OracleType = OracleType.Number, Direction = ParameterDirection.Output });

            var rest = cmd.ExecuteNonQuery();

            return Convert.ToInt32(rest) > 0;
        }

        public int GetPendentCustodyTransfers(CustodyTransferFilter filter, int? userId)
        {

            var cmd = dbi.CreateCommand("PCK_PORTAL_TRANSFERENCIAS.PROC_TRANSFERENCIAS_PENDENTES", CommandType.StoredProcedure);
            cmd.Parameters.Clear();
            
            if (String.IsNullOrEmpty(filter.Assessor))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTASSESSOR", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTASSESSOR", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.Assessor });

            if (String.IsNullOrEmpty(filter.ClientFilter))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTCLIENTE", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTCLIENTE", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.ClientFilter });

            if (userId.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = userId.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCount", OracleType = OracleType.Number, Direction = ParameterDirection.Output });

            cmd.ExecuteNonQuery();

            return (cmd.Parameters["pCount"].Value != null) ? Convert.ToInt32(cmd.Parameters["pCount"].Value) : 0;
        }
    }
}
