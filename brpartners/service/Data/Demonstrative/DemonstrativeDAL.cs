﻿using QX3.Portal.Contracts.DataContracts;
using QX3.Spinnex.Common.Services.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OracleClient;
using System.Data.SqlClient;

namespace QX3.Portal.Services.Data
{
    public class DemonstrativeDAL : DataProviderBase
    {
        public DemonstrativeDAL() : base("PortalConnectionString") { }

        public ExtractRendaResponse Extract(string CodEmpresa, string Cnpjcpf, string DtRefDe, string DtRefAte)
        {

            ExtractRendaResponse er = new ExtractRendaResponse();
            ExtractRenda e = new ExtractRenda();
            List<ExtractRenda> le = new List<ExtractRenda>();

            DBInterface dbiCRK = new DBInterface("CRK");

            var cmd = (SqlCommand)dbiCRK.CreateCommand("CRK..SP_REN_EXTRATO", CommandType.StoredProcedure);

            cmd.Parameters.Clear();

            cmd.Parameters.Add(new SqlParameter { ParameterName = "@tp_codempresa", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input, Value = Convert.ToInt32(CodEmpresa) });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@cd_cpf_cnpj", SqlDbType = SqlDbType.Decimal, Direction = ParameterDirection.Input, Value = Convert.ToDecimal(Cnpjcpf) });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@dt_ref_de", SqlDbType = SqlDbType.Date, Direction = ParameterDirection.Input, Value = Convert.ToDateTime(DtRefDe) });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@dt_ref_ate", SqlDbType = SqlDbType.Date, Direction = ParameterDirection.Input, Value = Convert.ToDateTime(DtRefAte) });

            cmd.Connection.Open();

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {

                    while (dr.Read())
                    {
                        e = new ExtractRenda();

                        // e.tp_registro = dr["tp_registro"] != DBNull.Value ? dr["tp_registro"].ToString() : "";
                        e.dt_operacao = dr["dt_operacao"] != DBNull.Value ? dr["dt_operacao"].ToString() : "";
                        e.dc_evento = dr["dc_evento"] != DBNull.Value ? dr["dc_evento"].ToString() : "";
                        e.dc_grupo_papel = dr["dc_grupo_papel"] != DBNull.Value ? dr["dc_grupo_papel"].ToString() : "";
                        e.dc_indexador = dr["dc_indexador"] != DBNull.Value ? dr["dc_indexador"].ToString() : "";
                        e.tx_operacao = dr["tx_operacao"] != DBNull.Value ? Convert.ToDecimal(dr["tx_operacao"]) : 0;
                        e.tx_percentual = dr["tx_percentual"] != DBNull.Value ? Convert.ToDecimal(dr["tx_percentual"]) : 0;
                        e.dt_vencimento = dr["dt_vencimento"] != DBNull.Value ? dr["dt_vencimento"].ToString() : "";
                        e.vl_operacao = dr["vl_operacao"] != DBNull.Value ? Convert.ToDecimal(dr["vl_operacao"]) : 0;
                        e.tx_ir = dr["tx_ir"] != DBNull.Value ? Convert.ToDecimal(dr["tx_ir"]) : 0;
                        e.tx_iof = dr["tx_iof"] != DBNull.Value ? Convert.ToDecimal(dr["tx_iof"]) : 0;
                        e.vl_bruto = dr["vl_bruto"] != DBNull.Value ? Convert.ToDecimal(dr["vl_bruto"]) : 0;
                        e.vl_liquido = dr["vl_liquido"] != DBNull.Value ? Convert.ToDecimal(dr["vl_liquido"]) : 0;

                        le.Add(e);
                    }
                }
                dr.Close();
            }


            er.Extrato = le;

            return er;

        }

        public List<ExtractFunction> ExtractFunction(string codEmpresa, string dtRefDe, string dtRefAte, string cnpjcpf, string userName)
        {
            ExtractNDFResponse bnr = new ExtractNDFResponse();

            ExtractNDF t = new ExtractNDF();
            List<ExtractNDF> lt = new List<ExtractNDF>();

            DBInterface dbiCRK = new DBInterface("F_ATIVO");

            var cmd = (SqlCommand)dbiCRK.CreateCommand("F_ATIVO..FI_SP_RLAtacado_MTRCVMODV16", CommandType.StoredProcedure);
            cmd.Parameters.Clear();

            /*exec FI_SP_RLAtacado_MTRCVMODV16
            @DTREF = '2016-10-03 00:00:00',
            @IMPRIMEPARC = 0,
            @DTINI = '1950-01-01 00:00:00',
            @DTFIM = '2050-12-31 00:00:00',
            @SITCONTABIL = 7,
            @POSICAO = 1,
            @CEDIDO = 2,
            @PCIMPRCED = 0,
            @CONVENIO = NULL,
            @CESSAO = NULL,
            @TPANALITICO = 0,
            @ANALITICOPOR = 0,
            @ORDEMEMISSAO = 0,
            @CNPJ = N'476.521.019-72',
            @FILTROPADRAO = NULL,
            @CTMODAS = N'011',
            @NRCCB = NULL,
            @CODSIS = N'009',
            @FIANCA = 0,
            @USUARIO = N'ROMULO.CANO'*/



            /*cmd.Parameters.Add(new SqlParameter { ParameterName = "@DTREF", SqlDbType = SqlDbType.DateTime, Direction = ParameterDirection.Input, Value = DateTime.Parse("2016-10-03 00:00:00") });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@IMPRIMEPARC", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input, Value = 0 });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@DTINI", SqlDbType = SqlDbType.DateTime, Direction = ParameterDirection.Input, Value = DateTime.Parse("1950-01-01 00:00:00") });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@DTFIM", SqlDbType = SqlDbType.DateTime, Direction = ParameterDirection.Input, Value = DateTime.Parse("2050-12-31 00:00:00") });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@SITCONTABIL", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input, Value = 7});
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@POSICAO", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input, Value = 1 });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@CEDIDO", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input, Value = 2 });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@PCIMPRCED", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input, Value = 0 });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@CONVENIO", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@CESSAO", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@TPANALITICO", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input, Value = 0 });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@ANALITICOPOR", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input, Value = 0 });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@ORDEMEMISSAO", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input, Value = 0 });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@CNPJ", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = "476.521.019-72" });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@FILTROPADRAO", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@CTMODAS", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = "011" });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@NRCCB", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@CODSIS", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = "009" });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@FIANCA", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input, Value = 0 });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@USUARIO", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = "ROMULO.CANO" });
             */

            cmd.Parameters.Add(new SqlParameter { ParameterName = "@DTREF", SqlDbType = SqlDbType.DateTime, Direction = ParameterDirection.Input, Value = DateTime.Now });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@IMPRIMEPARC", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input, Value = 0 });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@DTINI", SqlDbType = SqlDbType.DateTime, Direction = ParameterDirection.Input, Value = DateTime.Parse(dtRefDe) });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@DTFIM", SqlDbType = SqlDbType.DateTime, Direction = ParameterDirection.Input, Value = DateTime.Parse(dtRefAte) });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@SITCONTABIL", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input, Value = 7 });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@POSICAO", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input, Value = 1 });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@CEDIDO", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input, Value = 2 });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@PCIMPRCED", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input, Value = 0 });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@CONVENIO", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@CESSAO", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@TPANALITICO", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input, Value = 0 });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@ANALITICOPOR", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input, Value = 0 });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@ORDEMEMISSAO", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input, Value = 0 });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@CNPJ", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = cnpjcpf });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@FILTROPADRAO", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@CTMODAS", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = "011" });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@NRCCB", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@CODSIS", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = "009" });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@FIANCA", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input, Value = 0 });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@USUARIO", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = userName });

            cmd.Connection.Open();

            var list = new List<ExtractFunction>();

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        var row = new ExtractFunction();
                        var PANRPARC = dr["PANRPARC"] == DBNull.Value ? null : dr["PANRPARC"].ToString();
                        var MDSIMB = dr["MDSIMB"] == DBNull.Value ? null : dr["MDSIMB"].ToString();

                        row.OPDTBASE = dr["OPDTBASE"] == DBNull.Value ? null : dr["OPDTBASE"].ToString();
                        row.PADTVCTO = dr["PADTVCTO"] == DBNull.Value ? null : dr["PADTVCTO"].ToString();
                        row.PANROPER = dr["PANROPER"] == DBNull.Value ? null : dr["PANROPER"].ToString();
                        row.OPQTDDPARC_PANRPARC = dr["OPQTDDPARC"] == DBNull.Value ? null : dr["OPQTDDPARC"].ToString() + "/" + PANRPARC;
                        row.CLNOMECLI = dr["CLNOMECLI"] == DBNull.Value ? null : dr["CLNOMECLI"].ToString();
                        row.PATXJRAP = dr["PATXJRAP"] == DBNull.Value ? null : dr["PATXJRAP"].ToString();
                        row.OPDTBASE = dr["OPDTBASE"] == DBNull.Value ? null : dr["OPDTBASE"].ToString();
                        row.OPCODMD_MDSIMB = dr["OPCODMD"] == DBNull.Value ? null : dr["OPCODMD"].ToString() + "/" + MDSIMB;
                        row.SLDDATA = dr["SLDDATA"] == DBNull.Value ? null : dr["SLDDATA"].ToString();
                        row.VLRFINAL = dr["VLRFINAL"] == DBNull.Value ? null : dr["VLRFINAL"].ToString();
                        row.SLDMORA = dr["SLDMORA"] == DBNull.Value ? null : dr["SLDMORA"].ToString();
                        row.SLDMULTA = dr["SLDMULTA"] == DBNull.Value ? null : dr["SLDMULTA"].ToString();
                        row.SLDIOF = dr["SLDIOF"] == DBNull.Value ? null : dr["SLDIOF"].ToString();
                        row.SLDDESPTAR = dr["SLDDESPTAR"] == DBNull.Value ? null : dr["SLDDESPTAR"].ToString();
                        row.SLDDEV = dr["SLDDEV"] == DBNull.Value ? null : dr["SLDDEV"].ToString();
                        list.Add(row);
                    }
                }
            }
            return list;
        }

        public TradingSummaryResponse ListTradingSummary(int cdCliente, string cdAtivo, DateTime dtInicial, DateTime dtFinal)
        {

            TradingSummaryResponse tsr = new TradingSummaryResponse();

            TradingSummary t = new TradingSummary();
            List<TradingSummary> lt = new List<TradingSummary>();

            var cmd = dbi.CreateCommand("PCK_MOVIMENTACAO.PROC_MOV_RESUMO_NEG", CommandType.StoredProcedure);
            cmd.Parameters.Clear();


            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = cdCliente });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_NEGOCIO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = cdAtivo });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_INICIAL", OracleType = OracleType.DateTime, Direction = ParameterDirection.Input, Value = dtInicial });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_FINAL", OracleType = OracleType.DateTime, Direction = ParameterDirection.Input, Value = dtFinal });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pRESUMO", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        t = new TradingSummary();

                        t.Data = dr["DT_NEGOCIO"] != DBNull.Value ? Convert.ToDateTime(dr["DT_NEGOCIO"]) : DateTime.MinValue;

                        if (Convert.ToString(dr["CD_NATOPE"]) == "V")
                        {
                            t.QtdVenda = dr["QTDE"] != DBNull.Value ? Convert.ToInt32(dr["QTDE"]) : 0;
                            t.TotalFinVenda = dr["SOMA_TOTALNEG"] != DBNull.Value ? Convert.ToDecimal(dr["SOMA_TOTALNEG"]) : 0;
                        }
                        else
                        {
                            t.QtdCompra = dr["QTDE"] != DBNull.Value ? Convert.ToInt32(dr["QTDE"]) : 0;
                            t.TotalFinCompra = dr["SOMA_TOTALNEG"] != DBNull.Value ? Convert.ToDecimal(dr["SOMA_TOTALNEG"]) : 0;
                        }

                        t.Preco = dr["PRECO"] != DBNull.Value ? Convert.ToDecimal(dr["PRECO"]) : 0;

                        t.FinanceiroLiquido = t.TotalFinVenda - t.TotalFinCompra;

                        lt.Add(t);
                    }


                }
                dr.Close();
            }

            tsr.ResumoNegociacao = lt;

            return tsr;

        }

        public ProceedsDemonstrativeResponse ListProceeds(int cdCliente, string provisionados, DateTime dtInicial, DateTime dtFinal)
        {
            ProceedsDemonstrativeResponse pdr = new ProceedsDemonstrativeResponse();

            pdr.Tipo = provisionados;
            pdr.dtInicial = dtInicial;
            pdr.dtFinal = dtFinal;

            ProceedsDemonstrative t = new ProceedsDemonstrative();
            List<ProceedsDemonstrative> lt = new List<ProceedsDemonstrative>();

            var cmd = dbi.CreateCommand("PCK_MOVIMENTACAO.PROC_MOV_PROVENTOS", CommandType.StoredProcedure);
            cmd.Parameters.Clear();


            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = cdCliente });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_PAGO", OracleType = OracleType.Char, Direction = ParameterDirection.Input, Value = provisionados });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_INICIAL", OracleType = OracleType.DateTime, Direction = ParameterDirection.Input, Value = dtInicial });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_FINAL", OracleType = OracleType.DateTime, Direction = ParameterDirection.Input, Value = dtFinal });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pPROVENTOS", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        t = new ProceedsDemonstrative();

                        t.Pagamento = dr["data_pgto_divi"] != DBNull.Value ? Convert.ToDateTime(dr["data_pgto_divi"]) : DateTime.MinValue;
                        t.Ativo = dr["cod_neg"] != DBNull.Value ? Convert.ToString(dr["cod_neg"]) : "";
                        t.Empresa = dr["nome_emp_emi"] != DBNull.Value ? Convert.ToString(dr["nome_emp_emi"]) : "";
                        t.Provento = dr["desc_tipo_prov"] != DBNull.Value ? Convert.ToString(dr["desc_tipo_prov"]) : "";
                        t.ValorBruto = dr["val_prov"] != DBNull.Value ? Convert.ToDecimal(dr["val_prov"]) : 0;
                        t.PercIr = dr["perc_ir_divi"] != DBNull.Value ? Convert.ToDecimal(dr["perc_ir_divi"]) : 0;
                        t.ValorLiquido = dr["vl_liquido"] != DBNull.Value ? Convert.ToDecimal(dr["vl_liquido"]) : 0;

                        lt.Add(t);
                    }


                }
                dr.Close();
            }

            pdr.Proventos = lt;

            return pdr;

        }

        public ExtractNDFResponse ListExtractNDF(string CodEmpresa, string Cnpjcpf, int Mes, int Ano)
        {
            ExtractNDFResponse bnr = new ExtractNDFResponse();

            ExtractNDF t = new ExtractNDF();
            List<ExtractNDF> lt = new List<ExtractNDF>();

            DBInterface dbiCRK = new DBInterface("CRK");

            var cmd = (SqlCommand)dbiCRK.CreateCommand("CRK..SP_NDF_EXTRATO_CONTABIL", CommandType.StoredProcedure);
            cmd.Parameters.Clear();

            cmd.Parameters.Add(new SqlParameter { ParameterName = "@dt_ano", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input, Value = Ano });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@dt_mes", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input, Value = Mes });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@cd_cpf_cnpj", SqlDbType = SqlDbType.Decimal, Direction = ParameterDirection.Input, Value = Convert.ToDecimal(Cnpjcpf) });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@tp_codempresa", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input, Value = Convert.ToInt32(CodEmpresa) });

            cmd.Connection.Open();

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        t = new ExtractNDF();

                        t.tp_operacao = dr["tp_operacao"] != DBNull.Value ? dr["tp_operacao"].ToString() : "";
                        t.dt_operacao = dr["dt_operacao"] != DBNull.Value ? dr["dt_operacao"].ToString() : "";
                        t.dt_vencimento = dr["dt_vencimento"] != DBNull.Value ? dr["dt_vencimento"].ToString() : "";
                        t.vl_base_moeda_estr = dr["vl_base_moeda_estr"] != DBNull.Value ? Convert.ToDecimal(dr["vl_base_moeda_estr"]) : 0;
                        t.tp_moeda_ref = dr["tp_moeda_ref"] != DBNull.Value ? dr["tp_moeda_ref"].ToString() : "";
                        t.tp_moeda_cot = dr["tp_moeda_cot"] != DBNull.Value ? dr["tp_moeda_cot"].ToString() : "";
                        t.dc_criterio_termo = dr["dc_criterio_termo"] != DBNull.Value ? dr["dc_criterio_termo"].ToString() : "";
                        t.vl_cotacao = dr["vl_cot_termo_fwd"] != DBNull.Value ? Convert.ToDecimal(dr["vl_cot_termo_fwd"]) : 0;
                        t.vl_bruto = dr["vl_bruto"] != DBNull.Value ? Convert.ToDecimal(dr["vl_bruto"]) : 0;
                        t.vl_ir = dr["vl_ir"] != DBNull.Value ? Convert.ToDecimal(dr["vl_ir"]) : 0;
                        t.vl_liquido = dr["vl_liquido"] != DBNull.Value ? Convert.ToDecimal(dr["vl_liquido"]) : 0;

                        lt.Add(t);
                    }

                }
                dr.Close();
            }

            bnr.ExtratoNDF = lt;

            cmd.Connection.Close();

            return bnr;

        }

        public ExtractSWPResponse ListExtractSWP(string CodEmpresa, string Cnpjcpf, int mes, int ano)
        {
            ExtractSWPResponse bnr = new ExtractSWPResponse();

            ExtractSWP t = new ExtractSWP();
            List<ExtractSWP> lt = new List<ExtractSWP>();

            DBInterface dbiCRK = new DBInterface("CRK");

            var cmd = (SqlCommand)dbiCRK.CreateCommand("CRK..SP_SWP_EXTRATO_CONTABIL", CommandType.StoredProcedure);
            cmd.Parameters.Clear();

            cmd.Parameters.Add(new SqlParameter { ParameterName = "@dt_ano", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input, Value = ano });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@dt_mes", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input, Value = mes });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@cd_cpf_cnpj", SqlDbType = SqlDbType.Decimal, Direction = ParameterDirection.Input, Value = Convert.ToDecimal(Cnpjcpf) });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@tp_codempresa", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input, Value = Convert.ToInt32(CodEmpresa) });

            cmd.Connection.Open();

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        t = new ExtractSWP();

                        t.tp_operacao = dr["tp_operacao"] != DBNull.Value ? dr["tp_operacao"].ToString() : "";
                        t.dt_operacao = dr["dt_operacao"] != DBNull.Value ? dr["dt_operacao"].ToString() : "";
                        t.dt_vencimento = dr["dt_vencimento"] != DBNull.Value ? dr["dt_vencimento"].ToString() : "";
                        t.vl_base = dr["vl_base"] != DBNull.Value ? Convert.ToDecimal(dr["vl_base"]) : 0;
                        t.dc_index_dado = dr["dc_index_dado"] != DBNull.Value ? dr["dc_index_dado"].ToString() : "";
                        t.vl_perc_index_data = dr["vl_perc_index_dado"] != DBNull.Value ? Convert.ToDecimal(dr["vl_perc_index_dado"]) : 0;
                        t.vl_taxa_dada = dr["vl_taxa_dado"] != DBNull.Value ? Convert.ToDecimal(dr["vl_taxa_dado"]) : 0;
                        t.dc_index_tomado = dr["dc_index_tomado"] != DBNull.Value ? dr["dc_index_tomado"].ToString() : "";
                        t.vl_perc_index_tomado = dr["vl_perc_index_tomado"] != DBNull.Value ? Convert.ToDecimal(dr["vl_perc_index_tomado"]) : 0;
                        t.vl_taxa_tomado = dr["vl_taxa_tomado"] != DBNull.Value ? Convert.ToDecimal(dr["vl_taxa_tomado"]) : 0;
                        t.vl_curva_dado = dr["vl_curva_dado"] != DBNull.Value ? Convert.ToDecimal(dr["vl_curva_dado"]) : 0;
                        t.vl_curva_tomado = dr["vl_curva_tomado"] != DBNull.Value ? Convert.ToDecimal(dr["vl_curva_tomado"]) : 0;
                        t.vl_bruto = dr["vl_bruto"] != DBNull.Value ? Convert.ToDecimal(dr["vl_bruto"]) : 0;
                        t.vl_ir = dr["vl_ir"] != DBNull.Value ? Convert.ToDecimal(dr["vl_ir"]) : 0;
                        t.vl_liquido = dr["vl_liquido"] != DBNull.Value ? Convert.ToDecimal(dr["vl_liquido"]) : 0;

                        lt.Add(t);
                    }

                }
                dr.Close();
            }

            bnr.ExtratoSWP = lt;

            cmd.Connection.Close();

            return bnr;

        }

        public BrokerageNoteResponse ListBrokeragenotesDetails(int cdCliente, int nrNota, DateTime dtNegocio)
        {
            BrokerageNoteResponse bnr = new BrokerageNoteResponse();
            BrokerageNoteHeader bnh = new BrokerageNoteHeader();
            BrokerageNoteSummary bns = new BrokerageNoteSummary();

            var cmd = dbi.CreateCommand("PCK_MOVIMENTACAO.PROC_MOV_NOTA_RESUMO", CommandType.StoredProcedure);
            cmd.Parameters.Clear();


            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = cdCliente });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pNR_NOTA", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = nrNota });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_EMPRESA", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = ConfigurationManager.AppSettings["CD_EMPRESA"] });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_NEGOCIO", OracleType = OracleType.DateTime, Direction = ParameterDirection.Input, Value = dtNegocio });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pNOTA", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    if (dr.Read())
                    {
                        bnh.NomeCliente = dr["NM_CLIENTE"] != DBNull.Value ? Convert.ToString(dr["NM_CLIENTE"]) : "";
                        bnh.Logradouro = dr["NM_LOGRADOURO"] != DBNull.Value ? Convert.ToString(dr["NM_LOGRADOURO"]) : "";
                        bnh.NrLogradouro = dr["NR_PREDIO"] != DBNull.Value ? Convert.ToString(dr["NR_PREDIO"]) : "";
                        bnh.Complemento = dr["NM_COMPENDE"] != DBNull.Value ? Convert.ToString(dr["NM_COMPENDE"]) : "";
                        bnh.Bairro = dr["NM_BAIRRO"] != DBNull.Value ? Convert.ToString(dr["NM_BAIRRO"]) : "";
                        bnh.Cidade = dr["NM_CIDADE"] != DBNull.Value ? Convert.ToString(dr["NM_CIDADE"]) : "";
                        bnh.Uf = dr["SG_ESTADO"] != DBNull.Value ? Convert.ToString(dr["SG_ESTADO"]) : "";
                        bnh.Cep = dr["NR_CEP"] != DBNull.Value ? Convert.ToString(dr["NR_CEP"]) : "";

                        bnh.Cpf = dr["CD_CPFCGC"] != DBNull.Value ? Convert.ToDecimal(dr["CD_CPFCGC"]) : 0;
                        bnh.CdCliente = dr["CD_CLIENTE"] != DBNull.Value ? Convert.ToInt32(dr["CD_CLIENTE"]) : 0;
                        bnh.NrNota = dr["NR_NOTA"] != DBNull.Value ? Convert.ToInt32(dr["NR_NOTA"]) : 0;
                        bnh.DtPregao = dr["DT_NEGOCIO_FILTRO"] != DBNull.Value ? Convert.ToDateTime(dr["DT_NEGOCIO_FILTRO"]) : DateTime.MinValue;

                        //negociação
                        bns.VlDebentures = dr["VL_DEBENT"] != DBNull.Value ? Convert.ToDecimal(dr["VL_DEBENT"]) : 0;
                        bns.VlVendasVista = dr["VL_VDAVIS"] != DBNull.Value ? Convert.ToDecimal(dr["VL_VDAVIS"]) : 0;
                        bns.VlComprasVista = dr["VL_CPAVIS"] != DBNull.Value ? Convert.ToDecimal(dr["VL_CPAVIS"]) : 0;
                        bns.VlVendasOpc = dr["VL_VDAOPC"] != DBNull.Value ? Convert.ToDecimal(dr["VL_VDAOPC"]) : 0;
                        bns.VlComprasOpc = dr["VL_CPAOPC"] != DBNull.Value ? Convert.ToDecimal(dr["VL_CPAOPC"]) : 0;
                        bns.VlTermo = dr["VL_TERMO"] != DBNull.Value ? Convert.ToDecimal(dr["VL_TERMO"]) : 0;
                        bns.VlTitulos = dr["VL_TITPUB"] != DBNull.Value ? Convert.ToDecimal(dr["VL_TITPUB"]) : 0;
                        bns.VlOperacoes = dr["VL_TOTNEG"] != DBNull.Value ? Convert.ToDecimal(dr["VL_TOTNEG"]) : 0;

                        //cblc
                        bns.VlLiquidoOperacoes = dr["VL_TOT_VALLIQOPER"] != DBNull.Value ? Convert.ToDecimal(dr["VL_TOT_VALLIQOPER"]) : 0;
                        bns.VlLiquidoOperacoesDC = bns.VlLiquidoOperacoes < 0 ? "D" : "C";

                        bns.TaxaLiquidacao = dr["VL_EMOLUM_CB"] != DBNull.Value ? Convert.ToDecimal(dr["VL_EMOLUM_CB"]) : 0;
                        bns.TaxaLiquidacaoDC = "D"; //dr["VL_LIQFAT_DC"] != DBNull.Value ? Convert.ToString(dr["VL_LIQFAT_DC"]) : "";

                        bns.TaxaRegistro = dr["VL_TAXREG_CB"] != DBNull.Value ? Convert.ToDecimal(dr["VL_TAXREG_CB"]) : 0;
                        bns.TaxaRegistroDC = "D"; //dr["VL_LIQFAT_DC"] != DBNull.Value ? Convert.ToString(dr["VL_LIQFAT_DC"]) : "";

                        bns.TotalCBLC = dr["VL_TOT_CBLC"] != DBNull.Value ? Convert.ToDecimal(dr["VL_TOT_CBLC"]) : 0;
                        bns.TotalCBLCDC = dr["VL_LIQNOT_CB_DC"] != DBNull.Value ? Convert.ToString(dr["VL_LIQNOT_CB_DC"]) : "";

                        //BOVESPA/SOMA
                        bns.TaxaTermoOpc = dr["VL_TAXREG_BV"] != DBNull.Value ? Convert.ToDecimal(dr["VL_TAXREG_BV"]) : 0;
                        bns.TaxaANA = dr["VL_TAXANA"] != DBNull.Value ? Convert.ToDecimal(dr["VL_TAXANA"]) : 0;
                        bns.Emolumentos = dr["VL_EMOLUM_BV"] != DBNull.Value ? Convert.ToDecimal(dr["VL_EMOLUM_BV"]) : 0;
                        bns.TotalBovespaSoma = dr["VL_TOT_BOV_SOM"] != DBNull.Value ? Convert.ToDecimal(dr["VL_TOT_BOV_SOM"]) : 0;

                        //CORRETAGEM / DESPESAS
                        bns.Corretagem = dr["VL_VALCOR"] != DBNull.Value ? Convert.ToDecimal(dr["VL_VALCOR"]) : 0;
                        bns.ISS = dr["VL_ISS"] != DBNull.Value ? Convert.ToDecimal(dr["VL_ISS"]) : 0;
                        bns.IRRF = dr["VL_IRRF"] != DBNull.Value ? Convert.ToDecimal(dr["VL_IRRF"]) : 0;
                        bns.VlBaseIR = dr["VL_BASEIR"] != DBNull.Value ? Convert.ToDecimal(dr["VL_BASEIR"]) : 0;
                        bns.Outras = 0; //dr["VL_TAXREG_BV"] != DBNull.Value ? Convert.ToDecimal(dr["VL_TAXREG_BV"]) : 0;
                        bns.TotalCorretagem = dr["VL_TOT_COR_DES"] != DBNull.Value ? Convert.ToDecimal(dr["VL_TOT_COR_DES"]) : 0;
                        bns.Liquido = dr["VL_LIQNOT"] != DBNull.Value ? Convert.ToDecimal(dr["VL_LIQNOT"]) : 0;
                        bns.LiquidoDC = dr["VL_LIQNOT_DC"] != DBNull.Value ? Convert.ToString(dr["VL_LIQNOT_DC"]) : "";
                        bns.LiquidoDt = dr["DT_DATPRO"] != DBNull.Value ? Convert.ToString(dr["DT_DATPRO"]) : "";

                        bns.TpNegocio = dr["TP_NEGOCIO"] != DBNull.Value ? Convert.ToString(dr["TP_NEGOCIO"]) : "";

                        bns.VlBaseIrDT = dr["VL_BASEIRDT"] != DBNull.Value ? Convert.ToDecimal(dr["VL_BASEIRDT"]) : 0;
                        bns.VlBaseIrOper = 0;// dr["VL_BASEIROPER"] != DBNull.Value ? Convert.ToDecimal(dr["VL_BASEIROPER"]) : 0;

                        bns.EspecDivEstorno = dr["MSG_ESPEC"] != DBNull.Value ? Convert.ToString(dr["MSG_ESPEC"]) : "";

                    }


                }
                dr.Close();
            }

            bnr.Cabecalho = bnh;
            bnr.Resumo = bns;

            return bnr;

        }

        public BrokerageNoteResponse ListBrokeragenotesNegociosDia(int cdCliente, DateTime dtNegocio, string tpNegocio)
        {
            BrokerageNoteResponse bnr = new BrokerageNoteResponse();
            BrokerageNoteBody bnb = new BrokerageNoteBody();

            List<BrokerageNoteBody> lbnb = new List<BrokerageNoteBody>();

            var cmd = dbi.CreateCommand("PCK_MOVIMENTACAO.PROC_MOV_NOTA_NEG", CommandType.StoredProcedure);
            cmd.Parameters.Clear();

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = cdCliente });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_NEGOCIO_FILTRO", OracleType = OracleType.DateTime, Direction = ParameterDirection.Input, Value = dtNegocio });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pTP_NEGOCIO", OracleType = OracleType.Char, Direction = ParameterDirection.Input, Value = tpNegocio });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_EMPRESA", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = ConfigurationManager.AppSettings["CD_EMPRESA"] });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pNOTA", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        bnb = new BrokerageNoteBody();

                        bnb.Negociacao = dr["PRACA"] != DBNull.Value ? Convert.ToString(dr["PRACA"]) : "";
                        bnb.NatOp = dr["COMPVEND"] != DBNull.Value ? Convert.ToString(dr["COMPVEND"]) : "";
                        bnb.TipoMercado = dr["MERCADO"] != DBNull.Value ? Convert.ToString(dr["MERCADO"]) : "";

                        bnb.Especificacao = dr["TP_ESPECIF"] != DBNull.Value ? Convert.ToString(dr["TP_ESPECIF"]) : "";
                        bnb.Obs = dr["IN_NEGOCIO"] != DBNull.Value ? Convert.ToString(dr["IN_NEGOCIO"]) : "";

                        bnb.Quatidade = dr["QTDE"] != DBNull.Value ? Convert.ToInt32(dr["QTDE"]) : 0;
                        bnb.Preco = dr["PRECO"] != DBNull.Value ? Convert.ToDecimal(dr["PRECO"]) : 0;
                        bnb.Valor = dr["VOLUME"] != DBNull.Value ? Convert.ToDecimal(dr["VOLUME"]) : 0;
                        bnb.DC = dr["DEBCRE"] != DBNull.Value ? Convert.ToString(dr["DEBCRE"]) : "";

                        bnb.Qualificado = dr["IN_QUALIFICADO"] != DBNull.Value ? Convert.ToString(dr["IN_QUALIFICADO"]) : "";

                        lbnb.Add(bnb);
                    }


                }
                dr.Close();
            }

            bnr.Corpo = lbnb;

            return bnr;

        }

        public BrokerageNoteResponse ListBrokeragenotesNegociosHist(int cdCliente, DateTime dtNegocio, string tpNegocio)
        {
            BrokerageNoteResponse bnr = new BrokerageNoteResponse();
            BrokerageNoteBody bnb = new BrokerageNoteBody();

            List<BrokerageNoteBody> lbnb = new List<BrokerageNoteBody>();

            var cmd = dbi.CreateCommand("PCK_MOVIMENTACAO.PROC_MOV_NOTA_NEG", CommandType.StoredProcedure);
            cmd.Parameters.Clear();

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = cdCliente });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_NEGOCIO_FILTRO", OracleType = OracleType.DateTime, Direction = ParameterDirection.Input, Value = dtNegocio });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pTP_NEGOCIO", OracleType = OracleType.Char, Direction = ParameterDirection.Input, Value = tpNegocio });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_EMPRESA", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = ConfigurationManager.AppSettings["CD_EMPRESA"] });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pNOTA", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        bnb = new BrokerageNoteBody();
                        bnb.Negociacao = dr["PRACA"] != DBNull.Value ? Convert.ToString(dr["PRACA"]) : "";
                        bnb.NatOp = dr["COMPVEND"] != DBNull.Value ? Convert.ToString(dr["COMPVEND"]) : "";
                        bnb.TipoMercado = dr["MERCADO"] != DBNull.Value ? Convert.ToString(dr["MERCADO"]) : "";

                        bnb.Especificacao = dr["TP_ESPECIF"] != DBNull.Value ? Convert.ToString(dr["TP_ESPECIF"]) : "";
                        bnb.Obs = dr["IN_NEGOCIO"] != DBNull.Value ? Convert.ToString(dr["IN_NEGOCIO"]) : "";

                        bnb.Quatidade = dr["QTDE"] != DBNull.Value ? Convert.ToInt32(dr["QTDE"]) : 0;
                        bnb.Preco = dr["PRECO"] != DBNull.Value ? Convert.ToDecimal(dr["PRECO"]) : 0;
                        bnb.Valor = dr["VOLUME"] != DBNull.Value ? Convert.ToDecimal(dr["VOLUME"]) : 0;
                        bnb.DC = dr["DEBCRE"] != DBNull.Value ? Convert.ToString(dr["DEBCRE"]) : "";

                        bnb.Qualificado = dr["IN_QUALIFICADO"] != DBNull.Value ? Convert.ToString(dr["IN_QUALIFICADO"]) : "";

                        lbnb.Add(bnb);
                    }


                }
                dr.Close();
            }

            bnr.Corpo = lbnb;

            return bnr;

        }
    }

}

