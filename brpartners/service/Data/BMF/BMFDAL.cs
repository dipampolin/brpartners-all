﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QX3.Spinnex.Common.Services.Data;
using QX3.Portal.Contracts.DataContracts;
using System.Data.OracleClient;
using System.Data;

namespace QX3.Portal.Services.Data.BMF
{
    public class BMFDAL: DataProviderBase
    {
        public BMFDAL(): base("PortalConnectionString") { }

        #region Repasse de Corretagem

        public List<BovespaBrokerageReport> LoadBovespaBrokerageReport(BrokerageReportFilter filter) 
        {
            
            List<BovespaBrokerageReport> result = null;

            var cmd = dbi.CreateCommand("PCK_RELATORIO_CORRETAGEM.PROC_REPASSE_BOVESPA", CommandType.StoredProcedure);
            cmd.Parameters.Clear();

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = filter.ClientCode });

            if (filter.InitialRequest.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_INICIO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.InitialRequest.Value.ToString("dd/MM/yyyy") });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_INICIO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });


            if (filter.FinalRequest.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_FIM", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.FinalRequest.Value.ToString("dd/MM/yyyy") });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_FIM", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });

            if (String.IsNullOrEmpty(filter.AssessorFilter))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTASSESSOR", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTASSESSOR", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.AssessorFilter });

            if (String.IsNullOrEmpty(filter.ClientFilter))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTCLIENTE", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTCLIENTE", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.ClientFilter });

            if (filter.UserID.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = filter.UserID.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCursor", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var reader = cmd.ExecuteReader()) 
            {
                if (reader.HasRows) 
                {
                    result = new List<BovespaBrokerageReport>();
                    while (reader.Read()) 
                    { 
                        
                        BovespaBrokerageReport obj = new BovespaBrokerageReport();

                        if (!reader.IsDBNull(0)) obj.ClientCode = reader.GetInt32(0); //cd_cliente
                        if (!reader.IsDBNull(1)) obj.GrossBrokerage = reader.GetDecimal(1); //sum(a.vl_cortot) vl_corretagem_bruta
                        if (!reader.IsDBNull(2)) obj.NetBrokerage = reader.GetDecimal(2); //sum(a.vl_valcor) vl_corretagem_liquida
                        if (!reader.IsDBNull(3)) obj.DiscountValue = reader.GetDecimal(3); //sum(a.vl_descon) vl_desconto

                        result.Add(obj);
                    }
                }
            }

            return result;
        }

        public List<BMFBrokerageReport> LoadBMFBrokerageReport(BrokerageReportFilter filter)
        {

            List<BMFBrokerageReport> result = null;

            var cmd = dbi.CreateCommand("PCK_RELATORIO_CORRETAGEM.PROC_REPASSE_BMF", CommandType.StoredProcedure);
            cmd.Parameters.Clear();

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = filter.ClientCode });

            if (filter.InitialRequest.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_INICIO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.InitialRequest.Value.ToString("dd/MM/yyyy") });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_INICIO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });


            if (filter.FinalRequest.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_FIM", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.FinalRequest.Value.ToString("dd/MM/yyyy") });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_FIM", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });

            if (String.IsNullOrEmpty(filter.AssessorFilter))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTASSESSOR", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTASSESSOR", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.AssessorFilter });

            if (String.IsNullOrEmpty(filter.ClientFilter))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTCLIENTE", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTCLIENTE", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.ClientFilter });

            if (filter.UserID.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = filter.UserID.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCursor", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var reader = cmd.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    result = new List<BMFBrokerageReport>();
                    while (reader.Read())
                    {
                        
                        BMFBrokerageReport obj = new BMFBrokerageReport();

                        if (!reader.IsDBNull(0)) obj.Broker = reader.GetString(0); //cd_cliente
                        var product = reader.GetString(1);
                        var market = reader.GetString(2);
                        var series = reader.GetString(3);
                        obj.Product = (String.IsNullOrEmpty(product) ? "" : product)
                                + " " +
                                (String.IsNullOrEmpty(market) ? "" : market)
                                + " " +
                                (String.IsNullOrEmpty(series) ? "" : series);

                        if (!reader.IsDBNull(4)) obj.GrossBrokerage = reader.GetDecimal(4);  //sum(a.VL_CORNEG)      vl_corretagem_bruta,
                        if (!reader.IsDBNull(5)) obj.TransferValue = reader.GetDecimal(5); //sum(a.VL_CORNEG_BRO)  vl_repasse,
                        if (!reader.IsDBNull(6)) obj.TransferPercentual = reader.GetDecimal(6); //trunc(decode(sum(a.VL_CORNEG),0,0,sum(a.VL_CORNEG_BRO)/sum(a.vl_corneg)*100),2)pc_repasse, 
                        if (!reader.IsDBNull(7)) obj.Revenue = reader.GetDecimal(7); //sum(a.VL_RECEITA)     vl_receita,
                        if (!reader.IsDBNull(8)) obj.ClientCode = reader.GetInt32(8); //a.cd_cliente

                        result.Add(obj);
                    }
                }
            }

            return result;
        }

        #endregion

        #region Faixa de Corretagem

        public BrokerageRange LoadBrokerageRange(BrokerageRangeFilter filter, FilterOptions filterOptions, out int count, out bool hasAnterior, out bool hasPosterior)
        {
            /*
                pLSTCLIENTE           in varchar2,
                 pLSTASSESSOR          in varchar2,
                 pINITROW              In NUMBER,
                 pFINALROW             In NUMBER,
                 pSORTCOLUMN           In NUMBER,
                 pSORTDIR              In Varchar2,
                 pCount                Out NUMBER,
                 pBOVESPA              out tCursor, 
                 pBMF                  out tCursor,                 
                 pANTERIOR             out number,
                 pPOSTERIOR            out number
             */
            BrokerageRange result = null;

            var cmd = dbi.CreateCommand("PCK_FAIXA_CORRETAGEM.PROC_FAIXA_CORRETAGEM", CommandType.StoredProcedure);
            cmd.Parameters.Clear();

            //cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = filter.ClientCode });


            if (String.IsNullOrEmpty(filter.ClientFilter))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTCLIENTE", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTCLIENTE", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.ClientFilter });
            
            if (String.IsNullOrEmpty(filter.AssessorFilter))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTASSESSOR", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTASSESSOR", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.AssessorFilter });
            
            if (filter.UserID.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_USUARIO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = filter.UserID.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_USUARIO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pINITROW", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = filterOptions.InitRow });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pFINALROW", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = filterOptions.FinalRow });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pSORTCOLUMN", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = filterOptions.SortColumn });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pSORTDIR", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filterOptions.SortDirection });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCount", OracleType = OracleType.Number, Direction = ParameterDirection.Output });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pAnterior", OracleType = OracleType.Number, Direction = ParameterDirection.Output });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pPosterior", OracleType = OracleType.Number, Direction = ParameterDirection.Output });


            cmd.Parameters.Add(new OracleParameter { ParameterName = "pbovespa", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pbmf", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var reader = cmd.ExecuteReader())
            {
                count = Convert.ToInt32(cmd.Parameters["pCount"].Value);
                hasAnterior = Convert.ToInt32(cmd.Parameters["pAnterior"].Value) == 1;
                hasPosterior = Convert.ToInt32(cmd.Parameters["pPosterior"].Value) == 1;

                if (reader.HasRows)
                {
                    result = new BrokerageRange();
                    var obj = new BrokerageBovespa();
                    while (reader.Read())
                    {
                        /*
                         	[DataMember]
		                    public string MarketType { get; set; }
		                    [DataMember]
		                    public decimal NormalOperationRate { get; set; }
		                    [DataMember]
		                    public decimal DayTradeRate { get; set; }
                         */
                        if (!reader.IsDBNull(0)) result.ClientCode = reader.GetInt32(0); 
                        if (!reader.IsDBNull(1)) obj.MarketType = reader.GetString(1); 
                        if (!reader.IsDBNull(2)) obj.NormalOperationRate = reader.GetDecimal(2); 
                        if (!reader.IsDBNull(3)) obj.DayTradeRate = reader.GetDecimal(3);
                        
                        result.Bovespa.Add(obj);
                    }
                }

                if (reader.NextResult()) 
                {
                    if (result == null) result = new BrokerageRange();

                    while (reader.Read())
                    {
                        var obj = new BrokerageBMF();
                        //TODO: Colocar range vencimento e faixa de contrato

                        if (!reader.IsDBNull(0)) obj.Stock = reader.GetString(0);
                        if (!reader.IsDBNull(1)) obj.Quantity = reader.GetDecimal(1);
                        if (!reader.IsDBNull(2)) obj.MarketType = reader.GetString(2);
                        if (!reader.IsDBNull(5)) obj.NormalOperationRate = reader.GetDecimal(5);
                        if (!reader.IsDBNull(6)) obj.DayTradeRate = reader.GetDecimal(6);

                        result.BMF.Add(obj);
                    }
                }

                if (result != null)
                {
                    OtherService other = new OtherService();

                    var client = other.LoadClientInformation(result.ClientCode.ToString(), "", null);
                    result.ClientName = client.Result.Client.Description;
                    result.AssessorName = client.Result.AssessorName;
                }
            }

            return result;
        }

        #endregion
    }
}
