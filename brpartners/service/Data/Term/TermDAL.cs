﻿using System;
using System.Collections.Generic;
using QX3.Spinnex.Common.Services.Data;
using System.Data;
using System.Data.OracleClient;
using QX3.Portal.Contracts.DataContracts;
using System.Linq;

namespace QX3.Portal.Services.Data
{
    public class TermDAL : DataProviderBase
    {
        public TermDAL() : base("PortalConnectionString") { }

        public List<TermItem> LoadTerms(TermFilter filter, int? userId)
        {
            List<TermItem> items = new List<TermItem>();

            var cmd = dbi.CreateCommand("PCK_TERMO.PROC_CONSULTA_TERMO", CommandType.StoredProcedure);

            if (userId.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = userId.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
            
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pLstAssessor", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.Assessors });
            if (string.IsNullOrEmpty(filter.Clients))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLstCliente", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLstCliente", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.Clients });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pD1", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = Convert.ToInt32(filter.D1) });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pD2", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = Convert.ToInt32(filter.D2) });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pD3", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = Convert.ToInt32(filter.D3) });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIdStatus", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = filter.StatusID });
            if (filter.Company != null)
            {
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pTipoFiltro", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = string.IsNullOrEmpty(filter.Company.Value) ? "T" : filter.Company.Value.ToUpper() });
                if (string.IsNullOrEmpty(filter.Company.Description))
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pFiltro", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
                else
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pFiltro", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.Company.Description.ToUpper() });
            }
            else
            {
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pTipoFiltro", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = 0 });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pFiltro", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            }

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSOR", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    TermItem item = new TermItem();

                    item.Client = new CommonType { Value = string.Format("{0}-{1}", dr["CDCLIENTE"], dr["DVCLIENTE"]), Description = Convert.ToString(dr["NOMECLIENTE"]) };
                    item.Assessor = new CommonType { Value = Convert.ToString(dr["IDASSESSOR"]), Description = Convert.ToString(dr["NOMEASSESSOR"]) };
                    item.ID = Convert.ToInt32(dr["IDSOLICITACAO"]);
                    item.Company = new CommonType { Value = Convert.ToString(dr["CDNEGOCIO"]), Description = Convert.ToString(dr["NOMEEMPRESA"]) };
                    if (!(dr["NUMEROCONTRATO"] is DBNull))
                        item.Contract = Convert.ToInt32(dr["NUMEROCONTRATO"]);
                    if (!(dr["QUANTIDADEDISPONIVEL"] is DBNull))
                        item.AvailableQuantity = Convert.ToInt64(dr["QUANTIDADEDISPONIVEL"]);
                    if (!(dr["QUANTIDADETOTAL"] is DBNull))
                        item.TotalQuantity = Convert.ToInt64(dr["QUANTIDADETOTAL"]);
                    if (!(dr["QUANTIDADELIQUIDAR"] is DBNull))
                        item.SettleQuantity = Convert.ToInt64(dr["QUANTIDADELIQUIDAR"]);
                    if (!(dr["IDTIPO"] is DBNull))
                        item.Type = new CommonType { Value = Convert.ToString(dr["IDTIPO"]), Description = Convert.ToString(dr["TIPO"]) };
                    if (!(dr["IDSTATUS"] is DBNull))
                        item.Status = new CommonType { Value = Convert.ToString(dr["IDSTATUS"]), Description = Convert.ToString(dr["STATUS"]) };
                    if (!(dr["DATALIQUIDACAO"] is DBNull))
                        item.SettleDate = Convert.ToDateTime(dr["DATALIQUIDACAO"]);
                    if (!(dr["DATAVENCIMENTO"] is DBNull))
                        item.DueDate = Convert.ToDateTime(dr["DATAVENCIMENTO"]);
                    if (!(dr["DATAROLAGEM"] is DBNull))
                        item.RolloverDate = Convert.ToDateTime(dr["DATAROLAGEM"]);
                    items.Add(item);
                }

                dr.Close();
            }

            return items;
        }

        public List<TermItem> LoadTerms(string lstContracts)
        {
            List<TermItem> items = new List<TermItem>();

            var cmd = dbi.CreateCommand("PCK_TERMO.PROC_CONSULTA_TERMO_LOTE", CommandType.StoredProcedure);

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pLstContrato", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = lstContracts });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSOR", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    TermItem item = new TermItem();

                    item.Client = new CommonType { Value = Convert.ToString(dr["CDCLIENTE"]), Description = Convert.ToString(dr["NMCLIENTE"]) };
                    item.ID = Convert.ToInt32(dr["IDSOLICITACAO"]);
                    item.Company = new CommonType { Value = Convert.ToString(dr["CDNEGOCIO"]) };
                    if (!(dr["NUMEROCONTRATO"] is DBNull))
                        item.Contract = Convert.ToInt32(dr["NUMEROCONTRATO"]);
                    if (!(dr["QUANTIDADEDISPONIVEL"] is DBNull))
                        item.AvailableQuantity = Convert.ToInt64(dr["QUANTIDADEDISPONIVEL"]);
                    if (!(dr["QUANTIDADETOTAL"] is DBNull))
                        item.TotalQuantity = Convert.ToInt64(dr["QUANTIDADETOTAL"]);
                    if (!(dr["QUANTIDADELIQUIDAR"] is DBNull))
                        item.SettleQuantity = Convert.ToInt64(dr["QUANTIDADELIQUIDAR"]);
                    if (!(dr["IDTIPO"] is DBNull))
                        item.Type = new CommonType { Value = Convert.ToString(dr["IDTIPO"]), Description = Convert.ToString(dr["TIPO"]) };
                    if (!(dr["IDSTATUS"] is DBNull))
                        item.Status = new CommonType { Value = Convert.ToString(dr["IDSTATUS"]), Description = Convert.ToString(dr["STATUS"]) };
                    if (!(dr["DATALIQUIDACAO"] is DBNull))
                        item.SettleDate = Convert.ToDateTime(dr["DATALIQUIDACAO"]);
                    items.Add(item);
                }

                dr.Close();
            }

            return items;
        }

        public TermItem LoadTerm(int termId)
        {
            TermItem item = new TermItem();

            var cmd = dbi.CreateCommand("PCK_TERMO.PROC_CARREGA_SOLICITACAO", CommandType.StoredProcedure);

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIdSolicitacao", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = termId });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSOR", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.Read())
                {
                    item.ID = Convert.ToInt32(dr["IDSOLICITACAO"]);
                    item.Client = new CommonType { Value = Convert.ToString(dr["CDCLIENTE"]) };
                    item.Company = new CommonType { Value = Convert.ToString(dr["CDNEGOCIO"]) };
                    item.Contract = Convert.ToInt32(dr["NUMEROCONTRATO"]);
                    item.AvailableQuantity = Convert.ToInt64(dr["QUANTIDADEDISPONIVEL"]);
                    item.TotalQuantity = Convert.ToInt64(dr["QUANTIDADETOTAL"]);
                    item.SettleQuantity = Convert.ToInt64(dr["QUANTIDADELIQUIDAR"]);
                    item.Type = new CommonType { Value = Convert.ToString(dr["IDTIPO"]) };
                    item.Status = new CommonType { Value = Convert.ToString(dr["IDSTATUS"]) };
                    if (!(dr["DATALIQUIDACAO"] is DBNull))
                        item.SettleDate = Convert.ToDateTime(dr["DATALIQUIDACAO"]);
                }

                dr.Close();
            }

            return item;
        }

        public List<TermItem> LoadClientTerms(TermFilter filter, int? userId)
        {
            List<TermItem> items = new List<TermItem>();

            var cmd = dbi.CreateCommand("PCK_TERMO.PROC_CONSULTA_SOLICITACAO", CommandType.StoredProcedure);

            if (userId.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = userId.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCdCliente", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.Clients });
            if (filter.Company != null)
            {
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pTipoFiltro", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = string.IsNullOrEmpty(filter.Company.Value) ? "T" : filter.Company.Value.ToUpper() });
                if (string.IsNullOrEmpty(filter.Company.Description))
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pFiltro", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
                else
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pFiltro", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.Company.Description.ToUpper() });
            }
            else
            {
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pTipoFiltro", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = "T" });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pFiltro", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            }
            if (string.IsNullOrEmpty(filter.StartDate))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDtInicio", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDtInicio", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.StartDate });
            if (string.IsNullOrEmpty(filter.EndDate))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDtFim", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDtFim", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.EndDate });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSOR", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    TermItem item = new TermItem();

                    item.ID = Convert.ToInt32(dr["IDSOLICITACAO"]);
                    item.Client = new CommonType { Value = Convert.ToString(dr["CDCLIENTE"]), Description = Convert.ToString(dr["NOMECLIENTE"]) };
                    item.Company = new CommonType { Value = Convert.ToString(dr["CDNEGOCIO"]) };
                    item.Contract = Convert.ToInt32(dr["NUMEROCONTRATO"]);
                    item.AvailableQuantity = Convert.ToInt64(dr["QUANTIDADEDISPONIVEL"]);
                    item.TotalQuantity = Convert.ToInt64(dr["QUANTIDADETOTAL"]);
                    item.SettleQuantity = Convert.ToInt64(dr["QUANTIDADELIQUIDAR"]);
                    item.Type = new CommonType { Value = Convert.ToString(dr["IDTIPO"]) };
                    item.Status = new CommonType { Value = Convert.ToString(dr["IDSTATUS"]) };
                    if (!(dr["DATALIQUIDACAO"] is DBNull))
                        item.SettleDate = Convert.ToDateTime(dr["DATALIQUIDACAO"]);
                    if (!(dr["DATAVENCIMENTO"] is DBNull))
                        item.DueDate = Convert.ToDateTime(dr["DATAVENCIMENTO"]);
                    if (!(dr["DATAROLAGEM"] is DBNull))
                        item.RolloverDate = Convert.ToDateTime(dr["DATAROLAGEM"]);
                    items.Add(item);
                }

                dr.Close();
            }

            if (items.Count > 0)
                items = (from i in items where i.AvailableQuantity > 0 select i).ToList();
            return items;
        }

        public bool SettleTerm(int termID)
        {
            var cmd = dbi.CreateCommand("PCK_TERMO.PROC_LIQUIDA_TERMO", CommandType.StoredProcedure);
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIdSolicitacao", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = termID });
            return cmd.ExecuteNonQuery() > 0;
        }

        public bool ExcludeTerm(int termID)
        {
            var cmd = dbi.CreateCommand("PCK_TERMO.PROC_EXCLUI_SOLICITACAO", CommandType.StoredProcedure);
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIdSolicitacao", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = termID });
            return cmd.ExecuteNonQuery() > 0;
        }

        public List<TermItem> InsertTerms(List<TermItem> terms)
        {
            foreach (TermItem t in terms)
            {
                var cmd = dbi.CreateCommand("PCK_TERMO.PROC_SOLICITA_LIQUIDACAO", CommandType.StoredProcedure);
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCdCliente", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = t.Client.Value });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCdNegocio", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = t.Company.Value });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pNumContrato", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = t.Contract });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pQuantidadeLiquidar", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = t.SettleQuantity });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pQuantidadeTotal", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = t.TotalQuantity });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIdTipo", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = Int32.Parse(t.Type.Value) });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDataLiquidacao", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = t.SettleDate.Value.ToString("dd/MM/yyyy") });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIdSolicitacao", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = t.ID });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pNovoId", OracleType = OracleType.Number, Direction = ParameterDirection.Output, Size = 6 });
                cmd.ExecuteNonQuery();
                if (cmd.Parameters["pNovoId"] != null && cmd.Parameters["pNovoId"].Value != null)
                    t.ID = Int32.Parse(cmd.Parameters["pNovoId"].Value.ToString());
            }

            return terms;
        }

        public bool ChangeQuantityToSettle(int termID, Int64 quantity)
        {
            var cmd = dbi.CreateCommand("PCK_TERMO.PROC_ATUALIZA_SOLICITACAO", CommandType.StoredProcedure);
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pQuantidadeLiquidar", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = quantity });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIdSolicitacao", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = termID });
            return cmd.ExecuteNonQuery() > 0;
        }

        public int GetNumberOfPending(string assessorsList)
        {
            var cmd = dbi.CreateCommand("PCK_TERMO.PROC_CONTA_PENDENTES", CommandType.StoredProcedure);

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pLstAssessor", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = assessorsList });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pPendentes", OracleType = OracleType.Number, Direction = ParameterDirection.Output, Size = 60 });
            cmd.ExecuteNonQuery();

            return cmd.Parameters["pPendentes"].Value == DBNull.Value ? 0 : Int32.Parse(cmd.Parameters["pPendentes"].Value.ToString());
        }

    }
}
