﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Data.OracleClient;
using QX3.Spinnex.Common.Services.Data;
using QX3.Portal.Contracts.DataContracts;
using QX3.Spinnex.Common.Services.Logging;
using System.Linq;

namespace QX3.Portal.Services.Data
{
    public class MarketDAL : DataProviderBase
    {
        public MarketDAL() : base("PortalConnectionString") { }

        #region Proceeds

        public List<Proceeds> LoadProceeds(ProceedsFilter filter, int? userId)
        {
            List<Proceeds> items = new List<Proceeds>();

            var cmd = dbi.CreateCommand("PCK_PROVENTOS.PROC_PROVENTOS", CommandType.StoredProcedure);

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_ASSESSOR_INICIO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = string.Empty });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_ASSESSOR_FIM", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = string.Empty });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE_INICIO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.ClientStart });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE_FIM", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.ClientEnd });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pNM_CLIENTE", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = string.Empty });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pNM_EMPRESA", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.CompanyName });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_NEGOCIO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.StockCode });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_INICIO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.StartDate });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_FIM", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.EndDate });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pTP_PROVENTO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = "" });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_PAGO", OracleType = OracleType.Char, Direction = ParameterDirection.Input, Value = filter.Type });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pDS_LISTA_ASSESSOR", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.Assessors });

            if (userId.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = userId.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });


            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSOR", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    Proceeds item = new Proceeds();

                    if (!(dr["COD_NEG"] is DBNull))
                        item.StockCode = Convert.ToString(dr["COD_NEG"]);
                    if (!(dr["COD_ISIN"] is DBNull))
                        item.ISIN = Convert.ToString(dr["COD_ISIN"]);
                    if (!(dr["COD_ASSE"] is DBNull))
                        item.AssessorCode = Int32.Parse(Convert.ToString(dr["COD_ASSE"]));
                    if (!(dr["NM_ASSESSOR"] is DBNull))
                        item.AssessorName = Convert.ToString(dr["NM_ASSESSOR"]);
                    if (!(dr["COD_CLI"] is DBNull))
                        item.ClientCode = Convert.ToInt32(dr["COD_CLI"]);
                    if (!(dr["NM_CLIENTE"] is DBNull))
                        item.ClientName = Convert.ToString(dr["NM_CLIENTE"]);
                    if (!(dr["DESC_TIPO_PROV"] is DBNull))
                        item.Proceed = Convert.ToString(dr["DESC_TIPO_PROV"]);
                    if (!(dr["QTDE_PROV"] is DBNull))
                        item.Quantity = Convert.ToInt64(dr["QTDE_PROV"]);
                    if (!(dr["VAL_PROV"] is DBNull))
                        item.GrossValue = Convert.ToDecimal(dr["VAL_PROV"]);
                    if (!(dr["PERC_IR_DIVI"] is DBNull))
                        item.IR = Convert.ToDecimal(dr["PERC_IR_DIVI"]);
                    if (!(dr["VL_IRRF"] is DBNull))
                        item.IRValue = Convert.ToDecimal(dr["VL_IRRF"]);
                    if (!(dr["VL_LIQUIDO"] is DBNull))
                        item.NetValue = Convert.ToDecimal(dr["VL_LIQUIDO"]);
                    if (!(dr["DATA_PGTO_DIVI"] is DBNull))
                        item.PaymentDate = Convert.ToDateTime(dr["DATA_PGTO_DIVI"]);
                    if (!(dr["COD_CART"] is DBNull))
                        item.Portfolio = Convert.ToInt32(dr["COD_CART"]);
                    items.Add(item);
                }

                dr.Close();
            }

            return items;
        }

        #endregion

        #region Common

        public List<Stock> LoadStocks()
        {
            List<Stock> items = new List<Stock>();

            var cmd = dbi.CreateCommand("PCK_PORTALWEB.PROC_CARREGA_ATIVOS", CommandType.StoredProcedure);
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSOR", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    Stock item = new Stock();

                    if (!(dr["EMPRESA"] is DBNull))
                        item.CompanyName = Convert.ToString(dr["EMPRESA"]);
                    if (!(dr["ATIVO"] is DBNull))
                        item.Symbol = Convert.ToString(dr["ATIVO"]);
                    items.Add(item);
                }

                dr.Close();
            }

            return items;
        }

        public string GetAssessorName(int assessorID)
        {
            var cmd = dbi.CreateCommand("PCK_PORTALWEB.PROC_BUSCA_NMASSESSOR", CommandType.StoredProcedure);
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIdAssessor", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = assessorID });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pNmAssessor", OracleType = OracleType.VarChar, Direction = ParameterDirection.Output, Size = 60 });
            cmd.ExecuteNonQuery();

            return cmd.Parameters["pNmAssessor"].Value.ToString();
        }

        public List<CommonType> AssessorNameSuggest(string assessorName)
        {
            var assessors = new List<CommonType>();

            var cmd = dbi.CreateCommand("PCK_PORTALWEB.PROC_BUSCA_OPERADOR", CommandType.StoredProcedure);
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pNM_ASSESSOR", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = assessorName });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCursor", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    assessors.Add(new CommonType { Value = Convert.ToString(dr["CD_ASSESSOR"]), Description = Convert.ToString(dr["NM_ASSESSOR"]) });
                }
            }

            return assessors;
        }

        public string GetNextBusinessDay(int numberOfDays, string targetDate)
        { 
            var cmd = dbi.CreateCommand("PCK_PORTALWEB.PROC_RETORNA_DIAUTIL", CommandType.StoredProcedure);
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pNumDias", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = numberOfDays });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pData", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = targetDate });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pProxDiaUtil", OracleType = OracleType.VarChar, Direction = ParameterDirection.Output, Size = 60 });
            cmd.ExecuteNonQuery();

            return cmd.Parameters["pProxDiaUtil"].Value.ToString();
        }

        public int GetNumberOfPending(string tableName, int statusId, string assessorsList)
        {
            var cmd = dbi.CreateCommand("PCK_PORTALWEB.PROC_CONTA_PENDENTES", CommandType.StoredProcedure);

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pTableName", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = tableName });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIdStatus", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = statusId });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pLstAssessor", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = assessorsList });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pPendentes", OracleType = OracleType.Number, Direction = ParameterDirection.Output, Size = 60 });
            cmd.ExecuteNonQuery();

            return cmd.Parameters["pPendentes"].Value == DBNull.Value ? 0 : Int32.Parse(cmd.Parameters["pPendentes"].Value.ToString());
        }

        public StockExchanges LoadClientExchanges(int clientCode)
        {
            StockExchanges exchanges = new StockExchanges();

            var cmd = dbi.CreateCommand("PCK_GARANTIA.PROC_CONSULTA_BOLSAS", CommandType.StoredProcedure);
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCdCliente", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = clientCode });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCursor", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.Read())
                {
                    exchanges.Bmf = Convert.ToBoolean(dr["bmf"]);
                    exchanges.Bovespa = Convert.ToBoolean(dr["bovespa"]);
                }

                dr.Close();
            }

            return exchanges;
        }

        #endregion

    }
}
