﻿using QX3.Portal.Contracts.DataContracts;
using QX3.Spinnex.Common.Services.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;

namespace QX3.Portal.Services.Data
{
    public class SuitabilityDAL : DataProviderBase
    {
        public SuitabilityDAL() : base("PortalConnectionString") { }

        //public SuitabilityForm Suitability(int client, int? testingFormId)
        //{
        //    SuitabilityForm frm = new SuitabilityForm();
        //    frm.Questions = new List<Suitability>();
        //    frm.Questions = new List<Suitability>();
        //    frm.Scores = new List<SuitabilityScore>();

        //    var cmd = dbi.CreateCommand("PCK_SUITABILITY.PROC_PERGUNTAS", CommandType.StoredProcedure);
        //    cmd.Parameters.Clear();
        //    cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = client });
        //    cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_FORMULARIO_TESTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = testingFormId.HasValue ? testingFormId.Value : (object)DBNull.Value });


        //    cmd.Parameters.Add(new OracleParameter { ParameterName = "pPERGUNTAS", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });
        //    cmd.Parameters.Add(new OracleParameter { ParameterName = "pRESPOSTAS", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });
        //    cmd.Parameters.Add(new OracleParameter { ParameterName = "pMATRIZES", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });
        //    cmd.Parameters.Add(new OracleParameter { ParameterName = "pPONTUACOES", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

        //    using (var dr = cmd.ExecuteReader())
        //    {
        //        while (dr.Read())
        //        {
        //            var s = new Suitability();
        //            s.Id = Convert.ToInt32(dr["ID"]);
        //            s.IdQuestion = dr["IDQUESTION"] != DBNull.Value ? Convert.ToInt32(dr["IDQUESTION"]) : 0;
        //            s.Question = dr["QUESTION"] != DBNull.Value ? Convert.ToString(dr["QUESTION"]) : "";
        //            s.QuestionType = (QuestionType)Convert.ToChar(dr["CD_TIPO"]);
        //            s.Weight = Convert.ToInt32(dr["VL_PESO"]);
        //            decimal? subId = dr["IDSUBQUESTION"] as decimal?;
        //            if (subId != null)
        //                s.SubQuestionId = (int)subId;


        //            s.Percentage = dr["PC_MINIMO"] as decimal?;
        //            s.PercentageDescription = dr["DS_PERCENTUAL"] as string;

        //            s.Answers = new List<SuitabilityAnswers>();
        //            frm.Questions.Add(s);
        //        }

        //        if (dr.NextResult())
        //        {
        //            while (dr.Read())
        //            {
        //                var sa = new SuitabilityAnswers();
        //                sa.IdAnswer = dr["IDANSWER"] != DBNull.Value ? Convert.ToInt32(dr["ID"]) : 0;
        //                sa.IdQuestion = dr["IDQUESTION"] != DBNull.Value ? Convert.ToInt32(dr["IDQUESTION"]) : 0;
        //                sa.Answer = dr["ANSWER"] != DBNull.Value ? Convert.ToString(dr["ANSWER"]) : "";
        //                sa.IdTypeInvestor = dr["ID_TYPE_INVESTOR"] != DBNull.Value ? Convert.ToInt32(dr["ID_TYPE_INVESTOR"]) : 0;
        //                sa.Point = (decimal)dr["POINT"];
        //                decimal? subId = dr["IDSUBQUESTION"] as decimal?;
        //                if (subId != null)
        //                    sa.SubQuestionId = (int)subId;
        //                var question = frm.Questions.Find(q => q.Id == sa.IdQuestion);
        //                sa.QuestionType = question.QuestionType;
        //                Debug.Assert(question != null);

        //                question.Answers.Add(sa);
        //            }
        //        }

        //        if (dr.NextResult())
        //        {

        //            while (dr.Read())
        //            {
        //                SuitabilityMatrixColumns matrix = new SuitabilityMatrixColumns();
        //                matrix.ID = (int)(decimal)dr["ID_COLUNA"];
        //                matrix.QuestionID = (int)(decimal)dr["IDQUESTION"];
        //                matrix.SortOrder = (int)(decimal)dr["NU_ORDEM"];
        //                matrix.Description = (string)dr["DS_COLUNA"];
        //                matrix.IsActive = dr["IN_ATIVO"].Equals(1);
        //                var question = frm.Questions.Find(q => q.Id == matrix.QuestionID);
        //                if (question.MatrixColumns == null)
        //                    question.MatrixColumns = new List<SuitabilityMatrixColumns>();
        //                question.MatrixColumns.Add(matrix);
        //            }
        //        }

        //        if (dr.NextResult())
        //        {
        //            while (dr.Read())
        //            {
        //                SuitabilityScore score = new SuitabilityScore();
        //                score.TypeId = Convert.ToInt32(dr["ID_TYPE"]);
        //                score.Description = (string)dr["DESCRIPTION"];
        //                score.Min = dr["VL_PONTOS_MIN"] as decimal? ?? 0.0M;
        //                score.Max = dr["VL_PONTOS_MAX"] as decimal? ?? Int32.MaxValue;

        //                if (frm.Scores.Any())
        //                    frm.Scores.Last().Max = score.Min;

        //                frm.Scores.Add(score);
        //            }
        //        }


        //        dr.Close();
        //    }

        //    return frm;

        //}

        public SuitabilityForm Suitability(int client, int? testingFormId)
        {
            SuitabilityForm frm = new SuitabilityForm();
            frm.Questions = new List<Suitability>();
            frm.Questions = new List<Suitability>();
            frm.Scores = new List<SuitabilityScore>();


            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PR_SUITABILITY_PERGUNTAS", CommandType.StoredProcedure);
            cmd.Parameters.Clear();
            cmd.Parameters.Add("@pID_CLIENTE", SqlDbType.Decimal).Value = client;
            cmd.Parameters.Add("@pID_FORMULARIO_TESTE", SqlDbType.Decimal).Value = testingFormId.HasValue ? testingFormId.Value : (object)DBNull.Value;

            cmd.Connection.Open();

            using (var dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    var s = new Suitability();
                    s.Id = Convert.ToInt32(dr["ID"]);
                    s.IdQuestion = dr["IDQUESTION"] != DBNull.Value ? Convert.ToInt32(dr["IDQUESTION"]) : 0;
                    s.Question = dr["QUESTION"] != DBNull.Value ? Convert.ToString(dr["QUESTION"]) : "";
                    s.QuestionType = (QuestionType)Convert.ToChar(dr["CD_TIPO"]);
                    s.Weight = Convert.ToInt32(dr["VL_PESO"]);
                    decimal? subId = dr["IDSUBQUESTION"] as decimal?;
                    if (subId != null)
                        s.SubQuestionId = (int)subId;


                    s.Percentage = dr["PC_MINIMO"] as decimal?;
                    s.PercentageDescription = dr["DS_PERCENTUAL"] as string;

                    s.Answers = new List<SuitabilityAnswers>();
                    frm.Questions.Add(s);
                }

                if (dr.NextResult())
                {
                    while (dr.Read())
                    {
                        var sa = new SuitabilityAnswers();
                        sa.IdAnswer = dr["IDANSWER"] != DBNull.Value ? Convert.ToInt32(dr["ID"]) : 0;
                        sa.IdQuestion = dr["IDQUESTION"] != DBNull.Value ? Convert.ToInt32(dr["IDQUESTION"]) : 0;
                        sa.Answer = dr["ANSWER"] != DBNull.Value ? Convert.ToString(dr["ANSWER"]) : "";
                        sa.IdTypeInvestor = dr["ID_TYPE_INVESTOR"] != DBNull.Value ? Convert.ToInt32(dr["ID_TYPE_INVESTOR"]) : 0;
                        sa.Point = (decimal)dr["POINT"];
                        decimal? subId = dr["IDSUBQUESTION"] as decimal?;
                        if (subId != null)
                            sa.SubQuestionId = (int)subId;
                        var question = frm.Questions.Find(q => q.Id == sa.IdQuestion);
                        sa.QuestionType = question.QuestionType;
                        Debug.Assert(question != null);

                        question.Answers.Add(sa);
                    }
                }

                if (dr.NextResult())
                {

                    while (dr.Read())
                    {
                        SuitabilityMatrixColumns matrix = new SuitabilityMatrixColumns();
                        matrix.ID = (int)dr["ID_COLUNA"];
                        matrix.QuestionID = (int)(decimal)dr["IDQUESTION"];
                        matrix.SortOrder = (int)(decimal)dr["NU_ORDEM"];
                        matrix.Description = (string)dr["DS_COLUNA"];
                        matrix.IsActive = dr["IN_ATIVO"].Equals(1);
                        var question = frm.Questions.Find(q => q.Id == matrix.QuestionID);
                        if (question.MatrixColumns == null)
                            question.MatrixColumns = new List<SuitabilityMatrixColumns>();
                        question.MatrixColumns.Add(matrix);
                    }
                }

                if (dr.NextResult())
                {
                    while (dr.Read())
                    {
                        SuitabilityScore score = new SuitabilityScore();
                        score.TypeId = Convert.ToInt32(dr["ID_TYPE"]);
                        score.Description = (string)dr["DESCRIPTION"];
                        score.Min = dr["VL_PONTOS_MIN"] as decimal? ?? 0.0M;
                        score.Max = dr["VL_PONTOS_MAX"] as decimal? ?? Int32.MaxValue;

                        if (frm.Scores.Any())
                            frm.Scores.Last().Max = score.Min;

                        frm.Scores.Add(score);
                    }
                }

                dr.Close();
            }

            cmd.Connection.Close();

            return frm;
        }

        //public bool GravarSuitability(SuitabilityInput userInput, string login, int cdCliente, int idClienteGlobal, int status, int investidor)
        //{
        //    DateTime insertDate = DateTime.Now;
        //    var cmd = dbi.CreateCommand("PCK_SUITABILITY.PROC_GRAVA_RESPOSTAS", CommandType.StoredProcedure);

        //    cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = idClienteGlobal });
        //    cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = cdCliente });
        //    cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_LOGIN", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = login, Size = login.Length });
        //    cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_INCLUSAO", OracleType = OracleType.DateTime, Direction = ParameterDirection.Input, Value = insertDate });
        //    cmd.Parameters.Add(new OracleParameter { ParameterName = "pSTATUS", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = status });
        //    cmd.Parameters.Add(new OracleParameter { ParameterName = "pTIPO_INVESTIDOR", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = investidor });
        //    //cmd.Parameters.Add(new OracleParameter { ParameterName = "pRESPOSTAS", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = respostas });
        //    cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_RESULTADO", OracleType = OracleType.Number, Direction = ParameterDirection.Output });
        //    cmd.ExecuteNonQuery();
        //    int id = Int32.Parse(cmd.Parameters["pID_RESULTADO"].Value.ToString());

        //    if (id > 0)
        //    {
        //        foreach (var item in userInput.Items)
        //        {
        //            cmd = dbi.CreateCommand("PCK_SUITABILITY.PROC_GRAVA_ITEM_RESPOSTA", CommandType.StoredProcedure);
        //            cmd.Parameters.Add(new OracleParameter() { ParameterName = "pID_RESULTADO", OracleType = OracleType.Int32, Value = id });
        //            cmd.Parameters.Add(new OracleParameter() { ParameterName = "pDT_RESULTADO", OracleType = OracleType.DateTime, Value = insertDate });
        //            cmd.Parameters.Add(new OracleParameter() { ParameterName = "pREGID", OracleType = OracleType.Int32, Value = cdCliente });
        //            cmd.Parameters.Add(new OracleParameter() { ParameterName = "pID_QUESTAO", OracleType = OracleType.Int32, Value = item.QuestionId });
        //            cmd.Parameters.Add(new OracleParameter() { ParameterName = "pID_RESPOSTA", OracleType = OracleType.Int32, Value = item.AnswerId });
        //            cmd.Parameters.Add(new OracleParameter() { ParameterName = "pIN_CORRET", OracleType = OracleType.Int32, Value = item.IsCorrect ? 1 : 0 });
        //            cmd.Parameters.Add(new OracleParameter() { ParameterName = "pPC_DISTRIBUICAO", OracleType = OracleType.Int32, Value = item.Percentage });

        //            cmd.ExecuteNonQuery();

        //            if (item.MatrixData != null)
        //            {
        //                foreach (var m in item.MatrixData)
        //                {
        //                    cmd = dbi.CreateCommand("PCK_SUITABILITY.PROC_GRAVA_ITEM_MATRIZ", CommandType.StoredProcedure);
        //                    cmd.Parameters.Add(new OracleParameter() { ParameterName = "pID_RESULTADO", OracleType = OracleType.Int32, Value = id });
        //                    cmd.Parameters.Add(new OracleParameter() { ParameterName = "pID_RESPOSTA", OracleType = OracleType.Int32, Value = item.AnswerId });
        //                    cmd.Parameters.Add(new OracleParameter() { ParameterName = "pID_COLUNA", OracleType = OracleType.Int32, Value = m.ID });
        //                    cmd.Parameters.Add(new OracleParameter() { ParameterName = "pIN_CHECKED", OracleType = OracleType.Int32, Value = m.IsChecked ? 1 : 0 });
        //                    cmd.ExecuteReader();
        //                }
        //            }

        //        }
        //        return true;
        //    }
        //    return false;
        //}

        public int GravarSuitability(SuitabilityInput userInput, string login, int cdCliente, int idClienteGlobal, int status, int investidor, int? score, int idForm = 0)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");

            DateTime insertDate = DateTime.Now;

            var cmd = (SqlCommand)dbiCRK.CreateCommand("PR_SUITABILITY_GRAVA_RESPOSTAS", CommandType.StoredProcedure);
            cmd.Parameters.Clear();

            cmd.Parameters.Add("@pID_CLIENTE", SqlDbType.Decimal).Value = idClienteGlobal;
            cmd.Parameters.Add("@pCD_CLIENTE", SqlDbType.Decimal).Value = cdCliente;

            if (string.IsNullOrEmpty(login))
                cmd.Parameters.Add("@pCD_LOGIN", SqlDbType.VarChar).Value = DBNull.Value;
            else
                cmd.Parameters.Add("@pCD_LOGIN", SqlDbType.VarChar).Value = login;

            if (score.HasValue)
                cmd.Parameters.Add("@pSCORE", SqlDbType.Int).Value = score;
            else
                cmd.Parameters.Add("@pSCORE", SqlDbType.Int).Value = DBNull.Value;

            cmd.Parameters.Add("@pDT_INCLUSAO", SqlDbType.DateTime).Value = insertDate;
            cmd.Parameters.Add("@pSTATUS", SqlDbType.Decimal).Value = status;
            cmd.Parameters.Add("@pTIPO_INVESTIDOR", SqlDbType.Decimal).Value = investidor;

            cmd.Parameters.Add("@pID_FORM", SqlDbType.Decimal).Value = idForm;

            cmd.Parameters.Add("@pID_RESULTADO", SqlDbType.Int).Direction = ParameterDirection.Output;

            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            cmd.Connection.Close();

            int id = Int32.Parse(cmd.Parameters["@pID_RESULTADO"].Value.ToString());

            if (id > 0)
            {
                foreach (var item in userInput.Items)
                {
                    cmd = (SqlCommand)dbiCRK.CreateCommand("PR_SUITABILITY_GRAVA_ITEM_RESPOSTA", CommandType.StoredProcedure);
                    cmd.Parameters.Add("@pID_RESULTADO", SqlDbType.Int).Value = id;
                    cmd.Parameters.Add("@pDT_RESULTADO", SqlDbType.DateTime).Value = insertDate;
                    cmd.Parameters.Add("@pREGID", SqlDbType.Int).Value = cdCliente;
                    cmd.Parameters.Add("@pID_QUESTAO", SqlDbType.Int).Value = item.QuestionId;
                    cmd.Parameters.Add("@pID_RESPOSTA", SqlDbType.Int).Value = item.AnswerId;
                    cmd.Parameters.Add("@pIN_CORRET", SqlDbType.Int).Value = item.IsCorrect ? 1 : 0;
                    cmd.Parameters.Add("@pPC_DISTRIBUICAO", SqlDbType.Int).Value = item.Percentage;

                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                    cmd.Connection.Close();



                    if (item.MatrixData != null)
                    {
                        foreach (var m in item.MatrixData)
                        {
                            cmd = (SqlCommand)dbiCRK.CreateCommand("PR_SUITABILITY_GRAVA_ITEM_MATRIZ", CommandType.StoredProcedure);
                            cmd.Parameters.Add("@pID_RESULTADO", SqlDbType.Int).Value = id;
                            cmd.Parameters.Add("@pID_RESPOSTA", SqlDbType.Int).Value = item.AnswerId;
                            cmd.Parameters.Add("@pID_COLUNA", SqlDbType.Int).Value = m.ID;
                            cmd.Parameters.Add("@pIN_CHECKED", SqlDbType.Int).Value = m.IsChecked ? 1 : 0;

                            cmd.Connection.Open();
                            cmd.ExecuteReader();
                            cmd.Connection.Close();
                        }
                    }

                }

                // UpdateUnframedInformation(idClienteGlobal, insertDate, false);
            }

            return id;
        }

        public bool GravarSuitabilityJustification(int clienteId, string message)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PROC_INSERE_DESENQUADRAMENTO_JUSTIFICATION", CommandType.StoredProcedure);
            cmd.Connection.Open();

            cmd.Parameters.Add(new SqlParameter { ParameterName = "@id_Cliente", SqlDbType = SqlDbType.Int, Value = clienteId });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@message", SqlDbType = SqlDbType.VarChar, Value = message });

            var dr = cmd.ExecuteNonQuery();

            cmd.Connection.Close();

            return dr > 1;
        }

        public List<SuitabilityPerfilCliente> PerfilClienteJustification(string cnpj, bool init)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PR_SUITABILITY_SELECIONA_CLIENTES_BY_CNPJ", CommandType.StoredProcedure);
            cmd.Parameters.Clear();
            cmd.Parameters.Add("@cnpjCli", SqlDbType.VarChar).Value = cnpj.Length == 0 ? 0 : double.Parse(cnpj);
            cmd.Parameters.Add("@init", SqlDbType.Bit).Value = init;

            cmd.Connection.Open();
            var list = new List<SuitabilityPerfilCliente>();
            using (var dr = cmd.ExecuteReader())
            {
                var question = new Suitability();
                while (dr.Read())
                {
                    var sa = new SuitabilityPerfilCliente();
                    sa.CnpjCpf = dr["CD_CPFCNPJ"] != DBNull.Value ? dr["CD_CPFCNPJ"].ToString() : null;
                    sa.DataCadastro = dr["INSERT_DATE"] != DBNull.Value ? dr["INSERT_DATE"].ToString() : null;
                    sa.Descricao = dr["DESCRIPTION"] != DBNull.Value ? dr["DESCRIPTION"].ToString() : null;
                    sa.Justificacao = dr["JUSTIFICATION"] != DBNull.Value ? dr["JUSTIFICATION"].ToString() : null;
                    sa.IdCliente = dr["ID_CLIENTE"] != DBNull.Value ? Convert.ToInt32(dr["ID_CLIENTE"]) : 0;

                    list.Add(sa);
                }
                dr.Close();
            }

            cmd.Connection.Close();
            return list;
        }

        //public SuitabilityClientsResponse SelecionaClientesSuitability(int? idClienteGlobal, long? cpfcnpj, string cdCliente, string status, string investidor, string desenquadrado)
        //{
        //    var cmd = dbi.CreateCommand("PCK_SUITABILITY.PROC_SELECIONA_CLIENTES", CommandType.StoredProcedure);
        //    SuitabilityClientsResponse sr = new SuitabilityClientsResponse();

        //    sr.CdBolsa = cdCliente;
        //    sr.Status = status;
        //    sr.InvestorType = investidor;
        //    sr.Unframed = desenquadrado;

        //    SuitabilityClients sc = new SuitabilityClients();
        //    List<SuitabilityClients> lsc = new List<SuitabilityClients>();
        //    cmd.Parameters.Clear();

        //    cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = (object)idClienteGlobal ?? DBNull.Value });
        //    cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CPFCNPJ", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = (object)cpfcnpj ?? DBNull.Value });

        //    if (!string.IsNullOrEmpty(cdCliente))
        //        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = Convert.ToInt32(cdCliente) });
        //    else
        //        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });

        //    if (!string.IsNullOrEmpty(status))
        //        cmd.Parameters.Add(new OracleParameter { ParameterName = "pSTATUS", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = Convert.ToInt32(status) });
        //    else
        //        cmd.Parameters.Add(new OracleParameter { ParameterName = "pSTATUS", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });

        //    if (!string.IsNullOrEmpty(investidor))
        //        cmd.Parameters.Add(new OracleParameter { ParameterName = "pTIPO_INVESTIDOR", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = Convert.ToInt32(investidor) });
        //    else
        //        cmd.Parameters.Add(new OracleParameter { ParameterName = "pTIPO_INVESTIDOR", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });

        //    if (!string.IsNullOrEmpty(desenquadrado))
        //        cmd.Parameters.Add(new OracleParameter { ParameterName = "pDESENQUADRADO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = desenquadrado });
        //    else
        //        cmd.Parameters.Add(new OracleParameter { ParameterName = "pDESENQUADRADO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });

        //    cmd.Parameters.Add(new OracleParameter { ParameterName = "pRESULTADO", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

        //    sr.Clients = lsc;

        //    using (var dr = cmd.ExecuteReader())
        //    {
        //        if (dr.HasRows)
        //        {
        //            while (dr.Read())
        //            {
        //                sc = new SuitabilityClients();
        //                sc.Status = dr["SUITABILITY_STATUS"] != DBNull.Value ? Convert.ToInt32(dr["SUITABILITY_STATUS"]) : 0;
        //                sc.InvestorType = dr["INVESTIDOR"] != DBNull.Value ? Convert.ToInt32(dr["INVESTIDOR"]) : 0;
        //                sc.Unframed = dr["DESENQUADRADO"] != DBNull.Value ? Convert.ToString(dr["DESENQUADRADO"]) : "";
        //                sc.IdCliente = Convert.ToInt32(dr["ID_CLIENTE"]);
        //                sc.CPFCNPJ = Convert.ToInt64(dr["CD_CPFCNPJ"]);
        //                sc.ClientName = dr["NM_CLIENTE"] != DBNull.Value ? Convert.ToString(dr["NM_CLIENTE"]) : "";
        //                string bolsa = dr["CD_BOLSA"] as string;
        //                if (bolsa != null)
        //                    sc.CodigosBolsa = bolsa.Split(',').Select(cod => Int32.Parse(cod)).ToArray();

        //                lsc.Add(sc);
        //            }
        //        }

        //        sr.Clients = lsc;

        //        dr.Close();
        //    }

        //    return sr;

        //}

        public SuitabilityClientsResponse SelecionaClientesSuitability(int? idClienteGlobal, long? cpfcnpj, string cdCliente, string status, string investidor, string desenquadrado)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");

            var cmd = (SqlCommand)dbiCRK.CreateCommand("PR_SUITABILITY_SELECIONA_CLIENTES", CommandType.StoredProcedure);

            SuitabilityClientsResponse sr = new SuitabilityClientsResponse();

            sr.CdBolsa = cdCliente;
            sr.Status = status;
            sr.InvestorType = investidor;
            sr.Unframed = desenquadrado;

            SuitabilityClients sc = new SuitabilityClients();
            List<SuitabilityClients> lsc = new List<SuitabilityClients>();
            cmd.Parameters.Clear();

            cmd.Parameters.Add("@pID_CLIENTE", SqlDbType.Decimal).Value = (object)idClienteGlobal ?? DBNull.Value;
            cmd.Parameters.Add("@pCD_CPFCNPJ", SqlDbType.Decimal).Value = (object)cpfcnpj ?? DBNull.Value;

            if (!string.IsNullOrEmpty(cdCliente))
                cmd.Parameters.Add("@pCD_CLIENTE", SqlDbType.Decimal).Value = Convert.ToInt32(cdCliente);
            else
                cmd.Parameters.Add("@pCD_CLIENTE", SqlDbType.Decimal).Value = DBNull.Value;
            if (!string.IsNullOrEmpty(status))
                cmd.Parameters.Add("@pSTATUS", SqlDbType.Decimal).Value = Convert.ToInt32(status);
            else
                cmd.Parameters.Add("@pSTATUS", SqlDbType.Decimal).Value = DBNull.Value;

            if (!string.IsNullOrEmpty(investidor))
                cmd.Parameters.Add("@pTIPO_INVESTIDOR", SqlDbType.Decimal).Value = Convert.ToInt32(investidor);
            else
                cmd.Parameters.Add("@pTIPO_INVESTIDOR", SqlDbType.Decimal).Value = DBNull.Value;

            if (!string.IsNullOrEmpty(desenquadrado))
                cmd.Parameters.Add("@pDESENQUADRADO", SqlDbType.Decimal).Value = desenquadrado;
            else
                cmd.Parameters.Add("@pDESENQUADRADO", SqlDbType.Decimal).Value = DBNull.Value;

            sr.Clients = lsc;

            cmd.Connection.Open();

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        sc = new SuitabilityClients();
                        sc.Id = Convert.ToInt32(dr["ID"]);
                        sc.Status = dr["SUITABILITY_STATUS"] != DBNull.Value ? Convert.ToInt32(dr["SUITABILITY_STATUS"]) : 0;
                        sc.InvestorType = dr["INVESTIDOR"] != DBNull.Value ? Convert.ToInt32(dr["INVESTIDOR"]) : 0;
                        sc.Unframed = dr["DESENQUADRADO"] != DBNull.Value ? Convert.ToString(dr["DESENQUADRADO"]) : "";
                        sc.IdCliente = Convert.ToInt32(dr["ID_CLIENTE"]);
                        sc.CPFCNPJ = Convert.ToInt64(dr["CD_CPFCNPJ"]);
                        sc.ClientName = dr["NOME"].ToString();
                        var cdBolsa = new int[] { 0 };
                        if (dr["CDBOLSA"] != DBNull.Value)
                        {
                            cdBolsa[0] = Convert.ToInt32(dr["CDBOLSA"]);
                        }
                        else
                            cdBolsa[0] = 0;

                        sc.CodigosBolsa = cdBolsa;
                        sc.Email = dr["EMAIL"].ToString();
                        sc.InvestorKind = dr["TIPO_INVESTIDOR"] != DBNull.Value ? Convert.ToInt32(dr["TIPO_INVESTIDOR"]) : 0;
                        sc.SendSuitabilityEmail = Convert.ToInt32(dr["ENVIAR_EMAIL_SUITABILITY"]) == 1 ? true : false;
                        sc.Blocked = Convert.ToInt32(dr["BLOQUEADO"]) == 1 ? true : false;

                        // TODO: Tornar CompletionDate nullable
                        sc.CompletionDate = dr["INSERT_DATE"] != DBNull.Value ? Convert.ToDateTime(dr["INSERT_DATE"]) : DateTime.MinValue;

                        lsc.Add(sc);
                    }
                }

                sr.Clients = lsc;

                dr.Close();
            }

            cmd.Connection.Close();

            return sr;

        }

        //public List<SuitabilityClients> HistoricoClientesSuitability(int? idCliente, string cdCliente, string login)
        //{
        //    var cmd = dbi.CreateCommand("PCK_SUITABILITY.PROC_SELECIONA_HISTORICO", CommandType.StoredProcedure);

        //    SuitabilityClients sc = new SuitabilityClients();
        //    List<SuitabilityClients> lsc = new List<SuitabilityClients>();
        //    cmd.Parameters.Clear();


        //    cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = (object)idCliente ?? DBNull.Value });

        //    if (!string.IsNullOrEmpty(cdCliente))
        //        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = Convert.ToInt32(cdCliente) });
        //    else
        //        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });

        //    if (string.IsNullOrWhiteSpace(login))
        //        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_LOGIN", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
        //    else
        //        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_LOGIN", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = login, Size = login.Length });

        //    cmd.Parameters.Add(new OracleParameter { ParameterName = "pRESULTADO", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });


        //    using (var dr = cmd.ExecuteReader())
        //    {
        //        if (dr.HasRows)
        //        {
        //            while (dr.Read())
        //            {
        //                sc = new SuitabilityClients();
        //                sc.Id = dr["ID"] != DBNull.Value ? Convert.ToInt32(dr["ID"]) : 0;
        //                sc.Status = dr["IDSTATUS"] != DBNull.Value ? Convert.ToInt32(dr["IDSTATUS"]) : 0;
        //                sc.InvestorType = dr["IDTYPE"] != DBNull.Value ? Convert.ToInt32(dr["IDTYPE"]) : 0;
        //                sc.CompletionDate = dr["INSERT_DATE"] != DBNull.Value ? Convert.ToDateTime(dr["INSERT_DATE"]) : DateTime.MinValue;

        //                lsc.Add(sc);
        //            }
        //        }


        //        dr.Close();
        //    }

        //    return lsc;

        //}

        public List<SuitabilityClients> HistoricoClientesSuitability(int? idCliente, string cdCliente, string login)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");

            var cmd = (SqlCommand)dbiCRK.CreateCommand("PR_SUITABILITY_SELECIONA_HISTORICO", CommandType.StoredProcedure);

            SuitabilityClients sc = new SuitabilityClients();
            List<SuitabilityClients> lsc = new List<SuitabilityClients>();
            cmd.Parameters.Clear();

            cmd.Parameters.Add("@pID_CLIENTE", SqlDbType.Decimal).Value = (object)idCliente ?? DBNull.Value;

            if (string.IsNullOrWhiteSpace(login))
                cmd.Parameters.Add("@pCD_LOGIN", SqlDbType.Decimal).Value = DBNull.Value;
            else
                cmd.Parameters.Add("@pCD_LOGIN", SqlDbType.Decimal).Value = login;

            cmd.Connection.Open();

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        sc = new SuitabilityClients();
                        sc.Id = dr["ID"] != DBNull.Value ? Convert.ToInt32(dr["ID"]) : 0;
                        sc.Status = dr["IDSTATUS"] != DBNull.Value ? Convert.ToInt32(dr["IDSTATUS"]) : 0;
                        sc.InvestorType = dr["IDTYPE"] != DBNull.Value ? Convert.ToInt32(dr["IDTYPE"]) : 0;
                        sc.CompletionDate = dr["INSERT_DATE"] != DBNull.Value ? Convert.ToDateTime(dr["INSERT_DATE"]) : DateTime.MinValue;
                        sc.UpdatedSolutionTech = dr["UPDATED_SOLUTIONTECH"] != DBNull.Value ? Convert.ToBoolean(dr["UPDATED_SOLUTIONTECH"]) : false;
                        sc.NotifiedBRP = dr["NOTIFIED_BRP"] != DBNull.Value ? Convert.ToBoolean(dr["NOTIFIED_BRP"]) : false;
                        sc.Score = dr["SCORE"] != DBNull.Value ? Convert.ToInt32(dr["SCORE"]) : (sc.Score = null);
                        sc.NeedUpdateDigitalPlatform = dr["NEED_TO_UPDATE_DIGITALPLATFORM"] != DBNull.Value ? Convert.ToBoolean(dr["NEED_TO_UPDATE_DIGITALPLATFORM"]) : false;
                        sc.SendDateDigitalPlatform = dr["SEND_DATE_DIGITALPLATFORM"] != DBNull.Value ? Convert.ToDateTime(dr["SEND_DATE_DIGITALPLATFORM"]) : (sc.SendDateDigitalPlatform = null);

                        lsc.Add(sc);
                    }
                }


                dr.Close();
            }

            cmd.Connection.Close();

            return lsc;

        }

        //public SuitabilityAnswersResponse SelecionaResultadoSuitability(string id)
        //{
        //    var cmd = dbi.CreateCommand("PCK_SUITABILITY.PROC_SELECIONA_RESPOSTAS", CommandType.StoredProcedure);
        //    SuitabilityAnswersResponse sar = new SuitabilityAnswersResponse();

        //    List<SuitabilityAnswers> lsa = new List<SuitabilityAnswers>();
        //    cmd.Parameters.Clear();

        //    if (!string.IsNullOrEmpty(id))
        //        cmd.Parameters.Add(new OracleParameter { ParameterName = "pID", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = Convert.ToInt32(id) });
        //    else
        //        cmd.Parameters.Add(new OracleParameter { ParameterName = "pID", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });

        //    cmd.Parameters.Add(new OracleParameter { ParameterName = "pRESULTADO", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });
        //    cmd.Parameters.Add(new OracleParameter { ParameterName = "pMATRIZES", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });
        //    cmd.Parameters.Add(new OracleParameter { ParameterName = "pCLIENTE", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

        //    sar.Answers = lsa;

        //    using (var dr = cmd.ExecuteReader())
        //    {
        //        if (dr.HasRows)
        //        {
        //            while (dr.Read())
        //            {

        //                var sc = new SuitabilityAnswers();
        //                sc.IdQuestion = dr["IDQUESTION"] != DBNull.Value ? Convert.ToInt32(dr["IDQUESTION"]) : 0;
        //                sc.IdAnswer = dr["IDANSWER"] != DBNull.Value ? Convert.ToInt32(dr["IDANSWER"]) : 0;
        //                sc.IdTypeInvestor = dr["IDTYPE"] != DBNull.Value ? Convert.ToInt32(dr["IDTYPE"]) : 0;
        //                sc.Answer = dr["ANSWER"] != DBNull.Value ? Convert.ToString(dr["ANSWER"]) : "";
        //                sc.Question = dr["QUESTION"] != DBNull.Value ? Convert.ToString(dr["QUESTION"]) : "";
        //                sc.Point = dr["POINT"] != DBNull.Value ? Convert.ToInt32(dr["POINT"]) : 0;
        //                decimal? idSub = dr["IDSUBQUESTION"] as decimal?;
        //                if (idSub != null)
        //                {
        //                    sc.SubQuestionId = (int?)idSub;
        //                }


        //                sc.QuestionType = (QuestionType)(int)((string)dr["CD_TIPO"])[0];
        //                sc.Percentage = (decimal)dr["PC_DISTRIBUICAO"];
        //                sc.IsCorrect = ((decimal)dr["IN_CORRET"]).Equals(1.0M);
        //                sc.PercentageDescription = dr["ds_percentual"] as string;

        //                lsa.Add(sc);
        //            }
        //        }

        //        sar.Answers = lsa;


        //        if (dr.NextResult())
        //        {
        //            while (dr.Read())
        //            {
        //                MatrixResult m = new MatrixResult();
        //                m.Name = (string)dr["DS_COLUNA"];
        //                m.IsChecked = dr["IN_CHECKED"].Equals(1.0M);

        //                int answerId = (int)(decimal)dr["ID_ANSWER"];

        //                var sc = sar.Answers.Single(s => s.IdAnswer == answerId);
        //                if (sc.MatrixData == null)
        //                    sc.MatrixData = new List<MatrixResult>();
        //                sc.MatrixData.Add(m);
        //            }
        //        }

        //        if (dr.NextResult())
        //        {
        //            if (dr.Read())
        //            {
        //                sar.GlobalClientId = (int)(decimal)dr["ID_CLIENTE"];
        //                sar.CdBolsa = dr["CDBOLSA"] != DBNull.Value ? Convert.ToString(dr["CDBOLSA"]) : "";
        //                sar.Date = dr["INSERT_DATE"] != DBNull.Value ? Convert.ToDateTime(dr["INSERT_DATE"]) : DateTime.MinValue;
        //                sar.IdTypeInvestor = dr["IDTYPE"] != DBNull.Value ? Convert.ToInt32(dr["IDTYPE"]) : 0;
        //                string login = dr["CD_LOGIN"] as string;
        //                if (!string.IsNullOrWhiteSpace(login))
        //                    sar.Name = dr["NOME"] as string;


        //                sar.CPFCNPJ = dr["NM_CLIENTE"] as string;
        //            }
        //        }


        //        dr.Close();
        //    }

        //    return sar;

        //}

        public SuitabilityAnswersResponse SelecionaResultadoSuitability(string id)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");

            var cmd = (SqlCommand)dbiCRK.CreateCommand("PR_SUITABILITY_SELECIONA_RESPOSTAS", CommandType.StoredProcedure);

            SuitabilityAnswersResponse sar = new SuitabilityAnswersResponse();

            List<SuitabilityAnswers> lsa = new List<SuitabilityAnswers>();
            cmd.Parameters.Clear();

            if (!string.IsNullOrEmpty(id))
                cmd.Parameters.Add("@pID", SqlDbType.Decimal).Value = Convert.ToInt32(id);
            else
                cmd.Parameters.Add("@pID", SqlDbType.Decimal).Value = DBNull.Value;

            cmd.Connection.Open();

            sar.Answers = lsa;

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        var sc = new SuitabilityAnswers();
                        sc.Id = dr["IDQ"] != DBNull.Value ? Convert.ToInt32(dr["IDQ"]) : 0;
                        sc.IdQuestion = dr["IDQUESTION"] != DBNull.Value ? Convert.ToInt32(dr["IDQUESTION"]) : 0;
                        sc.IdAnswer = dr["IDANSWER"] != DBNull.Value ? Convert.ToInt32(dr["IDANSWER"]) : 0;
                        sc.IdTypeInvestor = dr["IDTYPE"] != DBNull.Value ? Convert.ToInt32(dr["IDTYPE"]) : 0;
                        sc.Answer = dr["ANSWER"] != DBNull.Value ? Convert.ToString(dr["ANSWER"]) : "";
                        sc.Question = dr["QUESTION"] != DBNull.Value ? Convert.ToString(dr["QUESTION"]) : "";
                        sc.Point = dr["POINT"] != DBNull.Value ? Convert.ToInt32(dr["POINT"]) : 0;

                        decimal? idSub = dr["IDSUBQUESTION"] as decimal?;
                        if (idSub != null)
                            sc.SubQuestionId = (int?)idSub;

                        sc.QuestionType = (QuestionType)(int)((string)dr["CD_TIPO"])[0];
                        sc.Percentage = (decimal)dr["PC_DISTRIBUICAO"];
                        sc.IsCorrect = ((decimal)dr["IN_CORRET"]).Equals(1.0M);
                        sc.PercentageDescription = dr["ds_percentual"] as string;

                        lsa.Add(sc);
                    }
                }

                sar.Answers = lsa;


                if (dr.NextResult())
                {
                    while (dr.Read())
                    {
                        MatrixResult m = new MatrixResult();

                        m.IsChecked = dr["IN_CHECKED"].Equals(1);
                        //if (!m.IsChecked)
                        //    continue;

                        m.Id = (int)dr["ID_COLUNA"];
                        m.Name = (string)dr["DS_COLUNA"];
                        
                        
                        int answerId = (int)dr["ID_ANSWER"];

                        var sc = sar.Answers.SingleOrDefault(s => s.IdAnswer == answerId);
                        if (sc == null)
                            sc = new SuitabilityAnswers();
                        if (sc.MatrixData == null)
                            sc.MatrixData = new List<MatrixResult>();

                        sc.MatrixData.Add(m);
                    }
                }

                if (dr.NextResult())
                {
                    if (dr.Read())
                    {
                        sar.GlobalClientId = (int)dr["ID_CLIENTE"];
                        sar.CdBolsa = dr["CDBOLSA"] != DBNull.Value ? Convert.ToString(dr["CDBOLSA"]) : "";
                        sar.Date = dr["INSERT_DATE"] != DBNull.Value ? Convert.ToDateTime(dr["INSERT_DATE"]) : DateTime.MinValue;
                        sar.IdTypeInvestor = dr["IDTYPE"] != DBNull.Value ? Convert.ToInt32(dr["IDTYPE"]) : 0;
                        sar.Name = dr["NOME"] as string;
                        sar.CPFCNPJ = dr["CD_CPFCNPJ"] as string;
                        sar.Operator = dr["OPERATOR"] != DBNull.Value ? dr["OPERATOR"] as string : "";
                    }
                }


                dr.Close();
            }
            cmd.Connection.Close();
            return sar;

        }

        //public bool AtualizaStatusSuitability(int cdCliente, int status)
        //{
        //    var cmd = dbi.CreateCommand("PCK_SUITABILITY.PROC_ALTERA_STATUS", CommandType.StoredProcedure);
        //    bool res = false;
        //    try
        //    {
        //        cmd.Parameters.Clear();
        //        cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = cdCliente });
        //        cmd.Parameters.Add(new OracleParameter { ParameterName = "pSTATUS", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = status });

        //        using (var dr = cmd.ExecuteReader())
        //        {
        //            res = true;
        //        }
        //    }
        //    catch
        //    {
        //        return false;
        //    }

        //    return res;

        //}

        public bool AtualizaStatusSuitability(int cdCliente, int status)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PR_SUITABILITY_ALTERA_STATUS", CommandType.StoredProcedure);

            bool res = false;
            try
            {
                cmd.Parameters.Clear();

                cmd.Parameters.Add("@pID_CLIENTE", SqlDbType.Decimal).Value = cdCliente;
                cmd.Parameters.Add("@pSTATUS", SqlDbType.Decimal).Value = status;

                cmd.Connection.Open();

                using (var dr = cmd.ExecuteReader())
                {
                    res = true;
                }
            }
            catch
            {
                return false;
            }
            cmd.Connection.Close();
            return res;

        }

        //public List<SuitabilityClients> HistoricosSuitability(int? idCliente, string cdCliente, string dtInicio, string dtFim, string tipo)
        //{
        //    string query = "PCK_SUITABILITY.PROC_HISTORICO";

        //    if (tipo == "0")
        //        query = "PCK_SUITABILITY.PROC_HISTORICO_SEM_SUITABILITY";

        //    var cmd = dbi.CreateCommand(query, CommandType.StoredProcedure);

        //    SuitabilityClients sc = new SuitabilityClients();
        //    List<SuitabilityClients> lsc = new List<SuitabilityClients>();
        //    cmd.Parameters.Clear();

        //    cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = (object)idCliente ?? DBNull.Value });

        //    if (!string.IsNullOrEmpty(cdCliente))
        //        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = Convert.ToInt32(cdCliente) });
        //    else
        //        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });

        //    if (dtInicio != null)
        //        cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_INICIO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = dtInicio });
        //    else
        //        cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_INICIO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });

        //    if (dtFim != null)
        //        cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_FIM", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = dtFim });
        //    else
        //        cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_FIM", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });

        //    cmd.Parameters.Add(new OracleParameter { ParameterName = "pRESULTADO", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });


        //    using (var dr = cmd.ExecuteReader())
        //    {
        //        if (dr.HasRows)
        //        {
        //            while (dr.Read())
        //            {
        //                sc = new SuitabilityClients();
        //                sc.IdCliente = Convert.ToInt32(dr["ID_CLIENTE"]);
        //                sc.Id = dr["ID"] != DBNull.Value ? Convert.ToInt32(dr["ID"]) : 0;
        //                sc.Status = dr["IDSTATUS"] != DBNull.Value ? Convert.ToInt32(dr["IDSTATUS"]) : 0;
        //                sc.InvestorType = dr["IDTYPE"] != DBNull.Value ? Convert.ToInt32(dr["IDTYPE"]) : 0;
        //                sc.CompletionDate = dr["INSERT_DATE"] != DBNull.Value ? Convert.ToDateTime(dr["INSERT_DATE"]) : DateTime.MinValue;
        //                lsc.Add(sc);
        //            }
        //        }


        //        dr.Close();
        //    }

        //    return lsc;

        //}

        public List<SuitabilityClients> HistoricosSuitability(int? idCliente, string cdCliente, string dtInicio, string dtFim, string tipo)
        {
            string query = "PR_SUITABILITY_HISTORICO";

            if (tipo == "0")
                query = "PR_SUITABILITY_HISTORICO_SEM_SUITABILITY";

            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand(query, CommandType.StoredProcedure);


            SuitabilityClients sc = new SuitabilityClients();
            List<SuitabilityClients> lsc = new List<SuitabilityClients>();
            cmd.Parameters.Clear();

            cmd.Parameters.Add("@pSTATUS", SqlDbType.Decimal).Value = (object)idCliente ?? DBNull.Value;

            if (!string.IsNullOrEmpty(cdCliente))
                cmd.Parameters.Add("@pCD_CLIENTE", SqlDbType.Decimal).Value = Convert.ToInt32(cdCliente);
            else
                cmd.Parameters.Add("@pCD_CLIENTE", SqlDbType.Decimal).Value = DBNull.Value;

            if (dtInicio != null)
                cmd.Parameters.Add("@pDT_INICIO", SqlDbType.VarChar).Value = dtInicio;
            else
                cmd.Parameters.Add("@pDT_INICIO", SqlDbType.VarChar).Value = DBNull.Value;

            if (dtFim != null)
                cmd.Parameters.Add("@pDT_FIM", SqlDbType.VarChar).Value = dtFim;
            else
                cmd.Parameters.Add("@pDT_FIM", SqlDbType.VarChar).Value = DBNull.Value;

            cmd.Connection.Open();


            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        sc = new SuitabilityClients();
                        sc.IdCliente = Convert.ToInt32(dr["ID_CLIENTE"]);
                        sc.Id = dr["ID"] != DBNull.Value ? Convert.ToInt32(dr["ID"]) : 0;
                        sc.Status = dr["IDSTATUS"] != DBNull.Value ? Convert.ToInt32(dr["IDSTATUS"]) : 0;
                        sc.InvestorType = dr["IDTYPE"] != DBNull.Value ? Convert.ToInt32(dr["IDTYPE"]) : 0;
                        sc.CompletionDate = dr["INSERT_DATE"] != DBNull.Value ? Convert.ToDateTime(dr["INSERT_DATE"]) : DateTime.MinValue;
                        lsc.Add(sc);
                    }
                }


                dr.Close();
            }

            cmd.Connection.Close();

            return lsc;

        }

        /// <summary>
        /// Verifica desenquadramento para operações Bovespa
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idPerfil"></param>
        /// <param name="dtInicio"></param>
        /// <param name="dtFim"></param>
        /// <param name="tipo"></param>
        /// <returns></returns>
        //public List<SuitabilityUnframed> VerificaDesenquadramentoBovespa(int idCliente, int idPerfil, string dtInicio, string dtFim, string tipo)
        //{
        //    var cmd = dbi.CreateCommand("PCK_SUITABILITY.PROC_VERIFICA_DESENQUADRAMENTO", CommandType.StoredProcedure);

        //    SuitabilityUnframed sc = new SuitabilityUnframed();
        //    List<SuitabilityUnframed> lsc = new List<SuitabilityUnframed>();
        //    cmd.Parameters.Clear();


        //    cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = idCliente });
        //    cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });

        //    cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_PERFIL", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = idPerfil });

        //    if (dtInicio != null)
        //        cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_INICIO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = dtInicio });
        //    else
        //        cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_INICIO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });

        //    if (dtFim != null)
        //        cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_FIM", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = dtFim });
        //    else
        //        cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_FIM", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });

        //    if (tipo != null)
        //        cmd.Parameters.Add(new OracleParameter { ParameterName = "pTIPO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = tipo });
        //    else
        //        cmd.Parameters.Add(new OracleParameter { ParameterName = "pTIPO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });

        //    cmd.Parameters.Add(new OracleParameter { ParameterName = "pRESULTADO", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

        //    cmd.Connection.Open();

        //    using (var dr = cmd.ExecuteReader())
        //    {
        //        if (dr.HasRows)
        //        {
        //            while (dr.Read())
        //            {
        //                sc = new SuitabilityUnframed();

        //                sc.CdMercado = dr["CD_MERCAD"] != DBNull.Value ? Convert.ToString(dr["CD_MERCAD"]) : "";
        //                sc.DtDatOrd = dr["DT_DATORD"] != DBNull.Value ? Convert.ToDateTime(dr["DT_DATORD"]) : DateTime.MinValue;

        //                lsc.Add(sc);
        //            }
        //        }


        //        dr.Close();
        //    }

        //    return lsc;

        //}

        //VerificaDesenquadramentoBovespa será desativado / PCK_SUITABILITY.PROC_VERIFICA_DESENQUADRAMENTO

        //internal List<SuitabilityUnframedResponse> VerificaNovoDesenquadramento(int idCliente, DateTime? dtInicio, DateTime? dtFim)
        //{

        //    List<SuitabilityUnframedResponse> response = new List<SuitabilityUnframedResponse>();
        //    VerificaDesenquadramentoBovespa(response, idCliente, dtInicio, dtFim);
        //    VerificarDesenquadramentoCRK(response, idCliente, dtInicio, dtFim);

        //    return response;

        //}

        internal List<SuitabilityUnframedResponse> VerificaNovoDesenquadramento(int idCliente, DateTime? dtInicio, DateTime? dtFim)
        {

            List<SuitabilityUnframedResponse> response = new List<SuitabilityUnframedResponse>();
            //VerificaDesenquadramentoBovespa(response, idCliente, dtInicio, dtFim);
            VerificarDesenquadramentoCRK(response, idCliente, dtInicio, dtFim);

            bool unframed = false;

            if (response.Count > 0)
            {
                foreach (var item in response)
                {
                    unframed = item.Unframeds != null && item.Unframeds.Count > 0;
                    UpdateUnframedInformation(idCliente, item.CompletionDate, unframed);

                    //if (item.Unframeds != null && item.Unframeds.Count > 0)
                    //{
                    //    unframed = true;
                    //    break;
                    //}
                }
            }

            //UpdateUnframedInformation(idCliente, unframed);

            return response;
        }

        private void UpdateUnframedInformation(int idCliente, DateTime suitabilityDate, bool unframed)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PROC_ATUALIZA_DESENQUADRAMENTO", CommandType.StoredProcedure);
            cmd.Parameters.Add("@pID_CLIENTE", SqlDbType.Decimal).Value = idCliente;
            cmd.Parameters.Add("@pDT_SUITABILITY", SqlDbType.DateTime).Value = suitabilityDate;
            cmd.Parameters.Add("@pDESENQUADRADO", SqlDbType.VarChar).Value = unframed ? "S" : "N";

            cmd.Connection.Open();

            int result = cmd.ExecuteNonQuery();

            cmd.Connection.Close();

            //string sql = "UPDATE TB_CLIENTES SET DESENQUADRADO = @pDESENQUADRADO WHERE ID_CLIENTE = @pID_CLIENTE";

            //DBInterface dbiCRK = new DBInterface("CRK");
            //var cmd = (SqlCommand)dbiCRK.CreateCommand(sql, CommandType.Text);
            //cmd.Parameters.Add("@pDESENQUADRADO", SqlDbType.VarChar).Value = unframed ? "S" : "N";
            //cmd.Parameters.Add("@pID_CLIENTE", SqlDbType.Decimal).Value = idCliente;

            //cmd.Connection.Open();

            //int result = cmd.ExecuteNonQuery();

            //cmd.Connection.Close();

            //sql = "UPDATE TB_CA_USUARIO SET DESENQUADRADO = @pDESENQUADRADO WHERE ID_CLIENTE = @pID_CLIENTE";

            //cmd = (SqlCommand)dbiCRK.CreateCommand(sql, CommandType.Text);
            //cmd.Parameters.Add("@pDESENQUADRADO", SqlDbType.VarChar).Value = unframed ? "S" : "N";
            //cmd.Parameters.Add("@pID_CLIENTE", SqlDbType.Decimal).Value = idCliente;

            //cmd.Connection.Open();

            //result = cmd.ExecuteNonQuery();

            //cmd.Connection.Close();

            //if (unframed)
            //{
            //    sql = "UPDATE SUITABILITY_USER_STATUS_TYPE SET DESENQUADRADO = @pDESENQUADRADO WHERE ID_CLIENTE = @pID_CLIENTE";

            //    cmd = (SqlCommand)dbiCRK.CreateCommand(sql, CommandType.Text);
            //    cmd.Parameters.Add("@pDESENQUADRADO", SqlDbType.VarChar).Value = unframed ? "S" : "N";
            //    cmd.Parameters.Add("@pID_CLIENTE", SqlDbType.Decimal).Value = idCliente;

            //    cmd.Connection.Open();

            //    result = cmd.ExecuteNonQuery();

            //    cmd.Connection.Close();
            //}
        }

        private DataTable BuscarProdutosDesenquadramento(int idCliente, int idSistema, DateTime? dtInicio, DateTime? dtFim)
        {

            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PR_DESENQUADRAMENTO_PRODUTOS_DESENQUADRAMENTO", CommandType.StoredProcedure);

            cmd.Parameters.Add("@pID_CLIENTE", SqlDbType.Decimal).Value = idCliente;
            cmd.Parameters.Add("@pID_SISTEMA", SqlDbType.Decimal).Value = idSistema;
            cmd.Parameters.Add("@pDT_INICIO", SqlDbType.DateTime).Value = (object)dtInicio ?? DBNull.Value;
            cmd.Parameters.Add("@pDT_FIM", SqlDbType.DateTime).Value = (object)dtFim ?? DBNull.Value;

            cmd.Connection.Open();

            DataTable table = new DataTable();
            var da = dbiCRK.CreateDataAdapter();
            da.SelectCommand = cmd;
            da.Fill(table);
            cmd.Connection.Close();
            return table;

        }

        private void VerificarDesenquadramentoCRK(List<SuitabilityUnframedResponse> response, int idCliente, DateTime? dtInicio, DateTime? dtFim)
        {
            DataTable tbProdutos = BuscarProdutosDesenquadramento(idCliente, 2, dtInicio, dtFim);

            DataTable tbCodigos = new DataTable();
            tbCodigos.Columns.Add("CD_PRODUTO", typeof(string));

            using (DBInterface dbiCRK = new DBInterface("PORTAL"))
            {
                using (var cmd = (SqlCommand)dbiCRK.CreateCommand("PR_VERIFICAR_DESENQUADRAMENTO", CommandType.StoredProcedure))
                {
                    cmd.Parameters.Add("@CD_CPFCNPJ", SqlDbType.Decimal);
                    cmd.Parameters.Add("@PRODUTOS", SqlDbType.Structured);
                    cmd.Parameters.Add("@DT_INICIO", SqlDbType.Date);
                    cmd.Parameters.Add("@DT_FIM", SqlDbType.Date);

                    cmd.Connection.Open();

                    foreach (DataRow rowProduto in tbProdutos.Rows)
                    {
                        decimal cpfcnpj = (decimal)rowProduto["CD_CPFCNPJ"];
                        int idPerfil = (int)(decimal)rowProduto["idtype"];
                        string descricaoPerfil = (string)rowProduto["DESCRIPTION"];
                        DateTime dataInicio = (DateTime)rowProduto["DT_INICIO"];
                        DateTime dataFim = (DateTime)rowProduto["DT_FIM"];
                        string descricaoProdutos = (string)rowProduto["DS_PRODUTO"];

                        string[] produtos = ((string)rowProduto["CD_PRODUTOS"]).Split(',');

                        tbCodigos.Rows.Clear();
                        foreach (string codigo in produtos)
                        {
                            DataRow rowCodigo = tbCodigos.NewRow();
                            rowCodigo.SetField<string>("CD_PRODUTO", codigo);
                            tbCodigos.Rows.Add(rowCodigo);
                        }

                        cmd.Parameters["@CD_CPFCNPJ"].Value = cpfcnpj;
                        cmd.Parameters["@PRODUTOS"].Value = tbCodigos;
                        cmd.Parameters["@DT_INICIO"].Value = dataInicio; // DBNull.Value; 
                        cmd.Parameters["@DT_FIM"].Value = dtFim.HasValue ? dataFim : DateTime.Now;

                        using (var dr = cmd.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                SuitabilityUnframedResponse resp;

                                resp = response.Where(r => r.CompletionDate == dataInicio && r.InvestorType == idPerfil).SingleOrDefault();
                                if (resp == null)
                                {
                                    resp = new SuitabilityUnframedResponse();

                                    resp.InvestorType = idPerfil;
                                    resp.InvestorTypeDesc = descricaoPerfil;
                                    resp.CompletionDate = dataInicio;
                                    resp.Unframeds = new List<SuitabilityUnframed>();
                                }
                                while (dr.Read())
                                {
                                    SuitabilityUnframed item = new SuitabilityUnframed();
                                    item.CdMercado = descricaoProdutos;  //(string)dr["CD_PRODUTO"];
                                    item.DtDatOrd = (DateTime)dr["DT_OPERACAO"];
                                    resp.Unframeds.Add(item);
                                }
                                response.Add(resp);
                            }
                        }


                    }
                    cmd.Connection.Close();
                }

            }

        }

        //private void VerificaDesenquadramentoBovespa(List<SuitabilityUnframedResponse> response, int idCliente, DateTime? dtInicio, DateTime? dtFim)
        //{
        //    var cmd = dbi.CreateCommand("PCK_DESENQUADRAMENTO.DESENQUADRAMENTO_BOVESPA", CommandType.StoredProcedure);

        //    cmd.CreateParam("pID_CLIENTE", idCliente);
        //    cmd.CreateParam("pDT_INICIO", (object)dtInicio ?? DBNull.Value);
        //    cmd.CreateParam("pDT_FIM", (object)dtFim ?? DBNull.Value);
        //    cmd.CreateOracleCursor("cDADOS");

        //    cmd.Connection.Open();

        //    using (var dr = cmd.ExecuteReader())
        //    {

        //        while (dr.Read())
        //        {
        //            SuitabilityUnframed unframed = new SuitabilityUnframed();
        //            unframed.CdMercado = (string)dr["CD_MERCAD"];
        //            unframed.DtDatOrd = (DateTime)dr["DT_DATORD"];

        //            DateTime dtSuitability = (DateTime)dr["DT_SUITABILITY"];
        //            int idInvestidor = Convert.ToInt32(dr["ID_INVESTIDOR"]);
        //            string dsInvestidor = (string)dr["DS_INVESTIDOR"];

        //            SuitabilityUnframedResponse r = response.Find(x => x.InvestorType == idInvestidor);
        //            if (r == null)
        //            {
        //                r = new SuitabilityUnframedResponse();
        //                r.InvestorType = idInvestidor;
        //                r.InvestorTypeDesc = dsInvestidor;
        //                r.Unframeds = new List<SuitabilityUnframed>();
        //                r.CompletionDate = dtSuitability;
        //                response.Add(r);
        //            }
        //            r.Unframeds.Add(unframed);

        //        }
        //    }
        //}

        //VerificaDesenquadramentoBovespa será desavivado / PCK_DESENQUADRAMENTO.DESENQUADRAMENTO_BOVESPA

        /// <summary>
        /// Recupera os dados do cliente da CRK
        /// </summary>
        /// <param name="documentNumbers">Lista de CPFs a serem buscados</param>
        /// <returns></returns>
        public List<CorporateClient> GetCorporateInfo(long[] documentNumbers)
        {
            SortedList<long, CorporateClient> list = new SortedList<long, CorporateClient>();
            using (DBInterface dbi = new DBInterface("PORTAL"))
            {
                using (var cmd = (SqlCommand)dbi.CreateCommand("PR_BUSCAR_DADOS_CLIENTE", CommandType.StoredProcedure))
                {
                    DataTable tb = new DataTable();
                    tb.Columns.Add("CD_CPFCNPJ", typeof(decimal));

                    foreach (long number in documentNumbers)
                    {
                        DataRow row = tb.NewRow();
                        row["CD_CPFCNPJ"] = number;
                        tb.Rows.Add(row);
                    }

                    cmd.Parameters.Add("@CLIENTES", SqlDbType.Structured);
                    cmd.Parameters["@CLIENTES"].Value = tb;

                    dbi.Connection.Open();
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            long cpfcnpj = Convert.ToInt64(dr["CD_CPFCNPJ"]);
                            string name = (string)dr["NM_CLIENTE"];
                            int code = Convert.ToInt32(dr["CD_CLIENTe"]);


                            if (list.ContainsKey(cpfcnpj))
                            {
                                CorporateClient c = list[cpfcnpj];
                                c.Codes.Add(code);
                            }
                            else
                            {
                                CorporateClient c = new CorporateClient();
                                c.Document = cpfcnpj;
                                c.Name = name;
                                c.Codes = new List<int>();
                                c.Codes.Add(code);
                                list.Add(cpfcnpj, c);
                            }
                        }
                    }
                    dbi.Connection.Close();
                }
            }
            return list.Values.ToList();

        }

        public List<SuitabilityClients> GetCrkClients()
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");

            var cmd = (SqlCommand)dbiCRK.CreateCommand("PROC_LISTA_CLIENTES_CRK", CommandType.StoredProcedure);

            SuitabilityClients sc = new SuitabilityClients();
            List<SuitabilityClients> lsc = new List<SuitabilityClients>();
            cmd.Parameters.Clear();

            cmd.Connection.Open();

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        sc = new SuitabilityClients();

                        var cdBolsa = new int[] { 0 };
                        if (dr["CDBOLSA"] != DBNull.Value)
                        {
                            cdBolsa[0] = Convert.ToInt32(dr["CDBOLSA"]);
                        }
                        else
                            cdBolsa[0] = 0;

                        sc.CodigosBolsa = cdBolsa;

                        sc.CPFCNPJ = Convert.ToInt64(dr["CD_CPFCNPJ"]);
                        sc.ClientName = dr["NOME"].ToString();
                        sc.Email = dr["EMAIL"].ToString();
                        sc.Login = dr["LOGIN"].ToString();
                        sc.PersonType = dr["TP_PESSOA"].ToString();

                        sc.Active = Convert.ToInt32(dr["ATIVO"]) == 1 ? true : false;
                        sc.InvestorKind = dr["TIPO_INVESTIDOR"] != DBNull.Value ? Convert.ToInt32(dr["TIPO_INVESTIDOR"]) : 0;

                        sc.UserType = dr["TIPOUSUARIO"].ToString();
                        sc.ProfileId = Convert.ToInt32(dr["IDPERFIL"]);

                        sc.SendSuitabilityEmail = Convert.ToInt32(dr["ENVIAR_EMAIL_SUITABILITY"]) == 1 ? true : false;

                        lsc.Add(sc);
                    }
                }

                dr.Close();
            }

            cmd.Connection.Close();

            return lsc;
        }

        public List<StatementClient> GetClients(StatementClientFilter filter)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");

            var cmd = (SqlCommand)dbiCRK.CreateCommand("EXTRATO_BUSCAR_CLIENTES", CommandType.StoredProcedure);

            List<StatementClient> lst = new List<StatementClient>();
            cmd.Parameters.Clear();

            if (filter.ClientId.HasValue)
                cmd.Parameters.Add("@pID_CLIENTE", SqlDbType.Decimal).Value = filter.ClientId.Value;
            else
                cmd.Parameters.Add("@pID_CLIENTE", SqlDbType.Decimal).Value = DBNull.Value;

            if (filter.CpfCnpj.HasValue && filter.CpfCnpj.Value != 0)
                cmd.Parameters.Add("@pCD_CPFCNPJ", SqlDbType.Decimal).Value = Convert.ToInt64(filter.CpfCnpj.Value);
            else
                cmd.Parameters.Add("@pCD_CPFCNPJ", SqlDbType.Decimal).Value = DBNull.Value;

            if (filter.ClientCode.HasValue)
                cmd.Parameters.Add("@pCD_CLIENTE", SqlDbType.Decimal).Value = Convert.ToInt32(filter.ClientCode);
            else
                cmd.Parameters.Add("@pCD_CLIENTE", SqlDbType.Decimal).Value = DBNull.Value;

            if (!string.IsNullOrEmpty(filter.Name))
                cmd.Parameters.Add("@pNOME", SqlDbType.VarChar).Value = filter.Name;
            else
                cmd.Parameters.Add("@pNOME", SqlDbType.VarChar).Value = DBNull.Value;

            if (filter.AutomaticSending.HasValue)
                cmd.Parameters.Add("@pENVIO_AUTO", SqlDbType.Decimal).Value = filter.AutomaticSending.Value;
            else
                cmd.Parameters.Add("@pENVIO_AUTO", SqlDbType.Decimal).Value = DBNull.Value;

            cmd.Connection.Open();

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        var sc = new StatementClient();

                        sc.Id = Convert.ToInt32(dr["ID"]);
                        sc.CpfCnpj = Convert.ToInt64(dr["CD_CPFCNPJ"]);
                        sc.ClientId = dr["ID_CLIENTE"] != DBNull.Value ? Convert.ToInt32(dr["ID_CLIENTE"]) : 0;
                        sc.Name = dr["NOME"].ToString();
                        sc.ClientCode = dr["CDBOLSA"] != DBNull.Value ? Convert.ToInt32(dr["CDBOLSA"]) : 0;
                        sc.Email = dr["EMAIL"] != DBNull.Value ? Convert.ToString(dr["EMAIL"]) : "";
                        sc.PersonType = dr["TP_PESSOA"] != DBNull.Value ? Convert.ToString(dr["TP_PESSOA"]) : "";

                        if (dr["ENVIO_AUTO"] != DBNull.Value)
                            sc.AutomaticSending = Convert.ToInt32(dr["ENVIO_AUTO"]) == 1;

                        string x = dr["EXTRATOS"] != DBNull.Value ? dr["EXTRATOS"].ToString() : "";
                        string[] ids = x.Split('-');

                        sc.StatementIDs = new List<int>();

                        if (!string.IsNullOrEmpty(x))
                        {
                            foreach (var item in ids)
                                sc.StatementIDs.Add(Convert.ToInt32(item));
                        }

                        lst.Add(sc);
                    }
                }

                dr.Close();
            }

            cmd.Connection.Close();

            return lst;
        }

        public List<FixedIncomeDetails> GetFixedIncome(FixedIncomeFilter filter)
        {
            List<FixedIncomeDetails> result = new List<FixedIncomeDetails>();

            DBInterface dbiCRK = new DBInterface("CRK_REN");

            var cmd = (SqlCommand)dbiCRK.CreateCommand("SP_REN_EXTRATO", CommandType.StoredProcedure);

            cmd.Parameters.Clear();

            cmd.Parameters.Add(new SqlParameter { ParameterName = "@tp_codempresa", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input, Value = 1 });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@cd_cpf_cnpj", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = filter.CpfCnpj.ToString() });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@dt_ref_de", SqlDbType = SqlDbType.DateTime, Direction = ParameterDirection.Input, Value = Convert.ToDateTime(filter.Start.Value) });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@dt_ref_ate", SqlDbType = SqlDbType.DateTime, Direction = ParameterDirection.Input, Value = Convert.ToDateTime(filter.End.Value) });

            cmd.Connection.Open();

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        FixedIncomeDetails obj = new FixedIncomeDetails();

                        obj.tp_registro = dr["tp_registro"] != DBNull.Value ? dr["tp_registro"].ToString() : "";
                        obj.dt_operacao = dr["dt_operacao"] != DBNull.Value ? dr["dt_operacao"].ToString() : "";
                        obj.dc_evento = dr["dc_evento"] != DBNull.Value ? dr["dc_evento"].ToString() : "";
                        obj.dc_grupo_papel = dr["dc_grupo_papel"] != DBNull.Value ? dr["dc_grupo_papel"].ToString() : "";
                        obj.dc_indexador = dr["dc_indexador"] != DBNull.Value ? dr["dc_indexador"].ToString() : "";
                        obj.tx_operacao = dr["tx_operacao"] != DBNull.Value ? Convert.ToDecimal(dr["tx_operacao"]) : 0;
                        obj.tx_percentual = dr["tx_percentual"] != DBNull.Value ? Convert.ToDecimal(dr["tx_percentual"]) : 0;
                        obj.dt_vencimento = dr["dt_vencimento"] != DBNull.Value ? dr["dt_vencimento"].ToString() : "";
                        obj.vl_operacao = dr["vl_operacao"] != DBNull.Value ? Convert.ToDecimal(dr["vl_operacao"]) : 0;
                        obj.tx_ir = dr["tx_ir"] != DBNull.Value ? Convert.ToDecimal(dr["tx_ir"]) : 0;
                        obj.tx_iof = dr["tx_iof"] != DBNull.Value ? Convert.ToDecimal(dr["tx_iof"]) : 0;
                        obj.vl_bruto = dr["vl_bruto"] != DBNull.Value ? Convert.ToDecimal(dr["vl_bruto"]) : 0;
                        obj.vl_liquido = dr["vl_liquido"] != DBNull.Value ? Convert.ToDecimal(dr["vl_liquido"]) : 0;

                        result.Add(obj);
                    }
                }

                dr.Close();
            }

            cmd.Connection.Close();

            return result;
        }

        public List<NdfDetails> GetNdf(NdfFilter filter)
        {
            List<NdfDetails> result = new List<NdfDetails>();

            DBInterface dbiCRK = new DBInterface("CRK");

            string procedureName = filter.Type == NdfSwapType.CurvaContabil ? "SP_SWP_EXTRATO_CONTABIL_NDF" : "SP_SWP_EXTRATO_MERCADO_NDF";
            var cmd = (SqlCommand)dbiCRK.CreateCommand(procedureName, CommandType.StoredProcedure);

            cmd.Parameters.Clear();

            cmd.Parameters.Add(new SqlParameter { ParameterName = "@dt_mes", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input, Value = filter.Month });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@dt_ano", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input, Value = filter.Year });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@cd_cpf_cnpj", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = filter.CpfCnpj.ToString() });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@tp_codempresa", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input, Value = 1 });

            cmd.Connection.Open();

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        NdfDetails obj = new NdfDetails();

                        obj.tp_operacao = dr["tp_operacao"] != DBNull.Value ? dr["tp_operacao"].ToString() : "";
                        obj.dt_operacao = dr["dt_operacao"] != DBNull.Value ? dr["dt_operacao"].ToString() : "";
                        obj.dt_vencimento = dr["dt_vencimento"] != DBNull.Value ? dr["dt_vencimento"].ToString() : "";
                        obj.tp_moeda_ref = dr["tp_moeda_ref"] != DBNull.Value ? dr["tp_moeda_ref"].ToString() : "";
                        
                        obj.dc_criterio_termo = dr["dc_criterio_termo"] != DBNull.Value ? dr["dc_criterio_termo"].ToString() : "";
                        obj.dc_criterio_vencto = dr["dc_criterio_vencto"] != DBNull.Value ? dr["dc_criterio_vencto"].ToString() : "";
                        obj.vl_cot_termo_fwd = dr["vl_cot_termo_fwd"] != DBNull.Value ? Convert.ToDecimal(dr["vl_cot_termo_fwd"]) : 0;
                        obj.vl_base = dr["vl_base"] != DBNull.Value ? Convert.ToDecimal(dr["vl_base"]) : 0;
                        obj.vl_bruto = dr["vl_bruto"] != DBNull.Value ? Convert.ToDecimal(dr["vl_bruto"]) : 0;
                        obj.vl_liquido = dr["vl_liquido"] != DBNull.Value ? Convert.ToDecimal(dr["vl_liquido"]) : 0;
                        obj.vl_base_moeda_estr = dr["vl_base_moeda_estr"] != DBNull.Value ? Convert.ToDecimal(dr["vl_base_moeda_estr"]) : 0;

                        // Curva contábil
                        if (filter.Type == NdfSwapType.CurvaContabil)
                        {
                            obj.tp_moeda_cot = dr["tp_moeda_cot"] != DBNull.Value ? dr["tp_moeda_cot"].ToString() : "";
                            obj.vl_ir = dr["vl_ir"] != DBNull.Value ? Convert.ToDecimal(dr["vl_ir"]) : 0;
                        }

                        // Curva mercado
                        if (filter.Type == NdfSwapType.CurvaMercado)
                        {
                            obj.vlr_resultado_mtm = dr["vlr_resultado_mtm"] != DBNull.Value ? Convert.ToDecimal(dr["vlr_resultado_mtm"]) : 0;
                        }

                        result.Add(obj);
                    }
                }

                dr.Close();
            }

            cmd.Connection.Close();

            return result;
        }

        public List<SwapDetails> GetSwap(SwapFilter filter)
        {
            List<SwapDetails> result = new List<SwapDetails>();

            DBInterface dbiCRK = new DBInterface("CRK");

            string procedureName = filter.Type == NdfSwapType.CurvaContabil ? "SP_SWP_EXTRATO_CONTABIL" : "SP_SWP_EXTRATO_MERCADO";
            var cmd = (SqlCommand)dbiCRK.CreateCommand(procedureName, CommandType.StoredProcedure);

            cmd.Parameters.Clear();

            cmd.Parameters.Add(new SqlParameter { ParameterName = "@dt_mes", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input, Value = filter.Month });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@dt_ano", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input, Value = filter.Year });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@cd_cpf_cnpj", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = filter.CpfCnpj.ToString() });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@tp_codempresa", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input, Value = 1 });

            cmd.Connection.Open();

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        SwapDetails obj = new SwapDetails();

                        obj.tp_operacao = dr["tp_operacao"] != DBNull.Value ? dr["tp_operacao"].ToString() : "";
                        obj.dt_operacao = dr["dt_operacao"] != DBNull.Value ? dr["dt_operacao"].ToString() : "";
                        obj.dt_vencimento = dr["dt_vencimento"] != DBNull.Value ? dr["dt_vencimento"].ToString() : "";
                        obj.vl_base = dr["vl_base"] != DBNull.Value ? Convert.ToDecimal(dr["vl_base"]) : 0;
                        obj.dc_index_dado = dr["dc_index_dado"] != DBNull.Value ? dr["dc_index_dado"].ToString() : "";
                        obj.vl_perc_index_dado = dr["vl_perc_index_dado"] != DBNull.Value ? Convert.ToDecimal(dr["vl_perc_index_dado"]) : 0;
                        obj.vl_taxa_dado = dr["vl_taxa_dado"] != DBNull.Value ? Convert.ToDecimal(dr["vl_taxa_dado"]) : 0;
                        obj.dc_index_tomado = dr["dc_index_tomado"] != DBNull.Value ? dr["dc_index_tomado"].ToString() : "";
                        obj.vl_perc_index_tomado = dr["vl_perc_index_tomado"] != DBNull.Value ? Convert.ToDecimal(dr["vl_perc_index_tomado"]) : 0;
                        obj.vl_taxa_tomado = dr["vl_taxa_tomado"] != DBNull.Value ? Convert.ToDecimal(dr["vl_taxa_tomado"]) : 0;

                        // Curva contábil
                        if (filter.Type == NdfSwapType.CurvaContabil)
                        {
                            obj.vl_curva_dado = dr["vl_curva_dado"] != DBNull.Value ? Convert.ToDecimal(dr["vl_curva_dado"]) : 0;
                            obj.vl_curva_tomado = dr["vl_curva_tomado"] != DBNull.Value ? Convert.ToDecimal(dr["vl_curva_tomado"]) : 0;
                            obj.vl_bruto = dr["vl_bruto"] != DBNull.Value ? Convert.ToDecimal(dr["vl_bruto"]) : 0;
                            obj.vl_ir = dr["vl_ir"] != DBNull.Value ? Convert.ToDecimal(dr["vl_ir"]) : 0;
                            obj.vl_liquido = dr["vl_liquido"] != DBNull.Value ? Convert.ToDecimal(dr["vl_liquido"]) : 0;
                        }

                        // Curva mercado
                        if (filter.Type == NdfSwapType.CurvaMercado)
                        {
                            obj.vl_mtm_dado = dr["vl_mtm_dado"] != DBNull.Value ? Convert.ToDecimal(dr["vl_mtm_dado"]) : 0;
                            obj.vl_mtm_tomado = dr["vl_mtm_tomado"] != DBNull.Value ? Convert.ToDecimal(dr["vl_mtm_tomado"]) : 0;
                            obj.vlr_resultado_mtm = dr["vlr_resultado_mtm"] != DBNull.Value ? Convert.ToDecimal(dr["vlr_resultado_mtm"]) : 0;
                        }

                        result.Add(obj);
                    }
                }

                dr.Close();
            }

            cmd.Connection.Close();

            return result;
        }

        public StatementConfiguration GetStatementConfiguration(int clientId)
        {
            StatementConfiguration result = new StatementConfiguration();
            result.HasConfiguration = false;
            result.ClientId = clientId;
            result.StatementIDs = new List<int>();

            DBInterface dbi = new DBInterface("PORTAL");

            string sql = @"select ENVIO_AUTO, EXTRATOS from EXTRATO_CONFIGURACAO where ID_CLIENTE = @id_cliente";

            var cmd = (SqlCommand)dbi.CreateCommand(sql, CommandType.Text);

            cmd.Parameters.Clear();

            cmd.Parameters.Add(new SqlParameter { ParameterName = "@id_cliente", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input, Value = clientId });

            cmd.Connection.Open();

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    if (dr.Read())
                    {
                        StatementConfiguration obj = new StatementConfiguration();
                        result.HasConfiguration = true;
                        result.AutomaticSending = Convert.ToInt32(dr["ENVIO_AUTO"]) == 1;

                        string x = dr["EXTRATOS"] != DBNull.Value ? dr["EXTRATOS"].ToString() : "";
                        if (!string.IsNullOrEmpty(x))
                        {
                            string[] ids = x.Split('-');

                            foreach (var item in ids)
                                result.StatementIDs.Add(Convert.ToInt32(item));
                        }
                    }
                }

                dr.Close();
            }

            cmd.Connection.Close();

            return result;
        }

        public List<StatementLog> GetStatementLog(StatementLog filter)
        {
            List<StatementLog> result = new List<StatementLog>();

            DBInterface dbi = new DBInterface("PORTAL");

            var cmd = (SqlCommand)dbi.CreateCommand("EXTRATO_BUSCAR_LOG", CommandType.StoredProcedure);

            cmd.Parameters.Clear();

            if (filter.ClientId.HasValue)
                cmd.Parameters.Add("@pID_CLIENTE", SqlDbType.Decimal).Value = filter.ClientId.Value;
            else
                cmd.Parameters.Add("@pID_CLIENTE", SqlDbType.Decimal).Value = DBNull.Value;

            if (filter.CpfCnpj.HasValue && filter.CpfCnpj.Value != 0)
                cmd.Parameters.Add("@pCD_CPFCNPJ", SqlDbType.Decimal).Value = Convert.ToInt64(filter.CpfCnpj.Value);
            else
                cmd.Parameters.Add("@pCD_CPFCNPJ", SqlDbType.Decimal).Value = DBNull.Value;

            if (filter.ClientCode.HasValue)
                cmd.Parameters.Add("@pCD_CLIENTE", SqlDbType.Decimal).Value = Convert.ToInt32(filter.ClientCode);
            else
                cmd.Parameters.Add("@pCD_CLIENTE", SqlDbType.Decimal).Value = DBNull.Value;

            if (!string.IsNullOrEmpty(filter.Name))
                cmd.Parameters.Add("@pNOME", SqlDbType.VarChar).Value = filter.Name;
            else
                cmd.Parameters.Add("@pNOME", SqlDbType.VarChar).Value = DBNull.Value;

            if (filter.Year > 0)
                cmd.Parameters.Add("@pANO", SqlDbType.VarChar).Value = filter.Year;
            else
                cmd.Parameters.Add("@pANO", SqlDbType.VarChar).Value = DBNull.Value;

            if (filter.Month > 0)
                cmd.Parameters.Add("@pMES", SqlDbType.VarChar).Value = filter.Month;
            else
                cmd.Parameters.Add("@pMES", SqlDbType.VarChar).Value = DBNull.Value;

            if (filter.StatementIDs != null && filter.StatementIDs.Count > 0)
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@pEXTRATO", SqlDbType = SqlDbType.VarChar, Value = filter.StatementIDs[0].ToString(), Direction = ParameterDirection.Input });
            else
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@pEXTRATO", SqlDbType = SqlDbType.VarChar, Value = DBNull.Value, Direction = ParameterDirection.Input });


            cmd.Connection.Open();

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        StatementLog obj = new StatementLog();

                        obj.CpfCnpj = Convert.ToInt64(dr["CD_CPFCNPJ"]);
                        obj.ClientId = dr["ID_CLIENTE"] != DBNull.Value ? Convert.ToInt32(dr["ID_CLIENTE"]) : 0;
                        obj.Name = dr["NOME"].ToString();
                        obj.ClientCode = dr["CDBOLSA"] != DBNull.Value ? Convert.ToInt32(dr["CDBOLSA"]) : 0;
                        obj.Id = Convert.ToInt32(dr["ID"]);
                        obj.Year = Convert.ToInt32(dr["ANO"]);
                        obj.Month = Convert.ToInt32(dr["MES"]);

                        string x = dr["EXTRATOS"] != DBNull.Value ? dr["EXTRATOS"].ToString() : "";
                        string[] ids = x.Split('-');

                        obj.StatementIDs = new List<int>();

                        if (!string.IsNullOrEmpty(x))
                        {
                            foreach (var item in ids)
                                obj.StatementIDs.Add(Convert.ToInt32(item));
                        }

                        obj.SendDate = Convert.ToDateTime(dr["DATA_ENVIO"]);

                        result.Add(obj);
                    }
                }

                dr.Close();
            }

            cmd.Connection.Close();

            return result;
        }

        public int InsertStatementConfiguration(StatementConfiguration obj)
        {

            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("EXTRATO_INSERIR_CONFIGURACAO", CommandType.StoredProcedure);

            cmd.Connection.Open();

            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pIdCliente", SqlDbType = SqlDbType.Int, Value = obj.ClientId, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pEnvioAuto", SqlDbType = SqlDbType.Int, Value = obj.AutomaticSending ? 1 : 0, Direction = ParameterDirection.Input });

            if (obj.StatementIDs != null && obj.StatementIDs.Count > 0)
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@pExtratos", SqlDbType = SqlDbType.VarChar, Value = string.Join("-", obj.StatementIDs), Direction = ParameterDirection.Input });
            else
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@pExtratos", SqlDbType = SqlDbType.VarChar, Value = DBNull.Value, Direction = ParameterDirection.Input });

            int ret = cmd.ExecuteNonQuery();

            cmd.Connection.Close();

            return ret;
        }

        public int InsertStatementLog(StatementLog obj)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("EXTRATO_INSERIR_LOG", CommandType.StoredProcedure);

            cmd.Connection.Open();

            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pIdCliente", SqlDbType = SqlDbType.Int, Value = obj.ClientId, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pAno", SqlDbType = SqlDbType.Int, Value = obj.Year, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pMes", SqlDbType = SqlDbType.Int, Value = obj.Month, Direction = ParameterDirection.Input });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pExtratos", SqlDbType = SqlDbType.VarChar, Value = string.Join("-", obj.StatementIDs), Direction = ParameterDirection.Input });

            int ret = cmd.ExecuteNonQuery();

            cmd.Connection.Close();

            return ret;
        }

        public List<SuitabilityInvestorTypeXProduct> GetInvestorTypeXProduct()
        {
            List<SuitabilityInvestorTypeXProduct> result = new List<SuitabilityInvestorTypeXProduct>();

            DBInterface dbiCRK = new DBInterface("PORTAL");

            var cmd = (SqlCommand)dbiCRK.CreateCommand("PR_SUITABILITY_SELECIONAR_PERFIL_X_PRODUTOS", CommandType.StoredProcedure);

            cmd.Connection.Open();

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        SuitabilityInvestorTypeXProduct obj = new SuitabilityInvestorTypeXProduct();

                        obj.Id = dr["ID_PERFIL"] != DBNull.Value ? Convert.ToInt32(dr["ID_PERFIL"]) : 0;
                        obj.Type = dr["DESCRIPTION"] != DBNull.Value ? dr["DESCRIPTION"].ToString() : "";
                        obj.ProductCode = dr["CD_PRODUTO"] != DBNull.Value ? dr["CD_PRODUTO"].ToString() : "";
                        obj.ProductDescription = dr["DS_PRODUTO"] != DBNull.Value ? dr["DS_PRODUTO"].ToString() : "";

                        result.Add(obj);
                    }
                }

                dr.Close();
            }

            cmd.Connection.Close();

            return result;
        }

        public SuitabilityClients GetClient(long cpfCnpj)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");

            var cmd = (SqlCommand)dbiCRK.CreateCommand("PR_SUITABILITY_SELECIONAR_CLIENTE", CommandType.StoredProcedure);

            SuitabilityClients sc = new SuitabilityClients();

            cmd.Parameters.Clear();

            cmd.Parameters.Add("@pCD_CPFCNPJ", SqlDbType.Decimal).Value = (object)cpfCnpj ?? DBNull.Value;

            cmd.Connection.Open();

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    if (dr.Read())
                    {
                        sc.IdCliente = Convert.ToInt32(dr["ID_CLIENTE"]);
                        sc.CPFCNPJ = Convert.ToInt64(dr["CD_CPFCNPJ"]);
                        sc.ClientName = dr["NOME"].ToString();
                        sc.Status = dr["SUITABILITY_STATUS"] != DBNull.Value ? Convert.ToInt32(dr["SUITABILITY_STATUS"]) : 0;
                        sc.Unframed = dr["DESENQUADRADO"] != DBNull.Value ? Convert.ToString(dr["DESENQUADRADO"]) : "";
                        sc.PersonType = dr["TP_PESSOA"].ToString();
                        sc.InvestorType = dr["INVESTIDOR"] != DBNull.Value ? Convert.ToInt32(dr["INVESTIDOR"]) : 0;
                        sc.InvestorKind = dr["TIPO_INVESTIDOR"] != DBNull.Value ? Convert.ToInt32(dr["TIPO_INVESTIDOR"]) : 0;
                        sc.CompletionDate = dr["INSERT_DATE"] != DBNull.Value ? Convert.ToDateTime(dr["INSERT_DATE"]) : DateTime.MinValue;
                        sc.Id = dr["ID"] != DBNull.Value ? Convert.ToInt32(dr["ID"]) : 0;
                    }
                }

                dr.Close();
            }

            cmd.Connection.Close();

            return sc;
        }
    }

}

