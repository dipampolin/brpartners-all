﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Data.OracleClient;
using QX3.Spinnex.Common.Services.Data;
using QX3.Portal.Contracts.DataContracts;
using QX3.Spinnex.Common.Services.Logging;
using System.Linq;

namespace QX3.Portal.Services.Data
{
	public class ReportDAL : DataProviderBase
	{
		public enum ReportType { NotSet, Confirmation, CustomerStatement, CustomerLedger, StockRecord }

		public static IFormatProvider cultureInfo = new System.Globalization.CultureInfo("en-US", true);

		public ReportDAL() : base("PortalConnectionString") { }

		private void LoadCompanyAddress(ref ReportHeader header)
		{
			var cmd = dbi.CreateCommand("PCK_RELATORIOS_BV_SECURITIES.PROC_ENDERECO_CORRETORA", System.Data.CommandType.StoredProcedure);
			cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSOR", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

			using (var dr = cmd.ExecuteReader())
			{
				while (dr.Read())
				{
                    if (!(dr["DS_ENDE_EMPR"] is DBNull))
                        header.AddressLine1 = Convert.ToString(dr["DS_ENDE_EMPR"]);
					if (!(dr["NR_ENDE_EMPR"] is DBNull))
						header.Number = Convert.ToString(dr["NR_ENDE_EMPR"]);
					if (!(dr["DS_COMP_EMPR"] is DBNull))
						header.AddressLine2 = Convert.ToString(dr["DS_COMP_EMPR"]);
					if (!(dr["CD_CEP"] is DBNull))
						header.PostalCode = Convert.ToString(dr["CD_CEP"]);
					if (!(dr["CD_CEP_EXT"] is DBNull))
						header.PostalCodeExtension = Convert.ToString(dr["CD_CEP_EXT"]);
					if (!(dr["NM_BAIRRO"] is DBNull))
						header.Quarter = Convert.ToString(dr["NM_BAIRRO"]);
					if (!(dr["NM_CIDADE"] is DBNull))
						header.City = Convert.ToString(dr["NM_CIDADE"]);
					if (!(dr["CD_DDD_TEL"] is DBNull))
						header.PhoneDDDNumber = Convert.ToInt32(dr["CD_DDD_TEL"]);
					if (!(dr["NR_TELEFONE"] is DBNull))
						header.PhoneNumber = Convert.ToInt64(dr["NR_TELEFONE"]);
					if (!(dr["CD_DDD_FAX"] is DBNull))
						header.FaxDDDNumber = Convert.ToInt32(dr["CD_DDD_FAX"]);
					if (!(dr["NR_FAX"] is DBNull))
						header.FaxNumber = Convert.ToInt64(dr["NR_FAX"]);
					if (!(dr["CD_INTERNET"] is DBNull))
						header.InternetCode = Convert.ToString(dr["CD_INTERNET"]);
					if (!(dr["CD_EMAIL"] is DBNull))
						header.Email = Convert.ToString(dr["CD_EMAIL"]);
				}
				dr.Close();
			}
		}

		public Confirmation LoadConfirmationReports(int code, DateTime trading)
		{
			Confirmation confirmation = new Confirmation();
			ReportHeader header = new ReportHeader();
			ReportFooter footer = new ReportFooter();

			this.LoadCompanyAddress(ref header);

			confirmation.Header = header;
			confirmation.Footer = footer;

			List<ConfirmationItem> items = new List<ConfirmationItem>();

			string date = trading.ToString("dd-MM-yyyy");

			var cmd = dbi.CreateCommand("PCK_RELATORIOS_BV_SECURITIES.PROC_GERA_CONFIRMACAO", CommandType.StoredProcedure);
			cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = code });
			cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_PREGAO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = date });
			cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSOR", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

			var dr = cmd.ExecuteReader(CommandBehavior.SingleResult);
			int counter = 0;

			while (dr.Read())
			{
				ConfirmationItem item = new ConfirmationItem();

				if (counter == 0)
				{
					if (!(dr["CD_CLIENTE"] is DBNull))
						confirmation.ClientCode = Convert.ToInt32(dr["CD_CLIENTE"]);
					if (!(dr["NM_CLIENTE"] is DBNull))
						confirmation.ClientName = Convert.ToString(dr["NM_CLIENTE"]);
					if (!(dr["NM_ENDERECO"] is DBNull))
						confirmation.Address = Convert.ToString(dr["NM_ENDERECO"]);
					if (!(dr["NR_TELEFONE"] is DBNull))
						confirmation.PhoneNumber = Convert.ToString(dr["NR_TELEFONE"]);
					if (!(dr["NR_FAX"] is DBNull))
						confirmation.FaxNumber = Convert.ToString(dr["NR_FAX"]);
					if (!(dr["NM_EMAIL"] is DBNull))
						confirmation.Email = Convert.ToString(dr["NM_EMAIL"]);
				}
				if (!(dr["CD_NEGOCIO"] is DBNull))
					item.Symbol = Convert.ToString(dr["CD_NEGOCIO"]);
				if (!(dr["CD_AGENTE"] is DBNull))
					item.AgentCode = Convert.ToInt32(dr["CD_CLIENTE"]);
				if (!(dr["CD_NATOPE"] is DBNull))
					item.OperationType = Convert.ToString(dr["CD_NATOPE"]);
				if (!(dr["NR_NEGOCIO"] is DBNull))
					item.TradeId = Convert.ToInt32(dr["NR_NEGOCIO"]);
				if (!(dr["QT_QTDESP"] is DBNull))
					item.Quantity = Convert.ToDecimal(dr["QT_QTDESP"]);
				if (!(dr["CD_ISIN"] is DBNull))
					item.ISINCode = Convert.ToString(dr["CD_ISIN"]);
				if (!(dr["NM_EMPRESA"] is DBNull))
					item.CompanyName = Convert.ToString(dr["NM_EMPRESA"]);
				if (!(dr["NM_MOEDA"] is DBNull))
					item.CurrencyName = Convert.ToString(dr["NM_MOEDA"]);
				if (!(dr["VL_NEGOCIO"] is DBNull))
					item.TradeValue = Convert.ToDecimal(dr["VL_NEGOCIO"]);
				if (!(dr["DT_NEGOCIO"] is DBNull))
					item.TradeDate = Convert.ToDateTime(dr["DT_NEGOCIO"], cultureInfo);
				if (!(dr["DT_LIQUIDACAO"] is DBNull))
					item.SettlementDate = Convert.ToDateTime(dr["DT_LIQUIDACAO"], cultureInfo);
				if (!(dr["VL_TOTNEG"] is DBNull))
					item.PrincipalValue = Convert.ToDecimal(dr["VL_TOTNEG"]);
				if (!(dr["VL_CORRET"] is DBNull))
					item.CommissionValue = Convert.ToDecimal(dr["VL_CORRET"]);
				if (!(dr["VL_TAXAS"] is DBNull))
					item.TransactionFeeValue = Convert.ToDecimal(dr["VL_TAXAS"]);
				if (!(dr["VL_LIQUIDO"] is DBNull))
					item.NetAmmountValue = Convert.ToDecimal(dr["VL_LIQUIDO"]);
				if (!(dr["CD_CUSTODIANTE"] is DBNull))
					item.CustodyCode = Convert.ToInt32(dr["CD_CUSTODIANTE"]);

				items.Add(item);
			}

			dr.Close();

			confirmation.Items = items;

			return confirmation;
		}

		public List<Customer> LoadClients(int code, string consultType, DateTime tradingDate, int? userId)
		{
			List<Customer> list = new List<Customer>();

			var cmd = dbi.CreateCommand("PCK_RELATORIOS_BV_SECURITIES.PROC_GET_CLIENTS", CommandType.StoredProcedure);
			cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = code });
			cmd.Parameters.Add(new OracleParameter { ParameterName = "pTYPE_SEARCH", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = consultType });
			cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_PREGAO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = tradingDate.ToString("dd/MM/yyyy") });
			cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSOR", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

			if (userId.HasValue)
				cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = userId.Value });
			else
				cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
			
			var dr = cmd.ExecuteReader();

			while (dr.Read())
			{
				Customer custumer = new Customer();

				if (!(dr["CD_CLIENTE"] is DBNull))
					custumer.Code = Convert.ToInt32(dr["CD_CLIENTE"]);
				if (!(dr["NM_CLIENTE"] is DBNull))
					custumer.Name = Convert.ToString(dr["NM_CLIENTE"]);

				list.Add(custumer);
			}

			dr.Close();

			return list;
		}

		public TradeBlotter LoadTradeBlotterReports(DateTime tradingDate, int? userId)
		{
			TradeBlotter tradeBlotter = new TradeBlotter();
			ReportHeader header = new ReportHeader();
			List<TradeBlotterItem> itens = new List<TradeBlotterItem>();

			this.LoadCompanyAddress(ref header);

			var cmd = dbi.CreateCommand("PCK_RELATORIOS_BV_SECURITIES.PROC_GERA_TRADE_BLOTTER", CommandType.StoredProcedure);
			cmd.Parameters.Add(new OracleParameter { ParameterName = "pDATA_NEGOCIO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = tradingDate.ToString("dd-MM-yyyy") });
			cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSOR", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

			if (userId.HasValue)
				cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = userId.Value });
			else
				cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });

			using (var dr = cmd.ExecuteReader())
			{
				while (dr.Read())
				{
					TradeBlotterItem item = new TradeBlotterItem();

					if (!(dr["NR_NEGOCIO"] is DBNull))
						item.TradeId = Convert.ToInt64(dr["NR_NEGOCIO"]);
					if (!(dr["DT_NEGOCIO"] is DBNull))
						item.TradeDate = Convert.ToDateTime(dr["DT_NEGOCIO"], cultureInfo);
					if (!(dr["DT_LIQUIDACAO"] is DBNull))
						item.SettlementDate = Convert.ToDateTime(dr["DT_LIQUIDACAO"], cultureInfo);
					if (!(dr["CD_ISIN"] is DBNull))
						item.ISINCode = Convert.ToString(dr["CD_ISIN"]);
					if (!(dr["NM_EMPRESA"] is DBNull))
						item.CompanyName = Convert.ToString(dr["NM_EMPRESA"]);
					if (!(dr["QT_NEGOCIADA"] is DBNull))
						item.NumberOfShares = Convert.ToInt64(dr["QT_NEGOCIADA"]);
					if (!(dr["VL_NEGOCIO"] is DBNull))
						item.SharePrice = Convert.ToDecimal(dr["VL_NEGOCIO"]);
					if (!(dr["VL_VOLUME_COMPRA"] is DBNull))
						item.CustomerBuyAmount = Convert.ToDecimal(dr["VL_VOLUME_COMPRA"]);
					if (!(dr["VL_LIQUIDO_COMPRA"] is DBNull))
						item.NetBuyAmount = Convert.ToDecimal(dr["VL_LIQUIDO_COMPRA"]);
					if (!(dr["VL_VOLUME_VENDA"] is DBNull))
						item.CustomerSellAmount = Convert.ToDecimal(dr["VL_VOLUME_VENDA"]);
					if (!(dr["VL_LIQUIDO_VENDA"] is DBNull))
						item.NetSellAmount = Convert.ToDecimal(dr["VL_LIQUIDO_VENDA"]);
					if (!(dr["NM_MOEDA"] is DBNull))
						item.CurrencyName = Convert.ToString(dr["NM_MOEDA"]);
					if (!(dr["NM_CLIENTE"] is DBNull))
						item.ClientName = Convert.ToString(dr["NM_CLIENTE"]);

					itens.Add(item);
				}

				dr.Close();
			}
			tradeBlotter.Header = header;
			tradeBlotter.Items = itens;

			return tradeBlotter;
		}

		public ReportHeader LoadHeader()
		{
			ReportHeader header = new ReportHeader();
			this.LoadCompanyAddress(ref header);
			return header;
		}

		public CustomerStatement LoadCustomerStatementReports(int code, string date, int? userId)
		{
			CustomerStatement customerStatement = new CustomerStatement();
			ReportHeader header = new ReportHeader();
			ReportFooter footer = new ReportFooter();
			List<CustomerStatementItem> list = new List<CustomerStatementItem>();

			this.LoadCompanyAddress(ref header);

			using (var cmd = dbi.CreateCommand("PCK_RELATORIOS_BV_SECURITIES.PROC_CUSTOMER_STATEMENT", CommandType.StoredProcedure))
			{
				cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = code });
				cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_MES_ANO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = date });
				cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSOR", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });
				cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_INICIO", OracleType = OracleType.DateTime, Direction = ParameterDirection.Output });
				cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_FIM", OracleType = OracleType.DateTime, Direction = ParameterDirection.Output });
				
				if (userId.HasValue)
					cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = userId.Value });
				else
					cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
				
				var dr = cmd.ExecuteReader();

				int counter = 0;

				while (dr.Read())
				{
					CustomerStatementItem customerStatementItem = new CustomerStatementItem();

					if (counter == 0)
					{
						if (!(cmd.Parameters["pDT_INICIO"].Value is DBNull))
							customerStatement.BeginDate = Convert.ToDateTime(cmd.Parameters["pDT_INICIO"].Value);
						if (!(cmd.Parameters["pDT_FIM"].Value is DBNull))
							customerStatement.EndDate = Convert.ToDateTime(cmd.Parameters["pDT_FIM"].Value);
						if (!(dr["CD_CLIENTE"] is DBNull))
							customerStatement.ClientCode = Convert.ToInt32(dr["CD_CLIENTE"]);
						if (!(dr["NM_CLIENTE"] is DBNull))
							customerStatement.ClientName = Convert.ToString(dr["NM_CLIENTE"]);
						if (!(dr["NM_ENDERECO"] is DBNull))
							customerStatement.Address = Convert.ToString(dr["NM_ENDERECO"]);
						if (!(dr["NR_TELEFONE"] is DBNull))
							customerStatement.PhoneNumber = Convert.ToString(dr["NR_TELEFONE"]);
						if (!(dr["NR_FAX"] is DBNull))
							customerStatement.FaxNumber = Convert.ToString(dr["NR_FAX"]);
						if (!(dr["NM_EMAIL"] is DBNull))
							customerStatement.Email = Convert.ToString(dr["NM_EMAIL"]);

						counter = 1;
					}

					if (!(dr["NM_EMPRESA"] is DBNull))
						customerStatementItem.CompanyName = Convert.ToString(dr["NM_EMPRESA"]);
					if (!(dr["DT_NEGOCIO"] is DBNull))
						customerStatementItem.TradeDate = Convert.ToDateTime(dr["DT_NEGOCIO"], cultureInfo);
					if (!(dr["CD_NATOPE"] is DBNull))
						customerStatementItem.OperationCode = Convert.ToString(dr["CD_NATOPE"]);
					if (!(dr["VL_PRECO_MEDIO"] is DBNull))
						customerStatementItem.SharePrice = Convert.ToDecimal(dr["VL_PRECO_MEDIO"]);
					if (!(dr["NM_MOEDA"] is DBNull))
						customerStatementItem.CurrencyName = Convert.ToString(dr["NM_MOEDA"]);
					if (!(dr["QT_QTDESP"] is DBNull))
						customerStatementItem.Shares = Convert.ToInt32(dr["QT_QTDESP"]);
					if (!(dr["VL_LIQUIDO"] is DBNull))
						customerStatementItem.NetValue = Convert.ToDecimal(dr["VL_LIQUIDO"]);
					if (!(dr["DT_LIQUIDACAO"] is DBNull))
						customerStatementItem.SettlementDate = Convert.ToDateTime(dr["DT_LIQUIDACAO"], cultureInfo);
					if (!(dr["VL_LIQUIDO_EFETIVO"] is DBNull))
						customerStatementItem.SettleValue = Convert.ToDecimal(dr["VL_LIQUIDO_EFETIVO"]);
					if (!(dr["QT_LIQUIDO_FALHA"] is DBNull))
						customerStatementItem.OutstandingShares = Convert.ToInt32(dr["QT_LIQUIDO_FALHA"]);
					if (!(dr["VL_LIQUIDO_FALHA"] is DBNull))
						customerStatementItem.OutstandingValues = Convert.ToDecimal(dr["VL_LIQUIDO_FALHA"]);

					list.Add(customerStatementItem);
				}

				dr.Close();
			}

			customerStatement.Header = header;
			customerStatement.Items = list;
			customerStatement.Footer = footer;

			return customerStatement;
		}

		public CustomerLedger LoadCustomerLedgerReports(int code, string date)
		{
			CustomerLedger customerLedger = new CustomerLedger();
			ReportHeader header = new ReportHeader();
			ReportFooter footer = new ReportFooter();
			List<CustomerLedgerItem> list = new List<CustomerLedgerItem>();

			this.LoadCompanyAddress(ref header);

			using (var cmd = dbi.CreateCommand("PCK_RELATORIOS_BV_SECURITIES.PROC_CUSTOMER_LEDGER", CommandType.StoredProcedure))
			{
				cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_MES_ANO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = date });
				cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = code });
				cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSOR", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });
				cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_INICIO", OracleType = OracleType.DateTime, Direction = ParameterDirection.Output });
				cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_FIM", OracleType = OracleType.DateTime, Direction = ParameterDirection.Output });
				var dr = cmd.ExecuteReader();

				int counter = 0;

				while (dr.Read())
				{
					CustomerLedgerItem customerLedgerItem = new CustomerLedgerItem();

					if (counter == 0)
					{
						if (!(cmd.Parameters["pDT_INICIO"].Value is DBNull))
							customerLedger.StartDate = Convert.ToDateTime(cmd.Parameters["pDT_INICIO"].Value);
						if (!(cmd.Parameters["pDT_FIM"].Value is DBNull))
							customerLedger.EndDate = Convert.ToDateTime(cmd.Parameters["pDT_FIM"].Value);
						if (!(dr["CD_CLIENTE"] is DBNull))
							customerLedger.ClientCode = Convert.ToInt32(dr["CD_CLIENTE"]);
						if (!(dr["NM_CLIENTE"] is DBNull))
							customerLedger.ClientName = Convert.ToString(dr["NM_CLIENTE"]);
						counter = 1;
					}

					if (!(dr["NR_NEGOCIO"] is DBNull))
						customerLedgerItem.ConfirmID = Convert.ToInt32(dr["NR_NEGOCIO"]);
					if (!(dr["NM_EMPRESA"] is DBNull))
						customerLedgerItem.Security = Convert.ToString(dr["NM_EMPRESA"]);
					if (!(dr["DT_NEGOCIO"] is DBNull))
						customerLedgerItem.TradeDate = Convert.ToDateTime(dr["DT_NEGOCIO"], cultureInfo);
					if (!(dr["DT_LIQUIDACAO"] is DBNull))
						customerLedgerItem.SettleDate = Convert.ToDateTime(dr["DT_LIQUIDACAO"], cultureInfo);
					if (!(dr["CD_NATOPE"] is DBNull))
						customerLedgerItem.OperationCode = Convert.ToString(dr["CD_NATOPE"]);
					if (!(dr["VL_NEGOCIO"] is DBNull))
						customerLedgerItem.SharePrice = Convert.ToDecimal(dr["VL_NEGOCIO"]);
					if (!(dr["QT_QTDESP"] is DBNull))
						customerLedgerItem.Shares = Convert.ToInt32(dr["QT_QTDESP"]);
					if (!(dr["VL_LIQUIDADO"] is DBNull))
						customerLedgerItem.NetValue = Convert.ToDecimal(dr["VL_LIQUIDADO"]);
					if (!(dr["NM_MOEDA"] is DBNull))
						customerLedgerItem.CurrencyName = Convert.ToString(dr["NM_MOEDA"]);
					if (!(dr["QT_FALHA_EFETIVA"] is DBNull))
						customerLedgerItem.SettledShares = Convert.ToInt32(dr["QT_FALHA_EFETIVA"]);
					if (!(dr["VL_FALHA_EFETIVA"] is DBNull))
						customerLedgerItem.SettledValue = Convert.ToDecimal(dr["VL_FALHA_EFETIVA"]);
					if (!(dr["QT_FALHA"] is DBNull))
						customerLedgerItem.OutstandingShares = Convert.ToInt32(dr["QT_FALHA"]);
					if (!(dr["VL_FALHA"] is DBNull))
						customerLedgerItem.OutstandingValues = Convert.ToDecimal(dr["VL_FALHA"]);

					list.Add(customerLedgerItem);
				}

				dr.Close();
			}

			customerLedger.Header = header;
			customerLedger.Items = list;

			return customerLedger;
		}

		public StockRecord LoadStockRecordReports()
		{
			StockRecord stockRecord = new StockRecord();
			ReportHeader header = new ReportHeader();
			ReportFooter footer = new ReportFooter();

			this.LoadCompanyAddress(ref header);
            
			stockRecord.Header = header;
			stockRecord.Footer = footer;

			return stockRecord;
		}

		public List<PreStockRecordReport> LoadPreStockRecordReports(string date, int? userId)
		{
			List<PreStockRecordReport> list = new List<PreStockRecordReport>();

			using (var cmd = dbi.CreateCommand("PCK_RELATORIOS_BV_SECURITIES.PROC_STOCK_RECORD", CommandType.StoredProcedure))
			{
				cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_MES_ANO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = date });
				cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSOR", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });
				cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_INICIO", OracleType = OracleType.DateTime, Direction = ParameterDirection.Output });
				cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_FIM", OracleType = OracleType.DateTime, Direction = ParameterDirection.Output });

				if (userId.HasValue)
					cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = userId.Value });
				else
					cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });

				var dr = cmd.ExecuteReader();

				int count = 0;

				while (dr.Read())
				{
					PreStockRecordReport preStockRecordReport = new PreStockRecordReport();

					if (count == 0)
					{
						if (!(cmd.Parameters["pDT_INICIO"].Value is DBNull))
							preStockRecordReport.Start = Convert.ToDateTime(cmd.Parameters["pDT_INICIO"].Value);
						if (!(cmd.Parameters["pDT_FIM"].Value is DBNull))
							preStockRecordReport.End = Convert.ToDateTime(cmd.Parameters["pDT_FIM"].Value);

						count++;
					}

					if (!(dr["NR_NEGOCIO"] is DBNull))
						preStockRecordReport.ConfirmID = Convert.ToInt32(dr["NR_NEGOCIO"]);
					if (!(dr["NM_CLIENTE"] is DBNull))
						preStockRecordReport.ClientBroker = Convert.ToString(dr["NM_CLIENTE"]);
					if (!(dr["DT_LIQUIDACAO"] is DBNull))
                        preStockRecordReport.DateSettled = Convert.ToDateTime(dr["DT_LIQUIDACAO"], cultureInfo);
					if (!(dr["QT_NEGOCIO_COMPRA"] is DBNull))
						preStockRecordReport.Long = Convert.ToInt64(dr["QT_NEGOCIO_COMPRA"]);
					if (!(dr["QT_NEGOCIO_VENDA"] is DBNull))
						preStockRecordReport.Short = Convert.ToInt64(dr["QT_NEGOCIO_VENDA"]);
					if (!(dr["NM_EMPRESA"] is DBNull))
						preStockRecordReport.CompanyName = Convert.ToString(dr["NM_EMPRESA"]);

					list.Add(preStockRecordReport);
				}

				dr.Close();
			}

			return list;
		}

		public FailLedger LoadFailLedgerReports(int code, DateTime tradingDate, FailLedgerType type, int? userId)
		{
			FailLedger failLedger = new FailLedger();
			ReportHeader header = new ReportHeader();
			ReportFooter footer = new ReportFooter();
			List<FailLedgerItem> list = new List<FailLedgerItem>();

			this.LoadCompanyAddress(ref header);

			string proc = string.Empty;
			switch (type)
			{
				case FailLedgerType.FailToDeliver:
					proc = "PCK_RELATORIOS_BV_SECURITIES.PROC_FAIL_DELIVER";
					break;
				case FailLedgerType.FailToReceive:
					proc = "PCK_RELATORIOS_BV_SECURITIES.PROC_FAIL_RECIEVE";
					break;
				case FailLedgerType.APCustomer:
					proc = "PCK_RELATORIOS_BV_SECURITIES.PROC_AP_CUSTOMER";
					break;
				case FailLedgerType.ARCustomer:
					proc = "PCK_RELATORIOS_BV_SECURITIES.PROC_AR_CUSTOMER";
					break;
			}

			using (var cmd = dbi.CreateCommand(proc, CommandType.StoredProcedure))
			{
				if (code < 0)
					cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
				else
					cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = code });
				cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_NEGOCIO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = tradingDate.ToString("dd/MM/yyyy") });
				cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSOR", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

				if (userId.HasValue)
					cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = userId.Value });
				else
					cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });

				var dr = cmd.ExecuteReader();

				while (dr.Read())
				{
					FailLedgerItem item = new FailLedgerItem();
					if (!(dr["CD_CLIENTE"] is DBNull))
						item.ClientCode = Convert.ToInt32(dr["CD_CLIENTE"]);
					if (!(dr["NM_CLIENTE"] is DBNull))
						item.ClientName = Convert.ToString(dr["NM_CLIENTE"]);
					if (!(dr["NR_NEGOCIO"] is DBNull))
						item.ConfirmID = Convert.ToInt32(dr["NR_NEGOCIO"]);
					if (!(dr["NM_EMPRESA"] is DBNull))
						item.Security = Convert.ToString(dr["NM_EMPRESA"]);
					if (!(dr["CD_NATOPE"] is DBNull))
						item.OperationCode = Convert.ToString(dr["CD_NATOPE"]);
					if (!(dr["DT_NEGOCIO"] is DBNull))
						item.TradeDate = Convert.ToDateTime(dr["DT_NEGOCIO"], cultureInfo);
					if (!(dr["DT_LIQUIDACAO"] is DBNull))
						item.SettleDate = Convert.ToDateTime(dr["DT_LIQUIDACAO"], cultureInfo);
					if (!(dr["QT_FALHA"] is DBNull))
						item.Shares = Convert.ToInt32(dr["QT_FALHA"]);
					if (!(dr["VL_FALHA"] is DBNull))
						item.NetAmount = Convert.ToDecimal(dr["VL_FALHA"]);
					if (!(dr["PR_MERCADO"] is DBNull))
						item.MarketValuePerShare = Convert.ToDecimal(dr["PR_MERCADO"]);
					if (!(dr["VL_MERCADO"] is DBNull))
						item.MarketValueTotal = Convert.ToDecimal(dr["VL_MERCADO"]);

					list.Add(item);
				}
				dr.Close();
			}

			failLedger.Header = header;
			failLedger.Items = list;
			failLedger.Type = type;

			return failLedger;
		}
	}
}
