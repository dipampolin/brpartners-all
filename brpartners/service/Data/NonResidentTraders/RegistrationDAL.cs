﻿using System.Collections.Generic;
using System.Data.OracleClient;
using System.Data;
using System;
using QX3.Portal.Services.Properties;
using QX3.Portal.Contracts.DataContracts;
using QX3.Spinnex.Common.Services.Data;


namespace QX3.Portal.Services.Data
{
	public class RegistrationDAL : DataProviderBase
	{
		public static IFormatProvider cultureInfo = new System.Globalization.CultureInfo("en-US", true);

		public RegistrationDAL() : base("PortalConnectionString") { }

		public string GetCustomerName(int code)
		{
			var cmd = dbi.CreateCommand("PCK_BV_SECURITIES.PROC_BUSCA_NOME", CommandType.StoredProcedure);
			cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = code });
			cmd.Parameters.Add(new OracleParameter { ParameterName = "pNM_CLIENTE", OracleType = OracleType.VarChar, Direction = ParameterDirection.Output, Size = 60 });
			cmd.ExecuteNonQuery();

			return cmd.Parameters["pNM_CLIENTE"].Value.ToString();
		}

        public string GetCustomerName(int code, string assessors, int? userId)
        {
            var cmd = dbi.CreateCommand("PCK_BV_SECURITIES.PROC_BUSCA_NOME", CommandType.StoredProcedure);
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = code });

            if (String.IsNullOrEmpty(assessors))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTASSESSOR", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTASSESSOR", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = assessors });

            if (userId.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = userId.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pNM_CLIENTE", OracleType = OracleType.VarChar, Direction = ParameterDirection.Output, Size = 60 });
            cmd.ExecuteNonQuery();

            return cmd.Parameters["pNM_CLIENTE"].Value.ToString();
        }

		public List<NonResidentCustomer> LoadCustomers(int code, int? userId)
		{
			List<NonResidentCustomer> customers = new List<NonResidentCustomer>();

			var cmd = dbi.CreateCommand("PCK_BV_SECURITIES.PROC_CARREGA_CLIENTES", CommandType.StoredProcedure);
			cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = code });
			cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSOR", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

			if (userId.HasValue)
				cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = userId.Value });
			else
				cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });

			var dr = cmd.ExecuteReader();

			while (dr.Read())
			{
				NonResidentCustomer customer = new NonResidentCustomer();
				customer.Code = dr.GetInt32(0);
				if (!dr.IsDBNull(1))
					customer.BrokerCode = dr.GetInt32(1);
				customer.Name = dr.GetDBString(2);
				customer.Address = dr.GetDBString(3);
				customer.Email = dr.GetDBString(4);
				if (!dr.IsDBNull(5))
					customer.PhoneDDDNumber = dr.GetInt32(5);
				if (!dr.IsDBNull(6))
					customer.PhoneNumber = dr.GetInt64(6);
				if (!dr.IsDBNull(7))
					customer.FaxDDDNumber = dr.GetInt32(7);
				if (!dr.IsDBNull(8))
					customer.FaxNumber = dr.GetInt64(8);
				if (!dr.IsDBNull(9))
					customer.TransferBrokerage = dr.GetDecimal(9);
                if (!(dr["POSSUI_CADASTRO"] is DBNull))
                    customer.HasRegistration = Convert.ToBoolean(dr["POSSUI_CADASTRO"]);

				customers.Add(customer);
			}
			dr.Close();

			return customers;
		}

		public bool InsertCustomer(NonResidentCustomer customer)
		{
			var cmd = dbi.CreateCommand("PCK_BV_SECURITIES.PROC_COMPLEMENTO_CLIENTE", CommandType.StoredProcedure);

			cmd.Parameters.Add(new OracleParameter { ParameterName = "pTP_OPERACAO", OracleType = OracleType.Char, Value = "I" });
			cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Value = customer.Code });
			cmd.Parameters.Add(new OracleParameter { ParameterName = "pNM_CLIENTE", OracleType = OracleType.VarChar, Value = customer.Name });
			cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE_SECURITIES", OracleType = OracleType.Number, Value = customer.BrokerCode });
			cmd.Parameters.Add(new OracleParameter { ParameterName = "pNM_ENDERECO", OracleType = OracleType.VarChar, Value = customer.Address ?? "" });
			cmd.Parameters.Add(new OracleParameter { ParameterName = "pNM_EMAIL", OracleType = OracleType.VarChar, Value = customer.Email ?? "" });
			cmd.Parameters.Add(new OracleParameter { ParameterName = "pNR_DDD_TELEFONE", OracleType = OracleType.Number, Value = customer.PhoneDDDNumber });
			cmd.Parameters.Add(new OracleParameter { ParameterName = "pNR_TELEFONE", OracleType = OracleType.Number, Value = customer.PhoneNumber });
			cmd.Parameters.Add(new OracleParameter { ParameterName = "pNR_DDD_FAX", OracleType = OracleType.Number, Value = customer.FaxDDDNumber });
			cmd.Parameters.Add(new OracleParameter { ParameterName = "pNR_FAX", OracleType = OracleType.Number, Value = customer.FaxNumber });
			cmd.Parameters.Add(new OracleParameter { ParameterName = "pPC_REPASSE", OracleType = OracleType.Number, Value = customer.TransferBrokerage });

			return cmd.ExecuteNonQuery() > 0;
		}

		public SettlementDate LoadLastSettlement()
		{
			SettlementDate settlementDate = new SettlementDate();

			using (var cmd = dbi.CreateCommand(QueryResource.LastSettlementDate, CommandType.Text))
			{
				var dr = cmd.ExecuteReader();

				while (dr.Read())
				{
					if (!(dr["DT_ULTIMA_LIQUIDACAO"] is DBNull))
						settlementDate.LastSettlementDate = Convert.ToDateTime(dr["DT_ULTIMA_LIQUIDACAO"], cultureInfo);
				}

				dr.Close();
			}

			return settlementDate;
		}

		public List<PTax> LoadPTaxes(string date)
		{
			List<PTax> ptaxes = new List<PTax>();

			var cmd = dbi.CreateCommand("PCK_BV_SECURITIES.PROC_CARREGA_PTAX", CommandType.StoredProcedure);
			cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_REFERENCIA", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = date });
			cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSOR", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

			var dr = cmd.ExecuteReader();

			while (dr.Read())
			{
				PTax ptax = new PTax();
				if (!(dr["Dt_referencia"] is DBNull))
					ptax.Date = Convert.ToDateTime(dr["Dt_referencia"]);
				if (!(dr["vl_ptax"] is DBNull))
					ptax.Value = Convert.ToDecimal(dr["vl_ptax"]);
				ptaxes.Add(ptax);
			}
			dr.Close();

			return ptaxes;
		}

		public bool InsertPTax(PTax ptax, decimal transferPercentage)
		{
			var cmd = dbi.CreateCommand("PCK_BV_SECURITIES.PROC_GRAVA_PTAX", CommandType.StoredProcedure);

			cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_REFERENCIA", OracleType = OracleType.VarChar, Value = ptax.Date.ToString("dd/MM/yyyy") });
			cmd.Parameters.Add(new OracleParameter { ParameterName = "pVL_PTAX", OracleType = OracleType.Number, Value = ptax.Value });
			cmd.Parameters.Add(new OracleParameter { ParameterName = "pPC_REPASSE", OracleType = OracleType.Number, Value = transferPercentage });

			return cmd.ExecuteNonQuery() > 0;
		}

		public List<Failure> LoadFailures(int code, string date, int? userId)
		{
			List<Failure> failures = new List<Failure>();

			var cmd = dbi.CreateCommand("PCK_BV_SECURITIES.PROC_CONSULTA_FALHA", CommandType.StoredProcedure);
			cmd.Parameters.Add(new OracleParameter { ParameterName = "pdt_negocio", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = date });
			if (code < 0)
				cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
			else
				cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = code });
			cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSOR", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

			if (userId.HasValue)
				cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = userId.Value });
			else
				cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });

			var dr = cmd.ExecuteReader();

			while (dr.Read())
			{
				Failure failure = new Failure();

				if (!(dr["Cd_cliente"] is DBNull))
					failure.ClientCode = Convert.ToInt32(dr["Cd_cliente"]);
				if (!(dr["Nr_negocio"] is DBNull))
					failure.TradeNumber = Convert.ToInt32(dr["Nr_negocio"]);
				if (!(dr["NR_NEGOCIO"] is DBNull))
					failure.TradeCode = Convert.ToString(dr["NR_NEGOCIO"]);
				if (!(dr["Cd_isin"] is DBNull))
					failure.ISIN = Convert.ToString(dr["Cd_isin"]);
				if (!(dr["Cd_negocio"] is DBNull))
					failure.TradeCode = Convert.ToString(dr["Cd_negocio"]);
				if (!(dr["DT_NEGOCIO"] is DBNull))
					failure.TradeDate = Convert.ToDateTime(dr["DT_NEGOCIO"], cultureInfo);
				if (!(dr["Dt_liquidacao"] is DBNull))
					failure.SettlementDate = Convert.ToDateTime(dr["Dt_liquidacao"], cultureInfo);
				if (!(dr["Qt_qtdesp"] is DBNull))
					failure.Quantity = Convert.ToInt32(dr["Qt_qtdesp"]);
				if (!(dr["Vl_totneg"] is DBNull))
					failure.OperationValue = Convert.ToDecimal(dr["Vl_totneg"]);
				if (!(dr["Vl_liquido"] is DBNull))
					failure.TotalValue = Convert.ToDecimal(dr["Vl_liquido"]);
				if (!(dr["Qt_falha"] is DBNull))
					failure.FailureQuantity = Convert.ToInt32(dr["Qt_falha"]);
				if (!(dr["Vl_falha"] is DBNull))
					failure.FailureValue = Convert.ToDecimal(dr["Vl_falha"]);
				if (!(dr["Dt_atualizacao"] is DBNull))
					failure.ChangeDate = Convert.ToDateTime(dr["Dt_atualizacao"], cultureInfo);
				if (!(dr["Nm_usuario"] is DBNull))
					failure.Username = Convert.ToString(dr["Nm_usuario"]);
				if (!(dr["Cd_Natope"] is DBNull))
					failure.OperationType = Convert.ToChar(dr["Cd_Natope"]);

				failures.Add(failure);
			}
			dr.Close();

			return failures;
		}

		public bool InsertFailure(Failure failure)
		{
			var cmd = dbi.CreateCommand("PCK_BV_SECURITIES.PROC_GRAVA_FALHA", CommandType.StoredProcedure);

			cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Value = failure.ClientCode });
			cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_NEGOCIO", OracleType = OracleType.VarChar, Value = failure.TradeDate.ToString("dd/MM/yyyy") });
			cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_NEGOCIO", OracleType = OracleType.VarChar, Value = failure.TradeCode });
			cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_NATOPE", OracleType = OracleType.Char, Value = failure.OperationType });
			cmd.Parameters.Add(new OracleParameter { ParameterName = "pNR_NEGOCIO", OracleType = OracleType.Number, Value = failure.TradeNumber });
			cmd.Parameters.Add(new OracleParameter { ParameterName = "pQT_FALHA", OracleType = OracleType.Number, Value = failure.FailureQuantity });
			cmd.Parameters.Add(new OracleParameter { ParameterName = "pVL_FALHA", OracleType = OracleType.Number, Value = failure.FailureValue });
			cmd.Parameters.Add(new OracleParameter { ParameterName = "pNM_USUARIO", OracleType = OracleType.VarChar, Value = failure.Username });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_LIQUIDACAO", OracleType = OracleType.VarChar, Value = failure.SettlementDate.ToString("dd/MM/yyyy") });

			return cmd.ExecuteNonQuery() > 0;
		}

		public List<ReportType> LoadReportTypes()
		{
			List<ReportType> reportTypes = new List<ReportType>();

			using (var cmd = dbi.CreateCommand("PCK_BV_SECURITIES.PROC_COMBO_RELATORIO", CommandType.StoredProcedure))
			{
				cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSOR", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });
				var dr = cmd.ExecuteReader();

				while (dr.Read())
				{
					ReportType reportType = new ReportType();

					if (!(dr["ID_RELATORIO"] is DBNull))
						reportType.ReportId = Convert.ToInt32(dr["ID_RELATORIO"]);
					if (!(dr["DS_RELATORIO"] is DBNull))
						reportType.Description = Convert.ToString(dr["DS_RELATORIO"]);
					if (!(dr["HAS_DISCLAIMER"] is DBNull))
						reportType.HasDisclaimer = Convert.ToBoolean(dr["HAS_DISCLAIMER"]);

					reportTypes.Add(reportType);
				}

				dr.Close();
			}

			return reportTypes;
		}

		public bool InsertDisclaimer(Disclaimer disclaimer)
		{
			using (var cmd = dbi.CreateCommand("PCK_BV_SECURITIES.PROC_MANTEM_DISCLAIMER", CommandType.StoredProcedure))
			{
				cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_RELATORIO", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = disclaimer.ReportType.ReportId });
				cmd.Parameters.Add(new OracleParameter { ParameterName = "pDS_DISCLAIMER", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = disclaimer.Content });

				return cmd.ExecuteNonQuery() > 0;
			}
		}

		public Disclaimer LoadDisclaimer(int reportId)
		{
			Disclaimer disclaimer = new Disclaimer();

			using (var cmd = dbi.CreateCommand("PCK_BV_SECURITIES.PROC_CARREGA_DISCLAIMER", CommandType.StoredProcedure))
			{
				cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_RELATORIO", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = reportId });
				cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSOR", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

				var dr = cmd.ExecuteReader();

				while (dr.Read())
				{
					ReportType reportType = new ReportType();
					if (!(dr["ID_RELATORIO"] is DBNull))
						reportType.ReportId = Convert.ToInt32(dr["ID_RELATORIO"]);
					if (!(dr["DS_RELATORIO"] is DBNull))
						reportType.Description = Convert.ToString(dr["DS_RELATORIO"]);
					disclaimer.ReportType = reportType;
					if (!(dr["DS_DISCLAIMER"] is DBNull))
						disclaimer.Content = Convert.ToString(dr["DS_DISCLAIMER"]);	
				}
			}

			return disclaimer;
		}

        public List<Broker> LoadBrokers(string filter) 
        {
            List<Broker> list = null;
            var cmd = dbi.CreateCommand("PCK_PORTALWEB.PROC_RETORNA_LISTA_CORRETORAS", CommandType.StoredProcedure);
            
            if (!String.IsNullOrEmpty(filter))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pNmCorretora", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pNmCorretora", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSOR", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader()) 
            {
                if (dr.HasRows) 
                {
                    list = new List<Broker>();
                    while (dr.Read()) 
                    {
                        Broker b = new Broker();
                        if (!dr.IsDBNull(0)) b.BrokerCode = dr.GetInt32(0);
                        if (!dr.IsDBNull(1)) b.BrokerName = dr.GetString(1);

                        list.Add(b);
                    }
                }
            }
            return list;            
        }

        public List<Broker> LoadBrokersTransfer(string filter)
        {
            List<Broker> list = null;
            var cmd = dbi.CreateCommand("PCK_PORTALWEB.PROC_RETORNA_CORRETORA_REPASSE", CommandType.StoredProcedure);

            if (!String.IsNullOrEmpty(filter))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pNmCorretora", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pNmCorretora", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSOR", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    list = new List<Broker>();
                    while (dr.Read())
                    {
                        Broker b = new Broker();
                        if (!dr.IsDBNull(0)) b.BrokerCode = dr.GetInt32(0);
                        if (!dr.IsDBNull(1)) b.BrokerName = dr.GetString(1);

                        list.Add(b);
                    }
                }
            }
            return list;
        }


        public string LoadClientEmail(int clientCode) 
        {
            var cmd = dbi.CreateCommand("PCK_PORTALWEB.FUNC_RETORNA_EMAIL", CommandType.StoredProcedure);

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pClientCode", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = clientCode });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "nResult", OracleType = OracleType.VarChar, Size = 150, Direction = ParameterDirection.ReturnValue });

            cmd.ExecuteNonQuery();

            return cmd.Parameters["nResult"].Value.ToString();
        }

        public List<CommonType> LoadPortfolios(string filter)
        {
            List<CommonType> list = null;
            var cmd = dbi.CreateCommand("PCK_PORTALWEB.PROC_RETORNA_LISTA_CARTEIRAS", CommandType.StoredProcedure);

            if (!String.IsNullOrEmpty(filter))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCdCarteira", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCdCarteira", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCursor", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    list = new List<CommonType>();
                    while (dr.Read())
                    {
                        list.Add(new CommonType { Value = dr[0].ToString(), Description = dr[1].ToString() });
                    }
                }
            }
            return list;
        }
	}
}
