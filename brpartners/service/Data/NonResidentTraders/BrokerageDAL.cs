﻿using System.Collections.Generic;
using QX3.Portal.Contracts.DataContracts;
using System.Data;
using QX3.Spinnex.Common.Services.Data;
using System.Data.OracleClient;
using System;
using QX3.Portal.Services.Properties;
using System.Configuration;

namespace QX3.Portal.Services.Data
{
	public class BrokerageDAL : DataProviderBase
	{
		public static IFormatProvider cultureInfo = new System.Globalization.CultureInfo("en-US", true);

		public BrokerageDAL() : base("PortalConnectionString") { }

		public bool SettleBrokerageTransfer(string settlementDate, string settlementUsername)
		{
			var cmd = dbi.CreateCommand("PCK_BV_SECURITIES.PROC_REALIZA_LIQUIDACAO", CommandType.StoredProcedure);

			cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_LIQUIDACAO", OracleType = OracleType.VarChar, Value = settlementDate });
			cmd.Parameters.Add(new OracleParameter { ParameterName = "pNM_USUARIO", OracleType = OracleType.VarChar, Value = settlementUsername });

			return cmd.ExecuteNonQuery() > 0;
		}

		public List<BrokerageTransfer> LoadPendingBrokerageTransfers(int code, string date, int? userId)
		{
			List<BrokerageTransfer> pendingTransfers = new List<BrokerageTransfer>();

			var cmd = dbi.CreateCommand("PCK_BV_SECURITIES.PROC_CONSULTA_REPASSE_PENDENTE", CommandType.StoredProcedure);
			if (code < 0)
				cmd.Parameters.Add(new OracleParameter { ParameterName = "pcd_cliente", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
			else
				cmd.Parameters.Add(new OracleParameter { ParameterName = "pcd_cliente", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = code });
			cmd.Parameters.Add(new OracleParameter { ParameterName = "pdt_liquidacao", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = date });
			cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSOR", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

			if (userId.HasValue)
				cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = userId.Value });
			else
				cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });

			var dr = cmd.ExecuteReader();

			while (dr.Read())
			{
				BrokerageTransfer transfer = new BrokerageTransfer();
				if (!(dr["Cd_cliente_securities"] is DBNull))
					transfer.ClientBrokerCode = Convert.ToInt32(dr["Cd_cliente_securities"]);
				if (!(dr["Cd_cliente"] is DBNull))
					transfer.ClientCode = Convert.ToInt32(dr["Cd_cliente_securities"]);
				if (!(dr["Nm_cliente"] is DBNull))
					transfer.ClientName = Convert.ToString(dr["Nm_cliente"]);
				if (!(dr["Dt_Liquidacao"] is DBNull))
					transfer.BrokerageDate = Convert.ToDateTime(dr["Dt_Liquidacao"], cultureInfo);
				if (!(dr["Vl_cortot"] is DBNull))
					transfer.BrokerageValue = Convert.ToDecimal(dr["Vl_cortot"]);
				if (!(dr["Pc_repasse"] is DBNull))
					transfer.BrokerageValue = Convert.ToDecimal(dr["Pc_repasse"]);
				if (!(dr["Vl_repasse"] is DBNull))
					transfer.TransferValue = Convert.ToDecimal(dr["Vl_repasse"]);
				if (!(dr["Vl_convertido_ptax"] is DBNull))
					transfer.TransferValueToDolar = Convert.ToDecimal(dr["Vl_convertido_ptax"]);
				transfer.Status = BrokerageTransferStatus.Pending;

				pendingTransfers.Add(transfer);
			}
			dr.Close();

			return pendingTransfers;
		}

		public List<BrokerageTransfer> LoadSettledBrokerageTransfers(int code, string startDate, string endDate, int? userId)
		{
			List<BrokerageTransfer> pendingTransfers = new List<BrokerageTransfer>();

			var cmd = dbi.CreateCommand("PCK_BV_SECURITIES.PROC_CONSULTA_LIQUIDADO", CommandType.StoredProcedure);
			if (code < 0)
				cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
			else
				cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = code });
			cmd.Parameters.Add(new OracleParameter { ParameterName = "Pdt_liquidacao_inicio", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = startDate });
			cmd.Parameters.Add(new OracleParameter { ParameterName = "Pdt_liquidacao_fim", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = endDate });
			cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSOR", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

			if (userId.HasValue)
				cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = userId.Value });
			else
				cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });

			var dr = cmd.ExecuteReader();

			while (dr.Read())
			{
				BrokerageTransfer transfer = new BrokerageTransfer();
				if (!(dr["Cd_cliente_securities"] is DBNull))
					transfer.ClientBrokerCode = Convert.ToInt32(dr["Cd_cliente_securities"]);
				if (!(dr["Cd_cliente"] is DBNull))
					transfer.ClientCode = Convert.ToInt32(dr["Cd_cliente_securities"]);
				if (!(dr["Nm_cliente"] is DBNull))
					transfer.ClientName = Convert.ToString(dr["Nm_cliente"]);
				if (!(dr["Dt_Negocio"] is DBNull))
					transfer.BrokerageDate = Convert.ToDateTime(dr["Dt_Negocio"], cultureInfo);
				if (!(dr["Vl_cortot"] is DBNull))
					transfer.BrokerageValue = Convert.ToDecimal(dr["Vl_cortot"]);
				if (!(dr["Pc_repasse"] is DBNull))
					transfer.BrokerageValue = Convert.ToDecimal(dr["Pc_repasse"]);
				if (!(dr["Vl_repasse"] is DBNull))
					transfer.TransferValue = Convert.ToDecimal(dr["Vl_repasse"]);
				if (!(dr["Vl_convertido_ptax"] is DBNull))
					transfer.TransferValueToDolar = Convert.ToDecimal(dr["Vl_convertido_ptax"]);
				if (!(dr["Dt_liquidacao_repasse"] is DBNull))
					transfer.SettlementDate = Convert.ToDateTime(dr["Dt_liquidacao_repasse"], cultureInfo);
				if (!(dr["Nm_usuario_liquidou"] is DBNull))
                    transfer.UserName = Convert.ToString(dr["Nm_usuario_liquidou"]).Replace(ConfigurationManager.AppSettings["Portalweb.LoginPrefix"], "");
				transfer.Status = BrokerageTransferStatus.Settled;

				pendingTransfers.Add(transfer);
			}
			dr.Close();

			return pendingTransfers;
		}

		public decimal GetTransferBrokerageDefaultValue()
		{
			decimal value = 0;

			var cmd = dbi.CreateCommand(QueryResource.TransferBrokerageValue, CommandType.Text);

			using (var dr = cmd.ExecuteReader())
			{
				if (dr.Read())
				{
					if (!(dr["VlParametroSistema"] is DBNull))
						value = Convert.ToDecimal(dr["VlParametroSistema"]);
				}
				dr.Close();
			}

			return value;
		}
	}
}
