﻿using System;
using System.Collections.Generic;
using QX3.Spinnex.Common.Services.Data;
using System.Xml.Linq;
using System.Data;
using QX3.Portal.Services.Properties;
using QX3.Portal.Contracts.DataContracts;
using System.Linq;
using Newtonsoft.Json;
using System.Data.OracleClient;
using System.Data.SqlClient;

namespace QX3.Portal.Services.Data.Log
{
    public class LogDAL : DataProviderBase
    {
        public LogDAL() : base("PortalConnectionString") { }

        public List<HistoryData> LoadHistory(HistoryFilter filter)
        {

            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PROC_CARREGA_HISTORICO", CommandType.StoredProcedure);

            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pNm_Modulo", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = filter.ModuleName });
            if (string.IsNullOrEmpty(filter.StartDate))
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@pDt_Inicio", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@pDt_Inicio", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = filter.StartDate });
            if (string.IsNullOrEmpty(filter.EndDate))
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@pDt_Fim", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@pDt_Fim", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = filter.EndDate });

            cmd.Connection.Open();
            
            var dr = cmd.ExecuteReader();

            List<HistoryData> data = new List<HistoryData>();
            string xml = string.Empty;

            while (dr.Read())
            {
                if (!(dr["LOGDATA"] is DBNull))
                {
                    HistoryData itemData = JsonConvert.DeserializeObject<HistoryData>(Convert.ToString(dr["LOGDATA"]));
                    if (HistoryDataCheck(itemData, filter))
                        data.Add(itemData);
                }
            }
            dr.Close();

            cmd.Connection.Close();

            return data;
        }

        private bool HistoryDataCheck(HistoryData item, HistoryFilter filter)
        {
            if (item == null)
                return false;
            if (filter.ResponsibleID != 0 && item.ResponsibleID != filter.ResponsibleID)
                return false;
            if (!string.IsNullOrEmpty(filter.Action) && item.Action != filter.Action)
                return false;
            if (filter.TargetID != 0 && item.TargetID != filter.TargetID)
                return false;
            if (!string.IsNullOrEmpty(filter.TargetText) && item.Target != filter.TargetText)
                return false;
            if (filter.ID != 0 && item.ID != filter.ID)
                return false;
            if (!string.IsNullOrEmpty(filter.ExtraTargetText) && item.ExtraTarget != filter.ExtraTargetText)
                return false;

            return true;
        }

        public bool InsertHistory(HistoryLog log)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");
            var cmd = (SqlCommand)dbiCRK.CreateCommand("PROC_INSERE_HISTORICO", CommandType.StoredProcedure);
            cmd.Connection.Open();

            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pNm_Modulo", SqlDbType = SqlDbType.VarChar, Value = log.ModuleName });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pData", SqlDbType = SqlDbType.DateTime, Value = DateTime.Now });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@pDados", SqlDbType = SqlDbType.VarChar, Value = JsonConvert.SerializeObject(log.Data) });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@erro", SqlDbType = SqlDbType.Decimal, Direction = ParameterDirection.Output });
            
            var dr = cmd.ExecuteReader();

            cmd.Connection.Close();

            return Int32.Parse(cmd.Parameters["@erro"].Value.ToString()) == 0;
        }
    }
}
