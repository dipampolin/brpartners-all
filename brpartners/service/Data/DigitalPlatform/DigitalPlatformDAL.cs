﻿using QX3.Spinnex.Common.Services.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace QX3.Portal.Services.Data.DigitalPlatform
{
    public class DigitalPlatformDAL : DataProviderBase
    {
        public DigitalPlatformDAL() : base("PortalConnectionString") { }

        public void SetNeedToUpdateDigitalPlatform(int clientId, bool needToBeUpdatedInDigitalPlatform)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");

            var cmd = (SqlCommand)dbiCRK.CreateCommand("Update_NeedToUpdateDigitalPlatform_Status", CommandType.StoredProcedure);
            cmd.Parameters.Clear();

            cmd.Parameters.Add("@idCliente", SqlDbType.Int).Value = clientId;

            cmd.Parameters.Add("@needToUpdateDigitalPlatform", SqlDbType.Bit).Value = needToBeUpdatedInDigitalPlatform;

            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            cmd.Connection.Close();
        }

        public void SetSendDateDigitalPlatform(int clientId, DateTime suitabilityCompletionDate, DateTime sendDateDigitalPlatform)
        {
            DBInterface dbiCRK = new DBInterface("PORTAL");

            var cmd = (SqlCommand)dbiCRK.CreateCommand("Update_SendDateDigitalPlatform_Status", CommandType.StoredProcedure);
            cmd.Parameters.Clear();

            cmd.Parameters.Add("@idCliente", SqlDbType.Int).Value = clientId;
            cmd.Parameters.Add("@suitabilityCompletionDate", SqlDbType.DateTime).Value = suitabilityCompletionDate;
            cmd.Parameters.Add("@sendDateDigitalPlatform", SqlDbType.DateTime).Value = sendDateDigitalPlatform;

            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            cmd.Connection.Close();
        }
    }
}
