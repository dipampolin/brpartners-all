﻿using System;
using System.Data;
using System.Data.OracleClient;
using QX3.Portal.Contracts.DataContracts;
using QX3.Spinnex.Common.Services.Data;

namespace QX3.Portal.Services.Data
{
	public class GuaranteeDAL : DataProviderBase
	{
		public GuaranteeDAL() : base("PortalConnectionString") { }

		public GuaranteeClient ListGuarantees(GuaranteeClientFilter filter, int? userId, out int count, out int pos)
		{
			GuaranteeClient result = null; count = 0; pos = 0;

			var cmd = dbi.CreateCommand("PCK_GARANTIA_CLIENTE.PROC_LISTA_GARANTIAS", CommandType.StoredProcedure);
			cmd.Parameters.Clear();

			if (string.IsNullOrEmpty(filter.AssessorFilterText))
				cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTASSESSOR", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
			else
				cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTASSESSOR", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.AssessorFilterText });

			if (string.IsNullOrEmpty(filter.ClientFilterText))
				cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTCLIENTE", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
			else
				cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTCLIENTE", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.ClientFilterText });

			if (filter.ActivitType == 0)
				cmd.Parameters.Add(new OracleParameter { ParameterName = "pTIPO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
			else
				cmd.Parameters.Add(new OracleParameter { ParameterName = "pTIPO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.ActivitType });


			if (filter.Position.HasValue)
				cmd.Parameters.Add(new OracleParameter { ParameterName = "pPOSICAO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = filter.Position.Value });
			else
				cmd.Parameters.Add(new OracleParameter { ParameterName = "pPOSICAO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });

			if (userId.HasValue)
				cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_USUARIO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = userId.Value });
			else
				cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_USUARIO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });

			cmd.Parameters.Add(new OracleParameter { ParameterName = "pCount", OracleType = OracleType.Number, Direction = ParameterDirection.Output }); 
			cmd.Parameters.Add(new OracleParameter { ParameterName = "pCursorCliente", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });
			cmd.Parameters.Add(new OracleParameter { ParameterName = "pCursorTotais", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });
			cmd.Parameters.Add(new OracleParameter { ParameterName = "pCursorAtividades", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });
			cmd.Parameters.Add(new OracleParameter { ParameterName = "pCursorAtivos", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

			using (var dr = cmd.ExecuteReader())
			{
				if (dr.HasRows)
				{
					count = Convert.ToInt32(cmd.Parameters["pCount"].Value);

					result = new GuaranteeClient();
					result.ActivitType = filter.ActivitType;
					
					if (dr.Read())
					{
						if (dr["posicao"] != DBNull.Value) pos = Convert.ToInt16(dr["posicao"]);
						if (dr["cd_cliente"] != DBNull.Value) result.ClientCode = Convert.ToInt16(dr["cd_cliente"]);
						if (dr["nm_cliente"] != DBNull.Value) result.ClientName = dr["nm_cliente"].ToString();
						if (dr["cd_assessor"] != DBNull.Value) result.AssessorCode = Convert.ToInt16(dr["cd_assessor"]);
						if (dr["nm_assessor"] != DBNull.Value) result.AssessorName = dr["nm_assessor"].ToString();
					}

					if (dr.NextResult())
					{
						if (dr.Read())
						{
							if (dr["VL_TOTAL"] != DBNull.Value) result.TotalValue = Convert.ToDecimal(dr["VL_TOTAL"]);
							if (dr["VL_MARGEM"] != DBNull.Value) result.MarginCall = Convert.ToDecimal(dr["VL_MARGEM"]);
							if (dr["VL_SALDO"] != DBNull.Value) result.BalanceValue = Convert.ToDecimal(dr["VL_SALDO"]);
						}
					}

					if (dr.NextResult())
					{
						while (dr.Read())
						{
							GuaranteeActivit obj = new GuaranteeActivit();
							if (dr["tp_mercado"] != DBNull.Value) obj.Type = dr["tp_mercado"].ToString();
							if (dr["dt_vencimento"] != DBNull.Value) obj.DueDate = Convert.ToDateTime(dr["dt_vencimento"]);
							if (dr["vl_total"] != DBNull.Value) obj.TotalValue = Convert.ToDecimal(dr["vl_total"]);
							result.Activits.Add(obj);
						}
					}

					if (dr.NextResult())
					{
						while (dr.Read())
						{
							GuaranteeStock obj = new GuaranteeStock();
							if (dr["nm_ativo"] != DBNull.Value) obj.Stock = dr["nm_ativo"].ToString();
							if (dr["quantidade"] != DBNull.Value) obj.Quantity = Convert.ToInt32(dr["quantidade"]);
							if (dr["vl_total"] != DBNull.Value) obj.Value = Convert.ToDecimal(dr["vl_total"]);
							result.Stocks.Add(obj);
						}
					}
				}
				dr.Close();
			}
			return result;
		}
	}
}
