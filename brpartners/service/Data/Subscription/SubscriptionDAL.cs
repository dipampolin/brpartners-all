﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using QX3.Spinnex.Common.Services.Data;
using QX3.Portal.Contracts.DataContracts;
using System.Data.OracleClient;

namespace QX3.Portal.Services.Data
{
    public class SubscriptionDAL: DataProviderBase
    {
        public SubscriptionDAL(): base("PortalConnectionString") { }

        public List<Subscription> LoadSubscriptions(Subscription filter, FilterOptions options, bool getProspects, out int count)
        {
            count = 0;
            List<Subscription> results = null;

            var cmd = dbi.CreateCommand("PCK_PORTAL_SUBSCRICAO.PROC_CONSULTA_CADASTRO_PAG", CommandType.StoredProcedure);

            if (!String.IsNullOrEmpty(filter.ISINStock))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_NEGOCIO", Value = filter.ISINStock, IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_NEGOCIO", Value = DBNull.Value, IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input });

            if (filter.InitialDate.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_INICIO", Value = filter.InitialDate.Value, IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_INICIO", Value = DBNull.Value, IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input });

            if (filter.BrokerDate.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_CORRETORA", Value = filter.BrokerDate.Value, IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_CORRETORA", Value = DBNull.Value, IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input });

            if (filter.Status.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_STATUS", Value = filter.Status.Value, IsNullable = true, OracleType = OracleType.Char, Direction = ParameterDirection.Input });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_STATUS", Value = DBNull.Value, IsNullable = true, OracleType = OracleType.Char, Direction = ParameterDirection.Input });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pINITROW", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = options.InitRow });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pFINALROW", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = options.FinalRow });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pSORTCOLUMN", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = options.SortColumn });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pSORTDIR", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = options.SortDirection });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pTODOS", IsNullable = true, OracleType = OracleType.Int32, Direction = ParameterDirection.Input, Value = (options.AllRecords) ? 1 : 0 });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pPROSPECTO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = (getProspects) ? 1 : 0 });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCount", OracleType = OracleType.Number, Direction = ParameterDirection.Output });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSOR", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var reader = cmd.ExecuteReader())
            {
                count = Convert.ToInt32(cmd.Parameters["pCount"].Value);

                if (reader.HasRows) 
                {
                    results = new List<Subscription>();
                    int auxint = 0;

                    while (reader.Read()) 
                    {
                        var sub = new Subscription();

                        int counter = -1;
                       
                        if (!reader.IsDBNull(++counter)) sub.Company = reader.GetString(counter);
                        
                        if (!reader.IsDBNull(++counter)) sub.StockCode = reader.GetString(counter);
                        if (Int32.TryParse(sub.StockCode, out auxint)) sub.StockCode = "-";
                        
                        if (!reader.IsDBNull(++counter)) sub.RequestId = reader.GetInt32(counter);
                        
                        if (!reader.IsDBNull(++counter)) sub.InitialDate = reader.GetDateTime(counter);
                        if (!reader.IsDBNull(++counter)) sub.COMDate = reader.GetDateTime(counter);
                        if (!reader.IsDBNull(++counter)) sub.BrokerDate = reader.GetDateTime(counter);
                        if (!reader.IsDBNull(++counter)) sub.StockExchangeDate = reader.GetDateTime(counter);
                        if (!reader.IsDBNull(++counter)) sub.CompanyDate = reader.GetDateTime(counter);
                        
                        if (!reader.IsDBNull(++counter)) sub.DescStatus = reader.GetString(counter);
                        if (!reader.IsDBNull(++counter)) sub.BrokerHour = Convert.ToDateTime(reader.GetString(counter));

                        if (!reader.IsDBNull(++counter)) sub.ISINStock = reader.GetString(counter);
                        if (!reader.IsDBNull(++counter)) sub.ISINSubscription = reader.GetString(counter);
                        if (!reader.IsDBNull(++counter)) sub.RightPercentual = reader.GetDecimal(counter);
                        if (!reader.IsDBNull(++counter)) sub.RightValue = reader.GetDecimal(counter);

                        if (!reader.IsDBNull(++counter)) sub.Description = reader.GetString(counter);
                        if (!reader.IsDBNull(++counter)) sub.SubjectMail = reader.GetString(counter);
                        if (!reader.IsDBNull(++counter)) sub.BodyMail = reader.GetString(counter);
                        if (!reader.IsDBNull(++counter)) sub.Status = reader.GetString(counter).ToCharArray()[0];
                        
                        if (!reader.IsDBNull(++counter)) sub.Prospect = (byte[])reader[counter];

                        results.Add(sub);
                    }
                }
            }

            return results;
        }

        public bool ManageSubscription(Subscription request, char operationType, out int requestId) 
        {
            /*var cmd = dbi.CreateCommand("DECLARE A " + OracleType.Blob + "; " +
                            "BEGIN " +
                                "DBMS_LOB.CREATETEMPORARY(A, FALSE); " +
                                ":LOC := A; " +
                            "END;", CommandType.Text);*/

            var cmd = dbi.CreateCommand("PCK_PORTAL_SUBSCRICAO.PROC_CADASTRO_SUBSCRICAO", CommandType.StoredProcedure);
            cmd.Transaction = dbi.BeginTransaction();

            /*cmd.Parameters.Add(new OracleParameter { Direction = ParameterDirection.Output, ParameterName = "LOC", OracleType = OracleType.Blob });*/
            try
            {
                /*cmd.ExecuteNonQuery();

                OracleLob ora = (OracleLob)cmd.Parameters["LOC"].Value;

                cmd.Parameters.Clear();
                cmd.CommandText = "PCK_PORTAL_SUBSCRICAO.PROC_CADASTRO_SUBSCRICAO";
                cmd.CommandType = CommandType.StoredProcedure;*/

                if (operationType == 'I')
                {
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_SUBSCRICAO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
                }
                else
                {
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_SUBSCRICAO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = request.RequestId });
                }

                if (String.IsNullOrEmpty(request.Company))
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pDS_EMPRESA", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
                else
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pDS_EMPRESA", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = request.Company });

                if (String.IsNullOrEmpty(request.ISINStock))
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_ISIN_PAPEL", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
                else
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_ISIN_PAPEL", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = request.ISINStock });

                if (String.IsNullOrEmpty(request.ISINSubscription))
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_ISIN_SUBSCRICAO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
                else
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_ISIN_SUBSCRICAO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = request.ISINSubscription });

                if (!request.RightPercentual.HasValue)
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pPC_DIREITO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
                else
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pPC_DIREITO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = request.RightPercentual.Value });

                if (!request.RightValue.HasValue)
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pVL_DIREITO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
                else
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pVL_DIREITO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = request.RightValue.Value });

                if (!request.InitialDate.HasValue)
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_INICIO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
                else
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_INICIO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = request.InitialDate.Value.ToString("dd/MM/yyyy") });

                if (!request.COMDate.HasValue)
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_COM", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
                else
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_COM", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = request.COMDate.Value.ToString("dd/MM/yyyy") });

                if (!request.BrokerDate.HasValue)
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_CORRETORA", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
                else
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_CORRETORA", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = request.BrokerDate.Value.ToString("dd/MM/yyyy") });

                if (!request.BrokerHour.HasValue)
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pHH_CORRETORA", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
                else
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pHH_CORRETORA", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = request.BrokerHour.Value.ToString("HH:mm") });

                if (!request.StockExchangeDate.HasValue)
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_BOLSA", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
                else
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_BOLSA", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = request.StockExchangeDate.Value.ToString("dd/MM/yyyy") });

                if (!request.CompanyDate.HasValue)
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_EMPRESA", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
                else
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_EMPRESA", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = request.CompanyDate.Value.ToString("dd/MM/yyyy") });

                if (operationType == 'I')
                {
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_STATUS", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = "P" });
                }
                else
                {
                    if (request.Status.HasValue)
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_STATUS", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = request.Status.Value.ToString() });
                    else
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_STATUS", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
                }

                if (String.IsNullOrEmpty(request.Description))
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pDS_DESCRICAO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
                else
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pDS_DESCRICAO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = request.Description });

                if (String.IsNullOrEmpty(request.Description))
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pDS_ASSUNTO_EMAIL", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
                else
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pDS_ASSUNTO_EMAIL", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = request.SubjectMail });

                if (String.IsNullOrEmpty(request.Description))
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pDS_CORPO_EMAIL", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
                else
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pDS_CORPO_EMAIL", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = request.BodyMail });

                /*if (request.Prospect == null)
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pDS_PROSPECTO", IsNullable = true, OracleType = OracleType.Blob, Direction = ParameterDirection.Input, Value = DBNull.Value });
                else
                {
                    ora.Write(request.Prospect, 0, request.Prospect.Length);
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pDS_PROSPECTO", IsNullable = true, OracleType = OracleType.Blob, Direction = ParameterDirection.Input, Value = ora });
                }*/


                cmd.Parameters.Add(new OracleParameter { ParameterName = "pTP_OPERACAO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = operationType.ToString() });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_SUBSCRICAO_REQUEST", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Output });

                int exec = cmd.ExecuteNonQuery();

                requestId = Convert.ToInt32(cmd.Parameters["pID_SUBSCRICAO_REQUEST"].Value);

                cmd.Transaction.Commit();

                return (exec > 0);
            }
            catch
            {
                requestId = 0;
                if (cmd.Transaction != null)
                    cmd.Transaction.Rollback();
                return false;
            }
        }

        public List<SubscriptionRights> LoadSubscriptionRights(SubscriptionRights filter, FilterOptions options, out int count, int? userId) 
        {
            count = 0;
            List<SubscriptionRights> results = null;

            List<SubscriptionNotification> notifys = null;

			var cmd = dbi.CreateCommand("PCK_PORTAL_SUBSCRICAO.PROC_CONSULTA_DIREITO", CommandType.StoredProcedure);

            if (userId.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", Value = userId.Value, OracleType = OracleType.Number, Direction = ParameterDirection.Input });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", Value = DBNull.Value, OracleType = OracleType.Number, Direction = ParameterDirection.Input });

            if (!String.IsNullOrEmpty(filter.ClientCodeFilter))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTCLIENTE", Value = filter.ClientCodeFilter, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTCLIENTE", Value = DBNull.Value, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input });

            if (!String.IsNullOrEmpty(filter.AssessorFilter))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTASSESSOR", Value = filter.AssessorFilter, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTASSESSOR", Value = DBNull.Value, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input });

            if (filter.Scraps.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_SOBRAS", Value = filter.Scraps.Value.ToString(), OracleType = OracleType.VarChar, Direction = ParameterDirection.Input });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_SOBRAS", Value = DBNull.Value, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input });

            if (!String.IsNullOrEmpty(filter.StockCode))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_PAPEL", Value = filter.StockCode, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_PAPEL", Value = DBNull.Value, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input });

            if (filter.Status.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_STATUS", Value = filter.Status.Value.ToString(), OracleType = OracleType.VarChar, Direction = ParameterDirection.Input });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_STATUS", Value = DBNull.Value, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input });

            if (filter.Warning.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_AVISO", Value = filter.Warning.Value.ToString(), OracleType = OracleType.VarChar, Direction = ParameterDirection.Input });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_AVISO", Value = DBNull.Value, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input });

            if (filter.InitialDate.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_INICIO", Value = filter.InitialDate.Value.ToShortDateString(), OracleType = OracleType.VarChar, Direction = ParameterDirection.Input });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_INICIO", Value = DBNull.Value, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input });

            if (filter.FinalDate.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_FIM", Value = filter.FinalDate.Value.ToShortDateString(), OracleType = OracleType.VarChar, Direction = ParameterDirection.Input });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_FIM", Value = DBNull.Value, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input });

            if (filter.RequestId > 0)
            {
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_SUBSCRICAO", Value = filter.RequestId, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input });
                
                notifys = this.LoadNotifications(filter.RequestId);
            }
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_SUBSCRICAO", Value = DBNull.Value, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input });

			cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSOR", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            

            using (var reader = cmd.ExecuteReader()) 
            {
                if (reader.HasRows) 
                {
                    results = new List<SubscriptionRights>();
                    while (reader.Read()) 
                    { 
                        var right = new SubscriptionRights();

                        int counter = -1;

						if (!reader.IsDBNull(++counter)) right.ClientCode = reader.GetInt32(counter); 
						if (!reader.IsDBNull(++counter)) right.ClientName = reader.GetString(counter);
						if (!reader.IsDBNull(++counter)) right.AssessorCode = reader.GetInt32(counter);
						if (!reader.IsDBNull(++counter)) right.AssessorName = reader.GetString(counter);

                        if (!reader.IsDBNull(++counter)) right.StockCode = reader.GetString(counter);
                        if (!reader.IsDBNull(++counter)) right.EspecificationName = reader.GetString(counter);

						if (!reader.IsDBNull(++counter)) right.RightsQtty = reader.GetInt64(counter);
						if (!reader.IsDBNull(++counter)) right.RequestedQtty = reader.GetInt64(counter);
						if (!reader.IsDBNull(++counter)) right.AvailableQtty = reader.GetInt64(counter);

                        if (!reader.IsDBNull(++counter)) right.RightValue = reader.GetDecimal(counter);
                        if (!reader.IsDBNull(++counter)) right.TotalValue = reader.GetDecimal(counter);

                        if (!reader.IsDBNull(++counter)) right.StockExchangeDate = reader.GetDateTime(counter);

						if (!reader.IsDBNull(++counter)) right.Scraps = reader.GetString(counter).ToCharArray().First();
                        
						//if (!reader.IsDBNull(++counter)) right.Warning = reader.GetString(counter).ToCharArray().First();
						if (!reader.IsDBNull(++counter)) right.Status = reader.GetString(counter).ToCharArray().First();
						
                        if (!reader.IsDBNull(++counter)) right.RequestId = reader.GetInt32(counter);

                        if (!reader.IsDBNull(++counter)) right.ClientEmail = reader.GetString(counter);

                        if (notifys != null)
                        {
                            char? warning = null;

                            if (notifys.Where(a => a.ClientCode == right.ClientCode && a.SubscriptionId == right.RequestId).FirstOrDefault() != null)
                                if (notifys.Where(a => a.ClientCode == right.ClientCode && a.SubscriptionId == right.RequestId).FirstOrDefault().Warning != null)
                                    warning = notifys.Where(a => a.ClientCode == right.ClientCode && a.SubscriptionId == right.RequestId).FirstOrDefault().Warning.ToCharArray().First();

                            right.Warning = warning;
                        }

                        if (filter.Warning.HasValue) 
                        {
                            if (right.Warning == filter.Warning || (filter.Warning.Value == 'N' && !right.Warning.HasValue)) 
                            {
                                results.Add(right);
                            }
                        }
                        else{

                            results.Add(right);
                        }
                    }
					count = results.Count;
                }
            }
			return results;
        }

        public bool ManageSubscriptionRights(SubscriptionRights request, char operationType) 
        { 
            /*
             procedure PROC_SOLICITA_SUBSCRICAO ( pCD_CLIENTE       in Number, 
                                       pID_SUBSCRICAO    in Number,
                                       pQT_SOLICITADA    in Number,
                                       pIN_SOBRAS        in char,
                                       pIN_STATUS        in char, 
                                       pIN_AVISO         in char,
                                       pTP_OPERACAO      In char ) is
             */
            var cmd = dbi.CreateCommand("PCK_PORTAL_SUBSCRICAO.PROC_SOLICITA_SUBSCRICAO", CommandType.StoredProcedure);

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", Value = request.ClientCode, OracleType = OracleType.Number, Direction = ParameterDirection.Input, IsNullable = true });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_SUBSCRICAO", Value = request.RequestId, OracleType = OracleType.Number, Direction = ParameterDirection.Input, IsNullable = true });

            if (request.RequestedQtty.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pQT_SOLICITADA", Value = request.RequestedQtty.Value, OracleType = OracleType.Number, Direction = ParameterDirection.Input, IsNullable = true });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pQT_SOLICITADA", Value = DBNull.Value, OracleType = OracleType.Number, Direction = ParameterDirection.Input, IsNullable = true });

            if (request.Scraps.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_SOBRAS", Value = request.Scraps.Value, OracleType = OracleType.Char, Direction = ParameterDirection.Input, IsNullable = true });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_SOBRAS", Value = DBNull.Value, OracleType = OracleType.Char, Direction = ParameterDirection.Input, IsNullable = true });

            if (request.Status.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_STATUS", Value = request.Status.Value, OracleType = OracleType.Char, Direction = ParameterDirection.Input, IsNullable = true });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_STATUS", Value = DBNull.Value, OracleType = OracleType.Char, Direction = ParameterDirection.Input, IsNullable = true });

            if (request.Warning.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_AVISO", Value = request.Warning.Value, OracleType = OracleType.Char, Direction = ParameterDirection.Input, IsNullable = true });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIN_AVISO", Value = DBNull.Value, OracleType = OracleType.Char, Direction = ParameterDirection.Input, IsNullable = true });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pTP_OPERACAO", Value = operationType, OracleType = OracleType.Char, Direction = ParameterDirection.Input, IsNullable = true });

            int exec = cmd.ExecuteNonQuery();

            return (exec > 0);
        }

        public long GetRequestedQuantity(SubscriptionRights request) 
        {
            var cmd = dbi.CreateCommand("PCK_PORTAL_SUBSCRICAO.FUNC_RETORNA_SOLICITADO", CommandType.StoredProcedure);
            cmd.Parameters.Clear();

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", IsNullable = true, OracleType = OracleType.Char, Direction = ParameterDirection.Input, Value = request.ClientCode });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_SUBSCRICAO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = request.RequestId });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "rRetorno", IsNullable = false, OracleType = OracleType.Number, Direction = ParameterDirection.ReturnValue });

            cmd.ExecuteReader();

            return Convert.ToInt64(cmd.Parameters["rRetorno"].Value);
        }

        public long GetRightQuantity(SubscriptionRights request)
        {
            var cmd = dbi.CreateCommand("PCK_PORTAL_SUBSCRICAO.FUNC_RETORNA_DIREITO", CommandType.StoredProcedure);
            cmd.Parameters.Clear();

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", IsNullable = true, OracleType = OracleType.Char, Direction = ParameterDirection.Input, Value = request.ClientCode });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_SUBSCRICAO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = request.RequestId });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "rRetorno", IsNullable = false, OracleType = OracleType.Number, Direction = ParameterDirection.ReturnValue });

            cmd.ExecuteReader();

            return Convert.ToInt64(cmd.Parameters["rRetorno"].Value);
        }



        public List<SubscriptionNotification> LoadNotifications(int subscriptionId) 
        {
            List<SubscriptionNotification> results = null;

            var cmd = dbi.CreateCommand("PCK_PORTAL_SUBSCRICAO.PROC_NOTIFICACAO_SUBSCRICAO", CommandType.StoredProcedure);
            cmd.Parameters.Clear();

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_SUBSCRICAO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = subscriptionId });
            			
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSOR", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var reader = cmd.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    results = new List<SubscriptionNotification>();
                    while (reader.Read())
                    {
                        var obj = new SubscriptionNotification();

                        int counter = -1;

                        if (!reader.IsDBNull(++counter)) obj.SubscriptionId = reader.GetInt32(counter);
                        if (!reader.IsDBNull(++counter)) obj.ClientCode = reader.GetInt32(counter);
                        if (!reader.IsDBNull(++counter)) obj.Warning = reader.GetString(counter);

                        results.Add(obj);
                    }
                }
            }

            return results;
        }
                
    }
}
