﻿using QX3.Spinnex.Common.Services.Data;
using QX3.Portal.Contracts.DataContracts;
using System.Data;
using System.Data.OracleClient;
using System;
using System.Collections.Generic;

namespace QX3.Portal.Services.Data
{
    public class RiskDAL : DataProviderBase
    {
        public RiskDAL() : base("PortalConnectionString") { }

        public List<Int32> InsertGuarantee(Guarantee guarantee)
        {
            List<Int32> ids = new List<int>();

            foreach (GuaranteeItem item in guarantee.Items)
            {
                var cmd = dbi.CreateCommand("PCK_GARANTIA.PROC_SOLICITA_GARANTIA", CommandType.StoredProcedure);

                bool isMoney = !item.IsQuantity;
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCdCliente", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = Int32.Parse(guarantee.Client.Value) });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pInRetirada", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = Convert.ToInt32(item.IsWithdrawal) });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pInBovespa", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = Convert.ToInt32(item.IsBovespa) });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pAtivo", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = item.Guarantee });
                if (!isMoney)
                {
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pQuantidade", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = item.RequestedValue });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pValor", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = item.RequestedValue * item.UnitPrice });
                }
                else
                {
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pQuantidade", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pValor", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = item.RequestedValue });
                }
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIdSolicitacao", OracleType = OracleType.Number, Direction = ParameterDirection.Output, Size = 60 });
                cmd.ExecuteNonQuery();
                ids.Add((cmd.Parameters["pIdSolicitacao"].Value == DBNull.Value) ? 0 : Int32.Parse(cmd.Parameters["pIdSolicitacao"].Value.ToString()));
            }
            return ids;
        }

        public bool UpdateGuarantee(GuaranteeItem guaranteeItem)
        {
            List<Int32> ids = new List<int>();

            var cmd = dbi.CreateCommand("PCK_GARANTIA.PROC_ALTERA_GARANTIA", CommandType.StoredProcedure);

            bool isMoney = guaranteeItem.Guarantee.ToLower().IndexOf("dinheiro") >= 0;
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIdSolicitacao", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = guaranteeItem.ID });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pAtivo", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = guaranteeItem.Guarantee });
            if (!isMoney)
            {
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pQuantidade", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = guaranteeItem.RequestedValue });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pValor", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = guaranteeItem.RequestedValue * guaranteeItem.UnitPrice });
            }
            else
            {
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pQuantidade", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pValor", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = guaranteeItem.RequestedValue });
            }
            return cmd.ExecuteNonQuery() > 0;
        }

        public bool DeleteGuarantee(int guaranteeID)
        {
            var cmd = dbi.CreateCommand("PCK_GARANTIA.PROC_DELETA_GARANTIA", CommandType.StoredProcedure);
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIdSolicitacao", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = guaranteeID });
            return cmd.ExecuteNonQuery() > 0;
        }

        public bool ChangeGuaranteeStatus(int guaranteeID, int statusID)
        {
            var cmd = dbi.CreateCommand("PCK_GARANTIA.PROC_ALTERA_STATUS", CommandType.StoredProcedure);
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIdSolicitacao", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = guaranteeID });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIdStatus", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = statusID });
            return cmd.ExecuteNonQuery() > 0;
        }

        public List<Guarantee> LoadGuarantees(GuaranteeFilter filter, int? userId)
        {
            List<Guarantee> guarantees = new List<Guarantee>();

            var cmd = dbi.CreateCommand("PCK_GARANTIA.PROC_CONSULTA_GARANTIA", CommandType.StoredProcedure);

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIdStatus", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = filter.StatusID });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pLstAssessor", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.Assessors });
            if (userId.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = userId.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
            if (string.IsNullOrEmpty(filter.Clients))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLstCliente", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLstCliente", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.Clients });

            if (string.IsNullOrEmpty(filter.StartDate))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDataInicio", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDataInicio", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.StartDate });
            if (string.IsNullOrEmpty(filter.EndDate))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDataFim", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDataFim", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.EndDate });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCursor", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            string currentCode = string.Empty;

            using (var dr = cmd.ExecuteReader())
            {
                Guarantee guarantee = new Guarantee();
                List<GuaranteeItem> items = new List<GuaranteeItem>();
                while (dr.Read())
                {
                    string clientCode = Convert.ToString(dr["CDCLIENTE"]);

                    if (currentCode == "" || currentCode != clientCode)
                    {
                        if (currentCode != "")
                        {
                            guarantee.Items = items;
                            guarantees.Add(guarantee);
                        }
                        guarantee = new Guarantee();
                        guarantee.Client = new CommonType { Value = clientCode, Description = Convert.ToString(dr["NOMECLIENTE"]) };
                        guarantee.Assessor = new CommonType { Value = Convert.ToString(dr["CDASSESSOR"]), Description = Convert.ToString(dr["NOMEASSESSOR"]) };
                        items = new List<GuaranteeItem>();
                    }
                    GuaranteeItem item = new GuaranteeItem();
                    if (!(dr["IDSOLICITACAO"] is DBNull))
                        item.ID = Convert.ToInt32(dr["IDSOLICITACAO"]);
                    if (!(dr["TIPOSOLICITACAO"] is DBNull))
                    {
                        item.Type = Convert.ToString(dr["TIPOSOLICITACAO"]);
                        item.IsWithdrawal = item.Type.ToLower() == "retirada";
                    }
                    if (!(dr["IDSTATUS"] is DBNull))
                        item.Status = new CommonType { Value = Convert.ToString(dr["IDSTATUS"]), Description = Convert.ToString(dr["STATUS"]) };
                    if (!(dr["ATIVO"] is DBNull))
                        item.Guarantee = Convert.ToString(dr["ATIVO"]);
                    if (!(dr["MERCADO"] is DBNull))
                        item.IsBovespa = Convert.ToString(dr["MERCADO"]) != "BMF";
                    if (!(dr["DATASOLICITACAO"] is DBNull))
                        item.UpdateDate = Convert.ToDateTime(dr["DATASOLICITACAO"]);
                    if (!(dr["QUANTIDADE"] is DBNull))
                        item.Quantity = Convert.ToInt64(dr["QUANTIDADE"]);
                    if (!(dr["VALORUNITARIO"] is DBNull))
                        item.UnitPrice = Convert.ToDecimal(dr["VALORUNITARIO"]);
                    if (!(dr["VALORGARANTIA"] is DBNull))
                        item.Total = Convert.ToDecimal(dr["VALORGARANTIA"]);
                    if (!(dr["VALORSOLICITADO"] is DBNull))
                        item.RequestedValue = Convert.ToDecimal(dr["VALORSOLICITADO"]);
                    items.Add(item);

                    currentCode = clientCode;
                }

                if (!string.IsNullOrEmpty(currentCode))
                {
                    guarantee.Items = items;
                    guarantees.Add(guarantee);
                }

                dr.Close();
            }

            foreach (Guarantee g in guarantees)
                g.Balance = LoadGuaranteeTotal(g.Client.Value);

            return guarantees;
        }

        protected GuaranteeBalance LoadGuaranteeTotal(string clientCode)
        {
            GuaranteeBalance balance = new GuaranteeBalance();

            var cmd = dbi.CreateCommand("PCK_GARANTIA.PROC_CONSULTA_TOTAL", CommandType.StoredProcedure);

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCdCliente", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = Int32.Parse(clientCode) });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pDataSolicitacao", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DateTime.Now.ToString("dd/MM/yyyy") });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pTotal", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.Read())
                {
                    if (!(dr["TOTAL"] is DBNull))
                        balance.Total = Convert.ToDecimal(dr["TOTAL"]);
                    if (!(dr["UTILIZADO"] is DBNull))
                        balance.Used = Convert.ToDecimal(dr["UTILIZADO"]);
                    if (!(dr["LIVRE"] is DBNull))
                        balance.Free = Convert.ToDecimal(dr["LIVRE"]);
                    if (!(dr["APOSEFETUACOES"] is DBNull))
                        balance.AfterRealize = Convert.ToDecimal(dr["APOSEFETUACOES"]);
                }
                dr.Close();
            }

            return balance;
        }

        public int GetNumberOfPending(string assessorsList)
        {
            var cmd = dbi.CreateCommand("PCK_GARANTIA.PROC_CONTA_PENDENTES", CommandType.StoredProcedure);

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pLstAssessor", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = assessorsList });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pPendentes", OracleType = OracleType.Number, Direction = ParameterDirection.Output, Size = 60 });
            cmd.ExecuteNonQuery();

            return cmd.Parameters["pPendentes"].Value == DBNull.Value ? 0 : Int32.Parse(cmd.Parameters["pPendentes"].Value.ToString());
        }

        public List<GuaranteeItem> LoadClientGuarantees(GuaranteeFilter filter, int? userId)
        {
            List<GuaranteeItem> guarantees = new List<GuaranteeItem>();

            var cmd = dbi.CreateCommand("PCK_GARANTIA.PROC_CARREGA_GARANTIA_CLIENTE", CommandType.StoredProcedure);

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCdCliente", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = Int32.Parse(filter.Clients) });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pLstAssessor", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.Assessors });
            if(userId.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = userId.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pInBovespa", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = Convert.ToInt32(filter.Bovespa) });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCursor", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    GuaranteeItem item = new GuaranteeItem();
                    if (!(dr["IDSOLICITACAO"] is DBNull))
                        item.ID = Convert.ToInt32(dr["IDSOLICITACAO"]);
                    if (!(dr["TIPOSOLICITACAO"] is DBNull))
                        item.Type = Convert.ToString(dr["TIPOSOLICITACAO"]);
                    if (!(dr["IDSTATUS"] is DBNull))
                        item.Status = new CommonType { Value = Convert.ToString(dr["IDSTATUS"]), Description = Convert.ToString(dr["STATUS"]) };
                    if (!(dr["ATIVO"] is DBNull))
                        item.Guarantee = Convert.ToString(dr["ATIVO"]);
                    if (!(dr["MERCADO"] is DBNull))
                        item.IsBovespa = Convert.ToString(dr["MERCADO"]) != "BMF";
                    if (!(dr["DATASOLICITACAO"] is DBNull))
                        item.UpdateDate = Convert.ToDateTime(dr["DATASOLICITACAO"]);
                    if (!(dr["QUANTIDADE"] is DBNull))
                        item.Quantity = Convert.ToInt64(dr["QUANTIDADE"]);
                    if (!(dr["VALORUNITARIO"] is DBNull))
                        item.UnitPrice = Convert.ToDecimal(dr["VALORUNITARIO"]);
                    if (!(dr["VALORGARANTIA"] is DBNull))
                        item.Total = Convert.ToDecimal(dr["VALORGARANTIA"]);
                    if (!(dr["VALORSOLICITADO"] is DBNull))
                        item.RequestedValue = Convert.ToDecimal(dr["VALORSOLICITADO"]);
                    item.IsQuantity = item.Quantity > 0;

                    guarantees.Add(item);
                }
                dr.Close();
            }
            return guarantees;
        }

    }
}
