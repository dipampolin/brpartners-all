﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Data.OracleClient;
using QX3.Spinnex.Common.Services.Data;
using QX3.Portal.Contracts.DataContracts;
using QX3.Spinnex.Common.Services.Logging;
using System.Linq;
using System.Configuration;

namespace QX3.Portal.Services.Data
{
    public class CommonDAL : DataProviderBase
    {
        public CommonDAL() : base("PortalConnectionString") { }

        public List<CommonType> LoadTypes(string moduleName, string specification)
        {
            List<CommonType> items = new List<CommonType>();

            var cmd = dbi.CreateCommand("PCK_PORTALWEB.PROC_CARREGATIPOS", CommandType.StoredProcedure);

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pNm_Modulo", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = moduleName });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pNm_Espec", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = specification });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSOR", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    CommonType item = new CommonType();

                    if (!(dr["ID"] is DBNull))
                        item.Value = Convert.ToString(dr["ID"]);
                    if (!(dr["DESCRICAO"] is DBNull))
                        item.Description = Convert.ToString(dr["DESCRICAO"]);
                    items.Add(item);
                }

                dr.Close();
            }

            return items;
        }

        public List<WarningList> GetWarningList(UserInformation user, string areaName, string assessors, string clients, int? userId)
        {
            List<WarningList> items = null;

            var cmd = dbi.CreateCommand("PCK_PENDENTES.PROC_CONSULTA_PENDENTES", CommandType.StoredProcedure);

            /*
             pIdUsuario         in number,
            pFuncaoTela        in varchar2,
            pLSTASSESSOR       in varchar2,
            pLSTCLIENTE        in varchar2,
            pCursor            out tCursor
             */
            cmd.Parameters.Clear();

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIdUsuario", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = user.ID });

            if (!String.IsNullOrEmpty(areaName))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pFuncaoTela", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = areaName });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pFuncaoTela", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });

            if (!String.IsNullOrEmpty(assessors))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTASSESSOR", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = assessors });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTASSESSOR", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });

            if (!String.IsNullOrEmpty(clients))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTCLIENTE", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = clients });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTCLIENTE", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });

            if (userId.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = userId.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });


            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCursor", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    /*
                      [DataMember] //NOME
                    public string AreaName { get; set; }

                    [DataMember] //Apelido
                    public string PageName { get; set; }

                    [DataMember] //LINK
                    public string Link { get; set; }

                    [DataMember] //STATUSID
                    public string IdStatus { get; set; }

                    [DataMember] //NOME_STATUS
                    public string Action { get; set; }

                    [DataMember] //CONTADOR
                    public int Count { get; set; }
                     */

                    items = new List<WarningList>();

                    while (dr.Read())
                    {
                        WarningList item = new WarningList();

                        if (!dr.IsDBNull(0)) item.AreaName = dr.GetString(0);//NOME
                        if (!dr.IsDBNull(1)) item.PageName = dr.GetString(1);//APELIDO
                        if (!dr.IsDBNull(2)) item.Link = dr.GetString(2);//LINK
                        if (!dr.IsDBNull(3)) item.IdStatus = dr.GetString(3);//STATUSID
                        if (!dr.IsDBNull(4)) item.Action = dr.GetString(4);//NOME_STATUS
                        if (!dr.IsDBNull(5)) item.Count = dr.GetInt32(5);//CONTADOR

                        items.Add(item);
                    }
                }

                dr.Close();
            }

            return items;
        }



        public List<int> GetFullListOfAssessors()
        {
            List<int> items = new List<int>();

            var cmd = dbi.CreateCommand("PCK_PORTALWEB.PROC_COD_ASSESSORES", CommandType.StoredProcedure);
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCursor", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    while (dr.Read())
                        items.Add(dr.GetInt32(0));
                }

                dr.Close();
            }

            return items;
        }

        public string GetClientName(int code)
        {
            var cmd = dbi.CreateCommand("PCK_PORTALWEB.PROC_RETORNA_NOME_CLIENTE", CommandType.StoredProcedure);
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCdCliente", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = code });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pNmCliente", OracleType = OracleType.VarChar, Direction = ParameterDirection.Output, Size = 60 });
            cmd.ExecuteNonQuery();

            return cmd.Parameters["pNmCliente"].Value.ToString();
        }

        public RelatedPersons RelatedPersons(DateTime dtStart, DateTime dtEnd) {

            RelatedPersons rp = new RelatedPersons();

            var cmd = dbi.CreateCommand("PCK_INSTITUCIONAL.PROC_PESSOAS_VINCULADAS", CommandType.StoredProcedure);

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_INICIO", OracleType = OracleType.DateTime, Direction = ParameterDirection.Input, Value = dtStart });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_FIM", OracleType = OracleType.DateTime, Direction = ParameterDirection.Input, Value = dtEnd });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_EMPRESA", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = ConfigurationManager.AppSettings["CD_EMPRESA"] });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSOR1", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSOR2", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSOR3", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSOR4", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSOR5", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });


            using (var dr = cmd.ExecuteReader()) {
                if (dr.HasRows) {

                    if (dr.Read()) {
                        rp.QdtTotalTrading = dr["QT_TOTNEG"] != DBNull.Value ? Convert.ToDecimal(dr["QT_TOTNEG"]) : 0;
                    }

                    if (dr.NextResult()) {

                        if (dr.Read()) {
                            rp.QtdTradingRelatedPersons = dr["QT_TOTNEG_PV"] != DBNull.Value ? Convert.ToDecimal(dr["QT_TOTNEG_PV"]) : 0;
                        }
                    }

                    if (dr.NextResult()) {

                        if (dr.Read()) {
                            rp.QtdTradingRelatedPersonsECP = dr["QT_TOTNEG_PV_ECP"] != DBNull.Value ? Convert.ToDecimal(dr["QT_TOTNEG_PV_ECP"]) : 0;
                        }
                    }

                    if (dr.NextResult()) {

                        if (dr.Read()) {
                            rp.QtdTradingRelatedPersonsCCP = dr["QT_TOTNEG_PV_CCP"] != DBNull.Value ? Convert.ToDecimal(dr["QT_TOTNEG_PV_CCP"]) : 0;
                        }
                    }

                    if (dr.NextResult()) {

                        if (dr.Read()) {
                            rp.QtdTradingCP = dr["QT_TOTNEG_CP"] != DBNull.Value ? Convert.ToDecimal(dr["QT_TOTNEG_CP"]) : 0;
                        }
                    }

                }

            }

            return rp;
        }

        #region Modelo de Email - Funcionalidade

        public MailModel GetMailModel(int funcionalityId) 
        {
            MailModel result = null;

            var cmd = dbi.CreateCommand("PCK_MODELO_EMAIL.PROC_CARREGA_MODELO_EMAIL", CommandType.StoredProcedure);
            cmd.Parameters.Clear();

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_FUNCIONALIDADE", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, 
                    Value = funcionalityId });
                        
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCursor", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    result = new MailModel();
                    while (dr.Read())
                    {
                        /*
                          ID_MODELO_EMAIL,
                          ID_FUNCIONALIDADE,
                          REMETENTE,
                          EMAIL_REPLY,
                          ASSUNTO,
                          CONTEUDO
                         */

                        if (!dr.IsDBNull(1)) result.FuncionalityId = dr.GetInt32(1);
                        if (!dr.IsDBNull(2)) result.ReplyName = dr.GetString(2);
                        if (!dr.IsDBNull(3)) result.ReplyMail = dr.GetString(3);
                        if (!dr.IsDBNull(4)) result.Subject = dr.GetString(4);
                        if (!dr.IsDBNull(5)) result.Content = dr.GetString(5);

                    }
                }

                dr.Close();
            }

            return result;
        }

        public List<TagModel> GetTagModel()
        {
            List<TagModel> result = null;

            var cmd = dbi.CreateCommand("PCK_MODELO_EMAIL.PROC_CARREGA_TAGS", CommandType.StoredProcedure);
            cmd.Parameters.Clear();

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCursor", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    result = new List<TagModel>();
                    while (dr.Read())
                    {
                        var tag = new TagModel();
                        
                        if (!dr.IsDBNull(0)) tag.TagId = dr.GetInt32(0);
                        if (!dr.IsDBNull(1)) tag.TagName = dr.GetString(1);
                        if (!dr.IsDBNull(2)) tag.TagDescription = dr.GetString(2);

                        result.Add(tag);

                    }
                }

                dr.Close();
            }

            return result;
        }

        public bool ManageMailModel(MailModel model, char operation)
        {

            var cmd = dbi.CreateCommand("PCK_MODELO_EMAIL.PROC_CADASTRO_MODELO_EMAIL", CommandType.StoredProcedure);
            cmd.Parameters.Clear();

            /*
            pID_FUNCIONALIDADE      in number,
            pTP_OPERACAO            in char,
            pREMETENTE              in varchar2,
            pEMAILREPLY             in varchar2,
            pASSUNTO                in varchar2,
            pCONTEUDO               in varchar2
             */

            cmd.Parameters.Add(new OracleParameter
            {
                ParameterName = "pID_FUNCIONALIDADE",
                IsNullable = true,
                OracleType = OracleType.Number,
                Direction = ParameterDirection.Input,
                Value = model.FuncionalityId
            });

            cmd.Parameters.Add(new OracleParameter
            {
                ParameterName = "pTP_OPERACAO",
                IsNullable = true,
                OracleType = OracleType.Char,
                Direction = ParameterDirection.Input,
                Value = operation
            });
            ////
            if (String.IsNullOrEmpty(model.ReplyName))
            {
                cmd.Parameters.Add(new OracleParameter
                {
                    ParameterName = "pREMETENTE",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Direction = ParameterDirection.Input,
                    Value = DBNull.Value
                });
            }
            else 
            {
                cmd.Parameters.Add(new OracleParameter
                {
                    ParameterName = "pREMETENTE",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Direction = ParameterDirection.Input,
                    Value = model.ReplyName
                });
            }
            ////
            if (String.IsNullOrEmpty(model.ReplyMail))
            {
                cmd.Parameters.Add(new OracleParameter
                {
                    ParameterName = "pEMAILREPLY",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Direction = ParameterDirection.Input,
                    Value = DBNull.Value
                });
            }
            else
            {
                cmd.Parameters.Add(new OracleParameter
                {
                    ParameterName = "pEMAILREPLY",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Direction = ParameterDirection.Input,
                    Value = model.ReplyMail
                });
            }
            ////
            if (String.IsNullOrEmpty(model.Subject))
            {
                cmd.Parameters.Add(new OracleParameter
                {
                    ParameterName = "pASSUNTO",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Direction = ParameterDirection.Input,
                    Value = DBNull.Value
                });
            }
            else
            {
                cmd.Parameters.Add(new OracleParameter
                {
                    ParameterName = "pASSUNTO",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Direction = ParameterDirection.Input,
                    Value = model.Subject
                });
            }
            ////
            if (String.IsNullOrEmpty(model.Content))
            {
                cmd.Parameters.Add(new OracleParameter
                {
                    ParameterName = "pCONTEUDO",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Direction = ParameterDirection.Input,
                    Value = DBNull.Value
                });
            }
            else
            {
                cmd.Parameters.Add(new OracleParameter
                {
                    ParameterName = "pCONTEUDO",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Direction = ParameterDirection.Input,
                    Value = model.Content
                });
            }

            return (cmd.ExecuteNonQuery() > 0);
        }

        #endregion
    }
}
