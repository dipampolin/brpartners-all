﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OracleClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Serialization;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.Services.GrTraderService;
using QX3.Spinnex.Common.Services.Data;
using QX3.Spinnex.Common.Services.Mail;
using System.Diagnostics;

namespace QX3.Portal.Services.Data
{
    public class OtherDAL : DataProviderBase
    {
        public OtherDAL() : base("PortalConnectionString") { }

        #region Orders Correction

        public int InsertOrdersCorrection(OrdersCorrection correction)
        {
            bool update = correction.ID > 0;
            int correctionId = 0;

            var cmd = dbi.CreateCommand(update ? "PCK_CORRECAO_ORDENS.PROC_ATUALIZACORRECAO" : "PCK_CORRECAO_ORDENS.PROC_INSERECORRECAO", CommandType.StoredProcedure);

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIdTipo", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = Int32.Parse(correction.Type.Value ?? "0") });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIdAssessor", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = Int32.Parse(correction.Responsible.Value) });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCodClienteOrig", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = Int32.Parse(correction.ClientFrom.Value) });
            if (correction.ClientTo != null && !string.IsNullOrEmpty(correction.ClientTo.Value))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCodClienteDest", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = Int32.Parse(correction.ClientTo.Value) });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCodClienteDest", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIdStatus", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = Int32.Parse(correction.Status.Value) });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pObs", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = correction.Comments });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIdUsuario", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = correction.UserID });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIdDescricao", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = Int32.Parse(correction.Description.Value) });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pD1", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = correction.D1 ? 1 : 0 });
            if (update)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIdCorrecao", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = correction.ID });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIdCorrecao", OracleType = OracleType.Number, Direction = ParameterDirection.Output, Size = 6 });
            cmd.ExecuteNonQuery();
            correctionId = update ? correction.ID : (cmd.Parameters["pIdCorrecao"].Value == DBNull.Value) ? 0 : Int32.Parse(cmd.Parameters["pIdCorrecao"].Value.ToString());

            if (correctionId > 0)
            {
                foreach (OrdersCorrectionDetail item in correction.Details)
                {
                    try
                    {
                        var cmd2 = dbi.CreateCommand("PCK_CORRECAO_ORDENS.PROC_INSERECORRECAODETALHE", CommandType.StoredProcedure);
                        cmd2.Parameters.Add(new OracleParameter { ParameterName = "pIdCorrecao", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = correctionId });
                        cmd2.Parameters.Add(new OracleParameter { ParameterName = "pAtivo", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = item.StockCode });
                        cmd2.Parameters.Add(new OracleParameter { ParameterName = "pQuantidade", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = item.Quantity });
                        cmd2.Parameters.Add(new OracleParameter { ParameterName = "pPreco", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = item.Value });
                        cmd2.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        DeleteOrdersCorrection(correctionId);
                        throw ex;
                    }
                }
            }

            return correctionId;

        }

        public bool DeleteOrdersCorrection(int orderCorrectionID)
        {
            var cmd = dbi.CreateCommand("PCK_CORRECAO_ORDENS.PROC_DELETACORRECAO", CommandType.StoredProcedure);
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIdCorrecao", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = orderCorrectionID });
            return cmd.ExecuteNonQuery() > 0;
        }

        public bool ChangeOrdersCorrectionStatus(int orderCorrectionID, int statusID)
        {
            var cmd = dbi.CreateCommand("PCK_CORRECAO_ORDENS.PROC_ALTERASTATUSCORRECAO", CommandType.StoredProcedure);
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIdCorrecao", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = orderCorrectionID });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIdStatus", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = statusID });
            return cmd.ExecuteNonQuery() > 0;
        }

        public List<OrdersCorrection> LoadOrdersCorrections(OrdersCorrectionFilter filter)
        {
            List<OrdersCorrection> items = new List<OrdersCorrection>();

            var cmd = dbi.CreateCommand("PCK_CORRECAO_ORDENS.PROC_CARREGACORRECOES", CommandType.StoredProcedure);

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pLstAssessor", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.Assessors });

            if (string.IsNullOrEmpty(filter.Clients))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLstCliente", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLstCliente", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.Clients });

            if (filter.UserId.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = filter.UserId.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIdStatus", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = filter.StatusID });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIdTipo", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = filter.TypeID });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pDtInicio", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.StartDate });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pDtFim", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.EndDate });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCursor", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    OrdersCorrection item = new OrdersCorrection();

                    if (!(dr["ID"] is DBNull))
                        item.ID = Convert.ToInt32(dr["ID"]);
                    if (!(dr["IDTIPO"] is DBNull))
                        item.Type = new CommonType { Value = Convert.ToString(dr["IDTIPO"]), Description = Convert.ToString(dr["TIPO"]) };
                    if (!(dr["IDSTATUS"] is DBNull))
                        item.Status = new CommonType { Value = Convert.ToString(dr["IDSTATUS"]), Description = Convert.ToString(dr["STATUS"]) };
                    if (!(dr["IDDESCRICAO"] is DBNull))
                        item.Description = new CommonType { Value = Convert.ToString(dr["IDDESCRICAO"]), Description = Convert.ToString(dr["DESCRICAO"]) };
                    if (!(dr["IDASSESSOR"] is DBNull))
                        item.Responsible = new CommonType { Value = Convert.ToString(dr["IDASSESSOR"]), Description = Convert.ToString(dr["ASSESSOR"]) };
                    if (!(dr["IDCLIENTEORIGEM"] is DBNull))
                        item.ClientFrom = new CommonType { Value = Convert.ToString(dr["IDCLIENTEORIGEM"]), Description = Convert.ToString(dr["CLIENTEORIGEM"]) };
                    if (!(dr["IDCLIENTEDESTINO"] is DBNull))
                        item.ClientTo = new CommonType { Value = Convert.ToString(dr["IDCLIENTEDESTINO"]), Description = Convert.ToString(dr["CLIENTEDESTINO"]) };
                    if (!(dr["DATASOLICITACAO"] is DBNull))
                        item.Date = Convert.ToDateTime(dr["DATASOLICITACAO"]);
                    if (!(dr["OBSERVACAO"] is DBNull))
                        item.Comments = Convert.ToString(dr["OBSERVACAO"]);
                    if (!(dr["D1"] is DBNull))
                        item.D1 = Convert.ToString(dr["D1"]) == "1";
                    items.Add(item);
                }

                dr.Close();
            }

            foreach (OrdersCorrection order in items)
                order.Details = LoadOrdersCorrectionDetails(order.ID);

            return items;
        }

        protected List<OrdersCorrectionDetail> LoadOrdersCorrectionDetails(int correctionID)
        {
            var cmd2 = dbi.CreateCommand("PCK_CORRECAO_ORDENS.PROC_CARREGADETCORRECAO", CommandType.StoredProcedure);

            cmd2.Parameters.Add(new OracleParameter { ParameterName = "pIdCorrecao", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = correctionID });
            cmd2.Parameters.Add(new OracleParameter { ParameterName = "pCursor", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            List<OrdersCorrectionDetail> details = new List<OrdersCorrectionDetail>();

            using (var dr = cmd2.ExecuteReader())
            {
                while (dr.Read())
                {
                    OrdersCorrectionDetail item = new OrdersCorrectionDetail();
                    if (!(dr["PAPEL"] is DBNull))
                        item.StockCode = Convert.ToString(dr["PAPEL"]);
                    if (!(dr["QUANTIDADE"] is DBNull))
                        item.Quantity = Convert.ToInt32(dr["QUANTIDADE"]);
                    if (!(dr["PRECO"] is DBNull))
                        item.Value = Convert.ToDecimal(dr["PRECO"]);
                    details.Add(item);
                }

                dr.Close();
            }

            return details;
        }

        public OrdersCorrection LoadOrdersCorrection(int correctionID)
        {
            OrdersCorrection item = new OrdersCorrection();

            var cmd = dbi.CreateCommand("PCK_CORRECAO_ORDENS.PROC_CARREGACORRECAO", CommandType.StoredProcedure);

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIdCorrecao", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = correctionID });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCursor", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.Read())
                {
                    if (!(dr["ID"] is DBNull))
                        item.ID = Convert.ToInt32(dr["ID"]);
                    if (!(dr["IDTIPO"] is DBNull))
                        item.Type = new CommonType { Value = Convert.ToString(dr["IDTIPO"]), Description = Convert.ToString(dr["TIPO"]) };
                    if (!(dr["IDSTATUS"] is DBNull))
                        item.Status = new CommonType { Value = Convert.ToString(dr["IDSTATUS"]), Description = Convert.ToString(dr["STATUS"]) };
                    if (!(dr["IDDESCRICAO"] is DBNull))
                        item.Description = new CommonType { Value = Convert.ToString(dr["IDDESCRICAO"]), Description = Convert.ToString(dr["DESCRICAO"]) };
                    if (!(dr["IDASSESSOR"] is DBNull))
                        item.Responsible = new CommonType { Value = Convert.ToString(dr["IDASSESSOR"]), Description = Convert.ToString(dr["ASSESSOR"]) };
                    if (!(dr["IDCLIENTEORIGEM"] is DBNull))
                        item.ClientFrom = new CommonType { Value = Convert.ToString(dr["IDCLIENTEORIGEM"]), Description = Convert.ToString(dr["CLIENTEORIGEM"]) };
                    if (!(dr["IDCLIENTEDESTINO"] is DBNull))
                        item.ClientTo = new CommonType { Value = Convert.ToString(dr["IDCLIENTEDESTINO"]), Description = Convert.ToString(dr["CLIENTEDESTINO"]) };
                    if (!(dr["DATASOLICITACAO"] is DBNull))
                        item.Date = Convert.ToDateTime(dr["DATASOLICITACAO"]);
                    if (!(dr["OBSERVACAO"] is DBNull))
                        item.Comments = Convert.ToString(dr["OBSERVACAO"]);
                    if (!(dr["D1"] is DBNull))
                        item.D1 = Convert.ToString(dr["D1"]) == "1";
                }

                dr.Close();
            }

            item.Details = LoadOrdersCorrectionDetails(item.ID);

            return item;
        }

        #endregion

        #region Consolidated Position

        public CPClientInformation LoadClientInformation(string clientCode, string assessors, int? userId)
        {
            CPClientInformation obj = null;

            var cmd = dbi.CreateCommand("PCK_PORTALWEB.PROC_RETORNA_INFO_CLIENTE", CommandType.StoredProcedure);

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = Convert.ToInt32(clientCode) });
            if (string.IsNullOrEmpty(assessors))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTASSESSOR", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTASSESSOR", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = assessors });

            if (userId.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = userId.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSOR", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            var dr = cmd.ExecuteReader();

            if (dr.Read())
            {
                obj = new CPClientInformation();

                if (!(dr["DV_CLIENTE"] is DBNull))
                    clientCode = string.Format("{0}-{1}", clientCode, Convert.ToString(dr["DV_CLIENTE"]));

                string name = string.Empty;
                if (!(dr["NM_CLIENTE"] is DBNull))
                    name = Convert.ToString(dr["NM_CLIENTE"]);

                obj.Client = new CommonType { Value = clientCode, Description = name };

                if (!(dr["CD_CPFCGC"] is DBNull))
                    obj.CPFCNPJ = Convert.ToString(dr["CD_CPFCGC"]);

                if (!(dr["DT_NASC_FUND"] is DBNull))
                    obj.Birthdate = Convert.ToDateTime(dr["DT_NASC_FUND"]).ToShortDateString();

                if (!(dr["NM_ASSESSOR"] is DBNull))
                    obj.AssessorName = dr["NM_ASSESSOR"].ToString();
            }

            dr.Close();

            return obj;
        }

        private string GetClientPositionFromGrTrader(CPClientInformation client)
        {
            string xml = "";
            string login = "Portal.corretora";
            string password = "priscil@123";
            decimal clientCode = Convert.ToDecimal(client.Client.Value.Split('-')[0]);
            string cpfCnpj = client.CPFCNPJ.Trim().PadLeft(15, '0');
            string birthDate = client.Birthdate;

            oWebServiceGrTraderSoapTypeClient service = new oWebServiceGrTraderSoapTypeClient();
            var response = service.fPesquisaRapida(login, password, clientCode, cpfCnpj, birthDate);

            if (response != null && response.PesquisaRapida != null && response.PesquisaRapida.Informacoes != null && response.PesquisaRapida.Informacoes.Cadastro != null)
                response.PesquisaRapida.Informacoes.Cadastro.Validade = DateTime.Now.AddYears(1000).ToString("yyyy-MM-dd");

            xml = SerializeGrTraderObject(response);
            //string newXml = CleanXMLResult(xml);

            //newXml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + newXml;
            //newXml = newXml.Trim();
            return xml.Trim();
        }

        private string logExceptionConsolidatedPosition { get; set; }

        public ConsolidatedPosition LoadConsolidatedPosition(CPClientInformation client)
        {
            XmlDocument doc = new XmlDocument();

            if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["ConsolidatedPosition.UseMock"]))
            {
                var response = GetClientPositionFromGrTrader(client);

                if (!string.IsNullOrEmpty(response))
                {
                    if (response.Length > 0 && response[0] != '<')
                        response = response.Substring(1);

                    doc.LoadXml(response);
                }
            }
            else
            {
                /*** COMANDOS PARA FUNCIONAMENTO MOCK ***/
                doc.Load(System.Web.Hosting.HostingEnvironment.MapPath("~/XML/ConsolidatedPositionMock.xml"));
                /*** FIM - COMANDOS PARA FUNCIONAMENTO MOCK ***/
            }

            XmlElement root = doc.DocumentElement;

            ConsolidatedPosition position = new ConsolidatedPosition();

            #region ClientInformation - Informações do Cliente

            XmlNodeList nodes = doc.SelectNodes("//PesquisaRapida");
            if (nodes != null && nodes.Count > 0)
            {
                foreach (XmlNode node in nodes)
                {
                    client.AssessorName = FormatToString(node["AssessorNome"]);
                    client.PositionDate = FormatToShortDate(node["DataBase"]);
                    client.PositionHour = FormatToString(node["HoraBase"]);
                    client.Email = FormatToString(node["Email"]);
                    client.ClientName = FormatToString(node["ClienteNome"]);
                    client.Age = FormatToString(node["Idade"]);
                    client.Occupation = FormatToString(node["Profissao"]);
                    client.Suitability = FormatToString(node["Suitability"]);
                    client.NetWorth = FormatToDecimal(node["PatrimonioLiquido"]); // TODO: tag ainda não consta no xml do cliente, criada para fins de desenvolvimento
                    client.CPFCNPJ = FormatToString(node["CPF_CNPJ"]); // TODO: tag ainda não consta no xml do cliente, criada para fins de desenvolvimento
                    client.Birthdate = FormatToString(node["Birthdate"]); // TODO: tag ainda não consta no xml do cliente, criada para fins de desenvolvimento
                    client.TradingSince = FormatToString(node["OperaDesde"]);
                    client.Suitability = FormatToString(node["Suitability"]);
                    client.SuitabilityProfile = FormatToString(node["SuitabilityPerfil"]); // TODO: tag ainda não consta no xml do cliente, criada para fins de desenvolvimento
                    client.ReturnBrokeragePercentage = FormatToDecimal(node["ReturnBrokeragePercentage"]); // TODO: tag ainda não consta no xml do cliente, criada para fins de desenvolvimento
                    client.ClientDigit = FormatToString(node["ClienteDigito"]);
                    client.ClientCode = Convert.ToInt32(FormatToInt64(node["ClienteCodigo"]));
                    client.AssessorCode = FormatToString(node["AssessorCodigo"]);
                }
            }

            nodes = doc.SelectNodes("//Informacoes/Bovespa");
            if (nodes != null)
            {
                if (client.MarketInfo == null)
                    client.MarketInfo = new List<CPMarketInfo>();

                foreach (XmlNode node in nodes)
                {
                    client.MarketInfo.Add(
                        new CPMarketInfo()
                        {
                            MarketName = "Bovespa",
                            MarketType = FormatToString(node["Tipo"]),
                            Counterparty = FormatToString(node["ContraParte"]),
                            Qualified = FormatToString(node["Qualificado"])
                        });
                }
            }

            nodes = doc.SelectNodes("//Informacoes/BMF");
            if (nodes != null)
            {
                if (client.MarketInfo == null)
                    client.MarketInfo = new List<CPMarketInfo>();

                foreach (XmlNode node in nodes)
                {
                    client.MarketInfo.Add(
                            new CPMarketInfo()
                            {
                                MarketName = "BM&F",
                                MarketType = FormatToString(node["Tipo"]),
                                Counterparty = FormatToString(node["ContraParte"]),
                                Qualified = FormatToString(node["BmfMcSecundario"])
                            });
                }
            }

            nodes = doc.SelectNodes("//DadosContaCliente");
            if (nodes != null && nodes.Count > 0)
            {
                Address clientAddress = new Address();
                foreach (XmlNode node in nodes)
                {
                    clientAddress.AddressLine1 = FormatToString(node["Logradouro"]); // TODO: tag ainda não consta no xml do cliente, criada para fins de desenvolvimento
                    clientAddress.AddressLine2 = FormatToString(node["Complemento"]); // TODO: tag ainda não consta no xml do cliente, criada para fins de desenvolvimento
                    clientAddress.Cell = FormatToString(node["Celular"]); // TODO: tag ainda não consta no xml do cliente, criada para fins de desenvolvimento
                    clientAddress.City = FormatToString(node["Cidade"]); // TODO: tag ainda não consta no xml do cliente, criada para fins de desenvolvimento
                    clientAddress.Fax = FormatToString(node["Fax"]); // TODO: tag ainda não consta no xml do cliente, criada para fins de desenvolvimento
                    clientAddress.Number = FormatToString(node["Numero"]); // TODO: tag ainda não consta no xml do cliente, criada para fins de desenvolvimento
                    clientAddress.Phone = FormatToString(node["Fone"]); // TODO: tag ainda não consta no xml do cliente, criada para fins de desenvolvimento
                    clientAddress.PostalCode = FormatToString(node["CEP"]); // TODO: tag ainda não consta no xml do cliente, criada para fins de desenvolvimento
                    clientAddress.Quarter = FormatToString(node["Bairro"]); // TODO: tag ainda não consta no xml do cliente, criada para fins de desenvolvimento
                    clientAddress.StateCountry = FormatToString(node["Estado_Pais"]); // TODO: tag ainda não consta no xml do cliente, criada para fins de desenvolvimento
                }
                client.Address = clientAddress;
            }

            nodes = doc.SelectNodes("//ContasBancarias/Contas/resultadoContas");
            if (nodes.Count > 0)
            {
                List<BankAccount> lst = new List<BankAccount>();
                foreach (XmlNode node in nodes)
                {
                    BankAccount obj = new BankAccount();
                    obj.Account = FormatToString(node["CdConta"]); // TODO: tag ainda não consta no xml do cliente, criada para fins de desenvolvimento
                    obj.AccountDigit = FormatToString(node["DvConta"]); // TODO: tag ainda não consta no xml do cliente, criada para fins de desenvolvimento
                    obj.AgencyCode = FormatToString(node["CdAgencia"]); // TODO: tag ainda não consta no xml do cliente, criada para fins de desenvolvimento
                    obj.AgencyDigit = FormatToString(node["DvAgencia"]); // TODO: tag ainda não consta no xml do cliente, criada para fins de desenvolvimento
                    obj.BankCode = FormatToString(node["Banco"]); // TODO: tag ainda não consta no xml do cliente, criada para fins de desenvolvimento
                    obj.IsInactive = FormatToString(node["Inativo"]) == "N"; // TODO: tag ainda não consta no xml do cliente, criada para fins de desenvolvimento
                    obj.IsJointAccount = FormatToString(node["InConjunta"]) == "S"; // TODO: tag ainda não consta no xml do cliente, criada para fins de desenvolvimento
                    obj.IsPrincipal = FormatToString(node["Principal"]) == "S"; // TODO: tag ainda não consta no xml do cliente, criada para fins de desenvolvimento
                    //obj.JointAccountName = FormatToString(node["NmConjunta"]);
                    obj.Type = FormatToString(node["Tipo"]); // TODO: tag ainda não consta no xml do cliente, criada para fins de desenvolvimento
                    lst.Add(obj);
                }

                client.BankAccounts = lst;
            }

            nodes = doc.SelectNodes("//PessoasAutorizadasEmitorOrdem/Pessoas/resultadoPessoas");
            if (nodes.Count > 0)
            {
                List<OrdersIssuer> lst = new List<OrdersIssuer>();
                foreach (XmlNode node in nodes)
                {
                    OrdersIssuer obj = new OrdersIssuer();
                    obj.Name = FormatToString(node["Eminente"]);
                    obj.Market = FormatToString(node["Sistema"]);
                    obj.Type = FormatToString(node["Tipo"]);
                    lst.Add(obj);
                }

                client.OrdersIssuers = lst;
            }

            position.ClientInformation = client;

            #endregion

            #region TotalStock - Ações Total

            nodes = doc.SelectNodes("//PosicaoAcoesQuantitativas/resultadoPosicaoAcoesQuantitativas");

            if (nodes.Count > 0) {
                if (position.TotalStock == null)
                    position.TotalStock = new CPTotalStockModel();

                List<CPTotalStock> lst = new List<CPTotalStock>();
                foreach (XmlNode node in nodes) {
                    if (FormatToString(node["sPosicaoAcoesQuantitativas_TIPO_MERC"]).Equals("VIS")) {
                        CPTotalStock obj = new CPTotalStock();
                        obj.Stock = FormatToString(node["sPosicaoAcoesQuantitativas_COD_NEG"]);
                        obj.Portfolio = FormatToString(node["nPosicaoAcoesQuantitativas_COD_CART"]);
                        obj.AvailableQuantity = FormatToInt64(node["nPosicaoAcoesQuantitativas_QTDE_DISP"]);
                        obj.PendingQuantity = FormatToInt64(node["nPosicaoAcoesQuantitativas_QTDE_DA1"]);
                        obj.BlockedQuantity = FormatToInt64(node["nPosicaoAcoesQuantitativas_QTDE_DA2"]);
                        obj.SettleInD0 = FormatToInt64(node["nPosicaoAcoesQuantitativas_QTDE_DA3"]);
                        obj.Distribution = FormatToString(node["sPosicaoAcoesQuantitativas_NUM_DIST"]);
                        obj.BalanceQuantity = FormatToInt64(node["nPosicaoAcoesQuantitativas_SQ"]);
                        obj.CurrentPrice = FormatToDecimal(node["nPosicaoAcoesQuantitativas_PRECO"]);
                        obj.CurrentValue = FormatToDecimal(node["nPosicaoAcoesQuantitativas_VT"]);
                        obj.BlockedD1 = FormatToInt64(node["nPosicaoAcoesQuantitativas_QTDE_BLQD"]);
                        obj.BlockedD2 = FormatToInt64(node["nPosicaoAcoesQuantitativas_QTDE_PEND"]);
                        obj.BlockedD3 = FormatToInt64(node["nPosicaoAcoesQuantitativas_QTDE_ALQD"]);
                        obj.VACQuantity = FormatToInt64(node["nPosicaoAcoesQuantitativas_QTDE_AEXE"]);
                        obj.VACValue = FormatToDecimal(node["nPosicaoAcoesQuantitativas_VALR_AEXE"]); // TODO: Validar campo
                        obj.DiscountPercentage = FormatToDecimal(node["nPosicaoAcoesQuantitativas_PC_DESAGIO"]);
                        obj.DiscountValue = FormatToDecimal(node["nPosicaoAcoesQuantitativas_VR_DESAGIO"]);

                        lst.Add(obj);
                    }
                }

                position.TotalStock.TotalStockList = lst;
            }
            else {

                if (position.TotalStock == null)
                    position.TotalStock = new CPTotalStockModel();

            }

            #endregion

            #region OpeningStock - PregaoAtual

            nodes = doc.SelectNodes("//OperacoesPregaoAtual/resultadoOperacoesPregaoAtual");

            if (nodes.Count > 0)
            {
                if (position.TotalStock == null)
                    position.TotalStock = new CPTotalStockModel();

                List<CPOpeningStock> lstOpening = new List<CPOpeningStock>();

                foreach (XmlNode node in nodes)
                {
                    CPOpeningStock obj = new CPOpeningStock();
                    obj.Company = FormatToString(node["sOperacoesPregaoAtual_CD_NEGOCIO"]);
                    obj.OperationType = FormatToString(node["sOperacoesPregaoAtual_CD_NATOPE"]);
                    obj.MarketType = FormatToString(node["sOperacoesPregaoAtual_CD_MERCAD"]);
                    obj.Quantity = FormatToInt64(node["nOperacoesPregaoAtual_QT_NEGOCIO"]);
                    obj.Price = FormatToDecimal(node["nOperacoesPregaoAtual_vVALOR_OPERACAO_ORIGINAL"]);
                    obj.NegotiationValue = FormatToDecimal(node["nOperacoesPregaoAtual_VL_NEGOCIO"]);
                    obj.CurrentPrice = FormatToDecimal(node["nOperacoesPregaoAtual_VL_PRECOATUAL"]);
                    obj.ProfitGainLossValue = FormatToDecimal(node["nOperacoesPregaoAtual_vVALOR_LP"]);
                    obj.ProfitGainLossPercentual = FormatToDecimal(node["nOperacoesPregaoAtual_vPC_LP"]);
                    obj.Gate = FormatToString(node["sOperacoesPregaoAtual_CD_OPERADOR"]);
                    obj.Operator = FormatToString(node["nOperacoesPregaoAtual_CD_ASSESSOR"]);

                    lstOpening.Add(obj);
                }

                position.TotalStock.OpeningPositionList = lstOpening;
            }

            #endregion

            #region RentDonor - Aluguel Doador

            nodes = doc.SelectNodes("//BtcDoador/resultadoBtcDoador");

            if (nodes.Count > 0)
            {
                List<CPRentDonor> lst = new List<CPRentDonor>();
                foreach (XmlNode node in nodes)
                {
                    CPRentDonor obj = new CPRentDonor();
                    obj.ISIN = FormatToString(node["sBtcDoador_COD_ISIN"]);
                    obj.Portfolio = FormatToString(node["nBtcDoador_COD_CART"]);
                    obj.OpeningDate = FormatToShortDate(node["sBtcDoador_DATA_ABER"]);
                    obj.Quantity = FormatToInt64(node["nBtcDoador_QTDE_ACOE_ORIG"]);
                    obj.DueDate = FormatToShortDate(node["sBtcDoador_DATA_VENC"]);
                    obj.AveragePrice = FormatToDecimal(node["nBtcDoador_PREC_MED"]);
                    obj.RemunerationRate = FormatToDecimal(node["nBtcDoador_TAXA_REMU"]);
                    obj.CommisionRate = FormatToDecimal(node["nBtcDoador_TAXA_COMI"]);
                    obj.NetValue = FormatToDecimal(node["nBtcDoador_VAL_LIQ"]);
                    obj.CurrentPrice = FormatToDecimal(node["nBtcDoador_Preco"]);
                    obj.CurrentValue = FormatToDecimal(node["nBtcDoador_vValorCheio"]);
                    obj.Origin = FormatToShortDate(node["sBtcDoador_DATA_ORI"]);
                    obj.LiquidationGraceDate = FormatToShortDate(node["sBtcDoador_DATA_CARENCIA_LQDO"]);
                    obj.Company = FormatToString(node["sBtcDoador_COD_NEG"]);

                    lst.Add(obj);
                }

                position.RentDonor = lst;
            }

            #endregion

            #region RentTaker - Aluguel Tomador

            nodes = doc.SelectNodes("//BtcTomador/resultadoBtcTomador");

            if (nodes.Count > 0)
            {
                List<CPRentTaker> lst = new List<CPRentTaker>();
                foreach (XmlNode node in nodes)
                {
                    CPRentTaker obj = new CPRentTaker();
                    obj.ISIN = FormatToString(node["sBtcTomador_COD_ISIN"]);
                    obj.Portfolio = FormatToString(node["nBtcTomador_COD_CART"]);
                    obj.OpeningDate = FormatToShortDate(node["sBtcTomador_DATA_ABER"]);
                    obj.Quantity = FormatToInt64(node["sBtcTomador_QTDE_ACOE"]);
                    obj.DueDate = FormatToShortDate(node["sBtcTomador_DATA_VENC"]);
                    obj.AveragePrice = FormatToDecimal(node["nBtcTomador_PREC_MED"]);
                    obj.RemunerationRate = FormatToDecimal(node["nBtcTomador_TAXA_REMU"]);
                    obj.CommisionRate = FormatToDecimal(node["nBtcTomador_TAXA_COMI"]);
                    obj.NetValue = FormatToDecimal(node["nBtcTomador_VAL_LIQ"]);
                    obj.CurrentPrice = FormatToDecimal(node["nBtcTomador_Preco"]);
                    obj.CurrentValue = FormatToDecimal(node["nBtcTomador_vValorCheio"]);
                    obj.Origin = FormatToShortDate(node["sBtcTomador_DATA_ORI"]);
                    obj.LiquidationGraceDate = FormatToShortDate(node["sBtcTomador_DATA_CARN_LIQD"]);
                    obj.Company = FormatToString(node["sBtcTomador_COD_AGCO"]);

                    lst.Add(obj);
                }

                position.RentTaker = lst;
            }

            #endregion

            #region BMF Abertura

            nodes = doc.SelectNodes("//PosicaoAberturaBMF/resultadoPosicaoAberturaBMF");

            if (nodes.Count > 0)
            {
                if (position.BMF == null)
                    position.BMF = new CPBMFTotal();

                List<CPBMFOpening> lstOpen = new List<CPBMFOpening>();
                foreach (XmlNode node in nodes)
                {
                    CPBMFOpening obj = new CPBMFOpening();
                    obj.Commodity = FormatToString(node["sPosicaoAberturaBMF_CD_COMMOD"]);
                    obj.Quantity = FormatToInt64(node["nPosicaoAberturaBMF_QT_DIA_ATU"]);
                    obj.Closing = FormatToDecimal(node["nPosicaoAberturaBMF_PR_FECHAMENTO"]);
                    obj.Current = FormatToDecimal(node["nPosicaoAberturaBMF_PR_MEDIO_ATU"]);
                    obj.ProfitLoss = FormatToDecimal(node["nPosicaoAberturaBMF_VR_LP"]);
                    obj.ProfitLossPercentage = FormatToDecimal(node["nPosicaoAberturaBMF_PC_LP"]);

                    lstOpen.Add(obj);
                }

                position.BMF.BMFOpenList = lstOpen;
            }

            #endregion

            #region BMF Pregão Atual

            nodes = doc.SelectNodes("//PregaoBMF/resultadoPregaoBMF");

            if (nodes.Count > 0)
            {
                if (position.BMF == null) {
                    position.BMF = new CPBMFTotal();
                    position.BMF.BMFCurrentList = new List<CPBMFCurrent>();
                }

                List<CPBMFCurrent> lstCurrent = new List<CPBMFCurrent>();
                foreach (XmlNode node in nodes)
                {
                    CPBMFCurrent obj = new CPBMFCurrent();
                    obj.Commodity = FormatToString(node["sPregaoBMF_CD_COMMOD"]);
                    obj.TradeNumber = FormatToString(node["sPregaoBMF_NR_NEGOCIO"]);
                    obj.TradeHour = FormatToString(node["sPregaoBMF_HR_NEGOCIO"]);
                    obj.OperationType = FormatToString(node["sPregaoBMF_CD_NATOPE"]);
                    obj.AfterMarket = FormatToString(node["sPregaoBMF_IN_AFTER"]);
                    obj.Quantity = FormatToInt64(node["nPregaoBMF_QT_QTDESP"]);
                    obj.Price = FormatToDecimal(node["nPregaoBMF_PR_NEGOCIO"]);
                    obj.TotalPrice = FormatToDecimal(node["nPregaoBMF_PR_NEGOCIO"]); // TODO: Validar campo repetido
                    obj.CurrentPrice = FormatToDecimal(node["nPregaoBMF_VR_PRECO_ATUAL"]);
                    obj.ProfitLoss = FormatToDecimal(node["nPregaoBMF_VR_LP"]);
                    obj.ProfitLossPercentage = FormatToDecimal(node["nPregaoBMF_PC_LP"]);
                    obj.Type = FormatToString(node["sPregaoBMF_TP_REGISTRO"]);

                    lstCurrent.Add(obj);
                }

                position.BMF.BMFCurrentList = lstCurrent;
            }

            #endregion

            #region BMF Total

            nodes = doc.SelectNodes("//TotalPosicoaPregaoBMF/resultadoTotalPosicoaPregaoBMF"); // TODO: Verificar "Posicoa"

            if (nodes.Count > 0)
            {
                if (position.BMF == null) {
                    position.BMF = new CPBMFTotal();
                    position.BMF.BMFConsolidated = new List<CPBMFConsolidated>();
                }

                List<CPBMFConsolidated> lst = new List<CPBMFConsolidated>();
                foreach (XmlNode node in nodes)
                {
                    CPBMFConsolidated obj = new CPBMFConsolidated();
                    obj.Commodity = FormatToString(node["sMercadoria"]);
                    obj.Quantity = FormatToInt64(node["nQuantidade"]);
                    obj.ProfitLoss = FormatToDecimal(node["nLucroPrejuizo"]);

                    lst.Add(obj);
                }

                position.BMF.BMFConsolidated = lst;
            }

            #endregion

            #region CDBFinal - CDB Final

            nodes = doc.SelectNodes("//CDBFinal/resultadoCDBFinal");

            if (nodes.Count > 0)
            {
                List<CPCDBFinal> lst = new List<CPCDBFinal>();
                foreach (XmlNode node in nodes)
                {
                    CPCDBFinal obj = new CPCDBFinal();

                    obj.Title = FormatToString(node["sCDBFinal_TITULO"]);
                    obj.IssueDate = FormatToShortDate(node["sCDBFinal_EMISSAO"]);
                    obj.DueDate = FormatToShortDate(node["sCDBFinal_VENCIMENTO"]);
                    obj.Ratio = FormatToDecimal(node["sCDBFinal_TAXA"]);
                    obj.OperationDate = FormatToShortDate(node["sCDBFinal_DT_OPERACAO"]);
                    obj.RedemptionDate = FormatToShortDate(node["sCDBFinal_DT_RETORNO"]);
                    obj.Quantity = FormatToInt64(node["sCDBFinal_QTDE"]);
                    obj.GrossValue = FormatToDecimal(node["sCDBFinal_VR_BRUTO"]);
                    obj.IRIOF = FormatToDecimal(node["sCDBFinal_IF_IOF"]);
                    obj.NetValue = FormatToDecimal(node["sCDBFinal_VR_LIQ"]);

                    lst.Add(obj);
                }

                position.CDBFinal = lst;
            }

            #endregion

            #region CommitedCDB - Compromissada em CDB

            nodes = doc.SelectNodes("//CompromissadasCDB/resultadoCompromissadasCDB");

            if (nodes.Count > 0)
            {
                List<CPCommitedCDB> lst = new List<CPCommitedCDB>();
                foreach (XmlNode node in nodes)
                {
                    CPCommitedCDB obj = new CPCommitedCDB();
                    obj.Title = FormatToString(node["sCompromissadasCDB_TITULO"]);
                    obj.IssueDate = FormatToShortDate(node["sCompromissadasCDB_EMISSAO"]);
                    obj.DueDate = FormatToShortDate(node["sCompromissadasCDB_VENCIMENTO"]);
                    obj.Ratio = FormatToDecimal(node["sCompromissadasCDB_TAXA"]);
                    obj.OperationDate = FormatToShortDate(node["sCompromissadasCDB_DT_OPERACAO"]);
                    obj.RedemptionDate = FormatToShortDate(node["sCompromissadasCDB_DT_RETORNO"]);
                    obj.Quantity = FormatToInt64(node["sCompromissadasCDB_QTDE"]);
                    obj.GrossValue = FormatToDecimal(node["sCompromissadasCDB_VR_BRUTO"]);
                    obj.IRIOF = FormatToDecimal(node["sCompromissadasCDB_IF_IOF"]); //TODO: Validar campo
                    obj.NetValue = FormatToDecimal(node["sCompromissadasCDB_VR_LIQ"]);

                    lst.Add(obj);
                }

                position.CommitedCDB = lst;
            }

            #endregion

            #region Clubs - Clubes/Fundos

            nodes = doc.SelectNodes("//ClubesFundos/resultadoClubesFundos");

            if (nodes.Count > 0)
            {
                List<CPClubs> lst = new List<CPClubs>();
                foreach (XmlNode node in nodes)
                {
                    CPClubs obj = new CPClubs();

                    obj.ClubFund = FormatToString(node["sClubesFundos_FUNDO_CLUBE"]);
                    obj.ApplicationDate = FormatToShortDate(node["sClubesFundos_DT_APLICACAO"]);
                    obj.QuotaQuantity = FormatToInt64(node["sClubesFundos_QTDE_COTA"]);
                    obj.QuotaValue = FormatToDecimal(node["sClubesFundos_VR_COTA"]);
                    obj.GrossValue = FormatToDecimal(node["sClubesFundos_VR_BRUTO"]);
                    obj.IR = FormatToDecimal(node["sClubesFundos_IR"]);
                    obj.IOF = FormatToDecimal(node["sClubesFundos_IOF"]);
                    obj.NetValue = FormatToDecimal(node["sClubesFundos_VR_LIQUID"]);

                    lst.Add(obj);
                }

                position.Clubs = lst;
            }

            #endregion

            #region BMFGuarantees - Garantias BM&F

            nodes = doc.SelectNodes("//TotalGarantiasBMF/resultadoTotalGarantiasBMF");

            if (nodes.Count > 0)
            {
                if (position.BMFGuarantees == null)
                    position.BMFGuarantees = new CPBMFGuarantees();

                if (position.BMFGuarantees.OpeningPosition == null)
                    position.BMFGuarantees.OpeningPosition = new List<CPBMFGuaranteesOpening>();

                List<CPBMFGuaranteesOpening> lst = new List<CPBMFGuaranteesOpening>();
                foreach (XmlNode node in nodes)
                {
                    CPBMFGuaranteesOpening obj = new CPBMFGuaranteesOpening();

                    obj.TotalGuarantees = FormatToDecimal(node["nTotalGarantiasBMF_VL_TOTGAR"]);
                    obj.RequiredMargin = FormatToDecimal(node["nTotalGarantiasBMF_VL_MAREXCED"]);
                    obj.OwnGuarantees = FormatToDecimal(node["nTotalGarantiasBMF_VL_FIANCA_PROPRIA"]);
                    obj.ExternalGuarantees = FormatToDecimal(node["nTotalGarantiasBMF_VL_FIANCA_EXTERNA"]);
                    obj.Money = FormatToDecimal(node["nTotalGarantiasBMF_VL_DINHEIRO"]);
                    obj.Assets = FormatToDecimal(node["nTotalGarantiasBMF_VL_ATIVOS"]);
                    obj.Custody = FormatToDecimal(node["nTotalGarantiasBMF_VL_CUSTOD"]);
                    obj.Stocks = FormatToDecimal(node["nTotalGarantiasBMF_VL_ATIVOS"]); // TODO: Validar campo repetido

                    lst.Add(obj);
                }

                position.BMFGuarantees.OpeningPosition = lst;
            }

            nodes = doc.SelectNodes("//ComposicaoGarantiasBMF/resultadoComposicaoGarantiasBMF");

            if (nodes.Count > 0)
            {
                if (position.BMFGuarantees == null)
                    position.BMFGuarantees = new CPBMFGuarantees();

                if (position.BMFGuarantees.GuaranteesComposition == null)
                    position.BMFGuarantees.GuaranteesComposition = new List<CPBMFGuaranteesComposition>();

                List<CPBMFGuaranteesComposition> lst = new List<CPBMFGuaranteesComposition>();
                foreach (XmlNode node in nodes)
                {
                    CPBMFGuaranteesComposition obj = new CPBMFGuaranteesComposition();

                    obj.Stock = FormatToString(node["sComposicaoGarantiasBMF_CD_NEGOCIO"]);
                    obj.StockNumber = FormatToInt64(node["nComposicaoGarantiasBMF_NR_ATIVO"]);
                    obj.DueDate = FormatToShortDate(node["nComposicaoGarantiasBMF_DT_VENC"]);
                    obj.Quantity = FormatToInt64(node["nComposicaoGarantiasBMF_QT_ATIVO"]);
                    obj.InstitutionIssuer = FormatToString(node["sComposicaoGarantiasBMF_NM_EMITENTE"]);
                    obj.Value = FormatToDecimal(node["nComposicaoGarantiasBMF_VL_GARANTIA"]);
                    obj.Umbrella = FormatToString(node["sComposicaoGarantiasBMF_FIANCA_GUARDA_CHUVA"]);

                    lst.Add(obj);
                }

                position.BMFGuarantees.GuaranteesComposition = lst;
            }

            #endregion

            #region BovespaGuarantees - Bovespa (Abertura Pregão)

            nodes = doc.SelectNodes("//DescricaoGarantias/resultadoDescricaoGarantias");

            if (nodes.Count > 0)
            {
                List<CPBovespaGuarantees> lst = new List<CPBovespaGuarantees>();
                foreach (XmlNode node in nodes)
                {
                    CPBovespaGuarantees obj = new CPBovespaGuarantees();
                    obj.Stock = FormatToString(node["sDescricaoGarantias_COD_ATIV"]);
                    obj.GuaranteeValue = FormatToDecimal(node["nDescricaoGarantias_VAL_GARN_DEPO"]);
                    obj.Quantity = FormatToInt64(node["nDescricaoGarantias_QTDE_GARN"]);
                    obj.DepositDate = FormatToShortDate(node["sDescricaoGarantias_DATA_DEPO"]);
                    obj.Bank = string.Format("{0}-{1}", FormatToString(node["sDescricaoGarantias_COD_BANC"]), FormatToString(node["sDescricaoGarantias_NOME_BANC"]));
                    obj.DueDate = FormatToShortDate(node["sDescricaoGarantias_DATA_VENC"]);
                    obj.CurrentPrice = FormatToDecimal(node["sDescricaoGarantias_Patual"]);
                    obj.TotalValue = FormatToDecimal(node["nDescricaoGarantias_vnValorCarteira"]);
                    obj.Origin = FormatToString(node["sDescricaoGarantias_DESC_ORIG_SALD"]);
                    obj.Umbrella = FormatToString(node["nDescricaoGarantias_GUARDACHUVA"]);

                    lst.Add(obj);
                }

                position.BovespaGuarantees = lst;
            }

            #endregion

            #region Option - Opção

            nodes = doc.SelectNodes("//PosicaoAcoesQuantitativas/resultadoPosicaoAcoesQuantitativas");

            if (nodes.Count > 0) {
                List<CPOption> lst = new List<CPOption>();
                foreach (XmlNode node in nodes) {
                    if (FormatToString(node["sPosicaoAcoesQuantitativas_TIPO_MERC"]).Equals("OPC")) {
                        CPOption obj = new CPOption();
                        obj.Stock = FormatToString(node["sPosicaoAcoesQuantitativas_COD_NEG"]);
                        obj.Distribution = FormatToString(node["sPosicaoAcoesQuantitativas_NUM_DIST"]);
                        obj.Portfolio = FormatToString(node["nPosicaoAcoesQuantitativas_COD_CART"]);
                        obj.AvailableQuantity = FormatToInt64(node["nPosicaoAcoesQuantitativas_QTDE_DISP"]);
                        obj.PendingQuantity = FormatToInt64(node["nPosicaoAcoesQuantitativas_QTDE_DA1"]);
                        obj.BlockedQuantity = FormatToInt64(node["nPosicaoAcoesQuantitativas_QTDE_DA2"]);
                        obj.SettleInD0 = FormatToInt64(node["nPosicaoAcoesQuantitativas_QTDE_DA3"]);
                        obj.BlockedD1 = FormatToInt64(node["nPosicaoAcoesQuantitativas_QTDE_BLQD"]);
                        obj.BlockedD2 = FormatToInt64(node["nPosicaoAcoesQuantitativas_QTDE_PEND"]);
                        obj.BlockedD3 = FormatToInt64(node["nPosicaoAcoesQuantitativas_QTDE_ALQD"]);
                        obj.BalanceQuantity = FormatToInt64(node["nPosicaoAcoesQuantitativas_SQ"]);
                        obj.CurrentPrice = FormatToDecimal(node["nPosicaoAcoesQuantitativas_PRECO"]);
                        obj.CurrentValue = FormatToDecimal(node["nPosicaoAcoesQuantitativas_VT"]);
                        obj.DiscountPercentage = FormatToDecimal(node["nPosicaoAcoesQuantitativas_PC_DESAGIO"]);
                        obj.DiscountValue = FormatToDecimal(node["nPosicaoAcoesQuantitativas_VR_DESAGIO"]);
                        obj.DueDate = FormatToString(node["sPosicaoAcoesQuantitativas_DATA_VENC"]);
                        obj.VACQuantity = FormatToInt64(node["nPosicaoAcoesQuantitativas_QTDE_AEXE"]);
                        obj.VACValue = FormatToDecimal(node["nPosicaoAcoesQuantitativas_VALR_AEXE"]); // TODO: Validar campo

                        lst.Add(obj);
                    }
                }

                position.Option = lst;
            }
            else {
                position.Option = new List<CPOption>();
            }

            #endregion

            #region Gold - Ouro

            nodes = doc.SelectNodes("//Ouro/resultadoOuro");

            if (nodes.Count > 0)
            {
                List<CPGold> lst = new List<CPGold>();
                foreach (XmlNode node in nodes)
                {
                    CPGold obj = new CPGold();

                    obj.Quantity = FormatToDecimal(node["sOuro_QTDE_GRAMAS"]);
                    obj.DefaultQuantity = FormatToDecimal(node["sOuro_QTDE_PADRAO"]);
                    obj.CustodyValue = FormatToDecimal(node["sOuro_VR_CUSTODIA"]);
                    obj.ProvisionCustodyRatio = FormatToDecimal(node["sOuro_PROV_CUSTODIA"]);
                    obj.ProvisionLastMonth = FormatToDecimal(node["sOuro_PROV_ANT"]);

                    lst.Add(obj);
                }

                position.Gold = lst;
            }

            #endregion

            #region Terms - Termos (Lucro / Prejuízo)

            nodes = doc.SelectNodes("//PosicaoAcoesQuantitativas/resultadoPosicaoAcoesQuantitativas");

            if (nodes.Count > 0)
            {
                if (position.Terms == null)
                    position.Terms = new CPTerms();

                List<CPTerm> lst = new List<CPTerm>();
                foreach (XmlNode node in nodes)
                {
                    if (FormatToString(node["sPosicaoAcoesQuantitativas_TIPO_MERC"]).Equals("TER"))
                    {
                        CPTerm obj = new CPTerm();
                        obj.Stock = FormatToString(node["sPosicaoAcoesQuantitativas_COD_NEG"]);
                        obj.Portfolio = FormatToString(node["nPosicaoAcoesQuantitativas_COD_CART"]);
                        obj.Distribution = FormatToString(node["sPosicaoAcoesQuantitativas_NUM_DIST"]);
                        obj.AvailableQuantity = FormatToInt64(node["nPosicaoAcoesQuantitativas_QTDE_DISP"]);
                        obj.PendingQuantity = FormatToInt64(node["nPosicaoAcoesQuantitativas_QTDE_DA1"]);
                        obj.BlockedQuantity = FormatToInt64(node["nPosicaoAcoesQuantitativas_QTDE_DA2"]);
                        obj.SettleInD0 = FormatToInt64(node["nPosicaoAcoesQuantitativas_QTDE_DA3"]);
                        obj.BlockedD1 = FormatToInt64(node["nPosicaoAcoesQuantitativas_QTDE_BLQD"]);
                        obj.BlockedD2 = FormatToInt64(node["nPosicaoAcoesQuantitativas_QTDE_PEND"]);
                        obj.BlockedD3 = FormatToInt64(node["nPosicaoAcoesQuantitativas_QTDE_ALQD"]);
                        obj.BalanceQuantity = FormatToInt64(node["nPosicaoAcoesQuantitativas_SQ"]);
                        obj.CurrentPrice = FormatToDecimal(node["nPosicaoAcoesQuantitativas_PRECO"]);
                        obj.CurrentValue = FormatToDecimal(node["nPosicaoAcoesQuantitativas_VT"]);
                        obj.DiscountPercentage = FormatToDecimal(node["nPosicaoAcoesQuantitativas_PC_DESAGIO"]);
                        obj.DiscountValue = FormatToDecimal(node["nPosicaoAcoesQuantitativas_VR_DESAGIO"]);
                        obj.DueDate = FormatToString(node["sPosicaoAcoesQuantitativas_DATA_VENC"]);
                        obj.VACQuantity = FormatToInt64(node["nPosicaoAcoesQuantitativas_QTDE_AEXE"]);
                        obj.VACValue = FormatToDecimal(node["nPosicaoAcoesQuantitativas_VALR_AEXE"]); // TODO: Validar campo

                        lst.Add(obj);
                    }
                }

                position.Terms.Terms = lst;
            }

            #endregion

            #region RegisteredTerms - Termos Registrados

            nodes = doc.SelectNodes("//TermosRegistrados/resultadoTermosRegistrados");

            if (nodes.Count > 0)
            {
                //var hasRegTerms = root.SelectSingleNode("//TermosRegistrados/resultadoTermosRegistrados/sTermosRegistrados_DATA_VENC_TERM") != null;

                //if (hasRegTerms)
                //{
                if (position.Terms == null)
                    position.Terms = new CPTerms();

                List<CPRegisteredTerm> lst = new List<CPRegisteredTerm>();
                foreach (XmlNode node in nodes)
                {
                    CPRegisteredTerm obj = new CPRegisteredTerm();
                    obj.DueDate = FormatToShortDate(node["sTermosRegistrados_DATA_VENC_TERM"]);
                    obj.Stock = FormatToString(node["sTermosRegistrados_COD_NEG"]);
                    obj.OriginalQuantity = FormatToInt64(node["nTermosRegistrados_QTDE_PAP_ORIG"]);
                    obj.OriginalValue = FormatToDecimal(node["nTermosRegistrados_VAL_COTR"]);
                    obj.Contract = FormatToString(node["nTermosRegistrados_NUM_COTR"]);
                    obj.Opening = FormatToShortDate(node["sTermosRegistrados_DATA_NEGO"]);
                    obj.Operation = FormatToString(node["sTermosRegistrados_IND_POSI"]);
                    obj.CurrentPrice = FormatToDecimal(node["nTermosRegistrados_vnValorTermoOriginal"]);
                    obj.CurrentValue = FormatToDecimal(node["nTermosRegistrados_vnValorTermoAtual"]);
                    obj.ProfitLossValue = FormatToDecimal(node["nTermosRegistrados_vnValorLucroPrejuizo"]);
                    obj.ProfitLossPercentage = FormatToDecimal(node["nTermosRegistrados_vnPercentual"]);
                    obj.OriginalPrice = FormatToDecimal(node["nTermosRegistrados_VAL_NEGO"]);

                    lst.Add(obj);
                }
                position.Terms.RegisteredTerms = lst;
                //}
            }

            #endregion

            #region SettledTerms - Termos Liquidados

            nodes = doc.SelectNodes("//TermosLiquidados/resultadoTermosLiquidados");

            if (nodes.Count > 0)
            {
                List<CPSettledTerms> lst = new List<CPSettledTerms>();
                foreach (XmlNode node in nodes)
                {
                    CPSettledTerms obj = new CPSettledTerms();

                    obj.Stock = FormatToString(node["sTermosLiquidados_COD_NEG"]);
                    obj.Quantity = FormatToInt64(node["nTermosLiquidados_QTDE_PAP_DISP"]);
                    obj.SettledContractValue = FormatToDecimal(node["nTermosLiquidados_VAL_COTR"]);
                    obj.SettledContractReversed = FormatToDecimal(node["nTermosLiquidados_VAL_COTR"]); // TODO: Validar campo repetido
                    obj.BusinessNumber = FormatToInt64(node["nTermosLiquidados_NUM_NEGO"]);
                    obj.BusinessPrice = FormatToDecimal(node["nTermosLiquidados_VAL_NEGO"]);
                    obj.CV = FormatToString(node["sTermosLiquidados_IND_POSI"]);
                    obj.LiquidationType = FormatToString(node["sTermosLiquidados_TIPO_LIQD"]);
                    obj.LiquidationDate = FormatToShortDate(node["sTermosLiquidados_DATA_LIQD"]);
                    obj.ProfitLoss = FormatToDecimal(node["nTermosLiquidados_vnValorLucroPrejuizo"]);

                    lst.Add(obj);
                }

                position.SettledTerms = lst;
            }

            #endregion

            #region SettledTermsToday - Termos Liquidados Hoje

            nodes = doc.SelectNodes("//TermosLiquidadosHoje/resultadoTermosRegistradosHoje");

            if (nodes.Count > 0)
            {
                List<CPSettledTermsToday> lst = new List<CPSettledTermsToday>();
                foreach (XmlNode node in nodes)
                {
                    CPSettledTermsToday obj = new CPSettledTermsToday();

                    obj.Stock = FormatToString(node["sTermosLiquidadosHoje_COD_NEG"]);
                    obj.Quantity = FormatToInt64(node["nTermosLiquidadosHoje_QTDE_PAP_DISP"]);
                    obj.SettledContractValue = FormatToDecimal(node["nTermosLiquidadosHoje_VAL_COTR"]);
                    obj.SettledContractReversed = FormatToDecimal(node["nTermosLiquidadosHoje_VAL_COTR"]); // TODO: Validar campo repetido
                    obj.BusinessNumber = FormatToInt64(node["nTermosLiquidadosHoje_NUM_NEGO"]);
                    obj.BusinessPrice = FormatToDecimal(node["nTermosLiquidadosHoje_VAL_NEGO"]);
                    obj.CV = FormatToString(node["sTermosLiquidadosHoje_IND_POSI"]);
                    obj.LiquidationType = FormatToString(node["sTermosLiquidadosHoje_TIPO_LIQD"]);
                    obj.LiquidationDate = FormatToShortDate(node["sTermosLiquidadosHoje_DATA_LIQD"]);
                    obj.ProfitLoss = FormatToDecimal(node["nTermosLiquidadosHoje_vnValorLucroPrejuizo"]);

                    lst.Add(obj);
                }

                position.SettledTermsToday = lst;
            }

            #endregion

            #region PublicTitles - Títulos Públicos

            nodes = doc.SelectNodes("//TesouroDireto/resultadoTesouroDireto");

            if (nodes.Count > 0)
            {
                List<CPPublicTitles> lst = new List<CPPublicTitles>();
                foreach (XmlNode node in nodes)
                {
                    CPPublicTitles obj = new CPPublicTitles();

                    obj.Title = FormatToString(node["sTesouroDireto_TIPO_TITU"]);
                    obj.DueDate = FormatToShortDate(node["sTesouroDireto_DATA_VENC"]);
                    obj.Quantity = FormatToInt64(node["nTesouroDireto_QTDE_TITU"]);
                    obj.GrossValue = FormatToDecimal(node["nTesouroDireto_VAL_ORIG"]);
                    obj.NetValue = FormatToDecimal(node["nTesouroDireto_VAL_POSI"]);

                    lst.Add(obj);
                }

                position.PublicTitles = lst;
            }

            #endregion

            #region  TotalCheckingAccount - Total Conta Corrente

            nodes = doc.SelectNodes("//SaldoContasCorrentes/resultadoSaldoContasCorrentes");

            if (nodes.Count > 0)
            {
                // TODO: Acho que isso é uma lista porque no XML tem mais de um elemento.
                if (position.TotalCheckingAccount == null)
                    position.TotalCheckingAccount = new CPTotalCheckingAccount();

                position.TotalCheckingAccount.CheckingAccountBalance = new CPCheckingAccountBalance();
                position.TotalCheckingAccount.CheckingAccountBalance.AmountAvailable = FormatToDecimal(nodes[0]["nSaldoContasCorrentes_VL_DISPONIVEL"]);
                position.TotalCheckingAccount.CheckingAccountBalance.AmountProjected = FormatToDecimal(nodes[0]["nSaldoContasCorrentes_VL_PROJETADO"]);
                position.TotalCheckingAccount.CheckingAccountBalance.AmountTotal = FormatToDecimal(nodes[0]["nSaldoContasCorrentes_VL_TOTAL"]);
                position.TotalCheckingAccount.CheckingAccountBalance.DebtDaysMounth = FormatToInt64(nodes[0]["nSaldoContasCorrentes_QT_DIAS_DEV_DEC"]);
                position.TotalCheckingAccount.CheckingAccountBalance.DebtDaysTotal = FormatToInt64(nodes[0]["nSaldoContasCorrentes_QT_DIAS_DEV"]);
                position.TotalCheckingAccount.CheckingAccountBalance.Total = FormatToDecimal(nodes[0]["nSaldoContasCorrentes_VL_TOTAL_COMPREGAO"]);
                position.TotalCheckingAccount.CheckingAccountBalance.TradingProjected = FormatToDecimal(nodes[0]["nSaldoContasCorrentes_VL_PREGAO"]);
            }

            nodes = doc.SelectNodes("//ProjecoesContaCorrente/resultadoProjecoesContaCorrente");

            if (nodes.Count > 0)
            {
                // TODO: Acho que isso é uma lista porque no XML tem mais de um elemento.
                if (position.TotalCheckingAccount == null)
                    position.TotalCheckingAccount = new CPTotalCheckingAccount();

                position.TotalCheckingAccount.DescriptionProjected = new CPDescriptionProjected();
                position.TotalCheckingAccount.DescriptionProjected.ProjectedAmountCurrentTrading = FormatToDecimal(nodes[0]["nProjecoesContaCorrente_VL_PROJETADO"]);
                position.TotalCheckingAccount.DescriptionProjected.SettlementDate = FormatToShortDate(nodes[0]["sProjecoesContaCorrente_DT_LIQUIDACAO"]);
                position.TotalCheckingAccount.DescriptionProjected.AmountProjected = FormatToDecimal(nodes[0]["nProjecoesContaCorrente_VL_PROJETADO_POSICAO"]); // TODO: Validar campo
                position.TotalCheckingAccount.DescriptionProjected.Total = FormatToDecimal(nodes[0]["nProjecoesContaCorrente_VL_PROJETADO_TOTAL"]);
            }

            #endregion

            #region Resume - Totalizadores

            nodes = doc.SelectNodes("//TotaldoCliente");

            if (nodes != null && nodes.Count > 0)
            {
                List<CPResume> lst = new List<CPResume>();

                foreach (XmlNode node in nodes)
                {
                    LoadResumeItem(ref lst, "TotalStock", "Ações Total", node["nRT_Cliente_Normal_MercadoVistaLivre"], node["nRT_Cliente_Normal_MercadoVistaDemaisCarteira"], node["nRT_Cliente_Investimento_MercadoVistaLivre"], node["nRT_Cliente_Investimento_MercadoVistaDemaisCarteira"]); // TODO: tag ainda não consta no xml do cliente, criada para fins de desenvolvimento
                    LoadResumeItem(ref lst, "Option", "Opção", node["nRT_Cliente_Normal_PosicaoOpcaoCompra"], node["nRT_Cliente_Normal_PosicaoOpcaoVenda"], node["nRT_Cliente_Investimento_PosicaoOpcaoCompra"], node["nRT_Cliente_Investimento_PosicaoOpcaoVenda"]); // TODO: tag ainda não consta no xml do cliente, criada para fins de desenvolvimento
                    LoadResumeItem(ref lst, "Term", "Termo", node["nRT_Cliente_Normal_LpTermo"], node["nRT_Cliente_Investimento_LpTermo"]);
                    LoadResumeItem(ref lst, "SettledTerms", "Termos Liquidados", node["nRT_Cliente_Normal_LpTermoAntecipado"], node["nRT_Cliente_Investimento_LpTermoAntecipado"]);
                    LoadResumeItem(ref lst, "SettledTermsToday", "Termos Liquidados Hoje", node["nRT_Cliente_Normal_LpTermoProjetado"], node["nRT_Cliente_Investimento_LpTermoProjetado"]);
                    LoadResumeItem(ref lst, "RentDonor", "Aluguel Doador", node["nRT_Cliente_Normal_BTCDOADOR_Receita"], node["nRT_Cliente_Investimento_BTCDOADOR_Receita"]);
                    LoadResumeItem(ref lst, "RentTaker", "Aluguel Tomador", node["nRT_Cliente_Normal_BTCTOMADOR"], node["nRT_Cliente_Investimento_BTCTOMADOR"]);
                    LoadResumeItem(ref lst, "BovespaGuarantees", "Garantias Bovespa", node["nRT_Cliente_Normal_GarantiasACOES_SALDO"], node["nRT_Cliente_Investimento_GarantiasACOES_SALDO"]);
                    LoadResumeItem(ref lst, "BMFCurrent", "BM&F Total", node["nRT_Cliente_Normal_GarantiasBMF_Saldo"], node["nRT_Cliente_Investimento_GarantiasBMF_Saldo"]);
                    LoadResumeItem(ref lst, "Gold", "Ouro", node["nRT_Cliente_Normal_GarantiasACOES_OURO"], node["nRT_Cliente_Investimento_GarantiasACOES_OURO"]);
                    LoadResumeItem(ref lst, "BMFGuarantees", "Garantias BM&F", node["nRT_Cliente_Normal_GarantiasBMF_Acoes"], node["nRT_Cliente_Investimento_GarantiasBMF_Acoes"]);
                    LoadResumeItem(ref lst, "CDBFinal", "CDB Final", node["nRT_Cliente_Normal_GarantiasACOES_CDB"], node["nRT_Cliente_Investimento_GarantiasACOES_CDB"]);
                    LoadResumeItem(ref lst, "CommitedCDB", "Compromissada em CDB", node["nRT_Cliente_Normal_CompromissadaGarantiasACOES_CDB"], node["nRT_Cliente_Investimento_CompromissadaGarantiasACOES_CDB"]); // TODO: tag ainda não consta no xml do cliente, criada para fins de desenvolvimento
                    LoadResumeItem(ref lst, "PublicTitles", "Títulos Públicos", node["nRT_Cliente_Normal_GarantiasACOES_TITULOS_PUBLICOS"], node["nRT_Cliente_Investimento_GarantiasACOES_TITULOS_PUBLICOS"]);
                    LoadResumeItem(ref lst, "Clubs", "Clubes", node["nRT_Cliente_Normal_Fundos"], node["nRT_Cliente_Normal_Clubes"], node["nRT_Cliente_Investimento_Fundos"], node["nRT_Cliente_Investimento_Clubes"]);
                    LoadResumeItem(ref lst, "TotalCheckingAccount", "Total Conta Corrente", node["nRT_Cliente_Normal_Saldo_Cliente"], node["nRT_Cliente_Investimento_Saldo_Cliente"]);
                }

                position.Resume = lst;
            }

            #endregion

            NotifyFailTags();

            return position;
        }

        private decimal FormatToDecimal(XmlNode node)
        {
            try
            {
                if (node != null) {

                    string value = node.InnerText.Trim().Replace('.', ',');
                    if (string.IsNullOrEmpty(value))
                        return 0;

                    decimal v;
                    decimal.TryParse(value, out v);
                    return v;
                }

                return 0;
            }
            catch(Exception ex)
            {
                RegisterFailTag(ex);
                return 0;
            }
        }

        private Int64 FormatToInt64(XmlNode node)
        {
            try
            {
                string value = node.InnerText.Trim();
                if (string.IsNullOrEmpty(value))
                    return 0;

                Int64 v;
                Int64.TryParse(value, out v);
                return v;
            }
            catch(Exception ex)
            {
                RegisterFailTag(ex);
                return 0;
            }
        }

        private string FormatToShortDate(XmlNode node)
        {
            try
            {
                string value = node.InnerText.Trim().Replace("-", "/");
                if (string.IsNullOrEmpty(node.InnerText.Trim()))
                    return "-";

                DateTime date;
                DateTime.TryParse(value, out date);
                return date.ToString("dd/MM/yyyy");
            }
            catch(Exception ex)
            {
                RegisterFailTag(ex);
                return "-";
            }
        }

        private string FormatToString(XmlNode node)
        {
            try
            {
                if (node != null) {

                    if (string.IsNullOrEmpty(node.InnerText.Trim()))
                        return "-";

                    return node.InnerText.Trim();
                }

                return "-";
            }
            catch(Exception ex)
            {
                RegisterFailTag(ex);
                return "-";
            }
        }

        private void RegisterFailTag(Exception ex)
        {
            var a = string.Empty;
            StackTrace stackTrace = new StackTrace(true);
            for (var i = 0; i < stackTrace.FrameCount; i++)
            {
                a += string.Format("Filename: {0}<br>Method: {1}<br>Line: {2}<br>Column: {3}<br>Exception: {4}<br><br><br>",
                    stackTrace.GetFrame(i).GetFileName(),
                    stackTrace.GetFrame(i).GetMethod(),
                    stackTrace.GetFrame(i).GetFileLineNumber(),
                    stackTrace.GetFrame(i).GetFileColumnNumber(),
                    ex.StackTrace);
                if (stackTrace.GetFrame(i).GetMethod().ToString().Contains("QX3.Portal.Contracts.DataContracts.ConsolidatedPosition"))
                    i = stackTrace.FrameCount;
            }
            logExceptionConsolidatedPosition += a + "<hr>";
        }

        private void NotifyFailTags()
        {
            if (logExceptionConsolidatedPosition != null && logExceptionConsolidatedPosition.Length > 0)
            {
                SendMail sendMail = new SendMail();
                sendMail.EmailTo = "diogo.sobral@qx3.com.br";
                sendMail.EmailFrom = "diogo.sobral@qx3.com.br";
                sendMail.Subject = "VCNet - Erro no XML de Posição Consolidada - " + DateTime.Now;
                sendMail.Body = logExceptionConsolidatedPosition;
                sendMail.Send();
            }
        }

        private void LoadResumeItem(ref List<CPResume> lst, string type, string label, XmlNode normal, XmlNode investment)
        {
            if (normal != null || investment != null)
                if (!string.IsNullOrEmpty(normal.InnerText) || !string.IsNullOrEmpty(investment.InnerText))
                    if (!normal.InnerText.Equals("0") || !investment.InnerText.Equals("0")) {
                        CPResume obj = new CPResume();
                        obj.Type = type;
                        obj.Label = label;
                        obj.NormalAccount = FormatToDecimal(normal);
                        obj.InvestmentAccount = FormatToDecimal(investment);
                        obj.Total = obj.NormalAccount + obj.InvestmentAccount;
                        lst.Add(obj);
                    }
                    else {
                        CPResume obj = new CPResume();
                        obj.Type = type;
                        obj.Label = label;
                        obj.NormalAccount = 0;
                        obj.InvestmentAccount = 0;
                        obj.Total = 0;
                        lst.Add(obj);
                    }
        }

        private void LoadResumeItem(ref List<CPResume> lst, string type, string label, XmlNode normal, XmlNode normal2, XmlNode investment, XmlNode investment2)
        {
            if (normal != null || investment != null || normal2 != null || investment2 != null)
                if (!string.IsNullOrEmpty(normal.InnerText) || !string.IsNullOrEmpty(investment.InnerText) || !string.IsNullOrEmpty(normal2.InnerText) || !string.IsNullOrEmpty(investment2.InnerText))
                    if (!normal.InnerText.Equals("0") || !investment.InnerText.Equals("0") || !normal2.InnerText.Equals("0") || !investment2.InnerText.Equals("0")) {
                        CPResume obj = new CPResume();
                        obj.Type = type;
                        obj.Label = label;
                        obj.NormalAccount = FormatToDecimal(normal) + FormatToDecimal(normal2);
                        obj.InvestmentAccount = FormatToDecimal(investment) + FormatToDecimal(investment2);
                        obj.Total = obj.NormalAccount + obj.InvestmentAccount;
                        lst.Add(obj);
                    }
                    else {
                        CPResume obj = new CPResume();
                        obj.Type = type;
                        obj.Label = label;
                        obj.NormalAccount = 0;
                        obj.InvestmentAccount = 0;
                        obj.Total = 0;
                        lst.Add(obj);
                    }
        }

        private string CleanXMLResult(string value)
        {
            string result = value.Replace("'", "\"").Trim();

            result = result.Replace("&lt;", "<").Replace("&gt;", ">").Replace("?", "").Trim();
            result = result.Replace("<string>", "").Replace("</string>", "").Trim();
            result = result.Remove(result.IndexOf('<'), result.IndexOf('>')).Trim();
            result = result.Replace(" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"", "").Trim();
            result = result.Replace(" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"", "").Trim();
            result = result.Replace(" xmlns=\"http://tempuri.org/\"", "").Trim();
            result = result.Replace(" ", "").Trim();

            Regex regex = new Regex(@">\s*<");
            string cleanedXml = regex.Replace(result, "><");

            cleanedXml = cleanedXml.Replace("?", "").Trim();

            return cleanedXml;
        }

        /// <summary>
        /// Method to convert a custom Object to XML string
        /// </summary>
        public String SerializeGrTraderObject(Object pObject)
        {
            try
            {
                UTF8Encoding utfWithoutBom = new UTF8Encoding(false);
                String XmlizedString = null;
                MemoryStream memoryStream = new MemoryStream();
                XmlSerializer xs = new XmlSerializer(typeof(GRTrader));
                XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, utfWithoutBom);

                xs.Serialize(xmlTextWriter, pObject);
                memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
                XmlizedString = UTF8ByteArrayToString(memoryStream.ToArray());
                return XmlizedString;
            }
            catch (Exception e)
            {
                System.Console.WriteLine(e);
                return null;
            }
        }

        /// <summary>
        /// To convert a Byte Array of Unicode values (UTF-8 encoded) to a complete String.
        /// </summary>
        private String UTF8ByteArrayToString(Byte[] characters)
        {
            UTF8Encoding encoding = new UTF8Encoding(false);
            String constructedString = encoding.GetString(characters);
            return (constructedString);
        }

        /// <summary>
        /// Converts the String to UTF8 Byte array and is used in De serialization
        /// </summary>
        private Byte[] StringToUTF8ByteArray(String pXmlString)
        {
            UTF8Encoding encoding = new UTF8Encoding();
            Byte[] byteArray = encoding.GetBytes(pXmlString);
            return byteArray;
        }

        /// <summary>
        /// Method to reconstruct an Object from XML string
        /// </summary>
        public Object DeserializeObject(String pXmlizedString)
        {
            XmlSerializer xs = new XmlSerializer(typeof(GRTrader));
            MemoryStream memoryStream = new MemoryStream(StringToUTF8ByteArray(pXmlizedString));
            XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);

            return xs.Deserialize(memoryStream);
        }

        #endregion

        #region Operation Map

        public List<OperationsMap> LoadOperationsMap(OperationMapFilter filter)
        {
            List<OperationsMap> result = null;

            var cmd = dbi.CreateCommand("PCK_MAPA_OPERACOES.PROC_LISTA_MAPA_OPERACOES", CommandType.StoredProcedure);
            cmd.Parameters.Clear();

            /*if (filter.ClientCode != 0)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = filter.ClientCode });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });*/

            if (filter.TradingDate.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_NEGOCIO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.TradingDate.Value.ToShortDateString() });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_NEGOCIO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });

            if (!String.IsNullOrEmpty(filter.StockCode))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pATIVO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.StockCode });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pATIVO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });

            if (!String.IsNullOrEmpty(filter.Login))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pUSUARIO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.Login });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pUSUARIO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pMASTER", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.Master ? "S" : "N" });

            /*
            if (filter.OperatorCode != 0)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pOPERADOR", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = filter.OperatorCode });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pOPERADOR", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
            */

            if (filter.ClientFilter == null)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTCLIENTE", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTCLIENTE", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.ClientFilter });

            if (filter.UserId.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_USUARIO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = filter.UserId.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_USUARIO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCursor", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    int counter = 0;
                    result = new List<OperationsMap>();
                    while (dr.Read())
                    {
                        OperationsMap op = new OperationsMap();

                        op.Line = ++counter;
                        if (!dr.IsDBNull(0)) op.ClientCode = dr.GetInt32(0);
                        if (!dr.IsDBNull(1)) op.TradingDate = dr.GetDateTime(1);
                        if (!dr.IsDBNull(2)) op.Operation = dr.GetString(2).ToCharArray()[0];
                        if (!dr.IsDBNull(3)) op.StockCode = dr.GetString(3);
                        if (!dr.IsDBNull(4)) op.TradingQtty = dr.GetInt32(4);
                        if (!dr.IsDBNull(5)) op.TradingValue = dr.GetDecimal(5);
                        if (!dr.IsDBNull(6)) op.Volume = dr.GetDecimal(6);
                        if (!dr.IsDBNull(7)) op.BrokerageValue = dr.GetDecimal(7);
                        if (!dr.IsDBNull(8)) op.Operator = dr.GetString(8);
                        if (!dr.IsDBNull(9)) op.OperatorCode = dr.GetInt32(9);

                        result.Add(op);
                    }
                }

                dr.Close();

            }

            return result;
        }

        /*public List<DateTime> GetTradingDatesMock() 
        {
            List<DateTime> result = null;

            var cmd = dbi.CreateCommand("select Distinct a.Dt_Negocio from corrwin.torcomi a", CommandType.Text);

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    result = new List<DateTime>();
                    while (dr.Read())
                    {
                        result.Add(dr.GetDateTime(0));
                    }
                }

                dr.Close();

            }

            return result;
        }*/

        public Dictionary<String, String> GetTradingDates()
        {
            Dictionary<String, String> result = null;

            var cmd = dbi.CreateCommand("PCK_MAPA_OPERACOES.PROC_CARREGA_DATAS_PREGAO", CommandType.StoredProcedure);

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCursor", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    result = new Dictionary<String, String>();
                    while (dr.Read())
                    {
                        result.Add(dr.GetString(0), dr.GetString(1));
                    }
                }

                dr.Close();

            }

            return result;
        }

        public List<Operator> GetOperatorList(OperationMapFilter filter)
        {
            List<Operator> result = null;

            var cmd = dbi.CreateCommand("PCK_MAPA_OPERACOES.PROC_CARREGA_OPERADORES", CommandType.StoredProcedure);
            cmd.Parameters.Clear();

            if (filter.ClientCode != 0)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = filter.ClientCode });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });

            if (filter.TradingDate.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_NEGOCIO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.TradingDate.Value.ToShortDateString() });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_NEGOCIO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });

            if (!String.IsNullOrEmpty(filter.StockCode))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pATIVO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.StockCode });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pATIVO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });

            if (!String.IsNullOrEmpty(filter.Operator))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pUSUARIO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.Operator });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pUSUARIO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });

            if (filter.OperatorCode != 0)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_USUARIO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = filter.OperatorCode });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_USUARIO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCursor", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    result = new List<Operator>();
                    while (dr.Read())
                    {
                        Operator op = new Operator();

                        if (!dr.IsDBNull(0)) op.OperatorCode = dr.GetInt32(0);
                        if (!dr.IsDBNull(1)) op.OperatorName = dr.GetString(1);
                        //TODO: Colocar dados reais.
                        op.BrokerageValue = 2500;

                        result.Add(op);
                    }
                }

                dr.Close();

            }

            return result;
        }

        #endregion

        #region Operações

        public List<OperationClient> LoadOperationClients(OperationClientFilter filter, FilterOptions filterOptions, int? userId, out int count)
        {
            List<OperationClient> result = null;

            var cmd = dbi.CreateCommand("PCK_OPERACOES.PROC_OPERACOES", CommandType.StoredProcedure);

            cmd.Parameters.Clear();

            if (filter.TradingDate.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_NEGOCIO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.TradingDate.Value.ToShortDateString() });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_NEGOCIO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });

            if (String.IsNullOrEmpty(filter.AssessorFilter))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTASSESSOR", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTASSESSOR", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.AssessorFilter });

            if (filter.ClientFilter == null)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTCLIENTE", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTCLIENTE", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.ClientFilter });

            if (userId.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_USUARIO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = userId.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_USUARIO", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pINITROW", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = filterOptions.InitRow });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pFINALROW", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = filterOptions.FinalRow });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pSORTCOLUMN", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = filterOptions.SortColumn });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pSORTDIR", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filterOptions.SortDirection });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pTODOS", IsNullable = true, OracleType = OracleType.Int32, Direction = ParameterDirection.Input, Value = (filterOptions.AllRecords) ? 1 : 0 });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCount", OracleType = OracleType.Number, Direction = ParameterDirection.Output });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCursor", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    result = new List<OperationClient>();
                    while (dr.Read())
                    {
                        OperationClient op = new OperationClient();

                        if (!dr.IsDBNull(0)) op.TradingDate = dr.GetDateTime(0);
                        if (!dr.IsDBNull(1)) op.ClientCode = dr.GetInt32(1);
                        if (!dr.IsDBNull(2)) op.ClientName = dr.GetString(2);
                        if (!dr.IsDBNull(3)) op.AssessorName = dr.GetString(3);
                        if (!dr.IsDBNull(4)) op.DescStatus = dr.GetString(4);
                        if (!dr.IsDBNull(5)) op.Status = dr.GetString(5);
                        if (!dr.IsDBNull(6)) op.AssessorCode = dr.GetInt32(6);

                        result.Add(op);
                    }
                }

                dr.Close();

                count = Convert.ToInt32(cmd.Parameters["pCount"].Value);
            }

            return result;
        }

        public OperationReport LoadOperationReport(OperationReport filter)
        {
            OperationReport result = new OperationReport();

            var cmd = dbi.CreateCommand("PCK_OPERACOES.PROC_PORTAL_OPERACOES", CommandType.StoredProcedure);
            cmd.Parameters.Clear();

            if (filter.ClientCode == 0)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", IsNullable = true, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = filter.ClientCode });

            if (filter.TradeDate != null && filter.TradeDate != DateTime.MinValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_PREGAO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = filter.TradeDate.ToShortDateString() });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_PREGAO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pDETALHE", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pFINANCEIRO", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    result.ReportDetails = new List<OperationReportDetails>();

                    while (dr.Read())
                    {
                        OperationReportDetails op = new OperationReportDetails();

                        if (!dr.IsDBNull(0) && result.ClientCode == 0) result.ClientCode = dr.GetInt32(0);
                        if (!dr.IsDBNull(1)) op.TradeType = dr.GetString(1);
                        if (!dr.IsDBNull(2)) op.OperationType = dr.GetString(2);
                        if (!dr.IsDBNull(3)) op.MarketName = dr.GetString(3);
                        if (!dr.IsDBNull(4)) op.TradeCode = dr.GetString(4);
                        if (!dr.IsDBNull(5)) op.MarketType = dr.GetString(5);
                        if (!dr.IsDBNull(6)) op.TermDueDateType = dr.GetInt32(6);
                        if (!dr.IsDBNull(7)) op.CompanyName = dr.GetString(7);
                        if (!dr.IsDBNull(8)) op.SpecificationCode = dr.GetString(8);
                        if (!dr.IsDBNull(9)) op.IsinCode = dr.GetString(9);
                        if (!dr.IsDBNull(10)) op.Header = dr.GetString(10);
                        if (!dr.IsDBNull(11)) op.AvailableQuantity = dr.GetInt32(11);
                        if (!dr.IsDBNull(12)) op.TradeValue = dr.GetDecimal(12);
                        if (!dr.IsDBNull(13) && result.TradeDate == DateTime.MinValue) result.TradeDate = Convert.ToDateTime(string.Format("{0}/{1}/{2}", dr.GetString(13).Split('/')[1], dr.GetString(13).Split('/')[0], dr.GetString(13).Split('/')[2]));
                        if (!dr.IsDBNull(14)) op.SettlementDate = dr.GetDateTime(14);
                        if (!dr.IsDBNull(15)) op.TotalTradeValue = dr.GetInt32(15);
                        if (!dr.IsDBNull(16)) op.TotalNet = dr.GetDecimal(16);
                        if (!dr.IsDBNull(17)) op.BrokerageValue = dr.GetDecimal(17);

                        result.ReportDetails.Add(op);
                    }

                    dr.NextResult();

                    if (dr.HasRows)
                    {
                        result.ReportFinancial = new List<OperationReportFinancial>();

                        while (dr.Read())
                        {
                            OperationReportFinancial op = new OperationReportFinancial();

                            //if (!dr.IsDBNull(0)) op.MarketType = dr.GetString(0); 
                            if (!dr.IsDBNull(0)) op.TradeType = dr.GetString(0);
                            if (!dr.IsDBNull(1)) op.TotalBuying = dr.GetInt32(1);
                            if (!dr.IsDBNull(2)) op.TotalSelling = dr.GetInt32(2);
                            if (!dr.IsDBNull(3)) op.TotalTrade = dr.GetInt32(3);
                            if (!dr.IsDBNull(4)) op.BrokerageValue = dr.GetDecimal(4);
                            if (!dr.IsDBNull(5)) op.OperationalTaxCBLC = dr.GetDecimal(5);
                            if (!dr.IsDBNull(6)) op.OperationalTaxBovespa = dr.GetDecimal(6);
                            if (!dr.IsDBNull(7)) op.OperationalTax = dr.GetDecimal(7);
                            if (!dr.IsDBNull(8)) op.OtherExpenses = dr.GetDecimal(8);
                            if (!dr.IsDBNull(9)) op.IRRFDaytrade = dr.GetDecimal(9);
                            if (!dr.IsDBNull(10)) op.IRRFOperations = dr.GetDecimal(10);
                            if (!dr.IsDBNull(11)) op.TotalNet = dr.GetDecimal(11);
                            if (!dr.IsDBNull(12)) op.SettlementDate = dr.GetDateTime(12);

                            result.ReportFinancial.Add(op);
                        }
                    }
                }
                dr.Close();
            }
            return result;
        }

        public bool SinalizeMailSended(int clientCode, DateTime tradingDate)
        {
            var cmd = dbi.CreateCommand("PCK_OPERACOES.PROC_SINALIZA_ENVIO", CommandType.StoredProcedure);
            cmd.Parameters.Clear();

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = clientCode });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_PREGAO", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = tradingDate.ToShortDateString() });

            return Convert.ToInt32(cmd.ExecuteNonQuery()) > 0;
        }

        #endregion

        #region Informe de Rendimentos

        public IncomeReport LoadIncomeReport(IncomeReportFilter filter)
        {

            var cmd = dbi.CreateCommand("PCK_INFORME_RENDIMENTOS.PROC_INFORME_RENDIMENTOS", CommandType.StoredProcedure);

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_CLIENTE", IsNullable = false, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = filter.ClientCode });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pANO", IsNullable = false, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = filter.Year });

            if (filter.UserId.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_USUARIO", IsNullable = false, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = filter.UserId.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_USUARIO", IsNullable = false, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });


            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSORVISTA", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSORALUGUELACOES", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSOROPCOES", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSORBMFFUTURO", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSORBMFOURO", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSORBMFSWAP", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSORPROVENTOS", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSORTERMO", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSORIRDT", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSORIROPERACOES", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSORSALDO", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pDATAPOSICAO", OracleType = OracleType.VarChar, Direction = ParameterDirection.Output, Size = 14 });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pTIPOPESSOA", OracleType = OracleType.VarChar, Direction = ParameterDirection.Output, Size = 1 });


            IncomeReport report = new IncomeReport();

            using (var reader = cmd.ExecuteReader())
            {
                //if (reader.HasRows) 
                {
                    while (reader.Read())
                    {
                        //Vista
                        var obj = new StockMarket();
                        if (!reader.IsDBNull(2)) obj.StockCode = reader.GetString(2); //cd_negocio
                        if (!reader.IsDBNull(3)) obj.AvailableQuantity = reader.GetInt32(3); //Qt_Disponivel
                        if (!reader.IsDBNull(4)) obj.Price = reader.GetDecimal(4); //Pr_Medio
                        if (!reader.IsDBNull(5)) obj.TotalValue = reader.GetDecimal(5);//Vl_Volume

                        report.StockMarketList.Add(obj);
                    }

                    if (reader.NextResult())
                    {
                        while (reader.Read())
                        {
                            //Aluguel
                            var obj = new StockRent();

                            if (!reader.IsDBNull(2)) obj.StockCode = reader.GetString(2); //Cd_Negocio
                            if (!reader.IsDBNull(3)) obj.Type = reader.GetString(3); //Tp_Contrato
                            if (!reader.IsDBNull(4)) obj.OpenDate = reader.GetDateTime(4); //dt_abertura
                            if (!reader.IsDBNull(5)) obj.PaymentDate = reader.GetDateTime(5); //Dt_Vencimento
                            if (!reader.IsDBNull(6)) obj.Quantity = reader.GetInt32(6); //Qt_Disponivel
                            if (!reader.IsDBNull(7)) obj.Tax = reader.GetDecimal(7); //Vl_Taxas
                            if (!reader.IsDBNull(8)) obj.Quote = reader.GetDecimal(8); //Pr_Medio
                            if (!reader.IsDBNull(9)) obj.TotalValue = reader.GetDecimal(9); //Vl_Volume

                            report.StockRentList.Add(obj);
                        }
                    }

                    if (reader.NextResult())
                    {
                        while (reader.Read())
                        {
                            //Opções
                            var obj = new BMFOptions();

                            if (!reader.IsDBNull(2)) obj.StockCode = reader.GetString(2); //Cd_Negocio
                            if (!reader.IsDBNull(3)) obj.OperationType = reader.GetString(3); //cd_natope
                            if (!reader.IsDBNull(4)) obj.Type = reader.GetString(4); //tp_opcao
                            if (!reader.IsDBNull(5)) obj.PaymentDate = reader.GetDateTime(5); //dt_exercicio
                            if (!reader.IsDBNull(6)) obj.AvailableQtty = reader.GetInt32(6); //Qt_Disponivel
                            if (!reader.IsDBNull(7)) obj.Price = reader.GetDecimal(7); //pr_medio
                            if (!reader.IsDBNull(8)) obj.Volume = reader.GetDecimal(8); //vl_volume
                            if (!reader.IsDBNull(9)) obj.VistaVolume = 0; //vl_volume_vista

                            report.OptionsList.Add(obj);
                        }
                    }

                    if (reader.NextResult())
                    {
                        while (reader.Read())
                        {
                            //BMF - Futuro e Opções
                            var obj = new BMFOptionsFuture();

                            if (!reader.IsDBNull(2)) obj.StockCode = reader.GetString(2); //cd_commod
                            if (!reader.IsDBNull(3)) obj.Type = reader.GetString(3); //tp_mercado
                            if (!reader.IsDBNull(4)) obj.PaymentDate = reader.GetDateTime(4); //dt_vencimento
                            if (!reader.IsDBNull(5)) obj.Price = reader.GetDecimal(5); //pr_exercicio
                            if (!reader.IsDBNull(6)) obj.CV = reader.GetString(6); //in_natatu
                            if (!reader.IsDBNull(7)) obj.Position = reader.GetDecimal(7); //qt_atual

                            report.BMFOptionsFutureList.Add(obj);
                        }
                    }

                    if (reader.NextResult())
                    {
                        while (reader.Read())
                        {
                            //BMF - Ouro
                            var obj = new BMFGold();

                            if (!reader.IsDBNull(2)) obj.Pounds = reader.GetDecimal(2); //qt_dia_atu
                            if (!reader.IsDBNull(3)) obj.DefaultQuantity = reader.GetInt32(3); //qt_dia_atu_cont
                            if (!reader.IsDBNull(4)) obj.Quote = reader.GetDecimal(4); //vl_cotmenor
                            if (!reader.IsDBNull(5)) obj.TotalValue = reader.GetDecimal(5); //vl_volume

                            report.BMFGoldList.Add(obj);
                        }
                    }

                    if (reader.NextResult())
                    {
                        while (reader.Read())
                        {
                            //BMF - Swap

                            var obj = new BMFSwap();

                            if (!reader.IsDBNull(2)) obj.Contract = reader.GetString(2); //cd_commod
                            if (!reader.IsDBNull(3)) obj.Number = reader.GetInt32(3); //numbmf
                            if (!reader.IsDBNull(4)) obj.Base = reader.GetInt32(4); //tambas
                            if (!reader.IsDBNull(5)) obj.PaymentDate = reader.GetDateTime(5); //datvct
                            if (!reader.IsDBNull(6)) obj.RegisterDate = reader.GetDateTime(6); //datreg
                            if (!reader.IsDBNull(7)) obj.NetTotal = reader.GetDecimal(7); //vol

                            report.BMFSwapList.Add(obj);
                        }
                    }

                    if (reader.NextResult())
                    {
                        while (reader.Read())
                        {
                            //Proventos
                            var obj = new IncomeProceeds();

                            if (!reader.IsDBNull(2)) obj.StockCode = reader.GetString(2); //cd_negocio
                            if (!reader.IsDBNull(3)) obj.Type = reader.GetString(3); //ds_provento
                            if (!reader.IsDBNull(4)) obj.AvailableQuantity = reader.GetInt32(4); ; //qt_provento
                            if (!reader.IsDBNull(5)) obj.GrossValue = reader.GetDecimal(5); //vl_provento
                            if (!reader.IsDBNull(6)) obj.IRPerc = reader.GetDecimal(6); //pc_ir_dividendo
                            if (!reader.IsDBNull(7)) obj.IRValue = reader.GetDecimal(7); //vl_ir_dividendo
                            if (!reader.IsDBNull(8)) obj.NetValue = reader.GetDecimal(8); //vl_provento_liq
                            if (!reader.IsDBNull(9)) obj.PaymentDate = reader.GetDateTime(9); //dt_pagamento

                            report.IncomeProceedsList.Add(obj);
                        }
                    }

                    if (reader.NextResult())
                    {
                        while (reader.Read())
                        {
                            //Termos

                            var obj = new IncomeTerms();

                            if (!reader.IsDBNull(2)) obj.StockCode = reader.GetString(2); //Cd_Negocio
                            if (!reader.IsDBNull(3)) obj.AvailableQuantity = reader.GetInt32(3); //Qt_Disponivel
                            if (!reader.IsDBNull(4)) obj.Price = reader.GetDecimal(4); //Pr_Medio
                            if (!reader.IsDBNull(5)) obj.PaymentDate = reader.GetDateTime(5); //Dt_Vencimento
                            if (!reader.IsDBNull(6)) obj.Total = reader.GetDecimal(6); //Vl_Volume

                            report.IncomeTermsList.Add(obj);
                        }
                    }

                    if (reader.NextResult())
                    {
                        while (reader.Read())
                        {
                            //DT

                            var obj = new IRPeriod();

                            var client = reader.GetInt32(0);

                            if (client == filter.ClientCode)
                            {
                                if (!reader.IsDBNull(2))
                                {
                                    var dt = Convert.ToDateTime(reader.GetString(2));
                                    obj.InitDate = dt;
                                    obj.Month = dt.ToString("MMMM", System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat).Capiralize();
                                }
                                if (!reader.IsDBNull(3)) obj.GrossValue = reader.GetDecimal(3);
                                if (!reader.IsDBNull(4)) obj.Retained = reader.GetDecimal(4);

                                report.IRRFDayTradeList.Add(obj);
                            }


                        }

                    }

                    if (reader.NextResult())
                    {
                        while (reader.Read())
                        {
                            //Operações

                            var obj = new IRPeriod();

                            var client = reader.GetInt32(0);

                            if (client == filter.ClientCode)
                            {
                                if (!reader.IsDBNull(2))
                                {
                                    var dt = Convert.ToDateTime(reader.GetString(2));
                                    obj.InitDate = dt;
                                    obj.Month = dt.ToString("MMMM", System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat).Capiralize();
                                }
                                if (!reader.IsDBNull(3)) obj.GrossValue = reader.GetDecimal(3);
                                if (!reader.IsDBNull(4)) obj.Retained = reader.GetDecimal(4);

                                report.IRRFOperationsList.Add(obj);
                            }

                        }
                    }

                    if (reader.NextResult())
                    {
                        while (reader.Read())
                        {

                            if (!reader.IsDBNull(2)) report.Balance = reader.GetDecimal(2);

                        }
                    }
                }
            }

            report.Client = this.LoadClientInformation(filter.ClientCode.ToString(), null, null);

            if (!(cmd.Parameters["pDATAPOSICAO"].Value is System.DBNull))
                report.PositionDate = Convert.ToDateTime(cmd.Parameters["pDATAPOSICAO"].Value);

            var type = cmd.Parameters["pTIPOPESSOA"].Value.ToString();

            if (type == "F")
            {
                if (report.IRRFDayTradeList.Count > 0)
                {
                    var first = report.IRRFDayTradeList.FirstOrDefault();
                    for (var i = 1; i <= 12; i++)
                    {
                        if (report.IRRFDayTradeList.Where(a => a.InitDate.HasValue && a.InitDate.Value.Month == i).FirstOrDefault() == null)
                        {
                            var obj = new IRPeriod();
                            var dt = Convert.ToDateTime("01/" + i.ToString().PadLeft(2, '0') + "/" + first.InitDate.Value.Year.ToString());
                            obj.InitDate = dt;
                            obj.Month = dt.ToString("MMMM", System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat).Capiralize();

                            report.IRRFDayTradeList.Add(obj);
                        }

                    }
                    report.IRRFDayTradeList = report.IRRFDayTradeList.OrderBy(a => a.InitDate).ToList();
                }

                if (report.IRRFOperationsList.Count > 0)
                {
                    var first = report.IRRFOperationsList.FirstOrDefault();
                    for (var i = 1; i <= 12; i++)
                    {
                        if (report.IRRFOperationsList.Where(a => a.InitDate.HasValue && a.InitDate.Value.Month == i).FirstOrDefault() == null)
                        {
                            var obj = new IRPeriod();
                            var dt = Convert.ToDateTime("01/" + i.ToString().PadLeft(2, '0') + "/" + first.InitDate.Value.Year.ToString());
                            obj.InitDate = dt;
                            obj.Month = dt.ToString("MMMM", System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat).Capiralize();

                            report.IRRFOperationsList.Add(obj);
                        }

                    }
                    report.IRRFOperationsList = report.IRRFOperationsList.OrderBy(a => a.InitDate).ToList();
                }
            }

            return report;
        }

        #endregion

        #region Private

        public List<Private> LoadPrivates(DateTime initMov, DateTime endMov)
        {
            List<Private> result = null;

            var cmd = dbi.CreateCommand("PCK_PRIVATE.PROC_PRIVATE", CommandType.StoredProcedure);

            cmd.Parameters.Add(new OracleParameter { ParameterName = "PINIMOV", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = initMov.ToString("dd/MM/yyyy") });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "PFIMMOV", IsNullable = true, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = endMov.ToString("dd/MM/yyyy") });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCursor", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    result = new List<Private>();
                    while (dr.Read())
                    {
                        Private op = new Private();

                        if (!dr.IsDBNull(0)) op.CPFCGC = dr[0].ToString();
                        if (!dr.IsDBNull(1)) op.ClientName = dr.GetString(1);
                        if (!dr.IsDBNull(2)) op.Product = dr.GetString(2);
                        if (!dr.IsDBNull(3)) op.BrokerageValue = dr.GetDecimal(3);
                        if (!dr.IsDBNull(4)) op.PositionValue = dr.GetDecimal(4);
                        if (!dr.IsDBNull(5)) op.AvailableValue = dr.GetDecimal(5);
                        if (!dr.IsDBNull(6)) op.CaptationValue = dr.GetDecimal(6);
                        if (!dr.IsDBNull(7)) op.ApplicationValue = dr.GetDecimal(7);
                        if (!dr.IsDBNull(8)) op.RedemptionValue = dr.GetDecimal(8);

                        result.Add(op);
                    }
                }

                dr.Close();
            }

            return result;
        }

        #endregion

        #region Aluguel BB

        public BBRent LoadBBRent(DateTime tradeDate)
        {
            BBRent result = null;

            var cmd = dbi.CreateCommand("PCK_ALUGUEL_BB.PROC_GERA_PLANILHA", CommandType.StoredProcedure);

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pDT_PREGAO", IsNullable = false, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = tradeDate.ToString("dd/MM/yyyy") });

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pLIQUIDACAO", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pREPASSE", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pPROVENTOS", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    result = new BBRent();
                    result.Settlements = new List<BBSettlement>();

                    while (dr.Read())
                    {
                        BBSettlement op = new BBSettlement();

                        if (!dr.IsDBNull(0)) op.Date = dr.GetDateTime(0);
                        if (!dr.IsDBNull(1)) op.ClientCode = dr.GetInt32(1);
                        if (!dr.IsDBNull(2)) op.ByAccount = dr.GetInt32(2);
                        if (!dr.IsDBNull(3)) op.Contract = dr.GetInt32(3);
                        if (!dr.IsDBNull(4)) op.Stock = dr.GetString(4);
                        if (!dr.IsDBNull(5)) op.Quantity = dr.GetInt32(5);
                        if (!dr.IsDBNull(6)) op.Gross = dr.GetDecimal(6);
                        if (!dr.IsDBNull(7)) op.IR = dr.GetDecimal(7);
                        if (!dr.IsDBNull(8)) op.NetValue = dr.GetDecimal(8);
                        if (!dr.IsDBNull(9)) op.TotalCommission = dr.GetDecimal(9);
                        if (!dr.IsDBNull(10)) op.NetCommission = dr.GetDecimal(10);

                        result.Settlements.Add(op);
                    }

                    if (dr.NextResult())
                    {
                        result.Transfers = new List<BBTransfer>();

                        while (dr.Read())
                        {
                            BBTransfer op = new BBTransfer();

                            if (!dr.IsDBNull(0)) op.Date = dr.GetDateTime(0);
                            if (!dr.IsDBNull(1)) op.ClientCode = dr.GetInt32(1);
                            if (!dr.IsDBNull(2)) op.ByAccount = dr.GetInt32(2);
                            if (!dr.IsDBNull(3)) op.Contract = dr.GetInt32(3);
                            if (!dr.IsDBNull(4)) op.Stock = dr.GetString(4);
                            if (!dr.IsDBNull(5)) op.Quantity = dr.GetInt32(5);
                            if (!dr.IsDBNull(6)) op.Gross = dr.GetDecimal(6);
                            if (!dr.IsDBNull(7)) op.IR = dr.GetDecimal(7);
                            if (!dr.IsDBNull(8)) op.NetValue = dr.GetDecimal(8);
                            if (!dr.IsDBNull(9)) op.TotalCommission = dr.GetDecimal(9);
                            if (!dr.IsDBNull(10)) op.NetCommission = dr.GetDecimal(10);
                            if (!dr.IsDBNull(11)) op.Transfers = dr.GetDecimal(11);

                            result.Transfers.Add(op);
                        }
                    }

                    if (dr.NextResult())
                    {
                        result.Proceeds = new List<BBProceeds>();

                        while (dr.Read())
                        {
                            BBProceeds op = new BBProceeds();

                            if (!dr.IsDBNull(0)) op.Date = dr.GetDateTime(0);
                            if (!dr.IsDBNull(1)) op.ClientCode = dr.GetInt32(1);
                            if (!dr.IsDBNull(2)) op.ByAccount = dr.GetInt32(2);
                            if (!dr.IsDBNull(3)) op.Stock = dr.GetString(3);
                            if (!dr.IsDBNull(4)) op.Quantity = dr.GetInt32(4);
                            if (!dr.IsDBNull(5)) op.NetValue = dr.GetDecimal(5);
                            if (!dr.IsDBNull(6)) op.Type = dr.GetString(6);

                            result.Proceeds.Add(op);
                        }
                    }
                }
                dr.Close();
            }
            return result;
        }

        #endregion

        #region VAM

        public Dictionary<string, string> GetISINs(int? groupID)
        {
            Dictionary<string, string> result = null;

            var cmd = dbi.CreateCommand("PCK_POSICAO_GERAL_FUNDO.PROC_COMBO_ISIN", CommandType.StoredProcedure);
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pLstAssessor", IsNullable = false, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTCLIENTE", IsNullable = false, OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            if (groupID.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = false, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = groupID.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", IsNullable = false, OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCursor", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                if (dr.HasRows)
                {
                    result = new Dictionary<string, string>();
                    while (dr.Read())
                    {
                        string isin = Convert.ToString(dr["cod_isin"]);
                        result.Add(isin, isin);
                    }
                }

                dr.Close();

            }

            return result;
        }

        public List<VAMItem> LoadVAM(int? groupID, string isin)
        {
            List<VAMItem> items = new List<VAMItem>();

            var cmd = dbi.CreateCommand("PCK_POSICAO_GERAL_FUNDO.PROC_GERA_PLANILHA", CommandType.StoredProcedure);

            cmd.Parameters.Add(new OracleParameter { ParameterName = "pLSTCLIENTE", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pLstAssessor", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            if (string.IsNullOrEmpty(isin))
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_ISIN", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = DBNull.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCD_ISIN", OracleType = OracleType.VarChar, Direction = ParameterDirection.Input, Value = isin.Trim() });
            if (groupID.HasValue)
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = groupID.Value });
            else
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pID_GRUPO", OracleType = OracleType.Number, Direction = ParameterDirection.Input, Value = DBNull.Value });
            cmd.Parameters.Add(new OracleParameter { ParameterName = "pCURSOR", OracleType = OracleType.Cursor, Direction = ParameterDirection.Output });

            using (var dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    VAMItem item = new VAMItem();

                    if (!(dr["cod_cli"] is DBNull))
                        item.ClientCode = Convert.ToInt32(dr["cod_cli"]);
                    if (!(dr["dv_cliente"] is DBNull))
                        item.ClientDigit = Convert.ToInt32(dr["dv_cliente"]);
                    if (!(dr["nome_cli"] is DBNull))
                        item.ClientName = Convert.ToString(dr["nome_cli"]);
                    if (!(dr["cod_asse"] is DBNull))
                        item.AssessorCode = Convert.ToInt32(dr["cod_asse"]);
                    if (!(dr["nome_asse"] is DBNull))
                        item.AssessorName = Convert.ToString(dr["nome_asse"]);
                    if (!(dr["tipo_merc"] is DBNull))
                        item.Market = Convert.ToString(dr["tipo_merc"]);
                    if (!(dr["desc_tipo_merc"] is DBNull))
                        item.MarketDescription = Convert.ToString(dr["desc_tipo_merc"]);
                    if (!(dr["cod_cart"] is DBNull))
                        item.Portfolio = Convert.ToInt32(dr["cod_cart"]);
                    if (!(dr["desc_cart"] is DBNull))
                        item.PortfolioDescription = Convert.ToString(dr["desc_cart"]);
                    if (!(dr["prec_exer"] is DBNull))
                        item.Price = Convert.ToDecimal(dr["prec_exer"]);
                    if (!(dr["qtde_tot"] is DBNull))
                        item.TotalQuantity = Convert.ToInt64(dr["qtde_tot"]);
                    if (!(dr["qtde_disp"] is DBNull))
                        item.AvailableQuantity = Convert.ToInt64(dr["qtde_disp"]);
                    if (!(dr["qtde_da1"] is DBNull))
                        item.D1Quantity = Convert.ToInt64(dr["qtde_da1"]);
                    if (!(dr["qtde_da2"] is DBNull))
                        item.D2Quantity = Convert.ToInt64(dr["qtde_da2"]);
                    if (!(dr["qtde_da3"] is DBNull))
                        item.D3Quantity = Convert.ToInt64(dr["qtde_da3"]);
                    if (!(dr["qtde_blqd"] is DBNull))
                        item.BloquedQuantity = Convert.ToInt64(dr["qtde_blqd"]);
                    if (!(dr["qtde_pend"] is DBNull))
                        item.PendingQuantity = Convert.ToInt64(dr["qtde_pend"]);
                    if (!(dr["qtde_alqd"] is DBNull))
                        item.QuantityToSettle = Convert.ToInt64(dr["qtde_alqd"]);
                    if (!(dr["qtde_ngcv"] is DBNull))
                        item.NegotiableQuantity = Convert.ToInt64(dr["qtde_ngcv"]);
                    if (!(dr["data_venc"] is DBNull))
                        item.DueDate = Convert.ToString(dr["data_venc"]);
                    
                    items.Add(item);
                }

                dr.Close();
            }

            return items;
        }

        #endregion

    }
}
