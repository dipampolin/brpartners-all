﻿using System;
using System.Collections.Generic;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.Contracts.ServiceContracts;
using System.ServiceModel.Activation;
using QX3.Portal.Services.Business;
using QX3.Portal.Services.Business.Parameters;

namespace QX3.Portal.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class ClientRegistrationService: IClientRegistrationContract
    {
        private ClientRegistrationMethods clientController;
        private ParametersMethod parameters = new ParametersMethod();

        private ClientRegistrationMethods ClientController
        {
            get
            {
                clientController = clientController ?? new ClientRegistrationMethods();
                return clientController;
            }
        }

        public WcfResponse<List<Client>, int> LoadActiveAndInactiveClients(ClientFilter filter, FilterOptions options, int? userId)
        {
            try
            {
                int count = 0;
                var list = ClientController.LoadActiveAndInactiveClients(filter, options, userId, out count);
                return new WcfResponse<List<Client>, int> { Result = list, Data = count };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<Client>, int> { Result = null, Message = ex.Message };
            }
        }

		public WcfResponse<List<OverdueAndDue>, int> LoadOverdueAndDueRegistration(OverdueAndDueFilter filter, FilterOptions options, int? userId)
		{
			try
			{
				int count = 0;
				var list = ClientController.LoadOverdueAndDueRegistration(filter, options, userId, out count);
				return new WcfResponse<List<OverdueAndDue>, int> { Result = list, Data = count };
			}
			catch (Exception ex)
			{
				return new WcfResponse<List<OverdueAndDue>, int> { Result = null, Message = ex.Message };
			}
		}

        public WcfResponse<List<TransferBovespaItem>> LoadTransfersBovespa(TransferBovespaFilter filter, FilterOptions options, int? userId, out int count)
        {
            try
            {
                return new WcfResponse<List<TransferBovespaItem>> { Result = ClientController.LoadTransfersBovespa(filter, options, userId, out count) };
            }
            catch (Exception ex)
            {
                count = 0;
                return new WcfResponse<List<TransferBovespaItem>> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<List<TransferBMFItem>, int> LoadTransfersBMF(TransferBMFFilter filter, FilterOptions options)
        {
            try
            {
                int count = 0;
                var list = ClientController.LoadTransfersBMF(filter, options, out count);
                return new WcfResponse<List<TransferBMFItem>, int> { Result = list, Data = count };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<TransferBMFItem>, int> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<bool> InsertClientBond(TransferBMFItem transfer)
        {
            try
            {
                return new WcfResponse<bool> { Result = ClientController.InsertClientBond(transfer) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<bool> { Result = false, Message = ex.Message };
            }
        }

        public WcfResponse<Client> CheckClientBMF(int clientCode)
        {
            try
            {
                return new WcfResponse<Client> { Result = ClientController.CheckClientBMF(clientCode) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<Client> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<bool, int> CheckClientPerCPFCGC(string cpccgc)
        {
            try
            {
                int clientId = 0;
                var b = ClientController.CheckClientPerCPFCGC(cpccgc, out clientId);
                return new WcfResponse<bool, int> { Result = b, Data = clientId };
            }
            catch (Exception ex)
            {
                return new WcfResponse<bool, int> { Result = false, Message = ex.Message, Data = 0 };
            }
        }

        public WcfResponse<string> CheckClientType(string clientCodes, string assessors, int? userId) 
        {
            try
            {
                return new WcfResponse<string> { Result = ClientController.CheckClientType(clientCodes, assessors, userId) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<string> { Result = "N", Message = ex.Message };
            }
        }

        public WcfResponse<IndividualPerson> LoadIndividualPerson(int clientCode)
        {
            try
            {
                return new WcfResponse<IndividualPerson> { Result = ClientController.LoadIndividualPerson(clientCode) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<IndividualPerson> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<List<BankAccount>> LoadBanks(string bankName) 
        {
            try
            {
                return new WcfResponse<List<BankAccount>> { Result = ClientController.LoadBanks(bankName) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<BankAccount>> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<ClientOnline> LoadClientOnline(string idClient)
        {
            try
            {
                return new WcfResponse<ClientOnline> { Result = ClientController.LoadClientOnline(idClient) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<ClientOnline> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<ClientOnlineFinancial> LoadClientFinancial(int idClientOnline)
        {
            try
            {
                return new WcfResponse<ClientOnlineFinancial> { Result = ClientController.LoadClientFinancial(idClientOnline) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<ClientOnlineFinancial> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<ClientOnlineRepresentative> LoadRepresentative(int idClientOnline, int id)
        {
            try
            {
                return new WcfResponse<ClientOnlineRepresentative> { Result = ClientController.LoadRepresentative(idClientOnline, id) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<ClientOnlineRepresentative> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<Representatives> LoadRepresentatives(int idClientOnline)
        {
            try
            {
                return new WcfResponse<Representatives> { Result = ClientController.LoadRepresentatives(idClientOnline) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<Representatives> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<ClientOnlineDocuments> LoadClientDocument(int idClientOnline)
        {
            try
            {
                return new WcfResponse<ClientOnlineDocuments> { Result = ClientController.LoadClientDocument(idClientOnline) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<ClientOnlineDocuments> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<ClientOnlineRules> LoadDocumentsRepresentatives(int idDocument)
        {
            try
            {
                return new WcfResponse<ClientOnlineRules> { Result = ClientController.LoadDocumentsRepresentatives(idDocument) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<ClientOnlineRules> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<ClientsOnlineStatus> LoadClientsInFilling()
        {
            try
            {
                return new WcfResponse<ClientsOnlineStatus> { Result = ClientController.LoadClientsInFilling() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<ClientsOnlineStatus> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<ClientsOnlineStatus> LoadClientsInAnalysis()
        {
            try
            {
                return new WcfResponse<ClientsOnlineStatus> { Result = ClientController.LoadClientsInAnalysis() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<ClientsOnlineStatus> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<ClientsOnlineStatus> LoadClientsApproved()
        {
            try
            {
                return new WcfResponse<ClientsOnlineStatus> { Result = ClientController.LoadClientsApproved() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<ClientsOnlineStatus> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<ClientsOnlineStatus> LoadClientsDisapproved()
        {
            try
            {
                return new WcfResponse<ClientsOnlineStatus> { Result = ClientController.LoadClientsDisapproved() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<ClientsOnlineStatus> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<Documents> LoadDocument()
        {
            try
            {
                return new WcfResponse<Documents> { Result = ClientController.LoadDocument() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<Documents> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<Document> LoadDocumentById(int idDocument)
        {
            try
            {
                return new WcfResponse<Document> { Result = ClientController.LoadDocumentById(idDocument) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<Document> { Result = null, Message = ex.Message };
            }
        }
        
        public WcfResponse<int> UpsertDocument(Document document)
        {
            try
            {
                return new WcfResponse<int> { Result = ClientController.UpsertDocument(document) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<int> { Result = 0, Message = ex.Message };
            }
        }

        public WcfResponse<int> RemoveDocument(int idDocument)
        {
            try
            {
                return new WcfResponse<int> { Result = ClientController.RemoveDocument(idDocument) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<int> { Result = 0, Message = ex.Message };
            }
        }

        public WcfResponse<int> InsertUpdateRegistrationClient(ClientOnline client)
        {
            try
            {
                return new WcfResponse<int> { Result = ClientController.InsertUpdateRegistrationClient(client) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<int> { Result = 0, Message = ex.Message };
            }
        }

        public WcfResponse<int> UpsertDocumentRepresentative(ClientOnlineRules clientOnlineRules)
        {
            try
            {
                return new WcfResponse<int> { Result = ClientController.UpsertDocumentRepresentative(clientOnlineRules) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<int> { Result = 0, Message = ex.Message };
            }
        }

        public WcfResponse<int> SubmitClientOnline(int idClientOnline)
        {
            try
            {
                return new WcfResponse<int> { Result = ClientController.SubmitClientOnline(idClientOnline) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<int> { Result = 0, Message = ex.Message };
            }
        }

        public WcfResponse<int> ApproveClientOnline(int idClientOnline)
        {
            try
            {
                return new WcfResponse<int> { Result = ClientController.ApproveClientOnline(idClientOnline) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<int> { Result = 0, Message = ex.Message };
            }
        }

        public WcfResponse<int> DisapproveClientOnline(int idClientOnline)
        {
            try
            {
                return new WcfResponse<int> { Result = ClientController.DisapproveClientOnline(idClientOnline) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<int> { Result = 0, Message = ex.Message };
            }
        }

        public WcfResponse<int> InsertUpdateClientFinancial(ClientOnlineFinancial clientFinancial)
        {
            try
            {
                return new WcfResponse<int> { Result = ClientController.InsertUpdateClientFinancial(clientFinancial) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<int> { Result = 0, Message = ex.Message };
            }
        }

        public WcfResponse<int> UpsertDocumentClientOnline(ClientOnlineDocuments documents)
        {
            try
            {
                return new WcfResponse<int> { Result = ClientController.UpsertDocumentClientOnline(documents) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<int> { Result = 0, Message = ex.Message };
            }
        }

        public WcfResponse<bool> DeleteAssignById(int idFile)
        {
            try
            {
                return new WcfResponse<bool> { Result = ClientController.DeleteAssignById(idFile) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<bool> { Result = false, Message = ex.Message };
            }
        }

        public WcfResponse<bool> DeleteRepresentative(int idClientOnline, int idRepresentative)
        {
            try
            {
                return new WcfResponse<bool> { Result = ClientController.DeleteRepresentative(idClientOnline, idRepresentative) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<bool> { Result = false, Message = ex.Message };
            }
        }

        public WcfResponse<ClientAssign> GetAssignById(int id)
        {
            try
            {
                return new WcfResponse<ClientAssign> { Result = ClientController.GetAssignById(id) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<ClientAssign> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<ClientDocument> GetModelDocumentById(int id)
        {
            try
            {
                return new WcfResponse<ClientDocument> { Result = ClientController.GetModelDocumentById(id) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<ClientDocument> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<ClientDocument> GetDocumentById(int id)
        {
            try
            {
                return new WcfResponse<ClientDocument> { Result = ClientController.GetDocumentById(id) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<ClientDocument> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<int> UpsertRepresentative(ClientOnlineRepresentative representative)
        {
            try
            {
                return new WcfResponse<int> { Result = ClientController.UpsertRepresentative(representative) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<int> { Result = 0, Message = ex.Message };
            }
        }

        #region PARAMETROS

        public WcfResponse<List<Parameters>> GetState()
        {
            try
            {
                return new WcfResponse<List<Parameters>> { Result = parameters.GetState() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<Parameters>> { Message = ex.Message };
            }
        }

        public WcfResponse<List<Parameters>> GetCountries()
        {
            try
            {
                return new WcfResponse<List<Parameters>> { Result = parameters.GetCountries() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<Parameters>> { Message = ex.Message };
            }
        }

        public WcfResponse<List<Parameters>> GetAddressTypes()
        {
            try
            {
                return new WcfResponse<List<Parameters>> { Result = parameters.GetAddressTypes() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<Parameters>> { Message = ex.Message };
            }
        }

        public WcfResponse<List<Parameters>> GetFinalities()
        {
            try
            {
                return new WcfResponse<List<Parameters>> { Result = parameters.GetFinalities() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<Parameters>> { Message = ex.Message };
            }
        }

        public WcfResponse<List<Parameters>> GetActivities()
        {
            try
            {
                return new WcfResponse<List<Parameters>> { Result = parameters.GetActivities() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<Parameters>> { Message = ex.Message };
            }
        }

        public WcfResponse<List<Parameters>> GetOccupations()
        {
            try
            {
                return new WcfResponse<List<Parameters>> { Result = parameters.GetOccupations() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<Parameters>> { Message = ex.Message };
            }
        }

        public WcfResponse<List<Parameters>> GetSizes()
        {
            try
            {
                return new WcfResponse<List<Parameters>> { Result = parameters.GetSizes() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<Parameters>> { Message = ex.Message };
            }
        }

        public WcfResponse<List<Parameters>> GetProfessions()
        {
            try
            {
                return new WcfResponse<List<Parameters>> { Result = parameters.GetProfessions() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<Parameters>> { Message = ex.Message };
            }
        }

        public WcfResponse<List<Parameters>> GetMaritalStatus()
        {
            try
            {
                return new WcfResponse<List<Parameters>> { Result = parameters.GetMaritalStatus() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<Parameters>> { Message = ex.Message };
            }
        }

        public WcfResponse<List<Parameters>> GetSex()
        {
            try
            {
                return new WcfResponse<List<Parameters>> { Result = parameters.GetSex() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<Parameters>> { Message = ex.Message };
            }
        }

        public WcfResponse<List<Parameters>> GetStatus()
        {
            try
            {
                return new WcfResponse<List<Parameters>> { Result = parameters.GetStatus() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<Parameters>> { Message = ex.Message };
            }
        }

        public WcfResponse<List<Parameters>> GetDocumentTypes()
        {
            try
            {
                return new WcfResponse<List<Parameters>> { Result = parameters.GetDocumentTypes() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<Parameters>> { Message = ex.Message };
            }
        }

        public WcfResponse<List<Parameters>> GetEconomicGroups()
        {
            try
            {
                return new WcfResponse<List<Parameters>> { Result = parameters.GetEconomicGroups() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<Parameters>> { Message = ex.Message };
            }
        }

        public WcfResponse<List<Parameters>> GetClassifications()
        {
            try
            {
                return new WcfResponse<List<Parameters>> { Result = parameters.GetClassifications() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<Parameters>> { Message = ex.Message };
            }
        }

        public WcfResponse<List<Parameters>> GetEconomicActivities()
        {
            try
            {
                return new WcfResponse<List<Parameters>> { Result = parameters.GetEconomicActivities() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<Parameters>> { Message = ex.Message };
            }
        }

        public WcfResponse<List<Parameters>> GetRegisterType()
        {
            try
            {
                return new WcfResponse<List<Parameters>> { Result = parameters.GetRegisterType() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<Parameters>> { Message = ex.Message };
            }
        }

        public WcfResponse<List<Parameters>> GetClientTypes()
        {
            try
            {
                return new WcfResponse<List<Parameters>> { Result = parameters.GetClientTypes() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<Parameters>> { Message = ex.Message };
            }
        }

        public WcfResponse<List<Parameters>> GetCategories()
        {
            try
            {
                return new WcfResponse<List<Parameters>> { Result = parameters.GetCategories() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<Parameters>> { Message = ex.Message };
            }
        }

        public WcfResponse<List<Parameters>> GetManagers()
        {
            try
            {
                return new WcfResponse<List<Parameters>> { Result = parameters.GetManagers() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<Parameters>> { Message = ex.Message };
            }
        }

        public WcfResponse<List<Parameters>> GetNaturalness()
        {
            try
            {
                return new WcfResponse<List<Parameters>> { Result = parameters.GetNaturalness() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<Parameters>> { Message = ex.Message };
            }
        }

        public WcfResponse<List<Parameters>> GetNacionality()
        {
            try
            {
                return new WcfResponse<List<Parameters>> { Result = parameters.GetNacionality() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<Parameters>> { Message = ex.Message };
            }
        }

        public WcfResponse<List<Parameters>> GetTelephoneTypes()
        {
            try
            {
                return new WcfResponse<List<Parameters>> { Result = parameters.GetTelephoneTypes() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<Parameters>> { Message = ex.Message };
            }
        }

        public WcfResponse<List<Parameters>> GetMatrimonialRegime()
        {
            try
            {
                return new WcfResponse<List<Parameters>> { Result = parameters.GetMatrimonialRegime() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<Parameters>> { Message = ex.Message };
            }
        }

        public WcfResponse<List<Parameters>> GetChoice()
        {
            try
            {
                return new WcfResponse<List<Parameters>> { Result = parameters.GetChoice() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<Parameters>> { Message = ex.Message };
            }
        }

        public WcfResponse<List<Parameters>> GetFinancialProduct()
        {
            try
            {
                return new WcfResponse<List<Parameters>> { Result = parameters.GetFinancialProduct() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<Parameters>> { Message = ex.Message };
            }
        }

        public WcfResponse<List<Parameters>> GetFiscalNature()
        {
            try
            {
                return new WcfResponse<List<Parameters>> { Result = parameters.GetFiscalNature() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<Parameters>> { Message = ex.Message };
            }
        }

        public WcfResponse<List<Parameters>> GetAccountFinality()
        {
            try
            {
                return new WcfResponse<List<Parameters>> { Result = parameters.GetAccountFinality() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<Parameters>> { Message = ex.Message };
            }
        }

        public WcfResponse<List<Parameters>> GetAccountUtility()
        {
            try
            {
                return new WcfResponse<List<Parameters>> { Result = parameters.GetAccountUtility() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<Parameters>> { Message = ex.Message };
            }
        }

        public WcfResponse<List<Parameters>> GetAccountType()
        {
            try
            {
                return new WcfResponse<List<Parameters>> { Result = parameters.GetAccountType() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<Parameters>> { Message = ex.Message };
            }
        }

        public WcfResponse<List<Parameters>> GetAccountTypeBank()
        {
            try
            {
                return new WcfResponse<List<Parameters>> { Result = parameters.GetAccountTypeBank() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<Parameters>> { Message = ex.Message };
            }
        }

        public WcfResponse<List<Parameters>> GetBank()
        {
            try
            {
                return new WcfResponse<List<Parameters>> { Result = parameters.GetBank() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<Parameters>> { Message = ex.Message };
            }
        }

        public WcfResponse<List<Parameters>> GetRepresentationStyle()
        {
            try
            {
                return new WcfResponse<List<Parameters>> { Result = parameters.GetRepresentationStyle() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<Parameters>> { Message = ex.Message };
            }
        }

        public WcfResponse<List<Parameters>> GetRepresentationProfile()
        {
            try
            {
                return new WcfResponse<List<Parameters>> { Result = parameters.GetRepresentationProfile() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<Parameters>> { Message = ex.Message };
            }
        }

        public WcfResponse<List<Parameters>> GetQualifications()
        {
            try
            {
                return new WcfResponse<List<Parameters>> { Result = parameters.GetQualifications() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<Parameters>> { Message = ex.Message };
            }
        }

        public WcfResponse<List<Parameters>> GetRulesTypes()
        {
            try
            {
                return new WcfResponse<List<Parameters>> { Result = parameters.GetRulesTypes() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<Parameters>> { Message = ex.Message };
            }
        }

        public WcfResponse<List<Parameters>> GetExpirationDateDocument()
        {
            try
            {
                return new WcfResponse<List<Parameters>> { Result = parameters.GetExpirationDateDocument() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<Parameters>> { Message = ex.Message };
            }
        }

        #endregion
    }
}
