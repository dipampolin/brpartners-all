﻿using System;
using QX3.Spinnex.Common.Services.Logging;
using System.Xml;
using System.IO;
using System.Text;

namespace QX3.Portal.Services
{
    public class WcfLogger : ILogger
    {
        public void Init(string config)
        {

        }

        public bool PersistLog(LogContent content)
        {
            if (content == null)
                return false;

            try
            {
                var data = content.GetLogData();
                if (data == null)
                    return false;

                var doc = new XmlDocument();
                MemoryStream ms = new MemoryStream();
                XmlWriter xw = new XmlTextWriter(ms, Encoding.Default);
                data.Save(xw);
                xw.Flush();
                ms.Position = 0;
                doc.Load(ms);
                xw.Close();
                ms.Close();


                var service = new QX3.Portal.Services.Business.Log.Log();

                StringWriter sw = new StringWriter();
                XmlTextWriter tx = new XmlTextWriter(sw);
                doc.WriteTo(tx);

                service.CreateTraceRecord(sw.ToString());                

                return true;
            }
            catch (Exception ex)
            {
                content.AddError(ex);
                return false;
            }

        }
    }
}
