﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Activation;
using QX3.Portal.Contracts.ServiceContracts;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.Services.Business;

namespace QX3.Portal.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class BMFService: IBMFContract
    {
        private BMFMethods bmfController;

        private BMFMethods BMFController
        {
            get
            {
                bmfController = bmfController ?? new BMFMethods();
                return bmfController;
            }
        }

        public WcfResponse<List<BovespaBrokerageReport>, int> LoadBovespaBrokerageReport(BrokerageReportFilter filter)
        {
            try
            {
                var list = BMFController.LoadBovespaBrokerageReport(filter);
                return new WcfResponse<List<BovespaBrokerageReport>, int> { Result = list, Data = 0 };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<BovespaBrokerageReport>, int> { Result = null, Message = ex.Message, Data = 0 };
            }
        }

        public WcfResponse<List<BMFBrokerageReport>, int> LoadBMFBrokerageReport(BrokerageReportFilter filter)
        {
            try
            {
                var list = BMFController.LoadBMFBrokerageReport(filter);
                return new WcfResponse<List<BMFBrokerageReport>, int> { Result = list, Data = 0 };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<BMFBrokerageReport>, int> { Result = null, Message = ex.Message, Data = 0 };
            }
        }

        public WcfResponse<BrokerageRange, int> LoadBrokerageRange(BrokerageRangeFilter filter, FilterOptions filterOptions, out bool hasAnterior, out bool hasPosterior)
        {
            try
            {
                int count = 0;
                var list = BMFController.LoadBrokerageRange(filter, filterOptions, out count, out hasAnterior, out hasPosterior);
                return new WcfResponse<BrokerageRange, int> { Result = list, Data = count };
            }
            catch (Exception ex)
            {
                hasAnterior = false; hasPosterior = false;
                return new WcfResponse<BrokerageRange, int> { Result = null, Message = ex.Message, Data = 0 };
            }
        }
    }
}
