﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Activation;
using System.Text;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.Contracts.ServiceContracts;
using QX3.Portal.Services.Business;
using QX3.Portal.Services.SagazWebService;
using System.Configuration;
using System.IO;
using System.Net.Mail;
using QX3.Spinnex.Common.Services.Mail;
using System.Web;

namespace QX3.Portal.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class FundService: IFundContract
    {
        SagazWebService.SiteVotorantimSoapClient client;

        SagazWebService.SiteVotorantimSoapClient controller
        {
            get
            {
                if (client == null)
                    client = new SagazWebService.SiteVotorantimSoapClient();
                return client;
            }
        }

        VotoratimSt4.SiteVotorantimSoapClient st4Client;
        VotoratimSt4.SiteVotorantimSoapClient st4Controller
        {
            get
            {
                if (st4Client == null)
                    st4Client = new VotoratimSt4.SiteVotorantimSoapClient();
                return st4Client;
            }
        }

        public WcfResponse<List<Fund>> FN_LoadFunds()
        {
            try
            {
                var result = controller.FN_LoadFunds().Result;
                return new WcfResponse<List<Fund>>() { Result = (result == null) ? null : result.ToList() };
            }
            catch (Exception ex) 
            {
                return new WcfResponse<List<Fund>>() { Result = null, Message = ex.Message + " " + ex.StackTrace };
            }
        }

        public WcfResponse<FundDetails> FN_LoadFund(int id)
        {
            try
            {
                return new WcfResponse<FundDetails>() { Result = controller.FN_LoadFund(id).Result };
            }
            catch (Exception ex)
            {
                return new WcfResponse<FundDetails>() { Result = null, Message = ex.Message + " " + ex.StackTrace };
            }

        }

        public WcfResponse<List<Application>> FN_LoadClientApplicationsPerFund(int operationalCode, int fundId)
        {
            try
            {
                var result = controller.FN_LoadClientApplicationsPerFund(operationalCode, fundId).Result;
                return new WcfResponse<List<Application>>() { Result = (result == null) ? null : result.ToList() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<Application>>() { Result = null, Message = ex.Message + " " + ex.StackTrace };
            }
        }

        public WcfResponse<decimal> FN_LoadClientFundBalance(int operationalCode, int fundCode)
        {
            try
            {
                var result = controller.FN_LoadClientFundBalance(operationalCode, fundCode).Result;
                return new WcfResponse<decimal>() { Result = result };
            }
            catch (Exception ex)
            {
                return new WcfResponse<decimal>() { Result = 0, Message = ex.Message + " " + ex.StackTrace };
            }
        }

        public WcfResponse<List<Redemption>> FN_LoadClientRedemptionsPerFund(int operationalCode, int fundId)
        {
            try
            {
                var result = controller.FN_LoadClientRedemptionsPerFund(operationalCode, fundId).Result;
                return new WcfResponse<List<Redemption>>() { Result = (result == null) ? null : result.ToList() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<Redemption>>() { Result = null, Message = ex.Message + " " + ex.StackTrace };
            }
        }

        public WcfResponse<bool> FN_InsertApplication(Application obj)
        {
            try
            {
                var result = controller.FN_InsertApplication(obj).Result;
                return new WcfResponse<bool>() { Result = result};
            }
            catch (Exception ex)
            {
                return new WcfResponse<bool>() { Result = false, Message = ex.Message + " " + ex.StackTrace };
            }
        }

        public WcfResponse<bool> FN_InsertRedemption(Redemption obj)
        {
            try
            {
                var result = controller.FN_InsertRedemption(obj).Result;
                return new WcfResponse<bool>() { Result = result };
            }
            catch (Exception ex)
            {
                return new WcfResponse<bool>() { Result = false, Message = ex.Message + " " + ex.StackTrace };
            }
        }

        public WcfResponse<List<FundStatementItem>> FN_LoadStatement(DateTime startDate, DateTime endDate, int operationalCode, int fundCode)
        {
            try
            {
                var result = controller.FN_LoadStatement(startDate, endDate, operationalCode, fundCode).Result;
                return new WcfResponse<List<FundStatementItem>>() { Result = (result == null) ? null : result.ToList() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<FundStatementItem>>() { Result = null, Message = ex.Message + " " + ex.StackTrace };
            }
        }

        public WcfResponse<FundDetails> FN_LoadFundDetails(int? operationalCode, int fundCode)
        {
            try
            {
                var result = controller.FN_LoadFundDetails(operationalCode, fundCode).Result;
                return new WcfResponse<FundDetails>() { Result = result };
            }
            catch (Exception ex)
            {
                return new WcfResponse<FundDetails>() { Result = null, Message = ex.Message + " " + ex.StackTrace };
            }
        }

        public WcfResponse<List<Fund>> FN_LoadClientFunds(int operationalCode)
        {
            try
            {
                var result = controller.FN_LoadClientFunds(operationalCode).Result;
                return new WcfResponse<List<Fund>>() { Result = (result == null) ? null : result.ToList() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<Fund>>() { Result = null, Message = ex.Message + " " + ex.StackTrace };
            }
        }

        public WcfResponse<List<Fund>> FN_LoadClientCurrentFunds(int operationalCode)
        {
            try
            {
                var result = controller.FN_LoadClientCurrentFunds(operationalCode).Result;
                return new WcfResponse<List<Fund>>() { Result = (result == null) ? null : result.ToList() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<Fund>>() { Result = null, Message = ex.Message + " " + ex.StackTrace };
            }
        }

        public WcfResponse<FundBalance> FN_LoadBalance(int operationalCode, int fundCode)
        {
            try
            {
                var result = controller.FN_LoadBalance(operationalCode, fundCode).Result;
                return new WcfResponse<FundBalance>() { Result = result };
            }
            catch (Exception ex)
            {
                return new WcfResponse<FundBalance>() { Result = null, Message = ex.Message + " " + ex.StackTrace };
            }
        }

        public WcfResponse<List<FundBalanceChartItem>> FN_LoadBalanceChart(int operationalCode)
        {
            try
            {
                var result = controller.FN_LoadBalanceChart(operationalCode).Result;
                return new WcfResponse<List<FundBalanceChartItem>>() { Result = (result == null) ? null : result.ToList() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<FundBalanceChartItem>>() { Result = null, Message = ex.Message + " " + ex.StackTrace };
            }
        }

        public WcfResponse<List<FundBalancePendingApplicationItem>> FN_LoadBalancePendingApplications(int operationalCode)
        {
            try
            {
                var result = controller.FN_LoadBalancePendingApplications(operationalCode).Result;
                return new WcfResponse<List<FundBalancePendingApplicationItem>>() { Result = (result == null) ? null : result.ToList() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<FundBalancePendingApplicationItem>>() { Result = null, Message = ex.Message + " " + ex.StackTrace };
            }
        }

        public WcfResponse<bool> FN_InsertAccession(int operationalCode, int fundCode, int modalityId)
        {
            try
            {
                var result = controller.FN_InsertAccession(operationalCode, fundCode, modalityId).Result;
                return new WcfResponse<bool>() { Result = result };
            }
            catch (Exception ex)
            {
                return new WcfResponse<bool>() { Result = false, Message = ex.Message + " " + ex.StackTrace };
            }
        }

        public WcfResponse<List<FundDetails>> FN_SuggestFundsToInvest(int operationalCode, decimal balance)
        {
            try
            {
                var result = controller.FN_SuggestFundsToInvest(operationalCode, balance).Result;
                return new WcfResponse<List<FundDetails>>() { Result = (result == null) ? null : result.ToList() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<FundDetails>>() { Result = null, Message = ex.Message + " " + ex.StackTrace };
            }
        }

        public WcfResponse<decimal> FN_LoadBlockedBalance(int operationalCode)
        {
            try
            {
                var result = controller.FN_LoadBlockedBalance(operationalCode).Result;
                return new WcfResponse<decimal>() { Result = result };
            }
            catch (Exception ex)
            {
                return new WcfResponse<decimal>() { Result = 0, Message = ex.Message + " " + ex.StackTrace };
            }
        }

        public WcfResponse<byte[]> FN_DownloadDocument(Int32 id)
        {
            try
            {
                var result = controller.FN_DownloadDocument(id).Result;
                return new WcfResponse<byte[]>() { Result = result };
            }
            catch (Exception ex)
            {
                return new WcfResponse<byte[]>() { Result = null, Message = ex.Message + " " + ex.StackTrace };
            }
        }

        public WcfResponse<FinancialPositionDataItem> FN_GetUserBalance(FundClient client) 
        {
            try
            {
                var fn = new FinancialPositionDataItem();
                if (String.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["UseMockForUserBalance"]))
                {
                    //var result = st4Controller.GetFinanceiroDisponivelCliente(id.ToString().PadLeft(11, '0'));
                    var result = st4Controller.GetFinanceiroDisponivelCliente(client.FormattedClientCode);
                    //fn.CurrentAccountInitialBalance = Convert.ToDecimal(result[3]);
                    
                    fn.CurrentAccountInitialBalance = Convert.ToDecimal(result[14]);
                    fn.DailyLimit = Convert.ToDecimal(result[4]);
                    fn.ProjBalanceD1 = Convert.ToDecimal(result[5]);
                    fn.ProjBalanceD2 = Convert.ToDecimal(result[6]);
                    fn.ProjBalanceD3 = Convert.ToDecimal(result[7]);
                    fn.AskPendingValue = Convert.ToDecimal(result[8]);
                    fn.BidPendingValue = Convert.ToDecimal(result[9]);
                    fn.AskExecutedValue = Convert.ToDecimal(result[10]);
                    fn.BidExecutedValue = Convert.ToDecimal(result[11]);
                    //fn.AccountType = accountType;
                    fn.AvailableForWithDrawl = Convert.ToDecimal(result[14]);
                    fn.AvailableForStocks = Convert.ToDecimal(result[0]);
                    fn.AvailableForOptions = Convert.ToDecimal(result[1]);
                }
                else
                {
                    //Mock
                    fn.CurrentAccountInitialBalance = 2500;

                    fn.ProjBalanceD1 = 500;
                    fn.ProjBalanceD2 = 1000;
                    fn.ProjBalanceD3 = 1500;
                }
                return new WcfResponse<FinancialPositionDataItem>() { Result = fn };
            }
            catch (Exception ex) 
            {
                return new WcfResponse<FinancialPositionDataItem>() { Result = null, Message = ex.Message + " " + ex.StackTrace };
            }
        }

        public WcfResponse<bool> FN_SendTermUser(int documentId, string fileName, int fundCode, FundClient client) 
        {
            try
            {
                List<KeyValuePair<string, object>> messageValues = new List<KeyValuePair<string, object>>();
                messageValues.Add(new KeyValuePair<string, object>("strNome", client.ClientName));

                messageValues.Add(new KeyValuePair<string, object>("strUrlSite", ConfigurationManager.AppSettings["Subscription.RequestRights.SiteUrl"]));


                var response = this.FN_DownloadDocument(documentId);
                var responseFundDetails = this.FN_LoadFundDetails(client.ClientCode, fundCode).Result;
                
                if (responseFundDetails == null)
                    return new WcfResponse<bool> { Result = false, Message = "Fundo não encontrado." };

                messageValues.Add(new KeyValuePair<string, object>("strFundName", responseFundDetails.Name));

                string subject = "Termo de Adesão: " + responseFundDetails.Name;
                string emailTo = string.IsNullOrEmpty(ConfigurationManager.AppSettings["Subscription.RequestRights.EmailSendToMock"])
                    ? String.Format("{0}<{1}>", client.ClientName, client.ClientEmail)
                    : ConfigurationManager.AppSettings["Subscription.RequestRights.EmailSendToMock"];
                string emailReplyTo = ConfigurationManager.AppSettings["Subscription.RequestRights.EmailReplyTo"];

                Template template = new Template(HttpContext.Current.Server.MapPath("~/Template/FundTerm.htm"));
                template.ReplaceVars(messageValues.ToArray());
                string corpo = template.Body;

                MailAddress from = new MailAddress(emailReplyTo);
                MailAddress To = new MailAddress(emailTo);
                MailMessage msg = new MailMessage();

                msg.From = from;
                foreach (string s in emailTo.Split(';'))
                {
                    int i = msg.To.IndexOf(new MailAddress(s));
                    if ((s.Length > 0) && (i == -1))
                        msg.To.Add(s);
                }
                msg.Subject = subject;
                msg.Body = corpo;
                msg.IsBodyHtml = true;

                MemoryStream mst = new MemoryStream(response.Result);

                msg.Attachments.Add(new Attachment(mst, fileName + ".pdf"));

                SmtpClient smtp = new SmtpClient();

                smtp.Send(msg);

                return new WcfResponse<bool> { Result = true, Message = "Email enviado com sucesso." };

            }
            catch (Exception ex)
            {
                return new WcfResponse<bool> { Result = false, Message = ex.Message };
            }
        }                
    }
}
