﻿using System;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.Contracts.ServiceContracts;
using System.ServiceModel.Activation;
using System.Collections.Generic;
using QX3.Portal.Services.Business.OnlinePortfolio;

namespace QX3.Portal.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class OnlinePortfolioService : IOnlinePortfolioContract
    {
        private OnlinePortfolio onlinePortfolioController;

        private OnlinePortfolio OnlinePortfolioController
        {
            get
            {
                onlinePortfolioController = onlinePortfolioController ?? new OnlinePortfolio();
                return onlinePortfolioController;
            }
        }

        public WcfResponse<bool> InsertContact(int clientCode, PortfolioContactItem contact)
        {
            try
            {
                return new WcfResponse<bool> { Result = OnlinePortfolioController.InsertContact(clientCode, contact) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<bool> { Result = false, Message = ex.Message };
            }
        }

        public WcfResponse<PortfolioContact> LoadClientContacts(int clientCode)
        {
            try
            {
                return new WcfResponse<PortfolioContact> { Result = OnlinePortfolioController.LoadClientContacts(clientCode) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<PortfolioContact> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<List<CommonType>> SearchClients(string nameFilter, long operatorId, int maxItems)
        {
            try
            {
                return new WcfResponse<List<CommonType>> { Result = OnlinePortfolioController.SearchClients(nameFilter, operatorId, maxItems) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<CommonType>> { Result = null, Message = ex.Message };
            }
        }


        public WcfResponse<PortfolioClient> LoadClientName(int clientId)
        {
            try
            {
                return new WcfResponse<PortfolioClient> { Result = OnlinePortfolioController.LoadClientName(clientId) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<PortfolioClient> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<bool> UpdateObservation(int clientId, string observation)
        {
            try
            {
                return new WcfResponse<bool> { Result = OnlinePortfolioController.UpdateObservation(clientId, observation) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<bool> { Result = false, Message = ex.Message };
            }
        }

        public WcfResponse<string> LoadObservation(int clientId)
        {
            try
            {
                return new WcfResponse<string> { Result = OnlinePortfolioController.LoadObservation(clientId) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<string> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<List<Int32>> LoadOperatorClients(int operatorID, int filter)
        {
            try
            {
                return new WcfResponse<List<Int32>> { Result = OnlinePortfolioController.LoadOperatorClients(operatorID, filter) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<Int32>> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<bool> InsertClient(PortfolioClient obj, bool isUpdate)
        {
            try
            {
                return new WcfResponse<bool> { Result = OnlinePortfolioController.InsertClient(obj,isUpdate) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<bool> { Result = false, Message = ex.Message };
            }
        }

        public WcfResponse<bool> DeleteClient(string code)
        {
            try
            {
                return new WcfResponse<bool> { Result = OnlinePortfolioController.DeleteClient(code) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<bool> { Result = false, Message = ex.Message };
            }
        }

        public WcfResponse<List<PortfolioClient>> LoadPortfolioClients(PortfolioClientFilter filter)
        {
            try
            {
                return new WcfResponse<List<PortfolioClient>> { Result = OnlinePortfolioController.LoadPortfolioClients(filter) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<PortfolioClient>> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<List<CommonType>> LoadCalculationTypes()
        {
            try
            {
                return new WcfResponse<List<CommonType>>  { Result = OnlinePortfolioController.LoadCalculationTypes() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<CommonType>> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<PortfolioPosition> LoadClientPosition(int clientCode, long operatorID)
        {
            try
            {
                return new WcfResponse<PortfolioPosition> { Result = OnlinePortfolioController.LoadClientPortfolioPosition(clientCode, operatorID, "N") };
            }
            catch (Exception ex)
            {
                return new WcfResponse<PortfolioPosition> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<PortfolioClient> LoadPortfolioClient(string clientCode)
        {
            try
            {
                return new WcfResponse<PortfolioClient> { Result = OnlinePortfolioController.LoadPortfolioClient(clientCode) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<PortfolioClient> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<PortfolioClientSearchClients> LoadSinacorClients(string term)
        {
            try
            {
                return new WcfResponse<PortfolioClientSearchClients> { Result = OnlinePortfolioController.LoadSinacorClients(term) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<PortfolioClientSearchClients> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<PortfolioPosition> SearchClientPortfolioPosition(string clientCode, string operatorCode)
        {
            try
            {
                return new WcfResponse<PortfolioPosition> { Result = OnlinePortfolioController.SearchClientPortfolioPosition(clientCode, operatorCode) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<PortfolioPosition> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<PortfolioPosition> LoadClientHistory(int clientCode, long operatorID)
        {
            try
            {
                return new WcfResponse<PortfolioPosition> { Result = OnlinePortfolioController.LoadClientPortfolioPosition(clientCode, operatorID, "S") };
            }
            catch (Exception ex)
            {
                return new WcfResponse<PortfolioPosition> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<bool> DeletePosition(string id)
        {
            try
            {
                return new WcfResponse<bool> { Result = OnlinePortfolioController.DeletePosition(id) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<bool> { Result = false, Message = ex.Message };
            }
        }

        public WcfResponse<bool> InsertPosition(PortfolioPosition obj, string sourcePosition)
        {
            try
            {
                return new WcfResponse<bool> { Result = OnlinePortfolioController.InsertPosition(obj, sourcePosition) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<bool> { Result = false, Message = ex.Message };
            }
        }

    }
}
