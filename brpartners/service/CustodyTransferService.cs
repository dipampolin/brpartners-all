﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Activation;
using QX3.Portal.Contracts.ServiceContracts;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.Services.Business;

namespace QX3.Portal.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class CustodyTransferService: ICustodyTransferContract
    {
        private CustodyTransferMethods custodyTransferController;

        private CustodyTransferMethods CustodyTransferController
        {
            get
            {
                custodyTransferController = custodyTransferController ?? new CustodyTransferMethods();
                return custodyTransferController;
            }
        }

        public WcfResponse<List<CustodyTransfer>, int> LoadCustodyTransfers(CustodyTransferFilter filter, FilterOptions options, int? userId)
        {
            try
            {
                int count = 0;
                var list = CustodyTransferController.LoadCustodyTransfers(filter, options, userId, out count);
                return new WcfResponse<List<CustodyTransfer>, int> { Result = list, Data = count};
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<CustodyTransfer>, int> { Result = null, Message = ex.Message, Data = 0 };
            }    
        }

        public WcfResponse<bool> UpdateCustodyTransferStatus(List<CustodyTransfer> requestList)
        {
            try
            {
                var ret = CustodyTransferController.UpdateCustodyTransferStatus(requestList);
                return new WcfResponse<bool> { Result = ret};
            }
            catch (Exception ex)
            {
                return new WcfResponse<bool> { Result = false, Message = ex.Message };
            }
        }

		public WcfResponse<CustodyTransferReport> LoadCustodyTransferReport(string fromDate, string toDate)
		{
			try
			{
				var report = CustodyTransferController.LoadCustodyTransferReport(fromDate, toDate);
				return new WcfResponse<CustodyTransferReport> { Result = report };
			}
			catch (Exception ex)
			{
				return new WcfResponse<CustodyTransferReport> { Result = null, Message = ex.Message };
			}
		}

        public WcfResponse<bool> DeleteCustodyTransfer(List<CustodyTransfer> requestList)
        {
            try
            {
                var ret = CustodyTransferController.DeleteCustodyTransfer(requestList);
                return new WcfResponse<bool> { Result = ret };
            }
            catch (Exception ex)
            {
                return new WcfResponse<bool> { Result = false, Message = ex.Message };
            }
        }

        public WcfResponse<List<CustodyTransferDetails>> LoadClientPosition(CustodyTransferFilter filter, int? userId) 
        {
            try
            {
                var ret = CustodyTransferController.LoadClientPosition(filter, userId);
                return new WcfResponse<List<CustodyTransferDetails>> { Result = ret };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<CustodyTransferDetails>> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<bool, int> InsertCustodyTransfer(CustodyTransfer request) 
        {
            try
            {
                int requestId = 0;
                var result = CustodyTransferController.InsertCustodyTransfer(request, out requestId);
                return new WcfResponse<bool, int> { Result = result, Data = requestId };
            }
            catch (Exception ex)
            {
                return new WcfResponse<bool, int> { Result = false, Message = ex.Message, Data = 0 };
            } 
        }

        public WcfResponse<bool,int> EditCustodyTransfer(CustodyTransfer request)
        {
            try
            {
                var result = CustodyTransferController.EditCustodyTransfer(request);
                return new WcfResponse<bool,int> { Result = result };
            }
            catch (Exception ex)
            {
                return new WcfResponse<bool,int> { Result = false, Message = ex.Message };
            }
        }

        public WcfResponse<int> GetPendentCustodyTransfers(CustodyTransferFilter filter, int? userId) 
        {
            try
            {
                var result = CustodyTransferController.GetPendentCustodyTransfers(filter, userId);
                return new WcfResponse<int> { Result = result };
            }
            catch (Exception ex)
            {
                return new WcfResponse<int> { Result = 0, Message = ex.Message };
            }
        }
    }
}
