﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Activation;
using System.Text;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.Contracts.ServiceContracts;
using QX3.Portal.Services.Business;

namespace QX3.Portal.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class SubscriptionService: ISubscriptionContract
    {
        private SubscriptionMethods methods;

        private SubscriptionMethods SubscriptionController {
            get 
            {
                methods = methods ?? new SubscriptionMethods();
                return methods;
            }
        }

        #region Subscrições

        public WcfResponse<List<Subscription>, int> LoadSubscriptions(Subscription filter, FilterOptions options, bool getProspects)
        {
            try
            {
                int count = 0;
                var list = SubscriptionController.LoadSubscriptions(filter, options, getProspects, out count);
                return new WcfResponse<List<Subscription>, int>() { Result = list, Data = count };
            }
            catch (Exception e) 
            {
                return new WcfResponse<List<Subscription>, int>() { Result = null, Data = 0, Message = e.Message };
            }
        }

        public WcfResponse<bool, int> SubscriptionRequest(Subscription request)
        {
            try
            {
                int requestId = 0;
                bool response = SubscriptionController.SubscriptionRequest(request, out requestId);
                return new WcfResponse<bool, int>() { Result = response, Data = requestId };
            }
            catch (Exception e)
            {
                return new WcfResponse<bool, int>() { Result = false, Message = e.Message, Data = 0 };
            }
        }

        public WcfResponse<bool, int> SubscriptionUpdate(Subscription request)
        {
            try
            {
                int requestId = 0;
                bool response = SubscriptionController.SubscriptionUpdate(request, out requestId);
                return new WcfResponse<bool, int>() { Result = response, Data = requestId };
            }
            catch (Exception e)
            {
                return new WcfResponse<bool, int>() { Result = false, Message = e.Message, Data = 0 };
            }
        }

        public WcfResponse<bool, int> SubscriptionDelete(Subscription request)
        {
            try
            {
                int requestId = 0;
                bool response = SubscriptionController.SubscriptionDelete(request, out requestId);
                return new WcfResponse<bool, int>() { Result = response, Data = requestId };
            }
            catch (Exception e)
            {
                return new WcfResponse<bool, int>() { Result = false, Message = e.Message, Data = 0 };
            }
        }

        public WcfResponse<bool, int> SubscriptionUpdateStatus(Subscription request)
        {
            try
            {
                int requestId = 0;
                bool response = SubscriptionController.SubscriptionUpdateStatus(request, out requestId);
                return new WcfResponse<bool, int>() { Result = response, Data = requestId };
            }
            catch (Exception e)
            {
                return new WcfResponse<bool, int>() { Result = false, Message = e.Message, Data = 0 };
            }
        }

        #endregion

        #region Direitos

        public WcfResponse<List<SubscriptionRights>, int> LoadSubscriptionRights(SubscriptionRights filter, FilterOptions options, int? userId) 
        {
            try
            {
                int count = 0;
                var list = SubscriptionController.LoadSubscriptionRights(filter, options, out count, userId);
                return new WcfResponse<List<SubscriptionRights>, int>() { Result = list, Data = count };
            }
            catch (Exception e)
            {
                return new WcfResponse<List<SubscriptionRights>, int>() { Result = null, Data = 0, Message = e.Message };
            }
        }

        public WcfResponse<bool> SubscriptionRightsRequest(SubscriptionRights request)
        {
            try
            {
                return new WcfResponse<bool>() { Result = SubscriptionController.SubscriptionRightsRequest(request) };
            }
            catch (Exception e)
            {
                return new WcfResponse<bool>() { Result = false, Message = e.Message };
            }
        }

        public WcfResponse<bool> SubscriptionRightsUpdate(SubscriptionRights request)
        {
            try
            {
                return new WcfResponse<bool>() { Result = SubscriptionController.SubscriptionRightsUpdate(request) };
            }
            catch (Exception e)
            {
                return new WcfResponse<bool>() { Result = false, Message = e.Message };
            }
        }

        public WcfResponse<bool> SubscriptionRightsDelete(SubscriptionRights request)
        {
            try
            {
                return new WcfResponse<bool>() { Result = SubscriptionController.SubscriptionRightsDelete(request) };
            }
            catch (Exception e)
            {
                return new WcfResponse<bool>() { Result = false, Message = e.Message };
            }
        }

        public WcfResponse<long> GetRequestedQuantity(SubscriptionRights request)
        {
            try
            {
                return new WcfResponse<long>() { Result = SubscriptionController.GetRequestedQuantity(request) };
            }
            catch (Exception e)
            {
                return new WcfResponse<long>() { Result = 0, Message = e.Message };
            }
        }

        public WcfResponse<long> GetRightQuantity(SubscriptionRights request)
        {
            try
            {
                return new WcfResponse<long>() { Result = SubscriptionController.GetRightQuantity(request) };
            }
            catch (Exception e)
            {
                return new WcfResponse<long>() { Result = 0, Message = e.Message };
            }
        }

        public WcfResponse<bool> RequestRightsNotify(SubscriptionRights request, Subscription subscription)
        {
            try
            {
                return new WcfResponse<bool>() { Result = SubscriptionController.RequestRightsNotify(request, subscription) };
            }
            catch (Exception e)
            {
                return new WcfResponse<bool>() { Result = false, Message = e.Message };
            }
        }

        public WcfResponse<List<SubscriptionNotification>> LoadNotifications(int subscriptionId) 
        {
            try
            {
                return new WcfResponse<List<SubscriptionNotification>>() { Result = SubscriptionController.LoadNotifications(subscriptionId) };
            }
            catch (Exception e)
            {
                return new WcfResponse<List<SubscriptionNotification>>() { Result = null, Message = e.Message };
            }
        }

        #endregion
    }
}
