﻿using System;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.Contracts.ServiceContracts;
using System.ServiceModel.Activation;
using System.Collections.Generic;
using QX3.Portal.Services.Business.Term;

namespace QX3.Portal.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class TermService : ITermContract
    {
        private Term termController;

        private Term TermController
        {
            get
            {
                termController = termController ?? new Term();
                return termController;
            }
        }

        public WcfResponse<List<TermItem>> LoadTerms(TermFilter filter, int? userId)
        {
            try
            {
                var terms = TermController.LoadTerms(filter, userId);
                return new WcfResponse<List<TermItem>> { Result = terms };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<TermItem>> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<List<TermItem>> LoadLotTerms(string lstContracts)
        {
            try
            {
                var terms = TermController.LoadTerms(lstContracts);
                return new WcfResponse<List<TermItem>> { Result = terms };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<TermItem>> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<TermItem> LoadTerm(int termId)
        {
            try
            {
                var term = TermController.LoadTerm(termId);
                return new WcfResponse<TermItem> { Result = term };
            }
            catch (Exception ex)
            {
                return new WcfResponse<TermItem> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<bool> SettleTerm(int termID)
        {
            try
            {
                return new WcfResponse<bool> { Result = TermController.SettleTerm(termID) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<bool> { Result = false, Message = ex.Message };
            }
        }

        public WcfResponse<bool> ExcludeTerm(int termID)
        {
            try
            {
                return new WcfResponse<bool> { Result = TermController.ExcludeTerm(termID) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<bool> { Result = false, Message = ex.Message };
            }
        }

        public WcfResponse<List<TermItem>> LoadClientTerms(TermFilter filter, int? userId)
        {
            try
            {
                var terms = TermController.LoadClientTerms(filter, userId);
                return new WcfResponse<List<TermItem>> { Result = terms };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<TermItem>> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<List<TermItem>> InsertTerms(List<TermItem> terms)
        {
            try
            {
                var list = TermController.InsertTerms(terms);
                return new WcfResponse<List<TermItem>> { Result = list };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<TermItem>> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<bool> ChangeQuantityToSettle(int termID, Int64 quantity)
        {
            try
            {
                return new WcfResponse<bool> { Result = TermController.ChangeQuantityToSettle(termID, quantity) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<bool> { Result = false, Message = ex.Message };
            }
        }

        public WcfResponse<int> GetNumberOfPending(string assessorsList)
        {
            try
            {
                return new WcfResponse<Int32> { Result = TermController.GetNumberOfPending(assessorsList) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<Int32> { Result = 0, Message = ex.Message };
            }
        }
    }
}
