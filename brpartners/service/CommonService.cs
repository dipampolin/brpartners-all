﻿using System;
using System.Collections.Generic;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.Contracts.ServiceContracts;
using System.ServiceModel.Activation;
using QX3.Portal.Services.Business.Common;
using System.Net.Mail;
using System.IO;
using System.Configuration;
using System.Linq;
using QX3.Spinnex.Common.Services.Mail;
using QX3.Spinnex.Common.Services.Logging;

namespace QX3.Portal.Services
{
	[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class CommonService : ICommonContract
	{
		private Common commonController;

        private Common CommonController
		{
			get
			{
                commonController = commonController ?? new Common();
                return commonController;
			}
		}

        public WcfResponse<List<CommonType>> LoadTypes(string moduleName, string specification)
		{
			try
			{
                var types = CommonController.LoadTypes(moduleName, specification);
                return new WcfResponse<List<CommonType>> { Result = types };
			}
			catch (Exception ex)
			{
                return new WcfResponse<List<CommonType>> { Result = null, Message = ex.Message };
			}
		}

        public WcfResponse<List<WarningList>> GetWarningList(UserInformation user, string areaName, string assessors, string clients, int? userId) 
        {
            try
            {
                var list = CommonController.GetWarningList(user, areaName, assessors, clients, userId);
                return new WcfResponse<List<WarningList>> { Result = list };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<WarningList>> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<List<int>> GetFullListOfAssessors()
        {
            try
            {
                var list = CommonController.GetFullListOfAssessors();
                return new WcfResponse<List<int>> { Result = list };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<int>> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<String> LoadClientName(int code)
        {
            try
            {
                return new WcfResponse<string> { Result = CommonController.GetClientName(code) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<string> { Message = ex.Message };
            }
        }

        public WcfResponse<MailModel> GetMailModel(int funcionalityId)
        {
            try
            {
                return new WcfResponse<MailModel> { Result = CommonController.GetMailModel(funcionalityId) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<MailModel> { Message = ex.Message };
            }
        }

        public WcfResponse<List<TagModel>> GetTagModel()
        {
            try
            {
                return new WcfResponse<List<TagModel>> { Result = CommonController.GetTagModel() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<TagModel>> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<bool> UpdateMailModel(MailModel model) 
        {
            try
            {
                return new WcfResponse<bool> { Result = CommonController.UpdateMailModel(model) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<bool> { Result = false, Message = ex.Message };
            }
        }

        public WcfResponse<bool> InsertMailModel(MailModel model)
        {
            try
            {
                return new WcfResponse<bool> { Result = CommonController.InsertMailModel(model) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<bool> { Result = false, Message = ex.Message };
            }
        }

        public WcfResponse<bool> SendMailForUserWithAttachment(
            List<KeyValuePair<string, object>> variables
            , List<KeyValuePair<string, byte[]>> attachments
            , string templateFileName
            , string subject
            , string emailTo
            , string emailReplyTo
            , List<string> emailCCList
            )
        {
            try
            {
                LogManager.InitLog("Common.SendMailForUserWithAttachment");

                return new WcfResponse<bool> { Result = CommonController.SendMailForUserWithAttachment(variables, attachments, templateFileName, subject, emailTo, emailReplyTo, emailCCList) };
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return new WcfResponse<bool> { Result = false, Message = "Não foi possível enviar email." };
            }
        }

        public WcfResponse<RelatedPersons> RelatedPersons(DateTime dtStart, DateTime dtEnd) {
            try {
                return new WcfResponse<RelatedPersons> { Result = CommonController.RelatedPersons(dtStart, dtEnd) };
            }
            catch (Exception ex) {
                return new WcfResponse<RelatedPersons> { Message = ex.Message };
            }
        }


        public WcfResponse<bool> SendMailForUser(int funcionalityId, List<KeyValuePair<string, object>> variables, byte[] attach, FuncionalityClient client, string fileName)
        {
            try
            {
                LogManager.InitLog("Common.SendMailForUser");

                List<KeyValuePair<string, object>> messageValues = new List<KeyValuePair<string, object>>();
                
                //pegar email

                MailModel model = this.GetMailModel(funcionalityId).Result;

                if (model == null)
                    return new WcfResponse<bool> { Result = false, Message = "Modelo de email não encontrado." };


                foreach (var v in variables) 
                {                     
                    model.ReplyMail = model.ReplyMail.Replace(v.Key, v.Value.ToString());
                    model.ReplyName = model.ReplyName.Replace(v.Key, v.Value.ToString());
                    model.Subject = model.Subject.Replace(v.Key, v.Value.ToString());
                    model.Content = model.Content.Replace(v.Key, v.Value.ToString());
                }

                
                string emailTo = string.IsNullOrEmpty(ConfigurationManager.AppSettings["Subscription.RequestRights.EmailSendToMock"])
                    ? variables.Where(a => a.Key.ToUpper() == "$TO").FirstOrDefault().Value.ToString()
                    : ConfigurationManager.AppSettings["Subscription.RequestRights.EmailSendToMock"];
                                
                MailAddress from = new MailAddress(model.ReplyMail);
               
                MailMessage msg = new MailMessage();

                msg.From = from;
                foreach (string s in emailTo.Split(';'))
                {
                    int i = msg.To.IndexOf(new MailAddress(s));
                    if ((s.Length > 0) && (i == -1))
                        msg.To.Add(s);
                }

                string cc = variables.Where(a => a.Key.ToUpper() == "$CC").FirstOrDefault().Value.ToString();
                if (!String.IsNullOrEmpty(cc))
                {
                    foreach (string s in cc.Split(';'))
                    {
                        int i = msg.CC.IndexOf(new MailAddress(s));
                        if ((s.Length > 0) && (i == -1))
                            msg.CC.Add(s);
                    }
                }

                msg.Subject = model.Subject;
                msg.Body = model.Content;
                msg.IsBodyHtml = true;

                MemoryStream mst = new MemoryStream(attach);

                if (client.Funcionalities.Where(a => a.FuncId == funcionalityId).FirstOrDefault().SendFormat == "H")
                {
                    msg.Attachments.Add(new Attachment(mst, fileName + ".html"));
                }
                else 
                {
                    msg.Attachments.Add(new Attachment(mst, fileName +".pdf"));
                }                

                SmtpClient smtp = new SmtpClient();

                smtp.Send(msg);

                return new WcfResponse<bool> { Result = true, Message = "Email enviado com sucesso." };

            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return new WcfResponse<bool> { Result = false, Message = ex.Message };
            }
        }  
	}
}
