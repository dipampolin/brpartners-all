﻿using System;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.Contracts.ServiceContracts;
using System.ServiceModel.Activation;
using QX3.Portal.Services.Business.Other;
using System.Collections.Generic;

namespace QX3.Portal.Services
{
	[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class QuotesService : IQuotesContract
	{
        private Quotes quotesController;

        private Quotes QuotesController
		{
			get
			{
                quotesController = quotesController ?? new Quotes();
                return quotesController;
			}
		}

		public WcfResponse<List<StockPrice>> LoadQuotes(string[] stocks)
		{
			try
			{
                return new WcfResponse<List<StockPrice>> { Result = QuotesController.LoadQuotes(stocks) };
			}
			catch (Exception ex)
			{
                return new WcfResponse<List<StockPrice>> { Result = null, Message = ex.Message };
			}
		}
    }
}
