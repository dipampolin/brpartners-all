﻿using System;
using System.Collections.Generic;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.Contracts.ServiceContracts;
using System.ServiceModel.Activation;
using QX3.Portal.Services.Business;
using System.Net.Mail;
using System.IO;
using System.Configuration;
using System.Linq;
using QX3.Spinnex.Common.Services.Mail;
using QX3.Spinnex.Common.Services.Logging;

namespace QX3.Portal.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class GuaranteeService: IGuaranteeContract
    {
        private GuaranteeMethods guaranteeController;

		private GuaranteeMethods GuaranteeController
        {
            get
            {
                guaranteeController = guaranteeController ?? new GuaranteeMethods();
                return guaranteeController;
            }
        }

		public WcfResponse<GuaranteeClient, int> ListGuarantees(GuaranteeClientFilter filter, int? userId, out int pos)
		{
			try
			{
				int count = 0;
				var list = GuaranteeController.ListGuarantees(filter, userId, out count, out pos);
				return new WcfResponse<GuaranteeClient, int> { Result = list, Data = count };
			}
			catch (Exception ex)
			{
				pos = 0;
				return new WcfResponse<GuaranteeClient, int> { Result = null, Message = ex.Message, Data = 0 };
			}
		}
    }
}
