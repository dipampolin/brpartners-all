﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Activation;
using QX3.Portal.Contracts.ServiceContracts;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.Services.Business;

namespace QX3.Portal.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class BrokerageDiscountService : IBrokerageDiscountContract
    {
        private BrokerageDiscountMethods brokerageDiscountController;

        private BrokerageDiscountMethods BrokerageDiscountController
        {
            get
            {
                brokerageDiscountController = brokerageDiscountController ?? new BrokerageDiscountMethods();
                return brokerageDiscountController;
            }
        }

        public WcfResponse<List<BrokerageDiscount>> LoadBrokerageDiscounts(BrokerageDiscount brokerage, int? userId) 
        {
            try
            {
                return new WcfResponse<List<BrokerageDiscount>> { Result = BrokerageDiscountController.LoadBrokerageDiscounts(brokerage, userId) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<BrokerageDiscount>> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<List<BrokerageDiscount>, int> LoadBrokerageDiscountsPaginate(BrokerageDiscount brokerage, FilterOptions options, int? userId)
        {
            try
            {
                int count = 0;
                var list = BrokerageDiscountController.LoadBrokerageDiscountsPaginate(brokerage, options, userId, out count);
                return new WcfResponse<List<BrokerageDiscount>, int> { Result = list, Data = count };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<BrokerageDiscount>, int> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<bool> UpdateBrokerageDiscountStatus(List<BrokerageDiscount> requests)
        {
            try
            {
                return new WcfResponse<bool> { Result = BrokerageDiscountController.UpdateBrokerageDiscountStatus(requests) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<bool> { Result = false, Message = ex.Message };
            }
        }

        public WcfResponse<bool> DeleteBrokerageDiscount(List<BrokerageDiscount> requests)
        {
            try
            {
                return new WcfResponse<bool> { Result = BrokerageDiscountController.DeleteBrokerageDiscount(requests) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<bool> { Result = false, Message = ex.Message };
            }
        }

        public WcfResponse<bool, int> BrokerageDiscountRequest(BrokerageDiscount request)
        {
            try
            {
                int requestId = 0;
                var result = BrokerageDiscountController.BrokerageDiscountRequest(request, out requestId);
                return new WcfResponse<bool, int> { Result = result, Data = requestId };
            }
            catch (Exception ex)
            {
                return new WcfResponse<bool, int> { Result = false, Message = ex.Message };
            }
        }

        public WcfResponse<DateTime> GetProjectedDate(DateTime? date)
        {
            try
            {
                return new WcfResponse<DateTime> { Result = BrokerageDiscountController.GetProjectedDate(date) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<DateTime> { Result = DateTime.MinValue, Message = ex.Message };
            }
        }

        public WcfResponse<int> GetPendentBrokerageDiscounts(BrokerageDiscount filter, int? userId)
        {
            try
            {
                return new WcfResponse<int>() { Result = BrokerageDiscountController.GetPendentBrokerageDiscounts(filter, userId) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<int>() { Result = 0, Message = ex.Message };
            }
        }

        public WcfResponse<decimal> GetActualDiscount(int clientCode) 
        {
            try
            {
                return new WcfResponse<decimal>() { Result = BrokerageDiscountController.GetActualDiscount(clientCode) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<decimal>() { Result = 0, Message = ex.Message };
            }
        }

    }
}
