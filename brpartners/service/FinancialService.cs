﻿using System;
using System.Collections.Generic;
using System.ServiceModel.Activation;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.Contracts.ServiceContracts;
using QX3.Portal.Services.Business.Financial;

namespace QX3.Portal.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class FinancialService : IFinancialContract
    {
        private Financial financialController;

        private Financial FinancialController
        {
            get
            {
                financialController = financialController ?? new Financial();
                return financialController;
            }
        }

        public WcfResponse<List<ProjectedReportItem>> LoadProjectedReport(ProjectedReportFilter filter, int? userId)
        {
            try
            {
                var projectedReport = FinancialController.LoadProjectedReport(filter, userId);
                return new WcfResponse<List<ProjectedReportItem>> { Result = projectedReport };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<ProjectedReportItem>> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<List<RedemptionRequestItem>> LoadRedemptionRequest(RedemptionRequestFilter filter, int? userId)
        {
            try
            {
                var obj = FinancialController.LoadRedemptionRequest(filter, userId);
                return new WcfResponse<List<RedemptionRequestItem>> { Result = obj };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<RedemptionRequestItem>> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<bool> DeleteRedemptionRequest(int id, string clientId)
        {
            try
            {
                var result = FinancialController.DeleteRedemptionRequest(id, clientId);
                return new WcfResponse<bool> { Result = result };
            }
            catch (Exception ex)
            {
                return new WcfResponse<bool> { Result = false, Message = ex.Message };
            }
        }

        public WcfResponse<RedemptionRequestResult> InsertRedemptionRequest(RedemptionRequestItem item)
        {
            try
            {
                var result = FinancialController.InsertRedemptionRequest(item);
                return new WcfResponse<RedemptionRequestResult> { Result = result };
            }
            catch (Exception ex)
            {
                return new WcfResponse<RedemptionRequestResult> { Result = new RedemptionRequestResult { Result = false }, Message = ex.Message };
            }
        }

        public WcfResponse<RedemptionRequestBalance> LoadRedemptionRequestBalance(string clientCode, string assessor, int? userId)
		{
			try
			{
				RedemptionRequestBalance balance = FinancialController.LoadRedemptionRequestBalance(clientCode, assessor, userId);
				return new WcfResponse<RedemptionRequestBalance> { Result = balance };
			}
			catch (Exception ex)
			{
				return new WcfResponse<RedemptionRequestBalance> { Result = null, Message = ex.Message };
			}
		}

        public WcfResponse<List<RedemptionRequestBankAccount>> LoadBankAccountInformation(string clientCode)
        {
            try
            {
                var obj = FinancialController.LoadBankAccountInformation(clientCode);
                return new WcfResponse<List<RedemptionRequestBankAccount>> { Result = obj };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<RedemptionRequestBankAccount>> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<bool> ChangeRedemptionRequestStatus(RedemptionRequestItem item)
        {
            try
            {
                var result = FinancialController.ChangeRedemptionRequestStatus(item);
                return new WcfResponse<bool> { Result = result };
            }
            catch (Exception ex)
            {
                return new WcfResponse<bool> { Result = false, Message = ex.Message };
            }
        }

        public WcfResponse<int> GetPendentRedemptionRequest()
        {
            try
            {
                var result = FinancialController.GetPendentRedemptionRequest();
                return new WcfResponse<int> { Result = result };
            }
            catch (Exception ex)
            {
                return new WcfResponse<int> { Result = 0, Message = ex.Message };
            }
        }

        public WcfResponse<List<DailyFinancial>, int> LoadDailyFinancialReport(DailyFinancialFilter filter, FilterOptions options, int? userId)
        {
            try
            {
                int count = 0;
                var list = FinancialController.LoadDailyFinancialReport(filter, options, userId, out count);
                return new WcfResponse<List<DailyFinancial>, int> { Result = list, Data = count };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<DailyFinancial>, int> { Result = null, Message = ex.Message, Data = 0 };
            }
        }
    }
}
