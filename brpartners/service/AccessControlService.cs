﻿using System;
using System.Collections.Generic;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.Contracts.ServiceContracts;
using System.ServiceModel.Activation;
using QX3.Portal.Services.Business.AccessControl;

namespace QX3.Portal.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
	public class AccessControlService : IAccessControlContract
    {
		private AccessControl accessControlController;

		private AccessControl AccessControlController
		{
			get
			{
				accessControlController = accessControlController ?? new AccessControl();
				return accessControlController;
			}
		}

        public WcfResponse<ACProfile> LoadUserProfile(string email)
        {
            try
            {
				return new WcfResponse<ACProfile> { Result = AccessControlController.LoadUserProfile(email) };
            }
            catch (Exception ex)
            {
				return new WcfResponse<ACProfile> { Result = null, Message = ex.Message };
            }
        }

		public WcfResponse<List<ACElement>> LoadACElements(bool active)
		{
			try
			{
				return new WcfResponse<List<ACElement>> { Result = AccessControlController.LoadElements(active) };
			}
			catch (Exception ex)
			{
				return new WcfResponse<List<ACElement>> { Result = null, Message = ex.Message };
			}
		}

		public WcfResponse<Boolean> UpdateElementAlias(int elementID, string newAlias)
		{
			try
			{
				return new WcfResponse<Boolean> { Result = AccessControlController.UpdateElementAlias(elementID, newAlias) };
			}
			catch (Exception ex)
			{
				return new WcfResponse<Boolean> { Result = false, Message = ex.Message };
			}
		}

		public WcfResponse<Int32> InsertGroupOfAssessors(ACGroupOfAssessors group)
		{
			try
			{
				return new WcfResponse<Int32> { Result = AccessControlController.InsertGroupOfAssessors(group) };
			}
			catch (Exception ex)
			{
				return new WcfResponse<Int32> { Result = 0, Message = ex.Message };
			}
		}

		public WcfResponse<Boolean> InsertUserGroupRelationship(int userID, int userId)
		{
			try
			{
				return new WcfResponse<Boolean> { Result = AccessControlController.InsertUserGroupRelationship(userID, userId) };
			}
			catch (Exception ex)
			{
				return new WcfResponse<Boolean> { Result = false, Message = ex.Message };
			}
		}

        public WcfResponse<Int32> InsertUser(ACUser user)
        {
            try
            {
                return new WcfResponse<Int32> { Result = AccessControlController.InsertUser(user) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<Int32> { Result = 0, Message = string.Concat(ex.Message, ex.StackTrace) };
            }
        }

		public WcfResponse<Boolean> ValidateGroupName(string groupName, int userId)
		{
			try
			{
				return new WcfResponse<Boolean> { Result = AccessControlController.ValidateGroupName(groupName, userId) };
			}
			catch (Exception ex)
			{
				return new WcfResponse<Boolean> { Result = false, Message = ex.Message };
			}
		}

		public WcfResponse<Boolean> ValidateAssessorsList(string list, int userId)
		{
			try
			{
				return new WcfResponse<Boolean> { Result = AccessControlController.ValidateAssessorsList(list, userId) };
			}
			catch (Exception ex)
			{
				return new WcfResponse<Boolean> { Result = false, Message = ex.Message };
			}
		}

        public WcfResponse<Boolean> ValidateAssessorsAndClientsList(int groupId, string list, string includeList, string excludeList)
        {
            try
            {
                return new WcfResponse<Boolean> { Result = AccessControlController.ValidateAssessorsAndClientsList(groupId, list, includeList, excludeList) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<Boolean> { Result = false, Message = ex.Message };
            }
        }

		public WcfResponse<List<ACUser>> LoadGroupOfAssessorsFreeUsers(string nameFilter, string idsFilter)
		{
			try
			{
				return new WcfResponse<List<ACUser>> { Result = AccessControlController.LoadGroupOfAssessorsFreeUsers(nameFilter, idsFilter) };
			}
			catch (Exception ex)
			{
				return new WcfResponse<List<ACUser>> { Result = null, Message = ex.Message };
			}
		}

		public WcfResponse<List<ACUser>> LoadUsers()
		{
			try
			{
				return new WcfResponse<List<ACUser>> { Result = AccessControlController.LoadUsers() };
			}
			catch (Exception ex)
			{
				return new WcfResponse<List<ACUser>> { Result = null, Message = ex.Message };
			}
		}

        public WcfResponse<ACUser> LoadUser(int id)
        {
            try
            {
                return new WcfResponse<ACUser> { Result = AccessControlController.LoadUser(id) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<ACUser> { Result = null, Message = ex.Message };
            }
        }

		public WcfResponse<ACUser> LoadUserByEmail(string email)
		{
			try
			{
				return new WcfResponse<ACUser> { Result = AccessControlController.LoadUser(email) };
			}
			catch (Exception ex)
			{
				return new WcfResponse<ACUser> { Result = null, Message = ex.Message };
			}
		}

        public WcfResponse<List<ACProfile>> LoadProfiles()
        {
            try
            {
                return new WcfResponse<List<ACProfile>> { Result = AccessControlController.LoadProfiles() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<ACProfile>> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<ACProfile> LoadProfile(int profileID)
        {
            try
            {
                return new WcfResponse<ACProfile> { Result = AccessControlController.LoadProfile(profileID) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<ACProfile> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<Int32> InsertProfile(ACProfile profile)
        {
            try
            {
                return new WcfResponse<Int32> { Result = AccessControlController.InsertProfile(profile) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<Int32> { Result = 0, Message = ex.Message };
            }
        }

		public WcfResponse<List<ACGroupOfAssessors>> LoadGroupOfAssessors(bool minimum, int userId, string userName)

		{
			try
			{
				return new WcfResponse<List<ACGroupOfAssessors>> { Result = AccessControlController.LoadGroupOfAssessors(minimum, userId, userName) };
			}
			catch (Exception ex)
			{
				return new WcfResponse<List<ACGroupOfAssessors>> { Result = null, Message = ex.Message };
			}
		}

		public WcfResponse<List<ACGroupOfAssessors>> LoadGroupsOfAssessorsPerUser(long userId)
		{
			try
			{
				return new WcfResponse<List<ACGroupOfAssessors>> { Result = AccessControlController.LoadGroupsOfAssessorsPerUser(userId) };
			}
			catch (Exception ex)
			{
				return new WcfResponse<List<ACGroupOfAssessors>> { Result = null, Message = ex.Message };
			}
		}

		public WcfResponse<Boolean> DeleteGroupOfAssessors(int userId)
		{
			try
			{
				return new WcfResponse<Boolean> { Result = AccessControlController.DeleteGroupOfAssessors(userId) };
			}
			catch (Exception ex)
			{
				return new WcfResponse<Boolean> { Result = false, Message = ex.Message };
			}
		}

        public WcfResponse<Boolean> DeleteUser(int userID)
        {
            try
            {
                return new WcfResponse<Boolean> { Result = AccessControlController.DeleteUser(userID) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<Boolean> { Result = false, Message = ex.Message };
            }
        }

        public WcfResponse<List<ACProfile>> LoadProfilesAndUserCount(int profileID, string userName, bool withoutUser, List<ACElement> elements)
        {
            try
            {
                return new WcfResponse<List<ACProfile>> { Result = AccessControlController.LoadProfilesAndUserCount(profileID, userName, withoutUser, elements) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<ACProfile>> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<Boolean> DeleteProfile(int profileID)
        {
            try
            {
                return new WcfResponse<Boolean> { Result = AccessControlController.DeleteProfile(profileID) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<Boolean> { Result = false, Message = ex.Message };
            }
        }

        public WcfResponse<Boolean> ValidateProfileName(string profileName, int profileID)
        {
            try
            {
                return new WcfResponse<Boolean> { Result = AccessControlController.ValidateProfileName(profileName, profileID) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<Boolean> { Result = false, Message = ex.Message };
            }
        }

        public WcfResponse<Boolean> ValidateProfileElements(List<ACElement> elements, int profileID)
        {
            try
            {
                return new WcfResponse<Boolean> { Result = AccessControlController.ValidateProfileElements(elements, profileID) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<Boolean> { Result = false, Message = ex.Message };
            }
        }

        public WcfResponse<List<ACUser>> LoadUsersWithoutProfile()
        {
            try
            {
                return new WcfResponse<List<ACUser>> { Result = AccessControlController.LoadUsersWithoutProfile() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<ACUser>> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<bool> CheckIfExistsInIncludedClients(int clientCode, int userId)
        {
            try
            {
                return new WcfResponse<bool> { Result = AccessControlController.CheckIfExistsInIncludedClients(clientCode, userId) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<bool> { Result = false, Message = ex.Message };
            }
        }

        public WcfResponse<bool> CheckIfExistsInExcludedClients(int clientCode, int userId)
        {
            try
            {
                return new WcfResponse<bool> { Result = AccessControlController.CheckIfExistsInExcludedClients(clientCode, userId) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<bool> { Result = false, Message = ex.Message };
            }
        }

        public WcfResponse<List<FuncionalityClient>, int> ListFuncionalityClients(FuncionalityClientFilter filter)
        {
            try
            {
                int count = 0;
                var list = AccessControlController.ListFuncionalityClients(filter, out count);
                return new WcfResponse<List<FuncionalityClient>, int> { Result = list, Data = count };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<FuncionalityClient>, int> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<bool> InsertClientForFuncionality(FuncionalityClient client)
        {
            try
            {
                var result = AccessControlController.InsertClientForFuncionality(client);
                return new WcfResponse<bool> { Result = result };
            }
            catch (Exception ex)
            {
                return new WcfResponse<bool> { Result = false, Message = ex.Message };
            }
        }

        public WcfResponse<bool> DeleteClientForFuncionality(FuncionalityClient client)
        {
            try
            {
                var result = AccessControlController.DeleteClientForFuncionality(client);
                return new WcfResponse<bool> { Result = result };
            }
            catch (Exception ex)
            {
                return new WcfResponse<bool> { Result = false, Message = ex.Message };
            }
        }

        public WcfResponse<bool> UpdateClientForFuncionality(FuncionalityClient client)
        {
            try
            {
                var result = AccessControlController.UpdateClientForFuncionality(client);
                return new WcfResponse<bool> { Result = result };
            }
            catch (Exception ex)
            {
                return new WcfResponse<bool> { Result = false, Message = ex.Message };
            }
        }

        public WcfResponse<List<Funcionality>> ListFuncionalities()
        {
            try
            {
                var result = AccessControlController.ListFuncionalities();
                return new WcfResponse<List<Funcionality>> { Result = result };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<Funcionality>> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<List<ACUser>> ListClientsSINACOR(string clientes)
        {
            try
            {
                var result = AccessControlController.ListClientsSINACOR(clientes);
                return new WcfResponse<List<ACUser>> { Result = result };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<ACUser>> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<bool> Verify_SESSION_Login(string username)
        {
            try
            {
                var result = AccessControlController.Verify_SESSION_Login(username);
                return new WcfResponse<bool> { Result = result };
            }
            catch (Exception ex)
            {
                return new WcfResponse<bool> { Result = false, Message = ex.Message };
            }
        }

        public WcfResponse<bool> Delete_SESSION_Login(string username)
        {
            try
            {
                var result = AccessControlController.Delete_SESSION_Login(username);
                return new WcfResponse<bool> { Result = result };
            }
            catch (Exception ex)
            {
                return new WcfResponse<bool> { Result = false, Message = ex.Message };
            }
        }

        public WcfResponse<bool> Insert_SESSION(string username, string ipClient, DateTime dt, string sessionId)
        {
            try
            {
                var result = AccessControlController.Insert_SESSION(username, ipClient, dt, sessionId);
                return new WcfResponse<bool> { Result = result };
            }
            catch (Exception ex)
            {
                return new WcfResponse<bool> { Result = false, Message = ex.Message };
            }
        }

        public WcfResponse<bool> Verify_SESSION_IP(string ipClient)
        {
            try
            {
                var result = AccessControlController.Verify_SESSION_IP(ipClient);
                return new WcfResponse<bool> { Result = result };
            }
            catch (Exception ex)
            {
                return new WcfResponse<bool> { Result = false, Message = ex.Message };
            }
        }

        public WcfResponse<bool> Verify_SESSION(string username, string ipClient)
        {
            try
            {
                var result = AccessControlController.Verify_SESSION(username, ipClient);
                return new WcfResponse<bool> { Result = result };
            }
            catch (Exception ex)
            {
                return new WcfResponse<bool> { Result = false, Message = ex.Message };
            }
        }

        public WcfResponse<int> InsertClientOnline(ACUser user)
        {
            try
            {
                return new WcfResponse<Int32> { Result = AccessControlController.InsertClientOnline(user) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<Int32> { Result = 0, Message = string.Concat(ex.Message, ex.StackTrace) };
            }
        }

        public WcfResponse<ACUser> LoadClient(string cpf)
        {
            try
            {
                return new WcfResponse<ACUser> { Result = AccessControlController.LoadClient(cpf) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<ACUser> { Message = string.Concat(ex.Message, ex.StackTrace) };
            }
        }

        public WcfResponse<List<UserInfo>> ListUsers()
        {
            try
            {
                return new WcfResponse<List<UserInfo>> { Result = AccessControlController.ListUsers() };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<UserInfo>> { Message = string.Concat(ex.Message, ex.StackTrace) };
            }
        }

        public WcfResponse<bool> UpdateUsers(List<UserInfo> users)
        {
            try
            {
                return new WcfResponse<bool> { Result = AccessControlController.UpdateUsers(users) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<bool> { Message = string.Concat(ex.Message, ex.StackTrace) };
            }
        }
    }
}
