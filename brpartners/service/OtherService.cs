﻿using System;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.Contracts.ServiceContracts;
using System.ServiceModel.Activation;
using QX3.Portal.Services.Business.Other;
using System.Collections.Generic;
using QX3.Portal.Services.Business.Parameters;

namespace QX3.Portal.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class OtherService : IOtherContract
    {
        private Other otherController;
        private ParametersMethod parametersController;

        private Other OtherController
        {
            get
            {
                otherController = otherController ?? new Other();
                return otherController;
            }
        }

        private ParametersMethod ParametersController
        {
            get
            {
                parametersController = parametersController ?? new ParametersMethod();
                return parametersController;
            }
        }

        public WcfResponse<int> InsertOrderCorrection(OrdersCorrection correction)
        {
            try
            {
                return new WcfResponse<Int32> { Result = OtherController.InsertOrdersCorrection(correction) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<Int32> { Result = 0, Message = ex.Message };
            }
        }

        public WcfResponse<bool> DeleteOrderCorrection(int correctionID)
        {
            try
            {
                return new WcfResponse<bool> { Result = OtherController.DeleteOrdersCorrection(correctionID) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<bool> { Result = false, Message = ex.Message };
            }
        }

        public WcfResponse<bool> ChangeOrdersCorrectionStatus(int correctionID, int statusID)
        {
            try
            {
                return new WcfResponse<bool> { Result = OtherController.ChangeOrdersCorrectionStatus(correctionID, statusID) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<bool> { Result = false, Message = ex.Message };
            }
        }

        public WcfResponse<List<OrdersCorrection>> LoadOrdersCorrections(OrdersCorrectionFilter filter)
        {
            try
            {
                var corrections = OtherController.LoadOrdersCorrections(filter);
                return new WcfResponse<List<OrdersCorrection>> { Result = corrections };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<OrdersCorrection>> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<OrdersCorrection> LoadOrdersCorrection(int correctionID)
        {
            try
            {
                var corrections = OtherController.LoadOrdersCorrection(correctionID);
                return new WcfResponse<OrdersCorrection> { Result = corrections };
            }
            catch (Exception ex)
            {
                return new WcfResponse<OrdersCorrection> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<ConsolidatedPositionResponse> LoadConsolidatedPosition(string clientCode, string assessors, int? userId)
        {
            try
            {
                var obj = OtherController.LoadConsolidatedPosition(clientCode, assessors, userId);
                return new WcfResponse<ConsolidatedPositionResponse> { Result = obj };
            }
            catch (Exception ex)
            {
                return new WcfResponse<ConsolidatedPositionResponse> { Result = null, Message = ex.Message };
            }
        }

        #region Mapa de Operações
        public WcfResponse<List<OperationsMap>> LoadOperationsMap(OperationMapFilter filter)
        {
            try
            {
                var response = OtherController.LoadOperationsMap(filter);
                return new WcfResponse<List<OperationsMap>>() { Result = response };
            }
            catch (Exception e)
            {
                return new WcfResponse<List<OperationsMap>> { Result = null, Message = e.Message };
            }
        }

        public WcfResponse<Dictionary<String, String>> GetTradingDates()
        {
            try
            {
                var response = OtherController.GetTradingDates();
                return new WcfResponse<Dictionary<String, String>>() { Result = response };
            }
            catch (Exception e)
            {
                return new WcfResponse<Dictionary<String, String>> { Result = null, Message = e.Message };
            }
        }

        public WcfResponse<List<Operator>> GetOperatorList(OperationMapFilter filter)
        {
            try
            {
                var response = OtherController.GetOperatorList(filter);
                return new WcfResponse<List<Operator>>() { Result = response };
            }
            catch (Exception e)
            {
                return new WcfResponse<List<Operator>> { Result = null, Message = e.Message };
            }
        }
        #endregion

        public WcfResponse<CPClientInformation> LoadClientInformation(string clientCode, string assessors, int? userId)
        {
            try
            {
                var response = OtherController.LoadClientInformation(clientCode, assessors, userId);
                return new WcfResponse<CPClientInformation>() { Result = response };
            }
            catch (Exception e)
            {
                return new WcfResponse<CPClientInformation> { Result = null, Message = e.Message };
            }
        }

        #region Operações

        public WcfResponse<List<OperationClient>, int> LoadOperationClients(OperationClientFilter filter, FilterOptions filterOptions, int? userId)
        {
            try
            {
                int count = 0;
                var response = OtherController.LoadOperationClients(filter, filterOptions, userId, out count);
                return new WcfResponse<List<OperationClient>, int>() { Result = response, Data = count };
            }
            catch (Exception e)
            {
                return new WcfResponse<List<OperationClient>, int> { Result = null, Message = e.Message, Data = 0 };
            }
        }

        public WcfResponse<OperationReport> LoadOperationReport(OperationReport filter)
        {
            try
            {
                var response = OtherController.LoadOperationReport(filter);
                return new WcfResponse<OperationReport>() { Result = response };
            }
            catch (Exception e)
            {
                return new WcfResponse<OperationReport> { Result = null, Message = e.Message };
            }
        }

        public WcfResponse<bool> SinalizeMailSended(int clientCode, DateTime tradingDate)
        {
            try
            {
                var response = OtherController.SinalizeMailSended(clientCode, tradingDate);
                return new WcfResponse<bool>() { Result = response };
            }
            catch (Exception e)
            {
                return new WcfResponse<bool> { Result = false, Message = e.Message };
            }
        }

        #endregion

        #region Informe de rendimentos

        public WcfResponse<IncomeReport> LoadIncomeReport(IncomeReportFilter filter)
        {
            try
            {
                var response = OtherController.LoadIncomeReport(filter);
                return new WcfResponse<IncomeReport>() { Result = response };
            }
            catch (Exception e)
            {
                return new WcfResponse<IncomeReport> { Result = null, Message = e.Message };
            }
        }

        #endregion

        #region Private
        public WcfResponse<List<Private>> LoadPrivates(DateTime initmov, DateTime endmov)
        {
            try
            {
                var response = OtherController.LoadPrivates(initmov, endmov);
                return new WcfResponse<List<Private>>() { Result = response };
            }
            catch (Exception e)
            {
                return new WcfResponse<List<Private>> { Result = null, Message = e.Message };
            }
        }
        #endregion

        #region BB Rent

        public WcfResponse<BBRent> LoadBBRent(DateTime tradeDate)
        {
            try
            {
                var response = OtherController.LoadBBRent(tradeDate);
                return new WcfResponse<BBRent>() { Result = response };
            }
            catch (Exception e)
            {
                return new WcfResponse<BBRent> { Result = null, Message = e.Message };
            }
        }

        #endregion

        #region VAM

        public WcfResponse<Dictionary<string, string>> GetISINs(int? groupID)
        {
            try
            {
                var response = OtherController.GetISINs(groupID);
                return new WcfResponse<Dictionary<string, string>>() { Result = response };
            }
            catch (Exception e)
            {
                return new WcfResponse<Dictionary<string, string>> { Result = null, Message = e.Message };
            }
        }

        public WcfResponse<List<VAMItem>> LoadVAM(int? groupID, string isin)
        {
            try
            {
                var response = OtherController.LoadVAM(groupID, isin);
                return new WcfResponse<List<VAMItem>>() { Result = response };
            }
            catch (Exception e)
            {
                return new WcfResponse<List<VAMItem>> { Result = null, Message = e.Message };
            }
        }

        #endregion
    }
}
