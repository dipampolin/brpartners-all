﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.Contracts.ServiceContracts;
using QX3.Portal.Services.Business.Authentication;
using QX3.Spinnex.Common.Services;
using QX3.Spinnex.Authentication.Services;
using System.ServiceModel.Activation;
using QX3.Spinnex.Common.Services.Logging;

namespace QX3.Portal.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]

    public class AuthenticationService : IAuthenticationContract
    {
        #region Common

        private Authentication auth;

        private Authentication Auth
        {
            get
            {
                auth = auth ?? new Authentication();
                return auth;
            }
        }

        #endregion

        //Logar usando LDAP. TODO: Retirar este método quando não usar mais o LDAP
        public WcfResponse<UserInformation> Authenticate(string userName, string password, bool loadPermissions)
        {
            try
            {
                return new WcfResponse<UserInformation> { Result = Auth.Authenticate(userName, password, loadPermissions) };
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return new WcfResponse<UserInformation> { Result = null, Message = string.Concat(ex.Message, ex.StackTrace) };
            }
        }

        public WcfResponse<Boolean> CheckIfUserExists(string filter, LDAPSearchFilter filterType)
        {
            try
            {
                return new WcfResponse<Boolean> { Result = Auth.CheckUserExists(filter, filterType) };
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return new WcfResponse<Boolean> { Result = false, Message = ex.Message };
            }
        }

        public WcfResponse<UserInformation, StatusLogin> AuthenticateUser(string userName, string password, bool loadPermissions)
        {
            try
            {
                bool logged = false; string message = string.Empty; int errorType = 0; string ADKey = string.Empty;
                var user = Auth.AuthenticateUser(userName, password, out message, out logged, out errorType, out ADKey);
                return new WcfResponse<UserInformation, StatusLogin>() { Result = user, Data = new StatusLogin() { CanLogIn = logged, ErrorType = errorType, ADKey = ADKey }, Message = message };
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return new WcfResponse<UserInformation, StatusLogin> { Result = null, Data = new StatusLogin() { CanLogIn = false, ErrorType = 5 }, Message = ex.Message };
            }
        }

        public WcfResponse<UserInformation, StatusLogin> SelectUser(string userName)
        {
            try
            {
                bool logged = false; string message = string.Empty; int errorType = 0; 
                var user = Auth.SelectUser(userName, out message, out logged, out errorType);
                return new WcfResponse<UserInformation, StatusLogin>() { Result = user, Data = new StatusLogin() { CanLogIn = logged, ErrorType = errorType }, Message = message };
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return new WcfResponse<UserInformation, StatusLogin> { Result = null, Data = new StatusLogin() { CanLogIn = false, ErrorType = 5 }, Message = ex.Message };
            }
        }

        public WcfResponse<bool> ChangePassword(UserInformation userInfo) 
        {
            try
            {
                var message = String.Empty;
                var result = Auth.ChangePassword(userInfo, out message);
                return new WcfResponse<bool>() { Result = result, Message = message };
            }
            catch (Exception ex) 
            {
                LogManager.WriteError(ex);
                return new WcfResponse<Boolean> { Result = false, Message = ex.Message };
            }
        }

        public WcfResponse<Boolean, string> VerifyChangePassword(string guid)
        {
            try
            {
                string message = string.Empty;
                string email = string.Empty;
                bool response = Auth.VerifyChangePassword(guid, out message, out email);
                return new WcfResponse<Boolean, string> { Result = response, Message = message, Data = email };
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return new WcfResponse<Boolean, string> { Result = false, Message = ex.Message, Data = null };
            }
        }

        public WcfResponse<Boolean, string> CreateGuidToSend(UserInformation userInfo)
        {
            try
            {
                string guid = string.Empty;
                string message = string.Empty;
                var response = Auth.CreateGuidToSend(userInfo, out guid, out message);
                return new WcfResponse<bool, string>() { Result = response, Data = guid, Message = message};
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return new WcfResponse<Boolean, string> { Result = false, Data = "", Message = ex.Message };
            }
        }

		public WcfResponse<bool> SendRedefinePasswordEmail(string token, string email, string userName)
		{
			try
			{
				var response = Auth.SendRedefinePasswordEmail(token, email, userName);
				return new WcfResponse<bool>() { Result = response };
			}
			catch (Exception ex)
			{
				LogManager.WriteError(ex);
				return new WcfResponse<bool> { Result = false, Message = ex.Message };
			}
		}

        public WcfResponse<bool> SendWelcomeEmail(string token, string email, string userName)
        {
            try
            {
                var response = Auth.SendRedefinePasswordEmail(token, email, userName, true);
                return new WcfResponse<bool>() { Result = response };
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return new WcfResponse<bool> { Result = false, Message = ex.Message };
            }
        }

        //public WcfResponse<bool> TestEmail(string token, string email, string userName)
        //{
        //    try
        //    {
        //        var response = Auth.TestEmail(token, email, userName);
        //        return new WcfResponse<bool>() { Result = response };
        //    }
        //    catch (Exception ex)
        //    {
        //        LogManager.WriteError(ex);
        //        return new WcfResponse<bool> { Result = false, Message = ex.Message };
        //    }
        //}
    }
}
