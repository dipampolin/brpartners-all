﻿using System;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.Contracts.ServiceContracts;
using System.ServiceModel.Activation;
using QX3.Portal.Services.Business.Other;
using System.Collections.Generic;

namespace QX3.Portal.Services
{
	[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class RiskService : IRiskContract
	{
        private Risk riskController;

        private Risk RiskController
		{
			get
			{
                riskController = riskController ?? new Risk();
                return riskController;
			}
		}

        public WcfResponse<List<int>> InsertGuarantee(Guarantee guarantee)
		{
			try
			{
                return new WcfResponse<List<int>> { Result = RiskController.InsertGuarantee(guarantee) };
			}
			catch (Exception ex)
			{
                return new WcfResponse<List<int>> { Result = null, Message = ex.Message };
			}
		}

        public WcfResponse<bool> UpdateGuarantee(GuaranteeItem guaranteeItem)
        {
            try
            {
                return new WcfResponse<bool> { Result = RiskController.UpdateGuarantee(guaranteeItem) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<bool> { Result = false, Message = ex.Message };
            }
        }

        public WcfResponse<bool> DeleteGuarantee(int guaranteeID)
        {
            try
            {
                return new WcfResponse<bool> { Result = RiskController.DeleteGuarantee(guaranteeID) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<bool> { Result = false, Message = ex.Message };
            }
        }

        public WcfResponse<bool> ChangeGuaranteeStatus(int guaranteeID, int statusID)
        {
            try
            {
                return new WcfResponse<bool> { Result = RiskController.ChangeGuaranteeStatus(guaranteeID, statusID) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<bool> { Result = false, Message = ex.Message };
            }
        }

        public WcfResponse<List<Guarantee>> LoadGuarantees(GuaranteeFilter filter, int? userId)
        {
            try
            {
                var guarantees = RiskController.LoadGuarantees(filter, userId);
                return new WcfResponse<List<Guarantee>> { Result = guarantees };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<Guarantee>> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<int> GetNumberOfPending(string assessorsList)
        {
            try
            {
                return new WcfResponse<Int32> { Result = RiskController.GetNumberOfPending(assessorsList) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<Int32> { Result = 0, Message = ex.Message };
            }
        }

        public WcfResponse<List<GuaranteeItem>> LoadClientGuarantees(GuaranteeFilter filter, int? userId)
        {
            try
            {
                var guarantees = RiskController.LoadClientGuarantees(filter, userId);
                return new WcfResponse<List<GuaranteeItem>> { Result = guarantees };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<GuaranteeItem>> { Result = null, Message = ex.Message };
            }
        }

    }
}
