﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QX3.Portal.Contracts.ServiceContracts;
using QX3.Portal.Contracts.DataContracts;
using QX3.Spinnex.Common.Services.Logging;
using QX3.Portal.Services.Business;
using QX3.Spinnex.Authentication.Services.UserGroupData;
using System.ServiceModel.Activation;

namespace QX3.Portal.Services
{
	[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
	public class NonResidentTradersService : INonResidentTradersServiceContract
	{
		#region Common

		private Registration registrationController;

		private Registration RegistrationController
		{
			get
			{
				registrationController = registrationController ?? new Registration();
				return registrationController;
			}
		}

		private Reports reportController;

		private Reports ReportController
		{
			get
			{
				reportController = reportController ?? new Reports();
				return reportController;
			}
		}

		private Brokerage brokerageController;

		private Brokerage BrokerageController
		{
			get
			{
				brokerageController = brokerageController ?? new Brokerage();
				return brokerageController;
			}
		}

		public WcfResponse<String> LoadCustomerName(int code)
		{
			try
			{
				return new WcfResponse<string> { Result = RegistrationController.GetCustomerName(code) };
			}
			catch (Exception ex)
			{
				return new WcfResponse<string> { Message = ex.Message };
			}
		}

        public WcfResponse<String> GetCustomerName(int code, string assessors, int? userId)
        {
            try
            {
                return new WcfResponse<string> { Result = RegistrationController.GetCustomerName(code, assessors, userId) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<string> { Message = ex.Message };
            }
        }

		#endregion

		#region Registration

		public WcfResponse<List<NonResidentCustomer>> LoadCustomers(int code, int? userId)
		{
			try
			{
				return new WcfResponse<List<NonResidentCustomer>> { Result = RegistrationController.LoadCustomers(code, userId) };
			}
			catch (Exception ex)
			{
				return new WcfResponse<List<NonResidentCustomer>> { Message = ex.Message };
			}
		}

        public WcfResponse<List<Broker>> LoadBrokers(string filter)
        {
            try
            {
                return new WcfResponse<List<Broker>> { Result = RegistrationController.LoadBrokers(filter) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<Broker>> { Result = null,  Message = ex.Message };
            }
        }

        public WcfResponse<List<Broker>> LoadBrokersTransfer(string filter)
        {
            try
            {
                return new WcfResponse<List<Broker>> { Result = RegistrationController.LoadBrokersTransfer(filter) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<Broker>> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<string> LoadClientEmail(int clientCode)
        {
            try
            {
                return new WcfResponse<string> { Result = RegistrationController.LoadClientEmail(clientCode) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<string> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<List<CommonType>> LoadPortfolios(string filter)
        {
            try
            {
                return new WcfResponse<List<CommonType>> { Result = RegistrationController.LoadPortfolios(filter) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<CommonType>> { Result = null, Message = ex.Message };
            }
        }

		public WcfResponse<Boolean> InsertCustomer(NonResidentCustomer customer)
		{
			try
			{
				return new WcfResponse<bool> { Result = RegistrationController.InsertCustomer(customer) };
			}
			catch (Exception ex)
			{
				return new WcfResponse<bool> { Result = false, Message = ex.Message };
			}
		}

		public WcfResponse<SettlementDate> LoadLastSettlement()
		{
			try
			{
				return new WcfResponse<SettlementDate> { Result = RegistrationController.LoadLastSettlement() };
			}
			catch (Exception ex)
			{
				return new WcfResponse<SettlementDate> { Message = ex.Message };
			}
		}

		public WcfResponse<List<PTax>> LoadPTaxes(string date)
		{
			try
			{
				return new WcfResponse<List<PTax>> { Result = RegistrationController.LoadPTaxes(date) };
			}
			catch (Exception ex)
			{
				return new WcfResponse<List<PTax>> { Message = ex.Message };
			}
		}

		public WcfResponse<Boolean> InsertPTax(PTax ptax)
		{
			try
			{
				return new WcfResponse<bool> { Result = RegistrationController.InsertPTax(ptax) };
			}
			catch (Exception ex)
			{
				return new WcfResponse<bool> { Result = false, Message = ex.Message };
			}
		}

		public WcfResponse<List<Failure>> LoadFailures(string date, int code, int? userId)
		{
			try
			{
				return new WcfResponse<List<Failure>> { Result = RegistrationController.LoadFailures(date, code, userId) };
			}
			catch (Exception ex)
			{
				return new WcfResponse<List<Failure>> { Message = ex.Message };
			}
		}

		public WcfResponse<Boolean> InsertFailure(Failure failure)
		{
			try
			{
				return new WcfResponse<bool> { Result = RegistrationController.InsertFailure(failure) };
			}
			catch (Exception ex)
			{
				return new WcfResponse<bool> { Result = false, Message = ex.Message };
			}
		}

		public WcfResponse<List<ReportType>> LoadReportTypes()
		{
			try
			{
				return new WcfResponse<List<ReportType>> { Result = RegistrationController.LoadReportTypes() };
			}
			catch (Exception ex)
			{
				return new WcfResponse<List<ReportType>> { Result = null, Message = ex.Message };
			}
		}

		public WcfResponse<bool> InsertDisclaimer(Disclaimer disclaimer)
		{
			try
			{
				return new WcfResponse<bool> { Result = RegistrationController.InsertDisclaimer(disclaimer) };
			}
			catch (Exception ex)
			{
				return new WcfResponse<bool> { Result = false, Message = ex.Message };
			}
		}

		public WcfResponse<Disclaimer> LoadDisclaimer(int reportId)
		{
			try
			{
				return new WcfResponse<Disclaimer> { Result = RegistrationController.LoadDisclaimer(reportId) };
			}
			catch (Exception ex)
			{
				return new WcfResponse<Disclaimer> { Result = null, Message = ex.Message };
			}
		}

		public WcfResponse<List<Disclaimer>> LoadDisclaimers()
		{
			try
			{
				return new WcfResponse<List<Disclaimer>> { Result = RegistrationController.LoadDisclaimers() };
			}
			catch (Exception ex)
			{
				return new WcfResponse<List<Disclaimer>> { Result = null, Message = ex.Message };
			}
		}

		#endregion

		#region Report

		public WcfResponse<Confirmation> LoadConfirmationReports(int code, DateTime trading)
		{
			try
			{
				return new WcfResponse<Confirmation> { Result = ReportController.LoadConfirmationReports(code, trading) };
			}
			catch (Exception ex)
			{
				return new WcfResponse<Confirmation> { Message = ex.Message };
			}
		}

		public WcfResponse<List<Customer>> LoadClients(int code, string consultType, DateTime tradingDate, int? userId)
		{
			try
			{
				return new WcfResponse<List<Customer>> { Result = ReportController.LoadCustomers(code, consultType, tradingDate, userId) };
			}
			catch (Exception ex)
			{
				return new WcfResponse<List<Customer>> { Message = ex.Message };
			}
		}

		public WcfResponse<TradeBlotter> LoadTradeBlotterReports(DateTime tradingDate, int? userId)
		{
			try
			{
				return new WcfResponse<TradeBlotter> { Result = ReportController.LoadTradeBlotterReports(tradingDate, userId) };
			}
			catch (Exception ex)
			{
				return new WcfResponse<TradeBlotter> { Message = ex.Message };
			}
		}

		public WcfResponse<ReportHeader> LoadHeader()
		{
			try
			{
				return new WcfResponse<ReportHeader> { Result = ReportController.LoadHeader() };
			}
			catch (Exception ex)
			{
				return new WcfResponse<ReportHeader> { Message = ex.Message };
			}
		}

		public WcfResponse<CustomerStatement> LoadCustomerStatementReports(int code, string date, int? userId)
		{
			try
			{
				return new WcfResponse<CustomerStatement> { Result = ReportController.LoadCustomerStatementReports(code, date, userId) };
			}
			catch (Exception ex)
			{
				return new WcfResponse<CustomerStatement> { Message = ex.Message };
			}
		}

		public WcfResponse<CustomerLedger> LoadCustomerLedgerReports(int code, string date)
		{
			try
			{
				return new WcfResponse<CustomerLedger> { Result = ReportController.LoadCustomerLedgerReports(code, date) };
			}
			catch (Exception ex)
			{
				return new WcfResponse<CustomerLedger> { Message = ex.Message };
			}
		}

		public WcfResponse<StockRecord> LoadStockRecordReports(string date, int? userId)
		{
			try
			{
				return new WcfResponse<StockRecord> { Result = ReportController.LoadStockRecordReports(date, userId) };
			}
			catch (Exception ex)
			{
				return new WcfResponse<StockRecord> { Message = ex.Message };
			}
		}

		public WcfResponse<FailLedger> LoadFailLedgerReports(int code, DateTime tradingDate, FailLedgerType type, int? userId)
		{
			try
			{
				return new WcfResponse<FailLedger> { Result = ReportController.LoadFailLedgerReports(code, tradingDate, type, userId) };
			}
			catch (Exception ex)
			{
				return new WcfResponse<FailLedger> { Message = ex.Message };
			}
		}

		#endregion

		#region Brokerage

		public WcfResponse<Boolean> SettleBrokerageTransfers(string settlementDate, string settlementUsername)
		{
			try
			{
				return new WcfResponse<bool> { Result = BrokerageController.SettleTransfer(settlementDate, settlementUsername) };
			}
			catch (Exception ex)
			{
				return new WcfResponse<bool> { Result = false, Message = ex.Message };
			}
		}

		public WcfResponse<List<BrokerageTransfer>> LoadPendingBrokerageTransfers(int code, string date, int? userId)
		{
			try
			{
				return new WcfResponse<List<BrokerageTransfer>> { Result = BrokerageController.LoadPendingTransfers(code, date, userId) };
			}
			catch (Exception ex)
			{
				return new WcfResponse<List<BrokerageTransfer>> { Message = ex.Message };
			}
		}

		public WcfResponse<List<BrokerageTransfer>> LoadSettledBrokerageTransfers(int code, string startDate, string endDate, int? userId)
		{
			try
			{
				return new WcfResponse<List<BrokerageTransfer>> { Result = BrokerageController.LoadSettledTransfers(code, startDate, endDate, userId) };
			}
			catch (Exception ex)
			{
				return new WcfResponse<List<BrokerageTransfer>> { Message = ex.Message };
			}
		}

		#endregion
	}
}
