﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.Services.SagazWebService;

namespace QX3.Portal.Contracts.ServiceContracts
{
    [ServiceContract]
    public interface IFundContract
    {
        [OperationContract]
        WcfResponse<List<Fund>> FN_LoadFunds();

        [OperationContract]
        WcfResponse<FundDetails> FN_LoadFund(int id);

        [OperationContract]
        WcfResponse<List<Application>> FN_LoadClientApplicationsPerFund(int operationalCode, int fundId);

        [OperationContract]
        WcfResponse<decimal> FN_LoadClientFundBalance(int operationalCode, int fundCode);

        [OperationContract]
        WcfResponse<List<Redemption>> FN_LoadClientRedemptionsPerFund(int operationalCode, int fundId);

        [OperationContract]
        WcfResponse<bool> FN_InsertApplication(Application obj);

        [OperationContract]
        WcfResponse<bool> FN_InsertRedemption(Redemption obj);

        [OperationContract]
        WcfResponse<List<FundStatementItem>> FN_LoadStatement(DateTime startDate, DateTime endDate, int operationalCode, int fundCode);

        [OperationContract]
        WcfResponse<FundDetails> FN_LoadFundDetails(int? operationalCode, int fundCode);

        [OperationContract]
        WcfResponse<List<Fund>> FN_LoadClientFunds(int operationalCode);

        [OperationContract]
        WcfResponse<List<Fund>> FN_LoadClientCurrentFunds(int operationalCode);

        [OperationContract]
        WcfResponse<FundBalance> FN_LoadBalance(int operationalCode, int fundCode);

        [OperationContract]
        WcfResponse<List<FundBalanceChartItem>> FN_LoadBalanceChart(int operationalCode);

        [OperationContract]
        WcfResponse<List<FundBalancePendingApplicationItem>> FN_LoadBalancePendingApplications(int operationalCode);

        [OperationContract]
        WcfResponse<bool> FN_InsertAccession(int operationalCode, int fundCode, int modalityId);

        [OperationContract]
        WcfResponse<List<FundDetails>> FN_SuggestFundsToInvest(int operationalCode, decimal balance);

        [OperationContract]
        WcfResponse<decimal> FN_LoadBlockedBalance(int operationalCode);

        [OperationContract]
        WcfResponse<byte[]> FN_DownloadDocument(Int32 id);

        [OperationContract]
        WcfResponse<FinancialPositionDataItem> FN_GetUserBalance(FundClient client);

        [OperationContract]
        WcfResponse<bool> FN_SendTermUser(int documentId, string fileName, int fundCode, FundClient client);
    }
}
