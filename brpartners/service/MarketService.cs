﻿using System;
using System.Collections.Generic;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.Contracts.ServiceContracts;
using System.ServiceModel.Activation;
using QX3.Portal.Services.Business.Market;

namespace QX3.Portal.Services
{
	[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
	public class MarketService : IMarketContract
	{
		private Market marketController;

		private Market MarketController
		{
			get
			{
				marketController = marketController ?? new Market();
				return marketController;
			}
		}

        public WcfResponse<List<Proceeds>> LoadProceeds(ProceedsFilter filter, int? userId)
		{
			try
			{
                var proceeds = MarketController.LoadProceeds(filter, userId);
				return new WcfResponse<List<Proceeds>> { Result = proceeds };
			}
			catch (Exception ex)
			{
				return new WcfResponse<List<Proceeds>> { Result = null, Message = ex.Message };
			}
		}

		public WcfResponse<List<Stock>> LoadStocks()
		{
			try
			{
				return new WcfResponse<List<Stock>> { Result = MarketController.LoadStocks() };
			}
			catch (Exception ex)
			{
				return new WcfResponse<List<Stock>> { Result = null, Message = ex.Message };
			}
		}

        public WcfResponse<String> LoadAssessorName(int assessorID)
        {
            try
            {
                return new WcfResponse<string> { Result = MarketController.GetAssessorName(assessorID) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<string> { Message = ex.Message };
            }
        }

        public WcfResponse<List<CommonType>> AssessorNameSuggest(string assessorName)
        {
            try
            {
                return new WcfResponse<List<CommonType>> { Result = MarketController.AssessorNameSuggest(assessorName) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<CommonType>> { Message = ex.Message, Result = null };
            }
        }

        public WcfResponse<String> GetNextBusinessDay(int numberOfDays, string targetDate)
        {
            try
            {
                return new WcfResponse<string> { Result = MarketController.GetNextBusinessDay(numberOfDays, targetDate) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<string> { Message = ex.Message };
            }
        }

        public WcfResponse<int> GetNumberOfPending(string tableName, int statusId, string assessorsList)
        {
            try
            {
                return new WcfResponse<Int32> { Result = MarketController.GetNumberOfPending(tableName, statusId, assessorsList) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<Int32> { Result = 0, Message = ex.Message };
            }
        }

        public WcfResponse<StockExchanges> LoadClientExchanges(int clientCode)
        {
            try
            {
                return new WcfResponse<StockExchanges> { Result = MarketController.LoadClientExchanges(clientCode) };
            }
            catch (Exception ex)
            {
                return new WcfResponse<StockExchanges> { Result = null, Message = ex.Message };
            }
        }

	}
}
