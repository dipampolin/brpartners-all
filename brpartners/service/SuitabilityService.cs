﻿using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.Contracts.ServiceContracts;
using QX3.Portal.Services.Business;
using QX3.Portal.Services.Data.SuitabilityAdmin;
using QX3.Spinnex.Common.Services.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Activation;
using ADM = QX3.Portal.Contracts.DataContracts.SuitabilityAdmin;

namespace QX3.Portal.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class SuitabilityService : ISuitabilityContract
    {
        private SuitabilityMethods suitabilityController;

        private SuitabilityMethods SuitabilityController
        {
            get
            {
                suitabilityController = suitabilityController ?? new SuitabilityMethods();
                return suitabilityController;
            }
        }

        public WcfResponse<SuitabilityForm> Suitability(int client)
        {
            try
            {
                var form = SuitabilityController.Suitability(client);
                return new WcfResponse<SuitabilityForm> { Result = form };
            }
            catch (Exception ex)
            {
                return new WcfResponse<SuitabilityForm> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<SuitabilityForm> TestarSuitability(int idFormulario)
        {
            try
            {
                var form = SuitabilityController.TestarSuitability(idFormulario);
                return new WcfResponse<SuitabilityForm> { Result = form };
            }
            catch (Exception ex)
            {
                return new WcfResponse<SuitabilityForm> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<bool, int> GravarSuitability(SuitabilityInput userInput, string login, int cdCliente, int idClienteGlobal, int status, int investidor, int? score)
        {
            try
            {
                var id = SuitabilityController.GravarSuitability(userInput, login, cdCliente, idClienteGlobal, status, investidor, score);
                return new WcfResponse<bool, int> { Result = id > 0, Data = id };
            }
            catch (Exception ex)
            {
                return new WcfResponse<bool, int> { Result = false, Message = ex.Message };
            }
        }

        public WcfResponse<List<SuitabilityPerfilCliente>> PerfilClienteJustification(string cnpj, bool init)
        {
            try
            {
                var list = SuitabilityController.PerfilClienteJustification(cnpj, init);
                return new WcfResponse<List<SuitabilityPerfilCliente>> { Result = list };
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new FaultException(ex.Message);
            }
        }

        public WcfResponse<bool> GravarSuitabilityJustification(int suitabilityId, string message)
        {
            try
            {
                var list = SuitabilityController.GravarSuitabilityJustification(suitabilityId, message);
                return new WcfResponse<bool> { Result = list };
            }
            catch (Exception ex)
            {
                return new WcfResponse<bool> { Result = false, Message = ex.Message };
            }
        }        

        public WcfResponse<SuitabilityClientsResponse> SelecionaClientesSuitability(int? idClienteGlobal, long? cpfcnpj, string cdCliente, string status, string investidor, string desenquadrado)
        {
            try
            {
                var list = SuitabilityController.SelecionaClientesSuitability(idClienteGlobal, cpfcnpj, cdCliente, status, investidor, desenquadrado);
                return new WcfResponse<SuitabilityClientsResponse> { Result = list };
            }
            catch (Exception ex)
            {
                return new WcfResponse<SuitabilityClientsResponse> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<List<SuitabilityClients>> HistoricoClientesSuitability(int? idCliente, string cdCliente, string login)
        {
            try
            {
                var list = SuitabilityController.HistoricoClientesSuitability(idCliente, cdCliente, login);
                return new WcfResponse<List<SuitabilityClients>> { Result = list };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<SuitabilityClients>> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<SuitabilityAnswersResponse> SelecionaResultadoSuitability(string id)
        {
            var list = SuitabilityController.SelecionaResultadoSuitability(id);
            return new WcfResponse<SuitabilityAnswersResponse> { Result = list };

        }

        public WcfResponse<bool> AtualizaStatusSuitability(int cdCliente, int status)
        {
            try
            {
                var list = SuitabilityController.AtualizaStatusSuitability(cdCliente, status);
                return new WcfResponse<bool> { Result = list };
            }
            catch (Exception ex)
            {
                return new WcfResponse<bool> { Result = false, Message = ex.Message };
            }
        }

        public WcfResponse<List<SuitabilityClients>> HistoricosSuitability(int? idCliente, string cdCliente, string dtInicio, string dtFim, string tipo)
        {
            try
            {
                var list = SuitabilityController.HistoricosSuitability(idCliente, cdCliente, dtInicio, dtFim, tipo);
                return new WcfResponse<List<SuitabilityClients>> { Result = list };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<SuitabilityClients>> { Result = null, Message = ex.Message };
            }
        }

        //public WcfResponse<List<SuitabilityUnframedResponse>> VerificaDesenquadramento(int idCliente, int idPerfil, string dtInicio, string dtFim, string tipo)
        //{
        //    try
        //    {
        //        var list = SuitabilityController.VerificaDesenquadramento(idCliente, idPerfil, dtInicio, dtFim, tipo);
        //        return new WcfResponse<List<SuitabilityUnframedResponse>> { Result = list };
        //    }
        //    catch (Exception ex)
        //    {
        //        return new WcfResponse<List<SuitabilityUnframedResponse>> { Result = null, Message = ex.Message };
        //    }
        //}
        public WcfResponse<List<SuitabilityUnframedResponse>> VerificaNovoDesenquadramento(int idCliente, DateTime? dtInicio, DateTime? dtFim)
        {
            try
            {
                return new WcfResponse<List<SuitabilityUnframedResponse>>()
                {
                    Result = SuitabilityController.VerificaNovoDesenquadramento(idCliente, dtInicio, dtFim)
                };
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return new WcfResponse<List<SuitabilityUnframedResponse>>() { Message = ex.Message };
            }
        }

        public List<ADM.FormularioSuitability> ListarFormularios(string tipoPessoa, string status)
        {
            try
            {
                using (var dal = new SuitabilityAdminDAL())
                {
                    //dal.Connect();
                    return dal.ListarFormularios(tipoPessoa, status);
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new FaultException(ex.Message);
            }
        }

        public ADM.EdicaoSuitability AbrirFormulario(int id)
        {
            try
            {
                using (var dal = new SuitabilityAdminDAL())
                {
                    //dal.Connect();
                    return dal.AbrirFormulario(id);
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new FaultException(ex.Message);
            }
        }

        public ADM.EdicaoSuitability Salvar(ADM.RequisicaoSalvarSuitability requisicao)
        {
            try
            {
                if (requisicao.Formulario.Id < 0)
                    LogManager.WriteLog("Salvando novo formulário de Suitability");
                else
                    LogManager.WriteLog("Salvando alterações no formulário de Suitability (" + requisicao.Formulario.Id + ")");

                using (var dal = new SuitabilityAdminDAL())
                {
                    //dal.Connect();
                    //dal.BeginTransaction();
                    dal.PrepararComandosCrud();

                    var form = requisicao.Formulario;
                    if (form.Id < 0)
                        form.Id = dal.InserirFormulario(form);
                    else
                    {
                        dal.AtualizarFormulario(form);

                        if (requisicao.AlternativasRemovidas != null)
                            foreach (int id in requisicao.AlternativasRemovidas)
                                dal.ExcluirAlternativa(id);

                        if (requisicao.ColunasRemovidas != null)
                            foreach (int id in requisicao.ColunasRemovidas)
                                dal.ExcluirColuna(id);

                        if (requisicao.QuestoesRemovidas != null)
                            foreach (int id in requisicao.QuestoesRemovidas)
                                dal.ExcluirQuestao(id);
                    }

                    foreach (var q in requisicao.Questoes)
                    {
                        if (q.Id < 0)
                            q.Id = dal.InserirQuestao(form.Id, q);
                        else
                            dal.AtualizarQuestao(q);
                    }

                    foreach (var q in requisicao.Questoes)
                    {
                        foreach (var a in q.Alternativas)
                        {
                            if (a.UIDProximaQuestao.HasValue)
                            {
                                a.IdProximaQuestao = (from proxima in requisicao.Questoes
                                                      where proxima.UID == a.UIDProximaQuestao
                                                      select proxima.Id).Single();
                            }

                            if (a.Id < 0)
                                a.Id = dal.InserirAlternativa(q.Id, a);
                            else
                                dal.AtualizarAlternativa(a);
                        }

                        if (q.Colunas != null)
                        {
                            foreach (var m in q.Colunas)
                            {
                                if (m.Id < 0)
                                    m.Id = dal.InserirColuna(q.Id, m);
                                else
                                    dal.AtualizarColuna(m);
                            }
                        }
                    }

                    foreach (var p in requisicao.Pontuacao)
                    {
                        if (p.Id < 0)
                            p.Id = dal.InserirPontuacao(form.Id, p);
                        else
                            dal.AtualizarPontuacao(p);
                    }

                    // dal.Commit();
                    return dal.AbrirFormulario(form.Id);
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new FaultException(ex.Message);
            }
        }

        public bool DeletarFormulario(int id)
        {
            try
            {
                using (var dal = new SuitabilityAdminDAL())
                {
                    //dal.Connect();
                    if (dal.VerificarExclusao(id))
                    {
                        dal.PrepararComandosCrud();
                        dal.ExcluirFormulario(id);
                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw new FaultException(ex.Message);
            }
        }

        public WcfResponse<List<StatementClient>> GetClients(StatementClientFilter filter)
        {
            try
            {
                var list = SuitabilityController.GetClients(filter);
                return new WcfResponse<List<StatementClient>> { Result = list };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<StatementClient>> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<List<FixedIncomeDetails>> GetFixedIncome(FixedIncomeFilter filter)
        {
            try
            {
                var list = SuitabilityController.GetFixedIncome(filter);
                return new WcfResponse<List<FixedIncomeDetails>> { Result = list };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<FixedIncomeDetails>> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<List<NdfDetails>> GetNdf(NdfFilter filter)
        {
            try
            {
                var list = SuitabilityController.GetNdf(filter);
                return new WcfResponse<List<NdfDetails>> { Result = list };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<NdfDetails>> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<List<SwapDetails>> GetSwap(SwapFilter filter)
        {
            try
            {
                var list = SuitabilityController.GetSwap(filter);
                return new WcfResponse<List<SwapDetails>> { Result = list };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<SwapDetails>> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<StatementConfiguration> GetStatementConfiguration(int clientId)
        {
            try
            {
                var list = SuitabilityController.GetStatementConfiguration(clientId);
                return new WcfResponse<StatementConfiguration> { Result = list };
            }
            catch (Exception ex)
            {
                return new WcfResponse<StatementConfiguration> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<List<StatementLog>> GetStatementLog(StatementLog filter)
        {
            try
            {
                var list = SuitabilityController.GetStatementLog(filter);
                return new WcfResponse<List<StatementLog>> { Result = list };
            }
            catch (Exception ex)
            {
                return new WcfResponse<List<StatementLog>> { Result = null, Message = ex.Message };
            }
        }

        public WcfResponse<int> InsertStatementConfiguration(StatementConfiguration obj)
        {
            try
            {
                var list = SuitabilityController.InsertStatementConfiguration(obj);
                return new WcfResponse<int> { Result = list };
            }
            catch (Exception ex)
            {
                return new WcfResponse<int> { Result = 0, Message = ex.Message };
            }
        }

        public WcfResponse<int> InsertStatementLog(StatementLog obj)
        {
            try
            {
                var list = SuitabilityController.InsertStatementLog(obj);
                return new WcfResponse<int> { Result = list };
            }
            catch (Exception ex)
            {
                return new WcfResponse<int> { Result = 0, Message = ex.Message };
            }
        }
    }

}
