﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace QX3.Portal.Services
{
    public class Cryptography
    {
        public static string Encrypts(string text)
        {
            try
            {
                return EncryptValue(text, "votoweb");
            }
            catch (Exception ex)
            {
                return "String errada. " + ex.Message;
            }
        }

        public static string Decrypts(string text)
        {
            try
            {
                return DecryptValue(text, "votoweb");
            }
            catch (Exception ex)
            {
                return "String errada. " + ex.Message;
            }
        }

        protected static string EncryptValue(string text, string key)
        {
            try
            {
                var objcriptografaSenha = new TripleDESCryptoServiceProvider();
                var objcriptoMd5 = new MD5CryptoServiceProvider();

                string strTempKey = key;

                byte[] byteHash = objcriptoMd5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(strTempKey));
                objcriptoMd5 = null;
                objcriptografaSenha.Key = byteHash;
                objcriptografaSenha.Mode = CipherMode.ECB;

                byte[] byteBuff = ASCIIEncoding.ASCII.GetBytes(text);
                return Convert.ToBase64String(objcriptografaSenha.CreateEncryptor().TransformFinalBlock(byteBuff, 0, byteBuff.Length));
            }
            catch (Exception ex)
            {
                return "Digite os valores Corretamente." + ex.Message;
            }
        }

        protected static string DecryptValue(string text, string key)
        {
            try
            {
                var objdescriptografaSenha = new TripleDESCryptoServiceProvider();
                var objcriptoMd5 = new MD5CryptoServiceProvider();

                string strTempKey = key;

                byte[] byteHash = objcriptoMd5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(strTempKey));
                objcriptoMd5 = null;
                objdescriptografaSenha.Key = byteHash;
                objdescriptografaSenha.Mode = CipherMode.ECB;

                byte[] byteBuff = Convert.FromBase64String(text);
                string strDecrypted = ASCIIEncoding.ASCII.GetString(objdescriptografaSenha.CreateDecryptor().TransformFinalBlock(byteBuff, 0, byteBuff.Length));
                objdescriptografaSenha = null;

                return strDecrypted;
            }
            catch (Exception ex)
            {
                return "Digite os valores Corretamente." + ex.Message;
            }
        }
    }
}
