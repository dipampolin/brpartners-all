﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using QX3.Portal.WebSite.Helper;
using QX3.Portal.Contracts.DataContracts;
using QX3.Spinnex.Common.Services.Logging;
using QX3.Portal.WebSite.Models.Channel;
using QX3.Portal.WebSite.AccessControlService;
using QX3.Portal.WebSite.Properties;
using QX3.Portal.WebSite.Models;
using Newtonsoft.Json;
using QX3.Portal.WebSite.LogService;
using System.Text.RegularExpressions;
using QX3.Portal.WebSite.MarketService;
using QX3.Portal.WebSite.CommonService;
using System.Web;
using QX3.Portal.WebSite.RiskService;

namespace QX3.Portal.WebSite.Controllers
{

    public class RiskController : FileController
    {
        #region Load

        private List<GuaranteeListItem> GetGuaranteesFromSession()
        {
            List<GuaranteeListItem> guarantees = new List<GuaranteeListItem>();

            try
            {
                guarantees = Session["GuaranteesList"] != null ? ((GuaranteeListItem[])Session["GuaranteesList"]).ToList() : new List<GuaranteeListItem>();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }

            return guarantees;
        }

        [AccessControlAuthorize]
        public ActionResult Guarantees()
        {
            LogHelper.SaveAuditingLog("Risk.Guarantees");
            GuaranteesHelper gHelper = new GuaranteesHelper();

            IChannelProvider<ICommonContractChannel> channelProvider = ChannelProviderFactory<ICommonContractChannel>.Create("CommonClassName");
            var service = channelProvider.GetChannel().CreateChannel();

            service.Open();
            var status = service.LoadTypes("Risk.Guarantees", "STATUS").Result;
            service.Close();

            var statusToChange = (from s in status where s.Description.ToLower() != "para efetuar" select s).ToList();
            ViewBag.Status = new SelectList(statusToChange, "value", "description", "15");

            var filterStatus = status.ToList();
            filterStatus.Insert(0, new CommonType { Value = "-1", Description = "Em Garantia" });
            ViewBag.FilterStatus = new SelectList(filterStatus, "value", "description", SessionHelper.CurrentUser.IsBackOffice ? "15" : "0");

            return View();
        }

        public JsonResult LoadGuarantees(string Assessors, string Clients, string StatusID, string StartDate, string EndDate, int Index)
        {
            try
            {
                LogHelper.SaveAuditingLog("Risk.LoadGuarantees");

                GuaranteeFilter filter = new GuaranteeFilter();

                MarketHelper helper = new MarketHelper();
                filter.Assessors = (!String.IsNullOrEmpty(Assessors)) ? helper.GetAssessorsRangesToFilter(Assessors) : "";
                if (!string.IsNullOrEmpty(Clients))
                    filter.Clients = helper.GetClientsRangesToFilter(Clients);
                filter.StartDate = StartDate ?? DateTime.Now.AddDays(-3).ToString("dd/MM/yyyy");
                filter.EndDate = EndDate ?? DateTime.Now.ToString("dd/MM/yyyy");
                filter.StatusID = (!string.IsNullOrEmpty(StatusID)) ? Int32.Parse(StatusID) : 0;

                IChannelProvider<IRiskContractChannel> riskProvider = ChannelProviderFactory<IRiskContractChannel>.Create("RiskClassName");
                var riskService = riskProvider.GetChannel().CreateChannel();
                
                riskService.Open();
                var list = riskService.LoadGuarantees(filter, CommonHelper.GetLoggedUserGroupId()).Result.ToList();
                riskService.Close();

                int index = 0;
                int subIndex = 0;

                GuaranteeClientContent content = new GuaranteeClientContent();

                if (list.Count > 0)
                {
                    GuaranteeListItem[] guarantees = (from l in list
                                                      select new GuaranteeListItem
                                                      {
                                                          Assessor = l.Assessor.Description,
                                                          Client = l.Client,
                                                          Index = index++,
                                                          Balance = l.Balance,
                                                          Items = (from sl in l.Items
                                                                   select new GuaranteeListSubItem
                                                                   {
                                                                       Guarantee = sl.Guarantee,
                                                                       ID = sl.ID,
                                                                       Index = subIndex++,
                                                                       IsBovespa = sl.IsBovespa,
                                                                       IsWithdrawal = sl.IsWithdrawal,
                                                                       Quantity = sl.Quantity,
                                                                       Discount = sl.Discount,
                                                                       RequestedValue = sl.RequestedValue,
                                                                       Status = sl.Status,
                                                                       Total = sl.Total,
                                                                       Type = sl.Type,
                                                                       UnitPrice = sl.UnitPrice,
                                                                       UpdateDate = sl.UpdateDate
                                                                   }).ToList()
                                                      }).ToArray();

                    Session["GuaranteesList"] = guarantees;

                    GuaranteesHelper gHelper = new GuaranteesHelper();

                    content = gHelper.GetClientGuaranteeContent(guarantees[Index], filter.StatusID == -1);
                    content.Length = guarantees.Length;
                    content.Legend = string.Format("Exibindo {0} de {1}", (Index + 1), content.Length);
                }

                return Json(content);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return Json(new { Error = "Ocorreu um erro ao consultar as garantias." });
            }
        }

        public JsonResult LoadNextGuarantees(int Index, bool InGuarantee)
        {
            try
            {
                GuaranteeListItem[] guarantees = (GuaranteeListItem[])Session["GuaranteesList"];

                GuaranteesHelper gHelper = new GuaranteesHelper();

                GuaranteeClientContent content = gHelper.GetClientGuaranteeContent(guarantees[Index], InGuarantee);
                content.Length = guarantees.Length;
                content.Legend = string.Format("Exibindo {0} de {1}", Index + 1, content.Length);

                return Json(content);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return Json(new { Error = "Ocorreu um erro ao consultar as garantias." });
            }
        }

        public ActionResult GuaranteesToFile(bool isPdf)
        {
            try
            {
                var currentList = Session["GuaranteesList"] != null ? ((GuaranteeListItem[])Session["GuaranteesList"]).ToList() : new List<GuaranteeListItem>();
                //LogHelper.SaveAuditingLog("Risk.GuaranteesToFile", string.Format(LogResources.ObjectToFileParameters, isPdf ? "PDF" : "Excel", JsonConvert.SerializeObject(currentList)));
                string fileName = string.Format("garantias_{0}.{1}", DateTime.Now.ToString("dd-MM-yyyy"), isPdf ? "pdf" : "xls");

                LogHelper.SaveHistoryLog("Risk.Guarantees", new HistoryData { Action = "Exportar", OldData = string.Empty, NewData = JsonConvert.SerializeObject(currentList) });

                return isPdf ? ViewGuaranteesPdf(currentList, true, fileName) : ViewGuaranteesExcel(currentList, true, fileName);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = "Ocorreu um erro ao gerar relatório.";
                return this.RedirectToAction("Guarantees");
            }
        }

        private GuaranteeListItem GetGuaranteeFromSession(int subItemId)
        {
            GuaranteeListItem newGuarantee = new GuaranteeListItem();

            try
            {
                List<int> idList = new List<int>();
                idList.Add(subItemId);

                var currentGuarantees = GetGuaranteesFromSession();
                return (from g in currentGuarantees where g.Items.Exists(i => idList.Contains(i.ID)) select new GuaranteeListItem { Client = g.Client, Items = g.Items.Where(i => i.ID.Equals(subItemId)).ToList() }).FirstOrDefault();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }

            return newGuarantee;
        }

        public JsonResult GuaranteesSuggest(string term, string ClientCode, string IsBovespa)
        {
            List<GuaranteeSuggest> guarantees = new List<GuaranteeSuggest>();

            try
            {
                MarketHelper helper = new MarketHelper();

                if (!string.IsNullOrEmpty(ClientCode) && !string.IsNullOrEmpty(IsBovespa))
                {
                    GuaranteeFilter filter = new GuaranteeFilter();
                    filter.Clients = ClientCode;
                    filter.Assessors = ""; // helper.GetAssessorsRangesToFilter(string.Empty);
                    filter.Bovespa = IsBovespa == "1";

                    Session.Remove("GuaranteeSuggest");

                    IChannelProvider<IRiskContractChannel> channelProvider = ChannelProviderFactory<IRiskContractChannel>.Create("RiskClassName");
                    var service = channelProvider.GetChannel().CreateChannel();

                    service.Open();
                    var list = service.LoadClientGuarantees(filter, CommonHelper.GetLoggedUserGroupId()).Result.ToList();
                    service.Close();

                    guarantees = (from l in list select new GuaranteeSuggest { IsQuantity = l.IsQuantity.ToString(), UnitPrice = l.UnitPrice.ToString(), Guarantee = l.Guarantee, Available = l.Guarantee.ToLower().IndexOf("dinheiro") >= 0 ? "-" : l.Quantity.ToString("N0"), Total = l.Total.ToString("N2") }).Where(g => g.Guarantee.ToLower().Contains(term.ToLower())).OrderBy(u => u.Guarantee).ToList();

                    if (guarantees.Count == 0)
                        guarantees.Add(new GuaranteeSuggest { Guarantee = "Nenhuma garantia encontrada", Total = "-", Available = "-" });

                    Session["GuaranteeSuggest"] = guarantees;
                }

            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }

            return Json(guarantees, JsonRequestBehavior.AllowGet);

        }

        public JsonResult ValidateGuaranteesSuggest(string guarantee)
        {
            string errorMessage = string.Empty;

            if (string.IsNullOrEmpty(guarantee))
                return Json("true");

            try
            {
                var guarantees = (List<GuaranteeSuggest>)Session["GuaranteeSuggest"];
                return Json(guarantees.Any(g => g.Guarantee.Equals(guarantee)).ToString().ToLower());
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return Json("false");
            }
        }

        #endregion

        #region Request

        public ActionResult GuaranteesRequest(int id)
        {
            Guarantee guarantee = new Guarantee();
            try
            {
                LogHelper.SaveAuditingLog("Risk.GuaranteesRequest");

                if (id > 0)
                {
                    var guaranteeItem = GetGuaranteeFromSession(id);
                    guarantee.Client = guaranteeItem.Client;
                    guarantee.Items = (from i in guaranteeItem.Items select new GuaranteeItem { ID = i.ID, Total = i.Total, RequestedValue = i.RequestedValue, Guarantee = i.Guarantee, IsBovespa = i.IsBovespa, IsWithdrawal = i.IsWithdrawal, Quantity = i.Quantity, UnitPrice = i.UnitPrice }).ToList();
                    return View(guarantee);
                }
            }

            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }
            return View(guarantee ?? new Guarantee());
        }

        [HttpPost]
        public ActionResult GuaranteesRequest(Guarantee guarantee, string rbIsWithdrawal, string rbIsBovespa, FormCollection form)
        {
            string message = string.Empty;
            bool success = false;

            try
            {
                bool update = guarantee.Items[0].ID > 0;
                int id = guarantee.Items[0].ID;
                bool isWithdrawal;

                LogHelper.SaveAuditingLog("Risk.GuaranteesRequest.Post", string.Format(LogResources.AuditingInsertOrdersCorrection, JsonConvert.SerializeObject(guarantee)));

                IChannelProvider<IRiskContractChannel> channelProvider = ChannelProviderFactory<IRiskContractChannel>.Create("RiskClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                if (update)
                    isWithdrawal = form["hdfIsWithdrawal"].ToString() == "1" ? true : false;
                else
                    isWithdrawal = rbIsWithdrawal == "1";

                int counter = 0;
                guarantee.Items = new List<GuaranteeItem>();

                if (isWithdrawal)
                {
                    int wCounter = Int32.Parse(form["hdfCounter"]);

                    while (wCounter > counter)
                    {
                        GuaranteeItem i = new GuaranteeItem();
                        i.IsWithdrawal = isWithdrawal;
                        i.IsBovespa = rbIsBovespa == "1";
                        i.Guarantee = form["txtGuarantee" + counter].ToUpper();
                        i.RequestedValue = decimal.Parse(form["txtQuantity" + counter].Replace(".", ""));
                        i.UnitPrice = decimal.Parse(form["hdfUnitPrice" + counter].Replace(".", ""));
                        i.IsQuantity = i.Guarantee.IndexOf("DINHEIRO") <= -1;
                        guarantee.Items.Add(i);
                        counter++;
                    }
                }

                else
                {
                    var x = form["hdfDepositCounter"];
                    int depositCounter = Int32.Parse(form["hdfDepositCounter"]);

                    while (depositCounter > counter)
                    {
                        GuaranteeItem item = new GuaranteeItem();
                        item.IsWithdrawal = isWithdrawal;
                        item.IsBovespa = rbIsBovespa == "1";
                        item.Guarantee = form["txtDepGuarantee" + counter].ToUpper();
                        item.RequestedValue = decimal.Parse(form["txtDepQuantity" + counter].Replace(".", ""));
                        item.UnitPrice = decimal.Parse(form["txtDepUnitPrice" + counter].Replace(".", ""));
                        item.IsQuantity = item.Guarantee.IndexOf("DINHEIRO") <= -1;

                        guarantee.Items.Add(item);
                        counter++;
                    }
                }
                service.Open();
                if (update)
                {
                    guarantee.Items[0].ID = id;
                    var responseUpdate = service.UpdateGuarantee(guarantee.Items[0]);

                    if (responseUpdate.Result)
                    {
                        LogHelper.SaveHistoryLog("Risk.Guarantees", new HistoryData { Action = "Alterar Solicitação", ID = guarantee.Items[0].ID, Target = guarantee.Client.Description, TargetID = int.Parse(guarantee.Client.Value) });
                        success = responseUpdate.Result;
                        message = "Solicitação alterada com sucesso.";
                    }
                }
                else
                {
                    var response = service.InsertGuarantee(guarantee);
                    if (response.Result != null && response.Result.Count() > 0)
                        success = true;

                    foreach (int resultId in response.Result)
                    {
                        LogHelper.SaveHistoryLog("Risk.Guarantees", new HistoryData { Action = "Solicitar", ID = resultId, Target = guarantee.Client.Description, TargetID = int.Parse(guarantee.Client.Value) });
                        message = "Solicitação cadastrada com sucesso.";
                    }
                }
                service.Close();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                message = "Não foi possível salvar solicitação.";
            }

            return Json(new { message = message, success = success });
        }

        #endregion

        #region Realize

        public ActionResult GuaranteeConfirmRealize(string guaranteesIds)
        {
            List<string> ids = new List<string>();
            ids = guaranteesIds.Split(';').ToList();
            return View(ids);
        }

        [HttpPost]
        public ActionResult RealizeGuarantees(string guaranteesIds, string ddlStatusChange)
        {
            string message = string.Empty;
            bool success = false;

            try
            {
                LogHelper.SaveAuditingLog("Risk.RealizeGuarantees", string.Format(LogResources.AuditingDeleteOrdersCorrection, guaranteesIds));

                IChannelProvider<IRiskContractChannel> channelProvider = ChannelProviderFactory<IRiskContractChannel>.Create("RiskClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                string[] hdfIds = guaranteesIds.Split(';');
                bool changed = true;

                foreach (string hdfId in hdfIds)
                {
                    if (!string.IsNullOrEmpty(hdfId))
                    {
                        int guaranteeId = Int32.Parse(hdfId);
                        if (!service.ChangeGuaranteeStatus(guaranteeId, Int32.Parse(ddlStatusChange ?? "14")).Result)
                            changed = false;
                        else
                        {
                            var itemGuarantee = GetGuaranteeFromSession(guaranteeId);
                            LogHelper.SaveHistoryLog("Risk.Guarantees", new HistoryData { Action = "Efetuar", ID = guaranteeId, TargetID = Int32.Parse(itemGuarantee.Client.Value), Target = itemGuarantee.Client.Description });
                        }
                    }
                }

                if (changed)
                {
                    success = true;
                    message = string.Format("Solicitação {0} com sucesso.", string.IsNullOrEmpty(ddlStatusChange) || ddlStatusChange == "14" ? "efetuada" : "reprovada");
                    Session["GuaranteesList"] = null;
                }
                else
                    message = string.Format("Não foi possível {0} solicitação.", string.IsNullOrEmpty(ddlStatusChange) || ddlStatusChange == "14" ? "efetuar" : "reprovar");

                service.Close();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                message = string.Format("Ocorreu um erro ao  {0} solicitação.", string.IsNullOrEmpty(ddlStatusChange) || ddlStatusChange == "14" ? "efetuar" : "reprovar"); ;
            }

            return Json(new { message = message, success = success });
        }

        #endregion

        #region Delete

        public ActionResult GuaranteeConfirmDelete(int guaranteeId)
        {
            LogHelper.SaveAuditingLog("Risk.GuaranteeConfirmDelete");
            GuaranteeListItem item = new GuaranteeListItem();
            try
            {
                item = GetGuaranteeFromSession(guaranteeId);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }
            return View(item);
        }

        [HttpPost]
        public ActionResult GuaranteeDelete(string hdfGuaranteeId)
        {
            string message = string.Empty;
            bool success = false;

            try
            {
                LogHelper.SaveAuditingLog("Risk.GuaranteeDelete", string.Format(LogResources.AuditingDeleteTerm, hdfGuaranteeId));

                IChannelProvider<IRiskContractChannel> channelProvider = ChannelProviderFactory<IRiskContractChannel>.Create("RiskClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                int guaranteeId = Int32.Parse(hdfGuaranteeId);
                var changed = service.DeleteGuarantee(guaranteeId).Result;

                if (changed)
                {
                    success = true;
                    message = "Solicitação excluída com sucesso.";

                    var item = GetGuaranteeFromSession(guaranteeId);

                    LogHelper.SaveHistoryLog("Risk.Guarantees", new HistoryData { Action = "Excluir", ID = guaranteeId, Target = item.Client.Description, TargetID = Int32.Parse(item.Client.Value) });
                    Session["GuaranteesList"] = null;
                }
                else
                    message = "Não foi possível exluir solicitação.";

                service.Close();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                message = "Ocorreu um erro ao exluir a solicitação.";
            }

            return Json(new { message = message, success = success });
        }

        #endregion

        #region History

        public ActionResult GuaranteesHistory()
        {
            List<SelectListItem> listItems = new List<SelectListItem>();
            listItems.Add(new SelectListItem { Text = "Todas", Value = "", Selected = true });

            try
            {
                var actions = GuaranteesHelper.GuaranteesActions;
                foreach (string a in actions)
                    listItems.Add(new SelectListItem { Text = a, Value = a, Selected = false });

                ViewBag.ActionsList = listItems;

                LogHelper.SaveAuditingLog("Risk.GuaranteesHistory");

                return View();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = "Não foi possível carregar o histórico das garantias.";
                return View();
            }
        }

        public ActionResult GuaranteesHistoryToFile(bool isPdf, string detail)
        {
            return this.HistoryToFile(isPdf, "Guarantees", string.IsNullOrEmpty(detail) ? "GuaranteesHistory" : "GuaranteesHistoryDetail", "Garantias", "Cliente", true);
        }

        public ActionResult HistoryToFile(bool isPdf, string targetName, string sessionName, string targetTitle, string targetColumnTitle, bool hasAction)
        {
            List<HistoryData> currentList = new List<HistoryData>();

            try
            {
                if (Session[sessionName] != null)
                    currentList = (List<HistoryData>)Session[sessionName];

                //LogHelper.SaveAuditingLog(string.Concat("Risk.HistoryToFile.", targetName), string.Format(LogResources.ObjectToFileParameters, isPdf ? "PDF" : "Excel", JsonConvert.SerializeObject(currentList)));

                string name = new Regex(@"\s*").Replace(HttpUtility.HtmlDecode(targetTitle), string.Empty);
                targetTitle = string.Concat(isPdf ? "Histórico de " : "Hist&#243;rico de ", targetTitle);

                string fileName = string.Format("historico_{0}_{1}.{2}", name, DateTime.Now.ToString("dd-MM-yyyy"), isPdf ? "pdf" : "xls");

                return isPdf ? ViewHistoryPdf(currentList, true, fileName, targetTitle, targetColumnTitle, hasAction) : ViewHistoryExcel(currentList, true, fileName, targetTitle, targetColumnTitle, hasAction);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return this.RedirectToAction("OrdersCorrection");
            }


        }

        public ActionResult GuaranteesHistoryDetail(int guaranteeID)
        {
            try
            {
                LogHelper.SaveAuditingLog("Risk.GuaranteesHistoryDetail");

                var guarantee = GetGuaranteeFromSession(guaranteeID);

                HistoryFilter filter = new HistoryFilter { ModuleName = "Risk.Guarantees", ID = guaranteeID };

                var data = LogHelper.LoadHistory(filter);

                Session["GuaranteesHistoryDetail"] = data;
                ViewBag.GuaranteeHistoryDetails = data;

                return View(guarantee);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = "Não foi possível carregar o histórico da garantia.";
                return View(new Guarantee());
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public void LoadGuaranteesHistory(FormCollection form)
        {
            try
            {

                string startDate = form["txtStartDate"];
                string endDate = form["txtEndDate"];

                long responsibleID = string.IsNullOrEmpty(form["hdfResponsible"]) ? 0 : long.Parse(form["hdfResponsible"]);
                int targetID = string.IsNullOrEmpty(form["txtTarget"]) ? 0 : Int32.Parse(form["txtTarget"]);

                int id = 0;
                Int32.TryParse(form["hdfCorrectionID"], out id);

                string action = form["ddlGuaranteesActions"];

                LogHelper.SaveAuditingLog("Risk.LoadGuaranteesHistory", string.Format(LogResources.AuditingLoadHistory, targetID, responsibleID, action, startDate, endDate));

                HistoryFilter filter = new HistoryFilter { ModuleName = "Risk.Guarantees", StartDate = startDate, Action = action, EndDate = endDate, ResponsibleID = responsibleID, TargetID = targetID, ID = id };

                Session["GuaranteesHistory"] = LogHelper.LoadHistory(filter);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }
        }

        #endregion

    }
}
