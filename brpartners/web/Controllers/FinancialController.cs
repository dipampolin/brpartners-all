﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.WebSite.CommonService;
using QX3.Portal.WebSite.FinancialService;
using QX3.Portal.WebSite.Helper;
using QX3.Portal.WebSite.LogService;
using QX3.Portal.WebSite.Models;
using QX3.Portal.WebSite.Models.Channel;
using QX3.Portal.WebSite.Properties;
using QX3.Spinnex.Common.Services.Logging;

namespace QX3.Portal.WebSite.Controllers
{
    public class FinancialController : FileController
    {
        #region Projected Report

        [AccessControlAuthorize]
        public ActionResult ProjectedReport()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public void LoadProjectedReport(string Assessors, string Clients, char Type, string TradingDate, string SettleDate)
        {
            try
            {
                LogHelper.SaveAuditingLog("Financial.LoadProjectedReport");

                MarketHelper helper = new MarketHelper();

                ProjectedReportFilter filter = new ProjectedReportFilter();
                filter.Assessors = String.IsNullOrEmpty(Assessors) ? "" : helper.GetAssessorsRangesToFilter(Assessors);
                if (!string.IsNullOrEmpty(Clients))
                    filter.Clients = helper.GetClientsRangesToFilter(Clients);
                filter.TradingDate = TradingDate;
                filter.SettlementDate = SettleDate;
                filter.Type = Type;

                IChannelProvider<IFinancialContractChannel> channelProvider = ChannelProviderFactory<IFinancialContractChannel>.Create("FinancialClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                var report = service.LoadProjectedReport(filter, CommonHelper.GetLoggedUserGroupId()).Result;

                Session["Financial.ProjectedReport"] = report == null ? new List<ProjectedReportItem>() : report.ToList();

                LogHelper.SaveAuditingLog("Proceeds.LoadProceeds", string.Format(LogResources.AuditingLoadProceeds, JsonConvert.SerializeObject(filter), JsonConvert.SerializeObject(Session["Proceeds"])));

                service.Close();

            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }
        }

        public ActionResult ConfirmProjectedExport()
        {
            return View();
        }

        public ActionResult ProjectedReportToFile(string exportType, string groupBy)
        {
            List<ProjectedReportItem> currentList = new List<ProjectedReportItem>();
            bool exportToPdf = exportType == "PDF";
            try
            {
                if (Session["Financial.ProjectedReport"] != null)
                    currentList = (List<ProjectedReportItem>)Session["Financial.ProjectedReport"];

                //LogHelper.SaveAuditingLog("Financial.ProjectedReportToFile", string.Format(LogResources.ObjectToFileParameters, exportToPdf ? "PDF" : "Excel", JsonConvert.SerializeObject(currentList)));

                string fileName = string.Format("projetado_{0}.{1}", DateTime.Now.ToString("dd-MM-yyyy"), exportToPdf ? "pdf" : "xls");

                LogHelper.SaveHistoryLog("Financial.ProjectedReport", new HistoryData { Action = "Exportar", OldData = string.Empty, NewData = JsonConvert.SerializeObject(currentList) });

                return ViewProjectedReportToFile(currentList, true, fileName, groupBy, exportToPdf);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return this.RedirectToAction("ProjectedReport");
            }
        }

        #endregion

		#region Redemption Request

        [AccessControlAuthorize]
        public ActionResult RedemptionRequest()
        {
            string statusID = null;

            ViewBag.Status = new SelectList(this.LoadRedemptionRequestStatus(), "value", "description", statusID);
            ViewBag.Pending = this.LoadPendingRequests();

            return View();
        }

        public ActionResult RedemptionRequestItemsToFile(bool isPdf)
        {
            List<RedemptionRequestListItem> currentList = new List<RedemptionRequestListItem>();
            try
            {
				if (Session["RedemptionRequest.List"] != null)
					currentList = (List<RedemptionRequestListItem>)Session["RedemptionRequest.List"];

                string fileName = string.Format("solicitacaoresgate_{0}.{1}", DateTime.Now.ToString("dd-MM-yyyy"), isPdf ? "pdf" : "xls");

                LogHelper.SaveHistoryLog("Financial.RedemptionRequest", new HistoryData { Action = "Exportar", OldData = string.Empty, NewData = JsonConvert.SerializeObject(currentList) });

                return isPdf ? ViewRedemptionRequestItemsPdf(currentList, true, fileName) : ViewRedemptionRequestItemsExcel(currentList, true, fileName);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = "Ocorreu um erro ao gerar relatório.";
                return this.RedirectToAction("RedemptionRequest");
            }


        }

        private List<CommonType> LoadRedemptionRequestStatus()
        {
            try
            {
                List<CommonType> lst = (List<CommonType>)Session["RedemptionRequest.Status"];

                if (lst == null)
                {
                    lst = new List<CommonType>();
                    lst.Add(new CommonType { Value = "", Description = "Todos" });

                    IChannelProvider<ICommonContractChannel> channelProvider = ChannelProviderFactory<ICommonContractChannel>.Create("CommonClassName");
                    var service = channelProvider.GetChannel().CreateChannel();

                    service.Open();
                    var result = service.LoadTypes("Financial.RedemptionRequest", "status").Result;
                    service.Close();

                    if (result != null && result.Count() > 0)
                        lst.AddRange(result.ToList());

                    Session["RedemptionRequest.Status"] = lst;
                }

                return lst;
            }
            catch (Exception)
            {
                List<CommonType> lst = new List<CommonType>();
                lst.Add(new CommonType { Value = "", Description = "Todos" });
                return lst;
            }
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public void LoadRedemptionRequests(string assessors, string clients, string status, string startDate, string endDate)
        {
            try
            {
                LogHelper.SaveAuditingLog("Financial.LoadRedemptionRequests");

                RedemptionRequestFilter filter = new RedemptionRequestFilter();

                MarketHelper helper = new MarketHelper();
                filter.Assessors = String.IsNullOrEmpty(assessors) ? "" : helper.GetAssessorsRangesToFilter(assessors);
                if (!string.IsNullOrEmpty(clients))
                    filter.Clients = helper.GetClientsRangesToFilter(clients);

                filter.StatusID = string.IsNullOrEmpty(status) ? 0 : Convert.ToInt32(status);

                if (!string.IsNullOrEmpty(startDate))
                    filter.StartDate = startDate;

                if (!string.IsNullOrEmpty(endDate))
                    filter.EndDate = endDate;

                IChannelProvider<IFinancialContractChannel> financialProvider = ChannelProviderFactory<IFinancialContractChannel>.Create("FinancialClassName");
                var service = financialProvider.GetChannel().CreateChannel();

                service.Open();

                var list = service.LoadRedemptionRequest(filter, CommonHelper.GetLoggedUserGroupId()).Result;

                service.Close();

                int index = 0;
                Session["RedemptionRequest.List"] = (from t in list
                                                     select new RedemptionRequestListItem
                                                     {
                                                         Id = t.ID.ToString(),
                                                         RequestedDate = t.RequestDate.ToString("dd/MM/yyyy HH:mm"),
                                                         ClientName = t.Client.Description,
                                                         ClientCode = t.Client.Value,
                                                         Bank = string.Format("{0} - {1}", t.BankAccount.BankCode, t.BankAccount.BankName),
                                                         Agency = string.Format("{0}{1}", t.BankAccount.AgencyNumber.ToString(), string.IsNullOrEmpty(t.BankAccount.AgencyDigit) ? "" : "-" + t.BankAccount.AgencyDigit),
                                                         Account = string.Format("{0}{1}", t.BankAccount.AccountNumber.ToString(), string.IsNullOrEmpty(t.BankAccount.AccountDigit) ? "" : "-" + t.BankAccount.AccountDigit),
                                                         Amount = t.RedemptionValue.ToString("N2"),
                                                         StatusId = t.Status.Value,
                                                         StatusDescription = t.Status.Description,
                                                         Observation = t.Observation,
                                                         Index = index++
                                                     }).ToList();

            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }
        }

        public ActionResult RedemptionRequestEdit(string id)
        {
            RedemptionRequestItem obj = new RedemptionRequestItem();

            try
            {
                if (!string.IsNullOrEmpty(id) && !id.Equals("0"))
                {
                    var lstObj = (List<RedemptionRequestListItem>)Session["RedemptionRequest.List"];
                    if (lstObj != null)
                    {
                        var o = (from d in lstObj where d.Id.Equals(id) select d).FirstOrDefault();
                        obj.Client = new CommonType { Value = o.ClientCode.Split('-')[0], Description = o.ClientName };
                        obj.RedemptionValue = Convert.ToDecimal(o.Amount);
                        obj.ID = Convert.ToInt32(o.Id);
                        RedemptionRequestBankAccount bank = new RedemptionRequestBankAccount();
                        bank.AccountNumber = o.Account.Split('-')[0];
                        obj.BankAccount = bank;
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = "Não foi possível carregar a tela de Solicitação de Resgate.";
                return View();
            }

            return View(obj ?? new RedemptionRequestItem());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult RedemptionRequestEdit(string clientCode, string amount, string bank, string requestId)
        {
            try
            {
                LogHelper.SaveAuditingLog("Financial.RedemptionRequestEdit");

                RedemptionRequestItem obj = new RedemptionRequestItem();
                obj.Client = new CommonType { Value = clientCode };
                obj.RedemptionValue = Convert.ToDecimal(amount);
                obj.ID = Convert.ToInt32(requestId);

                RedemptionRequestBankAccount bankAccount = new RedemptionRequestBankAccount();

                string[] arrBank = bank.Split('#');
                bankAccount.BankCode = arrBank[0];

                if (arrBank[1].Contains('-'))
                {
                    string[] arrAgency = arrBank[1].Split('-');
                    bankAccount.AgencyNumber = arrAgency[0];
                    bankAccount.AgencyDigit = arrAgency[1];
                }
                else
                    bankAccount.AgencyNumber = arrBank[1];

                bankAccount.AccountType = arrBank[2];

                if (arrBank[3].Contains('-'))
                {
                    string[] arrAccount = arrBank[3].Split('-');
                    bankAccount.AccountNumber = arrAccount[0];
                    bankAccount.AccountDigit = arrAccount[1];
                }
                else
                    bankAccount.AccountNumber = arrBank[3];

                obj.BankAccount = bankAccount;

                IChannelProvider<IFinancialContractChannel> channelProvider = ChannelProviderFactory<IFinancialContractChannel>.Create("FinancialClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                var insert = service.InsertRedemptionRequest(obj).Result;
                
                service.Close();

                if (insert.Result)
                {
                    string action = obj.ID.Equals(0) ? "Solicitar" : "Alterar";
                    LogHelper.SaveHistoryLog("Financial.RedemptionRequest", new HistoryData { Action = action, ID = insert.Id, NewData = JsonConvert.SerializeObject(obj) });
                }

                return Json(new { Error = insert.Result ? "" : "Não foi possível solicitar resgate para o cliente." });
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return Json(new { Error = "Erro ao solicitar resgate para o cliente." });
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult LoadBankAccountInformation(string code)
        {
            try
            {
                IChannelProvider<IFinancialContractChannel> channelProvider = ChannelProviderFactory<IFinancialContractChannel>.Create("FinancialClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                var objAccount = service.LoadBankAccountInformation(code).Result;
                var objBalance = service.LoadRedemptionRequestBalance(code, "", CommonHelper.GetLoggedUserGroupId()).Result;

                service.Close();

                Session["RedemptionRequest.Balance"] = objBalance;
                RedemptionRequestBalanceItem balance = new RedemptionRequestBalanceItem();
                balance.Available = objBalance.Available.ToString("N2");
                balance.AvailableD1 = objBalance.AvailableD1.ToString("N2");
                balance.AvailableD2 = objBalance.AvailableD2.ToString("N2");
                balance.AvailableD3 = objBalance.AvailableD3.ToString("N2");
                balance.Total = objBalance.Total.ToString("N2");
                balance.Intended = objBalance.Intended.ToString("N2");

                string bankAccount = string.Empty;
                string error = string.Empty;
                if (objAccount != null && objAccount.Length > 0)
                {
                    foreach (var i in objAccount)
                    {
                        string agency = i.AgencyNumber + (string.IsNullOrEmpty(i.AgencyDigit) ? "" : "-" + i.AgencyDigit);
                        string account = i.AccountNumber + (string.IsNullOrEmpty(i.AccountDigit) ? "" : "-" + i.AccountDigit);
                        bankAccount += string.Format("<option value=\"{0}#{1}#{2}#{3}\">{4} - Ag. {1} - {2} {3}</option>", i.BankCode, agency, i.AccountType, account, i.BankName);
                    }
                }
                else
                    error = "<option>Error: O cliente não possui conta bancária cadastrada.<option>";

                return Json(new { BankAccount = bankAccount, Balance = balance, Error = error });
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return Json(new { BankAccount = "", Balance = "", Error = "<option>Error: Não foi possível carregar as contas do cliente.<option>", ErrorMessage = ex.Message, StackTrace = ex.StackTrace });
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult CalculateBalanceInformation(string amount)
        {
            try
            {
                RedemptionRequestBalance balance = (RedemptionRequestBalance)Session["RedemptionRequest.Balance"];

                RedemptionRequestBalanceItem obj = new RedemptionRequestBalanceItem();

                var requestedValue = Convert.ToDecimal(amount);
                obj.Available = (balance.Available - requestedValue).ToString("N2");
                obj.AvailableD1 = (balance.AvailableD1 - requestedValue).ToString("N2");
                obj.AvailableD2 = (balance.AvailableD2 - requestedValue).ToString("N2");
                obj.AvailableD3 = (balance.AvailableD3 - requestedValue).ToString("N2");

                obj.Total = (balance.Total - requestedValue).ToString("N2");
                obj.Intended = (balance.Intended - requestedValue).ToString("N2");

                return Json(new { Balance = obj });
            }
            catch (Exception)
            {
                return Json(new { Balance = "" });
            }
        }

        public ActionResult RedemptionRequestHistory()
        {
            List<SelectListItem> listItems = new List<SelectListItem>();
            listItems.Add(new SelectListItem { Text = "Todas", Value = "", Selected = true });

            try
            {
                var actions = new string[] { "Solicitar", "Alterar", "Aprovar", "Reprovar", "Efetuar", "Excluir", "Exportar Excel", "Exportar PDF" };
                foreach (string a in actions)
                    listItems.Add(new SelectListItem { Text = a, Value = a, Selected = false });

                ViewBag.ActionsList = listItems;

                LogHelper.SaveAuditingLog("Financial.RedemptionRequestHistory");

                return View();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = "Não foi possível carregar o histórico das garantias.";
                return View();
            }
        }

        [HttpPost]
        public void LoadRedemptionRequestHistory(FormCollection form)
        {
            try
            {
                string startDate = form["txtStartDate"];
                string endDate = form["txtEndDate"];

                long responsibleID = string.IsNullOrEmpty(form["hdfResponsible"]) ? 0 : long.Parse(form["hdfResponsible"]);
                int targetID = string.IsNullOrEmpty(form["txtTarget"]) ? 0 : Int32.Parse(form["txtTarget"]);

                int id = 0;
                Int32.TryParse(form["hdfCorrectionID"], out id);

                string action = form["ddlRedemptionRequestActions"];

                LogHelper.SaveAuditingLog("Financial.LoadRedemptionRequestHistory", string.Format(LogResources.AuditingLoadHistory, targetID, responsibleID, action, startDate, endDate));

                HistoryFilter filter = new HistoryFilter { ModuleName = "Financial.RedemptionRequest", StartDate = startDate, Action = action, EndDate = endDate, ResponsibleID = responsibleID, TargetID = targetID, ID = id };

                Session["RedemptionRequestHistory"] = LogHelper.LoadHistory(filter);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }
        }

        public ActionResult RedemptionRequestHistoryDetail(string requestId)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            try
            {
                LogHelper.SaveAuditingLog("Financial.RedemptionRequestHistoryDetail");

                var list = Session["RedemptionRequest.List"] != null ? (List<RedemptionRequestListItem>)Session["RedemptionRequest.List"] : new List<RedemptionRequestListItem>();
                var rdpRequest = (from c in list where c.Id.Equals(requestId) select c).FirstOrDefault();

                HistoryFilter filter = new HistoryFilter { ModuleName = "Financial.RedemptionRequest", ID = Convert.ToInt32(rdpRequest.Id) };

                var data = LogHelper.LoadHistory(filter);

                Session["RedemptionRequestHistoryDetail"] = data;
                ViewBag.RedemptionRequestHistoryDetails = data;

                ViewData.ModelState.Clear();

                return View(rdpRequest);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = "Não foi possível carregar o histórico deste termo.";
                return View(new RedemptionRequestListItem());
            }
        }

        public ActionResult RedemptionRequestHistoryToFile(bool isPdf, bool detail)
        {
            return this.HistoryToFile(isPdf, "RedemptionRequest", detail ? "RedemptionRequestHistoryDetail" : "RedemptionRequestHistory", "Solicita&#231;&#227;o de Resgate", null, true);
        }

        public ActionResult HistoryToFile(bool isPdf, string targetName, string sessionName, string targetTitle, string targetColumnTitle, bool hasAction)
        {
            List<HistoryData> currentList = new List<HistoryData>();

            try
            {
                if (Session[sessionName] != null)
                    currentList = (List<HistoryData>)Session[sessionName];

                //LogHelper.SaveAuditingLog(string.Concat("Financial.HistoryToFile.", targetName), string.Format(LogResources.ObjectToFileParameters, isPdf ? "PDF" : "Excel", JsonConvert.SerializeObject(currentList)));


                string name = new Regex(@"\s*").Replace(HttpUtility.HtmlDecode(targetTitle), string.Empty);
                targetTitle = string.Concat(isPdf ? "Histórico de " : "Hist&#243;rico de ", targetTitle);

                string fileName = string.Format("historico_{0}_{1}.{2}", name, DateTime.Now.ToString("dd-MM-yyyy"), isPdf ? "pdf" : "xls");

                return isPdf ? ViewHistoryPdf(currentList, true, fileName, targetTitle, targetColumnTitle, hasAction) : ViewHistoryExcel(currentList, true, fileName, targetTitle, targetColumnTitle, hasAction);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return this.RedirectToAction("RedemptionRequest");
            }

        }

        [HttpPost]
        public int GetPendingRequests()
        {
            return this.LoadPendingRequests();
        }

        public ActionResult RedemptionRequestConfirmDelete(string code, string name)
        {

            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DeleteRedemptionRequest(string hdfRequestId, string hdfClientCode)
        {
            try
            {
                IChannelProvider<IFinancialContractChannel> channelProvider = ChannelProviderFactory<IFinancialContractChannel>.Create("FinancialClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                var delete = service.DeleteRedemptionRequest(Convert.ToInt32(hdfRequestId), hdfClientCode).Result;

                service.Close();

                string message = string.Empty;
                string error = string.Empty;

                if (delete)
                {
                    LogHelper.SaveHistoryLog("Financial.RedemptionRequest", new HistoryData { Action = "Excluir", ID = Convert.ToInt32(hdfRequestId) });
                    message = "Solicitação de resgate excluída com sucesso.";
                }
                else
                {
                    message = "Não foi possível excluir a solicitação de resgate.";
                    error = "true";
                }

                return Json(new { Error = error, Message = message });
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return Json(new { Error = "true", Message = "Ocorreu um erro ao excluir a solicitação de resgate." });
            }
        }
		
		[AcceptVerbs(HttpVerbs.Post)]
		public JsonResult RedemptionRequestChangeStatus(string hdId, string hdClientCode, string ddlStatusChange, string taDisapproveNote)
        {
            try
            {
				LogHelper.SaveAuditingLog("Financial.RedemptionRequestChangeStatus", string.Format(LogResources.AuditingRedemptionRequestChangeStatus, hdId, ddlStatusChange, taDisapproveNote));

				IChannelProvider<IFinancialContractChannel> channelProvider = ChannelProviderFactory<IFinancialContractChannel>.Create("FinancialClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

				bool result = service.ChangeRedemptionRequestStatus(new RedemptionRequestItem()
				{
					ID = Convert.ToInt16(hdId),
                    Client = new CommonType() { Value = hdClientCode },
					Status = new CommonType() { Value = ddlStatusChange },
					Observation = taDisapproveNote
				}).Result;

                service.Close();

                string error = string.Empty;
                string message = string.Empty;

				if (result)
				{
                    string action = ddlStatusChange == "18" ? "Aprovar" : ddlStatusChange == "19" ? "Efetuar" : "Reprovar";
					LogHelper.SaveHistoryLog("Financial.RedemptionRequest", new HistoryData { Action = action, ID = Convert.ToInt32(hdId), NewData = ddlStatusChange });
                    message = "Status da solicitação alterado com sucesso.";
				}
				else
				{
                    message = "Não foi possível alterar o status da solicitação.";
                    error = "true";
				}

                return Json(new { Error = error, Messaage = message });
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return Json(new { Error = "true", Messaage = "Ocorreu um erro ao alterar o status da solicitação." });
            }
        }
		
        public ActionResult RedemptionRequestEffect(string requestsIds)
        {
            try
            {
                string[] arrRequestsIds = requestsIds.Split(';');
                var lstObj = (List<RedemptionRequestListItem>)Session["RedemptionRequest.List"];

                IChannelProvider<IFinancialContractChannel> channelProvider = ChannelProviderFactory<IFinancialContractChannel>.Create("FinancialClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                List<RedemptionRequestListItem> lstToEffect = new List<RedemptionRequestListItem>();
                List<RedemptionRequestListItem> lstToValidate = new List<RedemptionRequestListItem>();

                int index = 1;
                foreach (string i in arrRequestsIds)
                {
                    if (!string.IsNullOrEmpty(i))
                    {
                        RedemptionRequestListItem obj = (from r in lstObj where r.Id.Equals(i) select r).FirstOrDefault();

                        string assessor = null;
                        if (SessionHelper.CurrentUser.Assessors != null)
                            foreach (int j in SessionHelper.CurrentUser.Assessors)
                                assessor += j.ToString() + ";";

                        var requestedValue = Convert.ToDecimal(obj.Amount);
                        var balance = service.LoadRedemptionRequestBalance(obj.ClientCode.Split('-')[0], assessor, CommonHelper.GetLoggedUserGroupId()).Result;

                        var redemptionIntended = balance.Intended - requestedValue;
                        if (redemptionIntended < 0)
                        {
                            RedemptionRequestBalanceItem b = new RedemptionRequestBalanceItem();
                            b.Available = balance.Available.ToString("N2");
                            b.Intended = string.Format("{0}#{1}", balance.Intended.ToString("N2"), redemptionIntended.ToString("N2"));
                            obj.Balance = b;
                            obj.Index = index++;
                            lstToValidate.Add(obj);
                        }
                        //else if (balance.Total - requestedValue < 0)
                        //lstToValidate.Add(obj);

                        lstToEffect.Add(obj);
                    }
                }

                service.Close();

                Session["RedemptionRequest.ListToEffect"] = lstToEffect;
                ViewBag.RequestToValidate = lstToValidate;

            }
            catch (Exception)
            {
                
                throw;
            }


            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ConfirmRedemptionRequestEffect(string idsToCancel)
        {
            try
            {
                List<RedemptionRequestListItem> lstToEffect = (List<RedemptionRequestListItem>)Session["RedemptionRequest.ListToEffect"];

                List<RedemptionRequestListItem> lstUnvalid = new List<RedemptionRequestListItem>();
                string[] ids = idsToCancel.Split(';');
                if (ids.Length > 0)
                    foreach (string i in ids)
                        if (!string.IsNullOrEmpty(i))
                        {
                            RedemptionRequestListItem temp = (from r in lstToEffect where r.Id.Equals(i) select r).FirstOrDefault();
                            lstUnvalid.Add(temp);
                        }

                
                var validRequest = (from c in lstToEffect
                                    where !(from o in lstUnvalid select o.Id).Contains(c.Id)
                                    select c).ToList();

                if (validRequest.Count > 0)
                {

                    IChannelProvider<IFinancialContractChannel> channelProvider = ChannelProviderFactory<IFinancialContractChannel>.Create("FinancialClassName");
                    var service = channelProvider.GetChannel().CreateChannel();

                    service.Open();

                    foreach (var item in validRequest)
                    {
                        RedemptionRequestItem r = new RedemptionRequestItem();
                        r.Client = new CommonType { Value = item.ClientCode.Split('-')[0] };
                        r.ID = Convert.ToInt32(item.Id);
                        r.Status = new CommonType { Value = "19" };
                        var response = service.ChangeRedemptionRequestStatus(r);

                        if (response.Result)
                        {
                            LogHelper.SaveHistoryLog("Financial.RedemptionRequest", new HistoryData { Action = "Efetuar", ID = r.ID, NewData = JsonConvert.SerializeObject(r) });
                        }
                    }

                    service.Close();
                }
                else
                {
					return Json(new { Error = "0" });
                }

                return Json(new { Error = "" });
            }
            catch (Exception)
            {
                return Json(new { Error = "Erro ao efetuar resgate." });
            }
        }

        [HttpPost]
        public ActionResult RedemptionRequestApprove(string[] requestIds) 
        {
            try
            {
                var lstObj = (List<RedemptionRequestListItem>)Session["RedemptionRequest.List"];

                IChannelProvider<IFinancialContractChannel> channelProvider = ChannelProviderFactory<IFinancialContractChannel>.Create("FinancialClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                List<RedemptionRequestListItem> lstToApprove = new List<RedemptionRequestListItem>();
                List<RedemptionRequestListItem> lstToValidate = new List<RedemptionRequestListItem>();

                int index = 1;
                foreach (string i in requestIds)
                {
                    if (!string.IsNullOrEmpty(i))
                    {
                        RedemptionRequestListItem obj = (from r in lstObj where r.Id.Equals(i) select r).FirstOrDefault();

                        string assessor = null;
                        if (SessionHelper.CurrentUser.Assessors != null)
                            foreach (int j in SessionHelper.CurrentUser.Assessors)
                                assessor += j.ToString() + ";";

                        var requestedValue = Convert.ToDecimal(obj.Amount);
                        var balance = service.LoadRedemptionRequestBalance(obj.ClientCode.Split('-')[0], assessor, CommonHelper.GetLoggedUserGroupId()).Result;

                        var redemptionIntended = balance.Intended - requestedValue;
                        if (redemptionIntended < 0)
                        {
                            RedemptionRequestBalanceItem b = new RedemptionRequestBalanceItem();
                            b.Available = balance.Available.ToString("N2");
                            b.Intended = string.Format("{0}#{1}", balance.Intended.ToString("N2"), redemptionIntended.ToString("N2"));
                            obj.Balance = b;
                            obj.Index = index++;
                            lstToValidate.Add(obj);
                        }
                        //else if (balance.Total - requestedValue < 0)
                        //lstToValidate.Add(obj);

                        lstToApprove.Add(obj);
                    }
                }

                service.Close();

                Session["RedemptionRequest.ListToApprove"] = lstToApprove;
                ViewBag.RequestToValidate = lstToValidate;

            }
            catch (Exception)
            {

                throw;
            }


            return View();
        }

        private int LoadPendingRequests()
        {
            return CommonHelper.GetPendents("Financial.RedemptionRequest");
            /*int requests = 0;

            try
            {
                IChannelProvider<IFinancialContractChannel> channelProvider = ChannelProviderFactory<IFinancialContractChannel>.Create("FinancialClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                requests = service.GetPendentRedemptionRequest().Result;

                service.Close();
            }
            catch (Exception)
            {
                requests = 0;
            }

            return requests;*/
        }
        #endregion

        #region Relatório Financeiro Diário

        [HttpPost]
        public string InitialDailyFinancialFilter(FormCollection form)
        {
            WcfResponse<DailyFinancial[], int> list = new WcfResponse<DailyFinancial[], int>();
            IChannelProvider<IFinancialContractChannel> financialProvider = ChannelProviderFactory<IFinancialContractChannel>.Create("FinancialClassName");

            using (var financialService = financialProvider.GetChannel().CreateChannel())
            {
                financialService.Open();

                /*string assessor = String.Empty;
                if (SessionHelper.CurrentUser.Assessors != null)
                    foreach (int i in SessionHelper.CurrentUser.Assessors)
                        assessor += i.ToString() + ";";*/


                var filter = new DailyFinancialFilter()
                {
                    AssessorFilter = "",//new MarketHelper().GetAssessorsRangesToFilter(""),
                    Situation = 'T'
                };

                var options = new FilterOptions()
                {
                    InitRow = Convert.ToInt32(form["iDisplayStart"]),
                    FinalRow = Convert.ToInt32(form["iDisplayLength"]),
                    SortColumn = (form["iSortCol_0"] == "null" || form["iSortCol_0"] == "undefined" || form["iSortCol_0"] == "0") ? 1 : Convert.ToInt32(form["iSortCol_0"]) + 1,
                    SortDirection = form["sSortDir_0"],
                    AllRecords = false
                };

                list = financialService.LoadDailyFinancialReport(filter, options, CommonHelper.GetLoggedUserGroupId());

                Session["Financial.DailyFinancialReport"] = list.Result;
                Session["Financial.DailyFinancialReportFilter"] = filter;
                Session["Financial.DailyFinancialReportFilterOptions"] = options;

                financialService.Close();
            }
            return new FinancialHelper().DailyFinancialReportListObject((list.Result) != null ? list.Result.ToList() : new List<DailyFinancial>(), ref form, list.Data);
        }
        
        [HttpPost]
        public string DailyFinancialReportFilter(FormCollection form)
        {
            WcfResponse<DailyFinancial[], int> list = new WcfResponse<DailyFinancial[], int>();
            IChannelProvider<IFinancialContractChannel> financialProvider = ChannelProviderFactory<IFinancialContractChannel>.Create("FinancialClassName");

            using (var financialService = financialProvider.GetChannel().CreateChannel())
            {
                financialService.Open();
                            

                var filter = new DailyFinancialFilter()
                {
                    Situation = form["ddlStatus"].ToCharArray()[0],
                    D0 = (form["chkD0"] == "S")
                };

                if (!string.IsNullOrEmpty(form["txtClientFilter"]))
                    filter.ClientFilter = new MarketHelper().GetClientsRangesToFilter(form["txtClientFilter"]);
                
                filter.AssessorFilter = String.IsNullOrEmpty(form["txtAssessorFilter"]) ? "" : new MarketHelper().GetAssessorsRangesToFilter(form["txtAssessorFilter"]);

                if (!String.IsNullOrEmpty(form["txtMovementDate"]))
                    filter.MovementDate = Convert.ToDateTime(form["txtMovementDate"]);

                if (form["ddlCustody"] != "T")
                    filter.CustodyTax = form["ddlCustody"].Trim().ToUpper().Equals("S");

                var options = new FilterOptions()
                {
                    InitRow = Convert.ToInt32(form["iDisplayStart"]),
                    FinalRow = Convert.ToInt32(form["iDisplayLength"]),
                    SortColumn = (form["iSortCol_0"] == "null" || form["iSortCol_0"] == "undefined" || form["iSortCol_0"] == "0") ? 1 : Convert.ToInt32(form["iSortCol_0"]) + 1,
                    SortDirection = form["sSortDir_0"],
                    AllRecords = false
                };

                list = financialService.LoadDailyFinancialReport(filter, options, CommonHelper.GetLoggedUserGroupId());

                Session["Financial.DailyFinancialReport"] = list.Result;
                Session["Financial.DailyFinancialReportFilter"] = filter;
                Session["Financial.DailyFinancialReportFilterOptions"] = options;

                financialService.Close();
            }
            return new FinancialHelper().DailyFinancialReportListObject((list.Result) != null ? list.Result.ToList() : new List<DailyFinancial> (), ref form, list.Data);
        }

        [AccessControlAuthorize]
        public ActionResult DailyFinancialReport()
        {
            return View();
        }

		public ActionResult DailyFinancialReportToFile(bool isPdf)
		{
			List<DailyFinancial> currentList = new List<DailyFinancial>();
			try
			{
				if (Session["Financial.DailyFinancialReportFilter"] != null && Session["Financial.DailyFinancialReportFilterOptions"] != null)
				{
					DailyFinancialFilter filter = (DailyFinancialFilter)Session["Financial.DailyFinancialReportFilter"];
					FilterOptions options = (FilterOptions)Session["Financial.DailyFinancialReportFilterOptions"];
					options.AllRecords = true;

					IChannelProvider<IFinancialContractChannel> financialProvider = ChannelProviderFactory<IFinancialContractChannel>.Create("FinancialClassName");
					IFinancialContractChannel financialService = financialProvider.GetChannel().CreateChannel();
					financialService.Open();
					currentList = financialService.LoadDailyFinancialReport(filter, options, CommonHelper.GetLoggedUserGroupId()).Result.ToList<DailyFinancial>();
					financialService.Close();
				}

				LogHelper.SaveAuditingLog("Financial.DailyFinancialReportToFile"
                    , string.Format(LogResources.ObjectToFileParameters, isPdf ? "PDF" : "Excel", JsonConvert.SerializeObject(CommonHelper.PrepareObjectToSerialize<DailyFinancial>(currentList))));

				string fileName = string.Format("relatorioFinanceiro_{0}.{1}", DateTime.Now.ToString("dd-MM-yyyy"), isPdf ? "pdf" : "xls");

				LogHelper.SaveHistoryLog("Financial.DailyFinancialReport", new HistoryData { Action = "Exportar", OldData = string.Empty, NewData = JsonConvert.SerializeObject(currentList) });

				return isPdf ? ViewDailyFinancialReportPdf(currentList, true, fileName) : ViewDailyFinancialReportExcel(currentList, true, fileName);
			}
			catch (Exception ex)
			{
				LogManager.WriteError(ex);
				TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
				return this.RedirectToAction("DailyFinancialReport");
			}
		}

        #endregion
    }
}
