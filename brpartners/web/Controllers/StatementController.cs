﻿using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using iTextSharp.tool.xml.css;
using iTextSharp.tool.xml.html;
using iTextSharp.tool.xml.parser;
using iTextSharp.tool.xml.pipeline.css;
using iTextSharp.tool.xml.pipeline.end;
using iTextSharp.tool.xml.pipeline.html;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.WebSite.Helper;
using QX3.Portal.WebSite.Models;
using QX3.Portal.WebSite.Models.Channel;
using QX3.Portal.WebSite.Models.Statement;
using QX3.Portal.WebSite.SuitabilityService;
using QX3.Spinnex.Common.Services.Logging;
using QX3.Spinnex.Common.Services.Mail;
using Rotativa;
using Rotativa.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace QX3.Portal.WebSite.Controllers
{
    public class StatementController : BaseController
    {
        #region Private Methods

        private void GetMismatch(List<SuitabilityPerfilCliente> obj)
        {
            if (obj != null && obj.Count > 0)
            {
                obj.ForEach(f =>
                {
                    if (f.CnpjCpf != null)
                    {
                        f.CnpjCpf = f.CnpjCpf.Length <= 11 ? FormatCPF(f.CnpjCpf) : FormatCNPJ(f.CnpjCpf);
                    }

                    var unframed = VerifyMismatch(f.IdCliente, null, null);
                    if (unframed != null && unframed.Length > 0)
                    {
                        foreach (var item in unframed)
                        {
                            string d = Convert.ToDateTime(f.DataCadastro).ToString("dd/MM/yyyy");

                            if (item.CompletionDate.ToString("dd/MM/yyyy") == d)
                            {
                                List<string> list = new List<string>();

                                foreach (var u in item.Unframeds)
                                    list.Add(string.Format("{0} - {1}", u.CdMercado, u.DtDatOrd.ToString("dd/MM/yyyy")));

                                f.Desenquadramento = String.Join(", ", list.ToArray());
                            }
                        }
                    }

                });
            }
        }

        private SuitabilityUnframedResponse[] VerifyMismatch(int clientId, DateTime? start, DateTime? end)
        {
            SuitabilityUnframedResponse[] unframed = null;

            IChannelProvider<ISuitabilityContractChannel> channelProvider = ChannelProviderFactory<ISuitabilityContractChannel>.Create("SuitabilityClassName");
            var service = channelProvider.GetChannel().CreateChannel();

            service.Open();

            var response = service.VerificaNovoDesenquadramento(clientId, start, end);

            unframed = response.Result;

            service.Close();

            return unframed;
        }

        private class CustomImageTagProcessor : iTextSharp.tool.xml.html.Image
        {
            public override IList<IElement> End(IWorkerContext ctx, Tag tag, IList<IElement> currentContent)
            {
                IDictionary<string, string> attributes = tag.Attributes;
                string src;
                if (!attributes.TryGetValue(HTML.Attribute.SRC, out src))
                    return new List<IElement>(1);

                if (string.IsNullOrEmpty(src))
                    return new List<IElement>(1);

                if (src.StartsWith("data:image/", StringComparison.InvariantCultureIgnoreCase))
                {
                    // data:[<MIME-type>][;charset=<encoding>][;base64],<data>
                    var base64Data = src.Substring(src.IndexOf(",") + 1);
                    var imagedata = Convert.FromBase64String(base64Data);
                    var image = iTextSharp.text.Image.GetInstance(imagedata);

                    var list = new List<IElement>();
                    var htmlPipelineContext = GetHtmlPipelineContext(ctx);
                    list.Add(GetCssAppliers().Apply(new Chunk((iTextSharp.text.Image)GetCssAppliers().Apply(image, tag, htmlPipelineContext), 0, 0, true), tag, htmlPipelineContext));
                    return list;
                }
                else
                {
                    return base.End(ctx, tag, currentContent);
                }
            }
        }

        private static byte[] ConvertHtmltoPdf(string result, string nameOfTheFile)
        {
            using (var msOutput = new MemoryStream())
            {
                using (var stringReader = new StringReader(result))
                {
                    using (iTextSharp.text.Document document = new iTextSharp.text.Document())
                    {
                        var pdfWriter = PdfWriter.GetInstance(document, msOutput);
                        pdfWriter.InitialLeading = 12.5f;
                        document.Open();
                        var xmlWorkerHelper = XMLWorkerHelper.GetInstance();
                        var cssResolver = new StyleAttrCSSResolver();
                        var xmlWorkerFontProvider = new XMLWorkerFontProvider();
                        //foreach (string font in fonts)
                        //{
                        //    xmlWorkerFontProvider.Register(font);
                        //}

                        var tagProcessors = (DefaultTagProcessorFactory)Tags.GetHtmlTagProcessorFactory();
                        tagProcessors.RemoveProcessor(HTML.Tag.IMG); // remove the default processor
                        tagProcessors.AddProcessor(HTML.Tag.IMG, new CustomImageTagProcessor()); // use our new processor

                        var cssAppliers = new CssAppliersImpl(xmlWorkerFontProvider);

                        var htmlContext = new HtmlPipelineContext(cssAppliers);
                        htmlContext.SetTagFactory(Tags.GetHtmlTagProcessorFactory());

                        var hpc = new HtmlPipelineContext(new CssAppliersImpl(new XMLWorkerFontProvider()));
                        hpc.SetAcceptUnknown(true).AutoBookmark(true).SetTagFactory(tagProcessors); // inject the tagProcessors

                        PdfWriterPipeline pdfWriterPipeline = new PdfWriterPipeline(document, pdfWriter);
                        // HtmlPipeline htmlPipeline = new HtmlPipeline(htmlContext, pdfWriterPipeline);

                        var htmlPipeline = new HtmlPipeline(hpc, new PdfWriterPipeline(document, pdfWriter));

                        CssResolverPipeline cssResolverPipeline = new CssResolverPipeline(cssResolver, htmlPipeline);
                        XMLWorker xmlWorker = new XMLWorker(cssResolverPipeline, true);
                        XMLParser xmlParser = new XMLParser(xmlWorker);
                        xmlParser.Parse(stringReader);
                    }
                }
                return msOutput.ToArray();
            }
        }

        private static void DownloadPdf(HttpResponseBase Response, byte[] pdfByte)
        {
            Response.Clear();
            MemoryStream ms = new MemoryStream(pdfByte);
            ms.WriteTo(Response.OutputStream);
            Response.End();
        }

        private void AddResponseHeader(HttpResponseBase Response, string fileName)
        {
            Response.ContentType = "application/pdf";
            Response.ContentEncoding = Encoding.UTF8;
            Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
            Response.Buffer = true;
        }

        private static string FormatCNPJ(string CNPJ)
        {
            return Convert.ToUInt64(CNPJ).ToString(@"00\.000\.000\/0000\-00");
        }

        private static string FormatCPF(string CPF)
        {
            return Convert.ToUInt64(CPF).ToString(@"000\.000\.000\-00");
        }

        private long? FormatCpfCnpj(string obj, bool ifNullReturnZero = false)
        {
            long? cpfCnpj = null;
            if (!string.IsNullOrWhiteSpace(obj))
            {
                obj = Regex.Replace(obj, @"[^\d]", "");
                try
                {
                    cpfCnpj = Int64.Parse(obj);
                }
                catch { }
            }

            if (!cpfCnpj.HasValue && ifNullReturnZero)
                cpfCnpj = 0;

            return cpfCnpj;
        }

        private long FormatCpfCnpj(string obj)
        {
            return FormatCpfCnpj(obj, true).Value;
        }

        private string RenderRazorViewToString(string viewName, object model)
        {
            ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);

                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);

                return sw.GetStringBuilder().ToString();
            }
        }

        private List<StatementClient> GetClients(StatementClientFilter filter, out string message)
        {
            List<StatementClient> lst = new List<StatementClient>();
            message = string.Empty;

            IChannelProvider<ISuitabilityContractChannel> channelProvider = ChannelProviderFactory<ISuitabilityContractChannel>.Create("SuitabilityClassName");
            var service = channelProvider.GetChannel().CreateChannel();

            service.Open();

            var response = service.GetClients(filter);

            service.Close();

            if (response.Result != null && response.Result.Length > 0)
                lst = response.Result.ToList();

            if (string.IsNullOrEmpty(response.Message))
                message = response.Message;

            return lst;
        }

        private List<FixedIncomeDetails> GetFixedIncome(FixedIncomeFilter filter, out string message)
        {
            List<FixedIncomeDetails> lst = new List<FixedIncomeDetails>();
            message = string.Empty;

            IChannelProvider<ISuitabilityContractChannel> channelProvider = ChannelProviderFactory<ISuitabilityContractChannel>.Create("SuitabilityClassName");
            var service = channelProvider.GetChannel().CreateChannel();

            service.Open();

            var response = service.GetFixedIncome(filter);

            service.Close();

            if (response.Result != null && response.Result.Length > 0)
                lst = response.Result.ToList();

            if (string.IsNullOrEmpty(response.Message))
                message = response.Message;

            return lst;
        }

        private FixedIncomeDTO GetFixedIncome(FixedIncomeFilter filter)
        {
            FixedIncomeDTO model = new FixedIncomeDTO();

            List<FixedIncomeDetails> lst = new List<FixedIncomeDetails>();

            string message = string.Empty;
            lst = GetFixedIncome(filter, out message);

            StatementClientFilter filterClient = new StatementClientFilter();

            filterClient.CpfCnpj = filter.CpfCnpj;

            List<StatementClient> clients = new List<StatementClient>();

            string message2 = string.Empty;
            clients = GetClients(filterClient, out message2);

            model.CpfCnpj = filterClient.CpfCnpj.Value.ToString();
            model.Name = clients != null && clients.Count > 0 ? clients[0].Name : "";
            model.Start = filter.Start.Value;
            model.End = filter.End.Value;
            model.Movement = lst.Where(x => x.tp_registro == "O").ToList();
            model.OpenPosition = lst.Where(x => x.tp_registro == "P").ToList();

            return model;
        }

        private List<NdfDetails> GetNdf(NdfFilter filter, out string message)
        {
            List<NdfDetails> lst = new List<NdfDetails>();
            message = string.Empty;

            IChannelProvider<ISuitabilityContractChannel> channelProvider = ChannelProviderFactory<ISuitabilityContractChannel>.Create("SuitabilityClassName");
            var service = channelProvider.GetChannel().CreateChannel();

            service.Open();

            var response = service.GetNdf(filter);

            service.Close();

            if (response.Result != null && response.Result.Length > 0)
                lst = response.Result.ToList();

            if (string.IsNullOrEmpty(response.Message))
                message = response.Message;

            return lst;
        }

        private NdfDTO GetNdf(NdfFilter filter)
        {
            NdfDTO model = new NdfDTO();

            List<NdfDetails> lst = new List<NdfDetails>();

            string message = string.Empty;
            lst = GetNdf(filter, out message);

            StatementClientFilter filterClient = new StatementClientFilter();

            filterClient.CpfCnpj = filter.CpfCnpj;

            List<StatementClient> clients = new List<StatementClient>();

            string message2 = string.Empty;
            clients = GetClients(filterClient, out message2);

            model.CpfCnpj = filterClient.CpfCnpj.Value.ToString();
            model.Name = clients != null && clients.Count > 0 ? clients[0].Name : "";
            model.Month = filter.Month;
            model.Year = filter.Year;
            model.Type = filter.Type == NdfSwapType.CurvaContabil ? "0" : "1"; // "Curva Contábil" : "Curva Mercado";
            model.Movement = lst.Where(x => string.IsNullOrEmpty(x.dc_criterio_termo)).ToList();
            model.OpenPosition = lst.Where(x => !string.IsNullOrEmpty(x.dc_criterio_termo)).ToList();

            return model;
        }

        private List<SwapDetails> GetSwap(SwapFilter filter, out string message)
        {
            List<SwapDetails> lst = new List<SwapDetails>();
            message = string.Empty;

            IChannelProvider<ISuitabilityContractChannel> channelProvider = ChannelProviderFactory<ISuitabilityContractChannel>.Create("SuitabilityClassName");
            var service = channelProvider.GetChannel().CreateChannel();

            service.Open();

            var response = service.GetSwap(filter);

            service.Close();

            if (response.Result != null && response.Result.Length > 0)
                lst = response.Result.ToList();

            if (string.IsNullOrEmpty(response.Message))
                message = response.Message;

            return lst;
        }

        private SwapDTO GetSwap(SwapFilter filter)
        {
            SwapDTO model = new SwapDTO();

            List<SwapDetails> lst = new List<SwapDetails>();

            string message = string.Empty;
            lst = GetSwap(filter, out message);

            StatementClientFilter filterClient = new StatementClientFilter();

            filterClient.CpfCnpj = filter.CpfCnpj;

            List<StatementClient> clients = new List<StatementClient>();

            string message2 = string.Empty;
            clients = GetClients(filterClient, out message2);

            model.CpfCnpj = filterClient.CpfCnpj.Value.ToString();
            model.Name = clients != null && clients.Count > 0 ? clients[0].Name : "";
            model.Month = filter.Month;
            model.Year = filter.Year;
            model.Type = filter.Type == NdfSwapType.CurvaContabil ? "0" : "1"; //  "Curva Contábil" : "Curva Mercado";
            model.Movement = lst.Where(x => string.IsNullOrEmpty(x.dc_index_dado)).ToList();
            model.OpenPosition = lst.Where(x => !string.IsNullOrEmpty(x.dc_index_dado)).ToList();

            return model;
        }

        private List<StatementLog> GetLog(StatementLog filter, out string message)
        {
            List<StatementLog> lst = new List<StatementLog>();
            message = string.Empty;

            IChannelProvider<ISuitabilityContractChannel> channelProvider = ChannelProviderFactory<ISuitabilityContractChannel>.Create("SuitabilityClassName");
            var service = channelProvider.GetChannel().CreateChannel();

            service.Open();

            var response = service.GetStatementLog(filter);

            service.Close();

            if (response.Result != null && response.Result.Length > 0)
                lst = response.Result.ToList();

            if (string.IsNullOrEmpty(response.Message))
                message = response.Message;

            return lst;
        }

        private List<SelectListItem> GetMonthsList()
        {
            List<SelectListItem> list = new List<SelectListItem>();

            list.Add(new SelectListItem { Text = "Todos", Value = "", Selected = true });
            list.Add(new SelectListItem { Text = "Janeiro", Value = "1", Selected = false });
            list.Add(new SelectListItem { Text = "Fevereiro", Value = "2", Selected = false });
            list.Add(new SelectListItem { Text = "Março", Value = "3", Selected = false });
            list.Add(new SelectListItem { Text = "Abril", Value = "4", Selected = false });
            list.Add(new SelectListItem { Text = "Maio", Value = "5", Selected = false });
            list.Add(new SelectListItem { Text = "Junho", Value = "6", Selected = false });
            list.Add(new SelectListItem { Text = "Julho", Value = "7", Selected = false });
            list.Add(new SelectListItem { Text = "Agosto", Value = "8", Selected = false });
            list.Add(new SelectListItem { Text = "Setembro", Value = "9", Selected = false });
            list.Add(new SelectListItem { Text = "Outubro", Value = "10", Selected = false });
            list.Add(new SelectListItem { Text = "Novembro", Value = "11", Selected = false });
            list.Add(new SelectListItem { Text = "Dezembro", Value = "12", Selected = false });

            return list;
        }

        private List<SelectListItem> GetYearsList()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem { Text = "Todos", Value = "", Selected = true });

            int currentYear = DateTime.Now.Year;

            for (int i = 0; i < 5; i++)
            {
                int year = currentYear - i;
                list.Add(new SelectListItem { Text = year.ToString(), Value = year.ToString(), Selected = false });
            }

            return list;
        }

        private List<SelectListItem> GetStatementsList(string selectedValue = "")
        {
            List<SelectListItem> list = new List<SelectListItem>();

            list.Add(new SelectListItem { Text = "Todos", Value = "", Selected = selectedValue == "" });

            List<KeyValuePair<string, int>> types = StatementHelper.GetStatmentsNameValue();

            foreach (var type in types)
            {
                list.Add(new SelectListItem { Text = type.Key, Value = type.Value.ToString(), Selected = false });
            }

            return list;
        }

        private List<SelectListItem> GetStatementsCheckboxes(List<int> selectedValues)
        {
            List<SelectListItem> list = new List<SelectListItem>();

            bool select = selectedValues != null && selectedValues.Count > 0;

            List<KeyValuePair<string, int>> types = StatementHelper.GetStatmentsNameValue();

            foreach (var type in types)
            {
                list.Add(new SelectListItem { Text = type.Key, Value = type.Value.ToString(), Selected = select && selectedValues.Contains(type.Value) });
            }

            return list;
        }

        //private CheckBoxList GetStatementsCheckboxes(string selectedValue = "")
        //{
        //    CheckBoxList list = new CheckBoxList();

        //    List<KeyValuePair<string, int>> types = StatementHelper.GetStatmentsNameValue();

        //    foreach (var type in types)
        //    {
        //        list.Items.Add(new System.Web.UI.WebControls.ListItem
        //        {
        //            Text = type.Key,
        //            Value = type.Value.ToString(),
        //            Selected = selectedValue == type.Value.ToString()
        //        });
        //    }

        //    return list;
        //}

        private List<SelectListItem> GetSendingList(string selectedValue = "")
        {
            List<SelectListItem> list = new List<SelectListItem>();

            list.Add(new SelectListItem { Text = "Todos", Value = "", Selected = selectedValue == "" });
            list.Add(new SelectListItem { Text = "Não configurado", Value = "-1", Selected = selectedValue == "-1" });
            list.Add(new SelectListItem { Text = "Sim", Value = "1", Selected = selectedValue == "1" });
            list.Add(new SelectListItem { Text = "Não", Value = "0", Selected = selectedValue == "0" });

            return list;
        }

        private void InsertStatementLog(StatementLog obj)
        {
            IChannelProvider<ISuitabilityContractChannel> channelProvider = ChannelProviderFactory<ISuitabilityContractChannel>.Create("SuitabilityClassName");
            var service = channelProvider.GetChannel().CreateChannel();

            service.Open();

            var response = service.InsertStatementLog(obj);

            service.Close();
        }

        #endregion

        #region Clients

        [AccessControlAuthorize]
        [HttpGet]
        public ActionResult Clients()
        {
            ViewBag.SendingList = GetSendingList();

            return View();
        }

        [AccessControlAuthorize]
        [HttpPost]
        public ActionResult Clients(FormCollection form)
        {
            List<StatementClient> lst = new List<StatementClient>();

            ViewBag.SendingList = GetSendingList(form["automaticSending"]);

            try
            {
                LogHelper.SaveAuditingLog("Statement.Clients");

                string orderBy = ViewBag.OrderBy = form["hdnOrderBy"];
                string clientCode = ViewBag.ClientCode = form["clientCode"];
                string cpfCnpj = ViewBag.CpfCnpj = form["cpfCnpj"];
                string name = ViewBag.Name = form["name"];
                string automaticSending = form["automaticSending"];

                StatementClientFilter filter = new StatementClientFilter();

                if (!string.IsNullOrEmpty(clientCode))
                    filter.ClientCode = Convert.ToInt32(clientCode);

                filter.CpfCnpj = FormatCpfCnpj(cpfCnpj);
                filter.Name = name;

                if (!string.IsNullOrEmpty(automaticSending))
                {
                    int i = Convert.ToInt32(automaticSending);
                    filter.AutomaticSending = i;
                }

                string message = string.Empty;
                lst = GetClients(filter, out message);

                if (!string.IsNullOrEmpty(orderBy))
                {
                    switch (orderBy)
                    {
                        case "code":
                            lst = lst.OrderBy(x => x.ClientCode).ToList();
                            break;
                        case "name":
                            lst = lst.OrderBy(x => x.Name).ToList();
                            break;
                        default:
                            break;
                    }
                }

                if (!string.IsNullOrEmpty(message))
                    throw new Exception(message);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw ex;
            }

            return View(lst);
        }

        #endregion

        #region Renda Fixa

        [HttpGet]
        public ActionResult FixedIncomeSearch(string cpfCnpj)
        {
            ViewBag.CpfCnpj = cpfCnpj;

            return View();
        }

        [AccessControlAuthorize]
        [HttpPost]
        public ActionResult FixedIncome(FormCollection form) //(string cpfCnpj, DateTime start, DateTime end)
        {
            var dto = new FixedIncomeDTO();

            try
            {
                LogHelper.SaveAuditingLog("Statement.FixedIncome");

                string cpfCnpj = form["cpfCnpj"];
                DateTime start = Convert.ToDateTime(form["start"]);
                DateTime end = Convert.ToDateTime(form["end"]);

                FixedIncomeFilter filter = new FixedIncomeFilter();

                filter.CpfCnpj = FormatCpfCnpj(cpfCnpj); // 45631743353
                filter.Start = start; // Convert.ToDateTime("01/01/2017"); // start; // string.IsNullOrEmpty(start) ? DateTime.Now.Date : Convert.ToDateTime(start);
                filter.End = end; // Convert.ToDateTime("30/06/2020"); // end; // string.IsNullOrEmpty(end) ? DateTime.Now.Date : Convert.ToDateTime(end);

                string message = string.Empty;
                dto = GetFixedIncome(filter);

                if (!string.IsNullOrEmpty(message))
                    throw new Exception(message);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw ex;
            }

            return View(dto);
        }

        public void ExportFixedIncomeToPdf(string cpfCnpj, string start, string end)
        {
            LogHelper.SaveAuditingLog("Statement.FixedIncome");

            //string cpfCnpj = cpfCnpj; // form["cpfCnpj"];
            //string start = start;
            //string end = form["end"];

            FixedIncomeFilter filter = new FixedIncomeFilter();

            filter.CpfCnpj = FormatCpfCnpj(cpfCnpj); // 45631743353
            filter.Start = Convert.ToDateTime(start); // Convert.ToDateTime("01/01/2017"); // start; // string.IsNullOrEmpty(start) ? DateTime.Now.Date : Convert.ToDateTime(start);
            filter.End = Convert.ToDateTime(end); // Convert.ToDateTime("30/06/2020"); // end; // string.IsNullOrEmpty(end) ? DateTime.Now.Date : Convert.ToDateTime(end);

            string message = string.Empty;
            var fixedIncome = GetFixedIncome(filter);

            if (!string.IsNullOrEmpty(message))
                throw new Exception(message);

            string fileName = string.Format("RendaFixa_{0}_{1}.{2}", filter.CpfCnpj.ToString(), DateTime.Now.ToString("ddMMyyyymmss"), "pdf");

            var table = RenderRazorViewToString("FixedIncomePrint", fixedIncome);
            AddResponseHeader(Response, fileName);

            var pdfBytes = ConvertHtmltoPdf(table, fileName);

            DownloadPdf(Response, pdfBytes);
        }

        public ActionResult FixedIncomePrint(string cpfCnpj)
        {
            LogHelper.SaveAuditingLog("Statement.FixedIncome");

            //string cpfCnpj = cpfCnpj; // form["cpfCnpj"];
            //string start = start;
            //string end = form["end"];

            FixedIncomeFilter filter = new FixedIncomeFilter();

            filter.CpfCnpj = FormatCpfCnpj(cpfCnpj); // 45631743353
            filter.Start = Convert.ToDateTime("01/01/2017"); // start; // string.IsNullOrEmpty(start) ? DateTime.Now.Date : Convert.ToDateTime(start);
            filter.End = Convert.ToDateTime("30/06/2020"); // end; // string.IsNullOrEmpty(end) ? DateTime.Now.Date : Convert.ToDateTime(end);

            string message = string.Empty;
            var fixedIncome = GetFixedIncome(filter);

            if (!string.IsNullOrEmpty(message))
                throw new Exception(message);

            return View(fixedIncome);
        }

        #endregion

        #region NDF

        [HttpGet]
        public ActionResult NdfSearch(string cpfCnpj)
        {
            ViewBag.CpfCnpj = cpfCnpj;

            return View();
        }

        [AccessControlAuthorize]
        [HttpPost]
        public ActionResult NDF(FormCollection form)
        {
            var dto = new NdfDTO();
            NdfFilter filter = new NdfFilter();

            try
            {
                LogHelper.SaveAuditingLog("Statement.NDF");

                string cpfCnpj = form["cpfCnpj"];
                int month = Convert.ToInt32(form["month"]);
                int year = Convert.ToInt32(form["year"]);
                int type = Convert.ToInt32(form["type"]);

                filter.CpfCnpj = FormatCpfCnpj(cpfCnpj);
                filter.Month = month;
                filter.Year = year;
                filter.Type = type == 0 ? NdfSwapType.CurvaContabil : NdfSwapType.CurvaMercado;

                string message = string.Empty;
                dto = GetNdf(filter);

                if (!string.IsNullOrEmpty(message))
                    throw new Exception(message);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw ex;
            }

            if (filter.Type == NdfSwapType.CurvaContabil)
                return View("NDF", dto);
            else
                return View("NDF2", dto);
        }

        public void ExportNDFToPdf(string cpfCnpj, string month, string year, string type)
        {
            LogHelper.SaveAuditingLog("Statement.NDF");

            var dto = new NdfDTO();
            NdfFilter filter = new NdfFilter();

            NdfSwapType ndfType = Convert.ToInt32(type) == 0 ? NdfSwapType.CurvaContabil : NdfSwapType.CurvaMercado;

            filter.CpfCnpj = FormatCpfCnpj(cpfCnpj);
            filter.Month = Convert.ToInt32(month);
            filter.Year = Convert.ToInt32(year);
            filter.Type = ndfType;

            string message = string.Empty;
            dto = GetNdf(filter);

            if (!string.IsNullOrEmpty(message))
                throw new Exception(message);

            string fileName = string.Format("NDF_{0}_{1}.{2}", filter.CpfCnpj.ToString(), DateTime.Now.ToString("ddMMyyyymmss"), "pdf");

            string view = ndfType == NdfSwapType.CurvaContabil ? "NDFPrint" : "NDFPrint2";

            var table = RenderRazorViewToString(view, dto);
            AddResponseHeader(Response, fileName);

            var pdfBytes = ConvertHtmltoPdf(table, fileName);

            DownloadPdf(Response, pdfBytes);
        }

        public ActionResult NDFPrint(string cpfCnpj, string month, string year, string type)
        {
            LogHelper.SaveAuditingLog("Statement.NDF");

            var dto = new NdfDTO();
            NdfFilter filter = new NdfFilter();

            NdfSwapType ndfType = Convert.ToInt32(type) == 0 ? NdfSwapType.CurvaContabil : NdfSwapType.CurvaMercado;

            filter.CpfCnpj = FormatCpfCnpj(cpfCnpj);
            filter.Month = Convert.ToInt32(month);
            filter.Year = Convert.ToInt32(year);
            filter.Type = ndfType;

            string message = string.Empty;
            dto = GetNdf(filter);

            if (!string.IsNullOrEmpty(message))
                throw new Exception(message);

            string view = ndfType == NdfSwapType.CurvaContabil ? "NDFPrint" : "NDFPrint2";

            return View(view, dto);
        }

        #endregion

        #region SWAP

        [HttpGet]
        public ActionResult SwapSearch(string cpfCnpj)
        {
            ViewBag.CpfCnpj = cpfCnpj;

            return View();
        }

        [AccessControlAuthorize]
        [HttpPost]
        public ActionResult Swap(FormCollection form)
        {
            var dto = new SwapDTO();
            SwapFilter filter = new SwapFilter();

            try
            {
                LogHelper.SaveAuditingLog("Statement.Swap");

                string cpfCnpj = form["cpfCnpj"];
                int month = Convert.ToInt32(form["month"]);
                int year = Convert.ToInt32(form["year"]);
                int type = Convert.ToInt32(form["type"]);

                //cpfCnpj = "18520884000170";
                //month = 1;
                //year = 2017;

                filter.CpfCnpj = FormatCpfCnpj(cpfCnpj);
                filter.Month = month;
                filter.Year = year;
                filter.Type = type == 0 ? NdfSwapType.CurvaContabil : NdfSwapType.CurvaMercado;

                string message = string.Empty;
                dto = GetSwap(filter);

                if (!string.IsNullOrEmpty(message))
                    throw new Exception(message);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw ex;
            }

            if (filter.Type == NdfSwapType.CurvaContabil)
                return View("Swap", dto);
            else
                return View("Swap2", dto);
        }

        public void ExportSwapToPdf(string cpfCnpj, string month, string year, string type)
        {
            LogHelper.SaveAuditingLog("Statement.Swap");

            var dto = new SwapDTO();
            SwapFilter filter = new SwapFilter();

            NdfSwapType ndfType = Convert.ToInt32(type) == 0 ? NdfSwapType.CurvaContabil : NdfSwapType.CurvaMercado;

            //cpfCnpj = "18520884000170";
            //month = "1";
            //year = "2017";

            filter.CpfCnpj = FormatCpfCnpj(cpfCnpj);
            filter.Month = Convert.ToInt32(month);
            filter.Year = Convert.ToInt32(year);
            filter.Type = ndfType;

            string message = string.Empty;
            dto = GetSwap(filter);

            if (!string.IsNullOrEmpty(message))
                throw new Exception(message);

            string fileName = string.Format("Swap_{0}_{1}.{2}", filter.CpfCnpj.ToString(), DateTime.Now.ToString("ddMMyyyymmss"), "pdf");

            string view = ndfType == NdfSwapType.CurvaContabil ? "SwapPrint" : "SwapPrint2";

            var table = RenderRazorViewToString(view, dto);
            AddResponseHeader(Response, fileName);

            var pdfBytes = ConvertHtmltoPdf(table, fileName);

            DownloadPdf(Response, pdfBytes);
        }

        public ActionResult SwapPrint(string cpfCnpj, string month, string year, string type)
        {
            LogHelper.SaveAuditingLog("Statement.Swap");

            var dto = new SwapDTO();
            SwapFilter filter = new SwapFilter();

            NdfSwapType ndfType = Convert.ToInt32(type) == 0 ? NdfSwapType.CurvaContabil : NdfSwapType.CurvaMercado;

            //cpfCnpj = "18520884000170";
            //month = "1";
            //year = "2017";

            filter.CpfCnpj = FormatCpfCnpj(cpfCnpj);
            filter.Month = Convert.ToInt32(month);
            filter.Year = Convert.ToInt32(year);
            filter.Type = ndfType;

            string message = string.Empty;
            dto = GetSwap(filter);

            if (!string.IsNullOrEmpty(message))
                throw new Exception(message);

            string view = ndfType == NdfSwapType.CurvaContabil ? "SwapPrint" : "SwapPrint2";

            return View(view, dto);
        }

        #endregion

        #region Configuration

        [HttpGet]
        public ActionResult Configuration(string clientId)
        {
            ViewBag.ClientId = clientId;
            
            IChannelProvider<ISuitabilityContractChannel> channelProvider = ChannelProviderFactory<ISuitabilityContractChannel>.Create("SuitabilityClassName");
            var service = channelProvider.GetChannel().CreateChannel();

            service.Open();

            var response = service.GetStatementConfiguration(Convert.ToInt32(clientId));

            service.Close();

            ViewBag.StatementsList = GetStatementsCheckboxes(response.Result.StatementIDs);

            return View(response.Result);
        }

        [HttpPost]
        public ActionResult Configuration(FormCollection form)
        {
            string clientId = form["clientId"];
            bool automatic = Convert.ToBoolean(form["automatic"]);
            string statements = form["statements"];

            StatementConfiguration sc = new StatementConfiguration();
            sc.AutomaticSending = automatic;
            sc.ClientId = Convert.ToInt32(clientId);

            List<int> lst = new List<int>();

            if (!string.IsNullOrEmpty(statements))
            {
                string[] arr = statements.Split('-');
                foreach (var item in arr)
                    lst.Add(Convert.ToInt32(item));
            }

            sc.StatementIDs = lst;

            IChannelProvider<ISuitabilityContractChannel> channelProvider = ChannelProviderFactory<ISuitabilityContractChannel>.Create("SuitabilityClassName");
            var service = channelProvider.GetChannel().CreateChannel();

            service.Open();

            var response = service.InsertStatementConfiguration(sc);

            service.Close();

            ViewBag.StatementsList = GetStatementsCheckboxes(lst);

            return View(sc);
        }

        #endregion

        #region Log

        [AccessControlAuthorize]
        [HttpGet]
        public ActionResult Log()
        {
            ViewBag.YearsList = GetYearsList();
            ViewBag.MonthsList = GetMonthsList();
            ViewBag.StatementsList = GetStatementsList();

            return View();
        }

        [AccessControlAuthorize]
        [HttpPost]
        public ActionResult Log(FormCollection form)
        {
            List<StatementLog> lst = new List<StatementLog>();

            ViewBag.YearsList = GetYearsList();
            ViewBag.MonthsList = GetMonthsList();
            ViewBag.StatementsList = GetStatementsList();

            try
            {
                LogHelper.SaveAuditingLog("Statement.Log");

                string orderBy = ViewBag.OrderBy = form["hdnOrderBy"];
                string clientCode = ViewBag.ClientCode = form["clientCode"];
                string cpfCnpj = ViewBag.CpfCnpj = form["cpfCnpj"];
                string name = ViewBag.Name = form["name"];
                string year = ViewBag.Year = form["year"];
                string month = ViewBag.Month = form["month"];
                string statement = ViewBag.Statement = form["statement"];

                StatementLog filter = new StatementLog();

                if (!string.IsNullOrEmpty(clientCode))
                    filter.ClientCode = Convert.ToInt32(clientCode);

                filter.CpfCnpj = FormatCpfCnpj(cpfCnpj);
                filter.Name = name;

                if (!string.IsNullOrEmpty(year))
                    filter.Year = Convert.ToInt32(year);

                if (!string.IsNullOrEmpty(month))
                    filter.Month = Convert.ToInt32(month);

                if (!string.IsNullOrEmpty(statement))
                {
                    int i = Convert.ToInt32(statement);
                    filter.StatementIDs = new List<int>();
                    filter.StatementIDs.Add(i);
                }

                string message = string.Empty;
                lst = GetLog(filter, out message);

                if (!string.IsNullOrEmpty(orderBy))
                {
                    switch (orderBy)
                    {
                        case "code":
                            lst = lst.OrderBy(x => x.ClientCode).ToList();
                            break;
                        case "name":
                            lst = lst.OrderBy(x => x.Name).ToList();
                            break;
                        case "year":
                            lst = lst.OrderBy(x => x.Year).ToList();
                            break;
                        case "month":
                            lst = lst.OrderBy(x => x.Month).ToList();
                            break;
                        default:
                            break;
                    }
                }

                if (!string.IsNullOrEmpty(message))
                    throw new Exception(message);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw ex;
            }

            return View(lst);
        }


        #endregion

        #region E-mail

        // [AccessControlAuthorize]
        [HttpPost]
        public JsonResult SendStatements()
        {
            List<StatementClient> lst = new List<StatementClient>();

            try
            {
                LogHelper.SaveAuditingLog("Statement.SendStatements");

                StatementClientFilter filter = new StatementClientFilter();

                filter.AutomaticSending = 1;

                string message = string.Empty;
                lst = GetClients(filter, out message);

                foreach (var client in lst)
                {
                    // Para cada cliente, pega o extrato.
                    
                    if (client.StatementIDs != null && client.StatementIDs.Count > 0)
                    {
                        var mail = new SendMail
                        {
                            EmailTo = client.Email, // "diego.pampolin@qx3.com.br", // client.Email
                            Body = "",
                            Subject = "Extrato Único"
                        };

                        foreach (var statement in client.StatementIDs)
                        {
                            if (statement == 1)
                            {
                                #region Renda Fixa

                                FixedIncomeFilter f = new FixedIncomeFilter();

                                var firstDayOfMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1); // DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);

                                f.CpfCnpj = client.CpfCnpj; // 45631743353
                                f.Start = firstDayOfMonth; // Convert.ToDateTime("01/01/2017"); 
                                f.End = lastDayOfMonth; // Convert.ToDateTime("30/06/2020");

                                string m = string.Empty;
                                var fixedIncome = GetFixedIncome(f);

                                string fileName = string.Format("RendaFixa_{0}_{1}.{2}", f.CpfCnpj.ToString(), DateTime.Now.ToString("ddMMyyyymmss"), "pdf");

                                var table = RenderRazorViewToString("FixedIncomePrint", fixedIncome);
                                AddResponseHeader(Response, fileName);

                                var pdfBytes = ConvertHtmltoPdf(table, fileName);

                                Attachment attachment = new Attachment(new MemoryStream(pdfBytes), fileName, "application/pdf");
                                mail.Attachments.Add(attachment);

                                #endregion
                            }
                            else if (statement == 2 || statement == 3) 
                            {
                                #region NDF (Curva Contábil) e NDF (Curva Mercado)

                                var dto = new NdfDTO();
                                NdfFilter f = new NdfFilter();

                                NdfSwapType ndfType = statement == 2 ? NdfSwapType.CurvaContabil : NdfSwapType.CurvaMercado;

                                f.CpfCnpj = client.CpfCnpj; // 18520884000170; // client.CpfCnpj;
                                f.Month = DateTime.Now.Month; // 1; // DateTime.Now.Month;
                                f.Year = DateTime.Now.Year; // 2017; // DateTime.Now.Year;
                                f.Type = ndfType;

                                string m = string.Empty;
                                dto = GetNdf(f);

                                //if (!string.IsNullOrEmpty(m))
                                //    throw new Exception(m);

                                string fileName = string.Format("NDF_{0}_{1}.{2}", f.CpfCnpj.ToString(), DateTime.Now.ToString("ddMMyyyymmss"), "pdf");

                                string view = ndfType == NdfSwapType.CurvaContabil ? "NDFPrint" : "NDFPrint2";

                                var table = RenderRazorViewToString(view, dto);
                                AddResponseHeader(Response, fileName);

                                var pdfBytes = ConvertHtmltoPdf(table, fileName);

                                Attachment attachment = new Attachment(new MemoryStream(pdfBytes), fileName, "application/pdf");
                                mail.Attachments.Add(attachment);

                                #endregion
                            }
                            else if (statement == 4 || statement == 5)
                            {
                                #region Swap (Curva Contábil) e Swap (Curva Mercado)

                                var dto = new SwapDTO();
                                SwapFilter f = new SwapFilter();

                                NdfSwapType ndfType = statement == 4 ? NdfSwapType.CurvaContabil : NdfSwapType.CurvaMercado;

                                f.CpfCnpj = client.CpfCnpj; // 18520884000170; // client.CpfCnpj;
                                f.Month = DateTime.Now.Month; // 1; // DateTime.Now.Month;
                                f.Year = DateTime.Now.Year; // 2017; // DateTime.Now.Year;
                                f.Type = ndfType;

                                string m = string.Empty;
                                dto = GetSwap(f);

                                //if (!string.IsNullOrEmpty(m))
                                //    throw new Exception(m);

                                string fileName = string.Format("Swap_{0}_{1}.{2}", f.CpfCnpj.ToString(), DateTime.Now.ToString("ddMMyyyymmss"), "pdf");

                                string view = ndfType == NdfSwapType.CurvaContabil ? "SwapPrint" : "SwapPrint2";

                                var table = RenderRazorViewToString(view, dto);
                                AddResponseHeader(Response, fileName);

                                var pdfBytes = ConvertHtmltoPdf(table, fileName);

                                Attachment attachment = new Attachment(new MemoryStream(pdfBytes), fileName, "application/pdf");
                                mail.Attachments.Add(attachment);

                                #endregion
                            }
                        }

                        if (mail.Attachments != null && mail.Attachments.Count > 0)
                        {
                            mail.Send();

                            #region Log

                            StatementLog log = new StatementLog();
                            log.ClientId = client.ClientId;
                            log.StatementIDs = client.StatementIDs;
                            log.Year = DateTime.Now.Year;
                            log.Month = DateTime.Now.Month;

                            InsertStatementLog(log);

                            #endregion
                        }
                    }
                }

                if (!string.IsNullOrEmpty(message))
                    throw new Exception(message);
            }
            catch (Exception ex)
            {
                string error = string.Format("{0} - {1} - {2}",
                    ex.Message,
                    ex.InnerException != null ? ex.InnerException.ToString() : "",
                    ex.StackTrace);

                Response.StatusCode = 400;
                return Json(error, JsonRequestBehavior.AllowGet);
            }

            return Json("Ok", JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}
