﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QX3.Portal.WebSite.Models.Channel;
//using QX3.Portal.WebSite.BMFService;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.WebSite.Helper;
using QX3.Portal.WebSite.Models;
using QX3.Portal.WebSite.OtherService;
using QX3.Spinnex.Common.Services.Logging;

namespace QX3.Portal.WebSite.Controllers
{
    public class BMFController : FileController
    {

        //#region FinancialStatement - Extrato Financeiro

        //public ActionResult FinancialStatement()
        //{
        //    return View();
        //}

        //public ActionResult EditEmailModel()
        //{
        //    return View();
        //}

        //#endregion

        //#region Relatório de Corretagem

        //[AccessControlAuthorize]
        //public ActionResult BrokerageReport() 
        //{
        //    using (var service = ChannelProviderFactory<IOtherContractChannel>.Create("OtherClassName").GetChannel().CreateChannel())
        //    {
        //        var tradingDates = service.GetTradingDates().Result;
        //        ViewBag.TradingDate = tradingDates == null ? DateTime.Now.ToShortDateString() : tradingDates.Select(t=>t.Value).FirstOrDefault();
        //    }

        //    return View();
        //}

        //[HttpPost]
        //public ActionResult BrokerageReportFilter(string txtClients, string txtAssessors, DateTime? txtStartDate, DateTime? txtEndDate) 
        //{
        //    using (var service = ChannelProviderFactory<IBMFContractChannel>.Create("BMFClassName").GetChannel().CreateChannel()) 
        //    {
        //        BrokerageReportFilter filter = new BrokerageReportFilter()
        //        {
        //            AssessorFilter = String.IsNullOrEmpty(txtAssessors) ? "" : new MarketHelper().GetAssessorsRangesToFilter(txtAssessors),
        //            ClientFilter = new MarketHelper().GetClientsRangesToFilter(txtClients),
        //            InitialRequest = txtStartDate,
        //            FinalRequest = txtEndDate,
        //            UserID = CommonHelper.GetLoggedUserGroupId()
        //        };

        //        var model = new BrokerageTransferInfo();

        //        model.BMFInfo = service.LoadBMFBrokerageReport(filter).Result;
        //        model.BovespaInfo = service.LoadBovespaBrokerageReport(filter).Result;

        //        /*Filtrar clientes*/

        //        var clientList = 
        //            (model.BMFInfo != null) 
        //            ? (model.BovespaInfo != null)
        //                ? model.BMFInfo.Select(a => a.ClientCode).Distinct().Union(model.BovespaInfo.Select(a => a.ClientCode).Distinct()).OrderBy(a => a).ToArray()
        //                : model.BMFInfo.Select(a => a.ClientCode).Distinct().OrderBy(a => a).ToArray()
        //            : (model.BovespaInfo != null)
        //                ? model.BovespaInfo.Select(a => a.ClientCode).Distinct().OrderBy(a => a).ToArray()
        //                : null;

        //        Session["clientList"] = clientList;
        //        Session["modelList"] = model;

        //        if (clientList != null && clientList.Count() > 1)
        //            ViewBag.NextClient = 1;

        //        if (clientList != null && clientList.Count() > 0) 
        //        {
        //            using (var otherService = ChannelProviderFactory<IOtherContractChannel>.Create("OtherClassName").GetChannel().CreateChannel())
        //            {
        //                var assessor = !String.IsNullOrEmpty(txtAssessors) ? new MarketHelper().GetAssessorsRangesToFilter(txtAssessors) : "";
        //                ViewBag.ClientInfo = otherService.LoadClientInformation(clientList[0].ToString(), assessor, CommonHelper.GetLoggedUserGroupId()).Result;
        //            }
        //        }

        //        ViewBag.InfoCount = (clientList != null && clientList.Count() > 0) ? "Exibindo 1 de " + clientList.Count().ToString() : "";

        //        return PartialView("BrokerageReportList"
        //            , clientList == null 
        //                ? new BrokerageTransferInfo() 
        //                : new BrokerageTransferInfo() { 
        //                    BMFInfo = (model.BMFInfo != null) ? model.BMFInfo.Where(a=>a.ClientCode == clientList[0]).ToList() : null, 
        //                    BovespaInfo = (model.BovespaInfo != null) ? model.BovespaInfo.Where(a=>a.ClientCode == clientList[0]).ToList() : null
        //                }
        //            );
        //    }
        //}

        //[HttpPost]
        //public ActionResult BrokerageReportFilter_Paginate(int order)
        //{
        //    var model = (BrokerageTransferInfo)Session["modelList"];
        //    var clientList = (int[])Session["clientList"];

        //    /*Filtrar clientes*/
                       
        //    if (order < clientList.Count() - 1)
        //        ViewBag.NextClient = order + 1;

        //    if (order > 0)
        //        ViewBag.PreviousClient = order - 1;

        //    if (clientList.Count() > 1)
        //    {
        //        using (var otherService = ChannelProviderFactory<IOtherContractChannel>.Create("OtherClassName").GetChannel().CreateChannel())
        //        {
        //            ViewBag.ClientInfo = otherService.LoadClientInformation(clientList[order].ToString(), "", CommonHelper.GetLoggedUserGroupId()).Result;
        //        }
        //    }

        //    ViewBag.InfoCount = (clientList != null && clientList.Count() > 0) ? "Exibindo " + (order + 1).ToString()  + " de " + clientList.Count().ToString() : "";

        //    return PartialView("BrokerageReportList"
        //        , clientList == null
        //            ? new BrokerageTransferInfo()
        //            : new BrokerageTransferInfo()
        //            {
        //                BMFInfo = (model.BMFInfo != null) ? model.BMFInfo.Where(a => a.ClientCode == clientList[order]).ToList() : null,
        //                BovespaInfo = (model.BovespaInfo != null) ? model.BovespaInfo.Where(a => a.ClientCode == clientList[order]).ToList() : null
        //            }
        //        );
                
        //}

        //public ActionResult BrokerageReportToFile(bool isPdf)
        //{
        //    List<CustodyTransfer> currentList = new List<CustodyTransfer>();
        //    try
        //    {
        //        if (Session["modelList"] != null && Session["clientList"] != null)
        //        {
        //            var model = (BrokerageTransferInfo)Session["modelList"];
        //            var clientList = (int[])Session["clientList"];
                                        

        //            string fileName = string.Format("relatoriocorretagem_{0}.{1}", DateTime.Now.ToString("dd-MM-yyyy"), isPdf ? "pdf" : "xls");

        //            List<CPClientInformation> clientInfo = new List<CPClientInformation>();

        //            using (var service = ChannelProviderFactory<IOtherContractChannel>.Create("OtherClassName").GetChannel().CreateChannel()) 
        //            {
        //                foreach (var i in clientList)
        //                {
        //                    var client = service.LoadClientInformation(i.ToString(), "", CommonHelper.GetLoggedUserGroupId()).Result;
        //                    if (client != null) client.ClientCode = i;
        //                    clientInfo.Add(client);
        //                }
        //            }                    

        //            //LogHelper.SaveHistoryLog("BMF.BrokerageTransfer", new HistoryData { Action = "Exportar", OldData = string.Empty, NewData = JsonConvert.SerializeObject(currentList) });

        //            return isPdf ? ViewBrokerageReportPdf(model, clientList.ToList(), clientInfo, true, fileName) : ViewBrokerageReportExcel(model, clientList.ToList(), clientInfo, true, fileName);
                    
        //            //return ViewBrokerageTransferExcel(model, clientList.ToList(), clientInfo, true, fileName);
        //        }

        //        TempData["ErrorMessage"] = "Não há dados para o relatório.";
        //        return this.RedirectToAction("BrokerageTransfer"); 
        //    }
        //    catch (Exception ex)
        //    {
        //        LogManager.WriteError(ex);
        //        TempData["ErrorMessage"] = "Ocorreu um erro ao gerar relatório.";
        //        return this.RedirectToAction("BrokerageTransfer");
        //    }

        //}

        //#endregion

        //#region Faixa de Corretagem

        //public ActionResult BrokerageRange() 
        //{
        //    return View();
        //}

        //public ActionResult BrokerageRangeFilter(FormCollection form) 
        //{ 
        //    BrokerageRange result = null;

        //    BrokerageRangeFilter filter = new BrokerageRangeFilter()
        //    {
        //        ClientFilter = new MarketHelper().GetClientsRangesToFilter(form["txtClientFilter"]),
        //        AssessorFilter = new MarketHelper().GetAssessorsRangesToFilter(form["txtAssessorFilter"]),
        //        UserID = CommonHelper.GetLoggedUserGroupId()
        //    };

        //    FilterOptions options = new FilterOptions()
        //    {
        //        InitRow = Convert.ToInt32(form["DisplayStart"]),
        //        FinalRow = 1,
        //        SortColumn = 1,
        //        SortDirection = "asc",
        //        AllRecords = false
        //    };

        //    using (var service = ChannelProviderFactory<IBMFContractChannel>.Create("BMFClassName").GetChannel().CreateChannel()) 
        //    {
        //        bool hasAnterior, hasPosterior = false;
        //        var response = service.LoadBrokerageRange(out hasAnterior, out hasPosterior, filter, options);
        //        result = response.Result;

        //        ViewBag.HasAnterior = hasAnterior; if (ViewBag.HasAnterior) ViewBag.AnteriorValue = Convert.ToInt32(form["DisplayStart"]) - 1;
        //        ViewBag.HasPosterior = hasPosterior; if (ViewBag.HasPosterior) ViewBag.PosteriorValue = Convert.ToInt32(form["DisplayStart"]) + 1;

        //        ViewBag.Counter = "Exibindo " + (Convert.ToInt32(form["DisplayStart"]) + 1).ToString() + " de " + response.Data;

        //        if (result != null && result.BMF != null) 
        //        {
        //            var itemListStockCodes = result.BMF.Select(a => a.Stock).Distinct().ToList();

        //            var BrokerageRangeBMFList = (from a in itemListStockCodes
        //                                     select new BrokerageBMF()
        //                                     {
        //                                         Stock = a,
        //                                         Quantity = result.BMF.Where(b => b.Stock == a).FirstOrDefault().Quantity,
        //                                         MarketType = result.BMF.Where(b => b.Stock == a).FirstOrDefault().MarketType,
        //                                         NormalOperationRate = result.BMF.Where(b => b.Stock == a).Sum(c => c.NormalOperationRate),
        //                                         DayTradeRate = result.BMF.Where(b => b.Stock == a).Sum(c => c.DayTradeRate)
        //                                     }).ToList();

        //            Session["BrokerageRangeBMFList"] = BrokerageRangeBMFList;
        //            Session["BrokerageRangeBMFTotalList"] = result.BMF;
        //        }
                
        //    }            

        //    return PartialView("BrokerageRangeList", result);
        //}

        //[HttpPost]
        //public string LoadBrokerageRangeBMF(FormCollection form) 
        //{
        //    JsonModel dtJson = new JsonModel();

        //    dtJson.sEcho = Convert.ToInt32(form["sEcho"]);

        //    var list = (List<BrokerageBMF>)Session["BrokerageRangeBMFList"];

        //    if (list == null) list = new List<BrokerageBMF>();
            
        //    String[] column = { "Stock", "Quantity", "MarketType", "NormalOperationRate", "DayTradeRate" };

        //    CommonHelper.GenericSort(ref list, CommonHelper.SortListCommand(form, column));

        //    var NRCPartialList = CommonHelper.GenericPagingList(form, list);

        //    dtJson.sEcho = Convert.ToInt32(form["sEcho"]);
        //    /*dtJson.iTotalRecords = NRCPartialList.Count;
        //    dtJson.iTotalDisplayRecords = list.Count;*/
            
        //    dtJson.iTotalRecords = list.Count;
        //    dtJson.iTotalDisplayRecords = list.Count;

        //    var tableValues = new List<string[]>();
        //    List<String> rowData = new List<String>();

        //    foreach (BrokerageBMF cst in list)
        //    {
        //        rowData.Add(cst.Stock);
        //        rowData.Add(cst.Quantity.HasValue ? cst.Quantity.Value.ToString("N0") : "-");
        //        rowData.Add(cst.MarketType);
        //        rowData.Add(cst.NormalOperationRate.HasValue ? cst.NormalOperationRate.Value.ToString("N2") : "-");
        //        rowData.Add(cst.DayTradeRate.HasValue ? cst.DayTradeRate.Value.ToString("N2") : "-");
        //        rowData.Add("<a class=\"ico-detalhes fechado\" href=\"javascript:void(0);\" rel=\"" + cst.Stock + "\">mostrar detalhes</a>");

        //        tableValues.Add(rowData.ToArray());
        //        rowData = new List<string>();

        //    }

        //    dtJson.aaData = tableValues;

        //    return Newtonsoft.Json.JsonConvert.SerializeObject(dtJson);
        //}

        //public ActionResult BrokerageRangeDetails(string stockCode) 
        //{
        //    var list = (List<BrokerageBMF>)Session["BrokerageRangeBMFTotalList"];

        //    if (list != null)
        //    {
        //        list = list.Where(a => a.Stock == stockCode).ToList();
        //    }

        //    return View(list);
        //}

        //[HttpPost]
        //public ActionResult BrokerageRangeFilterOptions(FormCollection form)
        //{
        //    Response.Cache.SetCacheability(HttpCacheability.NoCache);

        //    try
        //    {
        //        var filter = new BrokerageRangeFilter();

        //        if (!string.IsNullOrEmpty(form["txtAssessorFilter"]))
        //            filter.AssessorFilter = new MarketHelper().GetAssessorsRangesToFilter(form["txtAssessorFilter"]);

        //        if (!string.IsNullOrEmpty(form["txtClientFilter"]))
        //            filter.ClientFilter = new MarketHelper().GetClientsRangesToFilter(form["txtClientFilter"]);


        //        var options = new FilterOptions()
        //        {
        //            InitRow = Convert.ToInt32(form["DisplayStart"]),
        //            FinalRow = 1,
        //            SortColumn = 1,
        //            SortDirection = "asc",
        //            AllRecords = true
        //        };

        //        Session["BrokerageRange.Filter"] = filter;
        //        Session["BrokerageRange.Options"] = options;

        //        return Json(true);
        //    }
        //    catch { return Json(false); }
        //}

        //public ActionResult BrokerageRangeToFile(bool isPdf)
        //{
        //    BrokerageRange currentList = new BrokerageRange();
        //    try
        //    {
        //        if (Session["BrokerageRange.Filter"] == null || Session["BrokerageRange.Options"] == null)
        //            throw new Exception();

        //        using (var service = ChannelProviderFactory<IBMFContractChannel>.Create("BMFClassName").GetChannel().CreateChannel())
        //        {
        //            bool hasAnterior, hasPosterior;
        //            var clientFilter = (BrokerageRangeFilter)Session["BrokerageRange.Filter"];
        //            var options = (FilterOptions)Session["BrokerageRange.Options"];

        //            currentList = service.LoadBrokerageRange(out hasAnterior, out hasPosterior, clientFilter, options).Result;

        //            string fileName = string.Format("faixa_corretagem_{0}.{1}", DateTime.Now.ToString("dd-MM-yyyy"), isPdf ? "pdf" : "xls");

        //            return isPdf ? ViewBrokerageRangePdf(currentList, true, fileName) : ViewBrokerageRangeExcel(currentList, true, fileName);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LogManager.WriteError(ex);
        //        TempData["ErrorMessage"] = "Ocorreu um erro ao gerar relatório.";
        //        return this.RedirectToAction("BrokerageRange");
        //    }

        //}
        //#endregion
    }
}
