﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using QX3.Portal.WebSite.Helper;
using QX3.Portal.Contracts.DataContracts;
using QX3.Spinnex.Common.Services.Logging;
using QX3.Portal.WebSite.Models.Channel;
using QX3.Portal.WebSite.AccessControlService;
using QX3.Portal.WebSite.Properties;
using QX3.Portal.WebSite.Models;
using Newtonsoft.Json;
using QX3.Portal.WebSite.LogService;
using System.Text.RegularExpressions;
using QX3.Spinnex.Common.Services.Security;
using QX3.Portal.Services;
using QX3.Portal.WebSite.AuthenticationService;
using System.Web;

namespace QX3.Portal.WebSite.Controllers
{

    public class AccessControlController : FileController
    {
        #region Funcionalities

        [AccessControlAuthorize]
        public ActionResult Funcionalities()
        {
            List<ACElement> elements = new List<ACElement>();

            try
            {
                LogHelper.SaveAuditingLog("AccessControl.Funcionalities");

                AccessControlHelper helper = new AccessControlHelper();
                elements = helper.LoadElements(true);

                return View(elements);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = "Não foi possível carregar as funcionalidades";
                return View(elements);
            }
        }

        public ActionResult FuncionalitiesHistory()
        {
            try
            {
                LogHelper.SaveAuditingLog("AccessControl.FuncionalitiesHistory");
                return View();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = "Não foi possível carregar o histórico das funcionalidades";
                return View();
            }
        }

        public ActionResult FuncionalitiesHistoryToFile(bool isPdf)
        {
            return this.HistoryToFile(isPdf, "Funcionalities", "FuncionalitiesHistory", "Funcionalidades", "Funcionalidade Afetada", false);
        }

        #endregion

        #region Groups Of Assessors

        [AccessControlAuthorize]
        public ActionResult GroupsOfAssessors()
        {
            List<SelectListItem> listItems = new List<SelectListItem>();
            listItems.Add(new SelectListItem { Text = "Todos", Value = "0", Selected = true });

            try
            {
                LogHelper.SaveAuditingLog("AccessControl.GroupsOfAssessors");

                IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();
                var groups = service.LoadGroupOfAssessors(true, 0, "").Result;
                listItems.AddRange((from g in groups
                                    select new SelectListItem
                                        {
                                            Text = g.Name,
                                            Value = g.ID.ToString()
                                        }).OrderBy(s => s.Text).ToList());
                service.Close();

            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }

            ViewBag.GroupOfAssessorsList = listItems;

            return View();

        }

        public ActionResult EditGroupOfAssessors(int id)
        {
            ACGroupOfAssessors group = new ACGroupOfAssessors();

            try
            {
                LogHelper.SaveAuditingLog("AccessControl.EditGroupOfAssessors", string.Format(LogResources.AuditingGroupOfAssessorsEdit, id));

                if (id > 0)
                {
                    IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
                    var service = channelProvider.GetChannel().CreateChannel();

                    service.Open();
                    var result = service.LoadGroupOfAssessors(false, id, "").Result;
                    service.Close();

                    if (result != null && result.Count() > 0)
                    {
                        group = result[0];
                        Session["CurrentGoupOfAssessors"] = group;
                    }
                    else
                        TempData["ErrorMessage"] = "Não foi possível carregar edição de grupo de assessores.";
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = "Não foi possível carregar edição de grupo de assessores.";
            }


            return View(group);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditGroupOfAssessors(FormCollection form)
        {
            try
            {
                IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                string filters = Request["hdfFilters"];
                int groupID = Int32.Parse(Request["hdfGroupID"]);

                ACGroupOfAssessors group = new ACGroupOfAssessors();
                group.ID = groupID;
                group.Name = Request["txtGroupName"];
                group.Assessors = Request["txtAssessors"];
                group.Users = new List<ACUser>();

                string[] users = Request["hdfUSers"].Split(',');
                foreach (string user in users)
                    if (!string.IsNullOrEmpty(user))
                        group.Users.Add(new ACUser { ID = Int32.Parse(user) });

                LogHelper.SaveAuditingLog("AccessControl.EditGroupOfAssessors.Post", string.Format(LogResources.AuditingGroupOfAssessorsEdit, JsonConvert.SerializeObject(group)));

                var response = service.InsertGroupOfAssessors(group);

                service.Close();

                if (response.Result > 0)
                {
                    LogHelper.SaveHistoryLog("AccessControl.GroupsOfAssessors", new HistoryData { Action = groupID == 0 ? "Criar Grupo" : "Alterar Grupo", OldData = string.Empty, NewData = JsonConvert.SerializeObject(group), TargetID = response.Result });

                    TempData["SuccessID"] = response.Result;
                    TempData["Success"] = groupID > 0 ? "edit" : "create";
                    TempData["Filters"] = filters;

                    Session["AccessControl.ACGroupOfAssessors"] = null;
                }
                else
                    TempData["ErrorMessage"] = "Não foi possível cadastrar este novo grupo de assessores.";
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = "Não foi possível cadastrar este novo grupo de assessores.";
            }


            return RedirectToAction("GroupsOfAssessors");
        }

        public ActionResult GroupOfAssessorsDetails(int id)
        {
            ACGroupOfAssessors group = new ACGroupOfAssessors();

            try
            {
                LogHelper.SaveAuditingLog("AccessControl.GroupOfAssessorsDetails", string.Format(LogResources.AuditingGroupOfAssessorsEdit, id));

                IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                var result = service.LoadGroupOfAssessors(false, id, "").Result;

                service.Close();

                if (result.Count() > 0)
                {
                    group = result[0];
                    LogHelper.SaveHistoryLog("AccessControl.GroupsOfAssessors", new HistoryData { Action = "Ver Detalhe", OldData = string.Empty, NewData = JsonConvert.SerializeObject(group), TargetID = id });
                }
                else
                    TempData["ErrorMessage"] = "Grupo de assessores não encontrado.";
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = "Não foi possível carregar grupo de assessores.";
            }

            return View(group);
        }

        public ActionResult ConfirmGroupOfAssessorsDelete(int groupId, string groupName)
        {
            return View();
        }

        public ActionResult ConfirmUserDelete(int userId)
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteGroupOfAssessors(string hdfGroupId, string hdfGroupName)
        {
            try
            {
                LogHelper.SaveAuditingLog("AccessControl.GroupOfAssessorsDetails", string.Format(LogResources.AuditingGroupOfAssessorsEdit, hdfGroupId));

                IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                int groupId = Int32.Parse(hdfGroupId);
                var changed = service.DeleteGroupOfAssessors(groupId).Result;

                if (changed)
                {
                    TempData["Success"] = "delete";
                    TempData["SuccessMessage"] = "Grupo excluído com sucesso.";
                    LogHelper.SaveHistoryLog("AccessControl.GroupsOfAssessors", new HistoryData { Action = "Excluir Grupo", TargetID = groupId, Target = hdfGroupName });
                    Session["AccessControl.ACGroupOfAssessors"] = null;
                }
                else
                    TempData["ErrorMessage"] = "Não foi possível exluir grupo.";

                service.Close();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
            }

            return RedirectToAction("GroupsOfAssessors");
        }

        public ActionResult GroupOfAssessorsToFile(bool isPdf)
        {
            List<GroupOfAssessorsListItem> currentList = new List<GroupOfAssessorsListItem>();

            try
            {
                if (Session["GroupsOfAssessors"] != null)
                    currentList = (List<GroupOfAssessorsListItem>)Session["GroupsOfAssessors"];

                LogHelper.SaveAuditingLog("AccessControl.GroupOfAssessorsToFile", string.Format(LogResources.ObjectToFileParameters, isPdf ? "PDF" : "Excel", JsonConvert.SerializeObject(currentList)));
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return this.RedirectToAction("GroupsOfAssessors");
            }

            string fileName = string.Format("gruposdeassessores_{0}.{1}", DateTime.Now.ToString("dd-MM-yyyy"), isPdf ? "pdf" : "xls");

            LogHelper.SaveHistoryLog("AccessControl.GroupsOfAssessors", new HistoryData { Action = "Exportar", OldData = string.Empty, NewData = JsonConvert.SerializeObject(currentList) });

            return isPdf ? ViewGroupOfAssessorsPdf(currentList, true, fileName) : ViewGroupOfAssessorsExcel(currentList, true, fileName);
        }

        public ActionResult GroupsOfAssessorsHistory()
        {
            List<SelectListItem> listItems = new List<SelectListItem>();
            listItems.Add(new SelectListItem { Text = "Todas", Value = "", Selected = true });

            try
            {
                var actions = AccessControlHelper.GroupOfAssessorsActions;
                foreach (string a in actions)
                    listItems.Add(new SelectListItem { Text = a, Value = a, Selected = false });

                ViewBag.GroupOfAssessorsActions = listItems;

                LogHelper.SaveAuditingLog("AccessControl.GroupOfAssessorsHistory");
                return View();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = "Não foi possível carregar o histórico dos grupos de assessores.";
                return View();
            }
        }

        public ActionResult GroupsOfAssessorsHistoryToFile(bool isPdf)
        {
            return this.HistoryToFile(isPdf, "GroupsOfAssessors", "GroupsOfAssessorsHistory", "Grupos de Assessores", "Grupo Afetado", true);
        }

        #endregion

        #region Users

        public ActionResult UsersHistory()
        {
            List<SelectListItem> listItems = new List<SelectListItem>();
            listItems.Add(new SelectListItem { Text = "Todas", Value = "", Selected = true });

            try
            {
                var actions = AccessControlHelper.UserActions;
                foreach (string a in actions)
                    listItems.Add(new SelectListItem { Text = a, Value = a, Selected = false });

                ViewBag.UserList = listItems;

                LogHelper.SaveAuditingLog("AccessControl.UsersHistory");
                return View();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = "Não foi possível carregar o histórico dos usuários.";
                return View();
            }
        }

        [AccessControlAuthorize]
        public ActionResult Users()
        {
            try
            {
                IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                var listItems = new List<SelectListItem>();
                var profiles = service.LoadProfiles().Result;
                listItems.Add(new SelectListItem { Text = "--selecione--", Value = "0", Selected = true });
                listItems.AddRange((from p in profiles
                                    select new SelectListItem
                                        {
                                            Text = p.Name,
                                            Value = p.ID.ToString()
                                        }).OrderBy(s => s.Text).ToList());

                ViewData["ProfilesList"] = listItems;


                var listItems2 = new List<SelectListItem>();
                var groups = service.LoadGroupOfAssessors(false, 0, "").Result;
                listItems2.Add(new SelectListItem { Text = "--selecione--", Value = "0", Selected = true });
                listItems2.AddRange((from g in groups
                                     select new SelectListItem
                                     {
                                         Text = g.Name,
                                         Value = g.ID.ToString()
                                     }).OrderBy(s => s.Text).ToList());

                ViewData["GroupsList"] = listItems2;

                TempData["ErrorMessage"] = null;

                service.Close();

            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }

            return View();

        }

        public ActionResult EditUser(int id)
        {
            List<SelectListItem> listItems = new List<SelectListItem>();
            listItems.Add(new SelectListItem { Text = "-Selecione-", Value = "0", Selected = true });

            ACUser user = new ACUser();

            var lstInvestorKind = new List<SelectListItem>();
            lstInvestorKind.Add(new SelectListItem { Text = "Nenhum", Value = "0" });
            lstInvestorKind.Add(new SelectListItem { Text = "Qualificado", Value = "1" });
            lstInvestorKind.Add(new SelectListItem { Text = "Profissional", Value = "2" });

            try
            {
                IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();
                var profiles = service.LoadProfiles().Result;

                listItems.AddRange(profiles.Select(p => new SelectListItem { Text = p.Name, Value = p.ID.ToString() }).OrderBy(s => s.Text).ToList());


                if (id > 0)
                {
                    user = service.LoadUser(id).Result;


                    if (user == null) TempData["ErrorMessage"] = "Não foi possível carregar edição do usuário.";
                    else
                    {
                        Session["CurrentUserEdit"] = user;

                        if (user.Profile != null && user.Profile.ID > 0)
                        {
                            listItems.Where(li => Convert.ToInt32(li.Value) == user.Profile.ID).SingleOrDefault().Selected = true;
                            listItems.Where(li => Convert.ToInt32(li.Value) == 0).SingleOrDefault().Selected = false;
                        }

                        lstInvestorKind.Where(li => Convert.ToInt32(li.Value) == user.InvestorKind).SingleOrDefault().Selected = true;
                    }
                }
                service.Close();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = "Não foi possível carregar edição do usuário.";
            }

            ViewBag.EditUserList = listItems;
            ViewBag.InvestorKindList = lstInvestorKind;

            return View(user);
        }

        [HttpPost] //Deprecated
        public ActionResult EditUser(FormCollection form)
        {
            try
            {
                IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                int userID = Int32.Parse(Request["hdfUserID"]);
                var login = Request["hdfLogin"].ToString();

                ACUser user = new ACUser();
                user.ID = userID;
                user.Name = Request["txtUserName"];
                user.Email = Request["hdfUserEmail"];
                user.AssessorID = Convert.ToInt32(Request["txtAssessorID"]);
                user.ActiveStatus = 1;
                user.Profile = service.LoadProfile(Convert.ToInt32(Request["ddlProfile"])).Result;
                user.Login = user.ID > 0 ? Request["txtLogin"].ToString() : login;
                user.CdCliente = /*Convert.ToInt32(Request["tbCdBolsa"])*/ 0;
                user.Blocked = Convert.ToInt32(Request["blocked"]) == 1 ? true : false;

                var response = service.InsertUser(user);

                service.Close();

                if (response.Result > 0)
                {
                    LogHelper.SaveHistoryLog("AccessControl.Users", new HistoryData { Action = userID == 0 ? "Criar Usuário" : "Alterar Usuário", OldData = string.Empty, NewData = JsonConvert.SerializeObject(user), TargetID = response.Result });

                    TempData["SuccessID"] = response.Result;
                    TempData["Success"] = userID > 0 ? "edit" : "create";
                }
                else
                    TempData["ErrorMessage"] = string.Format("Não foi possível {0} este usuário.", userID > 0 ? "editar" : "cadastrar");
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = "Não foi possível efetuar a operação.";
            }


            return RedirectToAction("Users");
        }

        public string GenerateChaveAD()
        {
            Random random = new Random();
            string chave = string.Empty;

            string letras = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            while (chave.Length < 3)
            {
                char letra = letras[(int)(random.NextDouble() * letras.Length)];
                chave = chave.Contains(letra.ToString()) ? string.Empty : chave + letra;
            }

            string numeros = "0123456789";
            chave = chave + numeros[(int)(random.NextDouble() * numeros.Length)];

            return chave.ToString().ToLower();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteUser(string hdfUserId)
        {
            try
            {

                IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();
                var user = service.LoadUser(Convert.ToInt32(hdfUserId)).Result;
                var changed = service.DeleteUser(Convert.ToInt32(hdfUserId)).Result;

                if (changed)
                {
                    //TODO : gerar log
                    TempData["Success"] = "delete";
                    LogHelper.SaveHistoryLog("AccessControl.Users", new HistoryData { Action = "Excluir Usuário", TargetID = Int32.Parse(hdfUserId), Target = user.Name });
                    TempData["SuccessMessage"] = "Usuário excluído com sucesso.";
                }
                else
                    TempData["ErrorMessage"] = "Não foi possível exluir usuário.";


                service.Close();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
            }

            return RedirectToAction("Users");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AssociateUsersToProfileView(int[] chkUser)
        {
            var users = new List<ACUser>();

            try
            {
                IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();
                var listUsers = service.LoadUsers().Result;

                users = listUsers.Where(u => chkUser.Contains(u.ID)).OrderBy(u => u.Name).ToList();


                service.Close();


            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }

            return PartialView("AssociateUsersToProfile", users);
        }

        public ActionResult AssociateUsersToProfile(string txtProfileName, int[] userID, int? hdfProfile)
        {
            var profile = new ACProfile();
            var errorMessage = string.Empty;
            var errorUserID = new List<int>();
            bool result = false;
            try
            {
                IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                profile = service.LoadProfiles().Result.Where(ga => hdfProfile.HasValue ? ga.ID == hdfProfile.Value : ga.Name == txtProfileName).SingleOrDefault(); ;


                if (profile != null)
                {
                    var users = service.LoadUsers().Result.Where(u => userID.Contains(u.ID)).ToList();

                    foreach (var user in users)
                    {
                        if (user.Profile.ID != profile.ID)
                        {
                            user.Profile.ID = profile.ID;
                            var response = service.InsertUser(user);
                            if (response.Result <= 0) errorUserID.Add(user.ID);
                            else LogHelper.SaveHistoryLog("AccessControl.Users", new HistoryData { Action = "Associar Usuário a um Perfil", OldData = string.Empty, NewData = JsonConvert.SerializeObject(user), TargetID = response.Result, Target = user.Name });
                        }
                    }
                    if (errorUserID.Count > 0) errorMessage = "Não foi possível associar usuários.";
                    else
                    {
                        result = true;
                        TempData["Success"] = result;
                        TempData["SuccessMessage"] = "Usuário(s) associado(s) com sucesso!";
                    }

                }
                else
                {
                    errorMessage = "O Perfil '" + txtProfileName + "' não existe.";
                }

                service.Close();
                return Json(new { error = errorMessage });
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return Json("Não foi possível editar este novo usuário.");
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AssociateUsersToGroupView(int[] chkUser)
        {
            var users = new List<ACUser>();

            try
            {
                IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();
                var listUsers = service.LoadUsers().Result;

                users = listUsers.Where(u => chkUser.Contains(u.ID)).OrderBy(u => u.Name).ToList();


                service.Close();


            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }

            return PartialView("AssociateUsersToGroup", users);
        }

        public JsonResult IncludeUserInList(string name, int? id, int[] userID)
        {
            var errorMessage = string.Empty;
            var user = new ACUser();
            try
            {
                IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                user = service.LoadUsers().Result.Where(u => (id != null ? u.ID == id : u.Name == name) && (userID != null ? !userID.Contains(u.ID) : true)).FirstOrDefault();

                if (user == null)
                {
                    errorMessage = "O usuário '" + name + "' não existe ou já foi adicionado a lista.";

                }

                service.Close();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                errorMessage = "Não foi possível localizar o usuário.";
            }


            return Json(new { error = errorMessage, user });
        }

        public ActionResult AssociateUsersToGroup(string txtGroupName, int[] userID, int? hdfGroup)
        {
            var group = new ACGroupOfAssessors();
            var errorMessage = string.Empty;
            var errorUserID = new List<int>();
            bool result = false;
            try
            {
                IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                group = service.LoadGroupOfAssessors(false, 0, "").Result.Where(ga => hdfGroup.HasValue ? ga.ID == hdfGroup.Value : ga.Name == txtGroupName).SingleOrDefault(); ;


                if (group != null)
                {
                    var users = service.LoadUsers().Result.Where(u => userID.Contains(u.ID)).ToList();

                    foreach (var user in users)
                    {
                        if (!user.AssessorsGroup.Where(ag => ag.ID == group.ID).Any())
                        {
                            //user.AssessorsGroup.Add(group);
                            //var response = service.InsertUser(user);
                            //if (response.Result <= 0) errorUserID.Add(user.ID);
                            //else LogHelper.SaveHistoryLog("AccessControl.Users", new HistoryData { Action = "Associar Usuário a um Grupo" , OldData = string.Empty, NewData = JsonConvert.SerializeObject(user), TargetID = response.Result, Target = user.Name });

                            var response = service.InsertUserGroupRelationship(user.ID, group.ID);
                            if (!response.Result) errorUserID.Add(user.ID);
                            else LogHelper.SaveHistoryLog("AccessControl.Users", new HistoryData { Action = "Associar Usuário a um Grupo", OldData = string.Empty, NewData = JsonConvert.SerializeObject(user), TargetID = group.ID, Target = group.Name });
                        }
                    }
                    if (errorUserID.Count > 0) errorMessage = "Não foi possível associar usuários.";
                    else
                    {
                        result = true;
                        TempData["Success"] = result;
                        TempData["SuccessMessage"] = "Usuário(s) associado(s) com sucesso!";
                    }

                }
                else
                {
                    errorMessage = "O Grupo '" + txtGroupName + "' não existe.";
                }

                service.Close();
                return Json(new { error = errorMessage });
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return Json("Não foi possível editar este novo usuário.");
            }
        }

        public ActionResult ConfirmResetPassword(int userId)
        {
            return View();
        }

        [HttpPost]
        public ActionResult ResetPassword(int userId)
        {
            try
            {
                IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
                using (var service = channelProvider.GetChannel().CreateChannel())
                {
                    var response = service.LoadUser(userId);

                    if (response.Result.ID > 0 && response.Result.SendSuitabilityEmail)
                    {

                        IChannelProvider<IAuthenticationContractChannel> channelProvider1 = ChannelProviderFactory<IAuthenticationContractChannel>.Create("AuthenticationClassName");
                        using (var service1 = channelProvider1.GetChannel().CreateChannel())
                        {
                            var response2 = service1.CreateGuidToSend(new UserInformation { Email = response.Result.Email });

                            if (response2.Result)
                            {
                                var response3 = service1.SendRedefinePasswordEmail(response2.Data, response.Result.Email, response.Result.Name).Result;

                                if (response3)
                                {
                                    TempData["Success"] = "ok";
                                    TempData["SuccessMessage"] = "Enviamos um e-mail para que o usuário redefina sua senha.";
                                }
                                else
                                {
                                    TempData["ErrorMessage"] = "Ocorreu um erro ao enviar e-mail de redefinição de senha.";
                                }
                            }
                            else
                            {
                                TempData["ErrorMessage"] = "Ocorreu um erro ao criar e-mail de redefinição de senha.";
                            }
                        }
                    }
                    else
                    {
                        TempData["ErrorMessage"] = string.Format("Não foi possível encontrar uma conta associada a {0}.", response.Result.Email);
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = "Ocorreu um erro ao buscar usuário.";
            }
            return RedirectToAction("Users");
        }

        #endregion

        #region Profile

        [AccessControlAuthorize]
        public ActionResult Profiles()
        {
            AccessControlHelper helper = new AccessControlHelper();
            ViewData["Elements"] = helper.LoadElements(true);

            List<SelectListItem> listItems = new List<SelectListItem>();
            listItems.Add(new SelectListItem { Text = "Todos", Value = "0", Selected = true });

            try
            {
                LogHelper.SaveAuditingLog("AccessControl.Profiles");

                List<ACElement> lstElements = new List<ACElement>();

                IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();
                var profiles = service.LoadProfilesAndUserCount(0, "", false, lstElements.ToArray()).Result;
                listItems.AddRange((from p in profiles
                                    select new SelectListItem
                                    {
                                        Text = p.Name,
                                        Value = p.ID.ToString()
                                    }).OrderBy(s => s.Text).ToList());

                service.Close();

                TempData["ErrorMessage"] = null;

            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }

            ViewBag.ProfilesList = listItems;


            return View();
        }

        public ActionResult ProfileDetails(int id)
        {
            try
            {
                LogManager.InitLog("AccessControl.ProfileDetails");
                LogManager.WriteLog("");

                IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                var result = service.LoadProfile(id).Result;

                service.Close();

                if (!result.ID.Equals(0))
                {
                    LogHelper.SaveHistoryLog("AccessControl.Profiles", new HistoryData { Action = "Ver Detalhes", OldData = string.Empty, NewData = JsonConvert.SerializeObject(result), TargetID = id });
                    return View(result);
                }
                else
                    TempData["ErrorMessage"] = "Perfil não encontrado.";
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = "Não foi possível carregar os detalhes do perfil.";
            }

            return View();
        }

        public ActionResult EditProfile(int id)
        {
            ACProfile profile = new ACProfile();

            //TODO: Testar feedback de erro

            try
            {
                LogManager.InitLog("AccessControl.EditProfile");
                LogManager.WriteLog("");

                AccessControlHelper helper = new AccessControlHelper();
                ViewData["Elements"] = helper.LoadElements(true);

                if (id != 0)
                {
                    IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
                    var service = channelProvider.GetChannel().CreateChannel();

                    service.Open();

                    var result = service.LoadProfile(id).Result;

                    service.Close();

                    Session["CurrentProfile"] = result;

                    string profileElements = string.Empty;

                    List<ACElement> lstElements = result.Permissions;
                    if (lstElements != null || lstElements.Count > 0)
                        foreach (ACElement element in lstElements)
                            profileElements = profileElements + string.Format("{0},", element.ID);

                    ViewData["ProfileElements"] = profileElements;

                    profile = result;
                }


            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = "Não foi possível carregar as funcionalidades";
            }

            return View(profile);
        }

        public ActionResult ConfirmProfileDelete(int profileId, string profileName)
        {

            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteProfile(string hdfProfileId, string hdfProfileName)
        {
            try
            {
                //LogHelper.SaveAuditingLog("AccessControl.ProfileDetails", string.Format(LogResources.AuditingProfileEdit, hdfProfileId));

                IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                int profileId = Int32.Parse(hdfProfileId);
                var deleted = service.DeleteProfile(profileId).Result;

                if (deleted)
                {
                    TempData["Success"] = "delete";
                    TempData["SuccessMessage"] = "Perfil excluído com sucesso.";
                    LogHelper.SaveHistoryLog("AccessControl.Profiles", new HistoryData { Action = "Excluir Perfil", TargetID = profileId, Target = hdfProfileName });
                    Session["AccessControl.ACProfiles"] = null;
                }
                else
                    TempData["ErrorMessage"] = "Não foi possível excluir o perfil.";

                service.Close();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
            }

            return RedirectToAction("Profiles");
        }

        #endregion

        #region Ajax Methods

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateElementAlias(int id, string oldAlias, string newAlias)
        {
            try
            {
                LogHelper.SaveAuditingLog("AccessControl.UpdateElementAlias", string.Format(LogResources.AuditingFuncionalitiesParameters, id, newAlias));

                IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                if (id == 0)
                    throw new Exception("ID inválido.");

                if (string.IsNullOrEmpty(newAlias))
                    throw new Exception("Apelido em branco.");

                service.Open();
                var changed = service.UpdateElementAlias(id, newAlias).Result;
                service.Close();

                if (changed)
                {
                    LogHelper.SaveHistoryLog("AccessControl.Funcionalities", new HistoryData { Action = "Alterar Nome", OldData = oldAlias, NewData = newAlias, TargetID = id });

                    AccessControlHelper helper = new AccessControlHelper();

                    helper.UpdateUserPermissions();
                    helper.ResetElementsSession(true);

                    //TempData["Success"] = true;
                }

                return Json(changed.ToString().ToLower());
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return Json("false");
            }
        }

        public JsonResult FuncionalitiesAutoSuggest(string term)
        {
            List<ACElement> elements = new List<ACElement>();
            try
            {
                AccessControlHelper helper = new AccessControlHelper();
                elements = helper.LoadElements(false);
                elements = elements.Where(i => i.Name.ToLower().Contains(term.ToLower())).OrderBy(u => u.Name).ToList();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }
            return Json(elements, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GroupsOfAssessorsAutoSuggest(string term)
        {
            List<ACGroupOfAssessors> groups = new List<ACGroupOfAssessors>();
            try
            {
                AccessControlHelper helper = new AccessControlHelper();
                groups = helper.LoadGroupsOfAssessors();
                groups = groups.Where(i => i.Name.ToLower().Contains(term.ToLower())).OrderBy(u => u.Name).ToList();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }
            return Json(groups, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UserAutoSuggest(string term, int[] userID, string isProfile)
        {
            List<ACUser> users = new List<ACUser>();

            try
            {
                AccessControlHelper helper = new AccessControlHelper();

                users = (!string.IsNullOrEmpty(isProfile) && isProfile.Equals("1")) ? helper.LoadUsersWithoutProfile() : helper.LoadUsers();
                //users = users.Where(i => i.Name.ToLower().Contains(term.ToLower())).OrderBy(u => u.Name).ToList();

                users = (from x in users where (x.Name.ToLower().Contains(term.ToLower()) || x.CdCliente.ToString().Contains(term)) orderby x.Name ascending select x).ToList();



                if (userID != null)
                    if (userID.Count() > 0) users = users.Where(i => !userID.Contains(i.ID)).ToList();

            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }

            return Json(users, JsonRequestBehavior.AllowGet);

        }

        public JsonResult ProfilesAutoSuggest(string term)
        {

            var profiles = new List<ACProfile>();

            try
            {
                IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
                var service = channelProvider.GetChannel().CreateChannel();
                service.Open();

                profiles = service.LoadProfiles().Result.Where(p => p.Name.ToLower().Contains(term.Trim().ToLower())).OrderBy(p => p.Name).ToList();

                service.Close();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }

            return Json(profiles, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public void LoadGroupOfAssessors(FormCollection form)
        {
            try
            {
                AccessControlHelper helper = new AccessControlHelper();
                int groupId = string.IsNullOrEmpty(form["GroupId"]) ? 0 : Int32.Parse(form["GroupId"]);
                string userName = form["UserName"] ?? string.Empty;
                string filterAssessors = form["Assessors"];
                List<int> assessors = helper.GetAssessorsIds(filterAssessors);

                if (!string.IsNullOrEmpty(filterAssessors) && assessors == null)
                {
                    Session["GroupsOfAssessors"] = new List<GroupOfAssessorsListItem>();
                    return;
                }

                IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();
                var groups = service.LoadGroupOfAssessors(false, groupId, userName.Trim()).Result;
                if (assessors != null && assessors.Count > 0)
                {
                    List<ACGroupOfAssessors> newGroups = new List<ACGroupOfAssessors>();
                    foreach (ACGroupOfAssessors ga in groups)
                    {
                        var groupAssessors = helper.GetAssessorsIds(ga.Assessors);
                        bool contains = (from a in assessors where (from ag in groupAssessors select ag).Contains(a) select a).Any();
                        if (contains)
                            newGroups.Add(ga);
                    }
                    groups = newGroups.ToArray();
                }
                Session["GroupsOfAssessors"] = (from g in groups select new GroupOfAssessorsListItem { ID = g.ID, Name = g.Name, Assessors = g.Assessors, UsersCount = g.Users.Count }).ToList();

                LogHelper.SaveAuditingLog("AccessControl.LoadGroupOfAssessors", string.Format(LogResources.AuditingLoadGroupOfAssessors, groupId, userName, filterAssessors, JsonConvert.SerializeObject(Session["GroupsOfAssessors"])));

                service.Close();

            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult LoadUsers(FormCollection form)
        {
            try
            {
                AccessControlHelper helper = new AccessControlHelper();
                int profileId = string.IsNullOrEmpty(form["ProfileId"]) ? 0 : Int32.Parse(form["ProfileId"]);
                int groupId = string.IsNullOrEmpty(form["GroupId"]) ? 0 : Int32.Parse(form["GroupId"]);
                //string assessorId = form["AssessorID"] ?? string.Empty;
                string userName = form["UserName"] ?? string.Empty;
                bool noProfile = Convert.ToBoolean(form["NoProfile"]);
                bool noGroup = Convert.ToBoolean(form["NoGroup"]);
                string filterAssessors = String.IsNullOrEmpty(form["AssessorID"]) ? "" : form["AssessorID"];
                List<int> assessors = helper.GetAssessorsIds(filterAssessors);


                if (!string.IsNullOrEmpty(filterAssessors) && assessors == null)
                {
                    Session["Users"] = new List<UsersListItem>();
                    return Json(new { message = "" });
                }

                IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();
                var users = service.LoadUsers().Result;

                service.Close();

                if (assessors != null && assessors.Count > 0)
                {
                    var usersList = new List<ACUser>();
                    var groups = service.LoadGroupOfAssessors(false, groupId, userName.Trim()).Result;
                    foreach (ACGroupOfAssessors ga in groups)
                    {
                        if (ga.Assessors != null)
                        {
                            List<int> groupAssessors = helper.GetAssessorsIds(ga.Assessors);
                            bool contains = (from a in assessors where (from ag in groupAssessors select ag).Contains(a) select a).Any();
                            if (contains)
                                usersList.AddRange(users.Where(u => u.AssessorsGroup.Select(ag => ag.ID).Contains(ga.ID) || assessors.Contains(u.AssessorID)));
                        }
                    }


                    users = usersList.Distinct().ToArray();
                }

                var listUser = new List<UserListModel>();
                foreach (var user in users)
                {
                    var itemUser = new UserListModel();
                    var listaCompleta = new List<int>();
                    foreach (var group in user.AssessorsGroup)
                    {
                        if (group.Assessors != null)
                            listaCompleta.AddRange(WebSiteHelper.ConvertRangeStrToNumList(group.Assessors));
                    }
                    itemUser.ID = user.ID.ToString();
                    itemUser.AssessorInterval = WebSiteHelper.ConvertNumListToRangeStr(listaCompleta.Distinct().OrderBy(i => i).ToList());

                    listUser.Add(itemUser);
                }

                Session["Users"] = users.Where(
                    u =>
                    (!noProfile ? (profileId == 0 ? true : u.Profile.ID == profileId) : u.Profile.ID == 0) &&
                        //Verifica Perfil, ou no caso Sem Perfil
                    (String.IsNullOrEmpty(userName.ToLower()) ? true : u.CpfCnpj.ToString().Equals(CommonHelper.CleanupCpfCnpj( userName)) ||  String.IsNullOrEmpty(userName.ToLower()) ? true : u.Name.ToLower().Contains(userName.ToLower()) || String.IsNullOrEmpty(userName.ToLower()) ? true : u.CdCliente.ToString().Contains(userName)) &&
                        // Verifica a existência do nome
                    (!noGroup
                         ? (groupId == 0 ? true : u.AssessorsGroup.Exists(p => p.ID == groupId))
                         : u.AssessorsGroup.Count == 0)) // Verifica Grupo, ou no caso Sem Grupo
                    .Select(u => new UsersListItem
                                     {
                                         ID = u.ID.ToString(),
                                         ClientID = u.CdCliente,
                                         Name = u.Name,
                                         AssessorGroup = String.Join(", ", u.AssessorsGroup.Select(ag => ag.Name)),
                                         AssessorID = u.AssessorID < 0 ? " - " : u.AssessorID.ToString("000"),
                                         ProfileName = u.Profile.Name,
                                         AssessorInterval = listUser.Count > 0 ? listUser.Where(lu => lu.ID == u.ID.ToString()).Select(lu => lu.AssessorInterval).SingleOrDefault() : "",
                                         Type = u.Type,
                                         Blocked = Convert.ToInt16(u.Blocked).ToString(),
                                         Email = u.Email,
                                         Login = u.Login,
                                         CpfCnpj = u.CpfCnpj
                                     }).OrderBy(u => u.Name).ToList();

                service.Close();
                return Json(new { Error = "" });
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return Json(new { Error = "Ocorreu um erro ao realizar pesquisa." });
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UpdateGroupOfAssessors(string txtGroupName, string hdfGroupID, int[] userID, string txtAssessors, int[] clientIncludeID, int[] clientExcludeID)
        {
            string errorMessage = string.Empty;
            var groupName = txtGroupName;
            var groupId = Int32.Parse(hdfGroupID);
            var assessors = txtAssessors;
            string clientsInclude = string.Empty, clientsExclude = string.Empty;
            List<int> listInclude = new List<int>();
            List<int> listExclude = new List<int>();

            if (clientIncludeID != null)
            {
                foreach (int clientId in clientIncludeID)
                    listInclude.Add(clientId);

                listInclude.Sort();

                foreach (int clientId in listInclude)
                    clientsInclude += clientId + ";";
            }

            if (clientExcludeID != null)
            {
                foreach (int clientId in clientExcludeID)
                    listExclude.Add(clientId);

                listExclude.Sort();

                foreach (int clientId in listExclude)
                    clientsExclude += clientId + ";";
            }

            try
            {
                AccessControlHelper helper = new AccessControlHelper();

                if (!helper.GroupNameIsValid(groupName, groupId))
                    errorMessage += "<li>Este nome de grupo já existe.</li>";
                /*if (!string.IsNullOrEmpty(assessors) && !helper.AssessorsListIsValid(assessors, groupId))
                    errorMessage += "<li>Lista de assessores inválida.</li>";*/
                if (!string.IsNullOrEmpty(assessors) && !helper.AssessorsListIsValid(groupId, assessors, clientsInclude, clientsExclude))
                    errorMessage += "<li>Lista de assessores inválida.</li>";

                if (!string.IsNullOrEmpty(errorMessage))
                    return Json(new { error = errorMessage });

                IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                ACGroupOfAssessors group = new ACGroupOfAssessors();
                group.ID = groupId;
                group.Name = groupName;
                group.Assessors = assessors;
                group.Users = new List<ACUser>();

                if (userID != null)
                    foreach (var user in userID)
                        if (user != 0) group.Users.Add(new ACUser { ID = user });

                if (clientIncludeID != null)
                {
                    group.ClientsIncluded = new List<Client>();

                    foreach (var clientCode in clientIncludeID)
                        if (clientCode != 0) group.ClientsIncluded.Add(new Client() { ClientCode = clientCode });
                }

                if (clientExcludeID != null)
                {
                    group.ClientsExcluded = new List<Client>();

                    foreach (var clientCode in clientExcludeID)
                        if (clientCode != 0) group.ClientsExcluded.Add(new Client() { ClientCode = clientCode });
                }

                var response = service.InsertGroupOfAssessors(group);

                service.Close();

                if (response.Result <= 0)
                    errorMessage = "Não foi possível editar este novo grupo de assessores.";
                else
                {
                    if (groupId == 0)
                    {
                        TempData["Success"] = response.Result;
                        TempData["SuccessMessage"] = "Grupo cadastrado com sucesso.";
                    }

                    ACGroupOfAssessors oldData = new ACGroupOfAssessors();
                    if (Session["CurrentGoupOfAssessors"] != null)
                        oldData = (ACGroupOfAssessors)Session["CurrentGoupOfAssessors"];

                    group.ID = response.Result;

                    LogHelper.SaveHistoryLog("AccessControl.GroupsOfAssessors", new HistoryData { Action = groupId == 0 ? "Criar Grupo" : "Alterar Grupo", OldData = JsonConvert.SerializeObject(oldData), NewData = JsonConvert.SerializeObject(group), TargetID = response.Result });

                    Session["AccessControl.ACGroupOfAssessors"] = null;
                    Session["CurrentGoupOfAssessors"] = null;
                }

                return Json(new { error = errorMessage, group, totalUsers = group.Users.Count });
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return Json(new { error = "Não foi possível editar este novo grupo de assessores." });
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ValidateUsernameSuggest(string userName)
        {
            string errorMessage = string.Empty;

            if (string.IsNullOrEmpty(userName))
                return Json("true");

            try
            {
                AccessControlHelper helper = new AccessControlHelper();

                var users = helper.LoadUsers();
                return Json((from u in users where u.Name.Equals(userName) select u).Any().ToString().ToLower());
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return Json("false");
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ValidateFuncionalityAliasSuggest(string alias)
        {
            string errorMessage = string.Empty;

            if (string.IsNullOrEmpty(alias))
                return Json("true");

            try
            {
                AccessControlHelper helper = new AccessControlHelper();

                var funcs = helper.LoadElements(false);
                return Json((from f in funcs where f.Alias.Equals(alias) select f).Any().ToString().ToLower());
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return Json("false");
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ValidateGroupOfAssessorsNameSuggest(string name)
        {
            string errorMessage = string.Empty;

            if (string.IsNullOrEmpty(name))
                return Json("true");

            try
            {
                AccessControlHelper helper = new AccessControlHelper();

                var groups = helper.LoadGroupsOfAssessors();
                return Json((from g in groups where g.Name.Equals(name) select g).Any().ToString().ToLower());
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return Json("false");
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ValidateProfileNameSuggest(string name)
        {
            string errorMessage = string.Empty;

            if (string.IsNullOrEmpty(name))
                return Json("true");

            try
            {
                AccessControlHelper helper = new AccessControlHelper();

                var profiles = helper.LoadProfiles();
                return Json((from p in profiles where p.Name.Equals(name) select p).Any().ToString().ToLower());
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return Json("false");
            }
        }

        [HttpPost]
        public JsonResult UpdateUser(int userId, string userName, string userEmail, string assessorID, string profileID, string type, string hasADInfo, string login, string cdCliente, bool bloqueado, string cpfcnpj, string investorKind, int sendSuitabilityEmail)
        {
            string errorMessage = string.Empty;

            try
            {
                LogManager.InitLog("AccessControl.UpdateUser");

                AccessControlHelper helper = new AccessControlHelper();

                if (!helper.EmailIsValid(userEmail, userId))
                    errorMessage += "<li>O E-mail usado já foi cadastrado.</li>";


                if (!helper.UsernameIsValid(login, userId))
                    errorMessage += "<li>O Login usado já foi cadastrado.</li>";

                //bool insertAD = (userId == 0 && hasADInfo == "false");

                string chaveAd = string.Empty;

                if (userId == 0)
                {
                    // if (insertAD)
                    // {
                    //	bool chaveAdOk = false;
                    //	do
                    //	{
                    chaveAd = this.GenerateChaveAD();
                    //		chaveAdOk = helper.ValidateLDAPLogin(chaveAd);
                    //	} 
                    //	while (!chaveAdOk);
                    // }
                }


                if (!string.IsNullOrEmpty(errorMessage))
                    return Json(new { error = errorMessage });

                IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                ACUser user = new ACUser();
                user.ID = userId;
                user.Name = userName;
                user.Email = userEmail;
                user.ChaveAD = chaveAd;
                user.AssessorID = assessorID.Trim() != "" ? Convert.ToInt32(assessorID) : -1;
                user.Profile = service.LoadProfile(Convert.ToInt32(profileID)).Result;
                user.Type = type;
                user.ActiveStatus = 1;
                user.Login = login.ToLower();
                PasswordGenerator pg = new PasswordGenerator();
                user.Password = pg.EncodePassword(CommonHelper.CreateRandomPassword(9));
                user.Blocked = bloqueado;
                user.SendSuitabilityEmail = sendSuitabilityEmail == 1 ? true : false;

                user.InvestorKind = string.IsNullOrEmpty(investorKind) ? 0 : Convert.ToInt32(investorKind);

                if (user.Type == "E")
                {
                    user.CdCliente = Convert.ToInt32(cdCliente);
                    if (!string.IsNullOrEmpty(cpfcnpj))
                        user.CpfCnpj = Int64.Parse(cpfcnpj);
                }
                else
                    user.CdCliente = 0;

                var response = service.InsertUser(user);

                service.Close();

                var oldData = new ACUser();
                if (response.Result <= 0)
                    errorMessage = string.Format("Não foi possível {0} este usuário.", userId == 0 ? "cadastrar" : "editar"); // response.Message; 
                else
                {
                    if (userId == 0)
                    {
                        if (user.SendSuitabilityEmail)
                        {
                            var feedbackEmail = SendWelcomeEmail(user.Email, user.Name);
                            if (!string.IsNullOrEmpty(feedbackEmail))
                                errorMessage = string.Format("{0} O usuário foi cadastrado.", feedbackEmail);
                        }
                    }
                    else
                    {
                        if (Session["CurrentUserEdit"] != null)
                            oldData = (ACUser)Session["CurrentUserEdit"];
                    }
                    Session["AccessControl.ACUsers"] = null;
                    LogHelper.SaveHistoryLog("AccessControl.Users", new HistoryData { Action = userId == 0 ? "Criar Usuário" : "Alterar Usuário", /*OldData = JsonConvert.SerializeObject(oldData), NewData = JsonConvert.SerializeObject(user),*/ TargetID = response.Result, Target = user.Name });

                }
                return Json(new { error = errorMessage });
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return Json(new { error = string.Format("Não foi possível {0} este novo usuário.", userId == 0 ? "cadastrar" : "editar") }); // string.Concat(ex.Message, ex.StackTrace)
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public void LoadFuncionalitiesHistory(FormCollection form)
        {
            try
            {
                List<HistoryData> data = new List<HistoryData>();
                AccessControlHelper helper = new AccessControlHelper();
                string startDate = form["txtStartDate"];
                string endDate = form["txtEndDate"];

                string responsible = form["txtResponsible"];
                long responsibleID = string.IsNullOrEmpty(form["hdfResponsible"]) ? 0 : long.Parse(form["hdfResponsible"]);
                string target = form["txtTarget"];
                int targetID = string.IsNullOrEmpty(form["hdfTarget"]) ? 0 : Int32.Parse(form["hdfTarget"]);

                LogHelper.SaveAuditingLog("AccessControl.LoadFuncionalitiesHistory", string.Format(LogResources.AuditingLoadHistory, targetID, responsibleID, string.Empty, startDate, endDate));

                HistoryFilter filter = new HistoryFilter { ModuleName = "AccessControl.Funcionalities", StartDate = startDate, EndDate = endDate, ResponsibleID = responsibleID, TargetID = targetID };

                IChannelProvider<ILogContractChannel> channelProvider = ChannelProviderFactory<ILogContractChannel>.Create("LogClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();
                var history = service.LoadHistory(filter).Result;
                service.Close();

                if (history != null && history.Count() > 0)
                {
                    foreach (HistoryData hd in history)
                    {
                        hd.Responsible = helper.GetUserName(hd.ResponsibleID);
                        hd.Target = helper.GetFuncionalityAlias(hd.TargetID);
                    }
                    data = history.ToList();
                }

                Session["FuncionalitiesHistory"] = history == null ? new List<HistoryData>() : data;
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public void LoadGroupsOfAssessorsHistory(FormCollection form)
        {
            try
            {
                List<HistoryData> data = new List<HistoryData>();
                AccessControlHelper helper = new AccessControlHelper();
                string startDate = form["txtStartDate"];
                string endDate = form["txtEndDate"];

                long responsibleID = string.IsNullOrEmpty(form["hdfResponsible"]) ? 0 : long.Parse(form["hdfResponsible"]);
                int targetID = string.IsNullOrEmpty(form["hdfTarget"]) ? 0 : Int32.Parse(form["hdfTarget"]);

                string action = form["ddlGroupsOfAssessorsActions"];

                LogHelper.SaveAuditingLog("AccessControl.LoadGroupsOfAssessorsHistory", string.Format(LogResources.AuditingLoadHistory, targetID, responsibleID, action, startDate, endDate));

                HistoryFilter filter = new HistoryFilter { ModuleName = "AccessControl.GroupsOfAssessors", StartDate = startDate, Action = action, EndDate = endDate, ResponsibleID = responsibleID, TargetID = targetID };

                IChannelProvider<ILogContractChannel> channelProvider = ChannelProviderFactory<ILogContractChannel>.Create("LogClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();
                var history = service.LoadHistory(filter).Result;
                service.Close();

                if (history != null && history.Count() > 0)
                {
                    foreach (HistoryData hd in history)
                    {
                        if (string.IsNullOrEmpty(hd.Responsible))
                            hd.Responsible = helper.GetUserName(hd.ResponsibleID);
                        if (string.IsNullOrEmpty(hd.Target))
                            hd.Target = helper.GetGroupName(hd.TargetID);
                    }
                    data = history.ToList();
                }

                Session["GroupsOfAssessorsHistory"] = history == null ? new List<HistoryData>() : data;
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public void LoadUsersHistory(FormCollection form)
        {
            try
            {
                List<HistoryData> data = new List<HistoryData>();
                AccessControlHelper helper = new AccessControlHelper();
                string startDate = form["txtStartDate"];
                string endDate = form["txtEndDate"];

                long responsibleID = string.IsNullOrEmpty(form["hdfResponsible"]) ? 0 : long.Parse(form["hdfResponsible"]);
                int targetID = string.IsNullOrEmpty(form["hdfTarget"]) ? 0 : Int32.Parse(form["hdfTarget"]);

                string action = form["ddlUserActions"];

                LogHelper.SaveAuditingLog("AccessControl.LoadUserHistory", string.Format(LogResources.AuditingLoadHistory, targetID, responsibleID, action, startDate, endDate));

                HistoryFilter filter = new HistoryFilter { ModuleName = "AccessControl.Users", StartDate = startDate, Action = action, EndDate = endDate, ResponsibleID = responsibleID, TargetID = targetID };

                IChannelProvider<ILogContractChannel> channelProvider = ChannelProviderFactory<ILogContractChannel>.Create("LogClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();
                var history = service.LoadHistory(filter).Result;
                service.Close();

                if (history != null && history.Count() > 0)
                {
                    foreach (HistoryData hd in history)
                    {
                        if (string.IsNullOrEmpty(hd.Responsible))
                            hd.Responsible = helper.GetUserName(hd.ResponsibleID);
                        if (string.IsNullOrEmpty(hd.Target))
                            hd.Target = helper.GetUserName(hd.TargetID);
                    }
                    data = history.ToList();
                }

                Session["UsersHistory"] = history == null ? new List<HistoryData>() : data;
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public void LoadProfilesHistory(FormCollection form)
        {
            try
            {
                List<HistoryData> data = new List<HistoryData>();
                AccessControlHelper helper = new AccessControlHelper();
                string startDate = form["txtStartDate"];
                string endDate = form["txtEndDate"];

                long responsibleID = string.IsNullOrEmpty(form["hdfResponsible"]) ? 0 : long.Parse(form["hdfResponsible"]);
                int targetID = string.IsNullOrEmpty(form["hdfTarget"]) ? 0 : Int32.Parse(form["hdfTarget"]);

                string action = form["ddlActions"];

                LogHelper.SaveAuditingLog("AccessControl.LoadProfilesHistory", string.Format(LogResources.AuditingLoadHistory, targetID, responsibleID, action, startDate, endDate));

                HistoryFilter filter = new HistoryFilter { ModuleName = "AccessControl.Profiles", StartDate = startDate, Action = action, EndDate = endDate, ResponsibleID = responsibleID, TargetID = targetID };

                IChannelProvider<ILogContractChannel> channelProvider = ChannelProviderFactory<ILogContractChannel>.Create("LogClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();
                var history = service.LoadHistory(filter).Result;
                service.Close();

                if (history != null && history.Count() > 0)
                {
                    foreach (HistoryData hd in history)
                    {
                        if (string.IsNullOrEmpty(hd.Responsible))
                            hd.Responsible = helper.GetUserName(hd.ResponsibleID);
                        if (string.IsNullOrEmpty(hd.Target))
                            hd.Target = helper.GetProfileName(hd.TargetID);
                    }
                    data = history.ToList();
                }

                Session["ProfilesHistory"] = history == null ? new List<HistoryData>() : data;
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }
        }

        public JsonResult UpdateProfile(string hdfProfileID, string txtProfileName, int[] userID, string listOfElements)
        {
            string errorMessage = string.Empty;
            var profileID = Int32.Parse(hdfProfileID);
            var profileName = txtProfileName;



            try
            {
                LogManager.InitLog("AccessControl.UpdateProfile");

                AccessControlHelper helper = new AccessControlHelper();

                if (string.IsNullOrEmpty(profileName))
                    errorMessage += "<li>Preencher nome do perfil.</li>";

                if (!helper.ProfileNameIsValid(profileName, profileID))
                    errorMessage += "<li>Este nome de perfil já existe.</li>";

                ACProfile profile = new ACProfile();
                profile.ID = profileID;
                profile.Name = profileName;
                profile.Users = new List<ACUser>();
                profile.Permissions = new List<ACElement>();

                string[] elements = listOfElements.Split(',');
                foreach (string element in elements)
                    if (!string.IsNullOrEmpty(element))
                        profile.Permissions.Add(new ACElement { ID = Int32.Parse(element) });

                //if (profile.Permissions.Count > 0)
                //    if (!helper.ProfileElementsAreValid(profile.Permissions, profile.ID))
                //         errorMessage += "<li>Já existe um perfil com estas permissões.</li>";

                if (!string.IsNullOrEmpty(errorMessage))
                    return Json(new { error = errorMessage });

                if (userID != null)
                    foreach (var user in userID)
                        if (user != 0) profile.Users.Add(new ACUser { ID = user });

                IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                var response = service.InsertProfile(profile);

                service.Close();

                if (response.Result <= 0)
                    errorMessage = "Não foi possível editar este perfil.";
                else
                {
                    //if (profileID == 0)
                    //{
                    //    TempData["Success"] = response.Result;
                    //    TempData["SuccessMessage"] = "Perfil cadastrado com sucesso.";
                    //}
                    ACProfile oldData = new ACProfile();
                    if (Session["CurrentProfile"] != null)
                        oldData = (ACProfile)Session["CurrentProfile"];
                    LogHelper.SaveHistoryLog("AccessControl.Profiles", new HistoryData { Action = profileID == 0 ? "Criar Perfil" : "Alterar Perfil", OldData = JsonConvert.SerializeObject(oldData), NewData = JsonConvert.SerializeObject(profile), TargetID = response.Result });
                    Session["AccessControl.ACProfiles"] = null;
                    Session["CurrentProfile"] = null;
                }

                return Json(new { error = errorMessage, profile, totalUsers = profile.Users.Count });
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return Json("Não foi possível editar este perfil.");
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public void LoadProfiles(FormCollection form)
        {
            try
            {
                string userName = form["UserName"] ?? string.Empty;
                int profileID = string.IsNullOrEmpty(form["ProfileId"]) ? 0 : Int32.Parse(form["ProfileId"]);

                bool profilesWithoutUsers = false;
                if (form["WithoutUsers"].Equals("1"))
                {
                    profilesWithoutUsers = true;
                    profileID = 0;
                }

                List<ACElement> lstElements = new List<ACElement>();

                if (!string.IsNullOrEmpty(form["Elements"]))
                {
                    int e;
                    string[] elements = form["Elements"].Split(',');

                    foreach (string element in elements)
                        if (!string.IsNullOrEmpty(element) && int.TryParse(element, out e))
                            lstElements.Add(new ACElement { ID = e });
                }

                IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();
                var profiles = service.LoadProfilesAndUserCount(profileID, userName, profilesWithoutUsers, lstElements.ToArray()).Result;

                Session["Profiles"] = (from p in profiles select new ProfilesListItem { ID = p.ID, Name = p.Name, UsersCount = p.UsersCount }).ToList();

                //LogHelper.SaveAuditingLog("AccessControl.LoadProfiles", string.Format(LogResources.AuditingLoadProfiles, profileID, userName, JsonConvert.SerializeObject(Session["Profiles"])));

                service.Close();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ValidateNewUserInLDAPByEmail(string email)
        {
            string errorMessage = string.Empty;

            if (string.IsNullOrEmpty(email))
                return Json("true");

            try
            {
                AccessControlHelper helper = new AccessControlHelper();
                return Json("true");
                //return Json(helper.CheckIfUserExistsInLDAP(email, AuthenticationService.LDAPSearchFilter.mail).ToString().ToLower());
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return Json("false");
            }
        }

        #endregion

        public ActionResult HistoryToFile(bool isPdf, string targetName, string sessionName, string targetTitle, string targetColumnTitle, bool hasAction)
        {
            List<HistoryData> currentList = new List<HistoryData>();

            try
            {
                if (Session[sessionName] != null)
                    currentList = (List<HistoryData>)Session[sessionName];

                LogHelper.SaveAuditingLog(string.Concat("AccessControl.HistoryToFile.", targetName), string.Format(LogResources.ObjectToFileParameters, isPdf ? "PDF" : "Excel", JsonConvert.SerializeObject(currentList)));
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return this.RedirectToAction("GroupsOfAssessors");
            }

            string name = new Regex(@"\s*").Replace(targetTitle, string.Empty);
            targetTitle = string.Concat(isPdf ? "Histórico de " : "Hist&#243;rico de ", targetTitle);

            string fileName = string.Format("historico_{0}_{1}.{2}", name, DateTime.Now.ToString("dd-MM-yyyy"), isPdf ? "pdf" : "xls");

            return isPdf ? ViewHistoryPdf(currentList, true, fileName, targetTitle, targetColumnTitle, hasAction) : ViewHistoryExcel(currentList, true, fileName, targetTitle, targetColumnTitle, hasAction);
        }

        public ActionResult UsersHistoryToFile(bool isPdf)
        {
            return this.HistoryToFile(isPdf, "Users", "UsersHistory", "Usu&#225;rios", "Usu&#225;rio Afetado", true);
        }

        public ActionResult UsersToFile(bool isPdf)
        {
            List<UsersListItem> currentList = new List<UsersListItem>();

            try
            {
                if (Session["Users"] != null)
                    currentList = (List<UsersListItem>)Session["Users"];

                LogHelper.SaveAuditingLog("AccessControl.UsersToFile", string.Format(LogResources.ObjectToFileParameters, isPdf ? "PDF" : "Excel", JsonConvert.SerializeObject(currentList)));
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return this.RedirectToAction("Users");
            }

            string fileName = string.Format("usuarios_{0}.{1}", DateTime.Now.ToString("dd-MM-yyyy"), isPdf ? "pdf" : "xls");

            LogHelper.SaveHistoryLog("AccessControl.Users", new HistoryData { Action = "Exportar", OldData = string.Empty, NewData = JsonConvert.SerializeObject(currentList) });

            return isPdf ? ViewUsersPdf(currentList, true, fileName) : ViewUsersExcel(currentList, true, fileName);
        }

        public ActionResult ProfilesHistory()
        {
            List<SelectListItem> listItems = new List<SelectListItem>();
            listItems.Add(new SelectListItem { Text = "Todas", Value = "", Selected = true });

            try
            {
                var actions = AccessControlHelper.ProfilesActions;
                foreach (string a in actions)
                    listItems.Add(new SelectListItem { Text = a, Value = a, Selected = false });

                ViewBag.ProfilesActions = listItems;

                LogHelper.SaveAuditingLog("AccessControl.ProfilesHistory");
                return View();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = "Não foi possível carregar o histórico dos perfis.";
                return View();
            }
        }

        public ActionResult ProfilesToFile(bool isPdf)
        {
            List<ProfilesListItem> currentList = new List<ProfilesListItem>();

            try
            {
                if (Session["Profiles"] != null)
                    currentList = (List<ProfilesListItem>)Session["Profiles"];

                LogHelper.SaveAuditingLog("AccessControl.ProfilesToFile", string.Format(LogResources.ObjectToFileParameters, isPdf ? "PDF" : "Excel", JsonConvert.SerializeObject(currentList)));
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return this.RedirectToAction("Profiles");
            }

            string fileName = string.Format("perfis_{0}.{1}", DateTime.Now.ToString("dd-MM-yyyy"), isPdf ? "pdf" : "xls");

            LogHelper.SaveHistoryLog("AccessControl.Profiles", new HistoryData { Action = "Exportar", OldData = string.Empty, NewData = JsonConvert.SerializeObject(currentList) });

            return isPdf ? ViewProfilesPdf(currentList, true, fileName) : ViewProfilesExcel(currentList, true, fileName);
        }

        public ActionResult ProfilesHistoryToFile(bool isPdf)
        {
            return this.HistoryToFile(isPdf, "Profiles", "ProfilesHistory", "Perfis", "Perfil Afetado", true);
        }

        public ActionResult CheckIfClientExists(int clientCode)
        {
            IChannelProvider<QX3.Portal.WebSite.CommonService.ICommonContractChannel> channelProvider =
                ChannelProviderFactory<QX3.Portal.WebSite.CommonService.ICommonContractChannel>.Create("CommonClassName");
            var service = channelProvider.GetChannel().CreateChannel();

            service.Open();

            var result = service.LoadClientName(clientCode);

            service.Close();

            return Json(new WcfResponse<bool>() { Result = !String.IsNullOrEmpty(result.Result), Message = result.Result });
        }

        [AccessControlAuthorize]
        public ActionResult UsersImports()
        {

            return View();
        }

        [AccessControlAuthorize]
        [HttpPost]
        public ActionResult UsersImports(FormCollection form)
        {
            try
            {
                IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                string cdClientes = form["cdClientes"].Replace(';', ',').Replace(' ', ',').Replace('|', ',');


                var resCli = service.ListClientsSINACOR(cdClientes);

                List<string> lmsgs = new List<string>();

                foreach (ACUser a in resCli.Result)
                {
                    var login = a.CdCliente;

                    ACUser user = new ACUser();
                    user.ID = 0;
                    user.Name = a.Name;
                    user.Email = a.Email;
                    user.AssessorID = -1;
                    user.ActiveStatus = 1;
                    user.Type = "E";
                    PasswordGenerator pg = new PasswordGenerator();
                    user.Password = pg.EncodePassword(CommonHelper.CreateRandomPassword(9));
                    user.ChaveAD = this.GenerateChaveAD();
                    user.Profile = service.LoadProfile(Convert.ToInt32(565)).Result;
                    user.Login = a.CdCliente.ToString();
                    user.CdCliente = a.CdCliente;
                    user.Blocked = true;

                    string errorMessage = "";
                    AccessControlHelper helper = new AccessControlHelper();

                    if (!helper.EmailIsValid(user.Email, 0))
                        errorMessage += string.Format("E-mail '{0}' já cadastrado. ", a.Email);


                    if (!helper.UsernameIsValid(login.ToString(), 0))
                        errorMessage += "Login já cadastrado.";

                    var response = service.InsertUser(user);

                    if (string.IsNullOrEmpty(errorMessage))
                    {

                        if (response.Result > 0)
                        {
                            var feedbackEmail = SendWelcomeEmail(user.Email, user.Name);

                            LogHelper.SaveHistoryLog("AccessControl.Users", new HistoryData { Action = "Criar Usuário", OldData = string.Empty, NewData = JsonConvert.SerializeObject(user), TargetID = response.Result });
                            lmsgs.Add(string.Format("Cliente {0} - {1}!", user.Login, "Ok"));
                        }
                        else
                        {
                            if (response.Message.Contains("constraint"))
                                lmsgs.Add(string.Format("Cliente {0} - {1}!", user.Login, "Já cadastrado"));
                            else
                                lmsgs.Add(string.Format("Cliente {0} - {1}!", user.Login, response.Message));

                        }

                    }
                    else
                    {
                        lmsgs.Add(string.Format("Cliente {0} - {1}", user.Login, errorMessage));
                    }


                }

                TempData["Msgs"] = lmsgs;

                service.Close();


            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = "Não foi possível efetuar a operação.";
            }


            return View();
        }

        private string SendWelcomeEmail(string email, string name)
        {
            string feedback = string.Empty;

            IChannelProvider<IAuthenticationContractChannel> channelProvider = ChannelProviderFactory<IAuthenticationContractChannel>.Create("AuthenticationClassName");
            var service = channelProvider.GetChannel().CreateChannel();

            service.Open();

            var response = service.CreateGuidToSend(new UserInformation { Email = email });

            if (response.Result)
            {
                if (!service.SendWelcomeEmail(response.Data, email, name).Result)
                    feedback = "Ocorreu um erro ao enviar o e-mail de redefinição de senha.";
            }
            else
                feedback = "Ocorreu um erro ao criar o e-mail de redefinição de senha.";

            service.Close();

            return feedback;
        }

        #region Cadastro de Cliente por Funcionalidade

        [AccessControlAuthorize]
        public ActionResult ClientsRegister()
        {
            return View();
        }

        public ActionResult ClientsRegisterRequestEdit(int? id)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            Session["AccessControl.ClientsRegisterRequestEdit.Emails"] = null;
            Session["AccessControl.ClientsRegisterRequestEdit.Copies"] = null;

            IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
            var service = channelProvider.GetChannel().CreateChannel();

            FuncionalityClient obj = null;

            service.Open();

            if (id.HasValue && !id.Value.Equals(0))
            {
                var filter = new FuncionalityClientFilter()
                {
                    InitRow = 0,
                    FinalRow = 0,
                    SortColumn = 1,
                    SortDirection = "asc",
                    AllRecords = true,
                    ClientCodes = new MarketHelper().GetClientsRangesToFilter(id.Value.ToString())
                };

                obj = service.ListFuncionalityClients(filter).Result.First();

                Session["AccessControl.ClientsRegisterRequestEdit.Emails"] = obj.Funcionalities.SelectMany(p => p.Emails.Select(t => t.Email)).Distinct().ToList();
                Session["AccessControl.ClientsRegisterRequestEdit.Copies"] = obj.Funcionalities.SelectMany(p => p.CopyTo.Select(t => t.Email)).Distinct().ToList();

                var lstAll = service.ListFuncionalities().Result;

                var lstOut = (from c in lstAll
                              where !(from o in obj.Funcionalities select o.FuncId).Contains(c.FuncId)
                              select c).ToList();

                obj.Funcionalities.AddRange(lstOut);
            }
            else
            {
                var lst = service.ListFuncionalities().Result;

                obj = new FuncionalityClient();
                obj.Funcionalities = lst.ToList();
            }

            service.Close();

            return View(obj);
        }

        public ActionResult ClientsDetails(int? id)
        {
            IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
            using (var service = channelProvider.GetChannel().CreateChannel())
            {
                var filter = new FuncionalityClientFilter()
                {
                    InitRow = 0,
                    FinalRow = 10,
                    SortColumn = 1,
                    SortDirection = "asc",
                    AllRecords = true,
                    ClientCodes = new MarketHelper().GetClientsRangesToFilter(id.Value.ToString())
                };

                var response = service.ListFuncionalityClients(filter);

                return View(response.Result.FirstOrDefault());
            }
        }

        [HttpGet]
        public ActionResult ClientsRegisterToFile(bool isPdf)
        {
            try
            {
                List<FuncionalityClient> list = new List<FuncionalityClient>();
                if (Session["AccessControl.ClientsRegister"] != null)
                {
                    list = (List<FuncionalityClient>)Session["AccessControl.ClientsRegister"];

                    //LogHelper.SaveAuditingLog("Subscription.SubscriptionsToFile", string.Format(LogResources.ObjectToFileParameters, isPdf ? "PDF" : "Excel", JsonConvert.SerializeObject(saldo)));
                    string fileName = string.Format("cadastro_cliente_funcionalidade_{0}.{1}", DateTime.Now.ToString("dd-MM-yyyy"), isPdf ? "pdf" : "xls");
                    LogHelper.SaveHistoryLog("AccessControl.ClientsRegister", new HistoryData { Action = "Exportar", OldData = string.Empty, NewData = JsonConvert.SerializeObject(list) });
                    return isPdf ? ViewClientsRegisterPdf(list, true, fileName) : ViewClientsRegisterExcel(list, true, fileName);
                }
                else
                {
                    TempData["ErrorMessage"] = "Ocorreu um erro ao gerar relatório.";
                    return this.RedirectToAction("ClientsRegister");
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = "Ocorreu um erro ao gerar relatório.";
                return this.RedirectToAction("ClientsRegister");
            }
        }

        [HttpPost]
        public string InitialClientsRegisterFilter(FormCollection form)
        {
            IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
            using (var service = channelProvider.GetChannel().CreateChannel())
            {
                var assessor = string.Empty;
                if (SessionHelper.CurrentUser.Assessors != null)
                    foreach (int i in SessionHelper.CurrentUser.Assessors)
                        assessor += i.ToString() + ";";

                var filter = new FuncionalityClientFilter()
                {
                    InitRow = Convert.ToInt32(form["iDisplayStart"]),
                    FinalRow = Convert.ToInt32(form["iDisplayLength"]),
                    SortColumn = (form["iSortCol_0"] == "null" || form["iSortCol_0"] == "undefined" || form["iSortCol_0"] == "0") ? 1 : Convert.ToInt32(form["iSortCol_0"]),
                    SortDirection = form["sSortDir_0"],
                    AllRecords = false,
                    Assessors = assessor,
                    ClientCodes = string.Empty,
                    UserID = CommonHelper.GetLoggedUserGroupId().Value
                };

                var response = service.ListFuncionalityClients(filter);

                filter.AllRecords = true;

                Session["AccessControl.ClientsRegister"] = service.ListFuncionalityClients(filter).Result.ToList();


                return new AccessControlHelper().ClientsRegisterReportListObject((response.Result) != null ? response.Result.ToList() : new List<FuncionalityClient>(), ref form, response.Data);
            }
        }

        public string ClientsRegisterFilter(FormCollection form)
        {
            IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
            using (var service = channelProvider.GetChannel().CreateChannel())
            {
                var assessor = string.Empty;
                if (SessionHelper.CurrentUser.Assessors != null)
                    foreach (int i in SessionHelper.CurrentUser.Assessors)
                        assessor += i.ToString() + ";";

                var filter = new FuncionalityClientFilter()
                {
                    InitRow = Convert.ToInt32(form["iDisplayStart"]),
                    FinalRow = Convert.ToInt32(form["iDisplayLength"]),
                    SortColumn = (form["iSortCol_0"] == "null" || form["iSortCol_0"] == "undefined" || form["iSortCol_0"] == "0") ? 1 : Convert.ToInt32(form["iSortCol_0"]),
                    SortDirection = form["sSortDir_0"],
                    AllRecords = false,
                    Assessors = assessor,
                    ClientCodes = string.Empty,
                    UserID = CommonHelper.GetLoggedUserGroupId().Value
                };

                MarketHelper helper = new MarketHelper();

                if (!string.IsNullOrEmpty(form["txtClientCode"].ToString()))
                    filter.ClientCodes = helper.GetClientsRangesToFilter(form["txtClientCode"].ToString());
                if (!string.IsNullOrEmpty(form["ddlFormat"].ToString()))
                    filter.SendFormat = form["ddlFormat"].ToString() == "T" ? null : form["ddlFormat"].ToString();

                var response = service.ListFuncionalityClients(filter);

                filter.AllRecords = true;

                Session["AccessControl.ClientsRegister"] = service.ListFuncionalityClients(filter).Result.ToList();

                return new AccessControlHelper().ClientsRegisterReportListObject((response.Result) != null ? response.Result.ToList() : new List<FuncionalityClient>(), ref form, response.Result.Count());
            }
        }

        public ActionResult ConfirmClientRegisterDelete(int? id)
        {
            IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
            using (var service = channelProvider.GetChannel().CreateChannel())
            {
                var filter = new FuncionalityClientFilter()
                {
                    InitRow = 0,
                    FinalRow = 10,
                    SortColumn = 1,
                    SortDirection = "asc",
                    AllRecords = true,
                    ClientCodes = new MarketHelper().GetClientsRangesToFilter(id.Value.ToString())
                };

                var response = service.ListFuncionalityClients(filter);

                return View(response.Result.FirstOrDefault());
            }
        }

        [HttpPost]
        public ActionResult ClientRegisterDelete(int hdfClientCode, string hdfClientName)
        {

            IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
            using (var service = channelProvider.GetChannel().CreateChannel())
            {
                var obj = service.ListFuncionalityClients(new FuncionalityClientFilter()
                {
                    InitRow = 0,
                    FinalRow = 10,
                    SortColumn = 1,
                    SortDirection = "asc",
                    AllRecords = true,
                    ClientCodes = hdfClientCode.ToString()
                }).Result.FirstOrDefault();

                var request = new FuncionalityClient()
                {
                    ClientCode = hdfClientCode
                };

                var response = service.DeleteClientForFuncionality(request);

                if (response.Result)
                {
                    response.Message = "Cliente excluído com sucesso.";
                    LogHelper.SaveHistoryLog("AccessControl.ClientsRegister", new HistoryData()
                    {
                        Action = "Excluir",
                        ID = hdfClientCode,
                        Target = hdfClientName,
                        OldData = JsonConvert.SerializeObject(obj)
                    });
                }
                else response.Message = "Não foi possível excluir o cliente. " + response.Message;

                return Json(response);
            }
        }

        public ActionResult ClientsRegisterHistory()
        {
            List<SelectListItem> listItems = new List<SelectListItem>();
            listItems.Add(new SelectListItem { Text = "Todas", Value = "", Selected = true });
            listItems.Add(new SelectListItem { Text = "Adicionar", Value = "Adicionar", Selected = false });
            listItems.Add(new SelectListItem { Text = "Editar", Value = "Editar", Selected = false });
            listItems.Add(new SelectListItem { Text = "Excluir", Value = "Excluir", Selected = false });
            listItems.Add(new SelectListItem { Text = "Exportar", Value = "Exportar", Selected = false });

            try
            {
                ViewBag.ClientsRegisterHistoryActions = listItems;

                LogHelper.SaveAuditingLog("AccessControl.ClientsRegisterHistory");
                return View();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = "Não foi possível carregar o histórico.";
                return View();
            }
        }

        [HttpPost]
        public void LoadClientsRegisterHistory(FormCollection form)
        {
            try
            {
                string startDate = form["txtStartDate"];
                string endDate = form["txtEndDate"];

                long responsibleID = string.IsNullOrEmpty(form["hdfResponsible"]) ? 0 : long.Parse(form["hdfResponsible"]);
                int clientID = string.IsNullOrEmpty(form["txtClient"]) ? 0 : Int32.Parse(form["txtClient"]);

                int id = 0;
                Int32.TryParse(form["hdfRequestID"], out id);

                string action = form["ddlHistoryActions"];

                LogHelper.SaveAuditingLog("AccessControl.ClientsRegisterHistory",
                    string.Format(LogResources.AuditingLoadHistory, clientID, responsibleID, action, startDate, endDate));

                var history = new List<HistoryData>();

                HistoryFilter filterClientsRegister = new HistoryFilter
                {
                    ModuleName = "AccessControl.ClientsRegister",
                    StartDate = startDate,
                    EndDate = endDate,
                    ResponsibleID = responsibleID,
                    ID = clientID,
                    Action = action
                };

                history.AddRange(LogHelper.LoadHistory(filterClientsRegister));
                Session["AccessControl.ClientsRegisterHistoryList"] = history;
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }
        }

        public ActionResult AccessControl_ClientsRegisterHistory(FormCollection form)
        {
            List<HistoryData> items = (List<HistoryData>)Session["AccessControl.ClientsRegisterHistoryList"];

            return new DataTablesController().AccessControl_ClientsRegisterHistory(form, items);
        }

        public ActionResult ClientsRegisterHistoryToFile(bool isPdf)
        {
            return this.HistoryToFile(isPdf
                , "AccessControl"
                , "Cadastro de Cliente por Funcionalidade"
                , "Cliente"
                , true
                , "AccessControl.ClientsRegisterHistoryList");
        }

        public ActionResult HistoryToFile(bool isPdf
            , string clientName
            , string targetTitle
            , string targetColumnTitle
            , bool hasAction
            , string sessionName
            , string extraTargetColumnTitle = null)
        {
            List<HistoryData> currentList = new List<HistoryData>();

            try
            {
                if (Session[sessionName] != null)
                    currentList = (List<HistoryData>)Session[sessionName];


                string name = new Regex(@"\s*").Replace(HttpUtility.HtmlDecode(targetTitle), string.Empty);
                targetTitle = string.Concat(isPdf ? "Histórico de " : "Hist&#243;rico de ", targetTitle);

                string fileName = string.Format("historico_{0}_{1}.{2}", name, DateTime.Now.ToString("dd-MM-yyyy"), isPdf ? "pdf" : "xls");

                return isPdf ? ViewHistoryPdf(currentList, true, fileName, targetTitle, targetColumnTitle, hasAction, extraTargetColumnTitle)
                    : ViewHistoryExcel(currentList, true, fileName, targetTitle, targetColumnTitle, hasAction, extraTargetColumnTitle);

            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return this.RedirectToAction("Index");
            }

        }

        public JsonResult EmailSuggest(string term, string currentList, string isCopy)
        {
            string listName = isCopy.Equals("1") ? "AccessControl.ClientsRegisterRequestEdit.Copies" : "AccessControl.ClientsRegisterRequestEdit.Emails";
            List<string> lst = Session[listName] != null ? (List<string>)Session[listName] : new List<string>();
            List<string> result = new List<string>();

            try
            {
                if (lst.Count() > 0)
                    result = lst.Where(i => i.Contains(term.ToLower())).ToList();

                if (!string.IsNullOrEmpty(currentList))
                {
                    string[] arr = currentList.Split(';');
                    if (arr.Count() > 0)
                        result = result.Where(i => !arr.Contains(i)).ToList();
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AddEmailToSuggest(string term, string isCopy)
        {
            try
            {
                string listName = isCopy.Equals("1") ? "AccessControl.ClientsRegisterRequestEdit.Copies" : "AccessControl.ClientsRegisterRequestEdit.Emails";
                List<string> lst = Session[listName] != null ? (List<string>)Session[listName] : new List<string>();

                if (!lst.Exists(delegate(string e) { return e.Equals(term.ToLower()); }))
                    lst.Add(term.ToLower());

                Session[listName] = lst;

                return Json(new { Error = "" });
            }
            catch (Exception)
            {

                return Json(new { Error = "Não foi possível adicionar o e-mail." });
            }
        }

        public JsonResult RemoveEmailFromSuggest(string term, string isCopy)
        {
            try
            {
                string listName = isCopy.Equals("1") ? "AccessControl.ClientsRegisterRequestEdit.Copies" : "AccessControl.ClientsRegisterRequestEdit.Emails";
                List<string> lst = Session[listName] != null ? (List<string>)Session[listName] : new List<string>();

                if (lst.Count > 0)
                    if (lst.Exists(delegate(string e) { return e.Equals(term); }))
                    {
                        lst.Remove(term);
                        Session[listName] = lst;
                    }

                return Json(new { Error = "" });
            }
            catch (Exception)
            {

                return Json(new { Error = "Não foi possível remover o e-mail." });
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ClientsRegisterRequestEdit(string hdnIsUpdate, string hdnClientName, string txtClientCode, string[] hdnFuncionality, string[] hdnEmail, string[] hdnCopy, string[] hdnFormat)
        {
            bool isUpdate = hdnIsUpdate.Equals("1") ? true : false;

            try
            {
                LogManager.InitLog("AccessControl.ClientsRegisterRequestEdit");
                LogManager.WriteLog("");

                IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                FuncionalityClient obj = new FuncionalityClient();
                obj.ClientCode = Convert.ToInt32(txtClientCode);

                List<Funcionality> lstFuncionalities = new List<Funcionality>();

                for (int i = 0; i < hdnFuncionality.Length; i++)
                {
                    Funcionality f = new Funcionality();
                    f.FuncId = Convert.ToInt32(hdnFuncionality[i]);
                    f.SendFormat = hdnFormat[i];

                    #region Emails
                    List<ClientEmail> lstEmails = new List<ClientEmail>();

                    string emails = hdnEmail[i];
                    if (!string.IsNullOrEmpty(emails))
                    {
                        var arrEmails = emails.Split(';');
                        if (arrEmails.Count() > 0)
                            foreach (var item in arrEmails)
                                if (!string.IsNullOrEmpty(item))
                                {
                                    ClientEmail e = new ClientEmail();
                                    e.Email = item.ToLower();
                                    lstEmails.Add(e);
                                }
                    }

                    f.Emails = lstEmails;
                    #endregion

                    #region Copies
                    List<ClientEmail> lstCopies = new List<ClientEmail>();

                    string copies = hdnCopy[i];
                    if (!string.IsNullOrEmpty(copies))
                    {
                        var arrCopies = copies.Split(';');
                        if (arrCopies.Count() > 0)
                            foreach (var item in arrCopies)
                                if (!string.IsNullOrEmpty(item))
                                {
                                    ClientEmail e = new ClientEmail();
                                    e.Email = item.ToLower();
                                    lstCopies.Add(e);
                                }
                    }

                    f.CopyTo = lstCopies;
                    #endregion

                    lstFuncionalities.Add(f);
                }

                obj.Funcionalities = lstFuncionalities;

                service.Open();

                var result = isUpdate ? service.UpdateClientForFuncionality(obj).Result : service.InsertClientForFuncionality(obj).Result;

                service.Close();

                if (result)
                    LogHelper.SaveHistoryLog("AccessControl.ClientsRegister", new HistoryData()
                    {
                        Action = isUpdate ? "Editar" : "Adicionar",
                        ID = obj.ClientCode,
                        Target = hdnClientName,
                        OldData = JsonConvert.SerializeObject(obj)
                    });

                return Json(new { Error = result ? "" : string.Format("Não foi possível {0} o cliente.", isUpdate ? "alterar" : "adicionar") });
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return Json(new { Error = string.Format("Não foi possível {0} o cliente.", isUpdate ? "alterar" : "adicionar") });
            }
        }

        public ActionResult ValidateClientRegisterExists(string clientCode)
        {

            try
            {
                IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                var filter = new FuncionalityClientFilter()
                {
                    InitRow = 0,
                    FinalRow = 0,
                    SortColumn = 1,
                    SortDirection = "asc",
                    AllRecords = true,
                    ClientCodes = new MarketHelper().GetClientsRangesToFilter(clientCode.Trim())
                };

                FuncionalityClient obj = service.ListFuncionalityClients(filter).Result.First();

                service.Close();

                return Json(new { Result = obj != null ? "true" : "false" });
            }
            catch (Exception)
            {
                return Json(new { Result = "false" });
            }

        }
        #endregion
    }
}
