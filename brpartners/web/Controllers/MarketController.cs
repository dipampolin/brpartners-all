﻿using System;
using System.Web;
using System.Web.Mvc;
using QX3.Portal.WebSite.Helper;
using QX3.Spinnex.Common.Services.Logging;
using QX3.Portal.Contracts.DataContracts;
using System.Collections.Generic;
using System.Linq;
using QX3.Portal.WebSite.MarketService;
using QX3.Portal.WebSite.Models.Channel;
using QX3.Portal.WebSite.Properties;
using QX3.Portal.WebSite.Models;

namespace QX3.Portal.WebSite.Controllers
{
    public class MarketController : Controller
    {
        public JsonResult StocksAutoSuggest(string term, string companyName)
        {
            List<Stock> stocks = new List<Stock>();

            try
            {
                MarketHelper helper = new MarketHelper();

                stocks = helper.LoadStocks();
                stocks = (from s in stocks
                          where
                              s.Symbol.ToLower().Contains(term.ToLower())
                              && (string.IsNullOrEmpty(companyName) || s.CompanyName.Equals(companyName))
                          select s).ToList();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }
            return Json(stocks, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LoadStockExchanges(int clientCode)
        {
            StockExchanges exchanges = new StockExchanges();

            try
            {
                IChannelProvider<IMarketContractChannel> channelProvider = ChannelProviderFactory<IMarketContractChannel>.Create("MarketClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                exchanges = service.LoadClientExchanges(clientCode).Result ?? new StockExchanges();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }
            return Json(exchanges, JsonRequestBehavior.AllowGet);
        }


        public JsonResult CompaniesAutoSuggest(string term)
        {
            List<string> companies = new List<string>();

            try
            {
                MarketHelper helper = new MarketHelper();

                var stocks = helper.LoadStocks();

                companies = (from s in stocks
                             where s.CompanyName.ToLower().Contains(term.ToLower())
                             select s.CompanyName).Distinct().ToList();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }
            return Json(companies, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetAssessorName(int code)
        {
            try
            {

                IChannelProvider<IMarketContractChannel> channelProvider = ChannelProviderFactory<IMarketContractChannel>.Create("MarketClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                var response = service.LoadAssessorName(code);

                service.Close();

                if (String.IsNullOrEmpty(response.Result))
                    response.Result = Resources.AbsentAssessorName;

                return Json(response.Result);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return Json(string.Empty);
            }
        }

        [HttpPost]
        public ActionResult GetNextBusinessDay(int numberOfDaysToAdd, string targetDate)
        {
            DateTime businessDay = DateTime.Now.AddDays(numberOfDaysToAdd);
            try
            {
                if (string.IsNullOrEmpty(targetDate))
                    targetDate = DateTime.Now.ToString("dd/MM/yyyy");

                IChannelProvider<IMarketContractChannel> channelProvider = ChannelProviderFactory<IMarketContractChannel>.Create("MarketClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();
                var response = service.GetNextBusinessDay(numberOfDaysToAdd, targetDate).Result;
                service.Close();

                DateTime.TryParse(response, out businessDay);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return Json(string.Empty);
            }
            return Json(businessDay.ToString("dd/MM/yyyy"));
        }

        [HttpPost]
        public ActionResult GetPendents(string moduleName)
        {
            int pendents = 0;

            try
            {
                pendents = CommonHelper.GetPendents(moduleName);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return Json(string.Empty);
            }
            return Json(pendents);
        }
    }
}
