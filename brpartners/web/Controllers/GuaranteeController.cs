﻿using System;
using System.Web.Mvc;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.WebSite.GuaranteeService;
using QX3.Portal.WebSite.Helper;
using QX3.Portal.WebSite.Models;
using QX3.Portal.WebSite.Models.Channel;
using QX3.Spinnex.Common.Services.Logging;

namespace QX3.Portal.WebSite.Controllers
{
    public class GuaranteeController : FileController
    {
		[AccessControlAuthorize]
		public ActionResult Guarantee()
		{
			return View();
		}

		public ActionResult GuaranteeFilter(FormCollection form)
		{
			using (var service = ChannelProviderFactory<IGuaranteeContractChannel>.Create("GuaranteeClassName").GetChannel().CreateChannel())
			{
				GuaranteeClientFilter filter = new GuaranteeClientFilter();

				if (!string.IsNullOrEmpty(form["txtAssessorFilter"]))
				    filter.AssessorFilterText = new MarketHelper().GetAssessorsRangesToFilter(form["txtAssessorFilter"]);

				if (!string.IsNullOrEmpty(form["txtClientFilter"]))
					filter.ClientFilterText = new MarketHelper().GetClientsRangesToFilter(form["txtClientFilter"]);

				if (!String.IsNullOrEmpty(form["radActivitType"]))
					filter.ActivitType = Convert.ToInt16(form["radActivitType"]);
				else
					filter.ActivitType = 1;

				if (!String.IsNullOrEmpty(form["acao"]))
					if (form["acao"].Equals("anterior"))
						filter.Position = (int)Session["Guarantee.GuaranteeFilter.Posicao"] - 1;
					else if (form["acao"].Equals("proximo"))
						filter.Position = (int)Session["Guarantee.GuaranteeFilter.Posicao"] + 1;
				
				int pos = 0;
				var result = service.ListGuarantees(out pos, filter, CommonHelper.GetLoggedUserGroupId());
				Session["Guarantee.GuaranteeFilter.Posicao"] = pos;
				Session["Guarantee.GuaranteeClient"] = result.Result;
				var count = result.Data;

				ViewBag.ClientCounter = string.Format("Exibindo {0} de {1}", pos, count);

				#region Navegação nos clientes

				if (count == 1)
				{
					ViewBag.HasAnterior = false;
					ViewBag.HasPosterior = false;
				}
				else if (count > 1)
				{
					if (pos == 1)
						ViewBag.HasAnterior = false;
					else if (pos > 1)
						ViewBag.HasAnterior = true;

					if (pos < count)
						ViewBag.HasPosterior = true;
					else if (pos == count)
						ViewBag.HasPosterior = false;
				}
				else
				{
					ViewBag.HasAnterior = false;
					ViewBag.HasPosterior = false;
				}

				#endregion

				return PartialView("GuaranteeActivities", result.Result);
			}
		}

		public ActionResult GuaranteeToFile(bool isPdf)
		{
			try
			{
				if (Session["Guarantee.GuaranteeClient"] == null)
					throw new Exception();

				GuaranteeClient guarantee = (GuaranteeClient)Session["Guarantee.GuaranteeClient"];
				string fileName = string.Format("garantia_cliente_{0}.{1}", DateTime.Now.ToString("dd-MM-yyyy"), isPdf ? "pdf" : "xls");
				return isPdf ? ViewGuaranteePdf(guarantee, true, fileName) : ViewGuaranteeExcel(guarantee, true, fileName);
			}
			catch (Exception ex)
			{
				LogManager.WriteError(ex);
				TempData["ErrorMessage"] = "Ocorreu um erro ao gerar relatório.";
				return this.RedirectToAction("Guarantee");
			}
		}
	}
}
