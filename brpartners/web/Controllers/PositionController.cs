﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.WebSite.Helper;
using QX3.Portal.WebSite.Models.Channel;
using QX3.Portal.WebSite.PositionService;

namespace QX3.Portal.WebSite.Controllers
{
    public class PositionController : FileController
    {
        #region POSIÇÃO CONSOLIDADA

        [AccessControlAuthorize]
        [HttpGet]
        public ActionResult ConsolidatedPosition() {

            ConsolidatedPositionResponse2 cpr = new ConsolidatedPositionResponse2();
            List<ConsolidatedPosition2> lcp = new List<ConsolidatedPosition2>();


            var user = SessionHelper.CurrentUser;
            int cdCliente = user.CdBolsa;

            cpr.CdBolsa = cdCliente;
            cpr.TipoUsuario = user.UserType;
            try {

                IChannelProvider<IPositionContractChannel> channelProvider = ChannelProviderFactory<IPositionContractChannel>.Create("PositionClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                cpr.ConsolidatedPosition = service.ListConsolidatedPosition(cdCliente).Result.ConsolidatedPosition;
                Session["Position.ConsolidatedPosition"] = cpr;

                service.Close();
            }
            catch {
                return Content(FeedbackErrorModel(string.Format("Falha ao carregar a posição do cliente {0}.", cdCliente)));
            }

            return View(cpr);
        }

        [AccessControlAuthorize]
        [HttpPost]
        public ActionResult ConsolidatedPosition(FormCollection form) {

            //Classe para receber a resposta do servico
            ConsolidatedPositionResponse2 cpr = new ConsolidatedPositionResponse2();

            var user = SessionHelper.CurrentUser;
            int cdCliente = user.UserType == "I" ? Convert.ToInt32(form["cdCliente"]) : user.CdBolsa;

            cpr.CdBolsa = cdCliente;
            cpr.TipoUsuario = user.UserType;

            try {

                IChannelProvider<IPositionContractChannel> channelProvider = ChannelProviderFactory<IPositionContractChannel>.Create("PositionClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                cpr.ConsolidatedPosition = service.ListConsolidatedPosition(cdCliente).Result.ConsolidatedPosition;
                Session["Position.ConsolidatedPosition"] = cpr;

                service.Close();
            }
            catch {
                return Content(FeedbackErrorModel(string.Format("Falha ao carregar a posição do cliente {0}.", cdCliente)));
            }

            return View(cpr);
        }

        public ActionResult ConsolidatedPositionToFile(bool isPdf) {

            List<ConsolidatedPositionDetails> lcpd = new List<ConsolidatedPositionDetails>();

            var currentPosition = Session["Position.ConsolidatedPosition"] == null ? new ConsolidatedPositionResponse2() : (ConsolidatedPositionResponse2)Session["Position.ConsolidatedPosition"];

            var cdCliente = currentPosition.CdBolsa;


            foreach (var i in currentPosition.ConsolidatedPosition) {

                IChannelProvider<IPositionContractChannel> channelProvider = ChannelProviderFactory<IPositionContractChannel>.Create("PositionClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                lcpd.AddRange(service.ListConsolidatedPositionDetails(cdCliente, i.TipoMercado).Result);

                service.Close();

            }


            string fileName = string.Format("PosicaoConsolidada_{0}_{1}.{2}", cdCliente, DateTime.Now.ToString("ddMMyyyymmss"), isPdf ? "pdf" : "xls");

            //return isPdf ? ConsolidatedPositionPdf(currentPosition, lcpd, true, fileName) : ConsolidatedPositionExcel(currentPosition, lcpd, true, fileName);

            return ConsolidatedPositionPdf(currentPosition, lcpd, true, fileName);
        }

        public JsonResult ConsolidatedPositionDetails(string id) {
            List<ConsolidatedPositionDetails> lcp = new List<ConsolidatedPositionDetails>();


            string[] key = id.Split('_');

            try {

                IChannelProvider<IPositionContractChannel> channelProvider = ChannelProviderFactory<IPositionContractChannel>.Create("PositionClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                lcp = service.ListConsolidatedPositionDetails(Convert.ToInt32(key[1]), key[0]).Result;

                service.Close();
            }
            catch {

            }

            return Json(lcp, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region POSIÇÃO DE CUSTÓDIA
        [AccessControlAuthorize]
        [HttpGet]
        public ActionResult CustodyPosition() {

            CustodyPositionResponse lcp = new CustodyPositionResponse();

            var user = SessionHelper.CurrentUser;
            int cdCliente = user.CdBolsa;
            string cdMercado = "TDO";

            lcp.CdBolsa = cdCliente;
            lcp.TipoMercado = cdMercado;
            lcp.TipoUsuario = user.UserType;


            try {

                IChannelProvider<IPositionContractChannel> channelProvider = ChannelProviderFactory<IPositionContractChannel>.Create("PositionClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                lcp.CustodyPosition = service.ListCustodyPosition(cdCliente, cdMercado).Result.CustodyPosition;
                ViewBag.ValorTotal = (from x in lcp.CustodyPosition select x.ValorAtual).Sum();

                Session["Position.CustodyPosition"] = lcp;

                service.Close();
            }
            catch {
                return Content(FeedbackErrorModel(string.Format("Falha ao carregar a posição do cliente {0}.", cdCliente)));
            }

            return View(lcp);
        }

        [HttpPost]
        public ActionResult CustodyPosition(FormCollection form) {

            CustodyPositionResponse cpr = new CustodyPositionResponse();
            List<CustodyPosition> lcp = new List<CustodyPosition>();

            var user = SessionHelper.CurrentUser;

            int cdCliente = user.UserType == "I" ? Convert.ToInt32(form["cdCliente"]) : user.CdBolsa;

            string[] cdMercado = form["cdMercado"].Split('-')[0].Split(',');
            string descMercado = form["cdMercado"].Split('-')[1];

            cpr.CdBolsa = cdCliente;
            cpr.TipoMercado = form["cdMercado"];
            cpr.DescTipoMercado = descMercado;
            cpr.TipoUsuario = user.UserType;

            try {

                IChannelProvider<IPositionContractChannel> channelProvider = ChannelProviderFactory<IPositionContractChannel>.Create("PositionClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                //Refazer
                foreach (var m in cdMercado) {
                    lcp.AddRange(service.ListCustodyPosition(cdCliente, m).Result.CustodyPosition);
                }

                ViewBag.ValorTotal = (from x in lcp select x.ValorAtual).Sum();
                cpr.CustodyPosition = lcp;

                Session["Position.CustodyPosition"] = cpr;

                service.Close();
            }
            catch {
                return Content(FeedbackErrorModel(string.Format("Falha ao carregar a posição do cliente {0}.", cdCliente)));
            }

            return View(cpr);
        }

        public ActionResult CustodyPositionToFile(bool isPdf) {
            
            var currentPosition = Session["Position.CustodyPosition"] == null ? new CustodyPositionResponse() : (CustodyPositionResponse)Session["Position.CustodyPosition"];
            var cdCliente = currentPosition.CdBolsa;

            string fileName = string.Format("PosicaoCustodia_{0}_{1}.{2}", cdCliente, DateTime.Now.ToString("ddMMyyyymmss"), isPdf ? "pdf" : "xls");

            return CustodyPositionExcel(currentPosition, true, fileName, "");
            //return isPdf ? ConsolidatedPositionPdf(currentPosition, lcpd, true, fileName) : ConsolidatedPositionExcel(currentPosition, lcpd, true, fileName);
        }
        #endregion
        
        #region POSIÇÃO FINANCEIRA

        [AccessControlAuthorize]
        [HttpGet]
        public ActionResult FinancialPosition() {

            FinancialPositionResponse fpr = new FinancialPositionResponse();

            var user = SessionHelper.CurrentUser;
            int cdCliente = user.CdBolsa;

            fpr.CdBolsa = cdCliente;
            fpr.TipoUsuario = user.UserType;

            try {

                IChannelProvider<IPositionContractChannel> channelProvider = ChannelProviderFactory<IPositionContractChannel>.Create("PositionClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                fpr.FinancialPosition = service.FinancialPosition(cdCliente).Result.FinancialPosition;
                Session["Position.FinancialPosition"] = fpr;

                service.Close();
            }
            catch {
                return Content(FeedbackErrorModel(string.Format("Falha ao carregar a posição do cliente {0}.", cdCliente)));
            }

            return View(fpr);
        }

        [HttpPost]
        public ActionResult FinancialPosition(FormCollection form) {

            FinancialPositionResponse fpr = new FinancialPositionResponse();

            var user = SessionHelper.CurrentUser;
            int cdCliente = user.UserType == "I" ? Convert.ToInt32(form["cdCliente"]) : user.CdBolsa;

            fpr.CdBolsa = cdCliente;
            fpr.TipoUsuario = user.UserType;

            try {

                IChannelProvider<IPositionContractChannel> channelProvider = ChannelProviderFactory<IPositionContractChannel>.Create("PositionClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                fpr.FinancialPosition = service.FinancialPosition(cdCliente).Result.FinancialPosition;
                Session["Position.FinancialPosition"] = fpr;

                service.Close();
            }
            catch {
                return Content(FeedbackErrorModel(string.Format("Falha ao carregar a posição do cliente {0}.", cdCliente)));
            }

            return View(fpr);
        }

        public ActionResult FinancialPositionToFile(bool isPdf) {
            var currentPosition = Session["Position.FinancialPosition"] == null ? new FinancialPositionResponse() : (FinancialPositionResponse)Session["Position.FinancialPosition"];
            var cdCliente = currentPosition.CdBolsa;

            if (currentPosition.FinancialPosition == null || currentPosition.FinancialPosition.Disponivel == null)
            {
                return Redirect("/Position/FinancialPosition");
            }


            string fileName = string.Format("PosicaoFinanceira_{0}_{1}.{2}", cdCliente, DateTime.Now.ToString("ddMMyyyymmss"), isPdf ? "pdf" : "xls");

            return FinancialPositionExcel(currentPosition, true, fileName);
            //return isPdf ? ConsolidatedPositionPdf(currentPosition, lcpd, true, fileName) : ConsolidatedPositionExcel(currentPosition, lcpd, true, fileName);
        }
        
        #endregion

        private string FeedbackErrorModel(string message) {

            return string.Format("<div id='feedbackError' class='erro' style='display:block;'><span>{0}</span></div>", message);
        }

        // SP_REN_EXTRATO

        [AccessControlAuthorize]
        [HttpGet]
        public ActionResult ExtratoRenda(FormCollection form)
        {

            //ExtratoRenda cpr = new ExtratoRenda();
            List <ExtratoRendaDatails> sr = new List<ExtratoRendaDatails>();

            var TpCodEmpresa = form["TpCodEmpresa"];
            var CpfCnpj = form["CpfCnpj"];
            var DtRefDe = form["DtRefDe"];
            var DtRefAte = form["DtRefAte"];
            try
            {

                IChannelProvider<IPositionContractChannel> channelProvider = ChannelProviderFactory<IPositionContractChannel>.Create("PositionClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();
                var response = service.ListExtratoRendaDatails(TpCodEmpresa, CpfCnpj, DtRefDe, DtRefAte);
                sr = response.Result;

                if (!string.IsNullOrEmpty(response.Message))
                    throw new Exception(response.Message);


                service.Close();
            }
            catch (Exception ex)
            {
                return Content(FeedbackErrorModel(string.Format("Falha ao carregar ExtratoRenda")));
            }

            return View(sr);

        }

        public ActionResult ExtratoRenda()
        {

            List<ExtratoRendaDatails> sr = new List<ExtratoRendaDatails>();

            string TpCodEmpresa = null;
            string CpfCnpj = null;
            string DtRefDe = null;
            string DtRefAte = null;


            try
            {

                IChannelProvider<IPositionContractChannel> channelProvider = ChannelProviderFactory<IPositionContractChannel>.Create("PositionClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();
                var response = service.ListExtratoRendaDatails(TpCodEmpresa, CpfCnpj, DtRefDe, DtRefAte);
                sr = response.Result;

                if (!string.IsNullOrEmpty(response.Message))
                    throw new Exception(response.Message);


                service.Close();
            }
            catch
            {
                return Content(FeedbackErrorModel(string.Format("Falha ao carregar ExtratoRenda")));
            }

            return View(sr);
        }

    }
}
