﻿//using SelectPdf;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.WebSite.Helper;
using QX3.Portal.WebSite.Models;
using QX3.Portal.WebSite.Models.Channel;
using QX3.Portal.WebSite.SuitabilityService;
using QX3.Portal.WebSite.Util;
using QX3.Spinnex.Common.Services.Logging;
using Rotativa;
using Rotativa.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace QX3.Portal.WebSite.Controllers
{
    public class ComplianceController : BaseController
    {
        #region Private Methods

        private void GetMismatch(SuitabilityPerfilCliente[] obj)
        {
            if (obj != null && obj.Length > 0)
                GetMismatch(obj.ToList());
        }

        private void GetMismatch(List<SuitabilityPerfilCliente> obj)
        {
            if (obj != null && obj.Count > 0)
            {
                obj.ForEach(f =>
                {
                    if (f.CnpjCpf != null)
                    {
                        f.CnpjCpf = f.CnpjCpf.Length <= 11 ? FormatCPF(f.CnpjCpf) : FormatCNPJ(f.CnpjCpf);
                    }

                    var unframed = VerifyMismatch(f.IdCliente, null, null);
                    if (unframed != null && unframed.Length > 0)
                    {
                        foreach (var item in unframed)
                        {
                            string d = Convert.ToDateTime(f.DataCadastro).ToString("dd/MM/yyyy");

                            if (item.CompletionDate.ToString("dd/MM/yyyy") == d)
                            {
                                List<string> list = new List<string>();

                                foreach (var u in item.Unframeds)
                                    list.Add(string.Format("{0} - {1}", u.CdMercado, u.DtDatOrd.ToString("dd/MM/yyyy")));

                                f.Desenquadramento = String.Join(", ", list.ToArray());
                            }
                        }
                    }

                });
            }
        }

        private SuitabilityUnframedResponse[] VerifyMismatch(int clientId, DateTime? start, DateTime? end)
        {
            SuitabilityUnframedResponse[] unframed = null;

            IChannelProvider<ISuitabilityContractChannel> channelProvider = ChannelProviderFactory<ISuitabilityContractChannel>.Create("SuitabilityClassName");
            var service = channelProvider.GetChannel().CreateChannel();

            service.Open();

            var response = service.VerificaNovoDesenquadramento(clientId, start, end);

            unframed = response.Result;

            service.Close();

            return unframed;
        }

        private static void DownloadPdf(HttpResponseBase Response, byte[] pdfByte)
        {
            Response.Clear();
            MemoryStream ms = new MemoryStream(pdfByte);
            ms.WriteTo(Response.OutputStream);
            Response.End();
        }

        private void AddResponseHeader(HttpResponseBase Response, string fileName)
        {
            Response.ContentType = "application/pdf";
            Response.ContentEncoding = Encoding.UTF8;
            Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
            Response.Buffer = true;
        }

        private static string FormatCNPJ(string CNPJ)
        {
            return Convert.ToUInt64(CNPJ).ToString(@"00\.000\.000\/0000\-00");
        }

        private static string FormatCPF(string CPF)
        {
            return Convert.ToUInt64(CPF).ToString(@"000\.000\.000\-00");
        }

        private string RenderRazorViewToString(string viewName, object model)
        {
            ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext,viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View,ViewData, TempData, sw);

                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);

                return sw.GetStringBuilder().ToString();
            }
        }

        #endregion

        [AccessControlAuthorize]
        [HttpGet]
        public ActionResult Suitability()
        {
            SuitabilityClientsResponse sr = new SuitabilityClientsResponse();
            sr.CdBolsa = "";

            return View(sr);
        }

        [AccessControlAuthorize]
        [HttpPost]
        public ActionResult Suitability(FormCollection form)
        {
            SuitabilityClientsResponse sr = new SuitabilityClientsResponse();

            try
            {
                LogHelper.SaveAuditingLog("Suitability.Suitability");

                var cdCliente = sr.CdBolsa = form["cdCliente2"];
                var investorType = sr.InvestorType = form["perfil"];
                var status = sr.Status = form["status"];
                var unframed = sr.Unframed = form["cbDes"];
                var vencimento = form["vencimento"];
                var investorKind = form["investorKind"];
                var orderBy = form["hdnOrderBy"];

                string sCpfCnpj = Request["cpfcnpj"];
                long? cpfcnpj = null;
                if (!string.IsNullOrWhiteSpace(sCpfCnpj))
                {
                    sCpfCnpj = Regex.Replace(sCpfCnpj, @"[^\d]", "");
                    try
                    {
                        cpfcnpj = Int64.Parse(sCpfCnpj);
                    }
                    catch { }
                }

                IChannelProvider<ISuitabilityContractChannel> channelProvider = ChannelProviderFactory<ISuitabilityContractChannel>.Create("SuitabilityClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();
                var response = service.SelecionaClientesSuitability(null, cpfcnpj, cdCliente, status, investorType, null);

                List<SuitabilityClients> clients = new List<SuitabilityClients>();

                response.Result.Clients.ForEach(c =>
                {
                    var list = QX3.Portal.WebSite.Controllers.ComplianceController.HistoricoCliente(c.IdCliente, null).Where(d => d.CompletionDate != DateTime.MinValue);
                    if (list.Count() > 0)
                    {
                        c.CompletionDate = list.Max(g => g.CompletionDate);

                        DateTime dueDate = c.CompletionDate.AddMonths(24);

                        string dueInfo = string.Empty;
                        if (dueDate > DateTime.Now)
                        {
                            int days = dueDate.Subtract(DateTime.Now).Days;
                            string d = days == 1 ? "dia" : "dias";
                            dueInfo = string.Format("{0}, {1} {2}", dueDate.ToString("dd/MM/yyyy"), dueDate.Subtract(DateTime.Now).Days.ToString(), d);
                        }
                        else if (dueDate.Date == DateTime.Now.Date)
                            dueInfo = dueInfo = string.Format("{0}, hoje", dueDate.ToString("dd/MM/yyyy"));
                        else
                        {
                            if (vencimento == "0")
                                dueInfo = string.Format("{0}", dueDate.ToString("dd/MM/yyyy"));
                            else
                                dueInfo = string.Format("{0}, vencido", dueDate.ToString("dd/MM/yyyy"));
                        }
                            
                        c.DueInfo = dueInfo;

                        if (dueDate > DateTime.Now && dueDate <= DateTime.Now.AddDays(30))
                        {
                            c.Situacao = "30";
                            clients.Add(c);
                        }
                        else if (dueDate > DateTime.Now && dueDate > DateTime.Now.AddDays(30) && dueDate <= DateTime.Now.AddDays(60))
                        {
                            c.Situacao = "60";
                            clients.Add(c);
                        }
                        else if (dueDate > DateTime.Now && dueDate > DateTime.Now.AddDays(60) && dueDate <= DateTime.Now.AddDays(90))
                        {
                            c.Situacao = "90";
                            clients.Add(c);
                        }
                        else if (dueDate < DateTime.Now && (vencimento == "0"))
                        {
                            c.Situacao = "0";
                            clients.Add(c);
                        }
                    }
                });

                if (vencimento == "30")
                    response.Result.Clients = response.Result.Clients.Where(c => c.Situacao == "30").ToList();

                if (vencimento == "60")
                    response.Result.Clients = response.Result.Clients.Where(c => c.Situacao == "30" || c.Situacao == "60").ToList();

                if (vencimento == "90")
                    response.Result.Clients = response.Result.Clients.Where(c => c.Situacao == "30" || c.Situacao == "60" || c.Situacao == "90").ToList();

                if (vencimento == "0")
                    response.Result.Clients = response.Result.Clients.Where(c => c.Situacao == "0").ToList();

                if (investorKind == "1")
                    response.Result.Clients = response.Result.Clients.Where(c => c.InvestorKind == 1).ToList();

                if (investorKind == "2")
                    response.Result.Clients = response.Result.Clients.Where(c => c.InvestorKind == 2).ToList();

                if (!string.IsNullOrEmpty(orderBy))
                {
                    switch (orderBy)
                    {
                        case "code":
                            response.Result.Clients = response.Result.Clients.OrderBy(x => x.CodigosBolsa[0]).ToList();
                            break;
                        case "name":
                            response.Result.Clients = response.Result.Clients.OrderBy(x => x.ClientName).ToList();
                            break;
                        case "profile":
                            response.Result.Clients = response.Result.Clients.OrderBy(x => x.InvestorTypeDesc).ToList();
                            break;
                        case "status":
                            response.Result.Clients = response.Result.Clients.OrderBy(x => x.StatusDesc).ToList();
                            break;
                        default:
                            break;
                    }
                }

                sr = response.Result;

                if (!string.IsNullOrEmpty(response.Message))
                    throw new Exception(response.Message);

                service.Close();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw ex;
            }

            return View(sr);
        }

        [HttpGet]
        public ActionResult SuitabilityResult(string id)
        {
            IChannelProvider<ISuitabilityContractChannel> channelProvider = ChannelProviderFactory<ISuitabilityContractChannel>.Create("SuitabilityClassName");
            var service = channelProvider.GetChannel().CreateChannel();

            service.Open();

            var sc = service.SelecionaResultadoSuitability(id);

            service.Close();

            ViewBag.Id = id;

            return View("~/Views/Suitability/SuitabilityResult.cshtml", sc);
        }

        [HttpGet]
        public void SuitabilityResultToPdf(string id)
        {
            IChannelProvider<ISuitabilityContractChannel> channelProvider = ChannelProviderFactory<ISuitabilityContractChannel>.Create("SuitabilityClassName");
            var service = channelProvider.GetChannel().CreateChannel();

            service.Open();

            var sc = service.SelecionaResultadoSuitability(id);

            service.Close();

            string fileName = string.Format("Suitability_{0}_{1}.{2}", id.ToString(), DateTime.Now.ToString("ddMMyyyymmss"), "pdf");
            AddResponseHeader(Response, fileName);

            var html = RenderRazorViewToString("~/Views/Suitability/SuitabilityResultPrint.cshtml", sc);
           
            var pdfBytes = PDFConverter.HtmlToPDF(html);

            DownloadPdf(Response, pdfBytes);
        }

        [HttpGet]
        public ActionResult SuitabilityResultPrint(string id)
        {
            IChannelProvider<ISuitabilityContractChannel> channelProvider = ChannelProviderFactory<ISuitabilityContractChannel>.Create("SuitabilityClassName");
            var service = channelProvider.GetChannel().CreateChannel();

            service.Open();

            var sc = service.SelecionaResultadoSuitability(id);

            service.Close();

            return View("~/Views/Suitability/SuitabilityResultPrint.cshtml", sc);
        }

        [HttpGet]
        public ActionResult Preencher(int id)
        {
            //IChannelProvider<ISuitabilityContractChannel> channelProvider = ChannelProviderFactory<ISuitabilityContractChannel>.Create("SuitabilityClassName");
            //using (var service = channelProvider.GetChannel().CreateChannel())
            //{
            //    //service.GravarSuitability()
            //}

            SessionHelper.ThirdPartySuitabilityClient = id;
            return RedirectToAction("Questions2", "Suitability");
        }

        public static List<SuitabilityClients> HistoricoCliente(int idCliente, string cdLogin)
        {
            List<SuitabilityClients> lsc = new List<SuitabilityClients>();

            IChannelProvider<ISuitabilityContractChannel> channelProvider = ChannelProviderFactory<ISuitabilityContractChannel>.Create("SuitabilityClassName");
            var service = channelProvider.GetChannel().CreateChannel();

            service.Open();

            lsc = service.HistoricoClientesSuitability(idCliente, null, cdLogin).Result.ToList();

            service.Close();

            return lsc;
        }

        public JsonResult SuitabilityAtualizaStatus(int idClient, int status)
        {
            var message = "Status atualizado com sucesso!";
            bool sc = false;

            try
            {
                IChannelProvider<ISuitabilityContractChannel> channelProvider = ChannelProviderFactory<ISuitabilityContractChannel>.Create("SuitabilityClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();
                sc = service.AtualizaStatusSuitability(idClient, status).Result;
                service.Close();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                message = "Não foi possível atualizar status!";
            }

            return Json(new { msg = message });
        }

        [HttpGet]
        public JsonResult SaveJustification(int clientId, string message)
        {
            IChannelProvider<ISuitabilityContractChannel> channelProvider = ChannelProviderFactory<ISuitabilityContractChannel>.Create("SuitabilityClassName");
            using (var service = channelProvider.GetChannel().CreateChannel())
            {
                service.GravarSuitabilityJustification(clientId, message);
                return Json(true, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult PerfilCliente()
        {
            ViewBag.Initial = true;
            return View(new List<SuitabilityPerfilCliente>());
        }

        public void ExtractToExecel(string cnpj)
        {
            IChannelProvider<ISuitabilityContractChannel> channelProvider = ChannelProviderFactory<ISuitabilityContractChannel>.Create("SuitabilityClassName");
            var response = new WcfResponse<SuitabilityPerfilCliente[]>(); //  new WcfResponse<List<SuitabilityPerfilCliente>>();
            using (var service = channelProvider.GetChannel().CreateChannel())
            {
                service.Open();
                string sCpfCnpj = Regex.Replace(cnpj, @"[^\d]", "").ToString();

                response = service.PerfilClienteJustification(sCpfCnpj, (sCpfCnpj.Length == 0));
            }

            var excel = new System.Data.DataTable("teste");
            excel.Columns.Add("CPF/CNPJ", typeof(string));
            excel.Columns.Add("Data Cadastro", typeof(string));
            excel.Columns.Add("Descrição", typeof(string));
            excel.Columns.Add("Justificação", typeof(string));
            excel.Columns.Add("Desenquadro", typeof(string));

            GetMismatch(response.Result);

            for (int row = 0; row < response.Result.Length; row++)
            {
                DataRow newrow = excel.NewRow();
                newrow[excel.Columns["CPF/CNPJ"]] = response.Result[row].CnpjCpf;
                newrow[excel.Columns["Data Cadastro"]] = response.Result[row].DataCadastro;
                newrow[excel.Columns["Descrição"]] = response.Result[row].Descricao;
                newrow[excel.Columns["Justificação"]] = response.Result[row].Justificacao;
                newrow[excel.Columns["Desenquadro"]] = response.Result[row].Desenquadramento;
                excel.Rows.Add(newrow);
            }

            var grid = new GridView();
            grid.DataSource = excel;
            grid.DataBind();
            grid.HeaderStyle.BackColor = Color.FromArgb(95, 136, 164);
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=ClientePerfil.xls");
            Response.ContentType = "application/ms-excel";

            Response.Charset = "utf-8";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            grid.RenderControl(htw);

            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }

        public void ExportToPdf(string cnpj)
        {
            IChannelProvider<ISuitabilityContractChannel> channelProvider = ChannelProviderFactory<ISuitabilityContractChannel>.Create("SuitabilityClassName");
            var response = new WcfResponse<SuitabilityPerfilCliente[]>(); // new WcfResponse<List<SuitabilityPerfilCliente>>();
            using (var service = channelProvider.GetChannel().CreateChannel())
            {
                service.Open();
                string sCpfCnpj = Regex.Replace(cnpj, @"[^\d]", "").ToString();

                response = service.PerfilClienteJustification(sCpfCnpj, (sCpfCnpj.Length == 0));
            }

            GetMismatch(response.Result);

            string fileName = string.Format("PerfilClienteRelatorio_{0}.{1}", DateTime.Now.ToString("ddMMyyyymmss"), "pdf");
            AddResponseHeader(Response, fileName);

            var html = RenderRazorViewToString("PerfilClientePrint", response.Result);

            var pdfBytes = PDFConverter.HtmlToPDF_Simple(html);

            DownloadPdf(Response, pdfBytes);
        }

        [HttpPost]
        public ActionResult PerfilCliente(FormCollection form)
        {
            IChannelProvider<ISuitabilityContractChannel> channelProvider = ChannelProviderFactory<ISuitabilityContractChannel>.Create("SuitabilityClassName");
            var response = new WcfResponse<SuitabilityPerfilCliente[]>(); // new WcfResponse<List<SuitabilityPerfilCliente>>();
            using (var service = channelProvider.GetChannel().CreateChannel())
            {
                service.Open();
                string sCpfCnpj = Regex.Replace(form["cpfcnpj"], @"[^\d]", "").ToString();

                response = service.PerfilClienteJustification(sCpfCnpj, (sCpfCnpj.Length == 0));
            }

            if (response.Result == null)
            {
                // response.Result = new List<SuitabilityPerfilCliente>();
                return View(new List<SuitabilityPerfilCliente>());
            }

            GetMismatch(response.Result);

            return View(response.Result.ToList());
        }

        public static SuitabilityUnframedResponse[] VerificarNovoDesenquadramento(int idCliente, DateTime? dtInicio, DateTime? dtFim)
        {
            try
            {
                IChannelProvider<ISuitabilityContractChannel> channelProvider = ChannelProviderFactory<ISuitabilityContractChannel>.Create("SuitabilityClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                var res = service.VerificaNovoDesenquadramento(idCliente, dtInicio, dtFim);
                if (res.Result == null)
                    throw new Exception(res.Message);
                else
                {
                    if (res.Result.Length > 0)
                    {
                        foreach (var item in res.Result)
                        {
                            if (item.Unframeds != null && item.Unframeds.Count > 0)
                            {
                                // Save unframed information

                                break;
                            }
                        }
                    }
                }

                service.Close();
                return res.Result;
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                throw;
            }

        }
    }
}
