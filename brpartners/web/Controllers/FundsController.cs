﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.DataVisualization.Charting;
using Newtonsoft.Json;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.Services.SagazWebService;
using QX3.Portal.WebSite.FundService;
using QX3.Portal.WebSite.Helper;
using QX3.Portal.WebSite.Models;
using QX3.Portal.WebSite.Models.Channel;
using QX3.Portal.WebSite.Properties;
using QX3.Spinnex.Common.Services.Logging;
using QX3.Spinnex.Common.Services.Mail;

namespace QX3.Portal.WebSite.Controllers
{
    public class FundsController : FileController
    {
        private IChannelProvider<IFundContractChannel> fndChn;
        public IChannelProvider<IFundContractChannel> FundProvider
        {
            get
            {
                if (fndChn == null)
                    fndChn = ChannelProviderFactory<IFundContractChannel>.Create("FundClassName");
                return fndChn;
            }

        }

        #region Funds - Fundos

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FindClientInfo(FormCollection form) 
        {
            if (String.IsNullOrEmpty(form["txtClientCode"]))
            {
                Session["FundClient"] = null;
                //TempData["fundGenericError"] = "Cliente não encontrado.";
            }
            else
            {
                var client = new FundHelper().GetFundClient(Convert.ToInt32(form["txtClientCode"]));
                Session["FundClient"] = client;
                if (client == null)
                    TempData["fundGenericError"] = "Cliente não encontrado.";
            }

            return Redirect(HttpContext.Request.UrlReferrer.AbsoluteUri);
        }

        [AccessControlAuthorize]
        public ActionResult Index()
        {
            FundClient client = (FundClient)Session["FundClient"];
            ViewBag.Client = client;

            var fds = new List<Fund>();
            using (var service = FundProvider.GetChannel().CreateChannel())
            {
                fds = service.FN_LoadFunds().Result;
            }
            
            return View(fds);
        }

		public ActionResult LoadFund(string fundCode)
		{
			FundDetails response = new FundDetails();
			if (!string.IsNullOrEmpty(fundCode))
			{
				using (var service = FundProvider.GetChannel().CreateChannel())
				{
					response = service.FN_LoadFund(Convert.ToInt16(fundCode)).Result;
					Session["Funds.FundDetail"] = response;

					if (response.Documents != null)
					{
						for (int i = 0; i < response.Documents.Length; i++)
						{
							response.Documents[i].Active = service.FN_DownloadDocument(response.Documents[i].Id).Result != null ? true : false;
						}
					}
				}
			}
			return View(response);
		}

		[HttpGet]
		public ActionResult FundDetailToFile()
		{
			try
			{
				FundDetails fundDetails = new FundDetails();

				if (Session["Funds.FundDetail"] != null)
				{
					fundDetails = (FundDetails)Session["Funds.FundDetail"];
				
					//LogHelper.SaveAuditingLog("Subscription.SubscriptionsToFile", string.Format(LogResources.ObjectToFileParameters, isPdf ? "PDF" : "Excel", JsonConvert.SerializeObject(saldo)));
					string fileName = string.Format("fundos_informacoes_{0}.{1}", DateTime.Now.ToString("dd-MM-yyyy"), "pdf");
					LogHelper.SaveHistoryLog("Funds.FundDetail", new HistoryData { Action = "Exportar", OldData = string.Empty, NewData = JsonConvert.SerializeObject(fundDetails) });
					return ViewFundDetailPdf(fundDetails, true, fileName);
				}
				else
				{
					return this.RedirectToAction("Index");
				}
			}
			catch (Exception ex)
			{
				LogManager.WriteError(ex);
				TempData["ErrorMessage"] = "Ocorreu um erro ao gerar relatório.";
				return this.RedirectToAction("Index");
			}
		}

        #endregion

        #region Application - Aplicação

        [AccessControlAuthorize]
        public ActionResult Application(string fundCode)
        {
            //renovando sessão se eu carregar a pagina via F5 - Necessário apenas nas telas Aplicação e resgate.
            if (Request.Headers.AllKeys.Where(a => a.Trim().ToLower() == "referer").FirstOrDefault() == null && Session["FundClient"] != null) {
                Session["FundClient"] = new FundHelper().GetFundClient(((FundClient)Session["FundClient"]).ClientCode);
            }

            var client = (FundClient)Session["FundClient"];// (Request.Headers.AllKeys.Where(a=>a == "referer")) (FundClient)Session["FundClient"];
            ViewBag.Client = client;

            ViewBag.ddlFunds = new SelectList(new List<string>());

            if (ViewBag.Client != null)
            {
                using (var service = FundProvider.GetChannel().CreateChannel())
                {
                    service.Open();

                    var fundsResult = service.FN_LoadFunds();

                    if (fundsResult.Result != null && fundsResult.Result.Count > 0)
                        ViewBag.ddlFunds = new SelectList(fundsResult.Result, "Code", "Name", fundCode);

                    else if (!String.IsNullOrEmpty(fundsResult.Message))
                        TempData["errorApplication"] = "Não foi possível carregar os fundos do cliente: " + fundsResult.Message;
                }
            }

            return View();
        }

        [HttpPost]
        public ActionResult ConfirmApplication(decimal Investment, int FundCode, bool UserHasApplication, bool UserHasAccepted) 
        {
            var client = (FundClient)Session["FundClient"];
            ViewBag.Client = client;

            if (client != null)
            { 
                using (var service = FundProvider.GetChannel().CreateChannel()) 
                { 
                    var fund = service.FN_LoadFundDetails(client.ClientCode, FundCode).Result;
                    if (fund != null) fund.Code = Convert.ToInt32(FundCode);
                    ViewBag.FundDetail = fund;
                }
                ViewBag.ApplicationValue = Investment;
            }
            
            return View();
        }

        [HttpPost]
        public ActionResult DoApplication(FormCollection form) 
        {
            //return Json(new WcfResponse<bool> { Result = false, Message = "erro." });

            FundClient client = (FundClient)Session["FundClient"];

            
            if (client == null)
                return Json(new WcfResponse<bool> { Result = false, Message = "Não foi possível definir o cliente." });

            WcfResponse<bool> responseApp = new WcfResponse<bool>();

            using (var service = FundProvider.GetChannel().CreateChannel()) 
            {
                QX3.Portal.Services.SagazWebService.Application app = new Application()
                {
                    FundCode = Convert.ToInt32(form["txtFundCode"]),
                    InsertDate = DateTime.Now,
                    StatusId = 1,
                    OperationalCode = client.ClientCode,
                    Amount = Convert.ToDecimal(form["txtValue"])
                };

                bool acession = Convert.ToBoolean(form["txtUserHasAccepted"]);

                responseApp = service.FN_InsertApplication(app);

                if (responseApp.Result) 
                {

                    LogHelper.SaveHistoryLog("Funds.Application",
                        new HistoryData
                        {
                            Action = "Aplicar",
                            NewData = JsonConvert.SerializeObject(responseApp.Result),
                            ID = client.ClientCode,
                            Target = app.FundCode.ToString(),
                            ExtraTarget = app.Amount.ToString("C2")
                        });

                    if (!acession)
                        service.FN_InsertAccession(client.ClientCode, Convert.ToInt32(form["txtFundCode"]), 2);

                    client = new FundHelper().GetFundClient(client.ClientCode);
                    Session["FundClient"] = client;

                }
            }

            return Json(new WcfResponse<bool, FundClient>()
            {
                Result = responseApp.Result,
                Message = responseApp.Message,
                Data = new FundClient() { BlockedBalance = client.BlockedBalance, AvailableBalance = client.AvailableBalance }
            });
        }

        [HttpPost]
        public ActionResult LoadFundDetails(int fundCode) 
        {
            var client = (FundClient)Session["FundClient"];
            
            if (client != null)
            {
                using (var service = FundProvider.GetChannel().CreateChannel())
                {
                    service.Open();

                    var fundsResult = service.FN_LoadFundDetails(client.ClientCode, fundCode);
                    
                    return Json(fundsResult);
                }
            }

            return Json(new WcfResponse<FundDetails>() { Message = "Não foi possível carregar as informações de fundo." });
        }

        public ActionResult DownloadFundDocument(int id, string fileName)
        {
            using (var service = FundProvider.GetChannel().CreateChannel())
            {
                service.Open();

                byte[] fileContent = service.FN_DownloadDocument(id).Result;


                if (fileContent != null)
                    return new BinaryContentResult(fileContent, "application/pdf", true, fileName);
                else
                {
                    TempData["errorApplication"] = "Não foi possível realizar o download do arquivo.";
                    return RedirectToAction("Application");
                }

            }
        }

        [HttpPost]
        public ActionResult TestDownload(int id) 
        {
            using (var service = FundProvider.GetChannel().CreateChannel())
            {
                service.Open();

                byte[] fileContent = service.FN_DownloadDocument(id).Result;

                return Json(fileContent != null);
            }
        }

        [HttpPost]
        public string GetPositionBalance() 
        {
            var client = (FundClient)Session["FundClient"];

            if (client != null)
            {

                return new FundHelper().BalancePositionResponse(ref client);
                
            }

            return Newtonsoft.Json.JsonConvert.SerializeObject(new FundClient());
        }

        #endregion

        #region Balance - Saldo

        [AccessControlAuthorize]
        public ActionResult Balance()
        {
			Session["Funds.Balance"] = null;
			Session["Funds.BalancePending"] = null;
			Session["Funds.BalanceChart"] = null;
			Session["Funds.BalanceChartImage"] = null;
            FundClient client = (FundClient)Session["FundClient"];
			ViewBag.Client = client;

            if (client != null)
            {
                ViewBag.ddlFunds = new SelectList(new List<string>()); 
                using (var service = FundProvider.GetChannel().CreateChannel())
                {
                    service.Open();

					ViewBag.ddlFunds = new SelectList(new List<string>());

                    /*Drop Fundos*/

                    var fundsResult = service.FN_LoadClientFunds(client.ClientCode);

                    if (fundsResult.Result != null && fundsResult.Result.Count > 0)
                        ViewBag.ddlFunds = new SelectList(fundsResult.Result, "Code", "Name", fundsResult.Result[0]);
                    else if ((fundsResult.Result == null) || fundsResult.Result != null && fundsResult.Result.Count == 0) 
                    {
                        TempData["errorBalance"] = "Cliente não tem aplicação em Fundos de Investimento.";
                        ViewBag.ddlFunds = null;
                    }
                    else if (!String.IsNullOrEmpty(fundsResult.Message))
                    {
                        TempData["errorBalance"] = "Não foi possível carregar os fundos do cliente: " + fundsResult.Message;
                    }

                    /*Chart (Caso o approach seja de carregar o gráfico ao load da página)*/

                    var chartData = service.FN_LoadBalanceChart(client.ClientCode);

                    /*** DADOS PARA FACILITAR O DESENVOLVIMENTO - REMOVER NA VERSÃO FINAL ***/
					/*chartData.Result.Add(new FundBalanceChartItemResponse() { Composition = "CVM 1", FundName = "Fundo de Telecomunicações", Value = 700 });
					chartData.Result.Add(new FundBalanceChartItemResponse() { Composition = "CVM 1", FundName = "Fundo de Turismo", Value = 1150 });
					chartData.Result.Add(new FundBalanceChartItemResponse() { Composition = "CVM 1", FundName = "Fundo Automobilístico", Value = 2000 });
					chartData.Result.Add(new FundBalanceChartItemResponse() { Composition = "CVM 1", FundName = "Fundo Internet", Value = 3500 });
					chartData.Result.Add(new FundBalanceChartItemResponse() { Composition = "CVM 1", FundName = "Fundo Alimentício", Value = 1280 });
					chartData.Result.Add(new FundBalanceChartItemResponse() { Composition = "CVM 1", FundName = "Fundo de Barracas de Cachorro Quente", Value = 600 });
					chartData.Result.Add(new FundBalanceChartItemResponse() { Composition = "CVM 1", FundName = "Fundo de Quintal", Value = 2500 });
					chartData.Result.Add(new FundBalanceChartItemResponse() { Composition = "CVM 1", FundName = "Fundo Meninas Iradas", Value = 1500 });
					chartData.Result.Add(new FundBalanceChartItemResponse() { Composition = "CVM 1", FundName = "1Fundo de Telecomunicações", Value = 1700 });
					chartData.Result.Add(new FundBalanceChartItemResponse() { Composition = "CVM 1", FundName = "2Fundo de Turismo", Value = 11150 });
					chartData.Result.Add(new FundBalanceChartItemResponse() { Composition = "CVM 1", FundName = "3Fundo Automobilístico", Value = 12000 });
					chartData.Result.Add(new FundBalanceChartItemResponse() { Composition = "CVM 1", FundName = "4Fundo Internet", Value = 13500 });
					chartData.Result.Add(new FundBalanceChartItemResponse() { Composition = "CVM 1", FundName = "5Fundo Alimentício", Value = 11280 });
					chartData.Result.Add(new FundBalanceChartItemResponse() { Composition = "CVM 1", FundName = "6Fundo de Barracas de Cachorro Quente", Value = 1600 });
					chartData.Result.Add(new FundBalanceChartItemResponse() { Composition = "CVM 1", FundName = "7Fundo de Quintal", Value = 12500 });
					chartData.Result.Add(new FundBalanceChartItemResponse() { Composition = "CVM 1", FundName = "8Fundo Meninas Iradas", Value = 11500 });*/
                    /*** FIM - DADOS PARA FACILITAR O DESENVOLVIMENTO - REMOVER NA VERSÃO FINAL ***/

                    if (chartData.Result != null && chartData.Result.Count > 1)
                    {
                        var sumData = chartData.Result.Sum(c => c.Value);
                        int i = -1;
                        var data = (from c in chartData.Result
                                   select new FundBalanceChartItemResponse
                                   {
                                       FundName = c.FundName
                                       , Composition = c.Composition
                                       , Value = c.Value
                                       , Percentage = ((c.Value / sumData) * 100).ToString("N2")
                                   }).ToList();
                        foreach (var c in data) 
                        {
                            c.RGBColor = System.Drawing.ColorTranslator.ToHtml(GetColorToPieChart(++i).Value);
                        }

						Session["Funds.BalanceChart"] = data;
                        ViewBag.ChartInfo = data;
                    }

                    /*Pendentes*/

                    var pendingData = service.FN_LoadBalancePendingApplications(client.ClientCode);

                    if (pendingData.Result != null && pendingData.Result.Count > 0)
                    {
						Session["Funds.BalancePending"] = pendingData.Result; 
						ViewBag.PendingMovements = pendingData.Result;
                    }
                }

            }
            else 
            {
                ViewBag.ddlFunds = new SelectList(new List<string>()); 
            }
            return View();
        }

        [HttpPost]
        public string GetBalance(FormCollection form) 
        { 
            FundClient client = (FundClient)Session["FundClient"];

            if (client != null) 
            { 
                using (var service = FundProvider.GetChannel().CreateChannel())
                {
                    var balanceResult = service.FN_LoadBalance(client.ClientCode, Convert.ToInt32(form["txtfundCode"]));

                    if (balanceResult.Result != null)
                    {
						Session["Funds.Balance"] = balanceResult.Result;
                        return new FundHelper().FundBalanceResponse(
                            balanceResult.Result
                            , ref form
                            , balanceResult.Result != null 
                                ? balanceResult.Result.Items != null 
                                    ? balanceResult.Result.Items.Count() 
                                    : 0 
                                : 0);
                    }

					/* Chart */
					var chartData = service.FN_LoadBalanceChart(client.ClientCode);

					if (chartData.Result != null && chartData.Result.Count > 1)
					{
						Session["Funds.BalanceChart"] = chartData.Result;
					}
                }
            }

            return Newtonsoft.Json.JsonConvert.SerializeObject(new FundBalanceResponse());
        }

		public ActionResult BalanceChart()
		{
			if (Session["Funds.BalanceChart"] != null)
			{
				List<FundBalanceChartItemResponse> balanceChart = (List<FundBalanceChartItemResponse>)Session["Funds.BalanceChart"];

				Chart Chart1 = new Chart();
				Chart1.AntiAliasing = AntiAliasingStyles.All;
				Chart1.TextAntiAliasingQuality = TextAntiAliasingQuality.High;
				Chart1.Compression = 0;
				Chart1.ImageType = ChartImageType.Png;
				Chart1.Width = 250;
				Chart1.Height = 175;
				Chart1.BackColor = Color.FromArgb(241, 242, 242);

				Chart1.ChartAreas.Add("ChartArea1");
				Chart1.ChartAreas[0].BackColor = Color.FromArgb(241, 242, 242);

				Chart1.Series.Add("Default");
				decimal[] yValues = balanceChart.Select(p => p.Value).ToArray();
				string[] xValues = balanceChart.Select(p => p.FundName).ToArray();
				Chart1.Series[0].Points.DataBindXY(xValues, yValues);
				Chart1.Series[0].ChartType = SeriesChartType.Pie; 
				Chart1.Series[0]["PieLabelStyle"] = "Outside";

				//Chart1.Legends.Add("Legend1");
				//Chart1.Legends[0].Enabled = true;
				//Chart1.Legends[0].Docking = Docking.Bottom;
				//Chart1.Legends[0].Alignment = System.Drawing.StringAlignment.Center;
				//Chart1.Legends[0].BackColor = Color.FromArgb(241, 242, 242);

				int count = 0;
				foreach (DataPoint ponto in Chart1.Series[0].Points)
				{
					ponto.LegendText = string.Format("{0} - {1}", xValues[count], yValues[count].ToString("N2"));
					ponto.BorderColor = Color.White;
					ponto.BorderWidth = 1;
					ponto.Font = new Font("Arial", 8, FontStyle.Regular);
					ponto.Label = "#PERCENT{P2}";
					ponto.LabelForeColor = Color.FromArgb(85, 84, 79);
					if (GetColorToPieChart(count) != null)
						ponto.Color = GetColorToPieChart(count).Value;
					count++;
				}

				MemoryStream memoryStream = new MemoryStream();
				Chart1.SaveImage(memoryStream, ChartImageFormat.Png);
				memoryStream.Seek(0, SeekOrigin.Begin);

				/*** Gráfico para exportação para PDF ***/
				Chart1.Width = 370;
				Chart1.Height = 120;
				Chart1.BackColor = Color.White;
				Chart1.ChartAreas[0].BackColor = Color.White;
				foreach (DataPoint ponto in Chart1.Series[0].Points)
				{
					ponto.BorderColor = Color.FromArgb(210, 210, 210);
					ponto.Font = new Font("Arial", 7, FontStyle.Regular);
				}
				MemoryStream memoryStream1 = new MemoryStream();
				Chart1.SaveImage(memoryStream1, ChartImageFormat.Png);
				memoryStream1.Seek(0, SeekOrigin.Begin);
				Session["Funds.BalanceChartImage"] = memoryStream1.GetBuffer();
				/*** Fim - Gráfico para exportação para PDF ***/

				return File(memoryStream.GetBuffer(), "image/png", "mychart.png");
			}
			else
			{
				return null;
			}
        }

		private Color? GetColorToPieChart(int pos = 0)
		{
			Color[] cores = new Color[] {
				Color.FromArgb(24, 132, 46),	// 0 - verde médio
				Color.FromArgb(254, 158, 46),	// 1 - laranja médio
				Color.FromArgb(38, 84, 169),	// 2 - azul médio
				Color.FromArgb(228, 35, 36),	// 3 - vermelho médio
				Color.FromArgb(140, 70, 195),	// 4 - lilás médio
				Color.FromArgb(179, 117, 56),	// 5 - marrom médio
				Color.FromArgb(59, 172, 82),	// 6 - verde claro
				Color.FromArgb(255, 226, 64),	// 7 - amarelo
				Color.FromArgb(89, 134, 217),	// 8 - azul claro
				Color.FromArgb(254, 121, 122),	// 9 - vermelho claro
				Color.FromArgb(210, 156, 250),	// 10 - lilás claro
				Color.FromArgb(215, 176, 137),	// 11 - marrom claro
				Color.FromArgb(6, 80, 21),		// 12 - verde escuro
				Color.FromArgb(222, 105, 2),	// 13 - laranja escuro
				Color.FromArgb(0, 37, 105),		// 14 - azul escuro
				Color.FromArgb(160, 27, 28),	// 15 - vermelho escuro
				Color.FromArgb(92, 36, 133),	// 16 - lilás escuro
				Color.FromArgb(113, 57, 0),		// 17- marrom escuro
			};
			try
			{
				return cores[pos];
			}
			catch
			{
				return null;
			}
		}

		[HttpGet]
		public ActionResult BalanceToFile(bool isPdf)
		{
			try
			{
				FundClient client = new FundClient();
				FundBalance balance = new FundBalance();
				List<FundBalancePendingApplicationItem> pending = new List<FundBalancePendingApplicationItem>();
				List<FundBalanceChartItemResponse> chart = new List<FundBalanceChartItemResponse>();
				byte[] chartImage = new byte[0];

				if (Session["FundClient"] != null
					&& Session["Funds.Balance"] != null
					&& Session["Funds.BalancePending"] != null
					&& Session["Funds.BalanceChart"] != null
					&& Session["Funds.BalanceChartImage"] != null)
				{
					client = (FundClient)Session["FundClient"];
					balance = (FundBalance)Session["Funds.Balance"];
					pending = (List<FundBalancePendingApplicationItem>)Session["Funds.BalancePending"];
					chart = (List<FundBalanceChartItemResponse>)Session["Funds.BalanceChart"];
					chartImage = (byte[])Session["Funds.BalanceChartImage"];
				}

				//LogHelper.SaveAuditingLog("Subscription.SubscriptionsToFile", string.Format(LogResources.ObjectToFileParameters, isPdf ? "PDF" : "Excel", JsonConvert.SerializeObject(saldo)));
				string fileName = string.Format("fundos_saldo_{0}.{1}", DateTime.Now.ToString("dd-MM-yyyy"), isPdf ? "pdf" : "xls");
				LogHelper.SaveHistoryLog("Funds.Balance", new HistoryData { Action = "Exportar", OldData = string.Empty, NewData = JsonConvert.SerializeObject(balance) });
				return isPdf ? ViewFundsBalancePdf(client, balance, pending, chart, chartImage, true, fileName) : ViewFundsBalanceExcel(client, balance, pending, chart, true, fileName);
			}
			catch (Exception ex)
			{
				LogManager.WriteError(ex);
				TempData["ErrorMessage"] = "Ocorreu um erro ao gerar relatório.";
				return this.RedirectToAction("Balance");
			}
		}

        #endregion

        #region Statement - Extrato

        [AccessControlAuthorize]
        public ActionResult Statement()
        {
            FundClient client = (FundClient)Session["FundClient"];
            ViewBag.Client = client;

            /*Periodos*/        
            var dictionary = new List<KeyValuePair<string,string>>();
            dictionary.Add(new KeyValuePair<string, string>(((DateTime.Now.Day == 1) ? 0 : 1 - (DateTime.Now.Day)).ToString(), "Mês Atual"));
            dictionary.Add(new KeyValuePair<string,string>("-90", "Três últimos meses"));
            ViewBag.ddlPeriod = new SelectList(dictionary, "Key", "Value");

            if (client != null)
            {
                using (var service = FundProvider.GetChannel().CreateChannel())
                {
                    service.Open();

                    /*Drop Fundos*/

                    var fundsResult = service.FN_LoadClientFunds(client.ClientCode);

                    if (fundsResult.Result != null && fundsResult.Result.Count > 0)
                        ViewBag.ddlFunds = new SelectList(fundsResult.Result, "Code", "Name", fundsResult.Result[0]);
                    else if ((fundsResult.Result == null) || fundsResult.Result != null && fundsResult.Result.Count == 0)
                    {
                        TempData["errorStatement"] = "Cliente não tem aplicações em Fundos de Investimento.";
                    }
                    else if (!String.IsNullOrEmpty(fundsResult.Message))
                        TempData["errorStatement"] = "Não foi possível carregar os fundos do cliente: " + fundsResult.Message;
                }
            }
            else
            {
                ViewBag.ddlFunds = new SelectList(new List<string>());
            }
            return View();
        }

        [HttpPost]
        public ActionResult LoadAnalyticalStatement(FormCollection form)
        {
            return this.ProcessStatementList("LoadAnalyticalStatement", form);
        }

        [HttpPost]
        public ActionResult LoadSyntheticStatement(FormCollection form)
        {
            return this.ProcessStatementList("LoadSyntheticStatement", form);
        }

        protected ActionResult ProcessStatementList(string viewName, FormCollection form) 
        {
            FundClient client = (FundClient)Session["FundClient"];

            if (client != null)
            {
                using (var service = FundProvider.GetChannel().CreateChannel())
                {
                    var initialDate = DateTime.Now.AddDays(Convert.ToInt32(form["ddlPeriod"]));
                    var finalDate = DateTime.Now;

                    var response = service.FN_LoadStatement(initialDate, finalDate, client.ClientCode, Convert.ToInt32(form["ddlFunds"]));
					Session["Funds.Statement"] = response.Result;

                    return PartialView(viewName, response.Result);
                }
            }

            return PartialView(viewName, null);
        }

		[HttpGet]
		public ActionResult AnalyticalStatementToFile(bool isPdf)
		{
			try
			{
				FundClient client = new FundClient();
				List<FundStatementItem> statement = new List<FundStatementItem>();

				if (Session["FundClient"] != null
					&& Session["Funds.Statement"] != null)
				{
					client = (FundClient)Session["FundClient"];
					statement = (List<FundStatementItem>)Session["Funds.Statement"];

					//LogHelper.SaveAuditingLog("Subscription.SubscriptionsToFile", string.Format(LogResources.ObjectToFileParameters, isPdf ? "PDF" : "Excel", JsonConvert.SerializeObject(saldo)));
					string fileName = string.Format("fundos_extrato_analitico_{0}.{1}", DateTime.Now.ToString("dd-MM-yyyy"), isPdf ? "pdf" : "xls");
					LogHelper.SaveHistoryLog("Funds.AnalyticalStatement", new HistoryData { Action = "Exportar", OldData = string.Empty, NewData = JsonConvert.SerializeObject(statement) });
					return isPdf ? ViewFundsAnalyticalStatementPdf(client, statement, true, fileName) : ViewFundsAnalyticalStatementExcel(client, statement, true, fileName);
				}
				else
				{
					return this.RedirectToAction("Statement");
				}
			}
			catch (Exception ex)
			{
				LogManager.WriteError(ex);
				TempData["ErrorMessage"] = "Ocorreu um erro ao gerar relatório.";
				return this.RedirectToAction("Statement");
			}
		}

		[HttpGet]
		public ActionResult SyntheticStatementToFile(bool isPdf)
		{
			try
			{
				FundClient client = new FundClient();
				List<FundStatementItem> statement = new List<FundStatementItem>();

				if (Session["FundClient"] != null
					&& Session["Funds.Statement"] != null)
				{
					client = (FundClient)Session["FundClient"];
					statement = (List<FundStatementItem>)Session["Funds.Statement"];

					//LogHelper.SaveAuditingLog("Subscription.SubscriptionsToFile", string.Format(LogResources.ObjectToFileParameters, isPdf ? "PDF" : "Excel", JsonConvert.SerializeObject(saldo)));
					string fileName = string.Format("fundos_extrato_sintetico_{0}.{1}", DateTime.Now.ToString("dd-MM-yyyy"), isPdf ? "pdf" : "xls");
					LogHelper.SaveHistoryLog("Funds.SyntheticStatement", new HistoryData { Action = "Exportar", OldData = string.Empty, NewData = JsonConvert.SerializeObject(statement) });
					return isPdf ? ViewFundsSyntheticStatementPdf(client, statement, true, fileName) : ViewFundsSyntheticStatementExcel(client, statement, true, fileName);
				}
				else
				{
					return this.RedirectToAction("Statement");
				}

			}
			catch (Exception ex)
			{
				LogManager.WriteError(ex);
				TempData["ErrorMessage"] = "Ocorreu um erro ao gerar relatório.";
				return this.RedirectToAction("Statement");
			}
		}

		#endregion

        #region Redemption - Resgate

        [AccessControlAuthorize]
        public ActionResult Redemption()
        {
            //renovando sessão se eu carregar a pagina via F5 - Necessário apenas nas telas Aplicação e resgate.
            if (Request.Headers.AllKeys.Where(a => a.Trim().ToLower() == "referer").FirstOrDefault() == null && Session["FundClient"] != null)
            {
                Session["FundClient"] = new FundHelper().GetFundClient(((FundClient)Session["FundClient"]).ClientCode);
            }

            var client = (FundClient)Session["FundClient"];
            ViewBag.Client = client;

            ViewBag.ddlFunds = new SelectList(new List<string>());

            if (ViewBag.Client != null)
            {
                using (var service = FundProvider.GetChannel().CreateChannel())
                {
                    service.Open();

                    var fundsResult = service.FN_LoadClientCurrentFunds(client.ClientCode);

                    /*Informações de saldo*/
                    var deposit = service.FN_GetUserBalance(client).Result;

                    ViewBag.BalanceDeposit = deposit;

                    if (fundsResult.Result != null && fundsResult.Result.Count > 0)
                        ViewBag.ddlFunds = new SelectList(fundsResult.Result, "Code", "Name", fundsResult.Result[0]);
                    else if (fundsResult.Result == null || (fundsResult.Result != null && fundsResult.Result.Count == 0))
                    {
                        TempData["errorRedemption"] = "O cliente informação não possui aplicações para resgate.";
                    }
                    else if (!String.IsNullOrEmpty(fundsResult.Message))
                        TempData["errorRedemption"] = "Não foi possível carregar os fundos do cliente: " + fundsResult.Message;
                }
            }


            return View();
        }

        [HttpPost]
        public ActionResult ConfirmRedemption(decimal Investment, int FundCode, bool TotalRedemption)
        {
            var client = (FundClient)Session["FundClient"];
            ViewBag.Client = client;

            if (client != null)
            {
                using (var service = FundProvider.GetChannel().CreateChannel())
                {
                    var fund = service.FN_LoadFundDetails(client.ClientCode, FundCode).Result;
                    if (fund != null) fund.Code = Convert.ToInt32(FundCode);
                    ViewBag.FundDetail = fund;
                }
                ViewBag.RedemptionValue = Investment;
                ViewBag.TotalRedemption = TotalRedemption;
            }

            return View();
        }

        [HttpPost]
        public ActionResult DoRedemption(FormCollection form)
        {

            FundClient client = (FundClient)Session["FundClient"];


            if (client == null)
                return Json(new WcfResponse<bool> { Result = false, Message = "Não foi possível definir o cliente." });

            WcfResponse<bool> responseRdp = new WcfResponse<bool>();

            using (var service = FundProvider.GetChannel().CreateChannel())
            {
                QX3.Portal.Services.SagazWebService.Redemption redemption = new Redemption()
                {
                    FundCode = Convert.ToInt32(form["txtFundCode"]),
                    StatusId = 1,
                    OperationalCode = client.ClientCode,
                    Amount = Convert.ToDecimal(form["txtValue"]),
                    Total = Convert.ToBoolean(form["chkTotal"]),
                };

                responseRdp = service.FN_InsertRedemption(redemption);

                if (responseRdp.Result)
                {

                    LogHelper.SaveHistoryLog("Funds.Redemption",
                        new HistoryData
                        {
                            Action = "Resgatar",
                            NewData = JsonConvert.SerializeObject(responseRdp.Result),
                            ID = client.ClientCode,
                            Target = redemption.FundCode.ToString(),
                            ExtraTarget = redemption.Amount.ToString("C2")
                        });
                    
                    client = new FundHelper().GetFundClient(client.ClientCode);
                    Session["FundClient"] = client;

                }
            }

            return Json(new WcfResponse<bool, FundClient>()
            {
                Result = responseRdp.Result,
                Message = responseRdp.Message,
                Data = new FundClient() { BlockedBalance = client.BlockedBalance, AvailableBalance = client.AvailableBalance, ClientCode = Convert.ToInt32(form["txtFundCode"]) }
            });
        }

        [HttpPost]
        public ActionResult LoadFundsDetailForRedemption(int fundCode) 
        {
             var client = (FundClient)Session["FundClient"];
            ViewBag.Client = client;

            ViewBag.ddlFunds = new SelectList(new List<string>());

            if (ViewBag.Client != null)
            {

                decimal amountTotal = 0;
                decimal amountRedemption = 0;
                decimal amountTotalPartial = 0;

                try
                {
                    using (var service = FundProvider.GetChannel().CreateChannel()) 
                    { 
                        //Busca o saldo atual em fundos aplicado
                        amountTotal = service.FN_LoadClientFundBalance(client.ClientCode, fundCode).Result;

                        //Busca os resgates feitos pelo cliente no tal fundo
                        var lstRedemption = service.FN_LoadClientRedemptionsPerFund(client.ClientCode, fundCode).Result;
                        if (lstRedemption != null && lstRedemption.Count() != 0)
                            amountRedemption = Convert.ToDecimal(lstRedemption.Sum(app => app.Amount));

                        amountTotalPartial = amountTotal - amountRedemption;

                        var fund = service.FN_LoadFund(fundCode).Result;

                        return Json(
                            new { 
                                    name = fund.Name,
                                    amount = amountTotal, 
                                    amountRedemption = amountRedemption, 
                                    amountPartial = amountTotalPartial,
                                    minValue = fund.MinimunInvestment 
                                });
                    }
                }
                catch
                {
                    return Json(null);
                }
            }
            return Json(null);
        }

        #endregion

        #region History
        public ActionResult FundsHistory()
        {
            List<SelectListItem> listItems = new List<SelectListItem>();
            listItems.Add(new SelectListItem { Text = "Todas", Value = "", Selected = true });
            listItems.Add(new SelectListItem { Text = "Aplicar", Value = "Aplicar", Selected = false });
            listItems.Add(new SelectListItem { Text = "Resgatar", Value = "Resgatar", Selected = false });

            try
            {
                ViewBag.FundActions = listItems;

                LogHelper.SaveAuditingLog("Funds.History");
                return View();
            }

            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = "Não foi possível carregar o histórico de movimentações de fundos.";
                return View();
            }
        }

        [HttpPost]
        public void LoadFundsHistory(FormCollection form)
        {
            try
            {
                string startDate = form["txtStartDate"];
                string endDate = form["txtEndDate"];

                long responsibleID = string.IsNullOrEmpty(form["hdfResponsible"]) ? 0 : long.Parse(form["hdfResponsible"]);
                int clientID = string.IsNullOrEmpty(form["txtClient"]) ? 0 : Int32.Parse(form["txtClient"]);
                //string FundCode = string.IsNullOrEmpty(form["txtFundCode"]) ? "" : form["txtFundCode"];

                int id = 0;
                Int32.TryParse(form["hdfRequestID"], out id);

                string action = form["ddlFundActions"];

                LogHelper.SaveAuditingLog("Funds.FundHistory",
                    string.Format(LogResources.AuditingLoadHistory, clientID, responsibleID, action, startDate, endDate));

                var history = new List<HistoryData>();

                HistoryFilter filterApplication = new HistoryFilter
                {
                    ModuleName = "Funds.Application",
                    StartDate = startDate,
                    EndDate = endDate,
                    ResponsibleID = responsibleID,
                    ID = clientID
                };

                //if (!String.IsNullOrEmpty(FundCode))
                //    filterApplication.TargetText = FundCode;

                HistoryFilter filterRedemption = new HistoryFilter
                {
                    ModuleName = "Funds.Redemption",
                    StartDate = startDate,
                    EndDate = endDate,
                    ResponsibleID = responsibleID,
                    ID = clientID
                };

                //if (!String.IsNullOrEmpty(FundCode))
                //    filterRedemption.TargetText = FundCode;

                if (action != "Aplicar" && action != "Resgatar") 
                { 
                    history.AddRange(LogHelper.LoadHistory(filterApplication));
                    history.AddRange(LogHelper.LoadHistory(filterRedemption));
                }
                else if (action == "Aplicar") {
                    history.AddRange(LogHelper.LoadHistory(filterApplication));
                }
                else {
                    history.AddRange(LogHelper.LoadHistory(filterRedemption));
                }

                Session["Funds.FundHistoryList"] = history;

            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }
        }

        public ActionResult Funds_FundHistory(FormCollection form)
        {
            List<HistoryData> items = (List<HistoryData>)Session["Funds.FundHistoryList"];

            return new DataTablesController().Funds_History(form, items);
        }

        public ActionResult FundsHistoryToFile(bool isPdf)
        {
            return this.HistoryToFile(isPdf
                , "Funds"
                , "Fundos"
                , "Fundo"
                , true
                , "Funds.FundHistoryList" 
                , "Valor");
        }

        public ActionResult HistoryToFile(bool isPdf
            , string clientName
            , string targetTitle
            , string targetColumnTitle
            , bool hasAction
            , string sessionName
            , string extraTargetColumnTitle = null)
        {
            List<HistoryData> currentList = new List<HistoryData>();

            try
            {
                if (Session[sessionName] != null)
                    currentList = (List<HistoryData>)Session[sessionName];


                string name = new Regex(@"\s*").Replace(HttpUtility.HtmlDecode(targetTitle), string.Empty);
                targetTitle = string.Concat(isPdf ? "Histórico de " : "Hist&#243;rico de ", targetTitle);

                string fileName = string.Format("historico_{0}_{1}.{2}", name, DateTime.Now.ToString("dd-MM-yyyy"), isPdf ? "pdf" : "xls");

                return isPdf ? ViewFundHistoryPdf(currentList, true, fileName, targetTitle, targetColumnTitle, hasAction, extraTargetColumnTitle) 
                    : ViewFundHistoryExcel(currentList, true, fileName, targetTitle, targetColumnTitle, hasAction, extraTargetColumnTitle);

            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return this.RedirectToAction("Index");
            }

        }

        #endregion

        public ActionResult SendTermMail(int documentId, string fileName, int fundCode) 
        {

            var client = (FundClient)Session["FundClient"];

            if (client == null)
                return Json(new WcfResponse<bool> { Result = false, Message = "Cliente não encontrado." });
            else{
                using (var service = FundProvider.GetChannel().CreateChannel())
                {
                    return Json(service.FN_SendTermUser(documentId, fileName, fundCode, client));
                }
            }
                
            /*try
            {
                List<KeyValuePair<string, object>> messageValues = new List<KeyValuePair<string, object>>();
                messageValues.Add(new KeyValuePair<string, object>("strNome", client.ClientName));
                messageValues.Add(new KeyValuePair<string, object>("strUrlSite", Request.Url.Scheme + "://" + Request.Url.Host + "/Content"));

                using (var service = FundProvider.GetChannel().CreateChannel()) 
                {
                    var response = service.FN_DownloadDocument(documentId);
                    var responseFundDetails = service.FN_LoadFundDetails(client.ClientCode, fundCode).Result;

                    if (responseFundDetails == null)
                        return Json(new WcfResponse<bool> { Result = false, Message = "Fundo não encontrado." });

                    string subject = "Termo de Adesão: " + responseFundDetails.Name;
                    string emailTo = string.IsNullOrEmpty(ConfigurationManager.AppSettings["EmailSendToMock"]) 
                        ? String.Format("{0}<{1}>", client.ClientName, client.ClientEmail) 
                        : ConfigurationManager.AppSettings["EmailSendToMock"];
                    string emailReplyTo = ConfigurationManager.AppSettings["EmailReplyTo"];

                    Template template = new Template(Server.MapPath("~/Content/Templates/FundTerm.htm"));
                    template.ReplaceVars(messageValues.ToArray());
                    string corpo = template.Body;

                    MailAddress from = new MailAddress(emailReplyTo);
                    MailAddress To = new MailAddress(emailTo);
                    MailMessage msg = new MailMessage();

                    msg.From = from;
                    foreach (string s in emailTo.Split(';'))
                    {
                        int i = msg.To.IndexOf(new MailAddress(s));
                        if ((s.Length > 0) && (i == -1))
                            msg.To.Add(s);
                    }
                    msg.Subject = subject;
                    msg.Body = corpo;
                    msg.IsBodyHtml = true;

                    MemoryStream mst = new MemoryStream(response.Result);

                    msg.Attachments.Add(new Attachment(mst, fileName + ".pdf"));

                    SmtpClient smtp = new SmtpClient();

                    smtp.Send(msg);

                    
    
                }
                return Json(new WcfResponse<bool> { Result = true, Message = "Email enviado com sucesso." });
                
            }
            catch (Exception ex)
            {
                return Json(new WcfResponse<bool> { Result = false, Message = ex.Message });
            }*/
        }

        protected override RedirectResult Redirect(string url)
        {
            return new RedirectHelper(url);
        }
    }
}
