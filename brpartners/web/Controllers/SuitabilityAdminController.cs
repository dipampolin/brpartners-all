﻿using QX3.Portal.Contracts.DataContracts.SuitabilityAdmin;
using QX3.Portal.WebSite.Helper;
using QX3.Portal.WebSite.Models.Channel;
using QX3.Portal.WebSite.SuitabilityService;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace QX3.Portal.WebSite.Controllers
{
    public class SuitabilityAdminController : BaseController
    {
        private ISuitabilityContractChannel CreateService()
        {
            IChannelProvider<ISuitabilityContractChannel> channelProvider = ChannelProviderFactory<ISuitabilityContractChannel>.Create("SuitabilityClassName");
            var service = channelProvider.GetChannel().CreateChannel();
            return service;
        }

        [AccessControlAuthorize]
        public ActionResult Index(string tipo = null, string status = null)
        {
            this.ViewBag.Tipos = new SelectListItem[] {

                new SelectListItem() { Value = "", Text = "Todos", Selected = string.IsNullOrEmpty(tipo)},
                new SelectListItem() { Value = "F", Text = "Pessoa Física", Selected = tipo == "F"},
                new SelectListItem() { Value = "J", Text = "Pessoa Jurídica", Selected = tipo == "J"}
            };

            this.ViewBag.Status = new SelectListItem[] {

                new SelectListItem() { Value = "", Text = "Todos", Selected = string.IsNullOrEmpty(status)},
                new SelectListItem() { Value = "D", Text = "Em Desenvolvimento", Selected = status == "D"},
                new SelectListItem() { Value = "P", Text = "Em Produção", Selected = status == "P"},
                new SelectListItem() { Value = "I", Text = "Em Inativo", Selected = status == "I"}
            };

            var service = CreateService();
            try
            {
                ViewBag.Title = "Gestão de Suitability";
                service.Open();
                var model = service.ListarFormularios(tipo, status);
                return View(model);
            }
            finally
            {
                service.Close();
            }
        }

        [AccessControlAuthorize]
        public ActionResult Edit(int? id = null)
        {
            ViewBag.id = id;
            return View();
        }

        [HttpPost]
        public ActionResult AbrirFormulario(int id)
        {
            var service = CreateService();
            try
            {
                service.Open();
                var form = service.AbrirFormulario(id);
                return Json(form);
            }
            finally
            {
                service.Close();
            }
        }

        [HttpPost]
        public ActionResult Salvar(string json)
        {
            RequisicaoSalvarSuitability requisicao;

            using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(json)))
            {
                var serialiser = new DataContractJsonSerializer(typeof(RequisicaoSalvarSuitability));
                requisicao = (RequisicaoSalvarSuitability)serialiser.ReadObject(ms);
            }

            var service = CreateService();
            EdicaoSuitability form = null;
            try
            {
                service.Open();
                form = service.Salvar(requisicao);
                // return Json(form);
            }
            finally
            {
                service.Close();
            }

            return Json(form);
        }

        [HttpPost]
        public ActionResult Remove(int id)
        {
            var service = CreateService();
            try
            {
                service.Open();
                bool permitido = service.DeletarFormulario(id);
                return Json(new { permitido = permitido });
            }
            finally
            {
                service.Close();
            }
        }
    }
}