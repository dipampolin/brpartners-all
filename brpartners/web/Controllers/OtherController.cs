﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.UI.DataVisualization.Charting;
using Newtonsoft.Json;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.WebSite.AccessControlService;
//using QX3.Portal.WebSite.BrokerageDiscountService;
using QX3.Portal.WebSite.CommonService;
using QX3.Portal.WebSite.CustodyTransferService;
using QX3.Portal.WebSite.Helper;
using QX3.Portal.WebSite.MarketService;
using QX3.Portal.WebSite.Models;
using QX3.Portal.WebSite.Models.Channel;
using QX3.Portal.WebSite.OtherService;
using QX3.Portal.WebSite.Properties;
using QX3.Spinnex.Common.Services.Logging;
using Data = QX3.Portal.Contracts.DataContracts;

namespace QX3.Portal.WebSite.Controllers
{
//    public class OtherController : FileController
//    {
//        #region Correção de Ordens

//        [AccessControlAuthorize]
//        public ActionResult OrdersCorrection()
//        {
//            try
//            {
//                LogHelper.SaveAuditingLog("Other.OrdersCorrection");

//                IChannelProvider<ICommonContractChannel> channelProvider = ChannelProviderFactory<ICommonContractChannel>.Create("CommonClassName");
//                var service = channelProvider.GetChannel().CreateChannel();

//                service.Open();

//                string statusID = null, typeID = null;

//                List<CommonType> status = service.LoadTypes("Other.OrdersCorrection", "status").Result.ToList();
//                ViewBag.Status = new SelectList(status ?? new List<CommonType>(), "value", "description", statusID);


//                List<CommonType> types = service.LoadTypes("Other.OrdersCorrection", "tipo").Result.ToList();
//                ViewBag.Types = new SelectList(types ?? new List<CommonType>(), "value", "description", typeID);
//                service.Close();
//            }

//            catch (Exception ex)
//            {
//                LogManager.WriteError(ex);
//            }
//            return View();
//        }

//        [AcceptVerbs(HttpVerbs.Post)]
//        public JsonResult SearchOrdersCorrections(string Assessors, string Clients, string StatusId, string TypeId, string StartDate, string EndDate)
//        {
//            try
//            {
//                LogHelper.SaveAuditingLog("Other.SearchOrdersCorrections");

//                OrdersCorrectionFilter filter = new OrdersCorrectionFilter();

//                MarketHelper helper = new MarketHelper();
//                filter.Assessors = (String.IsNullOrEmpty(Assessors)) ? "" : helper.GetAssessorsRangesToFilter(Assessors);
//                if (!string.IsNullOrEmpty(Clients))
//                    filter.Clients = helper.GetClientsRangesToFilter(Clients);
//                filter.StartDate = StartDate ?? DateTime.Now.AddDays(-3).ToString("dd/MM/yyyy");
//                filter.EndDate = EndDate ?? DateTime.Now.ToString("dd/MM/yyyy");
//                filter.StatusID = (!string.IsNullOrEmpty(StatusId)) ? Int32.Parse(StatusId) : 0;
//                filter.TypeID = (!string.IsNullOrEmpty(TypeId)) ? Int32.Parse(TypeId) : 0;
//                filter.UserId = CommonHelper.GetLoggedUserGroupId();

//                IChannelProvider<IOtherContractChannel> otherProvider = ChannelProviderFactory<IOtherContractChannel>.Create("OtherClassName");
//                var otherService = otherProvider.GetChannel().CreateChannel();

//                otherService.Open();

//                var list = otherService.LoadOrdersCorrections(filter).Result.ToList();
//                otherService.Close();

//                int pending = CommonHelper.GetPendents("Other.OrdersCorrection");  //helper.GetNumberOfPending("tb_correcao_ordem", 7);

//                Session["Other.OrdersCorrections"] = list;

//                OtherHelper oHelper = new OtherHelper();
//                return Json(new { Error = "", tBody = oHelper.GetOrdersCorrectionTableBody(list), Pending = pending });
//            }
//            catch (Exception ex)
//            {
//                LogManager.WriteError(ex);
//                return Json(new { Error = "Ocorreu um erro ao consultar as correções de ordens.", tBody = "", Pending = 0 });
//            }
//        }

//        public ActionResult OrdersCorrectionRequest(int id)
//        {
//            OrdersCorrection ordersCorrection = new OrdersCorrection();
//            try
//            {
//                LogHelper.SaveAuditingLog("Other.OrdersCorrectionRequest");

//                var descriptions = CommonHelper.LoadSelectTypes("Other.OrdersCorrection", "Descricao", "0", new Contracts.DataContracts.CommonType { Value = "0", Description = "--selecione--" });

//                if (id > 0)
//                {
//                    IChannelProvider<IOtherContractChannel> channelProvider = ChannelProviderFactory<IOtherContractChannel>.Create("OtherClassName");
//                    var service = channelProvider.GetChannel().CreateChannel();

//                    service.Open();
//                    var response = service.LoadOrdersCorrection(id);
//                    service.Close();

//                    if (response.Result != null)
//                    {
//                        var orders = response.Result;
//                        ordersCorrection = response.Result;
//                        if (orders.Type != null && orders.Type.Value != "")
//                        {
//                            descriptions.Where(li => li.Value == orders.Description.Value).SingleOrDefault().Selected = true;
//                            descriptions.Where(li => li.Value == "0").SingleOrDefault().Selected = false;
//                        }
//                    }
//                }

//                ViewBag.OrdersCorrectionDescription = descriptions;
//                ViewBag.OrdersCorrectionTypes = CommonHelper.LoadTypes("Other.OrdersCorrection", "Tipo"); ;
//            }

//            catch (Exception ex)
//            {
//                LogManager.WriteError(ex);
//            }

//            return View(ordersCorrection);
//        }

//        [AcceptVerbs(HttpVerbs.Post)]
//        public JsonResult OrdersCorrectionRequest(FormCollection form)
//        {
//            try
//            {
//                OrdersCorrection correction = new Data.OrdersCorrection();
//                correction.ID = Int32.Parse(form["hdfID"]);
//                correction.ClientFrom = new CommonType { Value = form["txtClientFrom"] };
//                if (!String.IsNullOrEmpty(form["txtClientTo"]))
//                    correction.ClientTo = new CommonType { Value = form["txtClientTo"] };

//                List<OrdersCorrectionDetail> details = new List<OrdersCorrectionDetail>();
//                string[] indexes = form["hdfIndexes"].Split(',');
//                foreach (string index in indexes)
//                    if (index != "")
//                        details.Add(new OrdersCorrectionDetail { StockCode = form["txtStockCode" + index], Quantity = int.Parse(form["txtStockQuantity" + index].Replace(".", "")), Value = decimal.Parse(form["txtStockPrice" + index].Replace(".", "")) });
//                correction.Details = details;

//                correction.Description = new CommonType { Value = form["ddlDescription"] };
//                correction.Responsible = new CommonType { Value = form["hdfResponsibleId"] };
//                correction.Comments = form["txtComments"];
//                correction.Type = new CommonType { Value = form["rblType"] };
//                correction.D1 = form["rblDate"] == "1";
//                correction.Status = new CommonType { Value = "7" }; //Para Efetuar
//                correction.UserID = SessionHelper.CurrentUser.ID;

//                LogHelper.SaveAuditingLog("Other.InsertOrdersCorrection.Post", string.Format(LogResources.AuditingInsertOrdersCorrection, JsonConvert.SerializeObject(correction)));

//                IChannelProvider<IOtherContractChannel> channelProvider = ChannelProviderFactory<IOtherContractChannel>.Create("OtherClassName");
//                var service = channelProvider.GetChannel().CreateChannel();

//                service.Open();
//                var response = service.InsertOrderCorrection(correction);
//                service.Close();

//                if (response.Result > 0)
//                {
//                    MarketHelper helper = new MarketHelper();
//                    int assessorId = correction.Responsible == null ? -1 : Int32.Parse(correction.Responsible.Value);
//                    LogHelper.SaveHistoryLog("Other.OrdersCorrection", new HistoryData { Action = correction.ID > 0 ? "Alterar" : "Solicitar", ID = (correction.ID > 0 ? correction.ID : response.Result), TargetID = Int32.Parse(correction.Responsible.Value), Target = form["lblResponsibleName"] ?? helper.GetAssessorName(assessorId) });
//                    return Json(new { Error = "", Message = correction.ID == 0 ? "Solicitação cadastrada com sucesso." : "Solicitação alterada com sucesso." });
//                }
//                else
//                {
//                    return Json(new { Error = "Não foi possível realizar a solicitação.", Message = "" });
//                }


//            }
//            catch (Exception ex)
//            {
//                LogManager.WriteError(ex);
//                return Json(new { Error = "Ocorreu um erro ao realizar a solicitação.", Message = "" });
//            }
//        }

//        [AcceptVerbs(HttpVerbs.Post)]
//        public void LoadOrdersCorrectionsHistory(FormCollection form)
//        {
//            try
//            {
//                string startDate = form["txtStartDate"];
//                string endDate = form["txtEndDate"];

//                long responsibleID = string.IsNullOrEmpty(form["hdfResponsible"]) ? 0 : long.Parse(form["hdfResponsible"]);
//                int targetID = string.IsNullOrEmpty(form["txtTarget"]) ? 0 : Int32.Parse(form["txtTarget"]);

//                int id = 0;
//                Int32.TryParse(form["hdfCorrectionID"], out id);

//                string action = form["ddlOrdersCorrectionActions"];

//                LogHelper.SaveAuditingLog("Other.LoadOrdersCorrectionsHistory", string.Format(LogResources.AuditingLoadHistory, targetID, responsibleID, action, startDate, endDate));

//                HistoryFilter filter = new HistoryFilter { ModuleName = "Other.OrdersCorrection", StartDate = startDate, Action = action, EndDate = endDate, ResponsibleID = responsibleID, TargetID = targetID, ID = id };

//                Session["OrdersCorrectionHistory"] = LogHelper.LoadHistory(filter);
//            }
//            catch (Exception ex)
//            {
//                LogManager.WriteError(ex);
//            }
//        }

//        public ActionResult OrdersCorrectionHistory()
//        {
//            List<SelectListItem> listItems = new List<SelectListItem>();
//            listItems.Add(new SelectListItem { Text = "Todas", Value = "", Selected = true });

//            try
//            {
//                var actions = OtherHelper.OrdersCorrectionsActions;
//                foreach (string a in actions)
//                    listItems.Add(new SelectListItem { Text = a, Value = a, Selected = false });

//                ViewBag.OrdersCorrectionsActions = listItems;

//                LogHelper.SaveAuditingLog("Other.OrdersCorrectionHistory");
//                return View();
//            }
//            catch (Exception ex)
//            {
//                LogManager.WriteError(ex);
//                TempData["ErrorMessage"] = "Não foi possível carregar o histórico das correções de ordens.";
//                return View();
//            }
//        }

//        public ActionResult OrdersCorrectionHistoryDetail(int correctionID)
//        {
//            try
//            {
//                LogHelper.SaveAuditingLog("Other.OrdersCorrectionHistoryDetail");

//                var corrections = Session["Other.OrdersCorrections"] != null ? (List<OrdersCorrection>)Session["Other.OrdersCorrections"] : new List<OrdersCorrection>();
//                var correction = (from c in corrections where c.ID.Equals(correctionID) select c).FirstOrDefault();

//                HistoryFilter filter = new HistoryFilter { ModuleName = "Other.OrdersCorrection", ID = correctionID };

//                List<HistoryData> data = LogHelper.LoadHistory(filter);

//                Session["OrdersCorrectionHistoryDetail"] = data;
//                ViewBag.CorrectionHistoryDetails = data;

//                return View(correction);
//            }
//            catch (Exception ex)
//            {
//                LogManager.WriteError(ex);
//                TempData["ErrorMessage"] = "Não foi possível carregar o histórico da correção de ordens.";
//                return View(new OrdersCorrection());
//            }
//        }

//        public ActionResult OrdersCorrectionHistoryToFile(bool isPdf, string detail)
//        {
//            return this.HistoryToFile(isPdf, "OrdersCorrection", string.IsNullOrEmpty(detail) ? "OrdersCorrectionHistory" : "OrdersCorrectionHistoryDetail", "Ordens de Corre&#231;&#227;o", "Operador Respons&#225;vel", true);
//        }

//        public ActionResult HistoryToFile(bool isPdf, string targetName, string sessionName, string targetTitle, string targetColumnTitle, bool hasAction)
//        {
//            List<HistoryData> currentList = new List<HistoryData>();

//            try
//            {
//                if (Session[sessionName] != null)
//                    currentList = (List<HistoryData>)Session[sessionName];

//                LogHelper.SaveAuditingLog(string.Concat("Other.HistoryToFile.", targetName), string.Format(LogResources.ObjectToFileParameters, isPdf ? "PDF" : "Excel", JsonConvert.SerializeObject(currentList)));
//            }
//            catch (Exception ex)
//            {
//                LogManager.WriteError(ex);
//                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
//                return this.RedirectToAction("OrdersCorrection");
//            }

//            string name = new Regex(@"\s*").Replace(HttpUtility.HtmlDecode(targetTitle), string.Empty);
//            targetTitle = string.Concat(isPdf ? "Histórico de " : "Hist&#243;rico de ", targetTitle);

//            string fileName = string.Format("historico_{0}_{1}.{2}", name, DateTime.Now.ToString("dd-MM-yyyy"), isPdf ? "pdf" : "xls");

//            return isPdf ? ViewHistoryPdf(currentList, true, fileName, targetTitle, targetColumnTitle, hasAction) : ViewHistoryExcel(currentList, true, fileName, targetTitle, targetColumnTitle, hasAction);
//        }

//        public ActionResult OrdersCorrectionToFile(bool isPdf)
//        {
//            List<OrdersCorrection> currentList = new List<OrdersCorrection>();

//            try
//            {
//                if (Session["Other.OrdersCorrections"] != null)
//                    currentList = (List<OrdersCorrection>)Session["Other.OrdersCorrections"];

//                LogHelper.SaveAuditingLog("Other.OrdersCorrectionToFile", string.Format(LogResources.ObjectToFileParameters, isPdf ? "PDF" : "Excel", JsonConvert.SerializeObject(currentList)));

//                string fileName = string.Format("correcaodeordens_{0}.{1}", DateTime.Now.ToString("dd-MM-yyyy"), isPdf ? "pdf" : "xls");

//                LogHelper.SaveHistoryLog("Other.OrdersCorrection", new HistoryData { Action = "Exportar", OldData = string.Empty, NewData = JsonConvert.SerializeObject(currentList) });

//                return isPdf ? ViewOrdersCorrectionPdf(currentList, true, fileName) : ViewOrdersCorrectionExcel(currentList, true, fileName);
//            }
//            catch (Exception ex)
//            {
//                LogManager.WriteError(ex);
//                TempData["ErrorMessage"] = "Ocorreu um erro ao gerar relatório.";
//                return this.RedirectToAction("OrdersCorrection");
//            }


//        }

//        public ActionResult OrdersCorrectionConfirmDelete(int correctionId)
//        {
//            var corrections = Session["Other.OrdersCorrections"] != null ? (List<OrdersCorrection>)Session["Other.OrdersCorrections"] : new List<OrdersCorrection>();
//            var correction = (from c in corrections where c.ID.Equals(correctionId) select c).FirstOrDefault();
//            return View(correction);
//        }

//        public ActionResult OrdersCorrectionConfirmRealize(string correctionIds)
//        {
//            List<string> ids = new List<string>();
//            ids = correctionIds.Split(';').ToList();
//            return View(ids);
//        }

//        [AcceptVerbs(HttpVerbs.Post)]
//        public JsonResult DeleteOrdersCorrection(string hdfCorrectionId, string hdfAssessorId, string hdfAssessor)
//        {
//            try
//            {
//                LogHelper.SaveAuditingLog("Other.DeleteOrdersCorrection", string.Format(LogResources.AuditingDeleteOrdersCorrection, hdfCorrectionId));

//                IChannelProvider<IOtherContractChannel> channelProvider = ChannelProviderFactory<IOtherContractChannel>.Create("OtherClassName");
//                var service = channelProvider.GetChannel().CreateChannel();

//                service.Open();

//                int correctionId = Int32.Parse(hdfCorrectionId);
//                var changed = service.DeleteOrderCorrection(correctionId).Result;

//                service.Close();

//                if (changed)
//                {
//                    LogHelper.SaveHistoryLog("Other.OrdersCorrection", new HistoryData { Action = "Excluir", ID = correctionId, TargetID = Int32.Parse(hdfAssessorId), Target = hdfAssessor });
//                    Session["Other.OrdersCorrections"] = null;
//                    return Json(new { Error = "", Message = "Solicitação excluída com sucesso." });
//                }
//                else
//                {
//                    return Json(new { Error = "Não foi possível exluir solicitação.", Message = "" });
//                }
//            }
//            catch (Exception ex)
//            {
//                LogManager.WriteError(ex);
//                return Json(new { Error = "Ocorreu um erro ao exluir solicitação.", Message = "" });
//            }
//        }

//        [AcceptVerbs(HttpVerbs.Post)]
//        public JsonResult RealizeOrdersCorrection(string correctionIds)
//        {
//            try
//            {
//                LogHelper.SaveAuditingLog("Other.RealizeOrdersCorrection", string.Format(LogResources.AuditingDeleteOrdersCorrection, correctionIds));

//                IChannelProvider<IOtherContractChannel> channelProvider = ChannelProviderFactory<IOtherContractChannel>.Create("OtherClassName");
//                var service = channelProvider.GetChannel().CreateChannel();

//                service.Open();

//                string[] hdfIds = correctionIds.Split(';');
//                bool changed = true;

//                var corrections = Session["Other.OrdersCorrections"] != null ? (List<OrdersCorrection>)Session["Other.OrdersCorrections"] : new List<OrdersCorrection>();

//                foreach (string hdfId in hdfIds)
//                {
//                    if (!string.IsNullOrEmpty(hdfId))
//                    {
//                        int correctionId = Int32.Parse(hdfId);
//                        if (!service.ChangeOrdersCorrectionStatus(correctionId, 8).Result)
//                            changed = false;
//                        else
//                        {
//                            var correction = corrections.Where(c => c.ID.Equals(correctionId)).FirstOrDefault();
//                            LogHelper.SaveHistoryLog("Other.OrdersCorrection", new HistoryData { Action = "Efetuar", ID = correctionId, TargetID = Int32.Parse(correction.Responsible.Value), Target = correction.Responsible.Description });
//                        }
//                    }
//                }

//                service.Close();

//                if (changed)
//                {
//                    Session["Other.OrdersCorrections"] = null;
//                    return Json(new { Error = "", Message = "Solicitação efetuada com sucesso." });
//                }
//                else
//                {
//                    return Json(new { Error = "", Message = "Não foi possível efetuar solicitação." });
//                }
//            }
//            catch (Exception ex)
//            {
//                LogManager.WriteError(ex);
//                return Json(new { Error = "", Message = "Ocorreu um erro ao efetuar a solicitação." });
//            }
//        }

//        public JsonResult AssessorNameSuggest(string term)
//        {
//            List<CommonType> clients = new List<CommonType>();

//            try
//            {
//                IChannelProvider<IMarketContractChannel> channelProvider = ChannelProviderFactory<IMarketContractChannel>.Create("MarketClassName");
//                var service = channelProvider.GetChannel().CreateChannel();

//                service.Open();
//                clients = service.AssessorNameSuggest(term).Result.ToList();
//                service.Close();
//            }
//            catch (Exception ex)
//            {
//                LogManager.WriteError(ex);
//            }

//            return Json(clients, JsonRequestBehavior.AllowGet);

//        }

//        #endregion

//        #region Transferência de Custódia

//        private IChannelProvider<ICustodyTransferContractChannel> cstTrnsf;
//        protected IChannelProvider<ICustodyTransferContractChannel> CustodyTransferProvider
//        {
//            get
//            {
//                if (cstTrnsf == null)
//                    cstTrnsf = ChannelProviderFactory<ICustodyTransferContractChannel>.Create("CustodyTransferClassName");
//                return cstTrnsf;
//            }
//        }

//        [HttpPost]
//        public string InitialCustodyTransferFilter(FormCollection form)
//        {
//            WcfResponse<List<CustodyTransfer>, int> cstList = new WcfResponse<List<CustodyTransfer>, int>();

//            using (var custodyTransferService = CustodyTransferProvider.GetChannel().CreateChannel())
//            {
//                custodyTransferService.Open();

//                var broker = new CustodyTransferFilter()
//                {
//                    Status = (String.IsNullOrEmpty(form["ddlSituation"])) ? null : form["ddlSituation"]
//                };

//                var options = new FilterOptions()
//                {
//                    InitRow = Convert.ToInt32(form["iDisplayStart"]),
//                    FinalRow = Convert.ToInt32(form["iDisplayLength"]),
//                    SortColumn = (form["iSortCol_0"] == "null" || form["iSortCol_0"] == "undefined" || form["iSortCol_0"] == "0") ? 1 : Convert.ToInt32(form["iSortCol_0"]),
//                    SortDirection = form["sSortDir_0"],
//                    AllRecords = false
//                };

//                cstList = custodyTransferService.LoadCustodyTransfers(broker, options, CommonHelper.GetLoggedUserGroupId());

//                custodyTransferService.Close();

//                Session["Other.CustodyTransfers"] = cstList.Result;
//                Session["Other.CustodyTransfersFilter"] = broker;
//                Session["Other.CustodyTransfersFilterOptions"] = options;
//            }
//            //}
//            return new CustodyTransferHelper().CustodyTransferListObject(cstList.Result, ref form, cstList.Data);
//        }

//        [HttpPost]
//        public string CustodyTransferFilter(FormCollection form)
//        {
//            WcfResponse<List<CustodyTransfer>, int> cstList = new WcfResponse<List<CustodyTransfer>, int>();

//            using (var custodyTransferService = CustodyTransferProvider.GetChannel().CreateChannel())
//            {
//                custodyTransferService.Open();

//                string assessor = (!String.IsNullOrEmpty(form["txtAssessor"])) ? new MarketHelper().GetAssessorsRangesToFilter(form["txtAssessor"]) : "";

//                var broker = new CustodyTransferFilter()
//                {
//                    Assessor = assessor,
//                    Status = form["ddlStatus"],
//                    CustodyTransferType = form["ddlTransferType"]
//                };

//                if (!string.IsNullOrEmpty(form["txtClientFilter"]))
//                    broker.ClientFilter = new MarketHelper().GetClientsRangesToFilter(form["txtClientFilter"]);

//                if (!String.IsNullOrEmpty(form["txtStartDate"])) broker.InitialRequest = Convert.ToDateTime(form["txtStartDate"]);
//                if (!String.IsNullOrEmpty(form["txtEndDate"])) broker.FinalRequest = Convert.ToDateTime(form["txtEndDate"]);


//                var options = new FilterOptions()
//                {
//                    InitRow = Convert.ToInt32(form["iDisplayStart"]),
//                    FinalRow = Convert.ToInt32(form["iDisplayLength"]),
//                    SortColumn = (form["iSortCol_0"] == "null" || form["iSortCol_0"] == "undefined" || form["iSortCol_0"] == "0") ? 1 : Convert.ToInt32(form["iSortCol_0"]),
//                    SortDirection = form["sSortDir_0"],
//                    AllRecords = false
//                };

//                cstList = custodyTransferService.LoadCustodyTransfers(broker, options, CommonHelper.GetLoggedUserGroupId());

//                custodyTransferService.Close();

//                Session["Other.CustodyTransfers"] = cstList.Result;
//                Session["Other.CustodyTransfersFilter"] = broker;
//                Session["Other.CustodyTransfersFilterOptions"] = options;
//            }

//            return new CustodyTransferHelper().CustodyTransferListObject(cstList.Result, ref form, cstList.Data);
//        }

//        [HttpPost]
//        public ActionResult GetCustodyTransferStatus(string status)
//        {
//            var results = new List<CommonType>();
//            results.Add(new CommonType() { Value = "--", Description = "Selecione" });

//            AccessControlHelper acHelper = new AccessControlHelper();

//            switch (status)
//            {
//                case "A":
//                    {
//                        if (acHelper.HasFuncionalityPermission("Other.CustodyTransfer", "btnEffective"))
//                            results.Add(new CommonType() { Value = "E", Description = "Efetuado" });

//                        break;
//                    }
//                default: break;
//            }

//            return Json(results);
//        }

//        [HttpPost]
//        public ActionResult ApproveCustodyTransfer(FormCollection form)
//        {
//            string[] ids = form["ids"].Split(';');
//            string status = form["ddlStatus"];

//            List<CustodyTransfer> requests = new List<CustodyTransfer>();

//            var list = (List<CustodyTransfer>)Session["Other.CustodyTransfers"];

//            foreach (string i in ids)
//            {
//                requests.Add(new CustodyTransfer()
//                {
//                    RequestId = Convert.ToInt32(i.Split('|')[0]),
//                    ClientCode = Convert.ToInt32(i.Split('|')[1]),
//                    Status = form["ddlStatus"]
//                });

//                string action = "";

//                if ((form["ddlStatus"] == "E")) action = "Efetuar";
//                else if ((form["ddlStatus"] == "R")) action = "Reprovar";

//                LogHelper.SaveHistoryLog("Other.CustodyTransfer",
//                    new HistoryData
//                    {
//                        Action = action,
//                        ID = Convert.ToInt32(i.Split('|')[0]),
//                        Target = (list != null)
//                                            ? (list.Where(a => a.RequestId == Convert.ToInt32(i.Split('|')[0])).FirstOrDefault() != null)
//                                                ? list.Where(a => a.RequestId == Convert.ToInt32(i.Split('|')[0])).FirstOrDefault().ClientName
//                                                : "-"
//                                            : "-",
//                        TargetID = Convert.ToInt32(i.Split('|')[1]),
//                        ExtraTarget = (list != null)
//                                            ? (list.Where(a => a.RequestId == Convert.ToInt32(i.Split('|')[0])).FirstOrDefault() != null)
//                                                ? list.Where(a => a.RequestId == Convert.ToInt32(i.Split('|')[0])).FirstOrDefault().StockCode
//                                                : "-"
//                                            : "-"
//                    });
//            }

//            WcfResponse<bool> response = null;

//            using (var custodyTransferService = CustodyTransferProvider.GetChannel().CreateChannel())
//            {
//                response = custodyTransferService.UpdateCustodyTransferStatus(requests);
//                if (!response.Result)
//                    response.Message = "Algumas notas de desconto não puderam ser atualizadas.";
//                else
//                {
//                    response.Message = "Notas de desconto atualizadas com sucesso.";
//                }
//            }

//            return Json(response);
//        }

//        [HttpPost]
//        public ActionResult RemoveCustodyTransfer(string id)
//        {
//            LogHelper.SaveAuditingLog("Other.CustodyTransfer.Remove");
//            var list = (List<CustodyTransfer>)Session["Other.CustodyTransfers"];

//            var requests = new List<CustodyTransfer>();
//            requests.Add(new CustodyTransfer()
//            {
//                RequestId = Convert.ToInt32(id.Split('|')[0]),
//                ClientCode = Convert.ToInt32(id.Split('|')[1])
//            });

//            WcfResponse<bool> response = null;
//            using (var custodyTransferService = CustodyTransferProvider.GetChannel().CreateChannel())
//            {
//                response = custodyTransferService.DeleteCustodyTransfer(requests);
//                if (!response.Result)
//                    response.Message = "Algumas solitações de transferência não puderam ser canceladas.";
//                else
//                {
//                    response.Message = "Solitação de transferência cancelada com sucesso.";

//                    LogHelper.SaveHistoryLog("Other.CustodyTransfer",
//                    new HistoryData
//                    {
//                        Action = "Excluir",
//                        ID = Convert.ToInt32(id.Split('|')[0]),
//                        TargetID = Convert.ToInt32(id.Split('|')[1]),
//                        ExtraTarget = (list != null)
//                                            ? (list.Where(a => a.RequestId == Convert.ToInt32(id.Split('|')[0])).FirstOrDefault() != null)
//                                                ? list.Where(a => a.RequestId == Convert.ToInt32(id.Split('|')[0])).FirstOrDefault().StockCode
//                                                : "-"
//                                            : "-",
//                        Target = (list != null)
//                                            ? (list.Where(a => a.RequestId == Convert.ToInt32(id.Split('|')[0])).FirstOrDefault() != null)
//                                                ? list.Where(a => a.RequestId == Convert.ToInt32(id.Split('|')[0])).FirstOrDefault().ClientName
//                                                : "-"
//                                            : "-"
//                    });
//                }
//            }

//            return Json(response);
//        }

//        [HttpPost]
//        public int GetPendingCustodyTransfers()
//        {
//            return new CustodyTransferHelper().GetCustodyTransferPendents();
//        }

//        [AccessControlAuthorize]
//        public ActionResult CustodyTransfer()
//        {
//            ViewBag.Pending = new CustodyTransferHelper().GetCustodyTransferPendents();

//            return View();
//        }

//        [HttpPost]
//        public ActionResult GetClientPosition(int clientCode)
//        {
//            var positions = new List<CustodyTransferDetails>();
//            using (var CustodyTransferService = CustodyTransferProvider.GetChannel().CreateChannel())
//            {
//                string assessor = null;
//                if (SessionHelper.CurrentUser.Assessors != null)
//                    foreach (int i in SessionHelper.CurrentUser.Assessors)
//                        assessor += i.ToString() + ";";

//                positions = CustodyTransferService.LoadClientPosition(new CustodyTransferFilter() { ClientCode = clientCode, Assessor = assessor }, CommonHelper.GetLoggedUserGroupId()).Result;
//            }

//            return Json(positions);
//        }

//        public ActionResult CustodyTransferRequest(string id)
//        {

//            Response.Cache.SetCacheability(HttpCacheability.NoCache);

//            if (!String.IsNullOrEmpty(id))
//            {
//                var list = Session["Other.CustodyTransfers"] != null ? (List<CustodyTransfer>)Session["Other.CustodyTransfers"] : new List<CustodyTransfer>();
//                var brk = (from c in list where c.RequestId.Equals(Convert.ToInt32(id.Split('|')[0])) && c.ClientCode.Equals(Convert.ToInt32(id.Split('|')[1])) select c).FirstOrDefault();

//                using (var service = CustodyTransferProvider.GetChannel().CreateChannel())
//                {

//                    var result = service.LoadClientPosition(new CustodyTransferFilter() { ClientCode = brk.ClientCode, Assessor = null }, null).Result;

//                    if (result != null)
//                    {
//                        if (result.Where(a => a.StockCode.Trim().ToUpper().Equals(brk.StockCode.Trim().ToUpper())).FirstOrDefault() != null)
//                        {
//                            ViewBag.OldPosition = result.Where(a => a.StockCode.Trim().ToUpper().Equals(brk.StockCode.Trim().ToUpper())).FirstOrDefault().StockQtty;
//                        }
//                        else { ViewBag.OldPosition = 0; }
//                    }
//                }

//                return View(brk);
//            }
//            else
//            {
//                return View();
//            }
//        }

//        private bool ProcessRequest(FormCollection form, bool isNew)
//        {
//            List<CustodyTransfer> requests = new List<CustodyTransfer>();

//            CustodyTransfer initialRequest = new CustodyTransfer()
//            {
//                ClientCode = Convert.ToInt32(form["txtClient"]),
//                DestinyBrokerCode = Convert.ToInt32(form["hdfDestinyBroker"]),
//                DestinyPortfolio = Convert.ToInt32(form["txtDestinyPortfolio"]),
//                DestinyClientCode = Convert.ToInt32(form["txtDestinyClientCode"]),
//                Observations = Regex.Replace(form["txtObservation"], "<.*?>", "")
//            };

//            // Especificar
//            String[] sStockCode = form.AllKeys.Where(a => a.Contains("hdfStockCode_")).ToArray();

//            for (int i = 0; i < sStockCode.Length; i++)
//            {
//                string order = sStockCode[i].Replace("hdfStockCode_", "");

//                if (!String.IsNullOrEmpty(form["txtStockQtty_" + order]))
//                {

//                    var request = new CustodyTransfer()
//                    {
//                        ClientCode = initialRequest.ClientCode,
//                        DestinyBrokerCode = initialRequest.DestinyBrokerCode,
//                        DestinyPortfolio = initialRequest.DestinyPortfolio,
//                        DestinyClientCode = initialRequest.DestinyClientCode,
//                        Observations = initialRequest.Observations
//                    };

//                    request.StockCode = form["hdfStockCode_" + order];
//                    request.Qtty = Convert.ToDecimal(form["txtStockQtty_" + order]);

//                    request.OrigPortfolio = Convert.ToInt32(form["txtOrigPortfolio_" + order]);

//                    if (!isNew)
//                        request.RequestId = Convert.ToInt32(form["hdfEditRequestId"]);

//                    requests.Add(request);
//                }
//            }

//            bool response = true;
//            using (var custodyTransferService = CustodyTransferProvider.GetChannel().CreateChannel())
//            {

//                foreach (var req in requests)
//                {
//                    WcfResponse<bool, int> resp = null;

//                    if (isNew)
//                        resp = custodyTransferService.InsertCustodyTransfer(req);
//                    else
//                        resp = custodyTransferService.EditCustodyTransfer(req);

//                    if (resp.Result)
//                    {
//                        IChannelProvider<QX3.Portal.WebSite.NonResidentTradersService.INonResidentTradersServiceContractChannel> channelProvider =
//                            ChannelProviderFactory<QX3.Portal.WebSite.NonResidentTradersService.INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
//                        var nonresidentService = channelProvider.GetChannel().CreateChannel();

//                        nonresidentService.Open();

//                        LogHelper.SaveHistoryLog("Other.CustodyTransfer",
//                                new HistoryData
//                                {
//                                    Action = (isNew) ? "Solicitar" : "Alterar",
//                                    NewData = JsonConvert.SerializeObject(req),
//                                    ID = (resp.Data == 0) ? req.RequestId : resp.Data,
//                                    TargetID = Convert.ToInt32(form["txtClient"]),
//                                    Target = nonresidentService.LoadCustomerName(Convert.ToInt32(form["txtClient"])).Result,
//                                    ExtraTarget = req.StockCode
//                                });

//                        nonresidentService.Close();
//                    }
//                    else if (!resp.Result && response)
//                        response = false;
//                }
//            }
//            return response;
//        }

//        [HttpPost]
//        public ActionResult CustodyTransferRequest(FormCollection form)
//        {
//            LogHelper.SaveAuditingLog("Other.CustodyTransfer.Request");

//            var result = this.ProcessRequest(form, true);

//            return Json(new WcfResponse<bool>() { Result = result, Message = (result) ? "Solicitações criadas com sucesso." : "Algumas solicitações não puderam ser criadas." });
//        }

//        [HttpPost]
//        public ActionResult CustodyTransferRequestUpdate(FormCollection form)
//        {
//            LogHelper.SaveAuditingLog("Other.CustodyTransfer.Edit");

//            var result = this.ProcessRequest(form, false);

//            return Json(new WcfResponse<bool>() { Result = result, Message = (result) ? "Solicitação editada com sucesso." : "Solicitação não pode ser editada." });
//        }

//        public ActionResult CustodyTransferRealize(string requestId, string status)
//        {
//            Response.Cache.SetCacheability(HttpCacheability.NoCache);

//            ViewBag.Ids = requestId;
//            ViewBag.Status = status;

//            return View();
//        }

//        public ActionResult CustodyTransferDelete(string requestId)
//        {
//            Response.Cache.SetCacheability(HttpCacheability.NoCache);

//            var custodies = (List<CustodyTransfer>)Session["Other.CustodyTransfers"];
//            var custody = custodies.Where(a => a.RequestId == Convert.ToInt32(requestId.Split('|')[0]) && a.ClientCode == Convert.ToInt32(requestId.Split('|')[1])).FirstOrDefault();

//            return View(custody);
//        }

//        [HttpGet]
//        public ActionResult CustodyTransferHistoryDetail(string requestId)
//        {
//            Response.Cache.SetCacheability(HttpCacheability.NoCache);

//            try
//            {
//                LogHelper.SaveAuditingLog("Other.CustodyTransferHistoryDetail");

//                var list = Session["Other.CustodyTransfers"] != null ? (List<CustodyTransfer>)Session["Other.CustodyTransfers"] : new List<CustodyTransfer>();
//                var brk = (from c in list where c.RequestId.Equals(Convert.ToInt32(requestId.Split('|')[0])) select c).FirstOrDefault();

//                List<HistoryData> data = new List<HistoryData>();
//                AccessControlHelper helper = new AccessControlHelper();
//                HistoryFilter filter = new HistoryFilter { ModuleName = "Other.CustodyTransfer", ID = brk.RequestId };

//                data = LogHelper.LoadHistory(filter);

//                Session["Other.CustodyTransferHistoryListDetail"] = data;
//                ViewBag.CustodyTransferHistoryDetails = data;

//                return View(brk);
//            }
//            catch (Exception ex)
//            {
//                LogManager.WriteError(ex);
//                TempData["ErrorMessage"] = "Não foi possível carregar o histórico desta solicitação.";
//                return View(new BrokerageDiscount());
//            }
//        }

//        [HttpGet]
//        public ActionResult CustodyTransferHistory()
//        {
//            List<SelectListItem> listItems = new List<SelectListItem>();
//            listItems.Add(new SelectListItem { Text = "Todas", Value = "", Selected = true });

//            try
//            {
//                var actions = OtherHelper.CustodyTransferActions;
//                foreach (string action in actions)
//                    listItems.Add(new SelectListItem { Text = action, Value = action, Selected = false });

//                ViewBag.CustodyTransferActions = listItems;

//                LogHelper.SaveAuditingLog("Other.CustodyTransfer.History");
//                return View();
//            }

//            catch (Exception ex)
//            {
//                LogManager.WriteError(ex);
//                TempData["ErrorMessage"] = "Não foi possível carregar o histórico de transferências.";
//                return View();
//            }
//        }

//        [HttpPost]
//        public void LoadCustodyTransferHistory(FormCollection form)
//        {
//            try
//            {
//                string startDate = form["txtStartDate"];
//                string endDate = form["txtEndDate"];

//                long responsibleID = string.IsNullOrEmpty(form["hdfResponsible"]) ? 0 : long.Parse(form["hdfResponsible"]);
//                int clientID = string.IsNullOrEmpty(form["txtClient"]) ? 0 : Int32.Parse(form["txtClient"]);
//                string StockCode = string.IsNullOrEmpty(form["txtFundCode"]) ? "" : form["txtFundCode"];

//                int id = 0;
//                Int32.TryParse(form["hdfRequestID"], out id);

//                string action = form["ddlFundActions"];

//                LogHelper.SaveAuditingLog("Funds.CustodyTransferHistory",
//                    string.Format(LogResources.AuditingLoadHistory, clientID, responsibleID, action, startDate, endDate));

//                HistoryFilter filter = new HistoryFilter
//                {
//                    ModuleName = "Other.CustodyTransfer",
//                    StartDate = startDate,
//                    Action = action,
//                    EndDate = endDate,
//                    ResponsibleID = responsibleID,
//                    TargetID = clientID,
//                    ID = id,
//                    ExtraTargetText = StockCode
//                };


//                Session["Other.CustodyTransferHistoryList"] = LogHelper.LoadHistory(filter);

//            }
//            catch (Exception ex)
//            {
//                LogManager.WriteError(ex);
//            }

//        }

//        public ActionResult CustodyTransferHistoryToFile(bool isPdf, string detail)
//        {
//            return this.HistoryToFile(isPdf, "CustodyTransfer", "Transfer&#234;ncia de Cust&#243;dia", "Cliente", true, (detail == "false") ? "Other.CustodyTransferHistoryList" : "Other.CustodyTransferHistoryListDetail", "Papel");
//        }

//        public ActionResult Other_CustodyTransferHistory(FormCollection form)
//        {
//            List<HistoryData> items = (List<HistoryData>)Session["Other.CustodyTransferHistoryList"];
//            return new DataTablesController().AccessControl_History(form, items);
//        }

//        public ActionResult CustodyTransferDetail()
//        {
//            return View();
//        }

//        public ActionResult CustodyTransferReports()
//        {
//            Session["CustodyTransferReport"] = null;
//            Session["CustodyTransferReportChart"] = null;
//            DateTime fromDate = QX3.Portal.WebSite.Models.CommonHelper.GetFirstWeekday(DateTime.Today.Month, DateTime.Today.Year);
//            ViewBag.Report = new CustodyTransferReport { FromDate = fromDate, ToDate = DateTime.Today };
//            return View();
//        }

//        public ActionResult CustodyTransferReportsRequest(string fromDate, string toDate)
//        {
//            try
//            {
//                DateTime FromDate1 = Convert.ToDateTime(fromDate);
//                DateTime ToDate1 = Convert.ToDateTime(toDate);
//                CustodyTransferReport report = new CustodyTransferReport { FromDate = FromDate1, ToDate = ToDate1 };

//                if (fromDate != null
//                    && toDate != null
//                    && fromDate.Length > 0
//                    && toDate.Length > 0
//                    && DateTime.TryParse(fromDate, out FromDate1)
//                    && DateTime.TryParse(toDate, out ToDate1))
//                {
//                    LogHelper.SaveAuditingLog("Other.CustodyTransferReports");

//                    IChannelProvider<ICustodyTransferContractChannel> channelProvider = ChannelProviderFactory<ICustodyTransferContractChannel>.Create("CustodyTransferClassName");
//                    ICustodyTransferContractChannel service = channelProvider.GetChannel().CreateChannel();

//                    service.Open();
//                    WcfResponse<CustodyTransferReport> result = service.LoadCustodyTransferReport(FromDate1.ToShortDateString(), ToDate1.ToShortDateString());
//                    report = result.Result;
//                    service.Close();
//                }

//                if (report.ChartOrigin.Count == 0 && report.ChartDestination.Count == 0)
//                {
//                    Session["CustodyTransferReportChart"] = null;
//                }

//                Session["CustodyTransferReport"] = report;
//                ViewBag.Report = report;
//            }
//            catch (Exception ex)
//            {
//                LogManager.WriteError(ex);
//            }
//            return View();
//        }

//        public ActionResult CustodyTransferReportsChart()
//        {
//            if (Session["CustodyTransferReport"] != null)
//            {
//                CustodyTransferReport report = (CustodyTransferReport)Session["CustodyTransferReport"];

//                Chart Chart1 = new Chart();
//                Chart1.AntiAliasing = AntiAliasingStyles.All;
//                Chart1.TextAntiAliasingQuality = TextAntiAliasingQuality.High;

//                Chart1.Width = 738;
//                Chart1.Height = 202;

//                Chart1.ChartAreas.Add("ChartArea1");
//                Chart1.ChartAreas["ChartArea1"].AxisY.LabelStyle = new LabelStyle() { Format = "{0:C0}" };

//                Chart1.ChartAreas["ChartArea1"].AxisY.LabelStyle.Font = new Font("Arial", 8, FontStyle.Regular);
//                Chart1.ChartAreas["ChartArea1"].AxisX.LabelStyle.Font = new Font("Arial", 8, FontStyle.Regular);

//                Chart1.ChartAreas["ChartArea1"].AxisX.MajorGrid.LineColor = Color.LightGray;
//                Chart1.ChartAreas["ChartArea1"].AxisX.MinorGrid.LineColor = Color.LightGray;
//                Chart1.ChartAreas["ChartArea1"].AxisY.MajorGrid.LineColor = Color.LightGray;
//                Chart1.ChartAreas["ChartArea1"].AxisY.MinorGrid.LineColor = Color.LightGray;

//                Chart1.ChartAreas["ChartArea1"].Position.Auto = false;
//                Chart1.ChartAreas["ChartArea1"].Position.X = 0;
//                Chart1.ChartAreas["ChartArea1"].Position.Y = 11;
//                Chart1.ChartAreas["ChartArea1"].Position.Width = 97;
//                Chart1.ChartAreas["ChartArea1"].Position.Height = 90;

//                Chart1.ChartAreas["ChartArea1"].BackColor = Color.White;
//                Chart1.ChartAreas["ChartArea1"].BorderColor = Color.Gray;


//                Chart1.Series.Add("Origem");
//                Chart1.Series["Origem"].ChartType = SeriesChartType.Line;
//                Chart1.Series["Origem"].IsValueShownAsLabel = true;
//                Chart1.Series["Origem"].LabelFormat = "{0:C}";
//                Chart1.Series["Origem"].Font = new Font("Arial", 8, FontStyle.Regular);
//                Chart1.Series["Origem"].LabelForeColor = Color.DarkRed;
//                Chart1.Series["Origem"].MarkerStyle = MarkerStyle.Circle;
//                Chart1.Series["Origem"].MarkerColor = Color.DarkRed;
//                Chart1.Series["Origem"].MarkerSize = 6;
//                Chart1.Series["Origem"].Color = Color.Red;
//                Chart1.Series["Origem"].BorderWidth = 2;

//                foreach (CustodyTransferChart x in report.ChartOrigin)
//                {
//                    Chart1.Series["Origem"].Points.AddXY(x.Date.ToString("MM/yyyy"), x.Amount);
//                }


//                Chart1.Series.Add("Destino");
//                Chart1.Series["Destino"].ChartType = SeriesChartType.Line;
//                Chart1.Series["Destino"].IsValueShownAsLabel = true;
//                Chart1.Series["Destino"].LabelFormat = "{0:C}";
//                Chart1.Series["Destino"].Font = new Font("Arial", 8, FontStyle.Regular);
//                Chart1.Series["Destino"].LabelForeColor = Color.DarkGreen;
//                Chart1.Series["Destino"].MarkerStyle = MarkerStyle.Circle;
//                Chart1.Series["Destino"].MarkerColor = Color.DarkGreen;
//                Chart1.Series["Destino"].MarkerSize = 6;
//                Chart1.Series["Destino"].Color = Color.Green;
//                Chart1.Series["Destino"].BorderWidth = 2;

//                foreach (CustodyTransferChart x in report.ChartDestination)
//                {
//                    Chart1.Series["Destino"].Points.AddXY(x.Date.ToString("MM/yyyy"), x.Amount);
//                }


//                Chart1.Legends.Add("Default");
//                Chart1.Legends["Default"].LegendStyle = LegendStyle.Row;
//                Chart1.Legends["Default"].Docking = Docking.Top;
//                Chart1.Legends["Default"].Position.Auto = false;
//                Chart1.Legends["Default"].Position.X = 0;
//                Chart1.Legends["Default"].Position.Y = 0;
//                Chart1.Legends["Default"].Position.Width = 25;
//                Chart1.Legends["Default"].Position.Height = 10;
//                Chart1.Legends["Default"].Font = new Font("Arial", 8, FontStyle.Regular);


//                MemoryStream memoryStream = new MemoryStream();
//                Chart1.SaveImage(memoryStream, ChartImageFormat.Png);
//                memoryStream.Seek(0, SeekOrigin.Begin);

//                /*** Gráfico para exportação para PDF ***/
//                MemoryStream memoryStream1 = new MemoryStream();
//                Chart1.Width = 918;
//                Chart1.Height = 251;
//                Chart1.SaveImage(memoryStream1, ChartImageFormat.Png);
//                memoryStream1.Seek(0, SeekOrigin.Begin);
//                Session["CustodyTransferReportChart"] = memoryStream1.ToArray();
//                /*** Fim - Gráfico para exportação para PDF ***/

//                return File(memoryStream.ToArray(), "image/png", "mychart.png");
//            }
//            else
//            {
//                return null;
//            }
//        }

//        public ActionResult CustodyTransferToFile(bool isPdf)
//        {
//            List<CustodyTransfer> currentList = new List<CustodyTransfer>();
//            try
//            {
//                if (Session["Other.CustodyTransfersFilter"] != null && Session["Other.CustodyTransfersFilterOptions"] != null)
//                {
//                    CustodyTransferFilter filter = (CustodyTransferFilter)Session["Other.CustodyTransfersFilter"];
//                    FilterOptions options = (FilterOptions)Session["Other.CustodyTransfersFilterOptions"];
//                    options.AllRecords = true;

//                    ICustodyTransferContractChannel contractChannel = CustodyTransferProvider.GetChannel().CreateChannel();
//                    contractChannel.Open();
//                    currentList = contractChannel.LoadCustodyTransfers(filter, options, CommonHelper.GetLoggedUserGroupId()).Result;
//                    contractChannel.Close();
//                }

//                //LogHelper.SaveAuditingLog("Other.CustodyTransferToFile", string.Format(LogResources.ObjectToFileParameters, isPdf ? "PDF" : "Excel", JsonConvert.SerializeObject(currentList)));

//                string fileName = string.Format("transferenciadecustodia_{0}.{1}", DateTime.Now.ToString("dd-MM-yyyy"), isPdf ? "pdf" : "xls");

//                LogHelper.SaveHistoryLog("Other.CustodyTransfer", new HistoryData { Action = "Exportar", OldData = string.Empty, NewData = JsonConvert.SerializeObject(currentList) });

//                return isPdf ? ViewCustodyTransferPdf(currentList, true, fileName) : ViewCustodyTransferExcel(currentList, true, fileName);
//            }
//            catch (Exception ex)
//            {
//                LogManager.WriteError(ex);
//                TempData["ErrorMessage"] = "Ocorreu um erro ao gerar relatório.";
//                return this.RedirectToAction("CustodyTransfer");
//            }

//        }

//        public ActionResult CustodyTransferReportsToFile()
//        {
//            CustodyTransferReport report = new CustodyTransferReport();
//            Byte[] reportChart = null;
//            try
//            {
//                if (Session["CustodyTransferReport"] != null)
//                {
//                    report = (CustodyTransferReport)Session["CustodyTransferReport"];
//                }

//                if (Session["CustodyTransferReportChart"] != null)
//                {
//                    reportChart = (Byte[])Session["CustodyTransferReportChart"];
//                }

//                LogHelper.SaveAuditingLog("Other.CustodyTransfer", string.Format(LogResources.ObjectToFileParameters, "PDF", JsonConvert.SerializeObject(report)));

//                string fileName = string.Format("relatoriotransferenciadecustodia_{0}.{1}", DateTime.Now.ToString("dd-MM-yyyy"), "pdf");

//                LogHelper.SaveHistoryLog("Other.CustodyTransfer", new HistoryData { Action = "Exportar", OldData = string.Empty, NewData = JsonConvert.SerializeObject(report) });

//                return ViewCustodyTransferReportPdf(report, reportChart, true, fileName);
//            }
//            catch (Exception ex)
//            {
//                LogManager.WriteError(ex);
//                TempData["ErrorMessage"] = "Ocorreu um erro ao gerar relatório em PDF.";
//                return this.RedirectToAction("CustodyTransferReport");
//            }
//        }

//        #endregion

//        #region Desconto de Corretagem

//        private IChannelProvider<IBrokerageDiscountContractChannel> brkDisc;
//        protected IChannelProvider<IBrokerageDiscountContractChannel> brokerDiscountProvider
//        {
//            get
//            {
//                if (brkDisc == null)
//                    brkDisc = ChannelProviderFactory<IBrokerageDiscountContractChannel>.Create("BrokerageDiscountClassName");
//                return brkDisc;
//            }
//        }

//        [AccessControlAuthorize]
//        public ActionResult BrokerageDiscount()
//        {
//            LogHelper.SaveAuditingLog("Other.BrokerageDiscount");
//            List<KeyValuePair<string, string>> options = new List<KeyValuePair<string, string>>();
//            options.Add(new KeyValuePair<string, string>("--", "--Selecione--"));
//            options.Add(new KeyValuePair<string, string>("F", "Efetuar"));
//            options.Add(new KeyValuePair<string, string>("E", "Efetuado"));
//            options.Add(new KeyValuePair<string, string>("R", "Reprovado"));

//            ViewBag.Status = new SelectList(options, "Key", "Value");

//            ViewBag.Pending = new BrokerageDiscountHelper().GetBrokerageDiscountPendents();

//            return View();
//        }

//        [HttpPost]
//        public string InitialBrokerageDiscountFilter(FormCollection form)
//        {
//            WcfResponse<List<BrokerageDiscount>, int> brokerList = null;
//            using (var brokerDiscountService = brokerDiscountProvider.GetChannel().CreateChannel())
//            {
//                var broker = new BrokerageDiscount()
//                {
//                    Situation = (String.IsNullOrEmpty(form["ddlSituation"])) ? null : form["ddlSituation"]
//                };

//                var options = new FilterOptions()
//                {
//                    InitRow = Convert.ToInt32(form["iDisplayStart"]),
//                    FinalRow = Convert.ToInt32(form["iDisplayLength"]),
//                    SortColumn = (form["iSortCol_0"] == "null" || form["iSortCol_0"] == "undefined" || form["iSortCol_0"] == "0") ? 1 : Convert.ToInt32(form["iSortCol_0"]),
//                    SortDirection = form["sSortDir_0"],
//                    AllRecords = false
//                };

//                brokerList = brokerDiscountService.LoadBrokerageDiscountsPaginate(broker, options, CommonHelper.GetLoggedUserGroupId());

//                Session["Other.BrokerageDiscounts"] = brokerList.Result;
//                Session["Other.BrokerageDiscountsFilter"] = broker;
//                Session["Other.BrokerageDiscountsFilterOptions"] = options;

//                return new BrokerageDiscountHelper().BrokerDiscountListObject(brokerList.Result, ref form, brokerList.Data);
//            }
//        }

//        [HttpPost]
//        public string BrokerageDiscountFilter(FormCollection form)
//        {
//            WcfResponse<List<BrokerageDiscount>, int> brokerList = null;

//            using (var brokerDiscountService = brokerDiscountProvider.GetChannel().CreateChannel())
//            {
//                var filter = new BrokerageDiscountHelper().CreateBrokerageDiscountFilter(form);

//                var options = new FilterOptions()
//                {
//                    InitRow = Convert.ToInt32(form["iDisplayStart"]),
//                    FinalRow = Convert.ToInt32(form["iDisplayLength"]),
//                    SortColumn = (form["iSortCol_0"] == "null" || form["iSortCol_0"] == "undefined" || form["iSortCol_0"] == "0") ? 1 : Convert.ToInt32(form["iSortCol_0"]),
//                    SortDirection = form["sSortDir_0"],
//                    AllRecords = false
//                };

//                brokerList = brokerDiscountService.LoadBrokerageDiscountsPaginate(filter, options, CommonHelper.GetLoggedUserGroupId());

//                Session["Other.BrokerageDiscounts"] = brokerList.Result;
//                Session["Other.BrokerageDiscountsFilter"] = filter;
//                Session["Other.BrokerageDiscountsFilterOptions"] = options;
//                return new BrokerageDiscountHelper().BrokerDiscountListObject(brokerList.Result, ref form, brokerList.Data);
//            }
//        }

//        protected ActionResult ProcessBrokerageDiscountStatus(FormCollection form)
//        {
//            string[] ids = form["ids"].Split(';');
//            string status = form["ddlStatus"];

//            List<BrokerageDiscount> requests = new List<BrokerageDiscount>();

//            var list = (List<BrokerageDiscount>)Session["Other.BrokerageDiscounts"];

//            foreach (string i in ids)
//            {
//                requests.Add(new BrokerageDiscount()
//                {
//                    RequestId = Convert.ToInt32(i.Split('|')[0]),
//                    ClientCode = Convert.ToInt32(i.Split('|')[1]),
//                    Situation = form["ddlStatus"]
//                });

//                string action = "";

//                if (form["ddlStatus"] == "F") action = "Aprovar";
//                else if ((form["ddlStatus"] == "E")) action = "Efetuar";
//                else if ((form["ddlStatus"] == "R")) action = "Reprovar";

//                LogHelper.SaveHistoryLog("Other.BrokerageDiscount",
//                    new HistoryData
//                    {
//                        Action = action,
//                        ID = Convert.ToInt32(i.Split('|')[0]),
//                        Target = (list != null)
//                                            ? (list.Where(a => a.RequestId == Convert.ToInt32(i.Split('|')[0])).FirstOrDefault() != null)
//                                                ? list.Where(a => a.RequestId == Convert.ToInt32(i.Split('|')[0])).FirstOrDefault().ClientName
//                                                : "-"
//                                            : "-",
//                        TargetID = Convert.ToInt32(i.Split('|')[1]),
//                        ExtraTarget = (list != null)
//                                            ? (list.Where(a => a.RequestId == Convert.ToInt32(i.Split('|')[0])).FirstOrDefault() != null)
//                                                ? list.Where(a => a.RequestId == Convert.ToInt32(i.Split('|')[0])).FirstOrDefault().StockCode
//                                                : "-"
//                                            : "-"
//                    });
//            }

//            WcfResponse<bool> response = null;

//            using (var brokerDiscountService = brokerDiscountProvider.GetChannel().CreateChannel())
//            {
//                response = brokerDiscountService.UpdateBrokerageDiscountStatus(requests);
//                if (!response.Result)
//                    response.Message = "Algumas notas de desconto não puderam ser atualizadas.";
//                else
//                {
//                    response.Message = "Notas de desconto atualizadas com sucesso.";
//                }

//            }

//            return Json(response);
//        }

//        [HttpPost]
//        public ActionResult ApproveBrokerageDiscount(FormCollection form)
//        {
//            LogHelper.SaveAuditingLog("Other.BrokerageDiscount.Approve");
//            return ProcessBrokerageDiscountStatus(form);
//        }

//        [HttpPost]
//        public ActionResult EffectuateBrokerageDiscount(FormCollection form)
//        {
//            LogHelper.SaveAuditingLog("Other.BrokerageDiscount.Effectuate");
//            return ProcessBrokerageDiscountStatus(form);
//        }

//        [HttpGet]
//        public ActionResult BrokerageDiscountRealize(string requestId, string status)
//        {
//            ViewBag.Ids = requestId;
//            ViewBag.Status = status;

//            return View();
//        }

//        [HttpGet]
//        public ActionResult BrokerageDiscountDelete(string requestId)
//        {
//            var brokerageDiscounts = (List<BrokerageDiscount>)Session["Other.BrokerageDiscounts"];

//            ViewBag.BrokerageDiscount = brokerageDiscounts.Where(a => a.RequestId == Convert.ToInt32(requestId.Split('|')[0])).FirstOrDefault();

//            return View();
//        }

//        [HttpPost]
//        public ActionResult RemoveBrokerageDiscount(string id)
//        {
//            //return Json(new WcfResponse<bool>() { Result = true, Message = "Yes" });
//            LogHelper.SaveAuditingLog("Other.BrokerageDiscount.Remove");
//            var list = (List<BrokerageDiscount>)Session["Other.BrokerageDiscounts"];

//            var requests = new List<BrokerageDiscount>();
//            requests.Add(new BrokerageDiscount()
//                {
//                    RequestId = Convert.ToInt32(id.Split('|')[0]),
//                    ClientCode = Convert.ToInt32(id.Split('|')[1])
//                });

//            WcfResponse<bool> response = null;
//            using (var brokerDiscountService = brokerDiscountProvider.GetChannel().CreateChannel())
//            {
//                response = brokerDiscountService.DeleteBrokerageDiscount(requests);
//                if (!response.Result)
//                    response.Message = "Algumas notas de desconto não puderam ser removidas.";
//                else
//                {
//                    response.Message = "Notas de desconto removidas com sucesso.";

//                    LogHelper.SaveHistoryLog("Other.BrokerageDiscount",
//                    new HistoryData
//                    {
//                        Action = "Excluir",
//                        ID = Convert.ToInt32(id.Split('|')[0]),
//                        TargetID = Convert.ToInt32(id.Split('|')[1]),
//                        ExtraTarget = (list != null)
//                                            ? (list.Where(a => a.RequestId == Convert.ToInt32(id.Split('|')[0])).FirstOrDefault() != null)
//                                                ? list.Where(a => a.RequestId == Convert.ToInt32(id.Split('|')[0])).FirstOrDefault().StockCode
//                                                : "-"
//                                            : "-",
//                        Target = (list != null)
//                                            ? (list.Where(a => a.RequestId == Convert.ToInt32(id.Split('|')[0])).FirstOrDefault() != null)
//                                                ? list.Where(a => a.RequestId == Convert.ToInt32(id.Split('|')[0])).FirstOrDefault().ClientName
//                                                : "-"
//                                            : "-"
//                    });
//                }
//            }

//            return Json(response);
//        }

//        [HttpGet]
//        public ActionResult BrokerageDiscountHistory()
//        {
//            List<SelectListItem> listItems = new List<SelectListItem>();
//            listItems.Add(new SelectListItem { Text = "Todas", Value = "", Selected = true });

//            try
//            {
//                var actions = OtherHelper.BrokerageDiscountActions;
//                foreach (string action in actions)
//                    listItems.Add(new SelectListItem { Text = action, Value = action, Selected = false });

//                ViewBag.BrokerageDiscountActions = listItems;

//                LogHelper.SaveAuditingLog("Other.BrokerageDiscount.History");
//                return View();
//            }

//            catch (Exception ex)
//            {
//                LogManager.WriteError(ex);
//                TempData["ErrorMessage"] = "Não foi possível carregar o histórico das solicitações de descontos.";
//                return View();
//            }
//        }

//        [HttpGet]
//        public ActionResult BrokerageDiscountHistoryDetail(string requestId)
//        {
//            try
//            {
//                LogHelper.SaveAuditingLog("BrokerageDiscount.BrokerageDiscountHistoryDetail");

//                var list = Session["Other.BrokerageDiscounts"] != null ? (List<BrokerageDiscount>)Session["Other.BrokerageDiscounts"] : new List<BrokerageDiscount>();
//                var brk = (from c in list where c.RequestId.Equals(Convert.ToInt32(requestId.Split('|')[0])) select c).FirstOrDefault();

//                HistoryFilter filter = new HistoryFilter { ModuleName = "Other.BrokerageDiscount", ID = brk.RequestId };

//                var data = LogHelper.LoadHistory(filter);

//                Session["Other.BrokerageDiscountHistoryListDetail"] = data;
//                ViewBag.BrokerageDiscountHistoryDetails = data;

//                return View(brk);
//            }
//            catch (Exception ex)
//            {
//                LogManager.WriteError(ex);
//                TempData["ErrorMessage"] = "Não foi possível carregar o histórico deste termo.";
//                return View(new BrokerageDiscount());
//            }
//        }

//        [HttpPost]
//        public void LoadBrokerageDiscountHistory(FormCollection form)
//        {
//            try
//            {
//                //List<HistoryData> data = new List<HistoryData>();
//                //AccessControlHelper helper = new AccessControlHelper();
//                string startDate = form["txtStartDate"];
//                string endDate = form["txtEndDate"];

//                long responsibleID = string.IsNullOrEmpty(form["hdfResponsible"]) ? 0 : long.Parse(form["hdfResponsible"]);
//                int clientID = string.IsNullOrEmpty(form["txtClient"]) ? 0 : Int32.Parse(form["txtClient"]);
//                string StockCode = string.IsNullOrEmpty(form["txtStockCode"]) ? "" : form["txtStockCode"];

//                int id = 0;
//                Int32.TryParse(form["hdfRequestID"], out id);

//                string action = form["ddlBrokerageDiscountActions"];

//                LogHelper.SaveAuditingLog("Other.BrokerageDiscountHistory",
//                    string.Format(LogResources.AuditingLoadHistory, clientID, responsibleID, action, startDate, endDate));

//                HistoryFilter filter = new HistoryFilter
//                {
//                    ModuleName = "Other.BrokerageDiscount",
//                    StartDate = startDate,
//                    Action = action,
//                    EndDate = endDate,
//                    ResponsibleID = responsibleID,
//                    TargetID = clientID,
//                    ID = id,
//                    ExtraTargetText = StockCode
//                };

//                Session["Other.BrokerageDiscountHistoryList"] = LogHelper.LoadHistory(filter); //history == null ? new List<HistoryData>() : data;

//            }
//            catch (Exception ex)
//            {
//                LogManager.WriteError(ex);
//            }

//        }

//        public ActionResult Other_BrokerageDiscountHistory(FormCollection form)
//        {
//            List<HistoryData> items = (List<HistoryData>)Session["Other.BrokerageDiscountHistoryList"];
//            return new DataTablesController().AccessControl_History(form, items);
//        }

//        public ActionResult BrokerageDiscountHistoryToFile(bool isPdf, string detail)
//        {
//            return this.HistoryToFile(isPdf, "BrokerageDiscount", "descontos de corretagem", "Cliente", true, string.IsNullOrEmpty(detail) ? "Other.BrokerageDiscountHistoryList" : "Other.BrokerageDiscountHistoryListDetail", "Papel");
//        }

//        public ActionResult HistoryToFile(bool isPdf, string clientName, string targetTitle, string targetColumnTitle, bool hasAction, string sessionName, string extraTargetColumnTitle = null)
//        {
//            List<HistoryData> currentList = new List<HistoryData>();

//            try
//            {
//                if (Session[sessionName] != null)
//                    currentList = (List<HistoryData>)Session[sessionName];

//                //LogHelper.SaveAuditingLog(string.Concat("Other.BrokerageDiscount.HistoryToFile.", clientName), string.Format(LogResources.ObjectToFileParameters, isPdf ? "PDF" : "Excel", JsonConvert.SerializeObject(currentList)));

//                string name = new Regex(@"\s*").Replace(HttpUtility.HtmlDecode(targetTitle), string.Empty);
//                targetTitle = string.Concat(isPdf ? "Histórico de " : "Hist&#243;rico de ", targetTitle);

//                string fileName = string.Format("historico_{0}_{1}.{2}", name, DateTime.Now.ToString("dd-MM-yyyy"), isPdf ? "pdf" : "xls");

//                return isPdf ? ViewHistoryPdf(currentList, true, fileName, targetTitle, targetColumnTitle, hasAction, extraTargetColumnTitle) : ViewHistoryExcel(currentList, true, fileName, targetTitle, targetColumnTitle, hasAction, extraTargetColumnTitle);

//            }
//            catch (Exception ex)
//            {
//                LogManager.WriteError(ex);
//                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
//                return this.RedirectToAction("Index");
//            }

//        }

//        [HttpPost]
//        public ActionResult BrokerageDiscountFilterOptions(FormCollection form)
//        {
//            Response.Cache.SetCacheability(HttpCacheability.NoCache);

//            try
//            {
//                var filter = new BrokerageDiscountHelper().CreateBrokerageDiscountFilter(form);

//                var options = new FilterOptions()
//                {
//                    SortColumn = (form["iSortCol_0"] == "null" || form["iSortCol_0"] == "undefined" || form["iSortCol_0"] == "0") ? 1 : Convert.ToInt32(form["iSortCol_0"]),
//                    SortDirection = form["sSortDir_0"],
//                    AllRecords = true
//                };

//                Session["Other.BrokerageDiscountsFilter"] = filter;
//                Session["Other.BrokerageDiscountsFilterOptions"] = options;

//                return Json(true);
//            }
//            catch { return Json(false); }
//        }

//        public ActionResult BrokerageDiscountToFile(bool isPdf)
//        {
//            List<BrokerageDiscount> currentList = new List<BrokerageDiscount>();
//            try
//            {
//                if (Session["Other.BrokerageDiscountsFilter"] != null && Session["Other.BrokerageDiscountsFilterOptions"] != null)
//                {
//                    BrokerageDiscount filter = (BrokerageDiscount)Session["Other.BrokerageDiscountsFilter"];
//                    FilterOptions options = (FilterOptions)Session["Other.BrokerageDiscountsFilterOptions"];
//                    options.AllRecords = true;

//                    IBrokerageDiscountContractChannel contractChannel = brokerDiscountProvider.GetChannel().CreateChannel();
//                    contractChannel.Open();
//                    currentList = contractChannel.LoadBrokerageDiscountsPaginate(filter, options, CommonHelper.GetLoggedUserGroupId()).Result;
//                    contractChannel.Close();
//                }

//                //LogHelper.SaveAuditingLog("Other.BrokerageDiscountToFile", string.Format(LogResources.ObjectToFileParameters, isPdf ? "PDF" : "Excel", JsonConvert.SerializeObject(currentList)));

//                string fileName = string.Format("descontocorretagem_{0}.{1}", DateTime.Now.ToString("dd-MM-yyyy"), isPdf ? "pdf" : "xls");

//                LogHelper.SaveHistoryLog("Other.BrokerageDiscount", new HistoryData { Action = "Exportar", OldData = string.Empty, NewData = JsonConvert.SerializeObject(currentList) });

//                return isPdf ? ViewBrokerageDiscountPdf(currentList, true, fileName) : ViewBrokerageDiscountExcel(currentList, true, fileName);
//            }
//            catch (Exception ex)
//            {
//                LogManager.WriteError(ex);
//                TempData["ErrorMessage"] = "Ocorreu um erro ao gerar relatório.";
//                return this.RedirectToAction("BrokerageDiscount");
//            }


//        }

//        public ActionResult BrokerageDiscountRequest()
//        {
//            using (var brokerageDiscountService = brokerDiscountProvider.GetChannel().CreateChannel())
//            {

//                var date = brokerageDiscountService.GetProjectedDate(null);
//                ViewBag.ProjectedDate = date.Result.ToString("dd/MM/yyyy");
//            }
//            return View();
//        }

//        [HttpPost]
//        public ActionResult BrokerageDiscountRequest(FormCollection form)
//        {
//            LogHelper.SaveAuditingLog("Other.BrokerageDiscount.Request");

//            List<BrokerageDiscount> requests = new List<BrokerageDiscount>();

//            BrokerageDiscount initialRequest = new BrokerageDiscount()
//            {
//                ClientCode = Convert.ToInt32(form["txtClient"]),
//                StockExchange = Convert.ToInt32(form["rdbMarket"]),
//                MarketType = form["ddlMarket"],
//                Operation = form["ddlOperation"],
//                Vigency = form["rdbVigency"],
//                Description = Regex.Replace(form["txtDescription"], "<.*?>", "")
//            };

//            //in_TODOS (passar em StockCode): S, se rdbNegocios = S || rdbVigency = P
//            if (form["rdbVigency"] == "P")
//            {
//                //Permanente
//                initialRequest.StockCode = "TODOS";

//                if (!String.IsNullOrEmpty(form["txtStockPercPermanent"]))
//                    initialRequest.StockDiscountPercentual = Convert.ToDecimal(form["txtStockPercPermanent"]);
//                else if (!String.IsNullOrEmpty(form["txtStockValuePermanent"]))
//                    initialRequest.StockDiscountValue = Convert.ToDecimal(form["txtStockValuePermanent"]);

//                requests.Add(initialRequest);
//            }
//            else if (form["rdbNegocios"] == "S")
//            {//Todos os negócios
//                initialRequest.StockCode = "TODOS";

//                if (!String.IsNullOrEmpty(form["txtStockPercAll"]))
//                    initialRequest.StockDiscountPercentual = Convert.ToDecimal(form["txtStockPercAll"]);
//                else if (!String.IsNullOrEmpty(form["txtStockValueAll"]))
//                    initialRequest.StockDiscountValue = Convert.ToDecimal(form["txtStockValueAll"]);

//                requests.Add(initialRequest);
//            }
//            else
//            {
//                // Especificar
//                String[] sStockCode = form.AllKeys.Where(a => a.Contains("txtStockCode_")).ToArray();

//                for (int i = 0; i < sStockCode.Length; i++)
//                {
//                    string order = sStockCode[i].Replace("txtStockCode_", "");

//                    var request = new BrokerageDiscount()
//                    {
//                        ClientCode = initialRequest.ClientCode,
//                        StockExchange = initialRequest.StockExchange,
//                        MarketType = initialRequest.MarketType,
//                        Operation = initialRequest.Operation,
//                        Vigency = initialRequest.Vigency,
//                        Description = initialRequest.Description
//                    };

//                    request.StockCode = form["txtStockCode_" + order];
//                    request.Qtty = Convert.ToDecimal(form["txtStockQtty_" + order]);

//                    if (!String.IsNullOrEmpty(form["txtStockPerc_" + order]))
//                        request.StockDiscountPercentual = Convert.ToDecimal(form["txtStockPerc_" + order]);
//                    else if (!String.IsNullOrEmpty(form["txtStockValue_" + order]))
//                        request.StockDiscountValue = Convert.ToDecimal(form["txtStockValue_" + order]);

//                    requests.Add(request);
//                }
//            }

//            bool response = true;
//            using (var brokerageDiscountService = brokerDiscountProvider.GetChannel().CreateChannel())
//            {

//                foreach (var req in requests)
//                {
//                    var resp = brokerageDiscountService.BrokerageDiscountRequest(req);
//                    /*TODO: LOG*/
//                    if (resp.Result)
//                    {
//                        LogHelper.SaveHistoryLog("Other.BrokerageDiscount",
//                                new HistoryData
//                                {
//                                    Action = "Solicitar",
//                                    NewData = JsonConvert.SerializeObject(req),
//                                    ID = resp.Data,
//                                    TargetID = Convert.ToInt32(form["txtClient"]),
//                                    Target = form["hdfClientName"],
//                                    ExtraTarget = req.StockCode
//                                });
//                    }
//                    else if (!resp.Result && response)
//                        response = false;
//                }
//            }

//            return Json(new WcfResponse<bool>() { Result = response, Message = (response) ? "Requisições efetuadas com sucesso." : "Algumas requisições não puderam ser efetuadas." });
//        }

//        [HttpPost]
//        public int GetPendingDiscounts()
//        {
//            return new BrokerageDiscountHelper().GetBrokerageDiscountPendents();
//        }

//        [HttpPost]
//        public decimal GetActualDiscount(int clientCode)
//        {
//            decimal result = 0;

//            using (var service = brokerDiscountProvider.GetChannel().CreateChannel())
//            {
//                service.Open();
//                result = service.GetActualDiscount(clientCode).Result;

//                service.Close();
//            }

//            return result;
//        }

//        [HttpPost]
//        public ActionResult GetMarketTypes(int stock)
//        {
//            return Json(new BrokerageDiscountHelper().GetMarketList(stock));
//        }

//        [HttpPost]
//        public ActionResult GetBrokerageDiscountStatus(string status)
//        {
//            List<KeyValuePair<string, string>> options = new List<KeyValuePair<string, string>>();
//            options.Add(new KeyValuePair<string, string>("--", "--Selecione--"));
//            options.Add(new KeyValuePair<string, string>("F", "Efetuar"));
//            options.Add(new KeyValuePair<string, string>("E", "Efetuado"));
//            options.Add(new KeyValuePair<string, string>("R", "Reprovado"));

//            var results = new List<CommonType>();
//            results.Add(new CommonType() { Value = "--", Description = "Selecione" });

//            AccessControlHelper acHelper = new AccessControlHelper();

//            switch (status)
//            {
//                case "A":
//                    {
//                        if (acHelper.HasFuncionalityPermission("Other.BrokerageDiscount", "btnApprove"))
//                            results.Add(new CommonType() { Value = "F", Description = "Aprovado" });
//                        if (acHelper.HasFuncionalityPermission("Other.BrokerageDiscount", "btnApprove"))
//                            results.Add(new CommonType() { Value = "R", Description = "Reprovado" });

//                        break;
//                    }
//                case "F":
//                    {
//                        if (acHelper.HasFuncionalityPermission("Other.BrokerageDiscount", "btnEffective"))
//                            results.Add(new CommonType() { Value = "E", Description = "Efetuado" });
//                        break;
//                    }
//                default: break;
//            }

//            return Json(results);
//        }

//        #endregion

//        #region Posição Consolidada

//        [AccessControlAuthorize]
//        public ActionResult ConsolidatedPosition()
//        {
//            return View();
//        }

//        public ActionResult ConsolidatedPositionClient(string id)
//        {
//            return View(((ConsolidatedPositionResponse)Session["ConsolidatedPosition." + id]).Result.ClientInformation);
//        }

//        [HttpPost]
//        public ActionResult LoadConsolidatedPosition(string clientCode)
//        {
//            try
//            {
//                IChannelProvider<IOtherContractChannel> channelProvider = ChannelProviderFactory<IOtherContractChannel>.Create("OtherClassName");
//                var service = channelProvider.GetChannel().CreateChannel();

//                service.Open();

//                MarketHelper helper = new MarketHelper();
//                string assessors = helper.GetAssessorsRangesToFilter("");
//                ConsolidatedPositionResponse result = service.LoadConsolidatedPosition(clientCode, assessors, CommonHelper.GetLoggedUserGroupId()).Result;

//                service.Close();

//                if (string.IsNullOrEmpty(result.Message))
//                {
//                    Session["ConsolidatedPosition." + clientCode] = result;
//                    ViewBag.ClientInformation = result.Result.ClientInformation;
//                    return PartialView("CPResume", result.Result.Resume);
//                }
//                else
//                    return Content(FeedbackErrorModel(result.Message));
//            }
//            catch
//            {
//                return Content(FeedbackErrorModel(string.Format("Falha ao carregar a posição do cliente {0}.", clientCode)));
//            }
//        }

//        [HttpPost]
//        public ActionResult PositionController(string positionType, string clientCode)
//        {
//            try
//            {
//                ConsolidatedPositionResponse position = (ConsolidatedPositionResponse)Session["ConsolidatedPosition." + clientCode];
//                //var position = (ConsolidatedPositionResponse)SerializeHelper.DeserializeObject("consolidated_" + clientCode + ".xml", typeof(ConsolidatedPositionResponse));

//                switch (positionType)
//                {
//                    case "Gold":
//                        return PartialView("CPGold", position.Result.Gold);
//                    case "Clubs":
//                        return PartialView("CPClubs", position.Result.Clubs);
//                    case "Option":
//                        return PartialView("CPOption", position.Result.Option);
//                    case "CommitedCDB":
//                        return PartialView("CPCommitedCDB", position.Result.CommitedCDB);
//                    case "BovespaGuarantees":
//                        return PartialView("CPBovespaGuarantees", position.Result.BovespaGuarantees);
//                    case "BMFGuarantees":
//                        return PartialView("CPBMFGuarantees", position.Result.BMFGuarantees);
//                    case "RentDonor":
//                        return PartialView("CPRentDonor", position.Result.RentDonor);
//                    case "RentTaker":
//                        return PartialView("CPRentTaker", position.Result.RentTaker);
//                    case "TotalStock":
//                        return PartialView("CPTotalStock", position.Result.TotalStock);
//                    case "CDBFinal":
//                        return PartialView("CPCDBFinal", position.Result.CDBFinal);
//                    case "PublicTitles":
//                        return PartialView("CPPublicTitles", position.Result.PublicTitles);
//                    case "Term":
//                        return PartialView("CPTerm", position.Result.Terms);
//                    case "SettledTerms":
//                        return PartialView("CPSettledTerms", position.Result.SettledTerms);
//                    case "SettledTermsToday":
//                        return PartialView("CPSettledTermsToday", position.Result.SettledTermsToday);
//                    case "BMFCurrent":
//                        return PartialView("CPBMFTotal", position.Result.BMF);
//                    case "TotalCheckingAccount":
//                        return PartialView("CPTotalCheckingAccount", position.Result.TotalCheckingAccount);
//                    default:
//                        return Content(FeedbackErrorModel("Erro no processamento."));
//                }
//            }
//            catch
//            {
//                return Content(FeedbackErrorModel("Erro ao carregar a posição."));
//            }
//        }

//        private string FeedbackErrorModel(string message)
//        {

//            return string.Format("<div id='feedbackError' class='erro' style='display:block;'><span>{0}</span></div>", message);
//        }

//        [HttpGet]
//        public ActionResult ConsolidatedPositionToFile(string clientCode)
//        {
//            try
//            {
//                ConsolidatedPositionResponse item = new ConsolidatedPositionResponse();

//                if (Session["ConsolidatedPosition." + clientCode] != null)
//                {
//                    item = (ConsolidatedPositionResponse)Session["ConsolidatedPosition." + clientCode];

//                    //LogHelper.SaveAuditingLog("Other.Operations", string.Format(LogResources.ObjectToFileParameters, isPdf ? "PDF" : "Excel", JsonConvert.SerializeObject(saldo)));
//                    string fileName = string.Format("posicao-{0}_{1}.{2}", clientCode, DateTime.Now.ToString("dd-MM-yyyy"), "pdf");
//                    return ViewConsolidatedPositionPdf(item, true, fileName);
//                }
//                else
//                {
//                    return this.RedirectToAction("ConsolidatedPosition");
//                }
//            }
//            catch (Exception ex)
//            {
//                LogManager.WriteError(ex);
//                TempData["ErrorMessage"] = "Ocorreu um erro ao gerar relatório.";
//                return this.RedirectToAction("ConsolidatedPosition");
//            }
//        }

//        //[HttpPost]
//        //public ActionResult SendConsolidatedPositionMail(int clientCode)
//        //{
//        //    var currentPosition = (ConsolidatedPositionResponse)Session["ConsolidatedPosition." + clientCode];

//        //    try
//        //    {
//        //        if (currentPosition != null)
//        //        {

//        //            using (var service = ChannelProviderFactory<ICommonContractChannel>.Create("CommonClassName").GetChannel().CreateChannel())
//        //            {
//        //                var variables = new List<KeyValuePair<string, object>>();
//        //                variables.Add(new KeyValuePair<string, object>("strNome", currentPosition.Result.ClientInformation.Client.Description));
//        //                variables.Add(new KeyValuePair<string, object>("strData", currentPosition.Result.ClientInformation.PositionDate));

//        //                string subject = "Posição Consolidada";
//        //                string emailTo = currentPosition.Result.ClientInformation.Email;
//        //                string emailReplyTo = SessionHelper.CurrentUser.Email;

//        //                var mem = new PDFHelper().CreateConsolidatedPositionToMemoryStream(currentPosition);

//        //                byte[] buf = new byte[mem.Length];
//        //                mem.Position = 0;
//        //                mem.Read(buf, 0, buf.Length);

//        //                List<KeyValuePair<string, byte[]>> attachments = new List<KeyValuePair<string, byte[]>>();

//        //                attachments.Add(new KeyValuePair<string, byte[]>("posicao-consolidada.pdf", buf));

//        //                var templateFile = "~/Template/ConsolidatedPositionModel.htm";

//        //                return Json(service.SendMailForUserWithAttachment(variables.ToArray(), attachments.ToArray(), templateFile, subject, emailTo, emailReplyTo, null));
//        //            }
//        //        }
//        //        return Json(new WcfResponse<bool> { Result = false, Message = "Cliente não encontrado." });
//        //    }
//        //    catch (Exception ex)
//        //    {
//        //        LogManager.WriteError(ex);
//        //        return Json(new WcfResponse<bool> { Result = false, Message = "Não foi possível enviar email." });
//        //    }

//        //}

//        #endregion

//        #region Mapa de Operações

//        [AccessControlAuthorize]
//        public ActionResult OperationsMap()
//        {
//            using (var service = ChannelProviderFactory<IOtherContractChannel>.Create("OtherClassName").GetChannel().CreateChannel())
//            {
//                var tradingDates = service.GetTradingDates().Result;

//                ViewBag.ddlTradingDate = new SelectList(tradingDates, "Key", "Value");
//            }

//            return View();
//        }

//        public ActionResult OperationMapDetails(int clientCode)
//        {
//            var response = (WcfResponse<OperationsMap[]>)Session["Other.OperationMapClientList"];

//            return PartialView("OperationMapDetails", response.Result.Where(a => a.ClientCode == clientCode).ToList());
//        }

//        public string LoadOperators(FormCollection form)
//        {
//            WcfResponse<OperationsMap[]> response = new WcfResponse<OperationsMap[]>();

//            AccessControlHelper acHelper = new AccessControlHelper();

//            using (var otherService = ChannelProviderFactory<IOtherContractChannel>.Create("OtherClassName").GetChannel().CreateChannel())
//            {
//                otherService.Open();

//                var filter = new OperationMapFilter()
//                {
//                    TradingDate = Convert.ToDateTime(form["ddlTradingDate"]),
//                    //Operator = form["txtOperator"],
//                    StockCode = form["txtStockCode"],
//                    Login = acHelper.HasFuncionalityPermission("Other.OperationsMap", "Master") ? "" : SessionHelper.CurrentUser.UserName,
//                    Master = acHelper.HasFuncionalityPermission("Other.OperationsMap", "Master"),
//                    ClientFilter = new MarketHelper().GetClientsRangesToFilter(form["txtClientFilter"]),
//                    UserId = CommonHelper.GetLoggedUserGroupId()
//                };

//                if (!String.IsNullOrEmpty(form["txtOperator"]))
//                    filter.OperatorCode = Convert.ToInt32(form["txtOperator"]);

//                var olderFilter = (OperationMapFilter)Session["Other.OperationMapClientFilter"];

//                if (olderFilter != null
//                    && olderFilter.TradingDate == filter.TradingDate
//                    && olderFilter.StockCode == filter.StockCode
//                    && olderFilter.Operator == filter.Operator
//                    && olderFilter.ClientFilter == filter.ClientFilter
//                    && olderFilter.OperatorCode == filter.OperatorCode
//                    && olderFilter.UserId == filter.UserId
//                    )
//                {
//                    if (Session["Other.OperationMapClientList"] != null)
//                    {
//                        response = (WcfResponse<OperationsMap[]>)Session["Other.OperationMapClientList"];
//                    }
//                    else
//                    {
//                        response = otherService.LoadOperationsMap(filter);

//                        if (response.Result != null)
//                        {
//                            var result = response.Result;
//                            if (!String.IsNullOrEmpty(form["txtClientFilter"]))
//                            {
//                                var i = WebSiteHelper.ConvertRangeStrToNumList(form["txtClientFilter"]);
//                                result = response.Result.Where(a => i.Contains(a.ClientCode)).ToArray();
//                                response.Result = result;
//                            }
//                        }
//                    }
//                }
//                else
//                {
//                    response = otherService.LoadOperationsMap(filter);

//                    if (response.Result != null)
//                    {
//                        var result = response.Result;
//                        if (!String.IsNullOrEmpty(form["txtClientFilter"]))
//                        {
//                            var i = WebSiteHelper.ConvertRangeStrToNumList(form["txtClientFilter"]);
//                            result = response.Result.Where(a => i.Contains(a.ClientCode)).ToArray();
//                            response.Result = result;
//                        }
//                    }
//                }

//                otherService.Close();

//                List<Operator> operatorList = new List<Operator>();

//                /*Operadores*/
//                if (response.Result != null)
//                {
//                    int line = 1;
//                    //p => CommonHelper.GetClientName(p)
//                    var auxOperatorList = response.Result.OrderBy(p => p.Operator).Select(a => a.OperatorCode).Distinct().ToList();
//                    operatorList = (from a in auxOperatorList
//                                    select new Operator
//                                    {
//                                        Position = line++,
//                                        OperatorCode = a,
//                                        OperatorName = response.Result.Where(b => b.OperatorCode == a).FirstOrDefault().Operator,
//                                        BrokerageValue = response.Result.Where(b => b.OperatorCode == a).Sum(c => c.BrokerageValue)
//                                    }).ToList();
//                }

//                Session["Other.OperationMapClientList"] = response;
//                Session["Other.OperationMapClientFilter"] = filter;
//                Session["Other.OperatorList"] = operatorList;

//                return new OtherHelper().OperatorsListObject(operatorList, form);
//            }
//        }

//        public string LoadOperatorClients(FormCollection form)
//        {
//            var response = (WcfResponse<OperationsMap[]>)Session["Other.OperationMapClientList"];

//            var operationsMapList = new List<OperationsMap>();

//            if (response.Result != null)
//            {
//                var itemListClientCodes = response.Result.Where(a => a.OperatorCode == Convert.ToInt32(form["txtOperatorCode"])).Select(a => a.ClientCode).Distinct().ToList();

//                operationsMapList = (from a in itemListClientCodes
//                                     select new OperationsMap()
//                                     {
//                                         ClientCode = a,
//                                         Volume = response.Result.Where(b => b.ClientCode == a).Sum(c => c.Volume),
//                                         BrokerageValue = response.Result.Where(b => b.ClientCode == a).Sum(c => c.BrokerageValue),
//                                     }).ToList();
//            }

//            return new OtherHelper().OperationsMapListObject(operationsMapList, form);

//        }

//        public ActionResult LoadOperatorDetails(int operatorCode)
//        {

//            var operationList = (List<Operator>)Session["Other.OperatorList"];

//            var op = operationList.Where(a => a.OperatorCode == operatorCode).FirstOrDefault();

//            ViewBag.Operator = op;

//            return PartialView("OperatorDetails", operationList);
//        }

//        [HttpGet]
//        public ActionResult OperationsMapToFile(bool isPdf)
//        {
//            try
//            {
//                List<OperationsMap> list = new List<OperationsMap>();

//                if (Session["Other.OperationMapClientList"] != null)
//                {
//                    list = ((WcfResponse<OperationsMap[]>)Session["Other.OperationMapClientList"]).Result.OrderBy(p => p.ClientCode).ToList();

//                    //LogHelper.SaveAuditingLog("Subscription.SubscriptionsToFile", string.Format(LogResources.ObjectToFileParameters, isPdf ? "PDF" : "Excel", JsonConvert.SerializeObject(saldo)));
//                    string fileName = string.Format("mapa_operacoes_{0}.{1}", DateTime.Now.ToString("dd-MM-yyyy"), isPdf ? "pdf" : "xls");
//                    LogHelper.SaveHistoryLog("Other.OperationsMap", new HistoryData { Action = "Exportar", OldData = string.Empty, NewData = JsonConvert.SerializeObject(list) });
//                    return isPdf ? ViewOperationsMapPdf(list, true, fileName) : ViewOperationsMapExcel(list, true, fileName);
//                }
//                else
//                {
//                    return this.RedirectToAction("OperationsMap");
//                }
//            }
//            catch (Exception ex)
//            {
//                LogManager.WriteError(ex);
//                TempData["ErrorMessage"] = "Ocorreu um erro ao gerar relatório.";
//                return this.RedirectToAction("OperationsMap");
//            }
//        }

//        #endregion

//        #region Operações

//        public ActionResult OperationReport(int clientCode, DateTime tradingDate)
//        {
//            WcfResponse<OperationReport> response = null;

//            using (var service = ChannelProviderFactory<IOtherContractChannel>.Create("OtherClassName").GetChannel().CreateChannel())
//            {
//                response = service.LoadOperationReport(new OperationReport() { ClientCode = clientCode, TradeDate = tradingDate });

//                ViewBag.ClientInformation = service.LoadClientInformation(clientCode.ToString()
//                    , ""//new MarketHelper().GetAssessorsRangesToFilter("")
//                    , CommonHelper.GetLoggedUserGroupId()).Result;

//                Session["Other.OperationsReport"] = response.Result;
//                Session["Other.OperationsClientInformation"] = ViewBag.ClientInformation;

//                return View(response.Result);
//            }
//        }

//        [AccessControlAuthorize]
//        public ActionResult Operations()
//        {
//            using (var service = ChannelProviderFactory<IOtherContractChannel>.Create("OtherClassName").GetChannel().CreateChannel())
//            {
//                Dictionary<string, string> tradingDates = service.GetTradingDates().Result;
//                ViewBag.ddlTradingDate = new SelectList(tradingDates, "Key", "Value");
//            }
//            return View();
//        }

//        [HttpPost]
//        public string ListOperations(FormCollection form)
//        {
//            WcfResponse<OperationClient[], int> list = new WcfResponse<OperationClient[], int>();
//            IChannelProvider<IOtherContractChannel> otherProvider = ChannelProviderFactory<IOtherContractChannel>.Create("OtherClassName");

//            using (var otherService = otherProvider.GetChannel().CreateChannel())
//            {
//                var helper = new MarketHelper();

//                OperationClientFilter filter = new OperationClientFilter()
//                {
//                    AssessorFilter = (form["txtAssessor"] != null) ? helper.GetAssessorsRangesToFilter(form["txtAssessor"].ToString()) : "",
//                    ClientFilter = form["txtCliente"],
//                    Status = string.IsNullOrEmpty(form["ddlStatus"]) ? string.Empty : form["ddlStatus"],
//                };

//                if (!string.IsNullOrEmpty(form["ddlTradingDate"]) && Convert.ToDateTime(form["ddlTradingDate"]) != DateTime.MinValue)
//                    filter.TradingDate = Convert.ToDateTime(form["ddlTradingDate"]);

//                FilterOptions options = new FilterOptions()
//                {
//                    InitRow = Convert.ToInt32(form["iDisplayStart"]),
//                    FinalRow = Convert.ToInt32(form["iDisplayLength"]),
//                    SortColumn = (form["iSortCol_0"] == "null" || form["iSortCol_0"] == "undefined" || form["iSortCol_0"] == "0") ? 1 : Convert.ToInt32(form["iSortCol_0"]),
//                    SortDirection = form["sSortDir_0"],
//                    AllRecords = false
//                };

//                list = otherService.LoadOperationClients(filter, options, CommonHelper.GetLoggedUserGroupId());

//                if (list != null && list.Data > 0)
//                    Session["Other.Operations"] = list.Result.ToList();
//                //Session["Other.OperationsFilter"] = filter;
//                //Session["Other.OperationsFilterOptions"] = options;
//            }
//            return new OtherHelper().OperationsListObject(list != null && list.Data > 0 ? list.Result.ToList() : new List<OperationClient>(), ref form, list.Data);
//        }

//        [HttpGet]
//        public ActionResult OperationsToFile(bool isPdf)
//        {
//            try
//            {
//                List<OperationClient> list = new List<OperationClient>();

//                if (Session["Other.Operations"] != null)
//                {
//                    list = (List<OperationClient>)Session["Other.Operations"];

//                    //LogHelper.SaveAuditingLog("Other.Operations", string.Format(LogResources.ObjectToFileParameters, isPdf ? "PDF" : "Excel", JsonConvert.SerializeObject(saldo)));
//                    string fileName = string.Format("operacoes_{0}.{1}", DateTime.Now.ToString("dd-MM-yyyy"), isPdf ? "pdf" : "xls");
//                    LogHelper.SaveHistoryLog("Other.Operations", new HistoryData
//                    {
//                        Action = "Exportar",
//                        ResponsibleID = Convert.ToInt16(SessionHelper.CurrentUser.ID),
//                        Responsible = SessionHelper.CurrentUser.Name,
//                        ExtraTarget = list.FirstOrDefault().TradingDate.Value.ToShortDateString(),
//                        OldData = string.Empty,
//                        NewData = JsonConvert.SerializeObject(list)
//                    });
//                    return isPdf ? ViewOperationsPdf(list, true, fileName) : ViewOperationsExcel(list, true, fileName);
//                }
//                else
//                {
//                    return this.RedirectToAction("Operations");
//                }
//            }
//            catch (Exception ex)
//            {
//                LogManager.WriteError(ex);
//                TempData["ErrorMessage"] = "Ocorreu um erro ao gerar relatório.";
//                return this.RedirectToAction("Operations");
//            }
//        }

//        public ActionResult OperationDetails(int clientCode, DateTime tradingDate)
//        {
//            WcfResponse<OperationReport> response = null;

//            using (var service = ChannelProviderFactory<IOtherContractChannel>.Create("OtherClassName").GetChannel().CreateChannel())
//            {
//                response = service.LoadOperationReport(new OperationReport() { ClientCode = clientCode, TradeDate = tradingDate });

//                ViewBag.ClientInformation = service.LoadClientInformation(clientCode.ToString()
//                    , "" //new MarketHelper().GetAssessorsRangesToFilter("")
//                    , CommonHelper.GetLoggedUserGroupId()).Result;

//                Session["Other.OperationsReport"] = response.Result;
//                Session["Other.OperationsClientInformation"] = ViewBag.ClientInformation;

//                return View(response.Result);
//            }
//        }

//        //protected WcfResponse<bool> SendEmailToClient(int clientCode, DateTime tradingDate)
//        //{
//        //    int funcionalityId = 2;

//        //    var accessControlService = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName").GetChannel().CreateChannel();
//        //    accessControlService.Open();
//        //    var client = accessControlService.ListFuncionalityClients(
//        //        new FuncionalityClientFilter
//        //        {
//        //            ClientCodes = clientCode.ToString() + ";",
//        //            SortColumn = 1,
//        //            SortDirection = "asc",
//        //            AllRecords = true
//        //        });

//        //    accessControlService.Close();

//        //    if (client.Result == null || client.Result.Count() == 0)
//        //        return new WcfResponse<bool> { Result = false, Message = "Cliente não está cadastrado para envio de email." };
//        //    else if (client.Result.FirstOrDefault().Funcionalities.Where(a => a.FuncId == funcionalityId).FirstOrDefault() == null)
//        //        return new WcfResponse<bool> { Result = false, Message = "Cliente não possui acesso a essa funcionalidade ou não possui emails cadastrados para envio." };

//        //    var funcionalityClient = client.Result.FirstOrDefault();

//        //    //montar variaveis

//        //    List<KeyValuePair<string, object>> variables = new List<KeyValuePair<string, object>>();

//        //    var commonService = ChannelProviderFactory<ICommonContractChannel>.Create("CommonClassName").GetChannel().CreateChannel();

//        //    commonService.Open();

//        //    List<TagModel> tags = commonService.GetTagModel().Result.ToList();

//        //    variables.Add(new KeyValuePair<string, object>("$CODIGO", clientCode.ToString()));

//        //    variables.Add(new KeyValuePair<string, object>("$NOME", funcionalityClient.ClientName));

//        //    variables.Add(new KeyValuePair<string, object>("$DATA", tradingDate.ToShortDateString()));

//        //    variables.Add(new KeyValuePair<string, object>("$HOJE", DateTime.Now.ToShortDateString()));

//        //    variables.Add(new KeyValuePair<string, object>("$ASSESSOR", SessionHelper.CurrentUser.Name));

//        //    variables.Add(new KeyValuePair<string, object>("$EMAILASSESSOR", SessionHelper.CurrentUser.Email));

//        //    var funcionality = funcionalityClient.Funcionalities.Where(a => a.FuncId == funcionalityId).FirstOrDefault();

//        //    var to = string.Empty;
//        //    foreach (var e in funcionality.Emails)
//        //    {
//        //        to = (String.IsNullOrEmpty(to)) ? String.Format("{0}", e.Email) : ";" + String.Format("{0}", e.Email);
//        //    }

//        //    variables.Add(new KeyValuePair<string, object>("$TO", to));

//        //    var cc = string.Empty;
//        //    foreach (var e in funcionality.CopyTo)
//        //    {
//        //        cc = (String.IsNullOrEmpty(cc)) ? String.Format("{0}", e.Email) : ";" + String.Format("{0}", e.Email);
//        //    }

//        //    variables.Add(new KeyValuePair<string, object>("$CC", cc));

//        //    //Montar attach
//        //    //TODO: Será feito assim que haver relatório.

//        //    byte[] b = null;
//        //    MemoryStream m = new MemoryStream();

//        //    if (funcionality.SendFormat == "H")
//        //    {
//        //        //TODO: Criar view com folha de estilo inline. Essa view será usada para o envio de e-mail.

//        //        StreamWriter sw = new StreamWriter(m, System.Text.ASCIIEncoding.UTF8);

//        //        ViewContext viewContext = new ViewContext(ControllerContext, new WebFormView(ControllerContext, "~/Other/OperationReport"), ViewData, TempData, sw);

//        //        HtmlHelper helper = new HtmlHelper(viewContext, new ViewPage());

//        //        helper.RenderAction("OperationReport", "Other", new { clientCode = clientCode, tradingDate = tradingDate });

//        //        sw.Flush();

//        //        b = m.ToArray();

//        //        sw.Close();
//        //    }
//        //    else
//        //    {

//        //        PDFHelper pdf = new PDFHelper();

//        //        var otherService = ChannelProviderFactory<IOtherContractChannel>.Create("OtherClassName").GetChannel().CreateChannel();
//        //        otherService.Open();

//        //        var resultReport = otherService.LoadOperationReport(new Data.OperationReport() { ClientCode = clientCode, TradeDate = tradingDate }).Result;

//        //        var clientInfo = otherService.LoadClientInformation(clientCode.ToString(), "", CommonHelper.GetLoggedUserGroupId()).Result;

//        //        otherService.Close();

//        //        m = pdf.CreateOperationsReportToMemoryStream(resultReport, clientInfo);

//        //        b = m.ToArray();
//        //    }

//        //    var response = commonService.SendMailForUser(funcionalityId, variables.ToArray(), b, funcionalityClient, "relatorio-operacoes");

//        //    commonService.Close();

//        //    if (response.Result)
//        //    {
//        //        response.Message = "Email enviado com sucesso.";

//        //        using (var otherService = ChannelProviderFactory<IOtherContractChannel>.Create("OtherClassName").GetChannel().CreateChannel())
//        //        {
//        //            var responseSend = otherService.SinalizeMailSended(clientCode, tradingDate).Result;
//        //            if (responseSend)
//        //            {
//        //                LogHelper.SaveHistoryLog("Other.Operations",
//        //                    new HistoryData
//        //                    {
//        //                        Action = "Enviar e-mail",
//        //                        ExtraTarget = tradingDate.ToShortDateString(),
//        //                        TargetID = clientCode,
//        //                        Target = funcionalityClient.ClientName,
//        //                        ResponsibleID = Convert.ToInt16(SessionHelper.CurrentUser.ID),
//        //                        Responsible = SessionHelper.CurrentUser.Name,
//        //                    });
//        //            }
//        //        }
//        //    }

//        //    return response;
//        //}

//        public ActionResult OperationsHistory()
//        {
//            List<SelectListItem> listItems = new List<SelectListItem>();
//            listItems.Add(new SelectListItem { Text = "Todas", Value = "", Selected = true });
//            listItems.Add(new SelectListItem { Text = "Exportar", Value = "Exportar" });
//            listItems.Add(new SelectListItem { Text = "Enviar e-mail", Value = "Enviar e-mail" });
//            listItems.Add(new SelectListItem { Text = "Alterar modelo de e-mail", Value = "AlterarModeloEmail" });

//            try
//            {
//                ViewBag.HistoryActions = listItems;
//                LogHelper.SaveAuditingLog("Other.OperationsHistory");
//                return View();
//            }
//            catch (Exception ex)
//            {
//                LogManager.WriteError(ex);
//                TempData["ErrorMessage"] = "Não foi possível carregar o histórico.";
//                return View();
//            }
//        }

//        [HttpPost]
//        public void LoadOperationsHistory(FormCollection form)
//        {
//            try
//            {
//                string startDate = form["txtStartDate"];
//                string endDate = form["txtEndDate"];

//                long responsibleID = string.IsNullOrEmpty(form["hdfResponsible"]) ? 0 : long.Parse(form["hdfResponsible"]);
//                int txtClientCode = string.IsNullOrEmpty(form["txtClientCode"]) ? 0 : Int32.Parse(form["txtClientCode"]);
//                string txtTradeDate = string.IsNullOrEmpty(form["txtTradeDate"]) ? string.Empty : form["txtTradeDate"];

//                int id = string.IsNullOrEmpty(form["hdfRequestID"]) ? 0 : Int16.Parse(form["hdfRequestID"]);

//                string action = form["ddlHistoryActions"];

//                LogHelper.SaveAuditingLog("Other.OperationsHistory",
//                    string.Format(LogResources.AuditingLoadHistory, txtClientCode, responsibleID, action, startDate, endDate));

//                HistoryFilter filter = new HistoryFilter
//                {
//                    ModuleName = "Other.Operations",
//                    StartDate = startDate,
//                    Action = action,
//                    EndDate = endDate,
//                    TargetID = txtClientCode,
//                    TargetText = CommonHelper.GetClientName(txtClientCode),
//                    ExtraTargetText = txtTradeDate,
//                    ResponsibleID = responsibleID,
//                    ID = id,
//                };
//                Session["Other.OperationsHistoryList"] = LogHelper.LoadHistory(filter);
//            }
//            catch (Exception ex)
//            {
//                LogManager.WriteError(ex);
//            }
//        }

//        public ActionResult OperationsHistoryList(FormCollection form)
//        {
//            List<HistoryData> items = (List<HistoryData>)Session["Other.OperationsHistoryList"];
//            return new DataTablesController().AccessControl_History(form, items);
//        }

//        public ActionResult OperationsHistoryToFile(bool isPdf, bool detail)
//        {
//            List<HistoryData> currentList = new List<HistoryData>();
//            string sessionName = detail ? "Other.OperationsHistoryListDetail" : "Other.OperationsHistoryList";
//            try
//            {
//                if (Session[sessionName] != null)
//                    currentList = (List<HistoryData>)Session[sessionName];

//                //LogHelper.SaveAuditingLog(string.Concat("Subscription.Index.HistoryToFile"), string.Format(LogResources.ObjectToFileParameters, isPdf ? "PDF" : "Excel", JsonConvert.SerializeObject(currentList)));
//                string targetTitle = string.Concat(isPdf ? "Histórico de Operações" : "Hist&#243;rico de Opera&#231;&#245;es");
//                string fileName = string.Format("historico_{0}_{1}.{2}", "operacoes", DateTime.Now.ToString("dd-MM-yyyy"), isPdf ? "pdf" : "xls");
//                return isPdf
//                    ? ViewHistoryPdf(currentList, true, fileName, targetTitle, "Cliente", true, "Data do Pregão")
//                    : ViewHistoryExcel(currentList, true, fileName, targetTitle, "Cliente", true, "Data do Preg&#227;o");
//            }
//            catch (Exception ex)
//            {
//                LogManager.WriteError(ex);
//                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
//                return this.RedirectToAction("Index");
//            }
//        }

//        //[HttpPost]
//        //public ActionResult Operations_SendEmailToClient(int clientCode, DateTime tradingDate)
//        //{
//        //    bool exec = true;
//        //    var response = this.SendEmailToClient(clientCode, tradingDate);

//        //    if (!response.Result)
//        //        exec = false;

//        //    return Json(new WcfResponse<bool> { Result = exec, Message = exec ? "Email enviado com sucesso." : "Ocorreu um erro ao enviar e-mail." });
//        //}

//        //[HttpPost]
//        //public ActionResult Operations_SendBatchEmails(int[] clientCodes, DateTime tradingDate)
//        //{
//        //    bool exec = true;
//        //    foreach (int code in clientCodes)
//        //    {
//        //        var response = this.SendEmailToClient(code, tradingDate);

//        //        if (!response.Result && exec)
//        //            exec = false;
//        //    }

//        //    return Json(new WcfResponse<bool> { Result = exec, Message = exec ? "Emails enviados com sucesso." : "Alguns e-mails não puderam ser enviados." });
//        //}

//        public ActionResult OperationsHistoryDetail(string tradingDate, int clientCode)
//        {
//            try
//            {
//                LogHelper.SaveAuditingLog("Other.OperationsHistoryDetail");

//                var operations = Session["Other.Operations"] != null ? (List<OperationClient>)Session["Other.Operations"] : new List<OperationClient>();
//                var operation = (from o in operations where o.ClientCode.Equals(clientCode) select o).FirstOrDefault();

//                HistoryFilter filter = new HistoryFilter { ModuleName = "Other.Operations", TargetID = clientCode, ExtraTargetText = tradingDate.Replace('-', '/') };

//                List<HistoryData> data = LogHelper.LoadHistory(filter);

//                Session["Other.OperationsHistoryListDetail"] = data;
//                ViewBag.OperationsHistoryDetail = data;

//                return View(operation);
//            }
//            catch (Exception ex)
//            {
//                LogManager.WriteError(ex);
//                TempData["ErrorMessage"] = "Não foi possível carregar o histórico da correção de ordens.";
//                return View(new OperationClient());
//            }
//        }

//        [HttpGet]
//        public ActionResult OperationsReportToFile()
//        {
//            try
//            {
//                OperationReport report = new OperationReport();
//                CPClientInformation client = new CPClientInformation();

//                if (Session["Other.OperationsReport"] != null && Session["Other.OperationsClientInformation"] != null)
//                {
//                    report = (OperationReport)Session["Other.OperationsReport"];
//                    client = (CPClientInformation)Session["Other.OperationsClientInformation"];
//                    string fileName = string.Format("relatorio_operacoes_{0}.pdf", DateTime.Now.ToString("dd-MM-yyyy"));
//                    return ViewOperationsReportPdf(report, client, true, fileName);
//                }
//                else
//                {
//                    return this.RedirectToAction("Operations");
//                }
//            }
//            catch (Exception ex)
//            {
//                LogManager.WriteError(ex);
//                TempData["ErrorMessage"] = "Ocorreu um erro ao gerar relatório.";
//                return this.RedirectToAction("Operations");
//            }
//        }

//        #endregion

//        #region Informe de Rendimento

//        [AccessControlAuthorize]
//        public ActionResult IncomeReport()
//        {
//            var list = new List<KeyValuePair<int, int>>();
//            for (var i = 0; i < 10; i++)
//            {
//                var year = DateTime.Now.AddYears(-i).Year;
//                list.Add(new KeyValuePair<int, int>(year, year));
//            }

//            ViewBag.ddlYears = new SelectList(list, "Key", "Value");

//            return View();
//        }

//        public ActionResult IncomeReportFilter(FormCollection form)
//        {
//            using (var service = ChannelProviderFactory<IOtherContractChannel>.Create("OtherClassName").GetChannel().CreateChannel())
//            {
//                var filter = new IncomeReportFilter()
//                {
//                    ClientCode = Convert.ToInt32(form["txtClientCode"]),
//                    Year = Convert.ToInt32(form["ddlYears"]),
//                    UserId = CommonHelper.GetLoggedUserGroupId()
//                };

//                var income = service.LoadIncomeReport(filter).Result;

//                if (income != null)
//                {
//                    Session["Other.IncomeReport"] = income;

//                    ViewBag.PositionDate = (income.PositionDate.HasValue) ? income.PositionDate.Value.ToString("dd/MM/yyyy") : "";

//                    if (income.IRRFDayTradeList.Count > 0)
//                    {
//                        if (income.IRRFDayTradeList.Count == 1)
//                        {
//                            TempData["IRRFDT"] = income.IRRFDayTradeList.FirstOrDefault().InitDate.Value.ToString(
//                                    "MMMM", System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat
//                                ).Capiralize() + " de " + income.IRRFDayTradeList.FirstOrDefault().InitDate.Value.ToString(
//                                    "yyyy", System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat
//                                );
//                            TempData["IRRFOperacoes"] = income.IRRFOperationsList.FirstOrDefault().InitDate.Value.ToString(
//                                    "MMMM", System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat
//                                ).Capiralize() + " de " + income.IRRFOperationsList.FirstOrDefault().InitDate.Value.ToString(
//                                    "yyyy", System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat
//                                );
//                        }
//                        else
//                        {
//                            var initial = income.IRRFDayTradeList.FirstOrDefault().InitDate.Value;
//                            var final = income.IRRFDayTradeList.LastOrDefault().InitDate.Value;

//                            TempData["IRRFDT"] = initial.ToString(
//                                    "MMMM", System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat
//                                ).Capiralize() + " a " + final.ToString(
//                                    "MMMM", System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat) + " de " +
//                                final.ToString(
//                                    "yyyy", System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat);

//                            initial = income.IRRFOperationsList.FirstOrDefault().InitDate.Value;
//                            final = income.IRRFOperationsList.LastOrDefault().InitDate.Value;

//                            TempData["IRRFOperacoes"] = initial.ToString(
//                                    "MMMM", System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat
//                                ).Capiralize() + " a " + final.ToString(
//                                    "MMMM", System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat) + " de " +
//                                final.ToString(
//                                    "yyyy", System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat);
//                        }
//                    }

//                    return PartialView("IncomeReportLists", income);
//                }
//                else
//                    return null;
//            }
//        }

//        public ActionResult IncomeReportToFileConfirm(bool isPdf)
//        {
//            return View(isPdf);
//        }

//        [HttpPost]
//        public ActionResult IncomeReportToFile(bool isPdf, bool chkPosicao, bool chkDayTrade, bool chkOperacoes)
//        {
//            try
//            {
//                IncomeReport item = new IncomeReport();
//                Dictionary<string, bool> checks = new Dictionary<string, bool>();
//                checks.Add("Posicao", chkPosicao);
//                checks.Add("DayTrade", chkDayTrade);
//                checks.Add("Operacoes", chkOperacoes);

//                if (Session["Other.IncomeReport"] != null)
//                {
//                    item = (IncomeReport)Session["Other.IncomeReport"];
//                    if ((checks["Posicao"] &&
//                            (item.StockMarketList.Count > 0 ||
//                            item.StockRentList.Count > 0 ||
//                            item.OptionsList.Count > 0 ||
//                            item.BMFOptionsFutureList.Count > 0 ||
//                            item.BMFGoldList.Count > 0 ||
//                            item.BMFSwapList.Count > 0 ||
//                            item.IncomeProceedsList.Count > 0 ||
//                            item.IncomeTermsList.Count > 0)) ||
//                        (checks["DayTrade"] && item.IRRFDayTradeList.Count > 0) ||
//                        (checks["Operacoes"] && item.IRRFOperationsList.Count > 0))
//                    {
//                        string fileName = string.Format("informe_rendimentos_{0}.{1}", DateTime.Now.ToString("dd-MM-yyyy"), isPdf ? "pdf" : "xls");
//                        return isPdf ? ViewIncomeReportPdf(item, checks, true, fileName) : ViewIncomeReportExcel(item, checks, true, fileName);
//                    }
//                    else
//                    {
//                        TempData["ErrorMessage"] = "Ocorreu um erro ao gerar relatório.";
//                        return this.RedirectToAction("IncomeReport");
//                    }
//                }
//                else
//                {
//                    TempData["ErrorMessage"] = "Ocorreu um erro ao gerar relatório.";
//                    return this.RedirectToAction("IncomeReport");
//                }
//            }
//            catch (Exception ex)
//            {
//                LogManager.WriteError(ex);
//                TempData["ErrorMessage"] = "Ocorreu um erro ao gerar relatório.";
//                return this.RedirectToAction("IncomeReport");
//            }
//        }

//        public string IncomeReport_StockMarketList(FormCollection form)
//        {
//            var items = (IncomeReport)Session["Other.IncomeReport"];
//            return new OtherHelper().IncomeReport_StockMarket(items, form);
//        }

//        public string IncomeReport_StockRentList(FormCollection form)
//        {
//            var items = (IncomeReport)Session["Other.IncomeReport"];
//            return new OtherHelper().IncomeReport_StockRent(items, form);
//        }

//        public string IncomeReport_OptionsList(FormCollection form)
//        {
//            var items = (IncomeReport)Session["Other.IncomeReport"];
//            return new OtherHelper().IncomeReport_Options(items, form);
//        }

//        public string IncomeReport_BMFOptionsFutureList(FormCollection form)
//        {
//            var items = (IncomeReport)Session["Other.IncomeReport"];
//            return new OtherHelper().IncomeReport_BMFOptionsFuture(items, form);
//        }

//        public string IncomeReport_BMFGoldList(FormCollection form)
//        {
//            var items = (IncomeReport)Session["Other.IncomeReport"];
//            return new OtherHelper().IncomeReport_BMFGold(items, form);
//        }

//        public string IncomeReport_BMFSwapList(FormCollection form)
//        {
//            var items = (IncomeReport)Session["Other.IncomeReport"];
//            return new OtherHelper().IncomeReport_BMFSwap(items, form);
//        }

//        public string IncomeReport_IncomeProceedsList(FormCollection form)
//        {
//            var items = (IncomeReport)Session["Other.IncomeReport"];
//            return new OtherHelper().IncomeReport_IncomeProceeds(items, form);
//        }

//        public string IncomeReport_IncomeTermsList(FormCollection form)
//        {
//            var items = (IncomeReport)Session["Other.IncomeReport"];
//            return new OtherHelper().IncomeReport_IncomeTerms(items, form);
//        }

//        public string IncomeReport_IRRFDayTradeList(FormCollection form)
//        {
//            var items = (IncomeReport)Session["Other.IncomeReport"];
//            return new OtherHelper().IncomeReport_IRRFDayTrade(items, form);
//        }

//        public string IncomeReport_IRRFOperationsList(FormCollection form)
//        {
//            var items = (IncomeReport)Session["Other.IncomeReport"];
//            return new OtherHelper().IncomeReport_IRRFOperations(items, form);
//        }

//        #endregion

//        #region Private

//        [AccessControlAuthorize]
//        public ActionResult PrivateReport()
//        {
//            var list = new List<KeyValuePair<int, int>>();
//            for (var i = 0; i < 10; i++)
//            {
//                var year = DateTime.Now.AddYears(-i).Year;
//                list.Add(new KeyValuePair<int, int>(year, year));
//            }

//            ViewBag.ddlYears = new SelectList(list, "Key", "Value", DateTime.Now.Year.ToString());

//            var listMonths = new List<KeyValuePair<int, string>>();
//            for (var i = 0; i <= 11; i++)
//            {
//                listMonths.Add(new KeyValuePair<int, string>(i + 1, System.Globalization.DateTimeFormatInfo.CurrentInfo.MonthNames[i].Capiralize()));
//            }

//            ViewBag.ddlMonths = new SelectList(listMonths, "Key", "Value", DateTime.Now.AddMonths(-1).Month.ToString());

//            return View();
//        }

//        [HttpPost]
//        public string PrivateReportFilter(FormCollection form)
//        {
//            var month = Convert.ToInt32(form["ddlMonths"]);
//            var year = Convert.ToInt32(form["ddlYears"]);
//            //var cpfcgc = Convert.ToInt64(form["txtCPFCGC"]);

//            var oldMonth = Session["PrivateFilterMonth"] == null ? 0 : (int)Session["PrivateFilterMonth"];
//            var oldYear = Session["PrivateFilterYear"] == null ? 0 : (int)Session["PrivateFilterYear"];

//            var initDate = Convert.ToDateTime("01/" + month + "/" + year);
//            var finalDate = Convert.ToDateTime(System.Globalization.DateTimeFormatInfo.CurrentInfo.Calendar.GetDaysInMonth(year, month) + "/" + month + "/" + year);

//            using (var otherService = ChannelProviderFactory<IOtherContractChannel>.Create("OtherClassName").GetChannel().CreateChannel())
//            {
//                WcfResponse<Private[]> privates = new WcfResponse<Private[]>();

//                if (Session["PrivateList"] == null || oldYear != year || oldMonth != month)
//                {
//                    privates = otherService.LoadPrivates(initDate, finalDate);
//                    Session["PrivateList"] = privates;

//                    Session["PrivateFilterMonth"] = month;
//                    Session["PrivateFilterYear"] = year;
//                }
//                else
//                {
//                    privates = (WcfResponse<Private[]>)Session["PrivateList"];
//                }

//                var refDate = System.Globalization.DateTimeFormatInfo.CurrentInfo.MonthNames[month - 1].Capiralize() + "/" + year.ToString();

//                return new OtherHelper().Private_List(
//                        (privates.Result == null)
//                        ? new List<Private>()
//                        : (String.IsNullOrEmpty(form["txtCPFCGC"]))
//                            ? privates.Result.ToList()
//                            : privates.Result.Where(a => a.CPFCGC.Equals(form["txtCPFCGC"].Trim())).ToList(), form, refDate);
//            }
//        }

//        [HttpPost]
//        public ActionResult PrivateReportFilterToFile(FormCollection form)
//        {
//            Response.Cache.SetCacheability(HttpCacheability.NoCache);

//            Session["PrivateFilterMonthToFile"] = Convert.ToInt32(form["ddlMonths"]);
//            Session["PrivateFilterYearToFile"] = Convert.ToInt32(form["ddlYears"]);
//            Session["PrivateFilterCPFCGCToFile"] = String.IsNullOrEmpty(form["txtCPFCGC"]) ? null : form["txtCPFCGC"];

//            return Json(true);
//        }

//        public ActionResult PrivateReportToFile(bool isPdf)
//        {
//            List<Private> currentList = new List<Private>();
//            try
//            {
//                var oldMonth = Session["PrivateFilterMonth"] == null ? 0 : (int)Session["PrivateFilterMonth"];
//                var oldYear = Session["PrivateFilterYear"] == null ? 0 : (int)Session["PrivateFilterYear"];

//                var fileMonth = (int)Session["PrivateFilterMonthToFile"];
//                var fileYear = (int)Session["PrivateFilterYearToFile"];

//                var refDate = System.Globalization.DateTimeFormatInfo.CurrentInfo.MonthNames[fileMonth - 1].Capiralize() + "/" + fileYear;

//                if (oldMonth == fileMonth && oldYear == fileYear)
//                {
//                    using (var service = ChannelProviderFactory<IOtherContractChannel>.Create("OtherClassName").GetChannel().CreateChannel())
//                    {
//                        var initDate = Convert.ToDateTime("01/" + oldMonth + "/" + oldYear);
//                        var finalDate = Convert.ToDateTime(System.Globalization.DateTimeFormatInfo.CurrentInfo.Calendar.GetDaysInMonth(oldYear, oldMonth) + "/" + oldMonth + "/" + oldYear);

//                        currentList = Session["PrivateFilterCPFCGCToFile"] == null
//                            ? service.LoadPrivates(initDate, finalDate).Result.ToList()
//                            : service.LoadPrivates(initDate, finalDate).Result.Where(a => a.CPFCGC.Equals(Session["PrivateFilterCPFCGCToFile"].ToString().Trim())).ToList();
//                    }
//                }
//                else
//                {
//                    currentList = Session["PrivateFilterCPFCGCToFile"] == null
//                        ? ((WcfResponse<Private[]>)Session["PrivateList"]).Result.ToList()
//                        : ((WcfResponse<Private[]>)Session["PrivateList"]).Result.Where(a => a.CPFCGC.Equals(Session["PrivateFilterCPFCGCToFile"].ToString().Trim())).ToList();
//                }

//                //LogHelper.SaveAuditingLog("Other.BrokerageDiscountToFile", string.Format(LogResources.ObjectToFileParameters, isPdf ? "PDF" : "Excel", JsonConvert.SerializeObject(currentList)));

//                string fileName = string.Format("relatorioprivate_{0}.{1}", DateTime.Now.ToString("dd-MM-yyyy"), isPdf ? "pdf" : "xls");

//                LogHelper.SaveHistoryLog("Other.PrivateReport", new HistoryData { Action = "Exportar", OldData = string.Empty, NewData = JsonConvert.SerializeObject(currentList) });

//                return isPdf ? ViewPrivateReportPdf(currentList, refDate, true, fileName) : ViewPrivateReportExcel(currentList, refDate, true, fileName);
//            }
//            catch (Exception ex)
//            {
//                LogManager.WriteError(ex);
//                TempData["ErrorMessage"] = "Ocorreu um erro ao gerar relatório.";
//                return this.RedirectToAction("PrivateReport");
//            }


//        }
        
//        [HttpPost]
//        public string ValidatePrivateReportFilter(string year, string month)
//        {
            
//            if (Convert.ToInt32(year) == DateTime.Now.Year)
//            {
//                if (Convert.ToInt32(month) >= DateTime.Now.Month)
//                    return "Período de consulta inválido";
//                return "";
//            }
//            else
//                return "";
//        }

//        #endregion

//        #region Aluguel BB

//        [AccessControlAuthorize]
//        public ActionResult BBRent()
//        {
//            using (var service = ChannelProviderFactory<IOtherContractChannel>.Create("OtherClassName").GetChannel().CreateChannel())
//            {
//                Dictionary<string, string> tradingDates = service.GetTradingDates().Result;
//                ViewBag.ddlTradingDate = new SelectList(tradingDates, "Key", "Value");
//            }
//            return View();
//        }

//        [HttpPost]
//        public ActionResult LoadBBRent(FormCollection form)
//        {
//            BBRent item = new BBRent();
//            IChannelProvider<IOtherContractChannel> otherProvider = ChannelProviderFactory<IOtherContractChannel>.Create("OtherClassName");
//            using (var otherService = otherProvider.GetChannel().CreateChannel())
//            {
//                item = otherService.LoadBBRent(Convert.ToDateTime(form["ddlTradingDate"])).Result;
//            }
//            if (item != null && (
//                item.Settlements.Count > 0 ||
//                item.Transfers.Count > 0 ||
//                item.Proceeds.Count > 0))
//            {
//                string fileName = string.Format("aluguel_bb_{0}.{1}", DateTime.Now.ToString("dd-MM-yyyy"), "xls");
//                return ViewBBRentExcel(item, true, fileName);
//            }
//            else
//            {
//                TempData["ErrorMessage"] = "Ocorreu um erro ao gerar relatório.";
//                return this.RedirectToAction("BBRent");
//            }
//        }

//        #endregion

//        #region Aluguel BB

//        [AccessControlAuthorize]
//        public ActionResult VAM()
//        {
//            using (var service = ChannelProviderFactory<IOtherContractChannel>.Create("OtherClassName").GetChannel().CreateChannel())
//            {
//                Dictionary<string, string> isins = service.GetISINs(CommonHelper.GetLoggedUserGroupId()).Result;
//                ViewBag.ddlIsin = new SelectList(isins, "Key", "Value");
//            }
//            return View();
//        }

//        [HttpPost]
//        public ActionResult LoadVAM(FormCollection form)
//        {
//            List<VAMItem> list = new List<VAMItem>();
//            IChannelProvider<IOtherContractChannel> otherProvider = ChannelProviderFactory<IOtherContractChannel>.Create("OtherClassName");
//            using (var otherService = otherProvider.GetChannel().CreateChannel())
//            {
//                list = otherService.LoadVAM(CommonHelper.GetLoggedUserGroupId(), form["ddlISIN"]).Result.ToList();
//            }
//            if (list != null && list.Count > 0)
//            {
//                string fileName = string.Format("posisao_vam_{0}.{1}", DateTime.Now.ToString("dd-MM-yyyy"), "xls");
//                return ViewVAMExcel(list, true, fileName);
//            }
//            else
//            {
//                TempData["ErrorMessage"] = "Ocorreu um erro ao gerar relatório.";
//                return this.RedirectToAction("VAM");
//            }
//        }

//        #endregion
//    }
}
