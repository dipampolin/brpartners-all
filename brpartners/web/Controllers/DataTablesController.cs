﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using QX3.Portal.WebSite.Models;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.WebSite.Properties;
using QX3.Portal.WebSite.Helper;

namespace QX3.Portal.WebSite.Controllers
{
    public class DataTablesController : Controller
    {
        #region NonResidentTraders

        IFormatProvider cultureInfo = new System.Globalization.CultureInfo("en-US", true);

        public ActionResult NonResidentTraders_Search(FormCollection form)
        {
            WcfResponse<NonResidentCustomer[]> response = (WcfResponse<NonResidentCustomer[]>)Session["Traders"];
            JsonModel jm = new JsonModel();

            if (response != null && response.Result != null)
            {
                if (response.Result != null)
                {
                    var NRCCompleteList = response.Result.ToList<NonResidentCustomer>();

                    List<String> list = new List<String>();

                    String[] column = { "Code", "Name" };

                    GenericSort(ref NRCCompleteList, this.SortListCommand(form, column));

                    //É a lista que irá mostrar um número limitado de registros dependendo da paginação corrente
                    var NRCPartialList = GenericPagingList(form, NRCCompleteList);

                    //Montagem do objeto Json
                    jm.sEcho = Convert.ToInt32(form["sEcho"]);
                    jm.iTotalRecords = NRCPartialList.Count;
                    jm.iTotalDisplayRecords = NRCCompleteList.Count;
                    jm.aaData = new List<String[]>();

                    for (int i = 0; i < NRCPartialList.Count; i++)
                    {
                        NonResidentCustomer nrc = (NonResidentCustomer)NRCPartialList[i];
                        list.Add(nrc.Code.ToString());
                        list.Add((!String.IsNullOrEmpty(nrc.Name)) ? nrc.Name : "");
                        list.Add(string.Format(String.Format("<a type=\"button\" id=\"insertTrader{0}\" class=\"btn-{3}\" href=\"javascript:void(0);\" onclick=\"InsertTrader({1});\"><span>{2}</span></a>", nrc.Code, nrc.Code, nrc.HasRegistration ? "Editar" : "Cadastrar", nrc.HasRegistration ? "secundario" : "padrao")));
                        jm.aaData.Add(list.ToArray());
                        list = new List<String>();
                    }
                }
            }

            return Json(jm);
        }

        public ActionResult NonResidentTraders_Confirmation(FormCollection form)
        {
            return Json(this.CustomersListJsonModel("ConfirmationClients", form));
        }

        public ActionResult NonResidentTraders_TradeBlotter(FormCollection form)
        {

            List<TradeBlotterItem> response = (List<TradeBlotterItem>)Session["TradeBlotterReports"];
            JsonModel jm = new JsonModel();

            if (response != null)
            {
                var CompleteList = response;

                List<String> list = new List<String>();

                String[] columns = { "TradeId", "TradeData", "SettlementDate", "ISINCode", "CompanyName", "NumberOfShares", "SharePrice", "CustomerBuyAmount", "NetBuyAmount", "CustomerSellAmount", "NetSellAmount", "CurrencyName", "ClientName" };

                GenericSort(ref CompleteList, this.SortListCommand(form, columns));

                var PartialList = GenericPagingList(form, CompleteList);

                jm.sEcho = Convert.ToInt32(form["sEcho"]);
                jm.iTotalRecords = PartialList.Count;
                jm.iTotalDisplayRecords = CompleteList.Count;
                jm.aaData = new List<String[]>();

                for (int i = 0; i < PartialList.Count; i++)
                {
                    TradeBlotterItem tbi = (TradeBlotterItem)PartialList[i];
                    list.Add(tbi.TradeId.ToString());
                    list.Add(tbi.TradeDate.ToString("MM/dd/yyyy", cultureInfo));
                    list.Add(tbi.SettlementDate.ToString("MM/dd/yyyy", cultureInfo));
                    list.Add(tbi.ISINCode.ToString());
                    list.Add(tbi.CompanyName.ToString());
                    list.Add(tbi.NumberOfShares.ToString("N0", cultureInfo));
                    list.Add(tbi.SharePrice.ToString("N2", cultureInfo));
                    list.Add(tbi.CustomerBuyAmount.ToString("N2", cultureInfo));
                    list.Add(tbi.NetBuyAmount.ToString("N2", cultureInfo));
                    list.Add(tbi.CustomerSellAmount.ToString("N2", cultureInfo));
                    list.Add(tbi.NetSellAmount.ToString("N2", cultureInfo));
                    list.Add(tbi.CurrencyName.ToString());
                    list.Add(tbi.ClientName.ToString());
                    jm.aaData.Add(list.ToArray());
                    list = new List<String>();
                }
            }

            return Json(jm);
        }

        public ActionResult NonResidentTraders_CustomerStatement(FormCollection form)
        {
            return Json(this.CustomersListJsonModel("CustomerStatementClients", form));
        }

        public ActionResult NonResidentTraders_CustomerLedger(FormCollection form)
        {
            return Json(this.CustomersListJsonModel("CustomerLedgerClients", form));
        }

        public ActionResult NonResidentTraders_StockRecord(FormCollection form)
        {
            return Json(this.CustomersListJsonModel("StockRecordClients", form));
        }

        public ActionResult NonResidentTraders_Failure(FormCollection form)
        {

            List<Failure> response = (List<Failure>)Session["Failures"];
            JsonModel jm = new JsonModel();

            if (response != null)
            {
                var CompleteList = response;

                List<String> list = new List<String>();

                String[] columns = { "TradeNumber", "TradeDate", "SettlementDate", "Quantity", "OperationValue", "TotalValue", "FailureQuantity", "FailureValue", "ChangeDate", "Username" };

                GenericSort(ref CompleteList, this.SortListCommand(form, columns));

                var PartialList = GenericPagingList(form, CompleteList);

                jm.sEcho = Convert.ToInt32(form["sEcho"]);
                jm.iTotalRecords = PartialList.Count;
                jm.iTotalDisplayRecords = CompleteList.Count;
                jm.aaData = new List<String[]>();

                for (int i = 0; i < PartialList.Count; i++)
                {
                    Failure tbi = (Failure)PartialList[i];
                    DateTime invalidDate = DateTime.Parse("01/01/0001");
                    string uniqueID = string.Concat(tbi.TradeNumber, "_", tbi.TradeDate.ToShortDateString().Replace("/", ""), "_", tbi.TradeCode, "_", tbi.ClientCode);

                    string keys = string.Format("{0};{1};{2};{3};{4}", tbi.ClientCode, tbi.TradeDate.ToShortDateString(), tbi.TradeCode, tbi.OperationType, tbi.TradeNumber);
                    string divValue = string.Format(Resources.FailureValueDiv, uniqueID, tbi.FailureValue.ToString("N2"), keys);
                    string divQuantity = string.Format(Resources.FailureQuantityDiv, uniqueID, tbi.FailureQuantity.ToString("N0"), keys);

                    list.Add(tbi.TradeNumber.ToString());
                    list.Add(tbi.TradeDate.ToString("MM/dd/yyyy"));
                    list.Add(tbi.SettlementDate.ToString("MM/dd/yyyy"));
                    list.Add(tbi.Quantity.ToString("N0"));
                    list.Add(tbi.OperationValue.ToString("N2"));
                    list.Add(tbi.TotalValue.ToString("N2"));
                    list.Add(string.Format("<a href='#' id='lnkQty{0}' onclick='ChangeFailureQuantity(\"{0}\", this);return false;'>{1}</a>{2}", uniqueID, tbi.FailureQuantity.ToString("N0"), divQuantity));
                    list.Add(string.Format("<a href='#' id='lnkVl{0}' onclick='ChangeFailureValue(\"{0}\", this);return false;'>{1}</a>{2}", uniqueID, tbi.FailureValue.ToString("N2"), divValue));
                    list.Add(string.Format("<label id='updateDate{0}'>{1}</label>", uniqueID, tbi.ChangeDate == invalidDate ? string.Empty : tbi.ChangeDate.ToString("dd/MM/yyyy")));
                    list.Add(string.Format("<label id='username{0}'>{1}</label>", uniqueID, tbi.Username));
                    jm.aaData.Add(list.ToArray());
                    list = new List<String>();
                }
            }

            return Json(jm);
        }

        private JsonModel CustomersListJsonModel(string sessionName, FormCollection form)
        {
            WcfResponse<Customer[]> response = (WcfResponse<Customer[]>)Session[sessionName];
            JsonModel jm = new JsonModel();

            if (response != null && response.Result != null)
            {
                var CompleteList = response.Result.ToList<Customer>();

                List<String> list = new List<String>();

                String[] columns = { "CheckAll", "Code", "Name", "Action" };

                GenericSort(ref CompleteList, this.SortListCommand(form, columns));

                var PartialList = GenericPagingList(form, CompleteList);

                jm.sEcho = Convert.ToInt32(form["sEcho"]);
                jm.iTotalRecords = PartialList.Count;
                jm.iTotalDisplayRecords = CompleteList.Count;
                jm.aaData = new List<String[]>();

                for (int i = 0; i < PartialList.Count; i++)
                {
                    Customer cust = (Customer)PartialList[i];
                    list.Add(String.Format("<input type='checkbox' id='client{0}' name='client{1}' value='{2}' onclick='addCodeIntoArray({3},false)'>", cust.Code, cust.Code, cust.Code, cust.Code));
                    list.Add(cust.Code.ToString());
                    list.Add((!String.IsNullOrEmpty(cust.Name)) ? cust.Name : "");
                    list.Add(string.Format(String.Format("<a href='#' class='btn-padrao' id='viewReport{0}' onclick='viewReport({1});'><span>Visualizar</span></a>", cust.Code, cust.Code)));
                    jm.aaData.Add(list.ToArray());
                    list = new List<String>();
                }
            }
            return jm;
        }

        #endregion

        #region AccessControl

        public ActionResult AccessControl_GroupOfAssessors(FormCollection form)
        {
            List<GroupOfAssessorsListItem> items = (List<GroupOfAssessorsListItem>)Session["GroupsOfAssessors"];
            JsonModel jm = new JsonModel();

            if (items != null)
            {
                var NRCCompleteList = items;

                List<String> list = new List<String>();

                String[] column = { "Name", "Assessors", "UsersCount" };

                GenericSort(ref NRCCompleteList, this.SortListCommand(form, column));

                var NRCPartialList = GenericPagingList(form, NRCCompleteList);

                jm.sEcho = Convert.ToInt32(form["sEcho"]);
                jm.iTotalRecords = NRCPartialList.Count;
                jm.iTotalDisplayRecords = NRCCompleteList.Count;
                jm.aaData = new List<String[]>();

                for (int i = 0; i < NRCPartialList.Count; i++)
                {
                    GroupOfAssessorsListItem nrc = (GroupOfAssessorsListItem)NRCPartialList[i];
                    list.Add((!String.IsNullOrEmpty(nrc.Name)) ? nrc.Name : "");
                    list.Add((!String.IsNullOrEmpty(nrc.Assessors)) ? nrc.Assessors : "");
                    list.Add(nrc.UsersCount.ToString("N0"));
                    list.Add(string.Format(String.Format("<a class=\"ico-visualizar\" href=\"javascript:void(0);\" id=\"linkId{0}_Check\"  title=\"visualizar grupo\" onclick=\"ViewGroup({0});return false;\"><span>visualizar</span></a><a href=\"javascript:void(0);\" title=\"editar grupo\" onclick=\"EditGroup({0});return false;\" class=\"ico-editar\">editar</a><a href=\"javascript:void(0);\" onclick=\"DeleteGroup({0}, '{1}');return false;\" title=\"excluir grupo\" class=\"ico-excluir\">excluir</a>", nrc.ID, nrc.Name)));
                    jm.aaData.Add(list.ToArray());
                    list = new List<String>();
                }

            }

            return Json(jm);
        }

        public ActionResult AccessControl_FuncionalitiesHistory(FormCollection form)
        {
            List<HistoryData> items = (List<HistoryData>)Session["FuncionalitiesHistory"];
            JsonModel jm = new JsonModel();

            if (items != null)
            {
                var NRCCompleteList = items;

                List<String> list = new List<String>();

                String[] column = { "Date", "Responsible", "Target" };

                GenericSort(ref NRCCompleteList, this.SortListCommand(form, column));

                var NRCPartialList = GenericPagingList(form, NRCCompleteList);

                jm.sEcho = Convert.ToInt32(form["sEcho"]);
                jm.iTotalRecords = NRCPartialList.Count;
                jm.iTotalDisplayRecords = NRCCompleteList.Count;
                jm.aaData = new List<String[]>();

                for (int i = 0; i < NRCPartialList.Count; i++)
                {
                    HistoryData nrc = (HistoryData)NRCPartialList[i];
                    list.Add(nrc.Date.ToString("dd/MM/yyyy - HH:mm"));
                    list.Add((!String.IsNullOrEmpty(nrc.Responsible)) ? nrc.Responsible : "");
                    list.Add(nrc.Target);
                    jm.aaData.Add(list.ToArray());
                    list = new List<String>();
                }

            }

            return Json(jm);
        }

        public ActionResult AccessControl_GroupsOfAssessorsHistory(FormCollection form)
        {
            List<HistoryData> items = (List<HistoryData>)Session["GroupsOfAssessorsHistory"];
            return this.AccessControl_History(form, items);
        }

        public ActionResult AccessControl_UsersHistory(FormCollection form)
        {
            List<HistoryData> items = (List<HistoryData>)Session["UsersHistory"];
            return this.AccessControl_History(form, items);
        }

        public ActionResult AccessControl_ProfilesHistory(FormCollection form)
        {
            List<HistoryData> items = (List<HistoryData>)Session["ProfilesHistory"];
            return this.AccessControl_History(form, items);
        }
        public ActionResult Proceeds_ProceedsHistory(FormCollection form)
        {
            List<HistoryData> items = (List<HistoryData>)Session["ProceedsHistory"];
            return this.AccessControl_History(form, items);
        }

        public ActionResult AccessControl_History(FormCollection form, List<HistoryData> items)
        {
            JsonModel jm = new JsonModel();

            if (items != null)
            {
                var NRCCompleteList = items;

                List<String> list = new List<String>();
                var hasExtraTarget = items.Select(s => s.ExtraTarget) != null;

                string[] column;

                column = hasExtraTarget ? new string[] { "Date", "Responsible", "Action", "Target", "ExtraTarget" } : new string[] { "Date", "Responsible", "Action", "Target" };

                GenericSort(ref NRCCompleteList, this.SortListCommand(form, column));

                var NRCPartialList = GenericPagingList(form, NRCCompleteList);

                jm.sEcho = Convert.ToInt32(form["sEcho"]);
                jm.iTotalRecords = NRCPartialList.Count;
                jm.iTotalDisplayRecords = NRCCompleteList.Count;
                jm.aaData = new List<String[]>();

                for (int i = 0; i < NRCPartialList.Count; i++)
                {
                    HistoryData nrc = (HistoryData)NRCPartialList[i];
                    list.Add(nrc.Date.ToString("dd/MM/yyyy - HH:mm"));
                    list.Add((!String.IsNullOrEmpty(nrc.Responsible)) ? nrc.Responsible : "-");
                    list.Add((!String.IsNullOrEmpty(nrc.Action)) ? nrc.Action : "-");
                    list.Add((!String.IsNullOrEmpty(nrc.Target)) ? nrc.Target : "-");
                    if (hasExtraTarget)
                        list.Add((!String.IsNullOrEmpty(nrc.ExtraTarget)) ? nrc.ExtraTarget : "-");
                    jm.aaData.Add(list.ToArray());
                    list = new List<String>();
                }

            }

            return Json(jm);
        }

        public ActionResult AccessControl_Users(FormCollection form)
        {
            var items = (List<UsersListItem>)Session["Users"];
            var jm = new JsonModel();

            if (items != null)
            {
                var nrcCompleteList = items;


                //var teste = form[]
                var list = new List<String>();

                //String[] column = { "", "ClientID", "CpfCnpj", "Name", "Login", "Email", "AssessorID", "Type", "ProfileName", "AssessorGroup", "AssessorInterval", "", "" };
                String[] column = { "", "CpfCnpj", "Name", "Login", "Email", "Type", "ProfileName", ""};



                GenericSort(ref nrcCompleteList, this.SortListCommand(form, column));

                var nrcPartialList = GenericPagingList(form, nrcCompleteList);

                jm.sEcho = Convert.ToInt32(form["sEcho"]);
                jm.iTotalRecords = nrcPartialList.Count;
                jm.iTotalDisplayRecords = nrcCompleteList.Count;
                jm.aaData = new List<String[]>();

                for (int i = 0; i < nrcPartialList.Count; i++)
                {
                    var nrc = nrcPartialList[i];
                    list.Add(string.Format(String.Format("<input type=\"checkbox\" name=\"chkUser[{0}]\" id=\"chkUser{1}\" value=\"{1}\"  /><input type=\"hidden\" name=\"chkUser[{0}]\" value=\"0\"  /> ", i, nrc.ID)));
                    //list.Add((!String.IsNullOrEmpty(Convert.ToString(nrc.ClientID))) ? Convert.ToString(nrc.ClientID) : "");
                    list.Add(nrc.CpfCnpj.HasValue ? CommonHelper.FormatCpfCnpj( nrc.CpfCnpj.Value) : "");
                    list.Add((!String.IsNullOrEmpty(nrc.Name)) ? nrc.Name : "");
                    list.Add((!String.IsNullOrEmpty(nrc.Login)) ? nrc.Login : "");
                    list.Add((!String.IsNullOrEmpty(nrc.Email)) ? nrc.Email : "");
                    //list.Add((!String.IsNullOrEmpty(nrc.AssessorID)) ? nrc.AssessorID : " - ");
                    list.Add((!String.IsNullOrEmpty(nrc.Type)) ? (nrc.Type == "I" ? "Interno" : "Externo") : " - ");
                    list.Add((!String.IsNullOrEmpty(nrc.ProfileName)) ? nrc.ProfileName : " - ");
                    //list.Add((!String.IsNullOrEmpty(nrc.AssessorGroup)) ? nrc.AssessorGroup : " - ");
                    //list.Add((!String.IsNullOrEmpty(nrc.AssessorInterval)) ? nrc.AssessorInterval : " - ");
                    //list.Add(
                    //    string.Format(
                    //        String.Format(
                    //            "<a id=\"btnAssociateGroup\" onclick=\"AssociateToGroup({0})\" href=\"javascript:;\">Grupo</a></td>", nrc.ID)));
                    list.Add(
                        string.Format(
                            String.Format(
								"<a href=\"javascript:void(0);\" id=\"linkId{0}_Check\" title=\"editar usuário\" onclick=\"EditUser({0});return false;\" class=\"ico-editar\">editar</a><a href=\"javascript:void(0);\" title=\"redefinir senha\" onclick=\"ResetPassword({0});return false;\" class=\"ico-resetar\">redefinir senha</a><a href=\"javascript:void(0);\" onclick=\"DeleteUser({0});return false;\" title=\"excluir usuário\" class=\"ico-excluir\">excluir</a>",
                                nrc.ID, nrc.Name)));
                    list.Add((!String.IsNullOrEmpty(nrc.Blocked)) ? nrc.Blocked : string.Empty);
                    jm.aaData.Add(list.ToArray());
                    list = new List<String>();
                }

            }


            return Json(jm);
        }

        public ActionResult AccessControl_Profiles(FormCollection form)
        {
            List<ProfilesListItem> items = (List<ProfilesListItem>)Session["Profiles"];
            JsonModel jm = new JsonModel();

            if (items != null)
            {
                var NRCCompleteList = items;

                List<String> list = new List<String>();

                String[] column = { "Name", "UsersCount" };

                GenericSort(ref NRCCompleteList, this.SortListCommand(form, column));

                var NRCPartialList = GenericPagingList(form, NRCCompleteList);

                jm.sEcho = Convert.ToInt32(form["sEcho"]);
                jm.iTotalRecords = NRCPartialList.Count;
                jm.iTotalDisplayRecords = NRCCompleteList.Count;
                jm.aaData = new List<String[]>();

                for (int i = 0; i < NRCPartialList.Count; i++)
                {
                    ProfilesListItem nrc = (ProfilesListItem)NRCPartialList[i];
                    list.Add((!String.IsNullOrEmpty(nrc.Name)) ? nrc.Name : "");
                    list.Add((!String.IsNullOrEmpty(nrc.UsersCount.ToString("N0"))) ? nrc.UsersCount.ToString("N0") : "");
                    list.Add(string.Format(String.Format("<a class=\"ico-visualizar\" href=\"javascript:void(0);\" id=\"linkId{0}_Check\"  title=\"visualizar perfil\" onclick=\"ViewProfile({0});return false;\"><span>visualizar</span></a><a href=\"javascript:void(0);\" title=\"editar perfil\" onclick=\"EditProfile({0});return false;\" class=\"ico-editar\">editar</a><a href=\"javascript:void(0);\" onclick=\"DeleteProfile({0}, '{1}');return false;\" title=\"excluir perfil\" class=\"ico-excluir\">excluir</a>", nrc.ID, nrc.Name)));
                    jm.aaData.Add(list.ToArray());
                    list = new List<String>();
                }

            }

            return Json(jm);
        }

        #endregion

        public ActionResult Proceeds_List(FormCollection form)
        {
            List<Proceeds> items = (List<Proceeds>)Session["Proceeds"];
            JsonModel jm = new JsonModel();

            if (items != null)
            {
                var NRCCompleteList = items;

                List<String> list = new List<String>();

                String[] column = { "StockCode", "ISIN", "AssessorCode", "AssessorName", "ClientCode", "ClientName", "Proceed", "Quantity", "GrossValue", "IR", "IRValue", "NetValue", "PaymentDate", "Portfolio" };

                GenericSort(ref NRCCompleteList, this.SortListCommand(form, column));

                var NRCPartialList = GenericPagingList(form, NRCCompleteList);

                jm.sEcho = Convert.ToInt32(form["sEcho"]);
                jm.iTotalRecords = NRCPartialList.Count;
                jm.iTotalDisplayRecords = NRCCompleteList.Count;
                jm.aaData = new List<String[]>();

                for (int i = 0; i < NRCPartialList.Count; i++)
                {
                    Proceeds nrc = (Proceeds)NRCPartialList[i];

                    list.Add(nrc.StockCode);
                    list.Add(nrc.ISIN);
                    list.Add(nrc.AssessorCode.ToString().PadLeft(3, '0'));
                    list.Add(nrc.AssessorName);
                    list.Add(nrc.ClientCode.ToString().PadLeft(6, '0'));
                    list.Add(nrc.ClientName);
                    list.Add(nrc.Proceed);
                    list.Add(nrc.Quantity.ToString("N0"));
                    list.Add(nrc.GrossValue.ToString("N2"));
                    list.Add(nrc.IR.ToString("N2"));
                    list.Add(nrc.IRValue.ToString("N2"));
                    list.Add(nrc.NetValue.ToString("N2"));
                    list.Add(nrc.PaymentDate.ToString("dd/MM/yyyy"));
                    list.Add(nrc.Portfolio.ToString());

                    jm.aaData.Add(list.ToArray());
                    list = new List<String>();
                }

            }

            return Json(jm);
        }

        public ActionResult Term_List(FormCollection form)
        {
            List<TermListItem> items = (List<TermListItem>)Session["Terms"];
            JsonModel jm = new JsonModel();

            if (items != null)
            {
                var NRCCompleteList = items;

                List<String> list = new List<String>();

                String[] column = { "", "Client", "Code", "Assessor", "Stock", "Company", "Contract", "AvailableQuantity", "TotalQuantity", "SettleQuantity", "TypeDescription", "SettleDate", "StatusDescription", "RolloverDate", "" };

                Dictionary<String, Type> columnTypes = new Dictionary<string, Type>();

                columnTypes.Add("Client", typeof(System.String));
                columnTypes.Add("Code", typeof(System.Int32));
                columnTypes.Add("Assessor", typeof(System.String));
                columnTypes.Add("Stock", typeof(System.String));
                columnTypes.Add("Company", typeof(System.String));
                columnTypes.Add("Contract", typeof(System.Int32));
                columnTypes.Add("AvailableQuantity", typeof(System.Int32));
                columnTypes.Add("TotalQuantity", typeof(System.Int32));
                columnTypes.Add("SettleQuantity", typeof(System.Int32));
                columnTypes.Add("TypeDescription", typeof(System.String));
                columnTypes.Add("SettleDate", typeof(System.DateTime));
                columnTypes.Add("StatusDescription", typeof(System.String));
                columnTypes.Add("RolloverDate", typeof(System.DateTime));

                var sort = this.SortListCommand(form, column);

                if (sort.Contains("SettleDate")) {
                
                    foreach (var nrc in NRCCompleteList)
                    {
                        if (nrc.StatusId.Equals("-1"))
                            nrc.SettleDate = nrc.DueDate;
                    }

                }

                GenericSort(ref NRCCompleteList, this.SortListCommand(form, column));

                var NRCPartialList = GenericPagingList(form, NRCCompleteList);

                jm.sEcho = Convert.ToInt32(form["sEcho"]);
                jm.iTotalRecords = NRCPartialList.Count;
                jm.iTotalDisplayRecords = NRCCompleteList.Count;
                jm.aaData = new List<String[]>();

                string d3 = MarketHelper.GetBusinessDay(3);
                DateTime? deadline = null;
                if (!string.IsNullOrEmpty(d3))
                    deadline = Convert.ToDateTime(d3);

                AccessControlHelper acHelper = new AccessControlHelper();
                bool canEdit = acHelper.HasFuncionalityPermission("Term.Index", "btnEdit");
                bool canDelete = acHelper.HasFuncionalityPermission("Term.Index", "btnDelete");
                bool canRealize = acHelper.HasFuncionalityPermission("Term.Index", "btnSettle");
                bool canSeeLog = acHelper.HasFuncionalityPermission("Term.Index", "btnLog");
                bool canRequest = acHelper.HasFuncionalityPermission("Term.Index", "btnRequest");

                for (int i = 0; i < NRCPartialList.Count; i++)
                {
                    TermListItem nrc = (TermListItem)NRCPartialList[i];

                    string disabled = string.Empty;
                    bool statusDisabled = nrc.StatusId.Equals("10");

                    if (statusDisabled)
                        disabled = "disabled=\"disabled\"";
                    else if (!canRequest && nrc.StatusId.Equals("-1"))
                        disabled = "disabled=\"disabled\"";
                    else if (!canRealize && nrc.StatusId.Equals("9"))
                        disabled = "disabled=\"disabled\"";

                    list.Add(string.Format(String.Format("<input type=\"checkbox\" name=\"chkTerm-{0}\" id=\"chkTerm-{0}\" value=\"{1}\" {2} />", nrc.ID.ToString(), nrc.Index.ToString(), disabled)));
                    list.Add(nrc.Client);
                    list.Add(nrc.Code);
                    list.Add(nrc.Assessor);
                    list.Add(nrc.Stock);
                    list.Add(nrc.Company);
                    list.Add(nrc.Contract.ToString("N0"));
                    list.Add(nrc.AvailableQuantity.ToString("N0"));
                    list.Add(nrc.TotalQuantity.ToString("N0"));
                    list.Add(nrc.SettleQuantity.ToString("N0"));
                    list.Add(nrc.TypeDescription);

                    if (nrc.StatusId.Equals("9"))
                    {
                        string settleDate = nrc.SettleDate.Value.ToString("dd/MM/yyyy");
                        list.Add((deadline.HasValue && DateTime.Compare(deadline.Value, nrc.SettleDate.Value) < 0) ? settleDate : string.Format("<span class=\"warning\">{0}</span>", settleDate));
                    }
                    else if(nrc.StatusId.Equals("-1"))
                        list.Add(nrc.DueDate.HasValue ? nrc.DueDate.Value.ToString("dd/MM/yyyy") : "");
                    else
                        list.Add(nrc.SettleDate.HasValue ? nrc.SettleDate.Value.ToString("dd/MM/yyyy") : "");

                    list.Add(nrc.StatusId.Equals("9") && canRealize ? string.Format("<a class=\"exibir-modal\" href=\"javascript:;\" onclick=\"ChangeStatus(this,{0}, {2});return false;\">{1}</a>", nrc.ID, nrc.StatusDescription, nrc.StatusId) : nrc.StatusDescription);
                    list.Add(nrc.RolloverDate.HasValue ? nrc.RolloverDate.Value.ToShortDateString() : "-");

                    if (!nrc.StatusId.Equals("-1"))
                    {
                        string actions = string.Empty;
                        if (nrc.StatusId.Equals("9"))
                        {
                            actions += canEdit ? string.Format("<a href=\"javascript:;\" title=\"editar\" class=\"ico-editar\" onclick=\"OpenRequest({0});return false;\">editar</a>", nrc.ID.ToString()) : string.Empty;
                            actions += canDelete ? string.Format("<a class=\"ico-excluir\" title=\"excluir\" onclick=\"DeleteRequest({0});return false;\" href=\"javascript:;\">excluir</a>", nrc.ID.ToString()) : string.Empty;
                        }
                        else
                            actions += string.Format("<img class=\"espaco\" src=\"../../img/ico/icon-space-table.gif\" /><img class=\"espaco\" src=\"../../img/ico/icon-space-table.gif\" />");
                        actions += canSeeLog ? string.Format("<a href=\"javascript:;\" title=\"visualizar\" class=\"ico-hist\" onclick=\"OpenHistoryDetail({0});return false;\">histórico</a>", nrc.ID.ToString()) : string.Empty;
                        list.Add(actions);
                    }
                    else
                        list.Add("");
                    

                    jm.aaData.Add(list.ToArray());
                    list = new List<String>();
                }

            }

            return Json(jm);
        }

        public ActionResult Other_OrdersCorrectionHistory(FormCollection form)
        {
            List<HistoryData> items = (List<HistoryData>)Session["OrdersCorrectionHistory"];
            return this.AccessControl_History(form, items);
        }

        public ActionResult Term_TermHistory(FormCollection form)
        {
            List<HistoryData> items = (List<HistoryData>)Session["TermHistory"];
            return this.AccessControl_History(form, items);
        }

        public ActionResult Risk_GuaranteesHistory(FormCollection form)
        {
            List<HistoryData> items = (List<HistoryData>)Session["GuaranteesHistory"];
            return this.AccessControl_History(form, items);
        }

        public ActionResult Financial_RedemptionRequestHistory(FormCollection form)
        {
            List<HistoryData> items = (List<HistoryData>)Session["RedemptionRequestHistory"];
            return this.AccessControl_History(form, items);
        }

        public ActionResult PortfolioAdminClients_List(FormCollection form)
        {
            List<PortfolioClientListItem> items = (List<PortfolioClientListItem>)Session["OnlinePortfolio.AdminClients"];
            JsonModel jm = new JsonModel();

            if (items != null)
            {
                var NRCCompleteList = items;

                List<String> list = new List<String>();

                String[] column = { "ClientCode", "ClientName", "Operators", "CalculationDescription", "" };

                GenericSort(ref NRCCompleteList, this.SortListCommand(form, column));

                var NRCPartialList = GenericPagingList(form, NRCCompleteList);

                jm.sEcho = Convert.ToInt32(form["sEcho"]);
                jm.iTotalRecords = NRCPartialList.Count;
                jm.iTotalDisplayRecords = NRCCompleteList.Count;
                jm.aaData = new List<String[]>();

                string d3 = MarketHelper.GetBusinessDay(3);
                DateTime? deadline = null;
                if (!string.IsNullOrEmpty(d3))
                    deadline = Convert.ToDateTime(d3);

                AccessControlHelper acHelper = new AccessControlHelper();
                bool canEdit = acHelper.HasFuncionalityPermission("OnlinePortfolio.AdminClients", "btnEdit");
                bool canDelete = acHelper.HasFuncionalityPermission("OnlinePortfolio.AdminClients", "btnDelete");
                //bool canSeeLog = acHelper.HasFuncionalityPermission("OnlinePortfolio.AdminClients", "btnLog");

                for (int i = 0; i < NRCPartialList.Count; i++)
                {
                    PortfolioClientListItem nrc = (PortfolioClientListItem)NRCPartialList[i];

                    list.Add(nrc.ClientCode.ToString());
                    list.Add(nrc.ClientName);
                    list.Add(nrc.Operators);
                    list.Add(nrc.CalculationDescription);

                    string actions = string.Empty;

                    actions += canEdit ? string.Format("<a href=\"javascript:;\" title=\"editar\" class=\"ico-editar\" onclick=\"OpenRequest({0});return false;\">editar</a>", nrc.ClientCode.ToString()) : string.Empty;
                    actions += canDelete ? string.Format("<a class=\"ico-excluir\" title=\"excluir\" onclick=\"DeleteRequest({0},'{1}');return false;\" href=\"javascript:;\">excluir</a>", nrc.ClientCode.ToString(), nrc.ClientName) : string.Empty;
                    //actions += canSeeLog ? string.Format("<a href=\"javascript:;\" title=\"visualizar\" class=\"ico-hist\" onclick=\"OpenHistoryDetail({0});return false;\">histórico</a>", nrc.ID.ToString()) : string.Empty;
                    list.Add(actions);

                    jm.aaData.Add(list.ToArray());
                    list = new List<String>();
                }

            }

            return Json(jm);
        }

        public ActionResult RedemptionRequest_List(FormCollection form)
        {
            List<RedemptionRequestListItem> items = (List<RedemptionRequestListItem>)Session["RedemptionRequest.List"];
            JsonModel jm = new JsonModel();

            if (items != null)
            {
                var NRCCompleteList = items;

                List<String> list = new List<String>();

                String[] column = { "", "RequestedDate", "ClientCode", "ClientName", "Bank", "Agency", "Account", "Amount", "StatusId", "" };

                Dictionary<string, Type> columnTypes = new Dictionary<string, Type>();
                columnTypes.Add("RequestedDate", typeof(System.DateTime));
                columnTypes.Add("ClientCode", typeof(System.String));
                columnTypes.Add("ClientName", typeof(System.String));
                columnTypes.Add("Bank", typeof(System.String));
                columnTypes.Add("Agency", typeof(System.Decimal));
                columnTypes.Add("Account", typeof(System.String));
                columnTypes.Add("Amount", typeof(System.Decimal));
                columnTypes.Add("StatusId", typeof(System.String));

                GenericSort(ref NRCCompleteList, this.SortListCommand(form, column), columnTypes);

                var NRCPartialList = GenericPagingList(form, NRCCompleteList);

                jm.sEcho = Convert.ToInt32(form["sEcho"]);
                jm.iTotalRecords = NRCPartialList.Count;
                jm.iTotalDisplayRecords = NRCCompleteList.Count;
                jm.aaData = new List<String[]>();

                AccessControlHelper acHelper = new AccessControlHelper();

                // TODO: Lógica das actions
                bool canEdit = acHelper.HasFuncionalityPermission("Financial.RedemptionRequest", "btnEdit");
                bool canDelete = acHelper.HasFuncionalityPermission("Financial.RedemptionRequest", "btnDelete");
                bool canApprove = acHelper.HasFuncionalityPermission("Financial.RedemptionRequest", "btnApprove");
                bool canEffetive = acHelper.HasFuncionalityPermission("Financial.RedemptionRequest", "btnEffective");
                bool canSeeLog = acHelper.HasFuncionalityPermission("Financial.RedemptionRequest", "btnLog");

                for (int i = 0; i < NRCPartialList.Count; i++)
                {
                    RedemptionRequestListItem nrc = (RedemptionRequestListItem)NRCPartialList[i];

                    string disabled = (nrc.StatusId.Equals("18")) ? "" : "disabled=\"disabled\"";
                    list.Add(string.Format(String.Format("<input type=\"checkbox\" name=\"chkRequest-{0}\" id=\"chkRequest-{0}\" value=\"{1}\" {2} rel=\"{3}\" />", nrc.Id.ToString(), nrc.Index.ToString(), disabled, nrc.StatusId)));
                    list.Add(nrc.RequestedDate);
                    list.Add(nrc.ClientCode);
                    list.Add(nrc.ClientName);
                    list.Add(nrc.Bank);
                    list.Add(nrc.Agency);
                    list.Add(nrc.Account);
                    list.Add(nrc.Amount);

                    string clientCode = nrc.ClientCode;

                    string[] codeParams = nrc.ClientCode.Split('-');
                    if (codeParams.Length > 0)
                        clientCode = codeParams[0];

                    #region status / update
                    if (!nrc.StatusId.Equals("19") && !nrc.StatusId.Equals("20"))  // efetuado e reprovado
                    {
                        string update = string.Empty;

                        if (nrc.StatusId.Equals("17") && canApprove) // para aprovar
                            update = string.Format("<a href=\"javascript:void(0);\" title=\"editar\" onclick=\"ChangeStatus(this, '{0}', '{1}', '{2}');return false;\">{3}</a>", nrc.Id, clientCode, nrc.StatusId, nrc.StatusDescription);

                        if (nrc.StatusId.Equals("18") && canEffetive) // para efetuar
                            update = string.Format("<a href=\"javascript:void(0);\" title=\"editar\" onclick=\"ChangeStatus(this, '{0}', '{1}', '{2}');return false;\">{3}</a>", nrc.Id, clientCode, nrc.StatusId, nrc.StatusDescription);

                        list.Add(string.IsNullOrEmpty(update) ? nrc.StatusDescription : update);
                    }
                    else
                        list.Add(nrc.StatusDescription);
                    #endregion

                    #region buttons
                    string actions = string.Empty;

                    if (!nrc.StatusId.Equals("19")) // efetuado
                        actions += canEdit ? string.Format("<a href=\"javascript:void(0);\" title=\"editar\" class=\"ico-editar\" onclick=\"OpenRequest({0});return false;\">editar</a>", nrc.Id.ToString()) : string.Empty;
                    else
                        actions += string.Format("<img class=\"espaco\" src=\"../../img/ico/icon-space-table.gif\" />");

                    if (!nrc.StatusId.Equals("19") && !nrc.StatusId.Equals("20")) // efetuado e reprovado
                        actions += canDelete ? string.Format("<a class=\"ico-excluir\" title=\"excluir\" onclick=\"DeleteRequest({0},{1});return false;\" href=\"javascript:void(0);\">excluir</a>", nrc.Id.ToString(), clientCode) : string.Empty;
                    else
                        actions += string.Format("<img class=\"espaco\" src=\"../../img/ico/icon-space-table.gif\" />");

                    actions += canSeeLog ? string.Format("<a href=\"javascript:void(0);\" title=\"visualizar\" class=\"ico-hist\" onclick=\"OpenHistoryDetail({0});return false;\">histórico</a>", nrc.Id.ToString()) : string.Empty;

                    list.Add(actions);
                    #endregion

                    jm.aaData.Add(list.ToArray());
                    list = new List<String>();
                }

            }

            return Json(jm);
        }

        #region Common

        private void GenericSort<T>(ref List<T> list, string sOrder)
        {
            if (list.Count > 0)
            {
                Type t = list[0].GetType();
                string[] OrderCommand = sOrder.Split('_');
                string OrderByProperty = OrderCommand[0];
                string Direction = OrderCommand[1];


                if (t.GetProperty(OrderByProperty) != null)
                {
                    if (Direction == "asc")
                    {
                        list = list.OrderBy
                            (
                                a => {
                                    var l = t.InvokeMember(OrderByProperty, System.Reflection.BindingFlags.GetProperty, null, a,
                                                 null);
                                    return l;
                                }
                            ).ToList();
                    }
                    else
                    {
                        list = list.OrderByDescending
                            (
                                a =>
                                t.InvokeMember(OrderByProperty, System.Reflection.BindingFlags.GetProperty, null, a,
                                                null)
                            ).ToList();
                    }
                }

            }
        }

        private void GenericSort<T>(ref List<T> list, string sOrder, Dictionary<string, Type> columnTypes)
        {
            if (list.Count > 0)
            {
                Type t = list[0].GetType();
                string[] OrderCommand = sOrder.Split('_');
                string OrderByProperty = OrderCommand[0];
                string Direction = OrderCommand[1];

                var j = columnTypes[OrderByProperty];


                if (t.GetProperty(OrderByProperty) != null)
                {
                    if (Direction == "asc")
                    {
                        list = list.OrderBy
                            (
                                a => {
                                    var l = t.InvokeMember(OrderByProperty, System.Reflection.BindingFlags.GetProperty, null, a,
                                                  null);
                                    if (l.GetType() == j)
                                        return l;
                                    else
                                        return System.ComponentModel.TypeDescriptor.GetConverter(j).ConvertFrom(l);
                                }
                            ).ToList();
                    }
                    else
                    {
                        list = list.OrderByDescending
                            (
                                a =>
                                {
                                    var l = t.InvokeMember(OrderByProperty, System.Reflection.BindingFlags.GetProperty, null, a,
                                                  null);
                                    if (l.GetType() == j)
                                        return l;
                                    else
                                        return System.ComponentModel.TypeDescriptor.GetConverter(j).ConvertFrom(l);
                                }
                            ).ToList();
                    }
                }

            }
        }

        private List<T> GenericPagingList<T>(FormCollection form, List<T> list)
        {
            List<T> _list = new List<T>();

            if (list.Count > 0)
            {
                if (!String.IsNullOrEmpty(form["iDisplayStart"]) && (!String.IsNullOrEmpty(form["iDisplayLength"])) && form["iDisplayLength"] != "-1")
                {
                    try
                    {
                        _list = list.AsEnumerable().Skip(Convert.ToInt32(form["iDisplayStart"])).Take(Convert.ToInt32(form["iDisplayLength"])).ToList();
                    }
                    catch (InvalidOperationException)
                    {
                        _list = list.AsEnumerable().Skip(0).Take(Convert.ToInt32(form["iDisplayLength"])).ToList();
                    }
                }
            }

            return _list;
        }

        private string SortListCommand(FormCollection form, String[] conlumn)
        {
            string sOrder = String.Empty;

            if (!String.IsNullOrEmpty(form["iSortCol_0"]) && !String.IsNullOrEmpty(form["iSortingCols"]))
            {
                int columnIndex = Convert.ToInt32(form["iSortCol_0"]);
                string columnName = conlumn[columnIndex];
                sOrder += columnName;
                sOrder += "_" + form["sSortDir_0"];
            }
            return sOrder;
        }

        #endregion


        public ActionResult ProjectedReport_List(FormCollection form)
        {
            List<ProjectedReportItem> items = (List<ProjectedReportItem>)Session["Financial.ProjectedReport"];
            JsonModel jm = new JsonModel();

            if (items != null)
            {
                var NRCCompleteList = items;

                List<String> list = new List<String>();

                String[] column = { "Assessor", "TradingDate", "ClientId", "ClientCode", "Percentage", "Note", "Volume", "Brokerage", "Record", "Fees", "SettlementDate", "ANA", "IRRF", "NetValue" };

                GenericSort(ref NRCCompleteList, this.SortListCommand(form, column));

                var NRCPartialList = GenericPagingList(form, NRCCompleteList);

                jm.sEcho = Convert.ToInt32(form["sEcho"]);
                jm.iTotalRecords = NRCPartialList.Count;
                jm.iTotalDisplayRecords = NRCCompleteList.Count;
                jm.aaData = new List<String[]>();

                for (int i = 0; i < NRCPartialList.Count; i++)
                {
                    ProjectedReportItem item = (ProjectedReportItem)NRCPartialList[i];

                    list.Add(item.Assessor);
                    list.Add(item.TradingDate.ToShortDateString());
                    list.Add(item.ClientID);
                    list.Add(item.ClientName);
                    list.Add(item.Percentage.ToString("N2"));
                    list.Add(item.Note.ToString());
                    list.Add(item.Volume.ToString("N2"));
                    list.Add(item.Brokerage.ToString("N2"));
                    list.Add(item.Record.ToString("N2"));
                    list.Add(item.Fees.ToString("N2"));
                    list.Add(item.SettlementDate.ToShortDateString());
                    list.Add(item.ANA.ToString("N2"));
                    list.Add(item.IRRF.ToString("N2"));
                    list.Add(item.NetValue.ToString("N2"));

                    jm.aaData.Add(list.ToArray());
                    list = new List<String>();
                }

            }

            return Json(jm);
        }

        //public ActionResult Funds_History(FormCollection form, List<HistoryData> items)
        //{
        //    List<QX3.Portal.Services.SagazWebService.Fund> funds = new List<Services.SagazWebService.Fund>();
        //    using (var service = QX3.Portal.WebSite.Models.Channel.ChannelProviderFactory<QX3.Portal.WebSite.FundService.IFundContractChannel>.Create("FundClassName").GetChannel().CreateChannel()) 
        //    {
        //        funds = service.FN_LoadFunds().Result;
        //    }

        //    JsonModel jm = new JsonModel();

        //    if (items != null)
        //    {
        //        var NRCCompleteList = items;

        //        List<String> list = new List<String>();
        //        var hasExtraTarget = items.Select(s => s.ExtraTarget) != null;

        //        string[] column;

        //        column = hasExtraTarget 
        //            ? new string[] { "Date", "Responsible", "Action", "ID" , "Target", "ExtraTarget" }
        //            : new string[] { "Date", "Responsible", "Action", "ID", "Target" };

        //        GenericSort(ref NRCCompleteList, this.SortListCommand(form, column));

        //        var NRCPartialList = GenericPagingList(form, NRCCompleteList);

        //        jm.sEcho = Convert.ToInt32(form["sEcho"]);
        //        jm.iTotalRecords = NRCPartialList.Count;
        //        jm.iTotalDisplayRecords = NRCCompleteList.Count;
        //        jm.aaData = new List<String[]>();

        //        for (int i = 0; i < NRCPartialList.Count; i++)
        //        {
        //            HistoryData nrc = (HistoryData)NRCPartialList[i];

        //            list.Add(nrc.Date.ToString("dd/MM/yyyy - HH:mm"));
        //            list.Add((!String.IsNullOrEmpty(nrc.Responsible)) ? nrc.Responsible : "-");
        //            list.Add((!String.IsNullOrEmpty(nrc.Action)) ? nrc.Action : "-");
        //            list.Add(nrc.ID != 0 ? CommonHelper.GetClientName(nrc.ID) : "-");

        //            list.Add(!String.IsNullOrEmpty(nrc.Target) ? (funds.Where(a => a.Code == Convert.ToInt32(nrc.Target)).FirstOrDefault() != null) ? funds.Where(a => a.Code == Convert.ToInt32(nrc.Target)).FirstOrDefault().Name : "-" : "-");
        //            if (hasExtraTarget)
        //                list.Add((!String.IsNullOrEmpty(nrc.ExtraTarget)) ? nrc.ExtraTarget : "-");
        //            jm.aaData.Add(list.ToArray());
        //            list = new List<String>();
        //        }
                
        //    }

        //    return Json(jm);
        //}

		public ActionResult AccessControl_ClientsRegisterHistory(FormCollection form, List<HistoryData> items)
		{
			return this.AccessControl_History(form, items);
		}
	}
}