﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.DataVisualization.Charting;
using Newtonsoft.Json;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.WebSite.AccessControlService;
using QX3.Portal.WebSite.CommonService;

using QX3.Portal.WebSite.Helper;

using QX3.Portal.WebSite.Models;
using QX3.Portal.WebSite.Models.Channel;
using QX3.Portal.WebSite.OtherService;
using QX3.Portal.WebSite.Properties;
using QX3.Spinnex.Common.Services.Logging;

namespace QX3.Portal.WebSite.Controllers
{
    public class MailController : Controller
    {
        public ActionResult EditEmailModel(int funcionalityId)
        {
            //pegar funcionalidade

            var accessControlService = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName").GetChannel().CreateChannel();

            accessControlService.Open();

            var funcionalities = accessControlService.ListFuncionalities().Result;

            accessControlService.Close();

            MailModel model = null;

            if (funcionalities != null) 
            {
                ViewBag.Funcionality = funcionalities.Where(a => a.FuncId == funcionalityId).FirstOrDefault();

                var commonService = ChannelProviderFactory<ICommonContractChannel>.Create("CommonClassName").GetChannel().CreateChannel();

                commonService.Open();

                model = commonService.GetMailModel(funcionalityId).Result;

                ViewBag.TagModel = commonService.GetTagModel().Result.ToList();

                commonService.Close();
            }            

            return View(model);
        }

        [ValidateInput(false)]
        public ActionResult UpdateEmailModel(FormCollection form) 
        {
            var model = new MailModel
            {
                ReplyMail = form["txtReplyMail"],
                ReplyName = form["txtReplyName"],
                Subject = form["txtSubject"],
                FuncionalityId = Convert.ToInt32(form["txtFuncId"]),
                Content = Microsoft.JScript.GlobalObject.unescape(form["txtContent"])
            };

            WcfResponse<bool> response = null;

            using (var service = ChannelProviderFactory<ICommonContractChannel>.Create("CommonClassName").GetChannel().CreateChannel()) 
            {
                response = service.UpdateMailModel(model);
                if (response.Result)
                    TempData["Success"] = "Modelo de e-mail alterado com sucesso.";
                //TODO: Log
            }

            return Json(response);
        }
    }
}
