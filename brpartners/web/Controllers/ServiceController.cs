﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QX3.Portal.WebSite.Models;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.WebSite.SuitabilityService;
using QX3.Portal.WebSite.Models.Channel;
using QX3.Spinnex.Common.Services.Logging;
using QX3.Portal.WebSite.Helper;
using QX3.Portal.WebSite.AuthenticationService;
using System.Diagnostics;
using log4net;
using QX3.Portal.WebSite.CommonService;
using System.Net.Mail;
using Tegra.HumanResources.Domain.Representations;
using QX3.Portal.WebSite.AccessControlService;
using QX3.Portal.WebSite.Util;

namespace QX3.Portal.WebSite.Controllers
{
    public class ServiceController : Controller
    {
        [HttpGet]
        public JsonResult DataMasking()
        {
            try
            {
                IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                var result = service.ListUsers().Result;

                service.Close();

                int i = 1;

                foreach (var user in result)
                {
                    user.Name = "Usuário " + i.ToString();

                    if (user.CpfCnpj.HasValue)
                        user.CpfCnpj = Convert.ToInt64(RandomCpf.Generate());

                    user.Email = "usuario" + i.ToString() + "@email.com";

                    i = i + 1;
                }

                channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
                service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                var result2 = service.UpdateUsers(result);

                service.Close();

                return Json("Ok", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = string.Format("{0} - {1} - {2}",
                   ex.Message,
                   ex.InnerException != null ? ex.InnerException.ToString() : "",
                   ex.StackTrace);

                Response.StatusCode = 400;
                return Json(error, JsonRequestBehavior.AllowGet);
            }
        }

    }
}
