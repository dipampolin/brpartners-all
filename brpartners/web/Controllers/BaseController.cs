﻿using System;
using System.Configuration;
using System.Web;
using System.Web.Mvc;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.WebSite.AccessControlService;
using QX3.Portal.WebSite.Helper;
using QX3.Portal.WebSite.Models.Channel;
using QX3.Spinnex.Common.Services.Logging;

namespace QX3.Portal.WebSite.Controllers
{
    public class BaseController : Controller
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (!SessionHelper.CheckUserSession() && filterContext.ActionDescriptor.ActionName != "CheckSession")
            {
                HttpContext.Response.Redirect("~/Account/Login");
            }
            else
            {
                IChannelProvider<IAccessControlContractChannel> channelProvider2 = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
                using (var service2 = channelProvider2.GetChannel().CreateChannel())
                {
                    var verifySession = service2.Verify_SESSION(SessionHelper.CurrentUser.UserName, Convert.ToString(Session["IPCLIENT"])).Result;

                    if (verifySession)
                    {
                        Session["CONTROLSESSION"] = null;

                        var elements = SessionHelper.CurrentUser.Profile.Permissions;

                        foreach (ACElement ac in elements)
                        {
                            if (ac.Alias.ToLower() == "suitability")
                            {
                                if (filterContext.ActionDescriptor.ActionName != "Questions"
                                    && (SessionHelper.CurrentUser.InvestorType == 0 || SessionHelper.CurrentUser.SuitabilityStatus == 1 || SessionHelper.CurrentUser.SuitabilityStatus == 3))
                                {
                                    if (SessionHelper.CurrentUser.InvestorKind == 0 || SessionHelper.CurrentUser.PersonType == "F")
                                    {
                                        Response.Redirect("~/Suitability/Questions");
                                        break;
                                    }
                                }
                            }

                        }


                        //if (SessionHelper.CurrentUser.Profile.Name.ToLower().Contains("client") && (SessionHelper.CurrentUser.SuitabilityStatus == 1 || SessionHelper.CurrentUser.SuitabilityStatus == 3) && filterContext.ActionDescriptor.ControllerDescriptor.ControllerName != "Suitability")
                        //{
                        //    HttpContext.Response.Redirect("~/Suitability/Index");
                        //}

                        base.OnActionExecuting(filterContext);
                    }
                    else
                    {
                        //var resVerLog = service2.Verify_SESSION_Login(SessionHelper.CurrentUser.UserName.ToLower()).Result;

                        //if (resVerLog)
                        //{
                        Session["CONTROLSESSION"] = "S";
                        HttpContext.Response.Redirect("~/Account/Login");
                        //}
                    }
                }


            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public string CheckSession()
        {
            try
            {
                return SessionHelper.CheckUserSession().ToString().ToLower();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return "false";
            }
        }

    }
}
