﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Configuration;
using Newtonsoft.Json;
using iTextSharp.text;
using QX3.Portal.WebSite.NonResidentTradersService;
using QX3.Spinnex.Common.Services.Logging;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.WebSite.Models;
using QX3.Portal.WebSite.Models.Channel;
using QX3.Portal.WebSite.Properties;
using QX3.Portal.WebSite.Helper;
using System.Web.Helpers;
using System.Collections;
using System.IO;
using System.Text;
using System.Web;

namespace QX3.Portal.WebSite.Controllers
{
    public class NonResidentTradersController : FileController
    {
        #region Registration

        private MemoryStream ConvertDisclaimerToStream(string content)
        {
            byte[] byteArray = Encoding.ASCII.GetBytes(content);
            MemoryStream stream = new MemoryStream(byteArray);
            return stream;
        }

        private string ConvertDisclaimerToString(MemoryStream stream)
        {
            StreamReader reader = new StreamReader(stream);
            string disclaimer = reader.ReadToEnd();
            return disclaimer;
        }

        private List<DisclaimerViewModel> LoadDisclaimerModel(bool reset)
        {
            List<DisclaimerViewModel> list = new List<DisclaimerViewModel>();

            if (Session["NonResidentTraders.Disclaimers"] == null || reset)
            {
                IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();
                var response = service.LoadReportTypes();

                foreach (ReportType rt in response.Result)
                {
                    string disclaimer = rt.HasDisclaimer ? service.LoadDisclaimer(rt.ReportId).Result.Content : string.Empty;
                    list.Add(new DisclaimerViewModel { ReportId = rt.ReportId, HasDisclaimer = rt.HasDisclaimer, Description = rt.Description, DisclaimerContent = disclaimer });
                }
                service.Close();
                Session["NonResidentTraders.Disclaimers"] = list;
            }
            return (List<DisclaimerViewModel>)Session["NonResidentTraders.Disclaimers"];
        }

        [AccessControlAuthorize]
        public ActionResult Search()
        {
            LogHelper.SaveAuditingLog("NonResidentTraders.Search");
            return View();
        }

        public ActionResult Registration(int id)
        {
            AccessControlHelper acHelper = new AccessControlHelper();
            if (!acHelper.HasFuncionalityPermission("NonResidentTraders.Search", "btnEditUser")){
                TempData["ErrorMessage"] = "Você não possui permissão.";
                return RedirectToAction("Index", "Home");
            }

            NonResidentCustomer customer = new NonResidentCustomer();
            customer.Code = id;

            try
            {
                LogHelper.SaveAuditingLog("NonResidentTraders.Registration", string.Format(LogResources.AuditingRegistrationParameters, id));

                IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                customer.Name = CommonHelper.GetClientName(id);

				var response = service.LoadCustomers(id, CommonHelper.GetLoggedUserGroupId());
                if (response.Result != null && response.Result.Length > 0)
                    customer = response.Result[0];

                service.Close();

                return View(customer);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = "Não foi possível carregar dados do cliente";
                return View(customer);
            }

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Registration(NonResidentCustomer customer, string hdfCode, string hdfName)
        {
            try
            {
                LogHelper.SaveAuditingLog("NonResidentTraders.Registration.Post", string.Format(LogResources.AuditingRegistrationParameters, JsonConvert.SerializeObject(customer)));

                IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                int customerCode = 0;
                Int32.TryParse(hdfCode, out customerCode);
                customer.Code = customerCode;
                customer.Name = hdfName;

                service.Open();

                var response = service.InsertCustomer(customer);

                service.Close();

                if (!response.Result)
                {
                    TempData["ErrorMessage"] = response.Message;
                    return View(customer);
                }
                TempData["Success"] = customer.Code;

            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }

            return RedirectToAction("Search");
        }

        [AccessControlAuthorize]
        public ActionResult Failure()
        {
            LogHelper.SaveAuditingLog("NonResidentTraders.Failure");
            return View();
        }

        [AccessControlAuthorize]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Failure(FormCollection form)
        {
            try
            {
                LogHelper.SaveAuditingLog("NonResidentTraders.Failure.Post", string.Format(LogResources.AuditingFailureParameters, form["txtTradingDate"], form["txtClientCode"]));

                int clientCode = -1;

                if (!string.IsNullOrEmpty(form["txtClientCode"]))
                    clientCode = Int32.Parse(form["txtClientCode"]);

                IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();
				var response = service.LoadFailures(form["txtTradingDate"], clientCode, CommonHelper.GetLoggedUserGroupId());
                service.Close();

                Session["Failures"] = response.Result.ToList();
                return Json(new ActionResultModel { Status = "True", Messages = null });
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                String[] strArr = { "vlnTradingDate", Resources.FailureErrorMessage };
                return Json(new ActionResultModel { Status = "False", Messages = strArr });
            }
        }

        [AccessControlAuthorize]
        public ActionResult PTAXRegistration()
        {
            LogHelper.SaveAuditingLog("NonResidentTraders.PTAXRegistration");
            return View();
        }

        [AccessControlAuthorize]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PTAXRegistration(string date, string value)
        {
            try
            {
                LogHelper.SaveAuditingLog("NonResidentTraders.PTAXRegistration.Post", string.Format(LogResources.AuditingPtaxParameters, date, value));

                if (!String.IsNullOrEmpty(date) && !String.IsNullOrEmpty(value))
                {
                    DateTime _date;
                    if (DateTime.TryParse(date, out _date))
                    {
                        IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                        var service = channelProvider.GetChannel().CreateChannel();

                        service.Open();

                        var resposnse = service.InsertPTax(new PTax { Date = Convert.ToDateTime(date), Value = Convert.ToDecimal(value) });

                        service.Close();

                        return Json(resposnse.Result);
                    }
                    else
                    {
                        return Json(false);
                    }
                }
                else
                {
                    return Json(false);
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return Json(false);
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SearchSettlementDate(string date)
        {
            ActionResultModel actionResultModel = new ActionResultModel();
            String[] strArr = new String[2];

            try
            {
                DateTime _date;

                if (!String.IsNullOrEmpty(date))
                {
                    if (DateTime.TryParse(date, out _date))
                    {
                        IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                        var service = channelProvider.GetChannel().CreateChannel();

                        service.Open();
                        var response = service.LoadLastSettlement();
                        service.Close();

                        int comparation = DateTime.Compare(_date, response.Result.LastSettlementDate);
                        switch (comparation)
                        {
                            case -1:
                            case 0:
                                strArr[0] = "vlnMessage";
                                strArr[1] = Resources.SettlementDone;
                                actionResultModel.Status = "False";
                                actionResultModel.Messages = strArr;
                                break;
                            case 1:
                                IChannelProvider<INonResidentTradersServiceContractChannel> _channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                                var _service = _channelProvider.GetChannel().CreateChannel();

                                _service.Open();
                                var ptax = _service.LoadPTaxes(date);
                                _service.Close();

                                strArr[0] = "txtPtax";
                                strArr[1] = ((ptax.Result != null && ptax.Result.Length > 0) ? ptax.Result[0].Value.ToString("N4") : "0,0000");
                                actionResultModel.Status = "True";
                                actionResultModel.Messages = strArr;
                                break;
                        }
                    }
                    else
                    {
                        strArr[0] = "vlnTxtDate";
                        strArr[1] = "Data inválida.";
                        actionResultModel.Status = "False";
                        actionResultModel.Messages = strArr;
                    }
                }
                else
                {
                    strArr[0] = "vlnTxtDate";
                    strArr[1] = "O campo data deve ser preeenchido.";
                    actionResultModel.Status = "False";
                    actionResultModel.Messages = strArr;
                }

                return Json(actionResultModel);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return Json(actionResultModel);
            }
        }

        [AccessControlAuthorize]
        public ActionResult Disclaimers()
        {
            List<DisclaimerViewModel> disclaimers = new List<DisclaimerViewModel>();
            try
            {
                LogHelper.SaveAuditingLog("NonResidentTraders.Disclaimers");

                disclaimers = LoadDisclaimerModel(false);
                Session["NonResidentTraders.Disclaimers"] = disclaimers;
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.DisclaimerLoadErrorMessage;
            }

            return View(disclaimers);
        }

        public ActionResult DisclaimerRegistration(int id)
        {
            AccessControlHelper acHelper = new AccessControlHelper();
            if (!acHelper.HasFuncionalityPermission("NonResidentTraders.Disclaimers", "btnEditDisclaimer"))
            {
                TempData["ErrorMessage"] = "Você não possui permissão.";
                return RedirectToAction("Index", "Home");
            }

            DisclaimerViewModel disclaimer = new DisclaimerViewModel();
            try
            {
                LogHelper.SaveAuditingLog("NonResidentTraders.DisclaimerRegistration", string.Format(LogResources.AuditingDisclaimerParameters, id));

                var disclaimers = LoadDisclaimerModel(false);
                if (disclaimers != null && disclaimers.Count > 0)
                    disclaimer = disclaimers.Where(d => d.ReportId.Equals(id)).FirstOrDefault();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.DisclaimerLoadErrorMessage;
            }

            return View(disclaimer);
        }

        [ValidateInput(false)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DisclaimerRegistration(FormCollection form)
        {
            DisclaimerViewModel disclaimer = new DisclaimerViewModel();
            int reportID = 0;
            try
            {
                FormCollection collection = new FormCollection(Request.Unvalidated().Form);

                reportID = Int32.Parse(form["hdfID"]);
                RichTextHelper helper = new RichTextHelper();

                string decodedDisclaimerContent = Server.HtmlDecode(form["txtDisclaimerContent"]);
                string disclaimerContent = Microsoft.JScript.GlobalObject.unescape(form["txtDisclaimerContent"]);

                LogHelper.SaveAuditingLog("NonResidentTraders.DisclaimerRegistration.Post", string.Format(LogResources.AuditingDisclaimerPostParameters, reportID, decodedDisclaimerContent));

                IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                ReportType reportType = new ReportType();
                reportType.ReportId = reportID;

                service.Open();
                var response = service.InsertDisclaimer(new Disclaimer { Content = disclaimerContent, ReportType = reportType });
                service.Close();

                if (!response.Result)
                    throw new Exception(response.Message ?? Resources.DisclaimerInsertErrorMessage);
                else
                {
                    TempData["Success"] = string.Format("Disclaimer {0} com sucesso.", reportID == 0 ? "cadastrado" : "editado");
                    LoadDisclaimerModel(true);
                    return this.RedirectToAction("Disclaimers");
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.DisclaimerInsertErrorMessage;
                var disclaimers = LoadDisclaimerModel(false);
                if (disclaimers != null && disclaimers.Count > 0)
                    disclaimer = disclaimers.Where(d => d.ReportId.Equals(reportID)).FirstOrDefault();
                return this.View(disclaimer);
            }
        }

        [HttpPost]
        public ActionResult LoadBrokers(string filter)
        {
            List<Broker> brokers = new List<Broker>();
            IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
            using (var service = channelProvider.GetChannel().CreateChannel())
            {
                var res = service.LoadBrokers(filter).Result;
                if (res != null) brokers = res.ToList();
            }

            return Json(brokers);
        }

        [HttpPost]
        public ActionResult LoadBrokersTransfer(string filter)
        {
            List<Broker> brokers = new List<Broker>();
            IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
            using (var service = channelProvider.GetChannel().CreateChannel())
            {
                var res = service.LoadBrokersTransfer(filter).Result;
                if (res != null) brokers = res.ToList();
            }

            return Json(brokers);
        }

        [HttpPost]
        public ActionResult LoadPortfolios(string filter)
        {
            List<CommonType> brokers = new List<CommonType>();

            IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
            using (var service = channelProvider.GetChannel().CreateChannel())
            {
                 brokers = service.LoadPortfolios(filter).Result.ToList();
            }
            return Json(brokers);
        }

        #endregion

        #region Ajax Methods

        [AcceptVerbs(HttpVerbs.Post)]
        public void LoadTraders(FormCollection form)
        {
            string code = form["Code"];

            LogManager.WriteLog(code);

            try
            {
                if (code != "0")
                {
                    IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                    var service = channelProvider.GetChannel().CreateChannel();

                    int enteredCode = 0;
                    Int32.TryParse(code, out enteredCode);

                    service.Open();

					Session["Traders"] = service.LoadCustomers(enteredCode, CommonHelper.GetLoggedUserGroupId());

                    service.Close();
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            };
        }

        [HttpPost]
        public ActionResult GetTraderName(int code)
        {
            try
            {

                IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                var response = CommonHelper.GetClientName(code);

                service.Close();

                if (String.IsNullOrEmpty(response))
                    response = Resources.AbsentTraderName;

                return Json(response);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return Json(string.Empty);
            }
        }

        [HttpPost]
        public ActionResult GetDisclaimer(int id)
        {
            try
            {

                IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();
                var response = service.LoadDisclaimer(id);
                service.Close();

                return Json(response.Result.Content ?? string.Empty);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return Json(string.Empty);
            }
        }

        [HttpPost]
        public ActionResult UpdateFailure(string keys, char type, string newValue)
        {
            try
            {
                LogHelper.SaveAuditingLog("NonResidentTraders.UpdateFailure.Post", string.Format(LogResources.AuditingFailurePostParameters, keys));

                IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                List<Failure> failures = (List<Failure>)Session["Failures"];

                if (failures != null && failures.Count > 0)
                {
                    string[] values = keys.Split(';');
                    if (values.Length == 5)
                    {
                        int clientCode = Int32.Parse(values[0]);
                        string tradeDate = values[1];
                        string stockCode = values[2];
                        string opCode = values[3];
                        int tradeNumber = Int32.Parse(values[4]);

                        Failure failure = (from f in failures
                                           where f.ClientCode.Equals(clientCode)
                                           && f.TradeDate.ToShortDateString().Equals(tradeDate)
                                           && f.TradeCode.Equals(stockCode)
                                           && f.OperationType.Equals(opCode.ToCharArray()[0])
                                           && f.TradeNumber.Equals(tradeNumber)
                                           select f).FirstOrDefault();

                        if (failure != null)
                        {
                            if (type == 'q')
                                failure.FailureQuantity = Int32.Parse(newValue);
                            else
                                failure.FailureValue = decimal.Parse(newValue);

                            failure.Username = SessionHelper.CurrentUser.UserName;

                            service.Open();
                            var response = service.InsertFailure(failure);
                            service.Close();

                            return Json(new { result = response.Result.ToString().ToLower(), updateInfo = JsonConvert.SerializeObject(failure) });
                        }
                    }
                }

                return Json(new { result = "false", updateInfo = "Item não encontrado na lista de falhas." });
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return Json(new { result = "false", updateInfo = ex.Message + " - " + ex.StackTrace });
            }
        }

        #endregion

        #region Reports

        #region Confirmation

        [AccessControlAuthorize]
        public ActionResult Confirmation()
        {
            LogHelper.SaveAuditingLog("NonResidentTraders.Confirmation");
            return View();
        }

        [AccessControlAuthorize]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Confirmation(FormCollection form)
        {
            try
            {
                LogHelper.SaveAuditingLog("NonResidentTraders.Confirmation.Post", string.Format(LogResources.AuditingReportParameters, form["txtClientCode"], form["txtTradingDate"]));

                string code = form["txtClientCode"];
                DateTime tradingDate;

                if (String.IsNullOrEmpty(code))
                {
                    code = "-1";
                }

                if (DateTime.TryParse(form["txtTradingDate"], out tradingDate))
                {
                    IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                    var service = channelProvider.GetChannel().CreateChannel();
                    service.Open();

					var response = service.LoadClients(Convert.ToInt32(code), "XX", tradingDate, CommonHelper.GetLoggedUserGroupId());

                    service.Close();

                    Session["ConfirmationClients"] = response;
                    return Json(new ActionResultModel { Status = "True", Messages = null });
                }
                else
                {
                    String[] strArr = { "vlnTradingDate", "Data inválida" };
                    return Json(new ActionResultModel { Status = "False", Messages = strArr });
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return Json(String.Format("Fail request: {0}", ex.Message));
            }
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ConfirmationReport(int code, string date)
        {
            try
            {
                LogHelper.SaveAuditingLog("NonResidentTraders.ConfirmationReport", string.Format(LogResources.AuditingReportParameters, code, date));

                DateTime tradingDate = DateTime.Parse(date);

                IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

				var response = service.LoadConfirmationReports(Convert.ToInt16(code), tradingDate);
                Confirmation report = response.Result;

                service.Close();

                ViewData["code"] = code;
                ViewData["date"] = date;

                return View(report);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return View("ConfirmarionReport");
            }
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ConfirmationReportToPDF(int code, string date)
        {
            try
            {
                LogHelper.SaveAuditingLog("NonResidentTraders.ConfirmationReportToPDF", string.Format(LogResources.AuditingReportParameters, code, date));

                DateTime tradingDate = DateTime.Parse(date);

                IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                var service = channelProvider.GetChannel().CreateChannel();
                service.Open();

				var response = service.LoadConfirmationReports(Convert.ToInt32(code), tradingDate);
                Confirmation report = response.Result;

                service.Close();

                string fileName = string.Format("confirmation_{0}_{1}.pdf", report.ClientCode, tradingDate.ToString("MM-dd-yyyy"));

                return ViewConfirmationPdf(response.Result, true, fileName);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return this.RedirectToAction("Confirmation");
            }

        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ConfirmationPDFReportsToZip(string codes, string date)
        {
            try
            {
                LogHelper.SaveAuditingLog("NonResidentTraders.ConfirmationPDFReportsToZip", string.Format(LogResources.AuditingReportParameters, codes, date));

                List<string> listCodes = new List<string>();
                listCodes = codes.Split(';').ToList();

                DateTime tradingDate = DateTime.Parse(date);

                IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                var service = channelProvider.GetChannel().CreateChannel();
                service.Open();

                PDFHelper helper = new PDFHelper();
                List<ZipItem> zipList = new List<ZipItem>();
                foreach (string c in listCodes)
                {
					Confirmation report = service.LoadConfirmationReports(Int32.Parse(c), tradingDate).Result;
                    if (report != null && report.ClientCode > 0)
                    {
                        string fileName = string.Format("confirmation_{0}_{1}.pdf", report.ClientCode, tradingDate.ToString("MM-dd-yyyy"));
                        zipList.Add(new ZipItem { FileStream = helper.CreateConfirmationToMemoryStream(report), FileName = fileName });
                    }
                }

                string filesPath = string.Format(ConfigurationManager.AppSettings["Report.Confirmation.PDFPath"], 144);
                string zipName = string.Format("confirmation_{0}_{1}.zip", tradingDate.ToString("MM-dd-yyyy"), DateTime.Now.ToString("MM-dd-yyyy-HH-mm"));

                service.Close();

                return ViewZip(zipList, true, filesPath, zipName);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return this.RedirectToAction("Confirmation");
            }

        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ConfirmationReportToExcel(int code, string date)
        {
            try
            {
                LogHelper.SaveAuditingLog("NonResidentTraders.ConfirmationReportToExcel", string.Format(LogResources.AuditingReportParameters, code, date));

                DateTime tradingDate = DateTime.Parse(date);

                IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                var service = channelProvider.GetChannel().CreateChannel();
                service.Open();

				var response = service.LoadConfirmationReports(Convert.ToInt32(code), tradingDate);
                Confirmation report = response.Result;

                service.Close();

                string fileName = string.Format("confirmation_{0}_{1}.xls", report.ClientCode, tradingDate.ToString("MM-dd-yyyy"));

                ViewData["code"] = code;
                ViewData["date"] = date;

                return ViewConfirmationExcel(response.Result, true, fileName);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return this.RedirectToAction("Confirmation");
            }

        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ConfirmationExcelReportsToZip(string codes, string date)
        {
            try
            {
                LogHelper.SaveAuditingLog("NonResidentTraders.ConfirmationExcelReportsToZip", string.Format(LogResources.AuditingReportParameters, codes, date));

                List<string> listCodes = new List<string>();
                listCodes = codes.Split(';').ToList();

                DateTime tradingDate = DateTime.Parse(date);

                IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                var service = channelProvider.GetChannel().CreateChannel();
                service.Open();

                ExcelHelper helper = new ExcelHelper();
                List<ZipItem> zipList = new List<ZipItem>();
                foreach (string c in listCodes)
                {
					Confirmation report = service.LoadConfirmationReports(Int32.Parse(c), tradingDate).Result;
                    if (report != null && report.ClientCode > 0)
                    {
                        string fileName = string.Format("confirmation_{0}_{1}.xls", report.ClientCode, tradingDate.ToString("MM-dd-yyyy"));
                        zipList.Add(new ZipItem { FileStream = helper.CreateConfirmationToMemoryStream(report), FileName = fileName });
                    }
                }

                string filesPath = string.Format(ConfigurationManager.AppSettings["Report.Confirmation.PDFPath"], 144);
                string zipName = string.Format("confirmation_{0}_{1}.zip", tradingDate.ToString("MM-dd-yyyy"), DateTime.Now.ToString("MM-dd-yyyy-HH-mm"));

                service.Close();

                return ViewZip(zipList, true, filesPath, zipName);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return this.RedirectToAction("Confirmation");
            }

        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PrintConfirmationReport(string codes, string date)
        {
            List<Confirmation> reports = new List<Confirmation>();

            try
            {
                LogHelper.SaveAuditingLog("NonResidentTraders.PrintConfirmationReport", string.Format(LogResources.AuditingReportParameters, codes, date));

                DateTime tradingDate = DateTime.Parse(date);

                IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                List<string> listCodes = new List<string>();
                listCodes = codes.Split(';').ToList();

                foreach (string code in listCodes)
                {
					var response = service.LoadConfirmationReports(Convert.ToInt16(code), tradingDate);
                    Confirmation report = response.Result;
                    if (report != null && report.ClientCode > 0)
                        reports.Add(report);
                }

                service.Close();

                return View(reports);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return View("Confirmation");
            }
        }

        #endregion

        #region Trade Blotter

        [AccessControlAuthorize]
        public ActionResult TradeBlotter()
        {
            try
            {
                LogHelper.SaveAuditingLog("NonResidentTraders.TradeBlotter");

                IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                var response = service.LoadHeader();

                service.Close();

                return View(response.Result);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return View();
            }
        }

        [AccessControlAuthorize]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult TradeBlotter(FormCollection form)
        {
            try
            {
                LogHelper.SaveAuditingLog("NonResidentTraders.TradeBlotter.Post", string.Format(LogResources.AuditingReportParameters, "", form["txtTradingDate"]));

                DateTime tradingDate;

                if (DateTime.TryParse(form["txtTradingDate"], out tradingDate))
                {
                    IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                    var service = channelProvider.GetChannel().CreateChannel();

                    service.Open();
					var response = service.LoadTradeBlotterReports(tradingDate, CommonHelper.GetLoggedUserGroupId());
                    service.Close();

                    Session["TradeBlotterReports"] = response.Result.Items;
                    return Json(new ActionResultModel { Status = "True", Messages = null }, "application/json" , JsonRequestBehavior.AllowGet);
                }
                else
                {
                    String[] strArr = { "vlnTradingDate", "Data inválida" };
                    return Json(new ActionResultModel { Status = "False", Messages = strArr });
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                String[] strArr = { "vlnTradingDate", Resources.TradeBlotterReportErrorMessage };
                return Json(new ActionResultModel { Status = "False", Messages = strArr });
            }
        }

        public ActionResult TradeBlotterToExcel(string date)
        {
            try
            {
                LogHelper.SaveAuditingLog("NonResidentTraders.TradeBlotterToExcel", string.Format(LogResources.AuditingReportParameters, "", date));

                DateTime tradingDate = DateTime.Parse(date);

                IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                var service = channelProvider.GetChannel().CreateChannel();
                service.Open();

				var response = service.LoadTradeBlotterReports(tradingDate, CommonHelper.GetLoggedUserGroupId());
                TradeBlotter report = response.Result;

                service.Close();

                string fileName = string.Format("tradeblotter_{0}.xls", tradingDate.ToString("dd-MM-yyyy"));

                ViewData["date"] = date;

                return ViewTradeBlotterExcel(response.Result, true, fileName);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return this.RedirectToAction("TradeBlotter");
            }

        }

        public ActionResult TradeBlotterToPDF(string date)
        {
            try
            {
                LogHelper.SaveAuditingLog("NonResidentTraders.TradeBlotterToPDF", string.Format(LogResources.AuditingReportParameters, "", date));

                DateTime tradingDate = DateTime.Parse(date);

                IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                var service = channelProvider.GetChannel().CreateChannel();
                service.Open();

				var response = service.LoadTradeBlotterReports(tradingDate, CommonHelper.GetLoggedUserGroupId());
                TradeBlotter report = response.Result;

                service.Close();

                string fileName = string.Format("tradeblotter_{0}.pdf", tradingDate.ToString("dd-MM-yyyy"));

                return ViewTradeBlotterPdf(response.Result, true, fileName);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return this.RedirectToAction("TradeBlotter");
            }

        }

        public ActionResult PrintTradeBlotterReport(string date)
        {
            try
            {
                LogHelper.SaveAuditingLog("NonResidentTraders.PrintTradeBlotterReport", string.Format(LogResources.AuditingReportParameters, "", date));

                DateTime tradingDate = DateTime.Parse(date);

                IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

				var response = service.LoadTradeBlotterReports(tradingDate, CommonHelper.GetLoggedUserGroupId());
                TradeBlotter report = response.Result;
                ViewData["date"] = tradingDate.ToString("MM/dd/yyyy");

                service.Close();

                return View(report);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return View("TradeBlotter");
            }
        }

        #endregion

        #region Customer Statement

        [AccessControlAuthorize]
        public ActionResult CustomerStatement()
        {
            try
            {
                LogHelper.SaveAuditingLog("NonResidentTraders.CustomerStatement");
                LoadMonthsAndYears();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
            }

            return View();
        }

        [AccessControlAuthorize]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CustomerStatement(string code, string date)
        {
            try
            {
                LogHelper.SaveAuditingLog("NonResidentTraders.CustomerStatement.Post", string.Format(LogResources.AuditingReportParameters, code, date));

                if (String.IsNullOrEmpty(code))
                {
                    code = "-1";
                }

                IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                var service = channelProvider.GetChannel().CreateChannel();
                service.Open();

				var response = service.LoadClients(Convert.ToInt32(code), "CS", Convert.ToDateTime("01/" + date), CommonHelper.GetLoggedUserGroupId());

                service.Close();

                Session["CustomerStatementClients"] = response;
                return Json(new ActionResultModel { Status = "True", Messages = null });
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return Json(new ActionResultModel { Status = "False", Messages = null });
            }
        }

        public ActionResult CustomerStatementReportToPDF(int code, string date)
        {
            try
            {
                LogHelper.SaveAuditingLog("NonResidentTraders.CustomerStatementReportToPDF", string.Format(LogResources.AuditingReportParameters, code, date));

                DateTime tradingDate = DateTime.Parse(date);

                IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                var service = channelProvider.GetChannel().CreateChannel();
                service.Open();

				var response = service.LoadCustomerStatementReports(Convert.ToInt32(code), tradingDate.ToString("MM/yyyy"), CommonHelper.GetLoggedUserGroupId());
                CustomerStatement report = response.Result;

                service.Close();

                string fileName = string.Format("customerstatement_{0}_{1}.pdf", report.ClientCode, tradingDate.ToString("MM-yyyy"));

                return ViewCustomerStatementPdf(report, true, fileName);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return this.RedirectToAction("CustomerStatement");
            }

        }

        public ActionResult CustomerStatementPDFReportsToZip(string codes, string date)
        {
            try
            {
                LogHelper.SaveAuditingLog("NonResidentTraders.CustomerStatementPDFReportsToZip", string.Format(LogResources.AuditingReportParameters, codes, date));

                List<string> listCodes = new List<string>();
                listCodes = codes.Split(';').ToList();

                DateTime tradingDate = DateTime.Parse("01/" + date);

                IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                var service = channelProvider.GetChannel().CreateChannel();
                service.Open();

                PDFHelper helper = new PDFHelper();
                List<ZipItem> zipList = new List<ZipItem>();
                foreach (string c in listCodes)
                {
					CustomerStatement report = service.LoadCustomerStatementReports(Int32.Parse(c), tradingDate.ToString("MM/yyyy"), CommonHelper.GetLoggedUserGroupId()).Result;
                    if (report != null && report.ClientCode > 0)
                    {
                        string fileName = string.Format("customerstatement_{0}_{1}.pdf", report.ClientCode, tradingDate.ToString("MM-yyyy"));
                        zipList.Add(new ZipItem { FileStream = helper.CreateCustomerStatementToMemoryStream(report), FileName = fileName });
                    }
                }

                string filesPath = string.Format(ConfigurationManager.AppSettings["Report.CustomerStatement.PDFPath"], 144);
                string zipName = string.Format("customerstatement_{0}_{1}.zip", tradingDate.ToString("MM-yyyy"), DateTime.Now.ToString("MM-dd-yyyy-HH-mm"));

                service.Close();

                return ViewZip(zipList, true, filesPath, zipName);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return this.RedirectToAction("CustomerStatement");
            }

        }

        public ActionResult CustomerStatementReportToExcel(int code, string date)
        {
            try
            {
                LogHelper.SaveAuditingLog("NonResidentTraders.CustomerStatementReportToExcel", string.Format(LogResources.AuditingReportParameters, code, date));

                DateTime tradingDate = DateTime.Parse(date);

                IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

				var response = service.LoadCustomerStatementReports(code, tradingDate.ToString("MM-yyyy"), CommonHelper.GetLoggedUserGroupId());
                CustomerStatement report = response.Result;

                service.Close();

                string fileName = string.Format("customerstatement_{0}_{1}.xls", report.ClientCode, tradingDate.ToString("MM-yyyy"));

                ViewData["code"] = code;
                ViewData["date"] = date;

                return ViewCustomerStatementExcel(report, true, fileName);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return this.RedirectToAction("CustomerStatement");
            }

        }

        public ActionResult CustomerStatementExcelReportsToZip(string codes, string date)
        {
            try
            {
                LogHelper.SaveAuditingLog("NonResidentTraders.CustomerStatementExcelReportsToZip", string.Format(LogResources.AuditingReportParameters, codes, date));

                List<string> listCodes = new List<string>();
                listCodes = codes.Split(';').ToList();

                DateTime tradingDate = DateTime.Parse("01/" + date);

                IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                ExcelHelper helper = new ExcelHelper();
                List<ZipItem> zipList = new List<ZipItem>();
                foreach (string c in listCodes)
                {
					CustomerStatement report = service.LoadCustomerStatementReports(Int32.Parse(c), tradingDate.ToString("MM-yyyy"), CommonHelper.GetLoggedUserGroupId()).Result;
                    if (report != null && report.ClientCode > 0)
                    {
                        string fileName = string.Format("customerstatement_{0}_{1}.xls", report.ClientCode, tradingDate.ToString("MM-yyyy"));
                        zipList.Add(new ZipItem { FileStream = helper.CreateCustomerStatementToMemoryStream(report), FileName = fileName });
                    }
                }

                string filesPath = string.Format(ConfigurationManager.AppSettings["Report.CustomerStatement.PDFPath"], 144);
                string zipName = string.Format("customerstatement_{0}_{1}.zip", tradingDate.ToString("MM-yyyy"), DateTime.Now.ToString("MM-dd-yyyy-HH-mm"));

                service.Close();

                return ViewZip(zipList, true, filesPath, zipName);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return this.RedirectToAction("CustomerStatement");
            }

        }

        public ActionResult CustomerStatementReport(string codes, string date)
        {
            List<CustomerStatement> reports = new List<CustomerStatement>();

            try
            {
                LogHelper.SaveAuditingLog("NonResidentTraders.CustomerStatementReport", string.Format(LogResources.AuditingReportParameters, codes, date));

                IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                List<string> listCodes = new List<string>();
                listCodes = codes.Split(';').ToList();

                foreach (string code in listCodes)
                {
					var response = service.LoadCustomerStatementReports(Convert.ToInt32(code), date, CommonHelper.GetLoggedUserGroupId());
                    CustomerStatement report = response.Result;
                    if (report != null && report.ClientCode > 0)
                        reports.Add(report);
                }

                service.Close();

                return View(reports);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return View("CustomerStatement");
            }
        }

        public ActionResult PrintCustomerStatementReport(string codes, string date)
        {
            List<CustomerStatement> reports = new List<CustomerStatement>();

            try
            {
                LogHelper.SaveAuditingLog("NonResidentTraders.PrintCustomerStatementReport", string.Format(LogResources.AuditingReportParameters, codes, date));

                IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                List<string> listCodes = new List<string>();
                listCodes = codes.Split(';').ToList();

                foreach (string code in listCodes)
                {
					var response = service.LoadCustomerStatementReports(Convert.ToInt32(code), date, CommonHelper.GetLoggedUserGroupId());
                    CustomerStatement report = response.Result;
                    if (report != null && report.ClientCode > 0)
                        reports.Add(report);
                }

                service.Close();

                return View(reports);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return View("CustomerStatement");
            }
        }

        #endregion

        #region Customer Ledger

        [AccessControlAuthorize]
        public ActionResult CustomerLedger()
        {
            try
            {
                LogHelper.SaveAuditingLog("NonResidentTraders.CustomerLedger");
                LoadMonthsAndYears();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
            }

            return View();
        }

        [AccessControlAuthorize]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CustomerLedger(string code, string date)
        {
            try
            {
                LogHelper.SaveAuditingLog("NonResidentTraders.CustomerLedger.Post", string.Format(LogResources.AuditingReportParameters, code, date));

                if (String.IsNullOrEmpty(code))
                {
                    code = "-1";
                }

                IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

				var response = service.LoadClients(Convert.ToInt32(code), "CL", Convert.ToDateTime("01/" + date), CommonHelper.GetLoggedUserGroupId());

                service.Close();

                Session["CustomerLedgerClients"] = response;
                return Json(new ActionResultModel { Status = "True", Messages = null });
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return Json(new ActionResultModel { Status = "False", Messages = null });
            }
        }

        public ActionResult CustomerLedgerReportToPDF(int code, string date)
        {
            try
            {
                LogHelper.SaveAuditingLog("NonResidentTraders.CustomerLedgerReportToPDF", string.Format(LogResources.AuditingReportParameters, code, date));

                DateTime tradingDate = DateTime.Parse(date);

                IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                var service = channelProvider.GetChannel().CreateChannel();
                service.Open();

                var response = service.LoadCustomerLedgerReports(Convert.ToInt32(code), tradingDate.ToString("MM/yyyy"));
                CustomerLedger report = response.Result;

                service.Close();

                string fileName = string.Format("customerledger_{0}_{1}.pdf", report.ClientCode, tradingDate.ToString("MM-yyyy"));

                return ViewCustomerLedgerPdf(report, true, fileName);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return this.RedirectToAction("CustomerLedger");
            }

        }

        public ActionResult CustomerLedgerPDFReportsToZip(string codes, string date)
        {
            try
            {
                LogHelper.SaveAuditingLog("NonResidentTraders.CustomerLedgerPDFReportsToZip", string.Format(LogResources.AuditingReportParameters, codes, date));

                List<string> listCodes = new List<string>();
                listCodes = codes.Split(';').ToList();

                DateTime tradingDate = DateTime.Parse("01/" + date);

                IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                PDFHelper helper = new PDFHelper();
                List<ZipItem> zipList = new List<ZipItem>();
                foreach (string c in listCodes)
                {
                    CustomerLedger report = service.LoadCustomerLedgerReports(Int32.Parse(c), tradingDate.ToString("MM/yyyy")).Result;
                    if (report != null && report.ClientCode > 0)
                    {
                        string fileName = string.Format("customerledger_{0}_{1}.pdf", report.ClientCode, tradingDate.ToString("MM-yyyy"));
                        zipList.Add(new ZipItem { FileStream = helper.CreateCustomerLedgerToMemoryStream(report), FileName = fileName });
                    }
                }

                string filesPath = string.Format(ConfigurationManager.AppSettings["Report.CustomerLedger.PDFPath"], 144);
                string zipName = string.Format("customerledger_{0}_{1}.zip", tradingDate.ToString("MM-yyyy"), DateTime.Now.ToString("MM-dd-yyyy-HH-mm"));

                service.Close();

                return ViewZip(zipList, true, filesPath, zipName);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return this.RedirectToAction("CustomerLedger");
            }

        }

        public ActionResult CustomerLedgerReportToExcel(int code, string date)
        {
            try
            {
                LogHelper.SaveAuditingLog("NonResidentTraders.CustomerLedgerReportToExcel", string.Format(LogResources.AuditingReportParameters, code, date));

                DateTime tradingDate = DateTime.Parse(date);

                IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                var response = service.LoadCustomerLedgerReports(code, tradingDate.ToString("MM/yyyy"));
                CustomerLedger report = response.Result;

                service.Close();

                string fileName = string.Format("customerledger_{0}_{1}.xls", report.ClientCode, tradingDate.ToString("MM-yyyy"));

                ViewData["code"] = code;
                ViewData["date"] = date;

                return ViewCustomerLedgerExcel(report, true, fileName);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return this.RedirectToAction("CustomerLedger");
            }

        }

        public ActionResult CustomerLedgerExcelReportsToZip(string codes, string date)
        {
            try
            {
                LogHelper.SaveAuditingLog("NonResidentTraders.CustomerLedgerReportToExcel", string.Format(LogResources.AuditingReportParameters, codes, date));

                List<string> listCodes = new List<string>();
                listCodes = codes.Split(';').ToList();

                DateTime tradingDate = DateTime.Parse("01/" + date);

                IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                ExcelHelper helper = new ExcelHelper();
                List<ZipItem> zipList = new List<ZipItem>();
                foreach (string c in listCodes)
                {
                    CustomerLedger report = service.LoadCustomerLedgerReports(Int32.Parse(c), tradingDate.ToString("MM-yyyy")).Result;
                    if (report != null && report.ClientCode > 0)
                    {
                        string fileName = string.Format("customerledger_{0}_{1}.xls", report.ClientCode, tradingDate.ToString("MM-yyyy"));
                        zipList.Add(new ZipItem { FileStream = helper.CreateCustomerLedgerToMemoryStream(report), FileName = fileName });
                    }
                }

                string filesPath = string.Format(ConfigurationManager.AppSettings["Report.CustomerLedger.PDFPath"], 144);
                string zipName = string.Format("customerledger_{0}_{1}.zip", tradingDate.ToString("MM-yyyy"), DateTime.Now.ToString("MM-dd-yyyy-HH-mm"));

                service.Close();

                return ViewZip(zipList, true, filesPath, zipName);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return this.RedirectToAction("CustomerLedger");
            }

        }

        public ActionResult PrintCustomerLedgerReport(string codes, string date)
        {
            List<CustomerLedger> reports = new List<CustomerLedger>();

            try
            {
                LogHelper.SaveAuditingLog("NonResidentTraders.PrintCustomerLedgerReport", string.Format(LogResources.AuditingReportParameters, codes, date));

                IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                List<string> listCodes = new List<string>();
                listCodes = codes.Split(';').ToList();

                foreach (string code in listCodes)
                {
                    var response = service.LoadCustomerLedgerReports(Convert.ToInt32(code), date);
                    CustomerLedger report = response.Result;
                    if (report != null && report.ClientCode > 0)
                        reports.Add(report);
                }

                service.Close();

                ViewData["code"] = codes;
                ViewData["date"] = date;

                return View(reports);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return View("CustomerLedger");
            }
        }

        public ActionResult CustomerLedgerReport(string code, string date)
        {
            CustomerLedger report = new CustomerLedger();

            try
            {
                LogHelper.SaveAuditingLog("NonResidentTraders.CustomerLedgerReport", string.Format(LogResources.AuditingReportParameters, code, date));

                IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                var response = service.LoadCustomerLedgerReports(Convert.ToInt32(code), date);
                report = response.Result;

                service.Close();

                ViewData["code"] = code;
                ViewData["date"] = date;

                return View(report);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return View("CustomerLedger");
            }
        }

        #endregion

        #region Stock Record

        [AccessControlAuthorize]
        public ActionResult StockRecord()
        {
            try
            {
                LogHelper.SaveAuditingLog("NonResidentTraders.StockRecord");
                LoadMonthsAndYears();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
            }

            return View();
        }

        [AccessControlAuthorize]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult StockRecord(string date)
        {
            try
            {
                LogHelper.SaveAuditingLog("NonResidentTraders.StockRecord.Post", string.Format(LogResources.AuditingReportParameters, "", date));

                IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

				var response = service.LoadStockRecordReports(date, CommonHelper.GetLoggedUserGroupId());

                service.Close();

                Session["StockRecordClients"] = response;
                return Json(new ActionResultModel { Status = "True", Messages = null });
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return Json(new ActionResultModel { Status = "False", Messages = null });
            }
        }

        public ActionResult StockRecordReportToPDF(string date)
        {
            try
            {
                LogHelper.SaveAuditingLog("NonResidentTraders.StockRecordReportToPDF", string.Format(LogResources.AuditingReportParameters, "", date));

                DateTime tradingDate = DateTime.Parse(date);

                IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

				var response = service.LoadStockRecordReports(tradingDate.ToString("MM/yyyy"), CommonHelper.GetLoggedUserGroupId());
                StockRecord report = response.Result;

                service.Close();

                string fileName = string.Format("stockrecord_{0}.pdf", tradingDate.ToString("MM-yyyy"));

                return ViewStockRecordPdf(report, true, fileName);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return this.RedirectToAction("StockRecord");
            }

        }

        public ActionResult StockRecordReportToExcel(string date)
        {
            try
            {
                LogHelper.SaveAuditingLog("NonResidentTraders.StockRecordReportToExcel", string.Format(LogResources.AuditingReportParameters, "", date));

                DateTime tradingDate = DateTime.Parse(date);

                IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

				var response = service.LoadStockRecordReports(tradingDate.ToString("MM/yyyy"), CommonHelper.GetLoggedUserGroupId());
                StockRecord report = response.Result;

                service.Close();

                string fileName = string.Format("stockrecord_{0}.xls", tradingDate.ToString("MM-yyyy"));

                ViewData["date"] = date;

                return ViewStockRecordExcel(report, true, fileName);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return this.RedirectToAction("StockRecord");
            }

        }

        public ActionResult PrintStockRecordReport(string date)
        {
            StockRecord report = new StockRecord();

            try
            {
                LogHelper.SaveAuditingLog("NonResidentTraders.PrintStockRecordReport", string.Format(LogResources.AuditingReportParameters, "", date));

                IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

				var response = service.LoadStockRecordReports(date, CommonHelper.GetLoggedUserGroupId());
                report = response.Result;

                service.Close();

                ViewData["date"] = date;

                return View(report);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return View("StockRecord");
            }
        }

        public ActionResult StockRecordReport(string date)
        {
            StockRecord report = new StockRecord();

            try
            {
                LogHelper.SaveAuditingLog("NonResidentTraders.StockRecordReport", string.Format(LogResources.AuditingReportParameters, "", date));

                IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

				var response = service.LoadStockRecordReports(date, CommonHelper.GetLoggedUserGroupId());
                report = response.Result;

                service.Close();

                ViewData["date"] = date;

                return View(report);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return View(report);
            }
        }

        #endregion

        #region Fail Ledger

        [AccessControlAuthorize]
        public ActionResult FailLedger()
        {
            LogHelper.SaveAuditingLog("NonResidentTraders.FailLedger");
            return View();
        }

        public ActionResult FailLedgerReportToPDF(string date, string type)
        {
            try
            {
                LogHelper.SaveAuditingLog("NonResidentTraders.FailLedgerReportToPDF", string.Format(LogResources.AuditingFailLedgerReportParameters, string.Empty, date, type));

                DateTime tradingDate = DateTime.Parse(date);

                IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

				var response = service.LoadFailLedgerReports(-1, tradingDate, (FailLedgerType)Int32.Parse(type), CommonHelper.GetLoggedUserGroupId());
                FailLedger report = response.Result;

                service.Close();

                string fileName = string.Format("failledger_{0}.pdf", tradingDate.ToString("MM-dd-yyyy"));

                return ViewFailLedgerPdf(report, true, fileName);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return this.RedirectToAction("FailLedger");
            }

        }

        public ActionResult FailLedgerReportToExcel(string date, int type)
        {
            try
            {
                LogHelper.SaveAuditingLog("NonResidentTraders.FailLedgerReportToExcel", string.Format(LogResources.AuditingFailLedgerReportParameters, string.Empty, date, type));

                DateTime tradingDate = DateTime.Parse(date);

                IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

				var response = service.LoadFailLedgerReports(-1, tradingDate, (FailLedgerType)type, CommonHelper.GetLoggedUserGroupId());
                FailLedger report = response.Result;

                service.Close();

                string fileName = string.Format("failledger_{0}.xls", tradingDate.ToString("MM-dd-yyyy"));

                ViewData["date"] = date;

                return ViewFailLedgerExcel(report, true, fileName);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return this.RedirectToAction("FailLedger");
            }

        }

        public ActionResult PrintFailLedgerReport(string date, int type)
        {
            FailLedger report = new FailLedger();

            try
            {
                LogHelper.SaveAuditingLog("NonResidentTraders.PrintFailLedgerReport", string.Format(LogResources.AuditingFailLedgerReportParameters, string.Empty, date, type));

                DateTime tradingDate = DateTime.Parse(date);

                IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

				var response = service.LoadFailLedgerReports(-1, tradingDate, (FailLedgerType)type, CommonHelper.GetLoggedUserGroupId());
                report = response.Result;

                service.Close();

                ViewData["type"] = type;
                ViewData["date"] = date;

                return View(report);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return View("FailLedger");
            }
        }

        public ActionResult FailLedgerReport(string date, string type)
        {
            FailLedger report = new FailLedger();

            try
            {
                LogHelper.SaveAuditingLog("NonResidentTraders.FailLedgerReport", string.Format(LogResources.AuditingFailLedgerReportParameters, string.Empty, date, type));

                IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                DateTime tradingDate = DateTime.Parse(date);
                FailLedgerType reportType = (FailLedgerType)Int32.Parse(type);

				var response = service.LoadFailLedgerReports(-1, tradingDate, reportType, CommonHelper.GetLoggedUserGroupId());
                report = response.Result;

                service.Close();

                ViewData["date"] = date;
                ViewData["type"] = type;

                return View(report);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return View(report);
            }
        }

        #endregion

        #endregion

        #region Brokerage

        [AccessControlAuthorize]
        public ActionResult Brokerage()
        {
            LogHelper.SaveAuditingLog("NonResidentTraders.Brokerage");
            return View();
        }

        public ActionResult BrokerageReport(string startDate, string endDate, int code, string status)
        {
            List<BrokerageTransfer> list = new List<BrokerageTransfer>();

            try
            {
                LogHelper.SaveAuditingLog("NonResidentTraders.BrokerageReport", string.Format(LogResources.AuditingBrokerageParameters, code, startDate, endDate, status));

                IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

				var response = (status == "0") ? service.LoadPendingBrokerageTransfers(code, startDate, CommonHelper.GetLoggedUserGroupId()) : service.LoadSettledBrokerageTransfers(code, startDate, endDate, CommonHelper.GetLoggedUserGroupId());
                list = response.Result.ToList();

                service.Close();

                ViewData["startDate"] = startDate;
                ViewData["endDate"] = endDate;
                ViewData["code"] = code;
                ViewData["status"] = status;

                return View(list);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return View(list);
            }
        }

        public ActionResult ConfirmSettlement(string Date)
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SettlePendingBrokerages(string hdfDate)
        {
            List<BrokerageTransfer> list = new List<BrokerageTransfer>();

            try
            {
                LogHelper.SaveAuditingLog("NonResidentTraders.SettlePendingBrokerages", string.Format(LogResources.AuditingSettleBrokerageParameters, hdfDate));

                IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                var response = service.SettleBrokerageTransfers(hdfDate, SessionHelper.CurrentUser.UserName);

                if (response.Result)
                    TempData["Success"] = true;
                else
                    TempData["ErrorMessage"] = "Repasses pendentes não foram liquidados.";

                TempData["FilterDate"] = hdfDate;

                service.Close();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
            }

            return RedirectToAction("Brokerage");
        }

        public ActionResult BrokerageToPDF(string startDate, string endDate, int code, string status)
        {
            try
            {
                LogHelper.SaveAuditingLog("NonResidentTraders.BrokerageReportToPDF", string.Format(LogResources.AuditingBrokerageParameters, code, startDate, endDate, status));

                IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                bool pending = (status == "0");

				var response = pending ? service.LoadPendingBrokerageTransfers(code, startDate, CommonHelper.GetLoggedUserGroupId()) : service.LoadSettledBrokerageTransfers(code, startDate, endDate, CommonHelper.GetLoggedUserGroupId());
                List<BrokerageTransfer> list = response.Result.ToList();

                var header = service.LoadHeader().Result;

                service.Close();

                string fileName = string.Format("repassesCorretagem_{0}_{1}{2}{3}.pdf", pending ? "pendentes" : "liquidados", startDate, !pending ? string.Concat("_", endDate) : string.Empty, (code != -1) ? string.Concat("_", code) : string.Empty);

                return ViewBrokeragePdf(list, true, fileName, startDate, endDate, header);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return this.RedirectToAction("Brokerage");
            }

        }

        public ActionResult BrokerageToExcel(string startDate, string endDate, int code, string status)
        {
            try
            {
                LogHelper.SaveAuditingLog("NonResidentTraders.BrokerageToExcel", string.Format(LogResources.AuditingBrokerageParameters, code, startDate, endDate, status));

                IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                bool pending = (status == "0");

				var response = pending ? service.LoadPendingBrokerageTransfers(code, startDate, CommonHelper.GetLoggedUserGroupId()) : service.LoadSettledBrokerageTransfers(code, startDate, endDate, CommonHelper.GetLoggedUserGroupId());
                List<BrokerageTransfer> list = response.Result.ToList();

                var header = service.LoadHeader().Result;

                service.Close();

                string fileName = string.Format("repassesCorretagem_{0}_{1}{2}{3}.xls", pending ? "pendentes" : "liquidados", startDate, !pending ? string.Concat("_", endDate) : string.Empty, (code != -1) ? string.Concat("_", code) : string.Empty);

                return ViewBrokerageExcel(list, true, fileName, startDate, endDate, pending);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return this.RedirectToAction("Brokerage");
            }

        }

        public ActionResult PrintBrokerageReport(string startDate, string endDate, int code, string status)
        {
            try
            {
                LogHelper.SaveAuditingLog("NonResidentTraders.PrintBrokerageReport", string.Format(LogResources.AuditingBrokerageParameters, code, startDate, endDate, status));

                IChannelProvider<INonResidentTradersServiceContractChannel> channelProvider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                bool pending = (status == "0");

				var response = pending ? service.LoadPendingBrokerageTransfers(code, startDate, CommonHelper.GetLoggedUserGroupId()) : service.LoadSettledBrokerageTransfers(code, startDate, endDate, CommonHelper.GetLoggedUserGroupId());
                List<BrokerageTransfer> list = response.Result.ToList();

                ViewBag.Header = service.LoadHeader().Result;

                service.Close();

                ViewData["startDate"] = startDate;
                ViewData["endDate"] = endDate;

                return View(list);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return View("Brokerage");
            }
        }

        #endregion

        protected void LoadMonthsAndYears()
        {
            String[] months = new String[12];
            string month;
            for (var i = 1; i < 13; i++)
            {
                DateTime dt = new DateTime(9999, i, 1);
                month = dt.ToString("MMMM");
                months[i - 1] = String.Concat(char.ToUpper(month[0]), month.Substring(1));
            }

            String[] years = new String[6];
            int year = DateTime.Today.Year;
            years[5] = year.ToString();
            for (var i = 4; i > -1; i--)
            {
                year--;
                years[i] = year.ToString();
            }

            ViewBag.Months = months;
            ViewBag.Years = years;
        }

    }
}
