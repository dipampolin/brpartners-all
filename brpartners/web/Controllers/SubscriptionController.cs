﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Newtonsoft.Json;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.WebSite.Helper;
using QX3.Portal.WebSite.Models;
using QX3.Portal.WebSite.Models.Channel;
using QX3.Portal.WebSite.Properties;
using QX3.Portal.WebSite.SubscriptionService;
using QX3.Spinnex.Common.Services.Logging;
using System.Linq;
using System.IO;

namespace QX3.Portal.WebSite.Controllers
{
	public class SubscriptionController : FileController
    {
        private IChannelProvider<ISubscriptionContractChannel> subChn;
        public IChannelProvider<ISubscriptionContractChannel> SubscriptionService
        {
            get 
            {
                if (subChn == null)
                    subChn = ChannelProviderFactory<ISubscriptionContractChannel>.Create("SubscriptionClassName");
                return subChn;
            }

        }

		private int[][] ListaIdsSubscricao
		{
			get
			{
				return (int[][])Session["Subscription.RequestRightsSubscriptionIdsList"];
			}
			set
			{
				Session["Subscription.RequestRightsSubscriptionIdsList"] = value;
			}
		}

        #region Subscrições

        [HttpPost]
        public string InitialSubscriptionFilter(FormCollection form) 
        {
            WcfResponse<List<Subscription>, int> list = new WcfResponse<List<Subscription>, int>();
            IChannelProvider<ISubscriptionContractChannel> subscriptionProvider = ChannelProviderFactory<ISubscriptionContractChannel>.Create("SubscriptionClassName");

            using (var subscriptionService = subscriptionProvider.GetChannel().CreateChannel())
            {
                subscriptionService.Open();

                var filter = new Subscription();

                if (!String.IsNullOrEmpty(form["ddlStatus"]))
                    filter.Status = form["ddlStatus"].ToCharArray()[0];

                var options = new FilterOptions()
                {
                    InitRow = Convert.ToInt32(form["iDisplayStart"]),
                    FinalRow = Convert.ToInt32(form["iDisplayLength"]),
                    SortColumn = (form["iSortCol_0"] == "null" || form["iSortCol_0"] == "undefined" || form["iSortCol_0"] == "0") ? 1 : Convert.ToInt32(form["iSortCol_0"]),
                    SortDirection = form["sSortDir_0"],
                    AllRecords = false
                };

                list = subscriptionService.LoadSubscriptions(filter, options, false);

                Session["Subscription.List"] = list.Result;
				Session["Subscription.SubscriptionFilter"] = filter;
				Session["Subscription.SubscriptionFilterOptions"] = options;

                subscriptionService.Close();
            }
            return new SubscriptionHelper().SubscriptionListObject((list.Result) != null ? list.Result : new List<Subscription>(), ref form, list.Data);
        }

        [HttpPost]
        public string SubscriptionFilter(FormCollection form)
        {
            WcfResponse<List<Subscription>, int> list = new WcfResponse<List<Subscription>, int>();
            IChannelProvider<ISubscriptionContractChannel> subscriptionProvider = ChannelProviderFactory<ISubscriptionContractChannel>.Create("SubscriptionClassName");

            using (var subscriptionService = subscriptionProvider.GetChannel().CreateChannel())
            {
                subscriptionService.Open();

                var filter = new Subscription();

                if (!string.IsNullOrEmpty(form["txtStockCode"]))
                filter.ISINStock = form["txtStockCode"].ToUpper().Trim();

                if (!string.IsNullOrEmpty(form["txtInitialDate"]))
                    filter.InitialDate = Convert.ToDateTime(form["txtInitialDate"]);
                if (!string.IsNullOrEmpty(form["txtBrokerDate"]))
                    filter.BrokerDate = Convert.ToDateTime(form["txtBrokerDate"]);

                if (!string.IsNullOrEmpty(form["ddlStatus"]))
                    filter.Status = form["ddlStatus"].ToCharArray()[0];

                var options = new FilterOptions()
                {
                    InitRow = Convert.ToInt32(form["iDisplayStart"]),
                    FinalRow = Convert.ToInt32(form["iDisplayLength"]),
                    SortColumn = (form["iSortCol_0"] == "null" || form["iSortCol_0"] == "undefined" || form["iSortCol_0"] == "0") ? 1 : Convert.ToInt32(form["iSortCol_0"]),
                    SortDirection = form["sSortDir_0"],
                    AllRecords = false
                };

                list = subscriptionService.LoadSubscriptions(filter, options, false);

                subscriptionService.Close();

                Session["Subscription.List"] = list.Result;
				Session["Subscription.SubscriptionFilter"] = filter;
				Session["Subscription.SubscriptionFilterOptions"] = options;

                subscriptionService.Close();
            }

            return new SubscriptionHelper().SubscriptionListObject((list.Result) != null ? list.Result : new List<Subscription>(), ref form, list.Data);
        }

        [AccessControlAuthorize]
        public ActionResult Index()
        {
            return View();
        }

        private Subscription GetSingleSubscription(int id) 
        {
            Subscription sub = null;
            if (id != 0)
            {
                using (var service = SubscriptionService.GetChannel().CreateChannel())
                {
                    service.Open();

                    var list = service.LoadSubscriptions(new Subscription(), new FilterOptions() { AllRecords = true, SortColumn = 1, SortDirection = "asc" }, true).Result;

                    if (list != null)
                        sub = list.Where(a => a.RequestId == id).FirstOrDefault();

                    service.Close();
                }

            }

            return sub;
        }

        public ActionResult SubscriptionDetail(int id)
        {
            Subscription sub = null;
            if (id != 0)
            {
                using (var service = SubscriptionService.GetChannel().CreateChannel())
                {
                    service.Open();

                    var list = service.LoadSubscriptions(new Subscription(), new FilterOptions() { AllRecords = true, SortColumn = 1, SortDirection = "asc" }, true).Result;

                    if (list != null)
                        sub = list.Where(a => a.RequestId == id).FirstOrDefault();

                    service.Close();
                }

            }
            return View(sub);
        }

        public ActionResult SubscriptionRequest(int id)
        {
            return View(GetSingleSubscription(id));
        }

        [HttpPost]
        public ActionResult SubscriptionRequest(FormCollection form) 
        {
            LogHelper.SaveAuditingLog("Subscription.Request");

            var result = this.ProcessSubscriptionRequest(form, true);

            return Json(new WcfResponse<bool>() { Result = result, Message = (result) ? "Subscrição criada com sucesso." : "A subscrição não foi criada." });
        }

        [HttpPost]
        public ActionResult SubscriptionRequestUpdate(FormCollection form)
        {
            LogHelper.SaveAuditingLog("Subscription.Request");

            /*Validando pdf*/
            /*if (Request.Files.Count > 0)
            {
                if (Request.Files[0].ContentLength > 0)
                {
                    if (Request.Files[0].ContentType != "application/pdf")
                        return Json(new WcfResponse<bool>() { Result = false, Message = "Arquivo inválido. É permitido apenas arquivos .pdf" });
                }
            }*/

            var result = this.ProcessSubscriptionRequest(form, false);

            return Json(new WcfResponse<bool>() { Result = result, Message = (result) ? "Subscrição editada com sucesso." : "A subscrição não foi editada." });
        }

        private bool ProcessSubscriptionRequest(FormCollection form, bool isNew) 
        {
            var request = new Subscription()
            {
                Company = form["txtCompany"],
                ISINStock = form["txtISINStock"],
                ISINSubscription = form["txtISINSubscription"],
                InitialDate = Convert.ToDateTime(form["txtInitialDate"]),
                COMDate = Convert.ToDateTime(form["txtCOMDate"]),
                BrokerDate = Convert.ToDateTime(form["txtBrokerDate"]),
                BrokerHour = Convert.ToDateTime(form["txtBrokerHour"]),
                StockExchangeDate = Convert.ToDateTime(form["txtStockExchangeDate"]),
                CompanyDate = Convert.ToDateTime(form["txtCompanyDate"]),
                RightPercentual = Convert.ToDecimal(form["txtRightPercentual"]),
                RightValue = Convert.ToDecimal(form["txtRightValue"]),
                Description = form["txtDescription"],
                SubjectMail = form["txtSubjectMail"],
                BodyMail = form["txtBodyMail"],
                Status = 'P'
            };

            /*if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                if (file != null)
                {
                    if (file.ContentLength > 0)
                    {
                        
                        byte[] b = new byte[file.ContentLength];

                        using (var bin = new BinaryReader(file.InputStream))
                        {
                            bin.Read(b, 0, file.ContentLength);

                            request.Prospect = b;
                        }
                    }
                }
            }*/

            WcfResponse<bool, int> response = null;

            using (var service = SubscriptionService.GetChannel().CreateChannel())
            {
                service.Open();

                if (isNew)
                {
                    response = service.SubscriptionRequest(request);
                }
                else 
                {
                    request.RequestId = Convert.ToInt32(form["hdfEditRequestId"]);
                    response = service.SubscriptionUpdate(request);
                }

                if (response.Result)
                {
                    LogHelper.SaveHistoryLog("Subscription.Index",
                                new HistoryData
                                {
                                    Action = (isNew) ? "Solicitar" : "Editar",
                                    NewData = JsonConvert.SerializeObject(request),
                                    ID = (isNew) ? response.Data : request.RequestId,
                                    Target = request.Company,
                                    ExtraTarget = request.ISINSubscription
                                });
                }
            }

            return response.Result;
        }

        public ActionResult SubscriptionConfirmDelete(int id) 
        {
            return View(GetSingleSubscription(id));
        }

        [HttpPost]
        public ActionResult SubscriptionDelete(FormCollection form) 
        {
            var request = new Subscription()
            {
                RequestId = Convert.ToInt32(form["hdfRequest"]),
                ISINStock = form["hdfISINStock"],
                ISINSubscription = form["hdfISINSubscription"]
            };

            WcfResponse<bool, int> response = null;

            using (var service = SubscriptionService.GetChannel().CreateChannel())
            {
                service.Open();

                response = service.SubscriptionDelete(request);

                if (response.Result) 
                {
                    LogHelper.SaveHistoryLog("Subscription.Index",
                                new HistoryData
                                {
                                    Action = "Excluir",
                                    NewData = JsonConvert.SerializeObject(request),
                                    ID = request.RequestId,
                                    Target = form["hdfCompanyName"],
                                    ExtraTarget = request.ISINSubscription
                                });
                }
            }

            return Json(new WcfResponse<bool> { Result = response.Result, Message = response.Result ? "Subscrição excluida com sucesso." : "Subscrição não excluida" });
        }

        public ActionResult ProspectView(int id) 
        {
            Subscription sub = GetSingleSubscription(id);
            
            if (sub.Prospect != null)
            {
                return new BinaryContentResult(sub.Prospect, "application/pdf", true, "prospecto_" + sub.Company + ".pdf");
            }
            else
            {
                TempData["ErrorMessage"] = "Não existe prospecto para a subscrição.";
                return RedirectToAction("Index", "Subscription");
            }
        }

        public ActionResult SubscriptionHistory()
        {
            List<SelectListItem> listItems = new List<SelectListItem>();
            listItems.Add(new SelectListItem { Text = "Todas", Value = "", Selected = true });

            try
            {
                var actions = SubscriptionHelper.SubscriptionActions;
                foreach (string action in actions)
                    listItems.Add(new SelectListItem { Text = action, Value = action, Selected = false });

                ViewBag.SubscriptionActions = listItems;

                LogHelper.SaveAuditingLog("Subscription.Index.History");
                return View();
            }

            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = "Não foi possível carregar o histórico de subscrições.";
                return View();
            }
        }

        [HttpPost]
        public void LoadSubscriptionHistory(FormCollection form)
        {
            try
            {
                string startDate = form["txtStartDate"];
                string endDate = form["txtEndDate"];

                long responsibleID = string.IsNullOrEmpty(form["hdfResponsible"]) ? 0 : long.Parse(form["hdfResponsible"]);
                string txtCompany = form["txtCompany"];
                //string StockCode = string.IsNullOrEmpty(form["txtStockCode"]) ? "" : form["txtStockCode"];

                int id = 0;
                Int32.TryParse(form["hdfRequestID"], out id);

                string action = form["ddlSubscriptionActions"];

                LogHelper.SaveAuditingLog("Subscription.SubscriptionHistory",
                    string.Format(LogResources.AuditingLoadHistory, txtCompany, responsibleID, action, startDate, endDate));

                HistoryFilter filter = new HistoryFilter
                {
                    ModuleName = "Subscription.Index",
                    StartDate = startDate,
                    Action = action,
                    EndDate = endDate,
                    //ResponsibleID = responsibleID, 
                    TargetText = txtCompany,
                    ID = id
                };

                
                Session["Subscription.SubscriptionHistoryList"] = LogHelper.LoadHistory(filter); 

            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }

        }

        public ActionResult Subscription_SubscriptionHistoryList(FormCollection form)
        {
            List<HistoryData> items = (List<HistoryData>)Session["Subscription.SubscriptionHistoryList"];
            return new DataTablesController().AccessControl_History(form, items);
        }

        [HttpGet]
        public ActionResult SubscriptionHistoryDetail(string requestId)
        {
            try
            {
                LogHelper.SaveAuditingLog("Subscription.SubscriptionHistoryDetail");

                var subscription = this.GetSingleSubscription(Convert.ToInt32(requestId));
                
                HistoryFilter filter = new HistoryFilter { ModuleName = "Subscription.Index", ID = subscription.RequestId };

                var data = LogHelper.LoadHistory(filter);

                Session["Subscription.SubscriptionHistoryListDetail"] = data;
                ViewBag.SubscriptionHistoryDetails = data;

                return View(subscription);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = "Não foi possível carregar o histórico deste termo.";
                return View(new Subscription());
            }
        }

        public ActionResult SubscriptionHistoryToFile(bool isPdf, bool detail)
        {
            List<HistoryData> currentList = new List<HistoryData>();

            string sessionName = detail ? "Subscription.SubscriptionHistoryListDetail" : "Subscription.SubscriptionHistoryList";

            try
            {
                if (Session[sessionName] != null)
                    currentList = (List<HistoryData>)Session[sessionName];

                //LogHelper.SaveAuditingLog(string.Concat("Subscription.Index.HistoryToFile"), string.Format(LogResources.ObjectToFileParameters, isPdf ? "PDF" : "Excel", JsonConvert.SerializeObject(currentList)));

                string targetTitle = string.Concat(isPdf ? "Histórico de subscrições" : "Hist&#243;rico de subscri&#231;&#245;es");

                string fileName = string.Format("historico_{0}_{1}.{2}", "subscricao", DateTime.Now.ToString("dd-MM-yyyy"), isPdf ? "pdf" : "xls");

                return isPdf
                    ? ViewHistoryPdf(currentList, true, fileName, targetTitle, "Empresa", true, "Subscri&#231;&#227;o")
                    : ViewHistoryExcel(currentList, true, fileName, targetTitle, "Empresa", true, "Subscri&#231;&#227;o");

            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return this.RedirectToAction("Index");
            }
        }

        [HttpGet]
        public ActionResult SubscriptionsToFile(bool isPdf)
        {
            List<Subscription> currentList = new List<Subscription>();
            try
            {
                if (Session["Subscription.SubscriptionFilter"] != null && Session["Subscription.SubscriptionFilterOptions"] != null)
                {
                    Subscription filter = (Subscription)Session["Subscription.SubscriptionFilter"];
                    FilterOptions options = (FilterOptions)Session["Subscription.SubscriptionFilterOptions"];
                    options.AllRecords = true;

                    ISubscriptionContractChannel contractChannel = SubscriptionService.GetChannel().CreateChannel();
                    contractChannel.Open();
                    currentList = contractChannel.LoadSubscriptions(filter, options, false).Result;
                    contractChannel.Close();
                }

                //LogHelper.SaveAuditingLog("Subscription.SubscriptionsToFile", string.Format(LogResources.ObjectToFileParameters, isPdf ? "PDF" : "Excel", JsonConvert.SerializeObject(currentList)));

                string fileName = string.Format("subscricoes_{0}.{1}", DateTime.Now.ToString("dd-MM-yyyy"), isPdf ? "pdf" : "xls");

                LogHelper.SaveHistoryLog("Subscription.Index", new HistoryData { Action = "Exportar", OldData = string.Empty, NewData = JsonConvert.SerializeObject(currentList) });

                return isPdf ? ViewSubscriptionsPdf(currentList, true, fileName) : ViewSubscriptionsExcel(currentList, true, fileName);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = "Ocorreu um erro ao gerar relatório.";
                return this.RedirectToAction("Subscription");
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ChangeStatus(string requestId, string isinStock, string isinSubscription, string status, string companyName)
        {
            try
            {
                LogHelper.SaveAuditingLog("Subscription.ChangeStatus");

                IChannelProvider<ISubscriptionContractChannel> channelProvider = ChannelProviderFactory<ISubscriptionContractChannel>.Create("SubscriptionClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                Subscription obj = new Subscription();
                obj.RequestId = Convert.ToInt32(requestId);
                obj.ISINStock = isinStock;
                obj.ISINSubscription = isinSubscription;
                obj.Status = status.ToCharArray()[0];

                var insert = service.SubscriptionUpdate(obj);

                service.Close();

                if (insert.Result)
                {
                    string action = status.Equals("A") ? "Aprovar" : "Reprovar";
                    LogHelper.SaveHistoryLog("Subscription.Index", new HistoryData
                    {
                        Action = action,
                        ID = Convert.ToInt32(requestId),
                        Target = companyName,
                        ExtraTarget = isinSubscription,
                        NewData = JsonConvert.SerializeObject(obj)
                    });
                }

                return Json(new { Error = insert.Result ? "" : "Erro na atualização." });
            }
            catch (Exception)
            {
                return Json(new { Error = "Erro na atualização." });
            }
        }

        public ActionResult SubscriptionApprove(string requestsIds)
        {
            try
            {
                string[] arrRequestsIds = requestsIds.Split(';');
                var lstObj = (List<Subscription>)Session["Subscription.List"];

                List<Subscription> lstToApprove = new List<Subscription>();

                int index = 1;
                foreach (string i in arrRequestsIds)
                    if (!string.IsNullOrEmpty(i))
                    {
                        Subscription obj = (from s in lstObj where s.RequestId.Equals(Convert.ToInt32(i)) && s.Status.Value.Equals('P') select s).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.Index = index++;
                            lstToApprove.Add(obj);
                        }
                    }

                Session["Subscription.ListToApprove"] = lstToApprove;
                ViewBag.SubscriptionToApprove = lstToApprove;
            }
            catch (Exception)
            {

                ViewBag.SubscriptionToApprove = null;
            }

            return View();
        }

        #endregion

		#region Subscrição de Direitos

        [AccessControlAuthorize]
        public ActionResult RequestRights()
        {
            Session["Subscription.RequestRightsFilter"] = null;
            Session["Subscription.RequestRightsList"] = null;

			WcfResponse<List<Subscription>, int> subscriptionsList = new WcfResponse<List<Subscription>, int>();
			IChannelProvider<ISubscriptionContractChannel> subscriptionProvider = ChannelProviderFactory<ISubscriptionContractChannel>.Create("SubscriptionClassName");

			using (ISubscriptionContractChannel subscriptionService = subscriptionProvider.GetChannel().CreateChannel())
			{
				FilterOptions options = new FilterOptions()
				{
					InitRow = 0,
					FinalRow = 0,
					SortColumn = 1,
					SortDirection = string.Empty,
					AllRecords = true
				};

                subscriptionsList = subscriptionService.LoadSubscriptions(new Subscription() { Status = 'S' }, options, false);
                var subTeste = (subscriptionsList.Result != null) ? subscriptionsList.Result.Where(a => a.Status.Value == 'A' || a.Status.Value == 'S' || a.Status.Value == 'F').ToList() : subscriptionsList.Result;

                Session["Subscription.RequestRightsSubscriptionsList"] = subTeste;//subscriptionsList.Result;
                ViewBag.TotalSubscricoes = subTeste != null ? subTeste.Count : 0;//subscriptionsList.Result.Count;
                if (subTeste != null)
				    ListaIdsSubscricao = subTeste.Select(p => new int[] { p.RequestId }).ToArray();
                //subscriptionsList.Result.Select(p => new int[] { p.RequestId }).ToArray();
			}

			Session["Subscription.FlagChangeFilterSubscription"] = string.Empty;

            return View();
        }

		public ActionResult RightsRequest(int requestId, string clientIds, string status)
		{
            ViewBag.Subscription = this.GetSingleSubscription(requestId);
            List<SubscriptionRights> right = null;

            if (!String.IsNullOrEmpty(clientIds))
            {
                clientIds = new MarketHelper().GetClientsRangesToFilter(clientIds);

                right = this.GetRightRequest(requestId, clientIds, String.IsNullOrEmpty(status) ? null : status, false);

                if (right != null)
                {
                    var listClients = right.Where(a => a.Status == 'P').Select(a => a.ClientCode).Distinct().ToList();

                    right = right.Where(a => !listClients.Contains(a.ClientCode)).ToList();
                }

                /*right = ((List<SubscriptionRights>)Session["Subscription.RequestRightsList"])
                    .Where(a => a.RequestId == requestId && clientIds.Contains(a.ClientCode.ToString()))
                    .ToList();

                if (status != null)
                    right = right.Where(a => a.Status == status.ToCharArray()[0]).ToList();*/
            }

			return View(right == null ? null : right.Where(a=>a.AvailableQtty > 0).ToList());
		}

        private List<SubscriptionRights> GetRightRequest(int requestId, string clientIds, string status, bool fromSession) 
        {
            using (var service = SubscriptionService.GetChannel().CreateChannel())
            {
                service.Open();
                List<SubscriptionRights> response = null;

                var rights = new SubscriptionRights() { RequestId = requestId, ClientCodeFilter = clientIds };
                /*if (!string.IsNullOrEmpty(status))
                    rights.Status = status.ToCharArray()[0];*/

                if (!fromSession)
                    response = service.LoadSubscriptionRights(rights,
                        new FilterOptions() { AllRecords = true, SortColumn = 1, SortDirection = "asc" }, CommonHelper.GetLoggedUserGroupId()).Result;

                else
                    response = ((List<SubscriptionRights>)Session["Subscription.RequestRightsList"])
                        .Where(a => a.RequestId == requestId && clientIds.Contains(a.ClientCode.ToString()))
                        .ToList();

                return response;
            }
        }

        [HttpPost]
        public ActionResult GetRightRequestForClient(int requestId, string clientIds, string status) 
        {
            if (requestId != 0 && !String.IsNullOrEmpty(clientIds))
            {
                clientIds = new MarketHelper().GetClientsRangesToFilter(clientIds);
                var result = this.GetRightRequest(requestId, clientIds, status, false); 
                /*((List<SubscriptionRights>)Session["Subscription.RequestRightsList"])
                    .Where(a => a.RequestId == requestId && clientIds.Contains(a.ClientCode.ToString()))
                    .ToList();*/

                if (result != null) 
                { 
                    var listClients = result.Where(a=>a.Status == 'P').Select(a=>a.ClientCode).Distinct().ToList();

                    result = result.Where(a => !listClients.Contains(a.ClientCode)).ToList();
                }

                return Json(result == null ? null : result.Where(a=>a.AvailableQtty > 0).FirstOrDefault());
            }
            else
                return null;
        }

        private bool ProcessRightsRequest(FormCollection form, bool isNew, out int countSuccess, out int countError) 
        {
            WcfResponse<bool> response = new WcfResponse<bool>();
            bool result = true;
            countSuccess = 0; countError = 0;

            using (var service = SubscriptionService.GetChannel().CreateChannel())
            {
                service.Open();
                foreach (var s in form.AllKeys.Where(a => a.Contains("txtClient_")))
                {
                    string order = s.Replace("txtClient_", "");
                    var clientCode = form[s];

                    var qtty = Convert.ToInt32(form["txtQuantity_" + order]);

                    if (qtty <= 0)
                    {
                        continue;
                    }

                    var request = new SubscriptionRights()
                    {
                        ClientCode = Convert.ToInt32(clientCode),
                        RequestId = Convert.ToInt32(form["hdfRequestId"]),
                        RequestedQtty = qtty,
                        Scraps = (form["chkScraps_" + order] == "on") ? 'S' : 'N',
                        Status = 'P'
                    };


                    if (isNew)
                    {
                        request.Warning = 'E';
                        response = service.SubscriptionRightsRequest(request);
                    }
                    else
                        response = service.SubscriptionRightsUpdate(request);

                    if (!response.Result) 
                    {
                        if (result) result = false;
                        countError++;
                    }
                    else
                    {
                        countSuccess++;

                        IChannelProvider<QX3.Portal.WebSite.NonResidentTradersService.INonResidentTradersServiceContractChannel> channelProvider = 
                            ChannelProviderFactory<QX3.Portal.WebSite.NonResidentTradersService.INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                        var nonresidentService = channelProvider.GetChannel().CreateChannel();

                        nonresidentService.Open();

                        LogHelper.SaveHistoryLog("Subscription.RequestRights",
                                    new HistoryData
                                    {
                                        Action = (isNew) ? "Solicitar" : "Editar",
                                        NewData = JsonConvert.SerializeObject(request),
                                        ID = request.RequestId,
                                        TargetID = request.ClientCode,
                                        ResponsibleID = Convert.ToInt32(SessionHelper.CurrentUser.ID),
                                        Target = GetSingleSubscription(request.RequestId).ISINSubscription,
                                        ExtraTarget = nonresidentService.LoadCustomerName(request.ClientCode).Result
                                    });

                        nonresidentService.Close();
                    }
                }
                service.Close();
            }

            return result;
        }

        [HttpPost]
        public ActionResult RightsRequest(FormCollection form) 
        {
            int countSuccess, countError = 0;
            bool result = this.ProcessRightsRequest(form, true, out countSuccess, out countError);

            var message = "";

            if (!result)
                message = "Nenhuma solicitação efetuada.";
            else
            {
                Session["Subscription.RequestRightsFilter"] = null;

                if (countSuccess == 0 && countError == 0)
                    message = "Nenhuma solicitação efetuada.";
                else
                {
                    if (countError > 0)
                        message = ((countError == 1) 
                                        ? "Uma Solicitação" : countError.ToString() + " Solicitações") + " não foram efetuada" + (countError == 1 ? "" : "s") + ".";
                    else if (countSuccess > 0)
                        message = ((countSuccess > 1) ? "Solicitações efetuadas" : "Solicitação efetuada") + " com sucesso.";
                }
            }

            return Json(new WcfResponse<bool>()
            {
                Result = result,
                Message = message
            });
        }

        [HttpPost]
        public ActionResult RightsRequestUpdate(FormCollection form)
        {
            int countSuccess, countError = 0;
            bool result = this.ProcessRightsRequest(form, false, out countSuccess, out countError);

            var message = "";

            if (!result)
                message = "Nenhuma solicitação alterada.";
            else
            {
                Session["Subscription.RequestRightsFilter"] = null;

                if (countSuccess == 0 && countError == 0)
                    message = "Nenhuma solicitação alterada.";
                else
                {
                    if (countError > 0)
                        message = ((countError == 1)
                                        ? "Uma Solicitação" : countError.ToString() + " Solicitações") + " não foram alterada" + (countError == 1 ? "" : "s") + ".";
                    else if (countSuccess > 0)
                        message = ((countSuccess > 1) ? "Solicitações alteradas" : "Solicitação alterada") + " com sucesso.";
                }
            }

            return Json(new WcfResponse<bool>() { 
                Result = result,
                Message = message
            });
        }

		[HttpPost]
		public string InitialRequestRightsFilter(FormCollection form)
		{
			WcfResponse<List<SubscriptionRights>, int> list = new WcfResponse<List<SubscriptionRights>, int>();
			IChannelProvider<ISubscriptionContractChannel> subscriptionProvider = ChannelProviderFactory<ISubscriptionContractChannel>.Create("SubscriptionClassName");

			using (var subscriptionService = subscriptionProvider.GetChannel().CreateChannel())
			{
                SubscriptionRights filter = new SubscriptionRights();
				if (ListaIdsSubscricao.Length > 0)
				{
					filter.RequestId = ListaIdsSubscricao[0][0];
				}

                MarketHelper helper = new MarketHelper();
                filter.AssessorFilter = "";//helper.GetAssessorsRangesToFilter("");

                if (!String.IsNullOrEmpty(form["ddlStatus"]))
                    filter.Status = form["ddlStatus"].ToCharArray()[0];

                var oldFilter = (SubscriptionRights)Session["Subscription.RequestRightsFilter"];
                

                Session["Subscription.RequestRightsFilter"] = filter;

                if (oldFilter != null && filter.AssessorFilter == oldFilter.AssessorFilter
                    && filter.RequestId == oldFilter.RequestId
                    && filter.Status == oldFilter.Status)
                {
                    var aux = (List<SubscriptionRights>)Session["Subscription.RequestRightsList"];
                    list = new WcfResponse<List<SubscriptionRights>, int>() { Result = aux, Data = aux.Count };
                }
                else
                {
                    list = subscriptionService.LoadSubscriptionRights(filter, new FilterOptions(), CommonHelper.GetLoggedUserGroupId());
                }

                Session["Subscription.RequestRightsList"] = list.Result;
			}
			return new SubscriptionHelper().RequestRightsListObject((list.Result) != null ? list.Result : new List<SubscriptionRights>(), ref form, list.Data);
		}

        public ActionResult RequestRightsHistory()
        {
            List<SelectListItem> listItems = new List<SelectListItem>();
            listItems.Add(new SelectListItem { Text = "Todas", Value = "", Selected = true });

            try
            {
                var actions = SubscriptionHelper.RequestRightsActions;
                foreach (string action in actions)
                    listItems.Add(new SelectListItem { Text = action, Value = action, Selected = false });

                ViewBag.SubscriptionActions = listItems;

                LogHelper.SaveAuditingLog("Subscription.RequestRights.History");
                return View();
            }

            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = "Não foi possível carregar o histórico de direitos.";
                return View();
            }
        }

        [HttpPost]
        public void LoadRequestRightsHistory(FormCollection form)
        {
            try
            {
                string startDate = form["txtStartDate"];
                string endDate = form["txtEndDate"];

                long responsibleID = string.IsNullOrEmpty(form["hdfResponsible"]) ? 0 : long.Parse(form["hdfResponsible"]);
                string txtSubscription = form["txtSubscription"];
                string txtClient = form["txtClient"];

                int id = 0;
                Int32.TryParse(form["hdfRequestID"], out id);

                string action = form["ddlRequestRightsActions"];

                LogHelper.SaveAuditingLog("Subscription.RequestRightsHistory",
                    string.Format(LogResources.AuditingLoadHistory, txtSubscription, responsibleID, action, startDate, endDate));

                HistoryFilter filter = new HistoryFilter
                {
                    ModuleName = "Subscription.RequestRights",
                    StartDate = startDate,
                    Action = action,
                    EndDate = endDate,
                    ResponsibleID = responsibleID,
                    TargetText = txtSubscription,
                    ExtraTargetText = txtClient,
                    ID = id
                };

                Session["Subscription.RequestRightsHistoryList"] = LogHelper.LoadHistory(filter);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }
        }

        public ActionResult Subscription_RequestRightsHistoryList(FormCollection form)
        {
            List<HistoryData> items = (List<HistoryData>)Session["Subscription.RequestRightsHistoryList"];
            return new DataTablesController().AccessControl_History(form, items);
        }

        [HttpGet]
        public ActionResult RequestRightsHistoryDetail(string requestId, string clientCode)
        {
            try
            {
                LogHelper.SaveAuditingLog("Subscription.RequestRightsHistoryDetail");

                var requestRights = ((List<SubscriptionRights>)Session["Subscription.RequestRightsList"]).Where(a => a.ClientCode == Convert.ToInt32(clientCode) && a.RequestId == Convert.ToInt32(requestId)).FirstOrDefault(); //this.GetSingleSubscriptionRights(Convert.ToInt32(requestId), clientCode + ";");

                HistoryFilter filter = new HistoryFilter { ModuleName = "Subscription.RequestRights", ID = requestRights.RequestId, TargetID = Convert.ToInt32(clientCode) };

                var data = LogHelper.LoadHistory(filter);

                Session["Subscription.RequestRightsHistoryListDetail"] = data;
                ViewBag.RequestRightsHistoryDetails = data;

                var subscription = this.GetSingleSubscription(Convert.ToInt32(requestId));
                ViewBag.Subscription = subscription.Company;

                return View(requestRights);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = "Não foi possível carregar o histórico deste termo.";
                return View(new SubscriptionRights());
            }
        }

        private SubscriptionRights GetSingleSubscriptionRights(int requestId, string clientCode)
        {
            SubscriptionRights sub = null;
            if (requestId != 0)
            {
                using (var service = SubscriptionService.GetChannel().CreateChannel())
                {
                    service.Open();
                    sub = service.LoadSubscriptionRights(new SubscriptionRights() { ClientCodeFilter = clientCode, RequestId = requestId }, new FilterOptions() { AllRecords = true, SortColumn = 1, SortDirection = "asc" }, CommonHelper.GetLoggedUserGroupId()).Result.ToList().FirstOrDefault();

                    service.Close();
                }

            }
            return sub;
        }

        public ActionResult RightRequestedConfirmDelete(int requestId, int clientId, string status)
        {
            ViewBag.Subscription = this.GetSingleSubscription(requestId);
            //string ids = new MarketHelper().GetClientsRangesToFilter(clientId.ToString());
            if (requestId != 0 && clientId > 0)
            {
                string clientIds = new MarketHelper().GetClientsRangesToFilter(clientId.ToString());
                var result = this.GetRightRequest(requestId, clientIds, String.IsNullOrEmpty(status) ? null : status, true);
                return View(result != null ? result.FirstOrDefault() : null);
            }
            else
                return View();
            
        }

        [HttpPost]
        public ActionResult RightRequestedDelete(FormCollection form)
        {
            var request = new SubscriptionRights()
            {
                RequestId = Convert.ToInt32(form["hdfRequestId"]),
                ClientCode = Convert.ToInt32(form["hdfClientCode"])
            };

            WcfResponse<bool> response = null;

            using (var service = SubscriptionService.GetChannel().CreateChannel())
            {
                service.Open();

                response = service.SubscriptionRightsDelete(request);

                if (response.Result)
                {
                    Session["Subscription.RequestRightsFilter"] = null;

                    IChannelProvider<QX3.Portal.WebSite.NonResidentTradersService.INonResidentTradersServiceContractChannel> channelProvider =
                        ChannelProviderFactory<QX3.Portal.WebSite.NonResidentTradersService.INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
                    var nonresidentService = channelProvider.GetChannel().CreateChannel();

                    nonresidentService.Open();

                    LogHelper.SaveHistoryLog("Subscription.RequestRights",
                                new HistoryData
                                {
                                    Action = "Excluir",
                                    NewData = JsonConvert.SerializeObject(request),
                                    ID = request.RequestId,
                                    TargetID = request.ClientCode,
                                    Target = GetSingleSubscription(request.RequestId).ISINSubscription,
                                    ResponsibleID = Convert.ToInt32(SessionHelper.CurrentUser.ID),
                                    ExtraTarget = nonresidentService.LoadCustomerName(request.ClientCode).Result
                                });

                    nonresidentService.Close();
                }
            }

            return Json(new WcfResponse<bool> { Result = response.Result, Message = response.Result ? "Solicitação excluida com sucesso." : "Solicitação não excluida" });
        }

        [HttpPost]
        public string RequestRightsFilter(FormCollection form)
		{
			#region Carregar subscrições

			WcfResponse<List<Subscription>, int> list1 = new WcfResponse<List<Subscription>, int>();
			IChannelProvider<ISubscriptionContractChannel> subscriptionProvider = ChannelProviderFactory<ISubscriptionContractChannel>.Create("SubscriptionClassName");
			bool FlagChangeFilterSubscription = false;

			string oQueVeio = (form["txtStockCode"] + form["txtInitialDate"] + form["txtBrokerDate"]);
			string oQueExiste = (string)Session["Subscription.FlagChangeFilterSubscription"];

			if (!oQueVeio.Equals(oQueExiste))
			{
				using (var subscriptionService = subscriptionProvider.GetChannel().CreateChannel())
				{
					Subscription filter = new Subscription();

					if (!string.IsNullOrEmpty(form["txtStockCode"]))
						filter.ISINStock = form["txtStockCode"].ToUpper();

					if (!string.IsNullOrEmpty(form["txtInitialDate"]))
						filter.InitialDate = Convert.ToDateTime(form["txtInitialDate"]);

					if (!string.IsNullOrEmpty(form["txtBrokerDate"]))
						filter.BrokerDate = Convert.ToDateTime(form["txtBrokerDate"]);

					FilterOptions options = new FilterOptions()
					{
						InitRow = 0,
						FinalRow = 0,
						SortColumn = 1,
						SortDirection = string.Empty,
						AllRecords = true
					};

					list1 = subscriptionService.LoadSubscriptions(filter, options, false);

					form["hPosicao"] = "0";
					Session["Subscription.RequestRightsSubscriptionsList"] = list1.Result;
					ListaIdsSubscricao = list1.Result == null ? (new List<int[]>()).ToArray() : list1.Result.Select(p => new int[] { p.RequestId }).ToArray();

					Session["Subscription.FlagChangeFilterSubscription"] = oQueVeio;
					FlagChangeFilterSubscription = true;
				}
			}

			#endregion

			#region Carregar direitos

			WcfResponse<List<SubscriptionRights>, int> list = new WcfResponse<List<SubscriptionRights>, int>();

			if (ListaIdsSubscricao != null && ListaIdsSubscricao.Length > 0)
			{
				using (var subscriptionService = subscriptionProvider.GetChannel().CreateChannel())
				{
					MarketHelper helper = new MarketHelper();
					var filter = new SubscriptionRights();
					filter.AssessorFilter = (String.IsNullOrEmpty(form["txtAssessor"])) ? "" : helper.GetAssessorsRangesToFilter(form["txtAssessor"]);

					if (!string.IsNullOrEmpty(form["hPosicao"]) && ListaIdsSubscricao != null)
						filter.RequestId = ListaIdsSubscricao[Convert.ToInt16(form["hPosicao"])][0];

					if (!string.IsNullOrEmpty(form["txtClient"]))
						filter.ClientCodeFilter = helper.GetClientsRangesToFilter(form["txtClient"]);

					if (!string.IsNullOrEmpty(form["txtStockCode"]))
						filter.StockCode = form["txtStockCode"].ToUpper();

					if (!string.IsNullOrEmpty(form["ddlStatus"]))
						filter.Status = form["ddlStatus"].ToCharArray()[0];

					if (!string.IsNullOrEmpty(form["ddlWarning"]))
						filter.Warning = form["ddlWarning"].ToCharArray()[0];

					if (!string.IsNullOrEmpty(form["txtInitialDate"]))
						filter.InitialDate = Convert.ToDateTime(form["txtInitialDate"]);
					if (!string.IsNullOrEmpty(form["txtBrokerDate"]))
						filter.FinalDate = Convert.ToDateTime(form["txtBrokerDate"]);

                    var oldFilter = (SubscriptionRights)Session["Subscription.RequestRightsFilter"];

                    Session["Subscription.RequestRightsFilter"] = filter;

                    if (oldFilter != null
                        && filter.AssessorFilter == oldFilter.AssessorFilter
                        && filter.RequestId == oldFilter.RequestId
                        && filter.ClientCodeFilter == oldFilter.ClientCodeFilter
                        && filter.StockCode == oldFilter.StockCode
                        && filter.Status == oldFilter.Status
                        && filter.Warning == oldFilter.Warning
                        && filter.InitialDate == oldFilter.InitialDate
                        && filter.FinalDate == oldFilter.FinalDate
                        )

                    {
                        var aux = (List<SubscriptionRights>)Session["Subscription.RequestRightsList"];
                        list = new WcfResponse<List<SubscriptionRights>, int>() { Result = aux, Data = aux.Count };
                    }
                    else
                    {
                        list = subscriptionService.LoadSubscriptionRights(filter, new FilterOptions(), CommonHelper.GetLoggedUserGroupId());
                    }
				}
			}

			Session["Subscription.RequestRightsList"] = list.Result;

			#endregion

			return new SubscriptionHelper().RequestRightsListObject((list.Result) != null ? list.Result : new List<SubscriptionRights>(), ref form, list.Data, ListaIdsSubscricao != null ? ListaIdsSubscricao.Length : 0, FlagChangeFilterSubscription);
		}

		[HttpGet]
		public ActionResult RequestRightsToFile(bool isPdf, int col, string dir, int pos)
		{
			List<SubscriptionRights> currentList = new List<SubscriptionRights>();
			Subscription subscription = new Subscription();
			List<Subscription> list = (List<Subscription>)Session["Subscription.RequestRightsSubscriptionsList"];
			try
			{
				if (Session["Subscription.RequestRightsList"] != null)
				{
					currentList = (List<SubscriptionRights>)Session["Subscription.RequestRightsList"];

					if (ListaIdsSubscricao.Length > 0)
					{
						subscription = list.Where(p => p.RequestId == ListaIdsSubscricao[pos][0]).First();
					}

					#region Ordenação com LINQ

					switch (Convert.ToInt16(col))
					{
						case 1: // nome cliente
							if (dir.Equals("desc"))
								currentList = currentList.OrderByDescending(p => p.ClientName).ToList();
							else
								currentList = currentList.OrderBy(p => p.ClientName).ToList();

							break;
						case 2: // cod cliente
							if (dir.Equals("desc"))
								currentList = currentList.OrderByDescending(p => p.ClientCode).ToList();
							else
								currentList = currentList.OrderBy(p => p.ClientCode).ToList();

							break;
						case 3: // nome assessor
							if (dir.Equals("desc"))
								currentList = currentList.OrderByDescending(p => p.AssessorName).ToList();
							else
								currentList = currentList.OrderBy(p => p.AssessorName).ToList();

							break;
						case 4: // qtde direito
							if (dir.Equals("desc"))
								currentList = currentList.OrderByDescending(p => p.RightsQtty).ToList();
							else
								currentList = currentList.OrderBy(p => p.RightsQtty).ToList();

							break;
						case 5: // solicitado
							if (dir.Equals("desc"))
								currentList = currentList.OrderByDescending(p => p.RequestedQtty).ToList();
							else
								currentList = currentList.OrderBy(p => p.RequestedQtty).ToList();

							break;
						case 6: // disponivel
							if (dir.Equals("desc"))
								currentList = currentList.OrderByDescending(p => p.AvailableQtty).ToList();
							else
								currentList = currentList.OrderBy(p => p.AvailableQtty).ToList();

							break;
						case 7: // preço unitário
							if (dir.Equals("desc"))
								currentList = currentList.OrderByDescending(p => p.RightValue).ToList();
							else
								currentList = currentList.OrderBy(p => p.RightValue).ToList();

							break;
						case 8: // preço total
							if (dir.Equals("desc"))
								currentList = currentList.OrderByDescending(p => p.TotalValue).ToList();
							else
								currentList = currentList.OrderBy(p => p.TotalValue).ToList();

							break;
						case 9: // data limite
							if (dir.Equals("desc"))
								currentList = currentList.OrderByDescending(p => p.StockExchangeDate).ToList();
							else
								currentList = currentList.OrderBy(p => p.StockExchangeDate).ToList();

							break;
						case 10: // sobras
							if (dir.Equals("desc"))
								currentList = currentList.OrderByDescending(p => p.DescScraps).ToList();
							else
								currentList = currentList.OrderBy(p => p.DescScraps).ToList();

							break;
						case 11: // notificação
							if (dir.Equals("desc"))
								currentList = currentList.OrderByDescending(p => p.DescWarning).ToList();
							else
								currentList = currentList.OrderBy(p => p.DescWarning).ToList();

							break;
						case 12: // situação
							if (dir.Equals("desc"))
								currentList = currentList.OrderByDescending(p => p.DescStatus).ToList();
							else
								currentList = currentList.OrderBy(p => p.DescStatus).ToList();

							break;
						default:
							break;
					}

					#endregion				
				}

				//LogHelper.SaveAuditingLog("Subscription.RequestRightsToFile", string.Format(LogResources.ObjectToFileParameters, isPdf ? "PDF" : "Excel", JsonConvert.SerializeObject(currentList)));

				string fileName = string.Format("solicitacao_direito_{0}.{1}", DateTime.Now.ToString("dd-MM-yyyy"), isPdf ? "pdf" : "xls");

				LogHelper.SaveHistoryLog("Subscription.RequestRights", new HistoryData { Action = "Exportar", OldData = string.Empty, NewData = JsonConvert.SerializeObject(currentList) });

				return isPdf ? ViewRequestRightsPdf(subscription, currentList, true, fileName) : ViewRequestRightsExcel(subscription, currentList, true, fileName);
			}
			catch (Exception ex)
			{
				LogManager.WriteError(ex);
				TempData["ErrorMessage"] = "Ocorreu um erro ao gerar relatório.";
				return this.RedirectToAction("RequestRights");
			}
		}

        public ActionResult RequestRightsEffect(string requestsIds)
        {
            try
            {
                string[] arrRequestsIds = requestsIds.Split(';');
                var lstObj = (List<SubscriptionRights>)Session["Subscription.RequestRightsList"];

                List<SubscriptionRights> lstToEffect = new List<SubscriptionRights>();

                foreach (string i in arrRequestsIds)
                    if (!string.IsNullOrEmpty(i))
                    {
                        SubscriptionRights obj = (from s in lstObj where s.ClientCode.Equals(Convert.ToInt32(i)) && s.Status.Value.Equals('P') select s).FirstOrDefault();
                        if (obj != null)
                            lstToEffect.Add(obj);
                    }

                Session["Subscription.ListToEffect"] = lstToEffect;
                ViewBag.RequestRightsToEffect = lstToEffect;
            }
            catch (Exception)
            {

                ViewBag.RequestRightsToEffect = null;
            }

            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult RequestRightsEffect()
        {
            try
            {
                LogHelper.SaveAuditingLog("Subscription.RequestRightsEffect");

                IChannelProvider<ISubscriptionContractChannel> channelProvider = ChannelProviderFactory<ISubscriptionContractChannel>.Create("SubscriptionClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                var lstToEffect = (List<SubscriptionRights>)Session["Subscription.ListToEffect"];

                if (lstToEffect != null && lstToEffect.Count > 0)
                {
                    List<SubscriptionRights> lstError = new List<SubscriptionRights>();

                    service.Open();

                    foreach (SubscriptionRights obj in lstToEffect)
                    {
                        SubscriptionRights temp = new SubscriptionRights();
                        temp.ClientCode = obj.ClientCode;
                        temp.RequestId = obj.RequestId;
                        temp.Status = 'E';
                        obj.Status = 'E';

                        var result = service.SubscriptionRightsUpdate(temp).Result;
                        if (result)
                            LogHelper.SaveHistoryLog("Subscription.RequestRights", new HistoryData
                            { 
                                Action = "Efetuar",
                                ID = obj.RequestId,
                                Target = obj.ClientCode.ToString(), 
                                TargetID = obj.ClientCode,
                                NewData = JsonConvert.SerializeObject(obj) 
                            });
                        else
                            lstError.Add(obj);
                    }

                    service.Close();

                    Session["Subscription.RequestRightsFilter"] = null;

                    return Json(new { Error = lstError.Count > 0 ? "Algumas solicitações não foram efetuadas." : "" });
                }

                return Json(new { Error = "Nenhuma solicitação selecionada." });
            }
            catch (Exception)
            {
                return Json(new { Error = "Erro na atualização." });
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ChangeRequestRightsStatus(string requestId, string clientCode, string status)
        {
            try
            {
                LogHelper.SaveAuditingLog("Subscription.ChangeRequestRightsStatus");

                IChannelProvider<ISubscriptionContractChannel> channelProvider = ChannelProviderFactory<ISubscriptionContractChannel>.Create("SubscriptionClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                SubscriptionRights obj = new SubscriptionRights();
                obj.RequestId = Convert.ToInt32(requestId);
                obj.ClientCode = Convert.ToInt32(clientCode);
                obj.Status = status.ToCharArray()[0];

                var insert = service.SubscriptionRightsUpdate(obj);

                service.Close();

                if (insert.Result)
                {
                    LogHelper.SaveHistoryLog("Subscription.RequestRights", new HistoryData
                    {
                        Action = "Efetuar",
                        ID = Convert.ToInt32(requestId),
                        TargetID = Convert.ToInt32(clientCode),
						Target = GetSingleSubscription(Convert.ToInt32(requestId)).ISINSubscription,
                        NewData = JsonConvert.SerializeObject(obj),
						ResponsibleID = Convert.ToInt32(SessionHelper.CurrentUser.ID),
						ExtraTarget = CommonHelper.GetClientName(Convert.ToInt32(clientCode))
                    });
                }

                Session["Subscription.RequestRightsFilter"] = null;

                return Json(new { Error = insert.Result ? "" : "Erro na atualização." });
            }
            catch (Exception)
            {
                return Json(new { Error = "Erro na atualização." });
            }
        }

		[HttpPost]
		public string RequestRightsSubscriptionHeader(int posicao)
		{
			string resultCode = string.Empty;
            if (ListaIdsSubscricao != null && ListaIdsSubscricao.Length > 0)
			{
				List<Subscription> list = (List<Subscription>)Session["Subscription.RequestRightsSubscriptionsList"];

				Subscription subscription = list.Where(p => p.RequestId == ListaIdsSubscricao[posicao][0]).First();

				resultCode += "<input type='hidden' value='" + subscription.RequestId + "' id='hdfCurrentSubscription' name='hdfCurrentSubscription' />";
                resultCode += "<input type='hidden' value='" + subscription.Status + "' id='hdfCurrentStatus' name='hdfCurrentStatus' />";
				resultCode += "<div class=\"cabecalho-grid\"><div class=\"etiqueta\">";
				resultCode += "<h2>" + subscription.Company + " - " + subscription.StockCode + " (" + subscription.RightPercentual.Value.ToString("N2") + "%)</h2>";
				resultCode += "<span class=\"quebrar\"></span>";
				resultCode += "<div class=\"esquerda\"><div class=\"bloco\"><span>" + subscription.ISINStock + "</span></div></div>";
				resultCode += "<div class=\"direita\"><div class=\"bloco\"><span>" + subscription.InitialDate.Value.ToShortDateString() + " até " + subscription.StockExchangeDate.Value.ToShortDateString() + "</span></div></div>";
				resultCode += "<span class=\"quebrar\"></span></div><span class=\"quebrar\"></span></div>";
			}
			return resultCode;
		}

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ChangeNotification(string requestId, string status, string clientCode)
        {
            try
            {
                LogHelper.SaveAuditingLog("Subscription.ChangeNotification");

                IChannelProvider<ISubscriptionContractChannel> channelProvider = ChannelProviderFactory<ISubscriptionContractChannel>.Create("SubscriptionClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                /*var right = service.LoadSubscriptionRights(
                    new SubscriptionRights()
                    {
                        ClientCodeFilter = new MarketHelper().GetClientsRangesToFilter(clientCode),
                        RequestId = Convert.ToInt32(requestId)
                    }
                    , new FilterOptions { AllRecords = true, SortColumn = 1, SortDirection = "asc" }, CommonHelper.GetLoggedUserGroupId());*/

                SubscriptionRights obj = new SubscriptionRights();
                obj.RequestId = Convert.ToInt32(requestId);
                obj.Warning = status[0];
                obj.ClientCode = Convert.ToInt32(clientCode);

                var insert = service.SubscriptionRightsUpdate(obj);

                service.Close();

                if (insert.Result)
                {
                    Session["Subscription.RequestRightsFilter"] = null;

                    string action = "Notificar";

                    LogHelper.SaveHistoryLog("Subscription.RequestRights",
                        new HistoryData
                        {
                            Action = action,
                            NewData = JsonConvert.SerializeObject(obj),
                            ID = Convert.ToInt32(requestId),
                            TargetID = Convert.ToInt32(clientCode),
                            ResponsibleID = Convert.ToInt32(SessionHelper.CurrentUser.ID),
							Target = GetSingleSubscription(Convert.ToInt32(requestId)).ISINSubscription,
							ExtraTarget = CommonHelper.GetClientName(Convert.ToInt32(clientCode))
                        });
                }

                return Json(new { Error = insert.Result ? "" : "Erro na atualização." });
            }
            catch (Exception)
            {
                return Json(new { Error = "Erro na atualização." });
            }
        }

        public ActionResult RequestRightsNotify(string requestsIds)
        {
            try
            {
                string[] arrRequestsIds = requestsIds.Split(';');
                var lstObj = (List<SubscriptionRights>)Session["Subscription.RequestRightsList"];

                List<SubscriptionRights> lstToNotify = new List<SubscriptionRights>();

                foreach (string i in arrRequestsIds)
                    if (!string.IsNullOrEmpty(i))
                    {
                        SubscriptionRights obj = (from s in lstObj where s.ClientCode.Equals(Convert.ToInt32(i)) select s).FirstOrDefault();
                        if (obj != null)
                            lstToNotify.Add(obj);
                    }

                List<SubscriptionRights> lstToConfirm = (from s in lstToNotify where s.DescWarning.Equals("Email") select s).ToList();

                Session["Subscription.ListToNotify"] = lstToNotify;
                Session["Subscription.ListToConfirm"] = lstToConfirm;
                ViewBag.NotifyToConfirm = lstToConfirm;
            }
            catch (Exception)
            {

                ViewBag.NotifyToConfirm = null;
            }

            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult RequestRightsSendNotification(string notifyAll)
        {
            try
            {
                LogHelper.SaveAuditingLog("Subscription.RequestRightsNotify");

                IChannelProvider<ISubscriptionContractChannel> channelProvider = ChannelProviderFactory<ISubscriptionContractChannel>.Create("SubscriptionClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                var lstSubscription = (List<Subscription>)Session["Subscription.RequestRightsSubscriptionsList"];

                var lstToNotify = (List<SubscriptionRights>)Session["Subscription.ListToNotify"];
                if (lstToNotify != null && lstToNotify.Count > 0)
                {
                    Subscription currentSubscription = null;

                    var lstError = new List<SubscriptionRights>();
                    var lstNoEmail = new List<SubscriptionRights>();

                    if (!string.IsNullOrEmpty(notifyAll) && notifyAll.Equals("0")) 
                    {
                        var lstToNotNotify = (List<SubscriptionRights>)Session["Subscription.ListToConfirm"];
                        if (lstToNotNotify != null && lstToNotNotify.Count > 0)
                        {
                            lstToNotify = (from c in lstToNotify
                                           where !(from o in lstToNotNotify select o.ClientCode).Contains(c.ClientCode)
                                           select c).ToList();
                        }
                    }

                    service.Open();

                    foreach (SubscriptionRights obj in lstToNotify)
                    {
                        if (currentSubscription == null)
                            currentSubscription = (from s in lstSubscription where s.RequestId.Equals(obj.RequestId) select s).FirstOrDefault();

                        obj.Warning = 'E';

                        if (obj.ClientEmail != null)
                        {
                            var result = service.RequestRightsNotify(obj, currentSubscription).Result;
                            if (result)
                            {
                                LogHelper.SaveHistoryLog("Subscription.RequestRights", new HistoryData
                                {
                                    Action = "Notificar",
                                    ID = obj.RequestId,
                                    TargetID = obj.ClientCode,
                                    NewData = JsonConvert.SerializeObject(obj),
                                    ResponsibleID = Convert.ToInt32(SessionHelper.CurrentUser.ID),
									Target = GetSingleSubscription(obj.RequestId).ISINSubscription,
									ExtraTarget = obj.ClientName
                                });

                                Session["Subscription.RequestRightsFilter"] = null;
                            }
                            else
                                lstError.Add(obj);
                        }
                        else
                        {
                            lstNoEmail.Add(obj);
                        }
                    }

                    var serviceError = lstError.Count > 0 ? "Algumas solicitações não foram notificadas. " : string.Empty;
                    var noEmail = lstNoEmail.Count > 0 ? "Alguns clientes não possuem email cadastrado." : string.Empty;

                    var error = serviceError + noEmail;

                    service.Close();

                    return Json(new { Error = error });
                }

                return Json(new { Error = "Nenhuma solicitação selecionada." });
            }
            catch (Exception)
            {
                return Json(new { Error = "Erro na notificação." });
            }
        }

        public ActionResult RequestRightsHistoryToFile(bool isPdf, bool detail)
        {
            List<HistoryData> currentList = new List<HistoryData>();

            string sessionName = detail ? "Subscription.RequestRightsHistoryListDetail" : "Subscription.RequestRightsHistoryList";

            try
            {
                if (Session[sessionName] != null)
                    currentList = (List<HistoryData>)Session[sessionName];

                //LogHelper.SaveAuditingLog(string.Concat("Subscription.RequestRights.HistoryToFile"), string.Format(LogResources.ObjectToFileParameters, isPdf ? "PDF" : "Excel", JsonConvert.SerializeObject(currentList)));

                string targetTitle = string.Concat(isPdf ? "Histórico de subscrições" : "Hist&#243;rico de subscri&#231;&#245;es");

                string fileName = string.Format("historico_{0}_{1}.{2}", "subscricao", DateTime.Now.ToString("dd-MM-yyyy"), isPdf ? "pdf" : "xls");

                return isPdf
                    ? ViewHistoryPdf(currentList, true, fileName, targetTitle, "Empresa", true, "Subscri&#231;&#227;o")
                    : ViewHistoryExcel(currentList, true, fileName, targetTitle, "Empresa", true, "Subscri&#231;&#227;o");

            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return this.RedirectToAction("Index");
            }
        }

		#endregion
	}
}
