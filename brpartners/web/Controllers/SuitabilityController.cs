﻿using log4net;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.WebSite.AccessControlService;
using QX3.Portal.WebSite.AuthenticationService;
using QX3.Portal.WebSite.CommonService;
using QX3.Portal.WebSite.Helper;
using QX3.Portal.WebSite.Models;
using QX3.Portal.WebSite.Models.Channel;
using QX3.Portal.WebSite.SuitabilityService;
using QX3.Portal.WebSite.Util;
using QX3.Spinnex.Common.Services.Logging;
using QX3.Spinnex.Common.Services.Mail;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Tegra.HumanResources.Domain.Representations;

namespace QX3.Portal.WebSite.Controllers
{
    public class SuitabilityController : BaseController
    {
        private ILog _log = log4net.LogManager.GetLogger(typeof(SuitabilityController));

        #region Private Methods

        private string RenderRazorViewToString(string viewName, object model)
        {
            ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);

                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);

                return sw.GetStringBuilder().ToString();
            }
        }

        private void SendSuitabilityPDF(string suitabilityId)
        {
            try
            {
                string mailTo = ConfigurationManager.AppSettings["Suitability.PDF.Mail.To"];

                if (!string.IsNullOrEmpty(mailTo))
                {
                    IChannelProvider<ISuitabilityContractChannel> channelProvider = ChannelProviderFactory<ISuitabilityContractChannel>.Create("SuitabilityClassName");
                    var service = channelProvider.GetChannel().CreateChannel();

                    service.Open();

                    var sc = service.SelecionaResultadoSuitability(suitabilityId);

                    service.Close();

                    var html = RenderRazorViewToString("~/Views/Suitability/SuitabilityResultPrint.cshtml", sc);

                    var pdfBytes = PDFConverter.HtmlToPDF(html);

                    string client = string.Format("{0} ({1})", sc.Result.Name, CommonHelper.FormatCpfCnpj(sc.Result.CPFCNPJ));
                    string body = string.Format("Segue o último questionário de Suitability preenchido pelo cliente '{0}', no sistema Portal - Gestão de Suitability.", client);
                    string subject = string.Format("Resultado do Suitability - {0}", client);

                    var mail = new SendMail
                    {
                        EmailTo = mailTo,
                        Body = body,
                        Subject = subject
                    };

                    string fileName = string.Format("Suitability_{0}_{1}.{2}", suitabilityId, DateTime.Now.ToString("ddMMyyyymmss"), "pdf");

                    Attachment attachment = new Attachment(new MemoryStream(pdfBytes), fileName, "application/pdf");
                    mail.Attachments.Add(attachment);

                    mail.Send();
                }
            }
            catch
            {
                // TODO: Log.
            }
        }

        #endregion

        [AccessControlAuthorize]
        [HttpGet]
        public ActionResult Index()
        {
            SuitabilityClientsResponse sr = new SuitabilityClientsResponse();

            var user = SessionHelper.CurrentUser;

            try
            {
                //LogHelper.SaveAuditingLog("Suitability.Suitability");

                IChannelProvider<ISuitabilityContractChannel> channelProvider = ChannelProviderFactory<ISuitabilityContractChannel>.Create("SuitabilityClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                SuitabilityUnframedResponse[] unframed = null;

                _log.Debug("Buscando dados de suitability do cliente:");
                _log.DebugFormat("\tID do Usuário: {0}", user.ID);
                _log.DebugFormat("\tLogin: {0}", user.UserName);
                _log.DebugFormat("\tID do Cliente: {0}", user.GlobalClientId);
                _log.DebugFormat("\tCNPJ: {0}", user.CpfCnpj);
                _log.DebugFormat("\tData de Suitability: {0}", user.SuitabilityDate);

                if (user.GlobalClientId.HasValue)
                {
                    //Verifica desenquadramento
                    if (user.SuitabilityDate.HasValue)
                    {
                        _log.Debug("Verificando desenquadramento...");
                        //var responseDesenq = service.VerificaDesenquadramento(user.GlobalClientId.Value, user.InvestorType, user.SuitabilityDate.Value.ToString("yyyyMMdd"), DateTime.Now.ToString("yyyyMMdd"), "1");
                        var responseDesenq = service.VerificaNovoDesenquadramento(user.GlobalClientId.Value, user.SuitabilityDate.Value, DateTime.Now);
                        _log.DebugFormat("\tMensagem: {0}", responseDesenq.Message);
                        unframed = responseDesenq.Result;
                    }

                    if (user.Profile.ID == 5)
                    {
                        _log.Debug("Buscando dados do cliente...");
                        var responseClientes = service.SelecionaClientesSuitability(null, user.CpfCnpj, null, "", "", "");
                        _log.DebugFormat("\tMensagem: {0}", responseClientes.Message);
                        sr = responseClientes.Result;
                    }
                    else
                    {
                        _log.Debug("Buscando dados do cliente...");
                        var responseClientes = service.SelecionaClientesSuitability(user.GlobalClientId, null, null, "", "", "");
                        _log.DebugFormat("\tMensagem: {0}", responseClientes.Message);
                        sr = responseClientes.Result;
                    }

                    if (unframed != null && unframed.Count() > 0)
                        sr.Unframed = "S";
                }
                else
                {
                    unframed = new SuitabilityUnframedResponse[0];
                    sr = new SuitabilityClientsResponse();
                    sr.Unframed = "N";
                }

                ViewBag.DoSuitabilityLabel = SessionHelper.CurrentUser.InvestorKind != 0 && SessionHelper.CurrentUser.PersonType == "J" ? "Responder o Suitability" : "Refazer o Suitability";

                service.Close();
            }
            catch (Exception ex)
            {
                QX3.Spinnex.Common.Services.Logging.LogManager.WriteError(ex);
                throw ex;
            }

            return View(sr);
        }

        [AccessControlAuthorize]
        [HttpGet]
        public ActionResult SuitabilityResult(string id)
        {
            SuitabilityAnswersResponse sc = new SuitabilityAnswersResponse();

            IChannelProvider<ISuitabilityContractChannel> channelProvider = ChannelProviderFactory<ISuitabilityContractChannel>.Create("SuitabilityClassName");
            var service = channelProvider.GetChannel().CreateChannel();

            service.Open();

            var result = service.SelecionaResultadoSuitability(id);

            service.Close();

            if (result.Result.GlobalClientId != SessionHelper.CurrentUser.GlobalClientId)
            {
                ViewBag.Mensagem = "Esta informação não está disponível para seu perfil.";
                return View();
            }

            return View(result);
        }

        [HttpGet]
        public ActionResult Questions()
        {
            ViewBag.Teste = false;
            int? currentClient = null;

            int? thirdParty = SessionHelper.ThirdPartySuitabilityClient;
            if (thirdParty == null)
            {
                if (!SessionHelper.CurrentUser.GlobalClientId.HasValue)
                {
                    ViewBag.Msg = "Este usuário não precisa preencher o Suitability.";
                    return View();
                }

                currentClient = SessionHelper.CurrentUser.GlobalClientId;
            }
            else
                currentClient = thirdParty;

            LogHelper.SaveAuditingLog("Suitability.Suitability");

            IChannelProvider<ISuitabilityContractChannel> channelProvider = ChannelProviderFactory<ISuitabilityContractChannel>.Create("SuitabilityClassName");
            using (var service = channelProvider.GetChannel().CreateChannel())
            {
                var res = service.Suitability(currentClient.Value);
                if (res.Message == null)
                    return View(res.Result);
                else
                {
                    ViewBag.Msg = res.Message;
                    return View();
                }
            }
        }

        [HttpGet]
        public ActionResult QuestionsByCompliance(int clientId, int id)
        {
            ViewBag.Teste = false;
            SessionHelper.ThirdPartySuitabilityClientId = id;
            SessionHelper.ThirdPartySuitabilityClient = clientId;
            int? currentClient = null;

            int? thirdParty = SessionHelper.ThirdPartySuitabilityClient;
            if (thirdParty == null)
            {
                if (!SessionHelper.CurrentUser.GlobalClientId.HasValue)
                {
                    ViewBag.Msg = "Este usuário não precisa preencher o Suitability.";
                    return View();
                }

                currentClient = SessionHelper.CurrentUser.GlobalClientId;
            }
            else
                currentClient = thirdParty;

            LogHelper.SaveAuditingLog("Suitability.Suitability");

            IChannelProvider<ISuitabilityContractChannel> channelProvider = ChannelProviderFactory<ISuitabilityContractChannel>.Create("SuitabilityClassName");
            using (var service = channelProvider.GetChannel().CreateChannel())
            {
                var res = service.Suitability(currentClient.Value);
                if (res.Message == null)
                    return View("Questions", res.Result);
                else
                {
                    ViewBag.Msg = res.Message;
                    return View("Questions");
                }
            }
        }

        [HttpGet]
        public ActionResult Testar(int id)
        {
            ViewBag.Teste = true;

            IChannelProvider<ISuitabilityContractChannel> channelProvider = ChannelProviderFactory<ISuitabilityContractChannel>.Create("SuitabilityClassName");
            using (var service = channelProvider.GetChannel().CreateChannel())
            {
                var res = service.TestarSuitability(id);
                if (res.Message == null)
                    return View("Questions", res.Result);
                else
                {
                    ViewBag.Msg = res.Message;
                    return View("Questions");
                }
            }
        }

        [AccessControlAuthorize]
        [HttpPost]
        public ActionResult Questions(SuitabilityInput input)
        {
            int? currentClient = null;

            int? thirdParty = SessionHelper.ThirdPartySuitabilityClient;
            if (thirdParty == null)
            {
                if (!SessionHelper.CurrentUser.GlobalClientId.HasValue)
                {
                    string message = "Este usuário não precisa preencher o Suitability.";
                    return Json(new { ok = false, message = message });
                }
                currentClient = SessionHelper.CurrentUser.GlobalClientId;
            }
            else
                currentClient = thirdParty;

            int count = 0;
            decimal pontos = 0;
            int perfil = 0;
            int status = 2;

            SuitabilityForm allQuestions = null;

            IChannelProvider<ISuitabilityContractChannel> channelProvider = ChannelProviderFactory<ISuitabilityContractChannel>.Create("SuitabilityClassName");
            using (var service = channelProvider.GetChannel().CreateChannel())
            {
                var formResponse = service.Suitability(currentClient.Value);
                if (formResponse.Message == null)
                    allQuestions = formResponse.Result;
                else
                {
                    string message = formResponse.Message;
                    return Json(new { ok = false, message = message });
                }

                foreach (var item in input.Items.ToLookup(i => i.QuestionId))
                {
                    var questionId = item.Key;
                    foreach (var userAnswer in item)
                    {
                        var question = allQuestions.Questions.Find(q => q.Id == questionId);
                        if (question == null)
                        {
                            string message = "Questão não encontrada: " + questionId;
                            return Json(new { ok = false, message = message });
                        }

                        var answer = question.Answers.Find(a => a.IdAnswer == userAnswer.AnswerId);
                        if (answer == null)
                        {
                            string message = "Resposta não encontrada: " + questionId + " " + userAnswer.AnswerId;
                            return Json(new { ok = false, message = message });
                        }

                        decimal localScore = question.Weight * answer.Point;
                        if (userAnswer.IsCorrect)
                            pontos += localScore;
                    }
                }

                var profile = allQuestions.Scores.Find(score => (pontos >= score.Min) && (pontos <= score.Max));
                if (profile == null)
                {
                    string message = "Pontuação fora do range cadastrado: " + pontos;
                    return Json(new { ok = false, message = message });
                }

                perfil = profile.TypeId;

                LogHelper.SaveAuditingLog("Suitability.Suitability");

                #region Log

                _log.Info("Gravando suitability....");
                _log.InfoFormat("\tPreenchido por {0}", SessionHelper.CurrentUser.UserName);
                _log.InfoFormat("\tCliente = {0}", currentClient.Value);

                foreach (var item in input.Items.ToLookup(i => i.QuestionId))
                {
                    var q = allQuestions.Questions.Where(a => a.Id == item.First().QuestionId).Single();

                    _log.InfoFormat("\t\tQ: {0}) {1}", q.IdQuestion, q.Question);

                    foreach (var a in item)
                    {
                        var answer = q.Answers.Where(asw => asw.IdAnswer == a.AnswerId).Single();

                        switch (q.QuestionType)
                        {
                            case QuestionType.Single:
                            case QuestionType.Multiple:
                                _log.InfoFormat("\t\t\tR: {0}", answer.Answer);
                                break;
                            case QuestionType.Distribution:
                                _log.InfoFormat("\t\t\tR: {0}% {1}", a.Percentage, answer.Answer);
                                break;
                            case QuestionType.Matrix:

                                if (a.MatrixData.Where(x => x.IsChecked).Count() == q.MatrixColumns.Count)
                                    _log.InfoFormat("\t\t\tR: {0}", answer.Answer);

                                break;
                        }
                    }

                    if (q.QuestionType == QuestionType.Distribution)
                    {

                    }
                }

                _log.InfoFormat("\tPerfil = {0}", profile.Description);

                #endregion

                #region Send suitability result

                //string recipient = SessionHelper.CurrentUser.Email;

                //if (thirdParty != null)
                //{
                //    int? clientId = SessionHelper.ThirdPartySuitabilityClientId;

                //    IChannelProvider<IAccessControlContractChannel> channelProviderAC = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
                //    var serviceAC = channelProviderAC.GetChannel().CreateChannel();

                //    serviceAC.Open();

                //    var user = serviceAC.LoadUser(clientId.Value).Result;
                //    recipient = user.Email;

                //    serviceAC.Close();
                //}

                //bool sendMail = true;

                //if (recipient == SessionHelper.CurrentUser.Email)
                //    sendMail = SessionHelper.CurrentUser.SendSuitabilityEmail;

                //if (sendMail)
                //{
                //    SendEmail email = new SendEmail();
                //    email.Subject = "Resultado do Questionário de Suitability";
                //    email.To = recipient;
                //    email.Body = input.htmlBody;

                //    Spinnex.Common.Services.Mail.SendMail sendEmail = new Spinnex.Common.Services.Mail.SendMail();
                //    sendEmail.Subject = email.Subject;
                //    sendEmail.EmailTo = email.To;
                //    sendEmail.Body = email.Body;
                //    sendEmail.IsBodyHtml = true;
                //    sendEmail.Send();
                //}

                #endregion

                var result = service.GravarSuitability(input, SessionHelper.CurrentUser.UserName, 0, currentClient.Value, status, perfil, (int)pontos);

                if (result.Result && result.Message == null)
                {
                    _log.Info("Suitability gravado com sucesso!");
                    IChannelProvider<IAuthenticationContractChannel> channelProvider2 = ChannelProviderFactory<IAuthenticationContractChannel>.Create("AuthenticationClassName");
                    var service2 = channelProvider2.GetChannel().CreateChannel();
                    service2.Open();

                    var response2 = service2.SelectUser(SessionHelper.CurrentUser.UserName.ToLower());

                    SessionHelper.CurrentUser = response2.Result;
                    SessionHelper.ThirdPartySuitabilityClient = null;
                    SessionHelper.ThirdPartySuitabilityClientId = null;

                    service2.Close();

                    SendSuitabilityPDF(result.Data.ToString());

                    string message = "Seu teste de perfil de investidor foi finalizado com sucesso!";
                    return Json(new { ok = true, message = message });
                }
                else
                {
                    _log.Error(result.Message);
                    string message = "Ocorreu um erro na gravação do seu teste de perfil de investidor. Entre em contato com a corretora.";
                    return Json(new { ok = false, message = message, erro = result.Message });
                }
            }

        }

        public static List<SuitabilityClients> HistoricoCliente(int? idCliente, string cdCliente, string cdLogin)
        {
            List<SuitabilityClients> lsc = new List<SuitabilityClients>();

            IChannelProvider<ISuitabilityContractChannel> channelProvider = ChannelProviderFactory<ISuitabilityContractChannel>.Create("SuitabilityClassName");
            var service = channelProvider.GetChannel().CreateChannel();

            service.Open();

            lsc = service.HistoricoClientesSuitability(idCliente, cdCliente, cdLogin).Result.ToList();

            service.Close();

            return lsc;
        }
    }
}
