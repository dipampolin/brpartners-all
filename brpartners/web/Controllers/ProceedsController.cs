﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using QX3.Portal.WebSite.Helper;
using QX3.Portal.Contracts.DataContracts;
using QX3.Spinnex.Common.Services.Logging;
using QX3.Portal.WebSite.Models.Channel;
using QX3.Portal.WebSite.Properties;
using QX3.Portal.WebSite.Models;
using Newtonsoft.Json;
using QX3.Portal.WebSite.LogService;
using System.Text.RegularExpressions;
using QX3.Portal.WebSite.MarketService;

namespace QX3.Portal.WebSite.Controllers
{

    public class ProceedsController : FileController
    {
        [AccessControlAuthorize]
        public ActionResult Index()
        {
            LogHelper.SaveAuditingLog("Proceeds.Index");
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public void LoadProceeds(FormCollection form)
        {
            try
            {
                AccessControlHelper helper = new AccessControlHelper();

                ProceedsFilter filter = new ProceedsFilter();

                filter.Assessors = (!string.IsNullOrEmpty(form["txtAssessors"])) ? new MarketHelper().GetAssessorsRangesToFilter(form["txtAssessors"]) : "";//new MarketHelper().GetAssessorsRangesToFilter(form["txtAssessors"]); 

                if (!string.IsNullOrEmpty(form["txtClient"]))
                {
                    var clientsRange = helper.ConvertClientRangesToStartEndFormat(form["txtClient"]).Split(';');
                    if (clientsRange.Length > 0)
                    {
                        filter.ClientStart = clientsRange[0];
                        filter.ClientEnd = clientsRange.Length == 2 ? clientsRange[1] : "";
                    }
                    else
                        filter.ClientStart = filter.ClientEnd = "";
                }
                else
                    filter.ClientStart = filter.ClientEnd = "";
                filter.StockCode = (!string.IsNullOrEmpty(form["txtStockCode"])) ? form["txtStockCode"] : "";
                filter.CompanyName = (form["txtCompany"] != null) ? form["txtCompany"] : "";
                filter.StartDate = (!string.IsNullOrEmpty(form["txtStartDate"])) ? form["txtStartDate"] : DateTime.Now.ToString("dd/MM/yyyy");
                filter.EndDate = (!string.IsNullOrEmpty(form["txtEndDate"])) ? form["txtEndDate"] : DateTime.Now.ToString("dd/MM/yyyy");
                filter.Type = (!string.IsNullOrEmpty(form["ddlProceeds"])) ? char.Parse(form["ddlProceeds"]) : 'N';

                IChannelProvider<IMarketContractChannel> channelProvider = ChannelProviderFactory<IMarketContractChannel>.Create("MarketClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                var proceeds = service.LoadProceeds(filter, CommonHelper.GetLoggedUserGroupId()).Result;

                Session["Proceeds"] = proceeds == null ? new List<Proceeds>() : proceeds.ToList();

                LogHelper.SaveAuditingLog("Proceeds.LoadProceeds", string.Format(LogResources.AuditingLoadProceeds, JsonConvert.SerializeObject(filter), JsonConvert.SerializeObject(Session["Proceeds"])));

                service.Close();

            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }
        }

        public ActionResult ProceedsHistory()
        {
            List<SelectListItem> listItems = new List<SelectListItem>();
            listItems.Add(new SelectListItem { Text = "Todas", Value = "", Selected = true });

            try
            {
                var actions = ProceedsHelper.ProceedsActions;
                foreach (string a in actions)
                    listItems.Add(new SelectListItem { Text = a, Value = a, Selected = false });

                ViewBag.UserList = listItems;

                LogHelper.SaveAuditingLog("Proceeds.ProceedsHistory");
                return View();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = "Não foi possível carregar o histórico dos Proventos.";
                return View();
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public void LoadProceedsHistory(FormCollection form)
        {
            try
            {
                string startDate = form["txtStartDate"];
                string endDate = form["txtEndDate"];

                long responsibleID = string.IsNullOrEmpty(form["hdfResponsible"]) ? 0 : long.Parse(form["hdfResponsible"]);
                int targetID = string.IsNullOrEmpty(form["hdfTarget"]) ? 0 : Int32.Parse(form["hdfTarget"]);

                string action = form["ddlProceedsActions"];

                LogHelper.SaveAuditingLog("Proceeds.LoadProceedsHistory", string.Format(LogResources.AuditingLoadHistory, targetID, responsibleID, action, startDate, endDate));

                HistoryFilter filter = new HistoryFilter { ModuleName = "Proceeds", StartDate = startDate, Action = action, EndDate = endDate, ResponsibleID = responsibleID, TargetID = targetID };
                
                Session["ProceedsHistory"] = LogHelper.LoadHistory(filter);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }
        }
        public ActionResult ConfirmExport()
        {
            return View();
        }

        public ActionResult ExportProceedsToFile(string exportType, string groupBy)
        {
            List<Proceeds> currentList = new List<Proceeds>();
            bool exportToPdf = exportType == "PDF";
            try
            {
                if (Session["Proceeds"] != null)
                    currentList = (List<Proceeds>)Session["Proceeds"];

                //LogHelper.SaveAuditingLog("Proceeds.ProceedsToFile", string.Format(LogResources.ObjectToFileParameters, exportToPdf ? "PDF" : "Excel", JsonConvert.SerializeObject(currentList)));

                string fileName = string.Format("proventos_{0}.{1}", DateTime.Now.ToString("dd-MM-yyyy"), exportToPdf ? "pdf" : "xls");

                LogHelper.SaveHistoryLog("Proceeds", new HistoryData { Action = "Exportar", OldData = string.Empty, NewData = JsonConvert.SerializeObject(currentList) });

                return exportToPdf ? ViewProceedsPdf(currentList, true, fileName, groupBy) : ViewProceedsExcel(currentList, true, fileName, groupBy);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return this.RedirectToAction("Index");
            }


        }

        public ActionResult ProceedsHistoryToFile(bool isPdf)
        {
            return this.HistoryToFile(isPdf, "Proceeds", "ProceedsHistory", "Proventos", "Cliente Afetado", true);
        }

        public ActionResult HistoryToFile(bool isPdf, string targetName, string sessionName, string targetTitle, string targetColumnTitle, bool hasAction)
        {
            List<HistoryData> currentList = new List<HistoryData>();

            try
            {
                if (Session[sessionName] != null)
                    currentList = (List<HistoryData>)Session[sessionName];

                //LogHelper.SaveAuditingLog(string.Concat("Proceeds.HistoryToFile", targetName), string.Format(LogResources.ObjectToFileParameters, isPdf ? "PDF" : "Excel", JsonConvert.SerializeObject(currentList)));

                string name = new Regex(@"\s*").Replace(targetTitle, string.Empty);
                targetTitle = string.Concat(isPdf ? "Histórico de " : "Hist&#243;rico de ", targetTitle);

                string fileName = string.Format("historico_{0}_{1}.{2}", name, DateTime.Now.ToString("dd-MM-yyyy"), isPdf ? "pdf" : "xls");

                return isPdf ? ViewHistoryPdf(currentList, true, fileName, targetTitle, targetColumnTitle, hasAction) : ViewHistoryExcel(currentList, true, fileName, targetTitle, targetColumnTitle, hasAction);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return this.RedirectToAction("Index");
            }


        }
    }
}
