﻿using iTextSharp.text.pdf;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.WebSite.CommonService;
using QX3.Portal.WebSite.Helper;
using QX3.Portal.WebSite.Models;
using QX3.Portal.WebSite.Models.Channel;
using QX3.Spinnex.Common.Services.Logging;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;


namespace QX3.Portal.WebSite.Controllers
{
    public class HomeController : BaseController
    {

        IChannelProvider<ICommonContractChannel> cmm;
        IChannelProvider<ICommonContractChannel> CommonService
        {
            get
            {
                if (cmm == null)
                    cmm = ChannelProviderFactory<ICommonContractChannel>.Create("CommonClassName");
                return cmm;
            }
        }

        public ActionResult Index()
        {

            if (SessionHelper.CurrentUser.Profile == null)
            {
                return View();
            }

            if (Request.Path.StartsWith("/RH"))
            {
                return RedirectToRoute("RH", new { controller = "Dashboard", action = "Index" });
            }

            if (SessionHelper.CurrentUser.Profile.Name.ToLower().Contains("client"))
            {

                //if (SessionHelper.CurrentUser.SuitabilityStatus == 1 || SessionHelper.CurrentUser.SuitabilityStatus == 3)
                //{
                //    return RedirectToAction("Suitability", "Index");
                //}

                return RedirectToRoute("Portal", new { controller = "Suitability", action = "Index" }); // return RedirectToRoute("Portal", new { controller = "Demonstrative", action = "Extract" });
            }
            else
            {
                return View();
            }

            //List<WarningList> list = null;
            //using (var service = CommonService.GetChannel().CreateChannel()) 
            //{
            //    var auxList = service.GetWarningList(SessionHelper.CurrentUser, null, "", null, CommonHelper.GetLoggedUserGroupId()).Result;
            //    list = auxList != null ? auxList.ToList() : new List<WarningList>();
            //}

            //return this.View(list);
        }


        public ActionResult Feedback()
        {
            return this.View();
        }

        [HttpPost]
        public int GetPending(string module)
        {
            int pending = 0;

            try
            {
                IChannelProvider<ICommonContractChannel> commonProvider = ChannelProviderFactory<ICommonContractChannel>.Create("CommonClassName");
                var commonService = commonProvider.GetChannel().CreateChannel();

                commonService.Open();
                pending = commonService.GetWarningList(SessionHelper.CurrentUser, module, "", null, CommonHelper.GetLoggedUserGroupId()).Result.Sum(a => a.Count);
                commonService.Close();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }
            return pending;
        }

    }
}