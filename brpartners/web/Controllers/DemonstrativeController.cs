﻿using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.WebSite.DemonstrativeService;
using QX3.Portal.WebSite.Helper;
using QX3.Portal.WebSite.Models.Channel;
using Rotativa;
using Rotativa.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace QX3.Portal.WebSite.Controllers
{
    public class DemonstrativeController : FileController
    {
        #region EXTRATO_CONTABIL NDF / SWP


        [HttpGet]
        [AccessControlAuthorize]
        public ActionResult ExtractFunction()
        {
            var list = new List<ExtractFunction>();
            ViewBag.Initial = true;

            return View(list);
        }

        [HttpPost]
        [AccessControlAuthorize]
        public ActionResult ExtractFunction(FormCollection form)
        {
            var CodEmpresa = form["codemp"];
            var DtRefDe = form["dtInicial"];
            var DtRefAte = form["dtFinal"];
            var user = SessionHelper.CurrentUser;
            var CpfCnpj = user.CpfCnpj.ToString().Length <= 11 ? FormatCPF(user.CpfCnpj.ToString()) : FormatCNPJ(user.CpfCnpj.ToString());
            var UserName = user.UserName;

            IChannelProvider<IDemonstrativeContractChannel> channelProvider = ChannelProviderFactory<IDemonstrativeContractChannel>.Create("DemonstrativeClassName");
            var response = new WcfResponse<List<ExtractFunction>>();
            using (var service = channelProvider.GetChannel().CreateChannel())
            {
                response = service.ExtractFunction(CodEmpresa, DtRefDe, DtRefAte, CpfCnpj, UserName);
            }

            if (response.Result == null)
            {
                response.Result = new List<ExtractFunction>();
            }

            response.Result.ForEach(c =>
            {
                if (c.OPDTBASE != null)
                {
                    c.OPDTBASE = DateTime.Parse(c.OPDTBASE).ToShortDateString().ToString();
                }
                if (c.PADTVCTO != null)
                {
                    c.PADTVCTO = DateTime.Parse(c.PADTVCTO).ToShortDateString().ToString();
                }
            });

            return View(response.Result);
        }

        [HttpGet]
        public void ExtractToExecel(string codemp, string dtInicial, string dtFinal)
        {
            var user = SessionHelper.CurrentUser;
            var CpfCnpj = user.CpfCnpj.ToString().Length <= 11 ? FormatCPF(user.CpfCnpj.ToString()) : FormatCNPJ(user.CpfCnpj.ToString());
            var UserName = user.UserName;
            IChannelProvider<IDemonstrativeContractChannel> channelProvider = ChannelProviderFactory<IDemonstrativeContractChannel>.Create("DemonstrativeClassName");
            var response = new WcfResponse<List<ExtractFunction>>();
            using (var service = channelProvider.GetChannel().CreateChannel())
            {
                response = service.ExtractFunction(codemp, dtInicial, dtFinal, CpfCnpj, UserName);
            }


            var excel = new System.Data.DataTable("teste");
            excel.Columns.Add("DT.BASE", typeof(string));
            excel.Columns.Add("DT.VCTO", typeof(string));
            excel.Columns.Add("NR.OPER", typeof(string));
            excel.Columns.Add("PARC", typeof(string));
            excel.Columns.Add("CLIENTE", typeof(string));
            excel.Columns.Add("TX.AP%AA", typeof(string));
            excel.Columns.Add("VR.PC.BASE", typeof(string));
            excel.Columns.Add("PAVLRPR", typeof(string));
            excel.Columns.Add("MD", typeof(string));
            excel.Columns.Add("SALDO NA DATA", typeof(decimal));
            excel.Columns.Add("VALOR FINAL", typeof(decimal));
            excel.Columns.Add("MORA", typeof(string));
            excel.Columns.Add("MULTA", typeof(string));
            excel.Columns.Add("IOF", typeof(string));
            excel.Columns.Add("DESP/TAR", typeof(string));
            excel.Columns.Add("SALDO DEV", typeof(decimal));

            if (response.Result != null)
            {
                for (int row = 0; row < response.Result.Count; row++)
                {
                    DataRow newrow = excel.NewRow();
                    newrow[excel.Columns["DT.BASE"]] = response.Result[row].OPDTBASE != null ? response.Result[row].OPDTBASE.Substring(0, 10) : null;
                    newrow[excel.Columns["DT.VCTO"]] = response.Result[row].PADTVCTO != null ? response.Result[row].PADTVCTO.Substring(0, 10) : null;
                    newrow[excel.Columns["NR.OPER"]] = response.Result[row].PANROPER;
                    newrow[excel.Columns["PARC"]] = response.Result[row].OPQTDDPARC_PANRPARC;
                    newrow[excel.Columns["CLIENTE"]] = response.Result[row].CLNOMECLI;
                    newrow[excel.Columns["TX.AP%AA"]] = response.Result[row].OPQTDDPARC_PANRPARC;
                    newrow[excel.Columns["VR.PC.BASE"]] = response.Result[row].PATXJRAP;
                    newrow[excel.Columns["PAVLRPR"]] = response.Result[row].PAVLRPR;
                    newrow[excel.Columns["MD"]] = response.Result[row].OPCODMD_MDSIMB;
                    newrow[excel.Columns["SALDO NA DATA"]] = response.Result[row].SLDDATA;
                    newrow[excel.Columns["VALOR FINAL"]] = response.Result[row].VLRFINAL;
                    newrow[excel.Columns["MORA"]] = response.Result[row].SLDMORA;
                    newrow[excel.Columns["MULTA"]] = response.Result[row].SLDMULTA;
                    newrow[excel.Columns["IOF"]] = response.Result[row].SLDIOF;
                    newrow[excel.Columns["DESP/TAR"]] = response.Result[row].SLDDESPTAR;
                    newrow[excel.Columns["SALDO DEV"]] = response.Result[row].SLDDEV;

                    excel.Rows.Add(newrow);
                }
            }
            else
            {
                excel.Rows.Add(excel.NewRow());
            }


            var grid = new GridView();
            grid.DataSource = excel;
            grid.DataBind();
            grid.HeaderStyle.BackColor = Color.FromArgb(95, 136, 164);
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=ExtratoFuncao.xls");
            Response.ContentType = "application/ms-excel";

            Response.Charset = "utf-8";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            grid.RenderControl(htw);

            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }

        [HttpGet]
        public ActionResult ExportToPdf(string codemp, string dtInicial, string dtFinal)
        {
            var user = SessionHelper.CurrentUser;
            var CpfCnpj = user.CpfCnpj.ToString().Length <= 11 ? FormatCPF(user.CpfCnpj.ToString()) : FormatCNPJ(user.CpfCnpj.ToString());
            var UserName = user.UserName;
            IChannelProvider<IDemonstrativeContractChannel> channelProvider = ChannelProviderFactory<IDemonstrativeContractChannel>.Create("DemonstrativeClassName");
            var response = new WcfResponse<List<ExtractFunction>>();
            using (var service = channelProvider.GetChannel().CreateChannel())
            {
                response = service.ExtractFunction(codemp, dtInicial, dtFinal, CpfCnpj, UserName);
            }

            return new ViewAsPdf("ExtractFunctionPrint", response.Result)
            {
                PageOrientation = Rotativa.Options.Orientation.Portrait,
                PageSize = Rotativa.Options.Size.A3,
                PageMargins = new Margins(10, 3, 10, 0),
                FileName = "ExtratoFuncaoRelatorio__" + DateTime.Now + "__.pdf"
            };
        }

        [AccessControlAuthorize]
        [HttpGet]
        public ActionResult ExtractNDF()
        {

            ExtractNDFResponse en = new ExtractNDFResponse();

            var user = SessionHelper.CurrentUser;

            en.TipoUsuario = user.UserType;
            //en.CodEmpresa = null;
            //en.Cnpjcpf = null;
            //en.DtRef = null;

            try
            {

                IChannelProvider<IDemonstrativeContractChannel> channelProvider = ChannelProviderFactory<IDemonstrativeContractChannel>.Create("DemonstrativeClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                en.ExtratoNDF = service.ListExtractNDF(en.CodEmpresa, en.Cnpjcpf, 0, 0).Result.ExtratoNDF;

                Session["Demonstrative.ExtractNDF"] = en;

                service.Close();
            }
            catch
            {
                return Content(FeedbackErrorModel(string.Format("Falha ao carregar tela")));
            }

            return View(en);
        }

        [AccessControlAuthorize]
        [HttpGet]
        public ActionResult ExtractSWP()
        {

            ExtractSWPResponse en = new ExtractSWPResponse();

            var user = SessionHelper.CurrentUser;

            en.TipoUsuario = user.UserType;
            //en.CodEmpresa = null;
            //en.Cnpjcpf = null;
            //en.DtRef = null;

            try
            {

                IChannelProvider<IDemonstrativeContractChannel> channelProvider = ChannelProviderFactory<IDemonstrativeContractChannel>.Create("DemonstrativeClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                //en.ExtratoSWP = service.ListExtractSWP(en.CodEmpresa, en.Cnpjcpf, en.DtRef).Result.ExtratoSWP;

                Session["Demonstrative.ExtractSWP"] = en;

                service.Close();
            }
            catch
            {
                return Content(FeedbackErrorModel(string.Format("Falha ao carregar tela")));
            }

            return View(en);
        }

        [HttpGet]
        public ActionResult BrokerageNoteDetails(string id)
        {

            BrokerageNoteResponse bnr = new BrokerageNoteResponse();

            try
            {

                var user = SessionHelper.CurrentUser;

                string[] key = id.Split('_');
                int cdCliente = Convert.ToInt32(key[1]);

                IChannelProvider<IDemonstrativeContractChannel> channelProvider = ChannelProviderFactory<IDemonstrativeContractChannel>.Create("DemonstrativeClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                string data = key[0].Substring(0, 8);

                var nota = service.ListBrokeragenotesDetails(cdCliente, Convert.ToInt32(key[0].Substring(8)), Convert.ToDateTime(string.Format("{0}/{1}/{2}", data.Substring(0, 2), data.Substring(2, 2), data.Substring(4, 4)))).Result;



                if (nota != null)
                {

                    bnr.CdBolsa = cdCliente;
                    bnr.TipoUsuario = user.UserType;
                    bnr.Cabecalho = nota.Cabecalho;
                    bnr.Resumo = nota.Resumo;

                    Session["Demonstrative.BrokerageNoteDetails"] = bnr;

                    var dtPregao = nota.Cabecalho.DtPregao;
                    var tpNegocio = nota.Resumo.TpNegocio;

                    bnr.Corpo = service.ListBrokeragenotesNegociosDia(cdCliente, dtPregao, tpNegocio).Result.Corpo;


                }


                service.Close();

                return View(bnr);

            }
            catch (Exception ex)
            {
                throw new Exception("Nota Erro", ex);
            }
        }

        [HttpGet]
        public ActionResult BrokerageNoteDetailsPdf(string id)
        {

            BrokerageNoteResponse bnr = new BrokerageNoteResponse();

            try
            {

                var user = SessionHelper.CurrentUser;

                string[] key = id.Split('_');
                int cdCliente = Convert.ToInt32(key[1]);

                IChannelProvider<IDemonstrativeContractChannel> channelProvider = ChannelProviderFactory<IDemonstrativeContractChannel>.Create("DemonstrativeClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                string data = key[0].Substring(0, 8);

                var nota = service.ListBrokeragenotesDetails(cdCliente, Convert.ToInt32(key[0].Substring(8)), Convert.ToDateTime(string.Format("{0}/{1}/{2}", data.Substring(0, 2), data.Substring(2, 2), data.Substring(4, 4)))).Result;

                //Nota

                if (nota != null)
                {

                    bnr.CdBolsa = cdCliente;
                    bnr.TipoUsuario = user.UserType;
                    bnr.Cabecalho = nota.Cabecalho;
                    bnr.Resumo = nota.Resumo;

                    Session["Demonstrative.BrokerageNoteDetails"] = bnr;

                    var dtPregao = nota.Cabecalho.DtPregao;
                    var tpNegocio = nota.Resumo.TpNegocio;

                    bnr.Corpo = service.ListBrokeragenotesNegociosDia(cdCliente, dtPregao, tpNegocio).Result.Corpo;


                }


                service.Close();

                string filename = string.Format("nota_corretagem_{0}_{1}.pdf", cdCliente, Convert.ToInt32(key[0].Substring(8)));

                return BrokerageNotePdf(bnr, false, filename);

            }
            catch (Exception ex)
            {
                throw new Exception("Nota Erro", ex);
            }
        }

        [AccessControlAuthorize]
        [HttpPost]
        public ActionResult ExtractNDF(FormCollection form)
        {
            ExtractNDFResponse en = new ExtractNDFResponse();

            var user = SessionHelper.CurrentUser;

            en.TipoUsuario = user.UserType;
            en.CodEmpresa = "1";
            en.Cnpjcpf = form["CpfCnpj"] == string.Empty ? null : form["CpfCnpj"];
            en.Mes = int.Parse(form["dtInicial"].Split('/')[0]);
            en.Ano = int.Parse(form["dtInicial"].Split('/')[1]);

            if (user.UserType == "E")
            {
                en.Cnpjcpf = user.CpfCnpj.ToString();
            }

            try
            {

                IChannelProvider<IDemonstrativeContractChannel> channelProvider = ChannelProviderFactory<IDemonstrativeContractChannel>.Create("DemonstrativeClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                en.ExtratoNDF = service.ListExtractNDF(en.CodEmpresa, en.Cnpjcpf, en.Mes, en.Ano).Result.ExtratoNDF;

                Session["Demonstrative.ExtratoNDF"] = en;

                service.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return Content(FeedbackErrorModel(string.Format("Falha ao carregar o extrato NDF")));
            }

            return View(en);
        }

        [AccessControlAuthorize]
        [HttpPost]
        public ActionResult ExtractSWP(FormCollection form)
        {
            ExtractSWPResponse en = new ExtractSWPResponse();

            var user = SessionHelper.CurrentUser;

            en.TipoUsuario = user.UserType;
            en.CodEmpresa = "1";
            en.Cnpjcpf = form["CpfCnpj"] == string.Empty ? null : form["CpfCnpj"];
            en.Mes = int.Parse(form["dtInicial"].Split('/')[0]);
            en.Ano = int.Parse(form["dtInicial"].Split('/')[1]);

            if (user.UserType == "E")
            {
                en.Cnpjcpf = user.CpfCnpj.ToString();
            }

            try
            {

                var channelProvider = ChannelProviderFactory<IDemonstrativeContractChannel>.Create("DemonstrativeClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                en.ExtratoSWP = service.ListExtractSWP(en.CodEmpresa, en.Cnpjcpf, en.Mes, en.Ano).Result.ExtratoSWP;

                Session["Demonstrative.ExtractSWP"] = en;

                service.Close();
            }
            catch
            {
                return Content(FeedbackErrorModel(string.Format("Falha ao carregar o extrato NDF")));
            }

            return View(en);
        }

        public ActionResult ExtractNDFToFile(bool isPdf)
        {
            var currentDemonstrative = Session["Demonstrative.ExtratoNDF"] == null ? new ExtractNDFResponse() : (ExtractNDFResponse)Session["Demonstrative.ExtratoNDF"];
            var cdCliente = currentDemonstrative.Cnpjcpf;

            string fileName = string.Format("ExtractNDF_{0}_{1}.{2}", cdCliente, DateTime.Now.ToString("ddMMyyyymmss"), isPdf ? "pdf" : "xls");

            return ExtractNDFExcel(currentDemonstrative, true, fileName);
        }

        public ActionResult ExtractSWPToFile(bool isPdf)
        {
            var currentDemonstrative = Session["Demonstrative.ExtractSWP"] == null ? new ExtractSWPResponse() : (ExtractSWPResponse)Session["Demonstrative.ExtractSWP"];
            var cdCliente = currentDemonstrative.Cnpjcpf;

            string fileName = string.Format("ExtractSWP_{0}_{1}.{2}", cdCliente, DateTime.Now.ToString("ddMMyyyymmss"), isPdf ? "pdf" : "xls");

            return ExtractSWPExcel(currentDemonstrative, true, fileName);
        }

        public ActionResult ExtractSWPToFilePDF()
        {
            var currentDemonstrative = Session["Demonstrative.ExtractSWP"] == null ? new ExtractSWPResponse() : (ExtractSWPResponse)Session["Demonstrative.ExtractSWP"];
            var cdCliente = currentDemonstrative.Cnpjcpf;

            string fileName = string.Format("ExtractSWP_{0}_{1}.{2}", cdCliente, DateTime.Now.ToString("ddMMyyyymmss"), "pdf");

            return ExtractSWPPDF(currentDemonstrative, true, fileName);
        }

        public ActionResult ExtractNDFToFilePDF()
        {
            var currentDemonstrative = Session["Demonstrative.ExtratoNDF"] == null ? new ExtractNDFResponse() : (ExtractNDFResponse)Session["Demonstrative.ExtratoNDF"];
            var cdCliente = currentDemonstrative.Cnpjcpf;

            string fileName = string.Format("ExtractNDF_{0}_{1}.{2}", cdCliente, DateTime.Now.ToString("ddMMyyyymmss"), "pdf");

            return ExtractNDFPDF(currentDemonstrative, true, fileName);
        }

        //public ActionResult BrokerageNoteToFile(bool isPdf)
        //{
        //    var currentDemonstrative = Session["Demonstrative.BrokerageNote"] == null ? new BrokerageNoteResponse() : (BrokerageNoteResponse)Session["Demonstrative.BrokerageNote"];
        //    var cdCliente = currentDemonstrative.CdBolsa;

        //    string fileName = string.Format("NotaCorretagem_{0}_{1}.{2}", cdCliente, DateTime.Now.ToString("ddMMyyyymmss"), isPdf ? "pdf" : "xls");

        //    return BrokerageNoteExcel(currentDemonstrative, true, fileName);
        //    //return isPdf ? ConsolidatedPositionPdf(currentPosition, lcpd, true, fileName) : ConsolidatedPositionExcel(currentPosition, lcpd, true, fileName);
        //}

        #endregion

        #region EXTRATO RENDA

        [AccessControlAuthorize]
        [HttpGet]
        public ActionResult Extract()
        {
            ExtractRendaResponse er = new ExtractRendaResponse();

            var user = SessionHelper.CurrentUser;

            er.TipoUsuario = user.UserType;

            try
            {

                IChannelProvider<IDemonstrativeContractChannel> channelProvider = ChannelProviderFactory<IDemonstrativeContractChannel>.Create("DemonstrativeClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                //er.Extrato = service.Extract(CodEmpresa, Cnpjcpf, DtRefDe, DtRefAte).Result.Extrato;
                Session["Demonstrative.Extract"] = er;

                service.Close();
            }
            catch
            {
                return Content(FeedbackErrorModel(string.Format("Falha ao carregar o extrato renda")));
            }

            return View(er);
        }

        [AccessControlAuthorize]
        [HttpPost]
        public ActionResult Extract(FormCollection form)
        {

            ExtractRendaResponse er = new ExtractRendaResponse();

            var user = SessionHelper.CurrentUser;

            er.TipoUsuario = user.UserType;
            er.CodEmpresa = form["TpCodEmpresa"] == string.Empty ? null : form["TpCodEmpresa"];
            er.Cnpjcpf = form["CpfCnpj"] == string.Empty ? null : form["CpfCnpj"];
            er.DtRefDe = form["dtInicial"] == string.Empty ? null : form["dtInicial"];
            er.DtRefAte = form["dtFinal"] == string.Empty ? null : form["dtFinal"];

            if (user.UserType == "E")
            {
                er.Cnpjcpf = user.CpfCnpj.ToString();
            }

            try
            {

                IChannelProvider<IDemonstrativeContractChannel> channelProvider = ChannelProviderFactory<IDemonstrativeContractChannel>.Create("DemonstrativeClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                er.Extrato = service.Extract(er.CodEmpresa, er.Cnpjcpf, er.DtRefDe, er.DtRefAte).Result.Extrato;
                Session["Demonstrative.Extract"] = er;

                service.Close();
            }
            catch
            {
                return Content(FeedbackErrorModel(string.Format("Falha ao carregar o extrato renda")));
            }

            return View(er);
        }

        public ActionResult ExtractToFile(bool isPdf)
        {
            var currentDemonstrative = Session["Demonstrative.Extract"] == null ? new ExtractRendaResponse() : (ExtractRendaResponse)Session["Demonstrative.Extract"];
            var cdCliente = currentDemonstrative.Cnpjcpf;

            string fileName = string.Format("Extrato_{0}_{1}.{2}", cdCliente, DateTime.Now.ToString("ddMMyyyymmss"), isPdf ? "pdf" : "xls");

            return ExtractExcel(currentDemonstrative, true, fileName);
        }

        public ActionResult ExtractToFilePDF()
        {
            var currentDemonstrative = Session["Demonstrative.Extract"] == null ? new ExtractRendaResponse() : (ExtractRendaResponse)Session["Demonstrative.Extract"];
            var cdCliente = currentDemonstrative.Cnpjcpf;

            string fileName = string.Format("Extrato_{0}_{1}.{2}", cdCliente, DateTime.Now.ToString("ddMMyyyymmss"), "pdf");

            return ExtractPDF(currentDemonstrative, true, fileName);
        }

        #endregion

        #region PROVENTOS

        [AccessControlAuthorize]
        [HttpGet]
        public ActionResult Proceeds()
        {
            ProceedsDemonstrativeResponse pdr = new ProceedsDemonstrativeResponse();

            var user = SessionHelper.CurrentUser;
            int cdCliente = user.CdBolsa;

            pdr.CdBolsa = cdCliente;
            pdr.TipoUsuario = user.UserType;
            pdr.Tipo = "S";
            pdr.dtInicial = Convert.ToDateTime(DateTime.Now.ToString("01/01/yyyy"));
            pdr.dtFinal = Convert.ToDateTime(DateTime.Now.ToString("31/12/yyyy"));

            try
            {

                IChannelProvider<IDemonstrativeContractChannel> channelProvider = ChannelProviderFactory<IDemonstrativeContractChannel>.Create("DemonstrativeClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                pdr.Proventos = service.ListProceeds(cdCliente, pdr.Tipo, pdr.dtInicial, pdr.dtFinal).Result.Proventos;
                Session["Demonstrative.Proceeds"] = pdr;

                service.Close();
            }
            catch
            {
                return Content(FeedbackErrorModel(string.Format("Falha ao carregar o extrato do cliente {0}.", cdCliente)));
            }

            return View(pdr);
        }

        [AccessControlAuthorize]
        [HttpPost]
        public ActionResult Proceeds(FormCollection form)
        {
            ProceedsDemonstrativeResponse pdr = new ProceedsDemonstrativeResponse();

            var user = SessionHelper.CurrentUser;
            int cdCliente = user.UserType == "I" ? Convert.ToInt32(form["cdCliente"]) : user.CdBolsa;

            pdr.CdBolsa = cdCliente;
            pdr.TipoUsuario = user.UserType;
            pdr.Tipo = form["rdProv"];
            pdr.dtInicial = Convert.ToDateTime(form["dtInicial"]);
            pdr.dtFinal = Convert.ToDateTime(form["dtFinal"]);

            DateTime dtI = pdr.dtInicial;
            DateTime dtF = pdr.dtFinal;
            string prov = pdr.Tipo;

            try
            {

                IChannelProvider<IDemonstrativeContractChannel> channelProvider = ChannelProviderFactory<IDemonstrativeContractChannel>.Create("DemonstrativeClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                pdr.Proventos = service.ListProceeds(cdCliente, prov, dtI, dtF).Result.Proventos;
                Session["Demonstrative.Proceeds"] = pdr;

                service.Close();
            }
            catch
            {
                return Content(FeedbackErrorModel(string.Format("Falha ao carregar o extrato do cliente {0}.", cdCliente)));
            }

            return View(pdr);
        }

        public ActionResult ProceedsToFile(bool isPdf)
        {
            var currentDemonstrative = Session["Demonstrative.Proceeds"] == null ? new ProceedsDemonstrativeResponse() : (ProceedsDemonstrativeResponse)Session["Demonstrative.Proceeds"];
            var cdCliente = currentDemonstrative.CdBolsa;

            string fileName = string.Format("Proventos_{0}_{1}.{2}", cdCliente, DateTime.Now.ToString("ddMMyyyymmss"), isPdf ? "pdf" : "xls");

            return ProceedsExcel(currentDemonstrative, true, fileName);
            //return isPdf ? ConsolidatedPositionPdf(currentPosition, lcpd, true, fileName) : ConsolidatedPositionExcel(currentPosition, lcpd, true, fileName);
        }

        #endregion

        [AccessControlAuthorize]
        [HttpGet]
        public ActionResult TradingSummary()
        {
            TradingSummaryResponse tsr = new TradingSummaryResponse();

            var user = SessionHelper.CurrentUser;
            int cdCliente = user.CdBolsa;

            tsr.CdBolsa = cdCliente;
            tsr.TipoUsuario = user.UserType;
            tsr.Ativo = "";
            tsr.Mes = DateTime.Now.ToString("MM/yyyy");

            return View(tsr);
        }

        [AccessControlAuthorize]
        [HttpPost]
        public ActionResult TradingSummary(FormCollection form)
        {
            TradingSummaryResponse tsr = new TradingSummaryResponse();

            var user = SessionHelper.CurrentUser;
            int cdCliente = user.UserType == "I" ? Convert.ToInt32(form["cdCliente"]) : user.CdBolsa;
            DateTime dt = Convert.ToDateTime(string.Format("01/{0}", form["mesAno"]));

            tsr.CdBolsa = cdCliente;
            tsr.TipoUsuario = user.UserType;
            tsr.Ativo = form["ativo"];
            tsr.Mes = dt.ToString("MM/yyyy");


            string cdAtivo = tsr.Ativo;
            try
            {

                IChannelProvider<IDemonstrativeContractChannel> channelProvider = ChannelProviderFactory<IDemonstrativeContractChannel>.Create("DemonstrativeClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                tsr.ResumoNegociacao = service.ListTradingSummary(cdCliente, cdAtivo, dt, dt.AddMonths(1).AddDays(-1)).Result.ResumoNegociacao;

                Session["Demonstrative.TradingSummary"] = tsr;

                ViewBag.TotalCompra = (from x in tsr.ResumoNegociacao select x.TotalFinCompra).Sum().ToString("N2");
                ViewBag.TotalVenda = (from x in tsr.ResumoNegociacao select x.TotalFinVenda).Sum().ToString("N2");
                ViewBag.TotalLiq = (from x in tsr.ResumoNegociacao select x.FinanceiroLiquido).Sum().ToString("N2");

                service.Close();
            }
            catch
            {
                return Content(FeedbackErrorModel(string.Format("Falha ao carregar o extrato do cliente {0}.", cdCliente)));
            }

            return View(tsr);
        }

        public ActionResult TradingSummaryToFile(bool isPdf)
        {
            var currentDemonstrative = Session["Demonstrative.TradingSummary"] == null ? new TradingSummaryResponse() : (TradingSummaryResponse)Session["Demonstrative.TradingSummary"];
            var cdCliente = currentDemonstrative.CdBolsa;

            string fileName = string.Format("ResumoNegociacao_{0}_{1}.{2}", cdCliente, DateTime.Now.ToString("ddMMyyyymmss"), isPdf ? "pdf" : "xls");


            return TradingSummaryExcel(currentDemonstrative, true, fileName);
            //return isPdf ? ConsolidatedPositionPdf(currentPosition, lcpd, true, fileName) : ConsolidatedPositionExcel(currentPosition, lcpd, true, fileName);
        }

        private string FeedbackErrorModel(string message)
        {

            return string.Format("<div id='feedbackError' class='erro' style='display:block;'><span>{0}</span></div>", message);
        }

        public static string FormatCNPJ(string CNPJ)
        {
            return Convert.ToUInt64(CNPJ).ToString(@"00\.000\.000\/0000\-00");
        }

        public static string FormatCPF(string CPF)
        {
            return Convert.ToUInt64(CPF).ToString(@"000\.000\.000\-00");
        }

    }
}
