﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QX3.Portal.WebSite.Models;
using QX3.Portal.WebSite.Models.Channel;
using QX3.Portal.WebSite.CommonService;
using QX3.Spinnex.Common.Services.Logging;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.WebSite.Properties;
using QX3.Portal.WebSite.Helper;
using QX3.Portal.WebSite.TermService;
using Newtonsoft.Json;
using QX3.Portal.WebSite.LogService;
using System.Text.RegularExpressions;
using QX3.Portal.WebSite.MarketService;

namespace QX3.Portal.WebSite.Controllers
{
    public class TermController : FileController
    {

        #region Load

        [AccessControlAuthorize]
        public ActionResult Index()
        {
            try
            {
                MarketHelper mHelper = new MarketHelper();
                TermHelper tHelper = new TermHelper();

                LogHelper.SaveAuditingLog("Term.Index");

                IChannelProvider<ICommonContractChannel> channelProvider = ChannelProviderFactory<ICommonContractChannel>.Create("CommonClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();
                var status = service.LoadTypes("Term.Index", "status").Result;
                service.Close();

                var statusToChange = (from s in status where s.Description.ToLower() != "para liquidar" select s).ToList();
                ViewBag.Status = new SelectList(statusToChange, "value", "description");

                var filterStatus = status.ToList();
                filterStatus.Insert(0, new CommonType { Value = "-1", Description = "Em Aberto" });

                ViewBag.FilterStatus = new SelectList(filterStatus, "value", "description");

                ViewBag.Company = new SelectList(MarketHelper.CompanyFilter, "key", "value");
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }

            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public void LoadTerms(string assessors, string clients, string stockType, string stockValue, string d1, string d2, string d3, string status)
        {
            try
            {
                LogHelper.SaveAuditingLog("Term.LoadTerms");

                TermFilter filter = new TermFilter();

                MarketHelper helper = new MarketHelper();
                filter.Assessors = String.IsNullOrEmpty(assessors) ? "" : helper.GetAssessorsRangesToFilter(assessors);
                if (!string.IsNullOrEmpty(clients))
                    filter.Clients = helper.GetClientsRangesToFilter(clients);

                filter.Company = new CommonType { Value = stockType, Description = stockValue };

                filter.D1 = d1.Equals("1");
                filter.D2 = d2.Equals("1");
                filter.D3 = d3.Equals("1");
                filter.StatusID = string.IsNullOrEmpty(status) ? 0 : Convert.ToInt32(status);

                IChannelProvider<ITermContractChannel> termProvider = ChannelProviderFactory<ITermContractChannel>.Create("TermClassName");
                var service = termProvider.GetChannel().CreateChannel();

                service.Open();

                var list = service.LoadTerms(filter, CommonHelper.GetLoggedUserGroupId()).Result;

                service.Close();

                int index = 0;
                Session["Terms"] = (from t in list
                                    select new TermListItem
                                    {
                                        ID = t.ID,
                                        Client = t.Client.Description,
                                        Code = t.Client.Value,
                                        Assessor = t.Assessor.Description,
                                        Stock = t.Company.Value,
                                        Company = t.Company.Description,
                                        Contract = t.Contract,
                                        AvailableQuantity = t.AvailableQuantity,
                                        TotalQuantity = t.TotalQuantity,
                                        SettleQuantity = t.SettleQuantity,
                                        TypeId = t.Type.Value,
                                        TypeDescription = t.Type.Description,
                                        SettleDate = t.SettleDate,
                                        StatusId = t.Status.Value,
                                        StatusDescription = t.Status.Description,
                                        AssessorCode = t.Assessor.Value,
                                        Index = index++,
                                        DueDate = t.DueDate, //Este campo não aparece ná listagem, sómente na solicitação em lote.
                                        RolloverDate = t.RolloverDate
                                    }).ToList();

                LogHelper.SaveAuditingLog("Term.LoadTerms", string.Format(LogResources.AuditingLoadTerms, JsonConvert.SerializeObject(filter), JsonConvert.SerializeObject(Session["Terms"])));

            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }
        }

        #endregion

        #region Settle

        public ActionResult TermConfirmSettle(string termIds)
        {
            List<string> ids = new List<string>();
            ids = termIds.Split(';').ToList();
            return View(ids);
        }

        [HttpPost]
        public ActionResult SettleTerm(string termIds)
        {
            string message = string.Empty;
            bool success = false;

            try
            {
                LogHelper.SaveAuditingLog("Term.SettleTerm", string.Format(LogResources.AuditingSettleTerm, termIds));

                IChannelProvider<ITermContractChannel> channelProvider = ChannelProviderFactory<ITermContractChannel>.Create("TermClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                string[] hdfIds = termIds.Split(';');
                bool changed = true;

                var terms = Session["Terms"] != null ? (List<TermListItem>)Session["Terms"] : new List<TermListItem>();

                foreach (string hdfId in hdfIds)
                {
                    if (!string.IsNullOrEmpty(hdfId))
                    {
                        int termId = Int32.Parse(hdfId);
                        if (!service.SettleTerm(termId).Result)
                            changed = false;
                        else
                        {
                            var correction = terms.Where(c => c.ID.Equals(termId)).FirstOrDefault();
                            LogHelper.SaveHistoryLog("Term.Index", new HistoryData { Action = "Liquidar", ID = termId, Target = correction.Client, TargetID = Int32.Parse(correction.Code.Split('-')[0]), ExtraTarget = correction.Stock });
                        }
                    }
                }

                if (changed)
                {
                    success = true;
                    message = hdfIds.Length > 1 ? "Solicitações liquidadas com sucesso." : "Solicitação liquidada com sucesso.";
                    Session["Terms"] = null;
                }
                else
                    message = hdfIds.Length > 1 ? "Não foi possível liquidadar solicitações." : "Não foi possível liquidadar solicitação.";

                service.Close();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                message = "Ocorreu um erro ao liquidar solicitação.";
            }

            return Json(new { message = message, success = success });
        }


        #endregion

        #region Delete

        public ActionResult TermConfirmDelete(int termId)
        {
            var terms = Session["Terms"] != null ? (List<TermListItem>)Session["Terms"] : new List<TermListItem>();
            var term = (from c in terms where c.ID.Equals(termId) select c).FirstOrDefault();
            return View(term);
        }

        [HttpPost]
        public ActionResult DeleteTerm(string hdfTermId)
        {
            string message = string.Empty;
            bool success = false;

            try
            {
                LogHelper.SaveAuditingLog("Term.DeleteTerm", string.Format(LogResources.AuditingDeleteTerm, hdfTermId));

                IChannelProvider<ITermContractChannel> channelProvider = ChannelProviderFactory<ITermContractChannel>.Create("TermClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                int termId = Int32.Parse(hdfTermId);
                var changed = service.ExcludeTerm(termId).Result;

                if (changed)
                {
                    success = true;
                    message = "Solicitação excluída com sucesso.";

                    var terms = Session["Terms"] != null ? (List<TermListItem>)Session["Terms"] : new List<TermListItem>();
                    var correction = terms.Where(c => c.ID.Equals(termId)).FirstOrDefault();

                    LogHelper.SaveHistoryLog("Term.Index", new HistoryData { Action = "Excluir", ID = termId, Target = correction.Client, TargetID = Int32.Parse(correction.Code.Split('-')[0]), ExtraTarget = correction.Stock });
                    Session["Terms"] = null;
                }
                else
                    message = "Não foi possível exluir solicitação.";

                service.Close();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                message = "Ocorreu um erro ao exluir a solicitação.";
            }

            return Json(new { message = message, success = success });
        }

        #endregion

        #region Request

        public ActionResult TermRequest(int id)
        {
            TermListItem term = new TermListItem();

            try
            {
                LogHelper.SaveAuditingLog("Term.Request");
                ViewBag.Company = new SelectList(MarketHelper.CompanyFilter, "key", "value");

                if (id > 0)
                {
                    var terms = Session["Terms"] != null ? (List<TermListItem>)Session["Terms"] : new List<TermListItem>();
                    term = (from c in terms where c.ID.Equals(id) select c).FirstOrDefault();
                    Session["Term.ClientTerms"] = term;
                    return View(term);
                }
            }

            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }

            return View(term ?? new TermListItem());
        }

        public ActionResult TermLotRequest(string lstIndexes)
        {
            List<TermListItem> lotTerms = new List<TermListItem>();
            try
            {
                if (lstIndexes.Length > 0)
                    lstIndexes = lstIndexes.Remove(lstIndexes.Length - 1);

                LogHelper.SaveAuditingLog("Term.TermLotRequest");
                List<int> indexes = lstIndexes.Split(';').Select(s => int.Parse(s)).ToList();

                lotTerms = (List<TermListItem>)Session["Terms"];
                lotTerms = (from t in lotTerms where indexes.Contains(t.Index) select t).ToList();
                
                Session["Term.ClientLotTerms"] = lotTerms.ToList();
            }

            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }

            return View(lotTerms);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult SearchClientTerms(string ClientCode, string FilterId, string FilterText, string StartDate, string EndDate)
        {
            try
            {
                LogHelper.SaveAuditingLog("Other.SearchClientTerms");
                MarketHelper helper = new MarketHelper();

                TermFilter filter = new TermFilter();
                filter.Clients = ClientCode;
                filter.Assessors = "";//helper.GetAssessorsRangesToFilter(string.Empty);
                if (!string.IsNullOrEmpty(FilterId) || !string.IsNullOrEmpty(FilterText))
                    filter.Company = new CommonType { Value = FilterId, Description = FilterText };
                if (!string.IsNullOrEmpty(StartDate))
                    filter.StartDate = StartDate;
                if (!string.IsNullOrEmpty(EndDate))
                    filter.EndDate = EndDate;

                IChannelProvider<ITermContractChannel> termProvider = ChannelProviderFactory<ITermContractChannel>.Create("TermClassName");
                var termService = termProvider.GetChannel().CreateChannel();

                termService.Open();
                var list = termService.LoadClientTerms(filter, CommonHelper.GetLoggedUserGroupId()).Result.ToList();
                termService.Close();

                if (list.Count > 10)
                    list = list.Take(10).ToList();

                TermHelper tHelper = new TermHelper();
                return Json(new { Error = "", tBody = tHelper.GetClientTermsTableBody(list) });
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return Json(new { Error = "Ocorreu um erro ao consultar os termos em aberto.", tBody = "" });
            }
        }

        [HttpPost]
        public ActionResult SaveRequest(FormCollection form)
        {
            string message = string.Empty;
            bool success = false;

            try
            {
                List<TermItem> items = new List<TermItem>();

                bool edit = form["hdfID"] != "0";

                if (edit)
                {
                    TermListItem currentClientTerm = (TermListItem)Session["Term.ClientTerms"];
                    Int64 settleQuantity = Int64.Parse(form["txtTerm" + currentClientTerm.Index].Replace(".", ""));

                    if (settleQuantity > 0)
                    {
                        IChannelProvider<ITermContractChannel> channelProvider = ChannelProviderFactory<ITermContractChannel>.Create("TermClassName");
                        var service = channelProvider.GetChannel().CreateChannel();

                        service.Open();
                        var response = service.ChangeQuantityToSettle(currentClientTerm.ID, settleQuantity);
                        service.Close();

                        if (response.Result)
                        {
                            LogHelper.SaveHistoryLog("Term.Index", new HistoryData { Action = "Alterar", ID = currentClientTerm.ID, TargetID = Int32.Parse(currentClientTerm.Code.Split('-')[0]), Target = currentClientTerm.Client, ExtraTarget = currentClientTerm.Stock });

                            success = response.Result;
                            message = "Solicitação atualizada com sucesso.";
                        }
                        else
                            message = "Não foi possível atualizada a solicitação.";
                    }
                    else
                        message = "Nenhuma solicitação alterada.";
                }
                else
                {
                    List<ClientTerm> currentClientTerms = (List<ClientTerm>)Session["Term.ClientTerms"];

                    foreach (ClientTerm ct in currentClientTerms)
                    {
                        DateTime settleDate = DateTime.Parse(form["hdfBusinessDate" + ct.Index]);
                        Int64 settleQuantity = Int64.Parse(form["txtTerm" + ct.Index].Replace(".", ""));
                        if (settleQuantity > 0)
                        {
                            CommonType type = new CommonType { Value = form["ddlTerm" + ct.Index] };
                            items.Add(new TermItem { ID = 0, Client = ct.Client, Company = ct.Company, Contract = ct.Contract, TotalQuantity = ct.TotalQuantity, SettleDate = settleDate, SettleQuantity = settleQuantity, Type = type, UserID = SessionHelper.CurrentUser.ID });
                        }
                    }

                    LogHelper.SaveAuditingLog("Other.InsertTerm.Post", string.Format(LogResources.AuditingInsertOrdersCorrection, JsonConvert.SerializeObject(items)));

                    if (items.Count > 0)
                    {
                        IChannelProvider<ITermContractChannel> channelProvider = ChannelProviderFactory<ITermContractChannel>.Create("TermClassName");
                        var service = channelProvider.GetChannel().CreateChannel();

                        service.Open();
                        var response = service.InsertTerms(items.ToArray());
                        service.Close();

                        var list = response.Result;

                        if (list != null && list.Count() > 0)
                        {
                            foreach (TermItem t in list)
                                LogHelper.SaveHistoryLog("Term.Index", new HistoryData { Action = "Solicitar", ID = t.ID, TargetID = Int32.Parse(t.Client.Value), Target = t.Client.Description, ExtraTarget = t.Company.Value });

                            success = response.Result.Count() > 0;
                            message = items.Count == 0 ? "Nenhuma solicitação cadastrada." : items.Count == 1 ? "Solicitação cadastrada com sucesso." : "Solicitações cadastradas com sucesso.";
                        }
                        else
                            message = "Não foi possível atualizada a solicitação.";
                    }
                    else
                        message = "Nenhuma solicitação cadastrada.";
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                message = ex.Message;
            }

            return Json(new { message = message, success = success });
        }

        [HttpPost]
        public ActionResult SaveLotRequest(FormCollection form)
        {
            string message = string.Empty;
            bool success = false;

            try
            {
                List<TermItem> items = new List<TermItem>();

                bool edit = false;

                List<TermListItem> currentClientTerms = (List<TermListItem>)Session["Term.ClientLotTerms"];

                foreach (TermListItem ct in currentClientTerms)
                {
                    DateTime settleDate = DateTime.Parse(form["hdfBusinessDate" + ct.Index]);
                    Int64 settleQuantity = Int64.Parse(form["txtTerm" + ct.Index].Replace(".", ""));
                    if (settleQuantity > 0)
                    {
                        CommonType type = new CommonType { Value = form["ddlTerm" + ct.Index] };
                        items.Add(new TermItem { ID = 0, Client = new CommonType { Value = ct.Code.Split('-')[0], Description = ct.Client }, Company = new CommonType { Value = ct.Stock, Description = ct.Company }, Contract = ct.Contract, TotalQuantity = ct.TotalQuantity, SettleDate = settleDate, SettleQuantity = settleQuantity, Type = type, UserID = SessionHelper.CurrentUser.ID });
                    }
                }

                LogHelper.SaveAuditingLog("Other.InsertTerm.Post", string.Format(LogResources.AuditingInsertOrdersCorrection, JsonConvert.SerializeObject(items)));

                if (items.Count > 0)
                {
                    IChannelProvider<ITermContractChannel> channelProvider = ChannelProviderFactory<ITermContractChannel>.Create("TermClassName");
                    var service = channelProvider.GetChannel().CreateChannel();

                    service.Open();
                    var response = service.InsertTerms(items.ToArray());
                    service.Close();

                    var list = response.Result;

                    if (list != null && list.Count() > 0)
                    {
                        foreach (TermItem t in list)
                            LogHelper.SaveHistoryLog("Term.Index", new HistoryData { Action = edit ? "Alterar" : "Solicitar", ID = t.ID, TargetID = Int32.Parse(t.Client.Value), Target = t.Client.Description, ExtraTarget = t.Company.Value });

                        success = response.Result.Count() > 0;
                        message = "Solicitações cadastradas com sucesso.";
                    }
                    else
                        message = response.Message;
                }
                else
                    message = "Nenhuma solicitação cadastrada.";
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                message = ex.Message;
            }

            return Json(new { message = message, success = success });
        }

        #endregion

        #region Export

        public ActionResult ConfirmExport()
        {
            return View();
        }

        public ActionResult ExportTermsToFile(string exportType, string groupBy)
        {
            List<TermListItem> currentList = new List<TermListItem>();
            bool exportToPdf = exportType == "PDF";
            try
            {
                if (Session["Terms"] != null)
                    currentList = (List<TermListItem>)Session["Terms"];

                //LogHelper.SaveAuditingLog("Term.TermsToFile", string.Format(LogResources.ObjectToFileParameters, exportToPdf ? "PDF" : "Excel", JsonConvert.SerializeObject(currentList)));

                string fileName = string.Format("termo_{0}.{1}", DateTime.Now.ToString("dd-MM-yyyy"), exportToPdf ? "pdf" : "xls");

                LogHelper.SaveHistoryLog("Term.Index", new HistoryData { Action = "Exportar", OldData = string.Empty, NewData = JsonConvert.SerializeObject(currentList) });

                return exportToPdf ? ViewTermPdf(currentList, true, fileName, groupBy) : ViewTermExcel(currentList, true, fileName, groupBy);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return this.RedirectToAction("Index");
            }


        }

        #endregion

        #region History

        public ActionResult TermHistory()
        {
            List<SelectListItem> listItems = new List<SelectListItem>();
            listItems.Add(new SelectListItem { Text = "Todas", Value = "", Selected = true });

            try
            {
                var actions = TermHelper.TermActions;
                foreach (string action in actions)
                    listItems.Add(new SelectListItem { Text = action, Value = action, Selected = false });

                ViewBag.TermActions = listItems;

                LogHelper.SaveAuditingLog("Term.TermHistory");
                return View();
            }

            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = "Não foi possível carregar o histórico das solicitações de termos.";
                return View();
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public void LoadTermHistory(FormCollection form)
        {
            try
            {

                string startDate = form["txtStartDate"];
                string endDate = form["txtEndDate"];

                long responsibleID = string.IsNullOrEmpty(form["hdfResponsible"]) ? 0 : long.Parse(form["hdfResponsible"]);
                int clientID = string.IsNullOrEmpty(form["txtClient"]) ? 0 : Int32.Parse(form["txtClient"]);
                string StockCode = string.IsNullOrEmpty(form["txtStockCode"]) ? "" : form["txtStockCode"];

                int id = 0;
                Int32.TryParse(form["hdfTermID"], out id);

                string action = form["ddlTermActions"];

                LogHelper.SaveAuditingLog("Term.LoadTermHistory", string.Format(LogResources.AuditingLoadHistory, clientID, StockCode, responsibleID, action, startDate, endDate));

                HistoryFilter filter = new HistoryFilter { ModuleName = "Term.Index", StartDate = startDate, Action = action, EndDate = endDate, ResponsibleID = responsibleID, TargetID = clientID, ID = id, ExtraTargetText = StockCode };

                Session["TermHistory"] = LogHelper.LoadHistory(filter);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }
        }

        public ActionResult TermHistoryToFile(bool isPdf, string detail)
        {
            return this.HistoryToFile(isPdf, "Term", "Solicitar Termos", "Cliente", true, string.IsNullOrEmpty(detail) ? "TermHistory" : "TermHistoryDetail", "Papel");
        }

        public ActionResult HistoryToFile(bool isPdf, string clientName, string targetTitle, string targetColumnTitle, bool hasAction, string sessionName, string extraTargetColumnTitle = null)
        {
            List<HistoryData> currentList = new List<HistoryData>();

            try
            {
                if (Session[sessionName] != null)
                    currentList = (List<HistoryData>)Session[sessionName];

                //LogHelper.SaveAuditingLog(string.Concat("Term.HistoryToFile.", clientName), string.Format(LogResources.ObjectToFileParameters, isPdf ? "PDF" : "Excel", JsonConvert.SerializeObject(currentList)));
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return this.RedirectToAction("Index");
            }

            string name = new Regex(@"\s*").Replace(HttpUtility.HtmlDecode(targetTitle), string.Empty);
            targetTitle = string.Concat(isPdf ? "Histórico de " : "Hist&#243;rico de ", targetTitle);

            string fileName = string.Format("historico_{0}_{1}.{2}", name, DateTime.Now.ToString("dd-MM-yyyy"), isPdf ? "pdf" : "xls");

            return isPdf ? ViewHistoryPdf(currentList, true, fileName, targetTitle, targetColumnTitle, hasAction, extraTargetColumnTitle) : ViewHistoryExcel(currentList, true, fileName, targetTitle, targetColumnTitle, hasAction, extraTargetColumnTitle);
        }

        public ActionResult TermHistoryDetail(int termID)
        {
            try
            {
                LogHelper.SaveAuditingLog("Term.TermHistoryDetail");

                var terms = Session["Terms"] != null ? (List<TermListItem>)Session["Terms"] : new List<TermListItem>();
                var term = (from c in terms where c.ID.Equals(termID) select c).FirstOrDefault();

                HistoryFilter filter = new HistoryFilter { ModuleName = "Term.Index", ID = termID };

                var data = LogHelper.LoadHistory(filter);

                Session["TermHistoryDetail"] = data;
                ViewBag.TermHistoryDetails = data;

                return View(term);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = "Não foi possível carregar o histórico deste termo.";
                return View(new TermListItem());
            }
        }

        #endregion


    }
}
