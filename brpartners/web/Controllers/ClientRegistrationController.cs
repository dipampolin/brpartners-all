﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.WebSite.Models;
using QX3.Portal.WebSite.ClientRegistrationService;
using QX3.Portal.WebSite.Models.Channel;
using QX3.Portal.WebSite.Helper;
using QX3.Spinnex.Common.Services.Logging;
using Newtonsoft.Json;
using iTextSharp.text.html;
using QX3.Portal.WebSite.OtherService;
using QX3.Portal.WebSite.Properties;
using System.IO;

namespace QX3.Portal.WebSite.Controllers
{
    public class ClientRegistrationController : FileController
	{
		#region Cadastro de Status

		[AccessControlAuthorize]
        public ActionResult StatusViewer()
        {

            return View();
        }

        public string StatusViewerFilter(FormCollection form)
        {
            using (var service = ChannelProviderFactory<IClientRegistrationContractChannel>.Create("ClientRegistrationClassName").GetChannel().CreateChannel()) 
            {
                var clientFilter = new ClientFilter();
                DateTime value;

                if (!String.IsNullOrEmpty(form["txtStartDate"]))
                {
                    if (DateTime.TryParse(form["txtStartDate"].ToString(), out value))
                        clientFilter.InitAcessionDate = Convert.ToDateTime(form["txtStartDate"]) ;
                }

                if (!String.IsNullOrEmpty(form["txtEndDate"]))
                {
                    if (DateTime.TryParse(form["txtEndDate"].ToString(), out value))
                        clientFilter.FinalAcessionDate = Convert.ToDateTime(form["txtEndDate"]);
                }

                if (!string.IsNullOrEmpty(form["txtAssessorFilter"]))
                    clientFilter.AssessorFilterText = new MarketHelper().GetAssessorsRangesToFilter(form["txtAssessorFilter"]);

                if (!string.IsNullOrEmpty(form["txtClientFilter"]))
                    clientFilter.ClientFilterText = new MarketHelper().GetClientsRangesToFilter(form["txtClientFilter"]);

                if (form["chkbmf"] == "true")//Filtrar bmf inativos
                    clientFilter.BMFActive = true;
                if (form["chkbovespa"] == "true")//Filtrar bovespa inativos
                    clientFilter.BovespaActive = true;

                var options = new FilterOptions()
                {
                    InitRow = Convert.ToInt32(form["iDisplayStart"]),
                    FinalRow = Convert.ToInt32(form["iDisplayLength"]),
                    SortColumn = (form["iSortCol_0"] == "null" || form["iSortCol_0"] == "undefined") ? 1 : Convert.ToInt32(form["iSortCol_0"]) + 1,
                    SortDirection = form["sSortDir_0"],
                    AllRecords = false
                };

                var list = service.LoadActiveAndInactiveClients(clientFilter, options, CommonHelper.GetLoggedUserGroupId());

                return this.StatusViewerListObject(list.Result != null ? list.Result : new List<Client>(), ref form, list.Data);
            }
        }

        public string StatusViewerListObject(List<Client> list, ref FormCollection form, int iCount)
        {
            JsonModel dtJson = new JsonModel();

            dtJson.sEcho = Convert.ToInt32(form["sEcho"]);

            if (list == null) list = new List<Client>();

            dtJson.iTotalRecords = iCount;
            dtJson.iTotalDisplayRecords = iCount;

            var tableValues = new List<string[]>();
            List<String> rowData = new List<String>();

            foreach (Client cst in list)
            {
                
                rowData.Add(cst.AccessionDate.HasValue ? cst.AccessionDate.Value.ToShortDateString() : "<span class='ico-nao-aplica'></span>");
                rowData.Add(cst.ClientCode.ToString());
                rowData.Add(cst.ClientName);
                rowData.Add(cst.CPFCGC.ToString());
                rowData.Add(cst.AssessorName.ToString());
                rowData.Add(cst.BMFActive.HasValue ? cst.BMFActive.Value ? "<span class='ico-check' title='Ativo'></span>" : "<span class='ico-inativo' title='Inativo'></span>" : "<span class='ico-nao-aplica' title='Não se aplica'></span>");
                rowData.Add(cst.BovespaActive.HasValue ? cst.BovespaActive.Value ? "<span class='ico-check' title='Ativo'></span>" : "<span class='ico-inativo' title='Inativo'></span>" : "<span class='ico-nao-aplica' title='Não se aplica'></span>");
                
                tableValues.Add(rowData.ToArray());
                rowData = new List<string>();

            }


            dtJson.aaData = tableValues;

            return Newtonsoft.Json.JsonConvert.SerializeObject(dtJson);
        }

        [HttpPost]
        public ActionResult StatusViewerFilterOptions(FormCollection form)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            try
            {

                var clientFilter = new ClientFilter();
                if (!String.IsNullOrEmpty(form["txtStartDate"]))
                    clientFilter.InitAcessionDate = Convert.ToDateTime(form["txtStartDate"]);
                if (!String.IsNullOrEmpty(form["txtEndDate"]))
                    clientFilter.FinalAcessionDate = Convert.ToDateTime(form["txtEndDate"]);

                if (!string.IsNullOrEmpty(form["txtAssessorFilter"]))
                    clientFilter.AssessorFilterText = new MarketHelper().GetAssessorsRangesToFilter(form["txtAssessorFilter"]);

                if (!string.IsNullOrEmpty(form["txtClientFilter"]))
                    clientFilter.ClientFilterText = new MarketHelper().GetClientsRangesToFilter(form["txtClientFilter"]);

                if (form["chkbmf"] == "true")//Filtrar bmf inativos
                    clientFilter.BMFActive = true;
                if (form["chkbovespa"] == "true")//Filtrar bovespa inativos
                    clientFilter.BovespaActive = true;

                var options = new FilterOptions()
                {
                    SortColumn = (form["iSortCol_0"] == "null" || form["iSortCol_0"] == "undefined") ? 1 : Convert.ToInt32(form["iSortCol_0"]) + 1,
                    SortDirection = form["sSortDir_0"],
                    AllRecords = true
                };

                Session["StatusViewer.Filter"] = clientFilter;
                Session["StatusViewer.Options"] = options;

                return Json(true);
            }
            catch { return Json(false); }
        }

        public ActionResult StatusViewerToFile(bool isPdf)
        {
            List<Client> currentList = new List<Client>();
            try
            {
                if (Session["StatusViewer.Filter"] == null || Session["StatusViewer.Options"] == null)
                    throw new Exception();

                using (var service = ChannelProviderFactory<IClientRegistrationContractChannel>.Create("ClientRegistrationClassName").GetChannel().CreateChannel())
                {
                    var clientFilter = (ClientFilter)Session["StatusViewer.Filter"];
                    var options = (FilterOptions)Session["StatusViewer.Options"];

                    currentList = service.LoadActiveAndInactiveClients(clientFilter, options, CommonHelper.GetLoggedUserGroupId()).Result;

                    string fileName = string.Format("cadastrostatus_{0}.{1}", DateTime.Now.ToString("dd-MM-yyyy"), isPdf ? "pdf" : "xls");

                    return isPdf ? ViewStatusViewerPdf(currentList, true, fileName) : ViewStatusViewerExcel(currentList, true, fileName);
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = "Ocorreu um erro ao gerar relatório.";
                return this.RedirectToAction("StatusViewer");
            }

		}

		#endregion

		#region Cadastro de Vencimentos

		[AccessControlAuthorize]
		public ActionResult OverdueAndDue()
		{
			return View();
		}

		public string OverdueAndDueFilter(FormCollection form)
		{
			using (var service = ChannelProviderFactory<IClientRegistrationContractChannel>.Create("ClientRegistrationClassName").GetChannel().CreateChannel())
			{
				OverdueAndDueFilter filter = new OverdueAndDueFilter();

				if (!string.IsNullOrEmpty(form["txtClient"]))
					filter.ClientCode = Convert.ToInt16(form["txtClient"]);

				if (!string.IsNullOrEmpty(form["txtAssessorFilter"]))
					filter.AssessorFilterText = new MarketHelper().GetAssessorsRangesToFilter(form["txtAssessorFilter"]);
				
				if (!String.IsNullOrEmpty(form["ddlSituation"]))
					filter.Situation = Convert.ToInt16(form["ddlSituation"]);

				if (!String.IsNullOrEmpty(form["chkActive"]) && form["chkActive"].Equals("true"))
					filter.SinacorActive = true;

				if (!String.IsNullOrEmpty(form["chkInactive"]) && form["chkInactive"].Equals("true"))
					filter.SinacorInactive = true;

				if (!String.IsNullOrEmpty(form["chkBlocked"]) && form["chkBlocked"].Equals("true"))
					filter.SinacorBlocked = true;

				if (!String.IsNullOrEmpty(form["radSpecificRange"]) && form["radSpecificRange"].Equals("true"))
				{
					if (!String.IsNullOrEmpty(form["txtDueDateStart"]))
						filter.DueDateStart = Convert.ToDateTime(form["txtDueDateStart"]);

					if (!String.IsNullOrEmpty(form["txtDueDateEnd"]))
						filter.DueDateEnd = Convert.ToDateTime(form["txtDueDateEnd"]);
				}

				FilterOptions options = new FilterOptions()
				{
					InitRow = Convert.ToInt32(form["iDisplayStart"]),
					FinalRow = Convert.ToInt32(form["iDisplayLength"]),
					SortColumn = (form["iSortCol_0"] == "null" || form["iSortCol_0"] == "undefined") ? 1 : Convert.ToInt32(form["iSortCol_0"]) + 1,
					SortDirection = form["sSortDir_0"],
					AllRecords = false
				};
				var list = service.LoadOverdueAndDueRegistration(filter, options, CommonHelper.GetLoggedUserGroupId());
				return this.OverdueAndDueListObject(list.Result != null ? list.Result : new List<OverdueAndDue>(), ref form, list.Data);
			}
		}

		public string OverdueAndDueListObject(List<OverdueAndDue> list, ref FormCollection form, int iCount)
		{
			JsonModel dtJson = new JsonModel();

			dtJson.sEcho = Convert.ToInt32(form["sEcho"]);

			if (list == null) list = new List<OverdueAndDue>();

			dtJson.iTotalRecords = iCount;
			dtJson.iTotalDisplayRecords = iCount;

			var tableValues = new List<string[]>();
			List<String> rowData = new List<String>();

			MarketHelper marketHelper = new MarketHelper();

			foreach (OverdueAndDue cst in list)
			{
				rowData.Add(cst.ClientCode.ToString());
				rowData.Add(HtmlEncoder.Encode(cst.ClientName));
				rowData.Add(cst.CPFCGC.ToString());
				rowData.Add(marketHelper.GetAssessorName(cst.AssessorCode));
				rowData.Add(HtmlEncoder.Encode(cst.RegisterType));
				if (cst.Situation == 2)
				{
					rowData.Add(string.Format("<span class=\"vencido\">{0}</span>", HtmlEncoder.Encode(cst.SituationDesc)));
				}
				else
				{
					rowData.Add(HtmlEncoder.Encode(cst.SituationDesc));
				}
				rowData.Add(cst.RegisterDate != null && cst.RegisterDate != DateTime.MinValue ? cst.RegisterDate.ToShortDateString() : "-");
				rowData.Add(HtmlEncoder.Encode(cst.SinacorDesc));
                rowData.Add("<a target='_new' class=\"btn-padrao \" href='" + Url.Action("GoToRegistration", "ClientRegistration", new { id = cst.ClientCode.ToString() }) + "' rel=\"" + cst.ClientCode + "\"><span>Gerar Cadastro</span></a>");
				tableValues.Add(rowData.ToArray());
				rowData = new List<string>();
			}
			dtJson.aaData = tableValues;
			return Newtonsoft.Json.JsonConvert.SerializeObject(dtJson);
		}

		[HttpPost]
		public ActionResult OverdueAndDueFilterOptions(FormCollection form)
		{
			Response.Cache.SetCacheability(HttpCacheability.NoCache);

			try
			{
				OverdueAndDueFilter filter = new OverdueAndDueFilter();

				if (!string.IsNullOrEmpty(form["txtClient"]))
					filter.ClientCode = Convert.ToInt16(form["txtClient"]);

				if (!string.IsNullOrEmpty(form["txtAssessorFilter"]))
					filter.AssessorFilterText = new MarketHelper().GetAssessorsRangesToFilter(form["txtAssessorFilter"]);

				if (!String.IsNullOrEmpty(form["ddlSituation"]))
					filter.Situation = Convert.ToInt16(form["ddlSituation"]);

				if (!String.IsNullOrEmpty(form["chkActive"]) && form["chkActive"].Equals("true"))
					filter.SinacorActive = true;

				if (!String.IsNullOrEmpty(form["chkInactive"]) && form["chkInactive"].Equals("true"))
					filter.SinacorInactive = true;

				if (!String.IsNullOrEmpty(form["chkBlocked"]) && form["chkBlocked"].Equals("true"))
					filter.SinacorBlocked = true;

				if (!String.IsNullOrEmpty(form["radSpecificRange"]) && form["radSpecificRange"].Equals("true"))
				{
					if (!String.IsNullOrEmpty(form["txtDueDateStart"]))
						filter.DueDateStart = Convert.ToDateTime(form["txtDueDateStart"]);

					if (!String.IsNullOrEmpty(form["txtDueDateEnd"]))
						filter.DueDateEnd = Convert.ToDateTime(form["txtDueDateEnd"]);
				}

				FilterOptions options = new FilterOptions()
				{
					SortColumn = (form["iSortCol_0"] == "null" || form["iSortCol_0"] == "undefined") ? 1 : Convert.ToInt32(form["iSortCol_0"]) + 1,
					SortDirection = form["sSortDir_0"],
					AllRecords = true
				};

				Session["OverdueAndDue.Filter"] = filter;
				Session["OverdueAndDue.Options"] = options;

				return Json(true);
			}
			catch { return Json(false); }
		}

		public ActionResult OverdueAndDueToFile(bool isPdf)
		{
			List<OverdueAndDue> currentList = new List<OverdueAndDue>();
			try
			{
				if (Session["OverdueAndDue.Filter"] == null || Session["OverdueAndDue.Options"] == null)
					throw new Exception();

				using (var service = ChannelProviderFactory<IClientRegistrationContractChannel>.Create("ClientRegistrationClassName").GetChannel().CreateChannel())
				{
					OverdueAndDueFilter filter = (OverdueAndDueFilter)Session["OverdueAndDue.Filter"];
					FilterOptions options = (FilterOptions)Session["OverdueAndDue.Options"];
					currentList = service.LoadOverdueAndDueRegistration(filter, options, CommonHelper.GetLoggedUserGroupId()).Result;
					string fileName = string.Format("cadastro_vencimentos_{0}.{1}", DateTime.Now.ToString("dd-MM-yyyy"), isPdf ? "pdf" : "xls");
					return isPdf ? ViewOverdueAndDuePdf(currentList, true, fileName) : ViewOverdueAndDueExcel(currentList, true, fileName);
				}
			}
			catch (Exception ex)
			{
				LogManager.WriteError(ex);
				TempData["ErrorMessage"] = "Ocorreu um erro ao gerar relatório.";
				return this.RedirectToAction("OverdueAndDue");
			}
		}

		#endregion

        #region Repasse Bovespa

        public ActionResult TransferBovespa()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public string LoadTransferBovespa(FormCollection form)
        {
            LogHelper.SaveAuditingLog("ClientRegistration.LoadTransferBovespa");

            int count = 0;
            TransferBovespaFilter filter = new TransferBovespaFilter();

            if (form["txtClientCode"] != null && form["txtClientCode"].ToString() != string.Empty)
                filter.ClientCode = form["txtClientCode"].ToString();

            if (form["txtCnpj"] != null && form["txtCnpj"].ToString() != string.Empty)
                filter.CNPJ = form["txtCnpj"].ToString();

            var options = new FilterOptions()
            {
                InitRow = Convert.ToInt32(form["iDisplayStart"]),
                FinalRow = Convert.ToInt32(form["iDisplayLength"]),
                SortColumn = (form["iSortCol_0"] == "null" || form["iSortCol_0"] == "undefined" || form["iSortCol_0"] == "0") ? 1 : Convert.ToInt32(form["iSortCol_0"]),
                SortDirection = form["sSortDir_0"],
                AllRecords = false
            };

            Session["ClientRegistration.TransferBovespaFilter"] = filter;
            Session["ClientRegistration.TransferBovespaFilterOptions"] = options;

            IChannelProvider<IClientRegistrationContractChannel> transferProvider = ChannelProviderFactory<IClientRegistrationContractChannel>.Create("ClientRegistrationClassName");
            var transferService = transferProvider.GetChannel().CreateChannel();

            transferService.Open();

            List<TransferBovespaItem> list = transferService.LoadTransfersBovespa(out count, filter, options, CommonHelper.GetLoggedUserGroupId()).Result.ToList();

            transferService.Close();

            Session["ClientRegistration.TransferBovespa"] = list;

            TransferHelper oHelper = new TransferHelper();
            return new TransferHelper().GetOrdersCorrectionTableBody(list, ref form, count);
        }

        public ActionResult TransferBovespaToFile(bool isPdf)
        {
            List<TransferBovespaItem> currentList = new List<TransferBovespaItem>();
            try
            {
                using (var service = ChannelProviderFactory<IClientRegistrationContractChannel>.Create("ClientRegistrationClassName").GetChannel().CreateChannel())
                {
                    var filter = (TransferBovespaFilter)Session["ClientRegistration.TransferBovespaFilter"];
                    var options = (FilterOptions)Session["ClientRegistration.TransferBovespaFilterOptions"];
                    options.AllRecords = true;

                    var count = 0;

                    currentList = service.LoadTransfersBovespa(out count, filter, options, CommonHelper.GetLoggedUserGroupId()).Result.ToList();
                    string fileName = string.Format("repasse_bovespa_{0}.{1}", DateTime.Now.ToString("dd-MM-yyyy"), isPdf ? "pdf" : "xls");
                    return isPdf ? ViewTransferBovespaPdf(currentList, true, fileName) : ViewTransferBovespaExcel(currentList, true, fileName);
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = "Ocorreu um erro ao gerar relatório.";
                return this.RedirectToAction("TransferBovespa");
            }
        }

        #endregion

        #region Repasse BMF
        
		[AccessControlAuthorize]
        public ActionResult TransferBMF()
        {
            return View();
        }

        public string TransferBMFFilter(FormCollection form)
        {
            using (var service = ChannelProviderFactory<IClientRegistrationContractChannel>.Create("ClientRegistrationClassName").GetChannel().CreateChannel())
            {
                var filter = new TransferBMFFilter();
                if (!String.IsNullOrEmpty(form["txtClient"]))
                    filter.ClientCode = Convert.ToInt32(form["txtClient"]);
                if (!String.IsNullOrEmpty(form["hdfBroker"]))
                    filter.BrokerId = Convert.ToInt32(form["hdfBroker"]);

                if (!string.IsNullOrEmpty(form["ddlType"]))
                    filter.Type = form["ddlType"];

                if (!string.IsNullOrEmpty(form["ddlSituation"]))
                    filter.Situation = form["ddlSituation"];

                filter.UserId = CommonHelper.GetLoggedUserGroupId();

                var options = new FilterOptions()
                {
                    InitRow = Convert.ToInt32(form["iDisplayStart"]),
                    FinalRow = Convert.ToInt32(form["iDisplayLength"]),
                    SortColumn = (form["iSortCol_0"] == "null" || form["iSortCol_0"] == "undefined") ? 1 : Convert.ToInt32(form["iSortCol_0"]) + 1,
                    SortDirection = form["sSortDir_0"],
                    AllRecords = false
                };

                var list = service.LoadTransfersBMF(filter, options);

				Session["TransferBMF.Filter"] = filter;
				Session["TransferBMF.Options"] = options;

                return this.TransferBMFListObject(list.Result != null ? list.Result : new List<TransferBMFItem>(), ref form, list.Data);
            }
        }

        public string TransferBMFListObject(List<TransferBMFItem> list, ref FormCollection form, int iCount)
        {
            JsonModel dtJson = new JsonModel();

            dtJson.sEcho = Convert.ToInt32(form["sEcho"]);

            if (list == null) list = new List<TransferBMFItem>();

            dtJson.iTotalRecords = iCount;
            dtJson.iTotalDisplayRecords = iCount;

            var tableValues = new List<string[]>();
            List<String> rowData = new List<String>();
            Random r = new Random();
            AccessControlHelper acHelper = new AccessControlHelper();

            foreach (TransferBMFItem cst in list)
            {
                rowData.Add("<input type='hidden' class='vinculo-id' value='" + cst.BondId + "|" + cst.BrokerId + "' />" + cst.BondId.ToString());
                rowData.Add(cst.BrokerName);
                rowData.Add((cst.ClientCode != 0) ? cst.ClientCode.ToString() : "<input type='text' class='input-cliente' id='input_"+ r.Next(1, 100000) +"' />");
                rowData.Add((cst.ClientCode != 0) ? cst.ClientName : "-");
                rowData.Add((cst.ClientCode != 0) ? cst.CPFCGC.ToString() : "-");
                rowData.Add(cst.TypeDesc.ToString());
                if (acHelper.HasFuncionalityPermission("ClientRegistration.TransferBMF", "btnSave"))
                {
                    rowData.Add(cst.ClientCode != 0 && cst.Type != "R" 
                            ? "<a href='javascript:;' class='lnkEditar'><span class='ico-editar' title='Editar'>Editar</span></a>"
                            : (cst.Type == "E")
                                ? "<a href='javascript:;' class='lnkInserir'><span class='ico-salvar' title='Salvar'>Salvar</span></a>"
                                : "");
                }
                else {
                    rowData.Add("");
                }

                tableValues.Add(rowData.ToArray());
                rowData = new List<string>();

            }

            dtJson.aaData = tableValues;

            return Newtonsoft.Json.JsonConvert.SerializeObject(dtJson);
        }

		public ActionResult TransferBMFToFile(bool isPdf)
		{
			List<TransferBMFItem> currentList = new List<TransferBMFItem>();
			try
			{
				if (Session["TransferBMF.Filter"] == null || Session["TransferBMF.Options"] == null)
					throw new Exception();

				using (var service = ChannelProviderFactory<IClientRegistrationContractChannel>.Create("ClientRegistrationClassName").GetChannel().CreateChannel())
				{
					TransferBMFFilter filter = (TransferBMFFilter)Session["TransferBMF.Filter"];
					FilterOptions options = (FilterOptions)Session["TransferBMF.Options"];
					options.AllRecords = true;
					currentList = service.LoadTransfersBMF(filter, options).Result;
					string fileName = string.Format("repasse_bmf_{0}.{1}", DateTime.Now.ToString("dd-MM-yyyy"), isPdf ? "pdf" : "xls");
					return isPdf ? ViewTransferBMFPdf(currentList, true, fileName) : ViewTransferBMFExcel(currentList, true, fileName);
				}
			}
			catch (Exception ex)
			{
				LogManager.WriteError(ex);
				TempData["ErrorMessage"] = "Ocorreu um erro ao gerar relatório.";
				return this.RedirectToAction("TransferBMF");
			}
		}

        [HttpPost]
        public ActionResult CheckClientBMF(int clientId) 
        {
            using (var service = ChannelProviderFactory<IClientRegistrationContractChannel>.Create("ClientRegistrationClassName").GetChannel().CreateChannel())
            {
                var response = service.CheckClientBMF(clientId);
                if (response.Result != null) 
                {
                    var returns = new 
                    {
                        Result = true, 
                        Message = "",
                        ClientName = response.Result.ClientName,
                        CPFCNPJ = response.Result.CPFCGC
                    };
                    return Json(returns);
                }
                return Json(new { Result = false, Message = "Cliente não encontrado." });
            }
        }

        [HttpPost]
        public ActionResult InsertClientBond(int bondId, int clientId, int brokerId) 
        {
            using (var service = ChannelProviderFactory<IClientRegistrationContractChannel>.Create("ClientRegistrationClassName").GetChannel().CreateChannel()) 
            {
                var response = service.InsertClientBond(
                    new TransferBMFItem()
                    {
                        BondId = bondId,
                        ClientCode = clientId,
                        BrokerId = brokerId
                    });

                if (response.Result) 
                {
                    var otherService = ChannelProviderFactory<IOtherContractChannel>.Create("OtherClassName").GetChannel().CreateChannel();
                    otherService.Open();
                    CPClientInformation info = otherService.LoadClientInformation(clientId.ToString(), "", CommonHelper.GetLoggedUserGroupId()).Result;
                    otherService.Close();

                    LogHelper.SaveHistoryLog("ClientRegistration.TransferBMF",
                       new HistoryData
                       {
                           Action = "Editar vínculos",
                           NewData = JsonConvert.SerializeObject(new TransferBMFItem()
                           {
                               BondId = bondId,
                               ClientCode = clientId,
                               BrokerId = brokerId
                           }),
                           ID = bondId,
                           TargetID = clientId,
                           Target = info.Client.Description,
                           ExtraTarget = bondId.ToString()
                       });

                    return Json(new { Executed = response.Result, ClientName = info.Client.Description, CPFCGC = info.CPFCNPJ, Message = "Vínculo cadastrado com sucesso." });    
                }

                return Json(new { Executed = response.Result, Message = response.Message });

            }
        }

        public ActionResult TransferBMFHistory() 
        {
            return View();
        }

        [HttpPost]
        public void LoadTransferBMFHistory(FormCollection form)
        {
            try
            {
                string startDate = form["txtStartDate"];
                string endDate = form["txtEndDate"];

                long responsibleID = string.IsNullOrEmpty(form["hdfResponsible"]) ? 0 : long.Parse(form["hdfResponsible"]);
                int txtClientCode = string.IsNullOrEmpty(form["txtClientCode"]) ? 0 : Int32.Parse(form["txtClientCode"]);
                string txtBondId = string.IsNullOrEmpty(form["txtBondId"]) ? string.Empty : form["txtBondId"];

                int id = string.IsNullOrEmpty(form["hdfRequestID"]) ? 0 : Int16.Parse(form["hdfRequestID"]);

                //string action = form["ddlHistoryActions"];

                LogHelper.SaveAuditingLog("ClientRegistration.TransferBMFHistory",
                    string.Format(LogResources.AuditingLoadHistory, txtClientCode, responsibleID, "Editar vínculos", startDate, endDate));

                HistoryFilter filter = new HistoryFilter
                {
                    ModuleName = "ClientRegistration.TransferBMF",
                    StartDate = startDate,
                    Action = "Editar vínculos",
                    EndDate = endDate,
                    TargetID = txtClientCode,
                    TargetText = CommonHelper.GetClientName(txtClientCode),
                    ExtraTargetText = txtBondId,
                    ResponsibleID = responsibleID
                };

                //if (!String.IsNullOrEmpty(txtBondId)) filter.ID = Convert.ToInt32(txtBondId);

                Session["ClientRegistration.TransferBMFHistory"] = LogHelper.LoadHistory(filter);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }
        }

        public ActionResult TransferBMFHistoryList(FormCollection form)
        {
            List<HistoryData> items = (List<HistoryData>)Session["ClientRegistration.TransferBMFHistory"];
            return new DataTablesController().AccessControl_History(form, items);

        }

        public ActionResult TransferBMFHistoryToFile(bool isPdf, bool detail)
        {
            List<HistoryData> currentList = new List<HistoryData>();
            string sessionName = detail ? "ClientRegistration.TransferBMFHistory" : "ClientRegistration.TransferBMFHistory";
            try
            {
                if (Session[sessionName] != null)
                    currentList = (List<HistoryData>)Session[sessionName];

                //LogHelper.SaveAuditingLog(string.Concat("Subscription.Index.HistoryToFile"), string.Format(LogResources.ObjectToFileParameters, isPdf ? "PDF" : "Excel", JsonConvert.SerializeObject(currentList)));
                string targetTitle = string.Concat(isPdf ? "Histórico de Repasse BMF" : "Hist&#243;rico de Repasse BMF");
                string fileName = string.Format("historico_{0}_{1}.{2}", "repasse bmf", DateTime.Now.ToString("dd-MM-yyyy"), isPdf ? "pdf" : "xls");
                return isPdf
                    ? ViewHistoryPdf(currentList, true, fileName, targetTitle, "Cliente", true, "Vínculo")
                    : ViewHistoryExcel(currentList, true, fileName, targetTitle, "Cliente", true, "V&#237;nculo");
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = Resources.GenericReportErrorMessage;
                return this.RedirectToAction("TransferBMF");
            }
        }

        #endregion

        [HttpPost]
        public ActionResult LoadBanks(string filter)
        {
            List<BankAccount> brokers = new List<BankAccount>();
            IChannelProvider<IClientRegistrationContractChannel> channelProvider = ChannelProviderFactory<IClientRegistrationContractChannel>.Create("ClientRegistrationClassName");
            using (var service = channelProvider.GetChannel().CreateChannel())
            {
                var res = service.LoadBanks(filter).Result;
                if (res != null) brokers = res.ToList();
            }

            return Json(brokers);
        }

        #region Cadastro

        [AccessControlAuthorize]
        public ActionResult Index() 
        {
            return View();
        }

        [HttpPost]
        public ActionResult NewPFRegistration() {
            Session["ClientRegistration.IndividualPerson"] = null;
            return Json(new { Result = true });
        }

        [HttpPost]
        public ActionResult NewPJRegistration()
        {
            Session["ClientRegistration.LegalPerson"] = null;
            return Json(new { Result = true });
        }

        protected bool CheckFormCollection(FormCollection form, string key) {
            return form.AllKeys.Where(a => a.Equals(key)).FirstOrDefault() != null;
        }

        protected void LoadStates(string name, DocumentInfo info) 
        {
            List<string> states = new List<string>();
            states.Add("--");
            states.Add("AC");
            states.Add("AL");
            states.Add("AM");
            states.Add("AP");
            states.Add("BA");
            states.Add("CE");
            states.Add("DF");
            states.Add("ES");
            states.Add("GO");
            states.Add("MA");
            states.Add("MG");
            states.Add("MS");
            states.Add("MT");
            states.Add("PA");
            states.Add("PB");
            states.Add("PE");
            states.Add("PI");
            states.Add("PR");
            states.Add("RJ");
            states.Add("RN");
            states.Add("RO");
            states.Add("RR");
            states.Add("RS");
            states.Add("SC");
            states.Add("SE");
            states.Add("SP");
            states.Add("TO");
            ViewData[name] = new SelectList(states, "--");
        }

        [HttpPost]
        public ActionResult AddNewAddress() 
        {
            ViewBag.NewAddress = true;
            this.LoadStates("ddlAddressState", null);
            return PartialView("Reg_Address");
        }

        [HttpPost]
        public ActionResult AddNewBankAccount(bool ctvm) 
        {
            ViewBag.NewBankAccount = true;

            ViewBag.CTVM = ctvm;
            return PartialView("Reg_BankAccounts");
        }

        [HttpPost]
        public ActionResult AddNewReferences()
        {
            ViewBag.NewReference = true;
            return PartialView("Reg_ComercialRef");
        }

        [HttpPost]
        public ActionResult AddNewPJReferences()
        {
            ViewBag.NewPJReference = true;
            return PartialView("Reg_PJ_ComercialRef");
        }
        
        [HttpPost]
        public ActionResult AddNewImobileGoods()
        {
            ViewBag.newImobile = true;
            return PartialView("Reg_ImobileGoods");
        }

        [HttpPost]
        public ActionResult AddNewMobileGoods()
        {
            ViewBag.NewMobile = true;
            return PartialView("Reg_MobileGoods");
        }

        [HttpPost]
        public ActionResult AddNewMensalRendiments()
        {
            ViewBag.NewRendiment = true;
            return PartialView("Reg_MensalRendiment");
        }

        [HttpPost]
        public ActionResult AddNewAuthorizedAccount()
        {
            ViewBag.NewAuthorizedAccount = true;
            return PartialView("Reg_Authorization");
        }

        [HttpPost]
        public ActionResult AddNewControlledLegalPerson()
        {
            ViewBag.NewControlledLegalPerson = true;
            return PartialView("Reg_PJ_ControlledLegalPerson");
        }

        [HttpPost]
        public ActionResult AddNewStockControl() 
        {
            ViewBag.NewStockControl = true;
            return PartialView("Reg_PJ_StockControls");
        }

        [HttpPost]
        public ActionResult AddNewAffiliated()
        {
            ViewBag.NewAffiliated = true;
            return PartialView("Reg_PJ_Affiliated");
        }

        [HttpPost]
        public ActionResult AddNewAttorney(bool ctvm)
        {
            ViewBag.NewAttorney = true;
            ViewBag.CTVM = ctvm;
            return PartialView("Reg_PJ_Attorney");
        }

        [HttpPost]
        public ActionResult AddNewSameAuthorizedAccount()
        {
            ViewBag.NewAuthorizedAccountSame = true;
            return PartialView("Reg_PJ_Authorization");
        }

        [HttpPost]
        public ActionResult AddNewOtherAuthorizedAccount()
        {
            ViewBag.NewAuthorizedAccountOthers = true;
            return PartialView("Reg_PJ_Authorization");
        }

        [HttpPost]
        public ActionResult AddNewAuthorizedMember(bool ctvm) 
        {
            ViewBag.NewAuthorizedMember = true;
            ViewBag.CTVM = ctvm;
            return PartialView("Reg_PJ_AuthorizationAccounts");
        }

        [HttpPost]
        public ActionResult AddNewPPE()
        {
            ViewBag.NewPPE = true;
            return PartialView("Reg_PJ_FinalDisposition");
        }

        #region Arquivos em Pdf
        public ActionResult PFToFile() 
        {
            var model = Session["ClientRegistration.IndividualPerson"] != null ?
                (IndividualPerson)Session["ClientRegistration.IndividualPerson"]
                : new IndividualPerson();

            System.IO.MemoryStream memPDF = new PDFHelper().GeneratePDFForIndividualPerson(model, RegistrationType.PF);
            byte[] buf = new byte[memPDF.Length];
            memPDF.Position = 0;
            memPDF.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/pdf", true, "ficha_cadastral_pf1.pdf");

        }

        public ActionResult PF2ToFile()
        {
            var model = Session["ClientRegistration.IndividualPerson"] != null ?
                (IndividualPerson)Session["ClientRegistration.IndividualPerson"]
                : new IndividualPerson();

            System.IO.MemoryStream memPDF = new PDFHelper().GeneratePDFForIndividualPerson(model, RegistrationType.PF2);
            byte[] buf = new byte[memPDF.Length];
            memPDF.Position = 0;
            memPDF.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/pdf", true, "ficha_cadastral_pf2.pdf");

        }

        public ActionResult PFCTVMToFile()
        {
            var model = Session["ClientRegistration.IndividualPerson"] != null ?
                (IndividualPerson)Session["ClientRegistration.IndividualPerson"]
                : new IndividualPerson();

            System.IO.MemoryStream memPDF = new PDFHelper().GeneratePDFForIndividualPerson(model, RegistrationType.PFCTVM);
            byte[] buf = new byte[memPDF.Length];
            memPDF.Position = 0;
            memPDF.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/pdf", true, "ficha_cadastral_pfCTVM.pdf");

        }

        public ActionResult PJToFile()
        {
            var model = Session["ClientRegistration.LegalPerson"] != null ?
                (LegalPerson)Session["ClientRegistration.LegalPerson"]
                : new LegalPerson();

            System.IO.MemoryStream memPDF = new PDFHelper().GeneratePDFForLegalPerson(model, RegistrationType.PJ);
            byte[] buf = new byte[memPDF.Length];
            memPDF.Position = 0;
            memPDF.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/pdf", true, "ficha_cadastral_pj1.pdf");

        }

        public ActionResult PJ2ToFile()
        {
            var model = Session["ClientRegistration.LegalPerson"] != null ?
                (LegalPerson)Session["ClientRegistration.LegalPerson"]
                : new LegalPerson();

            System.IO.MemoryStream memPDF = new PDFHelper().GeneratePDFForLegalPerson(model, RegistrationType.PJ2);
            byte[] buf = new byte[memPDF.Length];
            memPDF.Position = 0;
            memPDF.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/pdf", true, "ficha_cadastral_pj2.pdf");

        }

        public ActionResult PJCTVMToFile()
        {
            var model = Session["ClientRegistration.LegalPerson"] != null ?
                (LegalPerson)Session["ClientRegistration.LegalPerson"]
                : new LegalPerson();

            System.IO.MemoryStream memPDF = new PDFHelper().GeneratePDFForLegalPerson(model, RegistrationType.PJCTVM);
            byte[] buf = new byte[memPDF.Length];
            memPDF.Position = 0;
            memPDF.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/pdf", true, "ficha_cadastral_pjCTVM.pdf");

        }
        #endregion

        [HttpPost]
        public ActionResult RegistrationFilter(string clientCode, string type) 
        {
            var id = Convert.ToInt32(clientCode);
            clientCode = new MarketHelper().GetClientsRangesToFilter(clientCode);
            if (type == null) type = "N";
            using (var service = ChannelProviderFactory<IClientRegistrationContractChannel>.Create("ClientRegistrationClassName").GetChannel().CreateChannel()) 
            {
                var clientType = service.CheckClientType(clientCode, "", CommonHelper.GetLoggedUserGroupId());

                if (clientType.Result == "N")
                    return Json(new { Result = false, Message = "Cliente não encontrado." });
                else //if (clientType.Result == "F")
                {
                    var model = service.LoadIndividualPerson(id).Result;

                    Session["ClientRegistration.IndividualPerson"] = model;

                    return Json(new { Result = model != null, Location = type == "N" ? "/ClientRegistration/Registration" : "/ClientRegistration/RegistrationPFCTVM" });
                }
            }
        }

        [HttpGet]
        public ActionResult GoToRegistration(string id)
        {
            var clientCodeId = Convert.ToInt32(id);
            var clientCode = new MarketHelper().GetClientsRangesToFilter(id);

            using (var service = ChannelProviderFactory<IClientRegistrationContractChannel>.Create("ClientRegistrationClassName").GetChannel().CreateChannel())
            {
                var clientType = service.CheckClientType(clientCode, "", CommonHelper.GetLoggedUserGroupId());

                if (clientType.Result == "N")
                {
                    TempData["ErrorMessage"] = "Não foi possível acessar o cadastro desse cliente";
                    return Redirect("/ClientRegistration/OverdueAndDue");
                }
                else //if (clientType.Result == "F")
                {
                    var model = service.LoadIndividualPerson(clientCodeId).Result;

                    Session["ClientRegistration.IndividualPerson"] = model;

                    if (model != null)
                        return Redirect("/ClientRegistration/Registration");
                    else
                    {
                        TempData["ErrorMessage"] = "Não foi possível acessar o cadastro desse cliente";
                        return Redirect("/ClientRegistration/OverdueAndDue");
                    }

                }
            }
        }

        [HttpPost]
        public ActionResult GetRegistrationClient(string cpfcgc) 
        {
            cpfcgc = cpfcgc.Replace(".", "-").Replace("-", "");

            using (var service = ChannelProviderFactory<IClientRegistrationContractChannel>.Create("ClientRegistrationClassName").GetChannel().CreateChannel())
            {
                var response = service.CheckClientPerCPFCGC(cpfcgc);
                return Json(new { Result = response.Result, Data = response.Data });
            }
        }

        [AccessControlAuthorize]
        public ActionResult Registration() 
        {
        
            var model = Session["ClientRegistration.IndividualPerson"] != null ?
                (IndividualPerson)Session["ClientRegistration.IndividualPerson"]
                : new IndividualPerson();
            this.LoadStates("ddlDocumentState", null);
            return View(model);

        }

        [HttpPost]
        public ActionResult Back_PF_Phase1() {
            var model = Session["ClientRegistration.IndividualPerson"] != null ?
                (IndividualPerson)Session["ClientRegistration.IndividualPerson"]
                : new IndividualPerson();

            return PartialView("Reg_PF1_Passo1", model);
        }

        [HttpPost]
        public ActionResult Back_PF_Phase2()
        {
            var model = Session["ClientRegistration.IndividualPerson"] != null ?
                (IndividualPerson)Session["ClientRegistration.IndividualPerson"]
                : new IndividualPerson();

            return PartialView("Reg_PF1_Passo2", model);
        }

        [AccessControlAuthorize]
        public ActionResult RegistrationPJ() {
            var model = Session["ClientRegistration.LegalPerson"] != null ?
                (LegalPerson)Session["ClientRegistration.LegalPerson"]
                : new LegalPerson();
            return View(model);
        }

        [HttpPost]
        public ActionResult Back_PJ_Phase1() 
        {
            var model = Session["ClientRegistration.LegalPerson"] != null ?
                (LegalPerson)Session["ClientRegistration.LegalPerson"]
                : new LegalPerson();

            return PartialView("Reg_PJ1_Passo1", model);
        }

        [HttpPost]
        public ActionResult Back_PJCTVM_Phase1()
        {
            var model = Session["ClientRegistration.LegalPerson"] != null ?
                (LegalPerson)Session["ClientRegistration.LegalPerson"]
                : new LegalPerson();

            return PartialView("Reg_PJCTVM_Passo1", model);
        }

        [HttpPost]
        public ActionResult Back_PFCTVM_Phase1()
        {
            var model = Session["ClientRegistration.IndividualPerson"] != null ?
                (IndividualPerson)Session["ClientRegistration.IndividualPerson"]
                : new IndividualPerson();

            return PartialView("Reg_PFCTVM_Passo1", model);
        }

        [AccessControlAuthorize]
        public ActionResult RegistrationPFCTVM() 
        {
            var model = Session["ClientRegistration.IndividualPerson"] != null ?
                (IndividualPerson)Session["ClientRegistration.IndividualPerson"]
                : new IndividualPerson();

            return View(model);
        }

        [AccessControlAuthorize]
        public ActionResult RegistrationPJCTVM()
        {

            var model = Session["ClientRegistration.LegalPerson"] != null ?
                (LegalPerson)Session["ClientRegistration.LegalPerson"]
                : new LegalPerson();

            return View(model);
        }

        [HttpPost]
        public ActionResult Process_PF_Phase1(FormCollection form) 
        {
            this.LoadStates("ddlAddressState", null);
            var model = Session["ClientRegistration.IndividualPerson"] != null ? (IndividualPerson)Session["ClientRegistration.IndividualPerson"] : new IndividualPerson();

            this._Process_PF_Phase1(form, ref model);

            Session["ClientRegistration.IndividualPerson"] = model;

            return PartialView("Reg_PF1_Passo2", model);
        }

        [HttpPost]
        public ActionResult Process_PF_Phase2(FormCollection form)
        {
            var model = Session["ClientRegistration.IndividualPerson"] != null ? (IndividualPerson)Session["ClientRegistration.IndividualPerson"] : new IndividualPerson();

            this._Process_PF_Phase2(form, ref model);

            Session["ClientRegistration.IndividualPerson"] = model;

            return PartialView("Reg_PF1_Passo3", model);
        }

        [HttpPost]
        public ActionResult GetOtherPatrimony() 
        { 
            var model = Session["ClientRegistration.IndividualPerson"] != null ? (IndividualPerson)Session["ClientRegistration.IndividualPerson"] : new IndividualPerson();

            if (model != null)
            {
                return Json(new
                {
                    OtherEstimatedPatrimony = model.EstimatedPatrimony != null ? model.EstimatedPatrimony.Other.HasValue ? model.EstimatedPatrimony.Other.Value.ToString("N2") : "": "",
                    OtherPatrimonyOrigin = model.OtherOrigins,
                    OtherLiquidity = model.Liquidity != null ? model.Liquidity.Other.HasValue ? model.Liquidity.Other.Value.ToString("N2") : "" : "",
                    OtherAnnualIncome = model.AnnualIncome != null ? model.AnnualIncome.Other.HasValue ? model.AnnualIncome.Other.Value.ToString("N2") : "" : "",
                    OtherTransaction = model.Transaction != null ? model.Transaction.Other.HasValue ? model.Transaction.Other.Value.ToString("N2") : "" : "",
                    OtherInvestment = model.Investment != null ? model.Investment.Other.HasValue ? model.Investment.Other.Value.ToString("N2") : "" : "",
                });
            }
            else {
                return Json(new
                {
                    OtherEstimatedPatrimony = "",
                    OtherPatrimonyOrigin = "",
                    OtherLiquidity = "",
                    OtherAnnualIncome = "",
                    OtherTransaction = "",
                    OtherInvestment = "",
                });
            }
        }

        [HttpPost]
        public ActionResult Process_PF_Phase3(FormCollection form)
        {
            var model = Session["ClientRegistration.IndividualPerson"] != null ? (IndividualPerson)Session["ClientRegistration.IndividualPerson"] : new IndividualPerson();

            this._Process_PF_Phase3(form, ref model);

            Session["ClientRegistration.IndividualPerson"] = model;

            return Json(new { Result = true });
        }

        [HttpPost]
        public ActionResult Process_PF2(FormCollection form)
        {
            var model = Session["ClientRegistration.IndividualPerson"] != null ? (IndividualPerson)Session["ClientRegistration.IndividualPerson"] : new IndividualPerson();

            this._Process_PF_Phase1(form, ref model);
            this._Process_PF_Phase2(form, ref model);
            this._Process_PF_Phase3(form, ref model);

            this._Process_PF2(form, ref model);

            Session["ClientRegistration.IndividualPerson"] = model;

            return Json(new { Result = true });
        }

        protected void _Process_PF_Phase1(FormCollection form, ref IndividualPerson model) 
        {
            #region Informações básicas

            if (this.CheckFormCollection(form, "txtCPF"))
                model.CPF = form["txtCPF"];

            if (this.CheckFormCollection(form, "txtName"))
                model.Name = form["txtName"];

            if (this.CheckFormCollection(form, "txtBirthDate") && !String.IsNullOrEmpty(form["txtBirthDate"]))
                model.BirthDate = Convert.ToDateTime(form["txtBirthDate"]);

            if (this.CheckFormCollection(form, "txtNationality"))
                model.Nationality = form["txtNationality"];

            if (this.CheckFormCollection(form, "txtNaturality"))
                model.Naturality = form["txtNaturality"];

            if (this.CheckFormCollection(form, "rdbGender") && !String.IsNullOrEmpty(form["rdbGender"]))
                model.Gender = form["rdbGender"] == "M" ? Gender.Male : Gender.Female;

            if (this.CheckFormCollection(form, "txtFatherName"))
                model.FatherName = form["txtFatherName"];

            if (this.CheckFormCollection(form, "txtMotherName"))
                model.MotherName = form["txtMotherName"];

            #endregion

            #region Documents
            
            if (model.Document == null) model.Document = new DocumentInfo();

            if (CheckFormCollection(form, "ddlDocumentType") && !String.IsNullOrEmpty(form["ddlDocumentType"]))
                model.Document.DocumentType = (DocumentType)Convert.ToInt32(form["ddlDocumentType"]);

            if (this.CheckFormCollection(form, "txtDocumentNumber"))
                model.Document.DocumentNumber = form["txtDocumentNumber"];

            if (this.CheckFormCollection(form, "txtEmissorDate") && !String.IsNullOrEmpty(form["txtEmissorDate"]))
                model.Document.EmissorDate = Convert.ToDateTime(form["txtEmissorDate"]);

            if (this.CheckFormCollection(form, "txtEmissor"))
                model.Document.Emissor = form["txtEmissor"];

            if (this.CheckFormCollection(form, "ddlDocumentState0") && !String.IsNullOrEmpty(form["ddlDocumentState0"]))
                model.Document.DocumentState = (State)Convert.ToInt32(form["ddlDocumentState0"]);

            #endregion

            #region CivilState
            
            if (model.CivilState == null) model.CivilState = new CivilState();

            if (this.CheckFormCollection(form, "ddlCivilStateType") && !String.IsNullOrEmpty(form["ddlCivilStateType"]))
                model.CivilState.CivilStateType = (CivilStateType)Convert.ToInt32(form["ddlCivilStateType"]);

            if (this.CheckFormCollection(form, "ddlPropertyRegime") && !String.IsNullOrEmpty(form["ddlPropertyRegime"]))
                model.CivilState.PropertyRegime = (PropertyRegime)Convert.ToInt32(form["ddlPropertyRegime"]);

            if (this.CheckFormCollection(form, "txtSpouseName"))
                model.CivilState.SpouseName = form["txtSpouseName"];

            if (this.CheckFormCollection(form, "txtSpouseCPF"))
                model.CivilState.SpouseCPF = form["txtSpouseCPF"];

            #endregion

            #region investor - PPE
                
            if (this.CheckFormCollection(form, "rdbPPE") && !String.IsNullOrEmpty(form["rdbPPE"]))
                model.PPE = form["rdbPPE"] == "S";

            if (this.CheckFormCollection(form, "rdbQualifiedInvestor") && !String.IsNullOrEmpty(form["rdbQualifiedInvestor"]))
                model.QualifiedInvestor = form["rdbQualifiedInvestor"] == "S";
            
            #endregion
        }

        protected void _Process_PF_Phase2(FormCollection form, ref IndividualPerson model)
        {
            #region Address

            var list = form.AllKeys.Where(a => a.Contains("ddlAddressType_")).ToList();
            if (list != null && list.Count > 0)
            {
                model.Addresses = new List<PersonAddress>();

                foreach (var s in list)
                {
                    var i = s.Replace("ddlAddressType_", "");

                    PersonAddress address = new PersonAddress();

                    if (this.CheckFormCollection(form, "ddlAddressType_" + i) && !String.IsNullOrEmpty(form["ddlAddressType_" + i]))
                        address.Type = (AddressType)Convert.ToInt32(form["ddlAddressType_" + i]);

                    if (this.CheckFormCollection(form, "txtAddressLine_" + i) && !String.IsNullOrEmpty(form["txtAddressLine_" + i]))
                        address.AddressLine1 = form["txtAddressLine_" + i];

                    if (this.CheckFormCollection(form, "txtQuarter_" + i) && !String.IsNullOrEmpty(form["txtQuarter_" + i]))
                        address.Quarter = form["txtQuarter_" + i];

                    if (this.CheckFormCollection(form, "txtCity_" + i) && !String.IsNullOrEmpty(form["txtCity_" + i]))
                        address.City = form["txtCity_" + i];

                    if (this.CheckFormCollection(form, "ddlAddressState_" + i) && !String.IsNullOrEmpty(form["ddlAddressState_" + i]))
                        address.State = (State)Convert.ToInt32(form["ddlAddressState_" + i]);

                    if (this.CheckFormCollection(form, "txtPostalCode1_" + i) && this.CheckFormCollection(form, "txtPostalCode2_" + i)
                        && (!String.IsNullOrEmpty(form["txtPostalCode1_" + i]) && !String.IsNullOrEmpty(form["txtPostalCode2_" + i])))
                        address.PostalCode = form["txtPostalCode1_" + i] + "-" + form["txtPostalCode2_" + i];

                    if (this.CheckFormCollection(form, "txtStateCountry_" + i) && !String.IsNullOrEmpty(form["txtStateCountry_" + i]))
                        address.StateCountry = form["txtStateCountry_" + i];

                    model.Addresses.Add(address);
                }
            }

            if (this.CheckFormCollection(form, "ddlPersonAddressType") && !string.IsNullOrEmpty(form["ddlPersonAddressType"]))
                model.PersonalAddressType = (AddressType)Convert.ToInt32(form["ddlPersonAddressType"]);

            #endregion

            #region Contacts

            if (this.CheckFormCollection(form, "txtResidentialPhone"))
            {
                model.Phones = new List<Phone>();

                if (this.CheckFormCollection(form, "txtResidentialPhone") && !string.IsNullOrEmpty(form["txtResidentialPhone"]) && form["txtResidentialPhone"].Length == 14)
                    model.Phones.Add(
                        new Phone()
                        {
                            PhoneNumber = form["txtResidentialPhone"].Substring(5, 9),
                            DDDCode = Convert.ToInt32(form["txtResidentialPhone"].Substring(1, 2)),
                            ContactType = ContactType.ResidentialPhone
                        });

                if (this.CheckFormCollection(form, "txtCellularPhone") && !string.IsNullOrEmpty(form["txtCellularPhone"]) && form["txtCellularPhone"].Length == 14)
                    model.Phones.Add(
                        new Phone()
                        {
                            PhoneNumber = form["txtCellularPhone"].Substring(5, 9),
                            DDDCode = Convert.ToInt32(form["txtCellularPhone"].Substring(1, 2)),
                            ContactType = ContactType.CellularPhone
                        });

                if (this.CheckFormCollection(form, "txtComercialPhone") && !string.IsNullOrEmpty(form["txtComercialPhone"]) && form["txtComercialPhone"].Length == 14)
                    model.Phones.Add(
                        new Phone()
                        {
                            PhoneNumber = form["txtComercialPhone"].Substring(5, 9),
                            DDDCode = Convert.ToInt32(form["txtComercialPhone"].Substring(1, 2)),
                            ContactType = ContactType.ComercialPhone
                        });

                if (this.CheckFormCollection(form, "txtFaxPhone") && !string.IsNullOrEmpty(form["txtFaxPhone"]) && form["txtFaxPhone"].Length == 14)
                    model.Phones.Add(
                        new Phone()
                        {
                            PhoneNumber = form["txtFaxPhone"].Substring(5, 9),
                            DDDCode = Convert.ToInt32(form["txtFaxPhone"].Substring(1, 2)),
                            ContactType = ContactType.FaxPhone
                        });

                if (this.CheckFormCollection(form, "txtProfessionalMail"))
                    model.ProfessionalMail = form["txtProfessionalMail"];

                if (this.CheckFormCollection(form, "txtPersonalMail"))
                    model.PersonalMail = form["txtPersonalMail"];

                if (this.CheckFormCollection(form, "ddlPreferencialContact") && !string.IsNullOrEmpty(form["ddlPreferencialContact"]))
                    model.PersonalChoiceContact = (ContactType)Convert.ToInt32(form["ddlPreferencialContact"]);
            }

            #endregion

            #region Profissão

            if (model.Occupation == null) model.Occupation = new Profession();

            if (this.CheckFormCollection(form, "rdbBestProfessionalOption") && !string.IsNullOrEmpty(form["rdbBestProfessionalOption"]))
                model.Occupation.BestProfessionalOption = (BestProfessionalOption)Convert.ToInt32(form["rdbBestProfessionalOption"]);

            if (this.CheckFormCollection(form, "txtOccupation"))
                model.Occupation.Occupation = form["txtOccupation"];

            if (this.CheckFormCollection(form, "txtRole"))
                model.Occupation.Role = form["txtRole"];

            if (this.CheckFormCollection(form, "txtCompany"))
                model.Occupation.Company = form["txtCompany"];

            if (this.CheckFormCollection(form, "txtPropertyCNPJ"))
                model.Occupation.PropertyCNPJ = form["txtPropertyCNPJ"];
            
            #endregion
        }

        protected void _Process_PF_Phase3(FormCollection form, ref IndividualPerson model)
        {
            #region contas bancárias
         
            var list = form.AllKeys.Where(a => a.Contains("txtBankCode_")).ToList();

            if (list != null && list.Count > 0)
            {
                model.DepositAccounts = new List<AuthorizedAccount>();

                foreach (var s in list)
                {
                    var i = s.Replace("txtBankCode_", "");

                    AuthorizedAccount account = new AuthorizedAccount();
                    account.Account = new BankAccount();

                    if (this.CheckFormCollection(form, "txtBankCode_" + i))
                        account.Account.BankCode = form["txtBankCode_" + i];

                    if (this.CheckFormCollection(form, "txtBankAgency_" + i))
                        account.Account.AgencyCode = form["txtBankAgency_" + i];

                    if (this.CheckFormCollection(form, "txtBankAccount_" + i))
                        account.Account.Account = form["txtBankAccount_" + i];

                    if (this.CheckFormCollection(form, "txtBankCoTitular_" + i))
                        account.CoTitularName = form["txtBankCoTitular_" + i];

                    if (this.CheckFormCollection(form, "txtBankCoCPF_" + i))
                        account.CoCPF = form["txtBankCoCPF_" + i];

                    /*
                    <td>@Html.TextBox("txtBankCoTitular_" + next, account.CoTitularName)</td>
                    <td @displayClassPFCTVM >@Html.TextBox("txtBankCoCPF_" + next, account.CoCPF, new { @class = "txt-cpf" })
                     */

                    model.DepositAccounts.Add(account);
                }
            }
            #endregion

            if (this.CheckFormCollection(form, "txtTotalPatrimony") && !String.IsNullOrEmpty(form["txtTotalPatrimony"]))
                model.TotalPatrimony = Convert.ToDecimal(form["txtTotalPatrimony"]);

            #region Referencias

            var listReferences = form.AllKeys.Where(a => a.Contains("txtRefType_")).ToList();

            if (listReferences != null && listReferences.Count > 0)
            {
                model.References = new List<References>();

                foreach (var s in listReferences)
                {
                    var i = s.Replace("txtRefType_", "");

                    References refer = new References();

                    if (this.CheckFormCollection(form, "txtRefType_" + i))
                        refer.ReferType = (ReferenceType)Convert.ToInt32(form["txtRefType_" + i]);

                    if (this.CheckFormCollection(form, "txtRefName_" + i))
                        refer.Name = form["txtRefName_" + i];

                    if (this.CheckFormCollection(form, "txtRefPhone_" + i) && !String.IsNullOrEmpty(form["txtRefPhone_" + i]) && form["txtRefPhone_" + i].Length == 14)
                    {
                        refer.PhoneCode = Convert.ToInt32(form["txtRefPhone_" + i].Substring(1, 2));
                        refer.PhoneNumber = form["txtRefPhone_" + i].Substring(5, 9);
                    }

                    model.References.Add(refer);
                }
            }

            #endregion

            #region Patrimônio

            if (this.CheckFormCollection(form, "rdbEstimatedPatrimony"))
            {
                model.EstimatedPatrimony = new EstimatedPatrimony();

                model.EstimatedPatrimony.Yield = (Yield)Convert.ToInt32(form["rdbEstimatedPatrimony"]);

                if (!String.IsNullOrEmpty(form["txtOtherEstimatedPatrimony"])) 
                {
                    if (Convert.ToDecimal(form["txtOtherEstimatedPatrimony"]) > 0)
                        model.EstimatedPatrimony.Other = Convert.ToDecimal(form["txtOtherEstimatedPatrimony"]);
                }
            }

            if (this.CheckFormCollection(form, "chkEstimatedPatrimonyComposition"))
            {
                model.EstimatedPatrimonyComposition = new EstimatedPatrimonyComposition();

                if (!String.IsNullOrEmpty(form["txtEPC0"])) model.EstimatedPatrimonyComposition.ApplicationPerc = Convert.ToDecimal(form["txtEPC0"]);
                if (!String.IsNullOrEmpty(form["txtEPC1"])) model.EstimatedPatrimonyComposition.HabitationPerc = Convert.ToDecimal(form["txtEPC1"]);
                if (!String.IsNullOrEmpty(form["txtEPC2"])) model.EstimatedPatrimonyComposition.LandPerc = Convert.ToDecimal(form["txtEPC2"]);
                if (!String.IsNullOrEmpty(form["txtEPC3"])) model.EstimatedPatrimonyComposition.CorporatePerc = Convert.ToDecimal(form["txtEPC3"]);
                if (!String.IsNullOrEmpty(form["txtEPC4"])) model.EstimatedPatrimonyComposition.OtherPerc = Convert.ToDecimal(form["txtEPC4"]);
            }

            if (this.CheckFormCollection(form, "chkPatrimonyOrigin"))
            {
                model.PatrimonyOrigins = new List<PatrimonyOrigin>();
                var patrimony = form["chkPatrimonyOrigin"].Split(',');
                foreach (var p in patrimony) 
                {
                    model.PatrimonyOrigins.Add((PatrimonyOrigin)Convert.ToInt32(p));
                }

                if (!String.IsNullOrEmpty(form["txtOtherPatrimonyOrigin"]))
                        model.OtherOrigins = form["txtOtherPatrimonyOrigin"];
            }

            if (this.CheckFormCollection(form, "rdbLiquidity"))
            {
                model.Liquidity = new Liquidity();

                model.Liquidity.Value = (LiquidityValues)Convert.ToInt32(form["rdbLiquidity"]);

                if (!String.IsNullOrEmpty(form["txtOtherLiquidity"]))
                {
                    if (Convert.ToDecimal(form["txtOtherLiquidity"]) > 0)
                        model.Liquidity.Other = Convert.ToDecimal(form["txtOtherLiquidity"]);
                }
            }

            if (this.CheckFormCollection(form, "rdbInvestment"))
            {
                model.Investment = new Investment();

                model.Investment.Value = (InvestmentValues)Convert.ToInt32(form["rdbInvestment"]);

                if (!String.IsNullOrEmpty(form["txtOtherInvestment"]))
                {
                    if (Convert.ToDecimal(form["txtOtherInvestment"]) > 0)
                        model.Investment.Other = Convert.ToDecimal(form["txtOtherInvestment"]);
                }
            }

            if (this.CheckFormCollection(form, "rdbAnnualIncome"))
            {
                model.AnnualIncome = new AnnualIncome();

                model.AnnualIncome.Value = (AnnualIncomeValues)Convert.ToInt32(form["rdbAnnualIncome"]);

                if (!String.IsNullOrEmpty(form["txtOtherAnnualIncome"]))
                {
                    if (Convert.ToDecimal(form["txtOtherAnnualIncome"]) > 0)
                        model.AnnualIncome.Other = Convert.ToDecimal(form["txtOtherAnnualIncome"]);
                }
            }

            if (this.CheckFormCollection(form, "rdbTransaction"))
            {
                model.Transaction = new Transaction();

                model.Transaction.Value = (TransactionValues)Convert.ToInt32(form["rdbTransaction"]);

                if (!String.IsNullOrEmpty(form["txtOtherTransaction"]))
                {
                    if (Convert.ToDecimal(form["txtOtherTransaction"]) > 0)
                        model.Transaction.Other = Convert.ToDecimal(form["txtOtherTransaction"]);
                }
            }
            #endregion

        }

        protected void _Process_PF2(FormCollection form, ref IndividualPerson model) 
        {
            #region Administrador
            if (this.CheckFormCollection(form, "txtAdministrator"))
            {
                model.Administrator = form["txtAdministrator"];
            }

            if (this.CheckFormCollection(form, "rdbNonResidentInvestor"))
            {
                if (String.IsNullOrEmpty(form["rdbNonResidentInvestor"]))
                    model.NonResidentInvestor = null;
                else
                    model.NonResidentInvestor = form["rdbNonResidentInvestor"] == "S";
            }

            if (this.CheckFormCollection(form, "rdbCollectiveAccount"))
            {
                if (String.IsNullOrEmpty(form["rdbCollectiveAccount"]))
                    model.CollectiveAccount = null;
                else
                    model.CollectiveAccount = form["rdbCollectiveAccount"] == "S";
            }

            if (form["rdbCollectiveAccount"] == "S")
            {
                if (this.CheckFormCollection(form, "txtCollectiveAccountTitular"))
                    model.CollectiveAccountTitular = form["txtCollectiveAccountTitular"];
            }
            else {
                model.CollectiveAccountTitular = "";
            }

            if (form["rdbNonResidentInvestor"] == "S")
            {
                if (this.CheckFormCollection(form, "txtCVM"))
                    model.CVM = form["txtCVM"];

                if (this.CheckFormCollection(form, "txtRDE"))
                    model.RDE = form["txtRDE"];

                if (this.CheckFormCollection(form, "txtOriginCountry"))
                    model.OriginCountry = form["txtOriginCountry"];

                if (this.CheckFormCollection(form, "txtCustodian"))
                    model.Custodian = form["txtCustodian"];

                if (this.CheckFormCollection(form, "txtLegalRepresentative"))
                    model.LegalRepresentative = form["txtLegalRepresentative"];

                if (this.CheckFormCollection(form, "txtTributeRepresentative"))
                    model.TributeRepresentative = form["txtTributeRepresentative"];

                if (this.CheckFormCollection(form, "txtCoLegalRepresentative"))
                    model.CoLegalRepresentative = form["txtCoLegalRepresentative"];
            }
            else 
            {
                model.CVM = ""; model.RDE = ""; model.OriginCountry = ""; model.Custodian = ""; model.LegalRepresentative = ""; model.TributeRepresentative = ""; model.CoLegalRepresentative = "";
            }

            #endregion

            #region Imobilegoods

            var list = form.AllKeys.Where(a => a.Contains("ddlImobileType_")).ToList();

            decimal total = 0;

            if (list != null && list.Count > 0)
            {
                model.ImobileGoods = new List<ImobileGood>();

                foreach (var s in list)
                {
                    var i = s.Replace("ddlImobileType_", "");

                    ImobileGood imobile = new ImobileGood();
                    imobile.Address = new PersonAddress();

                    if (this.CheckFormCollection(form, "ddlImobileType_" + i) && !String.IsNullOrEmpty(form["ddlImobileType_" + i]))
                        imobile.Type = (GoodKind)Convert.ToInt32(form["ddlImobileType_" + i]);

                    if (this.CheckFormCollection(form, "txtImobileValue_" + i) && !String.IsNullOrEmpty(form["txtImobileValue_" + i]))
                    {
                        imobile.Value = Convert.ToDecimal(form["txtImobileValue_" + i]);
                        total += imobile.Value.Value;
                    }

                    if (this.CheckFormCollection(form, "txtImobileAddressLine_" + i) && !String.IsNullOrEmpty(form["txtImobileAddressLine_" + i]))
                        imobile.Address.AddressLine1 = form["txtImobileAddressLine_" + i];

                    if (this.CheckFormCollection(form, "txtImobileQuarter_" + i) && !String.IsNullOrEmpty(form["txtImobileQuarter_" + i]))
                        imobile.Address.Quarter = form["txtImobileQuarter_" + i];

                    if (this.CheckFormCollection(form, "txtImobileCity_" + i) && !String.IsNullOrEmpty(form["txtImobileCity_" + i]))
                        imobile.Address.City = form["txtImobileCity_" + i];

                    if (this.CheckFormCollection(form, "txtImobilePostalCode1_" + i) && this.CheckFormCollection(form, "txtImobilePostalCode2_" + i)
                            && (!String.IsNullOrEmpty(form["txtImobilePostalCode1_" + i]) && !String.IsNullOrEmpty(form["txtImobilePostalCode2_" + i])))
                        imobile.Address.PostalCode = form["txtImobilePostalCode1_" + i] + "-" + form["txtImobilePostalCode2_" + i];

                    if (this.CheckFormCollection(form, "ddlImobileAddressState_" + i) && !String.IsNullOrEmpty(form["ddlImobileAddressState_" + i]))
                        imobile.Address.State = (State)Convert.ToInt32(form["ddlImobileAddressState_" + i]);

                    if (this.CheckFormCollection(form, "txtImobileStateCountry_" + i) && !String.IsNullOrEmpty(form["txtImobileStateCountry_" + i]))
                        imobile.Address.StateCountry = form["txtImobileStateCountry_" + i];

                    model.ImobileGoods.Add(imobile);
                }
            }
            
            #endregion

            #region Mobile Goods
                        
            var listMobile = form.AllKeys.Where(a => a.Contains("txtMobileType_")).ToList();

            if (listMobile != null && listMobile.Count > 0)
            {
                model.MobileGoods = new List<MobileGood>();

                foreach (var s in listMobile)
                {
                    var i = s.Replace("txtMobileType_", "");

                    MobileGood mobile = new MobileGood();

                    if (this.CheckFormCollection(form, "txtMobileType_" + i) && !String.IsNullOrEmpty(form["txtMobileType_" + i]))
                        mobile.Type = (GoodKind)Convert.ToInt32(form["txtMobileType_" + i]);

                    if (this.CheckFormCollection(form, "txtMobileValue_" + i) && !String.IsNullOrEmpty(form["txtMobileValue_" + i]))
                    {
                        mobile.Value = Convert.ToDecimal(form["txtMobileValue_" + i]);
                        total += mobile.Value.Value;
                    }

                    if (this.CheckFormCollection(form, "txtMobileDescription_" + i) && !String.IsNullOrEmpty(form["txtMobileDescription_" + i]))
                        mobile.Description = form["txtMobileDescription_" + i];


                    model.MobileGoods.Add(mobile);
                }
            }

            #endregion

            #region Rendimento mensal

            var listMensal = form.AllKeys.Where(a => a.Contains("txtMensalType_")).ToList();

            if (listMensal != null && listMensal.Count > 0)
            {
                model.MensalRendiments = new List<MensalRendiment>();

                foreach (var s in listMensal)
                {
                    var i = s.Replace("txtMensalType_", "");

                    MensalRendiment mensal = new MensalRendiment();

                    if (this.CheckFormCollection(form, "txtMensalType_" + i) && !String.IsNullOrEmpty(form["txtMensalType_" + i]))
                        mensal.Type = (MensalRendimentType)Convert.ToInt32(form["txtMensalType_" + i]);

                    if (this.CheckFormCollection(form, "txtMensalDescription_" + i) && !String.IsNullOrEmpty(form["txtMensalDescription_" + i]))
                        mensal.Description = form["txtMensalDescription_" + i];

                    if (this.CheckFormCollection(form, "txtMensalValue_" + i) && !String.IsNullOrEmpty(form["txtMensalValue_" + i]))
                    {
                        mensal.Value = Convert.ToDecimal(form["txtMensalValue_" + i]);
                        total += mensal.Value.Value;
                    }

                    model.MensalRendiments.Add(mensal);
                }
            }

            if (total > 0)
                model.TotalPatrimony = total;

            #endregion

            #region Autorizações
            if (this.CheckFormCollection(form, "rdbOperatesOnItsOwn"))
            {
                if (String.IsNullOrEmpty(form["rdbOperatesOnItsOwn"]))
                    model.OperatesOnItsOwn = null;
                else
                    model.OperatesOnItsOwn = form["rdbOperatesOnItsOwn"] == "S";
            }

            if (this.CheckFormCollection(form, "rdbAuthorizesTransmissionOrders"))
            {
                if (String.IsNullOrEmpty(form["rdbAuthorizesTransmissionOrders"]))
                    model.AuthorizesTransmissionOrders = null;
                else
                    model.AuthorizesTransmissionOrders = form["rdbAuthorizesTransmissionOrders"] == "S";

                if (form["rdbAuthorizesTransmissionOrders"] == "S") 
                {
                    
                    var listAuthorizedMembers = form.AllKeys.Where(a => a.Contains("txtAuthorizedMemberName_")).ToList();

                    if (listAuthorizedMembers != null && listAuthorizedMembers.Count > 0)
                    {
                        model.AuthorizedMembers = new List<AuthorizedMember>();

                        foreach (var s in listAuthorizedMembers)
                        {
                            var i = s.Replace("txtAuthorizedMemberName_", "");

                            AuthorizedMember member = new AuthorizedMember();

                            if (this.CheckFormCollection(form, "txtAuthorizedMemberName_" + i) && !String.IsNullOrEmpty(form["txtAuthorizedMemberName_" + i]))
                                member.Name = form["txtAuthorizedMemberName_" + i];

                            if (this.CheckFormCollection(form, "txtAuthorizedMemberEmail_" + i) && !String.IsNullOrEmpty(form["txtAuthorizedMemberEmail_" + i]))
                                member.Email = form["txtAuthorizedMemberEmail_" + i];

                            if (this.CheckFormCollection(form, "txtAuthorizedMemberCPF_" + i) && !String.IsNullOrEmpty(form["txtAuthorizedMemberCPF_" + i]))
                                member.CPF = form["txtAuthorizedMemberCPF_" + i];

                            if (this.CheckFormCollection(form, "txtAuthorizedMemberRG_" + i) && !String.IsNullOrEmpty(form["txtAuthorizedMemberRG_" + i]))
                                member.RG = form["txtAuthorizedMemberRG_" + i];

                            if (this.CheckFormCollection(form, "txtAuthorizedMemberBirthDate_" + i) && !String.IsNullOrEmpty(form["txtAuthorizedMemberBirthDate_" + i]))
                                member.BirthDate = Convert.ToDateTime(form["txtAuthorizedMemberBirthDate_" + i]);

                            model.AuthorizedMembers.Add(member);
                        }
                    }
                }
            }

            var listAuthorizations = form.AllKeys.Where(a => a.Contains("txtAuthorizedAccountName_")).ToList();

            if (listAuthorizations != null && listAuthorizations.Count > 0)
            {
                model.AuthorizedAccounts = new List<AuthorizedAccount>();

                foreach (var s in listAuthorizations)
                {
                    var i = s.Replace("txtAuthorizedAccountName_", "");

                    AuthorizedAccount acc = new AuthorizedAccount();
                    acc.Account = new BankAccount();

                    if (this.CheckFormCollection(form, "txtAuthorizedAccountName_" + i))
                        acc.TitularName = form["txtAuthorizedAccountName_" + i];

                    if (this.CheckFormCollection(form, "txtAuthorizedAccountCPF_" + i))
                        acc.CPF = form["txtAuthorizedAccountCPF_" + i];

                    if (this.CheckFormCollection(form, "txtAuthorizedAccountBank_" + i))
                        acc.Account.BankCode = form["txtAuthorizedAccountBank_" + i];

                    if (this.CheckFormCollection(form, "txtAuthorizedAccountAgency_" + i))
                        acc.Account.AgencyCode = form["txtAuthorizedAccountAgency_" + i];

                    if (this.CheckFormCollection(form, "txtAuthorizedAccountNumber_" + i))
                        acc.Account.Account = form["txtAuthorizedAccountNumber_" + i];

                    model.AuthorizedAccounts.Add(acc);
                }
            }

            if (this.CheckFormCollection(form, "rdbBrokerBound"))
            {
                if (String.IsNullOrEmpty(form["rdbBrokerBound"]))
                    model.BrokerBound = null;
                else
                    model.BrokerBound = form["rdbBrokerBound"] == "S";
            }

            if (this.CheckFormCollection(form, "rdbApprovalTerm"))
            {
                if (String.IsNullOrEmpty(form["rdbApprovalTerm"]))
                    model.ApprovalTerm = null;
                else
                    model.ApprovalTerm = (ApprovalTerm)Convert.ToInt32(form["rdbApprovalTerm"]);
            }
            
            #endregion
        }

        //PJ
        [HttpPost]
        public ActionResult Process_PJ1_Phase1(FormCollection form)
        {
            var model = Session["ClientRegistration.LegalPerson"] != null ? (LegalPerson)Session["ClientRegistration.LegalPerson"] : new LegalPerson();
            
            this._Process_PJ_Phase1(form, ref model);

            Session["ClientRegistration.LegalPerson"] = model;

            return PartialView("Reg_PJ1_Passo2", model);
        }

        [HttpPost]
        public ActionResult Process_PJ1_Phase2(FormCollection form)
        {
            var model = Session["ClientRegistration.LegalPerson"] != null ? (LegalPerson)Session["ClientRegistration.LegalPerson"] : new LegalPerson();

            this._Process_PJ_Phase2(form, ref model);

            Session["ClientRegistration.LegalPerson"] = model;

            return Json(new { Result = true });
        }

        [HttpPost]
        public ActionResult Process_PJ2(FormCollection form)
        {
            var model = Session["ClientRegistration.LegalPerson"] != null ? (LegalPerson)Session["ClientRegistration.LegalPerson"] : new LegalPerson();

            this._Process_PJ_Phase1(form, ref model);
            this._Process_PJ_Phase2(form, ref model);
            this._Process_PJ2(form, ref model);

            Session["ClientRegistration.LegalPerson"] = model;

            return Json(new { Result = true });
        }

        protected void _Process_PJ_Phase1(FormCollection form, ref LegalPerson model)
        {
            #region Infomações básicas
            if (this.CheckFormCollection(form, "txtCNPJ"))
                model.CNPJ = form["txtCNPJ"];

            if (this.CheckFormCollection(form, "txtCorporateName"))
                model.CorporateName = form["txtCorporateName"];

            if (this.CheckFormCollection(form, "txtFancyName"))
                model.FancyName = form["txtFancyName"];

            if (this.CheckFormCollection(form, "txtNIRE"))
                model.NIRE = form["txtNIRE"];

            if (this.CheckFormCollection(form, "txtStateInscription"))
                model.StateInscription = form["txtStateInscription"];

            if (this.CheckFormCollection(form, "txtPrincipalActivity"))
                model.PrincipalActivity = form["txtPrincipalActivity"];

            if (this.CheckFormCollection(form, "txtConstitutionDate") && !String.IsNullOrEmpty(form["txtConstitutionDate"]))
                model.ConstitutionDate = Convert.ToDateTime(form["txtConstitutionDate"]);

            if (this.CheckFormCollection(form, "rdbConstitutionForm"))
            {
                if (String.IsNullOrEmpty(form["rdbConstitutionForm"]))
                    model.ConstitutionForm = null;
                else
                    model.ConstitutionForm = (ConstitutionForm)Convert.ToInt32(form["rdbConstitutionForm"]);
            }

            if (this.CheckFormCollection(form, "rdbConstitutionForm") && model.ConstitutionForm.HasValue && model.ConstitutionForm.Value == ConstitutionForm.Other)
                model.ConstitutionFormOtherDescription = form["txtConstitutionFormOtherDescription"];

            if (this.CheckFormCollection(form, "rdbOpenCapital"))
            {
                if (String.IsNullOrEmpty(form["rdbOpenCapital"]))
                    model.OpenCapital = null;
                else
                    model.OpenCapital = form["rdbOpenCapital"] == "S";
            }

            if (this.CheckFormCollection(form, "rdbStockControlType"))
            {
                if (String.IsNullOrEmpty(form["rdbStockControlType"]))
                    model.StockControlType = null;
                else
                    model.StockControlType = (StockControlType)Convert.ToInt32(form["rdbStockControlType"]);
            }

            if (this.CheckFormCollection(form, "rdbCapitalType"))
            {
                if (String.IsNullOrEmpty(form["rdbCapitalType"]))
                    model.CapitalType = null;
                else
                    model.CapitalType = (CapitalType)Convert.ToInt32(form["rdbCapitalType"]);
            }

            if (this.CheckFormCollection(form, "rdbQualifiedInvestor") && !String.IsNullOrEmpty(form["rdbQualifiedInvestor"]))
                model.QualifiedInvestor = form["rdbQualifiedInvestor"] == "S";

            #endregion

            #region address

            PersonAddress address = new PersonAddress();
            if (this.CheckFormCollection(form, "txtAddressLine_0"))
                address.AddressLine1 = form["txtAddressLine_0"];

            if (this.CheckFormCollection(form, "txtSite"))
                address.AddressLine2 = form["txtSite"];

            if (this.CheckFormCollection(form, "txtQuarter_0"))
                address.Quarter = form["txtQuarter_0"];

            if (this.CheckFormCollection(form, "txtCity_0"))
                address.City = form["txtCity_0"];

            if (this.CheckFormCollection(form, "ddlAddressState_0") && !String.IsNullOrEmpty(form["ddlAddressState_0"]))
                address.State = (State)Convert.ToInt32(form["ddlAddressState_0"]);

            if (this.CheckFormCollection(form, "txtPostalCode1_0") && this.CheckFormCollection(form, "txtPostalCode2_0")
                && (!String.IsNullOrEmpty(form["txtPostalCode1_0"]) && !String.IsNullOrEmpty(form["txtPostalCode2_0"])))
                address.PostalCode = form["txtPostalCode1_0"] + "-" + form["txtPostalCode2_0"];

            if (this.CheckFormCollection(form, "txtStateCountry_0"))
            {
                address.StateCountry = form["txtStateCountry_0"];
                model.Address = address;
            }                       

            #endregion

            #region Contacts

            if (this.CheckFormCollection(form, "txtComercialPhone"))
            {
                model.Contacts = new List<Phone>();

                if (!String.IsNullOrEmpty(form["txtComercialPhone"]) && form["txtComercialPhone"].Length == 14)
                {
                    model.Contacts.Add(new Phone()
                    {
                        PhoneNumber = form["txtComercialPhone"].Substring(5, 9),
                        DDDCode = Convert.ToInt32(form["txtComercialPhone"].Substring(1, 2)),
                        ContactType = ContactType.ComercialPhone
                    });
                }
            }

            if (this.CheckFormCollection(form, "txtFaxPhone"))
            {
                if (!String.IsNullOrEmpty(form["txtFaxPhone"]) && form["txtFaxPhone"].Length == 14)
                {
                    model.Contacts.Add(new Phone()
                    {
                        PhoneNumber = form["txtFaxPhone"].Substring(5, 9),
                        DDDCode = Convert.ToInt32(form["txtFaxPhone"].Substring(1, 2)),
                        ContactType = ContactType.FaxPhone
                    });
                }
            }

            if (this.CheckFormCollection(form, "txtEmail1"))
                model.Email1 = form["txtEmail1"];

            if (this.CheckFormCollection(form, "txtEmail2"))
                model.Email2 = form["txtEmail2"];
            
            #endregion
        }

        protected void _Process_PJ_Phase2(FormCollection form, ref LegalPerson model) 
        {
            if (this.CheckFormCollection(form, "txtBaseData") && !String.IsNullOrEmpty(form["txtBaseData"]))
                model.BaseData = Convert.ToDateTime(form["txtBaseData"]);

            if (this.CheckFormCollection(form, "txtCapital") && !String.IsNullOrEmpty(form["txtCapital"]))
                model.Capital = Convert.ToDecimal(form["txtCapital"]);

            if (this.CheckFormCollection(form, "txtIntegratedCapital") && !String.IsNullOrEmpty(form["txtIntegratedCapital"]))
                model.IntegratedCapital = Convert.ToDecimal(form["txtIntegratedCapital"]);

            if (this.CheckFormCollection(form, "txtNetWorth") && !String.IsNullOrEmpty(form["txtNetWorth"]))
                model.NetWorth = Convert.ToDecimal(form["txtNetWorth"]);

            if (this.CheckFormCollection(form, "txtAccountingContactName") && !String.IsNullOrEmpty(form["txtAccountingContactName"]))
                model.AccountingContactName = form["txtAccountingContactName"];

            if (this.CheckFormCollection(form, "txtAccountingContactPhone") && !String.IsNullOrEmpty(form["txtAccountingContactPhone"]))
                model.AccountingContactPhone = form["txtAccountingContactPhone"];


            var listStockControl = form.AllKeys.Where(a => a.Contains("txtStockControlName_")).ToList();
            if (listStockControl != null && listStockControl.Count > 0)
            {
                model.StockControls = new List<StockControl>();

                foreach (var s in listStockControl)
                {
                    var i = s.Replace("txtStockControlName_", "");

                    StockControl stc = new StockControl();

                    if (this.CheckFormCollection(form, "txtStockControlName_" + i) && !String.IsNullOrEmpty(form["txtStockControlName_" + i]))
                        stc.Name = form["txtStockControlName_" + i];

                    if (this.CheckFormCollection(form, "txtStockControlCPFCNPJ_" + i) && !String.IsNullOrEmpty(form["txtStockControlCPFCNPJ_" + i]))
                        stc.CPFCNPJ = form["txtStockControlCPFCNPJ_" + i];

                    if (this.CheckFormCollection(form, "txtStockControlEntryDate_" + i) && !String.IsNullOrEmpty(form["txtStockControlEntryDate_" + i]))
                        stc.EntryDate = Convert.ToDateTime(form["txtStockControlEntryDate_" + i]);

                    if (this.CheckFormCollection(form, "txtStockControlParticipation_" + i) && !String.IsNullOrEmpty(form["txtStockControlParticipation_" + i]))
                        stc.Participation = Convert.ToDecimal(form["txtStockControlParticipation_" + i]);
                                        
                    model.StockControls.Add(stc);
                }
            }

            var listControlled = form.AllKeys.Where(a => a.Contains("txtCTPCorporateName_")).ToList();
            if (listControlled != null && listControlled.Count > 0)
            {
                model.ControlledLegalPerson = new List<ControlledLegalPerson>();

                foreach (var s in listControlled)
                {
                    var i = s.Replace("txtCTPCorporateName_", "");

                    ControlledLegalPerson stc = new ControlledLegalPerson();

                    if (this.CheckFormCollection(form, "txtCTPCorporateName_" + i) && !String.IsNullOrEmpty(form["txtCTPCorporateName_" + i]))
                        stc.CorporateName = form["txtCTPCorporateName_" + i];

                    if (this.CheckFormCollection(form, "txtCTPCPFCNPJ_" + i) && !String.IsNullOrEmpty(form["txtCTPCPFCNPJ_" + i]))
                        stc.CPFCNPJ = form["txtCTPCPFCNPJ_" + i];
                                       
                    model.ControlledLegalPerson.Add(stc);
                }
            }

            var listAffiliated = form.AllKeys.Where(a => a.Contains("txtAFCorporateName_")).ToList();
            if (listAffiliated != null && listAffiliated.Count > 0)
            {
                model.AffiliatedLegalPerson = new List<AffiliatedLegalPerson>();

                foreach (var s in listAffiliated)
                {
                    var i = s.Replace("txtAFCorporateName_", "");

                    AffiliatedLegalPerson stc = new AffiliatedLegalPerson();

                    if (this.CheckFormCollection(form, "txtAFCorporateName_" + i) && !String.IsNullOrEmpty(form["txtAFCorporateName_" + i]))
                        stc.CorporateName = form["txtAFCorporateName_" + i];

                    if (this.CheckFormCollection(form, "txtAFCPFCNPJ_" + i) && !String.IsNullOrEmpty(form["txtAFCPFCNPJ_" + i]))
                        stc.CPFCNPJ = form["txtAFCPFCNPJ_" + i];

                    model.AffiliatedLegalPerson.Add(stc);
                }
            }

            var listAttorney = form.AllKeys.Where(a => a.Contains("txtATCorporateName_")).ToList();
            if (listAttorney != null && listAttorney.Count > 0)
            {
                model.AttorneyLegalPerson = new List<AttorneyLegalPerson>();

                foreach (var s in listAttorney)
                {
                    var i = s.Replace("txtATCorporateName_", "");

                    AttorneyLegalPerson stc = new AttorneyLegalPerson();

                    if (this.CheckFormCollection(form, "txtATCorporateName_" + i) && !String.IsNullOrEmpty(form["txtATCorporateName_" + i]))
                        stc.CorporateName = form["txtATCorporateName_" + i];

                    if (this.CheckFormCollection(form, "txtATCPFCNPJ_" + i) && !String.IsNullOrEmpty(form["txtATCPFCNPJ_" + i]))
                        stc.CPFCNPJ = form["txtATCPFCNPJ_" + i];

                    if (this.CheckFormCollection(form, "txtATBirthDate_" + i) && !String.IsNullOrEmpty(form["txtATBirthDate_" + i]))
                        stc.BirthDate = Convert.ToDateTime(form["txtATBirthDate_" + i]);

                    if (this.CheckFormCollection(form, "txtATRole_" + i) && !String.IsNullOrEmpty(form["txtATRole_" + i]))
                        stc.Role = form["txtATRole_" + i];
                    
                    model.AttorneyLegalPerson.Add(stc);
                }
            }

            #region contas bancárias

            var listReferenceAccounts = form.AllKeys.Where(a => a.Contains("txtBankCode_")).ToList();

            if (listReferenceAccounts != null && listReferenceAccounts.Count > 0)
            {
                model.ReferenceAccounts = new List<AuthorizedAccount>();

                foreach (var s in listReferenceAccounts)
                {
                    var i = s.Replace("txtBankCode_", "");

                    AuthorizedAccount account = new AuthorizedAccount();
                    account.Account = new BankAccount();

                    if (this.CheckFormCollection(form, "txtBankCode_" + i))
                        account.Account.BankCode = form["txtBankCode_" + i];

                    if (this.CheckFormCollection(form, "txtBankAgency_" + i))
                        account.Account.AgencyCode = form["txtBankAgency_" + i];

                    if (this.CheckFormCollection(form, "txtBankAccount_" + i))
                        account.Account.Account = form["txtBankAccount_" + i];

                    model.ReferenceAccounts.Add(account);
                }
            }
            #endregion

            #region Referencias

            var listReferences = form.AllKeys.Where(a => a.Contains("txtRefType_")).ToList();

            if (listReferences != null && listReferences.Count > 0)
            {
                model.References = new List<References>();

                foreach (var s in listReferences)
                {
                    var i = s.Replace("txtRefType_", "");

                    References refer = new References();

                    if (this.CheckFormCollection(form, "txtRefType_" + i))
                        refer.ReferType = (ReferenceType)Convert.ToInt32(form["txtRefType_" + i]);

                    if (this.CheckFormCollection(form, "txtRefName_" + i))
                        refer.Name = form["txtRefName_" + i];

                    if (this.CheckFormCollection(form, "txtRefPhone_" + i) && form["txtRefPhone_" + i].Length == 14)
                    {
                        refer.PhoneCode = Convert.ToInt32(form["txtRefPhone_" + i].Substring(1, 2));
                        refer.PhoneNumber = form["txtRefPhone_" + i].Substring(5, 9);
                    }
                    else if (form["txtRefPhone_" + i].Length != 14)
                    {
                        refer.PhoneCode = 0; refer.PhoneNumber = "";
                    }

                    model.References.Add(refer);
                }
            }

            #endregion

            var listAccountSame = form.AllKeys.Where(a => a.Contains("txtAuthorizedAccountBankSame_")).ToList();
            if (listAccountSame != null && listAccountSame.Count > 0)
            {
                model.AuthorizedAccountsSame = new List<AuthorizedAccount>();

                foreach (var s in listAccountSame)
                {
                    var i = s.Replace("txtAuthorizedAccountBankSame_", "");

                    AuthorizedAccount stc = new AuthorizedAccount();
                    stc.Account = new BankAccount();

                    if (this.CheckFormCollection(form, "txtAuthorizedAccountBankSame_" + i))
                        stc.Account.BankCode = form["txtAuthorizedAccountBankSame_" + i];

                    if (this.CheckFormCollection(form, "txtAuthorizedAccountAgencySame_" + i))
                        stc.Account.AgencyCode = form["txtAuthorizedAccountAgencySame_" + i];

                    if (this.CheckFormCollection(form, "txtAuthorizedAccountNumberSame_" + i))
                        stc.Account.Account = form["txtAuthorizedAccountNumberSame_" + i];

                    model.AuthorizedAccountsSame.Add(stc);
                }
            }

            var listAccountOthers = form.AllKeys.Where(a => a.Contains("txtAuthorizedAccountNameOthers_")).ToList();
            if (listAccountOthers != null && listAccountOthers.Count > 0)
            {
                model.AuthorizedAccountsOthers = new List<AuthorizedAccount>();

                foreach (var s in listAccountOthers)
                {
                    var i = s.Replace("txtAuthorizedAccountNameOthers_", "");

                    AuthorizedAccount stc = new AuthorizedAccount();
                    stc.Account = new BankAccount();

                    if (this.CheckFormCollection(form, "txtAuthorizedAccountNameOthers_" + i))
                        stc.TitularName = form["txtAuthorizedAccountNameOthers_" + i];

                    if (this.CheckFormCollection(form, "txtAuthorizedAccountCPFOthers_" + i))
                        stc.CPF = form["txtAuthorizedAccountCPFOthers_" + i];

                    if (this.CheckFormCollection(form, "txtAuthorizedAccountBankOthers_" + i))
                        stc.Account.BankCode = form["txtAuthorizedAccountBankOthers_" + i];

                    if (this.CheckFormCollection(form, "txtAuthorizedAccountAgencyOthers_" + i))
                        stc.Account.AgencyCode = form["txtAuthorizedAccountAgencyOthers_" + i];

                    if (this.CheckFormCollection(form, "txtAuthorizedAccountNumberOthers_" + i))
                        stc.Account.Account = form["txtAuthorizedAccountNumberOthers_" + i];

                    if (this.CheckFormCollection(form, "txtAuthorizedAccountCoNameOthers_" + i))
                        stc.CoTitularName = form["txtAuthorizedAccountCoNameOthers_" + i];

                    if (this.CheckFormCollection(form, "txtAuthorizedAccountCoCPFOthers_" + i))
                        stc.CoCPF = form["txtAuthorizedAccountCoCPFOthers_" + i];

                    model.AuthorizedAccountsOthers.Add(stc);
                }
            }

            if (this.CheckFormCollection(form, "rdbPPE") && !String.IsNullOrEmpty(form["rdbPPE"]))
                model.PPE = form["rdbPPE"] == "S";

            if (form["rdbPPE"] == "S")
            {
                var listPPE = form.AllKeys.Where(a => a.Contains("txtPoliticalName_")).ToList();
                if (listPPE != null && listPPE.Count > 0)
                {
                    model.PoliticalMembers = new List<AuthorizedMember>();

                    foreach (var s in listPPE)
                    {
                        var i = s.Replace("txtPoliticalName_", "");

                        AuthorizedMember stc = new AuthorizedMember();

                        if (this.CheckFormCollection(form, "txtPoliticalName_" + i) && !String.IsNullOrEmpty(form["txtPoliticalName_" + i]))
                            stc.Name = form["txtPoliticalName_" + i];

                        if (this.CheckFormCollection(form, "txtPoliticalCPF_" + i) && !String.IsNullOrEmpty(form["txtPoliticalCPF_" + i]))
                            stc.CPF = form["txtPoliticalCPF_" + i];

                        model.PoliticalMembers.Add(stc);
                    }
                }
            }
        }

        protected void _Process_PJ2(FormCollection form, ref LegalPerson model) 
        {
            if (this.CheckFormCollection(form, "txtAdministrator"))
                model.Administrator = form["txtAdministrator"];

            if (this.CheckFormCollection(form, "txtCVM"))
                model.CVM = form["txtCVM"];

            if (this.CheckFormCollection(form, "txtRDE"))
                model.RDE = form["txtRDE"];

            if (this.CheckFormCollection(form, "txtOriginCountry"))
                model.OriginCountry = form["txtOriginCountry"];

            if (this.CheckFormCollection(form, "txtCustodian"))
                model.Custodian = form["txtCustodian"];

            if (this.CheckFormCollection(form, "txtLegalRepresentative"))
                model.LegalRepresentative = form["txtLegalRepresentative"];

            if (this.CheckFormCollection(form, "txtTributeRepresentative"))
                model.TributeRepresentative = form["txtTributeRepresentative"];

            if (this.CheckFormCollection(form, "txtCoLegalRepresentative"))
                model.CoLegalRepresentative = form["txtCoLegalRepresentative"];

            if (this.CheckFormCollection(form, "txtCollectiveAccountTitular"))
                model.CollectiveAccountTitular = form["txtCollectiveAccountTitular"];

            if (this.CheckFormCollection(form, "rdbCollectiveAccount"))
            {
                if (String.IsNullOrEmpty(form["rdbCollectiveAccount"]))
                    model.CollectiveAccount = null;
                else
                    model.CollectiveAccount = form["rdbCollectiveAccount"] == "S";
            }

            #region Imobilegoods

            var list = form.AllKeys.Where(a => a.Contains("ddlImobileType_")).ToList();

            if (list != null && list.Count > 0)
            {
                model.ImobileGoods = new List<ImobileGood>();

                foreach (var s in list)
                {
                    var i = s.Replace("ddlImobileType_", "");

                    ImobileGood imobile = new ImobileGood();
                    imobile.Address = new PersonAddress();

                    if (this.CheckFormCollection(form, "ddlImobileType_" + i) && !String.IsNullOrEmpty(form["ddlImobileType_" + i]))
                        imobile.Type = (GoodKind)Convert.ToInt32(form["ddlImobileType_" + i]);

                    if (this.CheckFormCollection(form, "txtImobileValue_" + i) && !String.IsNullOrEmpty(form["txtImobileValue_" + i]))
                        imobile.Value = Convert.ToDecimal(form["txtImobileValue_" + i]);

                    if (this.CheckFormCollection(form, "txtImobileAddressLine_" + i) && !String.IsNullOrEmpty(form["txtImobileAddressLine_" + i]))
                        imobile.Address.AddressLine1 = form["txtImobileAddressLine_" + i];

                    if (this.CheckFormCollection(form, "txtImobileQuarter_" + i) && !String.IsNullOrEmpty(form["txtImobileQuarter_" + i]))
                        imobile.Address.Quarter = form["txtImobileQuarter_" + i];

                    if (this.CheckFormCollection(form, "txtImobileCity_" + i) && !String.IsNullOrEmpty(form["txtImobileCity_" + i]))
                        imobile.Address.City = form["txtImobileCity_" + i];

                    if (this.CheckFormCollection(form, "txtImobilePostalCode1_" + i) && this.CheckFormCollection(form, "txtImobilePostalCode2_" + i)
                            && (!String.IsNullOrEmpty(form["txtImobilePostalCode1_" + i]) && !String.IsNullOrEmpty(form["txtImobilePostalCode2_" + i])))
                        imobile.Address.PostalCode = form["txtImobilePostalCode1_" + i] + "-" + form["txtImobilePostalCode2_" + i];

                    if (this.CheckFormCollection(form, "ddlImobileAddressState_" + i) && !String.IsNullOrEmpty(form["ddlImobileAddressState_" + i]))
                        imobile.Address.State = (State)Convert.ToInt32(form["ddlImobileAddressState_" + i]);

                    if (this.CheckFormCollection(form, "txtImobileStateCountry_" + i) && !String.IsNullOrEmpty(form["txtImobileStateCountry_" + i]))
                        imobile.Address.StateCountry = form["txtImobileStateCountry_" + i];

                    model.ImobileGoods.Add(imobile);
                }
            }

            #endregion

            #region Mobile Goods

            var listMobile = form.AllKeys.Where(a => a.Contains("txtMobileType_")).ToList();

            if (listMobile != null && listMobile.Count > 0)
            {
                model.MobileGoods = new List<MobileGood>();

                foreach (var s in listMobile)
                {
                    var i = s.Replace("txtMobileType_", "");

                    MobileGood mobile = new MobileGood();

                    if (this.CheckFormCollection(form, "txtMobileType_" + i) && !String.IsNullOrEmpty(form["txtMobileType_" + i]))
                        mobile.Type = (GoodKind)Convert.ToInt32(form["txtMobileType_" + i]);

                    if (this.CheckFormCollection(form, "txtMobileValue_" + i) && !String.IsNullOrEmpty(form["txtMobileValue_" + i]))
                        mobile.Value = Convert.ToDecimal(form["txtMobileValue_" + i]);

                    if (this.CheckFormCollection(form, "txtMobileDescription_" + i) && !String.IsNullOrEmpty(form["txtMobileDescription_" + i]))
                        mobile.Description = form["txtMobileDescription_" + i];


                    model.MobileGoods.Add(mobile);
                }
            }

            #endregion

            if (this.CheckFormCollection(form, "txtMensalRendimentType1") 
                && this.CheckFormCollection(form, "txtMensalRendimentValue1")
                && !String.IsNullOrEmpty(form["txtMensalRendimentValue1"])) 
            {
                model.MensalRendiments = new List<MensalRendiment>();
                model.MensalRendiments.Add(new MensalRendiment()
                {
                    PJType = form["txtMensalRendimentType1"],
                    Value = Convert.ToDecimal(form["txtMensalRendimentValue1"])
                });

            }


            if (this.CheckFormCollection(form, "txtMensalRendimentType2")
                && this.CheckFormCollection(form, "txtMensalRendimentValue2")
                && !String.IsNullOrEmpty(form["txtMensalRendimentValue2"]))
            {
                if (model.MensalRendiments == null) model.MensalRendiments = new List<MensalRendiment>();
                model.MensalRendiments.Add(new MensalRendiment()
                {
                    PJType = form["txtMensalRendimentType2"],
                    Value = Convert.ToDecimal(form["txtMensalRendimentValue2"])
                });

            }

            if (this.CheckFormCollection(form, "txtFinancialPositionDate"))
                model.FinancialPositionDate = form["txtFinancialPositionDate"];

            if (this.CheckFormCollection(form, "txtPL") && !String.IsNullOrEmpty(form["txtPL"]))
                model.PL = Convert.ToDecimal(form["txtPL"]);

            if (this.CheckFormCollection(form, "txtTotalPatrimony") && !String.IsNullOrEmpty(form["txtTotalPatrimony"]))
                model.TotalPatrimony = Convert.ToDecimal(form["txtTotalPatrimony"]);

            if (this.CheckFormCollection(form, "rdbOperatesOnItsOwn"))
            {
                if (String.IsNullOrEmpty(form["rdbOperatesOnItsOwn"]))
                    model.OperatesOnItsOwn = null;
                else
                    model.OperatesOnItsOwn = form["rdbOperatesOnItsOwn"] == "S";
            }

            if (this.CheckFormCollection(form, "rdbAuthorizesTransmissionOrders"))
            {
                if (String.IsNullOrEmpty(form["rdbAuthorizesTransmissionOrders"]))
                    model.AuthorizesTransmissionOrders = null;
                else
                    model.AuthorizesTransmissionOrders = form["rdbAuthorizesTransmissionOrders"] == "S";
            }

            if (form["rdbAuthorizesTransmissionOrders"] == "S") 
            {
                var listAuthorizedMembers = form.AllKeys.Where(a => a.Contains("txtAuthorizedMemberName_")).ToList();

                if (listAuthorizedMembers != null && listAuthorizedMembers.Count > 0)
                {
                    model.AuthorizedMembers = new List<AuthorizedMember>();

                    foreach (var s in listAuthorizedMembers)
                    {
                        var i = s.Replace("txtAuthorizedMemberName_", "");

                        AuthorizedMember member = new AuthorizedMember();

                        if (this.CheckFormCollection(form, "txtAuthorizedMemberName_" + i) && !String.IsNullOrEmpty(form["txtAuthorizedMemberName_" + i]))
                            member.Name = form["txtAuthorizedMemberName_" + i];

                        if (this.CheckFormCollection(form, "txtAuthorizedMemberEmail_" + i) && !String.IsNullOrEmpty(form["txtAuthorizedMemberEmail_" + i]))
                            member.Email = form["txtAuthorizedMemberEmail_" + i];

                        if (this.CheckFormCollection(form, "txtAuthorizedMemberCPF_" + i) && !String.IsNullOrEmpty(form["txtAuthorizedMemberCPF_" + i]))
                            member.CPF = form["txtAuthorizedMemberCPF_" + i];

                        if (this.CheckFormCollection(form, "txtAuthorizedMemberRG_" + i) && !String.IsNullOrEmpty(form["txtAuthorizedMemberRG_" + i]))
                            member.RG = form["txtAuthorizedMemberRG_" + i];

                        if (this.CheckFormCollection(form, "txtAuthorizedMemberBirthDate_" + i) && !String.IsNullOrEmpty(form["txtAuthorizedMemberBirthDate_" + i]))
                            member.BirthDate = Convert.ToDateTime(form["txtAuthorizedMemberBirthDate_" + i]);
                        
                        model.AuthorizedMembers.Add(member);
                    }
                }
            }

            if (this.CheckFormCollection(form, "rdbBrokerBound"))
            {
                if (String.IsNullOrEmpty(form["rdbBrokerBound"]))
                    model.BrokerBound = null;
                else
                    model.BrokerBound = form["rdbBrokerBound"] == "S";
            }

            if (this.CheckFormCollection(form, "rdbApprovalTerm"))
            {
                if (String.IsNullOrEmpty(form["rdbApprovalTerm"]))
                    model.ApprovalTerm = null;
                else
                    model.ApprovalTerm = (ApprovalTerm)Convert.ToInt32(form["rdbApprovalTerm"]);
            }
        }

        protected void _Process_PFCTVM(FormCollection form, out IndividualPerson model) 
        {
            model = Session["ClientRegistration.IndividualPerson"] != null ? (IndividualPerson)Session["ClientRegistration.IndividualPerson"] : new IndividualPerson();

            this._Process_PF_Phase1(form, ref model);
            this._Process_PF_Phase2(form, ref model);
            this._Process_PF_Phase3(form, ref model);
            this._Process_PF2(form, ref model);

            Session["ClientRegistration.IndividualPerson"] = model;
        }

        [HttpPost]
        public ActionResult Process_PFCTVM_Phase1(FormCollection form)
        {
            IndividualPerson model;
            _Process_PFCTVM(form, out model);

            return PartialView("Reg_PFCTVM_Passo2", model);
        }

        [HttpPost]
        public ActionResult Process_PFCTVM_Phase2(FormCollection form)
        {
            IndividualPerson model;
            _Process_PFCTVM(form, out model);

            return Json(new { Result = true });
        }

        protected void _Process_PJCTVM(FormCollection form, out LegalPerson model)
        {
            model = Session["ClientRegistration.LegalPerson"] != null ? (LegalPerson)Session["ClientRegistration.LegalPerson"] : new LegalPerson();

            this._Process_PJ_Phase1(form, ref model);
            this._Process_PJ_Phase2(form, ref model);
            this._Process_PJ2(form, ref model);

            Session["ClientRegistration.LegalPerson"] = model;
        }

        [HttpPost]
        public ActionResult Process_PJCTVM_Phase1(FormCollection form)
        {
            LegalPerson model;
            _Process_PJCTVM(form, out model);

            return PartialView("Reg_PJCTVM_Passo2", model);
        }

        [HttpPost]
        public ActionResult Process_PJCTVM_Phase2(FormCollection form)
        {
            LegalPerson model;
            _Process_PJCTVM(form, out model);

            return Json(new { Result = true });
        }

        #endregion

        protected override RedirectResult Redirect(string url)
        {
            return new RedirectHelper(url);
        }


        public ActionResult ClientDocument()
        {
            IChannelProvider<IClientRegistrationContractChannel> channelProvider = ChannelProviderFactory<IClientRegistrationContractChannel>.Create("ClientRegistrationClassName");
            var service = channelProvider.GetChannel().CreateChannel();
            service.Open();

            var document = service.LoadDocument().Result;
            ViewBag.Products = CommonHelper.MontarRetornoSemValorBranco(service.GetFinancialProduct().Result);
            ViewBag.ExpirationDate = CommonHelper.MontarRetornoSemValorBranco(service.GetExpirationDateDocument().Result);

            return View(document);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult SaveDocument(FormCollection form)
        {
            IChannelProvider<IClientRegistrationContractChannel> channelProvider = ChannelProviderFactory<IClientRegistrationContractChannel>.Create("ClientRegistrationClassName");
            var service = channelProvider.GetChannel().CreateChannel();
            service.Open();

            var sd = form["IdDocument"].ToString();

            var document = new Document
            {
                Id = form["IdDocument"] == "" ? 0 : Convert.ToInt32(form["IdDocument"]),
                PhysicalPerson = Convert.ToBoolean(form["chkPhysicalPerson"].Split(',')[0]),
                LegalPerson = Convert.ToBoolean(form["chkLegalPerson"].Split(',')[0]),
                Funds = Convert.ToBoolean(form["chkFunds"].Split(',')[0]),
                NameFile = Convert.ToString(form["DocumentName"]),
                Instruction = Convert.ToString(form["Instruction"]),
                Required = Convert.ToBoolean(form["Required"].Split(',')[0]),
                IdValidation = Convert.ToInt32(form["Validatetime"])
            };

            if (form["lstProducts"] != null)
            {
                var products = form["lstProducts"].ToString().Split(',');

                foreach (var item in products)
                    document.Products.Add(new DocumentProduct { IdProduct = Convert.ToInt32(item) });
            }

            GetFileUpload(document);
            service.UpsertDocument(document);
            service.Close();

            return RedirectToAction("ClientDocument");
        }

        public JsonResult GetDocumentById(int idDocument)
        {
            IChannelProvider<IClientRegistrationContractChannel> channelProvider = ChannelProviderFactory<IClientRegistrationContractChannel>.Create("ClientRegistrationClassName");
            var service = channelProvider.GetChannel().CreateChannel();
            service.Open();
            var document = service.LoadDocumentById(idDocument).Result;
            service.Close();

            return Json(document, JsonRequestBehavior.AllowGet);
        }

        public JsonResult RemoveDocumentById(int idDocument)
        {
            IChannelProvider<IClientRegistrationContractChannel> channelProvider = ChannelProviderFactory<IClientRegistrationContractChannel>.Create("ClientRegistrationClassName");
            var service = channelProvider.GetChannel().CreateChannel();
            service.Open();
            service.RemoveDocument(idDocument);
            service.Close();

            return Json(null, JsonRequestBehavior.AllowGet);
        }

        private void GetFileUpload(Document document)
        {
            if(Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                if (file.ContentLength > 0)
                {
                    byte[] bytes;
                    using (BinaryReader br = new BinaryReader(file.InputStream))
                    {
                        bytes = br.ReadBytes(file.ContentLength);
                    }

                    document.ContentType = file.ContentType;
                    document.Data = bytes;
                    document.DescriptionFile = file.FileName;
                }
            }
        }

        public ActionResult ClientAnalyze()
        {
            IChannelProvider<IClientRegistrationContractChannel> channelProvider = ChannelProviderFactory<IClientRegistrationContractChannel>.Create("ClientRegistrationClassName");
            var service = channelProvider.GetChannel().CreateChannel();
            service.Open();
            var response = service.LoadClientsInAnalysis().Result;
            service.Close();

            return View(response);
        }

        public ActionResult ClientApproved()
        {
            IChannelProvider<IClientRegistrationContractChannel> channelProvider = ChannelProviderFactory<IClientRegistrationContractChannel>.Create("ClientRegistrationClassName");
            var service = channelProvider.GetChannel().CreateChannel();
            service.Open();
            var response = service.LoadClientsApproved().Result;
            service.Close();

            return View(response);
        }

        public ActionResult ClientDisapproved()
        {
            IChannelProvider<IClientRegistrationContractChannel> channelProvider = ChannelProviderFactory<IClientRegistrationContractChannel>.Create("ClientRegistrationClassName");
            var service = channelProvider.GetChannel().CreateChannel();
            service.Open();
            var response = service.LoadClientsDisapproved().Result;
            service.Close();

            return View(response);
        }

        public ActionResult ClientFilling()
        {
            IChannelProvider<IClientRegistrationContractChannel> channelProvider = ChannelProviderFactory<IClientRegistrationContractChannel>.Create("ClientRegistrationClassName");
            var service = channelProvider.GetChannel().CreateChannel();
            service.Open();
            var response = service.LoadClientsInFilling().Result;
            service.Close();

            return View(response);
        }
    }
}
