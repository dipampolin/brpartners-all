﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using QX3.Portal.Contracts.DataContracts;
using QX3.Spinnex.Common.Services.Logging;
using QX3.Portal.WebSite.Models.Channel;
using QX3.Portal.WebSite.OnlinePortfolioService;
using System.Linq;
using QX3.Portal.WebSite.Properties;
using QX3.Portal.WebSite.Models;
using QX3.Portal.WebSite.Helper;
using Newtonsoft.Json;
using QX3.Portal.WebSite.AccessControlService;
using System.Web;
using System.Configuration;
using QX3.Portal.WebSite.CommonService;

namespace QX3.Portal.WebSite.Controllers
{

    public class OnlinePortfolioController : FileController
    {
        #region Position

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        [AccessControlAuthorize]
        public ActionResult Index(string id)
        {
            var clientCodes = OnlinePortfolioHelper.GetOperatorClients(PortfolioOperatorFilter.None);

            Session["OnlinePortfolio.Operator.Position.Clients"] = clientCodes;
            Session["OnlinePortfolio.Position.CurrentClientCode"] = null;

            if (!string.IsNullOrEmpty(id))
                ViewBag.OperatorClients = Int32.Parse(id);
            else
                ViewBag.OperatorClients = clientCodes == null ? string.Empty : string.Join(";", clientCodes);

            return View();
        }

        public JsonResult LoadClientPosition(string ClientCode, int Index, decimal Discount, bool IsFirst)
        {
            PortfolioPositionClientContent content = new PortfolioPositionClientContent();

            try
            {
                LogHelper.SaveAuditingLog("OnlinePortfolio.LoadClientPortfolio");

                if (!string.IsNullOrEmpty(ClientCode))
                    Session["OnlinePortfolio.Position.CurrentClientCode"] = ClientCode;
                else
                    Session["OnlinePortfolio.Position.CurrentClientCode"] = null;

                //decimal currentDiscount = Discount;
                content = GetCurrentClientPosition(Index, ClientCode, Discount, IsFirst);

                Session["OnlinePortfolio.Position"] = content;

                return Json(content);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return Json(new { Error = "Ocorreu um erro ao consultar a posição." });
            }
        }

        public JsonResult LoadClientPositionChart()
        {
            string chart = string.Empty;

            try
            {

                if (Session["OnlinePortfolio.Position.ChartItems"] != null)
                {
                    OnlinePortfolioHelper helper = new OnlinePortfolioHelper();
                    chart = helper.GetClientPositionChartHtml((List<PortfolioPositionItem>)Session["OnlinePortfolio.Position.ChartItems"]);
                }

                return Json(chart);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return Json(new { Error = "Ocorreu um erro ao carregar o gráfico." });
            }
        }

        protected PortfolioPositionClientContent GetCurrentClientPosition(int index, string filterClientCode, decimal currentDiscount, bool first)
        {
            PortfolioPositionClientContent content = new PortfolioPositionClientContent();

            try
            {
                Session["OnlinePortfolio.Position.ClientPosition"] = null;

                bool hasFilter = !string.IsNullOrEmpty(filterClientCode);
                int[] operatorClients = (int[])Session["OnlinePortfolio.Operator.Position.Clients"];
                string currentClient = string.IsNullOrEmpty(filterClientCode) ? operatorClients[index].ToString() : operatorClients.Where(o => o.Equals(Int32.Parse(filterClientCode))).FirstOrDefault().ToString();

                LogHelper.SaveAuditingLog("OnlinePortfolio.LoadClientPortfolio");

                IChannelProvider<IOnlinePortfolioContractChannel> portfolioProvider = ChannelProviderFactory<IOnlinePortfolioContractChannel>.Create("OnlinePortfolioClassName");
                var portfolioService = portfolioProvider.GetChannel().CreateChannel();

                portfolioService.Open();
                PortfolioPosition clientPosition = portfolioService.LoadClientPosition(Int32.Parse(currentClient), SessionHelper.CurrentUser.ID).Result;
                portfolioService.Close();

                List<PortfolioClientContactsListItem> contacts = new List<PortfolioClientContactsListItem>();

                if (clientPosition != null)
                {
                    OnlinePortfolioHelper pHelper = new OnlinePortfolioHelper();
                    content = pHelper.GetClientPosition(clientPosition, currentDiscount, first);
                    content.Length = hasFilter ? 1 : operatorClients.Length;
                    content.Legend = string.Format("Exibindo {0} de {1}", index + 1, content.Length);
                    content.Index = index;
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }

            return content;
        }

        public ActionResult PositionToFile(bool isPdf)
        {
            try
            {
                var currentPosition = Session["OnlinePortfolio.Position.ClientPosition"] == null ? new PortfolioPositionClientContent() : (PortfolioPositionClientContent)Session["OnlinePortfolio.Position.ClientPosition"];
                currentPosition.HtmlTableItems = string.Empty;
                
                string fileName = string.Format("carteiraonline_posicao_{0}.{1}", DateTime.Now.ToString("dd-MM-yyyy"), isPdf ? "pdf" : "xls");

                return ViewPortfolioPositionToFile(currentPosition, true, fileName, isPdf);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = ex.Message;
                TempData["StackTrace"] = ex.StackTrace;
                return this.RedirectToAction("Index");
            }
        }

        public JsonResult DeleteComments(int clientCode)
        {
            var errorMessage = string.Empty;
            try
            {
                IChannelProvider<IOnlinePortfolioContractChannel> portfolioProvider = ChannelProviderFactory<IOnlinePortfolioContractChannel>.Create("OnlinePortfolioClassName");
                var portfolioService = portfolioProvider.GetChannel().CreateChannel();

                portfolioService.Open();
                var result = portfolioService.UpdateObservation(clientCode, string.Empty).Result;
                portfolioService.Close();

                if (!result)
                    errorMessage = "Não foi possível excluir observação.";
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                errorMessage = "Não foi possível excluir observação.";
            }


            return Json(errorMessage);
        }

        public ActionResult ConfirmEmail()
        {
            return View();
        }


        #endregion

        #region History

        [AccessControlAuthorize]
        public ActionResult History(string id)
        {
            var clientCodes = OnlinePortfolioHelper.GetOperatorClients(PortfolioOperatorFilter.None);

            Session["OnlinePortfolio.Operator.History.Clients"] = clientCodes;
            Session["OnlinePortfolio.History.CurrentClientCode"] = null;

            if (!string.IsNullOrEmpty(id))
                ViewBag.OperatorClients = Int32.Parse(id);
            else
                ViewBag.OperatorClients = clientCodes == null ? string.Empty : string.Join(";", clientCodes);

            return View();
        }

        public JsonResult LoadClientHistory(string ClientCode, int Index)
        {
            PortfolioHistoryClientContent content = new PortfolioHistoryClientContent();

            try
            {
                LogHelper.SaveAuditingLog("OnlinePortfolio.LoadClientHistory");

                if (!string.IsNullOrEmpty(ClientCode))
                    Session["OnlinePortfolio.History.CurrentClientCode"] = ClientCode;
                else
                    Session["OnlinePortfolio.History.CurrentClientCode"] = null;

                content = GetCurrentClientHistory(Index, ClientCode);

                Session["OnlinePortfolio.History"] = content;

                return Json(content);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return Json(new { Error = "Ocorreu um erro ao consultar o histórico." });
            }
        }

        protected PortfolioHistoryClientContent GetCurrentClientHistory(int index, string filterClientCode)
        {
            PortfolioHistoryClientContent content = new PortfolioHistoryClientContent();

            try
            {
                Session["OnlinePortfolio.History.ClientPosition"] = null;

                bool hasFilter = !string.IsNullOrEmpty(filterClientCode);
                int[] operatorClients = (int[])Session["OnlinePortfolio.Operator.History.Clients"];
                string currentClient = string.IsNullOrEmpty(filterClientCode) ? operatorClients[index].ToString() : operatorClients.Where(o => o.Equals(Int32.Parse(filterClientCode))).FirstOrDefault().ToString();

                LogHelper.SaveAuditingLog("OnlinePortfolio.LoadClientHistory");

                IChannelProvider<IOnlinePortfolioContractChannel> portfolioProvider = ChannelProviderFactory<IOnlinePortfolioContractChannel>.Create("OnlinePortfolioClassName");
                var portfolioService = portfolioProvider.GetChannel().CreateChannel();

                portfolioService.Open();
                PortfolioPosition clientPosition = portfolioService.LoadClientHistory(Int32.Parse(currentClient), SessionHelper.CurrentUser.ID).Result;
                portfolioService.Close();

                if (clientPosition != null && clientPosition.Client != null)
                {
                    OnlinePortfolioHelper pHelper = new OnlinePortfolioHelper();
                    content = pHelper.GetClientHistory(clientPosition);
                    content.Length = hasFilter ? 1 : operatorClients.Length;
                    content.Legend = string.Format("Exibindo {0} de {1}", index + 1, content.Length);
                    content.Index = index;
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }

            return content;
        }

        public ActionResult HistoryToFile(bool isPdf)
        {
            try
            {
                var currentPosition = Session["OnlinePortfolio.History.ClientHistory"] == null ? new PortfolioHistoryClientContent() : (PortfolioHistoryClientContent)Session["OnlinePortfolio.History.ClientHistory"];
                currentPosition.HtmlTableItems = string.Empty;
                
                string fileName = string.Format("carteiraonline_historico_{0}.{1}", DateTime.Now.ToString("dd-MM-yyyy"), isPdf ? "pdf" : "xls");

                return ViewPortfolioHistoryToFile(currentPosition, true, fileName, isPdf);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = "Ocorreu um erro ao gerar relatório.";
                return this.RedirectToAction("History");
            }
        }

        #endregion

        #region Contacts

        [AccessControlAuthorize]
        public ActionResult Contacts(string id)
        {
            var clientCodes = OnlinePortfolioHelper.GetOperatorClients(PortfolioOperatorFilter.None);

            Session["OnlinePortfolio.Operator.Contacts.Clients"] = clientCodes;
            Session["OnlinePortfolio.Contacts.CurrentClientCode"] = null;

            if (!string.IsNullOrEmpty(id))
                ViewBag.OperatorClients = Int32.Parse(id);
            else
                ViewBag.OperatorClients = clientCodes == null ? string.Empty : string.Join(";", clientCodes);

            return View();
        }

        public JsonResult LoadClientContacts(string ClientCode, int Index)
        {
            PortfolioContactsClientContent content = new PortfolioContactsClientContent();

            try
            {
                LogHelper.SaveAuditingLog("OnlinePortfolio.LoadClientContacts");

                if (!string.IsNullOrEmpty(ClientCode))
                    Session["OnlinePortfolio.Contacts.CurrentClientCode"] = ClientCode;
                else
                    Session["OnlinePortfolio.Contacts.CurrentClientCode"] = null;

                content = GetCurrentClientContacts(Index, ClientCode);

                return Json(content);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return Json(new { Error = "Ocorreu um erro ao consultar os contatos." });
            }
        }

        public JsonResult LoadClientContactDetails(int Index)
        {
            PortfolioClientContactsListItem contact = new PortfolioClientContactsListItem();
            try
            {
                List<PortfolioClientContactsListItem> list = (List<PortfolioClientContactsListItem>)Session["PortfolioContacts.Contacts.CurrentContacts"];
                contact = list.Where(l => l.Index.Equals(Index)).FirstOrDefault();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }
            return Json(contact);
        }

        protected PortfolioContactsClientContent GetCurrentClientContacts(int index, string filterClientCode)
        {
            PortfolioContactsClientContent content = new PortfolioContactsClientContent();

            try
            {
                bool hasFilter = !string.IsNullOrEmpty(filterClientCode);
                int[] operatorClients = (int[])Session["OnlinePortfolio.Operator.Contacts.Clients"];
                string currentClient = string.IsNullOrEmpty(filterClientCode) ? operatorClients[index].ToString() : operatorClients.Where(o => o.Equals(Int32.Parse(filterClientCode))).FirstOrDefault().ToString();

                LogHelper.SaveAuditingLog("OnlinePortfolio.LoadClientContacts");

                IChannelProvider<IOnlinePortfolioContractChannel> portfolioProvider = ChannelProviderFactory<IOnlinePortfolioContractChannel>.Create("OnlinePortfolioClassName");
                var portfolioService = portfolioProvider.GetChannel().CreateChannel();

                portfolioService.Open();
                PortfolioContact clientContacts = portfolioService.LoadClientContacts(Int32.Parse(currentClient)).Result;
                portfolioService.Close();

                int indexCounter = 0;

                Session["OnlinePortfolio.Contacts.ClientContacts"] = clientContacts;

                List<PortfolioClientContactsListItem> contacts = new List<PortfolioClientContactsListItem>();

                if (clientContacts != null)
                {
                    content.ClientName = string.Format("{0} {1}", clientContacts.Client.Value, clientContacts.Client.Description);
                    content.ClientPhone = clientContacts.ClientPhone == "()" ? "-" : clientContacts.ClientPhone;
                    content.ClientEmail = string.IsNullOrEmpty(clientContacts.ClientEmail) ? "-" : clientContacts.ClientEmail;
                    content.ClientCode = clientContacts.Client.Value;

                    contacts = (from c in clientContacts.Contacts
                                select new PortfolioClientContactsListItem
                                {
                                    Date = c.ContactDate,
                                    Index = indexCounter++,
                                    Message = c.Contact,
                                    Operator = c.Operator.Description
                                }).ToList();

                    OnlinePortfolioHelper pHelper = new OnlinePortfolioHelper();
                    pHelper.GetClientContactsContent(contacts, ref content);
                    content.Length = hasFilter ? 1 : operatorClients.Length;
                    content.Legend = string.Format("Exibindo {0} de {1}", index + 1, content.Length);
                    content.Index = index;
                }

            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }

            return content;
        }

        public ActionResult ContactInsert()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ContactInsert(FormCollection form)
        {
            string clientCode = string.Empty;

            try
            {
                clientCode = form["hdfClientCode"] ?? "0";
                string date = form["txtDate"];
                string message = form["txtMessage"];

                IChannelProvider<IOnlinePortfolioContractChannel> channelProvider = ChannelProviderFactory<IOnlinePortfolioContractChannel>.Create("OnlinePortfolioClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();
                var insert = service.InsertContact(Int32.Parse(clientCode), new PortfolioContactItem { Contact = message, ContactDate = date, Operator = new CommonType { Value = SessionHelper.CurrentUser.ID.ToString() } }).Result;
                service.Close();

                if (insert)
                {
                    TempData["Success"] = insert;
                    TempData["SuccessMessage"] = "Contato registrado com sucesso.";
                }
                else
                {
                    TempData["ErrorMessage"] = "Não foi possível registrar contato.";
                    clientCode = "";
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = "Não foi possível registrar contato.";
            }
            return this.Redirect(string.Concat("~/OnlinePortfolio/Contacts", (clientCode != "") ? "/?ClientCode=" : "", clientCode));
        }

        public ActionResult ContactsToFile(bool isPdf)
        {
            try
            {
                var currentPosition = Session["OnlinePortfolio.Contacts.ClientContacts"] == null ? new PortfolioContact() : (PortfolioContact)Session["OnlinePortfolio.Contacts.ClientContacts"];
                //LogHelper.SaveAuditingLog("OnlinePortfolio.ContactsToFile", string.Format(LogResources.ObjectToFileParameters, isPdf ? "PDF" : "Excel", JsonConvert.SerializeObject(currentPosition)));
                string fileName = string.Format("carteiraonline_contatos_{0}.{1}", DateTime.Now.ToString("dd-MM-yyyy"), isPdf ? "pdf" : "xls");

                return ViewPortfolioContactsToFile(currentPosition, true, fileName, isPdf);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = "Ocorreu um erro ao gerar relatório.";
                return this.RedirectToAction("Index");
            }
        }

        #endregion

        [AccessControlAuthorize]
        public ActionResult Admin()
        {
            return View();
        }

        #region AdminClients

        [AccessControlAuthorize]
        public ActionResult AdminClients()
        {
            ViewBag.FilterCalculation = new SelectList(this.LoadCalculationTypes(), "value", "description");

            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public void LoadAdminClients(string clientCode, string calculation, string history, string attachType)
        {
            try
            {
                LogHelper.SaveAuditingLog("OnlinePortfolio.LoadAdminClients");

                PortfolioClientFilter filter = new PortfolioClientFilter();
                filter.Operator = SessionHelper.CurrentUser.ID.ToString();
                filter.Client = clientCode;
                filter.Calculation = calculation;
                filter.SendHistory = history;
                filter.SendAttachType = attachType;

                IChannelProvider<IOnlinePortfolioContractChannel> portfolioProvider = ChannelProviderFactory<IOnlinePortfolioContractChannel>.Create("OnlinePortfolioClassName");
                var service = portfolioProvider.GetChannel().CreateChannel();

                service.Open();

                var list = service.LoadPortfolioClients(filter).Result;

                service.Close();

                Session["OnlinePortfolio.AdminClients"] = (from t in list
                                                           select new PortfolioClientListItem
                                                           {
                                                               ClientCode = Convert.ToInt32(t.Client.Value),
                                                               ClientName = t.Client.Description,
                                                               CalculationType = Convert.ToInt32(t.Calculation.Value),
                                                               CalculationDescription = t.Calculation.Description,
                                                               SendHistory = t.SendHistory,
                                                               SendAttachType = t.SendAttachType,
                                                               StartDate = t.StartDate.ToShortDateString(),
                                                               InitialValue = t.InitialValue,
                                                               Operators = this.SelectOperatorsNames(t.Operators)
                                                           }).ToList();

                LogHelper.SaveAuditingLog("OnlinePortfolio.LoadAdminClients", string.Format(LogResources.AuditingLoadPortfolioAdminClients, JsonConvert.SerializeObject(filter), JsonConvert.SerializeObject(Session["OnlinePortfolio.AdminClients"])));
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }
        }

        private string SelectOperatorsNames(List<PortfolioOperator> operators)
        {
            string result = string.Empty;

            foreach (PortfolioOperator item in operators)
                result += string.Concat(item.Name, ", ");

            return result.Substring(0, result.Length - 2);
        }

        public ActionResult EditAdminClients(int id)
        {
            ViewBag.FilterCalculation = new SelectList(this.LoadCalculationTypes(), "value", "description");

            PortfolioClient client = new PortfolioClient();

            try
            {
                LogManager.InitLog("OnlinePortfolio.EditAdminClients");
                LogManager.WriteLog("");

                if (id != 0)
                {
                    IChannelProvider<IOnlinePortfolioContractChannel> channelProvider = ChannelProviderFactory<IOnlinePortfolioContractChannel>.Create("OnlinePortfolioClassName");
                    var service = channelProvider.GetChannel().CreateChannel();

                    service.Open();

                    var result = service.LoadPortfolioClient(id.ToString());

                    service.Close();

                    //Session["CurrentPortfolioAdminClient"] = result;

                    client = result.Result;
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = "Não foi possível carregar o cliente da Carteira Online.";
            }

            return View(client);
        }

        private List<CommonType> LoadCalculationTypes()
        {
            try
            {
                List<CommonType> lst = (List<CommonType>)Session["OnlinePortfolio.CalculationTypes"];

                if (lst == null)
                {
                    lst = new List<CommonType>();
                    //lst.Add(new CommonType { Value = "", Description = "Todos" });

                    IChannelProvider<IOnlinePortfolioContractChannel> portfolioProvider = ChannelProviderFactory<IOnlinePortfolioContractChannel>.Create("OnlinePortfolioClassName");
                    var service = portfolioProvider.GetChannel().CreateChannel();

                    service.Open();
                    var result = service.LoadCalculationTypes().Result;
                    service.Close();

                    if (result != null && result.Count() > 0)
                        lst.AddRange(result.ToList());

                    Session["OnlinePortfolio.CalculationTypes"] = lst;
                }

                return lst;
            }
            catch (Exception)
            {
                List<CommonType> lst = new List<CommonType>();
                lst.Add(new CommonType { Value = "", Description = "" });
                return lst;
            }
        }

        public JsonResult OperatorSuggest(string term, int[] operatorCode)
        {
            List<ACUser> users = new List<ACUser>();

            try
            {
                AccessControlHelper helper = new AccessControlHelper();

                users = helper.LoadUsers();
                users = users.Where(i => i.Name.ToLower().Contains(term.ToLower())).OrderBy(u => u.Name).ToList();

                if (operatorCode != null)
                    if (operatorCode.Count() > 0)
                        users = users.Where(i => !operatorCode.Contains(i.ID)).ToList();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }

            return Json(users, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditAdminClients(FormCollection frm)
        {
            ViewBag.FilterCalculation = new SelectList(this.LoadCalculationTypes(), "value", "description");

            try
            {
                LogManager.InitLog("OnlinePortfolio.EditAdminClients");
                LogManager.WriteLog("");

                PortfolioClient client = new PortfolioClient();
                client.Client = new CommonType { Value = frm["hdfClientCode"] };
                client.Calculation = new CommonType { Value = frm["ddlCalculation"] };
                client.SendHistory = frm["chkHistory"].Contains("true");
                client.SendAttachType = frm["rdbAttachType"];
                client.InitialValue = Convert.ToDecimal(frm["txtInitialValue"]);
                client.StartDate = Convert.ToDateTime(frm["txtStartDate"]);

                bool isUpdate = frm["hdfIsUpdate"].Equals("1");

                string operatorList = frm["hdfOperators"];
                if (!string.IsNullOrEmpty(operatorList))
                {
                    client.Operators = new List<PortfolioOperator>();
                    string[] lst = operatorList.Split(';');
                    foreach (var code in lst)
                        if (!string.IsNullOrEmpty(code))
                            client.Operators.Add(new PortfolioOperator { Code = Convert.ToInt32(code) });
                }

                IChannelProvider<IOnlinePortfolioContractChannel> channelProvider = ChannelProviderFactory<IOnlinePortfolioContractChannel>.Create("OnlinePortfolioClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                var insert = service.InsertClient(client, isUpdate).Result;

                service.Close();

                TempData["DebugClientObject"] = string.Format("ClientCode={0},Calculation={1},SendHistory={2}, SendAttachType={3},InitialValue={4},StartDate={5},IsUpdate={6},hdfOperators={7}", client.Client.Value, client.Calculation.Value, client.SendHistory.ToString(), client.SendAttachType, client.InitialValue.ToString(), client.StartDate.ToString(), isUpdate.ToString(), frm["hdfOperators"]);

                //JsonConvert.SerializeObject(client);

                if (insert)
                {
                    TempData["Success"] = true;
                    TempData["SuccessMessage"] = "Cliente atualizado com sucesso.";
                }
                else
                    TempData["ErrorMessage"] = "Não foi possível atualizar o cliente.";

            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = "Não foi possível inserir o cliente na Carteira Online. Erro: " + ex.StackTrace + ex.Message;
            }

            return RedirectToAction("AdminClients");
        }

        public JsonResult IncludeOperatorInList(string name, int? id, int[] userID)
        {
            // Observação: Os operadores são os usuários do sistema cadastrados.

            var errorMessage = string.Empty;
            var user = new ACUser();
            try
            {
                IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                user = service.LoadUsers().Result.Where(u => (id != null ? u.ID == id : u.Name == name) && (userID != null ? !userID.Contains(u.ID) : true)).FirstOrDefault();

                if (user == null)
                    errorMessage = "O operador '" + name + "' não existe ou já foi adicionado a lista.";

                service.Close();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                errorMessage = "Não foi possível localizar o operador.";
            }

            return Json(new { error = errorMessage, user });
        }

        public ActionResult ConfirmDeleteAdminClient(string code, string name)
        {

            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteAdminClient(string hdfClientCode)
        {
            IChannelProvider<IOnlinePortfolioContractChannel> channelProvider = ChannelProviderFactory<IOnlinePortfolioContractChannel>.Create("OnlinePortfolioClassName");
            var service = channelProvider.GetChannel().CreateChannel();

            service.Open();

            var delete = service.DeleteClient(hdfClientCode).Result;

            service.Close();

            if (delete)
            {
                TempData["Success"] = true;
                TempData["SuccessMessage"] = "Cliente excluído com sucesso.";
            }
            else
                TempData["ErrorMessage"] = "Não foi possível excluir o cliente da carteira online.";

            return RedirectToAction("AdminClients");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LoadSinacorClient(string term)
        {
            try
            {
                IChannelProvider<IOnlinePortfolioContractChannel> channelProvider = ChannelProviderFactory<IOnlinePortfolioContractChannel>.Create("OnlinePortfolioClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                var response = service.LoadSinacorClients(term).Result;

                service.Close();

                if (response.Exist)
                {
                    return Json("Cliente já foi cadastrado");
                }

                else if (response != null && response.Clients.Count() > 0)
                {
                    var client = response.Clients.FirstOrDefault();
                    return Json(string.Concat(client.Value, "-", client.Description));
                }
                else
                    return Json("Cliente não encontrado");
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return Json(string.Empty);
            }
        }

        #endregion

        public JsonResult ClientSuggest(string term)
        {
            List<CommonType> clients = new List<CommonType>();

            try
            {
                IChannelProvider<IOnlinePortfolioContractChannel> channelProvider = ChannelProviderFactory<IOnlinePortfolioContractChannel>.Create("OnlinePortfolioClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();
                string maxItems = ConfigurationManager.AppSettings["OnlinePortfolio.SuggestMaxItems"];
                clients = service.SearchClients(term, SessionHelper.CurrentUser.ID, string.IsNullOrEmpty(maxItems) ? 10 : Int32.Parse(maxItems)).Result.ToList();
                service.Close();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }

            return Json(clients, JsonRequestBehavior.AllowGet);

        }

        public ActionResult Observations(int clientId, string clientName)
        {
            try
            {
                IChannelProvider<IOnlinePortfolioContractChannel> channelProvider = ChannelProviderFactory<IOnlinePortfolioContractChannel>.Create("OnlinePortfolioClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();
                var observation = service.LoadObservation(clientId).Result;
                service.Close();

                ViewBag.Observation = observation != null ? observation : "";

                ViewBag.ClientId = clientId;
                ViewBag.ClientName = clientName;
            }

            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }

            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UpdateObservations(int ClientId, string txtObservation)
        {
            bool inserted = false;
            string errorMessage = string.Empty;
            string successMessage = string.Empty;

            try
            {
                IChannelProvider<IOnlinePortfolioContractChannel> channelProvider = ChannelProviderFactory<IOnlinePortfolioContractChannel>.Create("OnlinePortfolioClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();
                inserted = service.UpdateObservation(ClientId, txtObservation).Result;
                service.Close();

                if (inserted)
                {
                    successMessage = "Observação registrada com sucesso.";
                }
            }

            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                errorMessage = "Não foi possível registrar observação.";
            }

            return Json(new { inserted, errorMessage, successMessage, txtObservation });
        }

        public ActionResult AdminClientsToFile(bool isPdf)
        {
            List<PortfolioClientListItem> currentList = new List<PortfolioClientListItem>();

            try
            {
                if (Session["OnlinePortfolio.AdminClients"] != null)
                    currentList = (List<PortfolioClientListItem>)Session["OnlinePortfolio.AdminClients"];

                //LogHelper.SaveAuditingLog("OnlinePortfolio.AdminClientsToFile", string.Format(LogResources.ObjectToFileParameters, isPdf ? "PDF" : "Excel", JsonConvert.SerializeObject(currentList)));
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = "Ocorreu um erro ao gerar relatório.";
                return this.RedirectToAction("AdminClients");
            }

            string fileName = string.Format("adminclientes_{0}.{1}", DateTime.Now.ToString("dd-MM-yyyy"), isPdf ? "pdf" : "xls");

            LogHelper.SaveHistoryLog("Other.OrdersCorrection", new HistoryData { Action = "Exportar", OldData = string.Empty, NewData = JsonConvert.SerializeObject(currentList) });

            return isPdf ? ViewAdminClientsPdf(currentList, true, fileName) : ViewAdminClientsExcel(currentList, true, fileName);
        }

        #region AdminPosition

        [AccessControlAuthorize]
        public ActionResult AdminPosition()
        {


            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult SearchAdminPosition(string clientCode)
        {
            try
            {
                LogHelper.SaveAuditingLog("OnlinePortfolio.SearchAdminPosition");

                IChannelProvider<IOnlinePortfolioContractChannel> portfolioProvider = ChannelProviderFactory<IOnlinePortfolioContractChannel>.Create("OnlinePortfolioClassName");
                var service = portfolioProvider.GetChannel().CreateChannel();

                service.Open();

                var result = service.SearchClientPortfolioPosition(clientCode, SessionHelper.CurrentUser.ID.ToString()).Result;

                service.Close();

                OnlinePortfolioHelper portfolioHelper = new OnlinePortfolioHelper();
                var content = portfolioHelper.GetAdminClientPosition(result);

                return Json(content);
                //LogHelper.SaveAuditingLog("OnlinePortfolio.SearchAdminPosition", string.Format(LogResources.AuditingLoadPortfolioAdminClients, JsonConvert.SerializeObject(filter), JsonConvert.SerializeObject(Session["OnlinePortfolio.AdminClients"])));
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return Json(new { Error = ex.Message });
            }
        }

        public ActionResult AdminPositionToFile(bool isPdf)
        {
            try
            {
                var currentClientsList = Session["OnlinePortfolio.AdminPosition"] == null ? new PortfolioPosition() : (PortfolioPosition)Session["OnlinePortfolio.AdminPosition"];

                //LogHelper.SaveAuditingLog("OnlinePortfolio.AdminPositionToFile", string.Format(LogResources.ObjectToFileParameters, isPdf ? "PDF" : "Excel", JsonConvert.SerializeObject(currentClientsList)));
                string fileName = string.Format("carteiraonline_posicao_cliente_{0}.{1}", DateTime.Now.ToString("dd-MM-yyyy"), isPdf ? "pdf" : "xls");

                return ViewAdminPositionPdf(currentClientsList, true, fileName, isPdf);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["ErrorMessage"] = "Ocorreu um erro ao gerar relatório.";
                return this.RedirectToAction("AdminPosition");
            }
        }

        public ActionResult EditAdminPosition(string id)
        {
            PortfolioPosition obj = new PortfolioPosition();

            if (!string.IsNullOrEmpty(id) && !id.Equals("0"))
            {
                obj = (PortfolioPosition)Session["OnlinePortfolio.AdminPosition"];
                var position = (from d in obj.Details where d.Id == Convert.ToInt32(id) select d).FirstOrDefault();
                ViewBag.Position = position;
            }

            return View(obj);
        }

        public ActionResult AdminPositionPartition(string id)
        {
            PortfolioPosition obj = new PortfolioPosition();

            if (!string.IsNullOrEmpty(id) && !id.Equals("0"))
            {
                obj = (PortfolioPosition)Session["OnlinePortfolio.AdminPosition"];
                var position = (from d in obj.Details where d.Id == Convert.ToInt32(id) select d).FirstOrDefault();
                ViewBag.Position = position;
            }

            return View(obj);
        }

        public ActionResult ConfirmDeleteAdminPosition(string code, string name)
        {

            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DeleteAdminPosition(string requestId)
        {
            try
            {
                IChannelProvider<IOnlinePortfolioContractChannel> channelProvider = ChannelProviderFactory<IOnlinePortfolioContractChannel>.Create("OnlinePortfolioClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                var delete = service.DeletePosition(requestId).Result;

                service.Close();

                return Json(new { Error = delete ? "" : "Não foi possível excluir a posição do cliente." });
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return Json(new { Error = "Não foi possível excluir a posição do cliente." });
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult EditAdminPosition(string hdfClientCode, string hdfPositionId, string[] txtStock, string[] ddlOperation, string[] ddlMarket,
                                              string[] ddlLongShort, string[] txtQuantity, string[] txtPortfolioNumber, string[] txtPrice, string[] txtNetPrice, string[] txtPositionDate,
                                              string[] txtDueDate, string[] txtRollOverDate)
        {
            try
            {
                LogManager.InitLog("OnlinePortfolio.EditAdminPosition");
                LogManager.WriteLog("");

                PortfolioPosition obj = new PortfolioPosition();
                obj.Client = new CommonType { Value = hdfClientCode };

                List<PortfolioPositionItem> position = new List<PortfolioPositionItem>();
                for (int i = 0; i < txtStock.Length; i++)
                {
                    PortfolioPositionItem p = new PortfolioPositionItem();
                    p.Id = Convert.ToInt32(hdfPositionId);
                    p.StockCode = txtStock[i].ToUpper();
                    p.Operation = ddlOperation[i];
                    p.MarketType = ddlMarket[i];
                    p.LongShort = ddlLongShort[i].Equals("1");
                    p.Quantity = Convert.ToInt64(txtQuantity[i].Replace(".", ""));
                    p.PortfolioNumber = Convert.ToInt32(txtPortfolioNumber[i].Replace(".", ""));
                    p.Price = Convert.ToDecimal(txtPrice[i]);
                    p.NetPrice = Convert.ToDecimal(txtNetPrice[i]);
                    p.PositionDate = Convert.ToDateTime(txtPositionDate[i]);

                    if (p.MarketType.Equals("TER"))
                    {
                        p.DueDate = txtDueDate[i];
                        p.RollOverDate = txtRollOverDate[i];
                    }

                    if (p.Operation.Equals("V"))
                        p.Quantity = p.Quantity * -1;

                    position.Add(p);
                }

                obj.Details = position;

                IChannelProvider<IOnlinePortfolioContractChannel> channelProvider = ChannelProviderFactory<IOnlinePortfolioContractChannel>.Create("OnlinePortfolioClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                var insert = service.InsertPosition(obj, "").Result;

                service.Close();

                return Json(new { Error = insert ? "" : "Não foi possível atualizar a posição do cliente." });
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return Json(new { Error = "Não foi possível atualizar a posição do cliente. Erro: " + ex.StackTrace + ex.Message });
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AdminPositionPartition(string hdfClientCode, string hdfPositionId, string hdfStockCode, string hdfOperation, string hdfMarket, string hdfPortfolioNumber,
                                              string[] ddlLongShort, string[] txtQuantity, string[] txtPrice, string[] txtNetPrice, string[] txtPositionDate,
                                              string[] txtDueDate, string[] txtRollOverDate)
        {
            try
            {
                LogManager.InitLog("OnlinePortfolio.AdminPositionPartition");
                LogManager.WriteLog("");

                PortfolioPosition obj = new PortfolioPosition();
                obj.Client = new CommonType { Value = hdfClientCode };

                List<PortfolioPositionItem> position = new List<PortfolioPositionItem>();
                for (int i = 0; i < txtQuantity.Length; i++)
                {
                    PortfolioPositionItem p = new PortfolioPositionItem();
                    p.Id = 0;
                    p.StockCode = hdfStockCode.ToUpper();
                    p.Operation = hdfOperation;
                    p.MarketType = hdfMarket;
                    p.LongShort = ddlLongShort[i].Equals("1");
                    p.Quantity = Convert.ToInt64(txtQuantity[i].Replace(".", ""));
                    p.PortfolioNumber = Convert.ToInt32(hdfPortfolioNumber.Replace(".", ""));
                    p.Price = Convert.ToDecimal(txtPrice[i]);
                    p.NetPrice = Convert.ToDecimal(txtNetPrice[i]);
                    p.PositionDate = Convert.ToDateTime(txtPositionDate[i]);

                    if (p.MarketType.Equals("TER"))
                    {
                        p.DueDate = txtDueDate[i];
                        p.RollOverDate = txtRollOverDate[i];
                    }

                    if (p.Operation.Equals("V"))
                        p.Quantity = p.Quantity * -1;

                    position.Add(p);
                }

                obj.Details = position;

                IChannelProvider<IOnlinePortfolioContractChannel> channelProvider = ChannelProviderFactory<IOnlinePortfolioContractChannel>.Create("OnlinePortfolioClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                var insert = service.InsertPosition(obj, hdfPositionId).Result;

                service.Close();

                return Json(new { Error = insert ? "" : "Não foi possível atualizar a posição do cliente." });
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return Json(new { Error = "Não foi possível atualizar a posição do cliente. Erro: " + ex.StackTrace + ex.Message });
            }
        }

        #endregion


        //public ActionResult SendPortfolioMail() 
        //{
        //    var currentPosition = Session["OnlinePortfolio.Position.ClientPosition"] == null ? new PortfolioPositionClientContent() : (PortfolioPositionClientContent)Session["OnlinePortfolio.Position.ClientPosition"];
        //    currentPosition.HtmlTableItems = string.Empty;

        //    if (currentPosition != null)
        //    {
        //        try
        //        {
        //            using (var service = ChannelProviderFactory<ICommonContractChannel>.Create("CommonClassName").GetChannel().CreateChannel())
        //            {
        //                var variables = new List<KeyValuePair<string, object>>();
        //                variables.Add(new KeyValuePair<string, object>("strNome", currentPosition.ClientName));
        //                variables.Add(new KeyValuePair<string, object>("strDate", DateTime.Now.ToShortDateString()));
        //                string subject = "Carteira online";
        //                string emailTo = currentPosition.ClientEmail;
        //                string emailReplyTo = SessionHelper.CurrentUser.Email;

        //                var opService = ChannelProviderFactory<IOnlinePortfolioContractChannel>.Create("OnlinePortfolioClassName").GetChannel().CreateChannel();
        //                opService.Open();
        //                var clientInfo = opService.LoadPortfolioClient(currentPosition.ClientCode);
        //                List<KeyValuePair<string, byte[]>> attachments = new List<KeyValuePair<string, byte[]>>();

        //                if (clientInfo.Result != null)
        //                {

        //                    if (clientInfo.Result.SendAttachType.Equals("N"))
        //                    {
        //                        var mem = new ExcelHelper().CreatePortfolioPositionToMemoryStream(currentPosition);

        //                        byte[] buf = new byte[mem.Length];
        //                        mem.Position = 0;
        //                        mem.Read(buf, 0, buf.Length);

        //                        attachments.Add(new KeyValuePair<string, byte[]>("posicao-carteira.xls", buf));
        //                    }
        //                    else if (clientInfo.Result.SendAttachType.Equals("S"))
        //                    {
        //                        var mem = new PDFHelper().CreatePortfolioPositionToMemoryStream(currentPosition);

        //                        byte[] buf = new byte[mem.Length];
        //                        mem.Position = 0;
        //                        mem.Read(buf, 0, buf.Length);

        //                        attachments.Add(new KeyValuePair<string, byte[]>("posicao-carteira.pdf", buf));
        //                    }
        //                    else
        //                    {
        //                        return Json(new WcfResponse<bool> { Result = false, Message = "Email não enviado. Favor escolher o tipo de anexo para este cliente." });
        //                    }
        //                }
        //                opService.Close();

        //                var templateFile = "~/Template/OnlinePortfolioModel.htm";

        //                List<string> listCC = new List<string>();
        //                listCC.Add(SessionHelper.CurrentUser.Email);

        //                return Json(service.SendMailForUserWithAttachment(variables.ToArray(), attachments.ToArray(), templateFile, subject, emailTo, emailReplyTo, listCC.ToArray()));
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            LogManager.WriteError(ex);
        //            return Json(new WcfResponse<bool> { Result = false, Message = "Não foi possível enviar email." });
        //        }
        //    }

        //    return Json(new WcfResponse<bool> { Result = false, Message = "Cliente não encontrado." });

        //}
    }
}
