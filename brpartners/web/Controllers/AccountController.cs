﻿using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.WebSite.AccessControlService;
using QX3.Portal.WebSite.AuthenticationService;
using QX3.Portal.WebSite.ClientRegistrationService;
using QX3.Portal.WebSite.Helper;
using QX3.Portal.WebSite.Models;
using QX3.Portal.WebSite.Models.Channel;
using QX3.Portal.WebSite.OtherService;
using QX3.Portal.WebSite.Properties;
using QX3.Spinnex.Common.Services.Logging;
using QX3.Spinnex.Common.Services.Security;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace QX3.Portal.WebSite.Controllers
{
    public class AccountController : Controller
    {
        #region Login

        public ActionResult Login()
        {
            if (Session["CONTROLSESSION"] != null && Session["CONTROLSESSION"].ToString() == "S")
            {
                ViewBag.ErrorMsg = "Você foi desconectado.";
                ViewBag.ErrorClass = string.Empty;
                return View();
            }

            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [ValidateInput(false)]
        public ActionResult Login(string txtLogin, string txtPassword)
        {
            try
            {
                Session["NAME"] = txtLogin;
                Session["USERNAME"] = txtLogin;

                LogHelper.SaveAuditingLog("Account.Login", string.Format(LogResources.AuditingLoginParameters, txtLogin.ToLower()));

                if (String.IsNullOrEmpty(txtLogin))
                {
                    ViewBag.ErrorMsg = "O campo login deve ser preenchido.";
                    ViewBag.ErrorClass = string.Empty;
                    return View();
                }

                if (String.IsNullOrEmpty(txtPassword))
                {
                    ViewBag.ErrorMsg = "O campo senha deve ser preenchido.";
                    ViewBag.ErrorClass = string.Empty;
                    return View();
                }

                if (ModelState.IsValid)
                {
                    var ip = GetIPAddress();
                    Session["IPCLIENT"] = ip;

                    var accessControl = new AccessControlHelper();
                    var login = accessControl.Login(txtLogin, txtPassword, ip, Session.SessionID);

                    Session["NomePerfil"] = login.Profile.Name;

                    if(login.Profile.Name.ToLower() == "pre-cadastro")
                        Session["CLIENTONLINE"] = login.IdClient;

                    if (login.LoginError == null)
                    {
                        foreach (ACElement ac in login.Element)
                        {
                            if (ac.Alias.ToLower().Contains("suitability"))
                            {
                                return RedirectToAction("Index", "Suitability");
                            }

                            if (ac.Alias.ToLower().Contains("recursos humanos"))
                            {
                                return RedirectToRoute("RH", new { controller = "Dashboard", action = "Index" });
                            }

                            if (ac.Alias.ToLower().Contains("ficha cadastral") && login.Profile.Name.ToLower() == "pre-cadastro")
                            {
                                return RedirectToAction("RegistrationForm", "Account");
                            }
                        }

                        return RedirectToRoute("Index");
                    }
                    else
                    {
                        //var ldapHasError = ldapResponse.Message != null;
                        //var serviceHasError = response.Message != null;
                        //TempData["StackTrace"] = "Service Error: " + (serviceHasError ? response.Message : "") + "LDAP Error: " + (ldapHasError ? ldapResponse.Message : "");
                        //ViewBag.ErrorMsg = ldapHasError ? "Ocorreu um erro ao autenticar usuário." : response.Message;

                        ViewBag.ErrorMsg = login.LoginError.Message;
                        ViewBag.ErrorClass = login.LoginError.ErrorType;
                        return View();

                    }
                }
                else
                {
                    return View();
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["StackTrace"] = string.Format("Message: {0} - StackTrace: {1}", ex.Message, ex.StackTrace);
                ViewBag.ErrorMsg = "Ocorreu um erro ao autenticar usuário.";
                ViewBag.ErrorClass = "erro";
                return View();
            }
        }

        public ActionResult Logout()
        {
            LogHelper.SaveAuditingLog("Account.Logout");
            Session.Clear();
            FormsAuthentication.SignOut();
            return RedirectToAction("Login");
        }

        [HttpGet]
        public ActionResult RedefinePassword(string token)
        {
            bool emailIsValid = false;
            string email;

            IChannelProvider<IAuthenticationContractChannel> channelProvider = ChannelProviderFactory<IAuthenticationContractChannel>.Create("AuthenticationClassName");
            using (var service = channelProvider.GetChannel().CreateChannel())
            {
                var response = service.VerifyChangePassword(token);
                emailIsValid = response.Result;
                email = response.Data;

                if (emailIsValid && email != null)
                {
                    Session["UserEmail"] = email;
                    Session["token"] = token;
                    return View();
                }
                else
                    return RedirectToAction("LinkExpired");
            }
        }

        [HttpPost]
        public ActionResult ChangePassword(string txtPassword)
        {
            var user = new UserInformation();
            try
            {
                IChannelProvider<IAuthenticationContractChannel> channelProvider = ChannelProviderFactory<IAuthenticationContractChannel>.Create("AuthenticationClassName");
                using (var service = channelProvider.GetChannel().CreateChannel())
                {
                    PasswordGenerator pg = new PasswordGenerator();

                    var email = Session["UserEmail"].ToString();

                    IChannelProvider<IAccessControlContractChannel> provider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
                    var acService = provider.GetChannel().CreateChannel();
                    acService.Open();

                    var acUser = acService.LoadUserByEmail(email);
                    user.Password = pg.EncodePassword(txtPassword);
                    user.Email = email;
                    //user.UserName = acUser.Result.Login;
                    user.UserName = acUser.Result.ChaveAD;

                    acService.Close();

                    var response = service.ChangePassword(user);
                    var success = response.Result;

                    if (success)
                    {
                        TempData["SuccessMessage"] = "Senha alterada com sucesso!";
                        Session["token"] = null;
                    }
                    else
                    {
                        TempData["Error"] = response.Message;
                        return RedirectToAction("RedefinePassword", new { @token = (Session["token"] == null) ? null : Session["token"].ToString() });
                    }
                }

                return RedirectToAction("Login");
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["Error"] = "Ocorreu um erro ao atualizar senha.";
                return View("RedefinePassword");
            }
        }

        public ActionResult EmailSent()
        {
            return View();
        }

        public ActionResult LinkExpired()
        {
            return View();
        }

        public ActionResult FindAccount()
        {
            return View();
        }

        [HttpPost]
        public ActionResult FindAccount(string txtEmail)
        {
            try
            {
                IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
                using (var service = channelProvider.GetChannel().CreateChannel())
                {
                    var response = service.LoadUserByEmail(txtEmail);

                    if (response.Result.ID > 0)
                    {
                        if (response.Result.Blocked)
                        {
                            TempData["Error"] = "O usuário esta bloqueado. Não foi possível redefinir nova senha.";
                            return View();
                        }

                        if (!response.Result.SendSuitabilityEmail)
                        {
                            TempData["Error"] = "O usuário está configurado para não receber e-mail. Não foi possível redefinir nova senha.";
                            return View();
                        }

                        IChannelProvider<IAuthenticationContractChannel> channelProvider1 = ChannelProviderFactory<IAuthenticationContractChannel>.Create("AuthenticationClassName");
                        using (var service1 = channelProvider1.GetChannel().CreateChannel())
                        {
                            var response2 = service1.CreateGuidToSend(new UserInformation { Email = txtEmail });

                            if (response2.Result)
                            {
                                var response3 = service1.SendRedefinePasswordEmail(response2.Data, txtEmail, response.Result.Name);

                                if (response3.Result)
                                {
                                    return RedirectToAction("EmailSent");
                                }
                                else
                                {
                                    TempData["Error"] = "Ocorreu um erro ao enviar e-mail de redefinição de senha." + response3.Message;
                                }
                            }
                            else
                            {
                                TempData["Error"] = "Ocorreu um erro ao criar e-mail de redefinição de senha.";
                            }
                        }
                    }
                    else
                    {
                        TempData["Error"] = string.Format("Não foi possível encontrar uma conta associada a {0}.", txtEmail);
                    }
                    return View();
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                TempData["Error"] = "Ocorreu um erro ao buscar usuário.";
                return View();
            }
        }

        public String GetIPAddress()
        {
            String ip = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (string.IsNullOrEmpty(ip))
                ip = Request.ServerVariables["REMOTE_ADDR"];
            else
                ip = ip.Split(',')[0];

            return ip;
        }

        #endregion

        #region CADASTRO ONLINE

        public ActionResult Register()
        {
            ACUser user = new ACUser();
            return View(user);
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Register(FormCollection form)
        {
            ACUser user = new ACUser();

            try
            {
                user.Name = form["txtNome"];
                user.CpfCnpj = Convert.ToInt64(form["txtCpf"]);
                user.Email = form["txtEmail"];
                user.Telephone = form["txtTelefone"];
                user.Login = form["textLogin"];

                var idClient = RegisterNewClient(user, Convert.ToString(form["txtSenha"]));
                Session["CLIENTONLINE"] = idClient;
                return Login(user.Login, form["txtSenha"]);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return View(user);
            }
        }

        private ACProfile GetProfile(string profileName, IAccessControlContractChannel service)
        {
            var profiles = service.LoadProfiles().Result.ToList();
            var preClient = profiles.FirstOrDefault(x => x.Name == profileName);

            return preClient;
        }

        public string GenerateChaveAD()
        {
            Random random = new Random();
            string chave = string.Empty;

            string letras = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            while (chave.Length < 3)
            {
                char letra = letras[(int)(random.NextDouble() * letras.Length)];
                chave = chave.Contains(letra.ToString()) ? string.Empty : chave + letra;
            }

            string numeros = "0123456789";
            chave = chave + numeros[(int)(random.NextDouble() * numeros.Length)];

            return chave.ToString().ToLower();
        }

        public ActionResult RegistrationForm(string id)
        {
            var user = Session["USERNAME"];

            if (user == null)
                return RedirectToRoute("Index");

            IChannelProvider<IClientRegistrationContractChannel> channelProvider = ChannelProviderFactory<IClientRegistrationContractChannel>.Create("ClientRegistrationClassName");
            var service = channelProvider.GetChannel().CreateChannel();
            service.Open();
            var client = new ClientOnline();

            if (id != null)
                client = service.LoadClientOnline(Convert.ToString(id)).Result;
            else if (Session["CLIENTONLINE"] != null)
                client = service.LoadClientOnline(Convert.ToString(Session["CLIENTONLINE"])).Result;

            Session["CLIENTONLINE"] = client.Id;
            Session["IDFICHACADASTRAL"] = client.IdFichaCadastral;
            Session["STATUSFORM"] = client.IdStatusRegistration;

            ViewBag.Emails = MontaObjeto(client.Emails);
            ViewBag.NomePerfil = Session["NomePerfil"];
            ViewBag.Assinaturas = MontaAssinaturas(client.ClientFiles);
            ViewBag.StatusForm = IsAllowChangeForm();
            ViewBag.Estados = MontarRetornoComValorBranco(service.GetState().Result);
            ViewBag.Paises = MontarRetornoComValorBranco(service.GetCountries().Result);
            ViewBag.TiposEnderecos = MontarRetornoComValorBranco(service.GetAddressTypes().Result);
            ViewBag.Finalidades = MontarRetornoComValorBranco(service.GetFinalities().Result);
            ViewBag.Atividades = MontarRetornoComValorBranco(service.GetActivities().Result);
            ViewBag.Ocupacao = MontarRetornoComValorBranco(service.GetOccupations().Result);
            ViewBag.Porte = MontarRetornoComValorBranco(service.GetSizes().Result);
            ViewBag.Profissao = MontarRetornoComValorBranco(service.GetProfessions().Result); ;
            ViewBag.EstadoCivil = MontarRetornoComValorBranco(service.GetMaritalStatus().Result);
            ViewBag.Sexo = MontarRetornoComValorBranco(service.GetSex().Result);
            ViewBag.Status = MontarRetornoComValorBranco(service.GetStatus().Result);
            ViewBag.TipoDocumento = MontarRetornoComValorBranco(service.GetDocumentTypes().Result);
            ViewBag.GrupoEconomico = MontarRetornoComValorBranco(service.GetEconomicGroups().Result);
            ViewBag.Classificacao = MontarRetornoComValorBranco(service.GetClassifications().Result);
            ViewBag.AtividadeEconomica = MontarRetornoComValorBranco(service.GetEconomicActivities().Result);
            ViewBag.TipoCadastro = MontarRetornoComValorBranco(service.GetRegisterType().Result);
            ViewBag.TipoCliente = MontarRetornoComValorBranco(service.GetClientTypes().Result);
            ViewBag.Categoria = MontarRetornoComValorBranco(service.GetCategories().Result);
            ViewBag.Gerente = MontarRetornoComValorBranco(service.GetManagers().Result);
            ViewBag.Naturalidade = MontarRetornoComValorBranco(service.GetNaturalness().Result);
            ViewBag.Nacionalidade = MontarRetornoComValorBranco(service.GetNacionality().Result);
            ViewBag.TipoTel = MontarRetornoComValorBranco(service.GetTelephoneTypes().Result);
            ViewBag.RegimeMatrimonial = MontarRetornoComValorBranco(service.GetMatrimonialRegime().Result);
            ViewBag.Choice = MontarRetornoComValorBranco(service.GetChoice().Result);

            service.Close();

            return View(client);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult RegistrationForm(ClientOnline client, FormCollection form)
        {
            if (IsAllowChangeForm())
            {
                client.Id = Convert.ToInt32(Session["CLIENTONLINE"]);

                if (client.Id == 0)
                {
                    var user = new ACUser
                    {
                        Name = client.Name,
                        CpfCnpj = Convert.ToInt64(client.Cpf),
                        Email = client.EmailPrincipal,
                        Telephone = "1111",
                        Login = client.Cpf
                    };

                    var idClient = RegisterNewClient(user, Convert.ToString(client.Cpf));
                    Session["CLIENTONLINE"] = idClient;
                }


                if (form.AllKeys.Contains("lstEmail"))
                    client.Emails = form["lstEmail"].Split(',').ToList();

                client.Contact.AddRange(MontaContato(form["HdnContato"]));
                client.Address.AddRange(MontaEndereco(form["HdnEndereco"]));
                client.ClientFiles.AddRange(Upload());

                IChannelProvider<IClientRegistrationContractChannel> channelProvider = ChannelProviderFactory<IClientRegistrationContractChannel>.Create("ClientRegistrationClassName");
                var service = channelProvider.GetChannel().CreateChannel();
                service.Open();

                client.Id = Convert.ToInt32(Session["CLIENTONLINE"]);
                client.IdFichaCadastral = Convert.ToInt32(Session["IDFICHACADASTRAL"]);
                service.InsertUpdateRegistrationClient(client);

                service.Close();
            }

            return RedirectToAction("FinancialInformation");
        }

        public ActionResult FinancialInformation()
        {
            var user = Session["USERNAME"];

            if (user == null)
                return RedirectToRoute("Index");

            IChannelProvider<IClientRegistrationContractChannel> channelProviderClient = ChannelProviderFactory<IClientRegistrationContractChannel>.Create("ClientRegistrationClassName");
            var serviceClient = channelProviderClient.GetChannel().CreateChannel();
            serviceClient.Open();

            var clientId = Session["CLIENTONLINE"];
            var clientFinancial = serviceClient.LoadClientFinancial(Convert.ToInt32(clientId)).Result;
            Session["IDCLIENTFINANCIAL"] = clientFinancial.Id;

            ViewBag.ClientProducts = MontaProdutoCliente(clientFinancial.Products);
            ViewBag.Profile = Session["NomePerfil"];
            ViewBag.Products = MontarRetornoSemValorBranco(serviceClient.GetFinancialProduct().Result);
            ViewBag.FiscalNature = MontarRetornoComValorBranco(serviceClient.GetFiscalNature().Result);
            ViewBag.AccountFinality = MontarRetornoComValorBranco(serviceClient.GetAccountFinality().Result);
            ViewBag.AccountUtility = MontarRetornoComValorBranco(serviceClient.GetAccountUtility().Result);
            ViewBag.Accounttype = MontarRetornoComValorBranco(serviceClient.GetAccountType().Result);
            ViewBag.AccountTypeBank = MontarRetornoComValorBranco(serviceClient.GetAccountTypeBank().Result);
            ViewBag.Bank = MontarRetornoComValorBranco(serviceClient.GetBank().Result);
            ViewBag.StatusForm = IsAllowChangeForm();

            serviceClient.Close();

            return View(clientFinancial);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult FinancialInformation(ClientOnlineFinancial clientFinancial, FormCollection form)
        {
            if (IsAllowChangeForm())
            {
                IChannelProvider<IClientRegistrationContractChannel> channelProvider = ChannelProviderFactory<IClientRegistrationContractChannel>.Create("ClientRegistrationClassName");
                var service = channelProvider.GetChannel().CreateChannel();
                service.Open();

                var products = form["lstProducts"];
                var lstProducts = new List<string>();

                if (products != null)
                    lstProducts = products.Split(',').ToList();

                clientFinancial.IdClientOnline = Convert.ToInt32(Session["CLIENTONLINE"]);
                clientFinancial.Id = Convert.ToInt32(Session["IDCLIENTFINANCIAL"]);
                clientFinancial.Accounts.AddRange(MontaConta(form["HdnAccount"]));

                foreach (var item in lstProducts)
                {
                    clientFinancial.Products.Add(new FinancialProduct
                    {
                        IdClientOnline = clientFinancial.IdClientOnline,
                        IdProduct = Convert.ToInt32(item)
                    });
                }

                var save = service.InsertUpdateClientFinancial(clientFinancial).Result;
                service.Close();
            }

            return RedirectToAction("Documents");
        }

        public ActionResult Documents()
        {
            var userLogin = Session["USERNAME"];

            if (userLogin == null)
                return RedirectToRoute("Index");

            IChannelProvider<IClientRegistrationContractChannel> channelProvider = ChannelProviderFactory<IClientRegistrationContractChannel>.Create("ClientRegistrationClassName");
            var service = channelProvider.GetChannel().CreateChannel();

            service.Open();
            var documents = service.LoadClientDocument(Convert.ToInt32(Session["CLIENTONLINE"])).Result;
            service.Close();

            ViewBag.StatusForm = IsAllowChangeForm();

            return View(documents);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult Documents(FormCollection form)
        {
            if (IsAllowChangeForm())
            {
                var files = new ClientOnlineDocuments
                {
                    IdClientOnline = Convert.ToInt32(Session["CLIENTONLINE"])
                };

                for (int i = 0; i < Request.Files.Count; i++)
                {
                    var idDocument = Convert.ToInt32(Request.Files.Keys[i].Split('|')[1]);
                    var hasPysical = form["chk|" + idDocument] == null ? false : true;
                    var arquivo = Request.Files[i];

                    if (arquivo.ContentLength > 0)
                    {
                        byte[] bytes;
                        using (BinaryReader br = new BinaryReader(arquivo.InputStream))
                        {
                            bytes = br.ReadBytes(arquivo.ContentLength);
                        }

                        var doc = new ClientDocument
                        {
                            IdDocClient = idDocument,
                            ContentType = arquivo.ContentType,
                            Data = bytes,
                            FileName = arquivo.FileName,
                            HasPysical = hasPysical
                        };

                        files.Documents.Add(doc);
                    }
                    else
                    {
                        files.Documents.Add(new ClientDocument
                        {
                            IdDocClient = idDocument,
                            HasPysical = hasPysical
                        });
                    }
                }

                IChannelProvider<IClientRegistrationContractChannel> channelProvider = ChannelProviderFactory<IClientRegistrationContractChannel>.Create("ClientRegistrationClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();
                service.UpsertDocumentClientOnline(files);
                service.Close();
            }

            return RedirectToAction("Representation");
        }

        public ActionResult Representation()
        {
            var user = Session["USERNAME"];

            if (user == null)
                return RedirectToRoute("Index");

            ViewBag.Profile = Convert.ToString(Session["NomePerfil"]).ToLower();

            IChannelProvider<IClientRegistrationContractChannel> channelProviderClient = ChannelProviderFactory<IClientRegistrationContractChannel>.Create("ClientRegistrationClassName");
            var serviceClient = channelProviderClient.GetChannel().CreateChannel();
            serviceClient.Open();
            ViewBag.DocumentType = MontarRetornoComValorBranco(serviceClient.GetDocumentTypes().Result);
            ViewBag.Profession = MontarRetornoComValorBranco(serviceClient.GetProfessions().Result); ;
            ViewBag.MaritalStatus = MontarRetornoComValorBranco(serviceClient.GetMaritalStatus().Result);
            ViewBag.TelephoneType = MontarRetornoComValorBranco(serviceClient.GetTelephoneTypes().Result);
            ViewBag.AddressType = MontarRetornoComValorBranco(serviceClient.GetAddressTypes().Result);
            ViewBag.Finality = MontarRetornoComValorBranco(serviceClient.GetFinalities().Result);
            ViewBag.State = MontarRetornoComValorBranco(serviceClient.GetState().Result);
            ViewBag.Country = MontarRetornoComValorBranco(serviceClient.GetCountries().Result);
            ViewBag.Choice = MontarRetornoComValorBranco(serviceClient.GetChoice().Result);
            ViewBag.RepresentationStyle = MontarRetornoSemValorBranco(serviceClient.GetRepresentationStyle().Result);
            ViewBag.RepresentationProfile = MontarRetornoSemValorBranco(serviceClient.GetRepresentationProfile().Result);
            ViewBag.StatusForm = IsAllowChangeForm();

            var client = serviceClient.LoadRepresentatives(Convert.ToInt32(Session["CLIENTONLINE"])).Result;
            serviceClient.Close();

            var opcoes1 = new List<SelectListItem>()
            {
                new SelectListItem { Text = "Não", Value = "0" },
                new SelectListItem { Text = "Sim", Value = "1" }
            };

            ViewBag.Escolha = opcoes1;

            return View(client);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult Representation(ClientOnlineRepresentative clientRepresentative, FormCollection form)
        {
            clientRepresentative.IdRepresentation = form["IdRepresentation"] == "" ? 0 : Convert.ToInt32(form["IdRepresentation"]);
            clientRepresentative.Contact.AddRange(MontaContato(form["HdnContato"]));
            clientRepresentative.Address.AddRange(MontaEndereco(form["HdnEndereco"]));
            clientRepresentative.IdClientOnline = Convert.ToInt32(Session["CLIENTONLINE"]);

            IChannelProvider<IClientRegistrationContractChannel> channelProvider = ChannelProviderFactory<IClientRegistrationContractChannel>.Create("ClientRegistrationClassName");
            var service = channelProvider.GetChannel().CreateChannel();
            service.Open();

            service.UpsertRepresentative(clientRepresentative);

            service.Close();
            return RedirectToAction("Representation");
        }

        public JsonResult GetRepresentative(string idRepresentative)
        {
            IChannelProvider<IClientRegistrationContractChannel> channelProviderClient = ChannelProviderFactory<IClientRegistrationContractChannel>.Create("ClientRegistrationClassName");
            var serviceClient = channelProviderClient.GetChannel().CreateChannel();

            serviceClient.Open();
            var response = serviceClient.LoadRepresentative(Convert.ToInt32(Session["CLIENTONLINE"]), Convert.ToInt32(idRepresentative)).Result;
            serviceClient.Close();

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteRepresentative(string idRepresentative)
        {
            IChannelProvider<IClientRegistrationContractChannel> channelProviderClient = ChannelProviderFactory<IClientRegistrationContractChannel>.Create("ClientRegistrationClassName");
            var serviceClient = channelProviderClient.GetChannel().CreateChannel();

            serviceClient.Open();
            var response = serviceClient.DeleteRepresentative(Convert.ToInt32(Session["CLIENTONLINE"]), Convert.ToInt32(idRepresentative)).Result;
            serviceClient.Close();

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Rules()
        {
            var user = Session["USERNAME"];

            if (user == null)
                return RedirectToRoute("Index");

            IChannelProvider<IClientRegistrationContractChannel> channelProviderClient = ChannelProviderFactory<IClientRegistrationContractChannel>.Create("ClientRegistrationClassName");
            var serviceClient = channelProviderClient.GetChannel().CreateChannel();
            serviceClient.Open();

            var documents = serviceClient.LoadClientDocument(Convert.ToInt32(Session["CLIENTONLINE"])).Result;

            ViewBag.Tipo = MontarRetornoSemValorBranco(serviceClient.GetRulesTypes().Result);
            ViewBag.Qualification = MontarRetornoSemValorBranco(serviceClient.GetQualifications().Result);
            ViewBag.Documents = MontaDocumentos(documents);
            ViewBag.Representatives = MontaRepresentante(serviceClient.LoadRepresentatives(Convert.ToInt32(Session["CLIENTONLINE"])).Result);
            ViewBag.ClientOnline = Convert.ToInt32(Session["CLIENTONLINE"]);
            ViewBag.StatusForm = IsAllowChangeForm();

            return View(documents);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult Rules(FormCollection form)
        {
            IChannelProvider<IClientRegistrationContractChannel> channelProvider = ChannelProviderFactory<IClientRegistrationContractChannel>.Create("ClientRegistrationClassName");
            var service = channelProvider.GetChannel().CreateChannel();
            service.Open();

            var documentRep = new ClientOnlineRules
            {
                IdClientOnline = Convert.ToInt32(Session["CLIENTONLINE"]),
                IdDocument = Convert.ToInt32(form["IdDocument"]),
                StartValidateDocument = Convert.ToDateTime(form["StartValidateDocument"]),
                FinalValidateDocument = Convert.ToDateTime(form["FinalValidateDocument"]),
                IdDocumentType = Convert.ToInt32(form["IdDocumentType"])
            };
            documentRep.Representatives.AddRange(GetRepresentatives(form["HdnRepresentative"]));
            documentRep.DeleteRepresentatives.AddRange(GetDeletedRepresentatives(form["HdnRepresentativeDelete"]));

            service.UpsertDocumentRepresentative(documentRep);

            return RedirectToAction("Rules");
        }

        public ActionResult GetRules(int idDocument)
        {
            IChannelProvider<IClientRegistrationContractChannel> channelProviderClient = ChannelProviderFactory<IClientRegistrationContractChannel>.Create("ClientRegistrationClassName");
            var serviceClient = channelProviderClient.GetChannel().CreateChannel();

            serviceClient.Open();
            var response = serviceClient.LoadDocumentsRepresentatives(idDocument).Result;
            serviceClient.Close();

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public ActionResult FinishRegistration()
        {
            return View();
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult FinishRegistration(FormCollection form)
        {
            return RedirectToAction("FinishRegistration");
        }

        public JsonResult SubmitClient()
        {
            IChannelProvider<IClientRegistrationContractChannel> channelProvider = ChannelProviderFactory<IClientRegistrationContractChannel>.Create("ClientRegistrationClassName");
            var service = channelProvider.GetChannel().CreateChannel();

            service.Open();
            var response = service.SubmitClientOnline(Convert.ToInt32(Session["CLIENTONLINE"]));
            service.Close();

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ApproveClient()
        {
            IChannelProvider<IClientRegistrationContractChannel> channelProvider = ChannelProviderFactory<IClientRegistrationContractChannel>.Create("ClientRegistrationClassName");
            var service = channelProvider.GetChannel().CreateChannel();

            service.Open();
            var response = service.ApproveClientOnline(Convert.ToInt32(Session["CLIENTONLINE"])).Result;
            service.Close();

            CleanIdClientOnline();
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DisapproveClient()
        {
            IChannelProvider<IClientRegistrationContractChannel> channelProvider = ChannelProviderFactory<IClientRegistrationContractChannel>.Create("ClientRegistrationClassName");
            var service = channelProvider.GetChannel().CreateChannel();

            service.Open();
            var response = service.DisapproveClientOnline(Convert.ToInt32(Session["CLIENTONLINE"])).Result;
            service.Close();

            CleanIdClientOnline();
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public string RetornaNome(string fileName)
        {
            return fileName;
        }

        public ActionResult DownloadFile(int idFile)
        {
            IChannelProvider<IClientRegistrationContractChannel> channelProvider = ChannelProviderFactory<IClientRegistrationContractChannel>.Create("ClientRegistrationClassName");
            var service = channelProvider.GetChannel().CreateChannel();
            service.Open();

            var file = service.GetAssignById(idFile).Result;

            if (file != null && file.Id > 0)
                return File(file.Data, file.ContentType, file.FileName);

            return RedirectToAction("FinancialInformation");
        }

        public ActionResult DownloadModelDocument(int idFile)
        {
            IChannelProvider<IClientRegistrationContractChannel> channelProvider = ChannelProviderFactory<IClientRegistrationContractChannel>.Create("ClientRegistrationClassName");
            var service = channelProvider.GetChannel().CreateChannel();
            service.Open();

            var file = service.GetModelDocumentById(idFile).Result;

            service.Close();

            if (file != null && file.Id > 0)
                return File(file.Data, file.ContentType, file.FileName);

            return RedirectToAction("Documents");
        }

        public ActionResult DownloadDocument(int idFile)
        {
            IChannelProvider<IClientRegistrationContractChannel> channelProvider = ChannelProviderFactory<IClientRegistrationContractChannel>.Create("ClientRegistrationClassName");
            var service = channelProvider.GetChannel().CreateChannel();
            service.Open();

            var file = service.GetDocumentById(idFile).Result;

            service.Close();

            if (file != null && file.Id > 0)
                return File(file.Data, file.ContentType, file.FileName);

            return RedirectToAction("Documents");
        }

        [HttpGet]
        public bool RemoveFile(int idFile)
        {
            IChannelProvider<IClientRegistrationContractChannel> channelProvider = ChannelProviderFactory<IClientRegistrationContractChannel>.Create("ClientRegistrationClassName");
            var service = channelProvider.GetChannel().CreateChannel();

            service.Open();
            var response = service.DeleteAssignById(idFile).Result;
            service.Close();

            return response;
        }

        private List<SelectListItem> MontarRetornoComValorBranco(List<Parameters> parameters)
        {
            var lstParameters = new List<SelectListItem>();
            lstParameters.Add(new SelectListItem());

            if (parameters != null)
            {
                foreach (var item in parameters)
                {
                    lstParameters.Add(new SelectListItem { Text = item.Descricao, Value = item.Valor });
                }
            }

            return lstParameters;
        }

        private List<SelectListItem> MontarRetornoSemValorBranco(List<Parameters> parameters)
        {
            var lstParameters = new List<SelectListItem>();

            if (parameters != null)
            {
                foreach (var item in parameters)
                {
                    lstParameters.Add(new SelectListItem { Text = item.Descricao, Value = item.Valor });
                }
            }

            return lstParameters;
        }

        private List<SelectListItem> MontaObjeto(List<string> emails)
        {
            var lstParameters = new List<SelectListItem>();

            foreach (var item in emails)
            {
                lstParameters.Add(new SelectListItem { Text = item, Value = item });
            }

            return lstParameters;
        }

        private List<SelectListItem> MontaAssinaturas(List<ClientAssign> files)
        {
            var lstParameters = new List<SelectListItem>();

            foreach (var item in files)
            {
                lstParameters.Add(new SelectListItem { Value = Convert.ToString(item.Id), Text = item.FileName });
            }

            return lstParameters;
        }

        private List<ClientAssign> Upload()
        {
            var files = new List<ClientAssign>();

            for (int i = 0; i < Request.Files.Count; i++)
            {
                var arquivo = Request.Files[i];

                if (arquivo.ContentLength > 0)
                {
                    byte[] bytes;
                    using (BinaryReader br = new BinaryReader(arquivo.InputStream))
                    {
                        bytes = br.ReadBytes(arquivo.ContentLength);
                    }

                    files.Add(new ClientAssign
                    {
                        ContentType = arquivo.ContentType,
                        Data = bytes,
                        FileName = arquivo.FileName
                    });
                }
            }

            return files;
        }

        private List<ContactClientOnline> MontaContato(string dados)
        {
            if (!string.IsNullOrEmpty(dados))
            {
                var rows = dados.Split(';');
                var items = new List<ContactClientOnline>();

                foreach (var row in rows)
                {
                    var valores = row.Split(',');
                    var contact = new ContactClientOnline
                    {
                        Type = valores[0],
                        Ddi = valores[1],
                        Ddd = valores[2],
                        Number = valores[3],
                        Ramal = valores[4],
                        EmailContact = valores[5]
                    };
                    items.Add(contact);
                }

                return items;
            }

            return new List<ContactClientOnline>();
        }

        private List<DocumentRepresentative> GetRepresentatives(string dados)
        {
            if (!string.IsNullOrEmpty(dados))
            {
                var rows = dados.Split(';');
                var items = new List<DocumentRepresentative>();

                foreach (var row in rows)
                {
                    var valores = row.Split(',');
                    if (valores.Count() == 4)
                    {
                        var contact = new DocumentRepresentative
                        {
                            Id = Convert.ToInt32(valores[0]),
                            IdRepresentative = Convert.ToInt32(valores[1]),
                            NumberDocument = valores[2],
                            IdQualification = Convert.ToInt32(valores[3])
                        };
                        items.Add(contact);
                    }
                    else if (valores.Count() == 3)
                    {
                        var contact = new DocumentRepresentative
                        {
                            IdRepresentative = Convert.ToInt32(valores[0]),
                            NumberDocument = valores[1],
                            IdQualification = Convert.ToInt32(valores[2])
                        };
                        items.Add(contact);
                    }
                }

                return items;
            }

            return new List<DocumentRepresentative>();
        }

        private List<string> GetDeletedRepresentatives(string dadosDeletados)
        {
            if (!string.IsNullOrEmpty(dadosDeletados))
            {
                var rows = dadosDeletados.Split(';');
                var items = new List<string>();

                foreach (var row in rows)
                {
                    var valor = Convert.ToString(row.Split(',')[0]);

                    if (valor != "")
                        items.Add(valor);
                }

                return items;
            }

            return new List<string>();
        }

        private List<AddressClientOnline> MontaEndereco(string dados)
        {
            if (!string.IsNullOrEmpty(dados))
            {
                var rows = dados.Split(';');
                var items = new List<AddressClientOnline>();

                foreach (var row in rows)
                {
                    var valores = row.Split(',');
                    var address = new AddressClientOnline
                    {
                        AddressType = valores[0],
                        Finality = valores[1],
                        StreetType = valores[2],
                        StreetName = valores[3],
                        Number = valores[4],
                        City = valores[5],
                        State = valores[6],
                        Country = valores[7]
                    };
                    items.Add(address);
                }

                return items;
            }

            return new List<AddressClientOnline>();
        }

        private List<FinancialAccount> MontaConta(string dados)
        {
            if (!string.IsNullOrEmpty(dados))
            {
                var rows = dados.Split(';');
                var items = new List<FinancialAccount>();

                foreach (var row in rows)
                {
                    var valores = row.Split(',');
                    var address = new FinancialAccount
                    {
                        IdFinality = valores[0] == "" ? 0 : Convert.ToInt32(valores[0]),
                        IdUtility = valores[1] == "" ? 0 : Convert.ToInt32(valores[1]),
                        IdAccountType = valores[2] == "" ? 0 : Convert.ToInt32(valores[2]),
                        IdAccountTypeBank = valores[3] == "" ? 0 : Convert.ToInt32(valores[3]),
                        IdBank = valores[4] == "" ? 0 : Convert.ToInt32(valores[4]),
                        Agency = valores[5],
                        NumberAccount = valores[6]
                    };
                    items.Add(address);
                }

                return items;
            }

            return new List<FinancialAccount>();
        }

        private List<SelectListItem> MontaProdutoCliente(List<FinancialProduct> products)
        {
            var lstParameters = new List<SelectListItem>();

            foreach (var item in products)
            {
                lstParameters.Add(new SelectListItem { Text = item.DescriptionProduct, Value = Convert.ToString(item.IdProduct) });
            }

            return lstParameters;
        }

        private List<SelectListItem> MontaRepresentante(Representatives representatives)
        {
            var lstRepresentatives = new List<SelectListItem>();

            foreach (var item in representatives.ClientRepresentatives)
            {
                lstRepresentatives.Add(new SelectListItem { Text = item.Name, Value = Convert.ToString(item.IdRepresentation) });
            }

            return lstRepresentatives;
        }

        private List<SelectListItem> MontaDocumentos(ClientOnlineDocuments clientOnlineDocuments)
        {
            var lstRepresentatives = new List<SelectListItem>();

            foreach (var item in clientOnlineDocuments.Documents)
            {
                lstRepresentatives.Add(new SelectListItem { Text = item.Description, Value = Convert.ToString(item.Id) });
            }

            return lstRepresentatives;
        }

        private int RegisterNewClient(ACUser user, string password)
        {
            PasswordGenerator pg = new PasswordGenerator();
            IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
            var service = channelProvider.GetChannel().CreateChannel();
            service.Open();

            var prof = GetProfile("Pre-Cadastro", service);

            user.Password = pg.EncodePassword(Convert.ToString(password));
            user.Profile = prof;
            user.Type = "P";
            user.ActiveStatus = 1;
            user.ChaveAD = GenerateChaveAD();

            AccessControlHelper helper = new AccessControlHelper();

            if (helper.CpfCadastrado(user.CpfCnpj))
            {
                TempData["EmailMessage"] = "CPF já cadastrado";
                throw new Exception();
            }

            if (helper.EmailCadastrado(user.Email))
            {
                TempData["EmailMessage"] = "Email já cadastrado";
                throw new Exception();
            }

            var preClient = prof;

            if (preClient == null)
                throw new Exception();

            var reponse = service.InsertClientOnline(user).Result;
            service.Close();
            var feedbackEmail = helper.SendWelcomeEmail(user.Email, user.Name);

            return reponse;
        }

        private bool IsAllowChangeForm()
        {
            var statusForm = Convert.ToInt32(Session["STATUSFORM"]);
            var profile = Convert.ToString(Session["NomePerfil"]);

            if (profile == "Cadastro")
                return true;
            else if (statusForm != 2)
                return true;

            return false;
        }

        private void CleanIdClientOnline()
        {
            Session["CLIENTONLINE"] = "";
        }
    }

    #endregion
}