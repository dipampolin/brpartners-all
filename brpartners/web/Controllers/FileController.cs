﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.Services.SagazWebService;
using QX3.Portal.WebSite.Models;

namespace QX3.Portal.WebSite.Controllers
{
    public class FileController : BaseController
    {
        protected ActionResult ViewZip(List<ZipItem> streamFiles, bool download, string folderPath, string fileName)
        {
            ZipHelper helper = new ZipHelper();
            return new BinaryContentResult(helper.GenerateZipFile(streamFiles, folderPath), "application/zip", download, fileName);
        }

        #region Reports

        protected ActionResult ViewConfirmationPdf(Confirmation report, bool download, string fileName)
        {
            PDFHelper helper = new PDFHelper();

            MemoryStream memPDF = helper.CreateConfirmationToMemoryStream(report);
            byte[] buf = new byte[memPDF.Length];
            memPDF.Position = 0;
            memPDF.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/pdf", download, fileName);
        }

        protected ActionResult ViewConfirmationExcel(Confirmation report, bool download, string fileName)
        {
            ExcelHelper helper = new ExcelHelper();

            MemoryStream memExcel = helper.CreateConfirmationToMemoryStream(report);
            byte[] buf = new byte[memExcel.Length];
            memExcel.Position = 0;
            memExcel.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/vnd.xls", download, fileName);
        }

        protected ActionResult ViewTradeBlotterExcel(TradeBlotter report, bool download, string fileName)
        {
            ExcelHelper helper = new ExcelHelper();

            MemoryStream memExcel = helper.CreateTradeBlotterToMemoryStream(report);
            byte[] buf = new byte[memExcel.Length];
            memExcel.Position = 0;
            memExcel.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/vnd.xls", download, fileName);
        }

        protected ActionResult ViewTradeBlotterPdf(TradeBlotter report, bool download, string fileName)
        {
            PDFHelper helper = new PDFHelper();

            MemoryStream memPDF = helper.CreateTradeBlotterToMemoryStream(report);
            byte[] buf = new byte[memPDF.Length];
            memPDF.Position = 0;
            memPDF.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/pdf", download, fileName);
        }

        protected ActionResult ViewCustomerStatementPdf(CustomerStatement report, bool download, string fileName)
        {
            PDFHelper helper = new PDFHelper();

            MemoryStream memPDF = helper.CreateCustomerStatementToMemoryStream(report);
            byte[] buf = new byte[memPDF.Length];
            memPDF.Position = 0;
            memPDF.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/pdf", download, fileName);
        }

        protected ActionResult ViewCustomerStatementExcel(CustomerStatement report, bool download, string fileName)
        {
            ExcelHelper helper = new ExcelHelper();

            MemoryStream memExcel = helper.CreateCustomerStatementToMemoryStream(report);
            byte[] buf = new byte[memExcel.Length];
            memExcel.Position = 0;
            memExcel.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/vnd.xls", download, fileName);
        }

        protected ActionResult ViewCustomerLedgerPdf(CustomerLedger report, bool download, string fileName)
        {
            PDFHelper helper = new PDFHelper();

            MemoryStream memPDF = helper.CreateCustomerLedgerToMemoryStream(report);
            byte[] buf = new byte[memPDF.Length];
            memPDF.Position = 0;
            memPDF.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/pdf", download, fileName);
        }

        protected ActionResult ViewCustomerLedgerExcel(CustomerLedger report, bool download, string fileName)
        {
            ExcelHelper helper = new ExcelHelper();

            MemoryStream memExcel = helper.CreateCustomerLedgerToMemoryStream(report);
            byte[] buf = new byte[memExcel.Length];
            memExcel.Position = 0;
            memExcel.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/vnd.xls", download, fileName);
        }

        protected ActionResult ViewStockRecordPdf(StockRecord report, bool download, string fileName)
        {
            PDFHelper helper = new PDFHelper();

            MemoryStream memPDF = helper.CreateStockRecordToMemoryStream(report);
            byte[] buf = new byte[memPDF.Length];
            memPDF.Position = 0;
            memPDF.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/pdf", download, fileName);
        }

        protected ActionResult ViewStockRecordExcel(StockRecord report, bool download, string fileName)
        {
            ExcelHelper helper = new ExcelHelper();

            MemoryStream memExcel = helper.CreateStockRecordToMemoryStream(report);
            byte[] buf = new byte[memExcel.Length];
            memExcel.Position = 0;
            memExcel.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/vnd.xls", download, fileName);
        }

        protected ActionResult ViewFailLedgerPdf(FailLedger report, bool download, string fileName)
        {
            PDFHelper helper = new PDFHelper();

            MemoryStream memPDF = helper.CreateFailLedgerToMemoryStream(report);
            byte[] buf = new byte[memPDF.Length];
            memPDF.Position = 0;
            memPDF.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/pdf", download, fileName);
        }

        protected ActionResult ViewFailLedgerExcel(FailLedger report, bool download, string fileName)
        {
            ExcelHelper helper = new ExcelHelper();

            MemoryStream memExcel = helper.CreateFailLedgerToMemoryStream(report);
            byte[] buf = new byte[memExcel.Length];
            memExcel.Position = 0;
            memExcel.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/vnd.xls", download, fileName);
        }

        protected ActionResult ViewBrokeragePdf(List<BrokerageTransfer> transfers, bool download, string fileName, string startDate, string endDate, ReportHeader header)
        {
            PDFHelper helper = new PDFHelper();

            MemoryStream memPDF = helper.CreateBrokerageToMemoryStream(transfers, startDate, endDate, header);
            byte[] buf = new byte[memPDF.Length];
            memPDF.Position = 0;
            memPDF.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/pdf", download, fileName);
        }

        protected ActionResult ViewBrokerageExcel(List<BrokerageTransfer> transfers, bool download, string fileName, string startDate, string endDate, bool pending)
        {
            ExcelHelper helper = new ExcelHelper();

            MemoryStream memExcel = helper.CreateBrokerageToMemoryStream(transfers, pending, startDate, endDate);
            byte[] buf = new byte[memExcel.Length];
            memExcel.Position = 0;
            memExcel.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/vnd.xls", download, fileName);
        }

        #endregion

        #region Access Control

        protected ActionResult ViewGroupOfAssessorsPdf(List<GroupOfAssessorsListItem> groups, bool download, string fileName)
        {
            PDFHelper helper = new PDFHelper();

            MemoryStream memPDF = helper.CreateGroupOfAssessorsToMemoryStream(groups);
            byte[] buf = new byte[memPDF.Length];
            memPDF.Position = 0;
            memPDF.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/pdf", download, fileName);
        }

        protected ActionResult ViewGroupOfAssessorsExcel(List<GroupOfAssessorsListItem> groups, bool download, string fileName)
        {
            ExcelHelper helper = new ExcelHelper();

            MemoryStream memExcel = helper.CreateGroupsOfAssessorsToMemoryStream(groups);
            byte[] buf = new byte[memExcel.Length];
            memExcel.Position = 0;
            memExcel.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/vnd.xls", download, fileName);
        }

        protected ActionResult ViewHistoryPdf(List<HistoryData> data, bool download, string fileName, string targetTitle, string targetColumnTitle, bool hasAction, string extraTargetColumnTitle = null)
        {
            PDFHelper helper = new PDFHelper();

            MemoryStream memPDF = helper.CreateHistoryToMemoryStream(data, targetTitle, targetColumnTitle, hasAction, extraTargetColumnTitle);
            byte[] buf = new byte[memPDF.Length];
            memPDF.Position = 0;
            memPDF.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/pdf", download, fileName);
        }

        protected ActionResult ViewHistoryExcel(List<HistoryData> data, bool download, string fileName, string targetTitle, string targetColumnTitle, bool hasAction, string extraTargetTitle = null)
        {
            ExcelHelper helper = new ExcelHelper();

            MemoryStream memExcel = helper.CreateHistoryToMemoryStream(data, targetTitle, targetColumnTitle, hasAction, extraTargetTitle);
            byte[] buf = new byte[memExcel.Length];
            memExcel.Position = 0;
            memExcel.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/vnd.xls", download, fileName);
        }

        //protected ActionResult ViewFundHistoryPdf(List<HistoryData> data, bool download, string fileName, string targetTitle, string targetColumnTitle, bool hasAction, string extraTargetColumnTitle = null)
        //{
        //    PDFHelper helper = new PDFHelper();

        //    MemoryStream memPDF = helper.CreateFundHistoryToMemoryStream(data, targetTitle, targetColumnTitle, hasAction, extraTargetColumnTitle);
        //    byte[] buf = new byte[memPDF.Length];
        //    memPDF.Position = 0;
        //    memPDF.Read(buf, 0, buf.Length);

        //    return new BinaryContentResult(buf, "application/pdf", download, fileName);
        //}

        //protected ActionResult ViewFundHistoryExcel(List<HistoryData> data, bool download, string fileName, string targetTitle, string targetColumnTitle, bool hasAction, string extraTargetTitle = null)
        //{
        //    ExcelHelper helper = new ExcelHelper();

        //    MemoryStream memExcel = helper.CreateFundHistoryToMemoryStream(data, targetTitle, targetColumnTitle, hasAction, extraTargetTitle);
        //    byte[] buf = new byte[memExcel.Length];
        //    memExcel.Position = 0;
        //    memExcel.Read(buf, 0, buf.Length);

        //    return new BinaryContentResult(buf, "application/vnd.xls", download, fileName);
        //}

        protected ActionResult ViewUsersExcel(List<UsersListItem> users, bool download, string fileName)
        {
            ExcelHelper helper = new ExcelHelper();

            MemoryStream memExcel = helper.CreateUsersToMemoryStream(users);
            byte[] buf = new byte[memExcel.Length];
            memExcel.Position = 0;
            memExcel.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/vnd.xls", download, fileName);
        }

        protected ActionResult ViewUsersPdf(List<UsersListItem> users, bool download, string fileName)
        {
            PDFHelper helper = new PDFHelper();

            MemoryStream memPDF = helper.CreateUsersToMemoryStream(users);
            byte[] buf = new byte[memPDF.Length];
            memPDF.Position = 0;
            memPDF.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/pdf", download, fileName);
        }

        protected ActionResult ViewProfilesPdf(List<ProfilesListItem> groups, bool download, string fileName)
        {
            PDFHelper helper = new PDFHelper();

            MemoryStream memPDF = helper.CreateProfilesToMemoryStream(groups);
            byte[] buf = new byte[memPDF.Length];
            memPDF.Position = 0;
            memPDF.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/pdf", download, fileName);
        }

        protected ActionResult ViewProfilesExcel(List<ProfilesListItem> groups, bool download, string fileName)
        {
            ExcelHelper helper = new ExcelHelper();

            MemoryStream memExcel = helper.CreateProfilesToMemoryStream(groups);
            byte[] buf = new byte[memExcel.Length];
            memExcel.Position = 0;
            memExcel.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/vnd.xls", download, fileName);
        }

		protected ActionResult ViewClientsRegisterExcel(List<FuncionalityClient> list, bool download, string fileName)
		{
			ExcelHelper helper = new ExcelHelper();

			MemoryStream memExcel = helper.CreateClientsRegisterToMemoryStream(list);
			byte[] buf = new byte[memExcel.Length];
			memExcel.Position = 0;
			memExcel.Read(buf, 0, buf.Length);

			return new BinaryContentResult(buf, "application/vnd.xls", download, fileName);
		}

		protected ActionResult ViewClientsRegisterPdf(List<FuncionalityClient> list, bool download, string fileName)
		{
			PDFHelper helper = new PDFHelper();

			MemoryStream memPDF = helper.CreateClientsRegisterToMemoryStream(list);
			byte[] buf = new byte[memPDF.Length];
			memPDF.Position = 0;
			memPDF.Read(buf, 0, buf.Length);

			return new BinaryContentResult(buf, "application/pdf", download, fileName);
		}

        #endregion

        #region Other

        protected ActionResult ViewOrdersCorrectionExcel(List<OrdersCorrection> orders, bool download, string fileName)
        {
            ExcelHelper helper = new ExcelHelper();

            MemoryStream memExcel = helper.CreateOrdersToMemoryStream(orders);
            byte[] buf = new byte[memExcel.Length];
            memExcel.Position = 0;
            memExcel.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/vnd.xls", download, fileName);
        }

        protected ActionResult ViewOrdersCorrectionPdf(List<OrdersCorrection> orders, bool download, string fileName)
        {
            PDFHelper helper = new PDFHelper();

            MemoryStream memPDF = helper.CreateOrdersToMemoryStream(orders);
            byte[] buf = new byte[memPDF.Length];
            memPDF.Position = 0;
            memPDF.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/pdf", download, fileName);
        }

		protected ActionResult ViewBrokerageDiscountExcel(List<BrokerageDiscount> discounts, bool download, string fileName)
		{
			ExcelHelper helper = new ExcelHelper();

			MemoryStream memExcel = helper.CreateDiscountsToMemoryStream(discounts);
			byte[] buf = new byte[memExcel.Length];
			memExcel.Position = 0;
			memExcel.Read(buf, 0, buf.Length);

			return new BinaryContentResult(buf, "application/vnd.xls", download, fileName);
		}

		protected ActionResult ViewBrokerageDiscountPdf(List<BrokerageDiscount> discounts, bool download, string fileName)
		{
			PDFHelper helper = new PDFHelper();

			MemoryStream memPDF = helper.CreateDiscountsToMemoryStream(discounts);
			byte[] buf = new byte[memPDF.Length];
			memPDF.Position = 0;
			memPDF.Read(buf, 0, buf.Length);

			return new BinaryContentResult(buf, "application/pdf", download, fileName);
		}

		protected ActionResult ViewCustodyTransferExcel(List<CustodyTransfer> list, bool download, string fileName)
		{
			ExcelHelper helper = new ExcelHelper();

			MemoryStream memExcel = helper.CreateCustodyTransferToMemoryStream(list);
			byte[] buf = new byte[memExcel.Length];
			memExcel.Position = 0;
			memExcel.Read(buf, 0, buf.Length);

			return new BinaryContentResult(buf, "application/vnd.xls", download, fileName);
		}

		protected ActionResult ViewCustodyTransferPdf(List<CustodyTransfer> list, bool download, string fileName)
		{
			PDFHelper helper = new PDFHelper();

			MemoryStream memPDF = helper.CreateCustodyTransferToMemoryStream(list);
			byte[] buf = new byte[memPDF.Length];
			memPDF.Position = 0;
			memPDF.Read(buf, 0, buf.Length);

			return new BinaryContentResult(buf, "application/pdf", download, fileName);
		}

		protected ActionResult ViewCustodyTransferReportPdf(CustodyTransferReport report, Byte[] reportChart, bool download, string fileName)
		{
			PDFHelper helper = new PDFHelper();

			MemoryStream memPDF = helper.CreateCustodyTransferReportToMemoryStream(report, reportChart);
			byte[] buf = new byte[memPDF.Length];
			memPDF.Position = 0;
			memPDF.Read(buf, 0, buf.Length);

			return new BinaryContentResult(buf, "application/pdf", download, fileName);
		}

		protected ActionResult ViewOperationsMapExcel(List<OperationsMap> list, bool download, string fileName)
        {
            ExcelHelper helper = new ExcelHelper();

			MemoryStream memExcel = helper.CreateOperationsMapToMemoryStream(list);
            byte[] buf = new byte[memExcel.Length];
            memExcel.Position = 0;
            memExcel.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/vnd.xls", download, fileName);
        }

		protected ActionResult ViewOperationsMapPdf(List<OperationsMap> list, bool download, string fileName)
        {
            PDFHelper helper = new PDFHelper();

			MemoryStream memPDF = helper.CreateOperationsMapToMemoryStream(list);
            byte[] buf = new byte[memPDF.Length];
            memPDF.Position = 0;
            memPDF.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/pdf", download, fileName);
        }

		protected ActionResult ViewOperationsExcel(List<OperationClient> list, bool download, string fileName)
		{
			ExcelHelper helper = new ExcelHelper();

			MemoryStream memExcel = helper.CreateOperationsToMemoryStream(list);
			byte[] buf = new byte[memExcel.Length];
			memExcel.Position = 0;
			memExcel.Read(buf, 0, buf.Length);

			return new BinaryContentResult(buf, "application/vnd.xls", download, fileName);
		}

		protected ActionResult ViewOperationsPdf(List<OperationClient> list, bool download, string fileName)
		{
			PDFHelper helper = new PDFHelper();

			MemoryStream memPDF = helper.CreateOperationsToMemoryStream(list);
			byte[] buf = new byte[memPDF.Length];
			memPDF.Position = 0;
			memPDF.Read(buf, 0, buf.Length);

			return new BinaryContentResult(buf, "application/pdf", download, fileName);
		}

		protected ActionResult ViewOperationsReportPdf(OperationReport report, CPClientInformation client, bool download, string fileName)
        {
            PDFHelper helper = new PDFHelper();

			MemoryStream memPDF = helper.CreateOperationsReportToMemoryStream(report, client);
            byte[] buf = new byte[memPDF.Length];
            memPDF.Position = 0;
            memPDF.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/pdf", download, fileName);
        }

		protected ActionResult ViewIncomeReportPdf(IncomeReport item, Dictionary<string, bool> checks, bool download, string fileName)
		{
			PDFHelper helper = new PDFHelper();

			MemoryStream memPDF = helper.CreateIncomeReportPositionToMemoryStream(item, checks);
			byte[] buf = new byte[memPDF.Length];
			memPDF.Position = 0;
			memPDF.Read(buf, 0, buf.Length);

			return new BinaryContentResult(buf, "application/pdf", download, fileName);
		}

		protected ActionResult ViewIncomeReportExcel(IncomeReport item, Dictionary<string, bool> checks, bool download, string fileName)
		{
			ExcelHelper helper = new ExcelHelper();

			MemoryStream memExcel = helper.CreateIncomeReportPositionToMemoryStream(item, checks);
			byte[] buf = new byte[memExcel.Length];
			memExcel.Position = 0;
			memExcel.Read(buf, 0, buf.Length);

			return new BinaryContentResult(buf, "application/vnd.xls", download, fileName);
		}

		protected ActionResult ViewConsolidatedPositionPdf(ConsolidatedPositionResponse item, bool download, string fileName)
		{
			PDFHelper helper = new PDFHelper();

			MemoryStream memPDF = helper.CreateConsolidatedPositionToMemoryStream(item);
			byte[] buf = new byte[memPDF.Length];
			memPDF.Position = 0;
			memPDF.Read(buf, 0, buf.Length);

			return new BinaryContentResult(buf, "application/pdf", download, fileName);
		}

        protected ActionResult ViewPrivateReportExcel(List<Private> item, string refDate, bool download, string fileName)
		{
			ExcelHelper helper = new ExcelHelper();

            MemoryStream memExcel = helper.CreatePrivateReportToMemoryStream(item, refDate);
			byte[] buf = new byte[memExcel.Length];
			memExcel.Position = 0;
			memExcel.Read(buf, 0, buf.Length);

			return new BinaryContentResult(buf, "application/vnd.xls", download, fileName);
		}

        protected ActionResult ViewPrivateReportPdf(List<Private> item, string refDate, bool download, string fileName)
        {
            PDFHelper helper = new PDFHelper();

            MemoryStream memPDF = helper.CreatePrivateReportToMemoryStream(item, refDate);
            byte[] buf = new byte[memPDF.Length];
            memPDF.Position = 0;
            memPDF.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/pdf", download, fileName);
        }

		protected ActionResult ViewBBRentExcel(BBRent item, bool download, string fileName)
		{
			ExcelHelper helper = new ExcelHelper();

			MemoryStream memExcel = helper.CreateBBRentToMemoryStream(item);
			byte[] buf = new byte[memExcel.Length];
			memExcel.Position = 0;
			memExcel.Read(buf, 0, buf.Length);

			return new BinaryContentResult(buf, "application/vnd.xls", download, fileName);
		}

        protected ActionResult ViewVAMExcel(List<VAMItem> list, bool download, string fileName)
        {
            ExcelHelper helper = new ExcelHelper();

            MemoryStream memExcel = helper.CreateVAMToMemoryStream(list);
            byte[] buf = new byte[memExcel.Length];
            memExcel.Position = 0;
            memExcel.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/vnd.xls", download, fileName);
        }

        #endregion

		#region Financial

		protected ActionResult ViewRedemptionRequestItemsExcel(List<RedemptionRequestListItem> list, bool download, string fileName)
		{
			ExcelHelper helper = new ExcelHelper();

			MemoryStream memExcel = helper.CreateRedemptionRequestItemsToMemoryStream(list);
			byte[] buf = new byte[memExcel.Length];
			memExcel.Position = 0;
			memExcel.Read(buf, 0, buf.Length);

			return new BinaryContentResult(buf, "application/vnd.xls", download, fileName);
		}

		protected ActionResult ViewRedemptionRequestItemsPdf(List<RedemptionRequestListItem> list, bool download, string fileName)
		{
			PDFHelper helper = new PDFHelper();

			MemoryStream memPDF = helper.CreateRedemptionRequestItemsToMemoryStream(list);
			byte[] buf = new byte[memPDF.Length];
			memPDF.Position = 0;
			memPDF.Read(buf, 0, buf.Length);

			return new BinaryContentResult(buf, "application/pdf", download, fileName);
		}

		protected ActionResult ViewDailyFinancialReportExcel(List<DailyFinancial> list, bool download, string fileName)
		{
			ExcelHelper helper = new ExcelHelper();

			MemoryStream memExcel = helper.CreateDailyFinancialReportToMemoryStream(list);
			byte[] buf = new byte[memExcel.Length];
			memExcel.Position = 0;
			memExcel.Read(buf, 0, buf.Length);

			return new BinaryContentResult(buf, "application/vnd.xls", download, fileName);
		}

		protected ActionResult ViewDailyFinancialReportPdf(List<DailyFinancial> list, bool download, string fileName)
		{
			PDFHelper helper = new PDFHelper();

			MemoryStream memPDF = helper.CreateDailyFinancialReportToMemoryStream(list);
			byte[] buf = new byte[memPDF.Length];
			memPDF.Position = 0;
			memPDF.Read(buf, 0, buf.Length);

			return new BinaryContentResult(buf, "application/pdf", download, fileName);
		}

		#endregion

        #region Proceeds

        protected ActionResult ViewProceedsPdf(List<QX3.Portal.Contracts.DataContracts.Proceeds> list, bool download, string fileName, string groupBy)
        {
            PDFHelper helper = new PDFHelper();

            MemoryStream memPDF = helper.CreateProceedsToMemoryStream(list, groupBy);
            byte[] buf = new byte[memPDF.Length];
            memPDF.Position = 0;
            memPDF.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/pdf", download, fileName);
        }

        protected ActionResult ViewProceedsExcel(List<QX3.Portal.Contracts.DataContracts.Proceeds> list, bool download, string fileName, string groupBy)
        {
            ExcelHelper helper = new ExcelHelper();

            MemoryStream memExcel = helper.CreateProceedsToMemoryStream(list, groupBy);
            byte[] buf = new byte[memExcel.Length];
            memExcel.Position = 0;
            memExcel.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/vnd.xls", download, fileName);
        }

        protected ActionResult ViewTermPdf(List<TermListItem> list, bool download, string fileName, string groupBy)
        {
            PDFHelper helper = new PDFHelper();

            MemoryStream memPDF = helper.CreateTermToMemoryStream(list, groupBy);
            byte[] buf = new byte[memPDF.Length];
            memPDF.Position = 0;
            memPDF.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/pdf", download, fileName);
        }

        protected ActionResult ViewTermExcel(List<TermListItem> list, bool download, string fileName, string groupBy)
        {
            ExcelHelper helper = new ExcelHelper();

            MemoryStream memExcel = helper.CreateTermToMemoryStream(list, groupBy);
            byte[] buf = new byte[memExcel.Length];
            memExcel.Position = 0;
            memExcel.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/vnd.xls", download, fileName);
        }


        #endregion

        #region Risk

        protected ActionResult ViewGuaranteesExcel(List<GuaranteeListItem> guarantees, bool download, string fileName)
        {
            ExcelHelper helper = new ExcelHelper();

            MemoryStream memExcel = helper.CreateGuaranteesToMemoryStream(guarantees);
            byte[] buf = new byte[memExcel.Length];
            memExcel.Position = 0;
            memExcel.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/vnd.xls", download, fileName);
        }

        protected ActionResult ViewGuaranteesPdf(List<GuaranteeListItem> guarantees, bool download, string fileName)
        {
            PDFHelper helper = new PDFHelper();

            MemoryStream memPDF = helper.CreateGuaranteesToMemoryStream(guarantees);
            byte[] buf = new byte[memPDF.Length];
            memPDF.Position = 0;
            memPDF.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/pdf", download, fileName);
        }

        #endregion

        #region Online Portfolio

        protected ActionResult ViewAdminClientsExcel(List<PortfolioClientListItem> adminClients, bool download, string fileName)
        {
            ExcelHelper helper = new ExcelHelper();

            MemoryStream memExcel = helper.CreateAdminClientsToMemoryStream(adminClients);
            byte[] buf = new byte[memExcel.Length];
            memExcel.Position = 0;
            memExcel.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/vnd.xls", download, fileName);
        }

        protected ActionResult ViewAdminClientsPdf(List<PortfolioClientListItem> orders, bool download, string fileName)
        {
            PDFHelper helper = new PDFHelper();

            MemoryStream memPDF = helper.CreateAdminClientsToMemoryStream(orders);
            byte[] buf = new byte[memPDF.Length];
            memPDF.Position = 0;
            memPDF.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/pdf", download, fileName);
        }

        protected ActionResult ViewPortfolioContactsToFile(PortfolioContact clientContacts, bool download, string fileName, bool pdf)
        {
            MemoryStream mem = null;
            if (pdf)
            {
                PDFHelper pdfHelper = new PDFHelper();
                mem = pdfHelper.CreatePortfolioContactsToMemoryStream(clientContacts);
            }
            else
            {
                ExcelHelper excelHelper = new ExcelHelper();
                mem = excelHelper.CreatePortfolioContactsToMemoryStream(clientContacts);
            }

            byte[] buf = new byte[mem.Length];
            mem.Position = 0;
            mem.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, pdf ? "application/pdf" : "application/vnd.xls", download, fileName);
        }

        protected ActionResult ViewPortfolioPositionToFile(PortfolioPositionClientContent clientPosition, bool download, string fileName, bool pdf)
        {
            MemoryStream mem = null;
            if (pdf)
            {
                PDFHelper pdfHelper = new PDFHelper();
                mem = pdfHelper.CreatePortfolioPositionToMemoryStream(clientPosition);
            }
            else
            {
                ExcelHelper excelHelper = new ExcelHelper();
                mem = excelHelper.CreatePortfolioPositionToMemoryStream(clientPosition);
            }

            byte[] buf = new byte[mem.Length];
            mem.Position = 0;
            mem.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, pdf ? "application/pdf" : "application/vnd.xls", download, fileName);
        }

        protected ActionResult ViewPortfolioHistoryToFile(PortfolioHistoryClientContent clientPosition, bool download, string fileName, bool pdf)
        {
            MemoryStream mem = null;
            if (pdf)
            {
                PDFHelper pdfHelper = new PDFHelper();
                mem = pdfHelper.CreatePortfolioHistoryToMemoryStream(clientPosition);
            }
            else
            {
                ExcelHelper excelHelper = new ExcelHelper();
                mem = excelHelper.CreatePortfolioHistoryToMemoryStream(clientPosition);
            }

            byte[] buf = new byte[mem.Length];
            mem.Position = 0;
            mem.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, pdf ? "application/pdf" : "application/vnd.xls", download, fileName);
        }

        protected ActionResult ViewAdminPositionPdf(PortfolioPosition position, bool download, string fileName, bool isPdf)
        {
            MemoryStream mem = null;
            if (isPdf)
            {
                PDFHelper pdfHelper = new PDFHelper();
                mem = pdfHelper.CreateAdminPositionToMemoryStream(position);
            }
            else
            {
                ExcelHelper excelHelper = new ExcelHelper();
                mem = excelHelper.CreateAdminPositionToMemoryStream(position);
            }

            byte[] buf = new byte[mem.Length];
            mem.Position = 0;
            mem.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, isPdf ? "application/pdf" : "application/vnd.xls", download, fileName);
        }

        #endregion

        #region Financial

        protected ActionResult ViewProjectedReportToFile(List<ProjectedReportItem> list, bool download, string fileName, string groupBy, bool pdf)
        {
            MemoryStream mem = null;
            if (pdf)
            {
                PDFHelper pdfHelper = new PDFHelper();
                mem = pdfHelper.CreateProjectedReportToMemoryStream(list, groupBy);
            }
            else
            {
                ExcelHelper excelHelper = new ExcelHelper();
                mem = excelHelper.CreateProjectedReportToMemoryStream(list, groupBy);
            }

            byte[] buf = new byte[mem.Length];
            mem.Position = 0;
            mem.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, pdf ? "application/pdf" : "application/vnd.xls", download, fileName);
        }

        #endregion

		#region Subscription

		protected ActionResult ViewSubscriptionsExcel(List<Subscription> list, bool download, string fileName)
		{
			ExcelHelper helper = new ExcelHelper();

			MemoryStream memExcel = helper.CreateSubscriptionsToMemoryStream(list);
			byte[] buf = new byte[memExcel.Length];
			memExcel.Position = 0;
			memExcel.Read(buf, 0, buf.Length);

			return new BinaryContentResult(buf, "application/vnd.xls", download, fileName);
		}

		protected ActionResult ViewSubscriptionsPdf(List<Subscription> list, bool download, string fileName)
		{
			PDFHelper helper = new PDFHelper();

			MemoryStream memPDF = helper.CreateSubscriptionsToMemoryStream(list);
			byte[] buf = new byte[memPDF.Length];
			memPDF.Position = 0;
			memPDF.Read(buf, 0, buf.Length);

			return new BinaryContentResult(buf, "application/pdf", download, fileName);
		}

		protected ActionResult ViewRequestRightsExcel(Subscription subscription, List<SubscriptionRights> list, bool download, string fileName)
		{
			ExcelHelper helper = new ExcelHelper();

			MemoryStream memExcel = helper.CreateRequestRightsToMemoryStream(subscription, list);
			byte[] buf = new byte[memExcel.Length];
			memExcel.Position = 0;
			memExcel.Read(buf, 0, buf.Length);

			return new BinaryContentResult(buf, "application/vnd.xls", download, fileName);
		}

		protected ActionResult ViewRequestRightsPdf(Subscription subscription, List<SubscriptionRights> list, bool download, string fileName)
		{
			PDFHelper helper = new PDFHelper();

			MemoryStream memPDF = helper.CreateRequestRightsToMemoryStream(subscription, list);
			byte[] buf = new byte[memPDF.Length];
			memPDF.Position = 0;
			memPDF.Read(buf, 0, buf.Length);

			return new BinaryContentResult(buf, "application/pdf", download, fileName);
		}

		#endregion

		#region Funds

        //protected ActionResult ViewFundsBalanceExcel(
        //    FundClient client, 
        //    FundBalance balance, 
        //    List<FundBalancePendingApplicationItem> pending,
        //    List<FundBalanceChartItemResponse> chart, 
        //    bool download, string fileName)
        //{
        //    ExcelHelper helper = new ExcelHelper();

        //    MemoryStream memExcel = helper.CreateFundsBalanceToMemoryStream(client, balance, pending, chart);
        //    byte[] buf = new byte[memExcel.Length];
        //    memExcel.Position = 0;
        //    memExcel.Read(buf, 0, buf.Length);

        //    return new BinaryContentResult(buf, "application/vnd.xls", download, fileName);
        //}

        //protected ActionResult ViewFundsBalancePdf(
        //    FundClient client, 
        //    FundBalance balance, 
        //    List<FundBalancePendingApplicationItem> pending,
        //    List<FundBalanceChartItemResponse> chart, 
        //    byte[] chartImage, bool download, string fileName)
        //{
        //    PDFHelper helper = new PDFHelper();

        //    MemoryStream memPDF = helper.CreateFundsBalanceToMemoryStream(client, balance, pending, chart, chartImage);
        //    byte[] buf = new byte[memPDF.Length];
        //    memPDF.Position = 0;
        //    memPDF.Read(buf, 0, buf.Length);

        //    return new BinaryContentResult(buf, "application/pdf", download, fileName);
        //}

        //protected ActionResult ViewFundsAnalyticalStatementExcel(FundClient client, List<FundStatementItem> statement, bool download, string fileName)
        //{
        //    ExcelHelper helper = new ExcelHelper();

        //    MemoryStream memExcel = helper.CreateFundsAnalyticalStatementToMemoryStream(client, statement);
        //    byte[] buf = new byte[memExcel.Length];
        //    memExcel.Position = 0;
        //    memExcel.Read(buf, 0, buf.Length);

        //    return new BinaryContentResult(buf, "application/vnd.xls", download, fileName);
        //}

        //protected ActionResult ViewFundsAnalyticalStatementPdf(FundClient client, List<FundStatementItem> statement, bool download, string fileName)
        //{
        //    PDFHelper helper = new PDFHelper();

        //    MemoryStream memPDF = helper.CreateFundsAnalyticalStatementToMemoryStream(client, statement);
        //    byte[] buf = new byte[memPDF.Length];
        //    memPDF.Position = 0;
        //    memPDF.Read(buf, 0, buf.Length);

        //    return new BinaryContentResult(buf, "application/pdf", download, fileName);
        //}

        //protected ActionResult ViewFundsSyntheticStatementExcel(FundClient client, List<FundStatementItem> statement, bool download, string fileName)
        //{
        //    ExcelHelper helper = new ExcelHelper();

        //    MemoryStream memExcel = helper.CreateFundsSyntheticStatementToMemoryStream(client, statement);
        //    byte[] buf = new byte[memExcel.Length];
        //    memExcel.Position = 0;
        //    memExcel.Read(buf, 0, buf.Length);

        //    return new BinaryContentResult(buf, "application/vnd.xls", download, fileName);
        //}

        //protected ActionResult ViewFundsSyntheticStatementPdf(FundClient client, List<FundStatementItem> statement, bool download, string fileName)
        //{
        //    PDFHelper helper = new PDFHelper();

        //    MemoryStream memPDF = helper.CreateFundsSyntheticStatementToMemoryStream(client, statement);
        //    byte[] buf = new byte[memPDF.Length];
        //    memPDF.Position = 0;
        //    memPDF.Read(buf, 0, buf.Length);

        //    return new BinaryContentResult(buf, "application/pdf", download, fileName);
        //}

        //protected ActionResult ViewFundDetailPdf(FundDetails fundDetails, bool download, string fileName)
        //{
        //    PDFHelper helper = new PDFHelper();

        //    MemoryStream memPDF = helper.CreateFundDetailToMemoryStream(fundDetails);
        //    byte[] buf = new byte[memPDF.Length];
        //    memPDF.Position = 0;
        //    memPDF.Read(buf, 0, buf.Length);

        //    return new BinaryContentResult(buf, "application/pdf", download, fileName);
        //}

		#endregion

        #region Relatório de Corretagem

        protected ActionResult ViewBrokerageReportExcel(BrokerageTransferInfo model, List<int> clientList, List<CPClientInformation> clientInfo, bool download, string fileName)
        {
            ExcelHelper helper = new ExcelHelper();

            MemoryStream memExcel = helper.CreateBrokerageReportsToMemoryStream(model, clientList, clientInfo);
            byte[] buf = new byte[memExcel.Length];
            memExcel.Position = 0;
            memExcel.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/vnd.xls", download, fileName);
        }

        protected ActionResult ViewBrokerageReportPdf(BrokerageTransferInfo model, List<int> clientList, List<CPClientInformation> clientInfo, bool download, string fileName)
        {
            PDFHelper helper = new PDFHelper();

            MemoryStream memPDF = helper.CreateBrokerageReportsToMemoryStream(model, clientList, clientInfo);
            byte[] buf = new byte[memPDF.Length];
            memPDF.Position = 0;
            memPDF.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/pdf", download, fileName);
        }

        protected ActionResult ViewBrokerageRangeExcel(BrokerageRange model, bool download, string fileName)
        {
            ExcelHelper helper = new ExcelHelper();

            MemoryStream memExcel = helper.CreateBrokerageRangeToMemoryStream(model);
            byte[] buf = new byte[memExcel.Length];
            memExcel.Position = 0;
            memExcel.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/vnd.xls", download, fileName);
        }

        protected ActionResult ViewBrokerageRangePdf(BrokerageRange model, bool download, string fileName)
        {
            PDFHelper helper = new PDFHelper();

            MemoryStream memPDF = helper.CreateBrokerageRangeToMemoryStream(model);
            byte[] buf = new byte[memPDF.Length];
            memPDF.Position = 0;
            memPDF.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/pdf", download, fileName);
        }

        #endregion

        #region ClientRegistration
        
		protected ActionResult ViewStatusViewerExcel(List<Client> list, bool download, string fileName)
        {
            ExcelHelper helper = new ExcelHelper();

            MemoryStream memExcel = helper.CreateStatusViewerToMemoryStream(list);
            byte[] buf = new byte[memExcel.Length];
            memExcel.Position = 0;
            memExcel.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/vnd.xls", download, fileName);
        }

        protected ActionResult ViewStatusViewerPdf(List<Client> list, bool download, string fileName)
        {
            PDFHelper helper = new PDFHelper();

            MemoryStream memPDF = helper.CreateStatusViewerToMemoryStream(list);
            byte[] buf = new byte[memPDF.Length];
            memPDF.Position = 0;
            memPDF.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/pdf", download, fileName);
        }

		protected ActionResult ViewOverdueAndDueExcel(List<OverdueAndDue> list, bool download, string fileName)
		{
			ExcelHelper helper = new ExcelHelper();

			MemoryStream memExcel = helper.CreateOverdueAndDueToMemoryStream(list);
			byte[] buf = new byte[memExcel.Length];
			memExcel.Position = 0;
			memExcel.Read(buf, 0, buf.Length);

			return new BinaryContentResult(buf, "application/vnd.xls", download, fileName);
		}

		protected ActionResult ViewOverdueAndDuePdf(List<OverdueAndDue> list, bool download, string fileName)
		{
			PDFHelper helper = new PDFHelper();

			MemoryStream memPDF = helper.CreateOverdueAndDueToMemoryStream(list);
			byte[] buf = new byte[memPDF.Length];
			memPDF.Position = 0;
			memPDF.Read(buf, 0, buf.Length);

			return new BinaryContentResult(buf, "application/pdf", download, fileName);
		}

        protected ActionResult ViewTransferBovespaExcel(List<TransferBovespaItem> list, bool download, string fileName)
        {
            ExcelHelper helper = new ExcelHelper();
            MemoryStream memExcel = helper.CreateTransferBovespaToMemoryStream(list);
            byte[] buf = new byte[memExcel.Length];
            memExcel.Position = 0;
            memExcel.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/vnd.xls", download, fileName);

            throw new NotImplementedException();
        }

        protected ActionResult ViewTransferBovespaPdf(List<TransferBovespaItem> list, bool download, string fileName)
        {
            PDFHelper helper = new PDFHelper();

            MemoryStream memPDF = helper.CreateTransferBovespaToMemoryStream(list);
            byte[] buf = new byte[memPDF.Length];
            memPDF.Position = 0;
            memPDF.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/pdf", download, fileName);
        }

		protected ActionResult ViewTransferBMFExcel(List<TransferBMFItem> list, bool download, string fileName)
		{
			ExcelHelper helper = new ExcelHelper();

			MemoryStream memExcel = helper.CreateTransferBMFToMemoryStream(list);
			byte[] buf = new byte[memExcel.Length];
			memExcel.Position = 0;
			memExcel.Read(buf, 0, buf.Length);

			return new BinaryContentResult(buf, "application/vnd.xls", download, fileName);
		}

		protected ActionResult ViewTransferBMFPdf(List<TransferBMFItem> list, bool download, string fileName)
		{
			PDFHelper helper = new PDFHelper();

			MemoryStream memPDF = helper.CreateTransferBMFToMemoryStream(list);
			byte[] buf = new byte[memPDF.Length];
			memPDF.Position = 0;
			memPDF.Read(buf, 0, buf.Length);

			return new BinaryContentResult(buf, "application/pdf", download, fileName);
		}

        #endregion

		#region Guarantee

		protected ActionResult ViewGuaranteePdf(GuaranteeClient guarantee, bool download, string fileName)
		{
			PDFHelper helper = new PDFHelper();

			MemoryStream memPDF = helper.CreateGuaranteeToMemoryStream(guarantee);
			byte[] buf = new byte[memPDF.Length];
			memPDF.Position = 0;
			memPDF.Read(buf, 0, buf.Length);

			return new BinaryContentResult(buf, "application/pdf", download, fileName);
		}

		protected ActionResult ViewGuaranteeExcel(GuaranteeClient guarantee, bool download, string fileName)
		{
			ExcelHelper helper = new ExcelHelper();

			MemoryStream memExcel = helper.CreateGuaranteeToMemoryStream(guarantee);
			byte[] buf = new byte[memExcel.Length];
			memExcel.Position = 0;
			memExcel.Read(buf, 0, buf.Length);

			return new BinaryContentResult(buf, "application/vnd.xls", download, fileName);
		}

		#endregion

        #region Position

        protected ActionResult ConsolidatedPositionExcel(ConsolidatedPositionResponse2 item, List<ConsolidatedPositionDetails> itemDetails, bool download, string fileName) {
            ExcelHelper helper = new ExcelHelper();

            MemoryStream memExcel = helper.CreateConsolidatedPositionReportToMemoryStream(item, itemDetails);
            byte[] buf = new byte[memExcel.Length];
            memExcel.Position = 0;
            memExcel.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/vnd.xls", download, fileName);
        }

        protected ActionResult ConsolidatedPositionPdf(ConsolidatedPositionResponse2 item, List<ConsolidatedPositionDetails> itemDetails, bool download, string fileName) {
            ExcelHelper helper = new ExcelHelper();

            MemoryStream memExcel = helper.CreateConsolidatedPositionReportToMemoryStream(item, itemDetails);
            byte[] buf = new byte[memExcel.Length];
            memExcel.Position = 0;
            memExcel.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/vnd.xls", download, fileName);
        }

        protected ActionResult CustodyPositionExcel(CustodyPositionResponse item, bool download, string fileName, string nomeCustodia)
        {
            ExcelHelper helper = new ExcelHelper();

            MemoryStream memExcel = helper.CreateCustodyPositionReportToMemoryStream(item, nomeCustodia);
            byte[] buf = new byte[memExcel.Length];
            memExcel.Position = 0;
            memExcel.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/vnd.xls", download, fileName);
        }

        protected ActionResult FinancialPositionExcel(FinancialPositionResponse item, bool download, string fileName)
        {
            ExcelHelper helper = new ExcelHelper();

            MemoryStream memExcel = helper.CreateFinancialPositionReportToMemoryStream(item);
            byte[] buf = new byte[memExcel.Length];
            memExcel.Position = 0;
            memExcel.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/vnd.xls", download, fileName);
        }

        #endregion 

        #region Demonstrative

        protected ActionResult ExtractExcel(ExtractRendaResponse item, bool download, string fileName)
        {
            ExcelHelper helper = new ExcelHelper();

            MemoryStream memExcel = helper.CreateExtractReportToMemoryStream(item);
            byte[] buf = new byte[memExcel.Length];
            memExcel.Position = 0;
            memExcel.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/vnd.xls", download, fileName);
        }

        protected ActionResult ExtractPDF(ExtractRendaResponse item, bool download, string fileName)
        {
            PDFHelper helper = new PDFHelper();

            MemoryStream memPDF = helper.ExtractPDFReportToMemoryStream(item);
            byte[] buf = new byte[memPDF.Length];
            memPDF.Position = 0;
            memPDF.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/pdf", download, fileName);
        }

        protected ActionResult ProceedsExcel(ProceedsDemonstrativeResponse item, bool download, string fileName)
        {
            ExcelHelper helper = new ExcelHelper();

            MemoryStream memExcel = helper.CreateProceedsReportToMemoryStream(item);
            byte[] buf = new byte[memExcel.Length];
            memExcel.Position = 0;
            memExcel.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/vnd.xls", download, fileName);
        }

        protected ActionResult TradingSummaryExcel(TradingSummaryResponse item, bool download, string fileName)
        {
            ExcelHelper helper = new ExcelHelper();

            MemoryStream memExcel = helper.CreateTradingSummaryReportToMemoryStream(item);
            byte[] buf = new byte[memExcel.Length];
            memExcel.Position = 0;
            memExcel.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/vnd.xls", download, fileName);
        }

        protected ActionResult ExtractNDFExcel(ExtractNDFResponse item, bool download, string fileName)
        {
            ExcelHelper helper = new ExcelHelper();

            MemoryStream memExcel = helper.ExtractNDFExcelReportToMemoryStream(item);
            byte[] buf = new byte[memExcel.Length];
            memExcel.Position = 0;
            memExcel.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/vnd.xls", download, fileName);
        }

        protected ActionResult ExtractSWPExcel(ExtractSWPResponse item, bool download, string fileName)
        {
            ExcelHelper helper = new ExcelHelper();

            MemoryStream memExcel = helper.ExtractSWPExcelReportToMemoryStream(item);
            byte[] buf = new byte[memExcel.Length];
            memExcel.Position = 0;
            memExcel.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/vnd.xls", download, fileName);

        }

        protected ActionResult ExtractSWPPDF(ExtractSWPResponse item, bool download, string fileName)
        {
            PDFHelper helper = new PDFHelper();

            MemoryStream memPDF = helper.ExtractSWPPDFReportToMemoryStream(item);
            byte[] buf = new byte[memPDF.Length];
            memPDF.Position = 0;
            memPDF.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/pdf", download, fileName);
        }

        protected ActionResult ExtractNDFPDF(ExtractNDFResponse item, bool download, string fileName)
        {
            PDFHelper helper = new PDFHelper();

            MemoryStream memPDF = helper.ExtractNDFPDFReportToMemoryStream(item);
            byte[] buf = new byte[memPDF.Length];
            memPDF.Position = 0;
            memPDF.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/pdf", download, fileName);
        }

        protected ActionResult BrokerageNotePdf(BrokerageNoteResponse item, bool download, string fileName)
        {
            PDFHelper helper = new PDFHelper();

            MemoryStream memPDF = helper.CreateBrokerageNoteToMemoryStream(item);
            byte[] buf = new byte[memPDF.Length];
            memPDF.Position = 0;
            memPDF.Read(buf, 0, buf.Length);

            return new BinaryContentResult(buf, "application/pdf", download, fileName);
        }

        #endregion
    }

    public class BinaryContentResult : ActionResult
    {
        private string ContentType;
        private byte[] ContentBytes;
        private bool Download;
        private string FileName;

        public BinaryContentResult(byte[] contentBytes, string contentType, bool download, string fileName)
        {
            this.ContentBytes = contentBytes;
            this.ContentType = contentType;
            this.Download = download;
            this.FileName = fileName;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            var response = context.HttpContext.Response;
            response.Clear();
            //response.Cache.SetCacheability(HttpCacheability.NoCache);
            response.ContentType = this.ContentType;
            if (this.Download)
                response.AddHeader("Content-Disposition", "attachment; filename=\"" + this.FileName + "\";");
            var stream = new MemoryStream(this.ContentBytes);
            stream.WriteTo(response.OutputStream);
            stream.Dispose();
        }
    }
}
