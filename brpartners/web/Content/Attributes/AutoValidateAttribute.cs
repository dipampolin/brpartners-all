﻿using QX3.Portal.WebSite.HumanResources.Exceptions;
using System;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;

namespace QX3.Portal.WebSite.Attributes
{
    public class AutoValidateAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {

            var viewData = filterContext.Controller.ViewData;

            if (!viewData.ModelState.IsValid)
            {
                filterContext.HttpContext.Response.StatusCode = 412;
                filterContext.Result = new JsonResult()
                {
                    Data = new
                    {
                        Message = "Falha na validação",
                        Errors = viewData.ModelState.Where(x => x.Value.Errors.Count() > 0).Select(x => new
                        {
                            Field = x.Key,
                            Messages = x.Value.Errors.Select(y => y.Exception.Message).ToList()
                        })
                    },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
                return;
            }

            base.OnActionExecuting(filterContext);
        }
    }
}