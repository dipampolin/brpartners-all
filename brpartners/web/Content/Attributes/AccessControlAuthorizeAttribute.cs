﻿using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.WebSite.AccessControlService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QX3.Portal.WebSite.Attributes
{
    public class AccessControlAuthorizeAttribute : AuthorizeAttribute
    {

        public IAccessControlContract AccessControlService { get; set; }

        private string path;

        public AccessControlAuthorizeAttribute(string path = null)
        {
            if (!string.IsNullOrWhiteSpace(path))
            {
                this.path = path.Replace(".", "/");
            }

        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (!httpContext.User.Identity.IsAuthenticated)
            {
                return false;
            }

            var user = (UserInformation)httpContext.Session["CurrentUser"];

            if (user == null)
            {
                return false;
            }

            var ip = Convert.ToString(httpContext.Session["IPCLIENT"]);

            var uri = this.path == null ? httpContext.Request.Url.AbsolutePath : this.path;

            string[] path = uri.TrimStart('/').Split('/');
            int pathCount = path.Count();

            if (path[0] == "RH" && pathCount == 1)
            {
                var plist = new System.Collections.Generic.List<string>(path);
                plist.Add("Index");
                path = plist.ToArray();
                pathCount = path.Count();
            }

            if (pathCount < 2)
            {
                return false;
            }

            var requestedElement = string.Join(".", path);

            if (path[0] == "RH" && pathCount < 3)
            {
                requestedElement += ".Index";
            }

            //var sessionData = AccessControlService.Verify_SESSION(user.UserName, ip);

            //var verifySession = sessionData.Result;

            //if (!verifySession)
            //{
            //    httpContext.Session["CONTROLSESSION"] = "S";
            //    return false;
            //}

            var allowedElements = user.Profile.Permissions;

            if (allowedElements == null)
            {
                return false;
            }

            return allowedElements.Exists(x => x.Name.ToUpper().Equals(requestedElement.ToUpper()));

        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {

            filterContext.HttpContext.Session.Abandon();

            if (filterContext.RequestContext.HttpContext.Request.AcceptTypes[0].ToLower().Equals("application/json"))
            {
                filterContext.HttpContext.Response.StatusCode = 403;
                filterContext.Result = new JsonResult()
                {
                    Data = new
                    {
                        Error = "Sua sessão expirou, faça login novamente"
                    },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                };
                return;
            }

            filterContext.Result = new RedirectResult("~/Account/Login");
        }
    }
}