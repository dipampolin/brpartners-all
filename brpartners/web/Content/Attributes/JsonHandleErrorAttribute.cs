﻿using QX3.Portal.WebSite.HumanResources.Exceptions;
using System.Linq;
using System.Web.Mvc;

namespace QX3.Portal.WebSite.Attributes
{
    public class JsonHandleErrorAttribute : HandleErrorAttribute
    {

        public override void OnException(ExceptionContext filterContext)
        {

            if (filterContext.Exception.GetType() != typeof(ValidationException))
            {
                return;
            }

            var exception = (ValidationException)filterContext.Exception;

            filterContext.ExceptionHandled = true;
            filterContext.HttpContext.Response.StatusCode = 412;
            filterContext.Result = new JsonResult()
            {
                Data = new
                {
                    Message = "Falha na validação",
                    Errors = exception.ModelState.Select(x => new
                    {
                        Field = x.Key,
                        Messages = x.Value.Errors.Select(y => y.ErrorMessage).ToList()
                    })
                },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

        }
    }
}