﻿(function () {
    var originalAjaxMethod = jQuery.ajax;

    jQuery.ajax = function (options) {
        if (options.type.toUpperCase() == "POST") {
            if (options.data != undefined) {
                options.data.__RequestVerificationToken = $('#__AjaxAntiForgeryForm input[name=__RequestVerificationToken]').val();
            }
            else {
                options.data = { __RequestVerificationToken: $('#__AjaxAntiForgeryForm input[name=__RequestVerificationToken]').val() };
            }
        }

        return originalAjaxMethod(options);
    }
})();