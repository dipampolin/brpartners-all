﻿//Scripts Rodrigo Ribeiro
//

// Adiciona validação nos campos
$(document).ready(function () {
    $(".btn-input-submit").click(function () {
        $("#formCP, ").validate({
            rules: {
                cdCliente: {
                    required: true,
                },
                CpfCnpj: {
                    required: true,
                },
                dtInicial: {
                    required: true,
                },
                dtFinal: {
                    required: true
                },
                ativo: {
                    required: true
                },
                mesAno: {
                    required: true
                }
            },
            messages: {
                cdCliente: {
                    required: "Você deve preencher o campo 'Cliente'",
                },
                CpfCnpj: {
                    required: "Você deve preencher o campo 'Cpf/Cnpj'",
                },
                dtInicial: {
                    required: "Você deve preencher o campo 'Data inicial'",
                },
                dtFinal: {
                    required: "Você deve preencher o campo 'Data Final'"
                },
                ativo: {
                    required: "Você deve preencher o campo 'Ativo'"
                },
                mesAno: {
                    required: "Você deve preencher o campo 'Mês'"
                },
            }
        });
    });

    // Adiciona a classe 'ultimo' à última tr no corpo da tabela
    $('.table-extract table tbody tr:first-child').addClass('inicial');

    // Adiciona a zebrado nas tabelas
    $('tr:odd').css('background', '#EEE');

    //Valida a exportação para Excel
    $('#lnkHistoryExcel').click(function ()
    {
        //var rowCount = $('#TradingTable >tbody >tr').length;

        var TableEmpty = $('.ultimo td').size();

        if (TableEmpty == 1)
        {
            alert('Não existe dados para exportação.');
            return false;
        }

    });

    //Valida a exportação para PDF
    $('#lnkHistoryPDF').click(function () {
       
        var TableEmpty = $('.ultimo td').size();

        if (TableEmpty == 1) {
            alert('Não existe dados para exportação.');
            return false;
        }

    });

    //Add mascaras
    $("#dtInicial").mask("99/99/9999");
    $("#dtFinal").mask("99/99/9999");
    $("#mesAno").mask("99/9999");

    //Controle de exibição de sub-tabela posição consolidada
    $('.cPosition').each(function(){
        $(this).bind('click',function(e){
            var _trHidden = $(this).parent().parent().next();

            if (_trHidden.is(':visible'))
                _trHidden.hide();
            else
                 _trHidden.show();

            e.preventDefault();
        });
    });

   

});
