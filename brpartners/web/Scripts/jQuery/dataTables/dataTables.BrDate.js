﻿jQuery.extend(jQuery.fn.dataTableExt.oSort, {
    "datetime-pre": function (a) {
        if ($.trim(a) != '') {
            var frDatea = $.trim(a).split(' ');
            var frTimea = frDatea[1].split(':');
            var frDatea2 = frDatea[0].split('/');
            var x = (frDatea2[2] + frDatea2[1] + frDatea2[0] + frTimea[0] + frTimea[1] + frTimea[2]) * 1;
        } else {
            var x = 10000000000000; // = l'an 1000 ...
        }

        return x;
    },

    "datetime-asc": function (a, b) {
        return a - b;
    },

    "datetime-desc": function (a, b) {
        return b - a;
    }
});
