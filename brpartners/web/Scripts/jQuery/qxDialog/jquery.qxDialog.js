﻿/*
*   QXDialog jQuery Plugin
*   Edgar Mesquita, Diego Rio and Leonardo Majowka
*   
*   BASED ON
*   Leobox Plugin 1.1 - Easy and simple div box absoluted centered on the screen
*   author: Leonardo Majowka
*   date: 2010-07-18
*
*   AND
*   Center Plugin 1.0 - Easy cross-browser centering a div!
*   Version 1.0.1
*   @requires jQuery v1.3.0
* 
*   Copyright (c) 2010 Matthias Isler
*   Licensed under the GPL licenses:
*   http://www.gnu.org/licenses/gpl.html
* 
*
* EXAMPLE:
 
<SCRIPT LANGUAGE="JavaScript" SRC="jquery.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="jquery.qxDialog.js"></SCRIPT>

<input type="button" value="open" onclick='$("#divtest").openBox();'>

<div id="divtest" class="dialog"><input type="button" value="close" style="" onClick='$("#divtest").closeBox();'></div>

<style>
.dialog {
width:300Px;
position:absolute;
background:#FFF;
border:2Px solid red;
z-index:2;
display:none;
top:0;
left:0;
height:100Px;
padding:20Px;
}
</style>
*/

var modalPid = null;
var modalHeight = 0;

jQuery.fn.openBox = function (init) { return this; };
jQuery.fn.closeBox = function (init) { return this; };
jQuery.fn.center = function (init) { return this; };

////////////////////////////////////////////////////////////////////

var Modal = {
    Show: function (content, title, width) {
        $().qxModal({
            type: 'MessageBox',
            title: title,
            content: content
        });
    },

    Close: function () {
        $().qxModal('close');
    }

};

var MessageBox = {
    Show: function (message, title, buttons, callback, width) {

    },

    Close: function () {
        $().qxModal('close');
    }
};

var DialogResult = {
    Yes: "Yes",
    No: "No",
    OK: "OK",
    Cancel: "Cancel"
};

var MessageBoxButtons = {
    OK: "OK",
    OKCancel: "OKCancel",
    YesNo: "YesNo",
    YesNoCancel: "YesNoCancel"
};