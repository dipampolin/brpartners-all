/*
* Fn: styleInputs
* Description:  Estilizar campos do tipo "select", "radio" e "checkbox"
*
* Options: 
* *** selectclass:      Classe para os inputs do tipo "select"  (default: selectStyled)
* *** checkclass:       Classe para os inputs do tipo "checkbox" (default: checkStyled)
* *** radioclass:       Classe para os inputs do tipo "radio" (default: radioStyled)
* *** animationspeed:   Velocidade das animações (default: fast)
*
* ToDo:
* *** Adicionar compatibilidade com teclas (top-arrow, bottom-arrow e letra inicial da opção)
* *** Fazer funcionar mesmo em elementos escondidos (caso não consiga pegar a largura do "select", tentar buscar o CSS)
* *** Melhorar o modo com se trata inputs desabilitados
* *** Clean para as instâncias de radio-box e checkbox
*
*/
(function ($) {
    $.fn.styleInputs = function (options, callback) {
        //Variáveis
        var _SELF = $(this);

        //Opções
        options = $.extend({
            width: "auto",
            selectboxClass: "selectStyled",
            checkboxClass: "checkStyled",
            radioboxClass: "radioStyled",
            focusClass: "focus",
            optionsClass: "options",
            animationSpeed: 100,
            maxHeight: "100px",
            zIndex: "60"
        }, options);

        //Limpa qualquer instância anterior
        _SELF.clean = function (_this) {
            var tagName = $(_this).get(0).tagName.toLowerCase();

            switch (tagName) {
                case "select":
                    if ($(_this).next().is('.' + options.selectboxClass))
                        $(_this).show().next().remove();
                    break;
                case "radio":
                    break;
                case "checkbox":
                    break
                default:
                    break;
            };
        };

        //EACH
        $(this).each(function (index) {
            //Variáveis
            var _this = $(this);
            var html = "";
            _this.next = _this.param = _this.selectbox = _this.radiobox = _this.checkbox = null;

            //Parâmetros dinâmicos
            _this.param = {
                tagName: $(_this).get(0).tagName.toLowerCase(),
                type: $(_this).attr('type'),
                index: index,
                width: null
            };

            //Limpa qualquer instância já programada
            _SELF.clean(_this);

            //-> Selectbox
            _this.selectbox = function () {

                //Calcula tamanho do select
                (options.width != "auto") ? _this.param.width = parseInt(options.width) : _this.param.width = parseInt(_this.outerWidth());

                //Constrói HTML
                html += '<ul class="' + options.selectboxClass + '" style=" position:relative; width:' + _this.param.width + 'px; z-index:' + (options.zIndex--) + ';">';
                html += '<li class="' + options.focusClass + '">' + _this.children('option:selected').html() + '</li>';
                html += '<li class="' + options.optionsClass + '" style="position:absolute;">';
                html += '<ul style="width:' + _this.param.width + 'px;">';
                $(_this).find('option').each(function (index) {
                    $(this).attr('data-index', index);
                    if (!$(this).hasClass('not')) {
                        html += '<li data-index="' + index + '">' + $(this).html() + '</li>';
                    }
                });
                html += '</ul>';
                html += '</li>';
                html += '</ul>';

                //Escreve HTML
                _this.after(html);
                _this.next = $(_this).next();
                _this.optionsElem = _this.next.children('.' + options.optionsClass);
                _this.focusElem = _this.next.children('.' + options.focusClass);

                //Ajusta posição
                _this.optionsElem.css({ 'margin-left': parseInt(_this.next.css('border-left-width')) * -1 });

                //Verifica se está desabilitado
                if ($(_this).is('[disabled]'))
                    _this.next.addClass('disabled');

                //Esconde opções
                _this.next.children('.' + options.optionsClass).hide();

                //Ação ao sair
                _this.onexit = function (others, event) {

                    if (_SELF.is('select'))
                        if (others == true)
                            _SELF.not(_this).next().find('.' + options.optionsClass).stop().slideUp(options.animationSpeed);
                        else
                            _SELF.not(_this).next().find('.' + options.optionsClass).stop().slideUp(options.animationSpeed);
                    else
                        _SELF.next().children('.' + options.optionsClass).slideUp(options.animationSpeed);
                    _this.toTrigger = false;
                };

                //Ação ao clicar
                _this.onclick = function () {
                    _this.stop(true, true).optionsElem.slideToggle(options.animationSpeed);
                    _this.onexit(true);
                };

                //Ação ao clicar nas opções
                _this.optionsElem.find('ul li').each(function (index) {
                    $(this).bind('click', function (event) {
                        var thisIndex = $(this).attr('data-index');
                        _this.focusElem.html($(this).text());
                        _this.children('option').each(function () {
                            if ($(this).attr('data-index') == thisIndex) {
                                $(this).attr({ selected: 'selected' });
                                return false;
                            }
                        });
                        _this.toTrigger = true;
                        _this.trigger('change');
                    });
                });

                //Ação ao mudar opção no select original
                _this.bind('change', function () {
                    if (!_this.toTrigger) {
                        var thisIndex = _this.children('option:selected').attr('data-index');
                        _this.optionsElem.find('ul li').each(function (index) {
                            $(this).removeClass('selected');
                            if ($(this).attr('data-index') == thisIndex) {
                                $(this).addClass('selected').trigger('click');
                            }
                        });
                    }
                    this.toTrigger == false;
                });

                //Ação ao clicar na página
                $('html').bind('click', function () {
                    _this.onexit(false);
                });

                //Ação ao clicar no select estilizado
                _this.next.bind('click', function (event) {
                    _this.onclick();
                    event.stopPropagation();
                });

                //Outros
                if ($.browser.msie && ($.browser.version == "7.0" && $.browser.version == "6.0"))
                    _this.next.css({ "display": "inline", "zoom": "1" });
                else
                    _this.next.css({ "display": "inline-block" });
                _this.hide();
            };

            //-> Radiobox
            _this.radiobox = function () {
                //Esconde o radio
                if ($.browser.msie && ($.browser.version == "8.0" || $.browser.version == "7.0" || $.browser.version == "6.0"))
                    _this.css({ 'position': 'absolute', 'left': '-99999999px' }); //Hack IE
                else
                    _this.hide();

                //Adiciona HTML
                _this.after('<span class="' + options.radioboxClass + '"></span>');

                //Verifica se está checado
                if (_this.is(':checked'))
                    $(_this).next().addClass('selected');

                //Ação ao alterar
                $(_this).change(function () {
                    $('input[type="radio"][name="' + _this.attr('name') + '"]').next().removeClass('selected');
                    $(_this).next().addClass('selected');
                });

                //Ação ao clicar
                $(_this).next().bind('click', function () {
                    _this.attr('checked', true).trigger('change');
                });
            };

            //-> Checkbox
            _this.checkbox = function () {
                //Esconde o checkbox
                if ($.browser.msie && ($.browser.version == "8.0" || $.browser.version == "7.0" || $.browser.version == "6.0"))
                    _this.css({ 'position': 'absolute', 'left': '-99999999px' }); //Hack IE
                else
                    _this.hide();

                //Adiciona HTML
                _this.after('<span class="' + options.checkboxClass + '"></span>');

                //Verifica se está checado
                if (_this.is(':checked'))
                    $(_this).next().addClass('selected');

                //Ação ao alterar
                $(_this).bind('change', function () {
                    if ($(_this).is(':checked'))
                        $(_this).next().addClass('selected');
                    else
                        $(_this).next().removeClass('selected');
                });

                //Ação ao clicar
                $(_this).next().bind('click', function () {
                    if ($(_this).is(':checked'))
                        $(_this).attr('checked', false).trigger('change');
                    else
                        $(_this).attr('checked', true).trigger('change');
                });
            };

            //INIT
            _this.init = function () {
                switch (_this.param.tagName) {
                    case "select":
                        _this.selectbox();
                        break;
                    case "input":
                        if (_this.param.type == "radio")
                            _this.radiobox();
                        else if (_this.param.type == "checkbox")
                            _this.checkbox();
                        break;
                };
            };

            //Inicialização
            _this.init();
        });

        //Callback
        if (typeof eval(callback) == 'function') {
            jQuery.fn.callback = callback;
            _self.callback();
        }

        //Retorno
        return this;
    };
})(jQuery);