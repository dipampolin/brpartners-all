//Menu Principal
$(document).ready(function () {
    $('#etiqueta-menu').click(function () {
        if ($('#menu-principal').css("left") == "0px") {
            $('#menu-principal').addClass('fechada').removeClass('aberta').animate({
                "left": "-397px"
            });
        }
        else {
            $('#menu-principal').addClass('aberta').removeClass('fechada').animate({
                "left": 0
            });
        }
        $('#menu-principal').click(function (event) {
            event.stopPropagation();
        });
        $('body').click(function () {
            $('#menu-principal.aberta').find('#etiqueta-menu').trigger('click');
        });
    });
    $('#menu-principal').find('span').click(function () {
        $(this).toggleClass('item-aberto');
    });
    $('.caixa').find('ul:last').css({'border' : 'none', 'padding-bottom' : '0'});
});



//Plugin DisableSelection
jQuery.fn.extend({
    disableSelection: function () {
        return this.each(function () {
            this.onselectstart = function () { return false; };
            this.unselectable = "on";
            jQuery(this).css('user-select', 'none');
            jQuery(this).css('-o-user-select', 'none');
            jQuery(this).css('-moz-user-select', 'none');
            jQuery(this).css('-khtml-user-select', 'none');
            jQuery(this).css('-webkit-user-select', 'none');
        });
    }
});



//Chamada do plugin selectBox
$(document).ready(function () {
    if ($.fn.styleInputs) {
        $('select').styleInputs();
    }
});

//Chamada do plugin disableSelection
$(document).ready(function () {
    $('#menu-principal .caixa').disableSelection();
});

$(document).ready(function () {
    // Adiciona a classe 'ultimo' � �ltima tr no corpo da tabela
    $('table tbody tr:last-child').addClass('ultimo');

    // Adiciona a classe 'ultimo' �s �ltimas th e td no corpo da tabela
    $('table tr td:last-child').addClass('ultimo');
    $('table tr th:last-child').addClass('ultimo');
});



//Ajuste para o problema do suggest no I7 (ao rolar a tela com o suggest aberto dentro do modal a listagem dele n�o segue o campo texto)
$(document).ready(function () {
    $('body').bind('mousewheel', function () {
        setTimeout(function () {
            $('.ui-autocomplete.ui-menu').hide();
        }, 100);
    });
});
