﻿
var commomDataHelper = (
    function () {
        _getClientName = function (code, targetElementId, context, txtName) {
            if (code != "") {
                clientCode = parseInt(code.replace(/\./g, ''));
                $.ajax({
                    type: "POST",
                    url: "/NonResidentTraders/GetTraderName/",
                    dataType: "json",
                    data: { code: clientCode },
                    success: function (data) {
                        $("#" + targetElementId, context).html(String(data));
                        if (txtName != null && txtName != "" && data == "Cliente não encontrado.") {
                            $("#" + txtName, context).val("");
                        }
                    }
                });
            }
            else {
                $("#" + targetElementId, context).html("");
            }
        },
        _getAssessorName = function (sourceElementId, targetElementId, context) {
            var source = $("#" + sourceElementId, context);
            if (source.val() != "") {
                var assessorCode = parseInt(source.val());
                $.ajax({
                    type: "POST",
                    url: "/Market/GetAssessorName/",
                    dataType: "json",
                    data: { code: assessorCode },
                    success: function (data) {
                        $("#" + targetElementId, context).html(String(data));
                        if (data == "Assessor não encontrado.")
                            source.val("");
                    }
                });
            }
            else {
                $("#" + targetElementId, context).html("");
            }
        },
		_validateUsernameSuggest = function (value, targetName, context) {
		    if (value != "") {
		        $.ajax({
		            type: "POST",
		            url: "/AccessControl/ValidateUsernameSuggest/",
		            dataType: "json",
		            data: { userName: value },
		            success: function (data) {
		                if (data != "true") {
		                    $(targetName, context).val("-1");
		                }
		            }
		        });
		    }
		},
		_validateFuncionalityAliasSuggest = function (value, targetName, context) {
		    if (value != "") {
		        $.ajax({
		            type: "POST",
		            url: "/AccessControl/ValidateFuncionalityAliasSuggest/",
		            dataType: "json",
		            data: { alias: value },
		            success: function (data) {
		                if (data != "true") {
		                    $(targetName, context).val("-1");
		                }
		            }
		        });
		    }
		},
		_validateGroupOfAssessorsNameSuggest = function (value, targetName, context) {
		    if (value != "") {
		        $.ajax({
		            type: "POST",
		            url: "/AccessControl/ValidateGroupOfAssessorsNameSuggest/",
		            dataType: "json",
		            data: { name: value },
		            success: function (data) {
		                if (data != "true") {
		                    $(targetName, context).val("-1");
		                }
		            }
		        });
		    }
		},
		_validateProfileNameSuggest = function (value, targetName, context) {
		    if (value != "") {
		        $.ajax({
		            type: "POST",
		            url: "/AccessControl/ValidateProfileNameSuggest/",
		            dataType: "json",
		            data: { name: value },
		            success: function (data) {
		                if (data != "true") {
		                    $(targetName, context).val("-1");
		                }
		            }
		        });
		    }
		}

        return { GetClientName: _getClientName, GetAssessorName: _getAssessorName, ValidateUsernameSuggest: _validateUsernameSuggest, ValidateFuncionalityAliasSuggest: _validateFuncionalityAliasSuggest, ValidateGroupOfAssessorsNameSuggest: _validateGroupOfAssessorsNameSuggest, ValidateProfileNameSuggest: _validateProfileNameSuggest };
    } ()
);

