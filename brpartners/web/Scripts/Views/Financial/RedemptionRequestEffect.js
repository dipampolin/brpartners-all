﻿

function Next(count) {
    if (count == "-1")
        Submit();
    else
        ValidateSubmit(count);
}

function Cancel(count, id) {
    if (count == "-1")
        Modal.Close();
    else {
        $("#frmRedemptionRequestEffect #hdfIds").val($("#hdfIds").val() + ";" + id);
        ValidateSubmit(count);
    }
}

function ValidateSubmit(count) {
    var total = $("#frmRedemptionRequestEffect #hdfTotal").val();
    if (total == count) {
        Submit();
    } else {
    var next = parseInt(count) + 1;
        $("#frmRedemptionRequestEffect #divConfirm-" + count).hide();
        $("#frmRedemptionRequestEffect #divConfirm-" + next).show();
    }
}

function Submit() {

    $.ajax({
        url: "/Financial/ConfirmRedemptionRequestEffect",
        type: "POST",
        dataType: "json",
        data: { idsToCancel: $("#frmRedemptionRequestEffect #hdfIds").val() },
        success: function (data) {
            if (data.Error == "") {
                Modal.Close();
                $("div#divRedemptionRequestFeedback span").html("Solicitação de resgate efetuada com sucesso.");
                $("div#divRedemptionRequestFeedback").show();
                fnSearch($('#redemptionRequestFilterContent'));
            } else if (data.Error == "0") {
                Modal.Close();
            }
            else {
                $("#feedbackError").html(data.Error);
                $("#feedbackError", editContext).show();
            }
            ReloadPending();
            $("#chkAll").attr('checked', false);
        }
    });

}