﻿var DailyFinancialReportFilter = {
    ValidateFilterFields: function () {
        var messageError = "";
        if (!validateHelper.ValidateList($("#txtAssessorFilter").val()))
            messageError += "<li>Faixa de assessores inválida.</li>";

        if (!validateHelper.ValidateList($("#txtClientFilter").val()))
            messageError += "<li>Faixa de clientes inválida.</li>";

        if (messageError.length > 0)
            messageError = "<ul>" + messageError + "</ul>";

        return { Result: (messageError == ""), Message: messageError }
    }
};

$(function () {
    var context = $('#DailyFinancialReportContent');

    $("#txtMovementDate").setMask("99/99/9999").datepicker().datepicker($.datepicker.regional['pt-BR']);

    $("#DailyFinancialReportFilter").click(function () {
        $('#DailyFinancialReportContent').slideToggle('fast');
        $('select').styleInputs();
        $(this).toggleClass('aberto');
    });

    $(context).bind('keydown', function (event) {
        if (event.which == 13) {
            $("#lnkSubmitFilter").click();
        }
    });

    $("#lnkSubmitFilter").unbind("click");
    $("#lnkSubmitFilter").bind("click", function () {

        var feedback = DailyFinancialReportFilter.ValidateFilterFields();

        if (feedback.Result) {
            $("#filterFeedbackError").html("").hide();
            $("#DailyFinancialReportContent").hide();
            $("#loadingArea").show();
            $("#table-area").hide();
            $("#feedback").hide().find("span").html('');
            $("#feedbackError").hide().find("span").html('');

            if (oTableDailyFinancial != null)
                oTableDailyFinancial = null;

            oTableDailyFinancial = $("#tblDailyFinancialReport").dataTable(objDailyFilter);

            DailyFinancialReportMethods.enableExportButtons();
        }
        else {
            $("#filterFeedbackError").html(feedback.Message).show();
        }
    });
});
