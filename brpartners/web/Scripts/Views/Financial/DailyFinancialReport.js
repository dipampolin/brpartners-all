﻿var oTableDailyFinancial = null;

var fnDrawCallback = function (settings) {
    $("#loadingArea").hide();
    $("#table-area").show();

    var totalRecords = settings.fnSettings().fnRecordsDisplay();

    DailyFinancialReportMethods.enableExportButtons();

    var totalPerPage = settings.fnSettings()._iDisplayLength;

    var totalPages = Math.ceil(totalRecords / totalPerPage);

    var currentIndex = parseInt(settings.fnSettings()._iDisplayStart);
    var currentPage = totalPages > 0 ? Math.ceil(1 + (currentIndex / totalPerPage)) : 0;

    $("#tblDailyFinancialReport_info").html("Exibindo " + currentPage + " de " + totalPages);

    if (totalRecords > 0 && totalPages > 1) {
        $("#tblDailyFinancialReport_info").css("display", "");
        $("#tblDailyFinancialReport_length").css("display", "");
        $("#tblDailyFinancialReport_paginate").css("display", "");
    }
    else {
        $("#tblDailyFinancialReport_info").css("display", "none");
        $("#tblDailyFinancialReport_length").css("display", "none");
        $("#tblDailyFinancialReport_paginate").css("display", "none");
    }
}

var objDailyFilter = {
    "bAutoWidth": false
            , "bPaginate": true
            , "bDestroy": true
            , "sPaginationType": "full_numbers"
            , "sDom": 'trip'
            , "oLanguage": {
                "sLengthMenu": "Exibindo _MENU_ registros",
                "sZeroRecords": "Nenhum registro encontrado.",
                "sInfo": "Encontramos _TOTAL_ registros",
                "sInfoEmpty": "Nenhum registro encontrado",
                "sInfoFiltered": "(Filtrando _MAX_ registros)",
                "sSearch": "Buscar:",
                "sProcessing": "Processando...",
                "oPaginate": {
                    "sFirst": "Primeiro",
                    "sPrevious": "Anterior",
                    "sNext": "Próximo",
                    "sLast": "Último"
                }
            }
            , "bProcessing": true
            , "bServerSide": true
            , "sAjaxSource": "../Financial/DailyFinancialReportFilter"
            , "fnServerData": function (sSource, aoData, fnCallback) {
                if (aoData != null && aoData != undefined) {
                    aoData.push({ "name": "txtAssessorFilter", "value": $('#txtAssessorFilter').val() });
                    aoData.push({ "name": "txtClientFilter", "value": $('#txtClientFilter').val() });
                    aoData.push({ "name": "chkD0", "value": $("input[name='chkD0']").is(":checked") ? "S" : "N" });
                    aoData.push({ "name": "ddlStatus", "value": $('#ddlStatus').val() });
                    aoData.push({ "name": "ddlCustody", "value": $("#ddlCustody").val() });
                    aoData.push({ "name": "txtMovementDate", "value": $("#txtMovementDate").val() });
                }
                $.ajax({
                    "cache": false,
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "beforeSend": function () {
                        $("#table-area").hide();
                        $("#loadingArea").show();
                        $("#emptyArea").hide();
                    },
                    "success": function (data) {
                        fnCallback(data);

                        if (data.iTotalRecords == 0) {
                            $("#table-area").hide();
                            $('#emptyArea').html('Sua pesquisa não encontrou resultados válidos. Tente novamente.').show();
                        }
                        else {
                            $('#emptyArea').html('').hide();
                        }

                        DailyFinancialReportMethods.enableExportButtons();
                    }
                });
            }
            , fnDrawCallback: function () {
                fnDrawCallback(this);
            }
            , "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                for (var i = 3; i <= 10; i++) {

                    if (!isNaN(parseFloat(aData[i]))) {

                        if (i != 5 && i != 6) {
                            var number = parseFloat(aData[i].replace(/\./g, "").replace(/\,/g, "."));

                            if (number > 0) {

                                $(nRow).find("td:eq(" + i.toString() + ")").addClass("positivo");
                            }
                            else if (number < 0) {
                                $(nRow).find("td:eq(" + i.toString() + ")").addClass("negativo");
                            }
                        }
                        else if (i == 5) {
                            $(nRow).find("td:eq(" + i.toString() + ")").addClass("positivo");
                        }
                        else {
                            $(nRow).find("td:eq(" + i.toString() + ")").addClass("negativo");
                        }
                    }
                }

                return nRow;
            }
            , aoColumns: [
                null,
                null,
                null,
                { "sClass": "direita" },
                { "sClass": "direita" },
                { "sClass": "direita" },
                { "sClass": "direita" },
                { "sClass": "direita" },
                { "sClass": "direita" },
                { "sClass": "direita" },
                { "sClass": "direita" },
                null
            ]
};



var objDailyInitial = {
    "bAutoWidth": false
            , "bPaginate": true
            , "bDestroy": true
            , "sPaginationType": "full_numbers"
            , "sDom": 'trip'
            , "oLanguage": {
                "sLengthMenu": "Exibindo _MENU_ registros",
                "sZeroRecords": "Nenhum registro encontrado.",
                "sInfo": "Encontramos _TOTAL_ registros",
                "sInfoEmpty": "Nenhum registro encontrado",
                "sInfoFiltered": "(Filtrando _MAX_ registros)",
                "sSearch": "Buscar:",
                "sProcessing": "Processando...",
                "oPaginate": {
                    "sFirst": "Primeiro",
                    "sPrevious": "Anterior",
                    "sNext": "Próximo",
                    "sLast": "Último"
                }
            }
            , "bProcessing": true
            , "bServerSide": true
            , "sAjaxSource": "../Financial/InitialDailyFinancialFilter"
            , "fnServerData": function (sSource, aoData, fnCallback) {
                $.ajax({
                    "cache": false,
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "beforeSend": function () {
                        $("#table-area").hide();
                        $("#loadingArea").show();
                        $("#emptyArea").hide();
                    },
                    "success": function (data) {
                        fnCallback(data);

                        if (data.iTotalRecords == 0) {
                            $("#table-area").hide();
                            $('#emptyArea').html('Sua pesquisa não encontrou resultados válidos. Tente novamente.').show();
                        }
                        else {
                            $('#emptyArea').html('').hide();
                        }

                        DailyFinancialReportMethods.enableExportButtons();
                    }
                });
            }
            , fnDrawCallback: function () {
                fnDrawCallback(this);

            }
            , "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                for (var i = 3; i <= 10; i++) {

                    if (!isNaN(parseFloat(aData[i]))) {

                        if (i != 5 && i != 6) {
                            var number = parseFloat(aData[i].replace(/\./g, "").replace(/\,/g, ".")); 
                            
                            if (number > 0) {

                                $(nRow).find("td:eq(" + i.toString() + ")").addClass("positivo");
                            }
                            else if (number < 0) {
                                $(nRow).find("td:eq(" + i.toString() + ")").addClass("negativo");
                            }
                        }
                        else if (i == 5) {
                            $(nRow).find("td:eq(" + i.toString() + ")").addClass("positivo");
                        }
                        else {
                            $(nRow).find("td:eq(" + i.toString() + ")").addClass("negativo");
                        }
                    }
                }

                return nRow;
            }
            , aoColumns: [
                null,
                null,
                null,
                { "sClass": "direita" },
                { "sClass": "direita" },
                { "sClass": "direita" },
                { "sClass": "direita" },
                { "sClass": "direita" },
                { "sClass": "direita" },
                { "sClass": "direita" },
                { "sClass": "direita" },
                null
            ]
};

var DailyFinancialReportMethods = {

    enableExportButtons: function () {
        var data = $("#tblDailyFinancialReport tbody tr");
        var emptyData = (data.length == 1 && data[0].lastChild.className == "dataTables_empty") ? true : false;
        var excelLink = $("#lnkDailyExcel");
        var pdfLink = $("#lnkDailyPdf");
        excelLink.attr("href", (emptyData) ? "#" : "/Financial/DailyFinancialReportToFile/?isPdf=false");
        //excelLink.attr("target", (emptyData) ? "_self" : "_blank");
        pdfLink.attr("href", (emptyData) ? "#" : "/Financial/DailyFinancialReportToFile/?isPdf=true");
        //pdfLink.attr("target", (emptyData) ? "_self" : "_blank");
    }
       
};

$(function () {
    if (oTableDailyFinancial != null)
        oTableDailyFinancial = null;
    oTableDailyFinancial = $("#tblDailyFinancialReport").dataTable(objDailyInitial);

    DailyFinancialReportMethods.enableExportButtons();

});
