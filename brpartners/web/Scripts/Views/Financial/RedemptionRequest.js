﻿/// <reference path="../../References.js" />

var oTable;
var emptyDataMessage = "Sua pesquisa não encontrou resultados válidos. Tente novamente.";
var filterPage = 1;
var feedbackCount = 1;

var fnApprove = function () {

    $("#btnApprove").unbind("click").bind("click", function () {
        var checkLink = false;
        var checkedRequests = $("[id^='chkRequest']:checked");
        if (checkedRequests.length > 0) {
            var ids = [];
            $(checkedRequests).each(function () {
                if ($(this).attr("rel") != '17' && !checkLink){
                    checkLink = true;
                }

                ids.push($(this).attr('id').split("-")[1]);
            });

            if (!checkLink){
                var url = "/Financial/RedemptionRequestApprove/";
                commonHelper.OpenModalPost(url, 890, ".content", "", $.param({ requestsIds: ids }, true));
            }
            else{
                $("#feedbackError").html('Algumas requisições já foram previamente aprovadas.');
            }
        }
        else {
            $("#feedbackError").html('Nenhuma requisição selecionada.');
        }

    });
};

$(function () {
    /*** modal de mudança de status ***/
    $("#lbDisapproveNote").hide();
    $("#ddlStatusChange").change(function () {
        if ($("#ddlStatusChange option:selected").val() == "20") {
            $("#lbDisapproveNote").show();
            $("#taDisapproveNote").show();
            $("#taDisapproveNote").maxlength({ limit: 60 });
        }
        else {
            $("#lbDisapproveNote").hide();
            $("#taDisapproveNote").hide();
            $("#taDisapproveNote").val('');
        }
    });
    /*** fim - modal de mudança de status ***/

    $("#chkAll").click(function () {
        var check = $(this).is(':checked');
        $("table#redemptionRequestList input:checkbox:not(:disabled)").each(function () { $(this).attr('checked', check); })
    });

    $('.closeLink').bind({
        "click": function () {
            $('.modal-situacao').hide();
            return false;
        }
    });

    var context = $('#redemptionRequestFilterContent');

    $("#txtStartDate", context).setMask("99/99/9999").datepicker().datepicker($.datepicker.regional['pt-BR']);
    $("#txtEndDate", context).setMask("99/99/9999").datepicker().datepicker($.datepicker.regional['pt-BR']);

    //LoadDefaultSearch(context);

    $(context).bind('keydown', function (event) {
        if (event.which == 13) {
            fnSearch(context);
            return false;
        }
    });

    $("#btnSearch", context).click(function () {

        fnSearch(context);

        return false;
    });

    enableExportButtons();

    setTimeout(function () {
        fnSearch(context);

        //exibindo Div de Pendentes
        if ($("#pendingTag").length > 0) {
            if ($('#trilha').children('ul').find('li').last().length > 0)
                $('#trilha').children('ul').find('li').last().append($("#pendingTag"));
            else
                $('#trilha').children('ul').append($("#pendingTag"));
            $("#pendingTag").show();
        }
    }, 250);

    if ($("#pendingTag").length > 0) {
        if ($('#trilha').children('ul').find('li').last().length > 0)
            $('#trilha').children('ul').find('li').last().append($("#pendingTag"));
        else
            $('#trilha').children('ul').append($("#pendingTag"));
        $("#pendingTag").show();
    }
});

function fnSearch(context) {
    if (feedbackCount == 0)
        $("#feedbackError").hide();

    var feedback = ValidateFilterFields(context);

    if (feedback == "") {

        $("#emptyArea").hide();
        $("#tableArea").hide();
        $("#loadingArea").show();

        if (context.css("display") != "none")
            $("#redemptionRequestFilter").click();

        $("#filterFeedbackError", context).hide();

        feedbackCount = 0;

        /*var status = commonHelper.GetParameterByName("status");
        if (status != null && status != undefined && status != "" && !actionClick) {
        $("#ddlStatus", context).val(status);
        }*/

        $.ajax({
            cache: false,
            type: "POST",
            async: true,
            url: "/Financial/LoadRedemptionRequests",
            data: { assessors: $("#txtAssessors", context).val(), clients: $("#txtClient", context).val(), status: $("#ddlStatus", context).val(), startDate: $("#txtStartDate", context).val(), endDate: $("#txtEndDate", context).val() },
            success: function () {
                obj = {};
                obj = {
                    "aoColumns": [
                            { "bSortable": false },
                            { "sType": "datetime" },
                            null,
                            null,
                            { "bSortable": false },
                            { "bSortable": false },
                            { "bSortable": false },
                            { "sClass": "direita" },
                            null,
                            { "bSortable": false }
                        ],
                    "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                        var i = 7;
                        if (!isNaN(parseFloat(aData[i]))) {
                            var number = parseFloat(aData[i].replace(/\./g, "").replace(/\,/g, "."));
                            if (number > 0) {
                                $(nRow).find("td:eq(" + i.toString() + ")").addClass("positivo");
                            }
                            else if (number < 0) {
                                $(nRow).find("td:eq(" + i.toString() + ")").addClass("negativo");
                            }
                        }
                        return nRow;
                    },
                    "aaSorting": [[1, "desc"]],
                    "bSort": true,
                    "bAutoWidth": false,
                    "bInfo": true,
                    "iDisplayLength": 10,
                    "bDestroy": true,
                    "oLanguage": {
                        "sLengthMenu": "<span>Exibir:</span> _MENU_ <span>por página</span>",
                        "sZeroRecords": "Sua pesquisa não encontrou resultados válidos. Tente novamente.",
                        "oPaginate": {
                            "sPrevious": "Anteriores",
                            "sNext": "Próximos"
                        }
                    },
                    "sPaginationType": "full_numbers",
                    "sDom": 'rtip',
                    "fnDrawCallback": function () {
                        var totalRecords = this.fnSettings().fnRecordsDisplay();
                        var emptyArea = $("#emptyArea");
                        var tableArea = $("#tableArea");

                        enableExportButtons();

                        $("#loadingArea").hide();

                        if (totalRecords == 0) {
                            emptyArea.html(emptyDataMessage);
                            emptyArea.show();
                            tableArea.hide();
                        }
                        else {
                            tableArea.show();
                            emptyArea.hide();

                            var totalPerPage = this.fnSettings()._iDisplayLength;

                            var totalPages = Math.ceil(totalRecords / totalPerPage);

                            var currentIndex = parseInt(this.fnSettings()._iDisplayStart);
                            var currentPage = totalPages > 0 ? Math.ceil(1 + (currentIndex / totalPerPage)) : 0;

                            $("#redemptionRequestList_info").html("Exibindo " + currentPage + " de " + totalPages);

                            if (totalRecords > 0 && totalPages > 1) {
                                $("#redemptionRequestList_info").css("display", "");
                                $("#redemptionRequestList_length").css("display", "");
                                $("#redemptionRequestList_paginate").css("display", "");
                            }
                            else {
                                $("#redemptionRequestList_info").css("display", "none");
                                $("#redemptionRequestList_length").css("display", "none");
                                $("#redemptionRequestList_paginate").css("display", "none");
                            }
                            $('select', '#redemptionRequestList_length').styleInputs();

                            enableExportButtons();
                        }
                        // Adiciona a classe 'ultimo' à última tr no corpo da tabela
                        $('table tbody tr:last-child').addClass('ultimo');

                        // Adiciona a classe 'ultimo' às últimas th e td no corpo da tabela
                        $('table tr td:last-child').addClass('ultimo');
                        $('table tr th:last-child').addClass('ultimo');

                        $('table thead tr th').removeClass('direita');

                        fnApprove();
                    },
                    "bProcessing": false,
                    "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                        $('#redemptionRequestList_first').css("visibility", "hidden");
                        $('#redemptionRequestList_last').css("visibility", "hidden");
                    },
                    "bRetrieve": true,
                    "bServerSide": true,
                    "sAjaxSource": "../DataTables/RedemptionRequest_List",
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        $.ajax({
                            cache: false,
                            dataType: 'json',
                            type: "POST",
                            url: sSource,
                            data: aoData,
                            success: fnCallback
                        });
                    }
                };

                if (oTable != null)
                    oTable.fnDestroy();
                oTable = $("#redemptionRequestList").dataTable(obj);
                oTable.fnClearTable();
                oTable.fnDraw();
            }
        });

    }
    else {
        $("#filterFeedbackError", context).html(feedback).show();
    }
}

function enableExportButtons() {
	var data = $("#redemptionRequestList tbody tr");
	var emptyData = (data.length == 1 && data[0].lastChild.className == "dataTables_empty") ? true : false;
	var excelLink = $("#lnkRedemptionRequestExcel");
	var pdfLink = $("#lnkRedemptionRequestPdf");
	excelLink.attr("href", (emptyData) ? "#" : "/Financial/RedemptionRequestItemsToFile/?isPdf=false");
	//excelLink.attr("target", (emptyData) ? "_self" : "_blank");
	pdfLink.attr("href", (emptyData) ? "#" : "/Financial/RedemptionRequestItemsToFile/?isPdf=true");
	//pdfLink.attr("target", (emptyData) ? "_self" : "_blank");
}

function OpenRequest(id) {
    commonHelper.OpenModal("/Financial/RedemptionRequestEdit/" + id, 890, ".content", "");
}

function ViewHistory() {
    commonHelper.OpenModal("/Financial/RedemptionRequestHistory", 671, ".content", "");
}

function OpenHistoryDetail(id) {
    var url = "/Financial/RedemptionRequestHistoryDetail/?requestId=" + id;
    commonHelper.OpenModal(url, 671, ".content", "");
    $("#MsgBox").center();
}

function DeleteRequest(id, clientCode) {
    var url = "/Financial/RedemptionRequestConfirmDelete/?requestId=" + id + "&clientCode=" + clientCode;
    commonHelper.OpenModal(url, 450, ".content", "");
}

function ValidateFilterFields(context) {
    var messageError = "";

    if (!validateHelper.ValidateList($("#txtAssessors", context).val()))
        messageError += "<li>Faixa de assessores inválida.</li>";

    if (!validateHelper.ValidateList($("#txtClient", context).val()))
        messageError += "<li>Faixa de clientes inválida.</li>";

    var invalidDate = false;
    $("input[id*='Date']", context).each(function () {
        if ($(this).val() != "" && !invalidDate) {
            if (!DateValidate($(this).val())) {
                invalidDate = true;
                messageError += "<li>Data inválida.</li>";
            }
        }
    });

    if (!invalidDate) { 
        if (!dateHelper.CheckPeriod("#txtStartDate","#txtEndDate", context))
            messageError += "<li>Periodo de data inválido.</li>";
    }

    if (messageError.length > 0)
        messageError = "<ul>" + messageError + "</ul>";

    return messageError;
}

function DateValidate(date) {
    var reDate = /^((0[1-9]|[12]\d)\/(0[1-9]|1[0-2])|30\/(0[13-9]|1[0-2])|31\/(0[13578]|1[02]))\/\d{4}$/;
    if (reDate.test(date) || (date == '')) {
        return true
    } else {
        return false;
    }
}

function ChangeStatus(self, id, clientCode, statusId) {
	$('.modal-situacao').show();
	var offset = $(self).offset();
	$('.modal-situacao').css("top", offset.top);

	$("#ddlStatusChange option[value='']").addClass('not');

	if (statusId == "17") {
		$("#ddlStatusChange option[value='17']").addClass('not');
		$("#ddlStatusChange option[value='18']").removeClass('not');
		$("#ddlStatusChange option[value='19']").addClass('not');
		$("#ddlStatusChange option[value='20']").removeClass('not');
	}
	else if (statusId == "18") {
		$("#ddlStatusChange option[value='17']").addClass('not');
		$("#ddlStatusChange option[value='18']").addClass('not');
		$("#ddlStatusChange option[value='19']").removeClass('not');
		$("#ddlStatusChange option[value='20']").addClass('not');
	}

	$("#hdCurrentStatus", ".modal-situacao").val(statusId);
	$("#hdId", ".modal-situacao").val(id);
	$("#hdClientCode", ".modal-situacao").val(clientCode);

	$("#feedbackStatus", ".modal-situacao").hide();
	$("option:eq(0)", "#ddlStatusChange").attr('selected','selected').trigger('change');
	$("#ddlStatusChange").styleInputs();
}

// Função da modal para alterar situação in-line
function ValidateSettle() {
	var currentStatus = $("#hdCurrentStatus", ".modal-situacao").val();
	var ddlValue = $("#ddlStatusChange", ".modal-situacao").val();
	if (ddlValue != "" && ddlValue != currentStatus) {
	    $.ajax({
	        url: "/Financial/RedemptionRequestChangeStatus",
	        type: "POST",
	        dataType: "json",
	        data: { hdId: $("#hdId").val(), hdClientCode: $("#hdClientCode").val(), ddlStatusChange: $("#ddlStatusChange").val(), taDisapproveNote: $("#taDisapproveNote").val() },
	        success: function (data) {
	            $(".modal-situacao").hide();
	            if (data.Error == "") {
	                $("#divRedemptionRequestFeedback").find("span").html(data.Message);
	                fnSearch($('#redemptionRequestFilterContent'));
	            }
	            else {
	                fnSearch($('#redemptionRequestFilterContent'));
	                $("#feedbackError").html(data.Error);
	                $("#feedbackError").show();
	            }

	            ReloadPending();
	        }
	    });
	}
	else {
		$("#feedbackStatus", ".modal-situacao").show();
		return false;
	}
}

function OpenEffect() {
    var checkedRequests = $("[id^='chkRequest']:checked");
    if ($("[id^='chkRequest']:checked").length > 0) {
        var ids = "";
        $(checkedRequests).each(function () {
            ids += $(this).attr('id').split("-")[1] + ";";
        });
        var url = "/Financial/RedemptionRequestEffect/?requestsIds=" + ids;
        commonHelper.OpenModal(url, 890, ".content", "");
    }
}

function ReloadPending() {
    $.ajax({
        url: "/Home/GetPending",
        type: "POST",
        dataType: "json",
        async: false,
        data: { module: "Financial.RedemptionRequest" },
        success: function (data) {
            if (data > 0) {
                var pendingContent = "<span><strong>" + data + "</strong> Pendente" + (data > 1 ? "s" : "") + "</span>";

                if ($("#pendingTag").length > 0) {
                    $("#pendingTag").html(pendingContent);
                }
                else {
                    var pending = "<div id='pendingTag' class='aviso' style='display: none;'>" + pendingContent + "</div>";
                    if ($('#trilha').children('ul').find('li').last().length > 0)
                        $('#trilha').children('ul').find('li').last().append(pending);
                    else
                        $('#trilha').children('ul').append(pending);
                }
                $("#pendingTag").show();
            }
            else {
                $("#pendingTag").hide();
            }
        }
    });

}