﻿/// <reference path="../../References.js" />

$(document).ready(function () {

    var editContext = $("#divRedemptionRequestEdit");
    var clientCode = $("#hdfClientCode", editContext).val();

    $("#txtAmount", editContext).attr("disabled", "disabled");
    $("#chkTotalD0", editContext).attr("checked", "");
    $("#chkTotalD0", editContext).attr("disabled", "disabled");

    $("#lblClientName", editContext).hide();

    if (clientCode != "0") {
        $("#txtAmount", editContext).attr("disabled", "");
        $("#txtAmount", editContext).setMask('decimal');
        $("#chkTotalD0", editContext).attr("disabled", "");
        LoadBankAccountInformation(clientCode);
        CalculateBalanceInformation($("#txtAmount", editContext).val(), editContext);
    }

    $("#txtAmount", editContext).setMask('decimal');

    if ($("#hdfClientCode", editContext).val() != "0") { $("#txtClientName", editContext).attr("disabled", "disabled"); }

    $("#lnkSave", editContext).bind("click", function () {
        if ($("#hdfSave").val() == "false") {
            var feedback = ValidateFields(editContext);
            if (feedback != "") {
                $("#feedbackRequestError", editContext).html(feedback);
                $("#feedbackRequestError", editContext).show();
                return false;
            }
            var feedbackBalance = ValidateBalance(editContext);
            if (feedbackBalance != "") {
                $("#feedbackRequestError", editContext).html(feedbackBalance);
                $("#feedbackRequestError", editContext).show();
                $("#hdfSave").val("true");
                return false;
            }
            $("#hdfSave").val("true");
            $("#lnkSave", editContext).click();
        }
        else {
            $.ajax({
                url: "/Financial/RedemptionRequestEdit",
                type: "POST",
                dataType: "json",
                data: { clientCode: $("#hdfClientCode", editContext).val(), amount: $("#txtAmount", editContext).val(), bank: $("#ddlBankAccounts", editContext).val(), requestId: $("#hdfRequestId", editContext).val() },
                success: function (data) {
                    if (data.Error == "") {
                        var msg = $("#hdfRequestId", editContext).val() == "0" ? "Solicitação de resgate inserida com sucesso." : "Solicitação de resgate atualizada com sucesso.";
                        Modal.Close();
                        $("div#divRedemptionRequestFeedback span").html(msg);
                        $("div#divRedemptionRequestFeedback").show();
                        //$("#redemptionRequestFilterContent #btnSearch").click();
                        fnSearch($('#redemptionRequestFilterContent'));
                    }
                    else {
                        Modal.Close();
                        $("#feedbackError", editContext).html(data.Error);
                        $("#feedbackError", editContext).show();
                    }

                    ReloadPending();
                }
            });
        }
        return false;
    });

    $("#chkTotalD0", editContext).bind("click", function () {
        $("#hdfSave").val("false");
        $("#feedbackRequestError").hide();
        if ($(this).is(":checked")) {
            var txtAmount = $("#txtAmount", editContext);
            txtAmount.val($("#hdfBalanceD0", editContext).val().replace('-', ''));
            CalculateBalanceInformation(txtAmount.val(), editContext);
            //ShowSaveButton(false, editContext);
        }
        else {
            $("#txtAmount").val(0);
            $("#txtAmount", editContext).setMask('decimal');
            CalculateBalanceInformation($("#txtAmount", editContext).val(), editContext);
            //ShowSaveButton(false, editContext)
        }
    });

    $("#txtAmount", editContext).bind("blur", function () {
        $("#hdfSave").val("false");
        var amount = $(this).val();
        if (amount != "") {
            CalculateBalanceInformation(amount, editContext);
        }
        return false;
    });

    $("#txtClientName", editContext).unbind("blur").bind("blur", function () {
        $("#hdfSave").val("false");
        LoadBankAccountInformation("");
        $("#feedbackRequestError").hide();
        $("#txtAmount").show();
        $("#chkTotalD0").show();
        $("#lnkValidate").show();
        $("#lblAmount").show();
        $("#lblTotalD0").show();

        var code = $(this).val();

        $("#hdfClientCode", editContext).val("0");
        $("#lblClientName", editContext).html("").attr('title', "").hide();
        $("#ddlBankAccounts", editContext).html("");

        $("span[id^='lblBalance']", editContext).html("");
        //$("#divBalanceContent", editContext).hide();
        $("#hdfBalanceD0", editContext).val(0);
        $("#txtAmount", editContext).val(0).setMask('decimal').attr("disabled", "disabled");
        $("#chkTotalD0", editContext).attr("checked", "").attr("disabled", "disabled");
        //ShowSaveButton(false, editContext);

        $("span[id^='lblRedemption']").html("");

        if (code != "") {

            $.ajax({
                type: "POST",
                url: "/NonResidentTraders/GetTraderName/",
                dataType: "json",
                async: false,
                data: { code: code },
                success: function (data) {
                    $("#lblClientName", editContext).html(String(data)).attr('title', String(data)).show();
                }
            });

            var loadAccount = true;

            var lblClientName = $("#lblClientName", editContext).html();
            if (lblClientName != "Cliente não encontrado." && lblClientName != "") {
                $("#hdfClientCode", editContext).val(code);
                $("#chkTotalD0", editContext).attr("disabled", "");
                $("#txtAmount", editContext).attr("disabled", "").val(0).setMask('decimal');
                loadAccount = LoadBankAccountInformation(code);
            }

            var feedback = "";
            var balance = parseFloat($("#hdfBalanceD0", editContext).val().replace(/\./g, '').replace(',', '.'));

            if (balance <= 0 && lblClientName != "Cliente não encontrado." && loadAccount) {
                feedback += "<li>Não há saldo suficiente.</li>";
            }
            else if (!loadAccount) {
                feedback += "<li>Não é possível realizar operações para este cliente.</li>";
            }

            if (feedback != "") {
                $("#feedbackRequestError", editContext).html(feedback);
                $("#feedbackRequestError", editContext).show();
                $("#txtAmount", editContext).attr("disabled", "disabled");
                $("#chkTotalD0", editContext).attr("checked", "").attr("disabled", "disabled");
                return false;
            }
        }
    });


});



function LoadBankAccountInformation(code) {
    var loadAccount = true;
    $.ajax({
        url: "/Financial/LoadBankAccountInformation",
        type: "POST",
        dataType: "json",
        data: { code: code },
        async: false,
        success: function (data) {
            if (data.Error == "") {
                $("#ddlBankAccounts").append(data.BankAccount).styleInputs();
                //var bankAccountNumber = $("#hdfBankAccountNumber").val();
                if ($("#hdfBankAccountNumber").val() != "") {
                    $("#ddlBankAccounts option:contains(" + $("#hdfBankAccountNumber").val() + ")").attr('selected', 'selected');
                }

                $("#lblBalanceD0").html(data.Balance.Available);
                $("#lblBalanceD1").html(data.Balance.AvailableD1);
                $("#lblBalanceD2").html(data.Balance.AvailableD2);
                $("#lblBalanceD3").html(data.Balance.AvailableD3);
                $("#lblBalanceTotal").html(data.Balance.Total);
                $("#lblBalanceProjetado").html(data.Balance.Intended);
                $("#divBalanceContent").show();

                $("#hdfBalanceD0").val(data.Balance.Available);

            }
            else {
                $("#ddlBankAccounts").html("<option value=''></option>").styleInputs();
                loadAccount = false;
                //$("#feedbackRequestError").show().html(data.Error);
            }
        }
    });

    return loadAccount;
}

function CalculateBalanceInformation(amount, editContext) {
    $("#chkTotalD0", editContext).attr("checked", amount == $("#hdfBalanceD0").val());

    $.ajax({
        url: "/Financial/CalculateBalanceInformation",
        type: "POST",
        dataType: "json",
        data: { amount: amount },
        async: false,
        success: function (data) {
            if (data.Balance != "") {
                BalanceStyle("lblRedemptionD0", data.Balance.Available, editContext);
                BalanceStyle("lblRedemptionD1", data.Balance.AvailableD1, editContext);
                BalanceStyle("lblRedemptionD2", data.Balance.AvailableD2, editContext);
                BalanceStyle("lblRedemptionD3", data.Balance.AvailableD3, editContext);
                BalanceStyle("lblRedemptionTotal", data.Balance.Total, editContext);
                BalanceStyle("lblRedemptionProjetado", data.Balance.Intended, editContext);
            }
            else {
                //$("#ddlBankAccounts").append(data.Error).styleInputs();
            }
        }
    });
    return false;
}

function BalanceStyle(field, amount, context) {
    if (amount.indexOf('-') != -1) {
        $("#" + field, context).html(amount).removeClass("positivo");
        $("#" + field, context).html(amount).addClass("negativo");
    }
    else {
        $("#" + field, context).html(amount).removeClass("negativo");
        $("#" + field, context).html(amount).addClass("positivo");
    }
}

function ValidateFields(validateContext) {

    $("#feedbackRequestError", validateContext).html("");
    $("#feedbackRequestError", validateContext).hide();

    var feedback = "";

    if ($("#hdfClientCode", validateContext).val() == "0") {
        feedback += "<li>É necessário preencher um cliente.</li>";
    }

    if ($("#ddlBankAccounts", validateContext).val() == "") {
        feedback += "<li>É necessário escolher uma conta.</li>";
    }

    var amount = parseFloat($("#txtAmount", validateContext).val().replace(/\./g, '').replace(',', '.'));
    var balance = parseFloat($("#hdfBalanceD0", validateContext).val().replace(/\./g, '').replace(',', '.'));

    if (balance < 0) {
        feedback += "<li>Não há saldo suficiente.</li>";
    } else if (amount == 0) {
        feedback += "<li>É necessário preencher um valor.</li>";
    } else if (amount > balance) {
        feedback += "<li>O valor deve ser menor ou igual ao saldo em D0.</li>";
    }

    if (feedback.length > 0) {
        feedback = "<ul>" + feedback + "</ul>";
    }

    return feedback;
}

function ValidateBalance(validateContext) {

    $("#feedbackRequestError", validateContext).html("");
    $("#feedbackRequestError", validateContext).hide();

    var feedback = "";
    var amount = parseFloat($("#txtAmount", validateContext).val().replace(/\./g, '').replace(',', '.'));
    $("span[id^='lblRedemption'][id!='lblRedemptionProjetado']", validateContext).each(function () {
        var thisValue = $(this).html();
        if (parseFloat(thisValue.replace(/\./g, '').replace(',', '.')) < 0) {
            feedback += "<li>R$ " + thisValue + " em " + this.id.replace('lblRedemption', '') + "</li>";
            hasNegativeBalance = true;
        }
    });

    if (feedback.length > 0) {
        feedback = "Esta conta ficará com saldo negativo de <ul>" + feedback + "</ul> caso o resgate seja feito. Deseja prosseguir mesmo assim?";
    }

    return feedback;
}

