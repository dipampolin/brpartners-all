﻿/// <reference path="../../jQuery/jquery-1.4.3.js" />

$(function () {

    $(document).ready(function () {

        var filterContext = $("#redemptionRequestFilterContent");

        $("#redemptionRequestFilter").click(function () {
            $('#redemptionRequestFilterContent').slideToggle('fast');
            $('select').styleInputs();
            $(this).toggleClass('aberto');
        });
    });
});

