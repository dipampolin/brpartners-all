﻿$(function () {

	$(document).ready(function () {

		jQuery.validator.addMethod("decimalbr", function (value, element) {
			return value == "" || ValidateDecimal(value);
		}, jQuery.format("Funcionário inexistente."));

		$('#frmRegistration').validate({
			onfocusout: false,
			onkeyup: false,
			errorLabelContainer: "#feedbackError",
			wrapper: "",
			rules: {
				Email: { email: true },
				TransferBrokerage: { decimalbr: true }
			},
			messages: {
				Email: { email: 'email inválido.' },
				TransferBrokerage: { decimalbr: "valor de repasse inválido." }
			}

		});

		$("#BrokerCode").setMask("9999999");
		$("#PhoneDDDNumber").setMask("9999999");
		$("#PhoneNumber").setMask("9999999999");
		$("#FaxDDDNumber").setMask("9999999");
		$("#FaxNumber").setMask("9999999999");
	});
});


function ValidateDecimal(value) {
	var reDigits = /^([0-9]{1,2}|100)(\,\d{1,3}|00)?$/;
	return reDigits.test(value);
}

function TryToSubmit(link) {
	$($(link).parents('form')).validate();
}