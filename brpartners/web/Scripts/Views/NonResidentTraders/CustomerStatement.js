﻿/// <reference path="../../References.js" />

var oTable;
var arrayOfCodes = [];
var emptyDataMessage = "Sua pesquisa não encontrou resultados válidos. Tente novamente.";


$(function () {
    handlers();
});

function handlers() {
    var context = $("#filterArea");

    $("#txtClientCode", context).blur(function () {
        commomDataHelper.GetClientName($(this).val(), "lblTraderName", context);
    });

    $(context).bind('keydown', function (event) {
       	if (event.which == 13) {
       		$("#btnSearch").click();
       		return false;
       	}
    });

    $("#btnSearch", context).click(function () {
       	$("#reportArea").hide();
        searchRequest($("#txtClientCode", context).val(), $("#sltMonths", context).val() + "/" + $("#sltYears", context).val());
    });

    $("#checkAll", $("#tableArea")).click(function () {
        checkAll();
    });
}

function searchRequest(code, date) {
    commonHelper.ShowLoadingArea();
    $.ajax({
        async: false,
        url: "../NonResidentTraders/CustomerStatement",
        type: "POST",
        data: { "code": code, "date": date },
        dataType: "json",
        success: function (data) {
            if (data.Status == "True") {
                var tableId = "clientTable";
                obj = {
                    "bSort": true,
                    "bAutoWidth": false,
                    "bInfo": true,
                    "bSort": true,
                    "bLengthChange": false,
                    "oLanguage": {
                        "sProcessing": "Carregando...",
                        "sLengthMenu": "<span>Exibir:</span> _MENU_ <span>por página</span>",
                        "sZeroRecords": "",
                        "oPaginate": {
                            "sPrevious": "Anteriores",
                            "sNext": "Próximos"
                        }
                    },
                    "sPaginationType": "full_numbers",
                    "sDom": 'rtipl',
                    "fnDrawCallback": function () {
                        var totalRecords = this.fnSettings().fnRecordsDisplay();

                        if (totalRecords == 0) {
                            $("#emptyArea").html(emptyDataMessage);
                            $("#emptyArea").show();
                            $("#tableArea").hide();
                        }
                        else {
                            $("#tableArea").show();
                            $("#emptyArea").hide();

                            var totalPerPage = this.fnSettings()._iDisplayLength;
                            var totalPages = Math.ceil(totalRecords / totalPerPage);

                            var currentIndex = parseInt(this.fnSettings()._iDisplayStart);
                            var currentPage = totalPages > 0 ? Math.ceil(1 + (currentIndex / totalPerPage)) : 0;

                            $("#" + tableId + "_info").html("Exibindo " + currentPage + " de " + totalPages);

                            if (totalRecords > 0 && totalPages > 1) {
                                $("#" + tableId + "_info").css("display", "");
                                $("#" + tableId + "_length").css("display", "");
                                $("#" + tableId + "_paginate").css("display", "");
                            }
                            else {
                                $("#" + tableId + "_info").css("display", "none");
                                $("#" + tableId + "_length").css("display", "none");
                                $("#" + tableId + "_paginate").css("display", "none");
                            }
                        }
                        // Adiciona a classe 'ultimo' à última tr no corpo da tabela
                        $('table tbody tr:last-child').addClass('ultimo');

                        // Adiciona a classe 'ultimo' às últimas th e td no corpo da tabela
                        $('table tr td:last-child').addClass('ultimo');
                        $('table tr th:last-child').addClass('ultimo');
                        $("#loadingArea").hide();
                    },
                    "bProcessing": false,
                    "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                        $("#" + tableId + "_first").css("visibility", "hidden");
                        $("#" + tableId + "_last").css("visibility", "hidden");
                    },
                    "bRetrieve": true,
                    "bServerSide": true,
                    "sAjaxSource": "../DataTables/NonResidentTraders_CustomerStatement",
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        $.ajax({
                            cache: false,
                            dataType: 'json',
                            type: "POST",
                            url: sSource,
                            data: aoData,
                            success: fnCallback
                        });
                    }
                };
                oTable = $("#clientTable").dataTable(obj);
                oTable.fnClearTable();
                oTable.fnDraw();
                $(".unbind", $("#tableArea")).unbind("click");

                if (oTable != null && oTable.length > 0 && oTable[0].rows.length > 3) {
                    $("#tableArea").show();
                    $("#reportArea").hide();
                    $("#clientTable tbody", context).html("");
                }
            }
            else {
                for (var i = 0; i < data.Messages.length - 1; i++) {
                    $("#" + data.Messages[i], $("#filterArea")).html(data.Messages[i + 1]);
                }
            }
        }
    });
}

function checkAll() {
    var context = $("#tableArea");
    var elements = $("#clientTable tbody", context).find("input:checkbox");

    for (var i = 0; i < elements.length; i++) {
        var element = $(elements[i]);

        if ($("#checkAll", context).is(":checked")) {
            element.attr("checked", true);
        }
        else {
            element.attr("checked", false);
        }
    }
}

function viewReport(code) {
    commonHelper.ShowLoadingArea();
    var context = $("#filterArea");
    var contextReport = $("#reportArea");
    $("#tableArea").hide();
    $("#reportSubArea", contextReport).html("");
    
    
    var date = $("#sltMonths", context).val() + "/" + $("#sltYears", context).val();
    $("#customerStatPrintReport", contextReport).attr("href", "../NonResidentTraders/PrintCustomerStatementReport?codes=" + code + "&date=" + date);
    $("#customerStatExcelReport", contextReport).attr("href", "../NonResidentTraders/CustomerStatementReportToExcel?code=" + code + "&date=" + "01/" + date);
    $("#customerStatPDFReport", contextReport).attr("href", "../NonResidentTraders/CustomerStatementReportToPDF?code=" + code + "&date=" + "01/" + date);
    commonHelper.LoadView("../NonResidentTraders/CustomerStatementReport?noprint=true&codes=" + code + "&date=" + $("#sltMonths", context).val() + "/" + $("#sltYears", context).val(), "#reportSubArea", contextReport, function () {
        setTimeout(function () {
            $("#loadingArea").hide();
            $("#reportArea").show();
        }, 250);
    }); 
}

function backToList() {
    $("#tableArea").show();
    $("#reportArea").hide();
}

function addCodeIntoArray(code, isIncludeAll) {
    var context = $("#tableArea");

    if (!isIncludeAll) {
        if ($("#client" + code, context).is(":checked")) {
            for (var i = 0; i < arrayOfCodes.length; i++) {
                if (arrayOfCodes[i] == code)
                    return false;
            }

            arrayOfCodes.push(code);
        } else {
            for (var i = 0; i < arrayOfCodes.length; i++) {
                if (arrayOfCodes[i] == code)
                    arrayOfCodes.splice(i, 1);
            }
            $("#checkAll", context).attr("checked", false);
        }
    }
    else {
        if ($("#checkAll", context).is(":checked")) {
            arrayOfCodes = [];
            var elements = $("#clientTable tbody", context).find("input:checkbox");

            for (var i = 0; i < elements.length; i++) {
                var element = $(elements[i]);
                arrayOfCodes.push(element.val());
            }
        } else {
            arrayOfCodes = [];
        }
    }

    var codes;
    for (var i = 0; i < arrayOfCodes.length; i++) {
        if (i == 0)
            codes = arrayOfCodes[i];
        else
            codes += ";" + arrayOfCodes[i];
    }

    if (arrayOfCodes.length > 0) {
        var context = $("#filterArea");
        var date = $("#sltMonths", context).val() + "/" + $("#sltYears", context).val();

        var PDF_url = "../NonResidentTraders/CustomerStatementPDFReportsToZip?";
        var Excel_url = "../NonResidentTraders/CustomerStatementExcelReportsToZip?";
        var Print_url = "../NonResidentTraders/PrintCustomerStatementReport?";

        var PDF_url = PDF_url + "codes=" + codes + "&date=" + date;
        var Excel_url = Excel_url + "codes=" + codes + "&date=" + date;
        var Print_url = Print_url + "codes=" + codes + "&date=" + date;

        $("#customerStatPDFReportsToZip").attr("href", PDF_url);
        $("#customerStatExcelReportsToZip").attr("href", Excel_url);
        $("#printCustomerStatReports").attr("href", Print_url);
    }
    else {
        $("#customerStatPDFReportsToZip").removeAttr("href");
        $("#customerStatExcelReportsToZip").removeAttr("href");
        $("#printCustomerStatReports").removeAttr("href");
        $("#checkAll", context).attr("checked", false);
    }
}