﻿/// <reference path="../../References.js" />


var emptyDataMessage = "Sua pesquisa não encontrou resultados válidos. Tente novamente.";

$(function () {
    var context = $("#filterArea");

    $("body").bind('keydown', function (event) {
    	if (event.which == 13) {
    		$("#btnSearch").click();
    		return false;
    	}
    });

    $("#btnSearch", context).click(function () {
        commonHelper.ShowLoadingArea();
        $("#reportArea").hide();
        var date = $("#sltMonths", context).val() + "/" + $("#sltYears", context).val();
        $("#reportArea").html("");
        $("#reportArea").show();
        setTimeout(function() { 
            commonHelper.LoadView("../NonResidentTraders/StockRecordReport?date=" + date, "#reportArea", null, function() {
                $("#loadingArea").hide();
            });
        }, 400);    
    });
});
