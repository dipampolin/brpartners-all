﻿/// <reference path="../../References.js" />

var oTable;
var emptyDataMessage = "Sua pesquisa não encontrou resultados válidos. Tente novamente.";

$(function () {
	$(document).ready(function () {
		var enteredCode;

		$("#Code").setMask("9999999");

		$("#Code").blur(function () {
			commomDataHelper.GetClientName($(this).val(), "TraderName", $("#filterArea"));
		});

		$("#search").click(function () {
			enteredCode = $("#Code").val();
		});

		$("#searchAll").click(function () {
			enteredCode = "";
			$("#Code").val("       ");
			$("#TraderName").html("");
		});

		$("#filterArea").bind('keydown', function (event) {
			if (event.which == 13) {
				$("#search").click();
				return false;
			}
		});

		$("#search,#searchAll").click(function () {
			commonHelper.ShowLoadingArea();
			$.ajax({
				cache: false,
				async: false,
				type: "POST",
				url: "../NonResidentTraders/LoadTraders",
				data: { Code: enteredCode },
				success: function () {
					obj = {};
					obj = {
						"bSort": true,
						"bAutoWidth": false,
						"bInfo": true,
						"bLengthChange": false,
						"oLanguage": {
							"sProcessing": "Carregando...",
							"sLengthMenu": "<span>Exibir:</span> _MENU_ <span>por página</span>",
							"sZeroRecords": enteredCode != "" ? "Sua pesquisa por '#" + enteredCode + "' não encontrou resultados válidos. Tente novamente." : "Não existem clientes.",
							"oPaginate": {
								"sPrevious": "Anteriores",
								"sNext": "Próximos"
							}
						},
						"sPaginationType": "full_numbers",
						"sDom": 'rtipl',
						"fnDrawCallback": function () {
							var totalRecords = this.fnSettings().fnRecordsDisplay();

							if (totalRecords == 0) {
								$("#emptyArea").html(emptyDataMessage);
								$("#emptyArea").show();
								$("#tableArea").hide();
							}
							else {
								$("#tableArea").show();
								$("#emptyArea").hide();

								var totalPerPage = this.fnSettings()._iDisplayLength;

								var totalPages = Math.ceil(totalRecords / totalPerPage);

								var currentIndex = parseInt(this.fnSettings()._iDisplayStart);
								var currentPage = totalPages > 0 ? Math.ceil(1 + (currentIndex / totalPerPage)) : 0;

								$("#TradersList_info").html("Exibindo " + currentPage + " de " + totalPages);

								if (totalRecords > 0 && totalPages > 1) {
									$("#TradersList_info").css("display", "");
									$("#TradersList_length").css("display", "");
									$("#TradersList_paginate").css("display", "");
								}
								else {
									$("#TradersList_info").css("display", "none");
									$("#TradersList_length").css("display", "none");
									$("#TradersList_paginate").css("display", "none");
								}
								$('select', '#TradersList_length').styleInputs();
							}
							// Adiciona a classe 'ultimo' à última tr no corpo da tabela
							$('table tbody tr:last-child').addClass('ultimo');

							// Adiciona a classe 'ultimo' às últimas th e td no corpo da tabela
							$('table tr td:last-child').addClass('ultimo');
							$('table tr th:last-child').addClass('ultimo');
							$("#loadingArea").hide();
						},
						"bProcessing": false,
						"fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
						    $('#TradersList_first').css("visibility", "hidden");
						    $('#TradersList_last').css("visibility", "hidden");
						},
						"bRetrieve": true,
						"bServerSide": true,
						"sAjaxSource": "../DataTables/NonResidentTraders_Search",
						"fnServerData": function (sSource, aoData, fnCallback) {
							$.ajax({
								cache: false,
								dataType: 'json',
								type: "POST",
								url: sSource,
								data: aoData,
								success: fnCallback
							});
						}
					};

					oTable = $("#TradersList").dataTable(obj);
					oTable.fnClearTable();
					oTable.fnDraw();
				}
			});
		});

		//Para o feedback
		var editedCustomer = $("#EditedCustomer").val();
		if (editedCustomer != "") {
			$("#EditedCustomer").val("");
			$("#Code").val(editedCustomer);
			$("#search").click();
		}
	});
});

function InsertTrader(code) {
	window.location = "/NonResidentTraders/Registration/" + code;
}
