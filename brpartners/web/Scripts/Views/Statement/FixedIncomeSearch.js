﻿/// <reference path="../../References.js" />

$(function () {
    $(document).ready(function () {

        var context = $("#frmFixedIncomeSearch");

        $("#txtStart", context).mask("99/99/9999");
        $("#txtEnd", context).mask("99/99/9999");

        $("#lnkSearch", context).click(function () {
            var start = $("#txtStart", context).val();
            var end = $("#txtEnd", context).val();
            var cpfCnpj = $("#hdnCpfCnpj", context).val();

            $("#feedbackError", context).hide();

            if (start == "" || end == "")
            {
                $("#feedbackError", context).html("Por favor, preencha as datas.");
                $("#feedbackError", context).show();
                return;
            }

            // var parameters = "cpfCnpj=" + "45631743353" + "&start=" + start + "&end=" + end;
            // window.open("/Statement/FixedIncome/" + parameters);

            $.ajax({
                url: "/Statement/FixedIncome",
                type: "POST",
                dataType: "html",
                async: false,
                data: { cpfCnpj: cpfCnpj, start: start, end: end },
                success: function (dataFI) {
                    // console.log(dataFI);
                    var w = window.open(dataFI);
                    w.document.write(dataFI);
                },
                error: function (dataFI) {
                    alert('Error');
                },
            });
        });
    });
});
