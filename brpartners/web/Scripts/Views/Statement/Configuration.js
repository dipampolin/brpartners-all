﻿/// <reference path="../../References.js" />

$(function () {
    $(document).ready(function () {

        var context = $("#frmConfiguration");
        var automatic = false;

        $("input[name='rdbAutomaticSending']", context).change(function () {
            var v = $(this).val();
            if (v == "true")
            {
                // $(".chkConfiguration", context).attr('disabled', false);
                automatic = true;
            }
            else
            {
                // $(".chkConfiguration", context).attr('checked', false);
                // $(".chkConfiguration", context).attr('disabled', true);
                automatic = false;
            }
        });

        $("#lnkSave", context).click(function () {
            // var automatic = $("input[name='rdbAutomaticSending']", context).val();

            var clientId = $("#hdnClientId", context).val();

            var list = "";
            $("input[name='chkConfiguration']", context).each(function () {
                var t = $(this);
                if ($(this).is(':checked'))
                {
                    list += (list == "" ? $(this).val() : "-" + $(this).val());
                }
            });

            $("#feedbackError", context).hide();

            /*
            if (month == "" || year == "" || type == "")
            {
                $("#feedbackError", context).html("Por favor, preencha todos os campos do formulário.");
                $("#feedbackError", context).show();
                return;
            }*/

            $.ajax({
                url: "/Statement/Configuration",
                type: "POST",
                dataType: "html",
                async: false,
                data: { clientId: clientId, automatic: automatic, statements: list },
                success: function (dataFI) {
                    Modal.Close();
                    $("#formSC").submit();
                },
                error: function (dataFI) {
                    // alert('Error');
                },
            });
        });
    });
});
