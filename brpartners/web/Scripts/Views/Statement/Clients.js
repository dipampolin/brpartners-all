﻿
$(document).ready(function () {

    $('tr:odd').css('background', '#EEE');



    $("a[name='lnkFixedIncome']").click(function (event) {
        var cpfCnpj = event.target.id;
        //console.log(cpfCnpj);
        commonHelper.OpenModal("/Statement/FixedIncomeSearch/?cpfCnpj=" + cpfCnpj, 200, ".content", "");
        $("#MsgBox").center();
        $('select').styleInputs();
    });

    $("a[name='lnkNdf']").click(function (event) {
        var cpfCnpj = event.target.id;
        //console.log(cpfCnpj);
        commonHelper.OpenModal("/Statement/NdfSearch/?cpfCnpj=" + cpfCnpj, 200, ".content", "");
        $("#MsgBox").center();
        $('select').styleInputs();
    });

    $("a[name='lnkSwap']").click(function (event) {
        var cpfCnpj = event.target.id;
        //console.log(cpfCnpj);
        commonHelper.OpenModal("/Statement/SwapSearch/?cpfCnpj=" + cpfCnpj, 200, ".content", "");
        $("#MsgBox").center();
        $('select').styleInputs();
    });

    $("a[name='lnkConfiguration']").click(function (event) {
        var id = event.target.id;
        //console.log(cpfCnpj);
        commonHelper.OpenModal("/Statement/Configuration/?clientId=" + id, 200, ".content", "");
        $("#MsgBox").center();
        $('select').styleInputs();
    });

    $("a[name='lnkSendStatements']").click(function (event) {
       
        $("#loadingArea").show();

        $.ajax({
            url: "/Statement/SendStatements",
            type: "POST",
            dataType: "json",
            async: true,
            // data: { cpfCnpj: cpfCnpj, start: start, end: end },
            success: function () {
                // console.log(dataFI);
               $("#loadingArea").hide();
            },
            error: function () {
               $("#loadingArea").hide();
            },
        });

    });

});
