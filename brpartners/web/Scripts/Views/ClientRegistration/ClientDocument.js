﻿var idDoc;

$(function() {
    $(document).ready(function () {
        $("#btnNewDocument").click(function () {
            $("#frmDocumentModal").trigger('reset');
            $('#IdDocument').val('');
            $('#documentContent').show();
        });
    });
});

function closeModal(){
    $('#documentContent').hide();
}

function editDocument(value) {
    $.ajax({
        url: '/ClientRegistration/GetDocumentById/',
        data: { idDocument: value },
        type: 'GET',
        datatype: 'json',
        success: function (response) {
            $('#IdDocument').val(response.Id);
            $('#chkPhysicalPerson').attr('checked', response.PhysicalPerson == true ? 'checked' : '');
            $('#chkLegalPerson').attr('checked', response.LegalPerson == true ? 'checked' : '');
            $('#chkFunds').attr('checked', response.Funds == true ? 'checked' : '');
            $('#DocumentName').val(response.NameFile);
            $('#Instruction').val(response.Instruction);
            $('#Validatetime').val(response.IdValidation);
            $('#Required').attr('checked', response.Required == true ? 'checked' : '');

            $('#documentContent').show();
        }
    });
}

function removeDocument(value) {
    idDoc = value;
    $('#dialog-form-remove-doc').show();   
}

function submitInformation() {
    $.ajax({
        url: '/ClientRegistration/RemoveDocumentById/',
        data: { idDocument: idDoc },
        type: 'GET',
        datatype: 'json',
        success: function (response) {

            $(".tr-row").map(function () {
                if (this.id == idDoc) {
                    this.classList.add('fadeOut');

                    setTimeout(function() {
                        $(this).remove();
                    }, 500);
        
                    return false;
                }                              
            });

            closeModalConfirmation();
        }
    });
}

function closeModalConfirmation(){
    $('#dialog-form-remove-doc').hide(); 
}