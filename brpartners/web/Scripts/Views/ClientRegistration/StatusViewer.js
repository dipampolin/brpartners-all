﻿var oTableStatus = null;

var objStatusFilter = {
    "bAutoWidth": false,
    "bPaginate": true
            , "bDestroy": true
            , "sPaginationType": "full_numbers"
            , "sDom": 'trip'
            , "oLanguage": {
                "sLengthMenu": "Exibindo _MENU_ registros",
                "sZeroRecords": "Nenhum registro encontrado.",
                "sInfo": "Encontramos _TOTAL_ registros",
                "sInfoEmpty": "Nenhum registro encontrado",
                "sInfoFiltered": "(Filtrando _MAX_ registros)",
                "sSearch": "Buscar:",
                "sProcessing": "Processando...",
                "oPaginate": {
                    "sFirst": "Primeiro",
                    "sPrevious": "Anterior",
                    "sNext": "Próximo",
                    "sLast": "Último"
                }
            }
            , "bProcessing": true
            , "bServerSide": true
            , "sAjaxSource": "../ClientRegistration/StatusViewerFilter"
            , "fnServerData": function (sSource, aoData, fnCallback) {

                var context = $('#StatusViewerContent');

                if (aoData != null && aoData != undefined) {
                    aoData.push({ "name": "txtAssessorFilter", "value": $('#txtAssessorFilter', context).val() });
                    aoData.push({ "name": "txtClientFilter", "value": $('#txtClientFilter', context).val() });
                    aoData.push({ "name": "txtStartDate", "value": $('#txtStartDate', context).val() });
                    aoData.push({ "name": "txtEndDate", "value": $('#txtEndDate', context).val() });
                    aoData.push({ "name": "chkbmf", "value": $('#chkbmf', context).is(":checked") });
                    aoData.push({ "name": "chkbovespa", "value": $('#chkbovespa', context).is(":checked") });
                }
                $.ajax({
                    "cache": false,
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "beforeSend": function () {
                        $("#table-area").hide();
                        $("#loadingArea").show();
                        $("#emptyArea").hide();
                    },
                    "success": function (data) {
                        fnCallback(data);

                        /*if (data.aaData == "") {
                        $("#feedbackError").html("Ocorreu um erro ao realizar consulta.").show();
                        $("#loadingArea").hide();
                        $("#table-area").hide();
                        }
                        else*/
                        if (data.iTotalRecords == 0) {
                            $("#table-area").hide();
                            $('#emptyArea').html('Sua pesquisa não encontrou resultados válidos. Tente novamente.').show();
                        }
                        else {
                            $('#emptyArea').html('').hide();
                        }
                    }
                });
            }
            , "fnInitComplete": function () {
                oTableStatus.fnAdjustColumnSizing();
            }
            , fnDrawCallback: function () {
                $("#checkAll").attr("checked", false);

                $("#loadingArea").hide();
                $("#table-area").show();

                var totalRecords = this.fnSettings().fnRecordsDisplay();

                if (totalRecords <= 0) {
                    $("#lnkExcel").hide();
                    $("#lnkPdf").hide();
                    $("#dvExportar").hide();


                }
                else {
                    $("#lnkExcel").show();
                    $("#lnkPdf").show();
                    $("#dvExportar").show();

                    StatusViewerMethods.enableExportButtons();
                }

                var totalPerPage = this.fnSettings()._iDisplayLength;

                var totalPages = Math.ceil(totalRecords / totalPerPage);

                var currentIndex = parseInt(this.fnSettings()._iDisplayStart);
                var currentPage = totalPages > 0 ? Math.ceil(1 + (currentIndex / totalPerPage)) : 0;

                $("#tblStatusViewer_info").html("Exibindo " + currentPage + " de " + totalPages);

                if (totalRecords > 0 && totalPages > 1) {
                    $("#tblStatusViewer_info").css("display", "");
                    $("#tblStatusViewer_length").css("display", "");
                    $("#tblStatusViewer_paginate").css("display", "");
                }
                else {
                    $("#tblStatusViewer_info").css("display", "none");
                    $("#tblStatusViewer_length").css("display", "none");
                    $("#tblStatusViewer_paginate").css("display", "none");
                }

            }
            , aoColumns: [
                { sClass: "centralizado"},
                null,
                null,
                null,
                null,
                { sClass: "centralizado" },
                { sClass: "centralizado" }
            ]
};

var StatusViewerMethods = {
    enableExportButtons: function () {
        var data = $("#tblStatusViewer tbody tr");
        var emptyData = (data.length == 1 && data[0].lastChild.className == "dataTables_empty") ? true : false;
        var excelLink = $("#lnkExcel");
        var pdfLink = $("#lnkPdf");

        var fnExportFilter = function (isPDF) {
            $.ajax({
                type: "POST",
                cache: false,
                async: false,
                url: "../ClientRegistration/StatusViewerFilterOptions",
                data: {
                    txtAssessorFilter: $('#txtAssessorFilter').val(),
                    txtClientFilter: $('#txtClientFilter').val(),
                    txtStartDate: $('#txtStartDate').val(),
                    txtEndDate: $('#txtEndDate').val(),
                    chkbmf: $('#chkbmf').is(":checked"),
                    chkbovespa: $('#chkbovespa').is(":checked"),
                    iSortCol_0: oTableStatus.fnSettings().aaSorting[0][0],
                    sSortDir_0: oTableStatus.fnSettings().aaSorting[0][1]
                },
                beforeSend: function () {
                    $("#feedbackError").remove();
                },
                success: function (data) {
                    if (data)
                        window.location = "/ClientRegistration/StatusViewerToFile/?isPdf=" + isPDF;
                    else {
                        $("#emptyArea").before("<div id='feedbackError' class='erro'>Ocorreu um erro na hora da exportação.</div>");
                    }
                }
            });
        };

        excelLink.unbind("click");
        excelLink.bind("click", function () {
            fnExportFilter("false");
        });

        pdfLink.unbind("click");
        pdfLink.bind("click", function () {
            fnExportFilter("true");
        });
    },
    ValidateFilterFields: function () {
        var messageError = "";
        if (!validateHelper.ValidateList($("#txtAssessorFilter").val()))
            messageError += "<li>Faixa de assessores inválida.</li>";

        if (!validateHelper.ValidateList($("#txtClientFilter").val()))
            messageError += "<li>Faixa de clientes inválida.</li>";

        if ($("#txtStartDate").val()) {
            if (isNaN(Date.parse($("#txtStartDate").val()))) {
                messageError += "<li>Data inicial inválida.</li>";
            }
        }

        if ($("#txtEndDate").val() != "") {
            if (isNaN(Date.parse($("#txtEndDate").val()))) {
                messageError += "<li>Data final inválida.</li>";
            }
        }

        if (messageError.length > 0)
            messageError = "<ul>" + messageError + "</ul>";

        return { Result: (messageError == ""), Message: messageError }
    }
};

$(function () {
    if (oTableStatus != null)
        oTableStatus = null;
    oTableStatus = $("#tblStatusViewer").dataTable(objStatusFilter);

    var context = $('#StatusViewerContent');

    $("#txtStartDate", context).setMask("99/99/9999").datepicker().datepicker($.datepicker.regional['pt-BR']);
    $("#txtEndDate", context).setMask("99/99/9999").datepicker().datepicker($.datepicker.regional['pt-BR']);
    $.datepicker.setDefaults($.datepicker.regional['pt-BR']);

    $("#StatusViewerFilter").click(function () {
        $(context).slideToggle('fast');
        $('select').styleInputs();
        $(this).toggleClass('aberto');
    });


    $(context).bind('keydown', function (event) {
        if (event.which == 13) {
            $("#lnkSubmitStatusViewerFilter").click();
        }
    });

    $("#lnkSubmitStatusViewerFilter").unbind("click");
    $("#lnkSubmitStatusViewerFilter").bind("click", function () {

        var feedback = StatusViewerMethods.ValidateFilterFields();

        if (feedback.Result) {
            $("#filterFeedbackError").html("").hide();
            $("#StatusViewerContent").hide();
            $("#loadingArea").show();
            $("#table-area").hide();
            $("#feedback").hide().find("span").html('');
            $("#feedbackError").hide().find("span").html('');

            if (oTableStatus != null)
                oTableStatus.fnDestroy();

            oTableStatus = $("#tblStatusViewer").dataTable(objStatusFilter);

        }
        else {
            $("#filterFeedbackError").html(feedback.Message).show();
        }
    });
});
