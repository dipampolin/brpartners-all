﻿var oTableStatus = null;

var objStatusFilter = {
    "bAutoWidth": false,
    "bPaginate": true
    , "bDestroy": true
    , "sPaginationType": "full_numbers"
    , "sDom": 'trip'
    , "oLanguage": {
        "sLengthMenu": "Exibindo _MENU_ registros",
        "sZeroRecords": "Nenhum registro encontrado.",
        "sInfo": "Encontramos _TOTAL_ registros",
        "sInfoEmpty": "Nenhum registro encontrado",
        "sInfoFiltered": "(Filtrando _MAX_ registros)",
        "sSearch": "Buscar:",
        "sProcessing": "Processando...",
        "oPaginate": {
            "sFirst": "Primeiro",
            "sPrevious": "Anterior",
            "sNext": "Próximo",
            "sLast": "Último"
        }
    }
    , "bProcessing": true
    , "bServerSide": true
    , "sAjaxSource": "../ClientRegistration/OverdueAndDueFilter"
    , "fnServerData": function (sSource, aoData, fnCallback) {

        var context = $('#OverdueAndDueContent');

        if (aoData != null && aoData != undefined) {
            aoData.push({ "name": "txtClient", "value": $('#txtClient', context).val() });
            aoData.push({ "name": "txtAssessorFilter", "value": $('#txtAssessorFilter', context).val() });
            aoData.push({ "name": "ddlSituation", "value": $('#ddlSituation', context).val() });
            aoData.push({ "name": "chkActive", "value": $('#chkActive', context).is(":checked") });
            aoData.push({ "name": "chkInactive", "value": $('#chkInactive', context).is(":checked") });
            aoData.push({ "name": "chkBlocked", "value": $('#chkBlocked', context).is(":checked") });
            aoData.push({ "name": "radSpecificRange", "value": $('#radSpecificRange', context).is(":checked") });
            aoData.push({ "name": "txtDueDateStart", "value": $('#txtDueDateStart', context).val() });
            aoData.push({ "name": "txtDueDateEnd", "value": $('#txtDueDateEnd', context).val() });
        }
        $.ajax({
            "cache": false,
            "dataType": 'json',
            "type": "POST",
            "url": sSource,
            "data": aoData,
            "beforeSend": function () {
                $("#table-area").hide();
                $("#loadingArea").show();
                $("#emptyArea").hide();
            },
            "success": function (data) {
                fnCallback(data);
                if (data.iTotalRecords == 0) {
                    $("#table-area").hide();
                    $(".ferramentas").hide();

                    if ($('#txtClient').val() != '' || $('#txtAssessorFilter').val() != '') {
                        $('#emptyArea').html('Não há cadastros vencidos ou a vencer para este cliente/assessor.').show();
                    }
                    else {
                        $('#emptyArea').html('Sua pesquisa não encontrou resultados válidos. Tente novamente.').show();
                    }
                }
                else {
                    $(".ferramentas").show();
                    $('#emptyArea').html('').hide();
                }
            }
        });
    }
    , "fnInitComplete": function () {
        oTableStatus.fnAdjustColumnSizing();
    }
    , fnDrawCallback: function () {
        $("#checkAll").attr("checked", false);

        $("#loadingArea").hide();
        $("#table-area").show();

        var totalRecords = this.fnSettings().fnRecordsDisplay();

        if (totalRecords <= 0) {
            $("#lnkExcel").hide();
            $("#lnkPdf").hide();
            $("#dvExportar").hide();
        }
        else {
            $("#lnkExcel").show();
            $("#lnkPdf").show();
            $("#dvExportar").show();
            OverdueAndDueMethods.enableExportButtons();
        }

        var totalPerPage = this.fnSettings()._iDisplayLength;

        var totalPages = Math.ceil(totalRecords / totalPerPage);

        var currentIndex = parseInt(this.fnSettings()._iDisplayStart);
        var currentPage = totalPages > 0 ? Math.ceil(1 + (currentIndex / totalPerPage)) : 0;

        $("#tblOverdueAndDue_info").html("Exibindo " + currentPage + " de " + totalPages);

        if (totalRecords > 0 && totalPages > 1) {
            $("#tblOverdueAndDue_info").css("display", "");
            $("#tblOverdueAndDue_length").css("display", "");
            $("#tblOverdueAndDue_paginate").css("display", "");
        }
        else {
            $("#tblOverdueAndDue_info").css("display", "none");
            $("#tblOverdueAndDue_length").css("display", "none");
            $("#tblOverdueAndDue_paginate").css("display", "none");
        }

        OverdueAndDueMethods.RegistrationLink();
    }
    , aoColumns: [
        null,
        null,
        null,
        null,
        null,
        null,
		{ sClass: "centralizado" },
		null,
		null
    ]
};

var OverdueAndDueMethods = {
    enableExportButtons: function () {
        var data = $("#tblOverdueAndDue tbody tr");
        var emptyData = (data.length == 1 && data[0].lastChild.className == "dataTables_empty") ? true : false;
        var excelLink = $("#lnkExcel");
        var pdfLink = $("#lnkPdf");

        var fnExportFilter = function (isPDF) {
            $.ajax({
                type: "POST",
                cache: false,
                async: false,
                url: "../ClientRegistration/OverdueAndDueFilterOptions",
                data: {
                    txtClient: $('#txtClient').val(),
                    txtAssessorFilter: $('#txtAssessorFilter').val(),
                    ddlSituation: $('#ddlSituation').val(),
                    chkActive: $('#chkActive').is(":checked"),
                    chkInactive: $('#chkInactive').is(":checked"),
                    chkBlocked: $('#chkBlocked').is(":checked"),
                    radSpecificRange: $('#radSpecificRange').is(":checked"),
                    txtDueDateStart: $('#txtDueDateStart').val(),
                    txtDueDateEnd: $('#txtDueDateEnd').val(),

                    iSortCol_0: oTableStatus.fnSettings().aaSorting[0][0],
                    sSortDir_0: oTableStatus.fnSettings().aaSorting[0][1]
                },
                beforeSend: function () {
                    $("#feedbackError").remove();
                },
                success: function (data) {
                    if (data)
                        window.location = "/ClientRegistration/OverdueAndDueToFile/?isPdf=" + isPDF;
                    else {
                        $("#emptyArea").before("<div id='feedbackError' class='erro'>Ocorreu um erro ao exportar.</div>");
                    }
                }
            });
        };

        excelLink.unbind("click");
        excelLink.bind("click", function () {
            fnExportFilter("false");
        });

        pdfLink.unbind("click");
        pdfLink.bind("click", function () {
            fnExportFilter("true");
        });
    },
    ValidateFilterFields: function () {
        var messageError = "";
        if (!validateHelper.ValidateList($("#txtAssessorFilter").val()))
            messageError += "<li>Faixa de assessores inválida.</li>";

        if (!$('#chkActive').is(":checked")
			&& !$('#chkInactive').is(":checked")
			&& !$('#chkBlocked').is(":checked"))
            messageError += "<li>É necessário selecionar ao menos um status Sinacor.</li>";

        if (messageError.length > 0)
            messageError = "<ul>" + messageError + "</ul>";

        return { Result: (messageError == ""), Message: messageError }
    },
    RegistrationLink: function () {
        $(".lnkRegistration").unbind("click").bind("click", function () {
            var id = $(this).attr("rel");
            $.ajax({
                type: "POST",
                cache: false,
                url: "../ClientRegistration/RegistrationFilter",
                data: { clientCode: id },
                success: function (data) {
                    if (data.Result)
                        window.location = data.Location;
                }
            });
        });
    }
};

$(function () {
	if (oTableStatus != null)
		oTableStatus = null;
	objStatusFilter.aaSorting = [[6, 'asc']];
	oTableStatus = $("#tblOverdueAndDue").dataTable(objStatusFilter);

	var context = $('#OverdueAndDueContent');

	$("#txtDueDateStart").setMask("99/99/9999").datepicker().datepicker($.datepicker.regional['pt-BR']);
	$("#txtDueDateEnd").setMask("99/99/9999").datepicker().datepicker($.datepicker.regional['pt-BR']);
	$.datepicker.setDefaults($.datepicker.regional['pt-BR']);

	$("#OverdueAndDueFilter").click(function () {
		$(context).slideToggle('fast');
		$('select').styleInputs();
		$(this).toggleClass('aberto');
	});

	$("#divRange").hide();
	$("#radSpecificRange").click(function () {
		$("#divRange").show();
	});
	$("#radAnyRange").click(function () {
		$("#divRange").hide();
	});

	$(context).bind('keydown', function (event) {
		if (event.which == 13) {
			$("#lnkSubmitOverdueAndDueFilter").click();
		}
	});

	$("#lnkSubmitOverdueAndDueFilter").unbind("click");
	$("#lnkSubmitOverdueAndDueFilter").bind("click", function () {

		var feedback = OverdueAndDueMethods.ValidateFilterFields();

		if (feedback.Result) {
			$("#filterFeedbackError").html("").hide();
			$("#OverdueAndDueContent").hide();
			$("#loadingArea").show();
			$("#table-area").hide();
			$("#feedback").hide().find("span").html('');
			$("#feedbackError").hide().find("span").html('');

			if (oTableStatus != null)
				oTableStatus.fnDestroy();

			oTableStatus = $("#tblOverdueAndDue").dataTable(objStatusFilter);

		}
		else {
			$("#filterFeedbackError").html(feedback.Message).show();
		}
	});
});
