﻿function ValidateFilterFields() {
    var messageError = "";

    if (!validateHelper.ValidateList($("#txtAssessorFilter").val()))
        messageError += "<li>Faixa de assessores inválida.</li>";

    if (!validateHelper.ValidateList($("#txtClientFilter").val()))
        messageError += "<li>Faixa de clientes inválida.</li>";

    if (messageError.length > 0)
        messageError = "<ul>" + messageError + "</ul>";

    return { Result: (messageError == ""), Message: messageError }
}

$(function () {
	var context = $('#GuaranteeFilterContent');

	$("#GuaranteeFilter").click(function () {
		$(context).slideToggle('fast');
		$('select').styleInputs();
		$(this).toggleClass('aberto');
	});

	if ($("#feedbackError") != null && $("#feedbackError").css("display") == 'block')
		$('#emptyArea').hide();
	else
		$('#emptyArea').show();

	$(context).bind('keydown', function (event) {
		if (event.which == 13) {
			$("#lnkSubmitGuaranteeFilter").click();
		}
	});

	$("#lnkSubmitGuaranteeFilter").unbind("click");
	$("#lnkSubmitGuaranteeFilter").bind("click", function () {

		$("#feedback").hide().find("span").html('');
		$("#feedbackError").hide().find("span").html('');

		var valid = ValidateFilterFields();

		if (valid.Result) {
			$(context).slideToggle('fast');
			$(this).toggleClass('fechado');
			$("#filterFeedbackError").html("").hide();
			$("#GuaranteeFilterContent").hide();
			$("#emptyArea").hide();
			$("#loadingArea").show();
			$("#tableArea").hide();
			$("#dvExportar").hide();

			$.ajax({
				type: "POST",
				cache: false,
				url: "../Guarantee/GuaranteeFilter",
				data: {
					"txtAssessorFilter": $("#txtAssessorFilter").val(),
					"txtClientFilter": $("#txtClientFilter").val(),
					"radActivitType": ($("#radBovespa").is(":checked") ? 1 : 2)
				},
				dataType: 'html',
				beforeSend: function () { $("#tableArea").html(''); },
				success: function (data) {
					$("#tableArea").html(data).show();
					$("#loadingArea").hide();
					if ($('#tbGuaranteeBalance').length > 0) {
						$("#dvExportar").show();
					}
					else {
						$("#dvExportar").hide();
					}
				},
				error: function () {
					$("#tableArea").html("<div class='erro' style='margin: 0px'>Ocorreu um erro inesperado.</span>").show();
					$("#loadingArea").hide();
				}
			});
		}
		else {
			$("#filterFeedbackError").html(valid.Message).show();
		}
	});
});

function BindNavegation() {
	$(".lnk-navegacao").unbind("click");
	$(".lnk-navegacao").bind("click", function () {

		$("#loadingArea").show();
		$("#tableArea").hide();
		$("#dvExportar").hide();

		var valid = ValidateFilterFields();

		if (valid.Result) {
			$.ajax({
				type: "POST",
				cache: false,
				url: "../Guarantee/GuaranteeFilter",
				data: {
					"txtAssessorFilter": $("#txtAssessorFilter").val(),
					"txtClientFilter": $("#txtClientFilter").val(),
					"radActivitType": ($("#radBovespa").is(":checked") ? 1 : 2),
					"acao": $(this).attr("rel")
				},
				dataType: 'html',
				beforeSend: function () { $("#tableArea").html(''); },
				success: function (data) {
					$("#tableArea").html(data).show();
					$("#loadingArea").hide();
					if ($('#tbGuaranteeBalance').length > 0) {
						$("#dvExportar").show();
					}
					else {
						$("#dvExportar").hide();
					}
				},
				error: function () {
					$("#tableArea").html("<div class='erro' style='margin: 0px'>Ocorreu um erro inesperado.</span>").show();
					$("#loadingArea").hide();
				}
			});
		}
		else {
			$("#filterFeedbackError").html(valid.Message).show();
		}
	});

	$(".ico-detalhes").bind("click", function () {
		$('#trStock').toggle();
		if ($(this).is(".aberto")) {
			$(this).removeClass("aberto").addClass("fechado");
		}
		else {
			$(this).addClass("aberto").removeClass("fechado");
		}
	});
}