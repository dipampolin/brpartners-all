﻿$(document).ready(function () {

    var context = $("#frmTermLotRequest");

    $.mask.masks.stockQuantity = { mask: '999.999.999', type: 'reverse', defaultValue: '0' };

    $("[id^=txtTerm]", context).blur(function () {
        var index = parseInt(this.id.replace("txtTerm", ""));
        var totalQuantity = parseInt($("#totalQuantity" + index, context).html().replace(/\./g, ''));
        var settleQuantity = parseInt(this.value.replace(/\./g, ''));
        if (totalQuantity < settleQuantity)
            $(this).val("0");
    }).setMask();

    $("[id^=ddlTerm]", context).change(function () {
        var daysToAdd = 3;
        var index = parseInt(this.id.replace("ddlTerm", ""));

        switch (this.value) {
            case "11":
                daysToAdd = 1;
                break;
            case "13":
                daysToAdd = 2;
                break;
        }
        dateHelper.GetNextBusinessDay("", "hdfBusinessDate" + index, daysToAdd, $("#frmTermRequest"));
    });

})

function DoRequest() {

    var form = $("#frmTermLotRequest").serialize();
    $.post("/Term/SaveLotRequest", form, function (data) {
        if (data.success) {
            $("span", "#feedback").html(data.message);
            $("#feedback").show();
            ReloadResults();
        }
        else {
            $("#FeedbackError").html(data.message);
            $("#FeedbackError").show();
        }
        Modal.Close();
    }, "json");

    return false;
}

function ValidateFields(context) {
    var feedback = "";

    var blankQuantities = 0;

    $("[id^=txtTerm]", context).each(function () {
        var index = parseInt(this.id.replace("txtTerm", ""));
        if (this.value == "0") {
            blankQuantities++;
        }
    });

    if (blankQuantities > 0) {
        feedback = blankQuantities == 1 ? "1 quantidade não preenchida." : (blankQuantities + " quantidades não preenchidas.");
    }
    return feedback;
}
