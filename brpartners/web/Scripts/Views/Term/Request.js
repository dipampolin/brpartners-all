﻿var stockIndexes = [1];
var currentStockIndex = 1;

$(document).ready(function () {

    var context = $("#frmTermRequest");

    $.mask.masks.stockQuantity = { mask: '999.999.999', type: 'reverse', defaultValue: '0' };
    $.mask.masks.stockCode = { mask: "*******" };

    $("#txtStartDate", context).setMask("99/99/9999").datepicker().datepicker($.datepicker.regional['pt-BR']);
    $("#txtEndDate", context).setMask("99/99/9999").datepicker().datepicker($.datepicker.regional['pt-BR']);

    var id = parseInt($("#hdfID", context).val());

    if (id > 0) {
        $("[id^=txtTerm]", context).blur(function () {
            var index = parseInt(this.id.replace("txtTerm", ""));
            var totalQuantity = parseInt($("#totalQuantity" + index, context).html().replace(/\./g, ''));
            var settleQuantity = parseInt(this.value.replace(/\./g, ''));
            if (totalQuantity < settleQuantity)
                $(this).val("0");
        }).styleInputs().setMask();
    }
    else {
        $("#lnkSearchTermRequest", context).click(function () {

            var feedback = ValidateFields(context);
            if (feedback != "") {
                $("#requestFeedbackError", context).html(feedback).show();
                this.location = "#feedBackAnchor";
            }
            else {
                $("#emptyArea").hide();
                $("#tableArea").hide();
                $("#loadingArea").show();

                $("#requestFeedbackError", context).hide();

                $.ajax({
                    cache: false,
                    type: "POST",
                    dataType: "json",
                    async: true,
                    url: "/Term/SearchClientTerms",
                    data: { ClientCode: $("#txtClient", context).val(), FilterId: $("#ddlCompany", context).val(), FilterText: $("#txtCompany", context).val(), StartDate: $("#txtStartDate", context).val(), EndDate: $("#txtEndDate", context).val() },
                    success: function (data) {
                        $("#loadingArea").hide();

                        var emptyData = (data.tBody == null || data.tBody.length == 0);

                        if (emptyData) {
                            $("#emptyArea").show();
                            $("#tableArea").hide();
                        }
                        else {
                            $("#tbTerms").find("tbody").html(data.tBody);
                            $("#tableArea").show();
                            $("#emptyArea").hide();

                            var tableContext = $("#tableArea", context);
                            $('select', tableContext).styleInputs();
                            $('input', tableContext).styleInputs().setMask();

                            $("#MsgBox").center();

                            $("[id^=ddlTerm]", context).change(function () {
                                var daysToAdd = 3;
                                var index = parseInt(this.id.replace("ddlTerm", ""));

                                switch (this.value) {
                                    case "11":
                                        daysToAdd = 1;
                                        break;
                                    case "13":
                                        daysToAdd = 2;
                                        break;
                                }

                                dateHelper.GetNextBusinessDay("", "hdfBusinessDate" + index, daysToAdd, $("#frmTermRequest"));
                            });

                            $("[id^=txtTerm]", context).blur(function () {
                                var index = parseInt(this.id.replace("txtTerm", ""));
                                var totalQuantity = parseInt($("#totalQuantity" + index, context).html().replace(/\./g, ''));
                                var settleQuantity = parseInt(this.value.replace(/\./g, ''));
                                if (totalQuantity < settleQuantity)
                                    $(this).val("0");
                            });
                        }
                    }
                });
            }

            return false;
        });

        $("#ddlCompany", context).change(function () {
            switch (this.value) {
                case "P":
                case "E":
                    $("#txtCompany").val("").css({ visibility: "visible" });
                    break;
                case "T":
                    $("#txtCompany").val("").css({ visibility: "hidden" });
                    break;
            }
        });
    }

    $("#txtClient", context).blur(function () {
        commomDataHelper.GetClientName($(this).val(), "lblClientName", context, "txtClient");
        $("#requestFeedbackError", context).hide();
    }).setMask();
})


function DoRequest() {
    var form = $("#frmTermRequest").serialize();
    $.post("/Term/SaveRequest", form, function (data) {
        if (data.success) {
            $("span", "#feedback").html(data.message);
            $("#feedback").show();
            ReloadResults();
        }
        else {
            $("#FeedbackError").html(data.message);
            $("#FeedbackError").show();
        }
        Modal.Close();
    }, "json");

    return false;
}


function ValidateFields(context) {
    var feedback = "";

//    var blankQuantities = 0;

//    $("[id^=txtTerm]", context).each(function () {
//        var index = parseInt(this.id.replace("txtTerm", ""));
//        if (this.value == "0") {
//            blankQuantities++;
//        }
//    });

//    if (blankQuantities > 0) {
//        feedback = blankQuantities == 1 ? "1 quantidade não preenchida." : (blankQuantities + " quantidades não preenchidas.");
    //    }

    if ($("#txtClient", context).val() == "") {
        feedback += "<li>Preencha o campo cliente.</li>";
    }

    if ($("#ddlCompany", context).val() != "T" && $("#txtCompany", context).val() == "") {
        feedback += "<li>Preencha o campo papel.</li>";
    }

    var invalidDate = false;
    $("input[id*='Date']", context).each(function () {
        if ($(this).val() != "" && !invalidDate) {
            if (!DateValidate($(this).val())) {
                invalidDate = true;
                feedback += "<li>Data inválida.</li>";
            }
        }
    });

    if (!invalidDate) {
        if (!dateHelper.CheckPeriod("#txtStartDate", "#txtEndDate", context))
            feedback += "<li>Periodo de data inválido.</li>";
    }

    if (feedback.length > 0)
        feedback = "<ul>" + feedback + "</ul>";

    return feedback;
}

function DateValidate(date) {
    var reDate = /^((0[1-9]|[12]\d)\/(0[1-9]|1[0-2])|30\/(0[13-9]|1[0-2])|31\/(0[13578]|1[02]))\/\d{4}$/;
    if (reDate.test(date) || (date == '')) {
        return true
    } else {
        return false;
    }
}