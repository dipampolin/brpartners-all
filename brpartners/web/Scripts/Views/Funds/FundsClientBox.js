﻿$(function () {

    $.format.locale({
        number: {
            groupingSeparator: '.',
            decimalSeparator: ','
        }
    });

    $("#txtClientCode").unbind("blur").bind("blur", function () {
        //if ($("#txtClientCode").val() != "") {
            $("#frmFindClient").submit();
        //}
    });

    $(".ico-hist").unbind("click").bind("click", function () {
        var url = "/Funds/FundsHistory/";
        commonHelper.OpenModalPost(url, 671, ".content", "");
        $("#MsgBox").center();
    });

    $("#frmFindClient").ajaxForm({
        beforeSend: function () {
            $("#feedback").hide().find("span").html('');
            $("#feedbackError").hide().find("span").html('');
        },
        success: function (data) {
            $("#loadingArea").hide();

            if (data.Result) {
                $("#feedbackError").show().find("span").append(data.Message);
            }

        }
    });
});