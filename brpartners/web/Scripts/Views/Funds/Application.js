﻿var ApplicationFunctions = {
    ValidateApplicationTime: function (startTime, endTime) {
        if (startTime == "" || startTime == null || startTime == undefined || endTime == "" || endTime == null || endTime == undefined)
            return false;

        var startTimeCnv = Date.parse(startTime);

        var endTimeCnv = Date.parse(endTime);

        var today = Date.today().setTimeToNow();

        if (Date.compare(startTimeCnv, today) == -1)
            if (Date.compare(endTimeCnv, today) == 1) {
                return true;
            }

        return false;
    }
    , ValidateApplication: function () {
        var response = true;
        var availableValue = 0;
        var appValue = parseFloat($("#txtApplicationValue").val().replace(/\.g/, "").replace(/\,g/, "."));
        var fundCode = $("#ddlFunds").val();

        var initialInvestiment = 0;
        var additionalInvestment = 0;
        var userHasApplication = false;
        var userHasAccepted = false;

        $.ajax({
            type: "POST"
            , cache: false
            , async: false
            , url: "/Funds/LoadFundDetails"
            , data: { fundCode: fundCode }
            , success: function (data) {
                if (data != null) {
                    if (data.Result != null) {

                        initialInvestiment = data.Result.InitialInvestment;
                        additionalInvestiment = data.Result.AdditionalInvestment;

                        //ja possui fundo
                        userHasApplication = data.Result.UserHasApplication;

                        //termo de aceite
                        UserHasAccepted = data.Result.UserHasAccepted;

                    }
                }

            }
        });


        $.ajax({
            type: "POST"
            , cache: false
            , async: false
            , url: "../Funds/GetPositionBalance"
            , success: function (data) {
                if (data != null) {
                    if (data.AvailableBalance < appValue) {
                        //erro: não há saldo suficiente - 
                        $("#lblApplication").show().html("O cliente não tem saldo suficiente para aplicar nesse fundo.");
                        response = false;
                    }
                    else if (userHasApplication) {
                        //se possui aplicação, 
                        if (appValue < additionalInvestment) {
                            $("#lblApplication").show().html("A aplicação mínima é de R$ " +
                                $.format.number(additionalInvestiment, "#,##0.00#") + ".");
                            response = false;
                        }
                    }
                    else if (!userHasApplication) {
                        //se não possui aplicação.
                        if (appValue < initialInvestiment) {
                            $("#lblApplication").show().html("A aplicação mínima é de R$ " +
                                $.format.number(initialInvestiment, "#,##0.00#") + ".");
                            response = false;
                        }
                    }
                }
            }
        });

        return { Valid: response, Investment: appValue, FundCode: fundCode, UserHasApplication: userHasApplication, UserHasAccepted: userHasAccepted };
    }
    , ConfirmApplication: function () {
        var request = ApplicationFunctions.ValidateApplication();

        if (request.Valid) {
            var url = "/Funds/ConfirmApplication/";
            commonHelper.OpenModalPost(url, 450, ".content", "", request);
            $("#MsgBox").center();

        }
    }
};

$(function () {

    var context = $("#conteudo-aplicacao")[0];

    $("#txtApplicationValue", context).setMask("decimal");

    var fnDownload = function (documentId, fileName) {
        $(".lnk-document-" + documentId).unbind("click").bind("click", function () {
            $.ajax({
                type: "POST"
                , cache: false
                , url: "../Funds/TestDownload"
                , data: { id: documentId }
                , success: function (data) {
                    if (data) {
                        $("#feedbackError").hide().html('');
                        window.location =
                        "/Funds/DownloadFundDocument?id=" + documentId + "&fileName=" + fileName;
                    }
                    else {
                        $("#feedbackError").show().html('Não foi possível realizar o download do arquivo.');
                    }
                }
            });
        });
    }

    var fnLoadDetails = function (fundCode, context) {
        if (fundCode == "") {
            $("#txtAdditionalInfo").html('');
            $("#ulDocuments").html('');
            $("#divTimeLimit").hide();
            $("#lblApplication").hide().html('');
            $("#txtApplicationValue").val('0,00');
            $("#divApplicationPanel").hide();
            $("#divTermoAceite").hide();
        }
        else {
            $.ajax({
                type: "POST"
                , cache: false
                , url: "/Funds/LoadFundDetails"
                , data: { fundCode: fundCode }
                , beforeSend: function () {
                    $("#txtAdditionalInfo").html('');
                    $("#ulDocuments").html('');
                    $("#lblApplication").hide().html('');
                    $("#txtApplicationValue").val('0,00');
                }
                , success: function (data) {
                    if (data != null) {
                        if (data.Result != null) {
                            var text = "";

                            if (data.Result.InitialInvestment != "") { text = "Aplicação inicial mínima para esse fundo é de R$ " + $.format.number(data.Result.InitialInvestment, "#,##0.00#") + ".<br />"; }
                            if (data.Result.AdditionalInvestment != "") { text = text + " Demais movimentações, mínimo de R$ " + $.format.number(data.Result.AdditionalInvestment, "#,##0.00#") + "."; }
                            $("#txtAdditionalInfo").html(text);

                            for (var i = 0; i < data.Result.Documents.length; i++) {
                                if (data.Result.Documents[i].FileName != "") {
                                    $("#ulDocuments").append($("<li><a class='ico-pdf lnk-txt lnk-document-" + data.Result.Documents[i].Id + "' href='javascript:;'>" + data.Result.Documents[i].DocTypeName + "</a></li>").clone());

                                    fnDownload(data.Result.Documents[i].Id, data.Result.Documents[i].FileName);
                                }
                            }

                            var textTime = "O horário de movimentação é de " + data.Result.MovementStartTime + " às " + data.Result.MovementEndTime + " (dias úteis).";
                            textTime = textTime + (ApplicationFunctions.ValidateApplicationTime(data.Result.MovementStartTime, data.Result.MovementEndTime) ? "" : "<br>Sua aplicação será agendada para o próximo dia útil.");

                            $("#spnTimeLimit").html(textTime);

                            $("#divTimeLimit").show();

                            $("#lnkDetails").unbind("click").bind("click", function () {
                                var url = "/Funds/LoadFund/?fundCode=" + fundCode;
                                commonHelper.OpenModal(url, 450, ".content", "");
                                $("#MsgBox").center();
                            });

                            if (data.Result.UserHasAccepted == true) {
                                $("#divApplicationPanel").show();

                                $("#divTermoAceite").hide();
                            }
                            else {
                                $("#divApplicationPanel").hide();
                                $("#divTermoAceite").show();

                                var b = false;
                                var documentId = ""; var fileName = "";
                                for (var i = 0; i < data.Result.Documents.length; i++) {
                                    if (data.Result.Documents[i].DocTypeId == 1) {
                                        b = true;
                                        var documentId = data.Result.Documents[i].Id;
                                        var fileName = (data.Result.Documents[i].FileName == "" ? "termo_adesao" : data.Result.Documents[i].FileName);
                                    }

                                    if (b) {
                                        $(".link-termo-adesao").html('').append($("<a class='ico-pdf lnk-txt lnk-document-" + documentId + "' href='javascript:;'>Termo de Adesão</a>").clone()).show();
                                        fnDownload(documentId, fileName);

                                        $(".link-termo-adesao").append($("<div class='linha-btn'><a href='javascript:;' class='btn-padrao' id='btnSendMail'><span>Enviar Email</span></a></div>").clone());

                                        $("#btnSendMail").unbind("click").bind("click", function () {
                                            $.ajax({
                                                type: "POST",
                                                cache: false,
                                                url: "../Funds/SendTermMail",
                                                data: { documentId: documentId, fileName: fileName, fundCode: fundCode },
                                                success: function (data) {
                                                    var selector = "";
                                                    if (!data.Result) {
                                                        $("#feedbackError").html(data.Message).show();
                                                    }
                                                    else {
                                                        $("#divSuccessFeedback > span").html(data.Message);
                                                        $("#divSuccessFeedback").show();
                                                    }

                                                }
                                            });
                                        });
                                    }
                                    else {
                                        $(".link-termo-adesao").html('');
                                    }
                                }

                            }
                        }
                    }
                    else { $("#divApplicationPanel").hide(); }
                }
            });
        }
    };

    fnLoadDetails($("#ddlFunds").val(), context);


    $("#ddlFunds", context).unbind("change").bind("change", function () {
        fnLoadDetails($(this).val(), context);
    });

    $("#btnApplication").unbind("click").bind("click", function () {
        ApplicationFunctions.ConfirmApplication();
    });
});