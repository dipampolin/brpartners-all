﻿/// <reference path="../../References.js" />

var oTableBalance = null;

var fnDrawCallback = function (settings) {

    $("#loadingArea").hide();
    $("#tableArea").show();
    $(".dataTables_info").hide();
    /*
    var totalRecords = settings.fnSettings().fnRecordsDisplay();

    var totalPerPage = settings.fnSettings()._iDisplayLength;

    var totalPages = Math.ceil(totalRecords / totalPerPage);

    var currentIndex = parseInt(settings.fnSettings()._iDisplayStart);
    var currentPage = totalPages > 0 ? Math.ceil(1 + (currentIndex / totalPerPage)) : 0;
    $("#tblBalanceList_info").html("Exibindo " + currentPage + " de " + totalPages);

    if (totalRecords > 0 && totalPages > 1) {
        $("#tblBalanceList_info").css("display", "");
        $("#tblBalanceList_length").css("display", "");
        $("#tblBalanceList_paginate").css("display", "");
    }
    else {
        $("#tblBalanceList_info").css("display", "none");
        $("#tblBalanceList_length").css("display", "none");
        $("#tblBalanceList_paginate").css("display", "none");
    }*/

}

var balanceObj =
{
	"bAutoWidth": false
    , "bPaginate": false
    , "bSort": false
    , "bDestroy": true
    , "sPaginationType": "full_numbers"
    , "sDom": 'tri'
    , "oLanguage": {
    	"sLengthMenu": "Exibindo _MENU_ registros",
    	"sZeroRecords": "Nenhum registro encontrado.",
    	"sInfo": "Encontramos _TOTAL_ registros",
    	"sInfoEmpty": "Nenhum registro encontrado",
    	"sInfoFiltered": "(Filtrando _MAX_ registros)",
    	"sSearch": "Buscar:",
    	"sProcessing": "Processando...",
    	"oPaginate": {
    		"sFirst": "Primeiro",
    		"sPrevious": "Anterior",
    		"sNext": "Próximo",
    		"sLast": "Último"
    	}
    }
    , "bProcessing": true
    , "bServerSide": true
    , "sAjaxSource": "../Funds/GetBalance"
    , "fnServerData": function (sSource, aoData, fnCallback) {

    	if (aoData != null && aoData != undefined) {
    		aoData.push({ "name": "txtfundCode", "value": $("#ddlFunds").val() });
    	}
    	$.ajax({
    		"cache": false,
    		"dataType": 'json',
    		"type": "POST",
    		"url": sSource,
    		"data": aoData,
    		"beforeSend": function () {
    			$("#tableArea").hide();
    			$("#loadingArea").show();
    			$("#emptyArea").hide();
    		},
    		"success": function (data) {

    			fnCallback(data);

    			$("#lblGrossValue").text("R$ " + data.GrossValue);
    			$("#lblNetValue").text("R$ " + data.NetValue);
    			$("#lblBalanceDate").text(data.BalanceDate);
    			$("#lblFundName").text(data.FundName);

    			if (data.iTotalRecords == 0) {
    				$("#divBalanceList").hide();
    				$('#emptyArea').html('Sua pesquisa não encontrou resultados válidos. Tente novamente.').show();
    			}
    			else {
    				$('#emptyArea').html('').hide();
    				$("#divBalanceList").show();
    			}

    			$('table.tablescroll_head').css('width', '');

    			enableExportButtons();
    		}
    	});
    }
	, fnDrawCallback: function () {
	    fnDrawCallback(this);
	}	
    , "aoColumns": [
        null,
        { 'sClass': 'direita' },
        { 'sClass': 'centralizar' },
        { 'sClass': 'centralizar' },
        { 'sClass': 'direita' },
        { 'sClass': 'direita' },
        { 'sClass': 'direita' },
        { 'sClass': 'direita' },
		{ 'sClass': 'direita' },
        { 'sClass': 'direita' }
    ]
};

$(function () {

    var fnChange = function () {
        if ($("#ddlFunds").val() != null && $("#ddlFunds").val() != undefined && $("#ddlFunds").val() != '') {

            if (oTableBalance != null && oTableBalance != undefined) {
                oTableBalance.fnDestroy();
                oTableBalance = null;
            }
            $('#thetable').tableScroll({ height: 240 });
            oTableBalance = $(".tablescroll_head").dataTable(balanceObj);
        }
        else {
        	$("#divBalanceList").hide();
        	$("table.tablescroll_head tbody").html("");
        }
        enableExportButtons();
    }

    $("#ddlFunds").unbind("change").bind("change", function () {
        fnChange();
    });

    fnChange();

});

function enableExportButtons() {
	var data = $("table.tablescroll_head tbody tr");
	var emptyData = data.length == 0 || (data.length == 1 && data[0].lastChild.className == "dataTables_empty") ? true : false;
   	var excelLink = $("#lnkExcel");
   	var pdfLink = $("#lnkPdf");
   	excelLink.attr("href", (emptyData) ? "javascript:void(0)" : "/Funds/BalanceToFile/?isPdf=false");
   	pdfLink.attr("href", (emptyData) ? "javascript:void(0)" : "/Funds/BalanceToFile/?isPdf=true");
   	
	if (emptyData) {
		$("#lnkExcel").unbind("click").bind("click", function () {
			$("#divAlertaExportacao").show();
		});

		$("#lnkPdf").unbind("click").bind("click", function () {
			$("#divAlertaExportacao").show();
		});
	} else {
		$("#divAlertaExportacao").hide();
		$("#lnkExcel").unbind("click");
		$("#lnkPdf").unbind("click");
	}
}