﻿$(function () {
    $("#frmDoApplication").ajaxForm({
        beforeSend: function () {
            $("#divSuccessFeedback, #feedbackError, #lblApplication").hide();
        }
        , success: function (data) {
            if (data.Result == true) {
                $("#divSuccessFeedback").show().find('span').html("Sua solicitação de aplicação foi realizada com sucesso.");

                $("#txtApplicationValue").val('0,00');

                if (data.Data != null) {

                    $("#txtBlockedBalance").text("R$ " + $.format.number(data.Data.BlockedBalance, "#,##0.00#"));
                    $("#txtAvailableBalance").text("R$ " + $.format.number(data.Data.AvailableBalance, "#,##0.00#"));
                }
            }
            else {
                $("#feedbackError").show().html(data.Message);
            }
            Modal.Close();
        }
    });

    $("#btnConfirm").unbind("click").bind("click", function () {
        $("#frmDoApplication").submit();
    });
});