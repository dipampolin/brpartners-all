﻿var RedemptionFunctions = {

    ValidateRedemption: function () {
        var response = true;
        var ddlFunds = $("#ddlFunds");
        var txtAmount = $("#txtRedemptionValue");

        var totalValue = parseFloat($('[id$=lblTotalValue]').text().replace(/\./g, "").replace(/\,/g, "."));
        var minApplication = parseFloat($('[class$=lblMinValue]').eq(0).text().replace(/\./g, "").replace(/\,/g, "."));
        var amount = parseFloat(txtAmount.val().replace(/\./g, "").replace(/\,/g, "."));

        var total = $("#chkTotalValue").attr('checked');

        //Valida se Investimento Mínimo será respeitado
        if (isNaN(amount) || amount == 0) {
            $("#divErrorFeedback").show().html("Preencha o valor do resgate.");
            response = false;
        }
        else if ((totalValue - amount) < minApplication && (totalValue - amount) != 0) {
            $("#divErrorFeedback").show().html("Para continuar com aplicações neste fundo, você precisa deixar um saldo mínimo de R$ " +
            RedemptionFunctions.ParseMoney(minApplication) + "." +
            "Diminua o valor do resgate ou marque a opção \"Resgate total\".");
            response = false;
        }
        return { Valid: response, Investment: txtAmount.val(), FundCode: ddlFunds.val(), TotalRedemption: total };

    }
    , ConfirmApplication: function () {
        var request = RedemptionFunctions.ValidateRedemption();

        if (request.Valid) {
            var url = "/Funds/ConfirmRedemption/";
            commonHelper.OpenModalPost(url, 450, ".content", "", request);
            $("#MsgBox").center();

        }
    }
    , ParseMoney: function (value) {
        return $.format.number(value, "#,##0.00#");
    }
    , LoadDetails: function (fundCode, context) {
        if (fundCode == "") {
            $("#txtRedemptionValue").val('0,00');
            $("#pnlRedemption").hide();
            $("#tblBalanceRedemption").hide();
        }
        else {
            $.ajax({
                type: "POST"
                , cache: false
                , url: "../Funds/LoadFundsDetailForRedemption"
                , data: { fundCode: fundCode }
                , beforeSend: function () {
                    $("#txtApplicationValue", context).val('0,00');
                }
                , success: function (data) {
                    if (data != null) {
                        $("#pnlRedemption", context).show();
                        $("#tblBalanceRedemption", context).show();

                        if (data.amountPartial == 0) {
                            $("#txtRedemptionValue").attr("disabled", true);
                            $("#divErrorFeedback", context).show().html('Você não possui saldo disponível para resgate neste fundo.');
                            $("#btnRedemption").unbind("click").hide();
                        }
                        else {
                            $("#txtRedemptionValue").attr("disabled", false);
                            $("#btnRedemption").unbind("click").bind("click", function () {
                                RedemptionFunctions.ConfirmApplication();
                            }).show();
                        }

                        $('[class$=lblFundName]', context).text(data.name);
                        $('[id$=lblFundApplicationAmount]', context).text(RedemptionFunctions.ParseMoney(data.amount));

                        if (data.amountRedemption != 0) $('[id$=lblTotalValue]', context).text(RedemptionFunctions.ParseMoney(data.amountPartial));
                        else $('[id$=lblTotalValue]', context).text(RedemptionFunctions.ParseMoney(data.amount));

                        $('[class$=lblMinValue]', context).text(RedemptionFunctions.ParseMoney(data.minValue));
                        $('[id$=lblFundRecueAmount]', context).text(RedemptionFunctions.ParseMoney(data.amountRedemption));
                        $('[id$=lblFundApplicationAmountPartial]', context).text(RedemptionFunctions.ParseMoney(data.amountPartial));


                        $("#chkTotalValue", context).unbind("click").bind("click", function () {
                            if ($(this).attr("checked") == true) {
                                $("#txtRedemptionValue", context).val($("#lblTotalValue").text());
                            }
                            else {
                                $("#txtRedemptionValue", context).val('0,00');
                            }
                        });

                    }
                    else {
                        $("#divErrorFeedback", context).show().html('Você não possui saldo disponível para resgate neste fundo.');
                        $("#pnlRedemption", context).hide();
                        $("#tblBalanceRedemption", context).hide();
                    }
                }
            });
        }
    }
};

$(function () {

    var context = $("#conteudo-resgate")[0];

    $("#txtRedemptionValue", context).setMask("decimal");

    $("#ddlFunds", context).unbind("change").bind("change", function () {
        RedemptionFunctions.LoadDetails($(this).val(), context);
    });

    $("#btnRedemption").unbind("click").bind("click", function () {
        RedemptionFunctions.ConfirmApplication();
    });

    $("#txtRedemptionValue").unbind("keyup").bind("keyup", function (e) {

        $("#divErrorFeedback").hide().html('');

        if ($(this).val() == "") {
            $(this).val("0,00");
            return;
        }

        var amount = parseFloat($(this).val().replace(/\./g, "").replace(/\,/g, "."));
        var totalValue = parseFloat($('[id$=lblTotalValue]').text().replace(/\./g, "").replace(/\,/g, "."));

        if (amount > totalValue) {
            $(this).val($('[id$=lblTotalValue]').text());
            $("#chkTotalValue").attr('checked', true);
            //txtAmount.prop('disabled', true)
        }
        else {
            $("#chkTotalValue").attr('checked', false);
            //txtAmount.prop('disabled', false)
        }

        
    });
});