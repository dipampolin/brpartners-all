﻿$(function () {
    $("#frmDoRedemption").ajaxForm({
        beforeSend: function () {
            $("#divSuccessFeedback, #feedbackError").hide();
        }
        , success: function (data) {
            if (data.Result == true) {
                $("#divSuccessFeedback").show().find('span').html("Sua solicitação de resgate foi realizada com sucesso.");

                $("#txtRedemptionValue").val('0,00');
                
                if (data.Data != null) {

                    RedemptionFunctions.LoadDetails(data.Data.ClientCode, $("#conteudo-resgate")[0]);

                    $("#txtBlockedBalance").text("R$ " + RedemptionFunctions.ParseMoney(data.Data.BlockedBalance));
                    $("#txtAvailableBalance").text("R$ " + RedemptionFunctions.ParseMoney(data.Data.AvailableBalance));
                }
            }
            else {
                $("#feedbackError").show().html(data.Message);
            }
            Modal.Close();
        }
    });

    $("#btnConfirm").unbind("click").bind("click", function () {
        $("#frmDoRedemption").submit();
    });
});