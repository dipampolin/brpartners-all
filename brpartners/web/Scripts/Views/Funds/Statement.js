﻿$(function () {
    $("#ddlFunds, #ddlPeriod").unbind("change").bind("change", function () {
        if ($("#ddlFunds").val() != "" && $("#ddlFunds").val() != undefined && $("#ddlFunds").val() != null) {
            $("#statementArea").show();
            $.ajax({
                type: "POST",
                cache: false,
                dataType: "html",
                data: { ddlFunds: $("#ddlFunds").val(), ddlPeriod: $("#ddlPeriod").val() },
                url: "../Funds/LoadAnalyticalStatement",
                beforeSend: function () {
                    $("#AnalyticalStatementArea").html('');
                    $("#SyntheticStatementArea").html('');
                },
                success: function (content) {
                    $("#AnalyticalStatementArea").html(content);

                    enableAnalyticalExportButtons();
                    //if ($("#AnalyticalStatementArea > table").length > 0) {

                    $.ajax({
                        type: "POST",
                        cache: false,
                        dataType: "html",
                        data: { ddlFunds: $("#ddlFunds").val(), ddlPeriod: $("#ddlPeriod").val() },
                        url: "../Funds/LoadSyntheticStatement",
                        success: function (content) {
                            $("#SyntheticStatementArea").html(content);
                            enableSyntheticExportButtons();
                        }
                    });
                    //}
                }
            });
        } else {
            $("div#AnalyticalStatementArea table tbody").html("");
            $("div#SyntheticStatementArea table tbody").html("");
            $("#statementArea").hide();
        }
		enableAnalyticalExportButtons();
		enableSyntheticExportButtons();
    });
});

function enableAnalyticalExportButtons() {
	var data = $("div#AnalyticalStatementArea table tbody tr");
    var emptyData = data.length == 0 || (data.length == 1 && data[0].lastChild.className == "dataTables_empty") ? true : false;
    var excelLink = $("#lnkExcelA");
    var pdfLink = $("#lnkPdfA");
    excelLink.attr("href", (emptyData) ? "javascript:void(0)" : "/Funds/AnalyticalStatementToFile/?isPdf=false");
    pdfLink.attr("href", (emptyData) ? "javascript:void(0)" : "/Funds/AnalyticalStatementToFile/?isPdf=true");

    if (emptyData) {
        $("#lnkExcelA").unbind("click").bind("click", function () {
            $("#divAlertaExportacao").show();
        });

        $("#lnkPdfA").unbind("click").bind("click", function () {
            $("#divAlertaExportacao").show();
        });
    } else {
        $("#divAlertaExportacao").hide();
        $("#lnkExcelA").unbind("click");
        $("#lnkPdfA").unbind("click");
    }
}

function enableSyntheticExportButtons() {
    var data = $("div#SyntheticStatementArea table tbody tr");
    var emptyData = data.length == 0 || (data.length == 1 && data[0].lastChild.className == "dataTables_empty") ? true : false;
    var excelLink = $("#lnkExcelS");
    var pdfLink = $("#lnkPdfS");
    excelLink.attr("href", (emptyData) ? "javascript:void(0)" : "/Funds/SyntheticStatementToFile/?isPdf=false");
    pdfLink.attr("href", (emptyData) ? "javascript:void(0)" : "/Funds/SyntheticStatementToFile/?isPdf=true");

    if (emptyData) {
        $("#lnkExcelS").unbind("click").bind("click", function () {
            $("#divAlertaExportacao").show();
        });

        $("#lnkPdfS").unbind("click").bind("click", function () {
            $("#divAlertaExportacao").show();
        });
    } else {
        $("#divAlertaExportacao").hide();
        $("#lnkExcelS").unbind("click");
        $("#lnkPdfS").unbind("click");
    }
}

$(function () {
    $("#tabStatement").tabs();
});