﻿/// <reference path="../../References.js" />

var oTable;
var emptyDataMessage = "Sua pesquisa não encontrou resultados válidos. Tente novamente.";
var filterPage = 1;

var fnSearch = function (filterContext) {

    var ddlProfiles = $("#ddlProfiles", filterContext);
    var chkWithoutUsers = $("#chkWithoutUsers", filterContext);
    var txtUsers = $("#txtUsers", filterContext);
    var feedbackID = $("#hdfNewProfileID").val();

    var listOfElements = GetCheckedFilterElements();
    var withoutUser = chkWithoutUsers.is(':checked') ? 1 : 0;

    $.ajax({
        cache: false,
        async: false,
        type: "POST",
        url: "/AccessControl/LoadProfiles",
        data: { ProfileId: ddlProfiles.val(), UserName: txtUsers.val(), Elements: listOfElements, WithoutUsers: withoutUser },
        success: function () {
            obj = {};
            obj = {
                "bSort": true,
                "bAutoWidth": false,
                "bInfo": true,
                "oLanguage": {
                    "sProcessing": "<img src='/img/loading.gif'>",
                    "sLengthMenu": "<span>Exibir:</span> _MENU_ <span>por página</span>",
                    "sZeroRecords": "Sua pesquisa não encontrou resultados válidos. Tente novamente.",
                    "oPaginate": {
                        "sPrevious": "Anteriores",
                        "sNext": "Próximos"
                    }
                },
                "aoColumns": [
						null,
						null,
						{ "bSortable": false }
						],
                "sPaginationType": "full_numbers",
                "sDom": 'rtip',
                "fnDrawCallback": function () {
                    var totalRecords = this.fnSettings().fnRecordsDisplay();
                    var emptyArea = $("#emptyArea");
                    var tableArea = $("#tableArea");

                    if (totalRecords == 0) {
                        emptyArea.html(emptyDataMessage);
                        emptyArea.show();
                        tableArea.hide();
                    }
                    else {
                        tableArea.show();
                        emptyArea.hide();

                        var totalPerPage = this.fnSettings()._iDisplayLength;

                        var totalPages = Math.ceil(totalRecords / totalPerPage);

                        var currentIndex = parseInt(this.fnSettings()._iDisplayStart);
                        var currentPage = totalPages > 0 ? Math.ceil(1 + (currentIndex / totalPerPage)) : 0;

                        $("#ProfilesList_info").html("Exibindo " + currentPage + " de " + totalPages);

                        if (totalRecords > 0 && totalPages > 1) {
                            $("#ProfilesList_info").css("display", "");
                            $("#ProfilesList_length").css("display", "");
                            $("#ProfilesList_paginate").css("display", "");
                        }
                        else {
                            $("#ProfilesList_info").css("display", "none");
                            $("#ProfilesList_length").css("display", "none");
                            $("#ProfilesList_paginate").css("display", "none");
                        }
                        $('select', '#ProfilesList_length').styleInputs();
                    }
                    // Adiciona a classe 'ultimo' à última tr no corpo da tabela
                    $('table tbody tr:last-child', tableArea).addClass('ultimo');

                    // Adiciona a classe 'ultimo' às últimas th e td no corpo da tabela
                    $('table tr td:last-child', tableArea).addClass('ultimo');
                    $('table tr th:last-child', tableArea).addClass('ultimo');
                },
                "bProcessing": false,
                "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                    $('#ProfilesList_first').css("visibility", "hidden");
                    $('#ProfilesList_last').css("visibility", "hidden");
                },
                "bRetrieve": true,
                "bServerSide": true,
                "sAjaxSource": "../DataTables/AccessControl_Profiles",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    $.ajax({
                        cache: false,
                        dataType: 'json',
                        type: "POST",
                        url: sSource,
                        data: aoData,
                        success: fnCallback
                    });
                },
                "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                    var profileIdData = aData[2];
                    if (feedbackID != "" && feedbackID != "0" && profileIdData.indexOf("linkId" + feedbackID + "_Check") > 0)
                        nRow.className = nRow.className + ' selected';
                    return nRow;
                }
            };

            oTable = $("#ProfilesList").dataTable(obj);
            oTable.fnClearTable();
            oTable.fnDraw();
        }
    });
    
    return false;
}

$(function () {
    $(document).ready(function () {
        $("#closeFeedback").click(function () {
            $("#profileFeedback").slideToggle();
        });

        $("#btnNewProfile").click(function () {
            commonHelper.OpenModal("/AccessControl/EditProfile/0", 694, ".content", "", 503);
            $("#MsgBox").center();
        });


        var filterContext = $("#profileFilterContent");

        $("#btnSearch", filterContext).click(function () {
            fnSearch(filterContext);
            if ((filterContext).is(":visible")) { 
                $("#profileFilter").click()
            }
            $('.ui-autocomplete').hide();
            $('#divTreeFilter', filterContext).hide();
            return false;
        });

        setTimeout(function () {
            fnSearch(filterContext);
            return false;
        }, 250);

        $(filterContext).bind('keydown', function (event) {
            if (event.which == 13) {
                fnSearch(filterContext);
                $(filterContext).slideToggle('fast');
                $('.ui-autocomplete').hide();
                $('#divTreeFilter', filterContext).hide();
                return false;
            }
        });
    });
});


function ViewProfile(id) {
    commonHelper.OpenModal("/AccessControl/ProfileDetails/" + id, 694, ".content", "", 503);
}

function EditProfile(id) {
    commonHelper.OpenModal("/AccessControl/EditProfile/?id=" + id, 694, ".content", "", 503);
}

function DeleteProfile(id, name) {
    var url = "/AccessControl/ConfirmProfileDelete/?profileId=" + id + "&profileName=" + escape(name);
    commonHelper.OpenModal(url, 450, ".content", "");
    $("#MsgBox").center();
}

function ViewHistory() {
    commonHelper.OpenModal("/AccessControl/ProfilesHistory", 671, ".content", "");
}