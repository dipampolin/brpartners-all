﻿$(function () {
	handlers();
});

var currentId = 0;

function handlers() {

	$("#closeFeedback").click(function () {
		$("#feedback").slideToggle();
	});

	var context = $("#funcionalities");

	$("#funcionalities").treeview();

	$("[id^=lbl]", context).hover(function () {
		$("[id^=lnkOpenEdit]").each(function () { $(this).hide(); });
		var id = this.id.replace('lbl', '');
		var editButton = $("#lnkOpenEdit" + id, context);
		editButton.show();
	}, function () {
		var id = this.id.replace('lbl', '');
		var editButton = $("#lnkOpenEdit" + id, context);
		setTimeout(function () { editButton.hide(); }, 1200);
	});

	$("[id^=lnkOpenEdit]", context).click(function () {
		ResetAllFields();

		var id = this.id.replace('lnkOpenEdit', '');
		currentId = id;

		var divEdit = $("#editBox" + id, context);
		$("#lbl" + id, context).hide();
		$("#lnkOpenEdit" + id, context).hide();
		
		var lblAlias = $("#lbl" + id, context);
		$("#txtAlias" + id, context).val(lblAlias.html());

		divEdit.show();
		return false;
	});

	$("[id^=btnChangeAlias]", context).click(function () {
		var feedbackError = $("#feedbackError");
		var feedback = $("#feedback");
		feedbackError.hide();
		feedback.hide();

		var id = this.id.replace('btnChangeAlias', '');
		var txtNewAlias = $("#txtAlias" + id, context);
		var lblAlias = $("#lbl" + id, context);
		var newAlias = txtNewAlias.val();
		var oldAlias = lblAlias.html();

		txtNewAlias.removeClass("erro");

		if (newAlias != "" && newAlias.length >= 3 && id != "") {
			$.ajax({
				type: "POST",
				url: "/AccessControl/UpdateElementAlias/",
				dataType: "json",
				data: { id: id, oldAlias: oldAlias, newAlias: newAlias },
				success: function (data) {
					if (data == "true") {
						$("#lbl" + id).html(newAlias);
						feedback.show();
					}
					else {
						txtNewAlias.val($("#lbl" + id).html());
						feedbackError.html("<label class='error'>Oops. Não foi possível alterar o nome.</label>");
						feedbackError.show();
					}
					$("#lbl" + id, context).show();
					$("#editBox" + id, context).hide();
					$('html, body').animate({ scrollTop: 0 }, 'slow');
				}
			});
		}
		else {
			feedbackError.html("<label class='error'>Oops. Nome inválido.</label>");
			txtNewAlias.addClass("erro");
			feedbackError.show();
		}
		return false;
	});


	$(":not(.ignore)").bind("click", function (e) {
		if (currentId > 0) {
			ResetItemFields(currentId, context);
			currentId = 0;
		}
	});

	$("[id^=txtAlias]").click(function () { return false; });
}

function ViewHistory() {
    commonHelper.OpenModal("/AccessControl/FuncionalitiesHistory", 671, ".content", "");
}

/*function ResetAllFields() {
	var context = $("#funcionalities");
		$("[id^=editBox]", context).each(function () {
			var id = this.id.replace('editBox', '');
			ResetItemFields(id, context);
		});
}*/

function ResetItemFields(id, context) {
	$("#editBox" + id, context).hide();
	var lblAlias = $("#lbl" + id, context);
	$("#txtAlias" + id, context).val(lblAlias.html());
	lblAlias.show();
	$("#alias" + id, context).show();
}

function ResetAllFields() {
    var context = $("#funcionalities");

    $("[id^=editBox]", context).hide();
    $("[id^=lbl]", context).show();

    //$("[id^=txtAlias]", context).val( $(this).parent().parent().find('[id^=lbl]').text() );

    $("[id^=alias]", context).show();

}

