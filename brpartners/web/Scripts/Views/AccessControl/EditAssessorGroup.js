﻿
$(function () {

    var context = "#frmAssessors";

    //var context = $("#MsgBox");

    $("#lnkSaveGroup", context).click(function () {
        var groupName = $("#txtGroupName", context).val();
        var assessors = $("#txtAssessors", context).val();
        var feedback = ValidateGroupForm(groupName, assessors);

        if (feedback == "") {

            var groupID = $("#hdfGroupID", context).val();

            $.ajax({
                url: "/AccessControl/UpdateGroupOfAssessors",
                type: "POST",
                dataType: "json",
                data: $(context).serialize(),
                success: function (data) {
                    if (data.error == undefined || data.error == "") {
                        if (groupID > 0) {
                            var editedTr = $("#linkId" + data.group.ID + "_Check").parents('tr:first');
                            editedTr.addClass("selected");

                            var editedTds = $("td", editedTr);
                            if (editedTds.length == 4) {
                                $(editedTds[0]).html(data.group.Name);
                                $(editedTds[1]).html(data.group.Assessors);
                                $(editedTds[2]).html(data.totalUsers);
                            }

                            $("#groupOfAssessorsFeedback").find("span").html(groupID == 0 ? "Grupo cadastrado com sucesso." : "Grupo editado com sucesso.");
                            $("#groupOfAssessorsFeedback").show();
                            Modal.Close();
                        }
                        else {
                            window.location = "/AccessControl/GroupsOfAssessors";
                        }
                    }
                    else {
                        $("#feedbackError", context).html(data.error);
                        $("#feedbackError", context).show();
                        return false;
                    }
                }
            });
        }
        else {
            $("#feedbackError", context).html(feedback);
            $("#feedbackError", context).show();
            return false;
        }
    });


    $("#txtUserName", "#incluirNovoUsuario").autocomplete({
        source: function (request, response) {
            var data = "";

            data += $(context).serialize();
            if (data != "") data += "&";
            data += "term=" + request.term;

            $.ajax({
                url: "/AccessControl/UserAutoSuggest",
                type: "POST",
                dataType: "json",
                data: data,
                success: function (data) {
                    $("#hdfSelectedUser", "#incluirNovoUsuario").val("");
                    response($.map(data, function (item) {
                        return {
                            label: item.Name,
                            value: item.Name,
                            id: item.ID
                        }
                    }));
                }
            });
        },
        minLength: 1,
        select: function (event, ui) {
            $("#hdfSelectedUser", "#incluirNovoUsuario").val(ui.item.id);
        }
    });

    $("#lnkAddUser").click(function () {
        $("#feedbackError", context).hide();
        var userName = $("#txtUserName", "#incluirNovoUsuario").val();
        var userId = $("#hdfSelectedUser", "#incluirNovoUsuario").val();
        var data = "";

        data += $(context).serialize();
        if (data != "") data += "&";
        data += "id=" + userId + "&name=" + userName;

        $.post("/AccessControl/IncludeUserInList/", data, function (data) {
            if (data.error != "") {
                $("#feedbackError", context).html(data.error);
                $("#feedbackError", context).show();
                return false;
            }
            else {


                $("#listaUsuarios").append("<li/>");

                $("#listaUsuarios li:last").attr("id", "usuario" + data.user.ID);
                $("#listaUsuarios li:last").append("<a onclick=\"DeleteUserFromList(" + data.user.ID + ");return false;\">excluir</a>");
                $("#listaUsuarios li:last").append("<input type=hidden />");
                $("#listaUsuarios li:last").append(data.user.Name);

                $("#listaUsuarios li a:last").attr("class", "ico-excluir");

                $("#listaUsuarios li input:last").attr("value", data.user.ID);
                $("#listaUsuarios li input:last").attr("name", "userID");
                $("#listaUsuarios li input:last").attr("id", "userID" + data.user.ID);

                $("#txtUserName", "#incluirNovoUsuario").val("");
                $("#hdfUser", "#incluirNovoUsuario").val("");

            }
        }, "json");

    });

    var fnManageClients = function (operation) {
        $("#feedbackError", context).hide().html('');

        var input = operation == "I" ? "#txtInsertClient" : "#txtExcludeClient";

        var clientId = $(input, context).val();

        if (clientId != "") {

            $.ajax({
                type: "POST",
                cache: false,
                dataType: "json",
                url: "../AccessControl/CheckIfClientExists",
                data: { clientCode: clientId },
                beforeSend: function () {
                    $("#feedbackError", context).hide().html('');
                },
                success: function (data) {
                    var create = true;
                    if (data.Result) { //Cliente existe.
                        //checar na lista inserção se já existe 
                        var liInserted = $("#listaClientesInclusao > li", context);

                        liInserted.each(function () {
                            var client = $(this).attr("id").replace("cliente", "");

                            if (clientId == parseInt(client, 10) && create) {
                                $("#feedbackError", context).show().html('Este cliente já consta na lista de clientes incluidos do grupo.');
                                create = false;
                            }

                        });

                        var liExcluded = $("#listaClientesExclusao > li", context);

                        liExcluded.each(function () {
                            var client = $(this).attr("id").replace("cliente", "");

                            if (clientId == parseInt(client, 10) && create) {
                                $("#feedbackError", context).show().html('Este cliente já consta na lista de clientes excluidos do grupo.');
                                create = false;
                            }

                        });

                        if (create) {
                            if (operation == 'I') {
                                $("#listaClientesInclusao", context).append(
                                    $("<li id='cliente" + clientId + "'><span onclick='DeleteClientFromList(" + clientId + ", \"I\");return false;' class='ico-excluir'></span>" +
                                    "<input type='hidden' name='clientIncludeID' id='clientIncludeID" + clientId + "' value='" + clientId + "' />" +
                                    data.Message + "</li>").clone()
                                );
                            }
                            else {
                                $("#listaClientesExclusao", context).append(
                                    $("<li id='cliente" + clientId + "'><span onclick='DeleteClientFromList(" + clientId + ", \"E\");return false;' class='ico-excluir'></span>" +
                                    "<input type='hidden' name='clientExcludeID' id='clientExcludeID" + clientId + "' value='" + clientId + "' />" +
                                    data.Message + "</li>").clone()
                                );
                            }
                        }
                    }
                    else {
                        $("#feedbackError", context).show().html('Cliente não encontrado.');
                    }
                }
            });
        }

        $(input).val('');
    };

    $("#lnkAddClientInclude", context).unbind("click");
    $("#lnkAddClientInclude", context).click(function () { fnManageClients("I"); });

    $("#lnkAddClientExclude", context).unbind("click");
    $("#lnkAddClientExclude", context).click(function () { fnManageClients("E") });

});

function ValidateList(listValue) {
    var list = listValue.split(',');
    if (list.length > 0) {
        for (var i = 0; i < list.length; i++) {
            var item = list[i];
            if (item.indexOf('-') >= 0) {
                var items = item.split('-');
                if (items.length > 2 || isNaN(items[0]) || isNaN(items[1]))
                    return false;
                var start = parseInt(items[0]);
                var end = parseInt(items[1]);
                if (start >= end)
                    return false;
            }
            else {
                if (/^\d+$/.test(item) == false) {
                    return false;
                }
            }
        }
    }
    return true;
}

function ValidateGroupForm(groupName, assessors) {
    var feedback = "";

    var inclusos = $("#listaClientesInclusao").find("li:first").attr("id");
    var exclusos = $("#listaClientesExclusao").find("li:first").attr("id"); ;
    
    if (groupName == "" || groupName.length < 3) {
        feedback += "<li>Nome de grupo inválido.</li>";
    }

    if (assessors != "" && !ValidateList(assessors)) {
        feedback += "<li>Faixa de assessores inválida.</li>";
    }

    if (assessors == "" && (inclusos != undefined || exclusos != undefined)) {
        feedback += "<li>Para adicionar clientes avulsos, é necessário uma lista de assessores.</li>";
    }
    return feedback;
}

function DeleteUserFromList(id) {
    var userItem = $("#usuario" + id, "#listaUsuarios");
    userItem.remove();

}

function DeleteClientFromList(id, operation) {
    var context = (operation == 'I') ? "#listaClientesInclusao" : "#listaClientesExclusao";
    var clientItem = $("#cliente" + id, context);
    clientItem.remove();
}
