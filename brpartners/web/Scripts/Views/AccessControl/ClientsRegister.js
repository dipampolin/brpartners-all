﻿/// <reference path="../../References.js" />

var oTableClientsRegister = null;

$(function () {
    $("#btnAddClient").click(function () {
        EditClient(0);
    });
    if (oTableClientsRegister != null) {
        oTableClientsRegister.fnDestroy();
        oTableClientsRegister = null;
    }
    oTableClientsRegister = $("#tblClientList").dataTable(objClientsRegisterInitial);

    $(".ico-hist").unbind("click").bind("click", function () {
        var url = "/AccessControl/ClientsRegisterHistory/";
        commonHelper.OpenModalPost(url, 671, ".content", "");
        $("#MsgBox").center();
    });
});

var fnDrawCallback = function (settings) {
    $("#loadingArea").hide();
    $("#tableArea").show();

    var totalRecords = settings.fnSettings().fnRecordsDisplay();

    var totalPerPage = settings.fnSettings()._iDisplayLength;

    var totalPages = Math.ceil(totalRecords / totalPerPage);

    var currentIndex = parseInt(settings.fnSettings()._iDisplayStart);
    var currentPage = totalPages > 0 ? Math.ceil(1 + (currentIndex / totalPerPage)) : 0;

    $("#tblClientList_info").html("Exibindo " + currentPage + " de " + totalPages);

    if (totalRecords > 0 && totalPages > 1) {
        $("#tblClientList_info").css("display", "");
        $("#tblClientList_length").css("display", "");
        $("#tblClientList_paginate").css("display", "");
    }
    else {
        $("#tblClientList_info").css("display", "none");
        $("#tblClientList_length").css("display", "none");
        $("#tblClientList_paginate").css("display", "none");
    }
}

var objClientsRegisterFilter = {
    "bAutoWidth": false
            , "bPaginate": true
            , "bDestroy": true
            , "sPaginationType": "full_numbers"
            , "sDom": 'trip'
            , "oLanguage": {
                "sLengthMenu": "Exibindo _MENU_ registros",
                "sZeroRecords": "Nenhum registro encontrado.",
                "sInfo": "Encontramos _TOTAL_ registros",
                "sInfoEmpty": "Nenhum registro encontrado",
                "sInfoFiltered": "(Filtrando _MAX_ registros)",
                "sSearch": "Buscar:",
                "sProcessing": "Processando...",
                "oPaginate": {
                    "sFirst": "Primeiro",
                    "sPrevious": "Anterior",
                    "sNext": "Próximo",
                    "sLast": "Último"
                }
            }
            , "bProcessing": true
            , "bServerSide": true
            , "sAjaxSource": "../AccessControl/ClientsRegisterFilter"
            , "fnServerData": function (sSource, aoData, fnCallback) {
                if (aoData != null && aoData != undefined) {
                    aoData.push({ "name": "txtClientCode", "value": $('#txtClientCode').val() });
                    aoData.push({ "name": "ddlFormat", "value": $("#ddlFormat").val() });
                }
                $.ajax({
                    "cache": false,
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    /*"beforeSend": function () {
                        $("#tableArea").hide();
                        $("#loadingArea").show();
                        $("#emptyArea").hide();
                    },*/
                    "success": function (data) {
                        fnCallback(data);
                        if (data.iTotalRecords == 0) {
                            $("#tableArea").hide();
                            $('#emptyArea').html('Sua pesquisa não encontrou resultados válidos. Tente novamente.').show();
                            $("#loadingArea").hide();
                        }
                        else {
                            $('#emptyArea').html('').hide();
                            $("#loadingArea").hide();
                            $("#tableArea").show();
                        }

                        ClientsRegisterReportMethods.enableExportButtons();
                        ClientsRegisterReportMethods.viewDetails();
                        ClientsRegisterReportMethods.removeClients();
                    }
                });
            }
            , fnDrawCallback: function () {
                fnDrawCallback(this);
            }
            , fnDrawCallback: function () {
                fnDrawCallback(this);
            }
            , aoColumns: [
                null,
                null,
                { "bSortable": false },
                { "bSortable": false },
                { "bSortable": false }
            ]
};

var objClientsRegisterInitial = {
    "bAutoWidth": false
            , "bPaginate": true
            , "bDestroy": true
            , "sPaginationType": "full_numbers"
            , "sDom": 'trip'
            , "oLanguage": {
                "sLengthMenu": "Exibindo _MENU_ registros",
                "sZeroRecords": "Nenhum registro encontrado.",
                "sInfo": "Encontramos _TOTAL_ registros",
                "sInfoEmpty": "Nenhum registro encontrado",
                "sInfoFiltered": "(Filtrando _MAX_ registros)",
                "sSearch": "Buscar:",
                "sProcessing": "Processando...",
                "oPaginate": {
                    "sFirst": "Primeiro",
                    "sPrevious": "Anterior",
                    "sNext": "Próximo",
                    "sLast": "Último"
                }
            }
            , "bProcessing": true
            , "bServerSide": true
            , "sAjaxSource": "../AccessControl/InitialClientsRegisterFilter"
            , "fnServerData": function (sSource, aoData, fnCallback) {
                $.ajax({
                    "cache": false,
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    /*"beforeSend": function () {
                        $("#tableArea").hide();
                        $("#loadingArea").show();
                        $("#emptyArea").hide();
                    },*/
                    "success": function (data) {
                        fnCallback(data);
                        if (data.iTotalRecords == 0) {
                            $("#tableArea").hide();
                            $('#emptyArea').html('Sua pesquisa não encontrou resultados válidos. Tente novamente.').show();
                            $("#loadingArea").hide();
                        }
                        else {
                            $('#emptyArea').html('').hide();
                            $("#tableArea").show();
                            $("#loadingArea").hide();
                        }

                        ClientsRegisterReportMethods.enableExportButtons();
                        ClientsRegisterReportMethods.removeClients();
                        ClientsRegisterReportMethods.viewDetails();
                    }
                });
            }
            , fnDrawCallback: function () {
                fnDrawCallback(this);
            }
            , aoColumns: [
                null,
                null,
                { "bSortable": false },
                { "bSortable": false },
                { "bSortable": false }
            ]
};

var ClientsRegisterReportMethods = {

    enableExportButtons: function () {
        var data = $("#tblClientList tbody tr");
        var emptyData = (data.length == 1 && data[0].lastChild.className == "dataTables_empty") ? true : false;
        var excelLink = $("#lnkClientsExcel");
        var pdfLink = $("#lnkClientsPDF");
        excelLink.attr("href", (emptyData) ? "javascript:void(0);" : "/AccessControl/ClientsRegisterToFile/?isPdf=false");
        //excelLink.attr("target", (emptyData) ? "_self" : "_blank");
        pdfLink.attr("href", (emptyData) ? "javascript:void(0);" : "/AccessControl/ClientsRegisterToFile/?isPdf=true");
        //pdfLink.attr("target", (emptyData) ? "_self" : "_blank");
    },

    viewDetails: function () {
        $(".ico-visualizar").unbind("click").bind("click", function () {
            var clientId = $(this).attr("rel");
            if (clientId != undefined && clientId != "") {
                commonHelper.OpenModalPost("/AccessControl/ClientsDetails/", 450, ".content", "", { id: clientId });
            }
        });
    },

    removeClients: function () {
        $(".ico-excluir").unbind("click").bind("click", function () {
            var clientId = $(this).attr("rel");
            if (clientId != undefined && clientId != "") {
                commonHelper.OpenModalPost("/AccessControl/ConfirmClientRegisterDelete/", 450, ".content", "", { id: clientId });
            }
        });
    }

};

function EditClient(id) {
    commonHelper.OpenModal("/AccessControl/ClientsRegisterRequestEdit/?id=" + id, 450, ".content", "");
}