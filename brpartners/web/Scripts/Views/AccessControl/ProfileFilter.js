﻿$(function () {
    var context = $("#profileFilterContent");
    var chkWithoutUsers = $("#chkWithoutUsers", context);

    $(chkWithoutUsers).click(function () {
        if ($(this).is(':checked')) {
            $("ul.selectStyled li.focus", context).html("Todos");
        }
        else {
            $("#ddlProfiles", context).val("0");
        }
    });

    $("#ddlProfiles", context).change(function () {
        if ($(this).val() != "0")
            $(chkWithoutUsers).attr('checked', false);
    });

    $("#txtElements", context).click(function () {
        $("#divTreeFilter").slideToggle("fast");
    });

    $("#profileFilter").click(function () {
        $("#profileFilterContent").slideToggle('fast');
        $('select').styleInputs();
        $(this).toggleClass('aberto');
    });

    $("#txtUsers", context).autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/AccessControl/UserAutoSuggest",
                type: "POST",
                dataType: "json",
                data: { term: request.term },
                success: function (data) {
                    $("#hdfUser", context).val("");
                    response($.map(data, function (item) {
                        return {
                            label: item.Name,
                            value: item.Name,
                            id: item.ID
                        }
                    }));
                }
            });
        },
        minLength: 1,
        select: function (event, ui) {
            $("#hdfUser", context).val(ui.item.id);
        }
    });

    var treeFilter = $("#treeFuncionalitiesFilter");

    $("input[type=checkbox]", treeFilter).click(function () {

        // seleciona todos os filhos do pai marcado
        var parentLi = $(this).parents('li:first');
        parentLi.find(':checkbox').attr('checked', this.checked);

        if (this.checked) {
            // seleciona todos os pais do filho marcado
            $(this).parents().children(':checkbox').attr('checked', this.checked);
        }

    });

    $("a", treeFilter).removeAttr('href');
    treeFilter.find(">li").addClass("pai"); //.find(">ul>li>ul>li>span").addClass("bold")
    treeFilter.find("li:last-child", ".pai").addClass("ultimo");
});

function GetCheckedFilterElements() {

    var treeFilter = $("#treeFuncionalitiesFilter");

    var lstIDs = "";
    $("[id ^= chk]", treeFilter).each(function () {
        var obj = this;
        if ($(this).is(':checked')) {
            var id = this.id;
            lstIDs = lstIDs + id.replace("chk", "") + ",";
        }
    })
    return lstIDs;
}