﻿$(function () {
    $("#btnDelete").unbind("click").bind("click", function () {

        $("#frmClientRegisterDelete").submit();

    });

    $("#frmClientRegisterDelete").ajaxForm({
        beforeSend: function () {
            $("#feedback").hide().find("span").html('');
            $("#feedbackError").hide().find("span").html('');
        },
        success: function (data) {
            $("#loadingArea").hide();

            if (!data.Result) {
                $("#feedbackError").show().find("span").append(data.Message);
            }
            else {
                $("#feedback").show().find("span").append(data.Message);
            }

            if (oTableClientsRegister != null) {
                oTableClientsRegister.fnDraw(true);
            }

            Modal.Close();
        }
    });
});