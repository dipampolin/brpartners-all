﻿
$(document).ready(function () {

    var context = $("#frmClientsRegisterRequestEdit");

    $('table[id^="tblEmail-"] .ico-excluir').live("click", function () {
        var baseId = this.id;
        RemoveEmail(baseId);
        return false;
    });

    $('table[id^="tblCopy-"] .ico-excluir').live("click", function () {
        var baseId = this.id;
        RemoveCopy(baseId);
        return false;
    });

    $('table[id^="tblEmail-"]').each(function () {
        var table = $(this);
        if (table.find("tr").length > 0) {
            table.css("display", "");
        }
    });

    $('table[id^="tblCopy-"]').each(function () {
        var table = $(this);
        if (table.find("tr").length > 0) {
            table.css("display", "");
        }
    });

    $('input[id^="hdnFormat-"]').each(function () {
        var value = $(this).val();
        if (value != "") {
            var baseId = this.id;
            $(':radio[value=\"' + value + '\"]', '#' + baseId.replace('hdnFormat', 'trLine')).attr('checked', true);
        }
    });

    $('a[id^="btnAddEmail-"]').bind("click", function () {
        var baseId = this.id.split('-')[1];
        Add(baseId, "#txtEmail-", "#tblEmail-");
    });

    $('a[id^="btnAddCopy-"]').bind("click", function () {
        var baseId = this.id.split('-')[1];
        Add(baseId, "#txtCopy-", "#tblCopy-");
    });

    $('input[id^="rdbFormat-"]').change(function () {
        var baseId = this.id;
        var value = $(this).val();
        $("#" + baseId.replace("rdb", "hdn")).val(value);
    });

    $("#txtClientCode", context).blur(function () {
        //commomDataHelper.GetClientName($(this).val(), "lblClientName", context, "txtClientCode");
        GetClientNameAndValidate($(this).val(), "lblClientName", context, "txtClientCode");
        $("#requestFeedbackError", context).hide();
    }).setMask();

    $('input[id^="txtEmail-"]').autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/AccessControl/EmailSuggest",
                type: "POST",
                dataType: "json",
                data: { term: request.term, currentList: $(this.element[0]).parent().next().next().val(), isCopy: "0" },
                success: function (data) {
                    response($.map(data, function (item) {
                        return {
                            label: item,
                            value: item,
                            id: item
                        }
                    }));
                }
            });
        },
        minLength: 3,
        select: function (event, ui) {
            $(this).val(ui.item);
        }
    });

    $('input[id^="txtCopy-"]').autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/AccessControl/EmailSuggest",
                type: "POST",
                dataType: "json",
                data: { term: request.term, currentList: $(this.element[0]).parent().next().next().val(), isCopy: "1" },
                success: function (data) {
                    response($.map(data, function (item) {
                        return {
                            label: item,
                            value: item,
                            id: item
                        }
                    }));
                }
            });
        },
        minLength: 3,
        select: function (event, ui) {
            $(this).val(ui.item);
        }
    });

    $("#btnRequest").click(function () {

        var feedback = ValidateFields();
        if (feedback != "") {
            $("#feedbackRequestError", context).html(feedback);
            $("#feedbackRequestError", context).css("display", "block");
            return false;
        }

        $("#txtClientCode").attr("disabled", "");
        var data = "";
        data += $(context).serialize();

        $.ajax({
            url: "/AccessControl/ClientsRegisterRequestEdit",
            type: "POST",
            dataType: "json",
            data: data,
            success: function (data) {
                if (data.Error == "") {
                    var msg = $("#hdnIsUpdate", context).val() == "1" ? "Cliente alterado com sucesso." : "Cliente cadastrado com sucesso.";
                    Modal.Close();
                    $("div#feedback span").html(msg);
                    $("div#feedback").show();

                    oTableClientsRegister.fnDraw(false);
                    //$("#ClientsRegisterFilterContent #btnSearch").click();
                    //if ($("#ClientsRegisterFilterContent #txtClientCode").val() != 'undefined' && $("#ClientsRegisterFilterContent #txtClientCode").val() != "") { $("#ClientsRegisterFilterContent #btnSearch").click(); }
                }
                else {
                    $("#txtClientCode").attr("disabled", "disabled");
                    $("#feedbackRequestError", context).html(data.Error);
                    $("#feedbackRequestError", context).show();
                }
            }
        });

        return false;

    });

    function Add(baseId, emailPrefix, tablePrefix) {

        var txtEmail = $(emailPrefix + baseId, context); //var obj = $("#txtEmail", context);
        var table = $(tablePrefix + baseId, context);

        var emailValue = txtEmail.val();

        if (emailValue != "") {

            if (!ValidateEmail(emailValue)) {
                $("#feedbackRequestError", context).html("<ul><li>E-mail inválido.</li></ul>");
                $("#feedbackRequestError", context).css("display", "block");

                return false;
            }

            if (!ValidateEmailExists(emailValue, table)) {
                $("#feedbackRequestError", context).html("<ul><li>Este e-mail já foi adicionado para esta funcionalidade.</li></ul>");
                $("#feedbackRequestError", context).css("display", "block");

                return false;
            }

            if (!ValidateEmailExistsInOtherList(emailValue, table)) {
                var column = tablePrefix.indexOf("Copy") != -1 ? "E-mail" : "Cópia";

                $("#feedbackRequestError", context).html("<ul><li>Este e-mail já foi adicionado na coluna " + column + " para esta funcionalidade.</li></ul>");
                $("#feedbackRequestError", context).css("display", "block");

                return false;
            }

            $("#feedbackRequestError", context).html("");
            $("#feedbackRequestError", context).css("display", "none");

            rowCount = table.find("tr").length;
            var classTr = (rowCount % 2 == 0) ? "even" : "odd";
            var id = baseId + "-" + rowCount;

            var newTr = "<tr class=\"" + classTr + "\"><td><span>" + emailValue + "</span><a href=\"javascript:void(0);\" class=\"ico-excluir\" id=\"lnkRemove-" + id + "\">excluir</a></td></tr>";

            if (rowCount == 0) {
                table.css("display", "");
            }

            table.find("tbody").append(newTr);
            txtEmail.val("");

            var currentList = table.next();
            currentList.val(currentList.val() + ";" + emailValue);

            $.ajax({
                url: "/AccessControl/AddEmailToSuggest",
                type: "POST",
                dataType: "json",
                data: { term: emailValue, isCopy: emailPrefix.indexOf("Copy") != -1 ? "1" : "0" },
                success: function (data) {
                    if (data.Error == "") {

                    }
                    else {
                        // Error
                    }
                }
            });
        }
    }

    function RemoveEmail(baseId) {
        Remove(baseId, "#tblEmail-");
    }

    function RemoveCopy(baseId) {
        Remove(baseId, "#tblCopy-");
    }

    function Remove(baseId, tablePrefix) {

        // TODO: Excluir da lista da session

        var id = baseId.split("-"); // baseId = tr-function id-tr id
        var table = $(tablePrefix + id[1], context); // context table

        var currentList = table.next(); // e-mail list
        var currentValue = currentList.val();

        var email = $("#" + baseId, table).prev().html(); // e-mail to remove

        currentValue = currentValue.replace(email, '');
        currentList.val(currentValue);

        var tr = $("#" + baseId, table).parent().parent().remove();

        var rowCount = table.find("tr").length;
        if (rowCount == 0) {
            table.css("display", "none");
        }
    }

    function ValidateEmail(email) {
        var format = new RegExp("^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,3})*(\.[a-zA-Z]{2,3})$");
        //var format = new RegExp("^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$");

        if (!format.test(email)) {
            return false;
        }

        return true;
    }

    function ValidateEmailExists(email, table) {
        var currentList = $(table).next().val();
        if (currentList.indexOf(email) != -1) {
            return false;
        }

        return true;
    }

    function ValidateEmailExistsInOtherList(email, table) {

        var tableId = table.attr("id");
        table = tableId.indexOf("Copy") != -1 ? $("#" + tableId.replace("Copy", "Email")) : $("#" + tableId.replace("Email", "Copy"));

        var otherList = $(table).next().val();
        if (otherList.indexOf(email) != -1) {
            return false;
        }

        return true;
    }

    function GetClientNameAndValidate(code, targetElementId, context, txtName) {

        if (code != "") {
            var found = true;
            var clientName = "";
            clientCode = parseInt(code.replace(/\./g, ''));

            $.ajax({
                type: "POST",
                url: "/NonResidentTraders/GetTraderName/",
                dataType: "json",
                data: { code: clientCode },
                success: function (data) {
                    //$("#" + targetElementId, context).html(String(data));
                    clientName = String(data);
                    if (txtName != null && txtName != "" && data == "Cliente não encontrado.") {
                        $("#" + txtName, context).val("");
                        $("#hdnClientName", context).val("");
                        found = false;
                    }
                }
            });

            if (found) {
                $.ajax({
                    type: "POST",
                    url: "/AccessControl/ValidateClientRegisterExists/",
                    dataType: "json",
                    data: { clientCode: clientCode },
                    success: function (data) {
                        if (data.Result == "true") {
                            $("#" + targetElementId, context).html("Cliente já cadastrado.");
                            $("#" + txtName, context).val("");
                        } else {
                            $("#" + targetElementId, context).html(clientName);
                            $("#hdnClientName", context).val(clientName);
                        }
                    }
                });
            }

        }
        else {
            $("#" + targetElementId, context).html("");
        }
    }

    function ValidateFields() {
        var feedback = "";

        if ($("#txtClientCode", context).val() == "") {
            feedback += "<li>Cliente em branco.</li>";
        }

        var nothingFilled = true;

        $('tr[id^="trLine-"]').each(function () {

            var line = this.id;
            var id = line.split('-')[1];

            var funcionalityName = $(this).find(".nome-cliente").html();

            var emails = $("#hdnEmail-" + id).val();
            emails = emails.replace(/\;/g, '');

            if (emails != "") {
                var r = $(this).find('input[id^="rdbFormat-"]');

                if ($(r[0]).attr('checked') === false && $(r[1]).attr('checked') === false) {
                    feedback += "<li>Formato em branco na funcionalidade " + funcionalityName + ".</li>";
                }
            }

            var copies = $("#hdnCopy-" + id).val();
            copies = copies.replace(/\;/g, '');

            if (copies != "" && emails == "") {
                feedback += "<li>Preencha um e-mail para a funcionalidade " + funcionalityName + ".</li>";
            }

            if (emails != "" || (copies != "" && emails == "")) {
                nothingFilled = false;
            }

        });

        if (nothingFilled) {
            feedback += "<li>Preencha ao menos uma funcionalidade.</li>";
        }

        if (feedback != "") {
            feedback = "<ul>" + feedback + "</ul>";
        }

        return feedback;
    }

});

