﻿/// <reference path="../../References.js" />

var oTableClientsRegister = null;

var ClientsRegisterReportFilter = {
    ValidateFilterFields: function () {
        var messageError = "";

        if (!validateHelper.ValidateList($("#txtClientCode").val()))
            messageError += "<li>Faixa de clientes inválida.</li>";

        if (messageError.length > 0)
            messageError = "<ul>" + messageError + "</ul>";

        return { Result: (messageError == ""), Message: messageError }
    }
};

$(function () {
    var context = $('#ClientsRegisterFilterContent');

    $("#ClientsRegisterFilter").click(function () {
        $('#ClientsRegisterFilterContent').slideToggle('fast');
        $('select').styleInputs();
        $(this).toggleClass('aberto');
    });

    $(context).bind('keydown', function (event) {
        if (event.which == 13) {
            $("#btnSearch").click();
        }
    });

    $("#btnSearch").unbind("click");
    $("#btnSearch").bind("click", function () {
        var feedback = ClientsRegisterReportFilter.ValidateFilterFields();
        if (feedback.Result) {
            $("#filterFeedbackError").html("").hide();
            $("#ClientsRegisterFilterContent").hide();
            $("#loadingArea").show();
            $("#tableArea").hide();
            $("#feedback").hide().find("span").html('');
            $("#feedbackError").hide().find("span").html('');

            if (oTableClientsRegister != null) {
                oTableClientsRegister.fnDestroy();
                oTableClientsRegister = null;
            }

            oTableClientsRegister = $("#tblClientList").dataTable(objClientsRegisterFilter);

            //ClientsRegisterReportMethods.enableExportButtons();
        }
        else {
            $("#filterFeedbackError").html(feedback.Message).show();
        }
    });
});
