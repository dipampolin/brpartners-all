﻿$(function () {
    SortOptions.AddSortOptions();
	handlers();
});

function handlers() {
	var context = $("#historyArea");

	var emptyDataMessage = "Sua pesquisa não encontrou resultados válidos. Tente novamente.";

	$("#txtStartDate", context).setMask("99/99/9999");
	$("#txtEndDate", context).setMask("99/99/9999");

	$("#txtStartDate", context).datepicker();
	$("#txtEndDate", context).datepicker();

	$.datepicker.setDefaults($.datepicker.regional['pt-BR']);
	$("#txtStartDate", context).datepicker($.datepicker.regional['pt-BR']);
	$("#txtEndDate", context).datepicker($.datepicker.regional['pt-BR']);

	$("#txtResponsible", context).blur(function () {
		if (this.value == "")
			$("#hdfResponsible", context).val("");
		else
			commomDataHelper.ValidateUsernameSuggest(this.value, "#hdfResponsible", context);
	});
	$("#txtTarget", context).blur(function () {
		if (this.value == "")
			$("#hdfTarget", context).val("");
		else
			commomDataHelper.ValidateProfileNameSuggest(this.value, "#hdfTarget", context);
	});

	$("#txtResponsible", context).autocomplete({
		source: function (request, response) {
			$.ajax({
				url: "/AccessControl/UserAutoSuggest",
				type: "POST",
				dataType: "json",
				data: { term: request.term },
				success: function (data) {
					response($.map(data, function (item) {
						return {
							label: item.Name,
							value: item.Name,
							id: item.ID
						}
					}));
				}
			});
		},
		minLength: 1,
		select: function (event, ui) {
			$("#hdfResponsible", context).val(ui.item.id);
		}
	});

	$("#txtTarget", context).autocomplete({
		source: function (request, response) {
			$.ajax({
				url: "/AccessControl/ProfilesAutoSuggest",
				type: "POST",
				dataType: "json",
				data: { term: request.term },
				success: function (data) {
					response($.map(data, function (item) {
						return {
							label: item.Name,
							value: item.Name,
							id: item.ID
						}
					}));
				}
			});
		},
		minLength: 1,

		select: function (event, ui) {
			$("#hdfTarget", context).val(ui.item.id);
		}
	});

	var breadcrumb = $("#trilha");
	if (breadcrumb.length > 0) {
		$("#breadcrumb", context).html(breadcrumb.html());
	}

	$("#btnSearch", context).click(function () {
		$.ajax({
			cache: false,
			async: false,
			type: "POST",
			url: "/AccessControl/LoadProfilesHistory",
			data: { txtStartDate: $("#txtStartDate", context).val(), txtEndDate: $("#txtEndDate", context).val(), hdfResponsible: $("#hdfResponsible", context).val(), hdfTarget: $("#hdfTarget", context).val(), ddlActions: $("#ddlActions", context).val() },
			success: function () {
				obj = {};
				obj = {
					"bSort": true,
					"bAutoWidth": false,
					"bInfo": true,
					"bSort": true,
					"oLanguage": {
						"sProcessing": "<img src='/img/loading.gif'>",
						"sLengthMenu": "<span>Exibir:</span> _MENU_ <span>por página</span>",
						"sZeroRecords": "Sua pesquisa não encontrou resultados válidos. Tente novamente.",
						"oPaginate": {
							"sPrevious": "Anteriores",
							"sNext": "Próximos"
						}
					},
					"aaSorting": [[0, "desc"]],
					"sPaginationType": "full_numbers",
					"aoColumns": [
                        { sType: "date-euro" },
                        null,
                        null,
                        null
                    ],
					"sDom": 'rtip',
					"fnDrawCallback": function () {
						var totalRecords = this.fnSettings().fnRecordsDisplay();
						var emptyArea = $("#emptyArea");
						var tableArea = $("#tableArea");

						var excelLink = $("#lnkHistoryExcel", context);
						var pdfLink = $("#lnkHistoryPdf", context);

						if (excelLink.length > 0 && pdfLink.length > 0) {
							excelLink.attr("href", (totalRecords == 0) ? "#" : "../AccessControl/ProfilesHistoryToFile/?isPdf=false");
							excelLink.attr("target", (totalRecords == 0) ? "_self" : "_blank");
							pdfLink.attr("href", (totalRecords == 0) ? "#" : "../AccessControl/ProfilesHistoryToFile/?isPdf=true");
							pdfLink.attr("target", (totalRecords == 0) ? "_self" : "_blank");
						}

						if (totalRecords == 0) {
							emptyArea.html(emptyDataMessage);
							emptyArea.show();
							tableArea.hide();
						}
						else {
							tableArea.show();
							emptyArea.hide();

							var totalPerPage = this.fnSettings()._iDisplayLength;

							var totalPages = Math.ceil(totalRecords / totalPerPage);

							var currentIndex = parseInt(this.fnSettings()._iDisplayStart);
							var currentPage = totalPages > 0 ? Math.ceil(1 + (currentIndex / totalPerPage)) : 0;

							$("#historyList_info").html("Exibindo " + currentPage + " de " + totalPages);

							if (totalRecords > 0 && totalPages > 1) {
								$("#historyList_info").css("display", "");
								$("#historyList_length").css("display", "");
								$("#historyList_paginate").css("display", "");
							}
							else {
								$("#historyList_info").css("display", "none");
								$("#historyList_length").css("display", "none");
								$("#historyList_paginate").css("display", "none");
							}
							$('select', '#historyList_length').styleInputs();
			            }
			            // Adiciona a classe 'ultimo' à última tr no corpo da tabela
			            $('table tbody tr:last-child').addClass('ultimo');

			            // Adiciona a classe 'ultimo' às últimas th e td no corpo da tabela
			            $('table tr td:last-child').addClass('ultimo');
			            $('table tr th:last-child').addClass('ultimo');
						$("#MsgBox").center();
					},
					"bProcessing": false,
					"fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
					    $('#historyList_first').css("visibility", "hidden");
					    $('#historyList_last').css("visibility", "hidden");
					},
					"bRetrieve": true,
					"bServerSide": true,
					"sAjaxSource": "../DataTables/AccessControl_ProfilesHistory",
					"fnServerData": function (sSource, aoData, fnCallback) {
						$.ajax({
							cache: false,
							dataType: 'json',
							type: "POST",
							url: sSource,
							data: aoData,
							success: fnCallback
						});
					}
				};

				oTable = $("#historyList").dataTable(obj);
				oTable.fnClearTable();
				oTable.fnDraw();
			}
		});
		return false;
	});

}
