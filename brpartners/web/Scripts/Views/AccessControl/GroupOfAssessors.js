﻿/// <reference path="../../References.js" />

var oTable;
var emptyDataMessage = "Sua pesquisa não encontrou resultados válidos. Tente novamente.";
var filterPage = 1;

var fnSearch = function (filterContext) {
    var ddlGroups = $("#ddlGroupsOfAssessors", filterContext);
    var txtAssessors = $("#txtAssessors", filterContext);
    var txtUsers = $("#txtUsers", filterContext);
    var feedbackID = $("#hdfNewGroupID").val();

    $.ajax({
        cache: false,
        async: false,
        type: "POST",
        url: "/AccessControl/LoadGroupOfAssessors",
        data: { GroupId: ddlGroups.val(), UserName: txtUsers.val(), Assessors: txtAssessors.val() },
        success: function () {
            obj = {};
            obj = {
                "bSort": true,
                "bAutoWidth": false,
                "bInfo": true,
                "bSort": true,
                "oLanguage": {
                    "sProcessing": "<img src='/img/loading.gif'>",
                    "sLengthMenu": "<span>Exibir:</span> _MENU_ <span>por página</span>",
                    "sZeroRecords": "Sua pesquisa não encontrou resultados válidos. Tente novamente.",
                    "oPaginate": {
                        "sPrevious": "Anteriores",
                        "sNext": "Próximos"
                    }
                },
                "aoColumns": [
										null,
										{ "bSortable": false },
										{ "bSortable": false },
										{ "bSortable": false }
										],
                "sPaginationType": "full_numbers",
                "sDom": 'rtip',
                "fnDrawCallback": function () {
                    var totalRecords = this.fnSettings().fnRecordsDisplay();
                    var emptyArea = $("#emptyArea");
                    var tableArea = $("#tableArea");

                    var excelLink = $("#lnkGroupsExcel");
                    var pdfLink = $("#lnkGroupsPdf");

                    if (excelLink.length > 0 && pdfLink.length > 0) {
                        excelLink.attr("href", (totalRecords == 0) ? "#" : "../AccessControl/GroupOfAssessorsToFile/?isPdf=false");
                        excelLink.attr("target", (totalRecords == 0) ? "_self" : "_blank");
                        pdfLink.attr("href", (totalRecords == 0) ? "#" : "../AccessControl/GroupOfAssessorsToFile/?isPdf=true");
                        pdfLink.attr("target", (totalRecords == 0) ? "_self" : "_blank");
                    }

                    if (totalRecords == 0) {
                        emptyArea.html(emptyDataMessage);
                        emptyArea.show();
                        tableArea.hide();
                    }
                    else {
                        tableArea.show();
                        emptyArea.hide();

                        var totalPerPage = this.fnSettings()._iDisplayLength;

                        var totalPages = Math.ceil(totalRecords / totalPerPage);

                        var currentIndex = parseInt(this.fnSettings()._iDisplayStart);
                        var currentPage = totalPages > 0 ? Math.ceil(1 + (currentIndex / totalPerPage)) : 0;

                        $("#GroupOfAssessorsList_info").html("Exibindo " + currentPage + " de " + totalPages);

                        if (totalRecords > 0 && totalPages > 1) {
                            $("#GroupOfAssessorsList_info").css("display", "");
                            $("#GroupOfAssessorsList_length").css("display", "");
                            $("#GroupOfAssessorsList_paginate").css("display", "");
                        }
                        else {
                            $("#GroupOfAssessorsList_info").css("display", "none");
                            $("#GroupOfAssessorsList_length").css("display", "none");
                            $("#GroupOfAssessorsList_paginate").css("display", "none");
                        }
                        $('select', '#GroupOfAssessorsList_length').styleInputs();
                    }
                    // Adiciona a classe 'ultimo' à última tr no corpo da tabela
                    $('table tbody tr:last-child', tableArea).addClass('ultimo');

                    // Adiciona a classe 'ultimo' às últimas th e td no corpo da tabela
                    $('table tr td:last-child', tableArea).addClass('ultimo');
                    $('table tr th:last-child', tableArea).addClass('ultimo');
                },
                "bProcessing": false,
                "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                    $('#GroupOfAssessorsList_first').css("visibility", "hidden");
                    $('#GroupOfAssessorsList_last').css("visibility", "hidden");
                },
                "bRetrieve": true,
                "bServerSide": true,
                "sAjaxSource": "../DataTables/AccessControl_GroupOfAssessors",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    $.ajax({
                        cache: false,
                        dataType: 'json',
                        type: "POST",
                        url: sSource,
                        data: aoData,
                        success: fnCallback
                    });
                },
                "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                    var groupIdData = aData[3];
                    if (feedbackID != "" && feedbackID != "0" && groupIdData.indexOf("linkId" + feedbackID + "_Check") > 0)
                        nRow.className = nRow.className + ' selected';
                    return nRow;
                }
            };

            oTable = $("#GroupOfAssessorsList").dataTable(obj);
            oTable.fnClearTable();
            oTable.fnDraw();
        }
    });

    return false;
}

$(function () {
    //$(document).ready(function () {
    $("#closeFeedback").click(function () {
        $("#groupOfAssessorsFeedback").slideToggle();
    });

    $("#btnNewGroup").unbind("click");
    $("#btnNewGroup").click(function () {
        commonHelper.OpenModal("/AccessControl/EditGroupOfAssessors/0", 694, ".content", "");
        //$("#MsgBox").center();

    });

    var filterContext = $("#groupOfAssessorsFilterContent")

    $("#btnSearch", filterContext).click(function () {
        fnSearch(filterContext);
        $(filterContext).slideToggle('fast');
        return false;
    });

    setTimeout(function () {
        fnSearch(filterContext);
        return false;
    }, 250);

    $(filterContext).bind('keydown', function (event) {
        if (event.which == 13) {
            fnSearch(filterContext);
            $(filterContext).slideToggle('fast');
            return false;
        }
    });
    // });
});


function ViewGroup(id) {
    commonHelper.OpenModal("/AccessControl/GroupOfAssessorsDetails/" + id, 694, ".content", "");
}

function EditGroup(id) {

    commonHelper.OpenModal("/AccessControl/EditGroupOfAssessors/?id=" + id, 694, ".content", "");
    //$("#MsgBox").center();
}

function DeleteGroup(id, name) {
    var url = "/AccessControl/ConfirmGroupOfAssessorsDelete/?groupId=" + id + "&groupName=" + escape(name);
    commonHelper.OpenModal(url, 450, ".content", "");
    //$("#MsgBox").center();
}

function ViewHistory() {
    commonHelper.OpenModal("/AccessControl/GroupsOfAssessorsHistory", 671, ".content", "");
}