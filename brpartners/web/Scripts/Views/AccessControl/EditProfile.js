﻿$(function () {

    var context = $("#frmEditProfile");
    var tree = $("#treeFuncionalities");
    tree.treeview();

    $("input[type=checkbox]", tree).click(function () {

        // seleciona todos os filhos do pai marcado
        var parentLi = $(this).parents('li:first');
        parentLi.find(':checkbox').attr('checked', this.checked);

        if (this.checked) {
            // expande os filhos primogênitos se for selecionar
            $(this).siblings('ul:first').css("display", "block");

            // seleciona todos os pais do filho marcado
            $(this).parents().children(':checkbox').attr('checked', this.checked);

            // exibe mensagem se o item marcado possuir filhos
            if (parentLi.children('ul').size() > 0) {
                //alert("Todos os filhos serão selecionados.");
            }
        }
        else // recolhe os filhos quando desmarca
            $(this).siblings('ul:first').css("display", "none");

    });

    $("#txtUserName", "#incluirNovoUsuario").autocomplete({
        source: function (request, response) {
            var data = "";

            data += $(context).serialize();
            if (data != "") data += "&";
            data += "term=" + request.term;
            data += "&isProfile=1";

            $.ajax({
                url: "/AccessControl/UserAutoSuggest",
                type: "POST",
                dataType: "json",
                data: data,
                success: function (data) {
                    $("#hdfSelectedUser", "#incluirNovoUsuario").val("");
                    response($.map(data, function (item) {
                        return {
                            label: item.Name,
                            value: item.Name,
                            id: item.ID
                        }
                    }));
                }
            });
        },
        minLength: 1,
        select: function (event, ui) {
            $("#hdfSelectedUser", "#incluirNovoUsuario").val(ui.item.id);
        }
    });


    $("#lnkAddUser", context).click(function () {
        $("#feedbackError", context).hide();
        var userName = $("#txtUserName", "#incluirNovoUsuario").val();
        var userId = $("#hdfSelectedUser", "#incluirNovoUsuario").val();
        var data = "";

        data += $(context).serialize();
        if (data != "") data += "&";
        data += "id=" + userId + "&name=" + userName;

        $.post("/AccessControl/IncludeUserInList/", data, function (data) {
            if (data.error != "") {
                $("#feedbackError", context).html(data.error);
                $("#feedbackError", context).show();
                return false;
            }
            else {


                $("#listaUsuarios").append("<li/>");

                $("#listaUsuarios li:last").attr("id", "usuario" + data.user.ID);
                $("#listaUsuarios li:last").append("<a onclick=\"DeleteUserFromList(" + data.user.ID + ");return false;\">excluir</a>");
                $("#listaUsuarios li:last").append("<input type=hidden />");
                $("#listaUsuarios li:last").append(data.user.Name);

                $("#listaUsuarios li a:last").attr("class", "ico-excluir");

                $("#listaUsuarios li input:last").attr("value", data.user.ID);
                $("#listaUsuarios li input:last").attr("name", "userID");
                $("#listaUsuarios li input:last").attr("id", "userID" + data.user.ID);

                $("#txtUserName", "#incluirNovoUsuario").val("");
                $("#hdfUser", "#incluirNovoUsuario").val("");

            }
        }, "json");
    });

    $("#lnkSaveProfile").click(function () {
        var profileId = $("#hdfProfileID").val();
        //var profileName = $("#txtProfileName").val();
        var listOfElements = GetCheckedElements();

        var data = "";

        data += $(context).serialize();
        if (data != "") data += "&";
        data += "listOfElements=" + listOfElements;

        //if (feedback == "") {

        $.ajax({
            url: "/AccessControl/UpdateProfile",
            type: "POST",
            dataType: "json",
            data: data,
            success: function (data) {
                if (data.error == "") {
                    if (profileId > 0) {

                        var editedTr = $("#linkId" + data.profile.ID + "_Check").parents('tr:first');
                        editedTr.addClass("selected");

                        var editedTds = $("td", editedTr);
                        if (editedTds.length == 3) {
                            $(editedTds[0]).html(data.profile.Name);
                            $(editedTds[1]).html(data.totalUsers);
                        }

                        $("#profileFeedback").find("span").html("Perfil editado com sucesso.");
                        $("#profileFeedback").show();
                        Modal.Close();
                    }
                    else {
                        //document.location = "/AccessControl/Profiles";
                        Modal.Close();
                        $("#profileFeedback").find("span").html("Perfil cadastrado com sucesso.");
                        $("#profileFeedback").show();
                        $("#btnSearch", "#profileFilterContent").click();
                    }
                }
                else {
                    $("#feedbackError", context).html(data.error);
                    $("#feedbackError", context).show();
                    return false;
                }
            }
        });
    });


    $(document).ready(function () {
        CheckProfilePermissions();
    });

});

function DeleteUserFromList(id) {
    var userItem = $("#usuario" + id, "#listaUsuarios");
    userItem.remove();
}

function GetCheckedElements() {

    var tree = $("#treeFuncionalities");
    var lstIDs = "";
    $("[id ^= chk]",tree).each(function () {
        var obj = this;
        if ($(this).is(':checked')) {
            var id = this.id;
            lstIDs = lstIDs + id.replace("chk", "") + ",";
        }
    })
    return lstIDs;
}

function CheckProfilePermissions() {
    var tree = $("#treeFuncionalities");
    var values = $("#hdfProfileElements").val();
    if (values != "") {
        var v = values.split(',');

        for (i = 0; i <= v.length; i++) {
            if (v[i] != undefined) {
                var obj = $('input[id=chk' + v[i] + ']',tree);
                obj.attr('checked', true);
            }
        }
    }
}