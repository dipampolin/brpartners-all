﻿/// <reference path="../../References.js" />

var oTable;
var emptyDataMessage = "Sua pesquisa não encontrou resultados válidos. Tente novamente.";
var filterPage = 1;
var previousIndex = -1;
var nextIndex = 1;

var feedbackCount = 1;
var fnSearch;
var reload = false;

$(function () {
    $(document).ready(function () {

        var lnkPrevious = $("#lnkPreviousClient");
        var lnkNext = $("#lnkNextClient");
        var lnkPreviousTop = $("#lnkPreviousClientTop");
        var lnkNextTop = $("#lnkNextClientTop");

        var filterContext = $("#guaranteesFilterContent");

        $("#guaranteesFilter").click(function () {
            $('#guaranteesFilterContent').slideToggle('fast');
            $('select').styleInputs();
            $(this).toggleClass('aberto');
        });

        $("#guaranteesFilterContent").bind('keydown', function (event) {
            if (event.which == 13) {
                fnSearch(true);
                return false;
            }
        });

        $("#txtStartDate", filterContext).setMask("99/99/9999").datepicker().datepicker($.datepicker.regional['pt-BR']);
        $("#txtEndDate", filterContext).setMask("99/99/9999").datepicker().datepicker($.datepicker.regional['pt-BR']);
        $.datepicker.setDefaults($.datepicker.regional['pt-BR']);

        $("#chkAll").click(function () {
            var check = $(this).is(':checked');
            $("table#guaranteesList input:checkbox").each(function () { if (!$(this).is(':disabled')) { $(this).attr('checked', check); } })
        });

        fnSearch = function (actionClick) {
            if (feedbackCount == 0)
                $("#feedbackError").hide();

            lnkPrevious.hide();
            lnkNext.hide();
            lnkPreviousTop.hide();
            lnkNextTop.hide();

            $("#emptyArea").hide();
            $("#tableArea").hide();
            $("#loadingArea").show();

            $("#filterFeedbackError", filterContext).hide();

            if (filterContext.css("display") != "none") {
                $('#guaranteesFilterContent').slideToggle('fast');
                $('select').styleInputs();
                $(filterContext).toggleClass('aberto');
            }

            var inGuarantee = $("#ddlStatus", filterContext).val() == "-1";

            var status = commonHelper.GetParameterByName("status");
            if (status != null && status != undefined && status != "" && !actionClick) {
                $("#ddlStatus", filterContext).val(status);
            }

            $.ajax({
                cache: false,
                type: "POST",
                dataType: "json",
                async: true,
                url: "/Risk/LoadGuarantees",
                data: { Assessors: $("#txtAssessor", filterContext).val(), Clients: $("#txtClient", filterContext).val(), StatusID: $("#ddlStatus", filterContext).val(), StartDate: $("#txtStartDate", filterContext).val(), EndDate: $("#txtEndDate", filterContext).val(), Index: reload ? $("#listIndex").val() : 0 },
                success: function (data) {

                    feedbackCount = 0;

                    if (reload) {
                        reload = false;
                    }

                    $('input[id=selectAll]').attr('checked', false);

                    $("#loadingArea").hide();

                    var emptyData = (data.HtmlItems == null || data.HtmlItems.length == 0);

                    var excelLink = $("#lnkGuaranteesExcel");
                    var pdfLink = $("#lnkGuaranteesPdf");
                    excelLink.attr("href", (emptyData) ? "#" : "/Risk/GuaranteesToFile/?isPdf=false");
                    //excelLink.attr("target", (emptyData) ? "_self" : "_blank");
                    pdfLink.attr("href", (emptyData) ? "#" : "/Risk/GuaranteesToFile/?isPdf=true");
                    //pdfLink.attr("target", (emptyData) ? "_self" : "_blank");

                    if (oTable != undefined)
                        oTable.fnClearTable();

                    if (emptyData) {
                        $("#emptyArea").html(inGuarantee ? "Nenhuma garantia encontrada." : "Nenhuma solicitação encontrada.");
                        $("#emptyArea").show();
                        $("#tableArea").hide();
                    }
                    else {
                        $("#guaranteesList").find("tbody").html(data.HtmlItems);
                        $("#lblClientNameSpan").html(data.Client);
                        $("#lblAssessor").html(data.Assessor);
                        $("#lblClientCounter").html(data.Legend);
                        $("#lblClientCounterTop").html(data.Legend);

                        $("#listIndex").val(data.Index);

                        $("#tbGuaranteeBalance").find("tbody").html(data.HtmlBalanceItems);

                        var modalSituacao = $('.modal-situacao');
                        $('.exibir-modal').click(function () {
                            modalSituacao.show();

                            var guaranteeId = parseInt(this.id.replace("lnkRealize", ""));
                            $("#guaranteesIds", $(".modal-situacao")).val(guaranteeId);
                            var p = $(this);
                            var offset = p.offset();
                            $('.modal-situacao').css("top", offset.top);
                            $('.modal-situacao select').styleInputs();
                        });
                        $('.closeLink').bind({
                            "click": function () {
                                modalSituacao.hide();
                            }
                        });

                        previousIndex = data.Index - 1;
                        nextIndex = data.Index + 1;

                        var length = data.Length;

                        if (length < 2) {
                            lnkPrevious.hide();
                            lnkNext.hide();
                            lnkPreviousTop.hide();
                            lnkNextTop.hide();
                        }
                        else {
                            if (data.Index == (length - 1)) {
                                lnkNext.hide();
                                lnkPrevious.show();
                                lnkPreviousTop.show();
                                lnkNextTop.hide();
                            }
                            else {
                                if (data.Index != 0) {
                                    lnkPrevious.show();
                                    lnkPreviousTop.show();
                                    
                                }
                                else {
                                    lnkPrevious.hide();
                                    lnkPreviousTop.hide();
                                }
                                lnkNext.show();
                                lnkNextTop.show();
                            }
                        }

                        if (data.InGuarantee)
                            $(".InGuaranteeColumn").hide();
                        else
                            $(".InGuaranteeColumn").show();

                        SetDataTable();
                        $("#tableArea").show();
                        $("#emptyArea").hide();
                    }
                }
            });
        };

        $("#btnSearch", filterContext).click(function () {
            fnSearch(true);
            return false;
        });

        LoadResults();

    });
});


function GetClientGuarantees(next) {
    var lnkPrevious = $("#lnkPreviousClient");
    var lnkNext = $("#lnkNextClient");
    var lnkPreviousTop = $("#lnkPreviousClientTop");
    var lnkNextTop = $("#lnkNextClientTop");

    lnkPrevious.hide();
    lnkNext.hide();
    lnkPreviousTop.hide();
    lnkNextTop.hide();

    $("#tableArea").hide();

    $.ajax({
        cache: false,
        type: "POST",
        dataType: "json",
        async: true,
        url: "/Risk/LoadNextGuarantees",
        data: { Index: !next ? previousIndex : nextIndex, InGuarantee: ($("#ddlStatus", $("#guaranteesFilterContent")).val() == "-1") },
        success: function (data) {
            if (oTable != undefined)
                oTable.fnClearTable();
            $("#guaranteesList").find("tbody").html(data.HtmlItems);
            $("#lblClientNameSpan").html(data.Client);
            $("#lblAssessor").html(data.Assessor);
            $("#lblClientCounter").html(data.Legend);
            $("#lblClientCounterTop").html(data.Legend);

            $("#tbGuaranteeBalance").find("tbody").html(data.HtmlBalanceItems);

            $("#listIndex").val(data.Index);

            var modalSituacao = $('.modal-situacao');
            $('.exibir-modal').click(function () {
                modalSituacao.show();

                var guaranteeId = parseInt(this.id.replace("lnkRealize", ""));
                $("#guaranteesIds", $(".modal-situacao")).val(guaranteeId);
                var p = $(this);
                var offset = p.offset();
                $('.modal-situacao').css("top", offset.top);
                $('.modal-situacao select').styleInputs();
            });
            $('.closeLink').bind({
                "click": function () {
                    modalSituacao.hide();
                }
            });

            previousIndex = data.Index - 1;
            nextIndex = data.Index + 1;

            var length = data.Length;

            if (length < 2) {
                lnkPrevious.hide();
                lnkNext.hide();
                lnkPreviousTop.hide();
                lnkNextTop.hide();
            }
            else {
                if (data.Index == (length - 1)) {
                    lnkNext.hide();
                    lnkPrevious.show();
                    lnkPreviousTop.show();
                    lnkNextTop.hide();
                }
                else {
                    if (data.Index == 0) {
                        lnkPrevious.hide();
                        lnkPreviousTop.hide();
                    }
                    else {
                        lnkPrevious.show();
                        lnkPreviousTop.show();
                    }
                    lnkNext.show();
                    lnkNextTop.show();
                }
            }

            var inGuarantee = $("#ddlStatus", $("#guaranteesFilterContent")).val() == "-1";

            if (inGuarantee)
                $(".InGuaranteeColumn").hide();
            else
                $(".InGuaranteeColumn").show();
            SetDataTable();
            $("#tableArea").show();
        }
    });
    return false;
}

function ViewHistory() {
    commonHelper.OpenModal("/Risk/GuaranteesHistory", 671, ".content", "");
}

function OpenRequest(id) {
    commonHelper.OpenModal("/Risk/GuaranteesRequest/" + id, 645, ".content", "");
}

function DeleteGuarantee(id) {
    commonHelper.OpenModal("/Risk/GuaranteeConfirmDelete/?guaranteeId=" + id, 330, ".content", "");
}

function ValidateFilterFields(context) {
    var messageError = "";
    if (!validateHelper.ValidateList($("#txtAssessors", context).val()))
        messageError += "<li>Faixa de assessores inválida.</li>";
    if (!validateHelper.ValidateList($("#txtClient", context).val()))
        messageError += "<li>Faixa de clientes inválida.</li>";

    if (messageError.length > 0)
        messageError = "<ul>" + messageError + "</ul>";

    return messageError;
}

function ValidateStatusChange() {
    var ddlChange = $("#ddlStatusChange", ".modal-situacao").val();
    if (ddlChange != "") {
        $(".closeLink", ".modal-situacao").click();
        DoRealize();
    }
    else {
        $("#feedbackStatus", ".modal-situacao").show();
        return false;
    }
}

function OpenRealize() {
    var checkedGuarantees = $("[id^=chkGuarantee]:checked");
    if ($("[id^=chkGuarantee]:checked").length > 0) {
        var ids = "";
        $(checkedGuarantees).each(function () {
            ids += this.value + ";";
        });
        var url = "/Risk/GuaranteeConfirmRealize/?guaranteesIds=" + ids;
        commonHelper.OpenModal(url, 450, ".content", "");
    }
}

function OpenGuaranteeHistory(id) {
    var url = "/Risk/GuaranteesHistoryDetail/?guaranteeID=" + id;
    commonHelper.OpenModal(url, 671, ".content", "");
}

function SetDataTable() {
    oTable = $('#guaranteesList').dataTable(
		                        {
		                            "bDestroy": true,
		                            "bPaginate": false,
		                            "bLengthChange": false,
		                            "bFilter": false,
		                            "bInfo": false,
		                            "bAutoWidth": false,
		                            "aaSorting": [[1, "asc"]],
		                            "aoColumns": [
                                                { "bSortable": false },
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                                { "bSortable": false }
                                    ],
		                            "sPaginationType": "full_numbers"
		                        });
}


function LoadResults() {
    setTimeout(function () {
        fnSearch(false);
        ReloadPending();
    }, 250);
}

function ReloadResults() {
    setTimeout(function () {
        reload = true;
        fnSearch(false);
        ReloadPending();
    }, 250);
}


function ReloadPending() {
    $.post("/Home/GetPending", { module: "Risk.Guarantees" }, function (data) {
        if (data > 0) {

            var pendingContent = "<span><strong>" + data + "</strong> Pendente" + (data > 1 ? "s" : "") + "</span>";

            if ($("#pendingTag").length > 0) {
                $("#pendingTag").html(pendingContent);
            }
            else {
                var pending = "<div id='pendingTag' class='aviso' style='display: none;'>" + pendingContent + "</div>";
                if ($('#trilha').children('ul').find('li').last().length > 0)
                    $('#trilha').children('ul').find('li').last().append(pending);
                else
                    $('#trilha').children('ul').append(pending);
            }
            $("#pendingTag").show();
        }
        else {
            $("#pendingTag").hide();
        }
    }, "json");
}

function DoRealize() {
    $.post("/Risk/RealizeGuarantees", { guaranteesIds: $("#guaranteesIds", "#frmGuaranteeConfirmRealize").val(), ddlStatusChange: $("#ddlStatusChange", "#frmGuaranteeConfirmRealize").val() }, function (data) {
        if (data.success) {
            $("span", "#feedback").html(data.message);
            $("#feedback").show();
            ReloadResults();
        }
        else {
            $("#FeedbackError").html(data.message);
            $("#FeedbackError").show();
        }
        Modal.Close();
    }, "json");
}


function GetClientGuaranteesByIndex(index) {
    var lnkPrevious = $("#lnkPreviousClient");
    var lnkNext = $("#lnkNextClient");
    var lnkPreviousTop = $("#lnkPreviousClientTop");
    var lnkNextTop = $("#lnkNextClientTop");

    lnkPrevious.hide();
    lnkNext.hide();
    lnkPreviousTop.hide();
    lnkNextTop.hide();

    $("#tableArea").hide();

    $.ajax({
        cache: false,
        type: "POST",
        dataType: "json",
        async: true,
        url: "/Risk/LoadNextGuarantees",
        data: { Index: (nextIndex - 1), InGuarantee: ($("#ddlStatus", $("#guaranteesFilterContent")).val() == "-1") },
        success: function (data) {
            if (oTable != undefined)
                oTable.fnClearTable();
            $("#guaranteesList").find("tbody").html(data.HtmlItems);
            $("#lblClientNameSpan").html(data.Client);
            $("#lblAssessor").html(data.Assessor);
            $("#lblClientCounter").html(data.Legend);
            $("#lblClientCounterTop").html(data.Legend);

            $("#tbGuaranteeBalance").find("tbody").html(data.HtmlBalanceItems);

            var modalSituacao = $('.modal-situacao');
            $('.exibir-modal').click(function () {
                modalSituacao.show();

                var guaranteeId = parseInt(this.id.replace("lnkRealize", ""));
                $("#guaranteesIds", $(".modal-situacao")).val(guaranteeId);
                var p = $(this);
                var offset = p.offset();
                $('.modal-situacao').css("top", offset.top);
                $('.modal-situacao select').styleInputs();
            });
            $('.closeLink').bind({
                "click": function () {
                    modalSituacao.hide();
                }
            });

            previousIndex = data.Index - 1;
            nextIndex = data.Index + 1;

            var length = data.Length;

            if (length < 2) {
                lnkPrevious.hide();
                lnkNext.hide();
                lnkPreviousTop.hide();
                lnkNextTop.hide();
            }
            else {
                if (data.Index == (length - 1)) {
                    lnkNext.hide();
                    lnkPrevious.show();
                    lnkPreviousTop.show();
                    lnkNextTop.hide();
                }
                else {
                    if (data.Index == 0) {
                        lnkPrevious.hide();
                        lnkPreviousTop.hide();
                    }
                    else {
                        lnkPrevious.show();
                        lnkPreviousTop.show();
                    }
                    lnkNext.show();
                    lnkNextTop.show();
                }
            }

            var inGuarantee = $("#ddlStatus", $("#guaranteesFilterContent")).val() == "-1";

            if (inGuarantee)
                $(".InGuaranteeColumn").hide();
            else
                $(".InGuaranteeColumn").show();
            SetDataTable();
            $("#tableArea").show();
        }
    });
    return false;
}