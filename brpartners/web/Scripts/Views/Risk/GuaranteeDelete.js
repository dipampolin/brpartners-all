﻿$(document).ready(function () {

    var context = $("#frmConfirmGuaranteeDelete");

    $("#btnDelete", context).click(function () {
        $.post("/Risk/GuaranteeDelete", { hdfGuaranteeId: $("#hdfGuaranteeId", context).val() }, function (data) {
            if (data.success) {
                $("span", "#feedback").html(data.message);
                $("#feedback").show();
                ReloadResults();
            }
            else {
                $("#FeedbackError").html(data.message);
                $("#FeedbackError").show();
            }
            Modal.Close();
        }, "json");
    });

});
