﻿function approveCliente() {
    $.ajax({
        url: '/Account/ApproveClient/',
        type: 'GET',
        datatype: 'json'
    });

    $('#dialog-form-finish-client').show();
}

function disapproveClient() {
    $.ajax({
        url: '/Account/DisapproveClient/',
        type: 'GET',
        datatype: 'json'
    });

    $('#dialog-form-finish-client').show();
}