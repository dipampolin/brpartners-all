﻿/// <reference path="../../References.js" />

$(function () {

    var context = $("#filterArea");

    $("#btnRequest", context).click(function () {
    	var email = $("#txtEmail", context).val();
        var error = $("#feedbackRequestError", context);

        if (email == "") {
        	$(error).html("<ul><li>Insira um e-mail válido.</li></ul>");
            $(error).show();
            return false;
        }
        else {
        	var message = ValidateEmail(email);
            if (message != "") {
                $(error).html(message);
                $(error).show();
            }
            else
                $("form").submit();
        }
    });

});

function ValidateEmail(email) {
    var messageError = "";
    var formatoEmail = new RegExp("^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,3})$");

	if (!formatoEmail.test(email))
    	messageError += "<li>Insira um e-mail válido.</li>";

    if (messageError.length > 0)
        messageError = "<ul>" + messageError + "</ul>";

    return messageError;
}