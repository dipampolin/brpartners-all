﻿/// <reference path="../../knockoutjs/knockout-3.2.0.js" />
/// <reference path="../../knockoutjs/knockout.mapping-latest.js" />
/// <reference path="../../knockoutjs/knockout.validation.js" />
/// <reference path="../../modernizr-2.8.3.js" />


function newId() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}


function toNumber(x) {
    switch (typeof x) {
        case 'number':
            return x;
        case 'string':
            return Number(x.replace(/[^\d\,\.]/g, '').replace(',', '.')); break;
        default:
            debugger;
            return NaN;
    }
}


function EditorSuitability() {
    var me = this;


    this.idFormulario = ko.observable($("#idFormulario").val());

    this.questoesRemovidas = [];
    this.alternativasRemovidas = [];
    this.colunasRemovidas = [];


    this.modoProtegido = ko.observable(false);
    this.salvamentoPendente = ko.observable();

    window.onbeforeunload = function (e) {
        if (me.salvamentoPendente()) {
            return 'Este formulário contém questões que ainda não foram salvas. Tem certeza que deseja sair desta página? Todas as alterações que ainda não foram salvas serão perdidas!';
        }
    };

    this.validationSummary = ko.observableArray([]);
    this.globalValidationSummary = ko.observableArray([]);

    this.enunciadoOk = ko.observable(true);
    this.pesoOk = ko.observable(true);
    this.tipoQuestaoOk = ko.observable(true);
    this.percentualMinimoOk = ko.observable(true);

    this.nomeFormularioOk = ko.observable(true);



    this.modoVisualizacao = ko.observable('carregando');
    this.textoCarregando = ko.observable('Carregando...');




    this.modoVisualizacao.subscribe(function () {

        document.body.scrollTop = 0;

        setTimeout(function () {
            $('select').styleInputs();
        }, 1);
    });

    this.acaoAviso = ko.observable('C');

    this.formulario = ko.observable();

    this.questaoCorrente = ko.observable();
    this.indiceQuestaoCorrente = ko.observable();
    this.questoesSelecionaveis = ko.observableArray([]);



    this.totalQuestoes = ko.pureComputed(function () {
        var x = 0;
        for (var i = 0; i < me.formulario().Questoes().length; i++) {
            var q = me.formulario().Questoes()[i];
            if (q.QuestaoAtiva())
                x++;
        }
        return x;
    });

    this.pontuacoesOk = ko.observable([true, true, true]);


    this.formulario.subscribe(function (novo) {
        if (novo) {
            var pontuacoes = novo.Pontuacao();
            for (var i = 0; i < pontuacoes.length; i++) {
                var p = pontuacoes[i];

                p.Maximo.subscribe(function () {
                    var pontuacao = this;

                    var i = me.formulario().Pontuacao.indexOf(pontuacao);
                    if (i >= 0) {
                        var j = i + 1;
                        var posterior = me.formulario().Pontuacao()[j];
                        if (posterior) {
                            var min = pontuacao.Maximo();
                            min = toNumber(min) + 1;
                            posterior.Minimo(min);
                        }
                    }

                }.bind(p));

            }
        }
        setTimeout(function () {
            $(".pontuacoes input.freeform").mask('9?99')

        }, 1);
    });


    this.calcularPontuacaoQuestao = function (q) {

        var type = q.Tipo();
        /*
        if (type == 'M')
        {
            console.log(q.Enunciado());
            console.log(q);
        }
        */

        var distribuicao = [];
        var pontos = 0;
        for (var j = 0; j < q.Alternativas().length; j++) {

            var a = q.Alternativas()[j];
            if (a.Ativa()) {

                if (type === 'D')
                {
                    distribuicao.push(toNumber(a.Pontos()));
                }
                else if (type === 'M')
                {
                    //console.log(a);
                    //console.log(toNumber(a.Pontos()));
                    pontos += toNumber(a.Pontos());
                }
                else
                {
                    if (type === 'C') {
                        pontos += toNumber(a.Pontos());
                    }
                    else if (type === 'R') {
                        pontos = Math.max(pontos, toNumber(a.Pontos()));
                    }

                    pontos = Math.max(pontos, toNumber(a.Pontos()));
                }

                /*
                switch (q.Tipo()) {
                    case 'R':
                        pontos = Math.max(pontos, toNumber(a.Pontos()));
                        break;
                    case 'C':
                        pontos += toNumber(a.Pontos());
                        break;
                    case 'D':
                         pontos += toNumber(a.Pontos());
                        break;
                    case 'M':
                        pontos += toNumber(a.Pontos());
                        break;
                }

                pontos = Math.max(pontos, toNumber(a.Pontos()));
                */
            }
        }

        if (type == 'D')
        {
            distribuicao = distribuicao.reverse();
            var items = 100 / toNumber(q.PercentualMinimo());
            // console.log(toNumber(q.PercentualMinimo()));
            // console.log(items);
            for (var i = 0; i <= items - 1; i++) {
                if (distribuicao[i] != undefined || distribuicao[i] != null) {
                    pontos = pontos + distribuicao[i];
                }
            }
        }

        return pontos * toNumber(q.Peso());
    }

    this.pontuacaoMinima = ko.pureComputed(function () {
        var total = 0;

        if (me.formulario()) {

            for (var i = 0; i < me.formulario().Questoes().length; i++) {
                var q = me.formulario().Questoes()[i];

                if (q.QuestaoAtiva()) {
                    var menor = Number.MAX_VALUE;
                    for (var j = 0; j < q.Alternativas().length; j++) {
                        var a = q.Alternativas()[j];
                        if (a.Ativa()) {
                            menor = Math.min(toNumber(a.Pontos()), menor);
                        }
                    }
                    total += (menor * toNumber(q.Peso()));
                }
            }
        }
        return total;
    });

    this.pontuacaoMaxima = ko.pureComputed(function () {
        var total = 0;

        if (me.formulario()) {

            for (var i = 0; i < me.formulario().Questoes().length; i++) {
                var q = me.formulario().Questoes()[i];
                if (q.QuestaoAtiva())
                    total += me.calcularPontuacaoQuestao(q);
            }
        }
        return total;

    });



    this.questaoCorrente.subscribe(function (novoValor) {


        if (novoValor) {
            var disponiveis = [];
            for (var i = 0; i < me.formulario().Questoes().length; i++) {
                var q = me.formulario().Questoes()[i];

                if (q.UID() != novoValor.UID())
                    disponiveis.push(q);
            }

            me.questoesSelecionaveis(disponiveis);

            setTimeout(function () {
                $('select').styleInputs();

                $("#respostas .pontos").focus(function () {
                    var input = this;
                    setTimeout(function () {
                        $(input).select();
                    }, 10)

                });

            }, 1);



        }

        setTimeout(function () {
            if (novoValor) {
                $('select').styleInputs();
                me.mostrarModal();
            }
        }, 0);
    });



    this.reload = function () {



        if (me.idFormulario() == '') {
            var novo = {
                Formulario: {
                    Id: -1,
                    Descricao: '',
                    Tipo: '',
                    Status: 'D'
                }, Questoes: [],
                Perfis: [],
                Pontuacao: []
            };

            var perfis = ['Conservador', 'Moderado', 'Arrojado'];
            for (var i = 0; i < perfis.length; i++) {
                novo.Perfis.push({ Id: i + 1, Nome: perfis[i] });
                var pont = { Id: -1, IdPerfil: i + 1, Minimo: null, Maximo: null };
                novo.Pontuacao.push(pont);
            }

            novo = ko.mapping.fromJS(novo);
            me.formulario(novo);
            me.modoProtegido(false);
            me.modoVisualizacao('edicao');

        } else {

            $.ajax({
                url: '/SuitabilityAdmin/AbrirFormulario',
                data: { id: this.idFormulario() },
                type: 'POST',
                success: function (data, status, xhr) {

                    var mapeado = ko.mapping.fromJS(data);

                    me.formulario(mapeado);

                    var regex = /#copia/;
                    if (regex.test(document.location.href)) {
                        me.copiarFormulario();
                    }
                    else {
                        if (data.QuantidadePreenchimentos > 0) {
                            me.modoVisualizacao('copia');
                            me.modoProtegido(true);
                        }
                        else
                            me.modoVisualizacao('edicao');
                    }

                    if (window.console)
                        console.log(data);





                }
            });
        }
    }


    this.copiarFormulario = function () {

        me.formulario().Formulario.Id(-1);
        me.formulario().Formulario.Status('D');
        me.formulario().Formulario.Descricao('');
        for (var i = 0; i < me.formulario().Questoes().length; i++) {
            var q = me.formulario().Questoes()[i];
            q.Id(-1);
            for (var j = 0; j < q.Alternativas().length; j++) {
                var a = q.Alternativas()[j];
                a.Id(-1);
            }
            for (var j = 0; j < q.Colunas().length; j++) {
                var c = q.Colunas()[j];
                c.Id(-1);
            }
        }
        for (var i = 0; i < me.formulario().Pontuacao().length; i++) {
            var p = me.formulario().Pontuacao()[i];
            p.Id(-1);
        }

        me.modoProtegido(false);
        me.modoVisualizacao('edicao');

        document.location.href = '#';

    }

    this.avancarFormulario = function () {
        switch (me.acaoAviso()) {
            case 'E':
                me.modoVisualizacao('edicao'); break;
            case 'C':
                me.copiarFormulario(); break;
            case 'V':
                document.location.href = '/SuitabilityAdmin/Index'; break;
        }
    }

    this.mostrarModal = function () {

        $("#qxModal").fadeIn('fast');

        $(".pontos").mask('9?,9')

    }
    this.esconderModal = function (callback) {
        $("#qxModal").fadeOut('fast', callback);
    }


    this.validarQuestoesDependentes = function (q, errors) {
        var questoesDependentes = me.buscarQuestoesDependentes(q);
        for (var j = 0; j < questoesDependentes.length; j++) {
            var q2 = questoesDependentes[j];
            if (!q2.QuestaoAtiva()) {
                errors.push('A questão "' + me.formatarSubquestao(q) + '" abre a questão "' + me.formatarSubquestao(q2) + '" que não está habilitada.');
            }
            me.validarQuestoesDependentes(q2, errors);
        }
    }

    
    this.validarGeral = function () {
        var errors = [];

        var f = me.formulario();

        if (f.Formulario.Descricao().replace(/\s/g, '') == '') {
            errors.push('Informe o nome do formulário!');
            me.nomeFormularioOk(false);
        }
        else
            me.nomeFormularioOk(true);

        if (f.Questoes().length == 0) {
            errors.push('Cadastre ao menos uma questão.');
        } else if (f.Questoes()[0].Oculta()) {
            errors.push('Uma questão oculta não pode ser a primeira da lista.');
        }
        var algumaHabilitada = false;
        for (var i = 0; i < f.Questoes().length; i++) {
            var q = f.Questoes()[i];
            if (q.QuestaoAtiva() && !q.Oculta()) {
                algumaHabilitada = true;
                me.validarQuestoesDependentes(q, errors);

            }
        }
        if (!algumaHabilitada) {
            errors.push('Todas as questões estão desabilitadas ou são questões ocultas!');
        }


        me.validarPontuacoes(errors);

        me.globalValidationSummary(errors);

        var hasErrors = errors.length > 0;

        if (hasErrors) {
            $("#erros-globais").hide().slideDown('fast');
        }

        return !hasErrors;
    }

    this.validarPontuacoes = function (errors) {

        var ultimo = -1;
        var pontuacaoMaxima = me.pontuacaoMaxima();

        for (var i = 0; i < me.formulario().Pontuacao().length; i++) {
            var p = me.formulario().Pontuacao()[i];

            var min = toNumber((p.Minimo() || 0));
            var max = toNumber((p.Maximo() || Number.MAX_VALUE));

            var pontuacao_ok = false;

            var perfil = me.formulario().Perfis()[p.IdPerfil() - 1];
            if (isNaN(min)) {
                errors.push('Perfil ' + perfil.Nome() + ': informe a pontuação minima.');
            } else if (isNaN(max))
                errors.push('Perfil ' + perfil.Nome() + ': informe a pontuação máxima.');
            else if (min == 0 && max == Number.MAX_VALUE) {
                errors.push('Perfil ' + perfil.Nome() + ': preencha a pontuação para este perfil.')
            } else if (min > 0 && max > 0 && min > max) {
                errors.push('Perfil ' + perfil.Nome() + ': a pontuação máxima deve ser maior que a minima.');
            } else if (min < ultimo) {
                errors.push('Perfil ' + perfil.Nome() + ': a pontuação deste perfil está abaixo da pontuação do perfil anterior.');
            } else if (max < Number.MAX_VALUE && max > pontuacaoMaxima) {
                errors.push('Perfil ' + perfil.Nome() + ': a pontuação máxima excede a pontuação máxima do questionário.');
            } else if (max == Number.MAX_VALUE && min > pontuacaoMaxima) {
                errors.push('Perfil ' + perfil.Nome() + ': a pontuação mínima excede a pontuação máxima do questionário.');
            } else {
                ultimo = max;
                pontuacao_ok = true;
            }
            me.pontuacoesOk()[i] = pontuacao_ok;
        }

        me.pontuacoesOk.notifySubscribers();

        return pontuacao_ok;
    }

    this.salvar = function () {
        if (me.validarGeral()) {

            var obj = ko.mapping.toJS(me.formulario());
            var parametros = {
                Formulario: obj.Formulario,
                Questoes: obj.Questoes,
                Pontuacao: obj.Pontuacao,
                QuestoesRemovidas: me.questoesRemovidas,
                AlternativasRemovidas: me.alternativasRemovidas,
                ColunasRemovidas: me.colunasRemovidas
            };


            for (var i = 0; i < parametros.Questoes.length; i++) {
                var q = parametros.Questoes[i];
                for (var j = 0; j < q.Alternativas.length; j++) {
                    var a = q.Alternativas[j];

                    a.Ordem = j + 1;
                    a.Pontos = toNumber(a.Pontos);
                }
            }

            for (var i = 0; i < parametros.Pontuacao.length; i++) {
                var p = parametros.Pontuacao[i];
                if (typeof p.Minimo == 'string')
                    p.Minimo = toNumber(p.Minimo);
                if (typeof p.Maximo == 'string') {
                    p.Maximo = toNumber(p.Maximo);
                }
            }

            if (window.console)
                console.log(parametros);

            me.textoCarregando('Salvando...');
            me.modoVisualizacao('carregando');

            $.ajax({
                url: '/SuitabilityAdmin/Salvar',
                type: 'POST',
                data: { json: JSON.stringify(parametros) },
                error: function (xhr, status) {
                    alert('Ocorreu um erro  enquanto salvava o formulário!');
                }, success: function (data, status, xhr) {
                    var mapeado = ko.mapping.fromJS(data);
                    me.formulario(mapeado);
                    $("#msg_sucesso").show('fast').delay(5000).hide('fast');

                    me.salvamentoPendente(false);

                }, complete: function () {
                    me.modoVisualizacao('edicao');
                }

            })


        } else {
            document.body.scrollTop = 0;
        }
    }
    this.cancelar = function () {
        document.location.href = '/SuitabilityAdmin/Index';
    }
    this.testar = function () {

        if (me.idFormulario()) {
            var win = window.open('/Suitability/Testar/' + me.idFormulario(), 'testeSuitability');
            win.focus();
        } else {
            alert('Este formulário ainda não foi salvo! Salve o formulário antes de testá-lo.');
        }
    }



    this.subirQuestao = function (q) {
        var questoes = me.formulario().Questoes;
        var indice = questoes.indexOf(q);

        if (indice > 0) {
            var outra = questoes()[indice - 1];
            questoes()[indice] = outra;
            questoes()[indice - 1] = q;

            questoes.notifySubscribers();

            me.reindexarQuestoes();
        }
    }
    this.descerQuestao = function (q) {
        var questoes = me.formulario().Questoes;
        var indice = questoes.indexOf(q);

        if (indice < questoes().length - 1) {
            var outra = questoes()[indice + 1];
            questoes()[indice] = outra;
            questoes()[indice + 1] = q;

            questoes.notifySubscribers();
            me.reindexarQuestoes();
        }
    }

    this.editarQuestao = function (q) {

        me.indiceQuestaoCorrente(me.formulario().Questoes.indexOf(q));

        var obj = ko.mapping.toJS(q);
        var clone = ko.mapping.fromJS(obj);

        me.questaoCorrente(clone);

        me.validarQuestao(me.questaoCorrente());
    }

    this.excluirQuestao = function (q) {

        if (me.modoProtegido()) {
            me.exibirAlertaModoProtegido('q');
            return;
        }




        /*
           verificamos se a questão pode mesmo ser removida.
           ex: se a questão for referenciada por uma alternativa ela não pode ser removida
           */

        var dependencias = me.buscarQuestoesQueDepende(q);
        if (dependencias.length) {
            alert('AVISO! Esta pergunta não pode ser removida agora porque uma ou mais perguntas contém uma alternativa que direciona para esta pergunta!');
            return;
        }


        if (confirm('Deseja excluir a questão "' + q.Enunciado() + '"?')) {


            if (q.Id() > 0) {
                me.questoesRemovidas.push(q.Id());
            }
            me.formulario().Questoes.remove(q);
            me.reindexarQuestoes();
        }
    }

    this.incluirQuestao = function () {
        var q = {
            Id: -1,
            UID: newId(),
            Enunciado: '',
            Alternativas: [],
            Colunas: [],
            DescricaoPercentual: null,
            PercentualMinimo: null,
            Ordem: me.formulario().Questoes().length + 1,
            Peso: 1,
            Oculta: false,
            QuestaoAtiva: true,
            Tipo: 'R',
            NumeroSubQuestao: null
        };
        var mapeado = ko.mapping.fromJS(q);
        me.indiceQuestaoCorrente(-1);
        //me.formulario().Questoes.push(mapeado);

        me.questaoCorrente(mapeado);

    }

    this.reindexarQuestoes = function () {

        var idQuestao = 0, idSubQuestao;

        for (var i = 0; i < me.formulario().Questoes().length; i++) {
            var q = me.formulario().Questoes()[i];

            if (!q.Oculta() || i == 0) {
                idQuestao++;
                idSubQuestao = null;
            }
            else if (q.Oculta()) {
                idSubQuestao = (idSubQuestao || 0) + 1;
            }


            q.Ordem(idQuestao);
            q.NumeroSubQuestao(idSubQuestao);
        }
    }

    this.validarQuestao = function (v) {
        var errors = [];

        var regexPeso = /^\d+$/;

        var enunciadoOk = false, pesoOk = false, tipoQuestaoOk = false, percentualMinimoOk = false;;

        if (v.Enunciado().replace(/\s/g, '').length == 0)
            errors.push('Enunciado é obrigatório!');
        else
            enunciadoOk = true;

        me.enunciadoOk(enunciadoOk);

        if (!regexPeso.test(v.Peso().toString()))
            errors.push('Peso deve ser um número inteiro positivo!');
        else
            pesoOk = true;

        me.pesoOk(pesoOk);

        switch (v.Tipo()) {
            case 'D':
                percentualMinimoOk = true;
                if (!regexPeso.test((v.PercentualMinimo() || '').toString())) {
                    tipoQuestaoOk = false;
                    percentualMinimoOk = false;
                    errors.push('Informe o percentual minimo quando o tipo de questão é Distribuição.')
                } else
                    tipoQuestaoOk = true;
                break;
            case 'M':
                percentualMinimoOk = true;
                if (v.Colunas().length == 0) {
                    errors.push('Questões do tipo Matriz devem ter pelo menos uma coluna.');
                    tipoQuestaoOk = false;
                }
                else
                    tipoQuestaoOk = true;
                break;
            default:
                percentualMinimoOk = true;
                tipoQuestaoOk = true;
        }
        me.tipoQuestaoOk(tipoQuestaoOk);
        me.percentualMinimoOk(percentualMinimoOk);


        if (v.Alternativas().length < 2)
            errors.push('Cadastre ao menos duas alternativas.');
        else {
            var count = 0;
            for (var i = v.Alternativas().length - 1; i >= 0; i--) {
                var a = v.Alternativas()[i];
                if (a.Ativa())
                    count++;
            }
            if (count < 2) {
                errors.push('Selecione ao menos duas alternativas.');
            }
        }


        for (var i = 0; i < v.Alternativas().length; i++) {
            var a = v.Alternativas()[i];
            if (a.Texto().replace(/\s/g, '') == '') {
                errors.push('Resposta ' + me.letra(i) + ': Informe o texto.');
            }

            if (isNaN(toNumber(a.Pontos())))
                errors.push('Resposta ' + me.letra(i) + ': Informe a quantidade de pontos. Exemplos: "0,5", "1", "2" ...');
        }

        for (var i = 0; i < v.Colunas().length; i++) {
            var c = v.Colunas()[i];
            if (c.Descricao().replace(/\s/g, '') == '') {
                errors.push('Coluna ' + (i + 1) + ': Informe a descrição da coluna.');
            }
        }

        me.validationSummary(errors);
        return errors.length == 0;
    }

    this.concluirEdicaoQuestao = function () {


        if (!me.validarQuestao(me.questaoCorrente()))
            return;


        if (me.indiceQuestaoCorrente() == -1) {
            me.formulario().Questoes.push(me.questaoCorrente());
        } else {
            me.formulario().Questoes()[me.indiceQuestaoCorrente()] = me.questaoCorrente();
        }
        me.reindexarQuestoes();
        me.formulario().Questoes.notifySubscribers();


        //me.validarGeral();

        me.esconderModal(function () {
            me.questaoCorrente(null);
            me.salvamentoPendente(true);
        });




    }
    this.fecharModalEdicao = function () {
        if (confirm('Cancelar edição?')) {

            me.esconderModal(function () {
                me.questaoCorrente(null);
            })

        }

    }

    this.subirAlternativa = function (a) {
        var alternativas = me.questaoCorrente().Alternativas;
        var indice = alternativas.indexOf(a);
        if (indice > 0) {
            var outra = alternativas()[indice - 1];
            alternativas()[indice - 1] = a;
            alternativas()[indice] = outra;
            alternativas.notifySubscribers();
        }
    }
    this.descerAlternativa = function (a) {
        var alternativas = me.questaoCorrente().Alternativas;
        var indice = alternativas.indexOf(a);
        if (indice < alternativas().length - 1) {
            var outra = alternativas()[indice + 1];
            alternativas()[indice + 1] = a;
            alternativas()[indice] = outra;
            alternativas.notifySubscribers();
        }
    }
    this.excluirAlternativa = function (a) {
        if (a.Id() > 0) {

            if (me.modoProtegido()) {
                me.exibirAlertaModoProtegido('a');
                return;
            }

            me.alternativasRemovidas.push(a.Id());
        }
        me.questaoCorrente().Alternativas.remove(a);
    }

    this.adicionarAlternativa = function () {
        var q = me.questaoCorrente();

        var alternativa = {
            Ativa: true,
            Id: -1,
            IdProximaQuestao: null,
            UIDProximaQuestao: null,
            Ordem: q.Alternativas.length + 1,
            Pontos: 1,
            Texto: ''
        };

        alternativa = ko.mapping.fromJS(alternativa);

        q.Alternativas.push(alternativa);

        setTimeout(function () {
            var container = $("#container-respostas").get(0);
            container.scrollTop = container.scrollHeight;
            var input = $("table tr:last td input[type='text']:first", container);
            input.focus();

            $("select", container).styleInputs();

        }, 1);


    }


    this.subirColuna = function (c) {
        var colunas = me.questaoCorrente().Colunas;
        var indice = colunas.indexOf(c);
        if (indice > 0) {
            var outra = colunas()[indice - 1];
            colunas()[indice - 1] = c;
            colunas()[indice] = outra;
            colunas.notifySubscribers();
        }
    }
    this.descerColuna = function (c) {
        var colunas = me.questaoCorrente().Colunas;
        var indice = colunas.indexOf(c);
        if (indice < colunas().length) {
            var outra = colunas()[indice + 1];
            colunas()[indice + 1] = c;
            colunas()[indice] = outra;
            colunas.notifySubscribers();
        }
    }
    this.excluirColuna = function (c) {
        if (c.Id() > 0) {
            if (me.modoProtegido()) {
                me.exibirAlertaModoProtegido('c');
                return;
            }
            me.colunasRemovidas.push(c.Id());
        }
        me.questaoCorrente().Colunas.remove(c);
    }


    this.adicionarColuna = function () {
        var questao = me.questaoCorrente();

        var c = {
            Id: -1,
            Ativa: true,
            Descricao: '',
            Ordem: questao.Colunas().length + 1
        };

        c = ko.mapping.fromJS(c);

        questao.Colunas.push(c);
    }

    this.linhaAdicionada = function (element) {

        if (element.tagName == 'TR') {
            $('td', element).hide().fadeIn('fast');

            $(".pontos", element).mask('9?,9');

        }

    }

    this.exibirAlertaModoProtegido = function (tipo) {
        alert('AVISO! Você está editando um questionário que já foi preenchido por clientes! Não são permitidos remover nenhuma questão, alternativa ou coluna de matriz neste cenário!');
    }

    this.letra = function (i) {
        var c = "A".charCodeAt(0);
        c += i;
        return String.fromCharCode(c);
    }

    this.formatarSubquestao = function (q) {
        var num = q.Ordem();
        if (q.NumeroSubQuestao())
            num += '.' + q.NumeroSubQuestao();
        return num;
    }

    this.buscarQuestoesQueDepende = function (q) {
        var arr = [];;
        var questoes = me.formulario().Questoes();
        for (var i = 0; i < questoes.length; i++) {
            var q2 = questoes[i];
            if (q2.UID() != q.UID() /*&& q2.QuestaoAtiva()*/)
                for (var j = 0; j < q2.Alternativas().length; j++) {
                    var a = q2.Alternativas()[j];
                    if (a.UIDProximaQuestao() == q.UID())
                        arr.push(q2);
                }
        }
        return arr;
    }

    this.podeDesabilitarQuestao = function (q) {
        var dependentes = me.buscarQuestoesQueDepende(q);
        var abertas = me.buscarQuestoesDependentes(q);
        var indice = me.formulario().Questoes.indexOf(q);
        if (dependentes.length)
            return false;
        else if (abertas.length && indice == 0)
            return false;
        else
            return true;
    }

    this.obterExplicacaoCheckbox = function (q) {
        var outrasQuestoes = me.buscarQuestoesQueDepende(q);
        if (outrasQuestoes.length == 0)
            return '';
        else {

            var arrNumeros = [];
            for (var i = 0; i < outrasQuestoes.length; i++) {
                var formatado = me.formatarSubquestao(outrasQuestoes[i]);
                if (arrNumeros.indexOf(formatado) < 0)
                    arrNumeros.push(formatado);
            }
            if (arrNumeros.length == 1) {
                return 'Esta questão não pode ser desabilitada ou excluída porque a questão ' + arrNumeros[0] + ' abre esta questão.';
            } else
                return 'Esta questão não pode ser desabilita ou excluída porque as questões ' + arrNumeros.join(', ') + ' abrem esta questão.';
        }
    }

    this.buscarQuestoesDependentes = function (q) {
        var arr = [];;
        var questoes = me.formulario().Questoes();
        for (var i = 0; i < questoes.length; i++) {
            var q2 = questoes[i];
            if (q2.UID() != q.UID() /*&& q2.QuestaoAtiva()*/)
                for (var j = 0; j < q.Alternativas().length; j++) {
                    var a = q.Alternativas()[j];
                    if (a.UIDProximaQuestao() == q2.UID())
                        arr.push(q2);
                }
        }
        return arr;
    }

    this.margemBarra = function (i) {
        var tot = me.pontuacaoMaxima();

        var min = me.formulario().Pontuacao()[i].Minimo() || 0;
        return Math.round((min / tot) * 100);
    }

    this.tamanhoBarra = function (i) {
        var tot = me.pontuacaoMaxima();

        var min = me.formulario().Pontuacao()[i].Minimo() || 0;
        var max = me.formulario().Pontuacao()[i].Maximo() || tot;
        max = Math.min(max, tot);
        return Math.round(((max - min) / tot) * 100);
    }

    this.posicaoTraco = function () {
        return Math.round(100 * (me.pontuacaoMinima() / me.pontuacaoMaxima()));
    }

    this.recriarComboQuestoes = function (e) {
        setTimeout(function () {
            $('select', e).styleInputs();;
        }, 1)


    }
    this.formatarQuestaoCombo = function (item) {
        return me.formatarSubquestao(item) + ' - ' + item.Enunciado();
    }

    this.reload();
}
var vm = new EditorSuitability;
$(document).ready(function () {

    ko.applyBindings(vm);
    $("#esconderijo").show();
});
