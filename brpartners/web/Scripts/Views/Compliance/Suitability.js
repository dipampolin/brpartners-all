﻿//Scripts Rodrigo Ribeiro
//

// Adiciona validação nos campos
$(document).ready(function () {
    $(".btn-input-submit").click(function () {
    });
    
    // Adiciona a zebrado nas tabelas
    $('tr:odd').css('background', '#EEE');

    if ($('#cbDes').is(':checked')) {
        $("#dtInicial2").removeAttr('disabled', 'disabled');
        $("#dtFinal2").removeAttr('disabled', 'disabled');

        $("#dtInicial2").mask("99/99/9999");
        $("#dtFinal2").mask("99/99/9999");
    } else {
        $("#dtInicial2").attr('disabled', 'disabled');
        $("#dtFinal2").attr('disabled', 'disabled');
    }

    $('#cbDes').click(function () {
        var ck = $(this).attr('checked');
        if (ck) {
            $("#dtInicial2").removeAttr('disabled', 'disabled');
            $("#dtFinal2").removeAttr('disabled', 'disabled');

            $("#dtInicial2").mask("99/99/9999");
            $("#dtFinal2").mask("99/99/9999");

        } else {
            $("#dtInicial2").attr('disabled', 'disabled');
            $("#dtFinal2").attr('disabled', 'disabled');
        }
    });

    $(".ddlHist").change(function () {
        if ($(this).val() != "") {
            window.open("/Compliance/SuitabilityResult/" + $(this).val());
        }
    });

    $(".refazer").click(function () {
        var a = $(this).attr("data-view");
        var data = "idClient=" + a + "&status=3";

        $.post("/Compliance/SuitabilityAtualizaStatus/", data, function (data) {
            alert(data.msg);
        }, "json");
    });

    $(".inelegivel").click(function () {
        var a = $(this).attr("data-view");
        var data = "idClient=" + a + "&status=4";

        $.post("/Compliance/SuitabilityAtualizaStatus/", data, function (data) {
            alert(data.msg);
        }, "json");
    });


});
