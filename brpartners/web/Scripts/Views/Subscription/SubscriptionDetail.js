﻿/// <reference path="../../References.js" />

$(function () {
    var detailContext = $("#SubscriptionDetail");

    $("#btnRightsRequestDetail", detailContext).click(function () {
        var id = $("#hdfCurrentId", detailContext).val();
        var url = "/Subscription/RightsRequest/?requestId=" + id;
        commonHelper.OpenModal(url, 671, ".content", "");
    });
});