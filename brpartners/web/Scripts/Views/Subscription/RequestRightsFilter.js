﻿/// <reference path="../../jQuery/jquery-1.4.3.js" />

var RequestRightsFilter = {
    ValidateFilterFields: function (context) {
        var messageError = "";

        if (!validateHelper.ValidateList($("#txtAssessor", context).val()))
            messageError += "<li>Faixa de assessores inválida.</li>";

        if (!validateHelper.ValidateList($("#txtClient", context).val()))
            messageError += "<li>Faixa de clientes inválida.</li>";

        var invalidDate = false;
        $("input[id*='Date']", context).each(function () {
            if ($(this).val() != "" && !invalidDate) {
                if (!DateValidate($(this).val())) {
                    invalidDate = true;
                    messageError += "<li>Data inválida.</li>";
                }
            }
        });

        if (!invalidDate) {
            if (!dateHelper.CheckPeriod("#txtInitialDate", "#txtBrokerDate", context))
                messageError += "<li>Periodo de data inválido.</li>";
        }

        if (messageError.length > 0)
            messageError = "<ul>" + messageError + "</ul>";

        return { Result: (messageError == ""), Message: messageError }
    }
};

$(function () {
    var filterContext = $("#requestRightsFilterContent");

    $("#requestRightsFilter").click(function () {
        $(filterContext).slideToggle('fast');
        $('select').styleInputs();
        $(this).toggleClass('aberto');
    });

    $("#txtStockCode", filterContext).css("text-transform", "uppercase");
    $("#txtInitialDate", filterContext).setMask("99/99/9999").datepicker().datepicker($.datepicker.regional['pt-BR']);
    $("#txtBrokerDate", filterContext).setMask("99/99/9999").datepicker().datepicker($.datepicker.regional['pt-BR']);

    $("#btnSearch").unbind("click");
    $("#btnSearch").bind("click", function (event) {
        $("#requestRightsList > tbody").html('');
        var feedback = RequestRightsFilter.ValidateFilterFields(filterContext);
        if (feedback.Result) {
            $("#filterFeedbackError").html("").hide();
            $("#requestRightsContent").hide();
            $("#loadingArea").show();
            $("#tableArea").hide();
            $("#divSubscriptionFeedback").hide().find("span").html('');
            $("#feedbackError").hide().find("span").html('');

            if (oTableRequestRights != null) {
                oTableRequestRights.fnDestroy();
                oTableRequestRights = null;
            }

            oTableRequestRights = $("#requestRightsList").dataTable(objRequestRightsFilter);
            RequestRightsMethods.enableExportButtons();
        }
        else {
            $("#filterFeedbackError").html(feedback.Message).show();
        }

        if ($("#requestRightsFilterContent").is(":visible")) {
            $("#requestRightsFilter").click();
        }
    });

    $(filterContext).bind('keydown', function (event) {
        if (event.which == 13) {
            $("#btnSearch").click();
        }
    });

});


