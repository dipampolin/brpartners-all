﻿
$(document).ready(function () {
    var context = $("#frmConfirmRequestRightsNotify");

    $("#btnNotifyAll", context).click(function () {
        Submit(true);
    });

    $("#btnNotifyUnnotified", context).click(function () {
        Submit(false);
    });

});

function Submit(notifyAll) {

    var n = notifyAll ? "1" : "0";

    $.ajax({
        url: "/Subscription/RequestRightsSendNotification",
        type: "POST",
        dataType: "json",
        data: { notifyAll: n },
        success: function (data) {

            if (data.Error == "") {
                Modal.Close();
                $("#btnSearch").click();
                //oTableRequestRights.fnDraw(false);
                RequestRightsMethods.RequestRightsPendents();

                $("div#divSubscriptionFeedback span").html("Notificação enviada com sucesso.");
                $("div#divSubscriptionFeedback").css({ 'display': 'block' });
            }
            else {
                $("#feedbackRequestError").html(data.Error);
                $("#feedbackRequestError").css({ 'display': 'block' });
            }

        }
    });

}