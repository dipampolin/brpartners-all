﻿/// <reference path="../../References.js" />

var oTableSubscription = null;

var fnDrawCallback = function (settings) {
    $("#loadingArea").hide();
    $("#tableArea").show();

    var totalRecords = settings.fnSettings().fnRecordsDisplay();

    SubscriptionMethods.enableExportButtons();

    var totalPerPage = settings.fnSettings()._iDisplayLength;

    var totalPages = Math.ceil(totalRecords / totalPerPage);

    var currentIndex = parseInt(settings.fnSettings()._iDisplayStart);
    var currentPage = totalPages > 0 ? Math.ceil(1 + (currentIndex / totalPerPage)) : 0;
    $("#subscriptionList_info").html("Exibindo " + currentPage + " de " + totalPages);

    if (totalRecords > 0 && totalPages > 1) {
        $("#subscriptionList_info").css("display", "");
        $("#subscriptionList_length").css("display", "");
        $("#subscriptionList_paginate").css("display", "");
    }
    else {
        $("#subscriptionList_info").css("display", "none");
        $("#subscriptionList_length").css("display", "none");
        $("#subscriptionList_paginate").css("display", "none");
    }

    SubscriptionMethods.SubscriptionEdit();
    SubscriptionMethods.SubscriptionDetail();
    SubscriptionMethods.SubscriptionDelete();
    SubscriptionMethods.SubscriptionHistoryDetail();
}

var objSubscriptionFilter = {
    "bAutoWidth": false
            , "bPaginate": true
            , "bDestroy": true
            , "sPaginationType": "full_numbers"
            , "sDom": 'trip'
            , "oLanguage": {
                "sLengthMenu": "Exibindo _MENU_ registros",
                "sZeroRecords": "Nenhum registro encontrado.",
                "sInfo": "Encontramos _TOTAL_ registros",
                "sInfoEmpty": "Nenhum registro encontrado",
                "sInfoFiltered": "(Filtrando _MAX_ registros)",
                "sSearch": "Buscar:",
                "sProcessing": "Processando...",
                "oPaginate": {
                    "sFirst": "Primeiro",
                    "sPrevious": "Anterior",
                    "sNext": "Próximo",
                    "sLast": "Último"
                }
            }
            , "bProcessing": true
            , "bServerSide": true
            , "sAjaxSource": "../Subscription/SubscriptionFilter"
            , "fnServerData": function (sSource, aoData, fnCallback) {
                var filterContext = $("#subscriptionFilterContent");

                if (aoData != null && aoData != undefined) {
                    aoData.push({ "name": "txtStockCode", "value": $("#txtStockCode", filterContext).val() });
                    aoData.push({ "name": "ddlStatus", "value": $('#ddlStatus', filterContext).val() });
                    aoData.push({ "name": "txtInitialDate", "value": $("#txtInitialDate", filterContext).val() });
                    aoData.push({ "name": "txtBrokerDate", "value": $("#txtBrokerDate", filterContext).val() });
                }
                $.ajax({
                    "cache": false,
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "beforeSend": function () {
                        $("#tableArea").hide();
                        $("#loadingArea").show();
                        $("#emptyArea").hide();
                    },
                    "success": function (data) {
                        fnCallback(data);
                        if (data.iTotalRecords == 0) {
                            $("#tableArea").hide();
                            $('#emptyArea').html('Sua pesquisa não encontrou resultados válidos. Tente novamente.').show();
                        }
                        else {
                            $('#emptyArea').html('').hide();
                        }

                        SubscriptionMethods.enableExportButtons();
                    }
                });
            }
            , fnDrawCallback: function () {
                fnDrawCallback(this);
            }
            , "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                return nRow;
            }
            , "aoColumns": [
                { "bSortable": false },
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                { "bSortable": false }
            ]
};



var objSubscriptionInitial = {
    "bAutoWidth": false
            , "bPaginate": true
            , "bDestroy": true
            , "sPaginationType": "full_numbers"
            , "sDom": 'trip'
            , "oLanguage": {
                "sLengthMenu": "Exibindo _MENU_ registros",
                "sZeroRecords": "Nenhum registro encontrado.",
                "sInfo": "Encontramos _TOTAL_ registros",
                "sInfoEmpty": "Nenhum registro encontrado",
                "sInfoFiltered": "(Filtrando _MAX_ registros)",
                "sSearch": "Buscar:",
                "sProcessing": "Processando...",
                "oPaginate": {
                    "sFirst": "Primeiro",
                    "sPrevious": "Anterior",
                    "sNext": "Próximo",
                    "sLast": "Último"
                }
            }
            , "bProcessing": true
            , "bServerSide": true
            , "sAjaxSource": "../Subscription/InitialSubscriptionFilter"
            , "fnServerData": function (sSource, aoData, fnCallback) {
                var status = commonHelper.GetParameterByName("status");
                if (aoData != null && aoData != undefined && status != undefined && status != null && status != "") {
                    aoData.push({ "name": "ddlStatus", "value": status });

                    $("#ddlStatus").val(status);
                }
                $.ajax({
                    "cache": false,
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "beforeSend": function () {
                        $("#tableArea").hide();
                        $("#loadingArea").show();
                        $("#emptyArea").hide();
                    },
                    "success": function (data) {

                        fnCallback(data);

                        if (data.iTotalRecords == 0) {
                            $("#tableArea").hide();
                            $('#emptyArea').html('Sua pesquisa não encontrou resultados válidos. Tente novamente.').show();
                        }
                        else {
                            $('#emptyArea').html('').hide();
                        }

                        SubscriptionMethods.enableExportButtons();
                    }
                });
            }
            , fnDrawCallback: function () {
                fnDrawCallback(this);

            }
            , "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                return nRow;
            }
            , "aoColumns": [
                { "bSortable": false },
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                { "bSortable": false }
            ]
};

var SubscriptionMethods = {

    enableExportButtons: function () {
        var data = $("#subscriptionList tbody tr");
        var emptyData = (data.length == 1 && data[0].lastChild.className == "dataTables_empty") ? true : false;
        var excelLink = $("#lnkSubscriptionExcel");
        var pdfLink = $("#lnkSubscriptionPdf");
        excelLink.attr("href", (emptyData) ? "#" : "/Subscription/SubscriptionsToFile/?isPdf=false");
        pdfLink.attr("href", (emptyData) ? "#" : "/Subscription/SubscriptionsToFile/?isPdf=true");
    }

    , SubscriptionRequest: function (id) {
        $("#lnkRequest").unbind("click").bind("click", function () {
            commonHelper.OpenModal("/Subscription/SubscriptionRequest/" + id, 890, ".content", "");
            $("#MsgBox").center();
        });
    }
    , SubscriptionEdit: function () {
        $(".ico-editar").unbind("click").bind("click", function () {
            var id = $(this).parent().parent().find("input.hidden-req").attr('value');
            commonHelper.OpenModal("/Subscription/SubscriptionRequest/" + id, 890, ".content", "");
        });
    }
    , SubscriptionDetail: function () {
        $(".ico-visualizar, .lnk-detail").unbind("click").bind("click", function () {
            var id = $(this).parent().parent().find("input.hidden-req").attr('value');
            commonHelper.OpenModal("/Subscription/SubscriptionDetail/" + id, 890, ".content", "");
        });
    }
    , SubscriptionDelete: function () {
        $(".ico-excluir").unbind("click").bind("click", function () {
            var id = $(this).parent().parent().find(".hidden-req").attr('value');
            commonHelper.OpenModal("/Subscription/SubscriptionConfirmDelete/" + id, 890, ".content", "");
        });
    }
    , SubscriptionHistoryDetail: function () {
        $(".historico-detalhe-subscricao").unbind("click").bind("click", function () {
            var id = $(this).parent().parent().find("input.hidden-req").attr('value');
            var url = "/Subscription/SubscriptionHistoryDetail/?requestId=" + id;
            commonHelper.OpenModal(url, 671, ".content", "");
        });
    }
    , SubscriptionRightRequest: function () {
        $("#lnkRights").unbind("click").bind("click", function () {
            if ($("input[type='checkbox'][name^='chkSubscription']:checked").length != 1) {

                var message = ($("input[type='checkbox'][name^='chkSubscription']:checked").length > 1)
                    ? "Selecione apenas uma subscrição"
                    : "Nenhuma subscrição selecionada";

                $("#feedbackError").html(message).show();
            }
            else if ($("input[type='checkbox'][name^='chkSubscription']:checked").val() == "P") {
                $("#feedbackError").html('Não é possível solicitar direitos de uma subscrição que não foi aprovada.').show();
            }
            else {
                $("#feedbackError").html('').hide();
                var id = $("input[type='checkbox'][name^='chkSubscription']:checked").parent().parent().find("input.hidden-req").attr('value');
                var url = "/Subscription/RightsRequest/?requestId=" + id;
                commonHelper.OpenModal(url, 671, ".content", "");
            }
        });
    }
    , SubscriptionPendents: function () {
        $.post("/Home/GetPending", { module: "Subscription.Index" }, function (data) {
            if (data > 0) {

                var pendingContent = "<span><strong>" + data + "</strong> Pendente" + (data > 1 ? "s" : "") + "</span>";

                if ($("#pendingTag").length > 0) {
                    $("#pendingTag").html(pendingContent);
                }
                else {
                    var pending = "<div id='pendingTag' class='aviso' style='display: none;'>" + pendingContent + "</div>";
                    if ($('#trilha').children('ul').find('li').last().length > 0)
                        $('#trilha').children('ul').find('li').last().append(pending);
                    else
                        $('#trilha').children('ul').append(pending);
                }
                $("#pendingTag").show();
            }
            else {
                $("#pendingTag").hide();
            }
        }, "json");
    }
};

$(function () {

    $("#chkAll").click(function () {
        var check = $(this).is(':checked');
        $("table#subscriptionList input:checkbox:not(:disabled)").each(function () { $(this).attr('checked', check); })
    });

    $('.closeLink').bind({
        "click": function () {
            $('.modal-situacao').hide();
            return false;
        }
    });

    if (oTableSubscription != null)
        oTableSubscription = null;

    oTableSubscription = $("#subscriptionList").dataTable(objSubscriptionInitial);

    SubscriptionMethods.enableExportButtons();
    SubscriptionMethods.SubscriptionRequest(0);
    SubscriptionMethods.SubscriptionPendents();
    SubscriptionMethods.SubscriptionRightRequest();


});

function OpenRequest(id) {
    commonHelper.OpenModal("/Subscription/SubscriptionEdit/" + id, 890, ".content", "");
    $("#MsgBox").center();
}

function ViewHistory() {
    commonHelper.OpenModal("/Subscription/SubscriptionHistory", 671, ".content", "");
}

function OpenHistoryDetail(id) {
    var url = "/Subscription/SubscriptionHistoryDetail/?requestId=" + id;
    commonHelper.OpenModal(url, 671, ".content", "");
    $("#MsgBox").center();
}

function DeleteRequest(id, clientCode) {
    var url = "/Subscription/SubscriptionConfirmDelete/?requestId=" + id + "&clientCode=" + clientCode;
    commonHelper.OpenModal(url, 450, ".content", "");
    $("#MsgBox").center();
}

function DateValidate(date) {
    var reDate = /^((0[1-9]|[12]\d)\/(0[1-9]|1[0-2])|30\/(0[13-9]|1[0-2])|31\/(0[13578]|1[02]))\/\d{4}$/;
    if (reDate.test(date) || (date == '')) {
        return true
    } else {
        return false;
    }
}

function ChangeStatus(submit, self, requestId, isinStock, isinSubscription, company) {

    var modal = $("#divModalStatus"),
        hdfRequestId = $("#hdfRequestId", modal),
        hdfIsinStock = $("#hdfIsinStock", modal),
        hdfIsinSubscription = $("#hdfIsinSubscription", modal),
        ddlStatusChange = $("#ddlStatusChange", modal),
        hdfCompany = $("#hdfCompany", modal);

    $("#ddlStatusChange option[value='']", modal).addClass('not');

    if (submit) {

        if (ddlStatusChange.val() == "") {
            $("#feedbackRequestError", modal).show();
            return false;
        }
        else
            $("#feedbackRequestError", modal).hide();

        $.ajax({
            url: "/Subscription/ChangeStatus",
            type: "POST",
            dataType: "json",
            data: { requestId: hdfRequestId.val(), isinStock: hdfIsinStock.val(), isinSubscription: hdfIsinSubscription.val(), status: ddlStatusChange.val(), companyName: hdfCompany.val() },
            success: function (data) {
                if (data.Error == "") {
                    $("#subscriptionFilterContent #btnSearch").click();
                    modal.css({ 'display': 'none' });
                    $("div#divSubscriptionFeedback span").html("Atualização feita com sucesso.");
                    $("div#divSubscriptionFeedback").css({ 'display': 'block' });
                }
                else {
                    $("#feedbackRequestError", modal).html(data.Error);
                    $("#feedbackRequestError", modal).css({ 'display': 'block' });
                }
            }
        });
            
    }
    else {
        $("#feedbackRequestError", modal).hide();

        var offset = $(self).offset();
        
        modal.show().css("top", offset.top);
        ddlStatusChange.styleInputs();

        hdfRequestId.val(requestId);
        hdfIsinStock.val(isinStock);
        hdfIsinSubscription.val(isinSubscription);
        hdfCompany.val(company);
    }
}

function OpenApprove() {
    var checkedRequests = $("[id^='chkSubscription']:checked");
    if (checkedRequests.length > 0) {
        var ids = "";
        $(checkedRequests).each(function () {
            if ($(this).val() == "P") {
                ids += $(this).attr('id').split("-")[1] + ";";
            }
            else {
                $(this).attr('checked', false);
            }
        });

        if (ids != "") {
            $("#feedbackError").html("").css({ 'display': 'none' });
            var url = "/Subscription/SubscriptionApprove/?requestsIds=" + ids;
            commonHelper.OpenModal(url, 890, ".content", "");
            $("#MsgBox").center();
        } else {
            $("#feedbackError").html("Nenhuma subscrição para aprovar selecionada.").css({ 'display': 'block' });
            $("#subscriptionList #chkAll").attr('checked', false);
        }
    }
    else {
        $("#feedbackError").html("Nenhuma subscrição para aprovar selecionada.").css({ 'display': 'block' });
    }
}
