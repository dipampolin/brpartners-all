﻿
$(document).ready(function () {
    var context = $("#frmConfirmRequestRightsEffect");

    $("#btnEffect", context).click(function () {
        Submit();
    });

    $("#btnCancel", context).click(function () {
        Modal.Close();
    });

});

function Submit() {

    $.ajax({
        url: "/Subscription/RequestRightsEffect",
        type: "POST",
        dataType: "json",
        cache: false,
        success: function (data) {

            if (data.Error == "") {
                Modal.Close();
                $("#btnSearch").click();
                $("div#divSubscriptionFeedback span").html("Solicitação efetuada com sucesso.");
                $("div#divSubscriptionFeedback").css({ 'display': 'block' });
                RequestRightsMethods.RequestRightsPendents();
                //$("#btnSearch").click();
                //oTableRequestRights.fnDraw(false);
            }
            else {
                $("#feedbackRequestError").html(data.Error);
                $("#feedbackRequestError").css({ 'display': 'block' });
            }

        }
    });

}