﻿var BrokerageTransferMethods = {
    ValidateFilterFields: function () {
        var messageError = "";

        /*if ($("#txtClientCode").val() == "" || $("#txtClientCode").val() == null || $("#txtClientCode").val() == "0")
        messageError += "<li>Cliente não informado.</li>";*/

        if (!validateHelper.ValidateList($("#txtAssessors").val()))
            messageError += "<li>Faixa de assessores inválida.</li>";

        if (!validateHelper.ValidateList($("#txtClients").val()))
            messageError += "<li>Faixa de clientes inválida.</li>";

        if ($("#txtStartDate").val() == "") {
            messageError += "<li>Data inicial não informada.</li>";
        }
        else if (Date.parse($("#txtStartDate").val()) == null) {
            messageError += "<li>Data inicial inválida.</li>";
        }

        if ($("#txtEndDate").val() == "") {
            messageError += "<li>Data final não informada.</li>";
        }
        else if (Date.parse($("#txtEndDate").val()) == null) {
            messageError += "<li>Data final inválida.</li>";
        }



        if (messageError.length > 0)
            messageError = "<ul>" + messageError + "</ul>";

        return { Result: (messageError == ""), Message: messageError }
    },
    PaginateClients: function () {
        $("#lnkPreviousClientTop, #lnkNextClientTop").unbind("click").bind("click", function () {
            var id = $(this).attr('rel');

            $.ajax({
                type: "POST",
                cache: false,
                url: "../BMF/BrokerageReportFilter_Paginate",
                data: {
                    "order": id
                },
                dataType: 'html',
                beforeSend: function () {
                    $("#tableArea").html('');
                    $("#loadingArea").show();
                },
                success: function (data) {
                    $("#tableArea").html(data).show();

                    $("#loadingArea").hide();

                    BrokerageTransferMethods.PaginateClients();

                }
                , error: function () {
                    $("#tableArea").html("<div class='erro'>Ocorreu um erro inesperado.</span>").show();
                    $("#loadingArea").hide();
                }
            });

        });
    }
};

$(function () {
    var context = $('#BrokerageTransferFilterContent');

    $("#BrokerageTransferFilter").click(function () {
        $(context).slideToggle('fast');
        $('select').styleInputs();
        $(this).toggleClass('aberto');
    });

    $("#txtStartDate").setMask("99/99/9999").datepicker().datepicker($.datepicker.regional['pt-BR']);
    $("#txtEndDate").setMask("99/99/9999").datepicker().datepicker($.datepicker.regional['pt-BR']);
    $.datepicker.setDefaults($.datepicker.regional['pt-BR']);

    $(context).bind('keydown', function (event) {
        if (event.which == 13) {
            $("#btnSearch").click();
        }
    });

    $("#btnSearch").unbind("click");
    $("#btnSearch").bind("click", function () {

        $("#feedback").hide().find("span").html('');
        $("#feedbackError").hide().find("span").html('');
        $("#pesquisa").hide();
        var valid = BrokerageTransferMethods.ValidateFilterFields();

        if (valid.Result) {
            $(context).slideToggle('fast');
            $(this).toggleClass('fechado');
            $("#filterFeedbackError").html("").hide();
            $("#BrokerageTransferFilterContent").hide();
            $("#loadingArea").show();
            $("#tableArea").hide();

            $.ajax({
                type: "POST",
                cache: false,
                url: "../BMF/BrokerageReportFilter",
                data: {
                    "txtClients": $("#txtClients").val(),
                    "txtAssessors": $("#txtAssessors").val(),
                    "txtStartDate": $("#txtStartDate").val(),
                    "txtEndDate": $("#txtEndDate").val()
                },
                dataType: 'html',
                beforeSend: function () { $("#tableArea").html(''); },
                success: function (data) {
                    $("#tableArea").html(data).show();

                    $("#loadingArea").hide();

                    BrokerageTransferMethods.PaginateClients();

                }
                    , error: function () {
                        $("#tableArea").html("<div class='erro'>Ocorreu um erro inesperado.</span>").show();
                        $("#loadingArea").hide();
                    }
            });
        }
        else {
            $("#filterFeedbackError").html(valid.Message).show();
        }
    });
});
