﻿
var oTableBrokerageRange = null;

var objBrokerageRangeFilter = {
    "bAutoWidth": false
    , "bPaginate": true
    , "bDestroy": true
    , "sPaginationType": "full_numbers"
    , "sDom": 'trp'
    , "oLanguage": {
        "sLengthMenu": "Exibindo _MENU_ registros",
        "sZeroRecords": "Nenhum registro encontrado.",
        "sInfo": "Encontramos _TOTAL_ registros",
        "sInfoEmpty": "Nenhum registro encontrado",
        "sInfoFiltered": "(Filtrando _MAX_ registros)",
        "sSearch": "Buscar:",
        "sProcessing": "Processando...",
        "oPaginate": {
            "sFirst": "Primeiro",
            "sPrevious": "Anterior",
            "sNext": "Próximo",
            "sLast": "Último"
        }
    }
    , "bProcessing": true
    , "bServerSide": true
    , "sAjaxSource": "../BMF/LoadBrokerageRangeBMF"
    , "fnServerData": function (sSource, aoData, fnCallback) {
        //        var context = $('#OperationsMapContent');

        //        if (aoData != null && aoData != undefined) {
        //            //aoData.push({ "name": "txtOperator", "value": $('#txtOperator', context).val() });
        //            aoData.push({ "name": "txtClientFilter", "value": $('#txtClientFilter', context).val() });
        //            aoData.push({ "name": "txtStockCode", "value": $('#txtStockCode', context).val() });
        //            aoData.push({ "name": "ddlTradingDate", "value": $('#ddlTradingDate', context).val() });
        //        }

        $.ajax({
            "cache": false,
            "dataType": 'json',
            "type": "POST",
            "url": sSource,
            "data": aoData,
            "beforeSend": function () {
                $("#table-bmf-area").hide();
                //$("#loadingArea").show();
                //$("#emptyArea").hide();
                //$("#dvExportar").hide();
            },
            "success": function (data) {
                fnCallback(data);

                if (data.iTotalRecords == 0) {
                    $("#table-bmf-area").hide();
                    //$('#emptyArea').html('Sua pesquisa não encontrou resultados válidos. Tente novamente.').show();
                }
                else {
                    $("#table-bmf-area").show();
                    $('#emptyArea').html('').hide();
                }
            }
        });
    }
    , fnDrawCallback: function () {

        $("#loadingArea").hide();
        //$("#table-operator-area").show();

        var totalRecords = this.fnSettings().fnRecordsDisplay();

        if (totalRecords <= 0) {

            $("#dvExportar").hide();
        }
        else {
            BrokerageRangeMethods.enableExportButtons();
            $("#dvExportar").show();
        }

        var totalPerPage = this.fnSettings()._iDisplayLength;

        var totalPages = Math.ceil(totalRecords / totalPerPage);

        var currentIndex = parseInt(this.fnSettings()._iDisplayStart);
        var currentPage = totalPages > 0 ? Math.ceil(1 + (currentIndex / totalPerPage)) : 0;

        //$("#tblBrokerageDiscount_info").html("Exibindo " + currentPage + " de " + totalPages);

        if (totalRecords > 0 && totalPages > 1) {
            $("#tblBMFList_info").css("display", "");
            $("#tblBMFList_length").css("display", "");
            $("#tblBMFList_paginate").css("display", "");
        }
        else {
            $("#tblBMFList_info").css("display", "none");
            $("#tblBMFList_length").css("display", "none");
            $("#tblBMFList_paginate").css("display", "none");
        }


        // Adiciona a classe 'ultimo' à última tr no corpo da tabela
        $('table tbody tr:last-child').addClass('ultimo');

        // Adiciona a classe 'ultimo' às últimas th e td no corpo da tabela
        $('table tr td:last-child').addClass('ultimo');
        $('table tr th:last-child').addClass('ultimo');

        BrokerageRangeMethods.viewDetails();
    }
    , aoColumns: [
        { bSortable: false },
        { "sClass": "direita", bSortable: false },
        { bSortable: false },
        { "sClass": "direita", bSortable: false },
        { "sClass": "direita", bSortable: false },
        { bSearchable: false }
    ]
};


var BrokerageRangeMethods = {
    viewDetails: function () {
        $(".ico-detalhes").unbind("click").bind("click", function () {
            var that = $(this);

            var stock = that.attr("rel");

            if (that.attr("class").toString().indexOf("fechado") > -1) {

                var td = that.parent().parent();

                $.ajax({
                    type: "post",
                    url: "../BMF/BrokerageRangeDetails",
                    cache: false,
                    dataType: "html",
                    data: { stockCode: stock },
                    success: function (data) {
                        td.after(
                            $("<tr class='details' id='" + stock + "'><td colspan='6'>" + data + "</td></tr>").clone()
                        );

                        that.removeClass("fechado").addClass("aberto");
                    }
                });
            }
            else {
                $("#" + stock).remove();
                that.removeClass("aberto").addClass("fechado");
            }
        });
    }
    , ValidateFilterFields: function () {
        var messageError = "";
        if (!validateHelper.ValidateList($("#txtAssessorFilter").val()))
            messageError += "<li>Faixa de assessores inválida.</li>";

        if (!validateHelper.ValidateList($("#txtClientFilter").val()))
            messageError += "<li>Faixa de clientes inválida.</li>";

        if (messageError.length > 0)
            messageError = "<ul>" + messageError + "</ul>";

        return { Result: (messageError == ""), Message: messageError }
    },
    enableExportButtons: function () {
        var data = $("#tblStatusViewer tbody tr");
        var emptyData = (data.length == 1 && data[0].lastChild.className == "dataTables_empty") ? true : false;
        var excelLink = $("#lnkExcel");
        var pdfLink = $("#lnkPdf");

        var fnExportFilter = function (isPDF) {
            $.ajax({
                type: "POST",
                cache: false,
                async: false,
                url: "../BMF/BrokerageRangeFilterOptions",
                data: {
                    txtAssessorFilter: $('#txtAssessorFilter').val(),
                    txtClientFilter: $('#txtClientFilter').val(),
                    DisplayStart: $("#DisplayStart").val()
                },
                beforeSend: function () {
                    $("#feedbackError").remove();
                },
                success: function (data) {
                    if (data)
                        window.location = "/BMF/BrokerageRangeToFile/?isPdf=" + isPDF;
                    else {
                        $("#emptyArea").before("<div id='feedbackError' class='erro'>Ocorreu um erro na hora da exportação.</div>");
                    }
                }
            });
        };

        excelLink.unbind("click");
        excelLink.bind("click", function () {
            fnExportFilter("false");
        });

        pdfLink.unbind("click");
        pdfLink.bind("click", function () {
            fnExportFilter("true");
        });
    },
    submitFilter: function () {
        var feedback = BrokerageRangeMethods.ValidateFilterFields();

        if (feedback.Result) {


            $.ajax({
                type: "POST",
                url: "../BMF/BrokerageRangeFilter",
                dataType: "html",
                data: {
                    "txtAssessorFilter": $("#txtAssessorFilter").val(),
                    "txtClientFilter": $("#txtClientFilter").val(),
                    "DisplayStart": $("#DisplayStart").val()
                },
                beforeSend: function () {
                    $("#filterFeedbackError").html("").hide();
                    $("#BrokerageRangeContent").hide();
                    $("#loadingArea").show();
                    $("#table-client-area").hide();
                    $("#feedback").hide().find("span").html('');
                    $("#feedbackError").hide().find("span").html('');
                    $("#pesquisa").hide();
                },
                success: function (data) {
                    $("#table-client-area").html(data);

                    $(".lnk-anterior, .lnk-proximo").unbind('click').bind('click', function () {
                        var that = $(this);

                        var id = that.attr("rel");

                        $("#DisplayStart").val(id);

                        setTimeout(
                            function () { BrokerageRangeMethods.submitFilter(); }
                            , 250);
                    });

                    $("#loadingArea").hide();
                    $("#table-client-area").show();

                    if (oTableBrokerageRange != null)
                        oTableBrokerageRange.fnDestroy();

                    oTableBrokerageRange = $("#tblBMFList").dataTable(objBrokerageRangeFilter);
                }
            });

        }
        else {
            $("#filterFeedbackError").html(feedback.Message).show();
        }
    }
};

$(function () {

    var context = $('#BrokerageRangeContent');


    $("#BrokerageRangeFilter").click(function () {
        $(context).slideToggle('fast');
        $('select').styleInputs();
        $(this).toggleClass('aberto');
    });


    $(context).bind('keydown', function (event) {
        if (event.which == 13) {
            $("#lnkSubmitBrokerageRangeFilter").click();
        }
    });

    $("#lnkSubmitBrokerageRangeFilter").unbind("click");
    $("#lnkSubmitBrokerageRangeFilter").bind("click", function () {
        $("#DisplayStart").val(0);

        setTimeout(function () {
            BrokerageRangeMethods.submitFilter();
        }, 250);
    });

    /*setTimeout(
    function () { $("#lnkSubmitBrokerageRangeFilter").click(); }
    , 250);*/
});