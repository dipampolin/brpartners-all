﻿var oTableBroker = null;

var objBrokerFilter = {
    "bAutoWidth": true
            , "bPaginate": true
            , "bDestroy": true
            , "sPaginationType": "full_numbers"
            , "sDom": 'trip'
            , "oLanguage": {
                "sLengthMenu": "Exibindo _MENU_ registros",
                "sZeroRecords": "Nenhum registro encontrado.",
                "sInfo": "Encontramos _TOTAL_ registros",
                "sInfoEmpty": "Nenhum registro encontrado",
                "sInfoFiltered": "(Filtrando _MAX_ registros)",
                "sSearch": "Buscar:",
                "sProcessing": "Processando...",
                "oPaginate": {
                    "sFirst": "Primeiro",
                    "sPrevious": "Anterior",
                    "sNext": "Próximo",
                    "sLast": "Último"
                }
            }
            , "bProcessing": true
            , "bServerSide": true
            , "sAjaxSource": "../Other/BrokerageDiscountFilter"
            , "fnServerData": function (sSource, aoData, fnCallback) {

                var context = $('#BrokerageDiscountContent');

                if (aoData != null && aoData != undefined) {
                    aoData.push({ "name": "txtAssessor", "value": $('#txtAssessor', context).val() });
                    aoData.push({ "name": "txtClient", "value": $('#txtClientFilter', context).val() });
                    aoData.push({ "name": "ddlStockMarket", "value": $('#ddlStockMarket', context).val() });
                    aoData.push({ "name": "ddlMarketType", "value": $('#ddlMarketType', context).val() });
                    aoData.push({ "name": "ddlOperation", "value": $('#ddlOperation', context).val() });
                    aoData.push({ "name": "ddlVigency", "value": $('#ddlVigency', context).val() });
                    aoData.push({ "name": "ddlSituation", "value": $('#ddlSituation', context).val() });
                    aoData.push({ "name": "rdbDiscountType", "value": $("input[name='rdbDiscountType']:checked", context).val() });
                    aoData.push({ "name": "txtMinorDiscountPercentual", "value": $('#txtMinorDiscountPercentual', context).val() });
                    aoData.push({ "name": "txtMajorDiscountPercentual", "value": $('#txtMajorDiscountPercentual', context).val() });
                    aoData.push({ "name": "txtMinorDiscountValue", "value": $('#txtMinorDiscountValue', context).val() });
                    aoData.push({ "name": "txtMajorDiscountValue", "value": $('#txtMajorDiscountValue', context).val() });
                }
                $.ajax({
                    "cache": false,
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "beforeSend": function () {
                        $("#table-area").hide();
                        $("#loadingArea").show();
                        $("#emptyArea").hide();
                    },
                    "success": function (data) {
                        fnCallback(data);
                        
                        if (data.aaData == "") {
                            $("#feedbackError").html("Ocorreu um erro ao realizar consulta.").show();
                            $("#loadingArea").hide();
                            $("#table-area").hide();
                        }
                        else if (data.iTotalRecords == 0) {
                            $("#table-area").hide();
                            $('#emptyArea').html('Sua pesquisa não encontrou resultados válidos. Tente novamente.').show();
                        }
                        else {
                            $('#emptyArea').html('').hide();
                        }
                    }
                });
            }
            , "fnInitComplete": function () {
                oTableBroker.fnAdjustColumnSizing();
            }
            , fnDrawCallback: function () {
                $("#checkAll").attr("checked", false);

                $("#loadingArea").hide();
                $("#table-area").show();

                BrokerageDiscountMethods.fnConfirmRemove();
                BrokerageDiscountMethods.openStatusModal();
                BrokerageDiscountMethods.viewHistoryDetail();
                BrokerageDiscountMethods.viewDescriptionTooltip();

                var totalRecords = this.fnSettings().fnRecordsDisplay();

                if (totalRecords <= 0) {
                    $("#lnkDiscountExcel").hide();
                    $("#lnkDiscountPdf").hide();
                    $("#dvExportar").hide();
                }
                else {
                    $("#lnkDiscountExcel").show();
                    $("#lnkDiscountPdf").show();
                    $("#dvExportar").show();
                }

                var totalPerPage = this.fnSettings()._iDisplayLength;

                var totalPages = Math.ceil(totalRecords / totalPerPage);

                var currentIndex = parseInt(this.fnSettings()._iDisplayStart);
                var currentPage = totalPages > 0 ? Math.ceil(1 + (currentIndex / totalPerPage)) : 0;

                $("#tblBrokerageDiscount_info").html("Exibindo " + currentPage + " de " + totalPages);

                if (totalRecords > 0 && totalPages > 1) {
                    $("#tblBrokerageDiscount_info").css("display", "");
                    $("#tblBrokerageDiscount_length").css("display", "");
                    $("#tblBrokerageDiscount_paginate").css("display", "");
                }
                else {
                    $("#tblBrokerageDiscount_info").css("display", "none");
                    $("#tblBrokerageDiscount_length").css("display", "none");
                    $("#tblBrokerageDiscount_paginate").css("display", "none");
                }

            }
            , aoColumns: [
                { bSearchable: false, bSortable: false },
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                { sClass: "direita" },
                { sClass: "direita" },
                { sClass: "descricao" },
                null,
                null
            ]
};



var objBrokerInitial = {
    "bAutoWidth": true
            , "bPaginate": true
            , "bDestroy": true
    //, "iDisplayLength": 1
            , "sPaginationType": "full_numbers"
            , "sDom": 'trip'
            , "oLanguage": {
                "sLengthMenu": "Exibindo _MENU_ registros",
                "sZeroRecords": "Nenhum registro encontrado.",
                "sInfo": "Encontramos _TOTAL_ registros",
                "sInfoEmpty": "Nenhum registro encontrado",
                "sInfoFiltered": "(Filtrando _MAX_ registros)",
                "sSearch": "Buscar:",
                "sProcessing": "Processando...",
                "oPaginate": {
                    "sFirst": "Primeiro",
                    "sPrevious": "Anterior",
                    "sNext": "Próximo",
                    "sLast": "Último"
                }
            }
            , "bProcessing": true
            , "bServerSide": true
            , "sAjaxSource": "../Other/InitialBrokerageDiscountFilter"
            , "fnServerData": function (sSource, aoData, fnCallback) {
                var status = commonHelper.GetParameterByName("status");
                if (aoData != null && aoData != undefined && status != undefined && status != null && status != "") {
                    aoData.push({ "name": "ddlSituation", "value": status });

                    $("#ddlSituation").val(status);
                }

                $.ajax({
                    "cache": false,
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "beforeSend": function () {
                        $("#table-area").hide();
                        $("#loadingArea").show();
                        $("#emptyArea").hide();
                    },
                    "success": function (data) {
                        fnCallback(data);
                        if (data.aaData == "") {
                            $("#feedbackError").html("Ocorreu um erro ao realizar consulta.").show();
                            $("#loadingArea").hide();
                            $("#table-area").hide();
                        }
                        else if (data.iTotalRecords == 0) {
                            $("#table-area").hide();
                            $('#emptyArea').html('Sua pesquisa não encontrou resultados válidos. Tente novamente.').show();
                        }
                        else {
                            $('#emptyArea').html('').hide();
                        }
                    }
                });
            }
            , "fnInitComplete": function () {
                oTableBroker.fnAdjustColumnSizing();
            }
            , fnDrawCallback: function () {
                $("#loadingArea").hide();
                $("#table-area").show();
                $("#checkAll").attr("checked", false);

                BrokerageDiscountMethods.fnConfirmRemove();
                BrokerageDiscountMethods.openStatusModal();
                BrokerageDiscountMethods.viewHistoryDetail();
                BrokerageDiscountMethods.viewDescriptionTooltip();

                var totalRecords = this.fnSettings().fnRecordsDisplay();

                if (totalRecords <= 0) {
                    $("#lnkDiscountExcel").hide();
                    $("#lnkDiscountPdf").hide();
                    $("#dvExportar").hide();
                }
                else {
                    $("#lnkDiscountExcel").show();
                    $("#lnkDiscountPdf").show();
                    $("#dvExportar").show();
                }

                var totalPerPage = this.fnSettings()._iDisplayLength;

                var totalPages = Math.ceil(totalRecords / totalPerPage);

                var currentIndex = parseInt(this.fnSettings()._iDisplayStart);
                var currentPage = totalPages > 0 ? Math.ceil(1 + (currentIndex / totalPerPage)) : 0;

                $("#tblBrokerageDiscount_info").html("Exibindo " + currentPage + " de " + totalPages);

                if (totalRecords > 0 && totalPages > 1) {
                    $("#tblBrokerageDiscount_info").css("display", "");
                    $("#tblBrokerageDiscount_length").css("display", "");
                    $("#tblBrokerageDiscount_paginate").css("display", "");
                }
                else {
                    $("#tblBrokerageDiscount_info").css("display", "none");
                    $("#tblBrokerageDiscount_length").css("display", "none");
                    $("#tblBrokerageDiscount_paginate").css("display", "none");
                }

            }
            , aoColumns: [
                { bSearchable: false, bSortable: false },
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                { sClass: "direita" },
                { sClass: "direita" },
                { sClass: "descricao" },
                null,
                null
            ]
};

var BrokerageDiscountMethods = {
    fnConfirmRemove: function () {
        $(".exclude-brokerage").each(function () {

            var that = $(this);
            $(this).unbind("click");

            $(this).bind("click", function () {
                var ids = that.parent().parent().find('input.chk-broker-discount').attr('value');

                var url = "/Other/BrokerageDiscountDelete/?requestId=" + ids;
                commonHelper.OpenModal(url, 450, ".content", "");
                $("#MsgBox").center();

            });
        });

    }
        , fnRemove: function (bAjaxServer) {
            $(".exclude-brokerage").each(function () {

                var that = $(this);
                $(this).unbind("click");

                $(this).bind("click", function () {
                    var ids = that.parent().parent().find('input.chk-broker-discount').attr('value');

                    $.ajax({
                        type: "POST",
                        cache: false,
                        async: false,
                        url: '../Other/RemoveBrokerageDiscount',
                        data: { id: ids },
                        dataType: 'json',
                        beforeSend: function () {
                            $("#loadingArea").show();
                            $("#feedback").hide().find("span").html('');
                            $("#feedbackError").hide().find("span").html('');
                        },
                        success: function (data) {
                            $("#loadingArea").hide();
                            $("#feedbackError").hide().find("span").html("");
                            $("#feedback").hide().find("span").html("");

                            if (data.Result) {
                                selector = "#feedback";
                                if (!bAjaxServer) {

                                    that.parent().parent().remove();

                                } else {
                                    oTableBroker.fnDraw(true);

                                }
                                BrokerageDiscountMethods.getPendingDiscounts();
                            }
                            else {
                                selector = "#feedbackError";
                            }
                            $(selector).show().find("span").append(data.Message);

                        }
                    });


                });
            });
        }
        , fnApproveConfirm: function () {
            $("#lnkAprovar").unbind("click");
            $("#lnkAprovar").bind("click", function () {
                $("#feedbackError").hide().find("span").html("");
                $("#feedback").hide().find("span").html("");

                var ids = "";

                $("input.chk-broker-discount").each(function () {

                    var status = $(this).parent().find('.hdf-situacao').val();

                    if ($(this).attr("checked") == true && status == "A") {

                        if (ids == "")
                            ids = $(this).attr("value");
                        else
                            ids += ";" + $(this).attr("value");
                    }
                });

                if (ids == "") {
                    $("#feedbackError").show().find("span").append("Favor selecione pelo menos uma solicitação que tenha o status \"Para Aprovar\".");
                }
                else {
                    var url = "/Other/BrokerageDiscountRealize/?requestId=" + ids + "&status=F";
                    commonHelper.OpenModal(url, 450, ".content", "");
                    $("#MsgBox").center();
                }

            });
        }
         , fnEffectuateConfirm: function () {
             $("#lnkEfetuar").unbind("click");
             $("#lnkEfetuar").bind("click", function () {
                 $("#feedbackError").hide().find("span").html("");
                 $("#feedback").hide().find("span").html("");

                 var ids = "";

                 $("input.chk-broker-discount").each(function () {

                     var status = $(this).parent().find('.hdf-situacao').val();

                     if ($(this).attr("checked") == true && status == "F") {
                         if (ids == "")
                             ids = $(this).attr("value");
                         else
                             ids += ";" + $(this).attr("value");
                     }
                 });

                 if (ids == "") {
                     $("#feedbackError").show().find("span").append("Favor selecione pelo menos uma solicitação que tenha o status \"Para efetuar\".");
                 }
                 else {
                     var url = "/Other/BrokerageDiscountRealize/?requestId=" + ids + "&status=E";
                     commonHelper.OpenModal(url, 450, ".content", "");
                     $("#MsgBox").center();
                 }

             });
         }
        , fnApprove: function () {
            $("#lnkAprovar").unbind("click");
            $("#lnkAprovar").bind("click", function () {
                $("#feedbackError").hide().find("span").html("");
                $("#feedback").hide().find("span").html("");

                var ids = "";

                $("input.chk-broker-discount").each(function () {
                    if ($(this).attr("checked") == true) {

                        if (ids == "")
                            ids = $(this).attr("value");
                        else
                            ids += ";" + $(this).attr("value");
                    }
                });

                if (ids == "") {
                    $("#feedbackError").show().find("span").append("Favor selecione pelo menos uma solicitação.");
                }
                else {
                    $.ajax({
                        type: "POST",
                        cache: false,
                        async: false,
                        url: '../Other/ApproveBrokerageDiscount',
                        data: { ids: ids, ddlStatus: "F" },
                        dataType: 'json',
                        beforeSend: function () {
                            $("#loadingArea").show();
                            $("#feedback").hide().find("span").html('');
                            $("#feedbackError").hide().find("span").html('');
                        },
                        success: function (data) {
                            $("#loadingArea").hide();
                            $("#feedbackError").hide().find("span").html("");
                            $("#feedback").hide().find("span").html("");

                            if (data.Result) {
                                selector = "#feedback";

                                oTableBroker.fnDraw(true);
                                BrokerageDiscountMethods.getPendingDiscounts();
                            }
                            else {
                                selector = "#feedbackError";
                            }
                            $(selector).show().find("span").append(data.Message);
                        }
                    });
                }

            });
        }
        , fnEffectuate: function () {
            $("#lnkEfetuar").unbind("click");
            $("#lnkEfetuar").bind("click", function () {
                $("#feedbackError").hide().find("span").html("");
                $("#feedback").hide().find("span").html("");

                var ids = "";

                $("input.chk-broker-discount").each(function () {
                    if ($(this).attr("checked") == true) {

                        if (ids == "")
                            ids = $(this).attr("value");
                        else
                            ids += ";" + $(this).attr("value");
                    }
                });

                if (ids == "") {
                    $("#feedbackError").show().find("span").append("Favor selecione pelo menos uma solicitação.");
                }
                else {
                    $.ajax({
                        type: "POST",
                        cache: false,
                        async: false,
                        url: '../Other/EffectuateBrokerageDiscount',
                        data: { ids: ids, ddlStatus: "E" },
                        dataType: 'json',
                        beforeSend: function () {
                            $("#loadingArea").show();
                            $("#feedback").hide().find("span").html('');
                            $("#feedbackError").hide().find("span").html('');
                        },
                        success: function (data) {
                            $("#loadingArea").hide();
                            $("#feedbackError").hide().find("span").html("");
                            $("#feedback").hide().find("span").html("");
                            if (data.Result) {
                                selector = "#feedback";

                                oTableBroker.fnDraw(true);

                                BrokerageDiscountMethods.getPendingDiscounts();
                            }
                            else {
                                selector = "#feedbackError";
                            }
                            $(selector).show().find("span").append(data.Message);
                        }
                    });
                }

            });
        }
        , openStatusModal: function () {
            $(".exibir-modal").bind("click", function () {

                var offset = $(this).offset();
                $('.modal-situacao').css("top", offset.top);
                //$('.modal-situacao').css("left", offset.left);
                $("#hdfRequestId", ".modal-situacao").val($(this).parent().parent().find(".chk-broker-discount").attr("value"));
                $("#feedbackStatus", ".modal-situacao").hide();

                var status = $(this).parent().parent().find('.hdf-situacao').val();

                $.ajax({
                    cache: false,
                    async: false,
                    type: 'POST',
                    url: '../Other/GetBrokerageDiscountStatus',
                    data: { status: status },
                    dataType: "json",
                    success: function (data) {
                        $("#ddlStatusChange").html('');

                        for (var i = 0; i < data.length; i++) {
                            $("#ddlStatusChange").append(
                                $("<option value='" + data[i].Value + "'>" + data[i].Description + "</option>").clone()
                            );
                        }
                    }
                })


                $('#ddlStatusChange').styleInputs({ width: "104px" });

                $("#btnSettle").unbind("click");
                $("#btnSettle").bind("click", function () {
                    $("#feedbackStatus").hide();
                    if ($("#ddlStatusChange").val() != "--") {
                        $.ajax({
                            type: "POST",
                            cache: false,
                            async: false,
                            url: '../Other/ApproveBrokerageDiscount',
                            data: { ids: $("#hdfRequestId").val(), ddlStatus: $("#ddlStatusChange").val() },
                            dataType: 'json',
                            beforeSend: function () {
                                $("#loadingArea").show();
                                $("#feedbackError").hide().find("span").html("");
                                $("#feedback").hide().find("span").html("");
                            },
                            success: function (data) {
                                $("#loadingArea").hide();
                                if (data.Result) {
                                    selector = "#feedback";

                                    oTableBroker.fnDraw(true);

                                }
                                else {
                                    selector = "#feedbackError";
                                }
                                $(selector).show().find("span").append(data.Message);
                                $('.modal-situacao').hide();

                                BrokerageDiscountMethods.getPendingDiscounts();
                            }
                        });
                    }
                    else {
                        $("#feedbackStatus").show();
                    }
                });

                $('.modal-situacao').show();
            });

            $('.closeLink').bind({
                "click": function () {
                    $('.modal-situacao').hide();
                    return false;
                }
            });
        }
        , viewHistory: function () {
            $("#lnkViewHistory").bind("click", function () {
                commonHelper.OpenModal("/Other/BrokerageDiscountHistory", 671, ".content", "");
            });
        }
        , viewHistoryDetail: function () {
            $(".view-log-detail").each(function () {
                $(this).unbind("click");
                $(this).bind("click", function () {
                    var request = $(this).parent().parent().find(".chk-broker-discount").attr("value");
                    commonHelper.OpenModal("/Other/BrokerageDiscountHistoryDetail/?requestID=" + request, 671, ".content", "");
                });
            });
        }
        , viewDescriptionTooltip: function () {

            $(".descricao").hover(function () {
                $(this).find("div.tooltip").show();
            }, function () {
                $(this).find("div.tooltip").hide();
            });
        }
        , enableExportButtons: function () {
            var data = $("#tblBrokerageDiscount tbody tr");
            var emptyData = (data.length == 1 && data[0].lastChild.className == "dataTables_empty") ? true : false;
            var excelLink = $("#lnkDiscountExcel");
            var pdfLink = $("#lnkDiscountPdf");
            //excelLink.attr("href", (emptyData) ? "#" : "/Other/BrokerageDiscountToFile/?isPdf=false");
            //excelLink.attr("target", (emptyData) ? "_self" : "_blank");
            //pdfLink.attr("href", (emptyData) ? "#" : "/Other/BrokerageDiscountToFile/?isPdf=true");
            //pdfLink.attr("target", (emptyData) ? "_self" : "_blank");

            var fnExportFilter = function (isPDF) {
                $.ajax({
                    type: "POST",
                    cache: false,
                    async: false,
                    url: "../Other/BrokerageDiscountFilterOptions",
                    data: {
                        ddlStockMarket: $("#ddlStockMarket").val(),
                        ddlMarketType: $("#ddlMarketType").val(),
                        ddlOperation: $("#ddlOperation").val(),
                        ddlVigency: $("#ddlVigency").val(),
                        ddlSituation: $("#ddlSituation").val(),
                        txtClient: $("#txtClientFilter").val(),
                        txtAssessor: $("#txtAssessor").val(),
                        rdbDiscountType: $("input[name='rdbDiscountType']:checked").val(),
                        txtMinorDiscountPercentual: $('#txtMinorDiscountPercentual').val(),
                        txtMajorDiscountPercentual: $('#txtMajorDiscountPercentual').val(),
                        txtMinorDiscountValue: $('#txtMinorDiscountValue').val(),
                        txtMajorDiscountValue: $('#txtMajorDiscountValue').val(),
                        iSortCol_0: oTableBroker.fnSettings().aaSorting[0][0],
                        sSortDir_0: oTableBroker.fnSettings().aaSorting[0][1]
                    },
                    beforeSend: function () {
                        $("#feedbackError").remove();
                    },
                    success: function (data) {
                        if (data)
                            window.location = "/Other/BrokerageDiscountToFile/?isPdf=" + isPDF;
                        else {
                            $("#emptyArea").before("<div id='feedbackError' class='erro'>Ocorreu um erro na hora da exportação.</div>");
                        }
                    }
                });
            };

            excelLink.unbind("click");
            excelLink.bind("click", function () {
                fnExportFilter("false");
            });

            pdfLink.unbind("click");
            pdfLink.bind("click", function () {
                fnExportFilter("true");
            });
        }
        , openBrokerageDiscountRequest: function () {

            $("#btnRequest").bind("click", function () {
                commonHelper.OpenModal("/Other/BrokerageDiscountRequest/", 610, ".content", "");
                $("#MsgBox").center();
            });
        }
        , getPendingDiscounts: function () {

            $.ajax({
                type: "POST",
                cache: false,
                url: "../Other/GetPendingDiscounts",
                success: function (data) {
                    if (data != null && data != undefined) {
                        if (data > 0) {
                            $("#pendingTag > span > strong").html(data);
                        }
                        else {
                            $("#pendingTag").hide();
                        }
                    }
                }
            });

        }
};

    $(function () {
        if (oTableBroker != null)
            oTableBroker = null;
        oTableBroker = $("#tblBrokerageDiscount").dataTable(objBrokerInitial);


        BrokerageDiscountMethods.openBrokerageDiscountRequest();

        BrokerageDiscountMethods.fnApproveConfirm();
        BrokerageDiscountMethods.fnEffectuateConfirm();

        $("#checkAll").bind("click", function () {
            $(".chk-broker-discount:not(:disabled)").attr("checked", $("#checkAll").attr("checked"));
        });

        BrokerageDiscountMethods.enableExportButtons();

        BrokerageDiscountMethods.viewHistory();

        if ($("#pendingTag").length > 0) {
            if ($('#trilha').children('ul').find('li').last().length > 0)
                $('#trilha').children('ul').find('li').last().append($("#pendingTag"));
            else
                $('#trilha').children('ul').append($("#pendingTag"));
            $("#pendingTag").show();
        }

    });
