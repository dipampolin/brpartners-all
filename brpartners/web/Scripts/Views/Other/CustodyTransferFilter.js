﻿
$(function () {
    var context = $('#CustodyTransferContent');

    $("#txtStartDate").setMask("99/99/9999").datepicker().datepicker($.datepicker.regional['pt-BR']);
    $("#txtEndDate").setMask("99/99/9999").datepicker().datepicker($.datepicker.regional['pt-BR']);
    $.datepicker.setDefaults($.datepicker.regional['pt-BR']);

    $("#CustodyTransferFilter").click(function () {
        $(context).slideToggle('fast');
        $('select').styleInputs();
        $(this).toggleClass('aberto');
    });

    $(context).bind('keydown', function (event) {
        if (event.which == 13) {
            $("#lnkSubmitFilter").click();
        }
    });

    $("#lnkSubmitFilter").unbind("click");
    $("#lnkSubmitFilter").bind("click", function () {


        $("#filterFeedbackError").html("").hide();
        $("#CustodytransferContent").hide();
        $("#loadingArea").show();
        $("#table-area").hide();
        $("#feedback").hide().find("span").html('');
        $("#feedbackError").hide().find("span").html('');

        if (oTableCustodyTransfer != null) {
            oTableCustodyTransfer.fnDestroy();
            oTableCustodyTransfer = null;
        }

        oTableCustodyTransfer = $("#tblCustodyTransfer").dataTable(objCustodyFilter);
        $("#CustodyTransferFilter").click();
        //BrokerageDiscountMethods.enableExportButtons();

    });
});

