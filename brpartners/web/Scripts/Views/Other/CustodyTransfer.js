﻿
var oTableCustodyTransfer = null;

var objCustodyFilter = {
    "bAutoWidth": false
        , "bPaginate": true
        , "bDestroy": true
        , "sPaginationType": "full_numbers"
        , "sDom": 'trip'
        , "oLanguage": {
            "sLengthMenu": "Exibindo _MENU_ registros",
            "sZeroRecords": "Nenhum registro encontrado.",
            "sInfo": "Encontramos _TOTAL_ registros",
            "sInfoEmpty": "Nenhum registro encontrado",
            "sInfoFiltered": "(Filtrando _MAX_ registros)",
            "sSearch": "Buscar:",
            "sProcessing": "Processando...",
            "oPaginate": {
                "sFirst": "Primeiro",
                "sPrevious": "Anterior",
                "sNext": "Próximo",
                "sLast": "Último"
            }
        }
        , "bProcessing": true
        , "bServerSide": true
        , "sAjaxSource": "../Other/CustodyTransferFilter"
        , "fnServerData": function (sSource, aoData, fnCallback) {
            if (aoData != null && aoData != undefined) {
                aoData.push({ "name": "txtAssessor", "value": $('#txtAssessor').val() });
                aoData.push({ "name": "txtClientFilter", "value": $('#txtClientFilter').val() });
                aoData.push({ "name": "ddlStatus", "value": $('#ddlStatus').val() });
                aoData.push({ "name": "ddlTransferType", "value": $('#ddlTransferType').val() });
                aoData.push({ "name": "txtStartDate", "value": $('#txtStartDate').val() });
                aoData.push({ "name": "txtEndDate", "value": $('#txtEndDate').val() });
            }
            $.ajax({
                "cache": false,
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                "beforeSend": function () {
                    $("#table-area").hide();
                    $("#loadingArea").show();
                    $("#emptyArea").hide();
                },
                "success": function (data) {
                    fnCallback(data);

                    if (data.iTotalRecords == 0) {
                        $("#table-area").hide();
                        $('#emptyArea').html('Sua pesquisa não encontrou resultados válidos. Tente novamente.').show();
                    }
                    else {
                        $('#emptyArea').html('').hide();
                    }
                }
            });
        }
        , fnDrawCallback: function () {
            $("#checkAll").attr("checked", false);

            $("#loadingArea").hide();
            $("#table-area").show();

            CustodyTransferMethods.fnConfirmRemove();
            CustodyTransferMethods.openStatusModal();
            CustodyTransferMethods.viewHistoryDetail();
            CustodyTransferMethods.fnEdit();
            CustodyTransferMethods.viewDescriptionTooltip();

            var totalRecords = this.fnSettings().fnRecordsDisplay();

            if (totalRecords <= 0) {
                $("#lnkExcel").hide();
                $("#lnkPdf").hide();
                $("#dvExportar").hide();
            }
            else {
                $("#lnkExcel").show();
                $("#lnkPdf").show();
                $("#dvExportar").show();
            }

            var totalPerPage = this.fnSettings()._iDisplayLength;

            var totalPages = Math.ceil(totalRecords / totalPerPage);

            var currentIndex = parseInt(this.fnSettings()._iDisplayStart);
            var currentPage = totalPages > 0 ? Math.ceil(1 + (currentIndex / totalPerPage)) : 0;

            $("#tblCustodyTransfer_info").html("Exibindo " + currentPage + " de " + totalPages);

            if (totalRecords > 0 && totalPages > 1) {
                $("#tblCustodyTransfer_info").css("display", "");
                $("#tblCustodyTransfer_length").css("display", "");
                $("#tblCustodyTransfer_paginate").css("display", "");
            }
            else {
                $("#tblCustodyTransfer_info").css("display", "none");
                $("#tblCustodyTransfer_length").css("display", "none");
                $("#tblCustodyTransfer_paginate").css("display", "none");
            }
	    }
		, "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			var i = 5;
			if (!isNaN(parseFloat(aData[i]))) {
				var number = parseFloat(aData[i].replace(/\./g, "").replace(/\,/g, "."));
				if (number > 0) {
					$(nRow).find("td:eq(" + i.toString() + ")").addClass("positivo");
				}
				else if (number < 0) {
					$(nRow).find("td:eq(" + i.toString() + ")").addClass("negativo");
				}
			}
			return nRow;
        }
        , aoColumns: [
            { bSearchable: false, bSortable: false },
            null,
            null,
            null,
            null,
			{ "sClass": "direita" },
            null,
            null,
            null,
            null,
            null,
            null,
            { sClass: "descricao" },
            { bSearchable: false, bSortable: false }
        ]
};



var objCustodyInitial = {
    "bAutoWidth": false
            , "bPaginate": true
            , "bDestroy": true
            , "sPaginationType": "full_numbers"
            , "sDom": 'trip'
            , "oLanguage": {
                "sLengthMenu": "Exibindo _MENU_ registros",
                "sZeroRecords": "Nenhum registro encontrado.",
                "sInfo": "Encontramos _TOTAL_ registros",
                "sInfoEmpty": "Nenhum registro encontrado",
                "sInfoFiltered": "(Filtrando _MAX_ registros)",
                "sSearch": "Buscar:",
                "sProcessing": "Processando...",
                "oPaginate": {
                    "sFirst": "Primeiro",
                    "sPrevious": "Anterior",
                    "sNext": "Próximo",
                    "sLast": "Último"
                }
            }
            , "bProcessing": true
            , "bServerSide": true
            , "sAjaxSource": "../Other/InitialCustodyTransferFilter"
            , "fnServerData": function (sSource, aoData, fnCallback) {
                var status = commonHelper.GetParameterByName("status");
                if (aoData != null && aoData != undefined && status != undefined && status != null && status != "") {
                    aoData.push({ "name": "ddlSituation", "value": status });

                    $("#ddlSituation").val(status);
                }
                $.ajax({
                    "cache": false,
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "beforeSend": function () {
                        $("#table-area").hide();
                        $("#loadingArea").show();
                        $("#emptyArea").hide();
                    },
                    "success": function (data) {
                        fnCallback(data);

                        if (data.iTotalRecords == 0) {
                            $("#table-area").hide();
                            $('#emptyArea').html('Sua pesquisa não encontrou resultados válidos. Tente novamente.').show();
                        }
                        else {
                            $('#emptyArea').html('').hide();
                        }
                    }
                });
            }
            , fnDrawCallback: function () {
                $("#loadingArea").hide();
                $("#table-area").show();
                $("#checkAll").attr("checked", false);

                CustodyTransferMethods.fnConfirmRemove();
                CustodyTransferMethods.openStatusModal();
                CustodyTransferMethods.viewHistoryDetail();
                CustodyTransferMethods.fnEdit();
                CustodyTransferMethods.viewDescriptionTooltip();

                var totalRecords = this.fnSettings().fnRecordsDisplay();

                if (totalRecords <= 0) {
                    $("#lnkExcel").hide();
                    $("#lnkPdf").hide();
                    $("#dvExportar").hide();
                }
                else {
                    $("#lnkExcel").show();
                    $("#lnkPdf").show();
                    $("#dvExportar").show();
                }

                var totalPerPage = this.fnSettings()._iDisplayLength;

                var totalPages = Math.ceil(totalRecords / totalPerPage);

                var currentIndex = parseInt(this.fnSettings()._iDisplayStart);
                var currentPage = totalPages > 0 ? Math.ceil(1 + (currentIndex / totalPerPage)) : 0;

                $("#tblCustodyTransfer_info").html("Exibindo " + currentPage + " de " + totalPages);

                if (totalRecords > 0 && totalPages > 1) {
                    $("#tblCustodyTransfer_info").css("display", "");
                    $("#tblCustodyTransfer_length").css("display", "");
                    $("#tblCustodyTransfer_paginate").css("display", "");
                }
                else {
                    $("#tblCustodyTransfer_info").css("display", "none");
                    $("#tblCustodyTransfer_length").css("display", "none");
                    $("#tblCustodyTransfer_paginate").css("display", "none");
                }
		    }
			, "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
				var i = 5;
				if (!isNaN(parseFloat(aData[i]))) {
					var number = parseFloat(aData[i].replace(/\./g, "").replace(/\,/g, "."));
					if (number > 0) {
						$(nRow).find("td:eq(" + i.toString() + ")").addClass("positivo");
					}
					else if (number < 0) {
						$(nRow).find("td:eq(" + i.toString() + ")").addClass("negativo");
					}
				}
				return nRow;
            }
            , aoColumns: [
                { bSearchable: false, bSortable: false },
                null,
                null,
                null,
                null,
				{ "sClass": "direita" },
                null,
                null,
                null,
                null,
                null,
                null,
                { sClass: "descricao" },
                { bSearchable: false, bSortable: false }
            ]
};

var CustodyTransferMethods = {
    fnConfirmRemove: function () {
        $(".ico-excluir").each(function () {

            var that = $(this);
            $(this).unbind("click");

            $(this).bind("click", function () {
                var ids = that.parent().parent().find('input.chk-custody-transfer').attr('value');

                var url = "/Other/CustodyTransferDelete/?requestId=" + ids;
                commonHelper.OpenModal(url, 450, ".content", "");
                $("#MsgBox").center();

            });
        });

    }
    , fnEffectuateConfirm: function () {
        $("#lnkEfetuar").unbind("click");
        $("#lnkEfetuar").bind("click", function () {
            $("#feedbackError").hide().find("span").html("");
            $("#feedback").hide().find("span").html("");

            var ids = "";

            $("input.chk-custody-transfer").each(function () {

                var status = $(this).parent().find('.hdf-situacao').val();

                if ($(this).attr("checked") == true && status == "A") {
                    if (ids == "")
                        ids = $(this).attr("value");
                    else
                        ids += ";" + $(this).attr("value");
                }
            });

            if (ids == "") {
                $("#feedbackError").show().find("span").append("Favor selecione pelo menos uma solicitação que tenha o status \"Para efetuar\".");
            }
            else {
                var url = "/Other/CustodyTransferRealize/?requestId=" + ids + "&status=E";
                commonHelper.OpenModal(url, 450, ".content", "");
            }

        });
    }
    , openStatusModal: function () {
        $(".exibir-modal").bind("click", function () {

            var offset = $(this).offset();
            $('.modal-situacao').css("top", offset.top);
            $('.modal-situacao').css("left", offset.left);
            $("#hdfRequestId", ".modal-situacao").val($(this).parent().parent().find(".chk-custody-transfer").attr("value"));
            $("#feedbackStatus", ".modal-situacao").hide();

            var status = $(this).parent().parent().find('.hdf-situacao').val();

            $.ajax({
                cache: false,
                async: false,
                type: 'POST',
                url: '../Other/GetCustodyTransferStatus',
                data: { status: status },
                dataType: "json",
                success: function (data) {
                    $("#ddlStatusChange").html('');
                    for (var i = 0; i < data.length; i++) {
                        $("#ddlStatusChange").append(
                            $("<option value='" + data[i].Value + "'>" + data[i].Description + "</option>").clone()
                        );
                    }
                }
            });


            $('#ddlStatusChange').styleInputs({ width: "104px" });

            $("#btnSettle").unbind("click");
            $("#btnSettle").bind("click", function () {
                $("#feedbackStatus").hide();
                if ($("#ddlStatusChange").val() != "--") {
                    $.ajax({
                        type: "POST",
                        cache: false,
                        async: false,
                        url: '../Other/ApproveCustodyTransfer',
                        data: { ids: $("#hdfRequestId").val(), ddlStatus: $("#ddlStatusChange").val() },
                        dataType: 'json',
                        beforeSend: function () {
                            $("#loadingArea").show();
                            $("#feedbackError").hide().find("span").html("");
                            $("#feedback").hide().find("span").html("");
                        },
                        success: function (data) {
                            $("#loadingArea").hide();
                            if (data.Result) {
                                selector = "#feedback";

                                oTableCustodyTransfer.fnDraw(true);

                            }
                            else {
                                selector = "#feedbackError";
                            }
                            $(selector).show().find("span").append(data.Message);
                            $('.modal-situacao').hide();

                            CustodyTransferMethods.getPendingCustodyTransfers();
                        }
                    });
                }
                else {
                    $("#feedbackStatus").show();
                }
            });

            $('.modal-situacao').show();
        });

        $('.closeLink').bind({
            "click": function () {
                $('.modal-situacao').hide();
                return false;
            }
        });
    }
    , viewHistory: function () {
        $("#lnkViewHistory").bind("click", function () {
            commonHelper.OpenModal("/Other/CustodyTransferHistory", 671, ".content", "");
        });
    }
    , viewHistoryDetail: function () {
        $(".view-log-detail").each(function () {
            $(this).unbind("click");
            $(this).bind("click", function () {
                var request = $(this).parent().parent().find(".chk-custody-transfer").attr("value");
                commonHelper.OpenModal("/Other/CustodyTransferHistoryDetail/?requestID=" + request, 671, ".content", "");
            });
        });
    }
    , viewDescriptionTooltip: function () {

        $(".descricao").hover(function () {
            $(this).find("div.tooltip").show();
        }, function () {
            $(this).find("div.tooltip").hide();
        });
    }, enableExportButtons: function () {
        var data = $("#tblCustodyTransfer tbody tr");
        var emptyData = (data.length == 1 && data[0].lastChild.className == "dataTables_empty") ? true : false;
        var excelLink = $("#lnkExcel");
        var pdfLink = $("#lnkPdf");
        excelLink.attr("href", (emptyData) ? "#" : "/Other/CustodyTransferToFile/?isPdf=false");
        //excelLink.attr("target", (emptyData) ? "_self" : "_blank");
        pdfLink.attr("href", (emptyData) ? "#" : "/Other/CustodyTransferToFile/?isPdf=true");
        //pdfLink.attr("target", (emptyData) ? "_self" : "_blank");
    }
    , openCustodyTransferRequest: function () {

        $("#btnRequest").bind("click", function () {
            commonHelper.OpenModal("/Other/CustodyTransferRequest/", 485, ".content", "");
            $("#MsgBox").center();
        });
    }
    , getPendingCustodyTransfers: function () {

        $.ajax({
            type: "POST",
            cache: false,
            url: "../Other/GetPendingCustodyTransfers",
            success: function (data) {
                if (data != null && data != undefined) {
                    if (data > 0) {
                        $("#pendingTag > span > strong").html(data);
                    }
                    else {
                        $("#pendingTag").hide();
                    }
                }
            }
        });

    }
    , fnEdit: function () {
        $(".ico-editar").each(function () {

            var that = $(this);
            $(this).unbind("click");

            $(this).bind("click", function () {
                var ids = that.parent().parent().find('input.chk-custody-transfer').attr('value');

                var url = "/Other/CustodyTransferRequest/" + ids;
                commonHelper.OpenModal(url, 485, ".content", "");
                $("#MsgBox").center();

            });
        });
    }
};

$(function () {
    if (oTableCustodyTransfer != null)
        oTableCustodyTransfer = null;
    oTableCustodyTransfer = $("#tblCustodyTransfer").dataTable(objCustodyInitial);
    
    CustodyTransferMethods.openCustodyTransferRequest();

    CustodyTransferMethods.fnEffectuateConfirm();

    $("#checkAll").bind("click", function () {
        $(".chk-custody-transfer:not(:disabled)").attr("checked", $("#checkAll").attr("checked"));
    });

    CustodyTransferMethods.enableExportButtons();

    CustodyTransferMethods.viewHistory();

    if ($("#pendingTag").length > 0) {
        if ($('#trilha').children('ul').find('li').last().length > 0)
            $('#trilha').children('ul').find('li').last().append($("#pendingTag"));
        else
            $('#trilha').children('ul').append($("#pendingTag"));
        $("#pendingTag").show();
    }

});
