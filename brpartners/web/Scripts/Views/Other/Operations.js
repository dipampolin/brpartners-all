﻿var oTableOperations = null;

var objOperationsInitial = {
   	"bAutoWidth": false
    , "bPaginate": true
    , "bDestroy": true
    , "sPaginationType": "full_numbers"
    , "sDom": 'trip'
    , "oLanguage": {
        "sLengthMenu": "Exibindo _MENU_ registros",
        "sZeroRecords": "Nenhum registro encontrado.",
        "sInfo": "Encontramos _TOTAL_ registros",
        "sInfoEmpty": "Nenhum registro encontrado",
        "sInfoFiltered": "(Filtrando _MAX_ registros)",
        "sSearch": "Buscar:",
        "sProcessing": "Processando...",
        "oPaginate": {
            "sFirst": "Primeiro",
            "sPrevious": "Anterior",
            "sNext": "Próximo",
            "sLast": "Último"
        }
    }
    , "bProcessing": true
    , "bServerSide": true
    , "sAjaxSource": "../Other/ListOperations"
    , "fnServerData": function (sSource, aoData, fnCallback) {
    	var filterContext = $("#operationsFilterContent");
    	if (aoData != null && aoData != undefined) {
    		aoData.push({ "name": "ddlStatus", "value": $('#ddlStatus', filterContext).val() });
    		aoData.push({ "name": "ddlTradingDate", "value": $("#ddlTradingDate", filterContext).val() });
    	}
    	$.ajax({
            "cache": false,
            "dataType": 'json',
            "type": "POST",
            "url": sSource,
            "data": aoData,
            "beforeSend": function () {
            	$("#tableArea").hide();
            	$("#loadingArea").show();
            	$("#emptyArea").hide();
            },
            "success": function (data) {
            	fnCallback(data);
            	if (data.iTotalRecords == 0) {
            		$("#tableArea").hide();
            		$('#emptyArea').html('Sua pesquisa não encontrou resultados válidos. Tente novamente.').show();
            	}
            	else {
            		$('#emptyArea').html('').hide();
            	}
            	OperationsMethods.enableExportButtons();
            }
        });
    }
    , fnDrawCallback: function () {
        fnDrawCallback(this);
    }
    , "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        return nRow;
    }
    , "aoColumns": [
        { "bSortable": false },
        null,
        null,
        null,
        null,
        null,
        { "bSortable": false }
    ]
	, "aaSorting": [[ 1, "asc" ]]
};

var objOperationsFilter = {
    "bAutoWidth": false
    , "bPaginate": true
    , "bDestroy": true
    , "sPaginationType": "full_numbers"
    , "sDom": 'trip'
    , "oLanguage": {
        "sLengthMenu": "Exibindo _MENU_ registros",
        "sZeroRecords": "Nenhum registro encontrado.",
        "sInfo": "Encontramos _TOTAL_ registros",
        "sInfoEmpty": "Nenhum registro encontrado",
        "sInfoFiltered": "(Filtrando _MAX_ registros)",
        "sSearch": "Buscar:",
        "sProcessing": "Processando...",
        "oPaginate": {
            "sFirst": "Primeiro",
            "sPrevious": "Anterior",
            "sNext": "Próximo",
            "sLast": "Último"
        }
    }
    , "bProcessing": true
    , "bServerSide": true
    , "sAjaxSource": "../Other/ListOperations"
    , "fnServerData": function (sSource, aoData, fnCallback) {
        var filterContext = $("#operationsFilterContent");
        if (aoData != null && aoData != undefined) {
        	aoData.push({ "name": "txtAssessor", "value": $("#txtAssessor", filterContext).val() });
        	aoData.push({ "name": "txtCliente", "value": $("#txtCliente", filterContext).val() });
            aoData.push({ "name": "ddlStatus", "value": $('#ddlStatus', filterContext).val() });
            aoData.push({ "name": "ddlTradingDate", "value": $("#ddlTradingDate", filterContext).val() });
        }
        $.ajax({
            "cache": false,
            "dataType": 'json',
            "type": "POST",
            "url": sSource,
            "data": aoData,
            "beforeSend": function () {
                $("#tableArea").hide();
                $("#loadingArea").show();
                $("#emptyArea").hide();
            },
            "success": function (data) {
            	fnCallback(data);
                if (data.iTotalRecords == 0) {
                    $("#tableArea").hide();
                    $('#emptyArea').html('Sua pesquisa não encontrou resultados válidos. Tente novamente.').show();
                }
                else {
                    $('#emptyArea').html('').hide();
                }
                OperationsMethods.enableExportButtons();
            }
        });
    }
    , fnDrawCallback: function () {
        fnDrawCallback(this);
    }
    , "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        return nRow;
    }
    , "aoColumns": [
        { "bSortable": false },
        null,
        null,
        null,
        null,
        null,
        { "bSortable": false }
    ]
	, "aaSorting": [[ 1, "asc" ]]
};

var fnDrawCallback = function (settings) {
    $("#loadingArea").hide();
    $("#tableArea").show();

    var totalRecords = settings.fnSettings().fnRecordsDisplay();

    var totalPerPage = settings.fnSettings()._iDisplayLength;

    var totalPages = Math.ceil(totalRecords / totalPerPage);

    var currentIndex = parseInt(settings.fnSettings()._iDisplayStart);
    var currentPage = totalPages > 0 ? Math.ceil(1 + (currentIndex / totalPerPage)) : 0;
    $("#operationsList_info").html("Exibindo " + currentPage + " de " + totalPages);

    if (totalRecords > 0 && totalPages > 1) {
        $("#operationsList_info").css("display", "");
        $("#operationsList_length").css("display", "");
        $("#operationsList_paginate").css("display", "");
    }
    else {
        $("#operationsList_info").css("display", "none");
        $("#operationsList_length").css("display", "none");
        $("#operationsList_paginate").css("display", "none");
    }

    //OperationsMethods.OperationsDetail();
    //    OperationsMethods.OperationsDelete();
    OperationsMethods.OperationsHistoryDetail();
    OperationsMethods.OperationsSendMail();
}

var OperationsMethods = {
    enableExportButtons: function () {
        var data = $("#operationsList tbody tr");
        var emptyData = (data.length == 1 && data[0].lastChild.className == "dataTables_empty") ? true : false;
        var excelLink = $("#lnkOperationsExcel");
        var pdfLink = $("#lnkOperationsPdf");
        excelLink.attr("href", (emptyData) ? "javascript:void(0);" : "/Other/OperationsToFile/?isPdf=false");
        pdfLink.attr("href", (emptyData) ? "javascript:void(0);" : "/Other/OperationsToFile/?isPdf=true");
    }

    , OperationsDetail: function () {
        $(".ico-visualizar, .lnk-detail").unbind("click").bind("click", function () {
            var value = $(this).parent().parent().find("input.chkOperation").attr('value');
            var tradingDate = value.split("|")[0];
            var id = value.split("|")[1];
            commonHelper.OpenModalPost("/Other/OperationDetails", 890, ".content", "", { tradingDate: tradingDate, clientCode: id });
        });        
    }

	, OperationsHistory: function () {
	    commonHelper.OpenModal("/Other/OperationsHistory", 671, ".content", "");
	}

    , OperationsHistoryDetail: function () {
        $(".ico-hist", $("#operationsList")).unbind("click").bind("click", function () {
            var value = $(this).parent().parent().find("input.chkOperation").attr('value');
            var tradingDate = value.split("|")[0];
            var id = value.split("|")[1];

            var url = "/Other/OperationsHistoryDetail";
            commonHelper.OpenModalPost(url, "", ".content", "", { tradingDate: tradingDate, clientCode: id });
        });
    }
    , OperationsSendMail: function () {
        $(".ico-email").unbind("click").bind("click", function () {
            var value = $(this).parent().parent().find("input.chkOperation").attr('value');
            var tradingDate = value.split("|")[0];
            var id = value.split("|")[1];

            $.ajax({
                type: "POST",
                data: { tradingDate: tradingDate, clientCode: id },
                url: "../Other/Operations_SendEmailToClient",
                cache: false,
                beforeSend: function () {
                    $("#feedbackError").hide().html('');
                    $("#divOperationsFeedback").hide().find("span").html('');
                },
                success: function (data) {
                    $("#loadingArea").hide();

                    if (data.Result) {
                        
                        $("#divOperationsFeedback").show().find("span").append(data.Message);

                        oTableOperations.fnDraw(true);

                    }
                    else {
                        $("#feedbackError").show().append(data.Message);
                    }
                    

                }
            })
        });

    }
    , OperationsRequest: function () {
        $("#lnkRequest").unbind("click").bind("click", function () {

            $("#feedbackError").html("");

            var ids = new Array();
            var tradingDate = "";

            $("input.chkOperation:checked").each(function () {

                var value = $(this).parent().parent().find("input.chkOperation").attr('value');
                if (tradingDate == "") { tradingDate = value.split("|")[0]; }
                ids.push(value.split("|")[1]);

            });

            if (ids.length > 0) {
                $.ajax({
                    type: "POST",
                    data: { tradingDate: tradingDate, clientCodes: ids },
                    traditional: true,
                    url: "../Other/Operations_SendBatchEmails",
                    cache: false,
                    beforeSend: function () {
                        $("#feedbackError").hide().html('');
                        $("#divOperationsFeedback").hide().find("span").html('');
                    },
                    success: function (data) {
                        $("#loadingArea").hide();

                        if (data.Result) {
                            selector = "#divOperationsFeedback";

                            oTableOperations.fnDraw(true);

                        }
                        else {
                            selector = "#feedbackError";
                        }
                        $(selector).show().append(data.Message);

                    }
                });
            }
            else {
                $("#feedbackError").show().append("Selecione ao menos um cliente.");
            }
        });
    }
    , OperationsOpenMailModel: function () {
        $("#btnOpenModel").unbind("click").bind("click", function () {
            var url = "/Mail/EditEmailModel/";
            commonHelper.OpenModalPost(url, 450, ".content", "", { funcionalityId: 2 });
            $("#MsgBox").center();
        });
    }

};

$(function () {
    $("#chkAll").click(function () {
        var check = $(this).is(':checked');
        $("table#operationsList input:checkbox:not(:disabled)").each(function () { $(this).attr('checked', check); })
    });

    if (oTableOperations != null)
        oTableOperations = null;
    oTableOperations = $("#operationsList").dataTable(objOperationsInitial);

    OperationsMethods.OperationsOpenMailModel();

    OperationsMethods.OperationsRequest();

    tinyMCE.init({

        //mode: "textareas",
        mode: "none",
        theme: "advanced",

        valid_elements: "strong/b,em/i,u,br,table,tr,td,p[align|style],a[href],ul,ol,li",
        plugins: "autolink,lists,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",

        theme_advanced_buttons1: "bold,italic,underline,|,bullist,numlist,link",
        theme_advanced_buttons2: "",
        theme_advanced_buttons3: "",
        theme_advanced_buttons4: "",
        theme_advanced_toolbar_location: "top",
        theme_advanced_toolbar_align: "left",
        theme_advanced_statusbar_location: "",
        theme_advanced_resizing: false,
        skin: "o2k7",
        skin_variant: "silver",
        width: "100%",
        height: "240",
        relative_urls : 0,
        remove_script_host : 0
    });
});
