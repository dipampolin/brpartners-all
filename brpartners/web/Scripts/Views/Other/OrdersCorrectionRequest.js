﻿/// <reference path="../../References.js" />

var stockIndexes = [1];
var currentStockIndex = 1;

$(document).ready(function () {
    $('select').styleInputs();
    $.mask.masks.stockValue = { mask: '99,999.9', type: 'reverse', defaultValue: '000' };
    $.mask.masks.stockQuantity = { mask: '999.999.999', type: 'reverse', defaultValue: '0' };
    $.mask.masks.stockSignedQuantity = { mask: '999.999.999', type: 'reverse', defaultValue: '+0' };
    $.mask.masks.stockCode = { mask: "************" };
    $.mask.masks.clientCode = { mask: '9999999' };

    var context = "#frmNewOrdersCorrection";

    var id = $("#hdfID", context).val();
    if (id != "0") {
        var indexes = $("#hdfIndexes", context).val();
        indexes = indexes.substr(0, indexes.length - 1);
        stockIndexes = indexes.split(',');
        currentStockIndex = stockIndexes.length;
    }

    $("#rblType", context).change(function () {
        if ($(this).val() == 6) { //Troca de Comitente
            $("#pnlClientTo", context).show();
            $("#lblClientFrom", context).html("Cliente Origem");
            $("[id^=txtStockQuantity]").each(function () { $(this).attr("alt", "stockSignedQuantity"); $(this).setMask(); });
        }
        else {
            $("#pnlClientTo", context).hide();
            $("#lblClientFrom", context).html("Cliente");
            $("[id^=txtStockQuantity]").each(function () { $(this).attr("alt", "stockQuantity"); $(this).setMask(); });
        }

        $("#pnlCorrectionRequest", context).show();

        $("#MsgBox").center();
    });

    $("#lnkAddError", context).click(function () {
        currentStockIndex++;
        var tableContext = $("#tbErrors", context);

        var quantityMask = ($('input[name=rblType]:checked', context).val() == 6) ? "stockSignedQuantity" : "stockQuantity";

        var line = "<tr id='stockItem" + currentStockIndex + "'><td><input style='text-transform: uppercase' id='txtStockCode" + currentStockIndex + "' name='txtStockCode" + currentStockIndex + "' type='text' alt='stockCode' class='papel' value='' maxlength='5' /></td><td><input id='txtStockQuantity" + currentStockIndex + "' name='txtStockQuantity" + currentStockIndex + "' alt='" + quantityMask + "' type='text' class='qtde' value='' maxlength='9' /></td><td><input id='txtStockPrice" + currentStockIndex + "' name='txtStockPrice" + currentStockIndex + "'  type='text' maxlength='8' alt='stockValue' class='preco' value='' /></td><td><a class='ico-excluir lnkDeleteStock' onclick='DeleteStock(" + currentStockIndex + ")' href='#'>excluir</a></td></tr>";
        tableContext.find("tbody").append(line);

        $(".lnkDeleteStock").show();
        SetMasks(currentStockIndex, tableContext);
        stockIndexes.push(currentStockIndex.toString());

        return false;
    });

    $("#lnkSaveCorrection", context).click(function () {
        var feedback = ValidateFields(context);
        if (feedback == "") {
            var form = $("#frmNewOrdersCorrection").serialize();
            $.post("/Other/OrdersCorrectionRequest", form, function (data) {
                if (data.Error == "") {
                    $("span", "#feedback").html(data.Message);
                    $("#feedback").show();
                    fnSearch($("#ordersFilterContent"));
                }
                else {
                    $("#feedbackError").html(data.Error);
                    $("#feedbackError").show();
                }
                Modal.Close();
            }, "json");
        }
        else {
            $("#feedbackError", context).html(feedback);
            $("#feedbackError", context).show();
            return false;
        }
    });

    SetMasks(1, $("#tbErrors", context));
    $("#txtClientFrom", context).blur(function () {
        commomDataHelper.GetClientName($(this).val(), "lblClientFromName", context, "txtClientFrom");
    }).setMask();
    $("#txtClientTo").blur(function () {
        commomDataHelper.GetClientName($(this).val(), "lblClientToName", context, "txtClientTo");
    }).setMask();
    $("#txtResponsible").blur(function () {
        if ($("#txtResponsible").val() != $("#hdfResponsibleName").val())
            $("#txtResponsible").val('');
    });
    $("#txtResponsible").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/Other/AssessorNameSuggest",
                type: "POST",
                dataType: "json",
                data: { term: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        return {
                            label: item.Description,
                            value: item.Description,
                            id: item.Value
                        }
                    }));
                }
            });
        },
        minLength: 3,
        select: function (event, ui) {
            $("#txtResponsible").val(ui.item.label);
            $("#hdfResponsibleId").val(ui.item.id);
            $("#hdfResponsibleName").val(ui.item.label);
        }
    });

    $("#txtComments", context).keydown(function () {
        commonHelper.LimitTextArea("#txtComments", "", 150);
    });
})

function ValidateFields(context) {
    var feedback = "";
    var change = $('input[name=rblType]:checked', context).val() == 6;

    if ($("#txtClientFrom", context).val() == "") {
        feedback += change ? "<li>Cliente Origem em branco.</li>" : "<li>Cliente em branco.</li>";
    }
    if (change && $("#txtClientTo", context).val() == "") {
        feedback += "<li>Cliente Destino em branco.</li>";
    }

    var stockBlankCounter = 0;
    var hdfIndexes = "";

    for (var i = 0; i < stockIndexes.length; i++) {
        var index = stockIndexes[i];
        hdfIndexes += index + ",";

        var qty = $("#txtStockQuantity" + index + ":input", context).val();
        if (qty == "" || parseInt(qty) == 0)
            stockBlankCounter++;
        var price = $("#txtStockPrice" + index + ":input", context).val();
        price = price.replace(/\./g, "").replace(/\,/g, ".");
        if (price == "" || parseFloat(price) == 0)
            stockBlankCounter++;
        if ($("#txtStockCode" + index + ":input", context).val() == "")
            stockBlankCounter++;
    }

    if (stockIndexes.length == 0 || stockBlankCounter > 0)
        feedback += "<li>Operações em branco.</li>";
    else
        $("#hdfIndexes", context).val(hdfIndexes);

    if ($("#ddlDescription", context).val() == "0") {
        feedback += "<li>Descrição do Erro em branco.</li>";
    }
    
    if ($("#txtResponsible", context).val() == "") {
        feedback += "<li>Operador Responsável em branco.</li>";
    }

    if ($("#txtComments", context).val() == "") {
        feedback += "<li>Observação em branco.</li>";
    }

    var date = $('input[name=rblDate]:checked', context).val();
    if (date == "" || date == undefined) {
        feedback += "<li>Data de Compra/Venda em branco.</li>";
    }

    if (feedback.length > 0)
        feedback = "<ul>" + feedback + "</ul>";
    return feedback;
}

function DeleteStock(stockId) {
    $("#stockItem" + stockId, $("#tbErrors")).remove();

    for (var i = 0; i < stockIndexes.length; i++) {
        if (stockIndexes[i] == stockId)
            stockIndexes.splice(i, 1);
    }

    if (stockIndexes.length == 1) {
        $(".lnkDeleteStock").hide();
    }
}

function SetMasks(id, context) {
    $("[id^=txtStock]", context).setMask();

}