﻿$(document).ready(function () {
	SortOptions.AddSortOptions();
	handlers();
});

function handlers() {
	var context = $("#historyArea");

	$('#ddlHistoryActions', context).styleInputs();

	var emptyDataMessage = "Sua pesquisa não encontrou resultados válidos. Tente novamente.";

	$("#txtStartDate", context).setMask("99/99/9999");
	$("#txtEndDate", context).setMask("99/99/9999");
	$("#txtTradeDate", context).setMask("99/99/9999");

	$("#txtClientCode", context).blur(function () {
		commomDataHelper.GetClientName($(this).val(), "lblClientName", context);
	}).setMask();

	$.datepicker.setDefaults($.datepicker.regional['pt-BR']);
	$("#txtStartDate", context).datepicker($.datepicker.regional['pt-BR']);
	$("#txtEndDate", context).datepicker($.datepicker.regional['pt-BR']);
	$("#txtTradeDate", context).datepicker($.datepicker.regional['pt-BR']);

	$("#txtResponsible", context).blur(function () {
		if (this.value == "")
			$("#hdfResponsible", context).val("");
		else
			commomDataHelper.ValidateUsernameSuggest(this.value, "#hdfResponsible", context);
	});

	$("#txtResponsible", context).autocomplete({
		source: function (request, response) {
			$.ajax({
				url: "/AccessControl/UserAutoSuggest",
				type: "POST",
				dataType: "json",
				data: { term: request.term },
				success: function (data) {
					response($.map(data, function (item) {
						return {
							label: item.Name,
							value: item.Name,
							id: item.ID
						}
					}));
				}
			});
		},
		minLength: 1,
		select: function (event, ui) {
			$("#hdfResponsible", context).val(ui.item.id);
		}
	});

	var breadcrumb = $("#trilha");
	if (breadcrumb.length > 0) {
		$("#breadcrumb", context).html(breadcrumb.html());
	}

	$("#btnSearch", context).click(function () {
		//commonHelper.ShowLoadingArea();
		$.ajax({
			cache: false,
			async: false,
			type: "POST",
			url: "/Other/LoadOperationsHistory",
			beforeSend: function () {
				$("#loadingArea", context).show();
				$("#tableArea, #emptyArea", context).hide();
			},
			data: {
				txtStartDate: $("#txtStartDate", context).val(),
				txtEndDate: $("#txtEndDate", context).val(),
				hdfResponsible: $("#hdfResponsible", context).val(),
				txtTradeDate: $("#txtTradeDate", context).val(),
				txtClientCode: $("#txtClientCode", context).val(),
				ddlHistoryActions: $("#ddlHistoryActions", context).val(),
				hdfRequestID: $("#hdfRequestID", context).val()
			},
			success: function () {
				obj = {};
				obj = {
					"bSort": true,
					"bAutoWidth": false,
					"bInfo": true,
					"bSort": true,
					"oLanguage": {
						"sProcessing": "<img src='/img/loading.gif'>",
						"sLengthMenu": "<span>Exibir:</span> _MENU_ <span>por página</span>",
						"sZeroRecords": "Sua pesquisa não encontrou resultados válidos. Tente novamente.",
						"oPaginate": {
							"sPrevious": "Anteriores",
							"sNext": "Próximos"
						}
					},
					"aaSorting": [[0, "desc"]],
					"sPaginationType": "full_numbers",
					"sDom": 'rtip',
					"aoColumns": [
                        { sType: "date-euro" },
                        null,
                        null,
                        null,
                        { sType: "date-eu" }
                    ],
					"fnDrawCallback": function () {
						var totalRecords = this.fnSettings().fnRecordsDisplay();
						var emptyArea = $("#emptyArea", context);
						var tableArea = $("#tableArea", context);

						var excelLink = $("#lnkHistoryExcel", context);
						var pdfLink = $("#lnkHistoryPdf", context);

						if (excelLink.length > 0 && pdfLink.length > 0) {
							excelLink.attr("href", (totalRecords == 0) ? "javascript:void(0);" : "../Other/OperationsHistoryToFile/?isPdf=false&detail=false");
							//excelLink.attr("target", (totalRecords == 0) ? "_self" : "_blank");
							pdfLink.attr("href", (totalRecords == 0) ? "javascript:void(0);" : "../Other/OperationsHistoryToFile/?isPdf=true&detail=false");
							//pdfLink.attr("target", (totalRecords == 0) ? "_self" : "_blank");
						}

						if (totalRecords == 0) {
							emptyArea.html(emptyDataMessage);
							emptyArea.show();
							tableArea.hide();
						}
						else {
							tableArea.show();
							emptyArea.hide();

							var totalPerPage = this.fnSettings()._iDisplayLength;

							var totalPages = Math.ceil(totalRecords / totalPerPage);

							var currentIndex = parseInt(this.fnSettings()._iDisplayStart);
							var currentPage = totalPages > 0 ? Math.ceil(1 + (currentIndex / totalPerPage)) : 0;

							$("#historyList_info").html("Exibindo " + currentPage + " de " + totalPages);

							if (totalRecords > 0 && totalPages > 1) {
								$("#historyList_info").css("display", "");
								$("#historyList_length").css("display", "");
								$("#historyList_paginate").css("display", "");
							}
							else {
								$("#historyList_info").css("display", "none");
								$("#historyList_length").css("display", "none");
								$("#historyList_paginate").css("display", "none");
							}
							//$('select', '#historyList_length').styleInputs();
						}
						// Adiciona a classe 'ultimo' à última tr no corpo da tabela
						$('table tbody tr:last-child').addClass('ultimo');

						// Adiciona a classe 'ultimo' às últimas th e td no corpo da tabela
						$('table tr td:last-child').addClass('ultimo');
						$('table tr th:last-child').addClass('ultimo');
						$("#MsgBox").center();
						$("#loadingArea").hide();
					},
					"bProcessing": false,
					"fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
						$('#historyList_first').css("display", "none");
						$('#historyList_last').css("display", "none");
					},
					"bRetrieve": true,
					"bServerSide": true,
					"sAjaxSource": "../Other/OperationsHistoryList",
					"fnServerData": function (sSource, aoData, fnCallback) {
						$.ajax({
							cache: false,
							dataType: 'json',
							type: "POST",
							url: sSource,
							data: aoData,
							success: fnCallback
						});
					}
				};

				oTable = $("#historyList").dataTable(obj);
				oTable.fnClearTable();
				oTable.fnDraw();
			}
		});

		return false;
	});

	var hdfID = $("#hdfRequestId", context);
	if (hdfID.length > 0 && hdfID.val() != "0") {
		$("#btnSearch", context).click();
	}
}
