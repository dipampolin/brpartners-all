﻿$(function () {
	var context = $('#conteudo');

	$(context).bind('keydown', function (event) {
		if (event.which == 13) {
			$("#lnkSubmitBBRentFilter").click();
		}
	});

	$("#lnkSubmitBBRentFilter").unbind("click");
	$("#lnkSubmitBBRentFilter").bind("click", function () {
		$("form").submit();
	});
});
