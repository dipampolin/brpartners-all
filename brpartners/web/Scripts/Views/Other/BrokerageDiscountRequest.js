﻿$(function () {

    /*Pegar nome do cliente*/
    $("#txtClient").blur(function () {
        commomDataHelper.GetClientName($(this).val(), "lblClientName", $(".conteudo")[0], "txtClient");

        if ($("#txtClient").val() != null && $("#txtClient").val() != "" && $("#txtClient").val() != undefined) {
            $.ajax({
                url: "../Other/GetActualDiscount",
                cache: false,
                type: "POST",
                data: { clientCode: $("#txtClient").val() },
                success: function (data) {
                    if (data != null && data != undefined) {
                        $(".val_perc").html(data + ' %');
                    }
                }
            });
        }

    }).setMask('number7');

    /*Carregar drop de tipo de mercado*/
    $("input[name='rdbMarket']").bind("click", function () {
        var id = $("input[name='rdbMarket']:checked").val();
        $.ajax({
            type: "POST",
            cache: false,
            url: "../Other/GetMarketTypes",
            data: { stock: id },
            dataType: "json",
            success: function (data) {
                var dropDown = $("#ddlMarket");
                if (data.length > 0) {
                    dropDown.html('');

                    for (var i = 0; i < data.length; i++) {
                        dropDown.append($("<option value='" + data[i].Key + "'>" + data[i].Value + "</option>").clone());
                    }

                    dropDown.styleInputs();
                }
            }
        });
    });

    /*visualizar opções de vigencia e negócio*/
    $("input[name='rdbVigency']").bind("click", function () {
        var value = $("input[name='rdbVigency']:checked").val();

        var valueNegocios = $("input[name='rdbNegocios']:checked").val();

        if (value == "P") {
            $("#divPermanent").show();
            $("#divSpecify").hide();
            $("#divAll").hide();
            $("#divNegocio").hide();
        }
        else {
            $("#divPermanent").hide();
            $("#divNegocio").show();

            if (valueNegocios == "N") {
                $("#divSpecify").show();
                $("#divAll").hide();
            }
            else {

                $("#divSpecify").hide();
                $("#divAll").show();
            }
        }


    });

    $("input[name='rdbNegocios']").bind("click", function () {
        var value = $("input[name='rdbNegocios']:checked").val();

        $("#divPermanent").hide();

        if (value == "N") {
            $("#divSpecify").show();
            $("#divAll").hide();
        }
        else {

            $("#divSpecify").hide();
            $("#divAll").show();
        }
    });

    /*Remover ativo especificado*/
    var fnRemove = function () {
        $(".ico-excluir").unbind("click");
        $(".ico-excluir").bind("click", function () {
            $(this).parent().parent().remove();
        });
    }

    var fnAddMasks = function () {
        $('.txt-qtde-especificar').setMask('integer');
        $('.txt-desconto-perc-especificar').setMask('number10').css({ 'text-align' : 'right' });
        $('#txtStockPercAll, #txtStockPercPermanent').setMask('number10').css({ 'text-align': 'right' });

        $('.txt-desconto-valor-especificar').setMask('decimal-empty');
        $('#txtStockValueAll, #txtStockValuePermanent').setMask('decimal-empty');

        $('.txt-ativo-especificar').keyup(function (e) {
            $(this).val($(this).val().toUpperCase());
        });
    }

    fnRemove();
    fnAddMasks();

    /*Incluir ativo especificado*/
    $(".ico-incluir").bind("click", function () {
        //var count = $("table.tabela-form > tbody > tr").length;
        var count = Math.floor(Math.random() * 100001);
        $("table.tabela-form > tbody").append(

            $("<tr>" +
                "<td class='ativo'><input type='text' class='txt-ativo-especificar' id='txtStockCode_" + count + "' name='txtStockCode_" + count + "' /></td>" +
                "<td class='qtde'><input type='text' class='txt-qtde-especificar' id='txtStockQtty_" + count + "' name='txtStockQtty_" + count + "' /></td>" +
                "<td class='desconto'><input type='text' class='txt-desconto-perc-especificar' id='txtStockPerc_" + count + "' name='txtStockPerc_" + count + "' /> % " +
                "ou R$ <input type='text' class='txt-desconto-valor-especificar' id='txtStockValue_" + count + "' name='txtStockValue_" + count + "' /><a class='ico-excluir' href='javascript:;'>excluir</a></td>" +
             "</tr>"
            ).clone()

        );

        $("td.conjugado").attr("rowspan", parseInt($("td.conjugado").attr("rowspan"), 10) + 1);

        fnRemove();

        fnAddMasks();

    });

    var validateBrokerageDiscountRequest = function () {
        var request = true;

        $("#requestFeedbackError").hide().find("ul").html('');

        var ul = $("div#requestFeedbackError > ul");
        if ($("#txtClient").val() == null || $("#txtClient").val() == undefined || $("#txtClient").val() == "") {
            ul.append($("<li>Cliente não informado.</li>").clone());
            request = false;
        }

        if ($("#ddlMarket").val() == "" || $("#ddlMarket").val() == "--") {
            ul.append($("<li>Mercado não informado.</li>").clone());
            request = false;
        }

        if ($("#ddlOperation").val() == "" || $("#ddlOperation").val() == "--") {
            ul.append($("<li>Operação não informada.</li>").clone());
            request = false;
        }

        if ($("input[name='rdbMarket']:checked").val() == "" || $("input[name='rdbMarket']:checked").val() == "--") {
            ul.append($("<li>Bolsa não informada.</li>").clone());
            request = false;
        }

        var valueVigencia = $("input[name='rdbVigency']:checked").val();

        var valueNegocios = $("input[name='rdbNegocios']:checked").val();

        if (valueVigencia == "P") {
            $("#divPermanent").show();

            if ($("#txtStockPercPermanent").val() == "" && $("#txtStockValuePermanent").val() == "") {
                ul.append($("<li>Preencha o desconto (informe percentual ou valor).</li>").clone());
                request = false;
            }
        }
        else {

            if (valueNegocios == "N") {

                var counter = 1;
                $(".txt-ativo-especificar").each(function () {

                    var order = $(this).attr("id").replace("txtStockCode_", "");

                    if ($("#txtStockCode_" + order).val() == null || $("#txtStockCode_" + order).val() == undefined || $("#txtStockCode_" + order).val() == "") {
                        ul.append($("<li>Preencha o ativo na " + counter.toString() + "ª linha de especificação.</li>").clone());
                        request = false;
                    }

                    if ($("#txtStockQtty_" + order).val() == null || $("#txtStockQtty_" + order).val() == undefined || $("#txtStockQtty_" + order).val() == "") {
                        ul.append($("<li>Preencha a quantidade na " + counter.toString() + "ª linha de especificação.</li>").clone());
                        request = false;
                    }

                    if ($("#txtStockValue_" + order).val() == "" && $("#txtStockPerc_" + order).val() == "") {
                        ul.append($("<li>Preencha o desconto (percentual ou valor) na " + counter.toString() + "ª linha de especificação.</li>").clone());
                        request = false;
                    }

                    counter++;
                });

            }
            else {

                if ($("#txtStockPercAll").val() == "" && $("#txtStockValueAll").val() == "") {
                    ul.append($("<li>Preencha o desconto (informe percentual ou valor).</li>").clone());
                    request = false;
                }
            }
        }

        if (request == false) {
            $("#requestFeedbackError").show().focus();
        }
        else {
            $("#requestFeedbackError").hide();
            $("#requestFeedbackError > ul").html('');
        }

        return request;
    };

    var fnSubmit = function () {
        $("#hdfClientName").val($("#lblClientName").text());


        $("#frmBrokerageDiscountRequest").ajaxForm({
            beforeSend: function () {
                $("#feedback").hide().find("span").html('');
                $("#feedbackError").hide().find("span").html('');
            },
            success: function (data) {
                $("#loadingArea").hide();

                if (data.Result) {
                    selector = "#feedback";

                    oTableBroker.fnDraw(true);

                    BrokerageDiscountMethods.getPendingDiscounts();
                }
                else {
                    selector = "#feedbackError";
                }
                $(selector).show().find("span").append(data.Message);

                $("body").unbind("keydown");
                Modal.Close();
            }
        });

        if (validateBrokerageDiscountRequest()) {
            $("#frmBrokerageDiscountRequest").submit();
        }
    };

    $("body").unbind("keydown");
    $("body").bind("keydown", function (event) {

        if (event.which == 13 && $("*:focus").attr("id") != "txtDescription") {
            fnSubmit();
        }
    });

    $("#btnSubmit").click(function () {
        fnSubmit();
    });
});
