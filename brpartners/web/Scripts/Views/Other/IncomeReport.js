﻿var objIncomeReport = {
    "bAutoWidth": false
    , "bPaginate": true
    , "bDestroy": true
    , "sPaginationType": "full_numbers"
    , "sDom": 'trip'
    , "oLanguage": {
        "sLengthMenu": "Exibindo _MENU_ registros",
        "sZeroRecords": "Nenhum registro encontrado.",
        "sInfo": "Encontramos _TOTAL_ registros",
        "sInfoEmpty": "Nenhum registro encontrado",
        "sInfoFiltered": "(Filtrando _MAX_ registros)",
        "sSearch": "Buscar:",
        "sProcessing": "Processando...",
        "oPaginate": {
            "sFirst": "Primeiro",
            "sPrevious": "Anterior",
            "sNext": "Próximo",
            "sLast": "Último"
        }
    }
    , "bProcessing": true
    , "bServerSide": true
    , "sAjaxSource": ""
    , "fnServerData": function (sSource, aoData, fnCallback) {
        $.ajax({
            "cache": false,
            "dataType": 'json',
            "type": "POST",
            "url": sSource,
            "data": aoData,
            "beforeSend": function () {
                $("#table-area").hide();
                $("#emptyArea").hide();
            },
            "success": fnCallback
        });
    }
    , fnDrawCallback: function () {

        var id = $(this).attr("id");

        $("#" + id + "Area").show();
        var totalRecords = this.fnSettings().fnRecordsDisplay();

        enableExportButtons(totalRecords);

        var totalPerPage = this.fnSettings()._iDisplayLength;

        var totalPages = Math.ceil(totalRecords / totalPerPage);

        var currentIndex = parseInt(this.fnSettings()._iDisplayStart);
        var currentPage = totalPages > 0 ? Math.ceil(1 + (currentIndex / totalPerPage)) : 0;

        $("div[id$='_processing']").hide();

        if (totalRecords > 0 && totalPages > 1) {
            $("#" + id + "_info").css("display", "none");
            $("#" + id + "_length").css("display", "");
            $("#" + id + "_paginate").css("display", "");
        }
        else {
            $("#" + id + "_info").css("display", "none");
            $("#" + id + "_length").css("display", "none");
            $("#" + id + "_paginate").css("display", "none");
        }
    }
	, "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
	    for (var i = 0; i < aData.length; i++) {
	        if (aData[i] != null && aData[i] != undefined) {
	            if (!(aData[i].replace(/\//g, "-").match(/^(\d{1,2})-(\d{1,2})-(\d{4})$/))) {
	                var res = parseFloat(aData[i]);
	                if (!isNaN(res)) {
	                    if (!isNaN(aoColumnsCustom[this.selector][i].bCorNegativo) && aoColumnsCustom[this.selector][i].bCorNegativo) {
	                        if (res < 0) {
	                            $("td:eq(" + i + ")", nRow).addClass('negativo');
	                        }
	                        else if (res > 0) {
	                            $("td:eq(" + i + ")", nRow).addClass('positivo');
	                        }
	                    }
	                }
	            }
	        }
	    }
	    return nRow;
	}
};

var objIncomeReportIRs = {
    "bAutoWidth": false
    , "bDestroy": true
    , "sDom": 'trip'
    , "bProcessing": true
    , "bServerSide": true
    , "sAjaxSource": ""
    , "fnServerData": function (sSource, aoData, fnCallback) {
        $.ajax({
            "cache": false,
            "dataType": 'json',
            "type": "POST",
            "url": sSource,
            "data": aoData,
            "beforeSend": function () {
                $("#table-area").hide();
                $("#emptyArea").hide();
            },
            "success": fnCallback
        });
    }
    , fnDrawCallback: function () {

        var id = $(this).attr("id");

        $("#" + id + "Area").show();
        var totalRecords = this.fnSettings().fnRecordsDisplay();

        enableExportButtons(totalRecords);
        this.iTotalRecords = totalRecords;
        this.iTotalDisplayRecords = totalRecords;

        $("#" + id + "_info").css("display", "none");
        $("#" + id + "_length").css("display", "none");
        $("#" + id + "_paginate").css("display", "none");

    }
	, "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
	    for (var i = 0; i < aData.length; i++) {
	        if (aData[i] != null && aData[i] != undefined) {
	            if (!(aData[i].replace(/\//g, "-").match(/^(\d{1,2})-(\d{1,2})-(\d{4})$/))) {
	                var res = parseFloat(aData[i]);
	                if (!isNaN(res)) {
	                    if (!isNaN(aoColumnsCustom[this.selector][i].bCorNegativo) && aoColumnsCustom[this.selector][i].bCorNegativo) {
	                        if (res < 0) {
	                            $("td:eq(" + i + ")", nRow).addClass('negativo');
	                        }
	                        else if (res > 0) {
	                            $("td:eq(" + i + ")", nRow).addClass('positivo');
	                        }
	                    }
	                }
	            }
	        }
	    }
	    return nRow;
	}
};


var aoColumnsCustom = {
	"#tblStockMarketList": [
		{ bCorNegativo: false },
		{ bCorNegativo: false, sClass: "direita" },
		{ bCorNegativo: true, sClass: "direita" },
		{ bCorNegativo: true, sClass: "direita" }
	],
	"#tblStockRentList": [
		{ bCorNegativo: false },
		{ bCorNegativo: false },
		{ bCorNegativo: false, sClass: "centralizado" },
		{ bCorNegativo: false, sClass: "centralizado" },
		{ bCorNegativo: false, sClass: "direita" },
		{ bCorNegativo: true, sClass: "direita" },
		{ bCorNegativo: true, sClass: "direita" },
		{ bCorNegativo: true, sClass: "direita" }
	],
    "#tblOptionsList": [
		{ bCorNegativo: false },
		{ bCorNegativo: false },
        { bCorNegativo: false },
		{ bCorNegativo: false, sClass: "centralizado" },
		{ bCorNegativo: false, sClass: "direita" },
		{ bCorNegativo: true, sClass: "direita" },
		{ bCorNegativo: true, sClass: "direita" },
		{ bCorNegativo: true, sClass: "direita" }
	],
	"#tblBMFOptionsFutureList": [
		{ bCorNegativo: false },
		{ bCorNegativo: false },
		{ bCorNegativo: false, sClass: "centralizado" },
		{ bCorNegativo: true, sClass: "direita" },
		{ bCorNegativo: false },
		{ bCorNegativo: true, sClass: "direita" }
	],
	"#tblBMFGoldList": [
		{ bCorNegativo: false, sClass: "direita" },
		{ bCorNegativo: false, sClass: "direita" },
		{ bCorNegativo: false, sClass: "direita" },
		{ bCorNegativo: true, sClass: "direita" }
	],
	"#tblBMFSwapList": [
		{ bCorNegativo: false },
		{ bCorNegativo: false },
		{ bCorNegativo: false, sClass: "direita" },
		{ bCorNegativo: false, sClass: "centralizado" },
		{ bCorNegativo: false, sClass: "centralizado" },
		{ bCorNegativo: true, sClass: "direita" }
	],
	"#tblIncomeProceedsList": [
		{ bCorNegativo: false },
		{ bCorNegativo: false },
		{ bCorNegativo: false, sClass: "direita" },
		{ bCorNegativo: true, sClass: "direita" },
		{ bCorNegativo: true, sClass: "direita" },
		{ bCorNegativo: true, sClass: "direita" },
		{ bCorNegativo: true, sClass: "direita" },
		{ bCorNegativo: false, sClass: "centralizado" }
	],
	"#tblIncomeTermsList": [
		{ bCorNegativo: false },
		{ bCorNegativo: false, sClass: "direita" },
		{ bCorNegativo: false, sClass: "direita" },
		{ bCorNegativo: false, sClass: "centralizado" },
		{ bCorNegativo: true, sClass: "direita" }
	],
	"#tblIRRFDayTradeList": [
		{ bCorNegativo: false, bSortable: false },
		{ bCorNegativo: true, sClass: "direita", bSortable: false },
		{ bCorNegativo: true, sClass: "direita", bSortable: false }
	],
	"#tblIRRFOperationsList": [
		{ bCorNegativo: false, bSortable: false },
		{ bCorNegativo: true, sClass: "direita", bSortable: false },
		{ bCorNegativo: true, sClass: "direita", bSortable: false }
	]
};


$(function () {
    $("#tabIncomeReport").tabs();
});

function enableExportButtons(totalRecords) {
	if (totalRecords > 0) {
		$("#lnkExcel").unbind("click").bind("click", function () {
			commonHelper.OpenModalPost("/Other/IncomeReportToFileConfirm", null, ".content", "", { isPdf: false });
		});
		$("#lnkPdf").unbind("click").bind("click", function () {
			commonHelper.OpenModalPost("/Other/IncomeReportToFileConfirm", null, ".content", "", { isPdf: true });
		});
	}
	else {
		$("#lnkExcel").unbind("click");
		$("#lnkPdf").unbind("click");
	}
}