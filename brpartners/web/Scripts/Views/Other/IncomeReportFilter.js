﻿/// <reference path="../../References.js" />

var OperationsFilter = {
    ValidateFilterFields: function () {
        var messageError = "";

        if ($("#txtClientCode").val() == "" || $("#txtClientCode").val() == null || $("#txtClientCode").val() == "0")
            messageError += "<li>Cliente não informado.</li>";

        if (isNaN($("#txtClientCode").val()))
            messageError += "<li>Sómente números são permitidos.</li>";

        if (messageError.length > 0)
            messageError = "<ul>" + messageError + "</ul>";

        return { Result: (messageError == ""), Message: messageError }
    }
};

$(function () {
    var context = $('#IncomeReportContent');

    $("#IncomeReportFilter").click(function () {
        $(context).slideToggle('fast');
        $('select').styleInputs();
        $(this).toggleClass('aberto');
    });

    $(context).bind('keydown', function (event) {
        if (event.which == 13) {
            $("#lnkSubmitIncomeReportFilter").click();
        }
    });

    $("#lnkSubmitIncomeReportFilter").unbind("click");
    $("#lnkSubmitIncomeReportFilter").bind("click", function () {

        $("#feedback").hide().find("span").html('');
        $("#feedbackError").hide().find("span").html('');

        $("#pesquisa").hide();

        var valid = OperationsFilter.ValidateFilterFields();

        if (valid.Result) {
            $(context).slideToggle('fast');
            $(this).toggleClass('fechado');
            $("#filterFeedbackError").html("").hide();
            $("#IncomeReportContent").hide();
            $("#loadingArea").show();
            $("#incomeReportArea").hide();

            if (commonHelper.CheckSession()) {

                $.ajax({
                    type: "POST",
                    cache: false,
                    url: "../Other/IncomeReportFilter",
                    data: { "txtClientCode": $("#txtClientCode").val(), "ddlYears": $("#ddlYears").val() },
                    dataType: 'html',
                    beforeSend: function () { $("#incomeReportArea").html(''); },
                    success: function (data) {
                        if (data != "") {
                            $("#incomeReportArea").html(data).show(); 

                            $("#loadingArea").hide();
                            $("#tabIncomeReport").tabs();

                            objIncomeReport.sAjaxSource = "../Other/IncomeReport_StockMarketList";
                            objIncomeReport.aoColumns = aoColumnsCustom["#tblStockMarketList"];
                            $("#tblStockMarketList").dataTable(objIncomeReport);

                            objIncomeReport.sAjaxSource = "../Other/IncomeReport_StockRentList";
                            objIncomeReport.aoColumns = aoColumnsCustom["#tblStockRentList"];
                            $("#tblStockRentList").dataTable(objIncomeReport);

                            objIncomeReport.sAjaxSource = "../Other/IncomeReport_OptionsList";
                            objIncomeReport.aoColumns = aoColumnsCustom["#tblOptionsList"];
                            $("#tblOptionsList").dataTable(objIncomeReport);

                            objIncomeReport.sAjaxSource = "../Other/IncomeReport_BMFOptionsFutureList";
                            objIncomeReport.aoColumns = aoColumnsCustom["#tblBMFOptionsFutureList"];
                            $("#tblBMFOptionsFutureList").dataTable(objIncomeReport);

                            objIncomeReport.sAjaxSource = "../Other/IncomeReport_BMFGoldList";
                            objIncomeReport.aoColumns = aoColumnsCustom["#tblBMFGoldList"];
                            $("#tblBMFGoldList").dataTable(objIncomeReport);

                            objIncomeReport.sAjaxSource = "../Other/IncomeReport_BMFSwapList";
                            objIncomeReport.aoColumns = aoColumnsCustom["#tblBMFSwapList"];
                            $("#tblBMFSwapList").dataTable(objIncomeReport);

                            objIncomeReport.sAjaxSource = "../Other/IncomeReport_IncomeProceedsList";
                            objIncomeReport.aoColumns = aoColumnsCustom["#tblIncomeProceedsList"];
                            $("#tblIncomeProceedsList").dataTable(objIncomeReport);

                            objIncomeReport.sAjaxSource = "../Other/IncomeReport_IncomeTermsList";
                            objIncomeReport.aoColumns = aoColumnsCustom["#tblIncomeTermsList"];
                            $("#tblIncomeTermsList").dataTable(objIncomeReport);

                            objIncomeReportIRs.sAjaxSource = "../Other/IncomeReport_IRRFDayTradeList";
                            objIncomeReportIRs.aoColumns = aoColumnsCustom["#tblIRRFDayTradeList"];
                            $("#tblIRRFDayTradeList").dataTable(objIncomeReportIRs);

                            objIncomeReportIRs.sAjaxSource = "../Other/IncomeReport_IRRFOperationsList";
                            objIncomeReportIRs.aoColumns = aoColumnsCustom["#tblIRRFOperationsList"];
                            $("#tblIRRFOperationsList").dataTable(objIncomeReportIRs);
                        }
                        else {
                            $("#feedbackError").html("Ocorreu um erro ao realizar consulta").show();
                            $("#incomeReportArea").hide();
                            $("#loadingArea").hide();
                        }
                    }
                });
            }
        }
        else {
            $("#filterFeedbackError").html(valid.Message).show();
        }
    });
});