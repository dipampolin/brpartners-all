﻿$(function () {
    $("#btnRealize").unbind("click");
    $("#btnRealize").bind("click", function () {

        $.ajax({
            type: "POST",
            cache: false,
            async: false,
            url: '../Other/ApproveCustodyTransfer',
            data: { ids: $("#requestIds").val(), ddlStatus: $("#ddlStatus").val() },
            dataType: 'json',
            beforeSend: function () {
                $("#loadingArea").show();
                $("#feedbackError").hide().find("span").html("");
                $("#feedback").hide().find("span").html("");
            },
            success: function (data) {
                $("#loadingArea").hide();
                if (data.Result) {
                    selector = "#feedback";

                    oTableCustodyTransfer.fnDraw(true);

                    CustodyTransferMethods.getPendingCustodyTransfers();
                }
                else {
                    selector = "#feedbackError";
                }

                $(selector).show().find("span").append(data.Message);

                Modal.Close();
            }
        });

    });
});