﻿$(document).ready(function () {

    $('.linha-falsa-tabela').each(function () {
        var _this = $(this);
        var tableContent = _this.find('.caixa-tabela-expandida');
        $(this).find('.ico-detalhes').toggle(function (e) {
            $(this).removeClass('fechado').addClass('aberto');
            var style = 'height: 0; display: block;';
            tableContent.attr('style', style).animate({ 'height': '250' });
            LoadPartialPage($(this).attr("rel"), tableContent);
        }, function () {
            tableContent.animate({ 'height': '0' }, function () { $(this).css({ 'display': 'none' }); });
            $(this).removeClass('aberto').addClass('fechado');
            tableContent.first("div").html("");
        });
    });

    $('#lnkDetails').toggle(function () {
        $(this).addClass('aberto').text('Fechar tudo');
        $('.tabela-expansivel').find('a.ico-detalhes.fechado').trigger('click');
    }, function () {
        $(this).removeClass('aberto').text('Abrir tudo');
        $('.tabela-expansivel').find('a.ico-detalhes.aberto').trigger('click');
    });

    $("#txtStock")
            .focus(function () {
                if ($(this).val() == "Investimentos com esse ativo") {
                    $(this).val("");
                }
            })
            .blur(function () {
                if ($(this).val() == "") {
                    $(this).val("Investimentos com esse ativo");
                }
            })
            .keyup(function () {
                var stock = $(this).val();
                if (stock != undefined && stock.length > 1) {
                    stock = stock.toUpperCase();
                    $(this).val(stock);
                    /*Se houver algo fechado, abrir*/
                    //                    if ($("#lnkDetails").text() != "Fechar Tudo") {
                    //                        $(this).addClass('aberto').text('Fechar tudo');
                    //                        $('.tabela-expansivel').find('a.ico-detalhes.fechado').trigger('click');
                    //                    }

                    var trList = $("table.classificar > tbody > tr");
                    trList.css({ "background-color": "#E3E5E5" });
                    trList.each(function () {
                        var tdStock = $(this).find("td.ativo");

                        if (tdStock.length > 0) {
                            if (tdStock.html().toUpperCase().indexOf(stock) > -1) {
                                $(this).css({ "background-color": "#ffe199" });
                            }
                        }
                    });
                }
                else if (stock.length == 0) {
                    /*Tirar os highlights : TODO: Se houver zebrado, colocar função q monte isso*/
                    $("table.classificar > tbody > tr").css({ "background-color": "#E3E5E5" });
                }
            });

    /*Funcionalidade para abrir todas as abas. Será necessário atribuir a cada link um atributo rel com o nome do produto.*/
    //    $("a#lnkDetails").click(function () {
    //        $(".ico-detalhes[id!='openAll']").each(function () {
    //            var rel = $(this).attr("rel");
    //            if (rel != null && rel != undefined && rel != "") {
    //                var caixaDiv = $("#divCP-" + rel);

    //                if (caixaDiv.css("display") == "none") {
    //                    caixaDiv.css('height', '0').show().animate({ 'height': '250' });

    //                    LoadPartialPage(rel, caixaDiv);
    //                }
    //                else {
    //                    caixaDiv.animate({ 'height': '0' }, function () { $(this).hide(); });
    //                }

    //            }
    //        });
    //    });

    $("#lnkRegView").bind("click", function () {

        var id = $("#hdfClientCode").val();

        if (id != undefined && id != null && id != "") {
            commonHelper.OpenModal("/Other/ConsolidatedPositionClient/" + id, 450, ".content", "");
        }
	});

    $("#lnkGerarPDF").attr("href", "/Other/ConsolidatedPositionToFile/?clientCode=" + $("#hdfClientCode").val());
});

function LoadPartialPage(type, content) {
	$("#loadingAreaTab").clone().show().appendTo(content);
	$.ajax({
	    type: "POST",
	    url: "/Other/PositionController/",
	    dataType: "html",
	    data: { positionType: type, clientCode: $("#hdfClientCode").val() },
	    async: false,
	    success: function (data) {
	        if (data != "") {
	            content.first("div").html(data);
	            $(".tabela-expandida", content).show();
	            var trList = $("table.classificar > tbody > tr", content);
	            var stock = $("#txtStock").val();
	            if (stock != "Investimentos com esse ativo" && stock.length > 1) {
	                trList.each(function () {
	                    var html = $(this).html().toUpperCase();
	                    if (html.indexOf(stock) > -1) {
	                        $(this).css({ "background-color": "#ffe199" });
	                    }
	                });
	            }
	        }
	    }
	});
}

