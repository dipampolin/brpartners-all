﻿var OperationMapFilter = {
    ValidateFilterFields: function () {
        var messageError = "";

        if (!validateHelper.ValidateList($("#txtClientFilter").val()))
            messageError += "<li>Faixa de clientes inválida.</li>";

        /*if ($("#txtOperator").val() != "") { 
            if (isNaN($("#txtOperator").val())){
                messageError += "<li>Faixa de clientes inválida.</li>";
            }
        } */       

        if (messageError.length > 0)
            messageError = "<ul>" + messageError + "</ul>";

        return { Result: (messageError == ""), Message: messageError }
    }
};

$(function () {
    var context = $('#OperationsMapContent');

    $("#OperationsMapFilter").click(function () {
        $(context).slideToggle('fast');
        $('select').styleInputs();
        $(this).toggleClass('aberto');
    });

    $(context).bind('keydown', function (event) {
        if (event.which == 13) {
            $("#lnkSubmitOperationsMapFilter").click();
        }
    });

    $("#lnkSubmitOperationsMapFilter").unbind("click");
    $("#lnkSubmitOperationsMapFilter").bind("click", function () {

        $("#pesquisa").hide();
        var feedback = OperationMapFilter.ValidateFilterFields();

        if (feedback.Result) {
            $("#filterFeedbackError").html("").hide();
            $("#OperationsMapContent").hide();
            $("#loadingArea").show();
            $("#table-client-area").hide();
            $("#table-operator-area").hide();
            $("#feedback").hide().find("span").html('');
            $("#feedbackError").hide().find("span").html('');

            /*if (oTableOperationsMap != null)
            oTableOperationsMap.fnDestroy();

            oTableOperationsMap = $("#tblOperationsMap").dataTable(objFilter);*/

            if (oTableOperator != null) {
                oTableOperator.fnDestroy();
                oTableOperator = null;
            }

            oTableOperator = $("#tblOperator").dataTable(objOperatorFilter);

        }
        else {
            $("#filterFeedbackError").html(feedback.Message).show();
        }
    });
});
