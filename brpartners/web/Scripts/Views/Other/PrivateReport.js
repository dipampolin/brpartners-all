﻿var oTablePrivate = null;

var objPrivateFilter = {
    "bAutoWidth": false
            , "bPaginate": true
            , "bDestroy": true
            , "sPaginationType": "full_numbers"
            , "sDom": 'trip'
            , "oLanguage": {
                "sLengthMenu": "Exibindo _MENU_ registros",
                "sZeroRecords": "Nenhum registro encontrado.",
                "sInfo": "Encontramos _TOTAL_ registros",
                "sInfoEmpty": "Nenhum registro encontrado",
                "sInfoFiltered": "(Filtrando _MAX_ registros)",
                "sSearch": "Buscar:",
                "sProcessing": "Processando...",
                "oPaginate": {
                    "sFirst": "Primeiro",
                    "sPrevious": "Anterior",
                    "sNext": "Próximo",
                    "sLast": "Último"
                }
            }
            , "bProcessing": true
            , "bServerSide": true
            , "sAjaxSource": "../Other/PrivateReportFilter"
            , "fnServerData": function (sSource, aoData, fnCallback) {

                var context = $('#PrivateReportContent');

                if (aoData != null && aoData != undefined) {
                    aoData.push({ "name": "ddlMonths", "value": $('#ddlMonths', context).val() });
                    aoData.push({ "name": "ddlYears", "value": $('#ddlYears', context).val() });
                    aoData.push({ "name": "txtCPFCGC", "value": $('#txtCPFCGC', context).val() });
                }
                $.ajax({
                    "cache": false,
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "beforeSend": function () {
                        $("#table-area").hide();
                        $("#loadingArea").show();
                        $("#emptyArea").hide();
                    },
                    "success": function (data) {
                        fnCallback(data);

                        if (data.iTotalRecords == 0) {
                            $("#table-area").hide();
                            $('#emptyArea').html('Sua pesquisa não encontrou resultados válidos. Tente novamente.').show();
                        }
                        else {
                            $('#emptyArea').html('').hide();

                            $("#lblReferencia").html("Referência: " + data.RefDate);

                            $("tbody").find("td.direita").each(function () {
                                var thisValue = $(this).html();
                                thisValue = thisValue.replace(/\./g, '').replace(',', '.').replace("R", "").replace("$", "").replace(" ", "");
                                if (parseFloat(thisValue) < 0) {
                                    $(this).addClass("negativo");
                                }
                                else if (parseFloat(thisValue) > 0) {
                                    $(this).addClass("positivo");
                                }
                            });
                        }
                    }
                });
            }
            , "fnInitComplete": function () {
                oTablePrivate.fnAdjustColumnSizing();
            }
            , fnDrawCallback: function () {
                $("#checkAll").attr("checked", false);

                $("#loadingArea").hide();
                $("#table-area").show();

                var totalRecords = this.fnSettings().fnRecordsDisplay();

                if (totalRecords <= 0) {
                    $("#lnkPrivateExcel").hide();
                    $("#lnkPrivatePdf").hide();
                    $("#dvExportar").hide();
                }
                else {
                    $("#lnkPrivateExcel").show();
                    $("#lnkPrivatePdf").show();
                    $("#dvExportar").show();

                    PrivateFunctions.enableExportButtons();

                }

                var totalPerPage = this.fnSettings()._iDisplayLength;

                var totalPages = Math.ceil(totalRecords / totalPerPage);

                var currentIndex = parseInt(this.fnSettings()._iDisplayStart);
                var currentPage = totalPages > 0 ? Math.ceil(1 + (currentIndex / totalPerPage)) : 0;

                $("#tblPrivates_info").html("Exibindo " + currentPage + " de " + totalPages);

                if (totalRecords > 0 && totalPages > 1) {
                    $("#tblPrivates_info").css("display", "");
                    $("#tblPrivates_length").css("display", "");
                    $("#tblPrivates_paginate").css("display", "");
                }
                else {
                    $("#tblPrivates_info").css("display", "none");
                    $("#tblPrivates_length").css("display", "none");
                    $("#tblPrivates_paginate").css("display", "none");
                }

                $("#tblPrivates_processing").css("display", "none");
            }
            , aoColumns: [
                null,
                null,
                null,
                { sClass: "direita" },
                { sClass: "direita" },
                { sClass: "direita" },
                { sClass: "direita" },
                { sClass: "direita" },
                { sClass: "direita" }
            ]
};

var PrivateFunctions = {
        enableExportButtons: function () {
            var data = $("#tblPrivates tbody tr");
            var emptyData = (data.length == 1 && data[0].lastChild.className == "dataTables_empty") ? true : false;
            var excelLink = $("#lnkPrivateExcel");
            var pdfLink = $("#lnkPrivatePdf");

            var fnExportFilter = function (isPDF) {
                $.ajax({
                    type: "POST",
                    cache: false,
                    async: false,
                    url: "../Other/PrivateReportFilterToFile",
                    data: {
                        ddlMonths: $("#ddlMonths").val(),
                        ddlYears: $("#ddlYears").val(),
                        txtCPFCGC: $("#txtCPFCGC").val()
                    },
                    beforeSend: function () {
                        $("#feedbackError").hide().html('');
                    },
                    success: function (data) {
                        if (data)
                            window.location = "/Other/PrivateReportToFile/?isPdf=" + isPDF;
                        else {
                            $("#feedbackError").show().html("Ocorreu um erro na hora da exportação.");
                        }
                    }
                });
            };

            excelLink.unbind("click");
            excelLink.bind("click", function () {
                fnExportFilter("false");
            });

            pdfLink.unbind("click");
            pdfLink.bind("click", function () {
                fnExportFilter("true");
            });
        }
};


$(function () {
    var context = $('#PrivateReportContent');


    $("#PrivateReportFilter").click(function () {
        $(context).slideToggle('fast');
        $('select').styleInputs();
        $(this).toggleClass('aberto');
    });


    $(context).bind('keydown', function (event) {
        if (event.which == 13) {
            $("#lnkSubmitPrivateReportFilter").click();
        }
    });

    $("#lnkSubmitPrivateReportFilter").unbind("click");
    $("#lnkSubmitPrivateReportFilter").bind("click", function () {

        var feedback = ValidateFilterFields();
        
        if (feedback == "") {
            $("#filterFeedbackError").html("").hide();
            $("#PrivateReportContent").hide();
            $("#loadingArea").show();
            $("#table-area").hide();
            $("#feedback").hide().find("span").html('');
            $("#feedbackError").hide().find("span").html('');
            $("#pesquisa").hide();

            if (oTablePrivate != null)
                oTablePrivate.fnDestroy();

            oTablePrivate = $("#tblPrivates").dataTable(objPrivateFilter);
        }
        else {
            $("#filterFeedbackError").show().html(feedback);
        }
    });
});

function ValidateFilterFields() {
    var month = $("#ddlMonths").val();
    var year = $("#ddlYears").val();
    var cpf = $("#txtCPFCGC").val();
    var retorno = "";

    $.ajax({
        type: "POST",
        cache: false,
        async: false,
        url: "../Other/ValidatePrivateReportFilter",
        data: {
            year: year,
            month: month
        },
        success: function (data) {
            if (data != "")
                retorno = "<li>" + data + "</li>";
        }
    });

    if (isNaN(cpf))
        retorno += "<li>Somente números são permitidos.</li>";

    if (retorno == "")
        return retorno;
    else
        return "<ul>" + retorno + "</ul>";
}