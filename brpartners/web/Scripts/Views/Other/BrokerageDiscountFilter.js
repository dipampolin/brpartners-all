﻿var BrokerageDiscountFilter = {
    ValidateFilterFields: function () {
        var messageError = "";
        if (!validateHelper.ValidateList($("#txtAssessor").val()))
            messageError += "<li>Faixa de assessores inválida.</li>";

        if (!validateHelper.ValidateList($("#txtClientFilter").val()))
            messageError += "<li>Faixa de clientes inválida.</li>";

        if (messageError.length > 0)
            messageError = "<ul>" + messageError + "</ul>";

        return  { Result : (messageError == "") , Message : messageError }
    }
};

$(function () {
    var context = $('#BrokerageDiscountContent');

    $("#txtMinorDiscountPercentual, #txtMajorDiscountPercentual").setMask('decimal-empty');
    $("#txtMinorDiscountValue, #txtMajorDiscountValue").setMask('decimal-empty');

    $("#BrokerageDiscountFilter").click(function () {
        $(context).slideToggle('fast');
        $('select').styleInputs();
        $(this).toggleClass('aberto');
    });

    $("input[name='rdbDiscountType']").unbind("click");
    $("input[name='rdbDiscountType']").bind("click", function () {
        if ($("input[name='rdbDiscountType']:checked").val() == "P") {
            $("#divOptPercentual").show();
            $("#divOptDinheiro").hide();
        } else {
            $("#divOptPercentual").hide();
            $("#divOptDinheiro").show();
        }
    });

    $(context).bind('keydown', function (event) {
        if (event.which == 13) {
            $("#lnkSubmitBrokerageDiscountFilter").click();
        }
    });

    $("#lnkSubmitBrokerageDiscountFilter").unbind("click");
    $("#lnkSubmitBrokerageDiscountFilter").bind("click", function () {

        var feedback = BrokerageDiscountFilter.ValidateFilterFields();

        if (feedback.Result) {
            $("#filterFeedbackError").html("").hide();
            $("#BrokerageDiscountContent").hide();
            $("#loadingArea").show();
            $("#table-area").hide();
            $("#feedback").hide().find("span").html('');
            $("#feedbackError").hide().find("span").html('');

            if (oTableBroker != null)
                oTableBroker.fnDestroy();

            oTableBroker = $("#tblBrokerageDiscount").dataTable(objBrokerFilter);

            BrokerageDiscountMethods.enableExportButtons();
        }
        else {
            $("#filterFeedbackError").html(feedback.Message).show();
        }
    });
});
