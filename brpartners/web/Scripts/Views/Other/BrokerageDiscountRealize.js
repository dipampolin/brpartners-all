﻿$(function () {
    $("#btnRealize").unbind("click");
    $("#btnRealize").bind("click", function () {

        $.ajax({
            type: "POST",
            cache: false,
            async: false,
            url: '../Other/ApproveBrokerageDiscount',
            data: { ids: $("#requestIds").val(), ddlStatus: $("#ddlStatus").val() },
            dataType: 'json',
            beforeSend: function () {
                $("#loadingArea").show();
                $("#feedbackError").hide().find("span").html("");
                $("#feedback").hide().find("span").html("");
            },
            success: function (data) {
                $("#loadingArea").hide();
                if (data.Result) {
                    selector = "#feedback";

                    oTableBroker.fnDraw(true);

                }
                else {
                    selector = "#feedbackError";
                }

                $(selector).show().find("span").append(data.Message);

                Modal.Close();
            }
        });

    });
});