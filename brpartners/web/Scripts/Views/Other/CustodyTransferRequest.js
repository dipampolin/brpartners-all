﻿/// <reference path="../../References.js" />

$(function () {

    $.format.locale({
        number: {
            groupingSeparator: '.',
            decimalSeparator: ','
        }
    });

    /*Pegar nome do cliente*/
    $("#txtClient").blur(function () {

        if ($(this).val() != "") {
            commomDataHelper.GetClientName($(this).val(), "lblClientName", $(".conteudo")[0], "txtClient");

            $.ajax({
                url: "../Other/GetClientPosition",
                cache: false,
                type: "POST",
                data: { clientCode: $("#txtClient").val() },
                beforeSend: function () {
                    $("#loadingAreaRequest").show();
                    $("#emptyArea2").hide();
                    $("#table-area-request").hide();
                    fnDisableSubmit();
                }
                , error: function () {
                    $("#emptyArea2").show();
                    $("#loadingAreaRequest").hide();
                }
                , success: function (position) {
                    if (position != null && position != undefined) {

                        $("table.tabela-form > tbody").html('');
                        $("#loadingAreaRequest").hide();

                        if (position.length == 0) {
                            $("#emptyArea2").show();

                            fnDisableSubmit();
                        }
                        else {
                            for (var i = 0; i < position.length; i++) {

                                $("table.tabela-form > tbody").append(
                                    $(
                                        "<tr>" +
                                             "<td class='primeiro'><input type='hidden' id='hdfStockCode_" + i.toString() + "' name='hdfStockCode_" + i.toString() + "' value='" + position[i].StockCode + "' class='ativo-especificar' />" + position[i].StockCode + "</td>" +
                                             "<td><input type='text' id='txtOrigPortfolio_" + i.toString() + "' name='txtOrigPortfolio_" + i.toString() + "' value='" + position[i].PortfolioCode + "' class='carteira-especificar' /></td>" +
                                             "<td>" + $.format.number(position[i].StockQtty, "#,###") + "</td>" +
                                             "<td class='ultimo'><input type='text' id='txtStockQtty_" + i.toString() + "' name='txtStockQtty_" + i.toString() + "' class='qtde-especificar' />" +
                                             "<input type='hidden' class='hidden-qtde' value='0' id='hdfStockQtty_" + i.toString() + "' />" +
                                             "<input type='checkbox' id='chkTotal_" + i.toString() + "' name='chkTotal_" + i.toString() + "' class='chkTotal' />total</td>" +
                                        "</tr>"
                                    ).clone()
                                );

                            }

                            fnAddMasks();


                            $("#emptyArea2").hide();
                            $("#table-area-request").show();
                            $("#btnSubmit").bind("click", fnSubmit);

                            $("body").unbind("keydown");
                            $("body").bind("keydown", function (event) {
                                if (event.which == 13 && $("*:focus").attr("id") != "txtObservation") {
                                    fnSubmit();
                                }
                            });
                        }

                    }
                }
            });
        }

    }).setMask('999999999');


    $("#txtDestinyBroker").autocomplete({
        source: function (request, response) {
            $.ajax({
                cache: false,
                url: "/NonResidentTraders/LoadBrokers",
                type: "POST",
                dataType: "json",
                data: { filter: $("#txtDestinyBroker").val() },
                success: function (data) {
                    response($.map(data, function (item) {
                        return {
                            label: item.BrokerCode + " - " + item.BrokerName
                            , value: item.BrokerName
                            , code: item.BrokerCode
                        }
                    }));
                }
            });
        }
        , minLength: 3
        , select: function (event, ui) {
            $("#hdfDestinyBroker").val(ui.item.code);
        }
    });

    $("#txtDestinyPortfolio").autocomplete({
        source: function (request, response) {
            $.ajax({
                cache: false,
                url: "/NonResidentTraders/LoadPortfolios",
                type: "POST",
                dataType: "json",
                data: { filter: $("#txtDestinyPortfolio").val() },
                success: function (data) {
                    response($.map(data, function (item) {
                        return {
                            label: item.Value + " - " + item.Description
                            , value: item.Description
                            , code: item.Value
                        }
                    }));
                }
            });
        }
        , minLength: 3
        , select: function (event, ui) {
            $("#txtDestinyPortfolio").val(ui.item.code);
            $("#hdfDestinyPortfolio").val(ui.item.code);
            return false;
        }
    });

//    $("#txtDestinyPortfolio").blur(function () {
//        if ($("#txtDestinyPortfolio").val() != $("#hdfDestinyPortfolio").val())
//            $("#txtDestinyPortfolio").val("");
//    });

    $("#txtDestinyBroker").unbind("blur");
    $("#txtDestinyBroker").bind("blur", function () {

        if ($("#hdfDestinyBroker").val() == "" && $(this).val() != "") {

            $.ajax({
                cache: false,
                url: "/NonResidentTraders/LoadBrokers",
                type: "POST",
                dataType: "json",
                data: { filter: $("#txtDestinyBroker").val() },
                success: function (data) {
                    var items = eval(data);
                    if (data.length == 1) {
                        $("#hdfDestinyBroker").val(items[0].BrokerCode);
                        $("ul.ui-autocomplete").hide();
                    }
                }
            });

        }

    });


    var fnAddMasks = function () {
        $('#txtDestinyClientCode').setMask('999999999');
        $('.carteira-especificar, #txtDestinyPortfolio').setMask('99999');
        $('.qtde-especificar').setMask('integer');

        $('.ativo-especificar, #txtDestinyBroker').keyup(function (e) {
            $(this).val($(this).val().toUpperCase());
        });

        $("#txtObservation").maxlength({ limit: 200 });

        $(".chkTotal").each(function () {
            $(this).bind('click', function () {
                var tr = $(this).parent().parent();
                if ($(this).is(":checked")) {

                    var qtde = tr.find("td:eq(2)").html().replace(/\./g, "");
                    var qtdeAnterior = tr.find(".hidden-qtde").val().replace(/\./g, "");

                    if (!isNaN(qtde)) {
                        //tr.find("input.qtde-especificar").val(qtde);

                        var result = parseInt(qtde, 10) + parseInt(qtdeAnterior, 10);

                        tr.find("input.qtde-especificar").val($.format.number(result, "#,###"));
                    }
                }
                else {
                    tr.find("input.qtde-especificar").val("");
                }
            });
        });

        $(".qtde-especificar").each(function () {
            $(this).bind('blur', function () {
                var tr = $(this).parent().parent();
                var check = $(this).next();
                var value = $(this).val();
                var qtde = tr.find("td:eq(2)").html();
                var qtdeAnterior = tr.find(".hidden-qtde").val();

                value = value.replace(/\./g, "");
                qtde = qtde.replace(/\./g, "");
                qtdeAnterior = qtdeAnterior.replace(/\./g, "");

                if (value == qtde + qtdeAnterior) {
                    $(check).attr("checked", "checked");
                }
                else if (parseInt(value, 10) > (parseInt(qtde, 10) + parseInt(qtdeAnterior, 10))) {
                    $(this).val("");
                    $(check).attr("checked", "");
                }
                else {
                    $(check).attr("checked", "");
                }
            });
        });
    }

    fnAddMasks();

    var validateCustodyTransferRequest = function () {
        var request = true;

        $("#requestFeedbackError").hide().find("ul").html('');

        var ul = $("div#requestFeedbackError > ul");
        if ($("#txtClient").val() == null || $("#txtClient").val() == undefined || $("#txtClient").val() == "") {
            ul.append($("<li>Cliente não informado.</li>").clone());
            request = false;
        }
        else if ($("table.tabela-form > tbody > tr").length <= 0) {
            ul.append($("<li>Cliente não possui posição para ser transferida.</li>").clone());
            request = false;
        }

        if ($("#hdfDestinyBroker").val() == null || $("#hdfDestinyBroker").val() == undefined || $("#hdfDestinyBroker").val() == "") {
            ul.append($("<li>Corretora Destino não encontrada.</li>").clone());
            request = false;
        }

        if ($("#txtDestinyPortfolio").val() == null || $("#txtDestinyPortfolio").val() == undefined || $("#txtDestinyPortfolio").val() == "") {
            ul.append($("<li>Carteira Destino não informada.</li>").clone());
            request = false;
        }

        if ($("#txtDestinyClientCode").val() == null || $("#txtDestinyClientCode").val() == undefined || $("#txtDestinyClientCode").val() == "") {
            ul.append($("<li>Codigo do cliente na corretora de destino não informado.</li>").clone());
            request = false;
        }


        var boolQtty = false;

        $(".ativo-especificar").each(function () {

            var order = $(this).attr("id").replace("hdfStockCode_", "");
            var stockCode = $(this).val();


            if ($("#txtStockQtty_" + order).val() != null && $("#txtStockQtty_" + order).val() != undefined && $("#txtStockQtty_" + order).val() != "") {

                if (boolQtty == false) {
                    boolQtty = true;
                }

                var totalQtty = $(this).parent().parent().find("td:eq(2)").html();
                totalQtty = totalQtty.replace(/\./g, "");

                totalQtty = parseFloat(totalQtty) + parseFloat($(this).parent().parent().find(".hidden-qtde").val());

                var insertedQtty = $("#txtStockQtty_" + order).val();
                insertedQtty = insertedQtty.replace(/\./g, "");

                if (!isNaN(totalQtty)) {
                    if (parseFloat(totalQtty) < parseFloat(insertedQtty)) {
                        ul.append($("<li>Informe uma quantidade menor ou igual ao total em sua carteira para o ativo " + stockCode + ".</li>").clone());
                        request = false;
                    }
                }
            }

            if (($("#txtOrigPortfolio_" + order).val() == null || $("#txtOrigPortfolio_" + order).val() == undefined || $("#txtOrigPortfolio_" + order).val() == "")
                && ($("#txtStockQtty_" + order).val() != null && $("#txtStockQtty_" + order).val() != undefined && $("#txtStockQtty_" + order).val() != "")) {
                ul.append($("<li>Preencha a carteira de origem para o ativo " + stockCode + ".</li>").clone());
                request = false;
            }


            if (($("#txtOrigPortfolio_" + order).val() == $("#txtDestinyPortfolio").val() && $("#txtStockQtty_" + order).val() != "")
                && ($("#hdfDestinyBroker").val() == 21)) {
                ul.append($("<li>A carteira de origem não pode ser igual a carteira de destino para o ativo " + stockCode + ".</li>").clone());
                request = false;
            }

        });

        if (boolQtty == false) {
            ul.append($("<li>Nenhuma quantidade foi preenchida. A solicitação não pôde ser feita.</li>").clone());
            request = false;
        }

        if (request == false) {
            $("#requestFeedbackError").show().focus();
        }
        else {
            $("#requestFeedbackError").hide();
            $("#requestFeedbackError > ul").html('');
        }

        return request;
    };

    var fnSubmit = function () {


        $("#frmCustodyTransferRequest").ajaxForm({
            beforeSend: function () {
                $("#hdfClientName").val($("#lblClientName").text());
                $("#feedback").hide().find("span").html('');
                $("#feedbackError").hide().find("span").html('');
            },
            success: function (data) {
                $("#loadingArea").hide();

                if (data.Result) {
                    selector = "#feedback";

                    oTableCustodyTransfer.fnDraw(true);

                    CustodyTransferMethods.getPendingCustodyTransfers();
                }
                else {
                    selector = "#feedbackError";
                }
                $(selector).show().find("span").append(data.Message);

                Modal.Close();
            }
        });
        if (validateCustodyTransferRequest()) {
            $("#frmCustodyTransferRequest").submit();
        }

    };

    var fnDisableSubmit = function () {
        $("#btnSubmit").unbind("click");
    };

    if ($("#hdfEditRequestId").length > 0) {
        $("#btnSubmit").unbind("click");
        $("#btnSubmit").bind("click", fnSubmit);
    }

    $("body").unbind("keydown");
    $("body").bind("keydown", function (event) {
        if (event.which == 13 && $("#hdfEditRequestId").length > 0 && $("*:focus").attr("id") != "txtObservation") {
            fnSubmit();
        }
    });

});
