﻿$(function () {
    var context = $('#conteudo');

    $(context).bind('keydown', function (event) {
        if (event.which == 13) {
            $("#lnkSubmitVAMFilter").click();
        }
    });

    $("#lnkSubmitVAMFilter").unbind("click");
    $("#lnkSubmitVAMFilter").bind("click", function () {
        //$("#loadingArea").show();
        $("form").submit();
    });

//    $("form").submit(function () {
//        $("#loadingArea").hide();
//    });
});
