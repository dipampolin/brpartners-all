﻿var OperationsFilter = {
	ValidateFilterFields: function () {
		var messageError = "";
		if (!validateHelper.ValidateList($("#txtAssessor").val()))
			messageError += "<li>Faixa de assessores inválida.</li>";

		if (!validateHelper.ValidateList($("#txtCliente").val()))
			messageError += "<li>Faixa de clientes inválida.</li>";

		if (messageError.length > 0)
			messageError = "<ul>" + messageError + "</ul>";

		return { Result: (messageError == ""), Message: messageError }
	}
};

$(function () {
	var context = $('#operationsFilterContent');

	$("#operationsFilter").click(function () {
		$(context).slideToggle('fast');
		$('select').styleInputs();
		$(this).toggleClass('aberto');
	});

	$(context).bind('keydown', function (event) {
		if (event.which == 13) {
			$("#btnSearch").click();
		}
	});

	$("#btnSearch").unbind("click");
	$("#btnSearch").bind("click", function () {
		$(context).slideToggle('fast');
		$(this).toggleClass('fechado');
		$("#filterFeedbackError").html("").hide();
		$("#OperationsContent").hide();
		$("#loadingArea").show();
		$("#tableArea").hide();
		$("#feedback").hide().find("span").html('');
		$("#feedbackError").hide().find("span").html('');

		if (oTableOperations != null) {
			oTableOperations.fnDestroy();
			oTableOperations = null;
		}
		oTableOperations = $("#operationsList").dataTable(objOperationsFilter);
	});
});
