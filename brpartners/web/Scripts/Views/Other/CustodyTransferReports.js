﻿$(function () {
	$("#date1").setMask("99/99/9999").datepicker();
	$("#date2").setMask("99/99/9999").datepicker();

	$.ajax({
		type: 'POST',
		url: '/Other/CustodyTransferReportsRequest/',
		data: { fromDate: $("#date1").val(), toDate: $("#date2").val() },
		success: function (data) {
			$('.reportContent').html(data);

			$('#hdLastFromDate').val($("#date1").val());
			$('#hdLastToDate').val($("#date2").val());
			$('div.loadingArea').hide();

			disableExportButtons(false);
		},
		error: function (msg) {
			disableExportButtons(true);
		}
	});

	$("#lnkBuscar").click(function () {
		if ($("#date1").val().length > 0
				&& $("#date2").val().length > 0
				&& Date.parse($("#date1").val()) < Date.parse($("#date2").val())
				&& ($("#date1").val() != $("#hdLastFromDate").val() || $("#date2").val() != $("#hdLastToDate").val())) {
			$('div.loadingArea').show();
			$('.reportContent').html('');
			$.ajax({
				type: 'POST',
				url: '/Other/CustodyTransferReportsRequest/',
				data: { fromDate: $("#date1").val(), toDate: $("#date2").val() },
				success: function (data) {
					$('.reportContent').html(data);

					$('#hdLastFromDate').val($("#date1").val());
					$('#hdLastToDate').val($("#date2").val());
					$('div.loadingArea').hide();

					disableExportButtons(false);
				},
				error: function (msg) {
					disableExportButtons(true);
				}
			});
		}
	});
});

function disableExportButtons(disable) {
	$(".ico-pdf").attr("href", (disable) ? "#" : "/Other/CustodyTransferReportsToFile/");
	$(".ico-pdf").attr("target", (disable) ? "_self" : "_blank");
}