﻿var oTableOperationsMap = null;

var oTableOperator = null;

var objOperatorFilter = {
	"bAutoWidth": false
    , "bPaginate": true
    , "bDestroy": true
    , "sPaginationType": "full_numbers"
    , "sDom": 'trp'
    , "oLanguage": {
    	"sLengthMenu": "Exibindo _MENU_ registros",
    	"sZeroRecords": "Nenhum registro encontrado.",
    	"sInfo": "Encontramos _TOTAL_ registros",
    	"sInfoEmpty": "Nenhum registro encontrado",
    	"sInfoFiltered": "(Filtrando _MAX_ registros)",
    	"sSearch": "Buscar:",
    	"sProcessing": "Processando...",
    	"oPaginate": {
    		"sFirst": "Primeiro",
    		"sPrevious": "Anterior",
    		"sNext": "Próximo",
    		"sLast": "Último"
    	}
    }
    , "bProcessing": true
    , "bServerSide": true
    , "sAjaxSource": "../Other/LoadOperators"
    , "fnServerData": function (sSource, aoData, fnCallback) {
    	var context = $('#OperationsMapContent');

    	if (aoData != null && aoData != undefined) {
    		//aoData.push({ "name": "txtOperator", "value": $('#txtOperator', context).val() });
    		aoData.push({ "name": "txtClientFilter", "value": $('#txtClientFilter', context).val() });
    		aoData.push({ "name": "txtStockCode", "value": $('#txtStockCode', context).val() });
    		aoData.push({ "name": "ddlTradingDate", "value": $('#ddlTradingDate', context).val() });
    	}

    	$.ajax({
    		"cache": false,
    		"dataType": 'json',
    		"type": "POST",
    		"url": sSource,
    		"data": aoData,
    		"beforeSend": function () {
    			$("#table-operator-area").hide();
    			$("#loadingArea").show();
    			$("#emptyArea").hide();
    			$("#dvExportar").hide();
    		},
    		"success": function (data) {
    			fnCallback(data);

    			if (data.iTotalRecords == 0) {
    				$("#table-operator-area").hide();
    				$('#emptyArea').html('Sua pesquisa não encontrou resultados válidos. Tente novamente.').show();
    			}
    			else {
    				$('#emptyArea').html('').hide();
    			}
    		}
    	});
    }
    , fnDrawCallback: function () {

    	$("#loadingArea").hide();
    	$("#table-operator-area").show();

    	var totalRecords = this.fnSettings().fnRecordsDisplay();

    	if (totalRecords <= 0) {
    		$("#lnkOperationsMapExcel").attr("href", "javascript:void(0)");
    		$("#lnkOperationsMapPdf").attr("href", "javascript:void(0)");
    		$("#dvExportar").hide();
    	}
    	else {
    		$("#lnkOperationsMapExcel").attr("href", "/Other/OperationsMapToFile/?isPDF=false");
    		$("#lnkOperationsMapPdf").attr("href", "/Other/OperationsMapToFile/?isPDF=true");
    		$("#dvExportar").show();
    	}

    	var totalPerPage = this.fnSettings()._iDisplayLength;

    	var totalPages = Math.ceil(totalRecords / totalPerPage);

    	var currentIndex = parseInt(this.fnSettings()._iDisplayStart);
    	var currentPage = totalPages > 0 ? Math.ceil(1 + (currentIndex / totalPerPage)) : 0;

    	//$("#tblBrokerageDiscount_info").html("Exibindo " + currentPage + " de " + totalPages);

    	if (totalRecords > 0 && totalPages > 1) {
    		$("#tblOperator_info").css("display", "");
    		$("#tblOperator_length").css("display", "");
    		$("#tblOperator_paginate").css("display", "");
    	}
    	else {
    		$("#tblOperator_info").css("display", "none");
    		$("#tblOperator_length").css("display", "none");
    		$("#tblOperator_paginate").css("display", "none");
    	}

    	$(".link-operador").unbind("click").bind("click", function () {
    		var that = $(this);

    		var id = that.attr("rel");

    		var context = $('#OperationsMapContent');

    		$("#txtOperatorCode").val(id);

    		if (oTableOperationsMap != null)
    			oTableOperationsMap.fnDestroy();

    		oTableOperationsMap = $("#tblOperationsMap").dataTable(objFilter);
    	});

    	$(".link-operador").first().trigger("click");

    	// Adiciona a classe 'ultimo' à última tr no corpo da tabela
    	$('table tbody tr:last-child').addClass('ultimo');

    	// Adiciona a classe 'ultimo' às últimas th e td no corpo da tabela
    	$('table tr td:last-child').addClass('ultimo');
    	$('table tr th:last-child').addClass('ultimo');

    }
    , aoColumns: [
        { bSortable: false },
        { "sClass": "direita", bSortable: false }
    ]
};


var objFilter = {
	"bAutoWidth": false
    , "bPaginate": true
    , "bDestroy": true
    , "sPaginationType": "full_numbers"
    , "sDom": 'trp'
    , "oLanguage": {
    	"sLengthMenu": "Exibindo _MENU_ registros",
    	"sZeroRecords": "Nenhum registro encontrado.",
    	"sInfo": "Encontramos _TOTAL_ registros",
    	"sInfoEmpty": "Nenhum registro encontrado",
    	"sInfoFiltered": "(Filtrando _MAX_ registros)",
    	"sSearch": "Buscar:",
    	"sProcessing": "Processando...",
    	"oPaginate": {
    		"sFirst": "Primeiro",
    		"sPrevious": "Anterior",
    		"sNext": "Próximo",
    		"sLast": "Último"
    	}
    }
    , "bProcessing": true
    , "bServerSide": true
    , "sAjaxSource": "../Other/LoadOperatorClients"
    , "fnServerData": function (sSource, aoData, fnCallback) {
    	var context = $('#OperationsMapContent');

    	if (aoData != null && aoData != undefined) {
    		aoData.push({ "name": "txtOperatorCode", "value": $('#txtOperatorCode', context).val() });
    	}

    	$.ajax({
    		"cache": false,
    		"dataType": 'json',
    		"type": "POST",
    		"url": sSource,
    		"data": aoData,
    		"beforeSend": function () {
    			$("#table-client-area").hide();
    			$("#loadingArea").show();
    			$("#emptyArea").hide();
    		},
    		"success": function (data) {
    			fnCallback(data);

    			if (data.iTotalRecords == 0) {
    				$("#table-client-area").hide();
    				$('#emptyArea').html('Sua pesquisa não encontrou resultados válidos. Tente novamente.').show();
    			}
    			else {
    				$('#emptyArea').html('').hide();

    				OperationsMapMethods.LoadOperatorInfo(parseInt($('#txtOperatorCode', context).val(), 10));
    			}
    		}
    	});
    }
    , fnDrawCallback: function () {
    	$("#checkAll").attr("checked", false);

    	OperationsMapMethods.viewDetails();

    	$("#loadingArea").hide();
    	$("#table-client-area").show();

    	var totalRecords = this.fnSettings().fnRecordsDisplay();

    	if (totalRecords <= 0) {
    		$("#lnkOperationsMapExcel").attr("href", "javascript:void(0)");
    		$("#lnkOperationsMapPdf").attr("href", "javascript:void(0)");
    		$("#dvExportar").hide();
    	}
    	else {
    		$("#lnkOperationsMapExcel").attr("href", "/Other/OperationsMapToFile/?isPDF=false");
    		$("#lnkOperationsMapPdf").attr("href", "/Other/OperationsMapToFile/?isPDF=true");
    		$("#dvExportar").show();
    	}

    	var totalPerPage = this.fnSettings()._iDisplayLength;

    	var totalPages = Math.ceil(totalRecords / totalPerPage);

    	var currentIndex = parseInt(this.fnSettings()._iDisplayStart);
    	var currentPage = totalPages > 0 ? Math.ceil(1 + (currentIndex / totalPerPage)) : 0;

    	//$("#tblBrokerageDiscount_info").html("Exibindo " + currentPage + " de " + totalPages);

    	if (totalRecords > 0 && totalPages > 1) {
    		$("#tblOperationsMap_info").css("display", "");
    		$("#tblOperationsMap_length").css("display", "");
    		$("#tblOperationsMap_paginate").css("display", "");
    	}
    	else {
    		$("#tblOperationsMap_info").css("display", "none");
    		$("#tblOperationsMap_length").css("display", "none");
    		$("#tblOperationsMap_paginate").css("display", "none");
    	}

    	// Adiciona a classe 'ultimo' à última tr no corpo da tabela
    	$('table tbody tr:last-child').addClass('ultimo');

    	// Adiciona a classe 'ultimo' às últimas th e td no corpo da tabela
    	$('table tr td:last-child').addClass('ultimo');
    	$('table tr th:last-child').addClass('ultimo');

    }
    , aoColumns: [
        { bSortable: false },
        { bSortable: false },
        { "sClass": "direita", bSortable: false },
        { "sClass": "direita", bSortable: false },
        { bSearchable: false, bSortable: false }
    ]
};


var OperationsMapMethods = {
    viewDetails: function () {
        $(".ico-detalhes").unbind("click").bind("click", function () {
            var that = $(this);

            var id = that.attr("rel");

            if (that.attr("class").toString().indexOf("fechado") > -1) {

                var td = that.parent().parent();

                $.ajax({
                    type: "post",
                    url: "../Other/OperationMapDetails",
                    cache: false,
                    dataType: "html",
                    data: { clientCode: id },
                    success: function (data) {
                        td.after(
                            $("<tr class='details' id='" + id + "'><td colspan=5 >" + data + "</td></tr>").clone()
                        );

                        that.removeClass("fechado").addClass("aberto");
                    }
                });
            }
            else {
                $("#" + id).remove();
                that.removeClass("aberto").addClass("fechado");
            }
        });
    }
    , LoadOperatorInfo: function (operatorId) {
        $.ajax({
            cache: false,
            async: false,
            url: "../Other/LoadOperatorDetails",
            dataType: "html",
            type: 'POST',
            data: { operatorCode: operatorId },
            success: function (data) {
                $("#divOperatorContent").html(data);

                $(".lnk-anterior, .lnk-proximo").unbind('click').bind('click', function () {
                    var that = $(this);

                    var id = that.attr("rel");

                    var context = $('#OperationsMapContent');

                    $("#txtOperatorCode").val(id);

                    if (oTableOperationsMap != null)
                        oTableOperationsMap.fnDestroy();

                    oTableOperationsMap = $("#tblOperationsMap").dataTable(objFilter);
                });
            }
        });
    }
};