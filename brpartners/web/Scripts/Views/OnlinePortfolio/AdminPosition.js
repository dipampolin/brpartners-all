﻿/// <reference path="../../References.js" />

var emptyDataMessage = "Sua pesquisa não encontrou resultados válidos. Tente novamente.";
var clientWithoutPositionMessage = "O cliente não tem posição.";
var feedbackCount = 1;

$(function () {
    $(document).ready(function () {

        var context = $('#adminPositionFilterContent');

        $(context).bind('keydown', function (event) {
            if (event.which == 13) {
                $('#txtClient').blur();
                $("#btnSearch", context).click();
            }
        });

        $("#btnSearch", context).click(function () {

            $("#filterFeedbackError", context).hide();

            var feedback = ValidateFilterFields(context);
            if (feedback != "") {
                $("#filterFeedbackError", context).html(feedback).show();
                return false;
            }

            if (feedbackCount == 0)
                $("#feedbackError").hide();

            $("#emptyArea").hide();
            $("#tableArea").hide();
            $("#divClientHeader").hide();
            $("#loadingArea").show();

            if (context.css("display") != "none")
                $("#adminPositionFilter").click();

            $("#filterFeedbackError", context).hide();

            feedbackCount = 0;

            var clientCode = $("#hdfClientCode", context).val();

            $.ajax({
                type: "POST",
                url: "/OnlinePortfolio/SearchAdminPosition/",
                dataType: "json",
                data: { clientCode: clientCode },
                success: function (data) {

                    $("#loadingArea").hide();

                    if (data.Error == undefined) {

                        var excelLink = $("#lnkAdminPositionExcel");
                        var pdfLink = $("#lnkAdminPositionPdf");

                        if (data.ClientName == "") {
                            $("#emptyArea").html(emptyDataMessage).show();
                            excelLink.attr("href", "#");
                            excelLink.attr("target","_self");
                            pdfLink.attr("href", "#");
                            pdfLink.attr("target", "_self");
                            return false;
                        }

                        $("#lblClientName").html(clientCode + " " + data.ClientName);
                        $("#lblClientPhone").html(data.ClientPhone);
                        $("#lblClientEmail").html(data.ClientEmail);
                        $("#divClientHeader").show();

                        if (data.ClientEmail != "-") {
                            $("#lnkClientEmail").attr("href", "mailto:" + data.ClientEmail);
                        }
                        else { $("#lnkClientEmail").attr("href", "#"); }

                        if (data.HtmlTableItems != "") {
                            $("#tableArea tbody").html(data.HtmlTableItems);
                            $("#tableArea").show();
                        }
                        else {
                            $("#emptyArea").html(clientWithoutPositionMessage).show();
                            $("#tableArea").hide();
                        }

                        var emptyData = (data.HtmlTableItems == null || data.HtmlTableItems.length == 0);

                        excelLink.attr("href", (emptyData) ? "#" : "/OnlinePortfolio/AdminPositionToFile/?isPdf=false");
                        excelLink.attr("target", (emptyData) ? "_self" : "_blank");
                        pdfLink.attr("href", (emptyData) ? "#" : "/OnlinePortfolio/AdminPositionToFile/?isPdf=true");
                        pdfLink.attr("target", (emptyData) ? "_self" : "_blank");
                    }
                    else {
                        $("#feedbackError").html(data.Error).show();
                    }

                }
            });

            return false;
        });

    });

});

function ViewHistory() {
    commonHelper.OpenModal("/OnlinePortfolio/AdminClientsHistory/", 671, ".content", "");
}

function DeleteRequest(id) {

    var url = "/OnlinePortfolio/ConfirmDeleteAdminPosition/?id=" + id;
    commonHelper.OpenModal(url, 450, ".content", "");
    $("#MsgBox").center();
}

function OpenRequest(id) {
    commonHelper.OpenModal("/OnlinePortfolio/EditAdminPosition/" + id, 890, ".content", "");
    $("#MsgBox").center();
}

function OpenPartition(id) {
    commonHelper.OpenModal("/OnlinePortfolio/AdminPositionPartition/" + id, 875, ".content", "");
    $("#MsgBox").center();
}

function ValidateFilterFields(context) {
    var messageError = "";

    if ($("#hdfClientCode", context).val() == "")
        messageError += "<li>É necessário escolher um cliente.</li>";

    if (messageError.length > 0)
        messageError = "<ul>" + messageError + "</ul>";

    return messageError;
}

function DeletePosition(requestId) {

    $.ajax({
        url: "/OnlinePortfolio/DeleteAdminPosition",
        type: "POST",
        dataType: "json",
        data: { requestId: requestId },
        success: function (data) {
            if (data.Error == "") {
                Modal.Close();
                $("div#adminPositionfeedback span").html("Posição excluída com sucesso.");
                $("div#adminPositionfeedback").show();
                $("#adminPositionFilterContent #btnSearch").click();
            }
            else {
                $("#feedbackRequestError", context).html(data.Error);
                $("#feedbackRequestError", context).show();
            }
        }
    });

    return false;
}