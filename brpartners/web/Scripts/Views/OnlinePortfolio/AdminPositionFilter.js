﻿/// <reference path="../../jQuery/jquery-1.4.3.js" />


$(function () {

    $(document).ready(function () {

        var context = $('#adminPositionFilterContent');
        var selectedClientName = "";

        $("#adminPositionFilter").click(function () {
            $('#adminPositionFilterContent').slideToggle('fast');
            $('select').styleInputs();
            $(this).toggleClass('aberto');
        });

        $("#txtClient", context).autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/OnlinePortfolio/ClientSuggest",
                    type: "POST",
                    dataType: "json",
                    data: { term: request.term },
                    success: function (data) {
                        response($.map(data, function (item) {
                            return {
                                label: item.Description,
                                value: item.Description,
                                id: item.Value
                            }
                        }));
                    }
                });
            },
            minLength: 3,
            select: function (event, ui) {
                $("#hdfClientCode", context).val(ui.item.id);
                selectedClientName = ui.item.value;
            }
        });

        $("#txtClient", context).blur(function () {
            var value = $(this).val();
            var isInt = TryParseInt(value, false);
            if (!isInt && value != selectedClientName) {
                $(this).val("");
                selectedClientName = "";
                $("#hdfClientCode", context).val("");
            }

            if (value == isInt) {
                $("#hdfClientCode", context).val(value);
            }

            if (value == "") {
                $("#hdfClientCode", context).val("");
            }
        });

    });
});

function TryParseInt(str,defaultValue) {
    var retValue = defaultValue;

    if (typeof str != 'undefined' && str != null && str.length > 0) {
        str = str.replace(/\./g, '');
        if (!isNaN(str)){
               retValue = parseInt(str);
         }
    }
    return retValue;
}