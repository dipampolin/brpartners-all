﻿/// <reference path="../../References.js" />
var oTable;
var previousIndex = -1;
var nextIndex = 1;
var feedbackCount = 1;

$(function () {
    $(document).ready(function () {

        var filterContext = "#PortfolioFilterContent";

        $("#PortfolioFilter").click(function () {
            $('#PortfolioFilterContent').slideToggle('fast');
            $('select').styleInputs();
            $(this).toggleClass('aberto');
        });

        var lnkPrevious = $("#lnkPreviousClient");
        var lnkNext = $("#lnkNextClient");
        var lnkPreviousTop = $("#lnkPreviousClientTop");
        var lnkNextTop = $("#lnkNextClientTop");

        var clientCodeFromPosition = $("#ClientCodeFromPosition").val();
        if (clientCodeFromPosition != "") {
            setTimeout(function () {
                $("#btnSearch", filterContext).click();
            }, 150);
        }

        $("#btnSearch", filterContext).click(function () {

            if (feedbackCount == 0)
                $("#feedbackError").hide();

            lnkPrevious.hide();
            lnkNext.hide();
            lnkPreviousTop.hide();
            lnkNextTop.hide();

            $("#pnlNoHistory").hide();
            $("#tableArea").hide();
            $("#loadingArea").show();

            $("#filterFeedbackError", filterContext).hide();

            if ($(filterContext).css("display") != "none") {
                $('#PortfolioFilterContent').slideToggle('fast');
                $('select').styleInputs();
                $(filterContext).toggleClass('aberto');
            }

            var clientCode = $("#hdfFilterClientCode", filterContext).val();

            var clientCodeFromRequest = $("#ClientCodeFromPosition").val();
            if (clientCodeFromRequest != "") {
                clientCode = clientCodeFromRequest;
                $("#hdfFilterClientCode", filterContext).val(clientCodeFromRequest);
                $("#txtClient", filterContext).val(clientCodeFromRequest);
            }

            $.ajax({

                cache: false,
                type: "POST",
                dataType: "json",
                async: true,
                url: "/OnlinePortfolio/LoadClientHistory",
                data: { ClientCode: clientCode, Index: $("#listIndex").val() },
                success: function (data) {

                    feedbackCount = 0;

                    if (clientCodeFromRequest != "") {
                        $("#ClientCodeFromPosition").val("");
                    }

                    $("#loadingArea").hide();

                    var emptyData = (data == null || data.ClientName == "");

                    var excelLink = $("#lnkPositionExcel");
                    var pdfLink = $("#lnkPositionPdf");
                    excelLink.attr("href", (emptyData) ? "#" : "/OnlinePortfolio/HistoryToFile/?isPdf=false");
                    //excelLink.attr("target", (emptyData) ? "_self" : "_blank");
                    pdfLink.attr("href", (emptyData) ? "#" : "/OnlinePortfolio/HistoryToFile/?isPdf=true");
                    //pdfLink.attr("target", (emptyData) ? "_self" : "_blank");

                    if (oTable != undefined)
                        oTable.fnClearTable();

                    if (emptyData) {
                        $("#emptyArea").show();
                        $("#tableArea").hide();
                    }
                    else {

                        SetClientData(data);

                        previousIndex = data.Index - 1;
                        nextIndex = data.Index + 1;

                        var length = data.Length;

                        if (length < 2) {
                            lnkPrevious.hide();
                            lnkNext.hide();
                            lnkPreviousTop.hide();
                            lnkNextTop.hide();
                        }
                        else {
                            if (data.Index == (length - 1)) {
                                lnkNext.hide();
                                lnkPrevious.show();
                                lnkNextTop.hide();
                                lnkPreviousTop.show();
                            }
                            else {
                                if (data.Index != 0) {
                                    lnkPrevious.show();
                                    lnkPreviousTop.show();
                                }
                                else {
                                    lnkPrevious.hide();
                                    lnkPreviousTop.hide();
                                }
                                lnkNext.show();
                                lnkNextTop.show();
                            }
                        }

                        $("#tableArea").show();
                        $("#emptyArea").hide();
                    }
                    $('select').styleInputs();
                }
            });

            return false;
        });

        $("#txtClient", filterContext).autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/OnlinePortfolio/ClientSuggest",
                    type: "POST",
                    dataType: "json",
                    data: { term: request.term },
                    success: function (data) {
                        response($.map(data, function (item) {
                            return {
                                label: item.Description,
                                value: item.Description,
                                id: item.Value
                            }
                        }));
                    }
                });
            },
            minLength: 3,
            select: function (event, ui) {
                $("#hdfFilterClientCode", filterContext).val(ui.item.id);
                $("#hdfFilterClientName", filterContext).val(ui.item.value);
            }
        });

        $("#txtClient", filterContext).blur(function () {
            var selectedClient = $("#hdfFilterClientName", filterContext);
            if (this.value != "") {
                if (!isNaN(this.value)) {
                    $("#hdfFilterClientCode", filterContext).val(this.value);
                }
                else
                    if (this.value != selectedClient.val()) {
                        $("#hdfFilterClientCode", filterContext).val("");
                        $(selectedClient).val("");
                        $(this).val("");
                    }
            }
            else {
                $("#hdfFilterClientCode", filterContext).val("");
                $(selectedClient).val("");
            }

        });



        $("#ddlPage", "#tableArea").change(function () {
            if (this.value != "") {
                var clientCode = $("#hdfClientCode", "#tableArea").val();
                window.location = clientCode == "0" ? "/OnlinePortfolio/" + this.value + "/" : "/OnlinePortfolio/" + this.value + "/?ClientCode=" + clientCode;
            }
            else
                return false;
        });

        var operatorClients = $("#OperatorClients").val();
        if (operatorClients == "") {
            $("#emptyArea").show();
            $("#tableArea").hide();
            $("#loadingArea").hide();
        }
        else {
            setTimeout(function () {
                $("#btnSearch", filterContext).click();
            }, 250);
        }
    });
});

function GetClientList(next) {

    setTimeout(function () {
        LoadClientPosition("", (!next ? previousIndex : nextIndex));
    }, 250);

    return false;
}

function SetClientData(data) {

    var context = "#tableArea";

    if (data.HtmlTableItems == "") {
        $("#pnlNoHistory", context).show();
        $("#historyList", context).hide();
    }
    else {
        $("#pnlNoHistory", context).hide();
        $("#historyList", context).show();
        $("#historyList", context).html(data.HtmlTableItems);
    }
    $("#lblClientName", context).html(data.ClientName);
    $("#hdfClientCode", context).val(data.ClientCode);
    $("#lblClientPhone", context).html(data.ClientPhone);
    $("#lblClientEmail", context).html(data.ClientEmail);
    $("#lnkClientEmail", context).attr("href", "mailto:" + data.ClientEmail);
    $("#lblClientCounterTop", context).html(data.Length < 2 ? "" : data.Legend);
    $("#lblClientCounter", context).html(data.Length < 2 ? "" : data.Legend);

    $("#lblClientCounter", context).html(data.Length < 2 ? "" : data.Legend);
}


function LoadClientPosition(clientCode, index) {

    $("#tableArea").hide();
    $("#loadingArea").show();

    var lnkPrevious = $("#lnkPreviousClient");
    var lnkNext = $("#lnkNextClient");
    var lnkPreviousTop = $("#lnkPreviousClientTop");
    var lnkNextTop = $("#lnkNextClientTop");

    lnkPrevious.hide();
    lnkNext.hide();
    lnkPreviousTop.hide();
    lnkNextTop.hide();

    $.ajax({
        cache: false,
        type: "POST",
        dataType: "json",
        async: true,
        url: "/OnlinePortfolio/LoadClientHistory",
        data: { Index: index, ClientCode: clientCode },
        success: function (data) {
            if (oTable != undefined)
                oTable.fnClearTable();

            $("#loadingArea").hide();
            $("#tableArea").show();

            SetClientData(data);

            previousIndex = data.Index - 1;
            nextIndex = data.Index + 1;

            var length = data.Length;

            if (length < 2) {
                lnkPrevious.hide();
                lnkNext.hide();
                lnkPreviousTop.hide();
                lnkNextTop.hide();
            }
            else {
                if (data.Index == (length - 1)) {
                    lnkNext.hide();
                    lnkPrevious.show();
                    lnkNextTop.hide();
                    lnkPreviousTop.show();
                }
                else {
                    if (data.Index == 0) {
                        lnkPrevious.hide();
                        lnkPreviousTop.hide();
                    }
                    else {
                        lnkPrevious.show();
                        lnkPreviousTop.show();
                    }
                    lnkNext.show();
                    lnkNextTop.show();
                }
            }
        }
    });

    return false;
}