﻿/// <reference path="../../References.js" />

var oTable;
var emptyDataMessage = "Sua pesquisa não encontrou resultados válidos. Tente novamente.";
var filterPage = 1;
var feedbackCount = 1;

$(function () {
    $(document).ready(function () {

        var context = $('#adminClientsFilterContent');

        $(context).bind('keydown', function (event) {
            if (event.which == 13) {
                $('#txtClient').blur();
                $("#btnSearch", context).click();
            }
        });

        $("#btnSearch", context).click(function () {

            if (feedbackCount == 0)
                $("#feedbackError").hide();

            commonHelper.ShowLoadingArea();

            if (context.css("display") != "none")
                $("#adminClientsFilter").click();

            $("#filterFeedbackError", context).hide();

            feedbackCount = 0;
            $.ajax({
                cache: false,
                type: "POST",
                async: true,
                url: "/OnlinePortfolio/LoadAdminClients",
                data: { clientCode: $("#hdfClientCode", context).val(), calculation: $("#ddlCalculation", context).val(), history: $("#ddlHistory", context).val(), attachType: $("#ddlAttachType", context).val() },
                success: function () {
                    obj = {};
                    obj = {
//                        "aoColumns": [
//                                 { "sWidth": "10%" },
//                                 { "sWidth": "35%" },
//                                 { "sWidth": "35%" },
//                                 { "sWidth": "10%" },
//                                 { "bSortable": false, "sWidth": "10%" }
//                            ],

                        "aaSorting": [[1, "asc"]],
                        "bSort": true,
                        "bAutoWidth": false,
                        "bInfo": true,
                        "iDisplayLength": 10,
                        "oLanguage": {
                            "sLengthMenu": "<span>Exibir:</span> _MENU_ <span>por página</span>",
                            "sZeroRecords": "Sua pesquisa não encontrou resultados válidos. Tente novamente.",
                            "oPaginate": {
                                "sPrevious": "Anteriores",
                                "sNext": "Próximos"
                            }
                        },
                        "sPaginationType": "full_numbers",
                        "sDom": 'rtip',
                        "fnDrawCallback": function () {
                            var totalRecords = this.fnSettings().fnRecordsDisplay();
                            var emptyArea = $("#emptyArea");
                            var tableArea = $("#tableArea");

                            var excelLink = $("#lnkAdminClientsExcel");
                            var pdfLink = $("#lnkAdminClientsPdf");

                            if (excelLink.length > 0 && pdfLink.length > 0) {
                                if (totalRecords == 0) {
                                    $(excelLink).click(function () {
                                        excelLink.attr("href", "#");
                                        return false;
                                    });
                                    $(pdfLink).click(function () {
                                        pdfLink.attr("href", "#");
                                        return false;
                                    });
                                }
                                else {
                                    $(excelLink).click(function () {
                                        excelLink.attr("href", "/OnlinePortfolio/AdminClientsToFile/?isPdf=false");
                                        excelLink.attr("target", "_blank");

                                    });
                                    $(pdfLink).click(function () {
                                        pdfLink.attr("href", "/OnlinePortfolio/AdminClientsToFile/?isPdf=true");
                                        pdfLink.attr("target", "_blank");

                                    });
                                }
                            }

                            if (totalRecords == 0) {
                                emptyArea.html(emptyDataMessage);
                                emptyArea.show();
                                tableArea.hide();
                            }
                            else {
                                tableArea.show();
                                emptyArea.hide();

                                var totalPerPage = this.fnSettings()._iDisplayLength;

                                var totalPages = Math.ceil(totalRecords / totalPerPage);

                                var currentIndex = parseInt(this.fnSettings()._iDisplayStart);
                                var currentPage = totalPages > 0 ? Math.ceil(1 + (currentIndex / totalPerPage)) : 0;

                                $("#adminClientsList_info").html("Exibindo " + currentPage + " de " + totalPages);

                                if (totalRecords > 0 && totalPages > 1) {
                                    $("#adminClientsList_info").css("display", "");
                                    $("#adminClientsList_length").css("display", "");
                                    $("#adminClientsList_paginate").css("display", "");
                                }
                                else {
                                    $("#adminClientsList_info").css("display", "none");
                                    $("#adminClientsList_length").css("display", "none");
                                    $("#adminClientsList_paginate").css("display", "none");
                                }
                                $('select', '#adminClientsList_length').styleInputs();
                            }
                            // Adiciona a classe 'ultimo' à última tr no corpo da tabela
                            $('table tbody tr:last-child').addClass('ultimo');

                            // Adiciona a classe 'ultimo' às últimas th e td no corpo da tabela
                            $('table tr td:last-child').addClass('ultimo');
                            $('table tr th:last-child').addClass('ultimo');
                            $("#loadingArea").hide();
                        },
                        "bProcessing": false,
                        "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                            $('#adminClientsList_first').css("visibility", "hidden");
                            $('#adminClientsList_last').css("visibility", "hidden");
                        },
                        "bRetrieve": true,
                        "bServerSide": true,
                        "sAjaxSource": "../DataTables/PortfolioAdminClients_List",
                        "fnServerData": function (sSource, aoData, fnCallback) {
                            $.ajax({
                                cache: false,
                                dataType: 'json',
                                type: "POST",
                                url: sSource,
                                data: aoData,
                                success: fnCallback
                            });
                        }
                    };

                    oTable = $("#adminClientsList").dataTable(obj);
                    oTable.fnClearTable();
                    oTable.fnDraw();
                }
            });

            return false;
        });

        setTimeout(function () {
            $("#btnSearch", context).click();
        }, 250);

    });


});

function ViewHistory() {
    commonHelper.OpenModal("/OnlinePortfolio/AdminClientsHistory/", 671, ".content", "");
}

function DeleteRequest(id, name) {

    var url = "/OnlinePortfolio/ConfirmDeleteAdminClient/?code=" + id + "&name=" + escape(name);
    commonHelper.OpenModal(url, 450, ".content", "");
    $("#MsgBox").center();
}

function OpenRequest(id) {
    commonHelper.OpenModal("/OnlinePortfolio/EditAdminClients/" + id, 415, ".content", "");
    $("#MsgBox").center();
}
