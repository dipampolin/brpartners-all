﻿$(function () {

    var rowCount = 0;
    var context = $("#frmAdminPositionPartition");
    var table = $("#adminPartitionList");

    $(document).ready(function () {

        $("#lnkAdd", context).click(function () {
            LoadTableLine();
        });

        $(".ico-excluir", table).live("click", function () {
            $(this).parents('tr').remove();
            if ($('#adminPartitionList tr', context).length == 3) {
                $(".action-place-holder", table).hide();
            }

            $("#MsgBox").center();
        });

        $("#lnkSave", context).click(function () {
            var feedback = ValidateFields();
            if (feedback != "") {
                $("#feedbackRequestError", context).html(feedback);
                $("#feedbackRequestError", context).show();
                return false;
            }

            var data = "";
            data += $(context).serialize();

            $.ajax({
                url: "/OnlinePortfolio/AdminPositionPartition",
                type: "POST",
                dataType: "json",
                data: data,
                success: function (data) {
                    if (data.Error == "") {
                        Modal.Close();
                        $("div#adminPositionfeedback span").html("Posição atualizada com sucesso.");
                        $("div#adminPositionfeedback").show();

                        if ($("#adminPositionFilterContent #txtClient").val() != 'undefined' && $("#adminPositionFilterContent #txtClient").val() != "") { $("#adminPositionFilterContent #btnSearch").click(); }
                    }
                    else {
                        $("#feedbackRequestError", context).html(data.Error);
                        $("#feedbackRequestError", context).show();
                    }
                }
            });

            return false;
        });

        LoadTableLine();
        LoadTableLine();
    });

    function LoadTableLine() {
        var newTr = "";
        rowCount = $('#adminPartitionList tr', context).length;
        var classTr = (rowCount % 2 == 0) ? "even" : "odd";

        newTr += "<tr class='" + classTr + "'>";
        newTr += "<td>" + DropDownList("ddlLongShort-") + "</td>";
        newTr += "<td>" + InputText("txtQuantity-") + "</td>";
        newTr += "<td>" + InputText("txtPrice-") + "</td>";
        newTr += "<td>" + InputText("txtNetPrice-") + "</td>";
        newTr += "<td>" + InputText("txtPositionDate-") + "</td>";
        newTr += "<td>" + InputText("txtDueDate-") + "</td>";
        newTr += "<td>" + InputText("txtRollOverDate-") + "</td>";
        newTr += "<td class='action-place-holder' style='display:none;'>" + RemoveLink() + "</td>";
        newTr += "</tr>";

        $("#adminPartitionList tbody", context).append(newTr);

        if ($("#hdfMarket", context).val() == "TER") {
            $("#txtDueDate-" + rowCount + " , #txtRollOverDate-" + rowCount, table).datepicker($.datepicker.regional['pt-BR']).setMask("99/99/9999");
        }
        else {
            $("#txtDueDate-" + rowCount + " , #txtRollOverDate-" + rowCount, table).hide();
        }

        $('select').styleInputs();
        $("#txtPositionDate-" + rowCount).datepicker($.datepicker.regional['pt-BR']).setMask("99/99/9999");

        $.mask.masks.DecimalValue = { mask: '99,999.999.999', type: 'reverse', defaultValue: '000' };
        $("#txtPrice-" + rowCount + " , #txtNetPrice-" + rowCount, table).setMask('DecimalValue');

        $.mask.masks.Quantity = { mask: '999.999.999.999.999', type: 'reverse' };
        $("#txtQuantity-" + rowCount, table).setMask('Quantity');

        if (rowCount > 2) {
            $(".action-place-holder", table).show();
        }

        $("#MsgBox").center();
    }

    function InputText(name) {

        return "<input type='text' id='" + name + rowCount + "' name='" + name.replace('-', '') + "'  />";
    }

    function DropDownList(name) {
        var c = "<select id='" + name + rowCount + "' name='" + name.replace('-', '') + "'>";
        c += "<option value='1'>Sim</option><option value='0'>Não</option>";
        c += "</select>";

        return c;
    }

    function RemoveLink() {
        if (rowCount <= 2)
            return " ";

        return "<a href='javascript:void(0);' class='ico-excluir' id='lnkRemove'>x</a>";
    }

    function ValidateFields() {
        var feedback = "";

        $("input", context).each(function () {
            if ($(this).val() == "" && $(this).css('display') != 'none' && feedback == "") {
                feedback += "<li>É necessário preencher todos os campos.</li>";
            }
        });

        var total = 0;
        $("input[name='txtQuantity']", context).each(function () {
            var value = this.value.replace(/\./g, '');
            if (!isNaN(value) && value.length != 0) {
                total += parseInt(value, 10);
            }
        });

        var invalidDate = false;
        $("input[id*='Date-']", context).each(function () {
            if ($(this).val() != "" && !invalidDate) {
                if (!DateValidate($(this).val())) {
                    invalidDate = true;
                    feedback += "<li>Existe uma data inválida.</li>";
                }
            }
        });

        if (parseInt($("#hdfQuantity", context).val().replace(/\./g, ''), 10) != total) {
            feedback += "<li>A soma das quantidades de cada partição deve ser igual à total.</li>";
        }

        return feedback;
    }

    function TryParseInt(str, defaultValue) {
        var retValue = defaultValue;

        if (typeof str != 'undefined' && str != null && str.length > 0) {
            str = str.replace(/\./g, '');
            if (!isNaN(str)) {
                retValue = parseInt(str);
            }
        }
        return retValue;
    }

    function DateValidate(date) {
        var reDate = /^((0[1-9]|[12]\d)\/(0[1-9]|1[0-2])|30\/(0[13-9]|1[0-2])|31\/(0[13578]|1[02]))\/\d{4}$/;
        if (reDate.test(date) || (date == '')) {
            return true
        } else {
            return false;
        }
    }

});
