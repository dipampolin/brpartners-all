﻿
var EmailModelFunctions =
{
    trim: function (str) {
        return str.replace(/^\s+|\s+$/g, "");
    },

    checkHTMLContent: function (content) {
        var strInputCode = content;

        strInputCode = content.replace(/&(lt|gt);/g, function (strMatch, p1) {
            return (p1 == "lt") ? "<" : ">";
        });

        var strTagStrippedText = strInputCode.replace(/<\/?[^>]+(>|$)/g, "");

        return EmailModelFunctions.trim(strTagStrippedText).length > 0;
    },

    replaceAll: function (string, token, newtoken) {
        while (string.indexOf(token) != -1) {
            string = string.replace(token, newtoken);
        }
        return string;
    },

    saveDisclaimer: function () {
        var iframe = $("#txtContent_ifr");
        iframe.removeClass("error");
        $("#feedbackError").hide();
        var tineMCEContent = escape(tinyMCE.get('txtContent').getContent());
        if (EmailModelFunctions.checkHTMLContent(tineMCEContent)) {
            tinyMCE.triggerSave();
            $("#frmEmailModel").submit();

        }
        else {
            $("#feedbackError").html(commonHelper.FormatErrorMessage("Oops. Faltou preencher o campo abaixo", false));
            $("#feedbackError").show();
            iframe.addClass("error");
        }
    }
    , validate : function () {
        var request = true;

        $("#requestFeedbackError").hide().find("ul").html('');

        var ul = $("div#requestFeedbackError > ul");
        if ($("#txtReplyName").val() == null || $("#txtReplyName").val() == undefined || $("#txtReplyName").val() == "") {
            ul.append($("<li>Remetente não informado.</li>").clone());
            request = false;
        }

        if ($("#txtReplyMail").val() == "" || $("#txtReplyMail").val() == null) {
            ul.append($("<li>Email Remetente não informado.</li>").clone());
            request = false;
        }

        if ($("#txtSubject").val() == null || $("#txtSubject").val() == undefined || $("#txtSubject").val() == "") {
            ul.append($("<li>Assunto não informado.</li>").clone());
            request = false;
        }

        if (tinyMCE.get('txtContent').getContent() == ''){
            ul.append($("<li>Conteúdo não informado.</li>").clone());
            request = false;
        }

        if (request == false) {
            $("#requestFeedbackError").show().focus();
        }
        else {
            $("#requestFeedbackError").hide();
            $("#requestFeedbackError > ul").html('');
        }

        return request;
    }

};



$(function () {

    $("#frmEmailModel").ajaxForm({
        beforeSend: function () {
            $("#feedback").hide().find("span").html('');
            $("#feedbackError").hide().find("span").html('');
        },
        success: function (data) {
            $("#loadingArea").hide();

            if (data.Result) {
                selector = "#feedback";
                window.location = window.location;
            }
            else {
                selector = "#feedbackError";
                $(selector).show().find("span").append(data.Message);
                Modal.Close();
            }
        }
    });

    $("#btnSave").unbind("click").bind("click", function () {
        if (EmailModelFunctions.validate()) {
            EmailModelFunctions.saveDisclaimer();
        }
    });

    if (tinyMCE.getInstanceById("txtContent") != null) {
        try {
            tinyMCE.execCommand('mceFocus', false, 'txtContent');
            tinyMCE.execCommand('mceRemoveControl', true, "txtContent");
        }
        catch (ex) { console.log(ex); }
    }

    tinyMCE.execCommand('mceAddControl', false, 'txtContent');

    $(".qx-modal-btn-fechar").unbind("click").bind("click", function () {
        window.location = window.location;
        Modal.Close();
    });
});

