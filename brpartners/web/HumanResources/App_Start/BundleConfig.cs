﻿using System.Web.Optimization;

namespace Tegra.HumanResources.Web.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            var vendorBundle = new ScriptBundle("~/bundles/vendor")
                .Include("~/HumanResources/Scripts/vendor/bootstrap.min.js")
                .Include("~/HumanResources/Scripts/vendor/moment.min.js")
                .Include("~/HumanResources/Scripts/vendor/moment.br.js")
                .Include("~/HumanResources/Scripts/vendor/angular.min.js")
                .Include("~/HumanResources/Scripts/vendor/angular-messages.min.js")
                .Include("~/HumanResources/Scripts/vendor/angular-animate.min.js")
                .Include("~/HumanResources/Scripts/vendor/smart-table.min.js")
                .Include("~/HumanResources/Scripts/vendor/md-form-validator.js")
                .Include("~/HumanResources/Scripts/vendor/angular-locale_pt-br.min.js")
                .Include("~/HumanResources/Scripts/vendor/angular-input-masks-standalone.min.js")
                .Include("~/HumanResources/Scripts/vendor/angular-toastr.min.js")
                .Include("~/HumanResources/Scripts/vendor/angular-toastr.tpls.min.js")
                .Include("~/HumanResources/Scripts/vendor/ui-bootstrap-tpls-2.1.4.min.js")
                .Include("~/HumanResources/Scripts/vendor/angular-filter.min.js")
                .Include("~/HumanResources/Scripts/vendor/cep-promise-browser.min.js")
                .Include("~/HumanResources/Scripts/vendor/mask.min.js")
                .Include("~/HumanResources/Scripts/vendor/Chart.min.js")
                .Include("~/HumanResources/Scripts/vendor/angular-chart.min.js")
                .Include("~/HumanResources/Scripts/vendor/ng-file-upload.min.js")
                .Include("~/HumanResources/Scripts/vendor/angular-drag-and-drop-lists.min.js")
                .Include("~/HumanResources/Scripts/vendor/angular-sanitize.min.js");

            bundles.Add(vendorBundle);

            var bundle = new ScriptBundle("~/bundles/all")
                .Include("~/HumanResources/Scripts/app/App.js")
                .IncludeDirectory("~/HumanResources/Scripts/app/", "*.js", true);

            bundles.Add(bundle);
        }
    }
}