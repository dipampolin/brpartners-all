﻿using QX3.Portal.WebSite.Attributes;
using System.Web.Mvc;

namespace Tegra.HumanResources.Web.App_Start
{
    public class FilterConfig
    {

        public void FixEfProviderServicesProblem()
        {
            var instance = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }

        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new JsonHandleErrorAttribute());
        }

    }
}