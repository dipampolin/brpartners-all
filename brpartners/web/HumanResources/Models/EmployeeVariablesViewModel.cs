﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QX3.Portal.WebSite.HumanResources.Models
{
    public class EmployeeVariablesViewModel
    {
        public int Id { get; set; }

        public int? EmployeeVariablesTypeId { get; set; }

        public string Type { get; set; }

        public string Description { get; set; }

        public decimal Value { get; set; }

        public DateTime Date { get; set; }

        public int EmployeeId { get; set; }
    }
}