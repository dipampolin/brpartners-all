﻿using System;

namespace QX3.Portal.WebSite.HumanResources.Models
{
    public class ReportItemViewModel
    {
        public int Id { get; set; }

        public string Field { get; set; }

        public string FieldAlias { get; set; }

        public string Value { get; set; }

        public string Table { get; set; }

        public string TableAlias { get; set; }

        public string Type { get; set; }
    }
}