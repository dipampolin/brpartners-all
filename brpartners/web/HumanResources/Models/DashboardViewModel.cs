﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tegra.HumanResources.Domain.Entities;

namespace QX3.Portal.WebSite.HumanResources.Models
{
    public class DashboardViewModel
    {

        public string Name { get; internal set; }
        public int ActiveEmployees { get; internal set; }
        public int VacationEmployees { get; internal set; }
        public int NewEmployees { get; internal set; }
        public int InactiveEmployees { get; internal set; }
        public Dictionary<String, int> DepartmentDistribution { get; internal set; }

    }


}