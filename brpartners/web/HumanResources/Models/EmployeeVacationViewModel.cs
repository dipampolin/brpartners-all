﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QX3.Portal.WebSite.HumanResources.Models
{
    public class EmployeeVacationViewModel
    {
        public int Id { get; set; }

        public DateTime AquisitionStartDate { get; set; }

        public DateTime AquisitionEndDate { get; set; }

        public DateTime EnjoymentStartDate { get; set; }

        public DateTime EnjoymentEndDate { get; set; }

        public DateTime ReceiptDate { get; set; }

        public decimal Value { get; set; }

        public int EmployeeId { get; set; }
    }
}