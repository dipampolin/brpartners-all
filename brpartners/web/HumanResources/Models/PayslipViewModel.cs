﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tegra.HumanResources.Domain.Entities;

namespace QX3.Portal.WebSite.HumanResources.Models
{
    public class PayslipViewModel
    {
        public int Id { get; set; }

        public DateTime Date { get; set; }

        public string CompanyName { get; set; }

        public bool Successfully { get; set; }

        public int Year { get; set; }

        public int Month { get; set; }

        public string MonthName { get; set; }

    }
}