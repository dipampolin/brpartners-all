﻿using System.Data;

namespace QX3.Portal.WebSite.HumanResources.Models
{
    public class ShowReportViewModel
    {
        public DataTable Table { get; set; }

        public string Nome { get; set; }

        public int ReportId { get; set; }
    }
}