﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QX3.Portal.WebSite.HumanResources.Models
{
    public class NotificationViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Frequency { get; set; }

        public bool Active { get; set; }

        public string Query { get; set; }

        public List<NotificationItemViewModel> Items { get; set; }

        public List<NotificationDestinationViewModel> Destinations { get; set; }
    }
}