﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QX3.Portal.WebSite.HumanResources.Models
{
    public class CLIENTE
    {
        public CLIENTE()
        {
            CRK_COR_CLIENTE = new CRK_COR_CLIENTE();
            CRK_COR_PESSOAFISICA = new CRK_COR_PESSOAFISICA();
            CRK_COR_TIPOCADASTRO = new CRK_COR_TIPOCADASTRO();
            CRK_COR_CLIENTE_CONTA = new CRK_COR_CLIENTE_CONTA();
            ENDERECOS = new ENDERECOS();
        }

        public CRK_COR_CLIENTE CRK_COR_CLIENTE { get; set; }
        public CRK_COR_PESSOAFISICA CRK_COR_PESSOAFISICA { get; set; }
        public CRK_COR_TIPOCADASTRO CRK_COR_TIPOCADASTRO { get; set; }
        public CRK_COR_CLIENTE_CONTA CRK_COR_CLIENTE_CONTA { get; set; }
        public ENDERECOS ENDERECOS { get; set; }
    }

    public class CRK_COR_CLIENTE
    {
        public string ID_CLIENTE { get; set; }
        public string CD_CLIENTE { get; set; }
        public string NM_CLIENTE { get; set; }
        public string NM_ABREVIADO { get; set; }
        public string NM_APELIDO { get; set; }
        public string CD_TIPOCLIENTE { get; set; }
        public string IC_TIPOPESSOA { get; set; }
        public string IC_ESTRANGEIRO { get; set; }
        public string DS_CNPJ_CPF { get; set; }
        public string FL_INATIVO { get; set; }
        public string DS_OBS { get; set; }
        public string IC_CUSTODIA_INVEST { get; set; }
        public string DS_SITE { get; set; }
        public string DT_INCLUSAO { get; set; }
    }

    public class CRK_COR_TIPOCADASTRO
    {
        public string CD_TIPOCADASTRO { get; set; }
    }

    public class CRK_COR_PESSOAFISICA
    {
        public string ID_ESTADOCIVIL { get; set; }
        public string ID_REGIMECASAMENTO { get; set; }
        public string DT_NASCIMENTO { get; set; }
        public string IC_SEXO { get; set; }
        public string ID_GRUPOECONOMICO { get; set; }
        public string DS_CLASSIFICACAO { get; set; }
        public string ID_JURISDICAO { get; set; }
        public string CD_OPER_CVM { get; set; }
        public string TP_DOCTO { get; set; }
        public string DS_RG { get; set; }
        public string DS_ORGAOEMISSOR { get; set; }
        public string DT_EMISSAO_DOCTO { get; set; }
        public string SG_UF_RG { get; set; }

        // ...

        public string NM_PAIS { get; set; }
        public string SG_UF { get; set; }
        public string SG_UF_NACIONALIDADE { get; set; }
        public string NM_LOCALIDADE { get; set; }
        public string DS_TITULOELEITOR { get; set; }
        public string DS_ZONAELEITORAL { get; set; }
        public string NM_CONJUGE { get; set; }
        public string CPF_CONJUGE { get; set; }
        public string DT_NASCTO_CONJUGE { get; set; }
        public string NR_DEPENDENTE { get; set; }
        public string NM_PAI { get; set; }
        public string NM_MAE { get; set; }
        public string CD_PROFISSAO { get; set; }
        public string NM_EMPRESA_COM { get; set; }
        public string DT_ADMISSAO { get; set; }
        
        // ...
    }

    public class CRK_COR_CLIENTE_CONTA
    {
        public string ID_TIPOCONTA { get; set; }
        public string IC_CONTACORRENTE { get; set; }
        public string ID_EXTERNO { get; set; }
    }

    public class ENDERECOS
    {
       public ENDERECOS()
        {
            CRK_COR_CLIENTE_ENDERECO = new List<CRK_COR_CLIENTE_ENDERECO>();
            FONES = new FONES();
        }

       public List<CRK_COR_CLIENTE_ENDERECO> CRK_COR_CLIENTE_ENDERECO { get; set; }
       public FONES FONES { get; set; }
    }

    public class CRK_COR_CLIENTE_ENDERECO
    {
        public string ID_TIPOENDERECO { get; set; }
        public string DS_CEP { get; set; }
        public string FL_PRINCIPAL { get; set; }
        public string FL_ENDERECOCORRESPONDENCIA { get; set; }
        public string ID_TIPOLOGRADOURO { get; set; }
        public string NM_LOGRADOURO { get; set; }
        public string NR_LOGRADOURO { get; set; }
        public string DS_COMPLEMENTO { get; set; }
        public string NM_BAIRRO { get; set; }
        public string NM_MUNICIPIO { get; set; }
        public string SG_UF { get; set; }
        public string DS_PAIS { get; set; }
        public string ID_EXTERNO { get; set; }
    }

    public class FONES
    {
        public FONES()
        {
            CRK_COR_CLIENTE_TELEFONE = new List<CRK_COR_CLIENTE_TELEFONE>();
        }

        public List<CRK_COR_CLIENTE_TELEFONE> CRK_COR_CLIENTE_TELEFONE { get; set; }
    }

    public class CRK_COR_CLIENTE_TELEFONE
    {
        public string CD_TIPOTELEFONE { get; set; }
        public string NR_PREFIXO { get; set; }
        public string NR_TELEFONE { get; set; }
        public string DS_RAMAL { get; set; }
    }

}