﻿using System;
using System.ComponentModel.DataAnnotations;
using Tegra.HumanResources.Domain.Entities;

namespace QX3.Portal.WebSite.HumanResources.Models
{
    public class CourseEmployeeViewModel
    {
        public int? CourseId { get; set; }

        public int EmployeeId { get; set; }

        public DateTime? Deadline { get; set; }

        public string EmployeeName { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        [Required]
        public bool Required { get; set; }

        public CourseEmployee ToEntity()
        {
            return new CourseEmployee() { };
        }
    }
}