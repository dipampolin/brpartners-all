﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QX3.Portal.WebSite.HumanResources.Models
{
    public class EmployeeCourseViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int? CourseId { get; set; }

        public int EmployeeId { get; set; }

        public decimal? Value;

        public DateTime? Deadline { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public bool Required { get; set; }
    }
}