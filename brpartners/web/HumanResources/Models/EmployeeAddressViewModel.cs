﻿using QX3.Portal.WebSite.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Tegra.HumanResources.Domain.Entities;

namespace QX3.Portal.WebSite.HumanResources.Models
{

    public class EmployeeAddressViewModel
    {
        public int Id { get; set; }

        public int EmployeeId { get; set; }

        public int CityId { get; set; }

        public int StateId { get; set; }

        [Required(ErrorMessage = "Tipo obrigatório")]
        public string Type { get; set; }

        [Required(ErrorMessage = "Cep obrigatório")]
        public string Cep { get; set; }

        [Required(ErrorMessage = "Rua obrigatória")]
        public string Street { get; set; }

        [Required(ErrorMessage = "Número obrigatório")]
        public string Number { get; set; }

        public string Complement { get; set; }

        public string Neighborhood { get; set; }

        public bool Active { get; set; }

        public string State { get; set; }

        public string City { get; set; }

        public EmployeeAddress ToEntity()
        {
            return new EmployeeAddress()
            {
                Id = Id,
                EmployeeId = EmployeeId,
                CityId = CityId,
                Type = Type,
                CEP = Cep,
                Street = Street,
                Number = Number,
                Complement = Complement,
                Neighborhood = Neighborhood,
                Active = Active
            };
        }

    }

}