﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QX3.Portal.WebSite.HumanResources.Models
{
    public class SelectCompanyByReport
    {
        public string Code { get; set; }

        public string Value { get; set; }
    }
}