﻿using QX3.Portal.WebSite.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Tegra.HumanResources.Domain.Entities;

namespace QX3.Portal.WebSite.HumanResources.Models
{
    public class CourseViewModel
    {
        public int Id { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Nome do curso obrigatório")]
        public string Name { get; set; }

        public bool Active { get; set; }

        public List<CourseEmployeeViewModel> CourseEmployees { get; set; }

        public Course ToEntity()
        {
            if (this.CourseEmployees == null)
            {
                this.CourseEmployees = new List<CourseEmployeeViewModel>();
            }

            var entity = new Course(this.Name, this.Active)
            {
                Id = this.Id,
                CourseEmployees = this.CourseEmployees.Select(x => new CourseEmployee()
                {
                    Deadline = x.Deadline.Value,
                    EmployeeId = x.EmployeeId,
                    CourseId = x.CourseId,
                    Required = x.Required,
                    StartDate = x.StartDate ?? (DateTime?)null,
                    EndDate = x.EndDate ?? (DateTime?)null
                }).ToList()
            };
            return entity;
        }
    }


}