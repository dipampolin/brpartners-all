﻿using QX3.Portal.WebSite.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Tegra.HumanResources.Domain.Entities;

namespace QX3.Portal.WebSite.HumanResources.Models
{
    public class VehicleViewModel
    {
        public int Id { get; set; }

        [Required]
        public int EmployeeId { get; set; }

        public string Brand { get; set; }

        public string Model { get; set; }

        public string Color { get; set; }

        public string CarPlate { get; set; }

        [Required]
        public bool Active { get; set; }

        public EmployeeVehicle ToEntity()
        {
            return new EmployeeVehicle(this.EmployeeId, this.Brand, this.Model, this.Color, this.CarPlate, this.Active)
            {
                Id = this.Id
            };
        }
    }
}
