﻿using QX3.Portal.WebSite.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Tegra.HumanResources.Domain.Entities;

namespace QX3.Portal.WebSite.HumanResources.Models
{

    public class BankAccountViewModel
    {
        public int Id { get; set; }

        public int EmployeeId { get; set; }

        public bool Main { get; set; }

        // [Required(ErrorMessage = "Banco obrigatório")]
        public string Bank { get; set; }

        [Required(ErrorMessage = "Agência obrigatória")]
        public string Agency { get; set; }

        [Required(ErrorMessage = "Conta obrigatória")]
        public string Account { get; set; }

        public int BankId { get; set; }

        public EmployeeBankAccount ToEntity()
        {
            return new EmployeeBankAccount()
            {
                Id = Id,
                EmployeeId = EmployeeId,
                Account = Account,
                Agency = Agency,
                Bank = "",
                Main = Main,
                BankId = BankId
            };
        }

    }

}