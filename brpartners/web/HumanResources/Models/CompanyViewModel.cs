﻿using QX3.Portal.WebSite.Validators;
using System;
using System.ComponentModel.DataAnnotations;
using System.Web;
using Tegra.HumanResources.Domain.Entities;

namespace QX3.Portal.WebSite.HumanResources.Models
{
    public class CompanyViewModel
    {
        public int Id { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Razão social da empresa obrigatória")]
        public string Name { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "CNPJ Obrigatório")]
        [Cnpj(ErrorMessage = "CNPJ Inválido")]
        public string Cnpj { get; set; }

        [Required(ErrorMessage = "Código obrigatório")]
        [Range(1, Double.MaxValue, ErrorMessage = "Código deve ser um número positivo")]
        public string Code { get; set; }

        public Company ToEntity()
        {
            return new Company(this.Code, this.Name, this.Cnpj)
            {
                Id = this.Id
            };
        }
    }


}