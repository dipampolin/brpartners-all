﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Tegra.HumanResources.Domain;
using Tegra.HumanResources.Domain.Entities;

namespace QX3.Portal.WebSite.HumanResources.Models
{
    public class RoleViewModel
    {
        public int Id { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Cargo Obrigatório")]
        public string Name { get; set; }

        public bool Active { get; set; }

        [Format(Conversions.ConvertType.DATE)]
        public DateTime? CreatedDate { get; set; }


        public Role toEntity()
        {
            return new Role
            {
                Active = this.Active,
                CreatedDate = this.CreatedDate,
                Name = this.Name,
                Id = this.Id
            };
        }
    }
}