﻿using QX3.Portal.WebSite.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Tegra.HumanResources.Domain.Entities;

namespace QX3.Portal.WebSite.HumanResources.Models
{

    public class EmployeeContactViewModel
    {
        public int Id { get; set; }

        public int EmployeeId { get; set; }

        [Required(ErrorMessage = "Tipo obrigatório")]
        public string Type { get; set; }

        [Required(ErrorMessage = "Valor obrigatório")]
        public string Value { get; set; }

        public bool Active { get; set; }

        public EmployeeContact ToEntity()
        {
            return new EmployeeContact()
            {
                Id = Id,
                EmployeeId = EmployeeId,
                Type = Type,
                Active = Active,
                Value = Value
            };
        }

    }

}