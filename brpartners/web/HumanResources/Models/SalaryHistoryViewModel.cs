﻿using QX3.Portal.WebSite.HumanResources.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tegra.HumanResources.Domain.Entities;

namespace QX3.Portal.WebSite.HumanResources.Models
{
    public class SalaryHistoryViewModel
    {
        public int Id { get; set; }

        public string Description { get; set; }

        public string Category { get; set; }

        public string SalaryPeriod { get; set; }

        public string WorkLoad { get; set; }

        public decimal? Salary { get; set; }

        public decimal? ExtraHour { get; set; }

        public decimal? Gratification { get; set; }

        public decimal? Triennium { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int EmployeeId { get; set; }
    }
}