﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QX3.Portal.WebSite.HumanResources.Models
{
    public class EmployeeBenefitViewModel
    {
        public int Id { get; set; }

        public int BenefitId { get; set; }

        public int EmployeeId { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string Type { get; set; }

        public string Description { get; set; }

        public decimal? Value { get; set; }

        public string CardNumber { get; set; }
    }
}