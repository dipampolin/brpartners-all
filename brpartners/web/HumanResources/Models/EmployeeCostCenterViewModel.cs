﻿using QX3.Portal.WebSite.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Tegra.HumanResources.Domain.Entities;

namespace QX3.Portal.WebSite.HumanResources.Models
{

    public class EmployeeDepartmentViewModel
    {
        public int Id { get; set; }

        public int EmployeeId { get; set; }

        public int CostCenterId { get; set; }

        public string CostCenterName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool Active { get; set; }

        public EmployeeCostCenter ToEntity()
        {
            return new EmployeeCostCenter()
            {
                Id = Id,
                EmployeeId = EmployeeId,
                CostCenterId = CostCenterId,
                StartDate = StartDate,
                EndDate = EndDate
            };
        }

    }

}