﻿using System.ComponentModel.DataAnnotations;

namespace QX3.Portal.WebSite.HumanResources.Models
{
    public class NotificationDestinationViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string Query { get; set; }
    }
}