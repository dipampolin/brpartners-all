﻿using QX3.Portal.WebSite.Validators;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Repository;
using Tegra.HumanResources.Services;

namespace QX3.Portal.WebSite.HumanResources.Models
{

    [Serializable]
    public class EmployeeViewModel
    {

        public int Id { get; set; }

        public int CompanyId { get; set; }

        [Required(ErrorMessage = "Sigla obrigatório")]
        public string Initials { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Nome obrigatório")]
        public string Name { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "CPF obrigatório")]
        public string Cpf { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Genero obrigatório")]
        public string Gender { get; set; }

        [Required(ErrorMessage = "Estado civil obrigatório")]
        public int? CivilStatusId { get; set; }

        [Required(ErrorMessage = "Escolaridade obrigatório")]
        public int? EducationalStageId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Data de nascimento obrigatório")]
        public DateTime Birthdate { get; set; }

        public DateTime? AdmissionDate { get; set; }

        public string Registration { get; set; }

        public string IdentificationNumber { get; set; }

        public bool Foreigner { get; set; }

        public string Nationality { get; set; }

        public string MotherName { get; set; }

        public DateTime? MotherBirthdate { get; set; }

        public string FatherName { get; set; }

        public DateTime? FatherBirthdate { get; set; }

        public string MainPhone { get; set; }

        public string Picture { get; set; }

        public string Rg { get; set; }

        public string RgDispatcher { get; set; }

        public DateTime? RgDateExpedition { get; set; }

        public string Rne { get; set; }

        public string RneDispatcher { get; set; }

        public static explicit operator EmployeeViewModel(Type v)
        {
            throw new NotImplementedException();
        }

        public DateTime? RneDateExpedition { get; set; }

        public DateTime? RneDateExpiration { get; set; }

        public string VoterNumberCard { get; set; }

        public string VoterSession { get; set; }

        public string VoterZone { get; set; }

        public string EmploymentBooklet { get; set; }

        public string EmploymentBookletSerie { get; set; }

        public string EmploymentBookletUf { get; set; }

        public string Pis { get; set; }

        public string Passport { get; set; }

        public DateTime? PassportDateExpedition { get; set; }

        public DateTime? PassportDateExpiration { get; set; }

        public string PassportDispatcher { get; set; }

        public string PassportUf { get; set; }

        public int? PassportCityId { get; set; }

        public string Reservist { get; set; }

        public string ReservistCategory { get; set; }

        public bool Active { get; set; }

        public DateTime? ResignationDate { get; set; }

        public int? CostCenterId { get; set; }

        public int? SupervisorId { get; set; }

        public int? StateId { get; set; }

        public int? CityId { get; set; }

        public int RoleId { get; set; }

        public int RoleIdValue { get; set; }

        public EmployeeCostCenter CostCenter { get; set; }

        public int Percentage { get; set; }

        public string Cnh { get; set; }

        public string CnhCategory { get; set; }

        public DateTime? CnhDateExpedition { get; set; }

        public DateTime? CnhDateExpiration { get; set; }

        public DateTime? CnhDateFirst { get; set; }

        public bool Naturalized { get; set; }

        public string CountryOfBirth { get; set; }

        public string CityOfBirth { get; set; }

        private int ShowPercentage;

        public bool Clone { get; set; }

        public static Employee ToEntity(EmployeeViewModel model, CostCenter costCenter, Role role, Company company, bool copyForeignKeys = true)
        {
            var entity = new Employee(model.Initials, model.Name);

            entity.Id = model.Id;
            entity.Cpf = model.Cpf;
            entity.Gender = model.Gender;
            entity.CivilStatusId = model.CivilStatusId;
            entity.EducationalStageId = model.EducationalStageId;
            entity.Birthdate = model.Birthdate;
            entity.AdmissionDate = model.AdmissionDate;
            entity.Active = model.Active;
            entity.Initials = model.Initials;
            entity.CostCenterId = model.CostCenterId;
            entity.EmploymentBooklet = model.EmploymentBooklet;
            entity.EmploymentBookletSerie = model.EmploymentBookletSerie;
            entity.EmploymentBookletUf = model.EmploymentBookletUf;
            entity.FatherName = model.FatherName;
            entity.FatherBirthdate = GetFatherBirthdate(model);
            entity.Foreigner = model.Foreigner;
            entity.MainPhone = model.MainPhone;
            entity.Picture = model.Picture;
            entity.IdentificationNumber = model.IdentificationNumber;
            entity.MotherName = model.MotherName;
            entity.MotherBirthdate = GetMotherBirthdate(model);
            entity.Name = model.Name;
            entity.Nationality = model.Nationality;
            entity.Passport = model.Passport;
            entity.PassportDateExpedition = GetPassportDateExpedition(model);
            entity.PassportDateExpiration = GetPassportDateExpiration(model);
            entity.PassportDispatcher = model.PassportDispatcher;
            entity.PassportUf = model.PassportUf;
            entity.PassportCityId = model.PassportCityId;
            entity.Pis = model.Pis;
            entity.Registration = model.Registration;
            entity.Reservist = model.Reservist;
            entity.ReservistCategory = model.ReservistCategory;
            entity.ResignationDate = GetResignationDate(model);
            entity.Rg = model.Rg;
            entity.RgDateExpedition = GetRgDateExpedition(model);
            entity.RgDispatcher = model.RgDispatcher;
            entity.Rne = model.Rne;
            entity.RneDateExpedition = GetRneDateExpedition(model);
            entity.RneDateExpiration = GetRneDateExpiration(model);
            entity.RneDispatcher = model.RneDispatcher;
            entity.SupervisorId = model.SupervisorId;
            entity.VoterNumberCard = model.VoterNumberCard;
            entity.VoterSession = model.VoterSession;
            entity.VoterZone = model.VoterZone;
            entity.CityId = model.CityId;
            entity.Percentage = model.Percentage;
            entity.Cnh = model.Cnh;
            entity.CnhCategory = model.CnhCategory;
            entity.CnhDateExpedition = GetCnhDateExpedition(model);
            entity.CnhDateExpiration = GetCnhDateExpiration(model);
            entity.CnhDateFirst = GetCnhDateFirst(model);
            entity.Naturalized = model.Naturalized;
            entity.CountryOfBirth = model.CountryOfBirth;
            entity.CityOfBirth = model.CityOfBirth;

            if (entity.Id == 0 && copyForeignKeys)
            {
                //EmployeeCostCenter employeeDeparrtment = new EmployeeCostCenter();
                //employeeDeparrtment.CreatedDate = DateTime.Now;
                //employeeDeparrtment.StartDate = GetValue(model);
                //employeeDeparrtment.Employee = entity;
                //entity.EmployeeCostCenters.Add(employeeDeparrtment);
                //employeeDeparrtment.CostCenter = costCenter;

                EmployeeCompany employeeCompany = new EmployeeCompany();
                employeeCompany.CreatedDate = DateTime.Now;
                employeeCompany.StartDate = GetValue(model);
                employeeCompany.Employee = entity;
                entity.EmployeeCompanies = new Collection<EmployeeCompany>();
                entity.EmployeeCompanies.Add(employeeCompany);
                employeeCompany.Company = company;

                EmployeeRole employeeRole = new EmployeeRole();
                employeeRole.CreatedDate = DateTime.Now;
                employeeRole.StartDate = GetValue(model);
                employeeRole.Employee = entity;
                // employeeRole.RoleId = role.Id;
                // employeeRole.EmployeeId = model.Id;

                entity.EmployeeRoles = new Collection<EmployeeRole>();
                entity.EmployeeRoles.Add(employeeRole);
                employeeRole.Role = role;
            }


            return entity;
        }

        private static DateTime GetValue(EmployeeViewModel model)
        {
            if (model.AdmissionDate.HasValue)
            {
                return model.AdmissionDate.Value;
            }
            return new DateTime();
        }

        private static DateTime? GetRgDateExpedition(EmployeeViewModel model)
        {
            if (model.RgDateExpedition.HasValue)
            {
                return model.RgDateExpedition.Value;
            }
            return null;
        }

        private static DateTime? GetRneDateExpedition(EmployeeViewModel model)
        {
            if (model.RneDateExpedition.HasValue)
            {
                return model.RneDateExpedition.Value;
            }
            return null;
        }

        private static DateTime? GetRneDateExpiration(EmployeeViewModel model)
        {
            if (model.RneDateExpiration.HasValue)
            {
                return model.RneDateExpiration.Value;
            }
            return null;
        }

        private static DateTime? GetResignationDate(EmployeeViewModel model)
        {
            if (model.ResignationDate.HasValue)
            {
                return model.ResignationDate.Value;
            }
            return null;
        }

        private static DateTime? GetFatherBirthdate(EmployeeViewModel model)
        {
            if (model.FatherBirthdate.HasValue)
                return model.FatherBirthdate.Value;

            return null;
        }

        private static DateTime? GetMotherBirthdate(EmployeeViewModel model)
        {
            if (model.MotherBirthdate.HasValue)
                return model.MotherBirthdate.Value;

            return null;
        }

        private static DateTime? GetPassportDateExpedition(EmployeeViewModel model)
        {
            if (model.PassportDateExpedition.HasValue)
                return model.PassportDateExpedition.Value;

            return null;
        }

        private static DateTime? GetPassportDateExpiration(EmployeeViewModel model)
        {
            if (model.PassportDateExpiration.HasValue)
                return model.PassportDateExpiration.Value;

            return null;
        }

        private static DateTime? GetCnhDateExpedition(EmployeeViewModel model)
        {
            if (model.CnhDateExpedition.HasValue)
                return model.CnhDateExpedition.Value;

            return null;
        }

        private static DateTime? GetCnhDateExpiration(EmployeeViewModel model)
        {
            if (model.CnhDateExpiration.HasValue)
                return model.CnhDateExpiration.Value;

            return null;
        }

        private static DateTime? GetCnhDateFirst(EmployeeViewModel model)
        {
            if (model.CnhDateFirst.HasValue)
                return model.CnhDateFirst.Value;

            return null;
        }
    }

}