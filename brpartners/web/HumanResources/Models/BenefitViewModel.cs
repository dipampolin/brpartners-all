﻿using System.ComponentModel.DataAnnotations;
using Tegra.HumanResources.Domain.Entities;

namespace QX3.Portal.WebSite.HumanResources.Models
{
    public class BenefitViewModel
    {
        public int Id { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Tipo obrigatório")]
        public int BenefitTypeId { get; set; }

        public string Type { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public decimal Value { get; set; }

        public Benefit ToEntity()
        {
            return new Benefit(this.BenefitTypeId, this.Description, this.Value)
            {
                Id = this.Id,
                BenefitTypeId = this.BenefitTypeId,
                Description = this.Description,
                Value = this.Value
            };
        }
    }


}