﻿using QX3.Portal.WebSite.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Tegra.HumanResources.Domain.Entities;

namespace QX3.Portal.WebSite.HumanResources.Models
{

    public class EmployeeTypeViewModel
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public string TypeId { get; set; }
        public string TypeName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool Active { get; set; }

        public EmployeeType ToEntity()
        {
            return new EmployeeType()
            {
                Id = Id,
                EmployeeId = EmployeeId,
                TypeId = Convert.ToInt32(TypeId),
                StartDate = StartDate,
                EndDate = EndDate
            };
        }

    }

}