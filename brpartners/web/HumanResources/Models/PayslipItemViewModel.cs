﻿namespace QX3.Portal.WebSite.HumanResources.Models
{
    public class PayslipItemViewModel
    {
        public int IdPayslipItem { get; set; }

        public int Rubric { get; set; }

        public string RubricDescription { get; set; }

        public string RubricType { get; set; }

        public decimal Reference { get; set; }

        public decimal Value { get; set; }
    }
}