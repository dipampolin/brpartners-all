﻿using QX3.Portal.WebSite.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Tegra.HumanResources.Domain.Entities;

namespace QX3.Portal.WebSite.HumanResources.Models
{

    public class EmployeeRelationshipViewModel
    {
        public int Id { get; set; }

        public int EmployeeId { get; set; }

        public string Relationship { get; set; }

        public string Name { get; set; }

        public string Cpf { get; set; }

        public bool UseHealthPlan { get; set; }

        public bool UseOdontologyPlan { get; set; }

        public Nullable<int> OdontologyPlanId { get; set; }

        public Nullable<int> HealthPlanId { get; set; }
        
        public string HealthPlan { get; set; }

        public string OdontologyPlan { get; set; }

        public bool IR { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public DateTime? Birthdate { get; set; }

        public EmployeeRelationship ToEntity()
        {
            return new EmployeeRelationship()
            {
                Id = Id,
                EmployeeId = EmployeeId,
                Cpf = Cpf,
                EndDate = EndDate,
                IR = IR,
                Name = Name,
                OdontologyPlanId = OdontologyPlanId,
                HealthPlanId =  HealthPlanId,
                Relationship = Relationship,
                StartDate = StartDate,
                UseHealthPlan = UseHealthPlan,
                UseOdontologyPlan = UseOdontologyPlan,
                Birthdate = Birthdate
            };
        }

    }

}