﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tegra.HumanResources.Domain.Entities;

namespace QX3.Portal.WebSite.HumanResources.Models
{
    public class BenefitTypeViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }


        public BenefitType toEntity()
        {
            return new BenefitType
            {
                Id = this.Id,
                Name = this.Name
            };
        }

    }
}