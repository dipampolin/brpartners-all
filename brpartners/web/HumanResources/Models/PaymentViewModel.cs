﻿using System;
using System.Collections.Generic;

namespace QX3.Portal.WebSite.HumanResources.Models
{
    public class PaymentViewModel
    {
        public int IdPayslip { get; set; }

        public DateTime Date { get; set; }

        public int CompanyCode { get; set; }

        public string CompanyName { get; set; }

        public int CostCenterCode { get; set; }

        public string CostCenterDescription { get; set; }

        public int RoleCode { get; set; }

        public string RoleDescription { get; set; }

        public List<PayslipItemViewModel> Items { get; set; }

        public int IdCustomer { get; set; }

        public int Code { get; set; }

        public string Name { get; set; }
    }
}