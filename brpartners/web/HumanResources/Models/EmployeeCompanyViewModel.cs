﻿using QX3.Portal.WebSite.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Tegra.HumanResources.Domain.Entities;

namespace QX3.Portal.WebSite.HumanResources.Models
{

    public class EmployeeCompanyViewModel
    {
        public int Id { get; set; }

        public int EmployeeId { get; set; }

        public int CompanyId { get; set; }

        public string CompanyName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool Active { get; set; }

        public EmployeeCompany ToEntity()
        {
            return new EmployeeCompany()
            {
                Id = Id,
                EmployeeId = EmployeeId,
                CompanyId = CompanyId,
                StartDate = StartDate,
                EndDate = EndDate
            };
        }

    }

}