﻿using QX3.Portal.WebSite.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Tegra.HumanResources.Domain.Entities;

namespace QX3.Portal.WebSite.HumanResources.Models
{

    public class EmployeeRoleViewModel
    {
        public int Id { get; set; }

        public int EmployeeId { get; set; }

        public int RoleId { get; set; }

        public string RoleName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool Active { get; set; }

        public EmployeeRole ToEntity()
        {
            return new EmployeeRole()
            {
                Id = Id,
                EmployeeId = EmployeeId,
                RoleId = RoleId,
                StartDate = StartDate,
                EndDate = EndDate
            };
        }

    }

}