﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tegra.HumanResources.Domain.Entities;

namespace QX3.Portal.WebSite.HumanResources.Models
{
    public class CostCenterViewModel
    {
        public int Id { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public CostCenter ToEntity()
        {
            return new CostCenter
            {
                Code = this.Code,
                Id = this.Id,
                Name = this.Name
            };
        }
    }
}