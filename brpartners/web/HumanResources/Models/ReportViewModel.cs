﻿using System.Collections.Generic;

namespace QX3.Portal.WebSite.HumanResources.Models
{
    public class ReportViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public bool Active { get; set; }

        public string Query { get; set; }

        public virtual List<ReportItemViewModel> Items { get; set; }
    }
}
