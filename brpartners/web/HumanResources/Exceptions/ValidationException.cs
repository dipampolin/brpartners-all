﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QX3.Portal.WebSite.HumanResources.Exceptions
{
    public class ValidationException : Exception
    {

        public ModelStateDictionary ModelState { get; set; }


        public ValidationException(ModelStateDictionary modelState)
        {
            this.ModelState = modelState;
        }
    }
}