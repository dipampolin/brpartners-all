﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace QX3.Portal.WebSite.HumanResources.Helper
{
    public static class DESCrypto
    {
        /// <summary>
        /// Criptografar String
        /// </summary>
        /// <param name="originalString">String original</param>
        /// <param name="password">Chave de criptografia</param>
        /// <returns>String criptografada</returns>
        public static string Encrypt(string originalString, string password)
        {
            var bytes = ASCIIEncoding.ASCII.GetBytes(password);

            if (String.IsNullOrEmpty(originalString))
            {
                throw new ArgumentNullException
                       ("A string que precisa ser criptografado não pode ser nulo.");
            }
            DESCryptoServiceProvider cryptoProvider = new DESCryptoServiceProvider();
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream,
                cryptoProvider.CreateEncryptor(bytes, bytes), CryptoStreamMode.Write);
            StreamWriter writer = new StreamWriter(cryptoStream);
            writer.Write(originalString);
            writer.Flush();
            cryptoStream.FlushFinalBlock();
            writer.Flush();
            return Convert.ToBase64String(memoryStream.GetBuffer(), 0, (int)memoryStream.Length);
        }


        /// <summary>
        ///  Descriptografar uma string encriptada.
        /// </summary>
        /// <param name="cryptedString">String encriptada</param>
        /// <param name="password">Chave de criptografia</param>
        /// <returns>String descodificada</returns>
        public static string Decrypt(string cryptedString, string password)
        {
            var bytes = ASCIIEncoding.ASCII.GetBytes(password);

            if (String.IsNullOrEmpty(cryptedString))
            {
                throw new ArgumentNullException
                   ("A string que precisa ser decifrado não pode ser nulo.");
            }
            DESCryptoServiceProvider cryptoProvider = new DESCryptoServiceProvider();
            MemoryStream memoryStream = new MemoryStream
                    (Convert.FromBase64String(cryptedString));
            CryptoStream cryptoStream = new CryptoStream(memoryStream,
                cryptoProvider.CreateDecryptor(bytes, bytes), CryptoStreamMode.Read);
            StreamReader reader = new StreamReader(cryptoStream);
            return reader.ReadToEnd();
        }
    }
}