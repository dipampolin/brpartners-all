﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tegra.HumanResources.Domain;

namespace QX3.Portal.WebSite.HumanResources.Helper
{

    public static class HtmlHelperExtension
    {

        private static Conversions conversions;

        public static string Convert(
            this HtmlHelper html,
            string columnName,
            object value
            )
        {

            if (value == null || value.GetType() == typeof(DBNull))
            {
                return "";
            }

            if (conversions == null)
            {
                conversions = new Conversions();
            }

            var attribute = conversions.GetByAttribute(columnName);

            if (attribute == null)
            {
                return value.ToString();
            }

            return attribute.GetExpression().Compile()(value);
        }

        public static string Header(
            this HtmlHelper html,
            string columnName
            )
        {

            ////return columnName.Split('-')[1];
            return columnName;
        }

        public static string Convert(
            string columnName,
            object value
            )
        {

            if (value == null || value.GetType() == typeof(DBNull))
            {
                return "";
            }

            if (conversions == null)
            {
                conversions = new Conversions();
            }

            var attribute = conversions.GetByAttribute(columnName);

            if (attribute == null)
            {
                return value.ToString();
            }

            return attribute.GetExpression().Compile()(value);
        }

        public static string Header(
            string columnName
            )
        {

            ////return columnName.Split('-')[1];
            return columnName;
        }

    }
}