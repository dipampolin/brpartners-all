﻿(function (angular) {
    'use strict'

    angular.module('brp')
        .controller("PayslipListCtrl", ['$scope', 'toastr', 'PayslipService', function ($scope, toastr, PayslipService) {
            var $ctrl = this;

            $ctrl.years = [0,1,2,3,4,5,6,7,8,9].map(function (item) {
                return (new Date()).getFullYear() - item;
            });

            $ctrl.months = [
                { id: 1, name: "Janeiro" },
                { id: 2, name: "Fevereiro" },
                { id: 3, name: "Março" },
                { id: 4, name: "Abril" },
                { id: 5, name: "Maio" },
                { id: 6, name: "Junho" },
                { id: 7, name: "Julho" },
                { id: 8, name: "Agosto" },
                { id: 9, name: "Setembro" },
                { id: 10, name: "Outubro" },
                { id: 11, name: "Novembro" },
                { id: 12, name: "Dezembro" },
            ];

            this.loadData = function (state) {
                $ctrl.isLoading = true;

                PayslipService.list(state).then(function (result) {
                    $ctrl.data = result.data;
                    state.pagination.numberOfPages = result.pages;
                    $ctrl.isLoading = false;
                });
            };

            this.remove = function (item) {
                PayslipService.remove(item).then(function () {
                    toastr.success('Holerite excluído com sucesso');
                    $ctrl.refresh();
                }).catch(function () {
                    toastr.error('Falha ao excluir holerite!');
                });
            };

        }])
        .controller("PayslipCreateCtrl", ['$uibModalInstance', 'PayslipService', 'data', function ($uibModalInstance, PayslipService, data) {
            var $ctrl = this;

            this.data = data || {};

            $ctrl.ok = function () {
                PayslipService.save($ctrl.data).then($uibModalInstance.close);
            };

            $ctrl.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        }]);
})(window.angular)