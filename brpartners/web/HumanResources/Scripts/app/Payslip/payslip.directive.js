﻿
(function (angular) {
    'use strict';

    angular.module('brp')

    .directive("payslipCreate", ['$uibModal', 'PayslipService', 'ModalService', function ($uibModal, PayslipService, ModalService) {
        
        return {
            restrict: "A",
            scope: {
                refreshCallback: "&"
            },
            link: function (scope, elem, attrs) {
                
                angular.element(elem).on('click', function (event) {
                    event.preventDefault();

                    ModalService.show({
                        id: attrs.payslipCreate,
                        view: '/HumanResources/Static/Payslip/payslipImport.html',
                        controller: 'PayslipCreateCtrl',
                        service: 'PayslipService',
                        size: 'lg',
                        cb: scope.refreshCallback()
                    });
                    
                });

            }
        }

    }]);

})(window.angular);
