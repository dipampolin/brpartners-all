﻿(function (angular) {
    'use strict';

    angular.module('brp')

        .factory("BankService", ['$http', 'PaginateRequest', function ($http, PaginateRequest) {

        var list = function () {
            return $http({
                method: 'GET',
                url: '/RH/Bank/List'
            }).then(function (res) {
                return res.data;
            });
        };

        var listCombo = function () {
            return $http({
                method: 'GET',
                url: '/RH/Bank/ListCombo'
            }).then(function (res) {
                return res.data;
            });
        };

        var listAll = function () {
            return $http({
                method: 'GET',
                url: '/RH/Bank/ListAll'
            }).then(function (res) {
                return res.data;
            });
        };

        var getActiveEmployeeBank = function (id) {
            return $http({
                method: 'GET',
                url: '/RH/Bank/GetActiveEmployeeBank?id=' + id
            }).then(function (res) {
                return res.data;
            });
        };

        var paginateList = function (state) {
            return PaginateRequest.get('/RH/Bank/ListAll', state, 'Name')
                .then(function (res) {
                    return res.data;
                });
        };

        var save = function (role) {
            return $http.post('/RH/Bank/Save', role)
                .then(function (res) {
                    return res.data;
                });
        };

        var findById = function (id) {
            return $http({
                method: 'GET',
                url: '/RH/Bank/Find/' + id
            }).then(function (res) {
                return res.data;
            });
        }

        var findCache = {};

        var findByName = function (name) {
            if (findCache[name]) {
                return findCache[name];
            }

            findCache[name] = $http({
                method: 'GET',
                url: '/RH/Bank/FindByName?name=' + name
            }).then(function (res) {
                return res.data;
            });

            return findCache[name];
        }

        var remove = function (id) {
            return $http.get('/RH/Bank/Remove/' + id)
                    .then(function (res) {
                        return res.data;
                    });
        }

        return {
            list: list,
            listCombo: listCombo,
            listAll: listAll,
            paginateList: paginateList,
            getActiveEmployeeBank: getActiveEmployeeBank,
            save: save,
            findById: findById,
            findByName: findByName,
            remove: remove
        }

    }]);

})(window.angular);