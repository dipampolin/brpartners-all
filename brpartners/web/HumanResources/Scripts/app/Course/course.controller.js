﻿
(function (angular) {
    'use strict';
    angular.module('brp')
        .controller("CourseIndexCtrl", ['$scope', 'CourseService', 'toastr', function ($scope, CourseService, toastr) {
        var $ctrl = this;

        this.loadData = function (state) {
            $ctrl.isLoading = true;
            CourseService.list(state).then(function (result) {
                $ctrl.data = result.data;
                state.pagination.numberOfPages = result.pages;
                $ctrl.isLoading = false;
            });
        }

        this.remove = function (id) {
            CourseService.remove(id)
                .then(function (result) {
                    $ctrl.refresh();
                    toastr.success('Curso excluído com sucesso.');
                })
                .catch(function (err) {
                    if (err.status == 409) {
                        toastr.warning('Curso vinculado à um Funcionário!');
                    } else {
                        toastr.error('Falha ao excluir Curso!');
                    }
                });
        }

    }])
    .controller("CourseCreateCtrl", ['$uibModalInstance', '$uibModal', 'moment', 'CourseService', 'data', 'toastr', function ($uibModalInstance, $uibModal, moment, CourseService, data, toastr) {

        var $ctrl = this;
        this.data = data || {
            Id: undefined,
            Name: undefined,
            Active: undefined,
            CourseEmployees: []
        };

        if (!$ctrl.data.Id) {
            $ctrl.data.Active = true;
        }

        $ctrl.ok = function () {
            var data = {
                Id: $ctrl.data.Id,
                Name: $ctrl.data.Name,
                Active: !!$ctrl.data.Active,
                CourseEmployees: $ctrl.data.CourseEmployees.map(function (e) {
                    var employee = angular.copy(e);
                    employee.EmployeeId = e.Id || employee.EmployeeId;
                    employee.CourseId = $ctrl.data.Id;
                    employee.Deadline = moment(e.Deadline).format('YYYY-MM-DD');
                    return employee;
                })

            }

            CourseService.save(data).then(function (result) {
                toastr.success('Curso salvo com sucesso.');
                $uibModalInstance.close();
            }).catch(function (err) {
                toastr.error('Falha ao salvar Curso!');
            });
        };

        $ctrl.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        $ctrl.remove = function (index) {
            this.data.CourseEmployees.splice(index, 1);
        };

        $ctrl.addEmployees = function () {
            var modalInstance = $uibModal.open({
                templateUrl: '/HumanResources/Static/Course/selectEmployee.html',
                controller: 'CourseAddEmployeeCtrl',
                controllerAs: '$ctrl'
            });

            modalInstance.result.then(function (data) {
                $ctrl.data.CourseEmployees = $ctrl.data.CourseEmployees || [];
                data.employees.map(function (employee) {
                    var item = {
                        Id: employee.Id,
                        EmployeeName: employee.Name,
                        Required: !!data.Required,
                        Deadline: data.Deadline
                    }
                    $ctrl.data.CourseEmployees.push(item);
                })
            });
        }
    }])

    .controller("CourseAddEmployeeCtrl", ['$uibModalInstance', 'EmployeeService', function ($uibModalInstance, EmployeeService) {
        var $ctrl = this;
        this.data = {
            employees: []
        };

        this.getEmployees = function (val) {
            return EmployeeService.findByName(val);
        };

        this.isValid = function () {
            return typeof $ctrl.selected == 'object';
        };

        this.add = function () {
            var exists = this.data.employees.filter(function (item) {
                return item.Id == $ctrl.selected.Id
            }).length > 0;
            if (!exists) {
                this.data.employees.push($ctrl.selected);
            }
            $ctrl.selected = null;
        };

        this.remove = function (index) {
            this.data.employees.splice(index, 1);
        };

        $ctrl.ok = function () {
            $uibModalInstance.close($ctrl.data);
        };

        $ctrl.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }]);
})(window.angular);
