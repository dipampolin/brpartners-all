﻿
(function (angular, moment) {
    'use strict';

    angular.module('moment', [])
        .constant('moment', moment);

    angular.module('cep', [])
        .constant('cep', cep);

    angular.module('brp', ['moment', 'cep', 'ngAnimate', 'toastr', 'ngMessages', 'smart-table', 'mdFormValidator', 'ui.utils.masks', 'ui.bootstrap', 'dualmultiselect', 'angular.filter', 'ui.mask', 'ngFileUpload', 'chart.js', 'dndLists', 'ngSanitize'])
    .config(['$httpProvider', function ($httpProvider) {

        var disconnected = false;

        $httpProvider.interceptors.push(['$q', '$injector', function ($q, $injector) {
            return {
                responseError: function (response) {

                    if (disconnected) {
                        return $q.reject(response);
                    }
                    
                    if (response.status == 403) {
                        disconnected = true;

                        var TegraAlerts = $injector.get("TegraAlerts");

                        return TegraAlerts.alert(response.data.Error).then(function () {
                            top.location.href = "/";
                            return $q.reject(response);
                        });
                        
                    }

                    if (response.status >= 400) {
                        return $q.reject(response);
                    }

                    return response;
                }
            };
        }]);

        $httpProvider.interceptors.push(['$q', '$injector', function ($q, $injector) {
            var parseField = function (obj, field) {
                var value = obj[field];
                if (value && value.toString().search("^/Date\\(-?[0-9]+\\)/$") == 0) {
                    var date = moment(value);
                    obj[field] = date.isValid() ? date.toDate() : undefined;
                }
            };

            var parseDateFields = function (obj) {
                for (var field in obj) {
                    parseField(obj, field);
                }
            };

            return {
                response: function(response) {
                    var dataArray = [].concat(response && response.data);
                    for (var i = 0; i  < dataArray.length; i++) {
                        parseDateFields(dataArray[i]);
                    }
                    return response;
                }
            };
        }]);

    }]).run(function () {

    });


})(window.angular, window.moment);

