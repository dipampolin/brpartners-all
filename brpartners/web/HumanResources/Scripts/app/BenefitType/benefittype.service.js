﻿(function (angular) {
    'use strict';

    angular.module('brp')

        .factory("BenefitTypeService", ['$http', function ($http) {

            var listAll = function () {
                return $http({
                    method: 'GET',
                    url: '/RH/BenefitType/ListAll'
                }).then(function (res) {
                    return res.data;
                });
            };

            return {
                listAll: listAll
            }

        }]);

})(window.angular);