﻿(function (angular) {
    'use strict';

    angular.module('brp')

    .factory("CompanyService", ['$http', 'PaginateRequest', function ($http, PaginateRequest) {

        var list = function (state) {
            return PaginateRequest.get('/RH/Company/List', state, 'Name')
                .then(function (res) {
                    return res.data;
                });
        };

        var listAll = function () {
            return $http.get('/RH/Company/ListAll').then(function (res) {
                return res.data;
            });
        };

        var findById = function (id) {
            return $http({
                method: 'GET',
                url: '/RH/Company/Find/' + id
            }).then(function (res) {
                return res.data;
            });
        };

        var findReport = function (id) {
            return $http({
                method: 'GET',
                url: '/RH/Company/ListByReport/'
            }).then(function (res) {
                return res.data;
            });
        };
        
        var save = function (data) {
            return $http.post('/RH/Company/Save', data).then(function (res) {
                return res.data;
            });
        };

        var findCache = {};

        var findByName = function (name, id) {
            if (findCache[name]) {
                return findCache[name];
            }

            findCache[name] = $http({
                method: 'GET',
                url: '/RH/Company/FindByName?name=' + name
            }).then(function (res) {
                return res.data;
            });

            return findCache[name];
        }

        var remove = function (id) {
            return $http.get('/RH/Company/Remove/' + id)
                .then(function (res) {
                    return res.data;
                });
        }

        return {
            list: list,
            listAll: listAll,
            findById: findById,
            save: save,
            findByName: findByName,
            findReport: findReport,
            remove: remove
        };
    }]);

})(window.angular);