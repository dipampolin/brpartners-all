﻿
(function (angular) {
    'use strict';

    angular.module('brp')

    .directive("companyCreate", ['$uibModal', 'CompanyService', 'ModalService', function ($uibModal, CompanyService, ModalService) {

        return {
            restrict: "A",
            scope: {
                refreshCallback: "&"
            },
            link: function (scope, elem, attrs) {

                angular.element(elem).on('click', function (event) {
                    event.preventDefault();

                    ModalService.show({
                        id: attrs.companyCreate,
                        view: '/HumanResources/Static/Company/companyCreate.html',
                        controller: 'CompanyCreateCtrl',
                        service: 'CompanyService',
                        cb: scope.refreshCallback()
                    });
                });

            }
        }

    }]);

})(window.angular);
