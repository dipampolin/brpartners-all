﻿
(function (angular, employee) {
    'use strict';

    angular.module('brp')

        .controller("EmployeeEditCtrl", ['$scope', 'EmployeeService', function ($scope, EmployeeService) {

            var $ctrl = this;

            $ctrl.employee = employee;

            $ctrl.src = '/RH/Employee/GetPictureEmployee/' + $ctrl.employee.Id;

        }]);

})(window.angular, window.employee);
