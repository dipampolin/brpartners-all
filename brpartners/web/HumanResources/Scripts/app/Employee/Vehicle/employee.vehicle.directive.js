﻿
(function (angular) {
    'use strict';

    angular.module('brp')

    .directive("vehicleCreate", ['$uibModal', 'ModalService', function ($uibModal, ModalService) {
        return {
            restrict: "A",
            scope: {
                refreshCallback: "&",
                employeeId: "="
            },
            link: function (scope, elem, attrs) {

                angular.element(elem).on('click', function (event) {
                    event.preventDefault();
                    ModalService.show({
                        id: attrs.vehicleCreate,
                        view: '/HumanResources/Static/Employee/Vehicle/vehicleCreate.html',
                        controller: 'EmployeeVehicleCreateCtrl',
                        service: 'EmployeeVehicleService',
                        resolve: {
                            employeeId: scope.employeeId
                        },
                        cb: scope.refreshCallback()
                    });

                });
            }
        }
    }]);

})(window.angular);