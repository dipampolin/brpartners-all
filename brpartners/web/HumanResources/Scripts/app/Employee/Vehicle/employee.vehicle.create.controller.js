﻿
(function (angular) {
    'use strict';

    angular.module('brp')

    .controller("EmployeeVehicleCreateCtrl", ['$uibModalInstance', 'EmployeeVehicleService', 'data', 'employeeId', 'toastr', function ($uibModalInstance, EmployeeVehicleService, data, employeeId, toastr) {
        var $ctrl = this;

        this.data = data || { Model: null, Color: null, CarPlate: null, Brand: null, Active: true};
        this.data.EmployeeId = this.data.EmployeeId || employeeId;

        $ctrl.ok = function () {
            if ($ctrl.data.CarPlate) $ctrl.data.CarPlate = $ctrl.data.CarPlate.replace('-', '');
            EmployeeVehicleService.create($ctrl.data).then(function () {
                toastr.success('Veículo salvo com sucesso.');
                $uibModalInstance.close();
            }).catch(function (err) {
                console.log(err);
                toastr.error('Falha ao salvar o Veículo!');
            });
        };

        $ctrl.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        this.isRequired = function () {
            var elementsForm = angular.element("#vehicleCreate").find('input[type=text], select');

            for (var x = 0; x < elementsForm.length; x++) {
                var element = elementsForm[x];
                if (element.value)
                    return false;
            }

            return true;
        }
      
    }]);
})(window.angular);
