﻿(function (angular) {
    'use strict';

    angular.module('brp')
    .filter('carplate', function () {
        return function (val) {
            if (!val) {
                return "";
            }
            return val.toUpperCase().replace(/^([A-Z]{3})(\d{4})$/gi, '$1-$2');
        }
    });

})(window.angular);
