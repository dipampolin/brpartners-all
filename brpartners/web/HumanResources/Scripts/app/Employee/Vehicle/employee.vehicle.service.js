﻿(function (angular) {
    'use strict';

    angular.module('brp')

    .factory("EmployeeVehicleService", ['$http', 'PaginateRequest', function ($http, PaginateRequest) {

        var create = function (data) {
            return $http.post('/RH/Vehicle/Save', data)
                .then(function (res) {
                    return res.data;
                });
        }

        var list = function (id) {
            return $http.get('/RH/Vehicle/List', {
                params: {
                    employeeId: id
                }
            }).then(function (res) {
                return res.data;
            });
        }

        var findById = function (id) {
            return $http.get('/RH/Vehicle/Find', {
                params: {
                    id: id
                }
            }).then(function (res) {
                return res.data;
            });
        }

        var remove = function (id) {
            return $http.get('/RH/Vehicle/Remove', {
                params: {
                    id: id
                }
            }).then(function (res) {
                return res.data;
            });
        }

        return {
            create: create,
            list: list,
            findById: findById,
            remove: remove
        }
    }]);

})(window.angular);