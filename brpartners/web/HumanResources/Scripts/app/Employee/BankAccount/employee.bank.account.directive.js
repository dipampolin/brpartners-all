﻿
(function (angular) {
    'use strict';

    angular.module('brp')
    .directive("bankAccountCreate", ['$uibModal', 'ModalService', function ($uibModal, ModalService) {
        return {
            restrict: "A",
            scope: {
                refreshCallback: "&",
                employeeId: "="
            },
            link: function (scope, elem, attrs) {
                
                angular.element(elem).on('click', function (event) {
                    event.preventDefault();
                    ModalService.show({
                        id: attrs.bankAccountCreate,
                        view: '/HumanResources/Static/Employee/BankAccount/bankAccountCreate.html',
                        controller: 'EmployeeBankAccountCreateCtrl',
                        resolve: {
                            employeeId: scope.employeeId
                        },
                        service: 'EmployeeBankAccountService',
                        cb: scope.refreshCallback()
                    });

                });
            }
        }
    }]);

})(window.angular);