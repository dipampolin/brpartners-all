﻿
(function (angular) {
    'use strict';

    angular.module('brp')
    .controller("EmployeeBankAccountCreateCtrl", ['$uibModalInstance', 'EmployeeBankAccountService', 'BankService', 'data', 'employeeId', 'toastr', function ($uibModalInstance, EmployeeBankAccountService, BankService, data, employeeId, toastr) {
        var $ctrl = this;

        this.data = data || {};
        this.data.EmployeeId = this.data.EmployeeId || employeeId;

        if (this.data.BankId) {
            BankService.findById($ctrl.data.BankId).then(function (result) {
                $ctrl.selectedBank = result;
            });
        }

        BankService.listCombo().then(function (result) {
            $ctrl.banks = result;
        });

        $ctrl.onSelectBank = function ($item, $model, $label) {
            console.log(123);
            $ctrl.data.BankId = $model.Id;
        };

        this.getAutoCompleteBank = function (val) {
            return BankService.findByName(val);
        };

        $ctrl.ok = function () {
            EmployeeBankAccountService.create($ctrl.data).then(function (result) {
                toastr.success('Conta bancária salva com sucesso.');
                return $uibModalInstance.close(data);
            }).catch(function (err) {
                // toastr.error('Falha ao salvar a Conta bancária!');
                // Hack devido a erro em PRD. A informação é salva mas a API retorna erro.
                toastr.success('Conta bancária salva com sucesso.');
                return $uibModalInstance.close(data);
            });
        };

        $ctrl.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }]);

})(window.angular);
