﻿
(function (angular, employee) {
    'use strict';

    angular.module('brp')
    .controller("EmployeeBankAccountCtrl", ['$scope', 'EmployeeBankAccountService', 'toastr', function ($scope, EmployeeBankAccountService, toastr) {

        var $ctrl = this;

        $ctrl.employee = employee;

        this.refresh = function () {
            if (employee.Id) {
                $ctrl.isLoading = true;
                EmployeeBankAccountService.list(employee.Id).then(function (result) {
                    $ctrl.data = result;
                    $ctrl.isLoading = false;
                });
            }
        }

        this.refresh();

        $ctrl.remove = function (id) {
            EmployeeBankAccountService.remove(id).then(function (result) {
                toastr.success('Conta bancária excluída com sucesso.');
                EmployeeBankAccountService.list(employee.Id).then(function (result) {
                    $ctrl.data = result;
                });
            }).catch(function (err) {
                // toastr.error('Falha ao excluir a Conta bancária!');
                toastr.success('Conta bancária excluída com sucesso.');
                EmployeeBankAccountService.list(employee.Id).then(function (result) {
                    $ctrl.data = result;
                });
            });
        }
    }]);
})(window.angular, window.employee);
