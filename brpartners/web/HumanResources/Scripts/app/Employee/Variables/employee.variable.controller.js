﻿(function (angular, employee) {
    'use strict';

    angular.module('brp')

    .controller("EmployeeVariablesCtrl", ['$scope', 'EmployeeService', 'EmployeeVariablesService', 'toastr', function ($scope, EmployeeService, EmployeeVariablesService, toastr) {

        var $ctrl = this;
        $ctrl.employee = employee;

        this.loadData = function () {
            $ctrl.isLoading = true;
            if (employee.Id) {
                EmployeeVariablesService.list(employee.Id).then(function (result) {
                    $ctrl.data = result;
                    $ctrl.isLoading = false;
                });
            }
        }
        this.loadData();

        $ctrl.remove = function (id) {
            EmployeeVariablesService.remove(id).then(function (result) {
                toastr.success('Variável excluída com sucesso.');
                EmployeeVariablesService.list(employee.Id).then(function (result) {
                    $ctrl.data = result;
                });
            }).catch(function (err) {
                toastr.error('Falha ao excluir Variável!');
            });
        }
    }]);
})(window.angular, window.employee);
