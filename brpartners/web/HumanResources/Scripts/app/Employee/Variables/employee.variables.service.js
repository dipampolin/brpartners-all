﻿(function (angular) {
    'use strict';

    angular.module('brp')

    .factory("EmployeeVariablesService", ['$http', 'PaginateRequest', function ($http, PaginateRequest) {
        
        var list = function (employeeId) {
            return $http({
                method: 'GET',
                url: '/RH/EmployeeVariables/List',
                params: { EmployeeId: employeeId }
            }).then(function (res) {
                return res.data;
            });
        };

        var create = function (data) {
            return $http.post('/RH/EmployeeVariables/Create', data).then(function (res) {
                return res.data;
            });
        }

        var remove = function (id) {
            return $http.get('/RH/EmployeeVariables/Remove', {
                params: {
                    id: id
                }
            }).then(function (res) {
                return res.data;
            });
        }

        var findById = function (id) {
            return $http.get('/RH/EmployeeVariables/Find', {
                params: {
                    id: id
                }
            }).then(function (res) {
                return res.data;
            });
        }

        return {
            list: list,
            create: create,
            findById: findById,
            remove: remove
        }
    }]);

})(window.angular);