﻿(function (angular) {
    'use strict';

    angular.module('brp')

    .directive("salaryCreate", ['$uibModal', 'EmployeeSalaryService', 'ModalService', function ($uibModal, EmployeeSalaryService, ModalService) {

        return {
            restrict: "A",
            scope: {
                refreshCallback: "&"
            },
            link: function (scope, elem, attrs) {
                angular.element(elem).on('click', function (event) {
                    event.preventDefault();
                    ModalService.show({
                        id: attrs.salaryCreate,
                        view: '/HumanResources/Static/Employee/Salary/salaryCreate.html',
                        controller: 'EmployeeSalaryCreateCtrl',
                        service: 'EmployeeSalaryService',
                        cb: scope.refreshCallback()
                    });

                });
            }
        }
    }]);

})(window.angular);
