﻿(function (angular, employee) {
    'use strict';

    angular.module('brp')

    .controller("EmployeeSalaryCreateCtrl", ['$uibModalInstance', 'EmployeeSalaryService', 'data', 'toastr', function ($uibModalInstance, EmployeeSalaryService, data, toastr) {
        var $ctrl = this;

        this.data = data || {};

        $ctrl.ok = function () {

            $ctrl.data.EmployeeId = employee.Id || undefined;

            EmployeeSalaryService.create($ctrl.data).then(function (result) {
                toastr.success('Linha de Salário salvo com sucesso.');
                $uibModalInstance.close();
            }).catch(function (err) {
                toastr.error('Falha ao salvar Linha de Salário!');
            });
        };

        $ctrl.periodSalaries = EmployeeSalaryService.getPeriodSalaries();

        $ctrl.categorySalaries = EmployeeSalaryService.getCategorySalaries();

        $ctrl.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        this.isRequired = function () {
            var elementsForm = angular.element("#salaryCreate").find('input[type=text], select');

            for (var x = 0; x < elementsForm.length; x++) {
                var element = elementsForm[x];
                if (element.value)
                    return false;
            }

            return true;
        }

    }]);

})(window.angular, window.employee);
