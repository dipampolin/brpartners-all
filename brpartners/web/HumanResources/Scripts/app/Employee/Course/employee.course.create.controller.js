﻿
(function (angular, employee) {
    'use strict';

    angular.module('brp')

    .controller("EmployeeCourseCreateCtrl", ['$uibModalInstance', 'EmployeeCourseService', 'data', 'CourseService', 'toastr', function ($uibModalInstance, EmployeeCourseService, data, CourseService, toastr) {
        var $ctrl = this;

        this.data = data || {};

        this.data.Deadline

        CourseService.listAll().then(function (courses) {
            $ctrl.employeeCourses = courses;
        });

        $ctrl.ok = function () {
            $ctrl.data.EmployeeId = employee.Id;
            EmployeeCourseService.create($ctrl.data).then(function (result) {
                toastr.success('Curso salvo com sucesso.');
                $uibModalInstance.close();
            }).catch(function (err) {
                toastr.error('Falha ao salvar Curso!');
            });
        };

        $ctrl.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }]);

})(window.angular, window.employee);
