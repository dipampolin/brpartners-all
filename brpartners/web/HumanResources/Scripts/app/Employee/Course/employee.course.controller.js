﻿
(function (angular, employee) {
    'use strict';

    angular.module('brp')

    .controller("EmployeeCourseCtrl", ['$scope', 'EmployeeService', 'EmployeeCourseService', 'toastr', function ($scope, EmployeeService, EmployeeCourseService, toastr) {

        var $ctrl = this;

        $ctrl.employee = employee;

        this.loadData = function () {
            if (employee.Id) {
                $ctrl.isLoading = true;
                EmployeeCourseService.list(employee.Id).then(function (courses) {
                    $ctrl.courses = courses;
                    $ctrl.isLoading = false;
                });
            }
        }
        this.loadData();

        $ctrl.remove = function (id) {
            EmployeeCourseService.remove(id).then(function (result) {
                EmployeeCourseService.list(employee.Id).then(function (data) {
                    toastr.success('Curso excluído com sucesso.');
                    $ctrl.courses = data;
                });
            }).catch(function (err) {
                toastr.error('Falha ao excluir Curso!');
            });
        }
    }]);
})(window.angular, window.employee);
