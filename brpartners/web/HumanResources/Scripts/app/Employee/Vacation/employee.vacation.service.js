﻿(function (angular) {
    'use strict';

    angular.module('brp')

    .factory("EmployeeVacationService", ['$http', function ($http) {

        var list = function (employeeId) {
            return $http({
                method: 'GET',
                url: '/RH/EmployeeVacation/list',
                params: { EmployeeId: employeeId }
            }).then(function (res) {
                return res.data;
            });
        };

        var create = function (data) {
            return $http.post('/RH/EmployeeVacation/Create', data).then(function (res) {
                return res.data;
            });
        }

        var findById = function (id) {
            return $http.get('/RH/EmployeeVacation/Find', {
                params: {
                    id: id
                }
            }).then(function (res) {
                return res.data;
            });
        }

        var remove = function (id) {
            return $http.get('/RH/EmployeeVacation/Remove', {
                params: {
                    id: id
                }
            }).then(function (res) {
                return res.data;
            });
        }

        return {
            list: list,
            create: create,
            findById: findById,
            remove: remove
        }
    }]);

})(window.angular);