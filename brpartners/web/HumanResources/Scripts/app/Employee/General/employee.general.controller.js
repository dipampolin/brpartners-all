﻿
(function (angular, employee) {
    'use strict';

    angular.module('brp')
    .controller("EmployeeGeneralCtrl", ['$scope', '$timeout', 'EmployeeGeneralService', 'CostCenterService', 'CompanyService', 'RoleService', 'CivilStatusService', 'EducationalStageService', 'EmployeeAddressService', 'toastr', 'EmployeeService', 'EmployeeCompanyService', 'CityService', Controller]);


    function Controller($scope, $timeout, EmployeeGeneralService, CostCenterService, CompanyService, RoleService, CivilStatusService, EducationalStageService, EmployeeAddressService, toastr, EmployeeService, EmployeeCompanyService, CityService) {

        var $ctrl = this;

        this.data = employee;
        this.employee = employee;

        if (!this.data.Id) {
            this.data.Foreigner = false;
        }

        $scope.$watch("$ctrl.data.Birthdate", function (value) {
            var ageDifMs = Date.now() - new Date(value);
            var ageDate = new Date(ageDifMs);
            var age = Math.abs(ageDate.getUTCFullYear() - 1970);
            if (!isNaN(parseFloat(age)) && isFinite(age)) {
                $ctrl.data.Age = age;
            }
            else {
                $ctrl.data.Age = null;
            }
        });

        if (this.data.Id) {
            this.data.Birthdate = moment(this.data.Birthdate).toDate();
            this.data.RgDateExpedition = moment(this.data.RgDateExpedition).toDate();
            this.data.ResignationDate = moment(this.data.ResignationDate).toDate();
            this.data.AdmissionDate = moment(this.data.AdmissionDate).toDate();

            console.log(this.data);
        }

        $ctrl.clearField = function (item) {
            if (item === true) {
                $ctrl.data.Rg = undefined;
                $ctrl.data.RgDispatcher = undefined;
                $ctrl.data.RgDateExpedition = undefined;

                $ctrl.data.Naturalized = undefined;
                $ctrl.data.CountryOfBirth = undefined;
                $ctrl.data.CityOfBirth = undefined;
            }

            if (item === false) {
                $ctrl.data.Rne = undefined;
                $ctrl.data.RneDispatcher = undefined;
                $ctrl.data.RneDateExpedition = undefined;
                $ctrl.data.RneDateExpiration = undefined;
                $ctrl.data.Nationality = undefined;

                $ctrl.data.Naturalized = undefined;
                $ctrl.data.CountryOfBirth = undefined;
                $ctrl.data.CityOfBirth = undefined;
            }
        }

        $ctrl.clearNaturalized = function (item) {
            if (item === true) {
                $ctrl.data.StateId = undefined;
                $ctrl.selectedCity = undefined;
                $ctrl.data.CountryOfBirth = undefined;
                $ctrl.data.CityOfBirth = undefined;
            }

            if (item === false) {
                $ctrl.data.StateId = undefined;
                $ctrl.selectedCity = undefined;
                $ctrl.data.CountryOfBirth = undefined;
                $ctrl.data.CityOfBirth = undefined;
            }
        }

        EducationalStageService.list().then(function (result) {
            $ctrl.educationalStages = result;
        });

        CivilStatusService.list().then(function (result) {
            $ctrl.civilStatuses = result;
        });


        CompanyService.listAll().then(function (result) {
            $ctrl.companies = result;
        });

        CostCenterService.list().then(function (result) {
            $ctrl.departments = result;
        });

        RoleService.listAll().then(function (result) {
            console.log(result);
            $ctrl.roles = result;
        });

        CostCenterService.list().then(function (result) {
            $ctrl.costCenters = result;
        });

        $ctrl.foreigners = [{ Id: true, Value: "Sim" }, { Id: false, Value: "Não" }];

        $ctrl.genders = [{ Id: "Masculino", Value: "Masculino" }, { Id: "Feminino", Value: "Feminino" }];

        $ctrl.ufs = EmployeeAddressService.GetAllUfs();

        this.getEmployees = function (val) {
            return EmployeeService.findByName(val, $ctrl.data.Id || 0);
        };
        
        if (this.data.SupervisorId) {
            EmployeeService.findById($ctrl.data.SupervisorId).then(function (result) {
                $ctrl.selected = result;
            });
        }
        
        this.onSelect = function ($item, $model, $label) {
            $ctrl.data.SupervisorId = $model.Id;
        }

        $scope.$watch('$ctrl.data.RneDateExpedition', function (val) {
            if (!val) {
                $ctrl.data.RneDateExpedition = undefined;
            }
        });

        $scope.$watch('$ctrl.data.FatherBirthdate', function (val) {
            if (!val) {
                $ctrl.data.FatherBirthdate = undefined;
            }
        });

        $scope.$watch('$ctrl.data.MotherBirthdate', function (val) {
            if (!val) {
                $ctrl.data.MotherBirthdate = undefined;
            }
        });

        $scope.$watch('$ctrl.data.PassportDateExpedition', function (val) {
            if (!val) {
                $ctrl.data.PassportDateExpedition = undefined;
            }
        });

        $scope.$watch('$ctrl.data.PassportDateExpiration', function (val) {
            if (!val) {
                $ctrl.data.PassportDateExpiration = undefined;
            }
        });

        $scope.$watch('$ctrl.data.CnhDateExpedition', function (val) {
            if (!val) {
                $ctrl.data.CnhDateExpedition = undefined;
            }
        });

        $scope.$watch('$ctrl.data.CnhDateExpiration', function (val) {
            if (!val) {
                $ctrl.data.CnhDateExpiration = undefined;
            }
        });

        $scope.$watch('$ctrl.data.CnhDateFirst', function (val) {
            if (!val) {
                $ctrl.data.CnhDateFirst = undefined;
            }
        });

        this.getCities = function (val) {
            return CityService.findByName(val, $ctrl.data.StateId || 0);
        };

        if (this.data.CityId) {
            CityService.findById($ctrl.data.CityId).then(function (result) {
                $ctrl.selectedCity = result;
            });
        }

        this.onSelectCity = function ($item, $model, $label) {

            console.log($item);
            console.log($model);
            console.log($label);

            $ctrl.data.CityId = $model.Id;
        };

        if (this.data.PassportCityId) {
            CityService.findById($ctrl.data.PassportCityId).then(function (result) {
                $ctrl.selectedPassportCity = result;
            });
        }

        this.onSelectPassportCity = function ($item, $model, $label) {

            $ctrl.data.PassportCityId = $model.Id;
        };

        $ctrl.onSelectRole = function (roleId) {
            console.log("selected");
            console.log(roleId);
            $ctrl.data.RoleIdValue = roleId;
        }

        function InitialValues() {
            if (!employee.Id) {
                $ctrl.data.Active = true;
            }
            else {
                if ($ctrl.data.RneDateExpiration) {
                    $ctrl.data.RneDateExpiration = moment($ctrl.data.RneDateExpiration).toDate();
                }

                if ($ctrl.data.RneDateExpedition) {
                    $ctrl.data.RneDateExpedition = moment($ctrl.data.RneDateExpedition).toDate();
                }

                
                if ($ctrl.data.FatherBirthdate) {
                    $ctrl.data.FatherBirthdate = moment($ctrl.data.FatherBirthdate).toDate();
                }

                if ($ctrl.data.MotherBirthdate) {
                    $ctrl.data.MotherBirthdate = moment($ctrl.data.MotherBirthdate).toDate();
                }
                
                if ($ctrl.data.PassportDateExpedition) {
                    $ctrl.data.PassportDateExpedition = moment($ctrl.data.PassportDateExpedition).toDate();
                }

                if ($ctrl.data.PassportDateExpiration) {
                    $ctrl.data.PassportDateExpiration = moment($ctrl.data.PassportDateExpiration).toDate();
                }

                if ($ctrl.data.CnhDateExpedition) {
                    $ctrl.data.CnhDateExpedition = moment($ctrl.data.CnhDateExpedition).toDate();
                }

                if ($ctrl.data.CnhDateExpiration) {
                    $ctrl.data.CnhDateExpiration = moment($ctrl.data.CnhDateExpiration).toDate();
                }

                if ($ctrl.data.CnhDateFirst) {
                    $ctrl.data.CnhDateFirst = moment($ctrl.data.CnhDateFirst).toDate();
                }

                EmployeeCompanyService.getActiveEmployeeCompany(employee.Id).then(function (result) {
                    if (result != "") {
                        employee.CompanyId = result.CompanyId;
                        $ctrl.data.CompanyId = employee.CompanyId;
                    }
                    else {
                        $ctrl.data.CompanyId = null;
                    }
                });

                CostCenterService.getActiveEmployeeCostCenter(employee.Id).then(function (result) {
                    if (result != "") {
                        employee.CostCenterId = result.CostCenter.Id;
                        $ctrl.data.CostCenterId = employee.CostCenterId;
                    }
                });

                RoleService.getActiveEmployeeRole(employee.Id).then(function (result) {
                    if (result != "") {
                        employee.RoleId = result.Role.Id;
                        $ctrl.data.RoleId = employee.RoleId;
                    }
                    else {
                        $ctrl.data.RoleId = null;
                    }
                });
            }

            if ($ctrl.data.ResignationDate == "Invalid Date") {
                $ctrl.data.ResignationDate = undefined;
            }
            
        }

        InitialValues();

        $ctrl.ok = function () {

            var roleId = $ctrl.data.RoleIdValue == 0 ? $ctrl.data.RoleId : $ctrl.data.RoleIdValue;
            console.log(roleId);
            console.log($ctrl.data);

            var companyId = $ctrl.data.CompanyId;

            EmployeeGeneralService.create($ctrl.data).then(function (result) {
                if (result.Id && !$ctrl.employee.Id) {
                    window.location.href += '/' + result.Id;
                }
                delete result.Data.CompanyId;

                EmployeeService.findById(result.Id).then(function (data) {
                    data.Picture = result.Picture
                    angular.extend($ctrl.employee, data);

                    $ctrl.data.CompanyId = companyId;
                    $ctrl.data.RoleId = roleId;
                })

                toastr.success('Funcionário salvo com sucesso!');
            }).catch(function (err) {
                console.log(err);
                toastr.error('Falha ao salvar o Funcionário.');
            });
        }

        $ctrl.IsActive = function (active) {

            $ctrl.data.ResignationDate = active ? '' : $ctrl.data.ResignationDate;
            $ctrl.data.Clone = false;

            if (!active) {
                var ok = window.confirm('Gostaria de criar um novo cadastro com os dados do usuário inativado?');
                if (ok) {
                    $ctrl.data.Clone = true;
                } 
            }
        }

        $ctrl.RemovePhoto = function () {
            console.log($ctrl.data.Picture);

            $ctrl.data.Picture = '/RH/Employee/GetPictureEmployee/0';

            console.log($ctrl.data.Picture);
        }

    }
})(window.angular, window.employee);
