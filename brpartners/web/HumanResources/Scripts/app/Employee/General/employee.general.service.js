﻿(function (angular) {
    'use strict';

    angular.module('brp')

    .factory("EmployeeGeneralService", ['$http', function ($http) {

        
        var create = function (data) {
            return $http.post('/RH/Employee/CreatePost', data).then(function (res) {
                return res.data;
            });
        }

        return {
            create: create
        }

    }]);

})(window.angular);