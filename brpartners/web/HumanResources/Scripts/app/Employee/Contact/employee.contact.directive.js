﻿
(function (angular) {
    'use strict';

    angular.module('brp')

    .directive("contactCreate", ['$uibModal', 'EmployeeContactService', 'ModalService', function ($uibModal, EmployeeContactService, ModalService) {

        return {
            restrict: "A",
            scope: {
                refreshCallback: "&",
                employeeId: "="
            },
            link: function (scope, elem, attrs) {
                angular.element(elem).on('click', function (event) {
                    event.preventDefault();

                    ModalService.show({
                        id: attrs.contactCreate,
                        view: '/HumanResources/Static/Employee/Contact/contactCreate.html',
                        controller: 'EmployeeContactCreateCtrl',
                        service: 'EmployeeContactService',
                        resolve: {
                            employeeId: scope.employeeId
                        },
                        cb: scope.refreshCallback()
                    });
                });
            }
        }
    }]);

})(window.angular);
