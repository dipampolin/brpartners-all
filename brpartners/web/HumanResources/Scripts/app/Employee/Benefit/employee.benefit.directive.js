﻿(function (angular) {
    'use strict';

    angular.module('brp')

    .directive("employeeBenefitCreate", ['$uibModal', 'EmployeeBenefitService', 'ModalService', function ($uibModal, EmployeeBenefitService, ModalService) {

        return {
            restrict: "A",
            scope: {
                refreshCallback: "&"
            },
            link: function (scope, elem, attrs) {
                angular.element(elem).on('click', function (event) {
                    event.preventDefault();

                    ModalService.show({
                        id: attrs.employeeBenefitCreate,
                        view: '/HumanResources/Static/Employee/Benefit/employeeBenefitCreate.html',
                        controller: 'EmployeeBenefitCreateCtrl',
                        service: 'EmployeeBenefitService',
                        cb: scope.refreshCallback
                    });
                });
            }
        }
    }]);

})(window.angular);