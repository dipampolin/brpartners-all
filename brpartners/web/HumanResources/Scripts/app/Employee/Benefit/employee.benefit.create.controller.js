﻿
(function (angular, employee) {
    'use strict';

    angular.module('brp')

    .controller("EmployeeBenefitCreateCtrl", ['$uibModalInstance', 'EmployeeBenefitService', 'BenefitService', 'data', 'toastr', function ($uibModalInstance, EmployeeBenefitService, BenefitService, data, toastr) {
        var $ctrl = this;

        $ctrl.showCardNumber = false;

        $ctrl.onSelectBenefit = function (clear) {

            if ($ctrl.Benefits !== undefined && data) {

                var benefit = $ctrl.Benefits.filter(function (response) {
                    return response.Id === $ctrl.data.BenefitId;
                });

                benefit = benefit[0];
                console.log("onSelectBenefit", benefit);

                $ctrl.showCardNumber = benefit.Type === 'Cartão Corporativo' || benefit.Type === 'Cartão Coorporativo';
                if (clear) {
                    $ctrl.data.CardNumber = null;
                }
            }
        }

        $ctrl.ok = function () {

            $ctrl.data = {
                Id: $ctrl.data.Id || null,
                BenefitId: $ctrl.data.BenefitId || null,
                StartDate: $ctrl.data.StartDate || null,
                EndDate: $ctrl.data.EndDate || null,
                EmployeeId: employee.Id || null,
                CustomValue: $ctrl.data.CustomValue || null,
                CardNumber: $ctrl.data.CardNumber || null
            }

            EmployeeBenefitService.create($ctrl.data).then(function (result) {
                toastr.success('Benefício salvo com sucesso.');
                $uibModalInstance.close($ctrl.data);
            }).catch(function (err) {
                toastr.error('Falha ao salvar Benefício!');
            });
        };

        BenefitService.listAll().then(function (result) {
            $ctrl.Benefits = result;
            $ctrl.onSelectBenefit(false);
        });

        $ctrl.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        this.data = data || {};
        if (data) {
            this.data.StartDate = moment(this.data.StartDate).toDate();
            if (this.data.EndDate) {
                this.data.EndDate = moment(this.data.EndDate).toDate();
            }

            if (this.data.CardNumber !== undefined && this.data.CardNumber !== null && this.data.CardNumber !== '') {
                this.data.CardNumber = this.data.CardNumber.replace('Número: ', '');
            }

            // $ctrl.onSelectBenefit(false);
        }

    }]);

})(window.angular, window.employee);
