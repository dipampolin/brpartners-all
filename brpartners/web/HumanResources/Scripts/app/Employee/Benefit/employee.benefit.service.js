﻿(function (angular) {
    'use strict';

    angular.module('brp')

    .factory("EmployeeBenefitService", ['$http', 'PaginateRequest', function ($http, PaginateRequest) {

        var list = function (id) {
            return $http.get('/RH/EmployeeBenefit/List', {
                params: {
                    employeeId: id
                }
            }).then(function (res) {
                return res.data;
            });
        }

        var create = function (data) {
            return $http.post('/RH/EmployeeBenefit/Create', data)
                .then(function (res) {
                    return res.data;
                });
        };

        var remove = function (id) {
            return $http.get('/RH/EmployeeBenefit/Remove', {
                params: {
                    id: id
                }
            }).then(function (res) {
                return res.data;
            });
        };
        
        var findById = function (id) {
            return $http.get('/RH/EmployeeBenefit/Find', {
                params: {
                    id: id
                }
            }).then(function (res) {
                return res.data;
            });
        };

        return {
            list: list,
            create: create,
            findById: findById,
            remove: remove
        };
    }]);

})(window.angular);