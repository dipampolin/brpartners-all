﻿(function (angular) {
    'use strict';

    angular.module('brp')

    .factory("EmployeeService", ['$http', 'PaginateRequest', function ($http, PaginateRequest) {

        var list = function (state) {
            return PaginateRequest.get('/RH/Employee/List', state, 'Name')
                .then(function (res) {
                    return res.data;
                });
        };

        var findById = function (id) {
            return $http({
                method: 'GET',
                url: '/RH/Employee/Find/' + id
            }).then(function (res) {
                return res.data;
            });
        }

        var findCache = {};

        var findByName = function (name, id) {
            if (findCache[name]) {
                return findCache[name];
            }

            findCache[name] = $http({
                method: 'GET',
                url: '/RH/Employee/FindByName?name=' + name + '&' + "id=" + id
            }).then(function (res) {
                return res.data;
            });

            return findCache[name];
        }

        var create = function (data) {
            return $http.post('/RH/Employee/CreatePost', data).then(function (res) {
                return res.data;
            });
        }

        var remove = function (id) {
            return $http.get('/RH/Employee/Remove/' + id)
                .then(function (res) {
                    return res.data;
                });
        }

        return {
            list: list,
            findById: findById,
            findByName: findByName,
            create: create,
            remove: remove
        }

    }]);

})(window.angular);