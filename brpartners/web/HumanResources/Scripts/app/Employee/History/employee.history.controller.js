﻿
(function (angular, employee) {
    'use strict';
    angular.module('brp')
    .controller("EmployeeHistoryCtrl", ['$scope', 'EmployeeService', function ($scope, EmployeeService) {

        var $ctrl = this;
    }])
    .controller("EmployeeHistoryCompanyCtrl", ['$scope', 'CompanyService', 'EmployeeHistoryCompanyService', 'toastr', function ($scope, CompanyService, EmployeeHistoryService, toastr) {
        var $ctrl = this;

        $ctrl.employee = employee;

        this.refresh = function () {
            if (employee.Id) {
                $ctrl.isLoading = true;
                EmployeeHistoryService.list(employee.Id).then(function (result) {
                    $ctrl.data = result;
                    $ctrl.isLoading = false;
                });
            }
        }
        this.refresh();

        $ctrl.remove = function (id) {
            EmployeeHistoryService.remove(id).then(function (result) {
                toastr.success('Empresa excluída com sucesso.');
                EmployeeHistoryService.list(employee.Id).then(function (data) {
                    $ctrl.data = data;
                });
            }).catch(function (err) {
                toastr.error('Falha ao excluir Empresa!');
            });
        }
    }])
    .controller("EmployeeHistoryCostCenterCtrl", ['$scope', 'EmployeeHistoryCostCenterService', 'toastr', function ($scope, EmployeeHistoryService, toastr) {
        var $ctrl = this;

        $ctrl.employee = employee;

        this.refresh = function () {
            if (employee.Id) {
                $ctrl.isLoading = true;
                EmployeeHistoryService.list(employee.Id).then(function (result) {
                    $ctrl.data = result;
                    $ctrl.isLoading = false;
                });
            }
        }
        this.refresh();

        $ctrl.remove = function (id) {
            EmployeeHistoryService.remove(id).then(function (result) {
                toastr.success('Departamento excluído com sucesso.');
                EmployeeHistoryService.list(employee.Id).then(function (data) {
                    $ctrl.data = data;
                });
            }).catch(function (err) {
                toastr.error('Falha ao excluir Departamento!');
            });
        }
    }])
    .controller("EmployeeHistoryRoleCtrl", ['$scope', 'RoleService', 'EmployeeHistoryRoleService', 'toastr', function ($scope, RoleService, EmployeeHistoryService, toastr) {
        var $ctrl = this;

        $ctrl.employee = employee;

        this.refresh = function () {
            if (employee.Id) {
                $ctrl.isLoading = true;
                EmployeeHistoryService.list(employee.Id).then(function (result) {
                    $ctrl.data = result;
                    $ctrl.isLoading = false;
                });
            }
        }
        this.refresh();

        $ctrl.remove = function (id) {
            EmployeeHistoryService.remove(id).then(function (result) {
                toastr.success('Cargo excluído com sucesso.');
                EmployeeHistoryService.list(employee.Id).then(function (data) {
                    $ctrl.data = data;
                });
            }).catch(function (err) {
                toastr.error('Falha ao excluir Cargo!');
            });
        }
      
    }])
    .controller("EmployeeHistoryTypeCtrl", ['$scope', 'RoleService', 'EmployeeHistoryTypeService', 'toastr', function ($scope, RoleService, EmployeeHistoryService, toastr) {
            var $ctrl = this;

            $ctrl.employee = employee;

            this.refresh = function () {
                if (employee.Id) {
                    $ctrl.isLoading = true;
                    EmployeeHistoryService.list(employee.Id).then(function (result) {
                        $ctrl.data = result;
                        $ctrl.isLoading = false;
                    });
                }
            }
            this.refresh();

            $ctrl.remove = function (id) {
                EmployeeHistoryService.remove(id).then(function (result) {
                    toastr.success('Cargo excluído com sucesso.');
                    EmployeeHistoryService.list(employee.Id).then(function (data) {
                        $ctrl.data = data;
                    });
                }).catch(function (err) {
                    toastr.error('Falha ao excluir Cargo!');
                });
            }

        }]);
})(window.angular, window.employee);
