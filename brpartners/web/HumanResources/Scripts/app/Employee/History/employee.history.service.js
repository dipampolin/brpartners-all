﻿(function (angular) {
    'use strict';

    angular.module('brp')
    .factory("EmployeeHistoryCompanyService", ['$http', function ($http) {

        var company = {
            create : function (data) {
                return $http.post('/RH/EmployeeCompany/CreatePost', data)
                    .then(function (res) {
                        return res.data;
                    });
            },
            list : function(id) {
                return $http.get('/RH/EmployeeCompany/List', {
                    params: {
                        employeeId: id
                    }
                }).then(function (result) {
                    return result.data;
                });
            },
            remove : function(id) {
                return $http.get('/RH/EmployeeCompany/Remove', {
                    params: {
                        id: id
                    }
                }).then(function (res) {
                    return res.data;
                });
            },
            findById : function(id) {
                return $http.get('/RH/EmployeeCompany/Find', {
                    params: {
                        id: id
                    }
                }).then(function (result) {
                    return result.data;
                });
            }
        };

        return company;
    }]).factory("EmployeeHistoryCostCenterService", ['$http', function ($http) {

        var department = {
            create: function (data) {
                return $http.post('/RH/EmployeeCostCenter/CreatePost', data)
                    .then(function (res) {
                        return res.data;
                    });
            },
            list : function(id) {
                return $http.get('/RH/EmployeeCostCenter/List', {
                    params: {
                        employeeId: id
                    }
                }).then(function (result) {
                    return result.data;
                });
            },
            remove : function(id) {
                return $http.get('/RH/EmployeeCostCenter/Remove', {
                    params: {
                        id: id
                    }
                }).then(function (res) {
                    return res.data;
                });
            },
            findById : function(id) {
                return $http.get('/RH/EmployeeCostCenter/Find', {
                    params: {
                        id: id
                    }
                }).then(function (result) {
                    return result.data;
                });
            }
        };
        return department;
    }]).factory("EmployeeHistoryRoleService", ['$http', function ($http) {
        var role = {
            create: function (data) {
                return $http.post('/RH/EmployeeRole/CreatePost', data)
                    .then(function (res) {
                        return res.data;
                    });
            },
            list: function (id) {
                return $http.get('/RH/EmployeeRole/List', {
                    params: {
                        employeeId: id
                    }
                }).then(function (result) {
                    return result.data;
                });
            },
            remove : function (id) {
                return $http.get('/RH/EmployeeRole/Remove', {
                    params: {
                        id: id
                    }
                }).then(function (res) {
                    return res.data;
                });
            },
            findById : function (id) {
                return $http.get('/RH/EmployeeRole/Find', {
                    params: {
                        id: id
                    }
                }).then(function (result) {
                    return result.data;
                });
            }
        };

        return role;
    }])
    .factory("EmployeeHistoryTypeService", ['$http', function ($http) {
        var role = {
            create: function (data) {
                return $http.post('/RH/EmployeeType/CreatePost', data)
                    .then(function (res) {
                        return res.data;
                    });
            },
            list: function (id) {
                return $http.get('/RH/EmployeeType/List', {
                    params: {
                        employeeId: id
                    }
                }).then(function (result) {
                    return result.data;
                });
            },
            remove: function (id) {
                return $http.get('/RH/EmployeeType/Remove', {
                    params: {
                        id: id
                    }
                }).then(function (res) {
                    return res.data;
                });
            },
            findById: function (id) {
                return $http.get('/RH/EmployeeType/Find', {
                    params: {
                        id: id
                    }
                }).then(function (result) {
                    return result.data;
                });
            },
            getOptions: function () {
                return [{ Id: "1", Value: "Pró-Labore" }, { Id: "2", Value: "Estagiário" }, { Id: "3", Value: "Aprendiz" }, { Id: "4", Value: "Funcionário" }, { Id: "5", Value: "Prestador de Serviço" }];
            }
        };

        return role;
    }]);

})(window.angular);