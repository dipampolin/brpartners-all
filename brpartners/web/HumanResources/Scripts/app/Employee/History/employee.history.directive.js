﻿(function (angular) {
    'use strict';

    angular.module('brp')

    .directive("employeeCompanyCreate", ['$uibModal', 'ModalService', function ($uibModal, ModalService) {
        return {
            restrict: "A",
            scope: {
                refreshCallback: "&",
                employeeId: '='
            },
            link: function (scope, elem, attrs) {
                angular.element(elem).on('click', function (event) {
                    event.preventDefault();
                    ModalService.show({
                        id: attrs.employeeCompanyCreate,
                        view: '/HumanResources/Static/Employee/History/employeeCompanyCreate.html',
                        controller: 'EmployeeHistoryCompanyCreateCtrl',
                        cb: scope.refreshCallback(),
                        service: 'EmployeeHistoryCompanyService',
                        resolve: {
                            employeeId: scope.employeeId
                        },
                    });

                });
            }
        }
    }])
    .directive("employeeCostCenterCreate", ['$uibModal', 'ModalService', function ($uibModal, ModalService) {
        return {
            restrict: "A",
            scope: {
                refreshCallback: "&",
                employeeId: '='
            },
            link: function (scope, elem, attrs) {
                angular.element(elem).on('click', function (event) {
                    event.preventDefault();
                    ModalService.show({
                        id: attrs.employeeCostCenterCreate,
                        view: '/HumanResources/Static/Employee/History/employeeCostCenterCreate.html',
                        controller: 'EmployeeHistoryCostCenterCreateCtrl',
                        cb: scope.refreshCallback(),
                        service: 'EmployeeHistoryCostCenterService',
                        resolve: {
                            employeeId: scope.employeeId
                        },
                    });

                });
            }
        }
    }])
    .directive("employeeRoleCreate", ['$uibModal', 'ModalService', function ($uibModal, ModalService) {
        return {
            restrict: "A",
            scope: {
                refreshCallback: "&",
                employeeId: '='
            },
            link: function (scope, elem, attrs) {
                angular.element(elem).on('click', function (event) {
                    event.preventDefault();
                    ModalService.show({
                        id: attrs.employeeRoleCreate,
                        view: '/HumanResources/Static/Employee/History/employeeRoleCreate.html',
                        controller: 'EmployeeHistoryRoleCreateCtrl',
                        cb: scope.refreshCallback(),
                        service: 'EmployeeHistoryRoleService',
                        resolve: {
                            employeeId: scope.employeeId
                        },
                    });

                });
            }
        }
    }])
    .directive("employeeTypeCreate", ['$uibModal', 'ModalService', function ($uibModal, ModalService) {
            return {
                restrict: "A",
                scope: {
                    refreshCallback: "&",
                    employeeId: '='
                },
                link: function (scope, elem, attrs) {
                    angular.element(elem).on('click', function (event) {
                        event.preventDefault();
                        ModalService.show({
                            id: attrs.employeeTypeCreate,
                            view: '/HumanResources/Static/Employee/History/employeeTypeCreate.html',
                            controller: 'EmployeeHistoryTypeCreateCtrl',
                            cb: scope.refreshCallback(),
                            service: 'EmployeeHistoryTypeService',
                            resolve: {
                                employeeId: scope.employeeId
                            },
                        });

                    });
                }
            }
        }])
})(window.angular);