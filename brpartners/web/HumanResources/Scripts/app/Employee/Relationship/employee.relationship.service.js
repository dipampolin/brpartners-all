﻿(function (angular) {
    'use strict';

    angular.module('brp')
    .factory("EmployeeRelationshipService", ['$http', 'PaginateRequest', function ($http, PaginateRequest) {


        var create = function (data) {
            return $http.post('/RH/Relationship/Save', data)
                .then(function (res) {
                    return res.data;
                });
        }

        var list = function (id) {
            return $http.get('/RH/Relationship/List', {
                params: {
                    employeeId: id
                }
            }).then(function (res) {
                return res.data;
            });
        }
        
        var getHealthPlan = function () {
            return $http.get('/RH/Relationship/GetHealthPlan').then(function (res) {
                return res.data;
            });
        }

        var getDentalPlan = function () {
            return $http.get('/RH/Relationship/GetDentalPlan').then(function (res) {
                return res.data;
            });
        }

        var findById = function (id) {
            return $http.get('/RH/Relationship/Find', {
                params: {
                    id: id
                }
            }).then(function (res) {
                return res.data;
            });
        }

        var remove = function (id) {
            return $http.get('/RH/Relationship/Remove', {
                params: {
                    id: id
                }
            }).then(function (res) {
                return res.data;
            });
        }

        var getRelationshipTypes = function () {
            return [{ Id: 'Pai', Value: 'Pai' }, { Id: 'Mãe', Value: 'Mãe' }, { Id: 'Filho', Value: 'Filho' }, { Id: 'Cônjuge', Value: 'Cônjuge' }, { Id: 'Agregado', Value: 'Agregado' }, { Id: 'Enteado', Value: 'Enteado' }];;
        }

        return {
            create: create,
            list: list,
            findById: findById,
            remove: remove,
            getHealthPlan: getHealthPlan,
            getDentalPlan: getDentalPlan,
            getRelationshipTypes: getRelationshipTypes
        }
    }]);

})(window.angular);