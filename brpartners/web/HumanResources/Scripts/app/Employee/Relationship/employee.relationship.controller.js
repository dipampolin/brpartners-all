﻿(function (angular) {
    'use strict';

    angular.module('brp')

    .controller("EmployeeRelationshipCtrl", ['$scope', 'EmployeeService', 'EmployeeRelationshipService', 'toastr', function ($scope, EmployeeService, EmployeeRelationshipService, toastr) {
        var $ctrl = this;

        $ctrl.employee = employee;

        this.refresh = function () {
            $ctrl.isLoading = true;
            if (employee.Id) {
                EmployeeRelationshipService.list(employee.Id).then(function (result) {
                    $ctrl.data = result;
                    $ctrl.isLoading = false;
                });
            }
        }
        this.refresh();

        $ctrl.remove = function (id) {
            EmployeeRelationshipService.remove(id).then(function (result) {
                toastr.success('Pessoa Relacionada excluído com sucesso.');
                EmployeeRelationshipService.list(employee.Id).then(function (data) {
                    $ctrl.data = data;
                });
            }).catch(function (err) {
                toastr.error('Falha ao excluir Pessoa Relacionada!');
            });
        }

    }]);
})(window.angular);
