﻿(function (angular) {
	'use strict';

	angular.module('brp')

    .factory("EmployeeCompanyService", ['$http', function ($http) {

        var getActiveEmployeeCompany = function (id) {
    		return $http({
    			method: 'GET',
    			url: '/RH/EmployeeCompany/GetActiveEmployeeCompany?id=' + id
    		}).then(function (res) {
    			return res.data;
    		});
    	};

    	return {
    	    getActiveEmployeeCompany: getActiveEmployeeCompany
    	}

    }]);

})(window.angular);