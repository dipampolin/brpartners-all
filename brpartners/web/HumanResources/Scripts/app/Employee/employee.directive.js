﻿(function (angular) {
    'use strict';

    angular.module('brp')

    .directive("employeeCreate", ['$uibModal', 'EmployeeService', 'CompanyService', 'ModalService', function ($uibModal, EmployeeService, CompanyService, ModalService) {

        return {
            restrict: "A",
            scope: {
                refreshCallback: "&"
            },
            link: function (scope, elem, attrs) {
                angular.element(elem).on('click', function (event) {
                    event.preventDefault();

                    ModalService.show({
                        view: '/HumanResources/Static/Employee/employeeCreate.html',
                        controller: 'EmployeeCreateCtrl',
                        resolve: {
                            companies: function () {
                                return CompanyService.listAll();
                            }
                        }
                    }).then(function () {

                    })
                });
            }
        }
    }]);
})(window.angular);