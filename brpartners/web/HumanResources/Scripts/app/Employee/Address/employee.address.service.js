﻿(function (angular) {
    'use strict';

    angular.module('brp')
    .factory("EmployeeAddressService", ['$http', function ($http) {

        var create = function (data) {
            return $http.post('/RH/EmployeeAddress/CreatePost', data)
                .then(function (res) {
                    return res.data;
                });
        }

        var list = function (id) {
            return $http.get('/RH/EmployeeAddress/List', {
                params: {
                    employeeId: id
                }
            }).then(function (res) {
                return res.data;
            });
        }

        var remove = function (id) {
            return $http.get('/RH/EmployeeAddress/Remove', {
                params: {
                    id: id
                }
            }).then(function (res) {
                return res.data;
            });
        }

        var findById = function (id) {
            return $http.get('/RH/EmployeeAddress/Find', {
                params: {
                    employeeId: id
                }
            }).then(function (res) {
                return res.data;
            });
        }

        var GetAllUfs = function () {
            return [
                 {
                    "id": "1",
                    "Sigla": "AC",
                 },
                 {
                     "id": "2",
                     "Sigla": "AL",
                 },
                 {
                     "id": "3",
                     "Sigla": "AM",
                 },
                 {
                     "id": "4",
                     "Sigla": "AP",
                 },
                 {
                     "id": "5",
                     "Sigla": "BA",
                 },
                 {
                     "id": "6",
                     "Sigla": "CE",
                 },
                 {
                     "id": "7",
                     "Sigla": "DF",
                 },
                 {
                     "id": "8",
                     "Sigla": "ES",
                 },
                 {
                     "id": "9",
                     "Sigla": "GO",
                 },
                 {
                     "id": "10",
                     "Sigla": "MA",
                 },
                 {
                     "id": "11",
                     "Sigla": "MG",
                 },
                 {
                     "id": "12",
                     "Sigla": "MS",
                 },
                 {
                     "id": "13",
                     "Sigla": "MT",
                 },
                 {
                     "id": "14",
                     "Sigla": "PA",
                 },
                 {
                     "id": "15",
                     "Sigla": "PB",
                 },
                 {
                     "id": "16",
                     "Sigla": "PE",
                 },
                 {
                     "id": "17",
                     "Sigla": "PI",
                 },
                 {
                     "id": "18",
                     "Sigla": "PR",
                 },
                 {
                     "id": "19",
                     "Sigla": "RJ",
                 },
                 {
                     "id": "20",
                     "Sigla": "RN",
                 },
                 {
                     "id": "21",
                     "Sigla": "RO",
                 },
                 {
                     "id": "22",
                     "Sigla": "RR",
                 },
                 {
                     "id": "23",
                     "Sigla": "RS",
                 },
                 {
                     "id": "24",
                     "Sigla": "SC",
                 },
                 {
                     "id": "25",
                     "Sigla": "SE",
                 },
                 {
                     "id": "26",
                     "Sigla": "SP",
                 },
                 {
                     "id": "27",
                     "Sigla": "TO",
                 }]
        }

        return {
            create: create,
            list: list,
            findById: findById,
            GetAllUfs: GetAllUfs,
            remove: remove
        }
    }]);
})(window.angular);