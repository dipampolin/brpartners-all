﻿(function (angular) {
    'use strict';

    angular.module('brp')
    .controller("EmployeeAddressCreateCtrl", ['$scope', 'cepService', '$uibModalInstance', 'LocationService', 'states', 'data', 'EmployeeAddressService', 'employeeId', 'toastr',
        function ($scope, cep, $uibModalInstance, LocationService, states, data, EmployeeAddressService, employeeId, toastr) {
            var $ctrl = this;

            this.data = data || {};
            this.data.EmployeeId = this.data.EmployeeId || employeeId;
            this.data.Cep = this.data.Cep && this.data.Cep.toString();

            function InitialValues() {
                if (!$ctrl.data.Id) {
                    $ctrl.data.Active = true;
                }
            }

            InitialValues();

            $ctrl.addressTypes = [{ Id: "Comercial", Value: "Comercial" }, { Id: "Correspondência", Value: "Correspondência" }, { Id: "Residencial", Value: "Residencial" }, { Id: "Outros", Value: "Outros" }];

            if (this.data.StateId) {
                this.data.State = states.filter(function (state) {
                    return $ctrl.data.StateId == state.Id;
                })[0];

                LocationService.listCities(this.data.StateId)
                    .then(function (cities) {
                        $ctrl.cities = cities;

                        $ctrl.data.City = cities.filter(function (city) {
                            return city.Id == $ctrl.data.CityId;
                        })[0];
                    });
            }

            $ctrl.searchCep = function () {
                if (!$ctrl.data.Cep) {
                    return;
                }
                $ctrl.loadingCep = true;
                cep.search($ctrl.data.Cep).then(function (cep) {
                    return LocationService.findByUF(cep.state)
                        .then(function (state) {
                            $ctrl.data.StateId = state.Id;
                            $ctrl.data.State = state;

                            return LocationService.listCities(state.Id).then(function (cities) {
                                $ctrl.cities = cities;

                                $ctrl.data.City = cities.filter(function (city) {
                                    return city.Value == cep.city;
                                })[0];
                                $ctrl.data.Street = cep.street;
                                $ctrl.data.Neighborhood = cep.neighborhood;
                            });
                        });
                }).finally(function () {
                    $ctrl.loadingCep = false;
                });
            };

            $scope.$watch(function () {
                return $ctrl.data.State;
            }, function (state) {
                if (!state || $ctrl.data.StateId == state.Id) {
                    return;
                }

                $ctrl.data.StateId = state.Id;
                $ctrl.cities = [];
                LocationService.listCities(state.Id).then(function (cities) {
                    $ctrl.cities = cities;
                });
            });

            $ctrl.states = states;

            $ctrl.ok = function () {
                $ctrl.data.CityId = $ctrl.data.City.Id;
                EmployeeAddressService.create($ctrl.data).then(function (result) {
                    toastr.success('Endereço salvo com sucesso.');
                    return $uibModalInstance.close(data);
                }).catch(function (err) {
                    toastr.error('Falha ao salvar Endereço!');
                });
            };

            $ctrl.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        }]);

})(window.angular);