﻿
(function (angular) {
    'use strict';

    angular.module('brp')
    .directive("addressCreate", ['$uibModal', 'LocationService', 'ModalService', function ($uibModal, LocationService, ModalService) {

        return {
            restrict: "A",
            scope: {
                refreshCallback: "&",
                employeeId: "="
            },
            link: function (scope, elem, attrs) {

                angular.element(elem).on('click', function (event) {
                    event.preventDefault();

                    ModalService.show({
                        id: attrs.addressCreate,
                        view: '/HumanResources/Static/Employee/Address/addressCreate.html',
                        controller: 'EmployeeAddressCreateCtrl',
                        resolve: {
                            states: function () {
                                return LocationService.listStates();
                            },
                            employeeId: scope.employeeId
                        },
                        service: 'EmployeeAddressService',
                        cb: scope.refreshCallback()
                    });
                });

            }
        }

    }]);

})(window.angular);
