﻿(function (angular) {
    'use strict';

    angular.module('brp')

        .factory("EmployeeVariablesTypeService", ['$http', function ($http) {

            var listAll = function () {
                return $http.get('/RH/EmployeeVariablesType/ListAll')
                    .then(function (res) {
                        return res.data;
                    });
            };

            return {
                listAll: listAll
            }
        }]);

})(window.angular);