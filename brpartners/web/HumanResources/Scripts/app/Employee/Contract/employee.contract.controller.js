﻿(function (angular, employee) {
    'use strict';

    angular.module('brp')

    .controller("EmployeeContractCtrl", ['$scope', 'EmployeeContractService', 'toastr', function ($scope, EmployeeContractService, toastr) {
        var $ctrl = this;

        $ctrl.employee = employee;
        
        this.loadData = function () {
            if (employee.Id) {
                $ctrl.isLoading = true;
                EmployeeContractService.list(employee.Id).then(function (contracts) {
                    $ctrl.contracts = contracts;
                    $ctrl.isLoading = false;
                });
            }
        }
        this.loadData();

        $ctrl.remove = function (id) {
            EmployeeContractService.remove(id).then(function (result) {
                toastr.success('Vencimento contrato excluído com sucesso.');
                EmployeeContractService.list(employee.Id).then(function (data) {
                    $ctrl.contracts = data;
                });
            }).catch(function (err) {
                toastr.error('Falha ao excluir Contrato!');
            });
        }
    }]);

})(window.angular, window.employee);
