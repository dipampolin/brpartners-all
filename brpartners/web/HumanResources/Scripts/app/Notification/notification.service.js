﻿(function (angular) {
    'use strict';

    angular.module('brp')

    .factory("NotificationService", ['$http', 'PaginateRequest', function ($http, PaginateRequest) {

        var list = function (state) {
            return PaginateRequest.get('/RH/Notification/List', state, 'Name')
                .then(function (res) {
                    return res.data;
                });
        };

        var listItens = function () {
            return $http.get('/RH/Notification/ListItens')
               .then(function (res) {
                   return res.data;
               });
        };
        
        var create = function (data) {
            return $http.post('/RH/Notification/Create', { entity: data }).then(function (res) {
                return res.data;
            });
        }

        var remove = function (data) {
            return $http.post('/RH/Notification/Remove', { entity: data }).then(function (res) {
                return res.data;
            });
        }

        var notify = function (data) {
            return $http.post('/RH/Notification/SendEmail', { id: data }).then(function (res) {
                return res.data;
            });
        }

        var findById = function (id) {
            return $http({
                method: 'GET',
                url: '/RH/Notification/GetNotification/' + id
            }).then(function (res) {
                return res.data;
            });
        }


        var findFiltrableFields = function (id) {
            return $http({
                method: 'GET',
                url: '/RH/Notification/GetFiltrable?id=' + id
            }).then(function (res) {
                return res.data;
            });
        };

        var addFilterInQuery = function (filters, notificationId) {
            return $http.post('/RH/Notification/AddQueryFilter', { filters: filters.Filters, notificationId: notificationId }).then(function (res) {
                return res.data;
            });
        }

        var getRoles = function () {
            return $http({
                method: 'GET',
                url: '/RH/Notification/GetNotificationRoles'
            }).then(function (res) {
                return res.data;
            });
        };

        var getRoleDescritionByEnumValue = function (value) {
            return $http({
                method: 'GET',
                url: '/RH/Notification/GetRoleDescritionByEnumValue?value=' + value
            }).then(function (res) {
                return res.data;
            });
        }

        return {
            list: list,
            listItens: listItens,
            create: create,
            remove: remove,
            notify: notify,
            findById: findById,
            findFiltrableFields: findFiltrableFields,
            addFilterInQuery: addFilterInQuery,
            getRoles: getRoles,
            getRoleDescritionByEnumValue: getRoleDescritionByEnumValue
        }
     }]);

})(window.angular);