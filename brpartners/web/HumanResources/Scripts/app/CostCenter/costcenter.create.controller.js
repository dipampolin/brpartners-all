﻿(function (angular) {
    'use strict';

    angular.module('brp')

    .controller("CostCenterCreateCtrl", ['$uibModalInstance', 'data', 'toastr', 'CostCenterService', function ($uibModalInstance, data, toastr, CostCenterService) {
        var $ctrl = this;

        this.data = data || {};

        $ctrl.ok = function () {
            CostCenterService.create($ctrl.data).then(function (result) {
                toastr.success('Centro de Custo salvo com sucesso.');
                $uibModalInstance.close();
            }).catch(function () {
                toastr.error('Falha ao salvar Centro de Custo!');
            });
        };

        $ctrl.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }]);
})(window.angular);

