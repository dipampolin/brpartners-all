﻿(function (angular) {
    'use strict';

    angular.module('brp')

    .factory("CostCenterService", ['$http', 'PaginateRequest', function ($http, PaginateRequest) {

        var list = function () {
            return $http({
                method: 'GET',
                url: '/RH/CostCenter/List'
            }).then(function (res) {
                return res.data;
            });
        };

        var listAll = function (state) {
            return PaginateRequest.get('/RH/CostCenter/ListAll', state, 'Name')
                .then(function (res) {
                    return res.data;
                });
        };

        var create = function (data) {
            return $http.post('/RH/CostCenter/Create', data).then(function (res) {
                return res.data;
            });
        }

        var findById = function (id) {
            return $http({
                method: 'GET',
                url: '/RH/CostCenter/Find/' + id
            }).then(function (res) {
                return res.data;
            });
        }

        var findCache = {};

        var findByName = function (name, id) {
            if (findCache[name]) {
                return findCache[name];
            }

            findCache[name] = $http({
                method: 'GET',
                url: '/RH/CostCenter/FindByName?name=' + name
            }).then(function (res) {
                return res.data;
            });

            return findCache[name];
        }
        
        var listReport = function () {
            return $http({
                method: 'GET',
                url: '/RH/CostCenter/ListReport'
            }).then(function (res) {
                return res.data;
            });
        }

        var remove = function (id) {
            return $http.get('/RH/CostCenter/Remove/' + id)
                .then(function (res) {
                    return res.data;
                });
        }

        var getActiveEmployeeCostCenter = function (id) {
            return $http({
                method: 'GET',
                url: '/RH/EmployeeCostCenter/GetActiveEmployeeCostCenter?id=' + id
            }).then(function (res) {
                return res.data;
            });
        };

        return {
            list: list,
            listAll: listAll,
            create: create,
            findById: findById,
            findByName: findByName,
            listReport: listReport,
            remove: remove,
            getActiveEmployeeCostCenter: getActiveEmployeeCostCenter
        }
    }]);

})(window.angular);