﻿(function (angular) {
    'use strict';

    angular
        .module('brp')

        .controller("RoleIndexCtrl", ["$scope", "RoleService", "toastr", function ($scope, RoleService, toastr) {

            var $ctrl = this;

            this.loadData = function (state) {
                $ctrl.isLoading = true;

                RoleService.paginateList(state).then(function (result) {
                    $ctrl.data = result.data;
                    state.pagination.numberOfPages = result.pages;
                    $ctrl.isLoading = false;
                });
            }

            this.remove = function(id) {
                RoleService.remove(id)
                    .then(function (result) {
                        $ctrl.refresh();
                        toastr.success('Cargo excluído com sucesso.');
                    })
                    .catch(function (err) {
                        if (err.status == 409) {
                            toastr.warning('Cargo vinculado à um Funcionário!');
                        } else {
                            toastr.error('Falha ao excluir Cargo!');
                        }
                    });
            }

        }]);


})(window.angular);