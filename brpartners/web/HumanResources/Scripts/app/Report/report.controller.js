﻿(function (angular) {
    'use strict';

    angular.module('brp')
    .controller("ReportIndexCtrl", ['$scope', 'toastr', 'ReportService', function ($scope, toastr, ReportService) {

        var $ctrl = this;

        this.loadData = function (state) {
            $ctrl.isLoading = true;

            ReportService.list(state).then(function (result) {
                $ctrl.data = result.data;
                state.pagination.numberOfPages = result.pages;

                $ctrl.isLoading = false;
            });

            $ctrl.remove = function (item) {
                ReportService.remove(item).then(function () {
                    toastr.success('Relatório excluído com sucesso');
                    $ctrl.refresh();
                }).catch(function () {
                    toastr.error('Falha ao excluir relatório!');
                });
            };
        };

    }]).controller("ReportCreateCtrl", ['$uibModalInstance', '$uibModal', 'ReportService', 'ModalService', 'data', 'toastr', '$filter', function ($uibModalInstance, $uibModal, ReportService, ModalService, data, toastr, $filter) {
        var $ctrl = this;
        this.data = data || {};

        ReportService.findAlias().then(function (items) {
            items.find(removeSupervisor);
            $ctrl.data.Items = $ctrl.data.Items || [];
            $ctrl.data.Items.forEach(function (item) {
                var originalItem = items.filter(function (oitem) {
                    return oitem.Field == item.Field && oitem.Table == item.Table;
                })[0] || null;

                var index = items.indexOf(originalItem);

                if (index >= 0) {
                    items.splice(index, 1);
                }

            });

            $ctrl.options = {
                title: 'Selecione os Campos',
                orderProperty: 'Field',
                items: items,
                selectedItems: $ctrl.data.Items ? $ctrl.data.Items : [],
                groups: $filter('unique')(items, "Table")
            };
        });

        function removeSupervisor(item, index, array) {
            if (item) {
                if (item.Field == 'SupervisorId') {
                    array.splice(index, 1);
                }
            }
        }

        if (!$ctrl.data.Id) {
            $ctrl.data.Active = true;
        }

        $ctrl.ok = function () {

            ReportService.save(this.options.selectedItems, this.data).then(function (data) {
                if (data.Status === 200) {

                    ModalService.show({
                        view: '/HumanResources/Static/Report/reportFilterCreate.html',
                        controller: 'ReportFilterCtrl',
                        resolve: {
                            reportId: data.reportId
                        }
                    }).then(function (item) {

                    });

                    toastr.success('Relatório salvo com Sucesso!');
                    $uibModalInstance.close();
                }

                if (!data.Status) {
                    toastr.error(data);
                    $uibModalInstance.close();
                }

            }).catch(function () {
                toastr.error('Falha ao criar o relatório!');
            });
        };

        $ctrl.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

    }]).controller("ReportFilterCtrl", ['$uibModalInstance', 'ReportService', '$scope', 'toastr', 'data', 'reportId', 'EmployeeService', 'CompanyService', 'CostCenterService', function ($uibModalInstance, ReportService, $scope, toastr, data, reportId, EmployeeService, CompanyService, CostCenterService) {
        var $ctrl = this;
        this.data = data || {};

        if (reportId)
            ReportService.findFiltrableFields(reportId).then(function (res) {
                for (var i = 0; i < res.length; i++) {
                    if (res[i].Type === "Boolean" && res[i].Value != null) {
                        res[i].Value = (res[i].Value - 0);
                    }
                    res[i].Model = null;
                }
                $ctrl.data.Filters = res;
            });


        $ctrl.isLoading = true;

        $ctrl.addMask = function (item) {
            if (item.Type === 'DateTime') {
                return {
                    mask: '99/99/9999',
                    placeHolder: '__/__/____',
                    length: 10,
                    type: "Date"
                }
            }
            if (item.Type === 'String') {
                return {
                    mask: undefined,
                    placeHolder: undefined,
                    length: 50,
                    type: "String"
                }
            }
            if (item.Type === 'Boolean') {
                return {
                    mask: undefined,
                    placeHolder: undefined,
                    length: undefined,
                    type: "Boolean"
                }
            }
        };

        $ctrl.initialValues = function (item) {
            if (item.Field === 'SupervisorId') {
                var id = parseInt(item.Value);
                if (!isNaN(id)) {
                    EmployeeService.findById(id).then(function (result) {
                        item.Model = result.Name;
                    });
                }
            }
        }

        $ctrl.verifySupervisor = function (item) {
            if (!item.Model) {
                item.Value = null;
            }
        };

        $ctrl.selectDateInterval = ReportService.getDateInterval();

        $ctrl.allBooleanChoose = ReportService.getBooleanChoose();

        this.getAutoCompleteValue = function (val, item) {
            if (item.Table === 'Employee') {
                return EmployeeService.findByName(val, $ctrl.data.Id || 0);
            }
        };

        CostCenterService.listReport().then(function (result) {
            $ctrl.selectCostCenter = result;
        });

        CompanyService.findReport().then(function (result) {
            $ctrl.selectCompany = result;
        });

        this.onSelect = function ($item, $model, $label, item) {
            item.Value = $item.Id;
            item.Model = $item.Name;
        }

        this.onSupervisorSelect = function myfunction($item, $model, $label, item) {
            item.Value = $item.Id;
            item.Model = $item.Name;
        }

        $ctrl.ok = function () {
            if (reportId)
                ReportService.addFilterInQuery($ctrl.data.Filters, reportId).then(function (res) {
                    if (res.Message === "Ok") {
                        toastr.success('Filtro para consulta salvo com Sucesso!');
                        $uibModalInstance.close();
                    }
                    if (res.Message !== "Ok") {
                        toastr.error(res.Message);
                    }
                }).catch(function (err) {
                    toastr.error(err);
                });
        };

        $ctrl.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

    }]);
})(window.angular);

