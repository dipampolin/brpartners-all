﻿(function (angular) {
    'use strict';
    angular.module('brp')

    .factory("CivilStatusService", ['$http', function ($http) {

        var list = function () {
            return $http({
                method: 'GET',
                url: '/RH/CivilStatus/List'
            }).then(function (res) {
                return res.data;
            });
        };

        return {
            list: list
        }
    }]);

})(window.angular);