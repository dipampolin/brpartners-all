﻿(function (angular) {
    'use strict';

    angular.module('brp')

    .factory("BenefitService", ['$http', 'PaginateRequest', function ($http, PaginateRequest) {

        var list = function (state) {
            return PaginateRequest.get('/RH/Benefit/List', state, 'Type.Name')
                .then(function (res) {
                    return res.data;
                });
        };
        
        var findById = function (id) {
            return $http({
                method: 'GET',
                url: '/RH/Benefit/Find/' + id
            }).then(function (res) {
                return res.data;
            });
        }

        var save = function (data) {
            return $http.post('/RH/Benefit/Save', data).then(function (res) {
                return res.data;
            });
        }

        var listAll = function () {
            return $http({
                method: 'GET',
                url: '/RH/Benefit/ListAll'
            }).then(function (res) {
                return res.data;
            });
        }

        var remove = function (id) {
            return $http.get('/RH/Benefit/Remove/' + id)
                    .then(function (res) {
                        return res.data;
                    });
        }

        return {
            list: list,
            findById: findById,
            save: save,
            listAll: listAll,
            remove: remove
        }

    }]);

})(window.angular);