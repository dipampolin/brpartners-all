﻿(function (angular) {
    'use strict';

    angular.module('brp')

    .factory("LocationService", ['$http', '$q', 'PaginateRequest', function ($http, $q, PaginateRequest) {

        var cityCache = {};
        var stateCache = false;

        var listStates = function () {
            if (stateCache) {
                return $q.resolve(stateCache);
            }
            return $http.get('/RH/State/ListAll').then(function (res) {
                stateCache = res.data;
                return stateCache;
            });
        };

        var findByUF = function (uf) {
            return listStates().then(function (states) {
                return states.filter(function (state) {
                    return state.Uf == uf;
                })[0]
            });
        };

        var listCities = function (stateId) {

            if (cityCache[stateId]) {
                return $q.resolve(cityCache[stateId]);
            }

            return $http.get('/RH/City/ListAll?stateId=' + stateId).then(function (res) {
                cityCache[stateId] = res.data;
                return res.data;
            });
        };

        return {
            listStates: listStates,
            listCities: listCities,
            findByUF: findByUF
        }
    }]);
})(window.angular);