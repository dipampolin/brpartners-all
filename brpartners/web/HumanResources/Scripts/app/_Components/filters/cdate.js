﻿(function (angular) {
    'use strict';

    angular.module('brp')
        .filter('cdate', function () {
        return function (val) {

            if (typeof val == 'object') {
                return val;
            }

            return moment(val).toDate();
        }
    });
})(window.angular);
