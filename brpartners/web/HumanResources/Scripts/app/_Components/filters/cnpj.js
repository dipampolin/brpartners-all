﻿
(function (angular) {
    'use strict';

    angular.module('brp')

    .filter('cnpj', function () {
        return function (val) {
            return val.replace(/^(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})$/, '$1.$2.$3/$4-$5');
        }
    });

})(window.angular);
