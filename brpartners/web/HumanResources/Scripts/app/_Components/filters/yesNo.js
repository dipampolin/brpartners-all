﻿(function (angular) {
    'use strict';

    angular.module('brp')

    .filter('yesNo', function () {
        return function (val) {
            return val ? 'Sim' : 'Não';
        }
    });

})(window.angular);
