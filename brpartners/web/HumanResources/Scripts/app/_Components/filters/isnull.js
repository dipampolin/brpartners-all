﻿(function (angular) {
    'use strict';

    angular.module('brp')
        .filter('isnull', function () {
        return function (val) {
            if (val == 'Invalid Date') {
                return null;
            }

            return val;
        }
    });

})(window.angular);
