﻿(function (angular) {
    'use strict';

    angular.module('brp')

    .filter('activeUnactive', function () {
        return function (val) {
            return val ? 'Ativo' : 'Inativo';
        }
    });

})(window.angular);
