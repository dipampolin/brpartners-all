﻿(function (angular) {
    'use strict';

    angular.module('brp')
    .directive('hasFeedback', ['$compile', function ($compile) {
        return {
            restrict: "C",
            scope: false,
            priority: 1,
            compile: function compile(tElement, tAttrs, transclude) {
                return {
                    pre: function preLink(scope, iElement, iAttrs, controller) { 
                        
                        var el = angular.element(iElement);

                        var input = el.find("input, select, textarea").first();

                        var inputName = input.first().attr('name');
                        var formName = el.closest('form').attr('name');

                        if (input.attr('required')) {
                            el.addClass('required');
                        }
                        else if (input.attr('ng-required')) {
                            el.addClass('required');
                        }

                        iAttrs.$set('ng-class', "{'has-error': (" + formName + ".$submitted || " + formName + "." + inputName + ".$touched) && " + formName + "." + inputName + ".$invalid}");

                        el.removeClass("has-feedback");

                        $compile(iElement)(scope);
                    },
                    post: function postLink(scope, iElement, iAttrs, controller) {
                        var el = angular.element(iElement);
                        if (el.find(".form-control-feedback").length > 0) {
                            el.addClass("has-feedback");
                        }
                    }
                }
            },
            
        };
    }]);

})(window.angular);


