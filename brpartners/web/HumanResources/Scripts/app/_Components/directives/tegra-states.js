﻿(function (angular) {
    'use strict';

    angular.module('brp')
    .directive("selectState", ['LocationService', function (LocationService) {
    return {
        restrict: "E",
        template:
        "<select class='form-control input-sm' ng-model='model' ng-options='item.Id as item.Value for item in states' ><option value='' disabled>Selecione</option></select>",
        scope: {
            model: "=ngModel"
        },
        controller: ['$scope', function ($scope) {

            LocationService.listStates().then(function (states) {
                $scope.states = states;
            })
        }],
        link: function (scope, element, attrs, ctrl) {
        }
    }
    }]);
})(window.angular);