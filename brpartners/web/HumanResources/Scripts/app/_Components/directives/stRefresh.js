﻿
(function (angular) {
    'use strict';

    angular.module('smart-table')
    .directive('stRefresh', function () {
        return {
            restrict: "A",
            require: '^stTable',
            scope: {
                stRefresh: "="
            },
            link: function (scope, element, attrs, ctrl) {
                
                scope.stRefresh = function () {
                    ctrl.pipe();
                }

            },
            
        };
    });

})(window.angular);
