﻿(function (angular) {
    'use strict';

    angular.module('brp')
        .directive("buttonImageFile", ['$q', 'toastr', function ($q, toastr) {
            return {
                restrict: "E",
                template:
                "<button type='button' class='btn btn-sm btn-default' style='margin-top: 5px; width: 100%;' ng-click='openFileInput()'>Upload</button>"
                + "<input type='file' id='inputFile' style='display:none;' accept='image/jpg, image/png, image/jpeg' />",
                scope: {
                    model: "=ngModel",
                    maxWidthPx: "=maxWidthPx"
                },
                controller: ['$scope', function ($scope) {

                    $scope.openFileInput = () => {
                        angular.element('#inputFile').trigger('click');
                    }

                    angular.element("#inputFile").bind("change", (changeEvent) => {
                        $scope.$apply(() => {
                            var file = changeEvent.target.files[0];

                            readFile(file)
                                .then(convertToImage)
                                .then(i => compressFile(i, $scope.maxWidthPx))
                                .then((imageData) => {
                                    $scope.model = imageData;
                                })
                                .catch(function (err) {
                                    toastr.error("Validas somente imagens nos tipos JPG/JPEG/PNG");
                                    angular.element("#inputFile")[0].value = "";
                                });
                        });
                    });

                    const readFile = (file) => {

                        return $q((resolve, reject) => {
                            const reader = new FileReader();

                            reader.onload = (e) => {
                                resolve(reader.result);
                            };

                            reader.onerror = reader.onabort = reject;

                            if (file) {
                                reader.readAsDataURL(file);
                            }                            
                        });
                    };

                    const convertToImage = (data) => {
                        return $q((resolve, reject) => {
                            const image = new Image();
                            image.addEventListener('load', function () {
                                resolve(image);
                            }, false);

                            image.addEventListener('error', reject, false);
                            image.addEventListener('abort', reject, false);

                            image.src = data;
                        });
                    };

                    const compressFile = (image, maxWidth = null) => {
                        return $q((resolve, reject) => {
                            var canvas = $('<canvas />').get(0);

                            canvas.width = image.width;
                            canvas.height = image.height;

                            if (maxWidth && image.width > maxWidth) {
                                canvas.width = maxWidth;
                                canvas.height = (maxWidth * image.height) / image.width;
                            }

                            var context = canvas.getContext('2d');
                            context.drawImage(image, 0, 0, image.width, image.height, 0, 0, canvas.width, canvas.height);

                            resolve(canvas.toDataURL('image/jpeg'));
                        });
                    }

                }],
                link: function (scope, element, attrs, ctrl) {
                }
            }
        }]);
})(window.angular);