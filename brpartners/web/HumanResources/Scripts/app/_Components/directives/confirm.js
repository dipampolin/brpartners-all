﻿
(function (angular) {
    'use strict';

    angular.module('brp')
    .directive('confirm', ['TegraAlerts', function (TegraAlerts) {
        return {
            restrict: "A",
            priority: 1000,
            terminal: true,
            scope: false,
            link: function (scope, element, attrs, ctrl) {
                element.on('click', function (event) {

                    TegraAlerts.confirm(attrs.confirm, attrs.confirmTitle)
                        .then(function () {
                            if (attrs.ngClick) {
                                scope.$eval(attrs.ngClick);
                            }
                        }
                    );
                });
            }
            
        };
    }]);

})(window.angular);
