﻿(function (angular) {
    'use strict';

    angular.module('brp')
    .directive('mindate', [function () {
        return {
            restrict: 'A',
            require: 'ngModel',
            scope: {
                minDate: "=mindate"
            },
            link: function (scope, element, attr, ctrl) {
                ctrl.$validators.mindate = function (date) {
                    if (!date) {
                        return true;
                    }

                    var minDate = moment(scope.minDate);
                    if (scope.minDate && minDate.isValid() && attr.mindate !== '1900-01-01') {
                        return date >= minDate.toDate();
                    }
                    return date >= new Date('1900-01-01');

                }

                scope.$watch('minDate', ctrl.$validate);
            }
        };
    }]);

})(window.angular);


