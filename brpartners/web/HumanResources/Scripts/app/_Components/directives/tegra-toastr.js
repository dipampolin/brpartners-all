﻿
(function (angular) {
    'use strict';

    angular.module('brp')
    .directive('tegraToastr', ['toastr', function (toastr) {
        return {
            restrict: "E",
            scope: false,
            link: function (scope, element, attrs, ctrl) {
                if (!attrs.type) {
                    throw "Type is required!";
                }
                toastr[attrs.type](attrs.message || "No message in the toaster!");
            }
            
        };
    }]);

})(window.angular);
