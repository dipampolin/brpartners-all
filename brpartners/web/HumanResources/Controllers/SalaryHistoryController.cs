﻿using QX3.Portal.WebSite.Attributes;
using QX3.Portal.WebSite.HumanResources.Exceptions;
using QX3.Portal.WebSite.HumanResources.Helper;
using QX3.Portal.WebSite.HumanResources.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using Tegra.DDD.Pagination;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Repository;
using Tegra.HumanResources.Domain.Service;
using Tegra.HumanResources.Services;

namespace QX3.Portal.WebSite.HumanResources.Controllers
{
    [AccessControlAuthorize("RH.Employee.Index")]
    public class SalaryHistoryController : Controller
    {
        private readonly ISalaryHistoryRepository _salaryHistoryRepository;
        private readonly ISalaryHistoryService _salaryHistoryService;
        private readonly IEmployeeService _employeeService;
        private readonly IEmployeeRepository _employeeRepository;

        public SalaryHistoryController(ISalaryHistoryRepository salaryHistoryRepository,
            ISalaryHistoryService salaryHistoryService,
            IEmployeeService employeeService,
            IEmployeeRepository employeeRepository)
        {
            this._salaryHistoryRepository = salaryHistoryRepository;
            this._salaryHistoryService = salaryHistoryService;
            this._employeeService = employeeService;
            this._employeeRepository = employeeRepository;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult List(int EmployeeId)
        {
            var result = _salaryHistoryRepository.Query(c => c.EmployeeId == EmployeeId && c.DeletedDate == null).ToList();

            List<SalaryHistoryViewModel> list = new List<SalaryHistoryViewModel>();

            if (result != null && result.Count > 0)
            {
                foreach (var item in result)
                    list.Add(ToModel(item));
            }

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult Find(int id)
        {
            var data = _salaryHistoryRepository.Query(c => c.Id == id).FirstOrDefault();
            SalaryHistoryViewModel model = ToModel(data);

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Create(SalaryHistoryViewModel model)
        {
            EmployeeSalary entity = ToEntity(model);

            _salaryHistoryService.Save(entity);
            _employeeService.CalculateByPercentage(_employeeRepository.Find(model.EmployeeId));

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Remove(int id)
        {
            var employeeId = _salaryHistoryRepository.Find(id).EmployeeId;
            var result = _salaryHistoryService.Remove(id, false);
            _employeeService.CalculateByPercentage(null, employeeId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        private EmployeeSalary ToEntity(SalaryHistoryViewModel model)
        {
            return new EmployeeSalary
            {
                Category = model.Category,
                Description = model.Description,
                EmployeeId = model.EmployeeId,
                EndDate = model.EndDate,
                ExtraHour = Encrypt(model.ExtraHour),
                Gratification = Encrypt(model.Gratification),
                Id = model.Id,
                Salary = Encrypt(model.Salary),
                SalaryPeriod = model.SalaryPeriod,
                StartDate = model.StartDate,
                Triennium = Encrypt(model.Triennium),
                WorkLoad = model.WorkLoad,
            };
        }

        private SalaryHistoryViewModel ToModel(EmployeeSalary entity)
        {
            return new SalaryHistoryViewModel
            {
                Category = entity.Category,
                Description = entity.Description,
                EmployeeId = entity.EmployeeId,
                EndDate = entity.EndDate,
                ExtraHour = DecryptToDecimal(entity.ExtraHour),
                Gratification = DecryptToDecimal(entity.Gratification),
                Id = entity.Id,
                Salary = DecryptToDecimal(entity.Salary),
                SalaryPeriod = entity.SalaryPeriod,
                StartDate = entity.StartDate,
                Triennium = DecryptToDecimal(entity.Triennium),
                WorkLoad = entity.WorkLoad,
            };
        }

        private string CryptPassword = "AXYHP69Z";

        private string Encrypt(decimal? value)
        {
            if (!value.HasValue)
                return "";

            string v = value.Value.ToString("N2").Replace(".", "").Replace(",", ".");
            if (string.IsNullOrEmpty(v))
                return "";

            v = v.Replace(".", ",");

            return DESCrypto.Encrypt(v, CryptPassword);
        }

        private decimal? DecryptToDecimal(string value)
        {
            if (string.IsNullOrEmpty(value))
                return null;

            string v = DESCrypto.Decrypt(value, CryptPassword);

            return Convert.ToDecimal(v);
        }

    }
}