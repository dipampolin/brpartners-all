﻿using LinqKit;
using QX3.Portal.WebSite.Attributes;
using QX3.Portal.WebSite.HumanResources.Exceptions;
using QX3.Portal.WebSite.HumanResources.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using Tegra.DDD.Pagination;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Repository;
using Tegra.HumanResources.Services;

namespace QX3.Portal.WebSite.HumanResources.Controllers
{

    [AccessControlAuthorize("RH.Employee.Index")]
    public class CityController : Controller
    {

        private readonly ICityRepository _cityRepository;

        public CityController(ICityRepository cityRepository)
        {
            this._cityRepository = cityRepository;
        }

        [HttpGet]
        public JsonResult ListAll(int stateId)
        {
            var data = _cityRepository.QueryBy(x => x.DeletedDate == null && x.StateId == stateId, x => new CityViewModel
            {
                Id = x.Id,
                StateId = x.StateId,
                Value = x.Name
            }).OrderBy(x => x.Value).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult FindByName(string name, int stateId = 0)
        {
            try
            {
                if (stateId > 0)
                    return Json(_cityRepository.Query(c => c.Name.StartsWith(name) && c.StateId == stateId).ToList(), JsonRequestBehavior.AllowGet);

                return Json(_cityRepository.Query(c => c.Name.StartsWith(name)).ToList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Response.StatusCode = 500;
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult Find(int id)
        {
            var data = _cityRepository.SelectBy(x => x.Id == id).Single();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

    }
}