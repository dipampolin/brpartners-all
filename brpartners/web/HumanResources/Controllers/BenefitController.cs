﻿using QX3.Portal.WebSite.Attributes;
using QX3.Portal.WebSite.HumanResources.Models;
using System;
using System.Linq;
using System.Web.Mvc;
using Tegra.DDD.Pagination;
using Tegra.HumanResources.Domain.Exceptions;
using Tegra.HumanResources.Domain.Repository;
using Tegra.HumanResources.Domain.Representations;
using Tegra.HumanResources.Services;

namespace QX3.Portal.WebSite.HumanResources.Controllers
{

    [AccessControlAuthorize("RH.Benefit.Index")]
    public class BenefitController : Controller
    {
        private readonly IBenefitRepository _benefitRepository;
        private readonly IBenefitService _benefitService;

        public BenefitController(IBenefitRepository benefitRepository, IBenefitService benefitService)
        {
            this._benefitRepository = benefitRepository;
            this._benefitService = benefitService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult Find(int id)
        {
            var data = _benefitRepository.SelectBy(x => x.Id == id, x => new BenefitViewModel()
            {
                Id = x.Id,
                Type = x.Type.Name,
                BenefitTypeId = x.BenefitTypeId,
                Description = x.Description,
                Value = x.Value
            }).OrderBy(c => c.Type).ThenBy(c => c.Description).ThenByDescending(c => c.Value).Single();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult List(PaginationSettings settings)
        {
            var data = _benefitRepository.SelectPagedBy(settings, c => c.DeletedDate == null, x => new BenefitViewModel()
            {
                Id = x.Id,
                Type = x.Type.Name,
                Description = x.Description,
                Value = x.Value
            });

            data.data = settings.OrderDirection.ToString().Equals("Desc") ?
                data.data.OrderByDescending(c => c.Type).ThenBy(c => c.Value).ThenByDescending(c => c.Description).ToList() :
                data.data.OrderBy(c => c.Type).ThenBy(c => c.Value).ThenByDescending(c => c.Description).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ListAll()
        {
            var data = _benefitRepository.QueryBy(c => c.DeletedDate == null, x => new BenefitViewModel()
            {
                Id = x.Id,
                Type = x.Type.Name,
                Description = x.Description,
                Value = x.Value
            }).OrderBy(c => c.Type).ThenBy(c => c.Description).ThenByDescending(c => c.Value).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AutoValidate]
        public JsonResult Save([Bind(Exclude = "Type")] BenefitViewModel model)
        {
            try
            {
                _benefitService.Save(model.ToEntity());
                Response.StatusCode = 200;
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                Response.StatusCode = 500;
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult Remove(int id = 0)
        {
            try
            {
                _benefitService.Remove(id);
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (ForeignKeyEmployeeException ex)
            {
                Response.StatusCode = (int)StatusHttp.CONFLICT;
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }
    }
}