﻿using LinqKit;
using QX3.Portal.WebSite.Attributes;
using QX3.Portal.WebSite.HumanResources.Exceptions;
using QX3.Portal.WebSite.HumanResources.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using Tegra.DDD.Pagination;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Repository;
using Tegra.HumanResources.Services;

namespace QX3.Portal.WebSite.HumanResources.Controllers
{

    [AccessControlAuthorize("RH.Employee.Index")]
    public class StateController : Controller
    {

        private readonly IStateRepository _stateRepository;

        public StateController(IStateRepository stateRepository)
        {
            this._stateRepository = stateRepository;
        }

        [HttpGet]
        public JsonResult ListAll()
        {
            var data = _stateRepository.QueryBy(x => x.DeletedDate == null, x => new StateViewModel {
                Id = x.Id,
                Value = x.Name,
                Uf = x.UF
            }).OrderBy(x => x.Value).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }
        

    }
}