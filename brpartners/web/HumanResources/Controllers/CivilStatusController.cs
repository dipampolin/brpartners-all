﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tegra.HumanResources.Domain.Repository;

namespace QX3.Portal.WebSite.HumanResources.Controllers
{
    public class CivilStatusController : Controller
    {
        private readonly ICivilStatusRepository _repository;

        public CivilStatusController(ICivilStatusRepository repository)
        {
            this._repository = repository;
        }

        public JsonResult List()
        {
            var result = _repository.Query(c => c.Id > 0).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}