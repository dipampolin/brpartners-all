﻿using QX3.Portal.WebSite.Attributes;
using QX3.Portal.WebSite.HumanResources.Models;
using System;
using System.Linq;
using System.Web.Mvc;
using Tegra.HumanResources.Domain.Repository;
using Tegra.HumanResources.Services;

namespace QX3.Portal.WebSite.HumanResources.Controllers
{

    [AccessControlAuthorize("RH.Employee.Index")]
    public class EmployeeContactController : Controller
    {

        private readonly IContactRepository _contactRepository;
        private readonly IContactService _contactService;
        private readonly IEmployeeService _employeeService;
        private readonly IEmployeeRepository _employeeRepository;

        public EmployeeContactController(IContactRepository contactRepository, IContactService contactService, IEmployeeService employeeService, IEmployeeRepository employeeRepository)
        {
            this._contactRepository = contactRepository;
            this._contactService = contactService;
            this._employeeService = employeeService;
            this._employeeRepository = employeeRepository;
        }

        [HttpGet]
        public JsonResult List(int employeeId)
        {
            var data = _contactRepository.SelectBy(e => e.EmployeeId == employeeId && e.DeletedDate == null, x => new EmployeeContactViewModel()
            {
                Id = x.Id,
                EmployeeId = x.EmployeeId,
                Type = x.Type,
                Active = x.Active,
                Value = x.Value
            });

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult Find(int employeeId)
        {
            var data = _contactRepository.SelectBy(x => x.Id == employeeId, x => new EmployeeContactViewModel()
            {
                Id = x.Id,
                EmployeeId = x.EmployeeId,
                Type = x.Type,
                Active = x.Active,
                Value = x.Value
            }).Single();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AutoValidate]
        public JsonResult CreatePost(EmployeeContactViewModel model)
        {
            try
            {
                _contactService.Save(model.ToEntity());
                _employeeService.CalculateByPercentage(_employeeRepository.Find(model.EmployeeId));
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                Response.StatusCode = 500;
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult Remove(int id)
        {
            var employeeId = _contactRepository.Find(id).EmployeeId;
            var result = _contactService.Remove(id, false);
            _employeeService.CalculateByPercentage(null, employeeId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}