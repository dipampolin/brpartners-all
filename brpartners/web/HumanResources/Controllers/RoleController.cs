﻿using QX3.Portal.WebSite.Attributes;
using QX3.Portal.WebSite.HumanResources.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tegra.DDD.Pagination;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Exceptions;
using Tegra.HumanResources.Domain.Repository;
using Tegra.HumanResources.Domain.Representations;
using Tegra.HumanResources.Domain.Service;

namespace QX3.Portal.WebSite.HumanResources.Controllers
{
    public class RoleController : Controller
    {
        private readonly IRoleRepository _repository;
        private readonly IEmployeeRoleRepository _employeeRoleRepository;
        private readonly IRoleService _roleService;

        public RoleController(IRoleRepository repository, IEmployeeRoleRepository employeeRoleRepository, IRoleService roleService)
        {
            this._repository = repository;
            this._employeeRoleRepository = employeeRoleRepository;
            this._roleService = roleService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult ListAll()
        {
            var result = _repository.Query(c => c.Id > 0).OrderBy(c => c.Name).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult List(PaginationSettings settings)
        {
            var result = _repository.SelectPaged(settings, role => new RoleViewModel
            {
                Active = role.Active,
                CreatedDate = role.CreatedDate,
                Id = role.Id,
                Name = role.Name
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetActiveEmployeeRole(int id)
        {
            var result = _employeeRoleRepository.Query(c => c.EmployeeId == id).Include(c => c.Role).OrderByDescending(o => o.StartDate).FirstOrDefault();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AutoValidate]
        public JsonResult Save(RoleViewModel roleViewModel)
        {
            try
            {
                _roleService.Save(roleViewModel.toEntity());
                return Json(true, JsonRequestBehavior.AllowGet);
            } catch (Exception ex)
            {
                Response.StatusCode = 500;
                return Json(false, JsonRequestBehavior.AllowGet);
            }   
        }

        [HttpGet]
        public JsonResult Find(int id)
        {
            var data = _repository.SelectBy(role => role.Id == id,
                role => new RoleViewModel
                {
                    Active = role.Active,
                    Id = role.Id,
                    Name = role.Name
                }).OrderBy(role => role.Name).Single();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult Remove(int id = 0)
        {
            try
            {
                _roleService.Remove(id);
                return Json(true, JsonRequestBehavior.AllowGet);
            } catch (ForeignKeyEmployeeException ex)
            {
                Response.StatusCode = (int)StatusHttp.CONFLICT;
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }
    }
}