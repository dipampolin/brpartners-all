﻿using QX3.Portal.WebSite.HumanResources.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Repository;
using Tegra.HumanResources.Domain.Service;
using Tegra.HumanResources.Services;

namespace QX3.Portal.WebSite.HumanResources.Controllers
{
    public class EmployeeBenefitController : Controller
    {
        private readonly IEmployeeBenefitRepository _benefitrepository;
        private readonly IEmployeeBenefitService _benefitService;
        private readonly IEmployeeService _employeeService;
        private readonly IEmployeeRepository _employeeRepository;

        public EmployeeBenefitController(IEmployeeBenefitRepository benefitService,
            IEmployeeBenefitService benefitrepository,
            IEmployeeService employeeService,
            IEmployeeRepository employeeRepository)
        {
            this._benefitrepository = benefitService;
            this._benefitService = benefitrepository;
            this._employeeService = employeeService;
            this._employeeRepository = employeeRepository;
        }

        public JsonResult Create(EmployeeBenefit model)
        {
            _benefitService.Create(model);
            _employeeService.CalculateByPercentage(_employeeRepository.Find(model.EmployeeId));
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public JsonResult List(int employeeId)
        {
            var result = _benefitrepository.SelectBy(c => c.EmployeeId == employeeId && c.DeletedDate == null, e => new EmployeeBenefitViewModel
            {
                Id = e.Id,
                BenefitId = e.BenefitId,
                Description = e.Benefit.Description,
                EmployeeId = e.EmployeeId,
                EndDate = e.EndDate ?? null,
                StartDate = e.StartDate,
                Type = e.Benefit.Type.Name,
                Value = e.CustomValue ?? null,
                CardNumber = !string.IsNullOrEmpty(e.CardNumber) ? "Número: " + e.CardNumber : ""
            }).OrderBy(c => c.Description).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Find(int id)
        {
            var result = _benefitrepository.Query(c => c.Id == id).FirstOrDefault();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Remove(int id)
        {
            var employeeId = _benefitrepository.Find(id).EmployeeId;
            var result = _benefitService.Remove(id, false);
            _employeeService.CalculateByPercentage(null, employeeId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}