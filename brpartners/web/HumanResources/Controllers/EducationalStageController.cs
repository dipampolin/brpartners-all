﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tegra.HumanResources.Domain.Repository;

namespace QX3.Portal.WebSite.HumanResources.Controllers
{
    public class EducationalStageController : Controller
    {
        private IEducationalStageRepository _repository;

        public EducationalStageController(IEducationalStageRepository repository)
        {
            this._repository = repository;
        }

        public JsonResult List()
        {
            var result = _repository.Query(c => c.Id > 0).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}