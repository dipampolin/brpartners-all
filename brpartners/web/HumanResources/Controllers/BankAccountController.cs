﻿using QX3.Portal.WebSite.Attributes;
using QX3.Portal.WebSite.HumanResources.Models;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Repository;
using Tegra.HumanResources.Services;

namespace QX3.Portal.WebSite.HumanResources.Controllers
{
    [AccessControlAuthorize("RH.Employee.Index")]
    public class BankAccountController : Controller
    {
        private readonly IBankAccountRepository _bankAccountRepository;
        private readonly IBankAccountService _bankAccountService;
        private readonly IEmployeeService _employeeService;
        private readonly IEmployeeRepository _employeeRepository;

        public BankAccountController(IBankAccountRepository bankAccountRepository, IBankAccountService bankAccountService, IEmployeeService employeeService, IEmployeeRepository employeeRepository)
        {
            this._bankAccountRepository = bankAccountRepository;
            this._bankAccountService = bankAccountService;
            this._employeeRepository = employeeRepository;
            this._employeeService = employeeService;
        }

        private readonly Expression<Func<EmployeeBankAccount, BankAccountViewModel>> _columns = x => new BankAccountViewModel()
        {
            Id = x.Id,
            EmployeeId = x.EmployeeId,
            Account = x.Account,
            Agency = x.Agency,
            Bank = x.BankObj.Name + (string.IsNullOrEmpty(x.BankObj.Code) ? "" : " (" + x.BankObj.Code + ")"),
            Main = x.Main,
            BankId = x.BankId.HasValue ? x.BankId.Value : 0
        };

        [HttpGet]
        public JsonResult Find(int id)
        {
            var data = _bankAccountRepository.SelectBy(x => x.Id == id, _columns).Single();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult List(int employeeId)
        {
            var data = _bankAccountRepository.QueryBy(x => x.EmployeeId == employeeId && x.DeletedDate == null, _columns).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        // [AutoValidate]
        public JsonResult Save(BankAccountViewModel model)
        {
            try
            {
                _bankAccountService.Save(model.ToEntity());
                _employeeService.CalculateByPercentage(_employeeRepository.Find(model.EmployeeId));
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                string err = ex.Message + " - " + ex.StackTrace + " - " + (ex.InnerException != null ? ex.InnerException.ToString() : "");
                return Json(err, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult Remove(int id)
        {
            var employeeId = _bankAccountRepository.Find(id).EmployeeId;
            var result = _bankAccountService.Remove(id, false);
            _employeeService.CalculateByPercentage(null, employeeId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}