﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tegra.HumanResources.Domain.Repository;

namespace QX3.Portal.WebSite.HumanResources.Controllers
{
    public class EmployeeVariablesTypeController : Controller
    {

        private readonly IEmployeeVariablesTypeRepository _employeeVariablesTypeRepository;

        public EmployeeVariablesTypeController(IEmployeeVariablesTypeRepository employeeVariablesTypeRepository)
        {
            this._employeeVariablesTypeRepository = employeeVariablesTypeRepository;
        }

        public JsonResult ListAll()
        {
            var entity = _employeeVariablesTypeRepository.Query().OrderBy(variableType => variableType.Name);
            return Json(entity, JsonRequestBehavior.AllowGet);
        }
    }
}