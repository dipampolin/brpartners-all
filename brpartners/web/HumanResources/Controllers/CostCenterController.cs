﻿using QX3.Portal.WebSite.Attributes;
using QX3.Portal.WebSite.HumanResources.Models;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using Tegra.DDD.Pagination;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Exceptions;
using Tegra.HumanResources.Domain.Repository;
using Tegra.HumanResources.Domain.Representations;
using Tegra.HumanResources.Domain.Service;

namespace QX3.Portal.WebSite.HumanResources.Controllers
{
    [AccessControlAuthorize("RH.CostCenter.Index")]
    public class CostCenterController : Controller
    {
        private readonly ICostCenterRepository _repository;
        private readonly ICostCenterService _service;

        public CostCenterController(ICostCenterRepository repository, ICostCenterService service)
        {
            this._repository = repository;
            this._service = service;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult ListReport()
        {
            var result = _repository.Query(c => new CostCeterViewModelByReport
            {
                Code = c.Code,
                Value = c.Name,
            }).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult List()
        {
            var result = _repository.Query(c => c.Id > 0).OrderBy(c => c.Name).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult ListAll(PaginationSettings settings)
        {
            var data = _repository.SelectPagedBy(settings, c => c.DeletedDate == null, _columns);
            data.data = data.data.OrderBy(c => c.Name).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AutoValidate]
        public JsonResult Create(CostCenterViewModel model)
        {
            try
            {
                var entity = _service.Save(model.ToEntity());
                return Json(entity, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                Response.StatusCode = 500;
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult Find(int id)
        {
            var data = _repository.SelectBy(x => x.Id == id, _columns).OrderBy(c => c.Name).Single();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult FindByName(string name)
        {
            int code = 0;
            if (int.TryParse(name, out code))
            {
                return Json(_repository.Query(c => c.Code.Contains(code.ToString())).OrderBy(c => c.Name).ToList(), JsonRequestBehavior.AllowGet);
            }
            return Json(_repository.Query(c => c.Code.Contains(code.ToString())).OrderBy(c => c.Name).ToList(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult Remove(int id = 0)
        {
            try
            {
                _service.Remove(id);
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (ForeignKeyEmployeeException ex)
            {
                Response.StatusCode = (int)StatusHttp.CONFLICT;
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        private readonly Expression<Func<CostCenter, CostCenterViewModel>> _columns = x => new CostCenterViewModel()
        {
            Id = x.Id,
            Name = x.Name,
            Code = x.Code
        };
    }
}