﻿using QX3.Portal.WebSite.Attributes;
using QX3.Portal.WebSite.HumanResources.Exceptions;
using QX3.Portal.WebSite.HumanResources.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using Tegra.DDD.Pagination;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Repository;
using Tegra.HumanResources.Domain.Service;
using Tegra.HumanResources.Services;

namespace QX3.Portal.WebSite.HumanResources.Controllers
{
    [AccessControlAuthorize("RH.Employee.Index")]
    public class EmployeeVacationController : Controller
    {
        private readonly IEmployeeVacationRepository _vacationRepository;
        private readonly IEmployeeVacationService _vacationService;
        private readonly IEmployeeService _employeeService;
        private readonly IEmployeeRepository _employeeRepository;

        public EmployeeVacationController(IEmployeeVacationRepository vacationRepository, IEmployeeVacationService vacationService, IEmployeeService employeeService, IEmployeeRepository employeeRepository)
        {
            this._vacationRepository = vacationRepository;
            this._vacationService = vacationService;
            this._employeeService = employeeService;
            this._employeeRepository = employeeRepository;
        }

        public JsonResult Create(EmployeeVacation model)
        {
            _vacationService.Create(model);
            _employeeService.CalculateByPercentage(_employeeRepository.Find(model.EmployeeId));
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public JsonResult List(int employeeId)
        {
            var result = _vacationRepository.SelectBy(c => c.EmployeeId == employeeId && c.DeletedDate == null, e => new EmployeeVacationViewModel
            {
                Id = e.Id,
                AquisitionEndDate = e.AquisitionEndDate,
                AquisitionStartDate = e.AquisitionStartDate,
                EmployeeId = e.EmployeeId,
                EnjoymentEndDate = e.EnjoymentEndDate,
                EnjoymentStartDate = e.EnjoymentStartDate,
                ReceiptDate = e.ReceiptDate,
                Value = e.Value
            }).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Find(int id)
        {
            var result = _vacationRepository.Query(c => c.Id == id).FirstOrDefault();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Remove(int id)
        {
            var employeeId = _vacationRepository.Find(id).EmployeeId;
            var result = _vacationService.Remove(id, false);
            _employeeService.CalculateByPercentage(null, employeeId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}