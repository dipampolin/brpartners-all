﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Repository;
using Tegra.HumanResources.Services;

namespace QX3.Portal.WebSite.HumanResources.Controllers
{
    public class EmployeeContractController : Controller
    {
        private readonly IEmployeeContractRepository _repository;
        private readonly IEmployeeContractService _service;
        private readonly IEmployeeService _employeeService;
        private readonly IEmployeeRepository _employeeRepository;

        public EmployeeContractController(IEmployeeContractRepository repository, IEmployeeContractService service, IEmployeeService employeeService, IEmployeeRepository employeeRepository)
        {
            this._repository = repository;
            this._service = service;
            this._employeeService = employeeService;
            this._employeeRepository = employeeRepository;
        }

        [HttpGet]
        public JsonResult List(int EmployeeId)
        {
            var result = _repository.Query(c => c.EmployeeId == EmployeeId && c.DeletedDate == null).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Create(EmployeeContract model)
        {
            var result = _service.Create(model);
            _employeeService.CalculateByPercentage(_employeeRepository.Find(model.EmployeeId));
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Find(int id)
        {
            var result = _repository.Query(c => c.Id == id).FirstOrDefault();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Remove(int id)
        {
            var employeeId = _repository.Find(id).EmployeeId;
            var result = _service.Remove(id, false);
            _employeeService.CalculateByPercentage(null, employeeId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}