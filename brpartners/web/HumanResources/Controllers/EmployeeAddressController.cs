﻿using QX3.Portal.WebSite.Attributes;
using QX3.Portal.WebSite.HumanResources.Models;
using System;
using System.Linq;
using System.Web.Mvc;
using Tegra.HumanResources.Domain.Repository;
using Tegra.HumanResources.Services;

namespace QX3.Portal.WebSite.HumanResources.Controllers
{

    [AccessControlAuthorize("RH.Employee.Index")]
    public class EmployeeAddressController : Controller
    {

        private readonly IAddressRepository _addressRepository;
        private readonly IAddressService _addressService;
        private readonly IEmployeeService _employeeService;
        private readonly IEmployeeRepository _employeeRepository;
        public EmployeeAddressController(IAddressRepository addressRepository, IAddressService addressService, IEmployeeService employeeService, IEmployeeRepository employeeRepository)
        {
            this._addressRepository = addressRepository;
            this._addressService = addressService;
            this._employeeService = employeeService;
            this._employeeRepository = employeeRepository;
        }

        [HttpGet]
        public JsonResult List(int employeeId)
        {
            var data = _addressRepository.SelectBy(e => e.EmployeeId == employeeId && e.DeletedDate == null, x => new EmployeeAddressViewModel()
            {
                Id = x.Id,
                EmployeeId = x.EmployeeId,
                CityId = x.CityId,
                City = x.City.Name,
                State = x.City.State.Name,
                Type = x.Type,
                Cep = x.CEP,
                Street = x.Street,
                Number = x.Number,
                Complement = x.Complement,
                Neighborhood = x.Neighborhood,
                Active = x.Active
            });

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult Find(int employeeId)
        {
            var data = _addressRepository.SelectBy(x => x.Id == employeeId, x => new EmployeeAddressViewModel()
            {
                Id = x.Id,
                EmployeeId = x.EmployeeId,
                CityId = x.CityId,
                StateId = x.City.StateId,
                Type = x.Type,
                Cep = x.CEP,
                Street = x.Street,
                Number = x.Number,
                Complement = x.Complement,
                Neighborhood = x.Neighborhood,
                Active = x.Active
            }).Single();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Remove(int id)
        {
            var employeeId = _addressRepository.Find(id).EmployeeId;
            var result = _addressService.Remove(id, false);
            _employeeService.CalculateByPercentage(null, employeeId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AutoValidate]
        public JsonResult CreatePost(EmployeeAddressViewModel model)
        {
            try
            {
                 _addressService.Save(model.ToEntity());
                _employeeService.CalculateByPercentage(_employeeRepository.Find(model.EmployeeId));
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                Response.StatusCode = 500;
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }
    }
}