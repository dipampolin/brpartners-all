﻿using QX3.Portal.WebSite.Attributes;
using QX3.Portal.WebSite.HumanResources.Models;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Repository;
using Tegra.HumanResources.Services;

namespace QX3.Portal.WebSite.HumanResources.Controllers
{
    [AccessControlAuthorize("RH.Employee.Index")]
    public class VehicleController : Controller
    {
        private readonly IVehicleRepository _vehicleRepository;
        private readonly IVehicleService _vehicleService;
        private readonly IEmployeeService _employeeService;
        private readonly IEmployeeRepository _employeeRepository;

        private readonly Expression<Func<EmployeeVehicle, VehicleViewModel>> _columns = x => new VehicleViewModel()
        {
            Id = x.Id,
            EmployeeId = x.EmployeeId,
            Active = x.Active,
            Brand = x.Brand,
            Model = x.Model,
            Color = x.Color,
            CarPlate = x.CarPlate
        };

        public VehicleController(IVehicleRepository vehicleRepository, IVehicleService vehicleService, IEmployeeService employeeService, IEmployeeRepository employeeRepository)
        {
            this._vehicleRepository = vehicleRepository;
            this._vehicleService = vehicleService;
            this._employeeService = employeeService;
            this._employeeRepository = employeeRepository;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult Find(int id)
        {
            var data = _vehicleRepository.SelectBy(x => x.Id == id, _columns).Single();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult List(int employeeId)
        {
            var data = _vehicleRepository.QueryBy(x => x.EmployeeId == employeeId && x.DeletedDate == null, x => new VehicleViewModel()
            {
                Id = x.Id,
                Model = x.Model,
                Brand = x.Brand,
                CarPlate = x.CarPlate,
                Color = x.Color,
                Active = x.Active
            }).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AutoValidate]
        public JsonResult Save(VehicleViewModel entity)
        {
            try
            {
                _vehicleService.Save(entity.ToEntity());
                _employeeService.CalculateByPercentage(_employeeRepository.Find(entity.EmployeeId));

                return Json(true);
            }
            catch (Exception)
            {
                Response.StatusCode = 500;
                return Json(false);
            }
        }

        public JsonResult Remove(int id)
        {
            var employeeId = _vehicleRepository.Find(id).EmployeeId;
            var result = _vehicleService.Remove(id, false);
            _employeeService.CalculateByPercentage(null, employeeId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}