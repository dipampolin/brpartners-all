﻿using QX3.Portal.WebSite.HumanResources.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Repository;
using Tegra.HumanResources.Domain.Service;
using Tegra.HumanResources.Services;

namespace QX3.Portal.WebSite.HumanResources.Controllers
{
    public class EmployeeCourseController : Controller
    {
        private readonly IEmployeeCourseRepository _courseRepository;
        private readonly IEmployeeCourseService _courseService;
        private readonly IEmployeeService _employeeService;
        private readonly IEmployeeRepository _employeeRepository;

        public EmployeeCourseController(IEmployeeCourseRepository courseRepository, IEmployeeCourseService courseService, IEmployeeService employeeService, IEmployeeRepository employeeRepository)
        {
            this._courseRepository = courseRepository;
            this._courseService = courseService;
            this._employeeService = employeeService;
            this._employeeRepository = employeeRepository;
        }

        [HttpGet]
        public JsonResult List(int EmployeeId)
        {
            var result = _courseRepository.QueryBy(c => c.EmployeeId == EmployeeId && c.DeletedDate == null, c => new EmployeeCourseViewModel
            {
                Id = c.Id,
                Name = c.Course.Name,
                CourseId = c.CourseId,
                Deadline = c.Deadline,
                EmployeeId = c.EmployeeId,
                EndDate = c.EndDate,
                Required = c.Required,
                StartDate = c.StartDate,
                Value = c.Value
            }).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Create(CourseEmployee model)
        {
            _courseService.Create(model);
            _employeeService.CalculateByPercentage(_employeeRepository.Find(model.EmployeeId));
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Find(int id)
        {
            var result = _courseRepository.Query(c => c.Id == id).FirstOrDefault();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Remove(int id)
        {
            var employeeId = _courseRepository.Find(id).EmployeeId;
            var result = _courseService.Remove(id, false);
            _employeeService.CalculateByPercentage(null, employeeId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}