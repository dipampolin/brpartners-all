﻿using QX3.Portal.WebSite.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tegra.HumanResources.Domain.Repository;

namespace QX3.Portal.WebSite.HumanResources.Controllers
{
    public class BenefitTypeController : Controller
    {

        private readonly IBenefitTypeRepository _benefitTypeRepository;

        public BenefitTypeController(IBenefitTypeRepository benefitTypeRepository)
        {
            this._benefitTypeRepository = benefitTypeRepository;
        }

        public JsonResult ListAll()
        {
            var data = _benefitTypeRepository.Query().OrderBy(x => x.Name);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

    }
}