﻿using QX3.Portal.WebSite.Attributes;
using QX3.Portal.WebSite.HumanResources.Models;
using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Repository;

namespace QX3.Portal.WebSite.HumanResources.Controllers
{

    [AccessControlAuthorize("RH.Employee.Index")]
    public class EmployeeCostCenterController : Controller
    {

        private readonly IEmployeeCostCenterRepository _employeeCostCenterRepository;

        public EmployeeCostCenterController(IEmployeeCostCenterRepository employeeCostCenterRepository)
        {
            this._employeeCostCenterRepository = employeeCostCenterRepository;
        }

        [HttpGet]
        public JsonResult List(int employeeId)
        {
            var data = _employeeCostCenterRepository.Query(x => x.EmployeeId == employeeId && x.DeletedDate == null)
                .Include(x => x.CostCenter).OrderByDescending(c => c.StartDate).ToList().ConvertAll(x => new EmployeeDepartmentViewModel()
                {
                    Id = x.Id,
                    EmployeeId = x.EmployeeId,
                    CostCenterId = x.CostCenterId,
                    CostCenterName = x.CostCenter.Name,
                    StartDate = x.StartDate,
                    EndDate = x.EndDate,
                    Active = x.IsActive()
                });

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult Find(int id)
        {
            var data = _employeeCostCenterRepository.Query(x => x.Id == id)
                .Include(x => x.CostCenter).ToList().ConvertAll(x => new EmployeeDepartmentViewModel()
                {
                    Id = x.Id,
                    EmployeeId = x.EmployeeId,
                    CostCenterId = x.CostCenterId,
                    CostCenterName = x.CostCenter.Name,
                    StartDate = x.StartDate,
                    EndDate = x.EndDate,
                    Active = x.IsActive()
                }).Single();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Remove(int id)
        {
            var result = _employeeCostCenterRepository.Remove(id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AutoValidate]
        public JsonResult CreatePost(EmployeeDepartmentViewModel model)
        {
            try
            {
                EmployeeCostCenter c = null;

                var data = _employeeCostCenterRepository.Query(x => x.EmployeeId == model.EmployeeId && x.DeletedDate == null)
                .Include(x => x.CostCenter).ToList();

                bool hasActive = false;

                foreach (var item in data)
                {
                    if (item.Id == model.Id)
                        c = item;

                    if (item.IsActive() && item.Id != model.Id)
                        hasActive = true;
                }

                if (hasActive)
                {
                    Response.StatusCode = 409;
                    return Json(false, JsonRequestBehavior.AllowGet);
                }

                try // if (model.Id = 0
                {
                    var id = _employeeCostCenterRepository.InsertOrUpdate(model.ToEntity(), model.Id);
                    return Json(id, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    c.StartDate = model.StartDate;
                    c.EndDate = model.EndDate;
                    c.CostCenterId = model.CostCenterId;

                    var id = _employeeCostCenterRepository.Update(c);
                    return Json(id, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetActiveEmployeeCostCenter(int id)
        {
            var result = _employeeCostCenterRepository.Query(c => c.EmployeeId == id).Include(c => c.CostCenter).OrderByDescending(o => o.StartDate).FirstOrDefault();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}