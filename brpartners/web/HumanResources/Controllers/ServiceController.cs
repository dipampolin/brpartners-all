﻿using QX3.Portal.WebSite.Util;
using QX3.Spinnex.Common.Services.Mail;
using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Tegra.HumanResources.Domain.Repository;
using Tegra.HumanResources.Domain.Representations;
using Tegra.HumanResources.Domain.Service;
using Tegra.HumanResources.Domain.Util;

namespace QX3.Portal.WebSite.HumanResources.Controllers
{
    public class ServiceController : Controller
    {
        private readonly IAsyncNotificationService _asyncNotificationService;
        private readonly INotificationService _notificationService;
        private readonly INotificationDestinationRepository _destinationRepository;
        private readonly ITemplateGenerator _generator;

        private readonly IEmployeeRepository _employeeRepository;
        private readonly IContactRepository _contactRepository;

        public ServiceController(IAsyncNotificationService asyncNotificationService,
            INotificationService notificationService,
            INotificationDestinationRepository destinationRepository,
            ITemplateGenerator generator,
            IEmployeeRepository employeeRepository,
            IContactRepository contactRepository)
        {
            this._asyncNotificationService = asyncNotificationService;
            this._destinationRepository = destinationRepository;
            this._notificationService = notificationService;
            this._generator = generator;
            this._employeeRepository = employeeRepository;
            this._contactRepository = contactRepository;
        }

        [HttpPost]
        public JsonResult SendNotifications_old()
        {
            try
            {
                _asyncNotificationService.SendNotifications();

                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                Response.StatusCode = 400;
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult SendNotifications()
        {
            try
            {
                // _asyncNotificationService.SendNotifications();

                //if (DateTime.Now.Hour < 9 && DateTime.Now.Hour > 18)
                //{
                //    return Json(true, JsonRequestBehavior.AllowGet);
                //}

                _destinationRepository.Query(destination =>
                    destination.Notification.Active &&
                        (destination.LastSent == null ||
                        (destination.Notification.Frequency == Frequency.EVERY_DAY.ToString() && DbFunctions.DiffDays(destination.LastSent, DateTime.Today) >= 1) ||
                        (destination.Notification.Frequency == Frequency.EVERY_MONTH.ToString() && destination.LastSent.Value.Month != DateTime.Today.Month) ||
                        (destination.Notification.Frequency == Frequency.EVERY_THREE_DAYS.ToString() && DbFunctions.DiffDays(destination.LastSent, DateTime.Today) >= 3) ||
                        (destination.Notification.Frequency == Frequency.EVERY_FIFTEEN_DAYS.ToString() && DbFunctions.DiffDays(destination.LastSent, DateTime.Today) >= 15) ||
                        (destination.Notification.Frequency == Frequency.EVERY_WEEK.ToString() && DbFunctions.DiffDays(destination.LastSent, DateTime.Today) >= 7))
               ).Include(d => d.Notification).ToList().ForEach(destination =>
               {
                    var model = _notificationService.GenerateNotification(destination.Notification.Query);

                    if (model.Rows.Count == 0)
                    {
                        return;
                    }

                    var emailBody = _generator.Generate(model);

                   //var email = new SendEmail
                   //{
                   //    To = destination.Email,
                   //    Body = emailBody,
                   //    From = "site.qx3@qx3.com.br",
                   //    Subject = destination.Notification.Name
                   //};

                    var mail = new SendMail
                    {
                        EmailTo = destination.Email,
                        Body = emailBody,
                        Subject = destination.Notification.Name
                    };

                    try
                    {
                        // _notificationService.Send(email);
                        mail.Send();

                        destination.LastSent = DateTime.Now;

                        _destinationRepository.Update(destination);
                    }
                    catch (Exception e)
                    {
                        System.Diagnostics.Debug.WriteLine(e.Message);
                    }

               });

                return Json("Ok", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = string.Format("{0} - {1} - {2}",
                   ex.Message,
                   ex.InnerException != null ? ex.InnerException.ToString() : "",
                   ex.StackTrace);

                Response.StatusCode = 400;
                return Json(error, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DataMasking()
        {
            try
            {
                var clients = _employeeRepository.All();

                int i = 1;

                foreach (var client in clients)
                {
                    client.Name = "Funcionário " + i.ToString();
                    client.Cpf = RandomCpf.Generate();

                    var contacts = _contactRepository.SelectBy(e => e.EmployeeId == client.Id);

                    foreach (var contact in contacts)
                    {
                        if (contact.Type.Contains("mail"))
                        {
                            contact.Value = "funcionario" + i.ToString() + "@email.com";
                            _contactRepository.Update(contact);
                        }
                    }

                    _employeeRepository.Update(client);

                    i = i + 1;
                }

                return Json("Ok", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string error = string.Format("{0} - {1} - {2}",
                   ex.Message,
                   ex.InnerException != null ? ex.InnerException.ToString() : "",
                   ex.StackTrace);

                Response.StatusCode = 400;
                return Json(error, JsonRequestBehavior.AllowGet);
            }
        }
    }
}