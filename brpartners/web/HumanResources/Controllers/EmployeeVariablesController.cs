﻿using QX3.Portal.WebSite.HumanResources.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Repository;
using Tegra.HumanResources.Domain.Service;
using Tegra.HumanResources.Services;

namespace QX3.Portal.WebSite.HumanResources.Controllers
{
    public class EmployeeVariablesController : Controller
    {
        private readonly IEmployeeVariablesRepository _variableRepository;
        private readonly IEmployeeVariablesService _variableService;
        private readonly IEmployeeService _employeeService;
        private readonly IEmployeeRepository _employeeRepository;

        public EmployeeVariablesController(IEmployeeVariablesRepository variableRepository, IEmployeeVariablesService variableService, IEmployeeService employeeService, IEmployeeRepository employeeRepository)
        {
            this._variableRepository = variableRepository;
            this._variableService = variableService;
            this._employeeService = employeeService;
            this._employeeRepository = employeeRepository;
        }

        public JsonResult Create(EmployeeVariables model)
        {
            _variableService.Create(model);
            _employeeService.CalculateByPercentage(_employeeRepository.Find(model.EmployeeId));
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public JsonResult List(int employeeId)
        {
            var result = _variableRepository.SelectBy(c => c.EmployeeId == employeeId && c.DeletedDate == null,
                employeeVariables => new EmployeeVariablesViewModel()
                {
                    Date = employeeVariables.Date,
                    Description = employeeVariables.Description,
                    EmployeeId = employeeVariables.EmployeeId,
                    EmployeeVariablesTypeId = employeeVariables.EmployeeVariablesTypeId,
                    Id = employeeVariables.Id,
                    Type = employeeVariables.Type.Name,
                    Value = employeeVariables.Value
                }).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Find(int id)
        {
            var result = _variableRepository.Query(c => c.Id == id).FirstOrDefault();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Remove(int id)
        {
            var employeeId = _variableRepository.Find(id).EmployeeId;
            var result = _variableService.Remove(id, false);
            _employeeService.CalculateByPercentage(null, employeeId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}