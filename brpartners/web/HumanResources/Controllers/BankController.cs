﻿using QX3.Portal.WebSite.Attributes;
using QX3.Portal.WebSite.HumanResources.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tegra.DDD.Pagination;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Exceptions;
using Tegra.HumanResources.Domain.Repository;
using Tegra.HumanResources.Domain.Representations;
using Tegra.HumanResources.Domain.Service;

namespace QX3.Portal.WebSite.HumanResources.Controllers
{
    public class BankController : Controller
    {
        private readonly IBankRepository _bankRepository;
        private readonly IBankService _bankService;

        public BankController(IBankRepository bankRepository, IBankService bankService)
        {
            this._bankRepository = bankRepository;
            this._bankService = bankService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult List()
        {
            var result = _bankRepository.Query(c => c.DeletedDate == null).OrderBy(c => c.Name).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ListCombo()
        {
            var result = _bankRepository.Query(c => c.DeletedDate == null && c.Active).OrderBy(c => c.Name).ToList();

            if (result != null && result.Count > 0)
            {
                foreach (var item in result)
                {
                    if (!string.IsNullOrEmpty(item.Code))
                        item.Name = string.Format("{0} ({1})", item.Name, item.Code);
                }
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult ListAll(PaginationSettings settings)
        {
            var result = _bankRepository.SelectPagedBy(settings, c => c.DeletedDate == null, bank => new BankViewModel
            {
                Active = bank.Active,
                CreatedDate = bank.CreatedDate,
                Id = bank.Id,
                Code = bank.Code,
                Name = bank.Name
            });

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //public JsonResult GetActiveEmployeeBank(int id)
        //{
        //    var result = _employeeRoleRepository.Query(c => c.EmployeeId == id).Include(c => c.Role).OrderByDescending(o => o.StartDate).FirstOrDefault();
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}

        [HttpPost]
        [AutoValidate]
        public JsonResult Save(BankViewModel bankViewModel)
        {
            try
            {
                _bankService.Save(bankViewModel.toEntity());
                return Json(true, JsonRequestBehavior.AllowGet);
            } catch (Exception ex)
            {
                Response.StatusCode = 500;
                return Json(false, JsonRequestBehavior.AllowGet);
            }   
        }

        [HttpGet]
        public JsonResult Find(int id)
        {
            var data = _bankRepository.SelectBy(bank => bank.Id == id,
                bank => new BankViewModel
                {
                    Active = bank.Active,
                    Id = bank.Id,
                    Code = bank.Code,
                    Name = bank.Name
                }).OrderBy(bank => bank.Name).Single();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult FindByName(string name)
        {
            List<Bank> result = null;

            int code = 0;
            if (int.TryParse(name, out code))
                result = _bankRepository.Query(c => c.Code.Contains(code.ToString())).OrderBy(c => c.Name).ToList();
            else
                result = _bankRepository.Query(c => c.Name.Contains(name)).OrderBy(c => c.Name).ToList();

            if (result != null && result.Count > 0)
            {
                foreach (var item in result)
                {
                    if (!string.IsNullOrEmpty(item.Code))
                        item.Name = string.Format("{0} ({1})", item.Name, item.Code);
                }
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult Remove(int id = 0)
        {
            try
            {
                _bankRepository.Remove(id);
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (ForeignKeyEmployeeException ex)
            {
                Response.StatusCode = (int)StatusHttp.CONFLICT;
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }
    }
}