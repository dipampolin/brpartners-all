﻿using QX3.Portal.WebSite.Attributes;
using QX3.Portal.WebSite.HumanResources.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tegra.DDD.Pagination;
using Tegra.DDD.Repository;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Repository;
using Tegra.HumanResources.Domain.Representations;
using Tegra.HumanResources.Domain.Service;

namespace QX3.Portal.WebSite.HumanResources.Controllers
{
    [AccessControlAuthorize("RH.ReportViewer.Index")]
    public class ReportViewerController : Controller
    {
        private readonly IReportRepository _repository;
        private readonly IReportService _service;

        public ReportViewerController(IReportRepository repository, IReportService service)
        {
            this._repository = repository;
            this._service = service;
        }
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult Find(int id)
        {
            var data = _repository.SelectBy(x => x.Id == id && x.Active == true, r => new ReportViewModel
            {
                Id = r.Id,
                Name = r.Name,
                Description = r.Description,
                Query = r.Query,
                Active = r.Active,
                Items = r.Items.Select(ri => new ReportItemViewModel
                {
                    Id = ri.Id,
                    FieldAlias = ri.FieldAlias,
                    Field = ri.Field,
                    Table = ri.Table,
                    TableAlias = ri.TableAlias,

                }).ToList()
            }).FirstOrDefault();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult List(PaginationSettings settings)
        {
            var data = _repository.SelectPagedBy(settings, r => r.Active == true, r => new ReportViewModel
            {
                Id = r.Id,
                Name = r.Name,
                Description = r.Description,
                Query = r.Query,
                Active = r.Active,
                Items = r.Items.Select(ri => new ReportItemViewModel
                {
                    Id = ri.Id,
                    FieldAlias = ri.FieldAlias,
                    Field = ri.Field,
                    Table = ri.Table,
                    TableAlias = ri.TableAlias,

                }).ToList()
            });
            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}