﻿using QX3.Portal.WebSite.Attributes;
using QX3.Portal.WebSite.HumanResources.Models;
using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Repository;
using Tegra.HumanResources.Services;

namespace QX3.Portal.WebSite.HumanResources.Controllers
{

    [AccessControlAuthorize("RH.Employee.Index")]
    public class EmployeeTypeController : Controller
    {

        private readonly IEmployeeTypeRepository _employeeTypeRepository;
        private readonly IEmployeeService _employeeService;
        private readonly IEmployeeRepository _employeeRepository;

        public EmployeeTypeController(IEmployeeTypeRepository _employeeTypeRepository, IEmployeeService employeeService, IEmployeeRepository employeeRepository)
        {
            this._employeeTypeRepository = _employeeTypeRepository;
            this._employeeService = employeeService;
            this._employeeRepository = employeeRepository;
        }

        [HttpGet]
        public JsonResult List(int employeeId)
        {
            var data = _employeeTypeRepository.Query(e => e.EmployeeId == employeeId && e.DeletedDate == null).OrderByDescending(c => c.StartDate).ToList().ConvertAll(x => new EmployeeTypeViewModel()
                {
                    Id = x.Id,
                    EmployeeId = x.EmployeeId,
                    TypeId = x.TypeId.ToString(),
                    TypeName = x.TypeDescription(),
                    StartDate = x.StartDate,
                    EndDate = x.EndDate,
                    Active = x.IsActive()
                });

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult Find(int id)
        {
            var data = _employeeTypeRepository.Query(x => x.Id == id).ToList().ConvertAll(x => new EmployeeTypeViewModel()
                {
                    Id = x.Id,
                    EmployeeId = x.EmployeeId,
                    TypeId = x.TypeId.ToString(),
                    TypeName = x.TypeDescription(),
                    StartDate = x.StartDate,
                    EndDate = x.EndDate,
                    Active = x.IsActive()
                }).Single();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Remove(int id)
        {
            var result = _employeeTypeRepository.Remove(id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AutoValidate]
        public JsonResult CreatePost(EmployeeTypeViewModel model)
        {
            try
            {
                EmployeeType c = null;

                var data = _employeeTypeRepository.Query(x => x.EmployeeId == model.EmployeeId && x.DeletedDate == null).ToList();

                bool hasActive = false;

                foreach (var item in data)
                {
                    if (item.Id == model.Id)
                        c = item;

                    if (item.IsActive() && item.Id != model.Id)
                        hasActive = true;
                }

                if (hasActive)
                {
                    Response.StatusCode = 409;
                    return Json(false, JsonRequestBehavior.AllowGet);
                }

                try // if (model.Id = 0
                {
                    var id = _employeeTypeRepository.InsertOrUpdate(model.ToEntity(), model.Id);
                    _employeeService.CalculateByPercentage(_employeeRepository.Find(model.EmployeeId));

                    return Json(true, JsonRequestBehavior.AllowGet);
                }
                catch (Exception)
                {
                    c.StartDate = model.StartDate;
                    c.EndDate = model.EndDate;
                    c.TypeId = Convert.ToInt32(model.TypeId);

                    var id = _employeeTypeRepository.Update(c);
                    return Json(true, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception)
            {
                Response.StatusCode = 500;
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetActiveEmployeeType(int id)
        {
            var result = _employeeTypeRepository.Query(c => c.EmployeeId == id).OrderByDescending(o => o.StartDate).FirstOrDefault();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}