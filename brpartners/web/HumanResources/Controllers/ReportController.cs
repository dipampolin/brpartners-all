﻿using QX3.Portal.WebSite.Attributes;
using QX3.Portal.WebSite.HumanResources.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Tegra.DDD.Pagination;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Repository;
using Tegra.HumanResources.Domain.Representations;
using Tegra.HumanResources.Domain.Service;

namespace QX3.Portal.WebSite.HumanResources.Controllers
{
    [AccessControlAuthorize("RH.Report.Index")]
    public class ReportController : Controller
    {
        private readonly IReportRepository _repository;
        private readonly IReportService _service;

        public ReportController(IReportRepository repository, IReportService service)
        {
            this._repository = repository;
            this._service = service;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetAlias()
        {
            var data = _service.GetReportAlias().OrderBy(c => c.TableAlias).ThenBy(c => c.FieldAlias).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveQuery(List<ReportByAlias> alias, Report report)
        {
            try
            {
                var reportQuery = ReportByAlias.Factory(alias ?? new List<ReportByAlias>());
                report.Items = reportQuery;
                report.Query = _service.GenerateQuery(report);

                if (!_service.ExecuteQuery(report.Query))
                {
                    var tables = report.Items.Select(c => c.TableAlias).Distinct().Aggregate( (i, j) => i + " e " + j);
                    throw new Exception("Não foi possível criar o relacionamento entre: " + tables);
                }

                _service.Save(report);
                return Json(new { Status = 200, reportId = report.Id }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public JsonResult AddQueryFilter(List<ReportByAlias> filters, int reportId)
        {
            try
            {
                var reportQuery = ReportByAlias.FactoryFilter(filters ?? new List<ReportByAlias>());

                var report = _service.CreateFilters(reportQuery, reportId);

                var result = _service.UpdateQuery(report, reportQuery);
                return Json(result == null ? new { Message = "O Filtro salvo não trouxe nenhum resultado!" } : new { Message = "Ok" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult Find(int id)
        {
            var data = _repository.SelectBy(x => x.Id == id, r => new ReportViewModel
            {
                Id = r.Id,
                Name = r.Name,
                Description = r.Description,
                Query = r.Query,
                Active = r.Active,
                Items = r.Items.Select(ri => new ReportItemViewModel
                {
                    Id = ri.Id,
                    FieldAlias = ri.FieldAlias,
                    Field = ri.Field,
                    Table = ri.Table,
                    TableAlias = ri.TableAlias,
                    Type = ri.Type

                }).ToList()
            }).FirstOrDefault();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetFiltrable(int id)
        {
            var reportList = _repository.Query(c => c.Id == id).Include(c => c.Items).SelectMany(c => c.Items).Select(c => c.Table).Distinct();

            var finalList = _service.GetReportFilter(reportList.ToList(), id);
            return Json(finalList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult List(PaginationSettings settings)
        {
            var data = _repository.SelectPaged(settings, r => new ReportViewModel
            {
                Id = r.Id,
                Name = r.Name,
                Description = r.Description,
                Query = r.Query,
                Active = r.Active,
                Items = r.Items.Select(ri => new ReportItemViewModel
                {
                    Id = ri.Id,
                    FieldAlias = ri.FieldAlias,
                    Field = ri.Field,
                    Table = ri.Table,
                    TableAlias = ri.TableAlias,
                    Type = ri.Type

                }).ToList()
            });

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ShowReport(string reportId, bool isReport = false, string erro = "")
        {
            Report report = new Report();
            DataTable table = new DataTable();
            try
            {
                int id;
                if (int.TryParse(reportId, out id))
                {
                    report = _repository.Find(id);
                }
                table = _service.GenerateReport(report);

                CreateReport(isReport, report);

                return View(new ShowReportViewModel
                {
                    Nome = report.Name,
                    Table = table,
                    ReportId = report.Id
                });
            }
            catch (Exception)
            {
                ViewBag.Erro = "Não foi possível exibir o relatório!";
            }
            return View();
        }

        public ActionResult Remove(int reportId)
        {
            bool wasDeleted = _service.Remove(reportId);
            return Json(wasDeleted, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDateRoles()
        {
            var result = DateRole.GetDictionaryRoles();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        private void CreateReport(bool isReport, Report report)
        {
            if (isReport)
            {
                ViewBag.IsReport = isReport;
                Response.ContentType = "application/vnd.ms-excel;charset=utf-8";
                var date = DateTime.Now;
                Response.AddHeader("Content-Disposition",
                        string.Format("attachment; filename={0}-{1}-{2}.xls",
                        report.Name,
                        date.Day.ToString("00"),
                        date.Month.ToString("00")));
            }
        }
    }
}