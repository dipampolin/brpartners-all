﻿using QX3.Portal.WebSite.Attributes;
using QX3.Portal.WebSite.HumanResources.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tegra.HumanResources.Domain.Repository;

namespace QX3.Portal.WebSite.HumanResources.Controllers
{

    [AccessControlAuthorize("RH.Home.Index")]
    public class DashboardController : Controller
    {

        private IEmployeeRepository _repository;
        private IEmployeeVacationRepository _vacationRepository;
        private IEmployeeCostCenterRepository _costCenterRepository;
        private IEmployeeCompanyRepository _companyRepository;

        public DashboardController(IEmployeeRepository service, IEmployeeVacationRepository vacationRepository, IEmployeeCostCenterRepository costCenterRepository, IEmployeeCompanyRepository companyRepository)
        {
            this._repository = service;
            this._vacationRepository = vacationRepository;
            this._costCenterRepository = costCenterRepository;
            this._companyRepository = companyRepository;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View(new DashboardViewModel()
            {
                ActiveEmployees = _repository.Query(e => e.Active && e.DeletedDate == null).Count(),
                VacationEmployees = _vacationRepository.Query(v => v.Employee.Active && v.Employee.DeletedDate == null
                    && v.DeletedDate == null && v.EnjoymentStartDate >= DbFunctions.AddDays(DateTime.Today, -30) && v.EnjoymentEndDate <= DbFunctions.AddDays(DateTime.Today, 30)).GroupBy(x => x.EmployeeId).Count(),
                NewEmployees = _repository.Query(e => e.Active && e.DeletedDate == null && e.CreatedDate >= DbFunctions.AddDays(DateTime.Today, -30)).Count(),
                InactiveEmployees = _repository.Query(e => !e.Active && e.DeletedDate == null).Count(),
                DepartmentDistribution = _costCenterRepository.Query(d => d.DeletedDate == null && d.Employee.Active && d.Employee.DeletedDate == null).GroupBy(d => d.CostCenter.Name)
                 .Select(d => new { d.Key, Count = d.Count() }).ToDictionary(e => e.Key, e => e.Count)
            });
        }

        public JsonResult ListCompanyDistribution()
        {
            var result = _companyRepository.Query(c => c.DeletedDate == null && c.Employee.Active && c.Employee.DeletedDate == null && c.StartDate.Year <= DateTime.Today.Year && (!c.EndDate.HasValue || c.EndDate.Value.Year >= DateTime.Today.Year))
                .Select(c => new EmployeeCompanyViewModel { CompanyName = c.Company.Name, StartDate = c.StartDate, EndDate = c.EndDate }).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        public JsonResult ListEmployeesWithNearBirthdays()
        {
            var result = _repository.QueryBy(e => e.Active && e.DeletedDate == null && e.Birthdate < DateTime.Today
                     && ((e.Birthdate.Month == DateTime.Today.Month && e.Birthdate.Day >= DateTime.Today.Day) || (DateTime.Today.Month == 12 && e.Birthdate.Month == 1) || (DateTime.Today.Month != 12 && e.Birthdate.Month == DateTime.Today.Month + 1)),
                    e => new EmployeeViewModel
                    {
                        Name = e.Name,
                        Birthdate = e.Birthdate
                    }).OrderBy(e => DateTime.Today.Month == e.Birthdate.Month ? e.Birthdate.Day - DateTime.Today.Day : 31 + e.Birthdate.Day)
                        .ThenBy(e => e.Name).Take(10).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}