﻿using QX3.Portal.WebSite.Attributes;
using QX3.Portal.WebSite.HumanResources.Models;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Repository;
using Tegra.HumanResources.Services;

namespace QX3.Portal.WebSite.HumanResources.Controllers
{

    [AccessControlAuthorize("RH.Employee.Index")]
    public class RelationshipController : Controller
    {

        private readonly IRelationshipRepository _relationshipRepository;
        private readonly IRelationshipService _relationshipService;
        private readonly IEmployeeService _employeeService;
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IBenefitRepository _benefitRepository;

        public RelationshipController(IRelationshipRepository RelationshipRepository, IRelationshipService RelationshipService, IEmployeeService employeeService, IEmployeeRepository employeeRepository, IBenefitRepository benefitRepository)
        {
            this._relationshipRepository = RelationshipRepository;
            this._relationshipService = RelationshipService;
            this._employeeService = employeeService;
            this._employeeRepository = employeeRepository;
            this._benefitRepository = benefitRepository;
        }


        private readonly Expression<Func<EmployeeRelationship, EmployeeRelationshipViewModel>> _columns = x => new EmployeeRelationshipViewModel()
        {
            Id = x.Id,
            EmployeeId = x.EmployeeId,
            Cpf = x.Cpf,
            EndDate = x.EndDate,
            HealthPlanId = x.HealthPlan.Id,
            IR = x.IR,
            Name = x.Name,
            OdontologyPlanId = x.OdontologyPlan == null ? null : x.OdontologyPlanId,
            Relationship = x.Relationship,
            HealthPlan = x.HealthPlan.Description,
            OdontologyPlan = x.OdontologyPlan.Description,
            StartDate = x.StartDate,
            UseHealthPlan = x.UseHealthPlan,
            UseOdontologyPlan = x.UseOdontologyPlan,
            Birthdate = x.Birthdate ?? null
        };

        [HttpGet]
        public JsonResult List(int employeeId)
        {
            var data = _relationshipRepository.SelectBy(e => e.EmployeeId == employeeId && e.DeletedDate == null, _columns);

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult Find(int id)
        {
            var data = _relationshipRepository.SelectBy(x => x.Id == id, _columns).Single();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AutoValidate]
        public JsonResult Save(EmployeeRelationshipViewModel model)
        {
            try
            {
                var entity = model.ToEntity();

                _relationshipService.Save(entity);
                _employeeService.CalculateByPercentage(_employeeRepository.Find(model.EmployeeId));
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Response.StatusCode = 500;
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult Remove(int id)
        {
            var employeeId = _relationshipRepository.Find(id).EmployeeId;
            var result = _relationshipService.Remove(id, false);
            _employeeService.CalculateByPercentage(null, employeeId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetHealthPlan()
        {
            var result = _benefitRepository.Query(b => b.Type.Category == BenefitCategory.SAUDE).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDentalPlan()
        {
            var result = _benefitRepository.Query(b => b.Type.Category == BenefitCategory.ODONTOLOGICO).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}