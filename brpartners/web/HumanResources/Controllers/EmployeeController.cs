﻿using Newtonsoft.Json;
using QX3.Portal.WebSite.Attributes;
using QX3.Portal.WebSite.HumanResources.Models;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Serialization;
using Tegra.DDD.Pagination;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Repository;
using Tegra.HumanResources.Services;

namespace QX3.Portal.WebSite.HumanResources.Controllers
{

    [AccessControlAuthorize("RH.Employee.Index")]
    public class EmployeeController : Controller
    {

        private readonly IEmployeeRepository _employeeRepository;
        private readonly IEmployeeService _employeeService;
        private readonly IEmployeeCostCenterRepository _employeeDepartmentRepository;
        private readonly ICostCenterRepository _costCenterRepository;
        private readonly IRoleRepository _roleRepository;
        private readonly ICompanyRepository _companyRepository;
        private readonly IEmployeeRoleRepository _employeeRoleRepository;
        private readonly IEmployeeCompanyRepository _employeeCompanyRepository;
        private readonly IEmployeeCostCenterRepository _employeeCostCenterRepository;

        private readonly IAddressRepository _addressRepository;
        private readonly IContactRepository _contactRepository;
        private readonly IVehicleRepository _vehicleRepository;
        private readonly IBankAccountRepository _bankAccountRepository;
        private readonly IRelationshipRepository _relationshipRepository;
        private readonly IEmployeeCourseRepository _courseRepository;
        private readonly ISalaryHistoryRepository _salaryHistoryRepository;
        private readonly IEmployeeBenefitRepository _benefitrepository;
        private readonly IEmployeeVariablesRepository _variableRepository;
        private readonly IEmployeeVacationRepository _vacationRepository;
        private readonly IEmployeeAttachmentRepository _attachmentRepository;
        private readonly IEmployeeContractRepository _contractRepository;

        public EmployeeController
            (IEmployeeRepository employeeRepository,
            IEmployeeService employeeService,
            IEmployeeCostCenterRepository employeedepartmentRepository,
            ICostCenterRepository costcenterRepository,
            IRoleRepository roleRepository,
            ICompanyRepository companyRepository,
            IEmployeeRoleRepository employeeRoleRepository,
            IEmployeeCompanyRepository employeeCompanyRepository,
            IEmployeeCostCenterRepository employeeCostCenterRepository,
            IAddressRepository addressRepository,
            IContactRepository contactRepository,
            IVehicleRepository vehicleRepository,
            IBankAccountRepository bankAccountRepository,
            IRelationshipRepository relationshipRepository,
            IEmployeeCourseRepository courseRepository,
            ISalaryHistoryRepository salaryHistoryRepository,
            IEmployeeBenefitRepository benefitrepository,
            IEmployeeVariablesRepository variableRepository,
            IEmployeeVacationRepository vacationRepository,
            IEmployeeAttachmentRepository attachmentRepository,
            IEmployeeContractRepository contractRepository
            )
        {
            this._employeeRepository = employeeRepository;
            this._employeeService = employeeService;
            this._employeeDepartmentRepository = employeedepartmentRepository;
            this._costCenterRepository = costcenterRepository;
            this._roleRepository = roleRepository;
            this._companyRepository = companyRepository;
            this._employeeRoleRepository = employeeRoleRepository;
            this._employeeCompanyRepository = employeeCompanyRepository;
            this._employeeCostCenterRepository = employeeCostCenterRepository;
            this._addressRepository = addressRepository;
            this._contactRepository = contactRepository;
            this._vehicleRepository = vehicleRepository;
            this._bankAccountRepository = bankAccountRepository;
            this._relationshipRepository = relationshipRepository;
            this._courseRepository = courseRepository;
            this._salaryHistoryRepository = salaryHistoryRepository;
            this._benefitrepository = benefitrepository;
            this._variableRepository = variableRepository;
            this._vacationRepository = vacationRepository;
            this._attachmentRepository = attachmentRepository;
            this._contractRepository = contractRepository;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Create(int id = 0)
        {

            var data = new EmployeeViewModel();

            if (id > 0)
            {
                data = _employeeRepository.SelectBy(x => x.Id == id, _convert).Single();
            }

            return View(data);
        }

        [HttpGet]
        public JsonResult Find(int id)
        {
            var data = _employeeRepository.SelectBy(x => x.Id == id, _convert).Single();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetPictureEmployee(int id = 0)
        {
            if (id != 0)
            {
                var data = _employeeRepository.SelectBy(employee => employee.Id == id,
                    employee => employee.Picture).SingleOrDefault();

                if (!string.IsNullOrEmpty(data))
                {
                    var parts = data.Split(',')[1];
                    var image = Convert.FromBase64String(parts);
                    return File(image, "image/jpeg");
                }
            }

            return File(Server.MapPath("/HumanResources/Static/img/") + "employee-profile.jpg", "image/jpg");
        }

        private Expression<Func<Employee, EmployeeViewModel>> _convert = x =>
             new EmployeeViewModel()
             {
                 Id = x.Id,
                 Name = x.Name,
                 Initials = x.Initials,
                 Active = x.Active,
                 AdmissionDate = x.AdmissionDate,
                 Birthdate = x.Birthdate,
                 CivilStatusId = x.CivilStatusId,
                 CostCenterId = x.CostCenterId,
                 Cpf = x.Cpf,
                 EducationalStageId = x.EducationalStageId,
                 EmploymentBooklet = x.EmploymentBooklet,
                 EmploymentBookletSerie = x.EmploymentBookletSerie,
                 EmploymentBookletUf = x.EmploymentBookletUf,
                 FatherName = x.FatherName,
                 FatherBirthdate = x.FatherBirthdate,
                 Foreigner = x.Foreigner,
                 Gender = x.Gender,
                 IdentificationNumber = x.IdentificationNumber,
                 MainPhone = x.MainPhone,
                 MotherName = x.MotherName,
                 MotherBirthdate = x.MotherBirthdate,
                 Nationality = x.Nationality,
                 Passport = x.Passport,
                 PassportDateExpedition = x.PassportDateExpedition,
                 PassportDateExpiration = x.PassportDateExpiration,
                 PassportDispatcher = x.PassportDispatcher,
                 PassportUf = x.PassportUf,
                 PassportCityId = x.PassportCityId,
                 Pis = x.Pis,
                 Registration = x.Registration,
                 Reservist = x.Reservist,
                 ReservistCategory = x.ReservistCategory,
                 ResignationDate = x.ResignationDate,
                 Rg = x.Rg,
                 RgDateExpedition = x.RgDateExpedition,
                 RgDispatcher = x.RgDispatcher,
                 Rne = x.Rne,
                 RneDateExpedition = x.RneDateExpedition,
                 RneDateExpiration = x.RneDateExpiration,
                 RneDispatcher = x.RneDispatcher,
                 SupervisorId = x.SupervisorId,
                 VoterNumberCard = x.VoterNumberCard,
                 VoterSession = x.VoterSession,
                 VoterZone = x.VoterZone,
                 CityId = x.CityId,
                 StateId = x.City != null ? x.City.StateId : ((int?)null),
                 Cnh = x.Cnh,
                 CnhCategory = x.CnhCategory,
                 CnhDateExpedition = x.CnhDateExpedition,
                 CnhDateExpiration = x.CnhDateExpiration,
                 CnhDateFirst = x.CnhDateFirst,
                 Naturalized = x.Naturalized.HasValue ? x.Naturalized.Value : false,
                 CountryOfBirth = x.CountryOfBirth,
                 CityOfBirth = x.CityOfBirth
             };

        [HttpGet]
        public JsonResult List(PaginationSettings settings)
        {
            // var data = _employeeRepository.SelectPaged(settings, x => new EmployeeViewModel(
            var data = _employeeRepository.SelectPagedBy(settings, c => c.DeletedDate == null, x => new EmployeeViewModel()
            {
                Id = x.Id,
                Name = x.Name,
                Initials = x.Initials,
                Percentage = x.Percentage,
                Active = x.Active
            });

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult FindByName(string name, int id = 0)
        {
            try
            {
                if (id > 0)
                {
                    return Json(_employeeRepository.Query(c => c.Name.StartsWith(name) && c.Id != id).Select(_convert).ToList(), JsonRequestBehavior.AllowGet);
                }

                if (int.TryParse(name, out id))
                {
                    return Json(_employeeRepository.Query(c => c.Initials.StartsWith(id.ToString())).Select(_convert).ToList(), JsonRequestBehavior.AllowGet);
                }

                return Json(_employeeRepository.Query(c => c.Name.StartsWith(name)).Select(_convert).ToList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Response.StatusCode = 500;
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [AutoValidate]
        public JsonResult CreatePost(EmployeeViewModel model)
        {
            try
            {
                model.RoleId = model.RoleIdValue == 0 ? model.RoleId : model.RoleIdValue;

                var costCenter = model.CostCenterId.HasValue ? _costCenterRepository.Find(model.CostCenterId.Value) : null;
                var role = _roleRepository.Find(model.RoleId);
                var company = _companyRepository.Find(model.CompanyId);

                if (!string.IsNullOrEmpty(model.Picture) && model.Picture == "/RH/Employee/GetPictureEmployee/0")
                    model.Picture = "";

                if (model.Id > 0)
                {
                    var currentRole = _employeeRoleRepository.Query(c => c.EmployeeId == model.Id).Include(c => c.Role).OrderByDescending(o => o.StartDate).FirstOrDefault();
                    if (currentRole != null && currentRole.Role != null && currentRole.Role.Id != model.RoleId)
                    {
                        currentRole.RoleId = role.Id;
                        _employeeRoleRepository.Update(currentRole);
                    }
                    else
                    {
                        if (currentRole == null || currentRole.Role == null)
                        {
                            EmployeeRole employeeRole = new EmployeeRole();
                            employeeRole.CreatedDate = DateTime.Now;
                            employeeRole.StartDate = model.AdmissionDate.HasValue ? model.AdmissionDate.Value : new DateTime();
                            employeeRole.EmployeeId = model.Id;
                            employeeRole.Role = role;

                            _employeeRoleRepository.Add(employeeRole);
                        }
                    }

                    var currentCompany = _employeeCompanyRepository.Query(c => c.EmployeeId == model.Id).Include(c => c.Company).OrderByDescending(o => o.StartDate).FirstOrDefault();
                    if (currentCompany != null && currentCompany.Company != null && currentCompany.Company.Id != model.CompanyId)
                    {
                        currentCompany.CompanyId = company.Id;
                        _employeeCompanyRepository.Update(currentCompany);
                    }
                    else
                    {
                        if (currentCompany == null || currentCompany.Company == null)
                        {
                            EmployeeCompany employeeCompany = new EmployeeCompany();
                            employeeCompany.CreatedDate = DateTime.Now;
                            employeeCompany.StartDate = model.AdmissionDate.HasValue ? model.AdmissionDate.Value : new DateTime();
                            employeeCompany.EmployeeId = model.Id;
                            employeeCompany.Company = company;

                            _employeeCompanyRepository.Add(employeeCompany);
                        }
                    }

                    //var currentCostCenter = _employeeCostCenterRepository.Query(c => c.EmployeeId == model.Id).Include(c => c.CostCenter).OrderByDescending(o => o.StartDate).FirstOrDefault();
                    //if (currentCostCenter != null && currentCostCenter.CostCenter != null && currentCostCenter.CostCenter.Id != model.CompanyId)
                    //{
                    //    currentCostCenter.CostCenterId = costCenter.Id;
                    //    _employeeCostCenterRepository.Update(currentCostCenter);
                    //}
                }

                Employee employee = EmployeeViewModel.ToEntity(model, costCenter, role, company);

                var entity = _employeeService.Create(employee);

                _employeeService.CalculateByPercentage(entity);

                // Se o funcionário for inativado e o usuário escolher clonar...
                if (model.Clone)
                {
                    var costCenter2 = _costCenterRepository.Find(model.CostCenterId.Value);
                    var role2 = _roleRepository.Find(model.RoleId);
                    var company2 = _companyRepository.Find(model.CompanyId);

                    Clone(model, costCenter2, role2, company2);
                }

                // Se for um funcionário novo, faz a integração com o CPN
                if (model.Id == 0)
                {

                }

                EmptyList(entity);
                Response.StatusCode = 200;

                var data = JsonConvert.SerializeObject(entity, Newtonsoft.Json.Formatting.None, new JsonSerializerSettings()
                {
                    ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                });

                return new JsonResult()
                {
                    Data = new EmployeeCreateResponseViewModel { Data = data, Id = entity.Id, Picture = entity.Picture },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    MaxJsonLength = Int32.MaxValue
                };
            }
            catch (Exception e)
            {
                Response.StatusCode = 500;
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        private readonly Expression<Func<Company, CompanyViewModel>> _columns = x => new CompanyViewModel()
        {
            Id = x.Id,
            Cnpj = x.Cnpj,
            Name = x.Name,
            Code = x.Code
        };

        private void Clone(EmployeeViewModel model, CostCenter costCenter, Role role, Company company)
        {
            try
            {
                Employee entity = new Employee();

                entity.Initials = model.Initials;
                entity.Name = model.Name;
                entity.Cpf = model.Cpf;
                entity.Gender = model.Gender;
                entity.CivilStatusId = model.CivilStatusId;
                entity.EducationalStageId = model.EducationalStageId;
                entity.Birthdate = model.Birthdate;
                entity.AdmissionDate = model.AdmissionDate;
                entity.Active = true;
                entity.Initials = model.Initials;
                entity.CostCenterId = model.CostCenterId;
                entity.EmploymentBooklet = model.EmploymentBooklet;
                entity.EmploymentBookletSerie = model.EmploymentBookletSerie;
                entity.EmploymentBookletUf = model.EmploymentBookletUf;
                entity.FatherName = model.FatherName;

                if (model.FatherBirthdate.HasValue)
                    entity.FatherBirthdate = model.FatherBirthdate.Value;
                else
                    entity.FatherBirthdate = null;

                entity.Foreigner = model.Foreigner;
                entity.MainPhone = model.MainPhone;
                entity.Picture = model.Picture;
                entity.IdentificationNumber = model.IdentificationNumber;
                entity.MotherName = model.MotherName;

                if (model.MotherBirthdate.HasValue)
                    entity.MotherBirthdate = model.MotherBirthdate.Value;
                else
                    entity.MotherBirthdate = null;

                entity.Name = model.Name;
                entity.Nationality = model.Nationality;
                entity.Passport = model.Passport;

                if (model.PassportDateExpedition.HasValue)
                    entity.PassportDateExpedition = model.PassportDateExpedition.Value;
                else
                    entity.PassportDateExpedition = null;

                if (model.PassportDateExpiration.HasValue)
                    entity.PassportDateExpiration = model.PassportDateExpiration.Value;
                else
                    entity.PassportDateExpiration = null;

                entity.PassportDispatcher = model.PassportDispatcher;
                entity.PassportUf = model.PassportUf;
                entity.PassportCityId = model.PassportCityId;
                entity.Pis = model.Pis;
                entity.Registration = model.Registration;
                entity.Reservist = model.Reservist;
                entity.ReservistCategory = model.ReservistCategory;

                if (model.ResignationDate.HasValue)
                    entity.ResignationDate = model.ResignationDate.Value;
                else
                    entity.ResignationDate = null;

                entity.Rg = model.Rg;

                if (model.RgDateExpedition.HasValue)
                    entity.RgDateExpedition = model.RgDateExpedition.Value;
                else
                    entity.RgDateExpedition = null;

                entity.RgDispatcher = model.RgDispatcher;
                entity.Rne = model.Rne;

                if (model.RneDateExpedition.HasValue)
                    entity.RneDateExpedition = model.RneDateExpedition.Value;
                else
                    entity.RneDateExpedition = null;

                if (model.RneDateExpiration.HasValue)
                    entity.RneDateExpiration = model.RneDateExpiration.Value;
                else
                    entity.RneDateExpiration = null;

                entity.RneDispatcher = model.RneDispatcher;
                entity.SupervisorId = model.SupervisorId;
                entity.VoterNumberCard = model.VoterNumberCard;
                entity.VoterSession = model.VoterSession;
                entity.VoterZone = model.VoterZone;
                entity.CityId = model.CityId;
                entity.Percentage = model.Percentage;

                entity.Cnh = model.Cnh;
                entity.CnhCategory = model.CnhCategory;

                if (model.CnhDateExpedition.HasValue)
                    entity.CnhDateExpedition = model.CnhDateExpedition.Value;
                else
                    entity.CnhDateExpedition = null;

                if (model.CnhDateExpiration.HasValue)
                    entity.CnhDateExpiration = model.CnhDateExpiration.Value;
                else
                    entity.CnhDateExpiration = null;

                if (model.CnhDateFirst.HasValue)
                    entity.CnhDateFirst = model.CnhDateFirst.Value;
                else
                    entity.CnhDateFirst = null;


                entity.EmployeeCostCenters = new Collection<EmployeeCostCenter>();

                // Testar o insert sem esses dados.

                EmployeeCostCenter employeeDeparrtment = new EmployeeCostCenter();
                employeeDeparrtment.CreatedDate = DateTime.Now;
                employeeDeparrtment.StartDate = model.AdmissionDate.HasValue ? model.AdmissionDate.Value : new DateTime();
                employeeDeparrtment.Employee = entity;
                entity.EmployeeCostCenters.Add(employeeDeparrtment);
                employeeDeparrtment.CostCenter = costCenter;

                EmployeeCompany employeeCompany = new EmployeeCompany();
                employeeCompany.CreatedDate = DateTime.Now;
                employeeCompany.StartDate = model.AdmissionDate.HasValue ? model.AdmissionDate.Value : new DateTime();
                employeeCompany.Employee = entity;
                entity.EmployeeCompanies = new Collection<EmployeeCompany>();
                entity.EmployeeCompanies.Add(employeeCompany);
                employeeCompany.Company = company;

                EmployeeRole employeeRole = new EmployeeRole();
                employeeRole.CreatedDate = DateTime.Now;
                employeeRole.StartDate = model.AdmissionDate.HasValue ? model.AdmissionDate.Value : new DateTime();
                employeeRole.Employee = entity;
                entity.EmployeeRoles = new Collection<EmployeeRole>();
                entity.EmployeeRoles.Add(employeeRole);
                employeeRole.Role = role;

                var ent = _employeeService.Create(entity);

                var addresses = _addressRepository.SelectBy(e => e.EmployeeId == model.Id && e.DeletedDate == null);
                if (addresses != null && addresses.Count > 0)
                {
                    foreach (var x in addresses)
                    {
                        EmployeeAddress obj = new EmployeeAddress();
                        obj.EmployeeId = ent.Id;
                        obj.CreatedDate = x.CreatedDate;
                        obj.Active = x.Active;
                        obj.CEP = x.CEP;
                        obj.CityId = x.CityId;
                        obj.Complement = x.Complement;
                        obj.Neighborhood = x.Neighborhood;
                        obj.Number = x.Number;
                        obj.Street = x.Street;
                        obj.Type = x.Type;

                        _addressRepository.Add(obj);
                    }
                }

                var contacts = _contactRepository.SelectBy(e => e.EmployeeId == model.Id && e.DeletedDate == null);
                if (contacts != null && contacts.Count > 0)
                {
                    foreach (var x in contacts)
                    {
                        EmployeeContact obj = new EmployeeContact();
                        obj.EmployeeId = ent.Id;
                        obj.CreatedDate = x.CreatedDate;
                        obj.Active = x.Active;
                        obj.Type = x.Type;
                        obj.Value = x.Value;

                        _contactRepository.Add(obj);
                    }
                }

                var vehicles = _vehicleRepository.SelectBy(x => x.EmployeeId == model.Id && x.DeletedDate == null);
                if (vehicles != null && vehicles.Count > 0)
                {
                    foreach (var x in vehicles)
                    {
                        EmployeeVehicle obj = new EmployeeVehicle();
                        obj.EmployeeId = ent.Id;
                        obj.CreatedDate = x.CreatedDate;
                        obj.Active = x.Active;
                        obj.Brand = x.Brand;
                        obj.Model = x.Model;
                        obj.Color = x.Color;
                        obj.CarPlate = x.CarPlate;

                        _vehicleRepository.Add(obj);
                    }
                }

                var bankAccounts = _bankAccountRepository.SelectBy(x => x.EmployeeId == model.Id && x.DeletedDate == null);
                if (bankAccounts != null && bankAccounts.Count > 0)
                {
                    foreach (var x in bankAccounts)
                    {
                        EmployeeBankAccount obj = new EmployeeBankAccount();
                        obj.EmployeeId = ent.Id;
                        obj.CreatedDate = x.CreatedDate;
                        obj.Account = x.Account;
                        obj.Agency = x.Agency;
                        obj.Bank = x.Bank;
                        obj.Main = x.Main;

                        _bankAccountRepository.Add(obj);
                    }
                }

                var relationships = _relationshipRepository.SelectBy(x => x.EmployeeId == model.Id && x.DeletedDate == null);
                if (relationships != null && relationships.Count > 0)
                {
                    foreach (var x in relationships)
                    {
                        EmployeeRelationship obj = new EmployeeRelationship();
                        obj.EmployeeId = ent.Id;
                        obj.CreatedDate = x.CreatedDate;
                        obj.Cpf = x.Cpf;
                        obj.EndDate = x.EndDate;
                        obj.HealthPlanId = x.HealthPlanId;
                        obj.IR = x.IR;
                        obj.Name = x.Name;
                        obj.OdontologyPlanId = x.OdontologyPlanId == null ? null : x.OdontologyPlanId;
                        obj.Relationship = x.Relationship;
                        obj.HealthPlanId = x.HealthPlanId == null ? null : x.HealthPlanId;
                        obj.StartDate = x.StartDate;
                        obj.UseHealthPlan = x.UseHealthPlan;
                        obj.UseOdontologyPlan = x.UseOdontologyPlan;
                        obj.Birthdate = x.Birthdate == null ? null : x.Birthdate;

                        _relationshipRepository.Add(obj);
                    }
                }

                var courses = _courseRepository.SelectBy(x => x.EmployeeId == model.Id && x.DeletedDate == null);
                if (courses != null && courses.Count > 0)
                {
                    foreach (var x in courses)
                    {
                        CourseEmployee obj = new CourseEmployee();
                        obj.EmployeeId = ent.Id;
                        obj.CreatedDate = x.CreatedDate;
                        obj.CourseId = x.CourseId;
                        obj.Deadline = x.Deadline;
                        obj.EndDate = x.EndDate;
                        obj.Required = x.Required;
                        obj.StartDate = x.StartDate;
                        obj.Value = x.Value;

                        _courseRepository.Add(obj);
                    }
                }

                var salaries = _salaryHistoryRepository.SelectBy(e => e.EmployeeId == model.Id && e.DeletedDate == null);
                if (salaries != null && salaries.Count > 0)
                {
                    foreach (var x in salaries)
                    {
                        EmployeeSalary obj = new EmployeeSalary();
                        obj.EmployeeId = ent.Id;
                        obj.CreatedDate = x.CreatedDate;
                        obj.Category = x.Category;
                        obj.Description = x.Description;
                        obj.EndDate = x.EndDate;
                        obj.ExtraHour = x.ExtraHour;
                        obj.Gratification = x.Gratification;
                        obj.Salary = x.Salary;
                        obj.SalaryPeriod = x.SalaryPeriod;
                        obj.StartDate = x.StartDate;
                        obj.Triennium = x.Triennium;
                        obj.WorkLoad = x.WorkLoad;

                        _salaryHistoryRepository.Add(obj);
                    }
                }

                var benefits = _benefitrepository.SelectBy(e => e.EmployeeId == model.Id && e.DeletedDate == null);
                if (benefits != null && benefits.Count > 0)
                {
                    foreach (var x in benefits)
                    {
                        EmployeeBenefit obj = new EmployeeBenefit();
                        obj.EmployeeId = ent.Id;
                        obj.CreatedDate = x.CreatedDate;
                        obj.BenefitId = x.BenefitId;
                        obj.EndDate = x.EndDate ?? null;
                        obj.StartDate = x.StartDate;
                        obj.CustomValue = x.CustomValue ?? null;

                        _benefitrepository.Add(obj);
                    }
                }

                var variables = _variableRepository.SelectBy(e => e.EmployeeId == model.Id && e.DeletedDate == null);
                if (variables != null && variables.Count > 0)
                {
                    foreach (var x in variables)
                    {
                        EmployeeVariables obj = new EmployeeVariables();
                        obj.EmployeeId = ent.Id;
                        obj.CreatedDate = x.CreatedDate;
                        obj.Date = x.Date;
                        obj.Description = x.Description;
                        obj.EmployeeVariablesTypeId = x.EmployeeVariablesTypeId;
                        obj.Value = x.Value;

                        _variableRepository.Add(obj);
                    }
                }

                var vacations = _vacationRepository.SelectBy(e => e.EmployeeId == model.Id && e.DeletedDate == null);
                if (vacations != null && vacations.Count > 0)
                {
                    foreach (var x in vacations)
                    {
                        EmployeeVacation obj = new EmployeeVacation();
                        obj.EmployeeId = ent.Id;
                        obj.CreatedDate = x.CreatedDate;
                        obj.AquisitionEndDate = x.AquisitionEndDate;
                        obj.AquisitionStartDate = x.AquisitionStartDate;
                        obj.EnjoymentEndDate = x.EnjoymentEndDate;
                        obj.EnjoymentStartDate = x.EnjoymentStartDate;
                        obj.ReceiptDate = x.ReceiptDate;
                        obj.Value = x.Value;

                        _vacationRepository.Add(obj);
                    }
                }

                var attachments = _attachmentRepository.SelectBy(e => e.EmployeeId == model.Id && e.DeletedDate == null);
                if (attachments != null && attachments.Count > 0)
                {
                    foreach (var x in attachments)
                    {
                        EmployeeAttachment obj = new EmployeeAttachment();
                        obj.EmployeeId = ent.Id;
                        obj.CreatedDate = x.CreatedDate;
                        obj.Description = x.Description;
                        obj.FileContent = x.FileContent;
                        obj.FileName = x.FileName;

                        _attachmentRepository.Add(obj);
                    }
                }

                var contracts = _contractRepository.SelectBy(x => x.EmployeeId == model.Id && x.DeletedDate == null);
                if (contracts != null && contracts.Count > 0)
                {
                    foreach (var x in contracts)
                    {
                        EmployeeContract obj = new EmployeeContract();
                        obj.EmployeeId = ent.Id;
                        obj.CreatedDate = x.CreatedDate;
                        obj.Approved = x.Approved;
                        obj.Description = x.Description;
                        obj.EndDate = x.EndDate ?? null;
                        obj.StartDate = x.StartDate;

                        _contractRepository.Add(obj);
                    }
                }

                _employeeService.CalculateByPercentage(ent);

                //var companies = _employeeCompanyRepository.SelectBy(x => x.EmployeeId == model.Id && x.DeletedDate == null);
                //if (companies != null && companies.Count > 0)
                //{
                //    foreach (var x in companies)
                //    {
                //        EmployeeCompany obj = new EmployeeCompany();
                //        obj.EmployeeId = ent.Id;
                //        obj.CreatedDate = x.CreatedDate;
                //        obj.CompanyId = x.CompanyId;
                //        obj.StartDate = x.StartDate;
                //        obj.EndDate = x.EndDate;

                //        _employeeCompanyRepository.Add(obj);
                //    }
                //}



            }
            catch (Exception ex)
            {


            }
        }

        private static void EmptyList(Employee entity)
        {
            entity.EmployeeRoles = null;
            entity.EmployeeCostCenters = null;
            entity.EmployeeCompanies = null;
        }

        private void IntegrateCPN(EmployeeViewModel model)
        {
            CLIENTE obj = new CLIENTE();

            obj.CRK_COR_CLIENTE.CD_CLIENTE = ""; // ?
            obj.CRK_COR_CLIENTE.NM_CLIENTE = model.Name;
            obj.CRK_COR_CLIENTE.NM_APELIDO = model.Initials; // Verificar
            obj.CRK_COR_CLIENTE.CD_TIPOCLIENTE = "1"; // PF
            obj.CRK_COR_CLIENTE.IC_TIPOPESSOA = ""; // Colocar
            obj.CRK_COR_CLIENTE.IC_ESTRANGEIRO = ""; // Colocar
            obj.CRK_COR_CLIENTE.DS_CNPJ_CPF = model.Cpf;
            obj.CRK_COR_CLIENTE.FL_INATIVO = "N";
            obj.CRK_COR_CLIENTE.DT_INCLUSAO = DateTime.Now.ToString("YYYYMMAA");

            obj.CRK_COR_TIPOCADASTRO.CD_TIPOCADASTRO = "2"; // Funcionário

            obj.CRK_COR_PESSOAFISICA.ID_ESTADOCIVIL = "1"; // Pegar o código
            obj.CRK_COR_PESSOAFISICA.DT_NASCIMENTO = model.Birthdate.ToString("YYYYMMAA");
            obj.CRK_COR_PESSOAFISICA.IC_SEXO = model.Gender == "Masculino" ? "M" : "F";
            obj.CRK_COR_PESSOAFISICA.TP_DOCTO = "RG"; // Verificar
            obj.CRK_COR_PESSOAFISICA.DS_RG = model.Rg;
            obj.CRK_COR_PESSOAFISICA.DS_ORGAOEMISSOR = model.RgDispatcher; // Colocar campo obrigatório no cadastro
            obj.CRK_COR_PESSOAFISICA.DT_EMISSAO_DOCTO = model.RgDateExpedition.Value.ToString("YYYYMMAA");
            obj.CRK_COR_PESSOAFISICA.SG_UF_RG = ""; // Calcular e colocar

            obj.CRK_COR_CLIENTE_CONTA.ID_TIPOCONTA = ""; // Verificar
            obj.CRK_COR_CLIENTE_CONTA.IC_CONTACORRENTE = ""; // Verificar
            obj.CRK_COR_CLIENTE_CONTA.ID_EXTERNO = ""; // Verificar

            var data = _addressRepository.SelectBy(x => x.EmployeeId == model.Id && x.DeletedDate == null);
            var address = data[0];

            CRK_COR_CLIENTE_ENDERECO e = new CRK_COR_CLIENTE_ENDERECO();
            e.ID_TIPOENDERECO = address.Type == "Residencial" ? "4" : address.Type == "Comercial" ? "5" : "6";
            e.DS_CEP = address.CEP;
            e.FL_PRINCIPAL = "S";
            e.ID_TIPOLOGRADOURO = "";
            e.NR_LOGRADOURO = address.Number;
            e.NM_BAIRRO = address.Neighborhood;
            e.NM_MUNICIPIO = address.City.Name;
            e.SG_UF = address.City.State.Name;
            e.DS_PAIS = "Brasil";
            e.ID_EXTERNO = address.Id.ToString();

            obj.ENDERECOS.CRK_COR_CLIENTE_ENDERECO.Add(e);

            string xml = SerializeClean(obj);

            // TODO: Chama a procedure
        }

        private static string SerializeClean(CLIENTE obj)
        {
            XmlSerializer xs = null;

            XmlWriterSettings settings = null;
            XmlSerializerNamespaces ns = null;

            XmlWriter xw = null;

            String outString = String.Empty;

            try
            {
                settings = new XmlWriterSettings();
                settings.OmitXmlDeclaration = true;

                ns = new XmlSerializerNamespaces();
                ns.Add("", "");

                StringBuilder sb = new StringBuilder();

                xs = new XmlSerializer(typeof(CLIENTE));

                xw = XmlWriter.Create(sb, settings);

                xs.Serialize(xw, obj, ns);
                xw.Flush();

                outString = sb.ToString();
            }
            catch (Exception ex)
            {
                // Console.WriteLine(ex.ToString());
            }
            finally
            {
                if (xw != null)
                    xw.Close();
            }

            return outString;
        }

        public JsonResult Remove(int id)
        {
            var result = _employeeRepository.Remove(id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}


