﻿using QX3.Portal.WebSite.Attributes;
using QX3.Portal.WebSite.HumanResources.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Repository;
using Tegra.HumanResources.Domain.Service;
using Tegra.HumanResources.Services;

namespace QX3.Portal.WebSite.HumanResources.Controllers
{
    [AccessControlAuthorize("RH.Employee.Index")]
    public class EmployeeAttachmentController : Controller
    {
        private readonly IEmployeeAttachmentRepository _repository;
        private readonly IEmployeeAttachmentService _service;
        private readonly IEmployeeService _employeeService;
        private readonly IEmployeeRepository _employeeRepository;

        public EmployeeAttachmentController(IEmployeeAttachmentRepository repository, IEmployeeAttachmentService service, IEmployeeService employeeService, IEmployeeRepository employeeRepository)
        {
            this._repository = repository;
            this._service = service;
            this._employeeService = employeeService;
            this._employeeRepository = employeeRepository;
        }

        [HttpGet]
        public JsonResult List(int EmployeeId)
        {
            var result = _repository.Query(c => c.EmployeeId == EmployeeId && c.DeletedDate == null).ToList();
            var jsonResult = Json(result, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        [HttpGet]
        public JsonResult Find(int id)
        {
            var data = _repository.Query(c => c.Id == id).FirstOrDefault();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Create(int EmployeeId, string Description, string FileName, HttpPostedFileBase FileContent, int Id = 0)
        {
            var entity = new EmployeeAttachment
            {
                Id = Id,
                EmployeeId = EmployeeId,
                Description = Description,
                FileName = FileContent == null ? FileName : FileContent.FileName
            };

            entity.FileContent = FileContent == null ? _repository.Find(entity.Id).FileContent : _service.ReadFully(FileContent.InputStream);

            var data = _service.Save(entity);
            _employeeService.CalculateByPercentage(_employeeRepository.Find(entity.EmployeeId));
            var jsonResult = Json(true, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        public FileResult Download(int Id)
        {
            var model = _repository.Query(c => c.Id == Id).FirstOrDefault();
            byte[] fileBytes = model.FileContent;
            string fileName = model.FileName;
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        public JsonResult Remove(int id)
        {
            var employeeId = _repository.Find(id).EmployeeId;
            var result = _service.Remove(id, false);
            _employeeService.CalculateByPercentage(null, employeeId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}