﻿using iTextSharp.text;
using QX3.Portal.WebSite.Attributes;
using QX3.Portal.WebSite.HumanResources.Models;
using QX3.Portal.WebSite.HumanResources.Util;
using QX3.Spinnex.Common.Services.Mail;
using RazorEngine;
using RazorEngine.Templating;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Tegra.DDD.Pagination;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Repository;
using Tegra.HumanResources.Domain.Representations;
using Tegra.HumanResources.Domain.Service;

namespace QX3.Portal.WebSite.HumanResources.Controllers
{
    [AccessControlAuthorize("RH.Notification.Index")]
    public class NotificationController : Controller
    {
        private readonly INotificationRepository _notificationRepository;
        private readonly INotificationService _notificationService;
        private readonly IAsyncNotificationService _asyncNotificationService;


        public NotificationController(INotificationRepository notificationRepository,
                                      INotificationService notificationService,
                                      IAsyncNotificationService asyncNotificationService)
        {
            this._notificationRepository = notificationRepository;
            this._notificationService = notificationService;
            this._asyncNotificationService = asyncNotificationService;

        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult SendEmail(int Id)
        {
            try
            {
                var itens = _notificationRepository.GetNotification(Id).ToList();
                List<SendMail> email = new List<SendMail>();

                itens.SelectMany(d => d.Destinations).ToList().ForEach(c =>
                {
                    email.Add(new SendMail
                    {
                        EmailTo = c.Email,
                        Body = RenderPartialViewToString("EmailBody", _notificationService.GenerateNotification(c.Notification.Query)),
                        Subject = itens.FirstOrDefault(cf => cf.Id == Id).Name
                    });
                });

                email.ForEach(c =>
                {
                    c.Send();
                });

                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 400;
                return Json(true, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult Create(Notification entity)
        {
            try
            {
                var query = _notificationService.GenerateQuery(entity);

                if (_notificationService.ExecuteQuery(query))
                {
                    if (entity.Destinations != null)
                    {
                        entity.Query = query;
                    }
                    var model = _notificationService.Save(entity);
                    return Json(model.Id, JsonRequestBehavior.AllowGet);
                }

                Response.StatusCode = 400;
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Response.StatusCode = 412;
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult GetFiltrable(int id)
        {
            var notificationList = _notificationRepository.Query(c => c.Id == id).Include(c => c.Items).SelectMany(c => c.Items).Select(c => c.Table).Distinct();

            var finalList = _notificationService.GetNotificationFilter(notificationList.ToList(), id);
            return Json(finalList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Remove(Notification entity)
        {
            _notificationService.Remove(entity);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        protected string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

        [HttpGet]
        public JsonResult List(PaginationSettings settings)
        {
            var data = _notificationRepository.SelectPagedBy(settings, c => c.DeletedDate == null, n => new NotificationViewModel
            {
                Id = n.Id,
                Frequency = n.Frequency,
                Name = n.Name,
                Active = n.Active,
                Query = n.Query,
                Items = n.Items.Select(ni => new NotificationItemViewModel
                {
                    Id = ni.Id,
                    Table = ni.Table,
                    Field = ni.Field,
                    FieldAlias = ni.FieldAlias,
                    TableAlias = ni.TableAlias,
                }).ToList(),
                Destinations = n.Destinations.Select(nd => new NotificationDestinationViewModel
                {
                    Id = nd.Id,
                    Email = nd.Email,
                }).ToList()
            });
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EmailBody()
        {
            return View();
        }

        [HttpPost]
        public JsonResult AddQueryFilter(List<NotificationRepresentation> filters, int notificationId)
        {
            try
            {
                var notificationQuery = NotificationRepresentation.FactoryFilter(filters ?? new List<NotificationRepresentation>());

                var notification = _notificationService.CreateFilters(notificationQuery, notificationId);

                var result = _notificationService.UpdateQuery(notification, notificationQuery);
                return Json(result == null ? new { Message = "O Filtro salvo não trouxe nenhum resultado!" } : new { Message = "Ok" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetNotification(int id)
        {
            var data = _notificationRepository.SelectBy(c => c.Id == id && c.Active == true, n => new NotificationViewModel
            {
                Id = n.Id,
                Frequency = n.Frequency,
                Name = n.Name,
                Active = n.Active,
                Query = n.Query,
                Destinations = n.Destinations.Select(nd => new NotificationDestinationViewModel
                {
                    Id = nd.Id,
                    Email = nd.Email,
                }).ToList(),
                Items = n.Items.Select(ni => new NotificationItemViewModel
                {
                    Id = ni.Id,
                    Table = ni.Table,
                    Field = ni.Field,
                    FieldAlias = ni.FieldAlias,
                    TableAlias = ni.TableAlias,
                }).ToList()
            }).First();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetNotificationRoles()
        {
            var data = _notificationService.GetNotificationRoles();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetRoleDescritionByEnumValue(string value)
        {
            var data = _notificationService.GetRoleDescritionByEnumValue(value);
            return Json(data, JsonRequestBehavior.AllowGet);
        }


    }
}
