﻿using QX3.Portal.WebSite.Attributes;
using QX3.Portal.WebSite.HumanResources.Models;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using Tegra.DDD.Pagination;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Exceptions;
using Tegra.HumanResources.Domain.Repository;
using Tegra.HumanResources.Domain.Representations;
using Tegra.HumanResources.Services;

namespace QX3.Portal.WebSite.HumanResources.Controllers
{

    [AccessControlAuthorize("RH.Company.Index")]
    public class CompanyController : Controller
    {
        private readonly ICompanyRepository _companyRepository;
        private readonly ICompanyService _companyService;

        private readonly Expression<Func<Company, CompanyViewModel>> _columns = x => new CompanyViewModel()
        {
            Id = x.Id,
            Cnpj = x.Cnpj,
            Name = x.Name,
            Code = x.Code
        };

        public CompanyController(ICompanyRepository companyRepository, ICompanyService companyService)
        {
            this._companyRepository = companyRepository;
            this._companyService = companyService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult Find(int id)
        {
            var data = _companyRepository.SelectBy(x => x.Id == id, _columns).Single();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult List(PaginationSettings settings)
        {
            var data = _companyRepository.SelectPaged(settings, _columns);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult ListAll()
        {
            var data = _companyRepository.QueryBy(x => x.DeletedDate == null, x => new SelectViewModel
            {
                Id = x.Id,
                Value = x.Name
            }).OrderBy(x => x.Value).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult ListByReport()
        {
            var data = _companyRepository.Query(x => new SelectCompanyByReport
            {
                Code = x.Code,
                Value = x.Name
            }).OrderBy(x => x.Value).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult FindByName(string name)
        {
            int code = 0;
            if (int.TryParse(name, out code))
            {
                return Json(_companyRepository.Query(c => c.Code.Contains(code.ToString())).ToList(), JsonRequestBehavior.AllowGet);
            }
            return Json(_companyRepository.Query(c => c.Code.Contains(code.ToString())).ToList(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AutoValidate]
        public JsonResult Save(CompanyViewModel model)
        {
            try
            {
                _companyService.Save(model.ToEntity());
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                Response.StatusCode = 500;
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult Remove(int id = 0)
        {
            try
            {
                _companyService.Remove(id);
                return Json(true, JsonRequestBehavior.AllowGet);
            } catch (ForeignKeyEmployeeException ex)
            {
                Response.StatusCode = (int) StatusHttp.CONFLICT;
            }

            return Json(false, JsonRequestBehavior.AllowGet);
        }
    }
}