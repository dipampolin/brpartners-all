﻿using QX3.Portal.WebSite.Attributes;
using QX3.Portal.WebSite.HumanResources.Models;
using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Repository;
using Tegra.HumanResources.Services;

namespace QX3.Portal.WebSite.HumanResources.Controllers
{

    [AccessControlAuthorize("RH.Employee.Index")]
    public class EmployeeCompanyController : Controller
    {

        private readonly IEmployeeCompanyRepository _employeeCompanyRepository;
        private readonly IEmployeeService _employeeService;
        private readonly IEmployeeRepository _employeeRepository;

        public EmployeeCompanyController(IEmployeeCompanyRepository _employeeCompanyRepository, IEmployeeService employeeService, IEmployeeRepository employeeRepository)
        {
            this._employeeCompanyRepository = _employeeCompanyRepository;
            this._employeeService = employeeService;
            this._employeeRepository = employeeRepository;
        }

        [HttpGet]
        public JsonResult List(int employeeId)
        {

            var data = _employeeCompanyRepository.Query(e => e.EmployeeId == employeeId && e.DeletedDate == null)
                .Include(x => x.Company).OrderByDescending(c => c.StartDate).ToList().ConvertAll(x => new EmployeeCompanyViewModel()
                {
                    Id = x.Id,
                    EmployeeId = x.EmployeeId,
                    CompanyId = x.CompanyId,
                    CompanyName = x.Company.Name,
                    StartDate = x.StartDate,
                    EndDate = x.EndDate,
                    Active = x.IsActive()
                }).OrderByDescending(c => c.StartDate);

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult Find(int id)
        {
            var data = _employeeCompanyRepository.Query(x => x.Id == id)
                .Include(x => x.Company).ToList().ConvertAll(x => new EmployeeCompanyViewModel()
                {
                    Id = x.Id,
                    EmployeeId = x.EmployeeId,
                    CompanyId = x.CompanyId,
                    CompanyName = x.Company.Name,
                    StartDate = x.StartDate,
                    EndDate = x.EndDate,
                    Active = x.IsActive()
                }).Single();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Remove(int id)
        {
            var employeeId = _employeeCompanyRepository.Find(id).EmployeeId;
            var result = _employeeCompanyRepository.Remove(id);
            _employeeService.CalculateByPercentage(null, employeeId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AutoValidate]
        public JsonResult CreatePost(EmployeeCompanyViewModel model)
        {
            try
            {
                EmployeeCompany c = null;

                var data = _employeeCompanyRepository.Query(x => x.EmployeeId == model.EmployeeId && x.DeletedDate == null)
                .Include(x => x.Company).ToList();

                bool hasActive = false;

                foreach (var item in data)
                {
                    if (item.Id == model.Id)
                        c = item;

                    if (item.IsActive() && item.Id != model.Id)
                        hasActive = true;
                }

                if (hasActive)
                {
                    Response.StatusCode = 409;
                    return Json(false, JsonRequestBehavior.AllowGet);
                }

                try // if (model.Id = 0
                {
                    var id = _employeeCompanyRepository.InsertOrUpdate(model.ToEntity(), model.Id);
                    _employeeService.CalculateByPercentage(_employeeRepository.Find(model.EmployeeId));

                    return Json(true, JsonRequestBehavior.AllowGet);
                }
                catch (Exception)
                {
                    c.StartDate = model.StartDate;
                    c.EndDate = model.EndDate;
                    c.CompanyId = model.CompanyId;

                    var id = _employeeCompanyRepository.Update(c);
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {
                Response.StatusCode = 500;
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetActiveEmployeeCompany(int id)
        {
            var result = _employeeCompanyRepository.Query(e => e.EmployeeId == id).OrderByDescending(o => o.StartDate).FirstOrDefault();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}