﻿using QX3.Portal.WebSite.Attributes;
using QX3.Portal.WebSite.HumanResources.Models;
using System;
using System.Linq;
using System.Web.Mvc;
using Tegra.DDD.Pagination;
using Tegra.HumanResources.Domain.Exceptions;
using Tegra.HumanResources.Domain.Repository;
using Tegra.HumanResources.Domain.Representations;
using Tegra.HumanResources.Services;

namespace QX3.Portal.WebSite.HumanResources.Controllers
{

    [AccessControlAuthorize("RH.Course.Index")]
    public class CourseController : Controller
    {

        private readonly ICourseRepository _courseRepository;
        private readonly ICourseService _courseService;

        public CourseController(ICourseRepository courseRepository, ICourseService courseService)
        {
            this._courseRepository = courseRepository;
            this._courseService = courseService;
        }

        [HttpGet]
        public ActionResult Index()
        {

            return View();
        }

        [HttpGet]
        public JsonResult Find(int id)
        {
            var data = _courseRepository.SelectBy(x => x.Id == id, x => new CourseViewModel()
            {
                Id = x.Id,
                Name = x.Name,
                Active = x.Active,
                CourseEmployees = x.CourseEmployees.Select(y => new CourseEmployeeViewModel()
                {
                    CourseId = y.CourseId ?? (int?)null,
                    Deadline = y.Deadline,
                    EmployeeId = y.EmployeeId,
                    Required = y.Required,
                    EmployeeName = y.Employee.Name
                }).ToList()
            }).Single();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult List(PaginationSettings settings)
        {
            var data = _courseRepository.SelectPaged(settings, x => new CourseViewModel()
            {
                Id = x.Id,
                Name = x.Name,
                Active = x.Active
            });

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult ListAll()
        {
            var data = _courseRepository.QueryBy(c => c.DeletedDate == null, x => new CourseViewModel()
            {
                Id = x.Id,
                Name = x.Name,
                Active = x.Active
            });

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AutoValidate]
        public JsonResult Save(CourseViewModel model)
        {
            try
            {
                var entity = _courseService.Save(model.ToEntity());
                return Json(true);
            }
            catch (Exception)
            {
                Response.StatusCode = (int) StatusHttp.INTERNAL_SERVER_ERROR;
            }

            return Json(false);
        }

        [HttpGet]
        public JsonResult Remove(int id = 0)
        {
            try
            {
                _courseService.Remove(id);
                return Json(true, JsonRequestBehavior.AllowGet);
            } catch (ForeignKeyEmployeeException ex)
            {
                Response.StatusCode = (int)StatusHttp.CONFLICT;
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }
    }
}