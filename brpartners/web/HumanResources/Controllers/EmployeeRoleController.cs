﻿using QX3.Portal.WebSite.Attributes;
using QX3.Portal.WebSite.HumanResources.Models;
using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Repository;
using Tegra.HumanResources.Services;

namespace QX3.Portal.WebSite.HumanResources.Controllers
{

    [AccessControlAuthorize("RH.Employee.Index")]
    public class EmployeeRoleController : Controller
    {

        private readonly IEmployeeRoleRepository _employeeRoleRepository;
        private readonly IEmployeeService _employeeService;
        private readonly IEmployeeRepository _employeeRepository;

        public EmployeeRoleController(IEmployeeRoleRepository _employeeRoleRepository, IEmployeeService employeeService, IEmployeeRepository employeeRepository)
        {
            this._employeeRoleRepository = _employeeRoleRepository;
            this._employeeService = employeeService;
            this._employeeRepository = employeeRepository;
        }

        [HttpGet]
        public JsonResult List(int employeeId)
        {
            var data = _employeeRoleRepository.Query(e => e.EmployeeId == employeeId && e.DeletedDate == null)
                .Include(x => x.Role).OrderByDescending(c => c.StartDate).ToList().ConvertAll(x => new EmployeeRoleViewModel()
                {
                    Id = x.Id,
                    EmployeeId = x.EmployeeId,
                    RoleId = x.RoleId,
                    RoleName = x.Role.Name,
                    StartDate = x.StartDate,
                    EndDate = x.EndDate,
                    Active = x.IsActive()
                });

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult Find(int id)
        {
            var data = _employeeRoleRepository.Query(x => x.Id == id)
                .Include(x => x.Role).ToList().ConvertAll(x => new EmployeeRoleViewModel()
                {
                    Id = x.Id,
                    EmployeeId = x.EmployeeId,
                    RoleId = x.RoleId,
                    RoleName = x.Role.Name,
                    StartDate = x.StartDate,
                    EndDate = x.EndDate,
                    Active = x.IsActive()
                }).Single();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Remove(int id)
        {
            var result = _employeeRoleRepository.Remove(id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AutoValidate]
        public JsonResult CreatePost(EmployeeRoleViewModel model)
        {
            try
            {
                EmployeeRole c = null;

                var data = _employeeRoleRepository.Query(x => x.EmployeeId == model.EmployeeId && x.DeletedDate == null)
                .Include(x => x.Role).ToList();

                bool hasActive = false;

                foreach (var item in data)
                {
                    if (item.Id == model.Id)
                        c = item;

                    if (item.IsActive() && item.Id != model.Id)
                        hasActive = true;
                }

                if (hasActive)
                {
                    Response.StatusCode = 409;
                    return Json(false, JsonRequestBehavior.AllowGet);
                }

                try // if (model.Id = 0
                {
                    var id = _employeeRoleRepository.InsertOrUpdate(model.ToEntity(), model.Id);
                    _employeeService.CalculateByPercentage(_employeeRepository.Find(model.EmployeeId));

                    return Json(true, JsonRequestBehavior.AllowGet);
                }
                catch (Exception)
                {
                    c.StartDate = model.StartDate;
                    c.EndDate = model.EndDate;
                    c.RoleId = model.RoleId;

                    var id = _employeeRoleRepository.Update(c);
                    return Json(true, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception)
            {
                Response.StatusCode = 500;
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }
    }
}