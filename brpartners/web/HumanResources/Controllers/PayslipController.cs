﻿using Excel;
using QX3.Portal.WebSite.Attributes;
using Rotativa;
using Rotativa.Options;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tegra.DDD.Pagination;
using Tegra.HumanResources.Domain.Repository;
using Tegra.HumanResources.Domain.Representations;
using Tegra.HumanResources.Domain.Service;

namespace QX3.Portal.WebSite.HumanResources.Controllers
{

    [AccessControlAuthorize("RH.Payslip.Index")]
    public class PayslipController : Controller
    {
        private readonly IPayslipRepository _repository;
        private readonly IPayslipService _payslipService;
        private readonly IExcelService _excelService;

        public PayslipController(
            IPayslipRepository repository,
            IPayslipService payslipService,
            IExcelService excelService
        )
        {
            this._repository = repository;
            this._payslipService = payslipService;
            this._excelService = excelService;
        }

        [HttpGet]
        public ActionResult Index(bool Successful = false, string Erro = null)
        {
            var errorList = new List<string>();
            if (Erro != null)
            {
                Array.ForEach(Erro.Split(','), e =>
                {
                    if (e.Length > 1)
                    {
                        errorList.Add(e);
                    }
                    
                });
            }

            ViewBag.Successfully = Successful;
            ViewBag.Erro = errorList;
            return View();
        }

        [HttpPost]
        public ActionResult UploadFile(HttpPostedFileBase file)
        {
            try
            {
                if (IsValidFormat(file.FileName))
                {
                    IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(file.InputStream);

                    var data = ExtractExcelToDataBase(excelReader);
                    String erro = null;

                    data.Exceptions.ForEach(e =>
                    {
                        erro = erro + "," + e.Message;
                    });

                    return RedirectToAction("Index", new { Successful = (data.Exceptions.Count == 0), Erro = erro });
                }
                return RedirectToAction("Index", new { Erro =  "Arquivo inválido" });
            }
            catch (IndexOutOfRangeException)
            {
                return RedirectToAction("Index", new { Erro = "Layout do Arquivo invalído" } );
            }
            catch (Exception e)
            {
                return RedirectToAction("Index", new { Erro =  e.Message });
            }
        }

        private bool IsValidFormat(string fileName)
        {
            return fileName.ToLower().Substring(fileName.Length - 4).Equals("xlsx");
        }

        [HttpGet]
        public JsonResult List(PaginationSettings settings)
        {
            var data = _repository.List(settings);

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult PaymentView(int Year, int Month, int CompanyCode, bool isPrint = false)
        {
            ViewBag.IsPrint = isPrint;

            var payslipList = _repository.GetPayslipToReport(c => c.CompanyCode.Equals(CompanyCode) && c.Date.Equals(new DateTime(Year, Month, 1)));
            return View(payslipList);
        }

        [HttpPost]
        public JsonResult DeletePayment(DateTime date, int companyCode)
        {
            var result = _payslipService.RemoveByCompany(date, companyCode);
            return Json(result);
        }

        public ActionResult Print(int Year, int Month, int CompanyCode)
        {
            ViewBag.IsPrint = false;

            var payslipList = _repository.GetPayslipToReport(c => c.CompanyCode.Equals(CompanyCode) && c.Date.Equals(new DateTime(Year, Month, 1)));

            return new ViewAsPdf("PaymentDownload", payslipList)
            {
                PageOrientation = Orientation.Portrait,
                PageSize = Size.A4,
                PageMargins = new Margins(10, 3, 10, 0),
            };
        }

        public ActionResult Download(int Year, int Month, int CompanyCode)
        {
            ViewBag.IsPrint = true;

            var payslipList = _repository.GetPayslipToReport(c => c.CompanyCode.Equals(CompanyCode) && c.Date.Equals(new DateTime(Year, Month, 1)));

            return new ViewAsPdf("PaymentDownload", payslipList)
            {
                FileName = "FolhaPagamento__" + DateTime.Now + "__.pdf",
                PageOrientation = Rotativa.Options.Orientation.Portrait,
                PageSize = Rotativa.Options.Size.A4,
                PageMargins = new Margins(10, 3, 10, 0),
            };
        }

        private PayslipByException ExtractExcelToDataBase(IExcelDataReader excelReader)
        {
            var payslips = _excelService.ProcessExcel(excelReader);

            if (payslips.Exceptions.Count == 0)
            {
                _payslipService.VerifyIfExistThenDelete(payslips.Payslips);
                _payslipService.InsertAll(payslips.Payslips);
            }

            return payslips;
        }
    }
}