﻿using System;
using System.Data;

namespace QX3.Portal.WebSite.HumanResources.Util
{
    public static class Extension
    {
        public static void Convert(this DataColumn column, Func<object, object> conversion)
        {
            foreach (DataRow row in column.Table.Rows)
            {
                row[column] = conversion(row[column]);
            }
        }
    }
}