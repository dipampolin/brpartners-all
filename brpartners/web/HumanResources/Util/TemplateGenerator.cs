﻿using RazorEngine;
using Tegra.HumanResources.Domain.Util;

namespace QX3.Portal.WebSite.HumanResources.Util
{
    public class TemplateGenerator : ITemplateGenerator
    {

        string template = @"@using System.Data;
                            @using QX3.Portal.WebSite.HumanResources.Util;
                            @using QX3.Portal.WebSite.HumanResources.Helper;
                            @model DataTable

                            <div>
                                <table style='width: 100%; border-spacing: 0; font-size: 11px; border: 1px solid gray; border-collapse: collapse;'>
                                    <thead>
                                        <tr style='background-color: #6C8BA6'>
                                            @{
                                                var columnName = Model.Columns.Cast<DataColumn>()
                                                                    .Select(x => x.ColumnName).ToList();
                                            }

                                            @foreach (var column in columnName)
                                            {
                                                <th style='white-space: nowrap; padding: 4px 6px; text-align: left; text-align: center; font-size: 12px; padding-bottom: 10px; color: white; padding-top: 10px; border: 1px solid gray; border-collapse: collapse'>@HtmlHelperExtension.Header(column)</th>
                                            }
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach (DataRow rows in Model.Rows)
                                        {
                                            <tr style='border-bottom: 1px solid gray; white-space: nowrap; padding: 4px 6px; text-align: left'>
                                                @foreach (DataColumn dataColumn in Model.Columns)
                                                {
                                                    <td style='padding-top: 10px; padding-bottom: 10px;  padding-left: 6px; padding-right: 6px; text-transform: uppercase; border: 1px solid gray;'>@HtmlHelperExtension.Convert(dataColumn.ColumnName, rows[dataColumn.ColumnName])</td>
                                                }
                                            </tr>
                                        }
                                    </tbody>
                                </table>
                            </div>";


        public string Generate(object model)
        {
#pragma warning disable CS0618 // Type or member is obsolete
            return Razor.Parse(template, model);
#pragma warning restore CS0618 // Type or member is obsolete
        }
    }
}