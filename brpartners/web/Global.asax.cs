﻿using QX3.Portal.WebSite.AccessControlService;
using QX3.Portal.WebSite.Attributes;
using QX3.Portal.WebSite.AuthenticationService;
using QX3.Portal.WebSite.HumanResources.Util;
using SimpleInjector;
using SimpleInjector.Extensions.LifetimeScoping;
using SimpleInjector.Integration.Web;
using SimpleInjector.Integration.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Tegra.HumanResources.Domain.Util;
using Tegra.HumanResources.Infra;
using Tegra.HumanResources.Web.App_Start;

namespace QX3.Portal.WebSite
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "RH", // Route name
                "RH/{controller}/{action}/{id}", // URL with parameters
                new { controller = "Dashboard", action = "Index", id = UrlParameter.Optional }, // Parameter defaults,
                new string[] { "QX3.Portal.WebSite.HumanResources.Controllers" }
            );

            routes.MapRoute(
                "Index", // Route name
                "Home/Index", // URL with parameters
                new { controller = "Home", action = "Index" }, // Parameter defaults
                new string[] { "QX3.Portal.WebSite.Controllers" }
            );

            routes.MapRoute(
                "Portal", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional }, // Parameter defaults
                new string[] { "QX3.Portal.WebSite.Controllers" }
            );


        }

        protected void Application_BeginRequest(Object sender, EventArgs e)
        {

            log4net.Config.XmlConfigurator.Configure();


            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["ForceHttps"]))
            {
                if (!Request.IsSecureConnection)
                {
                    string path = string.Format("https{0}", Request.Url.AbsoluteUri.Substring(4));


                    Response.Redirect(path);
                }
            }
        }

        protected void Application_Start()
        {
            ViewEngines.Engines.Clear();

            var razorEngine = new RazorViewEngine();

            razorEngine.ViewLocationFormats = razorEngine.ViewLocationFormats
                .Concat(
                    new[] {
                        "~/HumanResources/Views/{1}/{0}.cshtml",
                        "~/HumanResources/Views/Shared/{0}.cshtml"
                }).ToArray();

            razorEngine.PartialViewLocationFormats = razorEngine.PartialViewLocationFormats
                .Concat(
                    new[] {
                        "~/HumanResources/Views/{1}/{0}.cshtml",
                        "~/HumanResources/Views/Shared/{0}.cshtml"
                }).ToArray();

            ViewEngines.Engines.Add(razorEngine);

            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);

            ValueProviderFactories.Factories.Add(new JsonValueProviderFactory());

            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);

            BundleConfig.RegisterBundles(BundleTable.Bundles);
#if !DEBUG
            BundleTable.EnableOptimizations = true;
#endif
            var container = new Container();

            container.Options.DefaultScopedLifestyle = Lifestyle.CreateHybrid(
                () => HttpContext.Current == null,
                new LifetimeScopeLifestyle(),
                new WebRequestLifestyle());

            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());
            container.RegisterMvcIntegratedFilterProvider();

            // Register WCF mappings
            DependencyInjection.Register(container);

            container.Register<IAuthenticationContract>(() => new AuthenticationContractClient(), Lifestyle.Scoped);
            container.Register<IAccessControlContract>(() => new AccessControlContractClient(), Lifestyle.Scoped);

            container.RegisterInitializer<AccessControlAuthorizeAttribute>(handler =>
            {
                handler.AccessControlService = container.GetInstance<IAccessControlContract>();
            });
            container.Register<ITemplateGenerator, TemplateGenerator>(Lifestyle.Scoped);

            container.Verify();
            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
        }

        /*protected void Application_Error(object sender, EventArgs e)
        {
            Exception objErr = Server.GetLastError().GetBaseException();

            String Site = String.Empty;
            String Browser = String.Empty;
            String Path = String.Empty;
            String Texto = String.Empty;
            String Campo = String.Empty;
            String IP = String.Empty;
            String Host = String.Empty;
            String strMensagem = String.Empty;

            String data = DateTime.Now.ToString("dd/MM/yyyy HH:mm");

            Site = Request.ServerVariables["SERVER_NAME"].ToLower();
            Browser = Request.ServerVariables["HTTP_USER_AGENT"].ToLower();
            Path = Request.ServerVariables["PATH_TRANSLATED"].ToLower();
            IP = Request.ServerVariables["REMOTE_ADDR"];
            Host = Request.Url.Host;


            Texto = "<b>Request.QueryString:</b> <br><br>";
            foreach (String i in HttpContext.Current.Request.QueryString.AllKeys)
            {
                Texto += i + " = " + HttpContext.Current.Request.QueryString[i] + "<br>";
            }

            Texto += "<br><b>Request.Form:</b> <br><br>";
            foreach (String i in HttpContext.Current.Request.Form.AllKeys)
            {
                Texto += i + " = " + HttpContext.Current.Request.Form[i] + "<br>";
            }

            Texto += "<br><b>Sessions:</b> <br><br>";

            foreach (string strKey in HttpContext.Current.Session.Keys)
            {
                if (HttpContext.Current.Session[strKey] != null)
                {
                    Texto += strKey + " : " + Session[strKey].ToString() + "<br>";
                }
            }

            strMensagem = "<html><body bgcolor=white><font face=arial size=2 color=black>";
            strMensagem += "<b>Data:</b> " + data + "<br>";
            strMensagem += "<b>Descrição:</b> " + Server.HtmlEncode(objErr.Message) + "<br>";
            strMensagem += "<b>Fonte do erro:</b> " + Server.HtmlEncode(objErr.Source) + "<br>";
            strMensagem += "<b>Rastreamento:</b> " + Server.HtmlEncode(objErr.StackTrace) + "<br>";
            strMensagem += "<b>Site:</b> " + Site + "<br>";
            strMensagem += "<b>Browser:</b> " + Browser + "<br>";
            strMensagem += "<b>Caminho Completo:</b> " + Path + "<br><br>";
            strMensagem += "<b>Host:</b> " + Host + "<br><br>";
            strMensagem += "<b>IP Solicitante:</b> " + IP + "<br><br>";
            strMensagem += Texto + "<br>";
            strMensagem += "</body></html>";

            string logMessage = objErr.Message + Environment.NewLine
                + objErr.Source + Environment.NewLine
                + objErr.StackTrace + Environment.NewLine
                + ((!String.IsNullOrEmpty(Browser)) ? Browser + Environment.NewLine : String.Empty)
                + ((!String.IsNullOrEmpty(Path)) ? Path + Environment.NewLine : String.Empty)
                + ((!String.IsNullOrEmpty(Host)) ? Host + Environment.NewLine : String.Empty)
                + ((!String.IsNullOrEmpty(IP)) ? IP + Environment.NewLine : String.Empty);

            QX3.Portal.WebSite.Models.CommonHelper.WriteLogInFile("log_erro", DateTime.Now, logMessage);

            //System.Net.Mail.MailMessage Message = new System.Net.Mail.MailMessage(
            //    System.Configuration.ConfigurationManager.AppSettings["ErrorMailFrom"]
            //    , System.Configuration.ConfigurationManager.AppSettings["ErrorMailTo"]);

            //Message.IsBodyHtml = true;
            //Message.Subject = System.Configuration.ConfigurationManager.AppSettings["ErrorMailSubject"];
            //Message.Body = strMensagem;

            //System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();

            //smtp.Send(Message);

            //Response.Redirect(VirtualPathUtility.ToAbsolute("~/Home/Erro"));
        }*/
    }
}