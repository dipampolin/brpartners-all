﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace QX3.Portal.WebSite.TermService {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="TermService.ITermContract")]
    public interface ITermContract {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITermContract/LoadTerms", ReplyAction="http://tempuri.org/ITermContract/LoadTermsResponse")]
        QX3.Portal.Contracts.DataContracts.WcfResponse<QX3.Portal.Contracts.DataContracts.TermItem[]> LoadTerms(QX3.Portal.Contracts.DataContracts.TermFilter filter, System.Nullable<int> userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITermContract/LoadLotTerms", ReplyAction="http://tempuri.org/ITermContract/LoadLotTermsResponse")]
        QX3.Portal.Contracts.DataContracts.WcfResponse<QX3.Portal.Contracts.DataContracts.TermItem[]> LoadLotTerms(string lstContracts);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITermContract/LoadTerm", ReplyAction="http://tempuri.org/ITermContract/LoadTermResponse")]
        QX3.Portal.Contracts.DataContracts.WcfResponse<QX3.Portal.Contracts.DataContracts.TermItem> LoadTerm(int termId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITermContract/SettleTerm", ReplyAction="http://tempuri.org/ITermContract/SettleTermResponse")]
        QX3.Portal.Contracts.DataContracts.WcfResponse<bool> SettleTerm(int termID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITermContract/ExcludeTerm", ReplyAction="http://tempuri.org/ITermContract/ExcludeTermResponse")]
        QX3.Portal.Contracts.DataContracts.WcfResponse<bool> ExcludeTerm(int termID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITermContract/LoadClientTerms", ReplyAction="http://tempuri.org/ITermContract/LoadClientTermsResponse")]
        QX3.Portal.Contracts.DataContracts.WcfResponse<QX3.Portal.Contracts.DataContracts.TermItem[]> LoadClientTerms(QX3.Portal.Contracts.DataContracts.TermFilter filter, System.Nullable<int> userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITermContract/InsertTerms", ReplyAction="http://tempuri.org/ITermContract/InsertTermsResponse")]
        QX3.Portal.Contracts.DataContracts.WcfResponse<QX3.Portal.Contracts.DataContracts.TermItem[]> InsertTerms(QX3.Portal.Contracts.DataContracts.TermItem[] terms);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITermContract/ChangeQuantityToSettle", ReplyAction="http://tempuri.org/ITermContract/ChangeQuantityToSettleResponse")]
        QX3.Portal.Contracts.DataContracts.WcfResponse<bool> ChangeQuantityToSettle(int termID, long quantity);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITermContract/GetNumberOfPending", ReplyAction="http://tempuri.org/ITermContract/GetNumberOfPendingResponse")]
        QX3.Portal.Contracts.DataContracts.WcfResponse<int> GetNumberOfPending(string assessorsList);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface ITermContractChannel : QX3.Portal.WebSite.TermService.ITermContract, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class TermContractClient : System.ServiceModel.ClientBase<QX3.Portal.WebSite.TermService.ITermContract>, QX3.Portal.WebSite.TermService.ITermContract {
        
        public TermContractClient() {
        }
        
        public TermContractClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public TermContractClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public TermContractClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public TermContractClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public QX3.Portal.Contracts.DataContracts.WcfResponse<QX3.Portal.Contracts.DataContracts.TermItem[]> LoadTerms(QX3.Portal.Contracts.DataContracts.TermFilter filter, System.Nullable<int> userId) {
            return base.Channel.LoadTerms(filter, userId);
        }
        
        public QX3.Portal.Contracts.DataContracts.WcfResponse<QX3.Portal.Contracts.DataContracts.TermItem[]> LoadLotTerms(string lstContracts) {
            return base.Channel.LoadLotTerms(lstContracts);
        }
        
        public QX3.Portal.Contracts.DataContracts.WcfResponse<QX3.Portal.Contracts.DataContracts.TermItem> LoadTerm(int termId) {
            return base.Channel.LoadTerm(termId);
        }
        
        public QX3.Portal.Contracts.DataContracts.WcfResponse<bool> SettleTerm(int termID) {
            return base.Channel.SettleTerm(termID);
        }
        
        public QX3.Portal.Contracts.DataContracts.WcfResponse<bool> ExcludeTerm(int termID) {
            return base.Channel.ExcludeTerm(termID);
        }
        
        public QX3.Portal.Contracts.DataContracts.WcfResponse<QX3.Portal.Contracts.DataContracts.TermItem[]> LoadClientTerms(QX3.Portal.Contracts.DataContracts.TermFilter filter, System.Nullable<int> userId) {
            return base.Channel.LoadClientTerms(filter, userId);
        }
        
        public QX3.Portal.Contracts.DataContracts.WcfResponse<QX3.Portal.Contracts.DataContracts.TermItem[]> InsertTerms(QX3.Portal.Contracts.DataContracts.TermItem[] terms) {
            return base.Channel.InsertTerms(terms);
        }
        
        public QX3.Portal.Contracts.DataContracts.WcfResponse<bool> ChangeQuantityToSettle(int termID, long quantity) {
            return base.Channel.ChangeQuantityToSettle(termID, quantity);
        }
        
        public QX3.Portal.Contracts.DataContracts.WcfResponse<int> GetNumberOfPending(string assessorsList) {
            return base.Channel.GetNumberOfPending(assessorsList);
        }
    }
}
