﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace QX3.Portal.WebSite.RiskService {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="RiskService.IRiskContract")]
    public interface IRiskContract {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IRiskContract/InsertGuarantee", ReplyAction="http://tempuri.org/IRiskContract/InsertGuaranteeResponse")]
        QX3.Portal.Contracts.DataContracts.WcfResponse<int[]> InsertGuarantee(QX3.Portal.Contracts.DataContracts.Guarantee guarantee);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IRiskContract/UpdateGuarantee", ReplyAction="http://tempuri.org/IRiskContract/UpdateGuaranteeResponse")]
        QX3.Portal.Contracts.DataContracts.WcfResponse<bool> UpdateGuarantee(QX3.Portal.Contracts.DataContracts.GuaranteeItem guaranteeItem);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IRiskContract/DeleteGuarantee", ReplyAction="http://tempuri.org/IRiskContract/DeleteGuaranteeResponse")]
        QX3.Portal.Contracts.DataContracts.WcfResponse<bool> DeleteGuarantee(int guaranteeID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IRiskContract/ChangeGuaranteeStatus", ReplyAction="http://tempuri.org/IRiskContract/ChangeGuaranteeStatusResponse")]
        QX3.Portal.Contracts.DataContracts.WcfResponse<bool> ChangeGuaranteeStatus(int guaranteeID, int statusID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IRiskContract/LoadGuarantees", ReplyAction="http://tempuri.org/IRiskContract/LoadGuaranteesResponse")]
        QX3.Portal.Contracts.DataContracts.WcfResponse<QX3.Portal.Contracts.DataContracts.Guarantee[]> LoadGuarantees(QX3.Portal.Contracts.DataContracts.GuaranteeFilter filter, System.Nullable<int> userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IRiskContract/GetNumberOfPending", ReplyAction="http://tempuri.org/IRiskContract/GetNumberOfPendingResponse")]
        QX3.Portal.Contracts.DataContracts.WcfResponse<int> GetNumberOfPending(string assessorsList);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IRiskContract/LoadClientGuarantees", ReplyAction="http://tempuri.org/IRiskContract/LoadClientGuaranteesResponse")]
        QX3.Portal.Contracts.DataContracts.WcfResponse<QX3.Portal.Contracts.DataContracts.GuaranteeItem[]> LoadClientGuarantees(QX3.Portal.Contracts.DataContracts.GuaranteeFilter filter, System.Nullable<int> userId);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IRiskContractChannel : QX3.Portal.WebSite.RiskService.IRiskContract, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class RiskContractClient : System.ServiceModel.ClientBase<QX3.Portal.WebSite.RiskService.IRiskContract>, QX3.Portal.WebSite.RiskService.IRiskContract {
        
        public RiskContractClient() {
        }
        
        public RiskContractClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public RiskContractClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public RiskContractClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public RiskContractClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public QX3.Portal.Contracts.DataContracts.WcfResponse<int[]> InsertGuarantee(QX3.Portal.Contracts.DataContracts.Guarantee guarantee) {
            return base.Channel.InsertGuarantee(guarantee);
        }
        
        public QX3.Portal.Contracts.DataContracts.WcfResponse<bool> UpdateGuarantee(QX3.Portal.Contracts.DataContracts.GuaranteeItem guaranteeItem) {
            return base.Channel.UpdateGuarantee(guaranteeItem);
        }
        
        public QX3.Portal.Contracts.DataContracts.WcfResponse<bool> DeleteGuarantee(int guaranteeID) {
            return base.Channel.DeleteGuarantee(guaranteeID);
        }
        
        public QX3.Portal.Contracts.DataContracts.WcfResponse<bool> ChangeGuaranteeStatus(int guaranteeID, int statusID) {
            return base.Channel.ChangeGuaranteeStatus(guaranteeID, statusID);
        }
        
        public QX3.Portal.Contracts.DataContracts.WcfResponse<QX3.Portal.Contracts.DataContracts.Guarantee[]> LoadGuarantees(QX3.Portal.Contracts.DataContracts.GuaranteeFilter filter, System.Nullable<int> userId) {
            return base.Channel.LoadGuarantees(filter, userId);
        }
        
        public QX3.Portal.Contracts.DataContracts.WcfResponse<int> GetNumberOfPending(string assessorsList) {
            return base.Channel.GetNumberOfPending(assessorsList);
        }
        
        public QX3.Portal.Contracts.DataContracts.WcfResponse<QX3.Portal.Contracts.DataContracts.GuaranteeItem[]> LoadClientGuarantees(QX3.Portal.Contracts.DataContracts.GuaranteeFilter filter, System.Nullable<int> userId) {
            return base.Channel.LoadClientGuarantees(filter, userId);
        }
    }
}
