﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace QX3.Portal.WebSite.SubscriptionService {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="SubscriptionService.ISubscriptionContract")]
    public interface ISubscriptionContract {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISubscriptionContract/LoadSubscriptions", ReplyAction="http://tempuri.org/ISubscriptionContract/LoadSubscriptionsResponse")]
        QX3.Portal.Contracts.DataContracts.WcfResponse<System.Collections.Generic.List<QX3.Portal.Contracts.DataContracts.Subscription>, int> LoadSubscriptions(QX3.Portal.Contracts.DataContracts.Subscription filter, QX3.Portal.Contracts.DataContracts.FilterOptions options, bool getProspects);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISubscriptionContract/SubscriptionRequest", ReplyAction="http://tempuri.org/ISubscriptionContract/SubscriptionRequestResponse")]
        QX3.Portal.Contracts.DataContracts.WcfResponse<bool, int> SubscriptionRequest(QX3.Portal.Contracts.DataContracts.Subscription request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISubscriptionContract/SubscriptionUpdate", ReplyAction="http://tempuri.org/ISubscriptionContract/SubscriptionUpdateResponse")]
        QX3.Portal.Contracts.DataContracts.WcfResponse<bool, int> SubscriptionUpdate(QX3.Portal.Contracts.DataContracts.Subscription request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISubscriptionContract/SubscriptionDelete", ReplyAction="http://tempuri.org/ISubscriptionContract/SubscriptionDeleteResponse")]
        QX3.Portal.Contracts.DataContracts.WcfResponse<bool, int> SubscriptionDelete(QX3.Portal.Contracts.DataContracts.Subscription request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISubscriptionContract/SubscriptionUpdateStatus", ReplyAction="http://tempuri.org/ISubscriptionContract/SubscriptionUpdateStatusResponse")]
        QX3.Portal.Contracts.DataContracts.WcfResponse<bool, int> SubscriptionUpdateStatus(QX3.Portal.Contracts.DataContracts.Subscription request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISubscriptionContract/LoadSubscriptionRights", ReplyAction="http://tempuri.org/ISubscriptionContract/LoadSubscriptionRightsResponse")]
        QX3.Portal.Contracts.DataContracts.WcfResponse<System.Collections.Generic.List<QX3.Portal.Contracts.DataContracts.SubscriptionRights>, int> LoadSubscriptionRights(QX3.Portal.Contracts.DataContracts.SubscriptionRights filter, QX3.Portal.Contracts.DataContracts.FilterOptions options, System.Nullable<int> userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISubscriptionContract/SubscriptionRightsRequest", ReplyAction="http://tempuri.org/ISubscriptionContract/SubscriptionRightsRequestResponse")]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(QX3.Portal.Contracts.DataContracts.WcfResponse<bool, int>))]
        QX3.Portal.Contracts.DataContracts.WcfResponse<bool> SubscriptionRightsRequest(QX3.Portal.Contracts.DataContracts.SubscriptionRights request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISubscriptionContract/SubscriptionRightsUpdate", ReplyAction="http://tempuri.org/ISubscriptionContract/SubscriptionRightsUpdateResponse")]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(QX3.Portal.Contracts.DataContracts.WcfResponse<bool, int>))]
        QX3.Portal.Contracts.DataContracts.WcfResponse<bool> SubscriptionRightsUpdate(QX3.Portal.Contracts.DataContracts.SubscriptionRights request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISubscriptionContract/SubscriptionRightsDelete", ReplyAction="http://tempuri.org/ISubscriptionContract/SubscriptionRightsDeleteResponse")]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(QX3.Portal.Contracts.DataContracts.WcfResponse<bool, int>))]
        QX3.Portal.Contracts.DataContracts.WcfResponse<bool> SubscriptionRightsDelete(QX3.Portal.Contracts.DataContracts.SubscriptionRights request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISubscriptionContract/GetRequestedQuantity", ReplyAction="http://tempuri.org/ISubscriptionContract/GetRequestedQuantityResponse")]
        QX3.Portal.Contracts.DataContracts.WcfResponse<long> GetRequestedQuantity(QX3.Portal.Contracts.DataContracts.SubscriptionRights request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISubscriptionContract/GetRightQuantity", ReplyAction="http://tempuri.org/ISubscriptionContract/GetRightQuantityResponse")]
        QX3.Portal.Contracts.DataContracts.WcfResponse<long> GetRightQuantity(QX3.Portal.Contracts.DataContracts.SubscriptionRights request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISubscriptionContract/RequestRightsNotify", ReplyAction="http://tempuri.org/ISubscriptionContract/RequestRightsNotifyResponse")]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(QX3.Portal.Contracts.DataContracts.WcfResponse<bool, int>))]
        QX3.Portal.Contracts.DataContracts.WcfResponse<bool> RequestRightsNotify(QX3.Portal.Contracts.DataContracts.SubscriptionRights request, QX3.Portal.Contracts.DataContracts.Subscription subscription);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISubscriptionContract/LoadNotifications", ReplyAction="http://tempuri.org/ISubscriptionContract/LoadNotificationsResponse")]
        QX3.Portal.Contracts.DataContracts.WcfResponse<System.Collections.Generic.List<QX3.Portal.Contracts.DataContracts.SubscriptionNotification>> LoadNotifications(int subscriptionId);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface ISubscriptionContractChannel : QX3.Portal.WebSite.SubscriptionService.ISubscriptionContract, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class SubscriptionContractClient : System.ServiceModel.ClientBase<QX3.Portal.WebSite.SubscriptionService.ISubscriptionContract>, QX3.Portal.WebSite.SubscriptionService.ISubscriptionContract {
        
        public SubscriptionContractClient() {
        }
        
        public SubscriptionContractClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public SubscriptionContractClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public SubscriptionContractClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public SubscriptionContractClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public QX3.Portal.Contracts.DataContracts.WcfResponse<System.Collections.Generic.List<QX3.Portal.Contracts.DataContracts.Subscription>, int> LoadSubscriptions(QX3.Portal.Contracts.DataContracts.Subscription filter, QX3.Portal.Contracts.DataContracts.FilterOptions options, bool getProspects) {
            return base.Channel.LoadSubscriptions(filter, options, getProspects);
        }
        
        public QX3.Portal.Contracts.DataContracts.WcfResponse<bool, int> SubscriptionRequest(QX3.Portal.Contracts.DataContracts.Subscription request) {
            return base.Channel.SubscriptionRequest(request);
        }
        
        public QX3.Portal.Contracts.DataContracts.WcfResponse<bool, int> SubscriptionUpdate(QX3.Portal.Contracts.DataContracts.Subscription request) {
            return base.Channel.SubscriptionUpdate(request);
        }
        
        public QX3.Portal.Contracts.DataContracts.WcfResponse<bool, int> SubscriptionDelete(QX3.Portal.Contracts.DataContracts.Subscription request) {
            return base.Channel.SubscriptionDelete(request);
        }
        
        public QX3.Portal.Contracts.DataContracts.WcfResponse<bool, int> SubscriptionUpdateStatus(QX3.Portal.Contracts.DataContracts.Subscription request) {
            return base.Channel.SubscriptionUpdateStatus(request);
        }
        
        public QX3.Portal.Contracts.DataContracts.WcfResponse<System.Collections.Generic.List<QX3.Portal.Contracts.DataContracts.SubscriptionRights>, int> LoadSubscriptionRights(QX3.Portal.Contracts.DataContracts.SubscriptionRights filter, QX3.Portal.Contracts.DataContracts.FilterOptions options, System.Nullable<int> userId) {
            return base.Channel.LoadSubscriptionRights(filter, options, userId);
        }
        
        public QX3.Portal.Contracts.DataContracts.WcfResponse<bool> SubscriptionRightsRequest(QX3.Portal.Contracts.DataContracts.SubscriptionRights request) {
            return base.Channel.SubscriptionRightsRequest(request);
        }
        
        public QX3.Portal.Contracts.DataContracts.WcfResponse<bool> SubscriptionRightsUpdate(QX3.Portal.Contracts.DataContracts.SubscriptionRights request) {
            return base.Channel.SubscriptionRightsUpdate(request);
        }
        
        public QX3.Portal.Contracts.DataContracts.WcfResponse<bool> SubscriptionRightsDelete(QX3.Portal.Contracts.DataContracts.SubscriptionRights request) {
            return base.Channel.SubscriptionRightsDelete(request);
        }
        
        public QX3.Portal.Contracts.DataContracts.WcfResponse<long> GetRequestedQuantity(QX3.Portal.Contracts.DataContracts.SubscriptionRights request) {
            return base.Channel.GetRequestedQuantity(request);
        }
        
        public QX3.Portal.Contracts.DataContracts.WcfResponse<long> GetRightQuantity(QX3.Portal.Contracts.DataContracts.SubscriptionRights request) {
            return base.Channel.GetRightQuantity(request);
        }
        
        public QX3.Portal.Contracts.DataContracts.WcfResponse<bool> RequestRightsNotify(QX3.Portal.Contracts.DataContracts.SubscriptionRights request, QX3.Portal.Contracts.DataContracts.Subscription subscription) {
            return base.Channel.RequestRightsNotify(request, subscription);
        }
        
        public QX3.Portal.Contracts.DataContracts.WcfResponse<System.Collections.Generic.List<QX3.Portal.Contracts.DataContracts.SubscriptionNotification>> LoadNotifications(int subscriptionId) {
            return base.Channel.LoadNotifications(subscriptionId);
        }
    }
}
