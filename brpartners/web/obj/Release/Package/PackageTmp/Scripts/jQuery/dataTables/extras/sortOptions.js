﻿var SortOptions = {
    Trim: function (str) {
        str = str.replace(/^\s+/, '');
        for (var i = str.length - 1; i >= 0; i--) {
            if (/\S/.test(str.charAt(i))) {
                str = str.substring(0, i + 1);
                break;
            }
        }
        return str;
    },

    AddSortOptions: function () {
        jQuery.fn.dataTableExt.oSort['date-euro-asc'] = function (a, b) {
            //console.log(a, b);
            if (SortOptions.Trim(a) != '') {
                var frDatea = SortOptions.Trim(a).split(' - ');
                var frTimea = frDatea[1].split(':');
                var frDatea2 = frDatea[0].split('/');

                var x = (frDatea2[2] + frDatea2[1] + frDatea2[0] + frTimea[0] + frTimea[1]) * 1;
            } else {
                var x = 10000000000000; // = l'an 1000 ...
            }

            if (SortOptions.Trim(b) != '') {
                var frDateb = SortOptions.Trim(b).split(' - ');
                var frTimeb = frDateb[1].split(':');
                frDateb = frDateb[0].split('/');

                var y = (frDateb[2] + frDateb[1] + frDateb[0] + frTimeb[0] + frTimeb[1]) * 1;
            } else {
                var y = 10000000000000;
            }
            var z = ((x < y) ? -1 : ((x > y) ? 1 : 0));

            return z;
        };

        jQuery.fn.dataTableExt.oSort['date-euro-desc'] = function (a, b) {
            //console.log(a, b);
            if (SortOptions.Trim(a) != '') {
                var frDatea = SortOptions.Trim(a).split(' - ');
                var frTimea = frDatea[1].split(':');
                var frDatea2 = frDatea[0].split('/');
                var x = (frDatea2[2] + frDatea2[1] + frDatea2[0] + frTimea[0] + frTimea[1]) * 1;
            } else {
                var x = 10000000000000;
            }

            if (SortOptions.Trim(b) != '') {
                var frDateb = SortOptions.Trim(b).split(' - ');
                var frTimeb = frDateb[1].split(':');
                frDateb = frDateb[0].split('/');
                var y = (frDateb[2] + frDateb[1] + frDateb[0] + frTimeb[0] + frTimeb[1]) * 1;
            } else {
                var y = 10000000000000;
            }
            var z = ((x < y) ? 1 : ((x > y) ? -1 : 0));

            return z;
        };

        jQuery.extend(jQuery.fn.dataTableExt.oSort, {
            "date-eu-pre": function (date) {
                /*
                var date = date.replace(" ", "");

                if (date.indexOf('.') > 0) {
                var eu_date = date.split('.');
                } else {
                var eu_date = date.split('/');
                }

                if (eu_date[2]) {
                var year = eu_date[2];
                } else {
                var year = 0;
                }

                var month = eu_date[1];
                if (month.length == 1) {
                month = 0 + month;
                }

                var day = eu_date[0];
                if (day.length == 1) {
                day = 0 + day;
                }

                return (year + month + day) * 1;*/
                console.log("oooooooooiiiiiiiiiii");
                return Date.parse(date);
            },

            "date-eu-asc": function (a, b) {
                console.log("lalla");
                return ((a < b) ? -1 : ((a > b) ? 1 : 0));
            },

            "date-eu-desc": function (a, b) {
                console.log("pppoooo");
                return ((a < b) ? 1 : ((a > b) ? -1 : 0));
            }
        });
    }
};



