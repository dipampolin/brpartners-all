﻿var dateHelper = (function () {
    _checkDate = function (element, contextElementId) { ///<sumary>Função que verifica se a data está fora do formato(dd/MM/aaaa)</sumary>
        var field = $(element, contextElementId);

        if (field.val() == "" || field.val().toString().length < 10) {
            field.val("");
            var date = new Date();
            var day = (date.getDate().toString().length == 1) ? "0" + date.getDate().toString() : date.getDate().toString();
            var month = ((date.getMonth() + 1).toString().length == 1) ? "0" + (date.getMonth() + 1).toString() : (date.getMonth() + 1).toString();
            var year = date.getFullYear();

            field.val(day + "/" + month + "/" + year);
        }
    },
    _setFormattedDate = function (elementId, yearsToAdd, monthsToAdd, daysToAdd, context) { ///<sumary>Função que retorna uma data no no formato dd/MM/yyyy.</sumary>
        var date = new Date();
        var day = date.getDate() + daysToAdd;
        var month = date.getMonth() + monthsToAdd;
        var year = date.getFullYear() + yearsToAdd;

        day = (day.toString().length == 1) ? "0" + day.toString() : day.toString();
        month = ((month + 1).toString().length == 1) ? "0" + (month + 1).toString() : (month + 1).toString();

        $(elementId, context).val(day + "/" + month + "/" + year);
    },
     _checkPeriod = function (startDateID, endDateID, context) { ///<sumary>Função que verifica se o período é válido.</sumary>
         var startDate = $(startDateID, context).val();
         var endDate = $(endDateID, context).val();

         if (startDate.length == 10 && endDate.length == 10) {
             var sDate = new Date(startDate.substring(6, 10),
                         startDate.substring(3, 5) - 1,
                         startDate.substring(0, 2));

             //sDate.setMonth(sDate.getMonth() - 1);

             var eDate = new Date(endDate.substring(6, 10),
                      endDate.substring(3, 5) - 1,
                      endDate.substring(0, 2));

             //eDate.setMonth(eDate.getMonth() - 1);

             if (sDate > eDate)
                 return false;
             else
                 return true;
         }
         return true;
     },
        _getNextBusinessDay = function (sourceDate, targetElementId, daysToAdd, context) {
            $.ajax({
                type: "POST",
                url: "/Market/GetNextBusinessDay/",
                dataType: "json",
                data: { numberOfDaysToAdd: daysToAdd, targetDate: sourceDate },
                success: function (data) {
                    $("#" + targetElementId, context).html(String(data));
                    $("#" + targetElementId, context).val(String(data));
                }
            });

        }

    return { CheckDate: _checkDate, CheckPeriod: _checkPeriod, SetFormattedDate: _setFormattedDate, GetNextBusinessDay: _getNextBusinessDay };
} ());
