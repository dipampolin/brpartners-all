﻿
var marketHelper = (
    function () {
    	_setStocksSuggest = function (targetName, companyTargetName, context) {
    		$("#" + targetName, context).autocomplete({
    			source: function (request, response) {
    				$.ajax({
    					url: "/Market/StocksAutoSuggest",
    					type: "POST",
    					dataType: "json",
    					data: { term: request.term, companyName: $("#" + companyTargetName, context).val() },
    					success: function (data) {
    						response($.map(data, function (item) {
    							return {
    								label: item.Symbol,
    								value: item.Symbol
    							}
    						}));
    					}
    				});
    			},
    			minLength: 1
    		});
    	},
		_setCompaniesSuggest = function (targetName, stocksTargetName, context) {
			$("#" + targetName, context).autocomplete({
				source: function (request, response) {
					$.ajax({
						url: "/Market/CompaniesAutoSuggest",
						type: "POST",
						dataType: "json",
						data: { term: request.term },
						success: function (data) {
							response($.map(data, function (item) {
								return {
									label: item,
									value: item
								}
							}));
						}
					});
				},
				minLength: 1,
				select: function (event, ui) {
					if (stocksTargetName != null && stocksTargetName != "") {
						$("#" + stocksTargetName, context).val("");
					}
				}
			});
		}

    	return { SetStocksSuggest: _setStocksSuggest, SetCompaniesSuggest: _setCompaniesSuggest };
    } ()
);