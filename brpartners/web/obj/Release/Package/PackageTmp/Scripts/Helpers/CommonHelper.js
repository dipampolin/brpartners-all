﻿
var commonHelper = (
    function () {
        _openModal = function (url, size, targetElement, context, height) {
            $.ajax({
                type: "POST",
                url: "/Base/CheckSession/",
                dataType: "string",
                cache: false,
                success: function (data) {
                    if (data != "true")
                        document.location = "/Account/Login";
                    else {
                        var obj = {
                            type: 'MessageBox',
                            title: "",
                            ajax: url,
                            calconchange: height ? false : true,
                            width: height != "" ? "auto" : size,
                            height: height ? height : "auto",
                            buttons: false
                        };

                        $().qxModal(obj, function () {
                            $('select', '#qxModal').styleInputs();
                            $('.qx-modal-topo', '#qxModal').prepend($('.qx-modal-conteudo', '#qxModal').find('h2'));

                            // Adiciona a classe 'ultimo' à última tr no corpo da tabela
                            $('table tbody tr:last-child').addClass('ultimo');

                            // Adiciona a classe 'ultimo' às últimas th e td no corpo da tabela
                            $('table tr td:last-child').addClass('ultimo');
                            $('table tr th:last-child').addClass('ultimo');
                        });

                        /*Modal.Show("", "", size);

                        $.ajax({
                        url: url,
                        cache: false,
                        dataType: "html",
                        success: function (data) {
                        if (context != "")
                        $(targetElement, context).html(data);
                        else
                        $(targetElement).html(data);
                        $("#MsgBox").center(); 
                        $('select').styleInputs();
                        }
                        });*/
                    }
                }
            });
        },
        _openModalPost = function (url, size, targetElement, context, params, height) {
            $.ajax({
                type: "POST",
                url: "/Base/CheckSession/",
                dataType: "string",
                success: function (data) {
                    if (data != "true")
                        document.location = "/Account/Login";
                    else {
                        var obj = {
                            type: 'MessageBox',
                            title: "",
                            ajax: url,
                            calconchange: true,
                            width: height != "" ? "auto" : size,
                            height: height ? height : "auto",
                            buttons: false,
                            ajaxmethod: 'POST',
                            ajaxdata: params
                        };

                        $().qxModal(obj, function () {
                            $('select', '#qxModal').styleInputs();
                            $('.qx-modal-topo', '#qxModal').prepend($('.qx-modal-conteudo', '#qxModal').find('h2'));

                            // Adiciona a classe 'ultimo' à última tr no corpo da tabela
                            $('table tbody tr:last-child').addClass('ultimo');

                            // Adiciona a classe 'ultimo' às últimas th e td no corpo da tabela
                            $('table tr td:last-child').addClass('ultimo');
                            $('table tr th:last-child').addClass('ultimo');

                        });

                    }
                }
            });
        }
        ,
		_loadView = function (url, targetElement, context, callback) {
		    $.ajax({
		        type: "POST",
		        url: "/Base/CheckSession/",
		        dataType: "string",
		        async: false,
		        success: function (data) {
		            if (data != "true")
		                document.location = "/Account/Login";
		            else {
		                $.ajax({
		                    url: url,
		                    cache: false,
		                    dataType: "html",
		                    success: function (data) {
		                        if (context != "")
		                            $(targetElement, context).html(data);
		                        else
		                            $(targetElement).html(data);
		                        if (typeof eval(callback) == 'function') {
		                            callback();
		                        }
		                    }
		                });
		            }
		        }

		    });
		},
		_formatErrorMessage = function (content, toList) {
		    if (toList)
		        return "<ul>" + content + "</ul>";
		    return "<label class='error'>" + content + "</label>";
		},
		_limitTextArea = function (limitFieldName, limitCounterName, limitNum) {
		    var fieldValue = $(limitFieldName).val();
		    if (fieldValue.length > limitNum) {
		        $(limitFieldName).val(fieldValue.substring(0, limitNum));
		    } else {
		        if (limitCounterName != "")
		            $(limitCounterName).val(limitNum - fieldValue.length);
		    }
		},
        _tryParseInt = function (str, defaultValue) {
            var retValue = defaultValue;
            if (str != null) {
                if (str.length > 0) {
                    if (!isNaN(str)) {
                        retValue = parseInt(str);
                    }
                }
            }
            return retValue;
        },
        _tryParseFloat = function (str, defaultValue) {
            var retValue = defaultValue;
            if (str != null) {
                if (str.length > 0) {
                    if (!isNaN(parseFloat(str)) && isFinite(str)) {
                        retValue = parseFloat(str);
                    }
                }
            }
            return retValue;
        },
        _formatNumber = function (value, decimal) {
            var retValue = value.toString();
            if (value > 0) {
                var number = value.toFixed(decimal).toString();
                number += '';
                x = number.split('.');
                x1 = x[0];
                x2 = x.length > 1 ? ',' + x[1] : '';
                var rgx = /(\d+)(\d{3})/;
                while (rgx.test(x1)) {
                    x1 = x1.replace(rgx, '$1' + '.' + '$2');
                }
                retValue = x1 + x2;
            }
            return retValue;
        },
        _showLoadingArea = function (context) {
            $(".sem-resultado", context).hide();
            $(".area-resultado", context).hide();
            $("#loadingArea", context).show();

        },
        _getParameterByName = function (name) {
            name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
            var regexS = "[\\?&]" + name + "=([^&#]*)";
            var regex = new RegExp(regexS);
            var results = regex.exec(window.location.search);
            if (results == null)
                return "";
            else
                return decodeURIComponent(results[1].replace(/\+/g, " "));
        },
        _checkSession = function () {
            var logged = false;
            $.ajax({
                type: "POST",
                url: "/Base/CheckSession/",
                dataType: "string",
                async: false,
                cache: false,
                success: function (data) {
                    if (data == "true")
                        logged = true;
                    else
                        document.location = "/Account/Login";
                }

            });
            return logged;
        }
        return { OpenModal: _openModal, OpenModalPost: _openModalPost, LoadView: _loadView, FormatErrorMessage: _formatErrorMessage, LimitTextArea: _limitTextArea, TryParseInt: _tryParseInt, TryParseFloat: _tryParseFloat, FormatNumber: _formatNumber, ShowLoadingArea: _showLoadingArea, GetParameterByName: _getParameterByName, CheckSession: _checkSession };
    } ()
);

