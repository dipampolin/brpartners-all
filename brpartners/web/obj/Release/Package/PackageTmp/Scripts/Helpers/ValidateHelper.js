﻿
var validateHelper = (
    function () {
        _ValidateList = function (list) {
            var listItems = list.split(',');
            if (listItems.length > 0) {
                for (var i = 0; i < listItems.length; i++) {
                    var item = listItems[i];
                    if (item.indexOf('-') >= 0) {
                        var items = item.split('-');
                        if (items.length > 2 || isNaN(items[0]) || isNaN(items[1]))
                            return false;
                        var start = parseInt(items[0]);
                        var end = parseInt(items[1]);
                        if (start >= end)
                            return false;
                    }
                    else {
                        if (isNaN(item))
                            return false;
                    }
                }
            }
            return true;
        },
		_validateISIN = function (isinValue) {
		    if (isinValue != null && isinValue != undefined && isinValue != "") {
		        if (isinValue.length != 12)
		            return false;
		        //2 primeiros caracteres sempre BR
		        //if (isinValue.substr(0, 2) != "BR")
		        //    return false;
		        //7 caracteres após o BR devem ser letras
		        //if (!(/^[a-zA-Z]+$/.test(isinValue.substr(2, 7))))
		        //    return false;
		        //Ultimo caractere sempre deve ser número
		        //if (isNaN(isinValue.substr(isinValue.length - 1, 1)))
		        //    return false;

		        return true;
		    }
		    else
		        return false;
		}
        return { ValidateList: _ValidateList, ValidateISIN: _validateISIN };
    } ()
);

