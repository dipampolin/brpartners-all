﻿/// <reference path="../../knockout-3.2.0.js" />
/// <reference path="../../knockout.mapping-latest.js" />
/// <reference path="../../knockout.validation.js" />


$(document).ready(function () {
    var jsonString = $("#JsonSource").val();

    var model = ko.mapping.fromJSON(jsonString);
    model.emTeste = ko.observable(document.location.href.toLowerCase().indexOf('/testar/')>0)

    model.questionsPath = ko.observableArray( [] );

    model.PushQuestion = function () {
        model.questionsPath.push(model.CurrentQuestion());


        model.HasPrevious(true);

    }
    model.PopQuestion = function () {
        var q = model.questionsPath.pop();
        model.HasPrevious(model.questionsPath().length > 0);
        return q;
    }
    model.ClearPath = function () {
        model.questionsPath.removeAll();
        model.HasPrevious(false);
    }

    for (var i = 0; i < model.Questions().length; i++) {
        var q = model.Questions()[i];
        q.ErrorMessage = ko.observable();
        q.IsSingle = ko.computed(function () {
            return this.QuestionType() == 82; // 'R'
        }, q);
        q.IsMultiple = ko.computed(function () {
            return this.QuestionType() == 67; // 'C'
        }, q);
        q.IsDistribution = ko.computed(function () {
            return this.QuestionType() == 68; // 'D'
        }, q);
        q.IsMatrix = ko.computed(function () {
            return this.QuestionType() == 77;
        }, q);

        q.QuestionText = ko.computed(function () {

            var pre = this.IdQuestion();
            if (this.SubQuestionId())
                pre += '.' + this.SubQuestionId();
            return pre + ') ' + this.Question();
        }, q);
        q.Total = ko.observable(0);
        q.UpdateTotal = function () {
            var q = this;
            var tot = 0;
            var hasZeroes = false;
            for (var i = 0; i < q.Answers().length; i++) {
                var a = q.Answers()[i];
                var input = clearDistribution(a.Distribution());
                tot += input;
                if (input === 0)
                    hasZeroes = true;

            }
            this.Total(tot);

            if (tot >= 100 || !hasZeroes)
                model.Validate();

        };
        if (q.IsMatrix()) {
            for (var j = 0; j < q.Answers().length; j++) {
                var a = q.Answers()[j];
                var matrixArray = [];
                for (var k = 0; k < q.MatrixColumns().length; k++)
                    matrixArray.push(ko.observable(false));
                a.MatrixData = ko.observableArray(matrixArray);
                a.MatrixColumns = q.MatrixColumns;
            }
        }

        for (var j = 0; j < q.Answers().length; j++) {
            var a = q.Answers()[j];
            a.Question = q;
            a.IsChecked = ko.observable(false);

            a.Distribution = ko.observable(null);
            a.Distribution.subscribe(function () {
                this.UpdateTotal();
            }, q);

            if (q.IsSingle()) {
                a.IsChecked.subscribe(function () {
                    if (this.IsChecked()) {
                        for (var i = 0; i < model.CurrentQuestion().Answers().length; i++) {
                            var other = model.CurrentQuestion().Answers()[i];
                            if (other != this) {
                                other.IsChecked(false);
                            }
                        }
                    }
                }, a);
            }


            a.IsChecked.subscribe(function () {
                model.Validate();
            }, model);

        }


    }
    model.PreambleVisible = ko.observable(true);
    model.ResultsVisible = ko.observable(false);
    model.CurrentQuestion = ko.observable(null);
    model.Saving = ko.observable(false);
    model.ResultMessage = ko.observable(null);


    if (!Modernizr.inputtypes.Number)
        model.CurrentQuestion.subscribe(function () {
            setTimeout(function () {
                var inputs = $("input.number");
                inputs.mask('9?99', { type: 'reverse' });
                inputs.focus(function () {
                    var me = this;
                    setTimeout(function () {
                        $(me).select();
                    }, 1)

                })
            }, 10);
        });

    model.ProfileId = ko.observable();
    model.ProfileDescription = ko.observable();

    model.Start = function () {
        model.CurrentQuestion(model.Questions()[0]);
        model.PreambleVisible(false);
    }

    model.HasNext = ko.computed(function () {

        if (model.CurrentQuestion() == null)
            return false;
        var i = model.Questions.indexOf(model.CurrentQuestion());

        var j = i + 1;
        while (j < model.Questions().length) {
            var q = model.Questions()[j];
            if (!q.SubQuestionId())
                return j;
            j++;
        }

        return false;
    }, model);
    model.HasPrevious = ko.observable();


    model.Validate = function () {

        var q = model.CurrentQuestion();


        if (q == null)
            return false;
        q.ErrorMessage(null);
        if (q.IsSingle() || q.IsMultiple()) {
            for (var i = 0; i < q.Answers().length; i++) {
                var a = q.Answers()[i];
                if (a.IsChecked()) {
                    return true;
                }
            }
            if (q.IsSingle())
                q.ErrorMessage("Selecione uma opção.");
            else
                q.ErrorMessage("Selecione ao menos uma opção.");

        } else if (q.IsDistribution()) {
            var ok = (q.Total() == 100);

            if (!ok)
                q.ErrorMessage('O total deve somar 100%.');
            return ok;
        } else if (q.IsMatrix()) {
            for (var i = 0; i < q.Answers().length; i++) {
                var a = q.Answers()[i];
                var md = a.MatrixData();
                for (var j = 0; j < md.length; j++) {
                    var isChecked = md[j]();
                    if (isChecked)
                        return true;
                }

            }

            q.ErrorMessage('Selecione ao menos uma opção.');
        } else {
            throw Error('Tipo de questão inválido!');
        }

        return false;
    }

    model.FindQuestionById = function (questionId) {
        for (var i = 0; i < model.Questions().length ; i++) {
            var q = model.Questions()[i];
            if (q.Id() == questionId)
                return q;
        }
        return null;
    }

    model.CheckQuestionPath = function (q) {
        var ok = (model.questionsPath.indexOf(q) < 0);
        if(model.emTeste() && !ok){
            alert("AVISO! A questão \"" + q.QuestionText() + "\"já foi respondida!\n\nO sistema não dá suporte a questionários circulares!!!");
        }
        return ok;
    }

    model.Next = function () {
        var ok = model.Validate();
        if (ok) {

            model.PushQuestion();


            var i = model.Questions.indexOf(model.CurrentQuestion());

            if (model.CurrentQuestion().IsSingle()) {
                for (var k = 0; k < model.CurrentQuestion().Answers().length; k++) {
                    var a = model.CurrentQuestion().Answers()[k];
                    if (a.IsChecked() != false) {
                        if (a.SubQuestionId()) {
                            var q = model.FindQuestionById(a.SubQuestionId());

                            if (model.CheckQuestionPath(q))
                                model.CurrentQuestion(q);
                            else
                                continue;
                            return;
                        }
                    }
                }
            }
            
            var next = model.HasNext();
            if (next) {
                var nextQuestion = model.Questions()[next];
                if(model.CheckQuestionPath(nextQuestion))
                    model.CurrentQuestion(nextQuestion);     
                else
                    model.PopQuestion();
            }
        }
    }
    model.Previous = function () {
        
        if (model.HasPrevious()) {
           
            var prevQuestion = model.PopQuestion();
            model.CurrentQuestion(prevQuestion);
        }

    }

    model.Finish = function () {
        if (model.CurrentQuestion) {
            var ok = model.Validate();
            if (ok) {
                model.PushQuestion();
                model.UpdateScore();


                // verifica se esta última pergunta libera uma pergunta secreta ....

                if (model.CurrentQuestion().IsSingle()) {
                    for (var k = 0; k < model.CurrentQuestion().Answers().length; k++) {
                        var a = model.CurrentQuestion().Answers()[k];
                        if (a.IsChecked() != false) {
                            if (a.SubQuestionId()) {
                                var q = model.FindQuestionById(a.SubQuestionId());

                                if (model.CheckQuestionPath(q))
                                    model.CurrentQuestion(q);
                                else
                                    continue;
                                return;
                            }
                        }
                    }
                }



                model.CurrentQuestion(null);
                model.ResultsVisible(true);
            }
        }
    }
    model.Reset = function () {
        for (var i = 0; i < model.Questions().length; i++) {
            var q = model.Questions()[i];

            for (var j = 0; j < q.Answers().length; j++) {
                var a = q.Answers()[j];
                if (a.IsChecked)
                    a.IsChecked(false);
                if (a.Distribution)
                    a.Distribution(null);
                if (q.IsMatrix()) {
                    for (var k = 0; k < a.MatrixData().length; k++) {
                        var data = a.MatrixData()[k];
                        data(false);
                    }
                }
            }
        }
        model.ClearPath();
        model.CurrentQuestion(model.Questions()[0]);
        model.ResultsVisible(false);

    }

    model.Save = function () {
        var request = { Items: [] };

        var questions = model.questionsPath();

        for (var i = 0; i < questions.length; i++) {
            var q = questions[i];
            var answers = q.Answers();
            for (var j = 0; j < answers.length; j++) {
                var a = answers[j];
                if (q.IsSingle() || q.IsMultiple()) {
                    if (a.IsChecked()) {
                        var item = {
                            QuestionId: q.Id(),
                            AnswerId: a.IdAnswer(),
                            IsCorrect: true,
                            Percentage: 0,
                            MatrixData: []
                        };
                        request.Items.push(item);
                    }
                } else if (q.IsDistribution()) {
                    var min = q.Percentage();
                    var distribution = clearDistribution(a.Distribution());
                    var item = {
                        QuestionId: q.Id(),
                        AnswerId: a.IdAnswer(),
                        IsCorrect: distribution >= min,
                        Percentage: distribution,
                        MatrixData: []
                    };
                    request.Items.push(item);
                } else if (q.IsMatrix()) {
                    var item = {
                        QuestionId: q.Id(),
                        AnswerId: a.IdAnswer(),
                        IsCorrect: true,
                        Percentage: 0,
                        MatrixData: []
                    };

                    for (var k = 0; k < q.MatrixColumns().length; k++) {
                        var checked = a.MatrixData()[k]();
                        item.MatrixData.push({
                            ID: q.MatrixColumns()[k].ID(),
                            IsChecked: checked
                        });
                        item.IsCorrect = item.IsCorrect && checked;
                    }

                    request.Items.push(item);
                }

            }

        }

        request.htmlBody = $("#resultadoFinal").html();

        model.Saving(true);
        $("#loadingArea").show();

        $.ajax({
            url: '/Suitability/Questions',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(request),
            success: function (data, status, xhr) {
                if (data.ok) {
                    $("#loadingArea").hide();
                    model.ResultMessage('Seu teste de perfil de investidor foi finalizado com sucesso!');
                }
                else {
                    model.ResultMessage(data.message);
                }
            }
        });

    }

    model.UpdateScore = function () {

        var score = 0;
        var questions = model.questionsPath();
        for (var i = 0; i < questions.length; i++) {
            var q = questions[i];
            var questionScore = 0;

            for (var j = 0; j < q.Answers().length; j++) {
                var a = q.Answers()[j];

                if (q.IsSingle() || q.IsMultiple()) {
                    if (a.IsChecked())
                        questionScore += a.Point();
                } else if (q.IsDistribution()) {
                    var mininum = q.Percentage();
                    var dist = a.Distribution();
                    dist = clearDistribution(dist);
                    dist = Number(dist);
                    if (dist >= mininum) {
                        questionScore += a.Point();
                    }
                } else if (q.IsMatrix()) {
                    var allChecked = true;
                    for (var k = 0; k < q.MatrixColumns().length; k++) {
                        allChecked = allChecked && a.MatrixData()[k]();
                    }
                    if (allChecked) {
                        questionScore += a.Point();
                    }
                }
            }

            questionScore *= q.Weight();
            score += questionScore;


        }



        for (var i = 0; i < model.Scores().length; i++) {
            var s = model.Scores()[i];
            if (score >= s.Min() && score < s.Max()) {
                model.ProfileId(s.TypeId());
                model.ProfileDescription(s.Description());
            }
        }

        return score;
    }

    function clearDistribution(value) {
        if (!value)
            return 0;
        value = value.toString();
        value = value.replace(/\D/g, '');
        value = parseInt(value);
        if (isNaN(value))
            return 0;
        return value;
    }

    ko.applyBindings(model);
})