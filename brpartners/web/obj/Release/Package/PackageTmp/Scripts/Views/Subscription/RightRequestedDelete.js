﻿$(function () {

    $("#frmRightRequestedDelete").ajaxForm({
        beforeSend: function () {
            $("#divSubscriptionFeedback").hide().find("span").html('');
            $("#feedbackError").hide().find("span").html('');
        },
        success: function (data) {
            $("#loadingArea").hide();

            if (data.Result) {
                selector = "#divSubscriptionFeedback";
                //oTableRequestRights.fnDraw(false);
                $("#btnSearch").click();
                $(selector).show().find("span").append(data.Message);

                Modal.Close();

                RequestRightsMethods.RequestRightsPendents();
            }
            else {

                selector = "#feedbackError";

                if (data.Message.indexOf("Arquivo inválido") > -1) {
                    $("#requestFeedbackError").show().focus().find('ul').html('').append(
                            $("<li>" + data.Message + "</li>").clone()
                        );
                }
                else {

                    $(selector).show().find("span").append(data.Message);
                    Modal.Close();
                }

            }

        }
    });

    $("#btnCancel").click(function () { Modal.Close(); });

    $("#btnDelete").click(function () {
        $("#frmRightRequestedDelete").submit();
    });
});