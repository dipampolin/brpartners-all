﻿/// <reference path="../../References.js" />

$(document).ready(function () {

    var context = $("#divFilter");
    var selectedClientName = "";

    //    $("#txtClient", context).autocomplete({
    //        source: function (request, response) {
    //            $.ajax({
    //                url: "/OnlinePortfolio/ClientSuggest",
    //                type: "POST",
    //                dataType: "json",
    //                data: { term: request.term },
    //                success: function (data) {
    //                    response($.map(data, function (item) {
    //                        return {
    //                            label: item.Description,
    //                            value: item.Description,
    //                            id: item.Value
    //                        }
    //                    }));
    //                }
    //            });
    //        },
    //        minLength: 3,
    //        select: function (event, ui) {
    //            $("#hdfClientCode", context).val(ui.item.id);
    //            selectedClientName = ui.item.value;
    //        }
    //    });

    $("#txtClient", context).blur(function () {
        var value = $(this).val();
        //            var isInt = TryParseInt(value, false);
        //            if (!isInt && value != selectedClientName) {
        //                $(this).val("");
        //                selectedClientName = "";
        //                $("#hdfClientCode", context).val("");
        //            }

        //if (value == isInt) {
        $("#hdfClientCode", context).val(value);
        //}

        if (value == "") {
            $("#hdfClientCode", context).val("");
        }
    });

    $("#btnSearch", context).click(function () {

        var clientCode = $("#hdfClientCode", context).val();
        if (clientCode == "")
            return false;

        $("#loadingArea").css({ 'display': 'block' });
        $("#divPositionArea").css({ 'display': 'none' });

        $.ajax({
            type: "POST",
            url: "/Other/LoadConsolidatedPosition/",
            dataType: "html",
            data: { clientCode: clientCode },
            success: function (data) {
                if (data != "") {
                    $("#divPositionArea").html(data).css({ 'display': 'block' });
                    $("#loadingArea").css({ 'display': 'none' });
                }
            },
            complete: function () {
                $(".linha-falsa-tabela").find("div[class^='linha-falsa-td']:not('.expandir, .vazia')").each(function () {
                    var thisValue = $(this).html();
                    thisValue = thisValue.replace(/\./g, '').replace(',', '.').replace("R", "").replace("$", "").replace(" ", "");
                    if (parseFloat(thisValue) < 0) {
                        $(this).addClass("negativo");
                    }
                    else if (parseFloat(thisValue) > 0) {
                        $(this).addClass("positivo");
                    }
                });

                $(".rodape-linha-falsa-tabela").find("div[class^='linha-falsa-td']:not('.vazia')").each(function () {
                    var thisValue = $(this).html();
                    thisValue = thisValue.replace(/\./g, '').replace(',', '.').replace("R", "").replace("$", "").replace(" ", "");
                    if (parseFloat(thisValue) < 0) {
                        $(this).addClass("negativo");
                    }
                    else if (parseFloat(thisValue) > 0) {
                        $(this).addClass("positivo");
                    }
                });

                $("#lnkRegEmail").unbind("click").bind("click", function () {
                    var id = $(this).attr("rel");
                    $.ajax({
                        type: "POST",
                        cache: false,
                        url: "../Other/SendConsolidatedPositionMail",
                        data: { clientCode: id },
                        beforeSend: function () {
                            $("#feedback").hide().find('span').html('');
                            $('#feedbackError').hide().find('span').html('');
                        },
                        success: function (data) {
                            if (data.Result) {
                                $("#feedback").show().find('span').append("E-mail enviado com sucesso");
                            }
                            else {
                                $("#feedbackError").show().find('span').append(data.Message);
                            }
                        }
                    });
                });
            }
        });

        return false;
    });

    $(context).bind('keydown', function (event) {
        if (event.which == 13) {
            $('#txtClient').blur();
            $("#btnSearch", context).click();
        }
    });

    $("#txtClient", context).setMask('9999999');

});

function TryParseInt(str, defaultValue) {
    var retValue = defaultValue;

    if (typeof str != 'undefined' && str != null && str.length > 0) {
        str = str.replace(/\./g, '');
        if (!isNaN(str)) {
            retValue = parseInt(str);
        }
    }
    return retValue;
}