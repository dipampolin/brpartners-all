﻿$(function () {

    $('> tbody > tr', '.tb-position').not('.hidden-tr').each(function () {
        $(this).attr('style', '');

        $('tr:odd').css('background', '#EEE');
    });
    
    $(".cPosition").click(function () {
        
        var mercado = $(this).attr("data-child");

        var cels = "";

        jQuery.ajax({
            url: "/Position/ConsolidatedPositionDetails",
            data: { id: mercado },
            dataType: "json",
            method: "POST",
            async: false,
            success: function (data) {

                if (data.length > 0) {

                    //console.log(data);

                    var cel = "";

                    for (var i = 0; i < data.length; i++) {

                        cel = cel + "<tr class='tr-height'><td>" + data[i].Ativo + "</td><td class='align-right'>" + formatNumber(data[i].QtdDisponivel, 0) + "</td><td class='align-right'>" + formatNumber(data[i].Preco, 2) + "</td><td class='align-right'>" + formatNumber(data[i].ValorAtual, 2) + "</td></tr>";
                        
                    }
                }

                //console.log(cel);
                $("#tb-position-" + mercado).html(cel);

                $('tr:odd').css('background', '#EEE');

            }

        });


        

    });

});


function formatNumber(value, decimal) {
    
    var retValue = value.toString();
    if (typeof value == 'number') {
        
        var number = value.toFixed(decimal).toString();
        number += '';
        x = number.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? ',' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        retValue = x1 + x2;
    }
    
    if (retValue.indexOf('-') == -1) {
        return "<span class='span-azul'>" + retValue + "</span>"        
    } else {
        return "<span class='span-vermelho'>" + retValue + "</span>";
    }

    return retValue;
}

var ShowPostionDetails = function () {


    
}