﻿var representationModel = [];

function showModal() {
    $('.modal-representation').show();
}

function showConfirmation() {
    $('#dialog-form').show();
}

function redirectPage() {
    window.location = '/Account/Rules/'
}

function closeModal() {
    $('.modal-representation').hide();
}

function save() {
    var rows = document.getElementById('contatoTable').getElementsByTagName("tr");
    for (var i = 1; i < rows.length; i++) {
        var cells = rows[i].cells;

        if (i > 1)
            document.getElementById("HdnContato").value += ";"

        for (var a = 0; a < (cells.length - 1); a++) {
            if (a > 0)
                document.getElementById("HdnContato").value += ",";

            document.getElementById("HdnContato").value += cells[a].id;
        }
    }

    var rowsEnd = document.getElementById('enderecoTable').getElementsByTagName("tr");
    for (var i = 1; i < rowsEnd.length; i++) {
        var cells = rowsEnd[i].cells;

        if (i > 1)
            document.getElementById("HdnEndereco").value += ";"

        for (var a = 0; a < (cells.length - 1); a++) {
            if (a > 0)
                document.getElementById("HdnEndereco").value += ",";

            document.getElementById("HdnEndereco").value += cells[a].id;
        }
    }

    closeModal();
}

function addDataToTable(model) {
    $("#representationTable").append(`<tr style='background: none;'>${this.buildRows(model)}</tr>`);
}

function buildRows(row) {
    var rows = "";
    $.each(row, function(property, propretyValue) {
        rows += `<td>${propretyValue}</td>`;
    });

    return rows;
}

function cleanInputs() {
    $(".modal-representation :input").each(function(index, input) {
        if (input.type == "text") {
            input.value = "";
        }
    });
}

function displayElement(valueDrop, element) {
    var value = valueDrop.options[valueDrop.selectedIndex].text;

    if (value.toLowerCase() == 'sim') {
        changeDisplay(element.id, "block");
    }
    else {
        changeDisplay(element.id, "none");
    }
}

function changeDisplay(element, display) {
    document.getElementById(element).style.display = display;
}

function addNewContact() {
    createElementTable('.contato', 'contatoTable')
}

function addNewAddress() {
    createElementTable('.endereco', 'enderecoTable')
}

function createElementTable(css, table) {
    var elementTr = createRow(table);
    var inputs = document.querySelectorAll(css);

    inputs.forEach(function(input) {

        if (input.type == "select-one")
            AddNewRowTable(input.options[input.selectedIndex].value, input.options[input.selectedIndex].text, elementTr);
        else
            AddNewRowTable(input.value, input.value, elementTr);
    });

    createButtonAction(elementTr);
    hiddenElementTable();
}

function removeParent(event) {
    event.target.parentNode.parentNode.classList.add('fadeOut');

    setTimeout(function() {
        event.target.parentNode.parentNode.remove();
        hiddenElementTable();
    }, 500);
}

function hiddenElementTable() {
    var rows = document.getElementById('enderecoTable').getElementsByTagName("tr").length
    if (rows > 1) {
        document.getElementById('enderecoTable').style.display = '';
    }
    else
        document.getElementById('enderecoTable').style.display = 'none';

    var rows1 = document.getElementById('contatoTable').getElementsByTagName("tr").length
    if (rows1 > 1) {
        document.getElementById('contatoTable').style.display = '';
    }
    else
        document.getElementById('contatoTable').style.display = 'none';
}

function removeRepresentative(event, value) {
    setTimeout(function() {
        $.ajax({
            url: '/Account/DeleteRepresentative/',
            data: { idRepresentative: value },
            type: 'GET',
            datatype: 'json',
            success: function(response) {
                event.target.parentNode.parentNode.classList.add('fadeOut');
                event.target.parentNode.parentNode.remove();
            }
        });
    }, 500);
}

function editRepresentative(value) {
    $.ajax({
        url: '/Account/GetRepresentative/',
        data: { idRepresentative: value },
        type: 'GET',
        datatype: 'json',
        success: function(response) {
            $('#IdRepresentation').val(response.IdRepresentation);
            $('#Name').val(response.Name);
            $('#Birthdate').val(dataFormatada(response.Birthdate));
            $('#Birthplace').val(response.Birthplace);
            $('#Nacionality').val(response.Nacionality);
            $('#MaritalStatus').val(response.MaritalStatus);
            $('#Profession').val(response.Profession);
            $('#IdRepresentationForm').val(response.IdRepresentationForm);
            $('#CssExpired').val(dataFormatada(response.CssExpired));
            $('#CssQualification').val(dataFormatada(response.CssQualification));
            $('#DocumentType').val(response.DocumentType);
            $('#NumberDocument').val(response.NumberDocument);
            $('#IssuingAgency').val(response.IssuingAgency);
            $('#ValidateDocument').val(dataFormatada(response.ValidateDocument));
            $('#IssuingDate').val(dataFormatada(response.IssuingDate));
            $('#WorkGovernment').val(response.WorkGovernment);
            $('#Agency').val(response.Agency);
            $('#StartDateJobGovernment').val(dataFormatada(response.StartDateJobGovernment));
            $('#FinalDateJobGovernment').val(dataFormatada(response.FinalDateJobGovernment));
            $('#IdProfile').val(response.IdProfile);
            $('#RelativeWorkGovernment').val(response.RelativeWorkGovernment);
            $('#RelativeName').val(response.RelativeName);
            $('#RelativeRelationship').val(response.RelativeRelationship);
            $('#JobRelativeName').val(response.JobRelativeName);
            $('#TaxResidence').val(response.TaxResidence);
            $('#TaxCountry').val(response.TaxCountry);
            $('#TaxNameResidence').val(response.TaxNameResidence);
            $('#RelationshipBrPartners').val(response.RelationshipBrPartners);

            response.Contact.forEach(function(cont) {
                var elementTr = createRow('contatoTable');
                AddNewRowTable(cont.Type, cont.DescriptionType, elementTr);
                AddNewRowTable(cont.Ddi, cont.Ddi, elementTr);
                AddNewRowTable(cont.Ddd, cont.Ddd, elementTr);
                AddNewRowTable(cont.Number, cont.Number, elementTr);
                AddNewRowTable(cont.Ramal, cont.Ramal, elementTr);
                AddNewRowTable(cont.EmailContact, cont.EmailContact, elementTr);
                createButtonAction(elementTr);
                hiddenElementTable();
            });

            response.Address.forEach(function (ad) {
                var elementTr = createRow('enderecoTable');
                AddNewRowTable(ad.AddressType, ad.DescriptionAddressType, elementTr);
                AddNewRowTable(ad.Finality, ad.DescriptionFinality, elementTr);
                AddNewRowTable(ad.StreetType, ad.StreetType, elementTr);
                AddNewRowTable(ad.StreetName, ad.StreetName, elementTr);
                AddNewRowTable(ad.Number, ad.Number, elementTr);
                AddNewRowTable(ad.City, ad.City, elementTr);
                AddNewRowTable(ad.State, ad.DescriptionState, elementTr);
                AddNewRowTable(ad.Country, ad.DescriptionCountry, elementTr);
                createButtonAction(elementTr);
                hiddenElementTable();
            });

            $('.modal-representation').show();
        }
    });
}

function dataFormatada(d) {
    if (d != null) {
        var dateFormat = d.replace('/Date(', '').replace(')/', '');
        var data = new Date(parseInt(dateFormat));
        var dia = data.getDate() < 10 ? '0' + data.getDate() : data.getDate();
        var mes = data.getMonth() + 1 < 10 ? '0' + (data.getMonth() + 1) : (data.getMonth() + 1);
        var ano = data.getFullYear();
        return [ano, mes, dia].join('-');
    }
}

function AddNewRowTable(id, value, elementTr) {
    var newElem = document.createElement('td');
    newElem.textContent = value;
    newElem.id = id;
    newElem.classList.add('table-td');
    elementTr.appendChild(newElem);
}

function createRow(table) {
    var elementTr = document.createElement('tr');
    var table = document.querySelector('#' + table);
    table.appendChild(elementTr);

    return elementTr;
}

function createButtonAction(element) {
    var TdInput = document.createElement('td');
    var elementInput = document.createElement("input");
    elementInput.setAttribute("type", "button");
    elementInput.setAttribute("value", "X");
    elementInput.className = 'center-component';
    TdInput.appendChild(elementInput);
    element.appendChild(TdInput);

    elementInput.addEventListener('click', function(event) {
        removeParent(event);
    });
}