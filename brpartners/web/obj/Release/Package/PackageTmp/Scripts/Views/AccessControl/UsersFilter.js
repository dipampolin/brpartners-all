﻿$(function () {
    var context = $('#UsersFilterContent');
//    $('select').styleInputs();

    $("#UsersFilter").click(function () {
        $('#UsersFilterContent').slideToggle('fast');
        $(this).toggleClass('aberto');
    });

    $("#txtUserName", context).autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/AccessControl/UserAutoSuggest",
                type: "POST",
                dataType: "json",
                data: { term: request.term },
                success: function (data) {
                    $("#hdfUser", context).val("");
                    response($.map(data, function (item) {
                        return {
                            label: item.CdCliente + ' - ' + item.Name,
                            value: item.Name,
                            id: item.ID
                        }
                    }));
                }
            });
        },
        minLength: 1,
        select: function (event, ui) {
            $("#hdfUser", context).val(ui.item.id);
        }

    });

//    $("#txtAssessor", context).autocomplete({
//        source: function (request, response) {
//            $.ajax({
//                url: "/AccessControl/AssessorAutoSuggest",
//                type: "POST",
//                dataType: "json",
//                data: { term: request.term },
//                success: function (data) {
//                    $("#hdfAssessor", context).val("");
//                    response($.map(data, function (item) {
//                        return {
//                            label: item.Name,
//                            value: item.Name,
//                            id: item.ID
//                        }
//                    }));
//                }
//            });
//        },
//        minLength: 3,
//        select: function (event, ui) {
//            $("#hdfAssessor", context).val(ui.item.id);
//        }

//    });

});

