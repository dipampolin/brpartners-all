﻿/// <reference path="../../References.js" />

var oTable;
var arrayOfCodes = [];
var emptyDataMessage = "Sua pesquisa não encontrou resultados válidos. Tente novamente.";

$(function () {
    handlers();
});

function handlers() {
    var context = $("#filterArea");

    $("#txtClientCode").blur(function () {
        commomDataHelper.GetClientName($(this).val(), "lblTraderName", context);
    });

    $("#txtTradingDate", context).blur(function () {
        $("#hdfTradingDate", context).val($(this).val());
    });

    $("#lnkSearchAll").click(function () {
        $("#txtClientCode").val("");
        $("#lblTraderName").html("");
    });

    $(context).bind('keydown', function (event) {
       	if (event.which == 13) {
       		$("#btnSearch").click();
       		return false;
       	}
    });

    $("#btnSearch, #lnkSearchAll", context).click(function () {
        $("#checkAll").attr("checked", false);
        $("#reportArea").html("");
        $("#reportArea").hide();
        $("#feedbackError", context).hide();
        $("#lblTradingDate", $("#tableArea")).html($("#txtTradingDate", context).val());
        searchRequest($("#txtTradingDate").val(), $("#txtClientCode").val(), context);
    });

    $("#checkAll", $("#tableArea")).click(function () {
        checkAll();
    });

    $("#txtTradingDate", context).blur(function () {
        checkDate(this);
    });

    $("#txtTradingDate").setMask("99/99/9999");
    $("#txtClientCode").setMask("999999999");

    $("#txtTradingDate").datepicker();
    $.datepicker.setDefaults($.datepicker.regional['pt-BR']);
    $("#txtTradingDate").datepicker($.datepicker.regional['pt-BR']);

}

function searchRequest(tradingDate, clientCode, context) {
    commonHelper.ShowLoadingArea();
    $.ajax({
        async: false,
        url: "../NonResidentTraders/Confirmation",
        type: "POST",
        data: { "txtTradingDate": tradingDate, "txtClientCode": clientCode },
        dataType: "json",
        success: function (data) {
            emptyDataMessage = clientCode != "" ? "Sua pesquisa por '#" + clientCode + "' não encontrou resultados válidos. Tente novamente." : "Sua pesquisa não encontrou resultados válidos. Tente novamente.";

            if (data.Status == "True") {
                $("#vlnTradingDate", $("#filterArea")).html("");
                var tableId = "clientTable";
                $("#hdfTradingDate", context).val(tradingDate);
                obj = {
                    "bSort": true,
                    "bAutoWidth": false,
                    "bInfo": true,
                    "bSort": true,
                    "bLengthChange": false,
                    "oLanguage": {
                        "sProcessing": "Carregando...",
                        "sLengthMenu": "<span>Exibir:</span> _MENU_ <span>por página</span>",
                        "sZeroRecords": emptyDataMessage,
                        "oPaginate": {
                            "sPrevious": "Anteriores",
                            "sNext": "Próximos"
                        }
                    },
                    "sPaginationType": "full_numbers",
                    "sDom": 'rtip',
                    "fnDrawCallback": function () {
                        var totalRecords = this.fnSettings().fnRecordsDisplay();

                        if (totalRecords == 0) {
                            $("#emptyArea").html(emptyDataMessage);
                            $("#emptyArea").show();
                            $("#tableArea").hide();
                        }
                        else {
                            $("#tableArea").show();
                            $("#emptyArea").hide();

                            var totalPerPage = this.fnSettings()._iDisplayLength;
                            var totalPages = Math.ceil(totalRecords / totalPerPage);

                            var currentIndex = parseInt(this.fnSettings()._iDisplayStart);
                            var currentPage = totalPages > 0 ? Math.ceil(1 + (currentIndex / totalPerPage)) : 0;

                            $("#" + tableId + "_info").html("Exibindo " + currentPage + " de " + totalPages);

                            if (totalRecords > 0 && totalPages > 1) {
                                $("#" + tableId + "_info").css("display", "");
                                $("#" + tableId + "_length").css("display", "");
                                $("#" + tableId + "_paginate").css("display", "");
                            }
                            else {
                                $("#" + tableId + "_info").css("display", "none");
                                $("#" + tableId + "_length").css("display", "none");
                                $("#" + tableId + "_paginate").css("display", "none");
                            }
                        }
                        // Adiciona a classe 'ultimo' à última tr no corpo da tabela
                        $('table tbody tr:last-child').addClass('ultimo');

                        // Adiciona a classe 'ultimo' às últimas th e td no corpo da tabela
                        $('table tr td:last-child').addClass('ultimo');
                        $('table tr th:last-child').addClass('ultimo');
                        $("#loadingArea").hide();
                    },
                    "bProcessing": false,
                    "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                        $("#" + tableId + "_first").css("visibility", "hidden");
                        $("#" + tableId + "_last").css("visibility", "hidden");
                    },
                    "bRetrieve": true,
                    "bServerSide": true,
                    "sAjaxSource": "../DataTables/NonResidentTraders_Confirmation",
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        $.ajax({
                            cache: false,
                            dataType: 'json',
                            type: "POST",
                            url: sSource,
                            data: aoData,
                            success: fnCallback
                        });
                    }
                };
                oTable = $("#clientTable").dataTable(obj);
                oTable.fnClearTable();
                oTable.fnDraw();
                $(".unbind", $("#tableArea")).unbind("click");

               // if (oTable != null && oTable.length > 0 && oTable[0].rows.length > 3) {
                    //$("#clientTable tbody", context).html("");
                    //$("#tableArea").show();
                    //$("#reportArea").hide();
                //}
            }
            else {
                for (var i = 0; i < data.Messages.length - 1; i++) {
                    $("#" + data.Messages[i], $("#filterArea")).html(data.Messages[i + 1]);
                }
            }
        }
    });
}

function checkAll() {
    var context = $("#tableArea");
    var elements = $("#clientTable tbody", context).find("input:checkbox");

    for (var i = 0; i < elements.length; i++) {
        var element = $(elements[i]);

        if ($("#checkAll", context).is(":checked")) {
            element.attr("checked", true);
        }
        else {
            element.attr("checked", false);
        }
    }
}

function checkDate(element) {
    var tradingDate = $(element);

    if (tradingDate.val() == "" || tradingDate.val().toString().length < 10) {
        tradingDate.val("");
        var date = new Date();
        var day = (date.getDate().toString().length == 1) ? "0" + date.getDate().toString() : date.getDate().toString();
        var month = ((date.getMonth() + 1).toString().length == 1) ? "0" + (date.getMonth() + 1).toString() : (date.getMonth() + 1).toString();
        var year = date.getFullYear();

        tradingDate.val(day + "/" + month + "/" + year);
    }
}

function viewReport(code) {
    commonHelper.ShowLoadingArea();
    $("#tableArea").hide();
    $("#reportArea").html("");
    $("#reportArea").show();
    commonHelper.LoadView("../NonResidentTraders/ConfirmationReport?code=" + code + "&date=" + $("#hdfTradingDate").val() + " #reportArea", "#reportArea", function () {
        setTimeout(function () { 
            $("#loadingArea").hide();
        }, 350);
    });
    
    
}

function backToList() {
    $("#tableArea").show();
    $("#reportArea").hide();
}

function addCodeIntoArray(code, isIncludeAll) {
    var context = $("#tableArea");

    if (!isIncludeAll) {
        if ($("#client" + code, context).is(":checked")) {
            for (var i = 0; i < arrayOfCodes.length; i++) {
                if (arrayOfCodes[i] == code)
                    return false;
            }

            arrayOfCodes.push(code);
        } else {
            for (var i = 0; i < arrayOfCodes.length; i++) {
                if (arrayOfCodes[i] == code)
                    arrayOfCodes.splice(i, 1);
            }
            $("#checkAll", context).attr("checked", false);
        }
    }
    else {
        if ($("#checkAll", context).is(":checked")) {
            arrayOfCodes = [];
            var elements = $("#clientTable tbody", context).find("input:checkbox");

            for (var i = 0; i < elements.length; i++) {
                var element = $(elements[i]);
                arrayOfCodes.push(element.val());
            }
        } else {
            arrayOfCodes = [];
        }
    }

    var codes;
    for (var i = 0; i < arrayOfCodes.length; i++) {
        if (i == 0)
            codes = arrayOfCodes[i];
        else
            codes += ";" + arrayOfCodes[i];
    }

    if (arrayOfCodes.length > 0) {
        var PDF_url = "../NonResidentTraders/ConfirmationPDFReportsToZip?";
        var Excel_url = "../NonResidentTraders/ConfirmationExcelReportsToZip?";
        var Print_url = "../NonResidentTraders/PrintConfirmationReport?";

        var PDF_url = PDF_url + "codes=" + codes + "&date=" + $("#hdfTradingDate").val();
        var Excel_url = Excel_url + "codes=" + codes + "&date=" + $("#hdfTradingDate").val();
        var Print_url = Print_url + "codes=" + codes + "&date=" + $("#hdfTradingDate").val();

        $("#confirmationPDFReportsToZip").attr("href", PDF_url);
        $("#confirmationExcelReportsToZip").attr("href", Excel_url);
        $("#printConfirmationReports").attr("href", Print_url);
    }
    else {
        $("#confirmationPDFReportsToZip").removeAttr("href");
        $("#confirmationExcelReportsToZip").removeAttr("href");
        $("#printConfirmationReports").removeAttr("href");
        $("#checkAll", context).attr("checked", false);
    }
}

function CallReportPrint() {
    var content = $("#confirmationReport");
    var winPrint = window.open('', '', 'letf=0,top=0,width=1,height=1,toolbar=0,scrollbars=0,sta tus=0');
    winPrint.document.write(content.innerHTML);
    winPrint.document.close();
    winPrint.focus();
    winPrint.print();
    winPrint.close();
} 
