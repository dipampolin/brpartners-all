﻿/// <reference path="../../References.js" />
var oTable;
var previousIndex = -1;
var nextIndex = 1;
var feedbackCount = 1;
var refreshIntervalId;

var wrongFilter = false;

var emptyDataMessage = "Sua pesquisa não encontrou resultados válidos. Tente novamente.";

$(function () {
    $(document).ready(function () {

        $('select').styleInputs();

        $.mask.masks.Discount = { mask: '99,999', type: 'reverse', defaultValue: '000' };
        $("#txtDiscount").setMask();

        var filterContext = "#PortfolioFilterContent";

        //Filter
        $("#PortfolioFilter").click(function () {
            $('#PortfolioFilterContent').slideToggle('fast');
            $(this).toggleClass('aberto');
        });

        var lnkPrevious = $("#lnkPreviousClient");
        var lnkNext = $("#lnkNextClient");
        var lnkPreviousTop = $("#lnkPreviousClientTop");
        var lnkNextTop = $("#lnkNextClientTop");

        var clientCodeFromPosition = $("#ClientCodeFromPosition").val();
        if (clientCodeFromPosition != "") {
            setTimeout(function () {
                $("#btnSearch", filterContext).click();
            }, 150);
        }

        $("#btnSearch", filterContext).click(function () {

            if (wrongFilter) {
                var ffeedback = $("#filterFeedbackError", "#PortfolioFilterContent");
                ffeedback.html("Filtro inválido.");
                ffeedback.show();
                return false;
            }
            else
                $("#filterFeedbackError", "#PortfolioFilterContent").hide();

            $("#txtDiscount").val("0,00");

            if (feedbackCount == 0)
                $("#feedbackError").hide();

            lnkPrevious.hide();
            lnkNext.hide();
            lnkPreviousTop.hide();
            lnkNextTop.hide();

            $("#emptyArea").hide();
            $("#tableArea").hide();
            $("#loadingArea").show();

            $("#filterFeedbackError", filterContext).hide();

            if ($(filterContext).css("display") != "none") {
                $('#PortfolioFilterContent').slideToggle('fast');
                $(filterContext).toggleClass('aberto');
            }

            var clientCode = $("#hdfFilterClientCode", filterContext).val();

            var clientCodeFromRequest = $("#ClientCodeFromPosition").val();
            if (clientCodeFromRequest != "") {
                clientCode = clientCodeFromRequest;
                $("#hdfFilterClientCode", filterContext).val(clientCodeFromRequest);
                $("#txtClient", filterContext).val(clientCodeFromRequest);
            }

            var clientCode = (clientCodeFromRequest != "") ? clientCodeFromRequest : $("#hdfFilterClientCode", filterContext).val();

            var discount = parseFloat($("#txtDiscount").val().replace(',', '.'));

            $.ajax({

                cache: false,
                type: "POST",
                dataType: "json",
                async: true,
                url: "/OnlinePortfolio/LoadClientPosition",
                data: { ClientCode: clientCode, Index: $("#listIndex").val(), Discount: discount, IsFirst: true },
                success: function (data) {

                    feedbackCount = 0;

                    if (clientCodeFromRequest != "") {
                        $("#ClientCodeFromPosition").val("");
                    }

                    $("#loadingArea").hide();

                    var emptyData = (data == null || data.ClientName == null || data.ClientName == "");

                    var excelLink = $("#lnkPositionExcel");
                    var pdfLink = $("#lnkPositionPdf");
                    excelLink.attr("href", (emptyData) ? "#" : "/OnlinePortfolio/PositionToFile/?isPdf=false");
                    //excelLink.attr("target", (emptyData) ? "_self" : "_blank");
                    pdfLink.attr("href", (emptyData) ? "#" : "/OnlinePortfolio/PositionToFile/?isPdf=true");
                    //pdfLink.attr("target", (emptyData) ? "_self" : "_blank");

                    if (oTable != undefined)
                        oTable.fnClearTable();

                    if (emptyData) {
                        $("#hdfClientCode").val("0");
                        $("#emptyArea").html("Nenhum cliente encontrado.");
                        $("#emptyArea").show();
                        $("#tableArea").hide();
                    }
                    else {

                        SetClientData(data);

                        previousIndex = data.Index - 1;
                        nextIndex = data.Index + 1;

                        var length = data.Length;

                        if (length < 2) {
                            lnkPrevious.hide();
                            lnkNext.hide();
                            lnkPreviousTop.hide();
                            lnkNextTop.hide();
                        }
                        else {
                            if (data.Index == (length - 1)) {
                                lnkNext.hide();
                                lnkPrevious.show();
                                lnkNextTop.hide();
                                lnkPreviousTop.show();
                            }
                            else {
                                if (data.Index != 0) {
                                    lnkPrevious.show();
                                    lnkPreviousTop.show();
                                }
                                else {
                                    lnkPrevious.hide();
                                    lnkPreviousTop.hide();
                                }
                                lnkNext.show();
                                lnkNextTop.show();
                            }
                        }
                        $("#emptyArea").hide();
                        $('select').styleInputs();
                    }
                }
            });

            return false;
        });

        $("#txtClient", filterContext).autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/OnlinePortfolio/ClientSuggest",
                    type: "POST",
                    dataType: "json",
                    data: { term: request.term },
                    success: function (data) {
                        response($.map(data, function (item) {
                            return {
                                label: item.Description,
                                value: item.Description,
                                id: item.Value
                            }
                        }));
                    }
                });
            },
            minLength: 3,
            select: function (event, ui) {
                $("#hdfFilterClientCode", filterContext).val(ui.item.id);
                $("#hdfFilterClientName", filterContext).val(ui.item.value);
            }
        });

        $("#txtClient", filterContext).blur(function () {
            var selectedClient = $("#hdfFilterClientName", filterContext);
            wrongFilter = false;
            if (this.value != "") {
                if (this.value != selectedClient.val()) {
                    if (isNaN(this.value)) {
                        $("#hdfFilterClientCode", filterContext).val("");
                        //$(selectedClient).val("");
                        //$(this).val("");
                        wrongFilter = true;
                    }
                    else {
                        $("#hdfFilterClientCode", filterContext).val(this.value);
                    }
                }
            }
            else {

                $("#hdfFilterClientCode", filterContext).val("");
                $(selectedClient).val("");

            }

        });

        $("#ddlPage", "#tableArea").change(function () {
            if (this.value != "") {
                var clientCode = $("#hdfClientCode", "#tableArea").val();
                window.location = clientCode == "0" ? "/OnlinePortfolio/" + this.value + "/" : "/OnlinePortfolio/" + this.value + "/?ClientCode=" + clientCode;
            }
            else
                return false;
        });

        $(filterContext).bind('keydown', function (event) {
            if (event.which == 13) {
                $("#txtClient", filterContext).blur();
                $("#btnSearch", filterContext).click();
                return false;
            }
        });

        setTimeout(function () {
            $("#btnSearch", filterContext).click();
        }, 250);
    });
});

function SetIntervalToUpdate() {
    clearInterval(refreshIntervalId);
    refreshIntervalId = setInterval(function () {
        var discount = parseFloat($("#txtDiscount").val().replace(',', '.'));
        RefreshClientPosition(CheckClientFilter(), discount, nextIndex - 1);
    }, 60000);
}

function CheckClientFilter() {
    var hiddenPrevious = $("#lnkPreviousClientTop", "#tableArea").css("display") == "none";
    var hiddenNext = $("#lnkNextClientTop", "#tableArea").css("display") == "none";
    var filter = $("#hdfFilterClientCode", "#PortfolioFilterContent").val();
    if (hiddenNext && hiddenPrevious && filter != "")
        return filter;
    return "";
}
function SetCommentsUpdate() {
    RefreshClientPosition("", $("#txtDiscount").val(), nextIndex - 1);
}

function GetClientPosition(next) {

    $("#txtDiscount").val("0,00");

    LoadClientPosition("", 0, (!next ? previousIndex : nextIndex), true);

    return false;
}

function GetClientPositionByDiscount() {
    var discount = parseFloat($("#txtDiscount").val().replace(',', '.'));
    LoadClientPosition(CheckClientFilter(), discount, previousIndex + 1, false);
}

function SetClientData(data) {

    GetChartContent();

    var context = "#tableArea";

    $("#lblClientName", context).html(data.ClientName);
    $("#hdfClientCode", context).val(data.ClientCode);
    $("#lblClientPhone", context).html(data.ClientPhone);
    $("#lblClientEmail", context).html(data.ClientEmail);
    $("#lnkClientEmail", context).attr("href", "mailto:" + data.ClientEmail);
    $("#lblClientCounterTop", context).html(data.Length < 2 ? "" : data.Legend);
    $("#lblClientCounter", context).html(data.Length < 2 ? "" : data.Legend);

    $("#txtDiscount").val(data.CCDiscount)

    $("#lblClientStartDate", context).html(data.ClientStartDate);
    $("#lblClientStartValue", context).html(data.ClientStartValue);
    $("#lblClientBound", context).html(data.ClientBound);

    //Financial
    if (data.Financial != null) {
        $("#lblFinancialPosition", context).html(data.Financial.Position);
        $("#lblFinancialCash", context).html(data.Financial.Cash);
        $("#lblFinancialTotal", context).html(data.Financial.Total);
    }

    //IBOV
    if (data.Ibov != null) {
        $("#lblIbovToday", context).html(data.Ibov.TodayPoints);
        $("#lblIbovTodayVariation", context).html(data.Ibov.TodayVariation);
        if (data.Ibov.TodayVariation.indexOf("-") == 0)
            $("#lblIbovTodayVariation", context).parent().attr("class", "negativo");
        else
            $("#lblIbovTodayVariation", context).parent().attr("class", "positivo");
        $("#lblIbovYearDate", context).html(data.Ibov.YearDate);
        $("#lblIbovYear", context).html(data.Ibov.YearPoints);
        $("#lblIbovYearVariation", context).html(data.Ibov.YearVariation);
        if (data.Ibov.YearVariation.indexOf("-") == 0)
            $("#lblIbovYearVariation", context).parent().attr("class", "negativo");
        else
            $("#lblIbovYearVariation", context).parent().attr("class", "positivo");
    }

    //C/C
    if (data.Discount != null) {
        $("#lblDiscount", context).html(data.Discount.DayTrade);
        $("#lblAvailable", context).html(data.Discount.Available);
        $("#lblOperations", context).html(data.Discount.DailyOperations);
        $("#lblD1", context).html(data.Discount.D1);
        $("#lblD2", context).html(data.Discount.D2);
        $("#lblD3", context).html(data.Discount.D3);
    }

    $("#lblClientCounter", context).html(data.Length < 2 ? "" : data.Legend);

    var noComments = data.Comments == null || data.Comments == "";

    $("#pnlComments", context).css("display", noComments ? "none" : "block");
    $("#lnkOpenComments", context).attr("class", noComments ? "ico-incluir" : "ico-alerta");
    $("#commentsContent", context).html(data.Comments);
    $("#lnkOpenComments", "#tableArea").css("display", noComments ? "block" : "none");

    if (data.HtmlTableItems == "") {
        $("#pnlNoPosition", context).show();
        $("#positionList", context).hide();
        if (refreshIntervalId != undefined)
            clearInterval(refreshIntervalId);
    }
    else {
        $("#pnlNoPosition", context).hide();
        $("#positionList", context).show();
        $("#positionList", context).find("tbody").html(data.HtmlTableItems);
        //Atualizar de 1 em 1 minuto o conteúdo da tela.
        SetIntervalToUpdate();
    }

    $(context).show();
}

function drawChart(chartLines) {

    var chartContent = "";

    for (var i = 0; i < chartLines.length; i++) {
        var bar = document.createElement("div");
        bar.id = 'bar' + i;
        bar.className = 'barStyle';
        bar.style.bottom = 10 * chartLine.Percentage + 'px';
        bar.style.left = '4px';
        var td = "<tr><td>" + chartLine.SectorName + "</td><td>" + bar + "</td><td>" + chartLine.Allocation + "</td><td>" + chartLine.Variation + "</td></tr>";
        chartContent += td;
    }

    $("#tbChart").find("tbody").html(chartContent);
}

function GetChartContent() {
    $.ajax({

        cache: false,
        type: "POST",
        dataType: "json",
        async: true,
        url: "/OnlinePortfolio/LoadClientPositionChart",
        success: function (data) {

            var emptyData = (data == null || data == "");

            if (emptyData) {
                $("#pnlChart").hide();
            }
            else {
                $("#pnlChart").show();
                $("#tbChart", "#pnlChart").find("tbody").html(data);
            }

            // Adiciona a classe 'ultimo' à última tr no corpo da tabela
            $('table tbody tr:last-child').addClass('ultimo');

            // Adiciona a classe 'ultimo' às últimas th e td no corpo da tabela
            $('table tr td:last-child').addClass('ultimo');
            $('table tr th:last-child').addClass('ultimo');

            // Zebrado
            $('table tbody tr:odd').addClass('odd');
            $('table tbody tr:even').addClass('even');
        }
    });
}

function LoadClientPosition(clientCode, discount, index, first) {

    $("#tableArea").hide();
    $("#loadingArea").show();

    var lnkPrevious = $("#lnkPreviousClient");
    var lnkNext = $("#lnkNextClient");
    var lnkPreviousTop = $("#lnkPreviousClientTop");
    var lnkNextTop = $("#lnkNextClientTop");

    //lnkPrevious.hide();
    // lnkNext.hide();
    //lnkPreviousTop.hide();
    //lnkNextTop.hide();

    $.ajax({
        cache: false,
        type: "POST",
        dataType: "json",
        async: true,
        url: "/OnlinePortfolio/LoadClientPosition",
        data: { Index: index, Discount: discount, ClientCode: clientCode, IsFirst: first },
        success: function (data) {
            if (oTable != undefined)
                oTable.fnClearTable();

            SetClientData(data);

            $("#tableArea").show();
            $("#loadingArea").hide();

            previousIndex = data.Index - 1;
            nextIndex = data.Index + 1;

            var length = data.Length;

            if (length < 2) {
                lnkPrevious.hide();
                lnkNext.hide();
                lnkPreviousTop.hide();
                lnkNextTop.hide();
            }
            else {
                if (data.Index == (length - 1)) {
                    lnkNext.hide();
                    lnkPrevious.show();
                    lnkNextTop.hide();
                    lnkPreviousTop.show();
                }
                else {
                    if (data.Index == 0) {
                        lnkPrevious.hide();
                        lnkPreviousTop.hide();
                    }
                    else {
                        lnkPrevious.show();
                        lnkPreviousTop.show();
                    }
                    lnkNext.show();
                    lnkNextTop.show();
                }
            }
        }
    });

    return false;
}

function ToggleComments(link, hide) {
    if (link.className == "ico-incluir" || link.className.indexOf("ico-editar") >= 0) {
        var cliendCode = $("#hdfClientCode", "#tableArea").val();
        var clientName = escape($("#lblClientName", "#tableArea").html());
        commonHelper.OpenModal("/OnlinePortfolio/Observations?clientId=" + cliendCode + "&clientName=" + clientName, 400, ".content", "");
    }
    else {
        $("#pnlComments", "#tableArea").toggle();
        $("#lnkOpenComments", "#tableArea").css("display", hide ? "block" : "none");
    }
}

function DeleteComments() {
    $.ajax({

        cache: false,
        type: "POST",
        dataType: "json",
        async: true,
        url: "/OnlinePortfolio/DeleteComments",
        data: { clientCode: $("#hdfClientCode", "#tableArea").val() },
        success: function (data) {
            if (data == "") {
                var context = "#tableArea";
                $("#pnlComments", context).hide();
                $("#lnkOpenComments", context).attr("class", "ico-incluir").show();
                $("#commentsContent", context).html("");
                $("span", "#feedback").html("Comentário excluído com sucesso");
                $("#feedback").show();
            }
        }
    });
}

function RefreshClientPosition(clientCode, discount, index) {

    var lnkPrevious = $("#lnkPreviousClient");
    var lnkNext = $("#lnkNextClient");
    var lnkPreviousTop = $("#lnkPreviousClientTop");
    var lnkNextTop = $("#lnkNextClientTop");

    //lnkPrevious.hide();
    //lnkNext.hide();
    //lnkPreviousTop.hide();
    //lnkNextTop.hide();

    $.ajax({
        cache: false,
        type: "POST",
        dataType: "json",
        async: true,
        url: "/OnlinePortfolio/LoadClientPosition",
        data: { Index: index, Discount: discount, ClientCode: clientCode, IsFirst: false },
        success: function (data) {
            if (oTable != undefined)
                oTable.fnClearTable();

            SetClientData(data);

            previousIndex = data.Index - 1;
            nextIndex = data.Index + 1;

            var length = data.Length;

            if (length < 2) {
                lnkPrevious.hide();
                lnkNext.hide();
                lnkPreviousTop.hide();
                lnkNextTop.hide();
            }
            else {
                if (data.Index == (length - 1)) {
                    lnkNext.hide();
                    lnkPrevious.show();
                    lnkNextTop.hide();
                    lnkPreviousTop.show();
                }
                else {
                    if (data.Index == 0) {
                        lnkPrevious.hide();
                        lnkPreviousTop.hide();
                    }
                    else {
                        lnkPrevious.show();
                        lnkPreviousTop.show();
                    }
                    lnkNext.show();
                    lnkNextTop.show();
                }
            }
        }
    });

    return false;
}

function SendClientPosition() {
    var url = "/OnlinePortfolio/ConfirmEmail/";
    commonHelper.OpenModal(url, 450, ".content", "");
    $("#MsgBox").center();
}