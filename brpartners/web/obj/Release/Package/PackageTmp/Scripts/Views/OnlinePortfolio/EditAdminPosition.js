﻿$(function () {

    var rowCount = 0;
    var context = $("#frmEditAdminPosition");
    var table = $("#adminPositionList");

    $(document).ready(function () {

        var selectedClientName = "";

        $("#txtClient", context).autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/OnlinePortfolio/ClientSuggest",
                    type: "POST",
                    dataType: "json",
                    data: { term: request.term },
                    success: function (data) {
                        response($.map(data, function (item) {
                            return {
                                label: item.Description,
                                value: item.Description,
                                id: item.Value
                            }
                        }));
                    }
                });
            },
            minLength: 3,
            select: function (event, ui) {
                $("#hdfClientCode", context).val(ui.item.id);
                selectedClientName = ui.item.value;
            }
        });

        $("#txtClient", context).blur(function () {
            var value = $(this).val();
            var isInt = TryParseInt(value, false);
            if (!isInt && value != selectedClientName) {
                $(this).val("");
                selectedClientName = "";
                $("#hdfClientCode", context).val("0");
            }

            if (value == isInt) {
                $("#hdfClientCode", context).val(value);
            }

            if (value == "") {
                $("#hdfClientCode", context).val("0");
            }
        });

        $("#lnkAdd", context).click(function () {
            LoadTableLine();
        });

        $(".ico-excluir", table).live("click", function () {
            $(this).parents('tr').remove();
            if ($('#adminPositionList tr', context).length == 2) {
                $(".action-place-holder", table).hide();
            }

            $("#MsgBox").center();
        });

        $("#lnkSave", context).click(function () {
            var feedback = ValidateFields();
            if (feedback != "") {
                $("#feedbackRequestError", context).html(feedback);
                $("#feedbackRequestError", context).show();
                return false;
            }

            var data = "";
            data += $(context).serialize();

            $.ajax({
                url: "/OnlinePortfolio/EditAdminPosition",
                type: "POST",
                dataType: "json",
                data: data,
                success: function (data) {
                    if (data.Error == "") {
                        var msg = $("#hdfPositionId", context).val() == "0" ? "Posição inserida com sucesso." : "Posição atualizada com sucesso.";
                        Modal.Close();
                        $("div#adminPositionfeedback span").html(msg);
                        $("div#adminPositionfeedback").show();

                        if ($("#adminPositionFilterContent #txtClient").val() != 'undefined' && $("#adminPositionFilterContent #txtClient").val() != "") { $("#adminPositionFilterContent #btnSearch").click(); }
                    }
                    else {
                        $("#feedbackRequestError", context).html(data.Error);
                        $("#feedbackRequestError", context).show();
                    }
                }
            });

            return false;
        });

        $(".market", table).live("change", function () {
            var baseId = this.id;
            baseId = baseId.split('-')[1];

            if ($(this).val() == "TER") {
                $("#txtDueDate-" + baseId, table).show();
                $("#txtRollOverDate-" + baseId, table).show();
            }
            else {
                $("#txtDueDate-" + baseId, table).hide();
                $("#txtRollOverDate-" + baseId, table).hide();
            }
        });

        if ($("#hdfPositionId", context).val() == "0") {
            LoadTableLine();
        }
        else {
            $("#txtPositionDate-1, #txtDueDate-1, #txtRollOverDate-1", table).datepicker($.datepicker.regional['pt-BR']).setMask("99/99/9999");

            $.mask.masks.DecimalValue = { mask: '99,999.999.999', type: 'reverse', defaultValue: '000' };
            $("#txtPrice-1, #txtNetPrice-1", table).setMask('DecimalValue');

            $.mask.masks.Quantity = { mask: '999.999.999.999.999', type: 'reverse', defaultValue: '0' };
            $("#txtQuantity-1", table).setMask('Quantity');

            $("#txtPortfolioNumber-1", table).setMask('99999');
            $("#txtStock-1", table).css("text-transform", "uppercase");
        }
    });

    function LoadTableLine() {

        //        for (s in data.Series) {
        //            series += NewSeriesModel(data.Series[s].InitialValue, data.Series[s].MaximumValue, data.Series[s].Quota);
        //        }

        var newTr = "";
        rowCount = $('#adminPositionList tr', context).length;
        var classTr = (rowCount % 2 == 0) ? "even" : "odd";

        newTr += "<tr class='" + classTr + "'>";
        newTr += "<td>" + InputText("txtStock-") + "</td>";
        newTr += "<td>" + DropDownList("ddlOperation-", "operation") + "</td>";
        newTr += "<td>" + DropDownList("ddlMarket-", "market") + "</td>";
        newTr += "<td>" + DropDownList("ddlLongShort-", "longshort") + "</td>";
        newTr += "<td>" + InputText("txtQuantity-") + "</td>";
        newTr += "<td>" + InputText("txtPrice-") + "</td>";
        newTr += "<td>" + InputText("txtNetPrice-") + "</td>";
        newTr += "<td>" + InputText("txtPortfolioNumber-") + "</td>";
        newTr += "<td>" + InputText("txtPositionDate-") + "</td>";
        newTr += "<td>" + InputText("txtDueDate-") + "</td>";
        newTr += "<td>" + InputText("txtRollOverDate-") + "</td>";
        newTr += "<td class='action-place-holder' style='display:none;'>" + RemoveLink() + "</td>";
        newTr += "</tr>";

        $("#adminPositionList tbody", context).append(newTr);

        $('select').styleInputs();
        $("#txtPositionDate-" + rowCount + " , #txtDueDate-" + rowCount + " , #txtRollOverDate-" + rowCount, table).datepicker($.datepicker.regional['pt-BR']).setMask("99/99/9999");

        $.mask.masks.DecimalValue = { mask: '99,999.999.999', type: 'reverse', defaultValue: '000' };
        $("#txtPrice-" + rowCount + " , #txtNetPrice-" + rowCount, table).setMask('DecimalValue');

        $.mask.masks.Quantity = { mask: '999.999.999.999.999', type: 'reverse', defaultValue: '0' };
        $("#txtQuantity-" + rowCount, table).setMask('Quantity');

        $("#txtPortfolioNumber-" + rowCount, table).setMask('99999');
        $("#txtStock-" + rowCount, table).css("text-transform", "uppercase");

        if (rowCount > 1) {
            $(".action-place-holder", table).show();
        }

        $("#MsgBox").center();
    }

    function InputText(name) {
        var style = "";
        if (name == "txtDueDate-" || name == "txtRollOverDate-") { style = " style='display:none;' "; }

        var maxlength = "";
        if (name == "txtStock-") { maxlength = " maxlength = '12' "; }

        return "<input type='text' id='" + name + rowCount + "' name='" + name.replace('-', '') + "' " + style + maxlength + " />";
    }

    function DropDownList(name, type) {

        var c = "<select id='" + name + rowCount + "' name='" + name.replace('-', '') + "' class='" + type + "'>";

        if (type == "operation") { c += "<option value='C'>Compra</option><option value='V'>Venda</option>"; }
        else if (type == "market") { c += "<option value='VIS'>Vista</option><option value='TER'>Termo</option><option value='OPC'>Opção</option>"; }
        else if (type == "longshort") { c += "<option value='1'>Sim</option><option value='0'>Não</option>"; }

        c += "</select>";

        return c;
    }

    function RemoveLink() {
        if (rowCount == 1)
            return " ";

        return "<a href='javascript:void(0);' class='ico-excluir' id='lnkRemove'>x</a>";
    }

    function ValidateFields() {
        var feedback = "";

        if ($("#hdfClientCode", context).val() == "0") {
            feedback += "<li>É necessário preencher um cliente.</li>";
        }

        $("input", context).each(function () {
            if ($(this).val() == "" && $(this).css('display') != 'none' && feedback == "") {
                feedback += "<li>É necessário preencher todos os campos.</li>";
            }
        });

        if (feedback == "") {
            $("input[id*='txtQuantity-']", context).each(function () {
                var isInt = TryParseInt($(this).val(), false);
                if (!isInt) {
                    feedback += "<li>É necessário preencher uma quantidade.</li>";
                }
            });

            var emptyPrice = false;
            $("input[id*='txtPrice-'], input[id*='txtNetPrice-']", context).each(function () {
                if ($(this).val() == "0,00" && !emptyPrice) {
                    feedback += "<li>É necessário preencher um valor para o preço.</li>";
                    emptyPrice = true;
                }
            });
        }

        var invalidDate = false;
        $("input[id*='Date-']", context).each(function () {
            if ($(this).val() != "" && !invalidDate) {
                if (!DateValidate($(this).val())) {
                    invalidDate = true;
                    feedback += "<li>Existe data inválida.</li>";
                }
            }
        });

        return feedback;
    }

    function TryParseInt(str, defaultValue) {
        var retValue = defaultValue;

        if (typeof str != 'undefined' && str != null && str.length > 0) {
            str = str.replace(/\./g, '');
            if (!isNaN(str)) {
                retValue = parseInt(str);
            }
        }
        return retValue;
    }

    function DateValidate(date) {
        var reDate = /^((0[1-9]|[12]\d)\/(0[1-9]|1[0-2])|30\/(0[13-9]|1[0-2])|31\/(0[13578]|1[02]))\/\d{4}$/;
        if (reDate.test(date) || (date == '')) {
            return true
        } else {
            return false;
        }
    }

});
