﻿/// <reference path="../../References.js" />

$(function () {

    var context = $("#filterArea");

    $("#btnRequest", context).click(function () {
        var password = $("#txtPassword", context).val();
        var confirmPassword = $("#txtConfirmPassword", context).val();
        var error = $("#feedbackRequestError", context);

        if (password == "") {
            $(error).html("<ul><li>Senha em branco</li></ul>");
            $(error).show();
            return false;
        }

        if (password != confirmPassword) {
            $(error).html("<ul><li>Senhas não conferem.</li></ul>");
            $(error).show();
            return false;
        }
        else {
            var message = validatePassword(password);
            if (message != "") {
                $(error).html(message);
                $(error).show();
            }
            else
                $("#frmRedefinePassword").submit();
        }
    });

});

function validatePassword(password) {
    var messageError = "";
    var letter = new RegExp("[a-zA-Z]");
    var number = new RegExp("[0-9]");
    var especial = new RegExp("[!#$%&'()*+,-./:;?@[\\\]_`{|}~]");
    
    if (password.length < 6)
        messageError += "<li>Senha deve possuir no mínimo 6 caracteres.</li>";

    if ((!letter.test(password)) || (!number.test(password) || (!especial.test(password))))
        //messageError += "<li>Senha deve possuir pelo menos uma letra e um número.</li>";
        messageError += "<li>Senha deve possuir pelo menos uma letra, um número e um caracter especial.</li>";

    if (messageError.length > 0)
        messageError = "<ul>" + messageError + "</ul>";

    return messageError;
}