﻿$(document).ready(function () {
    SortOptions.AddSortOptions();
    handlers();
});

function handlers() {
    var context = $("#historyArea");

    var emptyDataMessage = "Sua pesquisa não encontrou resultados válidos. Tente novamente.";

    $("#tbHistory", context).dataTable(
	{
	    "bPaginate": true,
	    "bLengthChange": false,
	    "bFilter": false,
	    "bInfo": false,
	    "bAutoWidth": false,
	    "iDisplayLength": 10,
	    "bSort": true,
	    "sPaginationType": "full_numbers",
        "aoColumns": [
            { sType: "date-euro" },
            null,
            null
        ],
	    "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
	        $('#tbHistory_previous').text('Anteriores');
	        $('#tbHistory_next').text('Próximos');
	        $('#tbHistory_previous').attr('title', 'Anteriores');
	        $('#tbHistory_next').attr('title', 'Próximos');
	        $('#tbHistory_first').hide();
	        $('#tbHistory_last').hide();

	        var numberOfItems = 10;

	        var totalPages = parseInt(aaData.length / numberOfItems) + ((aaData.length % numberOfItems) > 0 ? 1 : 0);

	        var page = (iStart / numberOfItems) + 1;

	        if (page == 1) {
	            $('#tbHistory_previous').css("display", "none");
	            $('#tbHistory_next').css("visibility", (totalPages > 1) ? "visible" : "hidden");
	        }

	        else {
	            $('#tbHistory_previous').css("display", "");
	            $('#tbHistory_next').css("visibility", (page == totalPages) ? "hidden" : "visible");
	        }

	        $('#tbHistory_length').css("visibility", (aaData.length <= 10) ? "hidden" : "visible");
	        $('#tbHistory_length>select').addClass("styled");
	        $('#tbHistory_paginate').css("display", (totalPages == 1) ? "none" : "block");

	    },
	    "oLanguage": {
	        "sZeroRecords": "Não existe registro de histórico."
	    }
	});

}
