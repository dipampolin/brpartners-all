﻿/// <reference path="../../References.js" />

jQuery.extend(jQuery.fn.dataTableExt.oSort, {
	"numeric-comma-pre": function (a) {
		var x = (a == "-") ? 0 : a.replace(/,/, ".");
		return parseFloat(x);
	},

	"numeric-comma-asc": function (a, b) {
		//return ((a < b) ? -1 : ((a > b) ? 1 : 0));
		var x = (a == "-") ? 0 : a.replace(/\./g, "").replace(/,/, ".");
		var y = (b == "-") ? 0 : b.replace(/\./g, "").replace(/,/, ".");
		x = parseFloat(x);
		y = parseFloat(y);
		return ((x < y) ? -1 : ((x > y) ? 1 : 0));
	},

	"numeric-comma-desc": function (a, b) {
		//return ((a < b) ? 1 : ((a > b) ? -1 : 0));
		var x = (a == "-") ? 0 : a.replace(/\./g, "").replace(/,/, ".");
		var y = (b == "-") ? 0 : b.replace(/\./g, "").replace(/,/, ".");
		x = parseFloat(x);
		y = parseFloat(y);
		return ((x < y) ? 1 : ((x > y) ? -1 : 0));
	}
});

var oTableRequestRights = null;

var fnDrawCallback = function (settings) {
    $("#loadingArea").hide();
    $("#tableArea").show();

    RequestRightsMethods.enableExportButtons();

    RequestRightsMethods.RequestRightsEdit();
    RequestRightsMethods.RequestRightsDetail();
    RequestRightsMethods.RequestRightsDelete();
    RequestRightsMethods.RequestRightsHistoryDetail();
}

var objRequestRightsFilter = {
	"bAutoWidth": false
    , "bPaginate": true
    , "bDestroy": true
    , "sPaginationType": "full_numbers"
    , "sDom": 'trip'
    , "iDisplayLength": 50
    , "oLanguage": {
    	"sInfo": ""
    }
    , "bProcessing": true
    , "bServerSide": true
	, "bDeferRender": true
    , "sAjaxSource": "../Subscription/RequestRightsFilter"
    , "fnServerData": function (sSource, aoData, fnCallback) {
        var that = this;

    	if (aoData != null && aoData != undefined) {
    		aoData.push({ "name": "txtClient", "value": $("#txtClient").val() });
    		aoData.push({ "name": "txtAssessor", "value": $("#txtAssessor").val() });
    		aoData.push({ "name": "txtStockCode", "value": $("#txtStockCode").val() });
    		aoData.push({ "name": "ddlStatus", "value": $('#ddlStatus').val() });
    		aoData.push({ "name": "ddlWarning", "value": $('#ddlWarning').val() });
    		aoData.push({ "name": "txtInitialDate", "value": $("#txtInitialDate").val() });
    		aoData.push({ "name": "txtBrokerDate", "value": $("#txtBrokerDate").val() });
    		aoData.push({ "name": "hPosicao", "value": $("#hPosicao").val() });
    	}
    	var legenda = "";
    	var divPosicao = "";
    	var btnAnterior = "";
    	var btnProxima = "";
    	var changeSubscrition = false;
    	if (aoData[2].value.length > 0 || aoData[5].value.length > 0 || aoData[6].value.length > 0) {
    		changeSubscrition = true;
    	}
    	$.ajax({
    		"cache": false,
    		"dataType": 'json',
    		"type": "POST",
    		"url": sSource,
    		"data": aoData,
    		"beforeSend": function () {
    			if (changeSubscrition) {
    				legenda = $("div.navegacao div.legenda").html();
    				divPosicao = $('#divPosicao').html();
    				btnAnterior = $(".btnAnterior").css('display');
    				btnProxima = $(".btnProxima").css('display');
    				$("div.navegacao div.legenda").html('');
    				$('#divPosicao').html('');
    				$(".btnAnterior").hide();
    				$(".btnProxima").hide();
    				$("#divRequestRightsSubscriptionHeader").html('');
    			}
    			else {
    				$(".btnAnterior").unbind();
    				$(".btnProxima").unbind();
    				LoadSubscriptionHeader();
    			}
    			$("#tableArea").hide();
    			$("#loadingArea").show();
    			$("#emptyArea").hide();
    		},
    		"success": function (data) {
    			if (data.FlagChangeFilterSubscription) {
    				$("#hPosicao").val(0);
    				$("#hTotalSubscricoes").val(data.TotalSubscription);
    				if (data.TotalSubscription == 0) {
    					$("div.navegacao div.legenda").html('');
    					$('#divPosicao').html('');
    					$(".btnAnterior").hide();
    					$(".btnProxima").hide();
    					$("#divRequestRightsSubscriptionHeader").html('');
    				}
    				else if (data.TotalSubscription == 1) {
    					$("div.navegacao div.legenda").html('1 subscrição encontrada');
    					$(".btnAnterior").hide();
    					$(".btnProxima").hide();
    					LoadSubscriptionHeader();
    				}
    				else {
    					$("div.navegacao div.legenda").html(data.TotalSubscription + ' subscrições encontradas');
    					$('#divPosicao').html('Exibindo 1 de ' + data.TotalSubscription);
    					$(".btnAnterior").hide();
    					$(".btnProxima").show();
    					if (!changeSubscrition) {
    						LoadSubscriptionNavigation();
    					}
    					LoadSubscriptionHeader();
    				}
    			}
    			else {
    				var hPosicao = parseInt($("#hPosicao").val());
    				var hTotalSubscricoes = parseInt($("#hTotalSubscricoes").val());
    				if (changeSubscrition) {
    					$("div.navegacao div.legenda").html(legenda);
    					$('#divPosicao').html(divPosicao);
    					$(".btnAnterior").css('display', btnAnterior);
    					$(".btnProxima").css('display', btnProxima);
    				}
    				else {
    					LoadSubscriptionNavigation();
    				}
    				LoadSubscriptionHeader();
    			}

    			fnCallback(data);
    			if (data.iTotalRecords == 0) {
    				$("#tableArea").hide();
    				$('#emptyArea').html('Sua pesquisa não encontrou resultados válidos. Tente novamente.').show();
    			}
    			else {
    				$('#emptyArea').html('').hide();
    				RequestRightsMethods.RequestRightsRequest();
    			}
                RequestRightsMethods.enableExportButtons();

                if (that.fnSettings()._iDisplayLength >= data.iTotalRecords) {
                    $("#requestRightsList_paginate").hide();
                }
    		}
    	});
    }
    , fnDrawCallback: function () {
    	fnDrawCallback(this);
    }
    , "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
    	for (var i = 4; i <= 8; i++) {
    		if (!isNaN(parseFloat(aData[i]))) {
    			var number = parseFloat(aData[i].replace(/\./g, "").replace(/\,/g, "."));

    			if (number > 0) {
    				$(nRow).find("td:eq(" + i.toString() + ")").addClass("positivo");
    			}
    			else if (number < 0) {
    				$(nRow).find("td:eq(" + i.toString() + ")").addClass("negativo");
    			}
    		}
    	}
    	return nRow;
    }
    , aoColumns: [
        { "bSortable": false, "bSearchable": false },
        { "bSortable": false, "sType": "string" },
        { "bSortable": false, "sType": "numeric" },
        { "bSortable": false, "sType": "string" },
        { "bSortable": false, "sType": "numeric", "sClass": "direita" },
        { "bSortable": false, "sType": "numeric", "sClass": "direita" },
        { "bSortable": false, "sType": "numeric", "sClass": "direita" },
        { "bSortable": false, "sType": "numeric-comma", "sClass": "direita" },
        { "bSortable": false, "sType": "numeric-comma", "sClass": "direita" },
        { "bSortable": false, "sType": "date" },
        { "bSortable": false, "sType": "string" },
        { "bSortable": false, "sType": "string" },
        { "bSortable": false, "sType": "string" },
        { "bSortable": false, "bSearchable": false }
    ]
};



var objRequestRightsInitial = {
    "bAutoWidth": false
    , "bPaginate": true
    , "bDestroy": true
    , "sPaginationType": "full_numbers"
    , "sDom": 'trip'
    , "iDisplayLength": 50
    , "oLanguage": {
        "sInfo": ""
    }
    , "bProcessing": true
    , "bServerSide": true
	, "bDeferRender": true
    , "sAjaxSource": "../Subscription/InitialRequestRightsFilter"
    , "fnServerData": function (sSource, aoData, fnCallback) {
        var that = this;
        var status = commonHelper.GetParameterByName("status");
        if (aoData != null && aoData != undefined && status != undefined && status != null && status != "") {
            aoData.push({ "name": "ddlStatus", "value": status });

            $("#ddlStatus").val(status);
        }

        $.ajax({
            "cache": false,
            "dataType": 'json',
            "type": "POST",
            "url": sSource,
            "data": aoData,
            "beforeSend": function () {
                $("#tableArea").hide();
                $("#loadingArea").show();
                $("#emptyArea").hide();
            },
            "success": function (data) {
                fnCallback(data);
                if (data.iTotalRecords == 0) {
                    $("#tableArea").hide();
                    $('#emptyArea').html('Sua pesquisa não encontrou resultados válidos. Tente novamente.').show();
                }
                else {
                    $('#emptyArea').html('').hide();
                    RequestRightsMethods.RequestRightsRequest();
                }
                RequestRightsMethods.enableExportButtons();

                if (that.fnSettings()._iDisplayLength >= data.iTotalRecords) {
                    $("#requestRightsList_paginate").hide();
                }
            }
        });
    }
    , fnDrawCallback: function () {
        fnDrawCallback(this);

    }
    , "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        for (var i = 4; i <= 8; i++) {
            if (!isNaN(parseFloat(aData[i]))) {
                var number = parseFloat(aData[i].replace(/\./g, "").replace(/\,/g, "."));

                if (number > 0) {
                    $(nRow).find("td:eq(" + i.toString() + ")").addClass("positivo");
                }
                else if (number < 0) {
                    $(nRow).find("td:eq(" + i.toString() + ")").addClass("negativo");
                }
            }
        }
        return nRow;
    }
    , aoColumns: [
        { "bSortable": false, "bSearchable": false },
        { "bSortable": false, "sType": "string" },
        { "bSortable": false, "sType": "numeric" },
        { "bSortable": false, "sType": "string" },
        { "bSortable": false, "sType": "numeric", "sClass": "direita" },
        { "bSortable": false, "sType": "numeric", "sClass": "direita" },
        { "bSortable": false, "sType": "numeric", "sClass": "direita" },
        { "bSortable": false, "sType": "numeric-comma", "sClass": "direita" },
        { "bSortable": false, "sType": "numeric-comma", "sClass": "direita" },
        { "bSortable": false, "sType": "date" },
        { "bSortable": false, "sType": "string" },
        { "bSortable": false, "sType": "string" },
        { "bSortable": false, "sType": "string" },
        { "bSortable": false, "bSearchable": false }
    ]
};

var RequestRightsMethods = {

    RequestRightsRequest: function () {
        $("#lnkRequest").unbind("click").bind("click", function () {
            $("#feedbackError").hide().html('');
            var counter = 0;

            var hasAlreadyOpened = false;
            //var subId = $("input[type='checkbox']:checked").eq(0).attr('id').replace('chkRequestRights-', '');

            var subId = $("#hdfCurrentSubscription").val();

            var subStatus = $("#hdfCurrentStatus").val();

            if (subStatus != 'S') {
                $("#feedbackError").show().html('A subscrição ainda não entrou no período de subscrição ou já está encerrada.');
            }
            else if (subId != undefined || subId != null) {
                if ($("input[type='checkbox'][name!='chkAll']:checked").length > 0) {
                    var clientIds = "";
                    $("input[type='checkbox'][name!='chkAll']:checked").each(function () {
                        if (counter > 100)
                            return false;

                        var clientID = $(this).parent().parent().find('input.lnk-client').attr('value');

                        var status = $(this).parent().parent().find('input.lnk-status').attr('value');

                        if (status == 'A') {
                            if (clientIds.indexOf(clientID) < 0) {
                                clientIds += (clientIds == "") ? clientID : "," + clientID;
                                counter++;
                            }
                        }
                        else if (!hasAlreadyOpened) {

                            hasAlreadyOpened = true;
                        }
                    });

                    if (counter > 100) {
                        $("#feedbackError").show().html('Por favor selecione um máximo de 100 clientes.');
                    } 
                    else {

                        if (hasAlreadyOpened) {
                            $("#feedbackError").show().html('Alguns clientes já possuem solicitações de direito. Desmarque-os antes de continuar.');
                        }
                        else {
                            if (clientIds != "")
                                commonHelper.OpenModal("/Subscription/RightsRequest/?IsNew=true&requestId=" + subId + "&clientIds=" + clientIds, 890, ".content", "");
                            else
                                commonHelper.OpenModal("/Subscription/RightsRequest/?requestId=" + subId, 890, ".content", "");
                        } 
                    }
                }
                else {
                    commonHelper.OpenModal("/Subscription/RightsRequest/?requestId=" + subId, 890, ".content", "");
                }
            }
            else {
                $("#feedbackError").show().html('Nenhuma subscrição selecionada');
            }

            $("#MsgBox").center();
        });
    }
    , RequestRightsEdit: function () {
        $(".ico-editar").unbind("click").bind("click", function () {
            //var id = $(this).parent().parent().find("input[type='checkbox']").attr('value');
            var id = $(this).parent().parent().find("input[type='checkbox']").attr('id').replace('chkRequestRights-', '');
            var clientID = $(this).parent().parent().find('input.lnk-client').attr('value');
            var status = $(this).parent().parent().find('input.lnk-status').attr('value');
            commonHelper.OpenModal("/Subscription/RightsRequest/?requestId=" + id + "&clientIds=" + clientID + "&status=" + status, 890, ".content", "");
        });
    }
    , RequestRightsDetail: function () {
        $(".ico-details").unbind("click").bind("click", function () {
            var id = $(this).parent().parent().find("input[type='checkbox']").attr('id').replace('chkRequestRights-', '');
            commonHelper.OpenModal("/Subscription/RequestRightsDetail/" + id, 890, ".content", "");
        });
    }
    , RequestRightsDelete: function () {
        $(".ico-excluir").unbind("click").bind("click", function () {
            var id = $(this).parent().parent().find("input[type='checkbox']").attr('id').replace('chkRequestRights-', '');
            var clientID = $(this).parent().parent().find('input.lnk-client').attr('value');
            var status = $(this).parent().parent().find('input.lnk-status').attr('value');
            commonHelper.OpenModal("/Subscription/RightRequestedConfirmDelete/?requestId=" + id + "&clientId=" + clientID + "&status=" + status, 890, ".content", "");
        });
    }
    , RequestRightsHistoryDetail: function () {
        $(".historico-detalhe-subscricao").unbind("click").bind("click", function () {
            var id = $(this).parent().parent().find("input[type='checkbox']").attr('id').replace('chkRequestRights-', '');
            var url = "/Subscription/RequestRightsHistoryDetail/?requestId=" + id;
            commonHelper.OpenModal(url, 671, ".content", "");
        });
    }
    , enableExportButtons: function () {
        var oTable = $('#requestRightsList').dataTable();
        var aaSorting = oTable.fnSettings().aaSorting[0];

        var data = $("#requestRightsList tbody tr");
        var emptyData = (data.length == 1 && data[0].lastChild.className == "dataTables_empty") ? true : false;
        var excelLink = $("#lnkRequestRightsExcel");
        var pdfLink = $("#lnkRequestRightsPdf");
        excelLink.attr("href", (emptyData) ? "#" : "/Subscription/RequestRightsToFile/?isPdf=false" + "&col=" + aaSorting[0] + "&dir=" + aaSorting[1] + "&pos=" + $("#hPosicao").val());
        pdfLink.attr("href", (emptyData) ? "#" : "/Subscription/RequestRightsToFile/?isPdf=true" + "&col=" + aaSorting[0] + "&dir=" + aaSorting[1] + "&pos=" + $("#hPosicao").val());
    }
    , RequestRightsPendents: function () {
        $.post("/Home/GetPending", { module: "Subscription.RequestRights" }, function (data) {
            if (data > 0) {

                var pendingContent = "<span><strong>" + data + "</strong> Pendente" + (data > 1 ? "s" : "") + "</span>";

                if ($("#pendingTag").length > 0) {
                    $("#pendingTag").html(pendingContent);
                }
                else {
                    var pending = "<div id='pendingTag' class='aviso' style='display: none;'>" + pendingContent + "</div>";
                    if ($('#trilha').children('ul').find('li').last().length > 0)
                        $('#trilha').children('ul').find('li').last().append(pending);
                    else
                        $('#trilha').children('ul').append(pending);
                }
                $("#pendingTag").show();
            }
            else {
                $("#pendingTag").hide();
            }
        }, "json");
    }
};

$(function () {
    var context = $('#requestRightsFilterContent');

    $("#chkAll").click(function () {
        var check = $(this).is(':checked');
        $("table#requestRightsList input:checkbox:not(:disabled)").each(function () { $(this).attr('checked', check); })
    });

    $('.closeLink').bind({
        "click": function () {
            $('.modal-situacao').hide();
            return false;
        }
    });

    RequestRightsMethods.RequestRightsPendents();

    if (oTableRequestRights != null) {
        oTableRequestRights.fnDestroy();
        oTableRequestRights = null;
    }

    var hPosicao = parseInt($("#hPosicao").val());
    var hTotalSubscricoes = parseInt($("#hTotalSubscricoes").val());

    if (hTotalSubscricoes == 0) {
        $("div.navegacao div.legenda").html('');
        $(".btnAnterior").hide();
        $(".btnProxima").hide();
        $('#divPosicao').html('');
        $("#tableArea").hide();
        $("#loadingArea").hide();
        //$("#emptyArea").hide();
        $('#emptyArea').html('Sua pesquisa não encontrou resultados válidos. Tente novamente.').show();
        $("#divPosicao").html('');
    }
    else if (hTotalSubscricoes == 1) {
        $("div.navegacao div.legenda").html('1 subscrição encontrada');
        $(".btnAnterior").hide();
        $(".btnProxima").hide();
        $('#divPosicao').html('');
        LoadSubscriptionHeader();
        oTableRequestRights = $("#requestRightsList").dataTable(objRequestRightsInitial);
    }
    else {
        $("div.navegacao div.legenda").html(hTotalSubscricoes + ' subscrições encontradas');
        $(".btnAnterior").hide();
        $('#divPosicao').html('Exibindo 1 de ' + hTotalSubscricoes);
        LoadSubscriptionNavigation();
        LoadSubscriptionHeader();
        oTableRequestRights = $("#requestRightsList").dataTable(objRequestRightsInitial);
    }

});

function OpenRequest(id) {
    commonHelper.OpenModal("/Subscription/RequestRightsEdit/" + id, 890, ".content", "");
    $("#MsgBox").center();
}

function ViewHistory() {
    commonHelper.OpenModal("/Subscription/RequestRightsHistory", 671, ".content", "");
}

function OpenHistoryDetail(id, clientCode) {
    var url = "/Subscription/RequestRightsHistoryDetail/?requestId=" + id + "&clientCode=" + clientCode;
    commonHelper.OpenModal(url, 671, ".content", "");
    $("#MsgBox").center();
}

function DeleteRequest(id, clientCode) {
    var url = "/Subscription/RequestRightsConfirmDelete/?requestId=" + id + "&clientCode=" + clientCode;
    commonHelper.OpenModal(url, 450, ".content", "");
    $("#MsgBox").center();
}

function DateValidate(date) {
    var reDate = /^((0[1-9]|[12]\d)\/(0[1-9]|1[0-2])|30\/(0[13-9]|1[0-2])|31\/(0[13578]|1[02]))\/\d{4}$/;
    if (reDate.test(date) || (date == '')) {
        return true
    } else {
        return false;
    }
}

function ChangeStatus(submit, self, requestId, clientCode) {

    var modal = $("#divModalStatus"),
        hdfRequestId = $("#hdfRequestId", modal),
        hdfClientCode = $("#hdfClientCode", modal),
        ddlStatusChange = $("#ddlStatusChange", modal);

    if (submit) {

        if (ddlStatusChange.val() == "")
            return false;

        $.ajax({
            url: "/Subscription/ChangeRequestRightsStatus",
            type: "POST",
            dataType: "json",
            cache: false,
            data: { requestId: hdfRequestId.val(), clientCode: hdfClientCode.val(), status: ddlStatusChange.val() },
            success: function (data) {
                if (data.Error == "") {
                    modal.hide();
                    $("#btnSearch").click();
                    //oTableRequestRights.fnDraw(false);
                    $("div#divSubscriptionFeedback span").html("Atualização feita com sucesso.");
                    $("div#divSubscriptionFeedback").show();
                }
                else {
                    $("#feedbackRequestError", modal).html(data.Error);
                    $("#feedbackRequestError", modal).show();
                }
            }
        });
            
    }
    else {
        var offset = $(self).offset();

        modal.show().css("top", offset.top);
        $("#ddlStatusChange", modal).val("");
        ddlStatusChange.styleInputs();

        hdfRequestId.val(requestId);
        hdfClientCode.val(clientCode);
    }
}

function OpenEffect() {

    var checkedRights = $("[id^='chkRequestRights']:checked");
    if (checkedRights.length > 0) {
        var ids = "";
        $(checkedRights).each(function () {
            if ($(this).val() == "P") {
                ids += $(this).parent().parent().find('input.lnk-client').attr('value') + ";";
            }
            else {
                $(this).attr('checked', false);
            }
        });

        if (ids != "") {
            $("#feedbackError").html("").css({ 'display': 'none' });
            var url = "/Subscription/RequestRightsEffect/?requestsIds=" + ids;
            commonHelper.OpenModal(url, 890, ".content", "");
            $("#MsgBox").center();
        } else {
            $("#feedbackError").html("Nenhuma solicitação para efetuar selecionada.").css({ 'display': 'block' });
            $("#requestRightsList #chkAll").attr('checked', false);
        }
    }
    else {
        $("#feedbackError").html("Nenhuma solicitação para efetuar selecionada.").css({ 'display': 'block' });
    }

}


function LoadSubscriptionHeader() {
    $("#lnkRequest").unbind("click");
	$.ajax({
		url: "/Subscription/RequestRightsSubscriptionHeader",
		type: "POST",
		dataType: "html",
		data: { "Posicao" : $("#hPosicao").val() },
		success: function (data) {
			if (data.length > 0)
			{
				$("#divRequestRightsSubscriptionHeader").html(data);
			}
		}
	});
}

function ChangeNotification(submit, self, requestId, clientCode) {
    var context = $("#frmRequestRightsNotificationChange");

    var modal = $("#divModalNotification"),
        hdfRequestId = $("#hdfSubscriptionId", modal),
        hdfClientCode = $("#hdfClientCode", modal),
        ddlNotificationChange = $("#ddlNotificationChange", modal);
    
    //$("#ddlNotificationChange option[value='']", modal).addClass('not');

    if (submit) {

        if (ddlNotificationChange.val() == "")
            return false;

        $.ajax({
            url: "/Subscription/ChangeNotification",
            type: "POST",
            dataType: "json",
            data: { requestId: hdfRequestId.val(), status: ddlNotificationChange.val(), clientCode: hdfClientCode.val() },
            success: function (data) {
                if (data.Error == "") {
                    modal.hide();
                    ddlNotificationChange.val("");

                    //oTableRequestRights.fnDraw(false);
                    $("#btnSearch").click();

                    $("div#divSubscriptionFeedback span").html("Atualização feita com sucesso.");
                    $("div#divSubscriptionFeedback").show();
                }
                else {
                    $("#feedbackRequestError", modal).html(data.Error);
                    $("#feedbackRequestError", modal).show();
                }
            }
        });

    }
    else {
        var offset = $(self).offset();

        modal.show().css("top", offset.top);
        ddlNotificationChange.styleInputs();

        hdfRequestId.val(requestId);
        hdfClientCode.val(clientCode);
    }
}

function OpenNotify() {

    var checkedRights = $("[id^='chkRequestRights']:checked");
    if (checkedRights.length > 0) {
        var ids = "";
        $(checkedRights).each(function () {
                ids += $(this).parent().parent().find('input.lnk-client').attr('value') + ";";
        });

        if (ids != "") {
            $("#feedbackError").html("").css({ 'display': 'none' });
            var url = "/Subscription/RequestRightsNotify/?requestsIds=" + ids;
            commonHelper.OpenModal(url, 890, ".content", "");
            $("#MsgBox").center();
        } else {
            $("#feedbackError").html("Nenhuma solicitação selecionada para notificar.").css({ 'display': 'block' });
            $("#requestRightsList #chkAll").attr('checked', false);
        }
    }
    else {
        $("#feedbackError").html("Nenhuma solicitação selecionada para notificar.").css({ 'display': 'block' });
    }

}

function LoadSubscriptionNavigation() {
	$(".btnAnterior").click(function () {
		var hPosicao = parseInt($("#hPosicao").val());
		var hTotalSubscricoes = parseInt($("#hTotalSubscricoes").val());
		var novaPosicao = 0;

		if (hPosicao - 1 == 0) {
			$("#hPosicao").val(0);
			$(".btnAnterior").hide();
			$(".btnProxima").show();
		}
		else {
			novaPosicao = hPosicao - 1;
			$(".btnAnterior").show();
			$(".btnProxima").show();
		}

		$('#divPosicao').html('Exibindo ' + (novaPosicao + 1) + ' de ' + hTotalSubscricoes);
		$("#hPosicao").val(novaPosicao);
		$("#requestRightsList > tbody").html('');
		$("#btnSearch").click();
	});

	$(".btnProxima").click(function () {
		var hPosicao = parseInt($("#hPosicao").val());
		var hTotalSubscricoes = parseInt($("#hTotalSubscricoes").val());
		var novaPosicao = hPosicao + 1;

		if (novaPosicao == hTotalSubscricoes - 1)
		{
			$(".btnProxima").hide();
		}
		else {
			$(".btnProxima").show();
		}
		$(".btnAnterior").show();
		$('#divPosicao').html('Exibindo ' + (novaPosicao + 1) + ' de ' + hTotalSubscricoes);
		$("#hPosicao").val(novaPosicao);
		$("#requestRightsList > tbody").html('');
		$("#btnSearch").click();
	});
}