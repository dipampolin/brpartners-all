﻿/// <reference path="../../References.js" />

var oTable;
var emptyDataMessage = "Sua pesquisa não encontrou resultados válidos. Tente novamente.";

$(function () {
	handlers();
});

function handlers() {
    //$('#frmTradeBlotter').bind("keypress", keypressHandler);

    $("#txtTradingDate").bind('keydown', function (event) {
        if (event.which == 13) {
            commonHelper.ShowLoadingArea();
            var tradingDate = $("#txtTradingDate", $("#filterArea")).val();
            searchRequest(tradingDate);
            assemblyUrlImport(tradingDate);
            $(".lblTradingDate", $("#tableArea")).html(formatUSDate(tradingDate));
            return false;

        }

    });

    $("#btnSearch", $("#filterArea")).unbind("click");
    $("#btnSearch", $("#filterArea")).bind("click", function () {
        commonHelper.ShowLoadingArea();
        var tradingDate = $("#txtTradingDate", $("#filterArea")).val();
        searchRequest(tradingDate);
        assemblyUrlImport(tradingDate);
        $(".lblTradingDate", $("#tableArea")).html(formatUSDate(tradingDate));

        return false;
    });

	$("#txtTradingDate", $("#filterArea")).setMask("99/99/9999");

	$("#txtTradingDate").datepicker();
	$.datepicker.setDefaults($.datepicker.regional['pt-BR']);
	$("#txtTradingDate").datepicker($.datepicker.regional['pt-BR']);

	$("#txtTradingDate", $("#filterArea")).blur(function () {
	    dateHelper.CheckDate($(this), "filterArea");
	});
}

function formatUSDate(date) {
	var items = date.split('/');

	if (items.length == 3)
		return items[1] + "/" + items[0] + "/" + items[2];
	else
		return date;
}

function searchRequest(tradingDate) {
    $.ajax({
        async: false,
        url: "../NonResidentTraders/TradeBlotter",
        type: "POST",
        data: { "txtTradingDate": $("#txtTradingDate", $("#filterArea")).val() },
        dataType: "json",
        success: function (data) {

            emptyDataMessage = tradingDate != "" ? "Sua pesquisa para " + tradingDate + " não encontrou resultados válidos. Tente novamente." : "Sua pesquisa não encontrou resultados válidos. Tente novamente.";

            if (data.Status == "True") {
                $("#vlnTradingDate", $("#filterArea")).html("");
                var tableId = "clientTable";
                obj = {
                    "bSort": true,
                    "bAutoWidth": false,
                    "bInfo": true,
                    "aoColumns": [
						{ "bSortable": false },
						{ "bSortable": false },
						{ "bSortable": false },
						{ "bSortable": false },
						{ "bSortable": false },
						{ "sClass": "numero", "bSortable": false },
						{ "sClass": "numero", "bSortable": false },
						{ "sClass": "numero", "bSortable": false },
						{ "sClass": "numero", "bSortable": false },
						{ "sClass": "numero", "bSortable": false },
						{ "sClass": "numero", "bSortable": false },
						{ "bSortable": false },
						{ "bSortable": false }
					],
                    "oLanguage": {
                        "sProcessing": "Carregando...",
                        "sLengthMenu": "<span>Exibir:</span> _MENU_ <span>por página</span>",
                        "sZeroRecords": emptyDataMessage,
                        "oPaginate": {
                            "sPrevious": "Anteriores",
                            "sNext": "Próximos"
                        }
                    },
                    "sPaginationType": "full_numbers",
                    "sDom": 'rtipl',
                    "fnDrawCallback": function () {
                        var totalRecords = this.fnSettings().fnRecordsDisplay();

                        if (totalRecords == 0) {
                            $("#emptyArea").html(emptyDataMessage);
                            $("#emptyArea").show();
                            $("#tableArea").hide();
                        }
                        else {
                            $("#tableArea").show();
                            $("#emptyArea").hide();

                            var totalPerPage = this.fnSettings()._iDisplayLength;
                            var totalPages = Math.ceil(totalRecords / totalPerPage);

                            var currentIndex = parseInt(this.fnSettings()._iDisplayStart);
                            var currentPage = totalPages > 0 ? Math.ceil(1 + (currentIndex / totalPerPage)) : 0;

                            $("#" + tableId + "_info").html("Exibindo " + currentPage + " de " + totalPages);

                            if (totalRecords > 0 && totalPages > 1) {
                                $("#" + tableId + "_info").css("display", "");
                                $("#" + tableId + "_length").css("display", "");
                                $("#" + tableId + "_paginate").css("display", "");
                            }
                            else {
                                $("#" + tableId + "_info").css("display", "none");
                                $("#" + tableId + "_length").css("display", "none");
                                $("#" + tableId + "_paginate").css("display", "none");
                            }
                        }
                        // Adiciona a classe 'ultimo' à última tr no corpo da tabela
                        $('table tbody tr:last-child').addClass('ultimo');

                        // Adiciona a classe 'ultimo' às últimas th e td no corpo da tabela
                        $('table tr td:last-child').addClass('ultimo');
                        $('table tr th:last-child').addClass('ultimo');
                        $("#loadingArea").hide();
                    },
                    "bProcessing": false,
                    "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                        $("#" + tableId + "_first").css("visibility", "hidden");
                        $("#" + tableId + "_last").css("visibility", "hidden");
                    },
                    "bRetrieve": true,
                    "bServerSide": true,
                    "sAjaxSource": "../DataTables/NonResidentTraders_TradeBlotter",
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        $.ajax({
                            cache: false,
                            dataType: 'json',
                            type: "POST",
                            url: sSource,
                            data: aoData,
                            success: fnCallback
                        });
                    }
                };
                oTable = $("#clientTable").dataTable(obj);
                oTable.fnClearTable();
                oTable.fnDraw();
                $("#clientTable_length").hide();

//                if (oTable != null && oTable.length > 0 && oTable[0].rows.length > 3)
//                    $("#tableArea").show();
//            }
//            else {
//                for (var i = 0; i < data.Messages.length - 1; i++) {
//                    $("#" + data.Messages[i], $("#filterArea")).html(data.Messages[i + 1]);
//                }
            }
        }
    });
}

function assemblyUrlImport(tradingDate) {
	var url_PDF = "../NonResidentTraders/TradeBlotterToPDF?date=" + tradingDate;
	var url_Excel = "../NonResidentTraders/TradeBlotterToExcel?date=" + tradingDate;
	var url_Print = "../NonResidentTraders/PrintTradeBlotterReport?date=" + tradingDate;

	$("#tradeBlotterToExcel", $("#tableArea")).attr("href", url_Excel);
	$("#tradeBlotterToPDF", $("#tableArea")).attr("href", url_PDF);
	$("#tradeBlotterToPrint", $("#tableArea")).attr("href", url_Print);
}

function keypressHandler(e) {
	if (e.which == 13) {
		$(this).blur();
		//$('#btnSearch').focus().click(); //give your submit an ID
		commonHelper.ShowLoadingArea();
		var tradingDate = $("#txtTradingDate", $("#filterArea")).val();
		searchRequest(tradingDate);
		assemblyUrlImport(tradingDate);
		$(".lblTradingDate", $("#tableArea")).html(formatUSDate(tradingDate));
		return false;

    }
    
}

