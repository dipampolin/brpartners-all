﻿/// <reference path="../../References.js" />

$(function () {
    handlers();
});

function handlers() {
    var context = $("#filterArea");

    //	$("#txtClientCode").blur(function () {
    //		commomDataHelper.GetClientName($(this).val(), "/NonResidentTraders/GetTraderName/", "lblTraderName", $("#filterArea"));
    //	});

    $(context).bind('keydown', function (event) {
    	if (event.which == 13) {
    		$("#btnSearch").click();
    		return false;
    	}
    });

    $("#txtTradingDate", context).blur(function () {
        $("#hdfTradingDate", context).val($(this).val());
    });

    $("#btnSearch", context).click(function () {
        var feedback = $("#feedbackError");
        $("#reportArea").css({ "display": "none" });
        feedback.hide();
        var date = $("#txtTradingDate", context).val();
        commonHelper.ShowLoadingArea();
        if (date != "") {
            var type = $("#reportType:checked", context).val();
            $("#reportArea").html("");
            $("#reportArea").show();


            setTimeout(function () {
                commonHelper.LoadView("../NonResidentTraders/FailLedgerReport?date=" + date + "&type=" + type, "#reportArea", null, function () {
                        $("#loadingArea").hide();
                    }); 
                }, 400);
        }
        else {
            feedback.html("<label class='error'>Preencha a data.</label>");
            feedback.show();
        }
    });


    $("#txtTradingDate", context).blur(function () {
        dateHelper.CheckDate($(this), "filterArea");
    });

    $("#txtTradingDate").setMask("99/99/9999");
    //$("#txtClientCode").setMask("999999999");

    $("#txtTradingDate").datepicker();
    $.datepicker.setDefaults($.datepicker.regional['pt-BR']);
    $("#txtTradingDate").datepicker($.datepicker.regional['pt-BR']);

    // Adiciona a classe 'ultimo' à última tr no corpo da tabela
    $('table tbody tr:last-child').addClass('ultimo');

    // Adiciona a classe 'ultimo' às últimas th e td no corpo da tabela
    $('table tr td:last-child').addClass('ultimo');
    $('table tr th:last-child').addClass('ultimo');

}

