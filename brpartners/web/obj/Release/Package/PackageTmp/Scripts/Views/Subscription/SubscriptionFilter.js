﻿/// <reference path="../../jQuery/jquery-1.4.3.js" />

var SubscriptionFilter = {
    ValidateFilterFields: function (context) {
        var messageError = "";

        var invalidDate = false;
        $("input[id*='Date']", context).each(function () {
            if ($(this).val() != "" && !invalidDate) {
                if (!DateValidate($(this).val())) {
                    invalidDate = true;
                    messageError += "<li>Data inválida.</li>";
                }
            }
        });

        if (!invalidDate) {
            if (!dateHelper.CheckPeriod("#txtInitialDate", "#txtBrokerDate", context))
                messageError += "<li>Periodo de data inválido.</li>";
        }

        if (messageError.length > 0)
            messageError = "<ul>" + messageError + "</ul>";

        return { Result: (messageError == ""), Message: messageError }
    }
};

$(function () {
    var filterContext = $("#subscriptionFilterContent");

    $("#subscriptionFilter").click(function () {
        $('#subscriptionFilterContent').slideToggle('fast');
        $('select').styleInputs();
        $(this).toggleClass('aberto');
    });

    $("#txtStockCode", filterContext).css("text-transform", "uppercase");
    $("#txtInitialDate", filterContext).setMask("99/99/9999").datepicker().datepicker($.datepicker.regional['pt-BR']);
    $("#txtBrokerDate", filterContext).setMask("99/99/9999").datepicker().datepicker($.datepicker.regional['pt-BR']);

    $("#btnSearch").unbind("click");
    $("#btnSearch").bind("click", function (event) {
        $("#filterFeedbackError", filterContext).html("").hide();
        $("#subscriptionContent").hide();
        $("#loadingArea").show();
        $("#tableArea").hide();
        $("#feedback").hide().find("span").html('');
        $("#feedbackError").hide().find("span").html('');

        var feedback = SubscriptionFilter.ValidateFilterFields(filterContext);
        if (feedback.Result) {
            $("#filterFeedbackError").html("").hide();
            $("#subscriptionContent").hide();
            $("#loadingArea").show();
            $("#tableArea").hide();
            $("#feedback").hide().find("span").html('');
            $("#feedbackError").hide().find("span").html('');

            if (filterContext.css("display") != "none") {
                $('#subscriptionFilterContent').slideToggle('fast');
                $('select').styleInputs();
                $(filterContext).toggleClass('aberto');
            }
            if (oTableSubscription != null)
                oTableSubscription.fnDestroy();
            oTableSubscription = $("#subscriptionList").dataTable(objSubscriptionFilter);
            SubscriptionMethods.enableExportButtons();
        }
        else {
            $("#filterFeedbackError").html(feedback.Message).show();
        }

    });

    $(filterContext).bind('keydown', function (event) {
        if (event.which == 13) {
            $("#btnSearch").click();
        }
    });

});


