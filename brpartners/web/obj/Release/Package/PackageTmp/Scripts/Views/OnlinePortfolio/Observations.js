﻿/// <reference path="../../References.js" />

$(function () {
    var context = $("#frmObservation");

    $("#btnIncluir", context).click(function () {
        try {
            var filterContext = "#PortfolioFilterContent";
            var feedback = ValidateFields(context);
            var clientCodeFromRequest = $("#ClientCodeFromPosition").val();
            var clientCode = (clientCodeFromRequest != "") ? clientCodeFromRequest : $("#hdfFilterClientCode", filterContext).val();

            if (feedback == "") {
                UpdateObservation();
            }
            else {
                $("#feedbackRequestError", context).html(feedback);
                $("#feedbackRequestError", context).show();
            }

        } catch (e) {
            $("#feedbackRequestError", context).html("Não foi possível incluir observação.");
            $("#feedbackRequestError", context).show();
        }

    });

    $("#txtObservation", context).blur(function () {
        try {
            if (this.value.length > 200)
                $("#txtObservation", context).val(this.value.substring(0, 200));
        } catch (e) {
        }

    });

});

function UpdateObservation() {
    $.post("/OnlinePortfolio/UpdateObservations", { ClientId: $("#ClientId").val(), txtObservation: $("#txtObservation").val() }, function (data) {

        if (data.inserted) {
            $("#commentsContent", "#pnlComments").html(data.txtObservation);
            $("span", "#feedback").html(data.successMessage);
            $("#feedback").show();
            Modal.Close();
            SetCommentsUpdate();
        }
        else {
            $("#FeedbackError").show();
            Modal.Close();
        }
    }, "json");
}

function ValidateFields(context) {
    var feedback = "";

    if ($("#txtObservation", context).val() == "") {
        feedback += "<li>Observação em branco.</li>";
    }
    if ($("#txtObservation").val().length > 200) {
        feedback += "<li>Observação não pode exceder 4000 caracteres.</li>";
    }

    if (feedback.length > 0)
        feedback = "<ul>" + feedback + "</ul>";
    return feedback;
}
