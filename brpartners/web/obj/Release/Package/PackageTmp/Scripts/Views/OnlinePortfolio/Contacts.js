﻿/// <reference path="../../References.js" />

var oTable;
var previousIndex = -1;
var nextIndex = 1;
var feedbackCount = 1;

$(function () {
    $(document).ready(function () {

        var lnkPrevious = $("#lnkPreviousClient");
        var lnkNext = $("#lnkNextClient");
        var lnkPreviousTop = $("#lnkPreviousClientTop");
        var lnkNextTop = $("#lnkNextClientTop");

        var filterContext = "#PortfolioContactsFilterContent";

        var clientCodeFromPosition = $("#ClientCodeFromPosition").val();
        if (clientCodeFromPosition != "") {
            setTimeout(function () {
                $("#btnSearch", filterContext).click();
            }, 150);
        }

        $("#PortfolioContactsFilter").click(function () {
            $('#PortfolioContactsFilterContent').slideToggle('fast');
            $('select').styleInputs();
            $(this).toggleClass('aberto');
        });

        $('.closeLink').bind({
            "click": function () {
                $('.modal-detalhe-contato').hide();
                return false;
            }
        });

        $("#btnSearch", filterContext).click(function () {

            if (feedbackCount == 0)
                $("#feedbackError").hide();

            lnkPrevious.hide();
            lnkNext.hide();
            lnkPreviousTop.hide();
            lnkNextTop.hide();

            $("#emptyArea").hide();
            $("#tableArea").hide();
            $("#loadingArea").show();

            $("#filterFeedbackError", filterContext).hide();

            if ($(filterContext).css("display") != "none") {
                $('#PortfolioContactsFilterContent').slideToggle('fast');
                $('select').styleInputs();
                $(filterContext).toggleClass('aberto');
            }

            var clientCode = $("#hdfFilterClientCode", filterContext).val();

            var clientCodeFromRequest = $("#ClientCodeFromPosition").val();
            if (clientCodeFromRequest != "") {
                clientCode = clientCodeFromRequest;
                $("#hdfFilterClientCode", filterContext).val(clientCodeFromRequest);
                $("#txtClient", filterContext).val(clientCodeFromRequest);
            }

            $.ajax({

                cache: false,
                type: "POST",
                dataType: "json",
                async: true,
                url: "/OnlinePortfolio/LoadClientContacts",
                data: { ClientCode: clientCode, Index: $("#listIndex").val() },
                success: function (data) {

                    feedbackCount = 0;

                    if (clientCodeFromRequest != "") {
                        $("#ClientCodeFromPosition").val("");
                    }

                    $("#loadingArea").hide();

                    var emptyData = (data == null || data.ClientName == "");

                    var hideFiles = data.HtmlItems == "";

                    var excelLink = $("#lnkContactsExcel");
                    var pdfLink = $("#lnkContactsPdf");
                    excelLink.attr("href", (hideFiles) ? "#" : "/OnlinePortfolio/ContactsToFile/?isPdf=false");
                    //excelLink.attr("target", (hideFiles) ? "_self" : "_blank");
                    pdfLink.attr("href", (hideFiles) ? "#" : "/OnlinePortfolio/ContactsToFile/?isPdf=true");
                    //pdfLink.attr("target", (hideFiles) ? "_self" : "_blank");

                    if (oTable != undefined)
                        oTable.fnClearTable();

                    if (emptyData) {
                        $("#hdfClientCode").val("0");
                        $("#emptyArea").html("Nenhum contato encontrado.");
                        $("#emptyArea").show();
                        $("#tableArea").hide();
                    }
                    else {

                        var context = "#tableArea";

                        if (data.HtmlItems == "") {
                            $("#pnlNoContact", context).show();
                            $("#ContactsList", context).hide();
                        }
                        else {
                            $("#pnlNoContact", context).hide();
                            $("#ContactsList", context).show();
                            $("#ContactsList", context).find("tbody").html(data.HtmlItems);
                        }

                        $("#lblClientName", context).html(data.ClientName);
                        $("#hdfClientCode", context).val(data.ClientCode);
                        $("#lblClientPhone", context).html(data.ClientPhone);
                        $("#lblClientEmail", context).html(data.ClientEmail);
                        $("#lnkClientEmail", context).attr("href", "mailto:" + data.ClientEmail);
                        $("#lblClientCounterTop", context).html(data.Length < 2 ? "" : data.Legend);
                        $("#lblClientCounter", context).html(data.Length < 2 ? "" : data.Legend);

                        previousIndex = data.Index - 1;
                        nextIndex = data.Index + 1;

                        var length = data.Length;

                        if (length < 2) {
                            lnkPrevious.hide();
                            lnkNext.hide();
                            lnkPreviousTop.hide();
                            lnkNextTop.hide();
                        }
                        else {
                            if (data.Index == (length - 1)) {
                                lnkNext.hide();
                                lnkPrevious.show();
                                lnkNextTop.hide();
                                lnkPreviousTop.show();
                            }
                            else {
                                if (data.Index != 0) {
                                    lnkPrevious.show();
                                    lnkPreviousTop.show();
                                }
                                else {
                                    lnkPrevious.hide();
                                    lnkPreviousTop.hide();
                                }
                                lnkNext.show();
                                lnkNextTop.show();
                            }
                        }

                        SetDataTable();
                        $("#tableArea").show();
                        $("#emptyArea").hide();
                        $('select').styleInputs();
                    }
                }
            });

            return false;
        });

        $("#txtClient", filterContext).autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/OnlinePortfolio/ClientSuggest",
                    type: "POST",
                    dataType: "json",
                    data: { term: request.term },
                    success: function (data) {
                        response($.map(data, function (item) {
                            return {
                                label: item.Description,
                                value: item.Description,
                                id: item.Value
                            }
                        }));
                    }
                });
            },
            minLength: 3,
            select: function (event, ui) {
                $("#hdfFilterClientCode", filterContext).val(ui.item.id);
                $("#hdfFilterClientName", filterContext).val(ui.item.value);
            }
        });

        $("#txtClient", filterContext).blur(function () {
            var selectedClient = $("#hdfFilterClientName", filterContext);
            if (this.value != "") {
                if (this.value != selectedClient.val()) {
                    $("#hdfFilterClientCode", filterContext).val("");
                    $(selectedClient).val("");
                    $(this).val("");
                }
            }
            else {
                $("#hdfFilterClientCode", filterContext).val("");
                $(selectedClient).val("");
            }

        });

        $("#ddlPage", "#tableArea").change(function () {
            if (this.value != "") {
                var clientCode = $("#hdfClientCode", "#tableArea").val();
                window.location = clientCode == "0" ? "/OnlinePortfolio/" + this.value + "/" : "/OnlinePortfolio/" + this.value + "/?ClientCode=" + clientCode;
            }
            else
                return false;
        });

        var operatorClients = $("#OperatorClients").val();
        if (operatorClients == "") {
            $("#emptyArea").show();
            $("#tableArea").hide();
            $("#loadingArea").hide();
        }
        else {
            setTimeout(function () {
                $("#btnSearch", filterContext).click();
            }, 250);
        }
    });
});


function GetClientContacts(next) {
    var lnkPrevious = $("#lnkPreviousClient");
    var lnkNext = $("#lnkNextClient");
    var lnkPreviousTop = $("#lnkPreviousClientTop");
    var lnkNextTop = $("#lnkNextClientTop");

    lnkPrevious.hide();
    lnkNext.hide();
    lnkPreviousTop.hide();
    lnkNextTop.hide();

    $("#tableArea").hide();
    $("#loadingArea").show();

    var context = "#tableArea";

    $.ajax({
        cache: false,
        type: "POST",
        dataType: "json",
        async: true,
        url: "/OnlinePortfolio/LoadClientContacts",
        data: { Index: !next ? previousIndex : nextIndex },
        success: function (data) {
            if (oTable != undefined)
                oTable.fnClearTable();

            var hideFiles = data.HtmlItems == "";

            var excelLink = $("#lnkContactsExcel");
            var pdfLink = $("#lnkContactsPdf");
            excelLink.attr("href", (hideFiles) ? "#" : "/OnlinePortfolio/ContactsToFile/?isPdf=false");
            //excelLink.attr("target", (hideFiles) ? "_self" : "_blank");
            pdfLink.attr("href", (hideFiles) ? "#" : "/OnlinePortfolio/ContactsToFile/?isPdf=true");
            //pdfLink.attr("target", (hideFiles) ? "_self" : "_blank");

            if (hideFiles) {
                $("#pnlNoContact", context).show();
                $("#ContactsList", context).hide();
            }
            else {
                $("#pnlNoContact", context).hide();
                $("#ContactsList", context).show();
                $("#ContactsList", context).find("tbody").html(data.HtmlItems);
            }

            $("#lblClientName", context).html(data.ClientName);
            $("#hdfClientCode", context).val(data.ClientCode);
            $("#lblClientPhone", context).html(data.ClientPhone);
            $("#lblClientEmail", context).html(data.ClientEmail);
            $("#lnkClientEmail", context).attr("href", "mailto:" + data.ClientEmail);
            $("#lblClientCounterTop", context).html(data.Length < 2 ? "" : data.Legend);
            $("#lblClientCounter", context).html(data.Length < 2 ? "" : data.Legend);

            previousIndex = data.Index - 1;
            nextIndex = data.Index + 1;

            var length = data.Length;

            if (length < 2) {
                lnkPrevious.hide();
                lnkNext.hide();
                lnkPreviousTop.hide();
                lnkNextTop.hide();
            }
            else {
                if (data.Index == (length - 1)) {
                    lnkNext.hide();
                    lnkPrevious.show();
                    lnkNextTop.hide();
                    lnkPreviousTop.show();
                }
                else {
                    if (data.Index == 0) {
                        lnkPrevious.hide();
                        lnkPreviousTop.hide();
                    }
                    else {
                        lnkPrevious.show();
                        lnkPreviousTop.show();
                    }
                    lnkNext.show();
                    lnkNextTop.show();
                }
            }
            SetDataTable();
            $("#loadingArea").hide();
            $("#tableArea").show();
        }
    });
    return false;
}

function OpenRequest(id) {
    commonHelper.OpenModal("/OnlinePortfolio/ContactInsert/", 380, ".content", "");
}

function OpenPosition() {
    var clientCode = $("#hdfClientCode", "#tableArea").val();

    window.location = clientCode == "0" ? "/OnlinePortfolio/Index" : "/OnlinePortfolio/Index?ClientCode=" + clientCode;
}

function OpenHistory() {
    var clientCode = $("#hdfClientCode", "#tableArea").val();
    window.location = clientCode == "0" ? "/OnlinePortfolio/History/" : "/OnlinePortfolio/History/?ClientCode=" + clientCode;
}

function ValidateFilterFields(context) {
    var messageError = "";

    if (!validateHelper.ValidateList($("#txtClient", context).val()))
        messageError += "<li>Faixa de clientes inválida.</li>";

    if (messageError.length > 0)
        messageError = "<ul>" + messageError + "</ul>";

    return messageError;
}

function SetDataTable() {
    oTable = $('#ContactsList').dataTable(
		                        {
		                            "bDestroy": true,
		                            "bPaginate": false,
		                            "bLengthChange": false,
		                            "bFilter": false,
		                            "bInfo": false,
		                            "bAutoWidth": false,
		                            "aaSorting": [[0, "desc"]],
		                            "sPaginationType": "full_numbers",
		                            "aoColumns": [
                                                { "sSortDataType": "dom-text", "sType": "br_date" },
                                                null,
                                                null,
                                                { "bSortable": false }
                                    ]
		                        });
}

function OpenDetails(self, index) {

    $.ajax({
        cache: false,
        type: "POST",
        dataType: "json",
        async: true,
        url: "/OnlinePortfolio/LoadClientContactDetails",
        data: { Index: index },
        success: function (data) {
            var context = ".modal-detalhe-contato";
            $("#detalhe-data", context).html(data.Date);
            $("#detalhe-operador", context).html(data.Operator);
            $("#detalhe-mensagem", context).html(data.Message);
            $('.modal-detalhe-contato').show();
            var offset = $(self).offset();
            $('.modal-detalhe-contato').css("top", offset.top);
        }
    });
}

function OpenFullComment(self, index) {
    if (self.className.indexOf("aberto") >= 0) {
        $(self).attr("class", self.className.replace(/aberto/gi, 'fechado'));
        $("#halfComment" + index, "#ContactsList").show();
        $("#fullComment" + index, "#ContactsList").hide();
    }
    else {
        $(self).attr("class", self.className.replace(/fechado/gi, 'aberto'));
        $("#halfComment" + index, "#ContactsList").hide();
        $("#fullComment" + index, "#ContactsList").show();
    }
}