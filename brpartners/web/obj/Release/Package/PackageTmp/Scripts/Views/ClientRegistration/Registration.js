﻿/// <reference path="../../References.js" />

var fnCPFCNPJMask = function () {
    $('.txt-cpf-cnpj').unbind('keypress').bind('keypress', function (e) {
        var tecla = (window.Event) ? e.which : e.keyCode;
        if ((tecla == 0 || tecla == 8 || tecla == 9 || tecla == 13 || tecla == 33 || (tecla >= 45 && tecla <= 57)) && $(this).val().length <= 19) {
            return true;
        }
        else return false;
    });
}

var fnBankAutoComplete = function () {
    $(".txt-banco").autocomplete("destroy");
    $(".txt-banco").autocomplete({
        source: function (request, response) {
            $.ajax({
                cache: false,
                url: "../ClientRegistration/LoadBanks",
                type: "POST",
                dataType: "json",
                data: { filter: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        return {
                            label: item.BankName
                                    , value: item.BankName
                                    , code: item.BankCode
                        }
                    }));
                }
            });
        }
        , minLength: 3
        /*, select: function (event, ui) {
        $("#hdfDestinyBroker").val(ui.item.code);
        }*/
    });
};

var RegistrationAreas = {
    Reg_InfoPF: function () {
        $("#txtCPF, #txtSpouseCPF", Registration.context).setMask("cpf");

        $("#txtCPF").unbind("focus").bind("focus", function () {
            if ($(this).val() != "" && $(this) != undefined) {
                $("#hdfCPF").val($(this).val());
            }
        });

        $("#txtCPF", Registration.context).unbind("blur").bind("blur", function () {
            if ($(this).val() != "" && $(this).val() != null && $(this).val() != undefined) {
                $.ajax({
                    type: "POST",
                    data: { cpfcgc: $("#txtCPF").val() },
                    url: "../ClientRegistration/GetRegistrationClient",
                    cache: false,
                    success: function (data) {
                        if (data.Result == true) {
                            $(".aviso-cliente-existente").show();
                            $("#hdfClientId").val(data.Data);
                        }
                        else {
                            $(".aviso-cliente-existente").hide();
                            $("#hdfClientId").val(0);
                        }
                    }
                });
            }
        });

        $("#lnkLoadClient").unbind("click").bind("click", function () {
            var checked = ($(".reg-type").val() == "PF") ? "N" : "C";
            $.ajax({
                type: "POST",
                url: "../ClientRegistration/RegistrationFilter",
                data: { "clientCode": $("#hdfClientId").val(), "type": checked },
                beforeSend: function () {
                    $("#loadingArea").show();
                    $(".client-registration").hide();
                },
                success: function (data) {
                    if (data.Result == false) {
                        $("#FeedbackError").show().find("span").html(data.Message);
                    }
                    else {
                        window.location = data.Location;
                    }
                }
            });
        })

        $("#txtBirthDate, #txtEmissorDate", Registration.context).setMask("39/19/2999");

        $("#lnkPFPhase1", Registration.context).unbind("click").bind("click", function () {
            $("#frmPF_Phase1").submit();
        });
    },
    Reg_CivilState: function () {
        var fnSpouse = function () {
            if ($("#ddlCivilStateType").val() == "1" || $("#ddlCivilStateType").val() == "5") {
                $("#divSpouse").show();
            }
            else {
                $("#divSpouse").hide();
            }
        }

        fnSpouse();

        $("#ddlCivilStateType").change(function () {
            fnSpouse();
        });
    },
    Reg_Address: function () {
        var fnDeleteAddress = function () {
            $(".ico-excluir", Registration.context).unbind("click").bind("click", function () {
                var rel = $(this).attr("rel");
                $("#address-" + rel).remove();

                if ($(".caixa-endereco").length == 1) {
                    $(".lnk-delete-address").hide();
                }
            });
        }

        fnDeleteAddress();

        $("#txtResidentialPhone, #txtCellularPhone, #txtFaxPhone, #txtComercialPhone", Registration.context).setMask("(99) 9999-9999");

        $(".number5").setMask("number5");
        $(".number3").setMask("number3");

        $("#lnkAddAddress").unbind("click").bind("click", function () {

            $.ajax({
                type: "POST",
                url: "../ClientRegistration/AddNewAddress",
                cache: false,
                success: function (data) {
                    if ($(".caixa-endereco").length < 3) {
                        $(".caixa-endereco").last().after(
                            $(data).clone()
                        );
                        fnDeleteAddress();
                        $('select').styleInputs();

                        $(".lnk-delete-address").show();
                        $(".number5").setMask("number5");
                        $(".number3").setMask("number3");
                    }
                }
            });
        });

        if ($(".caixa-endereco").length == 1) {
            $(".lnk-delete-address").hide();
        }
    },
    Reg_Profession: function () {
        var fnProfession = function () {

            if ($("input[name='rdbBestProfessionalOption']").length > 0) {
                var selected = $("input[name='rdbBestProfessionalOption']:checked").val();

                if (selected != null && selected != undefined) {
                    if (selected == '3') {
                        $('.div-profissao').hide();
                        $('#txtRole, #txtCompany').val('');
                    }
                    else if (selected == '5' || selected == '6') {
                        $('.div-profissao').hide();
                        $('.div-aposentado').hide();
                        $('#txtRole, #txtCompany, #txtPropertyCNPJ').val('');
                    }
                    else {
                        $('.div-profissao').show();
                        $('.div-aposentado').show();
                    }
                }
            }
        }

        fnProfession();

        $("input[name='rdbBestProfessionalOption']").change(function () {
            fnProfession();
        });
    },
    Reg_BankAccounts: function (ctvm) {

        var fnDeleteAccount = function () {
            $(".lnkRemoveAccount", Registration.context).unbind("click").bind("click", function () {
                if ($("#tblAccounts > tbody > tr").length > 1) {
                    $(this).parent().parent().remove();

                    if ($("#tblAccounts > tbody > tr").length == 1) {
                        $(".lnkRemoveAccount").hide();
                    }
                }
            });
        };

        $(".txt-numero", Registration.context).setMask("number10");
        $(".txt-cpf", Registration.context).setMask("cpf");
        fnBankAutoComplete();

        $("#lnkAddAccount").unbind("click").bind("click", function () {
            if ($("#tblAccounts > tbody > tr").length < 3) {

                $.ajax({
                    type: "POST",
                    url: "../ClientRegistration/AddNewBankAccount",
                    cache: false,
                    data: { ctvm: ctvm },
                    success: function (data) {
                        $("#tblAccounts > tbody").append($(data).clone());

                        $(".lnkRemoveAccount").show();

                        fnDeleteAccount();

                        $(".txt-numero", Registration.context).setMask("number10");
                        $(".txt-cpf", Registration.context).setMask("cpf");
                        fnBankAutoComplete();
                    }
                });

            }
        });

        fnDeleteAccount();

        if ($("#tblAccounts > tbody > tr").length == 1) {
            $(".lnkRemoveAccount").hide();
        }
    },
    Reg_References: function (url) {
        var fnDeleteRef = function () {
            $(".lnkRemoveRef", Registration.context).unbind("click").bind("click", function () {
                if ($("#tblReferences > tbody > tr").length > 1) {
                    $(this).parent().parent().remove();

                    if ($("#tblReferences > tbody > tr").length == 1) {
                        $(".lnkRemoveRef").hide();
                    }
                }
            });
        };

        $(".txt-phone", Registration.context).setMask("(99) 9999-9999");

        $("#lnkAddRef").unbind("click").bind("click", function () {
            if ($("#tblReferences > tbody > tr").length < 3) {
                $.ajax({
                    type: "POST",
                    url: "../ClientRegistration/" + url,
                    cache: false,
                    success: function (data) {
                        $("#tblReferences > tbody").append($(data).clone());

                        $(".lnkRemoveRef").show();

                        fnDeleteRef();

                        $(".txt-phone", Registration.context).setMask("(99) 9999-9999");
                    }
                });
            }
        });

        fnDeleteRef();

        if ($("#tblReferences > tbody > tr").length == 1) {
            $(".lnkRemoveRef").hide();
        }
    },
    Reg_PatrimonialData: function () {
        $("#EstimatedPatrimonyArea").append("<div>Especifique: <input type='text' name='txtOtherEstimatedPatrimony' id='txtOtherEstimatedPatrimony' /></div>");
        $("#PatrimonyOriginArea").append("<div>Especifique: <input type='text' name='txtOtherPatrimonyOrigin' id='txtOtherPatrimonyOrigin' /></div>");
        $("#LiquidityArea").append("<div>Especifique: <input type='text' name='txtOtherLiquidity' id='txtOtherLiquidity' /></div>");
        $("#AnnualIncomeArea").append("<div>Especifique: <input type='text' name='txtOtherAnnualIncome' id='txtOtherAnnualIncome' /></div>");
        $("#TransactionArea").append("<div>Especifique: <input type='text' name='txtOtherTransaction' id='txtOtherTransaction' /></div>");
        $("#InvestmentArea").append("<div>Especifique: <input type='text' name='txtOtherInvestment' id='txtOtherInvestment' /></div>");

        $("#txtOtherEstimatedPatrimony, #txtOtherLiquidity, #txtOtherAnnualIncome, #txtOtherTransaction, #txtOtherInvestment").setMask('decimal');

        $("input[name='rdbEstimatedPatrimony']").change(function () {
            if ($(this).val() == '5') {
                $("#EstimatedPatrimonyArea").show();
            }
            else {
                $("#EstimatedPatrimonyArea").hide();
            }
        });

        $("input[name='chkPatrimonyOrigin']").click(function () {
            if ($(this).val() == '6') {
                $("#PatrimonyOriginArea").toggle();
            }
        });

        $("input[name='rdbLiquidity']").change(function () {
            if ($(this).val() == '5') {
                $("#LiquidityArea").show();
            }
            else {
                $("#LiquidityArea").hide();
            }
        });

        $("input[name='rdbInvestment']").change(function () {
            if ($(this).val() == '5') {
                $("#InvestmentArea").show();
            }
            else {
                $("#InvestmentArea").hide();
            }
        });

        $("input[name='rdbAnnualIncome']").change(function () {
            if ($(this).val() == '8') {
                $("#AnnualIncomeArea").show();
            }
            else {
                $("#AnnualIncomeArea").hide();
            }
        });

        $("input[name='rdbTransaction']").change(function () {
            if ($(this).val() == '5') {
                $("#TransactionArea").show();
            }
            else {
                $("#TransactionArea").hide();
            }
        });

        $.ajax({
            type: "POST",
            cache: false,
            url: "../ClientRegistration/GetOtherPatrimony",
            success: function (data) {
                if (data != undefined && data != null) {
                    $("#txtOtherEstimatedPatrimony").val(data.OtherEstimatedPatrimony);
                    $("#txtOtherPatrimonyOrigin").val(data.OtherPatrimonyOrigin);
                    $("#txtOtherLiquidity").val(data.OtherLiquidity);
                    $("#txtOtherAnnualIncome").val(data.OtherAnnualIncome);
                    $("#txtOtherTransaction").val(data.OtherTransaction);
                    $("#txtOtherInvestment").val(data.OtherInvestment);
                }
            }
        });
    },
    Reg_Admin: function () {
        if ($("input[name=rdbNonResidentInvestor]:checked").val() == 'S') {
            $(".caixa-investidor").show();
        }

        $("input[name='rdbNonResidentInvestor']").change(function () {

            if ($(this).val() == 'S') {
                $(".caixa-investidor").show();
            }
            else {
                $(".caixa-investidor").hide();
            }
        });

        if ($("input[name='rdbCollectiveAccount']:checked").val() == 'S') {
            $(".titular-coletiva").show();
        }

        $("input[name='rdbCollectiveAccount']").change(function () {

            if ($(this).val() == 'S') {
                $(".titular-coletiva").show();
            }
            else {
                $(".titular-coletiva").hide();
            }
        });

    }
    , Reg_ImobileGoods: function () {
        var fnDeleteImobileGoods = function () {
            $(".lnkRemoveImobile", Registration.context).unbind("click").bind("click", function () {
                var rel = $(this).attr("rel");

                $("#imobile-" + rel).remove();

                if ($(".lnkRemoveImobile").length == 1) {
                    $(".lnkRemoveImobile").hide();
                }

            });
        };
        $(".txt-decimal", Registration.context).setMask("decimal");
        $(".txt-numero-cep", Registration.context).setMask("number5");
        $(".txt-numero-cep2", Registration.context).setMask("number3");

        $("#lnkAddImobile").unbind("click").bind("click", function () {
            $.ajax({
                type: "POST",
                url: "../ClientRegistration/AddNewImobileGoods",
                cache: false,
                success: function (data) {
                    if ($(".caixa-bens-imoveis").length < 3) {
                        $(".caixa-bens-imoveis").last().after(
                            $(data).clone()
                        );
                        fnDeleteImobileGoods();
                        $('select').styleInputs();

                        $(".lnkRemoveImobile").show();
                        $(".txt-decimal", Registration.context).setMask("decimal");
                        $(".txt-numero-cep", Registration.context).setMask("number5");
                        $(".txt-numero-cep2", Registration.context).setMask("number3");
                    }
                }
            });
        });

        fnDeleteImobileGoods();

        if ($(".lnkRemoveImobile").length == 1) {
            $(".lnkRemoveImobile").hide();
        }
    },
    Reg_MobileGoods: function () {
        var fnDeleteMobileGoods = function () {
            $(".lnkRemoveMobileGoods", Registration.context).unbind("click").bind("click", function () {
                if ($("#tblMobileGoods > tbody > tr").length > 1) {
                    $(this).parent().parent().remove();

                    if ($("#tblMobileGoods > tbody > tr").length == 1) {
                        $(".lnkRemoveMobileGoods").hide();
                    }
                }
            });
        };

        $(".txt-decimal", Registration.context).setMask("decimal");

        $("#lnkAddMobile").unbind("click").bind("click", function () {
            if ($("#tblMobileGoods > tbody > tr").length < 4) {
                $.ajax({
                    type: "POST",
                    url: "../ClientRegistration/AddNewMobileGoods",
                    cache: false,
                    success: function (data) {
                        $("#tblMobileGoods > tbody").append($(data).clone());

                        $(".lnkRemoveMobileGoods").show();
                        $('select').styleInputs();
                        fnDeleteMobileGoods();

                        $(".txt-decimal", Registration.context).setMask("decimal");

                    }
                });
            }
        });

        fnDeleteMobileGoods();

        if ($("#tblMobileGoods > tbody > tr").length == 1) {
            $(".lnkRemoveMobileGoods").hide();
        }
    },
    Reg_MensalRendiments: function () {
        var fnDeleteMensalRendiments = function () {
            $(".lnkRemoveMensalRendiment", Registration.context).unbind("click").bind("click", function () {
                if ($("#tblMensalRendiments > tbody > tr").length > 1) {
                    $(this).parent().parent().remove();

                    if ($("#tblMensalRendiments > tbody > tr").length == 1) {
                        $(".lnkRemoveMensalRendiment").hide();
                    }
                }
            });
        };

        $(".txt-decimal", Registration.context).setMask("decimal");

        var fnChange = function () {
            $(".ddlMensalType").unbind("change").bind("change", function () {
                if ($(this).val() == '2') { //Outros
                    $(this).parent().find(".txt-descricao").show();
                }
                else {
                    $(this).parent().find(".txt-descricao").hide();
                }
            });
        };

        fnChange();

        $("#lnkAddMensalRendiment").unbind("click").bind("click", function () {
            if ($("#tblMobileGoods > tbody > tr").length < 3) {
                $.ajax({
                    type: "POST",
                    url: "../ClientRegistration/AddNewMensalRendiments",
                    cache: false,
                    success: function (data) {
                        $("#tblMensalRendiments > tbody").append($(data).clone());

                        fnDeleteMensalRendiments();
                        $('select').styleInputs();

                        $(".lnkRemoveMensalRendiment").show();

                        $(".txt-decimal", Registration.context).setMask("decimal");

                        fnChange();

                    }
                });
            }
        });

        fnDeleteMensalRendiments();

        if ($("#tblMensalRendiments > tbody > tr").length == 1) {
            $(".lnkRemoveMensalRendiment").hide();
        }
    },
    Reg_Authorizations: function (countNumber) {
        if ($("input[name='rdbAuthorizesTransmissionOrders']:checked").val() == "S") {
            $("#divTableAuthorization").show();
        }
        else {
            $("#divTableAuthorization").hide();
        }

        $("input[name='rdbAuthorizesTransmissionOrders']").change(function () {
            if ($(this).val() == "S") {
                $("#divTableAuthorization").show();
            }
            else {
                $("#divTableAuthorization").hide();
            }
        });

        var fnDeleteAccount = function () {
            $(".lnkRemoveAccount", Registration.context).unbind("click").bind("click", function () {
                if ($("#tblAuthorizedAccounts > tbody > tr").length > 1) {
                    $(this).parent().parent().remove();

                    if ($("#tblAuthorizedAccounts > tbody > tr").length == 1) {
                        $(".lnkRemoveAccount").hide();
                    }
                }
            });
        };

        $(".txt-numero-conta", Registration.context).setMask("number10");
        $(".txt-cpf", Registration.context).setMask("cpf");


        $("#lnkAddAuthorizedAccount").unbind("click").bind("click", function () {
            if ($("#tblAuthorizedAccounts > tbody > tr").length < countNumber) {
                $.ajax({
                    type: "POST",
                    url: "../ClientRegistration/AddNewAuthorizedAccount",
                    cache: false,
                    success: function (data) {
                        $("#tblAuthorizedAccounts > tbody").append($(data).clone());

                        fnDeleteAccount();
                        $('select').styleInputs();

                        $(".lnkRemoveAccount").show();

                        $(".txt-numero-conta", Registration.context).setMask("number10");
                        $(".txt-cpf", Registration.context).setMask("cpf");

                        fnBankAutoComplete();

                    }
                });
            }
        });

        fnDeleteAccount();
        fnBankAutoComplete();

        if ($("#tblAuthorizedAccounts > tbody > tr").length == 1) {
            $(".lnkRemoveAccount").hide();
        }
    }
    , Reg_PJ_Authorizations: function (ctvm) {
        if ($("input[name='rdbAuthorizesTransmissionOrders']:checked").val() == "S") {
            $("#divTableAuthorization").show();
            $("#divTableAuthorizationMembers").show();
        }
        else {
            $("#divTableAuthorization").hide();
            $("#divTableAuthorizationMembers").hide();
        }

        $("input[name='rdbAuthorizesTransmissionOrders']").change(function () {
            if ($(this).val() == "S") {
                $("#divTableAuthorization").show();
                $("#divTableAuthorizationMembers").show();
            }
            else {
                $("#divTableAuthorization").hide();
                $("#divTableAuthorizationMembers").hide();
            }
        });

        var fnDelete = function () {
            $(".lnkRemoveMember", Registration.context).unbind("click").bind("click", function () {
                if ($("#tblAuthorizedMembers > tbody > tr").length > 1) {
                    $(this).parent().parent().remove();

                    if ($("#tblAuthorizedMembers > tbody > tr").length == 1) {
                        $(".lnkRemoveMember").hide();
                    }
                }
            });
        };

        $(".txt-numero-conta, .txt-numero", Registration.context).setMask("number10");
        $(".txt-cpf", Registration.context).setMask("cpf");
        $(".txt-data", Registration.context).setMask("39/19/2999");


        $("#lnkAddMember").unbind("click").bind("click", function () {
            if ($("#tblAuthorizedMembers > tbody > tr").length < 4) {
                $.ajax({
                    type: "POST",
                    url: "../ClientRegistration/AddNewAuthorizedMember",
                    cache: false,
                    data: { ctvm: ctvm },
                    success: function (data) {
                        $("#tblAuthorizedMembers > tbody").append($(data).clone());

                        fnDelete();
                        $('select').styleInputs();

                        $(".lnkRemoveMember").show();

                        $(".txt-data", Registration.context).setMask("39/19/2999");
                        $(".txt-cpf", Registration.context).setMask("cpf");
                        $(".txt-numero", Registration.context).setMask("number10");

                    }
                });
            }
        });

        fnDelete();

        if ($("#tblAuthorizedMembers > tbody > tr").length == 1) {
            $(".lnkRemoveMember").hide();
        }
    }
    , Reg_StockControls: function () {
        var fnDeleteStockControls = function () {
            $(".lnkRemoveStockControl", Registration.context).unbind("click").bind("click", function () {
                if ($("#tblStockControl > tbody > tr").length > 1) {
                    $(this).parent().parent().remove();

                    if ($("#tblStockControl > tbody > tr").length == 1) {
                        $(".lnkRemoveStockControl").hide();
                    }
                }
            });
        };

        $("#lnkAddStockControl").unbind("click").bind("click", function () {
            if ($("#tblStockControl > tbody > tr").length < 4) {
                $.ajax({
                    type: "POST",
                    url: "../ClientRegistration/AddNewStockControl",
                    cache: false,
                    success: function (data) {
                        $("#tblStockControl > tbody").append($(data).clone());

                        $(".lnkRemoveStockControl").show();
                        $('select').styleInputs();
                        fnDeleteStockControls();

                        $(".txt-decimal").setMask("decimal");
                        $(".txt-numero, .txt-numero-conta").setMask("number10");
                        $(".txt-data").setMask("39/19/2999");
                        fnCPFCNPJMask();

                    }
                });
            }
        });

        fnDeleteStockControls();

        if ($("#tblStockControl > tbody > tr").length == 1) {
            $(".lnkRemoveStockControl").hide();
        }

        fnCPFCNPJMask();
    }
    , Reg_ControlledLegalPerson: function () {
        var fnDeleteControlled = function () {
            $(".lnkCTP", Registration.context).unbind("click").bind("click", function () {
                if ($("#tblControlledLegalPerson > tbody > tr").length > 1) {
                    $(this).parent().parent().remove();

                    if ($("#tblControlledLegalPerson > tbody > tr").length == 1) {
                        $(".lnkCTP").hide();
                    }
                }
            });
        };

        $("#lnkAddControlled").unbind("click").bind("click", function () {
            if ($("#tblControlledLegalPerson > tbody > tr").length < 2) {
                $.ajax({
                    type: "POST",
                    url: "../ClientRegistration/AddNewControlledLegalPerson",
                    cache: false,
                    success: function (data) {
                        $("#tblControlledLegalPerson > tbody").append($(data).clone());

                        $(".lnkCTP").show();
                        $('select').styleInputs();
                        fnDeleteControlled();


                        $(".txt-numero, .txt-numero-conta").setMask("number10");
                        $(".txt-cnpj").setMask("cnpj");

                    }
                });
            }
        });

        fnDeleteControlled();

        if ($("#tblControlledLegalPerson > tbody > tr").length == 1) {
            $(".lnkCTP").hide();
        }
        $(".txt-cnpj").setMask("cnpj");
    }
    , Reg_Affiliated: function () {
        var fnDeleteAffiliated = function () {
            $(".lnkAF", Registration.context).unbind("click").bind("click", function () {
                if ($("#tblAffiliated > tbody > tr").length > 1) {
                    $(this).parent().parent().remove();

                    if ($("#tblAffiliated > tbody > tr").length == 1) {
                        $(".lnkAF").hide();
                    }
                }
            });
        };

        $("#lnkAddAffiliated").unbind("click").bind("click", function () {
            if ($("#tblAffiliated > tbody > tr").length < 2) {
                $.ajax({
                    type: "POST",
                    url: "../ClientRegistration/AddNewAffiliated",
                    cache: false,
                    success: function (data) {
                        $("#tblAffiliated > tbody").append($(data).clone());

                        $(".lnkAF").show();
                        $('select').styleInputs();
                        fnDeleteAffiliated();

                        $(".txt-numero, .txt-numero-conta").setMask("number10");
                        $(".txt-cnpj").setMask("cnpj");

                    }
                });
            }
        });

        fnDeleteAffiliated();

        if ($("#tblAffiliated > tbody > tr").length == 1) {
            $(".lnkAF").hide();
        }
        $(".txt-cnpj").setMask("cnpj");
    }
    , Reg_Attorney: function (ctvm) {
        var fnDeleteAttorney = function () {
            $(".lnkAT", Registration.context).unbind("click").bind("click", function () {
                if ($("#tblAttorney > tbody > tr").length > 1) {
                    $(this).parent().parent().remove();

                    if ($("#tblAttorney > tbody > tr").length == 1) {
                        $(".lnkAT").hide();
                    }
                }
            });
        };

        $("#lnkAddAttorney").unbind("click").bind("click", function () {
            if ($("#tblAttorney > tbody > tr").length < 4) {
                $.ajax({
                    type: "POST",
                    url: "../ClientRegistration/AddNewAttorney",
                    data: { ctvm: ctvm },
                    cache: false,
                    success: function (data) {
                        $("#tblAttorney > tbody").append($(data).clone());

                        $(".lnkAT").show();
                        $('select').styleInputs();
                        fnDeleteAttorney();

                        $(".txt-numero, .txt-numero-conta").setMask("number10");
                        $(".txt-data").setMask("39/19/9999");
                        fnCPFCNPJMask();
                    }
                });
            }
        });

        fnDeleteAttorney();

        if ($("#tblAttorney > tbody > tr").length == 1) {
            $(".lnkAT").hide();
        }
        fnCPFCNPJMask();
    }
    , Reg_PJ_Authorization_Same: function () {
        var fnDelete = function () {
            $(".lnkRemoveAccountSame", Registration.context).unbind("click").bind("click", function () {
                if ($("#tblAuthorizedAccountsSame > tbody > tr").length > 1) {
                    $(this).parent().parent().remove();

                    if ($("#tblAuthorizedAccountsSame > tbody > tr").length == 1) {
                        $(".lnkRemoveAccountSame").hide();
                    }
                }
            });
        };

        $("#lnkAddAccountSame").unbind("click").bind("click", function () {
            if ($("#tblAuthorizedAccountsSame > tbody > tr").length < 3) {
                $.ajax({
                    type: "POST",
                    url: "../ClientRegistration/AddNewSameAuthorizedAccount",
                    cache: false,
                    success: function (data) {
                        $("#tblAuthorizedAccountsSame > tbody").append($(data).clone());

                        $(".lnkRemoveAccountSame").show();
                        $('select').styleInputs();
                        fnDelete();

                        fnBankAutoComplete();

                        $(".txt-numero, .txt-numero-conta").setMask("number10");

                    }
                });
            }
        });

        fnDelete();

        fnBankAutoComplete();

        if ($("#tblAuthorizedAccountsSame > tbody > tr").length == 1) {
            $(".lnkRemoveAccountSame").hide();
        }
    }
    , Reg_PJ_Authorization_Others: function () {
        var fnDelete = function () {
            $(".lnkRemoveAccountOthers", Registration.context).unbind("click").bind("click", function () {
                if ($("#tblAuthorizedAccountsOthers > tbody > tr").length > 2) {
                    var rel = $(this).attr("rel");
                    $(".conta-" + rel).remove();
                    $(this).parent().parent().remove();

                    if ($("#tblAuthorizedAccountsOthers > tbody > tr").length == 2) {
                        $(".lnkRemoveAccountOthers").hide();
                    }
                }
            });
        };

        $("#lnkAddAccountOthers").unbind("click").bind("click", function () {
            if ($("#tblAuthorizedAccountsOthers > tbody > tr").length < 6) {
                $.ajax({
                    type: "POST",
                    url: "../ClientRegistration/AddNewOtherAuthorizedAccount",
                    cache: false,
                    success: function (data) {
                        $("#tblAuthorizedAccountsOthers > tbody").append($(data).clone());

                        $(".lnkRemoveAccountOthers").show();
                        $('select').styleInputs();
                        fnDelete();

                        $(".txt-numero, .txt-numero-conta").setMask("number10");
                        fnCPFCNPJMask();
                        fnBankAutoComplete();
                    }
                });
            }
        });

        fnDelete();
        fnBankAutoComplete();

        if ($("#tblAuthorizedAccountsOthers > tbody > tr").length == 2) {
            $(".lnkRemoveAccountOthers").hide();
        }
        fnCPFCNPJMask();
    }
    , Reg_PJ_PPE: function () {
        var fnDelete = function () {
            $(".lnkRemovePolitical", Registration.context).unbind("click").bind("click", function () {

                $(this).parent().parent().remove();

                if ($("#tblPoliticalMembers > tbody > tr").length == 1) {
                    $(".lnkRemovePolitical").hide();
                }

            });
        };

        $("#lnkAddPolitical").unbind("click").bind("click", function () {
            if ($("#tblPoliticalMembers > tbody > tr").length < 2) {
                $.ajax({
                    type: "POST",
                    url: "../ClientRegistration/AddNewPPE",
                    cache: false,
                    success: function (data) {
                        $("#tblPoliticalMembers > tbody").append($(data).clone());

                        $(".lnkRemovePolitical").show();
                        $('select').styleInputs();
                        fnDelete();

                        $(".txt-cpf").setMask("cpf");

                    }
                });
            }
        });

        fnDelete();

        if ($("#tblPoliticalMembers > tbody > tr").length == 1) {
            $(".lnkRemovePolitical").hide();
        }

        $("input[name='rdbPPE']").click(function () {
            if ($(this).val() == 'S') {
                $("#PoliticalArea").show();
            }
            else {
                $("#PoliticalArea").hide();
            }
        });

        if ($("input[name='rdbPPE']:checked").val() == "S") {
            $("#PoliticalArea").show();
        }
        else {
            $("#PoliticalArea").hide();
        }
    }
};

var Registration = {
    context: $("#tab-area")[0],
    PF_Phase1: function () {
        $('select').styleInputs();

        RegistrationAreas.Reg_InfoPF();

        RegistrationAreas.Reg_CivilState();

    },
    PF_Phase2: function () {
        $('select').styleInputs();

        RegistrationAreas.Reg_Address();

        $("#txtPropertyCNPJ", Registration.context).setMask("cnpj");

        RegistrationAreas.Reg_Profession();

        $("#lnkPFPhase2", Registration.context).unbind("click").bind("click", function () {
            $("#frmPF_Phase2").submit();
        });
    },
    PF_Phase3: function () {
        $('select').styleInputs();
        ///contas bancárias
        RegistrationAreas.Reg_BankAccounts(false);

        /// referencias

        RegistrationAreas.Reg_References("AddNewReferences");

        ///Movimentação financeira
        RegistrationAreas.Reg_PatrimonialData();

        $("#lnkPFPhase3", Registration.context).unbind("click").bind("click", function () {
            $("#frmPF_Phase3").submit();
        });
    },
    PF2: function () {

        $('select').styleInputs();

        RegistrationAreas.Reg_Admin();

        //Bens imóveis
        RegistrationAreas.Reg_ImobileGoods();

        //Bens móveis
        RegistrationAreas.Reg_MobileGoods();

        //Rendimentos mensais
        RegistrationAreas.Reg_MensalRendiments();

        //autorizações
        RegistrationAreas.Reg_Authorizations(2);

        RegistrationAreas.Reg_PJ_Authorizations(false);

    },
    PFCTVM_Phase1: function () {
        $('select').styleInputs();

        RegistrationAreas.Reg_InfoPF();

        RegistrationAreas.Reg_CivilState();

        RegistrationAreas.Reg_Address();

        $("#txtPropertyCNPJ", Registration.context).setMask("cnpj");

        $("#lnkPFCTVMPhase1", Registration.context).unbind("click").bind("click", function () {
            $("#frmPFCTVM_Phase1").submit();
        });
    },
    PFCTVM_Phase2: function () {
        $('select').styleInputs();

        RegistrationAreas.Reg_BankAccounts(true);

        /// referencias

        RegistrationAreas.Reg_References("AddNewReferences");

        ///Movimentação financeira
        RegistrationAreas.Reg_PatrimonialData();

        RegistrationAreas.Reg_ImobileGoods();

        //Bens móveis
        RegistrationAreas.Reg_MobileGoods();

        //Rendimentos mensais
        RegistrationAreas.Reg_MensalRendiments();

        //autorizações
        RegistrationAreas.Reg_Authorizations(4);

        RegistrationAreas.Reg_PJ_Authorizations(true);

        $("#lnkPFCTVMPhase2", Registration.context).unbind("click").bind("click", function () {
            $("#frmPFCTVM_Phase2").submit();
        });
    },
    PJ1_Phase1: function () {

        $("#txtCNPJ, #txtCNPJ2", Registration.context).setMask("cnpj");
        $(".txt_numero, #txtStateInscription", Registration.context).setMask("number10");
        $("#txtConstitutionDate", Registration.context).setMask("39/19/2999");
        $("#txtNIRE", Registration.context).setMask("999.999999-9");
        $(".txt-phone", Registration.context).setMask("phone");

        $("input[name='rdbConstitutionForm']").change(function () {
            if ($(this).val() == '2') {
                $("#ConstitutionFormArea").show();
            }
            else {
                $("#ConstitutionFormArea").hide();
            }
        });

        $("#lnkPJPhase1", Registration.context).unbind("click").bind("click", function () {
            $("#frmPJ1_Phase1").submit();
        });
    },
    PJ1_Phase2: function () {
        $(".txt-decimal").setMask("decimal");
        $(".txt-numero, .txt-numero-conta").setMask("number10");
        $(".txt-cpf").setMask("cpf");
        $(".txt-phone").setMask("phone");
        $(".txt-data").setMask("39/19/2999");


        //Controle Acionario - Stock Control
        RegistrationAreas.Reg_StockControls();

        RegistrationAreas.Reg_ControlledLegalPerson();

        RegistrationAreas.Reg_Affiliated();

        RegistrationAreas.Reg_Attorney(false);

        RegistrationAreas.Reg_BankAccounts(false);

        RegistrationAreas.Reg_References("AddNewPJReferences");

        RegistrationAreas.Reg_PJ_Authorization_Same();

        RegistrationAreas.Reg_PJ_Authorization_Others();


        $("#lnkPJPhase2", Registration.context).unbind("click").bind("click", function () {
            $("#frmPJ1_Phase2").submit();
        });
    }
    , PJ2: function () {
        $(".txt-decimal").setMask("decimal");
        $(".txt-numero, .txt-numero-conta").setMask("number10");
        $(".txt-cpf").setMask("cpf");
        $(".txt-phone").setMask("phone");
        $(".txt-data").setMask("39/19/2999");

        if ($("input[name='rdbCollectiveAccount']:checked").val() == 'S') {
            $(".titular-coletiva").show();
        }

        $("input[name='rdbCollectiveAccount']").change(function () {

            if ($(this).val() == 'S') {
                $(".titular-coletiva").show();
            }
            else {
                $(".titular-coletiva").hide();
            }
        });

        //Bens imóveis
        RegistrationAreas.Reg_ImobileGoods();

        //Bens móveis
        RegistrationAreas.Reg_MobileGoods();

        RegistrationAreas.Reg_PJ_Authorizations(false);

        /*$("#lnkPJ2").unbind("click").bind("click", function () {
        $("#frmPJ2").submit();
        });*/

        $("#lnkPJ2").unbind("click").bind("click", function () {
            $.ajax({
                type: "POST",
                data: $("form").serialize(),
                url: "../ClientRegistration/Process_PJ2",
                cache: false,
                success: function (data) {
                    if (data.Result == true) {
                        //gerar arquivo
                        window.location = "/ClientRegistration/PJ2ToFile"
                    }
                }
            });
        });
    }
    , PJCTVM_Phase1: function () {
        $('select').styleInputs();
        $("#txtCNPJ", Registration.context).setMask("cnpj");
        $(".txt_numero, #txtStateInscription", Registration.context).setMask("number10");
        $("#txtConstitutionDate", Registration.context).setMask("39/19/2999");
        $("#txtNIRE", Registration.context).setMask("999.999999-9");
        $(".txt-phone", Registration.context).setMask("phone");

        $("input[name='rdbConstitutionForm']").change(function () {
            if ($(this).val() == '2') {
                $("#ConstitutionFormArea").show();
            }
            else {
                $("#ConstitutionFormArea").hide();
            }
        });

        $("#lnkPJCTVMPhase1", Registration.context).unbind("click").bind("click", function () {
            $("#frmPJCTVM_Phase1").submit();
        });

    }
    , PJCTVM_Phase2: function () {
        $('select').styleInputs();
        $(".txt-decimal").setMask("decimal");
        $(".txt-numero, .txt-numero-conta").setMask("number10");
        $(".txt-cpf").setMask("cpf");
        $(".txt-phone").setMask("phone");
        $(".txt-data").setMask("39/19/2999");

        //Controle Acionario - Stock Control
        RegistrationAreas.Reg_StockControls();

        RegistrationAreas.Reg_ControlledLegalPerson();

        RegistrationAreas.Reg_Affiliated();

        RegistrationAreas.Reg_Attorney(true);

        //RegistrationAreas.Reg_BankAccounts(false);

        //Bens imóveis
        RegistrationAreas.Reg_ImobileGoods();

        //Bens móveis
        RegistrationAreas.Reg_MobileGoods();

        RegistrationAreas.Reg_PJ_Authorizations(false);

        RegistrationAreas.Reg_PJ_PPE();

        RegistrationAreas.Reg_BankAccounts(false);

        $("#lnkPJCTVMPhase2").unbind("click").bind("click", function () {
            $.ajax({
                type: "POST",
                data: $("form").serialize(),
                url: "../ClientRegistration/Process_PJCTVM_Phase2",
                cache: false,
                success: function (data) {
                    if (data.Result == true) {
                        //gerar arquivo
                        window.location = "/ClientRegistration/PJCTVMToFile"
                    }
                }
            });
        });

    }
}

$(function () {
    var content = $("#tab-area")[0];

    var tabs = $("#tab-area").tabs();

    var regType = $(".reg-type").val();

    $("#txtClientFilter").setMask("number7").css({"text-align": "left"});

    $("#RegistrationFilter").click(function () {

        $('#RegistrationContent').slideToggle('fast');
        $('select').styleInputs();
        $(this).toggleClass('aberto');
    });

    $("#lnkSubmitRegistrationFilter").unbind("click").bind("click", function () {
        if (!isNaN($("#txtClientFilter").val())) {
            $.ajax({
                type: "POST",
                url: "../ClientRegistration/RegistrationFilter",
                data: { "clientCode": $("#txtClientFilter").val(), "type": $("input[name='radRegType']:checked").val() },
                success: function (data) {
                    if (data.Result == false) {
                        $("#FeedbackError").show().find("span").html(data.Message);
                    }
                    else {
                        window.location = data.Location;
                    }
                }
            });
        }
    });

    $(".lnkRegistration").unbind('click').bind('click', function () {
        var regType = $(this).attr('rel');
        $.ajax({
            cache: false,
            async: false,
            url: (regType == "PF" || regType == "PFCTVM") ? "../ClientRegistration/NewPFRegistration" : "../ClientRegistration/NewPJRegistration",
            type: "POST"
        });
    });

    switch (regType) {
        case "PF":
            {
                var fnFormPhase1 = function () {
                    $("#frmPF_Phase1").ajaxForm({
                        beforeSend: function () {
                            $("#loadingArea").show();
                            $(".client-registration").hide();
                        },
                        complete: function () {
                            $("#loadingArea").hide();
                            $(".client-registration").show();
                        },
                        success: function (data) {

                            $("#PF1", content).html(data);

                            Registration.PF_Phase2();

                            window.scrollTo(0, 0);

                            $("#passo-1-back").unbind("click").bind("click", function () {
                                $.ajax({
                                    type: "POST",
                                    url: "../ClientRegistration/Back_PF_Phase1",
                                    cache: false,
                                    success: function (data) {
                                        $("#PF1", content).html(data);

                                        Registration.PF_Phase1();

                                        window.scrollTo(0, 0);

                                        fnFormPhase1();
                                    }
                                });

                            });

                            fnFormPhase2();
                        }
                    });
                }

                var fnFormPhase2 = function () {
                    $("#frmPF_Phase2").ajaxForm({
                        beforeSend: function () {
                            $("#loadingArea").show();
                            $(".client-registration").hide();
                        },
                        complete: function () {
                            $("#loadingArea").hide();
                            $(".client-registration").show();
                        },
                        success: function (data) {
                            $("#PF1", content).html(data);

                            Registration.PF_Phase3();

                            window.scrollTo(0, 0);

                            $("#passo-2-back").unbind("click").bind("click", function () {
                                $.ajax({
                                    type: "POST",
                                    url: "../ClientRegistration/Back_PF_Phase2",
                                    cache: false,
                                    beforeSend: function () {
                                        $("#loadingArea").show();
                                        $(".client-registration").hide();
                                    },
                                    complete: function () {
                                        $("#loadingArea").hide();
                                        $(".client-registration").show();
                                    },
                                    success: function (data) {
                                        $("#PF1", content).html(data);

                                        Registration.PF_Phase2();

                                        window.scrollTo(0, 0);


                                        fnFormPhase2();
                                        $("#passo-1-back").unbind("click").bind("click", function () {
                                            $.ajax({
                                                type: "POST",
                                                url: "../ClientRegistration/Back_PF_Phase1",
                                                cache: false,
                                                beforeSend: function () {
                                                    $("#loadingArea").show();
                                                    $(".client-registration").hide();
                                                },
                                                complete: function () {
                                                    $("#loadingArea").hide();
                                                    $(".client-registration").show();
                                                },
                                                success: function (data) {
                                                    $("#PF1", content).html(data);

                                                    Registration.PF_Phase1();

                                                    window.scrollTo(0, 0);

                                                    fnFormPhase1();
                                                }
                                            });

                                        });
                                    }
                                });

                            });

                            $("#frmPF_Phase3").ajaxForm({
                                /*beforeSend: function () {
                                $("#loadingArea").show();
                                $(".client-registration").hide();
                                },
                                complete: function () {
                                $("#loadingArea").hide();
                                $(".client-registration").show();
                                },*/
                                success: function (data) {
                                    if (data.Result == true) {
                                        /*tabs.tabs("select", 1);
                                        window.scrollTo(0, 0);*/
                                        window.location = "/ClientRegistration/PFToFile"
                                    }
                                }
                            });

                        }
                    });
                };

                fnFormPhase1();

                $("#lnkPF2").unbind("click").bind("click", function () {
                    $.ajax({
                        type: "POST",
                        data: $("form").serialize(),
                        url: "../ClientRegistration/Process_PF2",
                        cache: false,
                        success: function (data) {
                            if (data.Result == true) {
                                //gerar arquivo
                                window.location = "/ClientRegistration/PF2ToFile"
                            }
                        }
                    });
                });

                Registration.PF_Phase1();
                Registration.PF2();

                $("#lnkGeneratePFPDF").unbind("click").bind("click", function () {
                    var tab = tabs.tabs("option", "selected");
                    if (tab == 0) {
                        var url = "../ClientRegistration/Process_PF_Phase1";
                        if ($("#txtResidentialPhone").length == 1) {
                            url = "../ClientRegistration/Process_PF_Phase2";
                        }
                        else if ($("#tblAccounts").length == 1) {
                            url = "../ClientRegistration/Process_PF_Phase3";
                        }

                        $.ajax({
                            type: "POST",
                            url: url,
                            data: $($("form")[0]).serialize(),
                            cache: false,
                            success: function () {
                                window.location = "../ClientRegistration/PFToFile";
                            }
                        });
                    }
                    else if (tab == 1) {
                        $.ajax({
                            type: "POST",
                            url: "../ClientRegistration/Process_PF2",
                            data: $("form").serialize(),
                            cache: false,
                            success: function () {
                                window.location = "../ClientRegistration/PF2ToFile";
                            }
                        });
                    }
                });

                break;
            }
        case "PJ":
            {
                var fnFormPJ = function () {
                    $("#frmPJ1_Phase1").ajaxForm({
                        beforeSend: function () {
                            $("#loadingArea").show();
                            $(".client-registration").hide();
                        },
                        complete: function () {
                            $("#loadingArea").hide();
                            $(".client-registration").show();
                        },
                        success: function (data) {
                            $("#PJ1", content).html(data);

                            Registration.PJ1_Phase2();

                            window.scrollTo(0, 0);

                            $("#passo-2-back-pj").unbind("click").bind("click", function () {
                                $.ajax({
                                    type: "POST",
                                    url: "../ClientRegistration/Back_PJ_Phase1",
                                    cache: false,
                                    beforeSend: function () {
                                        $("#loadingArea").show();
                                        $(".client-registration").hide();
                                    },
                                    complete: function () {
                                        $("#loadingArea").hide();
                                        $(".client-registration").show();
                                    },
                                    success: function (data) {
                                        $("#PJ1", content).html(data);

                                        Registration.PJ1_Phase1();

                                        window.scrollTo(0, 0);

                                        fnFormPJ();
                                    }
                                });

                            });

                            $("#frmPJ1_Phase2").ajaxForm({
                                /*beforeSend: function () {
                                $("#loadingArea").show();
                                $(".client-registration").hide();
                                },
                                complete: function () {
                                $("#loadingArea").hide();
                                $(".client-registration").show();
                                },*/
                                success: function (data) {
                                    if (data.Result == true) {
                                        /*tabs.tabs("select", 1);
                                        window.scrollTo(0, 0);*/
                                        window.location = "../ClientRegistration/PJToFile";
                                    }
                                }
                            });

                        }
                    });
                };

                fnFormPJ();

                Registration.PJ1_Phase1();
                Registration.PJ2();

                $("#lnkGeneratePJPDF").unbind("click").bind("click", function () {
                    var tab = tabs.tabs("option", "selected");
                    if (tab == 0) {
                        var url = "../ClientRegistration/Process_PJ1_Phase1";
                        if ($("#txtBaseData").length == 1) {
                            url = "../ClientRegistration/Process_PJ1_Phase2";
                        }

                        $.ajax({
                            type: "POST",
                            url: url,
                            data: $($("form")[0]).serialize(),
                            cache: false,
                            success: function () {
                                window.location = "../ClientRegistration/PJToFile";
                            }
                        });
                    }
                    else if (tab == 1) {
                        $.ajax({
                            type: "POST",
                            url: "../ClientRegistration/Process_PJ2",
                            data: $("form").serialize(),
                            cache: false,
                            success: function () {
                                window.location = "../ClientRegistration/PJ2ToFile";
                            }
                        });
                    }
                });

                break;
            }
        case "PFCTVM":
            {
                var fnFormPFCTVM = function () {
                    $("#frmPFCTVM_Phase1").ajaxForm({
                        beforeSend: function () {
                            $("#loadingArea").show();
                            $(".client-registration").hide();
                        },
                        complete: function () {
                            $("#loadingArea").hide();
                            $(".client-registration").show();
                        },
                        success: function (data) {
                            $("#PFCTVM", content).html(data);

                            Registration.PFCTVM_Phase2();

                            window.scrollTo(0, 0);

                            $("#passo-2-back-pfctvm").unbind("click").bind("click", function () {
                                $.ajax({
                                    type: "POST",
                                    url: "../ClientRegistration/Back_PFCTVM_Phase1",
                                    cache: false,
                                    beforeSend: function () {
                                        $("#loadingArea").show();
                                        $(".client-registration").hide();
                                    },
                                    complete: function () {
                                        $("#loadingArea").hide();
                                        $(".client-registration").show();
                                    },
                                    success: function (data) {
                                        $("#PFCTVM", content).html(data);

                                        Registration.PFCTVM_Phase1();

                                        window.scrollTo(0, 0);

                                        fnFormPFCTVM();
                                    }
                                });

                            });

                            $("#frmPFCTVM_Phase2").ajaxForm({
                                beforeSend: function () {
                                    //colocar loading...
                                },
                                success: function (data) {
                                    window.location = "../ClientRegistration/PFCTVMToFile";
                                }
                            });

                        }
                    });
                };

                fnFormPFCTVM();

                Registration.PFCTVM_Phase1();

                $("#lnkGeneratePFCTVMPDF").unbind("click").bind("click", function () {
                    var tab = tabs.tabs("option", "selected");

                    var url = "../ClientRegistration/Process_PFCTVM_Phase1";
                    if ($("#rdbPPE").length == 1) {
                        url = "../ClientRegistration/Process_PFCTVM_Phase2";
                    }

                    $.ajax({
                        type: "POST",
                        url: url,
                        data: $($("form")[0]).serialize(),
                        cache: false,
                        success: function () {
                            window.location = "../ClientRegistration/PFCTVMToFile";
                        }
                    });


                });
                break;
            }
        case "PJCTVM":
            {
                var fnFormPJCTVM = function () {
                    $("#frmPJCTVM_Phase1").ajaxForm({
                        beforeSend: function () {
                            $("#loadingArea").show();
                            $(".client-registration").hide();
                        },
                        complete: function () {
                            $("#loadingArea").hide();
                            $(".client-registration").show();
                        },
                        success: function (data) {
                            $("#PJCTVM", content).html(data);

                            Registration.PJCTVM_Phase2();

                            window.scrollTo(0, 0);

                            $("#passo-2-back-pjctvm").unbind("click").bind("click", function () {
                                $.ajax({
                                    type: "POST",
                                    url: "../ClientRegistration/Back_PJCTVM_Phase1",
                                    cache: false,
                                    beforeSend: function () {
                                        $("#loadingArea").show();
                                        $(".client-registration").hide();
                                    },
                                    complete: function () {
                                        $("#loadingArea").hide();
                                        $(".client-registration").show();
                                    },
                                    success: function (data) {
                                        $("#PJCTVM", content).html(data);

                                        Registration.PJCTVM_Phase1();

                                        window.scrollTo(0, 0);

                                        fnFormPJCTVM();
                                    }
                                });

                            });

                            $("#frmPJCTVM_Phase2").ajaxForm({
                                beforeSend: function () {
                                    //colocar loading...
                                },
                                success: function (data) {
                                    window.location = "../ClientRegistration/PJCTVMToFile";
                                }
                            });

                        }
                    });
                };

                fnFormPJCTVM();

                Registration.PJCTVM_Phase1();

                $("#lnkGeneratePJCTVMPDF").unbind("click").bind("click", function () {
                    var tab = tabs.tabs("option", "selected");

                    var url = "../ClientRegistration/Process_PJCTVM_Phase1";
                    if ($("#rdbPPES").length == 1) {
                        url = "../ClientRegistration/Process_PJCTVM_Phase2";
                    }

                    $.ajax({
                        type: "POST",
                        url: url,
                        data: $($("form")[0]).serialize(),
                        cache: false,
                        success: function () {
                            window.location = "../ClientRegistration/PJCTVMToFile";
                        }
                    });


                });
                break;
            }
        default: break;
    }


});