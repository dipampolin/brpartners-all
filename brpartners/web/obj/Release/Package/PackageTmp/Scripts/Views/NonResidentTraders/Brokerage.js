﻿/// <reference path="../../References.js" />

$(function () {
	handlers();
});

function handlers() {
	var context = $("#filterArea");

	$("#txtClientCode").blur(function () {
		var code = $(this).val();
		if (code != "") {
			getClientName($(this).val());
		}
		else
			$("#lblTraderName").html("");
	});

	$("#txtSettlementDate", context).blur(function () {
		$("#hdfSettlementDate", context).val($(this).val());
	});

	$("#txtSettlementEndDate", context).blur(function () {
		$("#hdfSettlementEndDate", context).val($(this).val());
	});

	$("#rblStatus", context).change(function () {
		checkDate($("#txtSettlementDate", context));
		checkDate($("#txtSettlementEndDate", context));

		if (this.value == "0")
			$("#endDate", context).hide();
		else
			$("#endDate", context).show();

		$("#txtSettlementDate", context).prev().html((this.value == "0") ? "Data de liquidação" : "Data inicial");

		$("#txtClientCode", context).val("");
		$("#lblTraderName", context).html("");

		$("#reportArea").html("");
		$("#reportArea").hide();
	});

	$(context).bind('keydown', function (event) {
		if (event.which == 13) {
			$("#btnSearch").click();
			return false;
		}
	});

	$("#btnSearch", context).click(function () {
	    commonHelper.ShowLoadingArea();
	    $("#reportArea").hide();
	    var startDate = $("#txtSettlementDate", context).val();
	    var endDate = $("#txtSettlementEndDate", context).val();
	    var code = $("#txtClientCode", context).val();

	    if (startDate != "") {
	        var status = $("#rblStatus:checked", context).val();
	        $("#reportArea").html("");
	        $("#reportArea").show();

	        if (code == "")
	            code = -1;

	        if (status == "1" && endDate != "")
	            commonHelper.LoadView("../NonResidentTraders/BrokerageReport?startDate=" + startDate + "&endDate=" + endDate + "&code=" + code + "&status=" + status, '#reportArea', null, function () {
	                $("#loadingArea").hide(); 
	            });

	        else
	            commonHelper.LoadView("../NonResidentTraders/BrokerageReport?startDate=" + startDate + "&code=" + code + "&status=" + status, '#reportArea', function () {
	                $("#loadingArea").hide(); 
	            });
	    }
	});


	$("#txtSettlementDate", context).blur(function () {
		checkDate(this);
	});

	$("#txtSettlementEndDate", context).blur(function () {
		checkDate(this);
	});

	$("#txtSettlementDate").setMask("99/99/9999");
	$("#txtSettlementEndDate").setMask("99/99/9999");
	$("#txtClientCode").setMask("999999999");

	$("#txtSettlementDate").datepicker();
	$("#txtSettlementEndDate").datepicker();
	$.datepicker.setDefaults($.datepicker.regional['pt-BR']);
	$("#txtSettlementDate").datepicker($.datepicker.regional['pt-BR']);
	$("#txtSettlementEndDate").datepicker($.datepicker.regional['pt-BR']);

}

function getClientName(value) {
	if (value != "") {
		$.ajax({
			type: "POST",
			url: "/NonResidentTraders/GetTraderName/",
			dataType: "json",
			data: { code: value },
			success: function (data) {
				$("#lblTraderName").html(String(data));
			}
		});
	}
	return false;
}

function checkDate(element) {
	var settlementDate = $(element);

	if (settlementDate.val() == "" || settlementDate.val().toString().length < 10) {
		settlementDate.val("");
		var date = new Date();
		var day = (date.getDate().toString().length == 1) ? "0" + date.getDate().toString() : date.getDate().toString();
		var month = ((date.getMonth() + 1).toString().length == 1) ? "0" + (date.getMonth() + 1).toString() : (date.getMonth() + 1).toString();
		var year = date.getFullYear();

		settlementDate.val(day + "/" + month + "/" + year);
	}
}

function checkDatePeriod(startDate, endDate) {
	if (startDate.length == 10 && endDate.length == 10) {
		var sDate = new Date(startDate.substring(6, 10),
                         startDate.substring(3, 5),
                         startDate.substring(0, 2));

		sDate.setMonth(sDate.getMonth() - 1);

		var eDate = new Date(endDate.substring(6, 10),
                      endDate.substring(3, 5),
                      endDate.substring(0, 2));

		eDate.setMonth(eDate.getMonth() - 1);

		if (datInicio > datFim)
			return false;
		else
			return true;
	}
	return true;
}

