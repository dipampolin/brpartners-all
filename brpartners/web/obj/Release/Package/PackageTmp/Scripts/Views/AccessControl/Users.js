﻿/// <reference path="../../References.js" />

var oTable;
var emptyDataMessage = "Sua pesquisa não encontrou resultados válidos. Tente novamente.";
var filterPage = 1;

var fnSearch = function (filterContext) {
    var ddlProfiles = $("#ddlProfiles", filterContext);
    var ddlGroupOfAssessors = $("#ddlGroupOfAssessors", filterContext);
    var txtUserName = $("#txtUserName", filterContext);
    var txtAssessor = $("#txtAssessor", filterContext);
    var chkNoProfile = $("#chkNoProfile", filterContext);
    var chkNoGroup = $("#chkNoGroup", filterContext);
    var feedbackID = $("#hdfChangedID").val();

    $.ajax({
        cache: false,
        async: false,
        type: "POST",
        url: "/AccessControl/LoadUsers",
        data: { ProfileId: ddlProfiles.val(), NoProfile: chkNoProfile.attr('checked'), NoGroup: chkNoGroup.attr('checked'), GroupId: ddlGroupOfAssessors.val(), AssessorID: txtAssessor.val(), UserName: txtUserName.val() },
        success: function (data) {
            if (data.Error == "") {
                obj = {};
                obj = {
                    //"aoColumns": [{ "bSortable": false }, null, null, null, null, null, { "bSortable": false }, { "bSortable": false }, { "bSortable": false }, { "bSortable": false }, { "bSortable": false }, { "bSortable": false }, { "bSortable": false }],
                    "aoColumns": [{ "bSortable": false }, null, null, null, null, { "bSortable": false }, { "bSortable": false }, { "bSortable": false }],
                    "bSort": true,
                    "bAutoWidth": false,
                    "bInfo": true,
                    "oLanguage": {
                        "sProcessing": "<img src='/img/loading.gif'>",
                        "sLengthMenu": "Exibir: _MENU_ por página",
                        "sZeroRecords": "Sua pesquisa não encontrou resultados válidos. Tente novamente.",
                        "oPaginate": {
                            "sPrevious": "Anteriores",
                            "sNext": "Próximos"
                        }
                    },
                    "sPaginationType": "full_numbers",
                    "sDom": 'rtip',
                    "fnDrawCallback": function () {
                        var totalRecords = this.fnSettings().fnRecordsDisplay();
                        var emptyArea = $("#emptyArea");
                        var tableArea = $("#tableArea");

                        var excelLink = $("#lnkUsersExcel");
                        var pdfLink = $("#lnkUsersPdf");

                        if (excelLink.length > 0 && pdfLink.length > 0) {
                            excelLink.attr("href", (totalRecords == 0) ? "#" : "../AccessControl/UsersToFile/?isPdf=false");
                            excelLink.attr("target", (totalRecords == 0) ? "_self" : "_blank");
                            pdfLink.attr("href", (totalRecords == 0) ? "#" : "../AccessControl/UsersToFile/?isPdf=true");
                            pdfLink.attr("target", (totalRecords == 0) ? "_self" : "_blank");
                        }

                        if (totalRecords == 0) {
                            emptyArea.html(emptyDataMessage);
                            emptyArea.show();
                            tableArea.hide();
                        }
                        else {
                            tableArea.show();
                            emptyArea.hide();

                            var totalPerPage = this.fnSettings()._iDisplayLength;

                            var totalPages = Math.ceil(totalRecords / totalPerPage);

                            var currentIndex = parseInt(this.fnSettings()._iDisplayStart);
                            var currentPage = totalPages > 0 ? Math.ceil(1 + (currentIndex / totalPerPage)) : 0;

                            $("#UsersList_info").html("Exibindo " + currentPage + " de " + totalPages);

                            if (totalRecords > 0 && totalPages > 1) {
                                $("#UsersList_info").css("display", "");
                                $("#UsersList_length").css("display", "");
                                $("#UsersList_paginate").css("display", "");
                            }
                            else {
                                $("#UsersList_info").css("display", "none");
                                $("#UsersList_length").css("display", "none");
                                $("#UsersList_paginate").css("display", "none");
                            }
                            $('select', '#UsersList_length').styleInputs();
                        }

                        // Adiciona a classe 'ultimo' à última tr no corpo da tabela
                        $('table tbody tr:last-child', tableArea).addClass('ultimo');

                        // Adiciona a classe 'ultimo' às últimas th e td no corpo da tabela
                        $('table tr td:last-child', tableArea).addClass('ultimo');
                        $('table tr th:last-child', tableArea).addClass('ultimo');
                    },
                    "bProcessing": false,
                    "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                        if ($("#checkAll").is(":checked")) {
                            $("#checkAll").attr("checked", false);
                        }
                        $('#UsersList_first').css("visibility", "hidden");
                        $('#UsersList_last').css("visibility", "hidden");
                    },
                    "bRetrieve": true,
                    "bServerSide": true,
                    "sAjaxSource": "../DataTables/AccessControl_Users",
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        $.ajax({
                            cache: false,
                            dataType: 'json',
                            type: "POST",
                            url: sSource,
                            data: aoData,
                            success: fnCallback
                        });
                    },
                    "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                        var userIdData = aData[8];
                        if (feedbackID != "" && feedbackID != "0" && userIdData.indexOf("linkId" + feedbackID + "_Check") > 0)
                            nRow.className = nRow.className + ' selected';

                        if (aData[8] == "1") {
                            nRow.className = nRow.className + ' bloqueado';
                            $(nRow).attr("title", "Usuário bloqueado");
                        }

                        return nRow;
                    }
                };

                oTable = $("#UsersList").dataTable(obj);
                oTable.fnClearTable();
                oTable.fnDraw();
            }
            else {
                $("#feedbackError").find("ul").html("");
                $("#feedbackError").find("ul").append("<li>" + data.Error + "</li>");
                $("#feedbackError").show();
            }
        }
    });

    return false;
}

$(function () {
    //$(document).ready(function () {

        $("#closeFeedback").click(function () {
            $("#usersFeedback").slideToggle();
        });

        var filterContext = $("#UsersFilterContent")

        $("#btnSearch", filterContext).click(function () {
            fnSearch(filterContext);
            
			if ($(filterContext).css('display') == "block")
				$(filterContext).slideToggle('fast');

            $('.ui-autocomplete').hide();
            return false;
        });

        setTimeout(function () {
            fnSearch(filterContext);
            return false;
        }, 500);

        $(filterContext).bind('keydown', function (event) {
            if (event.which == 13) {
                fnSearch(filterContext);
                $(filterContext).slideToggle('fast');
                $('.ui-autocomplete').hide();
                return false;
            }
        });

        $("#btnNewUser").click(function () {
            commonHelper.OpenModal("/AccessControl/EditUser/0", 450, ".content", "");
            $("#MsgBox").center();
            $('select').styleInputs();
        });

        $("#checkAll").click(function () {
            $("#UsersList tbody td input[type='checkbox']").attr("checked", $(this).attr("checked"));
        });
   // });
});

function EditUser(id) {
    commonHelper.OpenModal("/AccessControl/EditUser/?id=" + id, 450, ".content", "");
    $("#MsgBox").center();
}

function DeleteUser(id) {
    commonHelper.OpenModal("/AccessControl/ConfirmUserDelete/?userId=" + id, 399, ".content", "");
    $("#MsgBox").center();
}

function ResetPassword(userId) {
    commonHelper.OpenModal("/AccessControl/ConfirmResetPassword/?userId=" + userId, 399, ".content", "");
    $("#MsgBox").center();
}

function AssociateToGroup(id) {
    var emptyArea = $("#emptyArea");
    var errorMessage = "É necessário escolher um usuário.";

    if (id == 0) {
        var elements = $("#UsersList tbody td input[type='checkbox']");
        var count = 0;
        for (i = 0; i < elements.length; i++) {
            var element = $(elements[i]);
            if (element.is(":checked")) {
                count++;
            }
        }
    }
    else {
        count = 1;
    }
    if (count > 0) {
        emptyArea.hide();

        $.post("/AccessControl/AssociateUsersToGroupView/", id == 0 ? $("#userListForm").serialize() : { chkUser: id }, function (data) {
            Modal.Show("", "", 399);
            $(".content").html(data);
            $("#MsgBox").center();
        }, "html");
    }
    else {
        emptyArea.html(errorMessage);
        emptyArea.show();
    }

}

function AssociateToProfile(id) {
    var emptyArea = $("#emptyArea");
    var errorMessage = "É necessário escolher um usuário.";

    if (id == 0) {
        var elements = $("#UsersList tbody td input[type='checkbox']");
        var count = 0;
        for (i = 0; i < elements.length; i++) {
            var element = $(elements[i]);
            if (element.is(":checked")) {
                count++;
            }
        }
    }
    else {
        count = 1;
    }
    if (count > 0) {
        emptyArea.hide();
        $.post("/AccessControl/AssociateUsersToProfileView/", id == 0 ? $("#userListForm").serialize() : { chkUser: id }, function (data) {
            Modal.Show("", "", 399);
            $(".content").html(data);
            $("#MsgBox").center();
        }, "html");
    }
    else {
        emptyArea.html(errorMessage);
        emptyArea.show();
    }

}

function ViewHistory() {
    commonHelper.OpenModal("/AccessControl/UsersHistory", 671, ".content", "");
    $("#MsgBox").center();
}