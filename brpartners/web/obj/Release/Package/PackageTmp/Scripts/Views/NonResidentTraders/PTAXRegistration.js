﻿/// <reference path="../../References.js" />
var isValdationOk = false;

$(function () {
    handlers();
});

function handlers() {
    var context = $("#filterArea");
    var ptaxRegistrationArea = $("#ptaxRegistrationArea");

    $.mask.masks.ptax = { mask: '9999,99', type: 'reverse', defaultValue: '00000', autoTab : false };

    $("#txtPtax:input", "#ptaxRegistrationArea").setMask();

    $("#feedback", $("#conteudo")).hide();
    $("#feedbackError", $("#conteudo")).hide();

    $(context).bind('keydown', function (event) {
    	if (event.which == 13) {
    		$("#search").click();
    		return false;
    	}
    });

    $("#search", context).click(function () {
        commonHelper.ShowLoadingArea();
        searchPTAX($("#txtDate", context).val());
        $("#loadingArea").hide();
    });

    $("#includePtax", ptaxRegistrationArea).click(function () {
        ptaxRegistration($("#txtDate", context).val(), $("#txtPtax", ptaxRegistrationArea).val());
    });

    $("#txtDate", context).blur(function () {
        if (isValdationOk) {
            searchPTAX($("#txtDate", context).val());
        }
    });
    
    $("#txtDate", context).setMask("99/99/9999");

    $("#txtDate", context).datepicker();
    $.datepicker.setDefaults($.datepicker.regional['pt-BR']);
    $("#txtDate", context).datepicker($.datepicker.regional['pt-BR']);

    $("#txtDate", context).blur(function () {
        dateHelper.CheckDate(this, context);
    });
}

function searchPTAX(date) {
    $.ajax({
        async: false,
        type: "POST",
        url: "../NonResidentTraders/SearchSettlementDate",
        dataType: "json",
        data: { date: date },
        success: function (response) {
            if (response.Status == "True") {
                isValdationOk = true;
                var context = $("#ptaxRegistrationArea");
                $("#" + response.Messages[0], context).val(response.Messages[1]);
                switchAreas(true);
            }
            else {
                isValdationOk = false;
                var context = $("#conteudo");
                switchAreas(false);
                $("#" + response.Messages[0], context).html(response.Messages[1]);
            }
        }
    });
}

function ptaxRegistration(date, value) {
	var context = $("#conteudo");
	var valueField = $("#txtPtax", context);

	if (value != "0,00") {
		valueField.removeClass("error");
		$.ajax({
			async: false,
			type: "POST",
			url: "../NonResidentTraders/PTAXRegistration",
			dataType: "json",
			data: { date: date, value: value },
			success: function (response) {
				if (response.toString().indexOf("true") >= 0) {
					$("#feedback", context).show();
					$("#ptaxRegistrationArea", context).hide();
					$("#feedbackError", context).hide();
				}
				else {
					$("#feedback", context).hide();
					var feedError = $("#feedbackError", context);
					feedError.html(commonHelper.FormatErrorMessage("Não foi possível salvar ptax.", false));
					valueField.addClass("error");
					feedError.show();
				}
			}
		});
	}
	else {
		$("#feedback", context).hide();
		var feedError = $("#feedbackError", context);
		feedError.html(commonHelper.FormatErrorMessage("Ptax inválido.", false));
		valueField.addClass("error");
		feedError.show();
	}
}

function switchAreas(boolean) {
    var context = $("#conteudo");

    if (boolean) {
        $("#messageArea", context).hide();
        $("#ptaxRegistrationArea", context).show();
    } else {
        $("#messageArea", context).show();
        $("#ptaxRegistrationArea", context).hide();
    }
}