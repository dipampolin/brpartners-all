﻿$(function () {

    var context = $(".solicitar-subscricao")[0];

    /*$("#uploadProspect").unbind("click").bind("click", function () {

    if ($(".prospecto").is(":visible") == false) {
    $(".prospecto").show().append($("<label>Prospecto</label><input type='file' name='txtProspect' id='txtProspect' />").clone());
    }
    else {
    $(".prospecto").hide().html('');
    }

    });*/

    var fnAddMasks = function () {

        $.mask.masks.Percent = { mask: '9999999999,999', type: 'reverse', defaultValue: '00000000000' };
        $.mask.masks.Value = { mask: '99,999.999.9', type: 'reverse', defaultValue: '000' };

        $('#txtRightValue', context).setMask();
        $('#txtRightPercentual', context).setMask();

        $("#txtInitialDate, #txtCOMDate, #txtBrokerDate, #txtStockExchangeDate, #txtCompanyDate", context).setMask("99/99/9999").datepicker().datepicker($.datepicker.regional['pt-BR']);

        $("#txtBrokerHour").setMask("time");

        $("#txtDescription").maxlength({ limit: 500 });

        $("#txtBodyMail").maxlength({ limit: 1000 });

        $("#txtISINStock", context).css("text-transform", "uppercase");
        $("#txtISINSubscription", context).css("text-transform", "uppercase");
    };

    fnAddMasks();

    var validateSubscription = function () {
        var request = true;

        $("#requestFeedbackError").hide().find("ul").html('');

        var ul = $("div#requestFeedbackError > ul");
        if ($("#txtCompany", context).val() == null || $("#txtCompany", context).val() == undefined || $("#txtCompany", context).val() == "") {
            ul.append($("<li>Nome da Empresa não informada.</li>").clone());
            request = false;
        }

        var stockISIN = $("#txtISINStock", context).val();
        if (stockISIN == null || stockISIN == undefined || stockISIN == "") {
            ul.append($("<li>ISIN do papel não informado.</li>").clone());
            request = false;
        }
        else {
            if (!validateHelper.ValidateISIN(stockISIN)) {
                ul.append($("<li>ISIN do papel com formato inválido.</li>").clone());
                request = false;
            }
        }

        var subscriptionISIN = $("#txtISINSubscription", context).val();
        if (subscriptionISIN == null || subscriptionISIN == undefined || subscriptionISIN == "") {
            ul.append($("<li>ISIN da subscrição não informado.</li>").clone());
            request = false;
        }
        else {
            if (!validateHelper.ValidateISIN(subscriptionISIN)) {
                ul.append($("<li>ISIN da subscrição com formato inválido.</li>").clone());
                request = false;
            }
        }

        if ($("#txtRightPercentual", context).val() == null || $("#txtRightPercentual", context).val() == undefined || $("#txtRightPercentual", context).val() == "") {
            ul.append($("<li>% do direito não informado.</li>").clone());
            request = false;
        }

        if ($("#txtRightValue", context).val() == null || $("#txtRightValue", context).val() == undefined || $("#txtRightValue", context).val() == "") {
            ul.append($("<li>Valor do direito não informado.</li>").clone());
            request = false;
        }

        if ($("#txtDescription", context).val() == null || $("#txtDescription", context).val() == undefined || $("#txtDescription", context).val() == "") {
            ul.append($("<li>Descrição não informada.</li>").clone());
            request = false;
        }

        if ($("#txtSubjectMail", context).val() == null || $("#txtSubjectMail", context).val() == undefined || $("#txtSubjectMail", context).val() == "") {
            ul.append($("<li>Assunto do e-mail não informado.</li>").clone());
            request = false;
        }

        if ($("#txtBodyMail", context).val() == null || $("#txtBodyMail", context).val() == undefined || $("#txtBodyMail", context).val() == "") {
            ul.append($("<li>Corpo do e-mail não informado.</li>").clone());
            request = false;
        }

        if ($("#txtBrokerHour", context).val() == null || $("#txtBrokerHour", context).val() == undefined || $("#txtBrokerHour", context).val() == "") {
            ul.append($("<li>Horário corretora não informado.</li>").clone());
            request = false;
        }

        var initialDate = Date.parse($("#txtInitialDate", context).val());
        var comDate = Date.parse($("#txtCOMDate", context).val());
        var brokerDate = Date.parse($("#txtBrokerDate", context).val());
        var stockExchangeDate = Date.parse($("#txtStockExchangeDate", context).val());
        var companyDate = Date.parse($("#txtCompanyDate", context).val());

        if (initialDate == null) {
            ul.append($("<li>Data de início não informada.</li>").clone());
            request = false;
        }

        if (comDate == null) {
            ul.append($("<li>Data COM não informada.</li>").clone());
            request = false;
        }

        if (brokerDate == null) {
            ul.append($("<li>Data Corretora não informada.</li>").clone());
            request = false;
        }

        if (stockExchangeDate == null) {
            ul.append($("<li>Data Bolsa não informada.</li>").clone());
            request = false;
        }

        if (companyDate == null) {
            ul.append($("<li>Data Empresa não informada.</li>").clone());
            request = false;
        }

        /*console.log(initialDate);
        console.log(comDate);
        console.log(brokerDate);
        console.log(stockExchangeDate);
        console.log(companyDate);

        if (!(initialDate < comDate < brokerDate < stockExchangeDate < companyDate))
        {
        ul.append($("<li>A ordem de datas está incorreta.</li>").clone());
        request = false;
        }*/

        /*if ($("#txtProspect").val() == "") {
        ul.append($("<li>Prospecto não informado.</li>").clone());
        request = false;
        }*/

        if (request == false) {
            $("#requestFeedbackError").show().focus();
        }
        else {
            $("#requestFeedbackError").hide();
            $("#requestFeedbackError > ul").html('');
        }

        return request;
    };

    var fnSubmit = function () {
        //$("#hdfClientName").val($("#lblClientName").text());

        $("#frmSubscriptionRequest").ajaxForm({
            beforeSend: function () {
                $("#divSubscriptionFeedback").hide().find("span").html('');
                $("#feedbackError").hide().find("span").html('');
            },
            success: function (data) {
                $("#loadingArea").hide();

                if (data.Result) {
                    selector = "#divSubscriptionFeedback";

                    oTableSubscription.fnDraw(true);

                    $(selector).show().find("span").append(data.Message);

                    SubscriptionMethods.SubscriptionPendents();

                    Modal.Close();
                }
                else {

                    selector = "#feedbackError";

                    if (data.Message.indexOf("Arquivo inválido") > -1) {
                        $("#requestFeedbackError").show().focus().find('ul').html('').append(
                            $("<li>" + data.Message + "</li>").clone()
                        );
                    }
                    else {

                        $(selector).show().find("span").append(data.Message);
                        Modal.Close();
                    }

                }

            }
        });

        if (validateSubscription()) {
            $("#frmSubscriptionRequest").submit();
        }

    };

    var fnDisableSubmit = function () {
        $("#btnSubmit").unbind("click");
    };

    //if ($("#hdfEditRequestId").length > 0) {
    $("#btnSubmit").unbind("click");
    $("#btnSubmit").bind("click", fnSubmit);
    //}

    $("body").unbind("keydown");
    $("body").bind("keydown", function (event) {
        if (event.which == 13 && $("#hdfEditRequestId").length > 0 && ($("*:focus").attr("id") != "txtDescription" || $("*:focus").attr("id") != "txtBodyMail")) {
            fnSubmit();
        }
    });
});