﻿/// <reference path="../../References.js" />

$(function () {

    $(document).ready(function () {

        var adminClientsContext = $("#divAdminClients");
        $("#txtClientName", adminClientsContext).setMask('999999999');
        $("#txtInitialValue", adminClientsContext).setMask('signed-decimal');

        $("#txtStartDate", adminClientsContext).datepicker($.datepicker.regional['pt-BR']).setMask("99/99/9999");

        if ($("#hdfClientCode", adminClientsContext).val() != "0") { $("#txtClientName", adminClientsContext).attr("disabled", "disabled"); }

        $("#lnkSave", adminClientsContext).click(function () {

            SelectOperatorCodeList();

            var feedback = ValidateFields(adminClientsContext);
            if (feedback != "") {
                $("#feedbackRequestError", adminClientsContext).html(feedback);
                $("#feedbackRequestError", adminClientsContext).show();
                return false;
            }

            $("#frmEditAdminClients").submit();
        });

        $("#lnkAddOperator", adminClientsContext).click(function () {
            $("#feedbackError", adminClientsContext).hide();
            var operatorName = $("#txtOperatorName", "#incluirNovoOperador").val();
            var operatorCode = $("#hdfSelectedOperator", "#incluirNovoOperador").val();

            if (operatorCode == "") {
                return false;
            }

            var data = "";
            data += $("#frmEditAdminClients").serialize();
            if (data != "") data += "&";
            data += "id=" + operatorCode + "&name=" + operatorName;

            $.post("/OnlinePortfolio/IncludeOperatorInList/", data, function (data) {
                if (data.error != "") {
                    $("#feedbackError", context).html(data.error);
                    $("#feedbackError", context).show();
                    return false;
                }
                else {
                    $("#listaOperadores").append("<li/>");

                    $("#listaOperadores li:last").attr("id", "operador" + data.user.ID);
                    $("#listaOperadores li:last").append("<a onclick=\"DeleteOperatorFromList(" + data.user.ID + ");return false;\">excluir</a>");
                    $("#listaOperadores li:last").append("<input type=hidden />");
                    $("#listaOperadores li:last").append(data.user.Name);

                    $("#listaOperadores li a:last").attr("class", "ico-excluir");

                    $("#listaOperadores li input:last").attr("value", data.user.ID);
                    $("#listaOperadores li input:last").attr("name", "operatorCode");
                    $("#listaOperadores li input:last").attr("id", "operatorCode" + data.user.ID);

                    $("#txtOperatorName", "#incluirNovoOperador").val("");
                    $("#hdfSelectedOperator", "#incluirNovoOperador").val("");
                    $('#incluirNovoUsuario', adminClientsContext).slideToggle();
                }
            }, "json");
        });

        $("#txtOperatorName", "#incluirNovoOperador").autocomplete({
            source: function (request, response) {
                var data = "";

                data += $("#frmEditAdminClients").serialize();
                if (data != "") data += "&";
                data += "term=" + request.term;

                $.ajax({
                    url: "/OnlinePortfolio/OperatorSuggest",
                    type: "POST",
                    dataType: "json",
                    data: data,
                    success: function (data) {
                        $("#hdfSelectedOperator", "#incluirNovoOperador").val("");
                        response($.map(data, function (item) {
                            return {
                                label: item.Name,
                                value: item.Name,
                                id: item.ID
                            }
                        }));
                    }
                });
            },
            minLength: 1,
            select: function (event, ui) {
                $("#hdfSelectedOperator", "#incluirNovoOperador").val(ui.item.id);
            }
        });


        $("#txtClientName", adminClientsContext).blur(function () {

            var term = $(this).val();

            $("#hdfClientCode", adminClientsContext).val("0");

            if (term != "") {
                $.ajax({
                    type: "POST",
                    url: "/OnlinePortfolio/LoadSinacorClient/",
                    dataType: "json",
                    data: { term: term },
                    success: function (data) {
                        if (data != "Cliente não encontrado" && data != "Cliente já foi cadastrado" && data != "") {
                            var client = data.split("-");
                            $("#lblClientName", adminClientsContext).html(client[1]);
                            $("#hdfClientCode", adminClientsContext).val(client[0]);
                        }
                        else if (data == "Cliente não encontrado")
                            $("#lblClientName", adminClientsContext).html("Cliente não encontrado.");
                        else if (data == "Cliente já foi cadastrado")
                            $("#lblClientName", adminClientsContext).html("Cliente já foi cadastrado.");
                        else
                            $("#lblClientName", adminClientsContext).html("Ocorreu um erro ao consultar cliente.");
                    }
                });
            }
            else {
                $("#lblClientName", adminClientsContext).html("");
            }

        });

        $('.ico-incluir').click(function () {
            $('#incluirNovoUsuario', adminClientsContext).slideToggle();
        });

    });

});

function DeleteOperatorFromList(id) {
    var operatorItem = $("#operador" + id, "#listaOperadores");
    operatorItem.remove();
}

function SelectOperatorCodeList() {
    var operatorListContext = $("#listaOperadores");

    $("#hdfOperators").val("");

    $("input", operatorListContext).each(function () {
        $("#hdfOperators").val($("#hdfOperators").val() + ";" + $(this).val());
    });
}

function ValidateFields(validateContext) {
    var feedback = "";

    if ($("#hdfClientCode", validateContext).val() == "0") {
        feedback += "<li>É necessário preencher um cliente.</li>";
    }

    if ($("#hdfOperators", validateContext).val() == "") {
        feedback += "<li>É necessário escolher um operador.</li>";
    }

    var date = $("#txtStartDate", validateContext).val();
    if (date == "") {
        feedback += "<li>É necessário preencher a data.</li>";
    } else if (!DateValidate(date)) {
        feedback += "<li>Data inválida.</li>";
    }

    if (!$("#rdbAttachType", validateContext).is(':checked')) {
        feedback += "<li>É necessário escolher um tipo de envio.</li>";
    }

    if (feedback.length > 0) {
        feedback = "<ul>" + feedback + "</ul>";
    }

    return feedback;
}

function DateValidate(date) {
    var reDate = /^((0[1-9]|[12]\d)\/(0[1-9]|1[0-2])|30\/(0[13-9]|1[0-2])|31\/(0[13578]|1[02]))\/\d{4}$/;
    if (reDate.test(date) || (date == '')) {
        return true
    } else {
        return false;
    }
}