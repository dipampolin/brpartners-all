﻿/// <reference path="../../References.js" />



$(function () {
    $(document).ready(function () {

        var context = $("#frmEditUser");

        var disabled = $("#hdfUserID").val() == 0 ? "" : "disabled";
        $("#txtLogin").attr("disabled", disabled);
        //$("#txtUserEmail").attr("disabled", disabled);

        $("#txtUserEmail", context).blur(function () {
            $("#feedbackError", context).html("");
            $("#feedbackError", context).hide();
            $("#emptyArea", context).hide();

            var emailValue = $(this).val();
            var feedback = "";

            feedback = ValidateUserEmail(emailValue, feedback);

            if (feedback != "undefined" && feedback != "") {
                $("#feedbackError", context).html(commonHelper.FormatErrorMessage(feedback, true));
                $("#feedbackError", context).show();
            }

            /* if (emailValue != "" && (feedback == "undefined" || feedback == "")) {
                 $.ajax({
                     type: "POST",
                     url: "/AccessControl/ValidateNewUserInLDAPByEmail/",
                     dataType: "json",
                     data: { email: emailValue },
                     success: function (data) {
                         $("#hasADInfo", context).val(data);
                         if (data != "true") {
                             //$("#Step2", context).show();
                             $("#emptyArea", context).show();
                             //$("#txtLogin", context).focus();
                         }
                         else {
                             $("#feedbackError", context).html(commonHelper.FormatErrorMessage("<li>O E-mail usado já foi cadastrado no Active Directory.</li>", true));
                             $("#feedbackError", context).show();
                             //$("#Step2", context).hide();
                             $("#emptyArea", context).hide();
                             //$("#txtLogin", context).val("");
                             //$("#txtLogin", context).val("");
                             //$("#txtUserEmail").val("");
                         }
                     }
                 });
             }
             else {
                 $("#feedbackError", context).html(commonHelper.FormatErrorMessage(feedback, true));
                 $("#feedbackError", context).show();
             }*/
        });

        $(".rdbType").click(function () {

            if ($(this).val() == "E") {

                $("#cdBoldaTipo2").show();

            } else {

                $("#cdBoldaTipo2").hide();
            }

        });


        var t = $("#ddlProfile option:selected").text();
        t = t.toLowerCase();

        if (t.indexOf('com') >= 0 && t.indexOf('suitability') >= 0) {
            $("#divSendSuitability").show();
        }
        else {
            $("#divSendSuitability").hide();
        }

        $("#ddlProfile", context).change(function () {

            console.log($("#rdbSendSuitability:checked", context).val());

            var type = $("#ddlProfile option:selected").text();
            type = type.toLowerCase();

            if (type.indexOf('com') >= 0 && type.indexOf('suitability') >= 0) {
                $("#divSendSuitability").show();
            }
            else {
                var r = $("#rdbSendSuitability", context)[0];
                $(r).attr("checked", true);

                $("#divSendSuitability").hide();
            }

            console.log($("#rdbSendSuitability:checked", context).val());
        });


        $("#lnkSave", context).click(function () {
            var userId = $("#hdfUserID", context).val();
            var userName = $("#txtUserName", context).val();
            var userEmail = $("#txtUserEmail", context).val();
            var assessorID = $("#txtAssessorID", context).val();
            var profileID = $("#ddlProfile", context).val();
            var type = $("#rdbType:checked", context).val();
            var hasInfo = $("#hasADInfo", context).val();
            var login = $("#txtLogin", context).val();
            var cdCliente = $("#tbCdBolsa", context).val();
            var bloqueado = $("#blocked").attr('checked') == null ? true : $("#blocked").attr('checked');
            var investorKind = $("#ddlInvestorKind", context).val();
            var sendSuitabilityEmail = $("#rdbSendSuitability:checked", context).val() === "Sim" ? 1 : 0;

            console.log(investorKind);

            var cpf = $("#tbCPFCNPJ", context).val().replace(/[^\d]+/g, '');

            var feedback = ValidateForm(context);
            if (feedback == "") {

                $.ajax({
                    url: "/AccessControl/UpdateUser",
                    type: "POST",
                    dataType: "json",
                    data: { userId: userId, userName: userName, userEmail: userEmail, assessorID: assessorID, profileID: profileID, type: type, hasADInfo: hasInfo, login: login, cdCliente: cdCliente, bloqueado: bloqueado, cpfcnpj: cpf, investorKind: investorKind, sendSuitabilityEmail: sendSuitabilityEmail },
                    success: function (data) {
                        if (data.error == "") {

                            $("#UsersFilterContent #btnSearch").click();

                            $("#usersFeedback").find("span").html(userId == 0 ? "Usuário cadastrado com sucesso." : "Usuário editado com sucesso.");
                            $("#usersFeedback").show();
                            Modal.Close();
                        }
                        else {
                            $("#feedbackError", context).html(commonHelper.FormatErrorMessage(data.error, true));
                            $("#feedbackError", context).show();
                            return false;
                        }
                    },
                    error: function () {
                        $("#feedbackError", context).html(commonHelper.FormatErrorMessage("Ocorreu um erro na solicitação. Por favor, tente mais tarde.", false));
                        $("#feedbackError", context).show();
                    }
                });
            }
            else {
                $("#feedbackError", context).html(commonHelper.FormatErrorMessage(feedback, true));
                $("#feedbackError", context).show();
                return false;
            }
        });


    });
});

function SetNumber(num) {
    var nums = "0123456789"
    var val;

    for (var i = 0; i < num.value.length; i++) {
        val = num.value.substring(i, i + 1)
        if (nums.indexOf(val) == -1) {
            num.value = num.value.substring(0, i);
            break;
        }
    }
}

function ValidateForm(context) {
    
    var feedback = "";



    var login = $("#txtLogin", context).val();
    if (login == "" || login.length < 3) {
        feedback += "<li>O login é obrigatório e deve conter no minimo 3 caracteres.</li>";
    }

    var userName = $("#txtUserName", context).val();
    if (userName == "" || userName.length < 3) {
        feedback += "<li>O nome é obrigatório e deve conter no minimo 3 caracteres.</li>";
    }

    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

    var userEmail = $("#txtUserEmail", context).val();
    if (userEmail == "") {
        feedback += "<li>O email é obrigatório.</li>";
    }
    else if (reg.test(userEmail) == false) {
        feedback += "<li>Email inválido.</li>";
    }

    var assessorID = $("#txtAssessorID", context).val();
    if (assessorID != "") {
        var valueAssessorID = assessorID.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
        var intRegex = /^\d+$/;
        if (!intRegex.test(valueAssessorID)) {
            feedback += "Campo de Código de Assessor deve ser numérico.";
        }
    }

    if ($(".rdbType:checked", context).val() == "E") {

        var cdBolsa = $("#tbCdBolsa", context).val();
        var cpf = $("#tbCPFCNPJ", context).val();

        cpf = cpf.replace(/[^\d]+/g, '');

        if (cpf == '') {
            feedback += "<li>Campo CPF/CNPJ é obrigatório se o usuário for externo.</li>";
        }
        else if (cpf.length <= 11) {
           // console.log(cpf);
            cpf = pad(cpf, 11);
           // console.log(cpf);
            var cpf_ok = validateCPF(cpf);
            if (!cpf_ok)
                feedback += "<li>Campo CPF/CNPJ não é um CPF válido.</li>";
        } else {
            cpf = cpf = pad(cpf, 14);
            var cnpj_ok = validarCNPJ(cpf);
            if (!cnpj_ok)
                feedback += "<li>Campo CPF/CNPJ não é um CNPJ válido.</li>";
        }
    }


    var profile = $("#ddlProfile", context).val();
    if (profile == 0)
        feedback += "Selecione um perfil de acesso.";

    return feedback;
}

function pad(n, width, z) {
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

function ValidateUserLogin(login) {
    return !(login == "" || login.length < 3 || login.length > 12);
}

function ValidateUserPassword(password) {
    return !(password == "" || password.length < 8);
}

function padleft(val, ch, num) {
    var re = new RegExp(".{" + num + "}$");
    var pad = "";
    if (!ch) ch = " ";
    do {
        pad += ch;
    } while (pad.length < num);
    return re.exec(pad + val)[0];
}

function ValidateUserEmail(userEmail, feedback) {
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if (userEmail == "") {
        feedback += "<li>O email é obrigatório.</li>";
    }
    else {
        if (reg.test(userEmail) == false)
            feedback += "<li>Email inválido.</li>";
    }
    return feedback;
}

function validateCPF(v) {
    var i;
    var strCPF = v.toString().replace(/[^\d]+/gi, '');
    var Soma;
    var Resto;
    Soma = 0;
    if (strCPF == "00000000000" ||
        strCPF == "11111111111" ||
        strCPF == "22222222222" ||
        strCPF == "33333333333" ||
        strCPF == "44444444444" ||
        strCPF == "55555555555" ||
        strCPF == "66666666666" ||
        strCPF == "77777777777" ||
        strCPF == "88888888888" ||
        strCPF == "99999999999")
        return false;
    for (i = 1; i <= 9; i++)
        Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i);
    Resto = (Soma * 10) % 11;
    if ((Resto == 10) || (Resto == 11))
        Resto = 0;
    if (Resto != parseInt(strCPF.substring(9, 10)))
        return false;
    Soma = 0;
    for (i = 1; i <= 10; i++)
        Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (12 - i);
    Resto = (Soma * 10) % 11;
    if ((Resto == 10) || (Resto == 11))
        Resto = 0;
    if (Resto != parseInt(strCPF.substring(10, 11)))
        return false;
    return true;
}
function validarCNPJ(cnpj) {

    cnpj = cnpj.replace(/[^\d]+/g, '');

    if (cnpj == '') return false;

    if (cnpj.length != 14)
        return false;

    // Elimina CNPJs invalidos conhecidos
    if (cnpj == "00000000000000" ||
        cnpj == "11111111111111" ||
        cnpj == "22222222222222" ||
        cnpj == "33333333333333" ||
        cnpj == "44444444444444" ||
        cnpj == "55555555555555" ||
        cnpj == "66666666666666" ||
        cnpj == "77777777777777" ||
        cnpj == "88888888888888" ||
        cnpj == "99999999999999")
        return false;

    // Valida DVs
    var tamanho = cnpj.length - 2
    var numeros = cnpj.substring(0, tamanho);
    var digitos = cnpj.substring(tamanho);
    var soma = 0;
    var pos = tamanho - 7;
    for (var i = tamanho; i >= 1; i--) {
        soma += numeros.charAt(tamanho - i) * pos--;
        if (pos < 2)
            pos = 9;
    }
    var resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(0))
        return false;

    tamanho = tamanho + 1;
    numeros = cnpj.substring(0, tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (var i = tamanho; i >= 1; i--) {
        soma += numeros.charAt(tamanho - i) * pos--;
        if (pos < 2)
            pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(1))
        return false;

    return true;

}