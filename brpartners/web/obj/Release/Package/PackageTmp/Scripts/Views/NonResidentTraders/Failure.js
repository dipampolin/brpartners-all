﻿/// <reference path="../../References.js" />

var oTable;
var emptyDataMessage = "Sua pesquisa não encontrou resultados válidos. Tente novamente.";

$(function () {
    handlers();
});

function handlers() {

    var context = $("#filterArea");

    $("#lnkSearchAll").click(function () {
        $("#txtClientCode").val("");
        $("#lblTraderName").html("");
    });

	$(context).bind('keydown', function (event) {
    	if (event.which == 13) {
    		$("#btnSearch").click();
    		return false;
    	}
    });

    $("#btnSearch, #lnkSearchAll", context).click(function () {
        commonHelper.ShowLoadingArea();
        var tradingDate = $("#txtTradingDate", context).val();
        var code = $("#txtClientCode", context).val();
        $("#lblTradingDate", $("#tableArea")).html(tradingDate);
        searchRequest(tradingDate, code);
    });

    $("#txtTradingDate", context).setMask("99/99/9999");

    $("#txtTradingDate").datepicker();
    $.datepicker.setDefaults($.datepicker.regional['pt-BR']);
    $("#txtTradingDate").datepicker($.datepicker.regional['pt-BR']);

    $("#txtTradingDate", context).blur(function () {
        dateHelper.CheckDate($(this), "filterArea");
    });

    $("#txtClientCode").blur(function () {
        commomDataHelper.GetClientName($(this).val(), "lblTraderName", $("#filterArea"));
    });
}

function searchRequest(tradingDate, clientCode) {
    $.ajax({
        async: false,
        url: "../NonResidentTraders/Failure",
        type: "POST",
        data: { "txtTradingDate": tradingDate, "txtClientCode": clientCode },
        dataType: "json",
        success: function (data) {

            emptyDataMessage = tradingDate != "" ? "Sua pesquisa para " + tradingDate + " não encontrou resultados válidos. Tente novamente." : "Sua pesquisa não encontrou resultados válidos. Tente novamente.";

            if (data.Status == "True") {
                $("#vlnTradingDate", $("#filterArea")).html("");
                var tableId = "failureTable";
                obj = {
                    "bSort": true,
                    "bAutoWidth": false,
                    "bInfo": true,
                    "bSort": true,
                    "aoColumns": [
						null,
						{ "sClass": "centralizado" },
						{ "sClass": "centralizado" },
						{ "sClass": "direita" },
						{ "sClass": "direita" },
						{ "sClass": "direita" },
						{ "sClass": "direita" },
						{ "sClass": "direita" },
						{ "sClass": "centralizado" },
						null
					],
                    "oLanguage": {
                        "sProcessing": "Carregando...",
                        "sLengthMenu": "<span>Exibir:</span> _MENU_ <span>por página</span>",
                        "sZeroRecords": emptyDataMessage,
                        "oPaginate": {
                            "sPrevious": "Anteriores",
                            "sNext": "Próximos"
                        }
                    },
                    "sPaginationType": "full_numbers",
                    "sDom": 'rtipl',
                    "fnDrawCallback": function () {
                        var totalRecords = this.fnSettings().fnRecordsDisplay();

                        if (totalRecords == 0) {
                            $("#emptyArea").html(emptyDataMessage);
                            $("#emptyArea").show();
                            $("#tableArea").hide();
                        }
                        else {
                            $("#tableArea").show();
                            $("#emptyArea").hide();

                            var totalPerPage = this.fnSettings()._iDisplayLength;
                            var totalPages = Math.ceil(totalRecords / totalPerPage);

                            var currentIndex = parseInt(this.fnSettings()._iDisplayStart);
                            var currentPage = totalPages > 0 ? Math.ceil(1 + (currentIndex / totalPerPage)) : 0;

                            $("#" + tableId + "_info").html("Exibindo " + currentPage + " de " + totalPages);

                            if (totalRecords > 0 && totalPages > 1) {
                                $("#" + tableId + "_info").css("display", "");
                                $("#" + tableId + "_length").css("display", "");
                                $("#" + tableId + "_paginate").css("display", "");
                            }
                            else {
                                $("#" + tableId + "_info").css("display", "none");
                                $("#" + tableId + "_length").css("display", "none");
                                $("#" + tableId + "_paginate").css("display", "none");
                            }

                            var tableContext = $("#tableArea");
                            $("[id^=txtQuantity]", tableContext).setMask("integer");
                            $("[id^=txtValue]", tableContext).setMask("decimal");
                        }
                        // Adiciona a classe 'ultimo' à última tr no corpo da tabela
                        $('table tbody tr:last-child').addClass('ultimo');

                        // Adiciona a classe 'ultimo' às últimas th e td no corpo da tabela
                        $('table tr td:last-child').addClass('ultimo');
                        $('table tr th:last-child').addClass('ultimo');
                        $("#loadingArea").hide();
                    },
                    "bProcessing": false,
                    "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                        $("#" + tableId + "_first").css("visibility", "hidden");
                        $("#" + tableId + "_last").css("visibility", "hidden");
                    },
                    "bRetrieve": true,
                    "bServerSide": true,
                    "sAjaxSource": "../DataTables/NonResidentTraders_Failure",
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        $.ajax({
                            cache: false,
                            dataType: 'json',
                            type: "POST",
                            url: sSource,
                            data: aoData,
                            success: fnCallback
                        });
                    }
                };
                oTable = $("#failureTable").dataTable(obj);
                oTable.fnClearTable();
                oTable.fnDraw();
                $("#failureTable_length").hide();

//                if (oTable != null && oTable.length > 0 && oTable[0].rows.length > 3)
//                    $("#tableArea").show();
//                    $("#tableArea").show();
//                    $("#reportArea").hide();
            }
            else {
                for (var i = 0; i < data.Messages.length - 1; i++) {
                    $("#" + data.Messages[i], $("#filterArea")).html(data.Messages[i + 1]);
                }
            }
        }
    });
}

function ChangeFailureQuantity(id, link) {

    var context = $("#tableArea");
    CloseEdits();

    $("#txtQuantity" + id, context).val(link.innerHTML);
    var divValue = $("#divFailureQuantityEdit" + id, context);

    if (divValue.css("display") != "none")
        divValue.hide();
    else
        divValue.show();
}

function ChangeFailureValue(id, link) {

    var context = $("#tableArea");
    CloseEdits();

    $("#txtValue" + id, context).val(link.innerHTML);
    var divQty = $("#divFailureValueEdit" + id, context);

    if (divQty.css("display") != "none")
        divQty.hide();
    else
        divQty.show();
}

function CloseEdits() {
    $("[id^=divFailureQuantityEdit]").hide();
    $("[id^=divFailureValueEdit]").hide();
}

function UpdateFailure(keys, type, id) {
    if (keys != "" && type != "") {

        var context = $("#tableArea");

        var newValue = type == 'q' ? $("#txtQuantity" + id, context).val().replace(/\./g, '') : $("#txtValue" + id, context).val().replace(/\./g, '');

        if (newValue != "" && newValue != "0" && newValue != "0,00") {
            $.ajax({
                type: "POST",
                url: "/NonResidentTraders/UpdateFailure/",
                dataType: "json",
                data: { keys: keys, type: type, newValue: newValue },
                success: function (data) {
                    if (data.result == "true") {
                        if (type == 'q') {
                            $("#txtQuantity" + id, context).val(newValue);
                            $("#lnkQty" + id, context).html(commonHelper.FormatNumber(parseInt(newValue.replace(/\./g, '').replace(/\,/g, '.')), 0));
                            $("#divFailureQuantityEdit" + id, context).hide();
                        }
                        else {
                            $("#txtValue" + id, context).val(newValue);
                            $("#lnkVl" + id, context).html(commonHelper.FormatNumber(parseFloat(newValue.replace(/\./g, '').replace(/\,/g, '.')), 2));
                            $("#divFailureValueEdit" + id, context).hide();

                        }
                        $("#updateDate" + id, context).html($("#hdfUpdateDate").val());
                        $("#username" + id, context).html($("#hdfUsername").val());
                    }
                    else {
                        alert("Não foi possível salvar " + (type == "q" ? "quantidade." : "valor."));
                    }
                }
            });
        }
        else {
            alert(type == "q" ? "Quantidade inválida" : "Data inválida");
        }
    }
    return false;
}