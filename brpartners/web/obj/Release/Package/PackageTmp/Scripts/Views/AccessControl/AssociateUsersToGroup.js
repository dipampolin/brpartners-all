﻿/// <reference path="../../References.js" />

var context = "#frmAssociateUserToGroup";
var queryStringToObject = function (q) {
    var e,
        a = /\+/g,  // Regex for replacing addition symbol with a space
        r = /([^&=]+)=?([^&]*)/g,
        d = function (s) { return decodeURIComponent(s.replace(a, " ")); },
        urlParams = {};

    while (e = r.exec(q))
        urlParams[d(e[1])] = d(e[2]);
    return urlParams;
}

function DeleteUserFromList(id) {
    var totalUsuarios = $("li", "#lista-usuarios").length;
   if ( totalUsuarios > 1) {
       var userItem = $("#usuario" + id, "#lista-usuarios");
       userItem.remove();
        
    }   
}
$(function () {

    //    $('#lista-usuarios li').sortElements(function (a, b) {
    //        return $(a).text() > $(b).text() ? 1 : -1;
    //    });

    $("#incluirUsuario").click(function () {
        $("#feedbackError", context).hide();
        var userName = $("#txtUserName", "#incluirNovoUsuario").val();
        var userId = $("#hdfUser", "#incluirNovoUsuario").val();

        var data = "";

        data += $(context).serialize();
        if (data != "") data += "&";
        data += "id=" + userId + "&name=" + userName;



        $.post("/AccessControl/IncludeUserInList/", data, function (data) {
            if (data.error != "") {
                $("#feedbackError", context).html(commonHelper.FormatErrorMessage(data.error, false));
                $("#feedbackError", context).show();
                return false;
            }
            else {

                var nextIndex = $("#lista-usuarios li").length;


                $("#lista-usuarios").append("<li/>");

                $("#lista-usuarios li:last").attr("id", "usuario" + data.user.ID);
                $("#lista-usuarios li:last").append("<a onclick=\"DeleteUserFromList(" + data.user.ID + ");return false;\">excluir</a>");
                $("#lista-usuarios li:last").append("<input type=hidden />");
                $("#lista-usuarios li:last").append(data.user.Name);

                $("#lista-usuarios li a:last").attr("class", "ico-excluir");

                $("#lista-usuarios li input:last").attr("value", data.user.ID);
                $("#lista-usuarios li input:last").attr("name", "userID");
                $("#lista-usuarios li input:last").attr("id", "userID" + data.user.ID);

                $('#lista-usuarios li').sortElements(function (a, b) {
                    return $(a).text() > $(b).text() ? 1 : -1;
                });
            }
        }, "json");

        $("#incluirNovoUsuario").slideToggle();
    });



    $(".ico-incluir", ".usuarios").click(function () {
        $("#incluirNovoUsuario").slideToggle();
        $("#txtUserName", "#incluirNovoUsuario").attr("value", "");
    });

    $("#txtGroupName", "#txtGroupNameBox").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/AccessControl/GroupsOfAssessorsAutoSuggest",
                type: "POST",
                dataType: "json",
                data: { term: request.term },
                success: function (data) {
                    $("#hdfGroup", "#txtGroupNameBox").val("");
                    response($.map(data, function (item) {
                        return {
                            label: item.Name,
                            value: item.Name,
                            id: item.ID
                        }
                    }));
                }
            });
        },
        minLength: 1,
        select: function (event, ui) {
            $("#hdfGroup", "#txtGroupNameBox").val(ui.item.id);
        }

    });
    $("#txtUserName", "#UserBox").autocomplete({
        source: function (request, response) {
            var data = "";

            data += $(context).serialize();
            if (data != "") data += "&";
            data += "term=" + request.term;


            $.ajax({
                url: "/AccessControl/UserAutoSuggest",
                type: "POST",
                dataType: "json",
                data: data,
                success: function (data) {
                    $("#hdfUser", "#UserBox").val("");
                    response($.map(data, function (item) {
                        return {
                            label: item.Name,
                            value: item.Name,
                            id: item.ID
                        }
                    }));
                }
            });
        },
        minLength: 1,
        select: function (event, ui) {
            $("#hdfUser", "#UserBox").val(ui.item.id);
        }

    });
    $("#btnAssociarUsuario").click(function () {
        var groupName = $.trim($("#txtGroupName", context).val());
        var feedback = "";

        if (groupName == "" || groupName.length < 3) { feedback = "<li>Nome do Grupo é obrigatório.</li>"; }

        if (feedback == "") {
            $.post("/AccessControl/AssociateUsersToGroup/", $(context).serialize(), function (data) {
                if (data.error != "") {
                    $("#feedbackError", context).html(commonHelper.FormatErrorMessage(data.error, false));
                    $("#feedbackError", context).show();
                    return false;
                }
                else window.location = "/AccessControl/Users";

            }, "json");
        }
        else {
            $("#feedbackError", context).html(commonHelper.FormatErrorMessage(feedback, true));
            $("#feedbackError", context).show();
            return false;
        }

    });

});