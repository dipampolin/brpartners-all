﻿$(function () {
    var context = $(".area-solicitacao-direito")[0];

    $.format.locale({
        number: {
            groupingSeparator: '.',
            decimalSeparator: ','
        }
    });

    var fnAddMasks = function () {
        $(".quantidade-solicitado").setMask("999999");
    }

    fnAddMasks();

    /*Pegar nome do cliente*/
    $("#txtClient_0", context).blur(function () {
        var that = $(this);
        if ($(this).val() != "") {
            commomDataHelper.GetClientName($(this).val(), "nome-cliente", $(".conteudo")[0], "txtClient_0");

            $.ajax({
                url: "../Subscription/GetRightRequestForClient",
                cache: false,
                type: "POST",
                data: { clientIds: $("#txtClient_0", context).val(), requestId: $("#hdfRequestId", context).val(), status: 'A' },
                beforeSend: function () {
                    $("#loadingAreaRequest", context).show();
                    $("#emptyArea2", context).hide();
                    $("#table-area-request", context).hide();
                    fnDisableSubmit();

                    $("#requestFeedbackError", context).hide().find("ul").html('');
                }
                , error: function () {
                    $("#emptyArea2", context).show();
                    $("#loadingAreaRequest", context).hide();
                }
                , success: function (position) {
                    $("#loadingAreaRequest", context).hide();

                    if (position != null && position != undefined) {
                        //$("table.tabela-form > tbody").html('');

                        if (position.length == 0) {
                            $("#emptyArea2", context).show();
                            that.val('');
                            that.parent().parent().find('.nome-cliente').text('-');
                            $("#txtQuantity_0").val('').attr("disabled", "disabled");

                            that.parent().parent().find(".nome-cliente").text("-");
                            that.parent().parent().find(".valor-direito").text("-");
                            that.parent().parent().find(".valor-solicitado").text("-");
                            that.parent().parent().find(".valor-disponivel").text("-");
                            that.parent().parent().find(".valor-total").text("-");
                        }
                        else {

                            if (position.AvailableQtty < 0) {
                                $("#requestFeedbackError").show().find("ul").html('').show().append($('<li>Não há valor disponível.</li>').clone());
                                $("#txtQuantity_0").attr("disabled", "disabled");
                            }
                            else {
                                $(".nome-cliente", context).text(position.ClientName);
                                $(".valor-direito", context).text($.format.number(position.RightValue, "#,##0.00#"));
                                $(".valor-solicitado", context).text($.format.number(position.RequestedQtty, "#,###"));
                                $(".valor-ja-solicitado", context).val(position.RequestedQtty, "#,###");
                                $(".valor-disponivel", context).text($.format.number(position.AvailableQtty, "#,###"));
                                $(".valor-total", context).text($.format.number(position.TotalValue, "#,##0.00#"));

                                fnAddMasks();

                                $("#txtQuantity_0").attr("disabled", "");

                                $("#emptyArea2", context).hide();
                                //$("#table-area-request", context).show();

                                $("#btnSubmit", context).bind("click", fnSubmit);

                                $("body").unbind("keydown");
                                $("body").bind("keydown", function (event) {
                                    if (event.which == 13 && $("*:focus").attr("id") != "txtObservation") {
                                        fnSubmit();
                                    }
                                });
                            }
                        }

                    }
                    else {
                        $("#emptyArea2", context).show();
                        that.val('');
                        that.parent().parent().find('.nome-cliente').text('-');

                        $("#txtQuantity_0").val('').attr("disabled", "disabled");

                        that.parent().parent().find(".nome-cliente").text("-");
                        that.parent().parent().find(".valor-direito").text("-");
                        that.parent().parent().find(".valor-solicitado").text("-");
                        that.parent().parent().find(".valor-disponivel").text("-");
                        that.parent().parent().find(".valor-total").text("-");
                    }
                    $("#table-area-request", context).show();
                }
            });
        }

    }).setMask('999999999');

    $(".quantidade-solicitado", context).blur(function () {

        var value = parseInt($(this).val().replace(/\./g, ""), 10);

        var parent = $(this).parent().parent();

        var available = parseInt(parent.find(".valor-disponivel", context).text().replace(/\./g, ""), 10);
        var requested = parseInt(parent.find(".valor-solicitado", context).text().replace(/\./g, ""), 10);
        var alreadyRequested = parseInt(parent.find(".valor-ja-solicitado", context).val().replace(/\./g, ""), 10);
        var unitaryPrice = parseFloat(parent.find(".valor-direito", context).text().replace(/\./g, "").replace(/\,/g, "."));

        /*console.log(context);
        console.log(available);
        console.log(requested);
        console.log(alreadyRequested);
        console.log(unitaryPrice);*/

        /*if (value > available + requested) {

        $("#requestFeedbackError").show().find("ul").html('').show().append($('<li>Valor solicitado maior que o valor disponível.</li>').clone());
        $(this).val(0);
        }
        else {
        $("#requestFeedbackError").hide().find("ul").html('');

        var total = available + requested;
        var newAvailable = total - value;
        var newRequested = value;*/

        if (value > (available + requested) - alreadyRequested) {
            $("#requestFeedbackError").show().find("ul").html('').show().append($('<li>Valor solicitado maior que o valor disponível.</li>').clone());
            $(this).val(0);
        }
        else {
            $("#requestFeedbackError").hide().find("ul").html('');
            var total = available + requested;
            var newAvailable = total - value - alreadyRequested;
            var newRequested = value + alreadyRequested;

            /*console.log(total);
            console.log(newAvailable);
            console.log(newRequested);*/

            //var total = available + requested;
            //var newRequested = value + requested;
            //var newAvailable = total - newRequested;
            parent.find(".valor-disponivel", context).text($.format.number(newAvailable, "#,###"));
            parent.find(".valor-solicitado", context).text($.format.number(newRequested, "#,###"));
            parent.find(".valor-total", context).text($.format.number(newRequested * unitaryPrice, "#,##0.00#"));
        }

    }).setMask("999999");


    /*var value = parseInt($(this).val().replace(/\./g, ""), 10);
    var parent = $(this).parent().parent();

    var available = parseInt(parent.find(".valor-disponivel", context).text().replace(/\./g, ""), 10);
    var requested = parseInt(parent.find(".valor-solicitado", context).text().replace(/\./g, ""), 10);
    var unitaryPrice = parseFloat(parent.find(".valor-direito", context).text().replace(/\./g, "").replace(/\,/g, "."));
    //if (value > available + requested) {
    if (value > available) {
    $("#requestFeedbackError").show().find("ul").html('').show().append($('<li>Valor solicitado maior que o valor disponível.</li>').clone());
    $(this).val(0);
    }
    else {
    $("#requestFeedbackError").hide().find("ul").html('');

    //var total = available + requested;
    var total = available + requested;

    var newRequested = value + requested;

    var newAvailable = total - newRequested;

    parent.find(".valor-disponivel", context).text($.format.number(newAvailable, "#,###"));
    parent.find(".valor-solicitado", context).text($.format.number(newRequested, "#,###"));

    parent.find(".valor-total", context).text($.format.number(newRequested * unitaryPrice, "#,##0.00#"));
    }*/

    var fnValidateRequest = function () {
        var request = true;

        $("#requestFeedbackError", context).hide().find("ul").html('');

        var ul = $("div#requestFeedbackError > ul", context);

        var linhaCliente = 1;
        $(".codigo-cliente[type='text']").each(function () {

            if ($(this).val() == null || $(this).val() == undefined || $(this).val() == "") {
                ul.append($("<li>Cliente na linha " + linhaCliente.toString() + " não informado.</li>").clone());
                request = false;
            }

            linhaCliente++;
        });

        if (request == false) {
            $("#requestFeedbackError", context).show().focus();
        }
        else {
            $("#requestFeedbackError", context).hide();
            $("#requestFeedbackError > ul", context).html('');
        }

        return request;
    }

    var fnSubmit = function () {
        $("#hdfClientName").val($("#lblClientName", context).text());


        $("#frmRightsRequest").ajaxForm({
            beforeSend: function () {
                $("#divSubscriptionFeedback").hide().find("span").html('');
                $("#feedbackError").hide().find("span").html('');

                $("#loadingAreaRequest", context).show();
                $("#emptyArea2", context).hide();
                $("#table-area-request", context).hide();
            },
            success: function (data) {

                $("#loadingAreaRequest", context).hide();
                $("#emptyArea2", context).hide();
                $("#table-area-request", context).show();

                if (data.Result) {


                    $("#requestRightsList > tbody").html('');
                    $("#btnSearch").click();

                    $("#divSubscriptionFeedback").show().find("span").append(data.Message);

                    //oTableRequestRights.fnDraw(false);
                    try
                    {
                        if (RequestRightsMethods != null && RequestRightsMethods != undefined)
                            RequestRightsMethods.RequestRightsPendents();
                    }
                    catch (e) {  }

                }
                else {
                    $("#feedbackError").show().append(data.Message);
                    $("#btnSearch").click();
                    //oTableRequestRights.fnDraw(false);
                }


                Modal.Close();


            }
        });


        if (fnValidateRequest()) {
            $("#frmRightsRequest").submit();
        }

    };

    var fnDisableSubmit = function () {
        $("#btnSubmit", context).unbind("click");
    };

    if ($("#hdfRequestId").length > 0) {
        $("#btnSubmit").unbind("click");
        $("#btnSubmit").bind("click", fnSubmit);
    }

    $("body").unbind("keydown");
    $("body").bind("keydown", function (event) {
        if (event.which == 13) {
            fnSubmit();
        }
    });
});
