﻿var scroll = 0;

function adicionaProduto() {
    let comboProduct = document.getElementById("Product");
    let selectedIndex = comboProduct.selectedIndex;

    let produto = comboProduct.options[selectedIndex].text;
    let id = comboProduct.options[selectedIndex].value;

    let listbox = document.querySelector("#lstProducts");

    let option = document.createElement("option");
    option.text = produto;
    option.value = id;

    let existInListBox;

    if (listbox.length == 0) {
        listbox.add(option);
    }

    for (let i = 0; i < listbox.options.length; i++) {
        if (existInListBox) {
            return;
        }
        existInListBox = listbox.options[i].value == id;
    }

    if (!existInListBox) {
        listbox.add(option);
    }
}

function removeProduto() {
    let listBox = document.querySelector("#lstProducts");
    listBox.remove(listBox.selectedIndex);
}

function addNewAccount(display) {
    var isValid = verifyField();

    if (isValid == 0)
        createElementTable('.account', 'accountTable', display)
}

function createElementTable(css, table, display) {
    var inputs = document.querySelectorAll(css);
    var elementTr = document.createElement('tr');
    var table = document.querySelector('#' + table);
    table.appendChild(elementTr);
    var number = 0;

    inputs.forEach(function (input) {
        var newElem = document.createElement('td');

        if (input.type == 'text') {
            newElem.textContent = input.value;
        }
        else if (input.type == "select-one") {
            newElem.textContent = input.options[input.selectedIndex].text
            newElem.id = input.value
        }

        newElem.classList.add('table-td');

        if (display == 'none' && (number >= 0 && number < 3)) {
            newElem.style.cssText = 'display:' + display + ';';
            number++;
        }

        elementTr.appendChild(newElem);
    });

    var TdInput = document.createElement('td');
    TdInput.className = 'table-td';

    var elementInput = document.createElement("a");
    elementInput.className = 'ico-excluir';
    elementInput.style.marginLeft = "27px";

    TdInput.appendChild(elementInput);
    elementTr.appendChild(TdInput);

    elementInput.addEventListener('click', function (event) {
        removeParent(event);
    });

    hiddenElementTable();
}

function removeParent(event) {
    event.target.parentNode.parentNode.classList.add('fadeOut');

    setTimeout(function () {
        event.target.parentNode.parentNode.remove();
        hiddenElementTable();
    }, 500);
}

function hiddenElementTable() {
    var rows = document.getElementById('accountTable').getElementsByTagName("tr").length
    if (rows > 1) {
        document.getElementById('accountTable').style.display = '';
    }
    else
        document.getElementById('accountTable').style.display = 'none';
}

function verifyField() {
    var inputs = document.querySelectorAll('.accountRequired');
    var error = 0;

    inputs.forEach(function (input) {
        if (input.type == 'text' || input.type == 'date' || input.type == "select-one") {
            var value = input.value;

            if (value == "") {
                if (!input.classList.contains('error'))
                    input.classList.add('error')

                error = 1;
            } else if (input.classList.contains('error')) {
                input.classList.remove("error");
            }
        }
    });

    return error;
}

function validatePatrimonialInformation() {
    var txtHousePatrimony = document.getElementById("HousePatrimony");
    var txtCarPatrimony = document.getElementById("CarPatrimony");
    var txtApplicationPatrimony = document.getElementById("ApplicationPatrimony");
    var divError = document.getElementById("div-error");
    var error = 0;

    if (txtHousePatrimony.value == "" && txtCarPatrimony.value == "" && txtApplicationPatrimony.value == "") {
        divError.style.display = "block";
        txtHousePatrimony.classList.add("error");
        txtCarPatrimony.classList.add("error");
        txtApplicationPatrimony.classList.add("error");

        if (scroll == 0) {
            $('html, body').animate({
                scrollTop: $("#" + txtHousePatrimony.parentNode.parentNode.id).offset().top
            }, 1000);

            scroll = 1;
        }

        error = 1;
    } else {
        divError.style.display = "none";
        txtHousePatrimony.classList.remove("error");
        txtCarPatrimony.classList.remove("error");
        txtApplicationPatrimony.classList.remove("error");
    }

    return error;
}

function validateFill() {
    var inputs = document.querySelectorAll('.required');
    var error = 0;

    inputs.forEach(function (input) {
        if (input.type == 'text' || input.type == 'date' || input.type == "select-one") {
            var value = input.value;

            if (value == "") {
                if (!input.classList.contains('error'))
                    input.classList.add('error')

                if (scroll == 0) {
                    $('html, body').animate({
                        scrollTop: $("#" + input.parentNode.id).offset().top
                    }, 1000);

                    scroll = 1;
                }

                error = 1;
            } else if (input.classList.contains('error')) {
                input.classList.remove("error");
            }
        }
    });

    return error;
}

function getValueTable() {
    var rows = document.getElementById('accountTable').getElementsByTagName("tr");
    for (var i = 1; i < rows.length; i++) {
        var cells = rows[i].cells;

        if (i > 1)
            document.getElementById("HdnAccount").value += ";"

        for (var a = 0; a < (cells.length - 1); a++) {
            if (a > 0)
                document.getElementById("HdnAccount").value += ",";

            if (cells[a].id != "")
                document.getElementById("HdnAccount").value += cells[a].id;
            else
                document.getElementById("HdnAccount").value += cells[a].innerHTML;
        }
    }
}