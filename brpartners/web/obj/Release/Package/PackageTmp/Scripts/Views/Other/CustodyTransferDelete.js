﻿$(function () {
    $("#btnDelete").unbind("click");
    $("#btnDelete").bind("click", function () {

        $.ajax({
            type: "POST",
            cache: false,
            async: false,
            url: '../Other/RemoveCustodyTransfer',
            data: { id: $("#hdfRequestId").val() },
            dataType: 'json',
            beforeSend: function () {
                $("#loadingArea").show();
                $("#feedback").hide().find("span").html('');
                $("#feedbackError").hide().find("span").html('');
            },
            success: function (data) {
                $("#loadingArea").hide();

                if (data.Result) {
                    selector = "#feedback";

                    oTableCustodyTransfer.fnDraw(true);

                    CustodyTransferMethods.getPendingCustodyTransfers();
                }
                else {
                    selector = "#feedbackError";
                }
                $(selector).show().find("span").append(data.Message);

                Modal.Close();

            }
        });

    });
});