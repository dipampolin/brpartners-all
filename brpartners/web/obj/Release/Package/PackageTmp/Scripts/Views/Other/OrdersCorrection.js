﻿/// <reference path="../../References.js" />

var oTable;
var emptyDataMessage = "Sua pesquisa não encontrou resultados válidos. Tente novamente.";
var filterPage = 1;

$(function () {
    var filterContext = $("#ordersFilterContent");

    DateHandler(filterContext);

    $(filterContext).bind('keydown', function (event) {
        if (event.which == 13) {
            fnSearch();
            return false;
        }
    });

    $("#ordersFilter").click(function () {
        $('#ordersFilterContent').slideToggle('fast');
        $('select').styleInputs();
        $(this).toggleClass('aberto');
    });

    $("#txtStartDate", filterContext).blur(function () {
        if (this.value == "")
            dateHelper.SetFormattedDate("#txtStartDate", 0, 0, -3, filterContext);
    });

    $("#txtEndDate", filterContext).blur(function () {
        if (this.value == "")
            dateHelper.SetFormattedDate("#txtEndDate", 0, 0, 0, filterContext);
    });

    $("#btnSearch", filterContext).click(function () {
        fnSearch(filterContext);
        return false;
    });

    setTimeout(function () {
        fnSearch(filterContext);
    }, 250);
});

function fnSearch(filterContext) {
    var feedback = ValidateFilterFields(filterContext);

    if (feedback == "") {

        $("#emptyArea").hide();
        $("#tableArea").hide();
        $("#loadingArea").show();

        $("#filterFeedbackError", filterContext).hide();

        if (filterContext.css("display") != "none") {
            $('#ordersFilterContent').slideToggle('fast');
            $('select').styleInputs();
            $(filterContext).toggleClass('aberto');
        }

        var status = commonHelper.GetParameterByName("status");
        if (status != null && status != undefined && status != "") {
            $("#ddlStatus", filterContext).val(status);
        }

        $.ajax({
            cache: false,
            type: "POST",
            dataType: "json",
            async: false,
            url: "/Other/SearchOrdersCorrections",
            data: { Assessors: $("#txtAssessors", filterContext).val(), Clients: $("#txtClient", filterContext).val(), StatusId: $("#ddlStatus", filterContext).val(), TypeId: $("#ddlType", filterContext).val(), StartDate: $("#txtStartDate", filterContext).val(), EndDate: $("#txtEndDate", filterContext).val() },
            success: function (data) {

                $('input[id=selectAll]').attr('checked', false);

                var pendingTag = $("#pendingTag");
                if (pendingTag != null && pendingTag.length > 0) {
                    $(pendingTag).find("strong").html(data.Pending);
                    $(pendingTag).find("strong").appendTo("Pendente" + (data.Pending > 1 ? "s" : ""));
                }
                else {
                    pendingTag = "<div id='pendingTag' class='aviso'><span><strong>" + data.Pending + "</strong> Pendente" + (data.Pending > 1 ? "s" : "") + "</span></div>";
                    $('#trilha').children('ul').find('li').last().append(pendingTag);
                }

                data.Pending > 0 ? $("#pendingTag").show() : $("#pendingTag").hide();

                $("#hdfPending").val(data.Pending);

                $("#loadingArea").hide();

                var emptyData = (data.tBody == null || data.tBody.length == 0);

                var excelLink = $("#lnkOrdersExcel");
                var pdfLink = $("#lnkOrdersPdf");
                excelLink.attr("href", (emptyData) ? "#" : "/Other/OrdersCorrectionToFile/?isPdf=false");
                //excelLink.attr("target", (emptyData) ? "_self" : "_blank");
                pdfLink.attr("href", (emptyData) ? "#" : "/Other/OrdersCorrectionToFile/?isPdf=true");
                //pdfLink.attr("target", (emptyData) ? "_self" : "_blank");

                if (emptyData) {
                    $("#emptyArea").show();
                    $("#tableArea").hide();
                }
                else {
                    $("#tbCorrections").find("tbody").html(data.tBody);
                    $("#tableArea").show();
                    $("#emptyArea").hide();

                    $(".ico-detalhes", ".area-resultado").click(function () {
                        var id = $(this).attr("ID").replace("buttonDetails", "");
                        $("#tbDetails" + id).toggle();
                        if ($(this).is(".aberto")) {
                            $(this).removeClass("aberto").addClass("fechado");
                        }
                        else {
                            $(this).addClass("aberto").removeClass("fechado");
                        }
                    });

                    var modalSituacao = $('.modal-situacao');
                    $('.exibir-modal').click(function () {
                        modalSituacao.show();

                        var correctionId = parseInt(this.id.replace("lnkRealize", ""));
                        $("#correctionIds", $(".modal-situacao")).val(correctionId);
                        var p = $(this);
                        var offset = p.offset();
                        $('.modal-situacao').css("top", offset.top);
                        $('.modal-situacao select').styleInputs();
                    });
                    $('.closeLink').bind({
                        "click": function () {
                            modalSituacao.hide();
                        }
                    });
                }
            }
        });
    }
    else {
        $("#filterFeedbackError", filterContext).html(feedback).show();
    }

    return false;
};

function Export(pdf) {
    var url = "/Other/OrdersCorrectionConfirmExport/?isPDF=" + pdf;
    commonHelper.OpenModal(url, 330, ".content", "");
    $("#MsgBox").center();
}

function ViewHistory() {
    commonHelper.OpenModal("/Other/OrdersCorrectionHistory", 671, ".content", "");
}

function OpenRequest(id) {
    commonHelper.OpenModal("/Other/OrdersCorrectionRequest/" + id, 420, ".content", "");
}

function ValidateFilterFields(context) {
    var messageError = "";
    if (!ValidateList($("#txtAssessors", context).val()))
        messageError += "<li>Faixa de assessores inválida.</li>";
    if (!ValidateList($("#txtClient", context).val()))
        messageError += "<li>Faixa de clientes inválida.</li>";

    if (messageError.length > 0)
        messageError = "<ul>" + messageError + "</ul>";

    return messageError;
}

function ValidateList(listValue) {
    var list = listValue.split(',');
    if (list.length > 0) {
        for (var i = 0; i < list.length; i++) {
            var item = list[i];
            if (item.indexOf('-') >= 0) {
                var items = item.split('-');
                if (items.length > 2 || isNaN(items[0]) || isNaN(items[1]))
                    return false;
                var start = parseInt(items[0]);
                var end = parseInt(items[1]);
                if (start >= end)
                    return false;
            }
            else {
                if (isNaN(item))
                    return false;
            }
        }
    }
    return true;
}


function DeleteCorrection(id) {
    var url = "/Other/OrdersCorrectionConfirmDelete/?correctionId=" + id;
    commonHelper.OpenModal(url, 450, ".content", "");
    $("#MsgBox").center();
}

function ConfirmDeleteCorrection() {
    var deleteContext = $("#frmConfirmCorrectionDelete");
    $.post("/Other/DeleteOrdersCorrection", { hdfCorrectionId: $("#hdfCorrectionId", deleteContext).val(), hdfAssessorId: $("#hdfAssessorId", deleteContext).val(), hdfAssessor: $("#hdfAssessor", deleteContext).val() }, function (data) {
        if (data.Error == "") {
            $("span", "#feedback").html(data.Message);
            $("#feedback").show();
            fnSearch($("#ordersFilterContent"));
        }
        else {
            $("#feedbackError").html(data.Error);
            $("#feedbackError").show();
        }
        Modal.Close();
    }, "json");
}

function RealizeCorrection(id) {
    var url = "/Other/OrdersCorrectionConfirmRealize/?correctionId=" + id;
    commonHelper.OpenModal(url, 450, ".content", "");
    $("#MsgBox").center();
}

function OpenRealize() {
    var checkedCorrections = $("[id^=chkCorrection]:checked");
    if ($("[id^=chkCorrection]:checked").length > 0) {
        var ids = "";
        $(checkedCorrections).each(function () {
            ids += this.value + ";";
        });
        var url = "/Other/OrdersCorrectionConfirmRealize/?correctionIds=" + ids;
        commonHelper.OpenModal(url, 450, ".content", "");
        $("#MsgBox").center();
    }
}

function OpenRealizeSingle(id) {
    var url = "/Other/OrdersCorrectionConfirmRealizeSingle/?correctionId=" + id;
    commonHelper.OpenModal(url, 450, ".content", "");
    $("#MsgBox").center();
}

function CheckAll(checkAll) {
    var context = $("#tbCorrections").find("tbody");
    var checked = $(checkAll).is(':checked');
    $("input:checkbox:not(:disabled)", context).each(function () {
        $(this).attr('checked', checked);
    });
}

function OpenCorrectionHistory(id) {
    var url = "/Other/OrdersCorrectionHistoryDetail/?correctionID=" + id;
    commonHelper.OpenModal(url, 671, ".content", "");
    $("#MsgBox").center();
}

function ValidateRealize() {
    if ($("#ddlSingleStatus", $(".modal-situacao")).val() != "7") {
        ConfirmRealize(false);
    }
    else
        return false;
}

function ConfirmRealize(isModal) {
    var realizeContext = $("#frmConfirmCorrectionRealize");
    $.post("/Other/RealizeOrdersCorrection", { correctionIds: $("#correctionIds", realizeContext).val() }, function (data) {
        if (data.Error == "") {
            $("span", "#feedback").html(data.Message);
            $("#feedback").show();
            fnSearch($("#ordersFilterContent"));
        }
        else {
            $("#feedbackError").html(data.Error);
            $("#feedbackError").show();
        }
        isModal ? Modal.Close() : $(".modal-situacao").hide();
    }, "json");
}

function DateHandler(filterContext) {
    $("#txtStartDate", filterContext).setMask("99/99/9999").datepicker().datepicker($.datepicker.regional['pt-BR']);
    $("#txtEndDate", filterContext).setMask("99/99/9999").datepicker().datepicker($.datepicker.regional['pt-BR']);
    $.datepicker.setDefaults($.datepicker.regional['pt-BR']);
}