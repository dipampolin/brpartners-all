﻿$(function () {
	var newDisclaimerContent = tinyMCE.init({
		// General options
		mode: "textareas",
		theme: "advanced",
		//onchange_callback: "myCustomOnChangeHandler",
		//convert_newlines_to_brs: true,
		//encoding: "xml",
		//inline_styles : false,
		valid_elements: "strong/b,em/i,u,br,table,tr,td,p[align|style]",
		plugins: "autolink,lists,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
		// Theme options
		theme_advanced_buttons1: "bold,italic,underline,|,justifyleft,justifycenter,justifyright,justifyfull,|,tablecontrols",
		theme_advanced_buttons2: "",
		theme_advanced_buttons3: "",
		theme_advanced_buttons4: "",
		theme_advanced_toolbar_location: "top",
		theme_advanced_toolbar_align: "left",
		theme_advanced_statusbar_location: "",
		theme_advanced_resizing: false,
		skin: "o2k7",
		skin_variant: "silver",
		width: "100%",
		height: "680"
	});


});

function SaveDisclaimer(id) {
	var iframe = $("#txtDisclaimerContent_ifr");
	iframe.removeClass("error");
	$("#feedbackError").hide();
	var tineMCEContent = escape(tinyMCE.get('txtDisclaimerContent').getContent());
	if (CheckHTMLContent(tineMCEContent)) {
		tinyMCE.triggerSave();
		$("#frmDisclaimer").submit();
	}
	else {
		$("#feedbackError").html(commonHelper.FormatErrorMessage("Oops. Faltou preencher o campo abaixo", false));
		$("#feedbackError").show();
		iframe.addClass("error");
	}
}

function Trim(str) { return str.replace(/^\s+|\s+$/g, ""); }

function CheckHTMLContent(content) {
	var strInputCode = content;

	strInputCode = content.replace(/&(lt|gt);/g, function (strMatch, p1) {
		return (p1 == "lt") ? "<" : ">";
	});
	var strTagStrippedText = strInputCode.replace(/<\/?[^>]+(>|$)/g, "");
	return Trim(strTagStrippedText).length > 0;
}

function replaceAll(string, token, newtoken) {
	while (string.indexOf(token) != -1) {
		string = string.replace(token, newtoken);
	}
	return string;
}