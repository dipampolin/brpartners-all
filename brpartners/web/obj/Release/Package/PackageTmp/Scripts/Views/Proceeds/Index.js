﻿/// <reference path="../../References.js" />

var oTable;
var emptyDataMessage = "Sua pesquisa não encontrou resultados válidos. Tente novamente.";
var filterPage = 1;


var fnSearch = function (filterContext) {
    var feedback = ValidateFilterFields(filterContext);

    if (feedback == "") {

        $("#emptyArea").hide();
        $("#tableArea").hide();
        $("#loadingArea").show();

        if (filterContext.css("display") != "none")
            $("#proceedsFilter").click();

        $("#filterFeedbackError", filterContext).hide();

        $.ajax({
            cache: false,
            type: "POST",
            async: true,
            url: "/Proceeds/LoadProceeds",
            data: { ddlProceeds: $("#ddlProceeds", filterContext).val(), txtStartDate: $("#txtStartDate", filterContext).val(), txtEndDate: $("#txtEndDate", filterContext).val(), txtAssessors: $("#txtAssessors", filterContext).val(), txtClient: $("#txtClient", filterContext).val(), txtStockCode: $("#txtStockCode", filterContext).val(), txtCompany: $("#txtCompany", filterContext).val() },
            success: function () {
                obj = {};
                obj = {
                    "bSort": true,
                    "bAutoWidth": false,
                    "bInfo": true,
                    "bSort": true,
                    "iDisplayLength": 10,
                    "oLanguage": {
                        "sLengthMenu": "<span>Exibir:</span> _MENU_ <span>por página</span>",
                        "sZeroRecords": "Sua pesquisa não encontrou resultados válidos. Tente novamente.",
                        "oPaginate": {
                            "sPrevious": "Anteriores",
                            "sNext": "Próximos"
                        }
                    },
                    "sPaginationType": "full_numbers",
                    "sDom": 'rtip',
                    "fnDrawCallback": function () {
                        var totalRecords = this.fnSettings().fnRecordsDisplay();
                        var emptyArea = $("#emptyArea");
                        var tableArea = $("#tableArea");

                        var excelLink = $("#lnkProceedsExcel");
                        var pdfLink = $("#lnkProceedsPdf");

                        if (excelLink.length > 0 && pdfLink.length > 0) {
                            if (totalRecords == 0) {
                                $(excelLink).click(function () { return false; });
                                $(pdfLink).click(function () { return false; });
                            }
                            else {
                                $(excelLink).click(function () { Export(false); return false; });
                                $(pdfLink).click(function () { Export(true); return false; });
                            }
                        }

                        $("#loadingArea").hide();

                        if (totalRecords == 0) {
                            emptyArea.html(emptyDataMessage);
                            emptyArea.show();
                            tableArea.hide();
                        }
                        else {
                            tableArea.show();
                            emptyArea.hide();

                            var totalPerPage = this.fnSettings()._iDisplayLength;

                            var totalPages = Math.ceil(totalRecords / totalPerPage);

                            var currentIndex = parseInt(this.fnSettings()._iDisplayStart);
                            var currentPage = totalPages > 0 ? Math.ceil(1 + (currentIndex / totalPerPage)) : 0;

                            $("#proceedsList_info").html("Exibindo " + currentPage + " de " + totalPages);

                            if (totalRecords > 0 && totalPages > 1) {
                                $("#proceedsList_info").css("display", "");
                                $("#proceedsList_length").css("display", "");
                                $("#proceedsList_paginate").css("display", "");
                            }
                            else {
                                $("#proceedsList_info").css("display", "none");
                                $("#proceedsList_length").css("display", "none");
                                $("#proceedsList_paginate").css("display", "none");
                            }
                            $('select', '#proceedsList_length').styleInputs();
                        }
                        // Adiciona a classe 'ultimo' à última tr no corpo da tabela
                        $('table tbody tr:last-child').addClass('ultimo');

                        // Adiciona a classe 'ultimo' às últimas th e td no corpo da tabela
                        $('table tr td:last-child').addClass('ultimo');
                        $('table tr th:last-child').addClass('ultimo');
                    },
                    "bProcessing": false,
                    "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                        $('#proceedsList_first').css("visibility", "hidden");
                        $('#proceedsList_last').css("visibility", "hidden");
                    },
                    "bRetrieve": true,
                    "bServerSide": true,
                    "sAjaxSource": "../DataTables/Proceeds_List",
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        $.ajax({
                            cache: false,
                            dataType: 'json',
                            type: "POST",
                            url: sSource,
                            data: aoData,
                            success: fnCallback
                        });
                    }
                };

                oTable = $("#proceedsList").dataTable(obj);
                oTable.fnClearTable();
                oTable.fnDraw();
            }
        });
    }
    else {
        $("#filterFeedbackError", filterContext).html(feedback).show();
    }
    return false;
};

$(function () {
    //$(document).ready(function () {

    var filterContext = $("#proceedsFilterContent");

    $("#txtStartDate", filterContext).setMask("99/99/9999").datepicker().datepicker($.datepicker.regional['pt-BR']);
    $("#txtEndDate", filterContext).setMask("99/99/9999").datepicker().datepicker($.datepicker.regional['pt-BR']);
    $.datepicker.setDefaults($.datepicker.regional['pt-BR']);

    $("#proceedsFilter").click(function () {
        $('#proceedsFilterContent').slideToggle('fast');
        $('select').styleInputs();
        $(this).toggleClass('aberto');

        marketHelper.SetStocksSuggest("txtStockCode", "txtCompany", filterContext);
        marketHelper.SetCompaniesSuggest("txtCompany", "txtStockCode", filterContext);
    });

    $("#txtStartDate", filterContext).blur(function () {
        if (this.value == "")
            dateHelper.SetFormattedDate("#txtStartDate", 0, 0, 0, filterContext);
    });

    $("#txtEndDate", filterContext).blur(function () {
        if (this.value == "")
            dateHelper.SetFormattedDate("#txtEndDate", 0, 1, 0, filterContext);
    });

    $("#btnSearch", filterContext).click(function () {
        fnSearch(filterContext);
        return false;
    });

    setTimeout(function () {
        fnSearch(filterContext);
        return false;
    }, 250);

    $(filterContext).bind('keydown', function (event) {
        if (event.which == 13) {
            fnSearch(filterContext);
            return false;
        }
    });
    // });
});

function Export(pdf) {
    if ($("#tableArea").css("display") != "none") {
        var url = "/Proceeds/ConfirmExport/?isPDF=" + pdf;
        commonHelper.OpenModal(url, 330, ".content", "");
        $("#MsgBox").center();
    }
    else
        return false;
}

function ViewHistory() {
    commonHelper.OpenModal("/Proceeds/ProceedsHistory", 671, ".content", "");
}

function ValidateFilterFields(context) {
    var messageError = "";
    if (!validateHelper.ValidateList($("#txtAssessors", context).val()))
        messageError += "<li>Faixa de assessores inválida.</li>";
    if (!validateHelper.ValidateList($("#txtClient", context).val()))
        messageError += "<li>Faixa de clientes inválida.</li>";

    if (messageError.length > 0)
        messageError = "<ul>" + messageError + "</ul>";

    return messageError;
}
