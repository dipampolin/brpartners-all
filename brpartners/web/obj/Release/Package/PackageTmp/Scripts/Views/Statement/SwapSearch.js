﻿/// <reference path="../../References.js" />

$(function () {
    $(document).ready(function () {

        var context = $("#frmSwapSearch");

        $("#lnkSearch", context).click(function () {
            var month = $("#ddlMonth", context).val();
            var year = $("#ddlYear", context).val();
            var type = $("#ddlType", context).val();
            var cpfCnpj = $("#hdnCpfCnpj", context).val();

            $("#feedbackError", context).hide();

            if (month == "" || year == "" || type == "")
            {
                $("#feedbackError", context).html("Por favor, preencha todos os campos do formulário.");
                $("#feedbackError", context).show();
                return;
            }

            $.ajax({
                url: "/Statement/Swap",
                type: "POST",
                dataType: "html",
                async: false,
                data: { cpfCnpj: cpfCnpj, month: month, year: year, type: type },
                success: function (dataFI) {
                    // console.log(dataFI);
                    var w = window.open(dataFI);
                    w.document.write(dataFI);
                },
                error: function (dataFI) {
                    alert('Error');
                },
            });
        });
    });
});
