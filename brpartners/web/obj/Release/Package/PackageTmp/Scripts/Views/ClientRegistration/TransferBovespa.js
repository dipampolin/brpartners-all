﻿/// <reference path="../../References.js" />

var oTableTransferBovespa = null;

var fnDrawCallback = function (settings) {
    $("#loadingArea").hide();
    $("#tableArea").show();

    var totalRecords = settings.fnSettings().fnRecordsDisplay();

    SubscriptionMethods.enableExportButtons();

    var totalPerPage = settings.fnSettings()._iDisplayLength;

    var totalPages = Math.ceil(totalRecords / totalPerPage);

    var currentIndex = parseInt(settings.fnSettings()._iDisplayStart);
    var currentPage = totalPages > 0 ? Math.ceil(1 + (currentIndex / totalPerPage)) : 0;
    $("#tbTransferBovespa_info").html("Exibindo " + currentPage + " de " + totalPages);

    if (totalRecords > 0 && totalPages > 1) {
        $("#tbTransferBovespa_info").css("display", "");
        $("#tbTransferBovespa_length").css("display", "");
        $("#tbTransferBovespa_paginate").css("display", "");
    }
    else {
        $("#tbTransferBovespa_info").css("display", "none");
        $("#tbTransferBovespa_length").css("display", "none");
        $("#tbTransferBovespa_paginate").css("display", "none");
    }
}

var objSubscriptionFilter = {
    "bAutoWidth": false
            , "bPaginate": true
            , "bDestroy": true
            , "sPaginationType": "full_numbers"
            , "sDom": 'trip'
            , "oLanguage": {
                "sLengthMenu": "Exibindo _MENU_ registros",
                "sZeroRecords": "Nenhum registro encontrado.",
                "sInfo": "Encontramos _TOTAL_ registros",
                "sInfoEmpty": "Nenhum registro encontrado",
                "sInfoFiltered": "(Filtrando _MAX_ registros)",
                "sSearch": "Buscar:",
                "sProcessing": "Processando...",
                "oPaginate": {
                    "sFirst": "Primeiro",
                    "sPrevious": "Anterior",
                    "sNext": "Próximo",
                    "sLast": "Último"
                }
            }
            , "bProcessing": true
            , "bServerSide": true
            , "sAjaxSource": "../ClientRegistration/LoadTransferBovespa"
            , "fnServerData": function (sSource, aoData, fnCallback) {
                var filterContext = $("#transferBovespaFilterContent");

                if (aoData != null && aoData != undefined) {
                    aoData.push({ "name": "txtClientCode", "value": $("#txtClientCode", filterContext).val() });
                    aoData.push({ "name": "txtCnpj", "value": $('#txtCnpj', filterContext).val() });
                }
                $.ajax({
                    "cache": false,
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "beforeSend": function () {
                        $("#tableArea").hide();
                        $("#loadingArea").show();
                        $("#emptyArea").hide();
                    },
                    "success": function (data) {
                        fnCallback(data);
                        if (data.iTotalRecords == 0) {
                            $("#loadingArea").hide();
                            $("#tableArea").hide();
                            $('#emptyArea').html('Sua pesquisa não encontrou resultados válidos. Tente novamente.').show();
                        }
                        else {
                            $("#tableArea").show();
                            $("#loadingArea").hide();
                            $('#emptyArea').html('').hide();
                        }

                        SubscriptionMethods.enableExportButtons();
                    }
                });
            }
            , fnDrawCallback: function () {
                fnDrawCallback(this);
            }
            , "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                return nRow;
            }
            , "aoColumns": [
                null,
                null,
                null
            ]
};



var objSubscriptionInitial = {
    "bAutoWidth": false
            , "bPaginate": true
            , "bDestroy": true
            , "sPaginationType": "full_numbers"
            , "sDom": 'trip'
            , "oLanguage": {
                "sLengthMenu": "Exibindo _MENU_ registros",
                "sZeroRecords": "Nenhum registro encontrado.",
                "sInfo": "Encontramos _TOTAL_ registros",
                "sInfoEmpty": "Nenhum registro encontrado",
                "sInfoFiltered": "(Filtrando _MAX_ registros)",
                "sSearch": "Buscar:",
                "sProcessing": "Processando...",
                "oPaginate": {
                    "sFirst": "Primeiro",
                    "sPrevious": "Anterior",
                    "sNext": "Próximo",
                    "sLast": "Último"
                }
            }
            , "bProcessing": true
            , "bServerSide": true
            , "sAjaxSource": "../ClientRegistration/LoadTransferBovespa"
            , "fnServerData": function (sSource, aoData, fnCallback) {
                $.ajax({
                    "cache": false,
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "beforeSend": function () {
                        $("#tableArea").hide();
                        $("#loadingArea").show();
                        $("#emptyArea").hide();
                    },
                    "success": function (data) {
                        fnCallback(data);
                        if (data.iTotalRecords == 0) {
                            $("#loadingArea").hide();
                            $("#tableArea").hide();
                            $('#emptyArea').html('Sua pesquisa não encontrou resultados válidos. Tente novamente.').show();
                        }
                        else {
                            $("#tableArea").show();
                            $("#loadingArea").hide();
                            $('#emptyArea').html('').hide();
                        }
                        SubscriptionMethods.enableExportButtons();
                    }
                });
            }
            , fnDrawCallback: function () {
                fnDrawCallback(this);

            }
            , "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                return nRow;
            }
            , "aoColumns": [
                null,
                null,
                null
            ]
};

var SubscriptionMethods = {
    enableExportButtons: function () {
        var data = $("#tbTransferBovespa tbody tr");
        var emptyData = (data.length == 1 && data[0].lastChild.className == "dataTables_empty") ? true : false;
        var excelLink = $("#lnkTransferBovespaExcel");
        var pdfLink = $("#lnkTransferBovespaPdf");
        excelLink.attr("href", (emptyData) ? "#" : "/ClientRegistration/TransferBovespaToFile/?isPdf=false");
        pdfLink.attr("href", (emptyData) ? "#" : "/ClientRegistration/TransferBovespaToFile/?isPdf=true");
    }
};

$(function () {
    if (oTableTransferBovespa != null)
        oTableTransferBovespa.fnDestroy();

    $("#transferBovespaFilter").click(function () {
        $("#transferBovespaFilterContent").slideToggle('fast');
        $('select').styleInputs();
        $(this).toggleClass('aberto');
    });

    $("#btnSearch").click(function () {
        var message = ValidateFilter();
        $("#filterFeedbackError").html("").hide();

        if (message == "") {
            if (oTableTransferBovespa != null)
                oTableTransferBovespa.fnDestroy();

            oTableTransferBovespa = $("#tbTransferBovespa").dataTable(objSubscriptionFilter);
            $("#transferBovespaFilter").click();
        }
        else {
            $("#filterFeedbackError").html(message).show();
        }
    });

    $("#transferBovespaFilterContent").bind('keydown', function (event) {
        if (event.which == 13) {
            $("#btnSearch").click();
        }
    });

    oTableTransferBovespa = $("#tbTransferBovespa").dataTable(objSubscriptionInitial);

    SubscriptionMethods.enableExportButtons();
});

function ValidateFilter() {
    var code = $("#txtClientCode").val();
    var cnpj = $("#txtCnpj").val();
    var message = "";

    if (isNaN(code))
        message += "<li>O código deve ser numérico.</li>";

    if (isNaN(cnpj))
        message += "<li>O CNPJ deve ser numérico.</li>";

    if (message != "")
        message = "<ul>" + message + "</ul>";

    return message;
}