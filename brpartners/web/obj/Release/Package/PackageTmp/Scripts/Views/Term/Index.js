﻿/// <reference path="../../References.js" />

var oTable;
var emptyDataMessage = "Sua pesquisa não encontrou resultados válidos. Tente novamente.";
var filterPage = 1;
var feedbackCount = 1;


var fnSearch = function (actionClick, context) {
    $('input[name=chkAll]').attr('checked', false);

    if (feedbackCount == 0)
        $("#feedbackError").hide();

    var feedback = ValidateFilterFields(context);

    if (feedback == "") {

        $("#emptyArea").hide();
        $("#tableArea").hide();
        $("#loadingArea").show();

        if (context.css("display") != "none")
            $("#TermFilter").click();


        $("#filterFeedbackError", context).hide();

        feedbackCount = 0;

        var chkD1 = $('#chkD1', context).is(':checked') ? "1" : "0";
        var chkD2 = $('#chkD2', context).is(':checked') ? "1" : "0";
        var chkD3 = $('#chkD3', context).is(':checked') ? "1" : "0";

        var status = commonHelper.GetParameterByName("status");
        if (status != null && status != undefined && status != "" && !actionClick) {
            $("#ddlStatus", context).val(status);
        }

        $.ajax({
            cache: false,
            type: "POST",
            async: false,
            url: "/Term/LoadTerms",
            data: { assessors: $("#txtAssessors", context).val(), clients: $("#txtClient", context).val(), stockType: $("#ddlCompany", context).val(), stockValue: $("#txtStock").val(), d1: chkD1, d2: chkD2, d3: chkD3, status: $("#ddlStatus").val() },
            success: function () {
                obj = {};
                obj = {
                    "aoColumns": [
                                { "bSortable": false },
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                        		{ "sClass": "direita" },
                        		{ "sClass": "direita" },
                        		{ "sClass": "direita" },
                                null,
                                null,
                                null,
                                null,
                                { "bSortable": false }
                            ],
                    "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    	for (var i = 7; i < 10; i++) {
                    		if (!isNaN(parseFloat(aData[i]))) {
                    			var number = parseFloat(aData[i].replace(/\./g, "").replace(/\,/g, "."));
                    			if (number > 0) {
                    				$(nRow).find("td:eq(" + i.toString() + ")").addClass("positivo");
                    			}
                    			else if (number < 0) {
                    				$(nRow).find("td:eq(" + i.toString() + ")").addClass("negativo");
                    			}
                    		}
                    	}
                    	return nRow;
                    },
                    "aaSorting": [[1, "asc"]],
                    "bSort": true,
                    "bAutoWidth": false,
                    "bInfo": true,
                    "iDisplayLength": 10,
                    "bDestroy": true,
                    "oLanguage": {
                        "sLengthMenu": "<span>Exibir:</span> _MENU_ <span>por página</span>",
                        "sZeroRecords": "Sua pesquisa não encontrou resultados válidos. Tente novamente.",
                        "oPaginate": {
                            "sPrevious": "Anteriores",
                            "sNext": "Próximos"
                        }
                    },
                    "sPaginationType": "full_numbers",
                    "sDom": 'rtip',
                    "fnDrawCallback": function () {
                        var totalRecords = this.fnSettings().fnRecordsDisplay();
                        var emptyArea = $("#emptyArea");
                        var tableArea = $("#tableArea");

                        var excelLink = $("#lnkTermExcel");
                        var pdfLink = $("#lnkTermPdf");

                        if (excelLink.length > 0 && pdfLink.length > 0) {
                            if (totalRecords == 0) {
                                $(excelLink).click(function () { return false; });
                                $(pdfLink).click(function () { return false; });
                            }
                            else {
                                $(excelLink).click(function () { Export(false); return false; });
                                $(pdfLink).click(function () { Export(true); return false; });
                            }
                        }

                        $("#loadingArea").hide();

                        if (totalRecords == 0) {
                            emptyArea.html(emptyDataMessage);
                            emptyArea.show();
                            tableArea.hide();
                        }
                        else {
                            tableArea.show();
                            emptyArea.hide();

                            var totalPerPage = this.fnSettings()._iDisplayLength;

                            var totalPages = Math.ceil(totalRecords / totalPerPage);

                            var currentIndex = parseInt(this.fnSettings()._iDisplayStart);
                            var currentPage = totalPages > 0 ? Math.ceil(1 + (currentIndex / totalPerPage)) : 0;

                            $("#termList_info").html("Exibindo " + currentPage + " de " + totalPages);

                            if (totalRecords > 0 && totalPages > 1) {
                                $("#termList_info").css("display", "");
                                $("#termList_length").css("display", "");
                                $("#termList_paginate").css("display", "");
                            }
                            else {
                                $("#termList_info").css("display", "none");
                                $("#termList_length").css("display", "none");
                                $("#termList_paginate").css("display", "none");
                            }
                            $('select', '#termList_length').styleInputs();
                        }
                        // Adiciona a classe 'ultimo' à última tr no corpo da tabela
                        $('table tbody tr:last-child').addClass('ultimo');

                        // Adiciona a classe 'ultimo' às últimas th e td no corpo da tabela
                        $('table tr td:last-child').addClass('ultimo');
                        $('table tr th:last-child').addClass('ultimo');

                        $('table thead tr th').remove('direita');
                    },
                    "bProcessing": false,
                    "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                        $('#termList_first').css("visibility", "hidden");
                        $('#termList_last').css("visibility", "hidden");
                    },
                    "bRetrieve": true,
                    "bServerSide": true,
                    "sAjaxSource": "../DataTables/Term_List",
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        $.ajax({
                            cache: false,
                            dataType: 'json',
                            type: "POST",
                            url: sSource,
                            data: aoData,
                            success: fnCallback
                        });
                    }
                };

                oTable = $("#termList").dataTable(obj);
                oTable.fnClearTable();
                oTable.fnDraw();
            }
        });
        return false;
    }
    else {
        $("#filterFeedbackError", context).html(feedback).show();
    }
};

$(document).ready(function () {

    $("#chkAll").click(function () {
        var check = $(this).is(':checked');
        $("table#termList input:checkbox").each(function () { if (!$(this).is(':disabled')) { $(this).attr('checked', check); } })
    });

    $('.closeLink').bind({
        "click": function () {
            $('.modal-situacao').hide();
            return false;
        }
    });

    var context = $('#TermFilterContent'); 

    $("#btnSearch", context).click(function () {

        fnSearch(true, context);
        return false;
    });

    $(context).bind('keydown', function (event) {
        if (event.which == 13) {
            fnSearch(true, context);
            return false;
        }
    });

    ReloadResults();

});

// Função do botão Liquidar
function OpenSettle() {
    $("#feedbackError").html("").css({ 'display': 'none' });
    var checkedCorrections = $("[id^=chkTerm]:checked");
    if ($("[id^=chkTerm]:checked").length > 0) {
        var ids = "";
        $(checkedCorrections).each(function () {
            var id = $(this).attr('id').split("-")[1];
            if (id != "0") {
                ids += id + ";";
            }
            else {
                $(this).attr('checked', false);
            }
        });

        if (ids != "") {
            var url = "/Term/TermConfirmSettle/?termIds=" + ids;
            commonHelper.OpenModal(url, 450, ".content", "");
            $("#MsgBox").center();
        } else {
            $("#feedbackError").html("Nenhum termo para liquidar selecionado.").css({ 'display': 'block' });
            $("#termList #chkAll").attr('checked', false);
        }
    }
    else {
        $("#feedbackError").html("Nenhum termo para liquidar selecionado.").css({ 'display': 'block' });
    }
}

// Função da modal para alterar situação in-line
function ValidateSettle() {
    var currentStatus = $("#hdfCurrentStatus", ".modal-situacao").val();
    var ddlValue = $("#ddlStatusChange", ".modal-situacao").val()
    if (ddlValue != "" && ddlValue != currentStatus) {
        $(".closeLink", ".modal-situacao").click();
        DoSettle();
    }
    else {
        $("#feedbackStatus", ".modal-situacao").show();
        return false;
    }
}

function DoSettle() {
    $.post("/Term/SettleTerm", { termIds: $("#termIds", "#frmTermConfirmSettle").val() }, function (data) {
        if (data.success) {
            $("span", "#feedback").html(data.message);
            $("#feedback").show();
            ReloadResults();
        }
        else {
            $("#FeedbackError").html(data.message);
            $("#FeedbackError").show();
        }
        Modal.Close();
    }, "json");   
}

// Função para checar todos os checkbox
function CheckAll(checkAll) {
    var context = $("#termList").find("tbody");
    var checked = $(checkAll).is(':checked');
    $("input:checkbox:not(:disabled)", context).each(function () {
        $(this).attr('checked', checked);
    });
}

function Export(pdf) {
    var url = "/Term/ConfirmExport/?isPDF=" + pdf;
    commonHelper.OpenModal(url, 330, ".content", "");
    $("#MsgBox").center();
}

function ViewHistory() {
    commonHelper.OpenModal("/Term/TermHistory", 671, ".content", "");
}

function OpenHistoryDetail(id) {
    var url = "/Term/TermHistoryDetail/?termID=" + id;
    commonHelper.OpenModal(url, 671, ".content", "");
    $("#MsgBox").center();
}

function DeleteRequest(id) {
    var url = "/Term/TermConfirmDelete/?termId=" + id;
    commonHelper.OpenModal(url, 450, ".content", "");
    $("#MsgBox").center();
}

function ValidateFilterFields(context) {
    var messageError = "";

    if (!validateHelper.ValidateList($("#txtAssessors", context).val()))
        messageError += "<li>Faixa de assessores inválida.</li>";

    if (!validateHelper.ValidateList($("#txtClient", context).val()))
        messageError += "<li>Faixa de clientes inválida.</li>";

    if (messageError.length > 0)
        messageError = "<ul>" + messageError + "</ul>";

    return messageError;
}

function OpenRequest(id) {
    var tableContext = $("#tableArea");
    var totalChecked = $("input:checked", $("tbody", tableContext)).length;
    var count = $("[id^=chkTerm-0]:checked").length;
    if (count == 0 && totalChecked == 0) {
        commonHelper.OpenModal("/Term/TermRequest/" + id, 600, ".content", "");
        $("#MsgBox").center();
    }
    else {
        if (count > 0) {
            var list = "";
            $("[id^=chkTerm-0]:checked").each(function () {
                list += this.value + ";";
            });

            commonHelper.OpenModal("/Term/TermLotRequest/?lstIndexes=" + list, 650, ".content", "");
            $("#MsgBox").center();
        }
        else
            return false;
    }

}

function ChangeStatus(self, id, statusId) {
    $("#termIds").val(id);
    $('.modal-situacao').show();
    var offset = $(self).offset();
    $('.modal-situacao').css("top", offset.top);
    $("#hdfCurrentStatus", ".modal-situacao").val(statusId);
    $("#feedbackStatus", ".modal-situacao").hide();
    $('.modal-situacao select').styleInputs();
}


function ReloadResults() {
    setTimeout(function () {
        fnSearch(false, $('#TermFilterContent'));
        ReloadPending();
    }, 250);
}


function ReloadPending() {
    $.ajax({
        url: "/Home/GetPending",
        type: "POST",
        dataType: "json",
        async: false,
        data: { module: "Term.Index" },
        success: function (data) {
            if (data > 0) {
                var pendingContent = "<span><strong>" + data + "</strong> Pendente" + (data > 1 ? "s" : "") + "</span>";

                if ($("#pendingTag").length > 0) {
                    $("#pendingTag").html(pendingContent);
                }
                else {
                    var pending = "<div id='pendingTag' class='aviso' style='display: none;'>" + pendingContent + "</div>";
                    if ($('#trilha').children('ul').find('li').last().length > 0)
                        $('#trilha').children('ul').find('li').last().append(pending);
                    else
                        $('#trilha').children('ul').append(pending);
                }
                $("#pendingTag").show();
            }
            else {
                $("#pendingTag").hide();
            }
        }
    });

}