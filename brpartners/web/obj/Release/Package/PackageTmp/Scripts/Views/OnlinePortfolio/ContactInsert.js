﻿var currentStockIndex = 1;

$(document).ready(function () {

    var context = "#pnlContactInsert";

    $("#txtDate", context).setMask("99/99/9999").datepicker({ maxDate: "+0d" }).datepicker($.datepicker.regional['pt-BR']);
    $.datepicker.setDefaults($.datepicker.regional['pt-BR']);

    $("#lnkContactInsert", context).click(function () {
        var feedback = ValidateFields(context);
        if (feedback == "") {
            $("#frmContactInsert").submit();
        }
        else {
            $("#feedbackRequestError", context).html(feedback);
            $("#feedbackRequestError", context).show();
            return false;
        }
    });

    $("#txtClient", context).autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/OnlinePortfolio/ClientSuggest",
                type: "POST",
                dataType: "json",
                data: { term: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        return {
                            label: item.Description,
                            value: item.Description,
                            id: item.Value
                        }
                    }));
                }
            });
        },
        minLength: 3,
        select: function (event, ui) {
            $("#hdfClientCode", context).val(ui.item.id);
            $("#hdfClientName", context).val(ui.item.value);
        }
    });
})

function ValidateFields(context) {
    var feedback = "";
    var change = $('input[name=rblType]:checked', context).val() == 6;

    if ($("#hdfClientCode", context).val() == "") {
        feedback += "<li>Cliente em branco.</li>";
    }
    if ($("#hdfClientName", context).val() != $("#txtClient", context).val()) {
        feedback += "<li>Cliente inválido.</li>";
    }
     if ($("#txtDate", context).val() == "") {
        feedback += "<li>Data em branco.</li>";
    }
     if ($("#txtMessage", context).val() == "") {
        feedback += "<li>Mensagem em branco.</li>";
    }
   
    if (feedback.length > 0)
        feedback = "<ul>" + feedback + "</ul>";
    return feedback;
}
