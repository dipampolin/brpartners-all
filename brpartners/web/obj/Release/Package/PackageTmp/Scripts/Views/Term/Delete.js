﻿$(document).ready(function () {

    var context = $("#frmConfirmTermDelete");

    $("#btnDelete", context).click(function () {
        $.post("/Term/DeleteTerm", { hdfTermId: $("#hdfTermId", context).val() }, function (data) {
            if (data.success) {
                $("span", "#feedback").html(data.message);
                $("#feedback").show();
                ReloadResults();
            }
            else {
                $("#FeedbackError").html(data.message);
                $("#FeedbackError").show();
            }
            Modal.Close();
        }, "json");
    });

});
