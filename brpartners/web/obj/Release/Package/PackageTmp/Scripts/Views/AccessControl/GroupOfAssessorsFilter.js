﻿$(function () {
	var context = $('#groupOfAssessorsFilterContent');

	$("#groupOfAssessorsFilter").click(function () {
	    $('#groupOfAssessorsFilterContent').slideToggle('fast');
	    $('select').styleInputs();
	    $(this).toggleClass('aberto');

		$("#txtUsers", context).autocomplete({
			source: function (request, response) {
				$.ajax({
				    url: "/AccessControl/UserAutoSuggest",
					type: "POST",
					dataType: "json",
					data: { term: request.term },
					success: function (data) {
						$("#hdfUser", context).val("");
						response($.map(data, function (item) {
							return {
								label: item.Name,
								value: item.Name,
								id: item.ID
							}
						}));
					}
				});
			},
			minLength: 1,
			select: function (event, ui) {
				$("#hdfUser", context).val(ui.item.id);
			}
		});

	});
});

