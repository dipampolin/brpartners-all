﻿function adicionaEmail() {
    let email = getElement('txtEmail').value;

    if (email != "") {
        let listbox = document.querySelector("#lstEmail");

        var option = createElement("option");
        option.text = email;
        option.value = email;

        var existValue;

        if (listbox.length == 0) {
            listbox.add(option);
            adicionaEmailPrincipal();
            return;
        }

        for (let i = 0; i < listbox.options.length; i++) {
            existValue = listbox.options[i].value == email;
            if (existValue) break;
        }

        if (!existValue) {
            listbox.add(option);
            adicionaEmailPrincipal();
        }
    }
}

function removeEmail() {
    var listbox = getElement("lstEmail");
    let valor = listbox.options[listbox.selectedIndex].value;

    listbox.remove(listbox.selectedIndex);
    removeEmailPrincipal(valor);
}

function adicionaEmailPrincipal() {
    let email = getElement('txtEmail').value;
    let cbxEmailPrincipal = getElement('EmailPrincipal');

    var option = createElement("option");
    option.text = email;
    option.value = email;
    cbxEmailPrincipal.add(option);
}

function removeEmailPrincipal(val) {
    $('#EmailPrincipal option[value=' + val + ']').remove();
}

function displayElement(elementChoice, element) {
    var value = elementChoice.options[elementChoice.selectedIndex].text;

    if (value.toLowerCase() == 'sim') {
        changeDisplay(element.id, "block");
    }
    else {
        changeDisplay(element.id, "none");
    }
};

function changeConjugueUniaoEstavel(val, element) {
    var select = document.getElementById("CommonLaw");
    var selectedText = select.options[select.selectedIndex].text;

    if (selectedText.toLowerCase() == 'sim') {
        changeDisplay(element.id, "block");
    }
    else {
        changeDisplay(element.id, "none");
    }
}

function changeConjugueEstadoCivil(element) {
    var select = document.getElementById("MaritalStatus");
    var selectedText = select.options[select.selectedIndex].text;

    if (selectedText.toLowerCase() == 'casado') {
        changeDisplay(element.id, "block");
    }
    else {
        changeDisplay(element.id, "none");
    }
}

function changeDocumentType() {
    var select = document.getElementById("Estrangeiro");
    var selectedText = select.options[select.selectedIndex].text;

    if (selectedText.toLowerCase() == 'sim') {
        var selectOptions = document.querySelector("#DocumentType").options;

        for (var i = 0; i < selectOptions.length; i++) {
            var valueText = selectOptions[i].innerHTML;

            if (valueText.toLowerCase() == 'rne') {
                selectOptions[i].selected = 'selected'
            }
        }

        document.getElementById('fatcaContent').style.display = 'block';
    }
    else
        document.getElementById('fatcaContent').style.display = 'none';
}

function addNewContact() {
    createElementTable('.contato', 'contatoTable')
}

function addNewAddress() {
    createElementTable('.endereco', 'enderecoTable')
}

function createElementTable(css, table) {
    var inputs = document.querySelectorAll(css);
    var elementTr = document.createElement('tr');
    var table = document.querySelector('#' + table);
    table.appendChild(elementTr);

    inputs.forEach(function(input) {
        var newElem = document.createElement('td');

        if (input.type == "select-one") {
            newElem.textContent = input.options[input.selectedIndex].text;
            newElem.id = input.options[input.selectedIndex].value;
        }
        else {
            newElem.textContent = input.value;
            newElem.id = input.value;
        }

        newElem.classList.add('table-td');
        elementTr.appendChild(newElem);
    });

    var TdInput = document.createElement('td');
    TdInput.classList.add('ultimo');
    TdInput.classList.add('table-td');

    var elementInput = document.createElement("a");
    elementInput.className = 'ico-excluir';

    if (table.id == 'enderecoTable')
        elementInput.style.margin = "0px 49px";
    else
        elementInput.style.margin = "0px 29px";

    TdInput.appendChild(elementInput);
    elementTr.appendChild(TdInput);

    elementInput.addEventListener('click', function(event) {
        removeParent(event);
    });

    hiddenElementTable();
}

function removeParent(event) {
    event.target.parentNode.parentNode.classList.add('fadeOut');

    setTimeout(function() {
        event.target.parentNode.parentNode.remove();
        hiddenElementTable();
    }, 500);
}

function getElement(val) {
    return document.getElementById(val);
}

function createElement(element) {
    return document.createElement(element);
}

function changeDisplay(element, display) {
    document.getElementById(element).style.display = display;
}

function createElementInput(element, css, idElement, type, width) {
    var element = document.createElement(element)
    element.id = idElement;
    element.type = type;
    element.style.width = width;

    if (css != '')
        element.classList.add(css);

    return element;
}

function createElement(element, css, textContentElement) {
    var element = document.createElement(element)
    element.textContent = textContentElement;

    if (css != '')
        element.classList.add(css);

    return element;
}

function carregaTabela(values) {
    alert(values);
}

function hiddenElementTable() {
    var rows = document.getElementById('enderecoTable').getElementsByTagName("tr").length
    if (rows > 1) {
        document.getElementById('enderecoTable').style.display = '';
    }
    else
        document.getElementById('enderecoTable').style.display = 'none';

    var rows1 = document.getElementById('contatoTable').getElementsByTagName("tr").length
    if (rows1 > 1) {
        document.getElementById('contatoTable').style.display = '';
    }
    else
        document.getElementById('contatoTable').style.display = 'none';
}

function changeLocalNasc(event) {
    var select = document.getElementById("PaisNascimento");
    var selectedText = select.options[select.selectedIndex].text;

    if (selectedText.toLowerCase() == 'outros') {
        displayElementByClass('.outros', 'block');
        displayElementByClass('.brasil', 'none');
    }
    else if (selectedText.toLowerCase() == 'brasil') {
        displayElementByClass('.outros', 'none');
        displayElementByClass('.brasil', 'block');
    }
}

function displayElementByClass(nameClass, nameDisplay) {
    var elements = document.querySelectorAll(nameClass)

    elements.forEach(function(element) {
        element.style.display = nameDisplay;
    });
}

// ------------------------- ASSINATURA ---------------------------------------------

function downloadFile() {
    var listbox = getElement("lstAssinatura");
    let valor = listbox.options[listbox.selectedIndex].value;
    window.location = '/Account/DownloadFile?idFile=' + valor;
}

function removeFile() {
    var listbox = getElement("lstAssinatura");
    let valor = listbox.options[listbox.selectedIndex].value;

    $.ajax({
        url: '/Account/RemoveFile/',
        data: { idFile: valor },
        type: 'GET',
        datatype: 'json',
        success: function(returnValue) {
            if (returnValue.toLowerCase() == 'true') {
                listbox.remove(listbox.selectedIndex);
            }
        }
    });
}

window.onload = function() {

    let elements = document.querySelectorAll('.desabilitar-cliente');

    for (var i = 0; i < elements.length; i++) {
        document.getElementById(elements[i].id).disabled = true;
    }

    var elementWorkGovernment = document.getElementById('WorkGovernment');
    var elementRelativeWorkGovernment = document.getElementById('RelativeWorkGovernment');
    var elementEstrangeiro = document.getElementById('Estrangeiro');
    var elementCommonLaw = document.getElementById('CommonLaw');
    var elementMarital = document.getElementById('MaritalStatus');

    if (elementWorkGovernment.options[elementWorkGovernment.selectedIndex].text.toLowerCase() == 'sim')
        document.getElementById('funcaoPublica').style.display = 'block';

    if (elementRelativeWorkGovernment.options[elementRelativeWorkGovernment.selectedIndex].text.toLowerCase() == 'sim')
        document.getElementById('parenteFuncaoPublica').style.display = 'block';

    if (elementEstrangeiro.options[elementEstrangeiro.selectedIndex].text.toLowerCase() == 'sim')
        document.getElementById('fatcaContent').style.display = 'block';

    if (elementCommonLaw.options[elementCommonLaw.selectedIndex].text.toLowerCase() == 'sim' || elementMarital.options[elementMarital.selectedIndex].text.toLowerCase() == 'casado')
        document.getElementById('conjugueContent').style.display = 'block';

    if ("@displayform" == "False") {
        $('#corpoContent').find("input, button, submit, textarea, select").attr("disabled", "disabled");
    }

    hiddenElementTable();
}