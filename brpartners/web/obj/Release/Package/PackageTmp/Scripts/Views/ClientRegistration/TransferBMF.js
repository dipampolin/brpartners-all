﻿var oTableBMF = null;

var objBMFFilter = {
    "bAutoWidth": false
    , "bPaginate": true
    , "bDestroy": true
    , "sPaginationType": "full_numbers"
    , "sDom": 'trip'
    , "oLanguage": {
        "sLengthMenu": "Exibindo _MENU_ registros",
        "sZeroRecords": "Nenhum registro encontrado.",
        "sInfo": "Encontramos _TOTAL_ registros",
        "sInfoEmpty": "Nenhum registro encontrado",
        "sInfoFiltered": "(Filtrando _MAX_ registros)",
        "sSearch": "Buscar:",
        "sProcessing": "Processando...",
        "oPaginate": {
            "sFirst": "Primeiro",
            "sPrevious": "Anterior",
            "sNext": "Próximo",
            "sLast": "Último"
        }
    }
    , "bProcessing": true
    , "bServerSide": true
    , "sAjaxSource": "../ClientRegistration/TransferBMFFilter"
    , "fnServerData": function (sSource, aoData, fnCallback) {

        var context = $('#TransferBMFContent');

        if (aoData != null && aoData != undefined) {
            aoData.push({ "name": "txtClient", "value": $('#txtClient', context).val() });
            aoData.push({ "name": "hdfBroker", "value": $('#hdfBroker', context).val() });
            aoData.push({ "name": "ddlType", "value": $('#ddlType', context).val() });
            aoData.push({ "name": "ddlSituation", "value": $('#ddlSituation', context).val() });

        }
        $.ajax({
            "cache": false,
            "dataType": 'json',
            "type": "POST",
            "url": sSource,
            "data": aoData,
            "beforeSend": function () {
                $("#table-area").hide();
                $("#loadingArea").show();
                $("#emptyArea").hide();
            },
            "success": function (data) {
                fnCallback(data);

                if (data.iTotalRecords == 0) {
                    $("#table-area").hide();
                    $('#emptyArea').html('Sua pesquisa não encontrou resultados válidos. Tente novamente.').show();
                }
                else {
                    $('#emptyArea').html('').hide();
                }
            }
        });
    }
    , "fnInitComplete": function () {
        oTableBMF.fnAdjustColumnSizing();
    }
    , fnDrawCallback: function () {
        $("#checkAll").attr("checked", false);

        $("#loadingArea").hide();
        $("#table-area").show();

        var totalRecords = this.fnSettings().fnRecordsDisplay();

        if (totalRecords <= 0) {
            $("#lnkExcel").hide();
            $("#lnkPdf").hide();
            $("#dvExportar").hide();


        }
        else {
            $("#lnkExcel").show();
            $("#lnkPdf").show();
            $("#dvExportar").show();

            TransferBMFMethods.enableExportButtons();
        }

        var totalPerPage = this.fnSettings()._iDisplayLength;

        var totalPages = Math.ceil(totalRecords / totalPerPage);

        var currentIndex = parseInt(this.fnSettings()._iDisplayStart);
        var currentPage = totalPages > 0 ? Math.ceil(1 + (currentIndex / totalPerPage)) : 0;

        $("#tblTransferBMF_info").html("Exibindo " + currentPage + " de " + totalPages);

        if (totalRecords > 0 && totalPages > 1) {
            $("#tblTransferBMF_info").css("display", "");
            $("#tblTransferBMF_length").css("display", "");
            $("#tblTransferBMF_paginate").css("display", "");
        }
        else {
            $("#tblTransferBMF_info").css("display", "none");
            $("#tblTransferBMF_length").css("display", "none");
            $("#tblTransferBMF_paginate").css("display", "none");
        }

        TransferBMFMethods.insertClientBond();

        TransferBMFMethods.checkClients();

        TransferBMFMethods.editClient();

        TransferBMFMethods.blockChars();
    }
    , aoColumns: [
        null,
        null,
        null,
        null,
        null,
        null,
        { bSortable: false }
    ]
};

var TransferBMFMethods = {
    /*enableExportButtons: function () {
    var data = $("#tblTransferBMF tbody tr");
    var emptyData = (data.length == 1 && data[0].lastChild.className == "dataTables_empty") ? true : false;
    var excelLink = $("#lnkExcel");
    var pdfLink = $("#lnkPdf");

    var fnExportFilter = function (isPDF) {
    $.ajax({
    type: "POST",
    cache: false,
    async: false,
    url: "../ClientRegistration/TransferBMFFilterOptions",
    data: {
    txtAssessorFilter: $('#txtAssessorFilter').val(),
    txtClientFilter: $('#txtClientFilter').val(),
    txtStartDate: $('#txtStartDate').val(),
    txtEndDate: $('#txtEndDate').val(),
    chkbmf: $('#chkbmf').is(":checked"),
    chkbovespa: $('#chkbovespa').is(":checked"),
    iSortCol_0: oTableBMF.fnSettings().aaSorting[0][0],
    sSortDir_0: oTableBMF.fnSettings().aaSorting[0][1]
    },
    beforeSend: function () {
    $("#feedbackError").remove();
    },
    success: function (data) {
    if (data)
    window.location = "/ClientRegistration/TransferBMFToFile/?isPdf=" + isPDF;
    else {
    $("#emptyArea").before("<div id='feedbackError' class='erro'>Ocorreu um erro na hora da exportação.</div>");
    }
    }
    });
    };

    excelLink.unbind("click");
    excelLink.bind("click", function () {
    fnExportFilter("false");
    });

    pdfLink.unbind("click");
    pdfLink.bind("click", function () {
    fnExportFilter("true");
    });
    },*/
    enableExportButtons: function () {
        var data = $("#tblTransferBMF tbody tr");
        var emptyData = (data.length == 1 && data[0].lastChild.className == "dataTables_empty") ? true : false;
        if (!emptyData) {
            $("#lnkExcel").attr("href", "/ClientRegistration/TransferBMFToFile/?isPdf=false");
            $("#lnkPdf").attr("href", "/ClientRegistration/TransferBMFToFile/?isPdf=true");
        }
    }
    , ValidateFilterFields: function () {
        var messageError = "";
        /*if (!validateHelper.ValidateList($("#txtAssessorFilter").val()))
        messageError += "<li>Faixa de assessores inválida.</li>";*/

        if ($("#txtClient").val() != "" && isNaN($("#txtClient").val()))
            messageError += "<li>Código de cliente inválido.</li>";

        if (messageError.length > 0)
            messageError = "<ul>" + messageError + "</ul>";

        return { Result: (messageError == ""), Message: messageError }
    },
    insertClientBond: function () {
        $(".lnkInserir").unbind('click').bind("click", function () {
            var that = $(this);
            var tr = $(this).parent().parent();
            var ids = tr.find('.vinculo-id').val();
            var bondId = ids.split("|")[0];
            var brokerId = ids.split("|")[1];
            var clientId = tr.find('.input-cliente').val();

            if (clientId != undefined && clientId != "") {

                $.ajax({
                    type: "POST",
                    url: "../ClientRegistration/CheckClientBMF",
                    cache: false,
                    data: { "clientId": clientId },
                    beforeSend: function () {
                        $("#feedbackError").hide().find('span').html('');
                    },
                    success: function (data) {
                        if (data.Result == true) {
                            $.ajax({
                                type: "POST",
                                url: "../ClientRegistration/InsertClientBond",
                                cache: false,
                                data: { bondId: bondId, clientId: clientId, brokerId: brokerId },
                                beforeSend: function () {
                                    $("#feedbackError").hide().find('span').html('');
                                },
                                success: function (data) {
                                    if (data.Executed) {
                                        oTableBMF.fnDraw(false);
                                        $("#feedback").show().find('span').html('Vínculo atualizado com sucesso.');
                                    }
                                    else {
                                        $("#feedbackError").show().find('span').html('Não foi possível inserir o vínculo.');
                                    }
                                    /*var tds = tr.find('td');
                                    tds.eq(2).html(clientId);
                                    tds.eq(3).html(data.ClientName);
                                    tds.eq(4).html(data.CPFCGC);
                                    that.remove();*/
                                }
                            });
                        }
                        else {
                            var tds = tr.find('td');
                            tds.eq(2).find('.negativo').remove();
                            tds.eq(2).append("<span class='negativo feedback'>Cliente não existente</span>");
                        }
                    }
                });

            }
        });
    }
    , checkClients: function () {
        $(".input-cliente").unbind("blur").bind("blur", function () {
            if ($(this).val() != "" && $(this).val() != undefined) {
                var that = $(this);
                var tr = $(this).parent().parent();
                $.ajax({
                    type: "POST",
                    url: "../ClientRegistration/CheckClientBMF",
                    cache: false,
                    data: { clientId: that.val() },
                    success: function (data) {
                        var tds = tr.find('td');
                        if (data.Result == false) {
                            tds.eq(2).find('.negativo').remove();
                            tds.eq(2).append("<span class='negativo feedback'>Cliente não existente</span>");
                            that.val('');
                            tds.eq(3).html("-");
                            tds.eq(4).html("-");
                        }
                        else {
                            tds.eq(2).find('.negativo').remove();
                            tds.eq(3).html(data.ClientName);
                            tds.eq(4).html(data.CPFCNPJ);
                        }
                    }
                });
            }
        });
    }
    , editClient: function () {
        $(".lnkEditar").unbind('click').bind('click', function () {
            var tr = $(this).parent().parent();
            var tds = tr.find('td');
            tds.eq(2).html($("<input type='text' id='input_" + Math.floor((Math.random() * 100000) + 1).toString() + "' class='input-cliente' value='" + tds.eq(2).html() + "' />").clone());
            tds.eq(6).html($("<a class='lnkInserir' href='javascript:;'><span title='Salvar' class='ico-salvar'>Salvar</span></a>").clone());

            TransferBMFMethods.insertClientBond();
            TransferBMFMethods.checkClients();

            TransferBMFMethods.blockChars();
        });
    }
    , viewHistory: function () {
        commonHelper.OpenModal("/ClientRegistration/TransferBMFHistory", 671, ".content", "");
    }
    , blockChars: function () {
        $(".input-cliente").unbind("keypress").bind("keypress", function (e) {
            var tecla = (window.Event) ? e.which : e.keyCode;

            if ((tecla == 0 || tecla == 8 || tecla == 9 || tecla == 13 || tecla == 33 || (tecla >= 48 && tecla <= 57)) && ($(this).val().length <= 7 || ($(this).val().length > 7 && tecla == 8))) {
                return true;
            }
            return false;
        });
    }
};

$(function () {
    if (oTableBMF != null)
        oTableBMF = null;
    oTableBMF = $("#tblTransferBMF").dataTable(objBMFFilter);

    var context = $('#TransferBMFContent');


    $("#TransferBMFFilter").click(function () {
        $(context).slideToggle('fast');
        $('select').styleInputs();
        $(this).toggleClass('aberto');
    });

    $("#lnkViewHistory").click(function () {
        TransferBMFMethods.viewHistory();
    });

    $("#txtBroker").keyup(function () { $(this).val($(this).val().toUpperCase()); }).autocomplete({
        source: function (request, response) {
            $.ajax({
                cache: false,
                url: "/NonResidentTraders/LoadBrokersTransfer",
                type: "POST",
                dataType: "json",
                data: { filter: $("#txtBroker").val() },
                success: function (data) {
                    response($.map(data, function (item) {
                        return {
                            label: item.BrokerCode + " - " + item.BrokerName
                            , value: item.BrokerName
                            , code: item.BrokerCode
                        }
                    }));
                }
            });
        }
        , minLength: 3
        , select: function (event, ui) {
            $("#hdfBroker").val(ui.item.code);
        }
        , change: function () {
            
            $.ajax({
                cache: false,
                url: "/NonResidentTraders/LoadBrokersTransfer",
                type: "POST",
                dataType: "json",
                async: false,
                data: { filter: $("#txtBroker").val() },
                success: function (data) {
                    var find = false;
                    if (data.length > 0) {
                        for (var i = 0; i < data.length; i++) {
                            if (data[i].BrokerName == $("#txtBroker").val()) {
                                $("#hdfBroker").val(data[i].BrokerCode);
                                find = true;
                                break;
                            }
                        }
                        if (!find)
                            $("#hdfBroker").val("");
                    }
                    else {
                        
                        $("#hdfBroker").val("");
                    }
                }
            });
        }

    });

    $("#btnRequest").unbind("click").bind("click", function () {
        var allSaved = true;
        var counter = 0;

        $(".input-cliente").each(function () {
            if ($(this).val() != null) {

                var that = $(this);
                var tr = $(this).parent().parent();
                var ids = tr.find('.vinculo-id').val();
                var bondId = ids.split("|")[0];
                var brokerId = ids.split("|")[1];
                var clientId = tr.find('.input-cliente').val();

                if (clientId != undefined && clientId != "" && clientId != null) {
                    counter++;
                    $.ajax({
                        type: "POST",
                        url: "../ClientRegistration/CheckClientBMF",
                        cache: false,
                        async: false,
                        data: { clientId: clientId },
                        success: function (data) {
                            if (data.Result == true) {
                                $.ajax({
                                    type: "POST",
                                    url: "../ClientRegistration/InsertClientBond",
                                    cache: false,
                                    async: false,
                                    data: { bondId: bondId, clientId: clientId, brokerId: brokerId },
                                    success: function (data) {
                                        if (!data.Executed && allSaved) {
                                            allSaved = false;
                                        }
                                    }
                                });
                            }
                            else {
                                if (!data.Result && allSaved) {
                                    allSaved = false;
                                }
                            }
                        }
                    });

                }
            }
        });

        if (!allSaved && counter > 0) {
            $("#feedback").hide();
            $("#feedbackError").show().find('span').html('Alguns vínculos não puderam ser salvos.');
            $("#emptyArea").hide();
            oTableBMF.fnDraw(false);
        }
        else if (allSaved && counter > 0) {
            $("#feedbackError").hide();
            $("#feedback").show().find('span').html('Vínculos salvos com sucesso.');
            $("#emptyArea").hide();
            oTableBMF.fnDraw(false);
        }
        else if (counter == 0) {
            $("#feedback").hide();
            $("#feedbackError").hide();
            $("#emptyArea").show().html('Nenhum item foi salvo.');
        }

    });

    $(context).bind('keydown', function (event) {
        if (event.which == 13) {
            $("#lnkSubmitTransferBMFFilter").click();
        }
    });

    $("#lnkSubmitTransferBMFFilter").unbind("click");
    $("#lnkSubmitTransferBMFFilter").bind("click", function () {

        var feedback = TransferBMFMethods.ValidateFilterFields();

        if (feedback.Result) {
            $("#filterFeedbackError").html("").hide();
            $("#TransferBMFContent").hide();
            $("#loadingArea").show();
            $("#table-area").hide();
            $("#feedback").hide().find("span").html('');
            $("#feedbackError").hide().find("span").html('');

            if (oTableBMF != null)
                oTableBMF.fnDestroy();

            oTableBMF = $("#tblTransferBMF").dataTable(objBMFFilter);

        }
        else {
            $("#filterFeedbackError").html(feedback.Message).show();
        }
    });
});
