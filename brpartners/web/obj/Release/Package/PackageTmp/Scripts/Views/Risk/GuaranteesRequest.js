﻿/// <reference path="../../References.js" />
var guaranteeCount = 1;
var guaranteeDepCount = 1;

$(document).ready(function () {
    $('select').styleInputs();
    $.mask.masks.Value = { mask: '99,999.999.999.999', type: 'reverse', defaultValue: '000' };
    $.mask.masks.Quantity = { mask: '999.999.999', type: 'reverse', defaultValue: '0' };
    $.mask.masks.clientCode = { mask: "9999999" };

    var context = "#frmGuaranteeRequest";

    //    $("input[id*='txtDepGuarantee']").each(function () { 
    //        
    //    });

    $("#lnkGuaranteeRequest", context).click(function () {
        var feedback = ValidateFields(context);
        if (feedback == "") {
            DoRequest();
        }
        else {
            $("#feedbackRequestError", context).html(feedback);
            $("#feedbackRequestError", context).show();
            return false;
        }
        return false;
    });

    var isWithdrawal = $("input[name=rbIsWithdrawal]:checked", context);

    if (isWithdrawal.length > 0) {
        var isWithdrawal = isWithdrawal[0].value == "1";
        SetTransferPanel(isWithdrawal, context);
    }

    $("input[name=rbIsWithdrawal]", context).click(function () {
        var isWithdrawal = this.value == "1";
        SetTransferPanel(isWithdrawal, context);
    });



    $("#lnkAddGuarantee", context).click(function () {

        var newGuaranteeLine = "<tr><td><input id='txtGuarantee" + guaranteeCount + "' type='text' value='' name='txtGuarantee" + guaranteeCount + "' maxlength='146'></td>";
        newGuaranteeLine += "<td width='162px'><input id='txtQuantity" + guaranteeCount + "' type='text' value='' name='txtQuantity" + guaranteeCount + "'></td><td class='total' width='50px'><input id='hdfUnitPrice" + guaranteeCount + "' type='hidden' value='0' name='hdfUnitPrice" + guaranteeCount + "'><input id='hdfIsQuantity" + guaranteeCount + "' type='hidden' value='' name='hdfIsQuantity" + guaranteeCount + "'><input id='chkTotal" + guaranteeCount + "' name='chkTotal" + guaranteeCount + "' class='td-hide-deposit' type='checkbox'><label class='td-hide-deposit'>Total</label>";
        newGuaranteeLine += "<td class='td-hide-deposit direita'><label id='lblAvailable" + guaranteeCount + "'>-</label></td><td class='td-hide-deposit direita'><label id='lblTotal" + guaranteeCount + "'>-</label></td>";

        $("tbody", "#tbGuarantees").append($(newGuaranteeLine).clone());
        SetGuaranteesFields(guaranteeCount, context);

        guaranteeCount++;

        $("#hdfCounter", context).val(guaranteeCount);
    });

    $("#lnkAddGuaranteeDep", context).click(function () {

        var newGuaranteeLine = "<tr><td><input id='txtDepGuarantee" + guaranteeDepCount + "' type='text' style='text-transform: uppercase;' value='' name='txtDepGuarantee" + guaranteeDepCount + "' maxlength='146'></td>";
        newGuaranteeLine += "<td><input id='txtDepQuantity" + guaranteeDepCount + "' type='text' value='' name='txtDepQuantity" + guaranteeDepCount + "'></td>";
        newGuaranteeLine += "<td><input id='txtDepUnitPrice" + guaranteeDepCount + "' type='text' value='' style='display:none;' name='txtDepUnitPrice" + guaranteeDepCount + "' alt='Value'><label id='lblDepUnitPrice" + guaranteeDepCount + "'>-</label></td><td><label id='lblDepTotal" + guaranteeDepCount + "'>-</label></td><tr>";

        $("tbody", "#tbGuaranteesDeposit").append($(newGuaranteeLine).clone());
        SetGuaranteesFields(guaranteeDepCount, context);

        guaranteeDepCount++;

        $("#hdfDepositCounter", context).val(guaranteeDepCount);
    });


    $("#txtClient", context).blur(function () {
        commomDataHelper.GetClientName($(this).val(), "lblClientName", context, "txtClient");
        $("input[id^='txtGuarantee']").val("");
        $("input[id^='txtQuantity']").val("0");
        SetStockExchanges(context);
        $("#hdfClientName", context).val($("#lblClientName", context).html());
        $("#requestFeedbackError", context).hide();
    }).setMask();

    SetGuaranteesFields(0, context);

    if ($("#txtClient", context).val() != "") {
        SetEditFields(context);
    }
    else {

        $("input[name=rbIsWithdrawal]").click(function () {

            var value = $(this).val();
            if (value == "1") {
                $("#txtQuantity0").attr("alt", "Quantity").setMask();
            }
            else {
                $("#txtDepQuantity0").attr("alt", "Quantity").setMask();
            }
        });

        $("#txtClient", context).focus();
       
    }

    

    SetStockExchanges(context);
});

function DoRequest() {
    var form = $("#frmGuaranteeRequest").serialize();
    $.post("/Risk/GuaranteesRequest", form, function (data) {
        if (data.success) {
            $("span", "#feedback").html(data.message);
            $("#feedback").show();
            ReloadResults();
        }
        else {
            $("#FeedbackError").html(data.message);
            $("#FeedbackError").show();
        }
        Modal.Close();
    }, "json");

    return false;
}

function SetTransferPanel(isWithdrawal, context) {
    if (isWithdrawal) {
        $("#pnlRequestItemsDeposit", context).hide();
        $("#pnlRequestItemsWithdrawal", context).show();
    }
    else {
        $("#pnlRequestItemsWithdrawal", context).hide();
        $("#pnlRequestItemsDeposit", context).show();
    }
}

function SetGuaranteesFields(id, context) {

    var isWithdrawal = $("input[name=rbIsWithdrawal]:checked", context);

    if (isWithdrawal.length == 0 || isWithdrawal[0].value == "1") {
        $("#txtGuarantee" + id, context).autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/Risk/GuaranteesSuggest",
                    type: "POST",
                    dataType: "json",
                    data: { term: request.term, ClientCode: $("#txtClient", context).val(), IsBovespa: $("input[name=rbIsBovespa]:checked", context).val() },
                    success: function (data) {
                        response($.map(data, function (item) {
                            return {
                                label: item.Guarantee,
                                value: item.Guarantee,
                                id: item.Guarantee,
                                available: item.Available,
                                total: item.Total,
                                unitPrice: item.UnitPrice,
                                isQuantity: item.IsQuantity
                            }
                        }));
                    }
                });
            },
            minLength: 1,
            //appendTo: "#pnlGuaranteeRequest",
            select: function (event, ui) {
                var isQuantity = ui.item.isQuantity;
                if (isQuantity) {
                    $("#txtQuantity" + id, context).attr("alt", "Quantity").setMask();
                }
                else {
                    $("#txtQuantity" + id, context).attr("alt", "Value").setMask();
                }
                $("#lblAvailable" + id, context).html(ui.item.available);
                $("#lblTotal" + id, context).html(ui.item.total);
                $("#hdfUnitPrice" + id, context).val(ui.item.unitPrice);
                $("#hdfIsQuantity" + id, context).val(ui.item.isQuantity);
            }
        });

        $("#chkTotal" + id, context).click(function () {
            if ($(this).is(":checked")) {
                var guarantee = $("#txtGuarantee" + id, context).val().toLowerCase();
                var isQuantity = $("#hdfIsQuantity" + id).val() == "True";
                if (isQuantity) {
                    $("#txtQuantity" + id, context).attr("alt", "Quantity").setMask();
                    var quantity = $("#lblAvailable" + id, context).html();
                    if (quantity != "-")
                        $("#txtQuantity" + id, context).val(quantity);
                }
                else {
                    $("#txtQuantity" + id, context).attr("alt", "Value").setMask();
                    var total = $("#lblTotal" + id, context).html();
                    if (total != "-")
                        $("#txtQuantity" + id, context).val(total);
                }
            }
        });

        $("#txtQuantity" + id, context).blur(function () {
            //var money = $("#txtGuarantee" + id, context).val().toLowerCase().indexOf("dinheiro") >= 0;
            var money = $("#hdfIsQuantity" + id).val() == "False";
            var requested = 0;
            var total = 0;
            if (money) {
                requested = parseInt($(this).val().replace(/\./g, ''));
                total = parseInt($("#lblTotal" + id, context).html().replace(/\./g, ''));
            }
            else {
                requested = parseFloat($(this).val().replace(/\./g, ''));
                total = parseFloat($("#lblAvailable" + id, context).html().replace(/\./g, ''));
            }

            $("#chkTotal" + id, context).attr("checked", (requested == total))

            if (requested > total)
                $(this).val(money ? "0,00" : "0");
        });

        $("#txtQuantity" + id, context).attr("alt", "Quantity").setMask();
        
        $("#txtGuarantee" + id, context).blur(function () {
            var txtGuarantee = $(this);

            if (isWithdrawal) {
                $.ajax({
                    type: "POST",
                    url: "/Risk/ValidateGuaranteesSuggest/",
                    dataType: "json",
                    data: { guarantee: $(txtGuarantee).val() },
                    success: function (data) {
                        if (data != "true") {
                            $(txtGuarantee).val("");
                            $("#lblAvailable" + id, context).html("");
                            $("#lblTotal" + id, context).html("");
                            $("#hdfUnitPrice" + id, context).val("");
                            $("#hdfIsQuantity" + id, context).val("");
                        }
                    }
                });
                if ($("#txtGuarantee" + id, context).val() == "Nenhuma garantia encontrada")
                    $("#txtGuarantee" + id, context).val("");
            }

            var money = $("#hdfIsQuantity" + id).val() == "False";
            //var money = $(this).val().toLowerCase().indexOf("dinheiro") >= 0;
            if (!money) {
                $("#txtQuantity" + id, context).attr("alt", "Quantity").setMask();
            }
            else {
                $("#txtQuantity" + id, context).attr("alt", "Value").setMask();
            }

        });
    }

    if (id == 0 || (isWithdrawal.length > 0 && isWithdrawal[0].value == "0")) {
        $("#txtDepGuarantee" + id, context).blur(function () {
            var money = $(this).val().toLowerCase().indexOf("dinheiro") >= 0;
            
            if (!money) {
                $("#txtDepQuantity" + id, context).attr("alt", "Quantity").setMask();
                $("#txtDepUnitPrice" + id, context).show().setMask();
                $("#lblDepUnitPrice" + id, context).hide();
            }
            else {
                $("#txtDepQuantity" + id, context).attr("alt", "Value").setMask();
                $("#txtDepUnitPrice" + id, context).val("0").hide();
                $("#lblDepUnitPrice" + id, context).show();
            }
            SetDepositTotal(id, context);
        });

        $("#txtDepQuantity" + id, context).blur(function () {
            SetDepositTotal(id, context);
        });
        $("#txtDepUnitPrice" + id, context).blur(function () {
            SetDepositTotal(id, context);
        });
    }

    $("#txtDepQuantity" + id, context).attr("alt", "Quantity").setMask();

}

function SetDepositTotal(id, context) {
    var money = $("#txtDepGuarantee" + id, context).val().toLowerCase().indexOf("dinheiro") >= 0;
    var unitPrice = commonHelper.TryParseFloat($("#txtDepUnitPrice" + id, context).val().replace(/\./g, '').replace(/\,/g, '.'), 0);
    var quantity = commonHelper.TryParseFloat($("#txtDepQuantity" + id, context).val().replace(/\./g, '').replace(/\,/g, '.'), 0);
    var total = 0;
    if (!money && unitPrice > 0 && quantity > 0) {
        total = unitPrice * quantity;
        $("#lblDepTotal" + id, context).html(commonHelper.FormatNumber(total, 2));
    }
    else
        $("#lblDepTotal" + id, context).html("-");

}

function SetEditFields(context) {
    var isWithdrawal = $("input[name=rbIsWithdrawal]:checked", context).val() == "1";
    var money;

    var id;
    if (isWithdrawal) {
        id = $("input[id*='txtGuarantee']").eq(0).attr("id").replace("txtGuarantee", "");
        money = $("#hdfIsQuantity" + id).val() == "0"; //"False";
    }
    else {
        money = $("#txtDepGuarantee0", context).val().toLowerCase().indexOf("dinheiro") >= 0;
    }

//    money = $(isWithdrawal ? "#txtGuarantee0" : "#txtDepGuarantee0", context).val().toLowerCase().indexOf("dinheiro") >= 0;

//    money = $("#hdfIsQuantity" + id).val() == "0"; //"False";

    

    if (isWithdrawal) {
        if (!money) {
            $("#txtQuantity0", context).attr("alt", "Quantity").setMask();
        }
        else {
            $("#txtQuantity0", context).attr("alt", "Value").setMask();
        }
    }
    else {
        if (!money) {
            $("#txtDepQuantity0", context).attr("alt", "Quantity").setMask();
        }
        else {
            $("#txtDepQuantity0", context).attr("alt", "Value").setMask();
        }
    }
}

function ValidateFields(context) {
    var feedback = "";

    if ($("#txtClient", context).val() == "") {
        feedback += "<li>Cliente em branco.</li>";
    }

    var isWithdrawal = $("input[name=rbIsWithdrawal]:checked", context);

    if (isWithdrawal.length == 0) {
        feedback += "<li>Tipo de transferência em branco.</li>";
    }
    else {
        var blankCounter = 0;

        if (isWithdrawal[0].value == "1") {
            $("[id^=txtGuarantee]", context).each(function () {
                if (this.value == "")
                    blankCounter++;
            });
            if (blankCounter > 0)
                feedback += "<li>" + blankCounter + " garantia" + (blankCounter == 1 ? "" : "s") + " em branco.</li>";

            blankCounter = 0;
            $("[id^=txtQuantity]", context).each(function () {
                if (this.value == "0" || this.value == "0,00")
                    blankCounter++;
            });
            if (blankCounter > 0)
                feedback += "<li>" + blankCounter + " quantidade" + (blankCounter == 1 ? "" : "s") + " em branco.</li>";
        }
        else {
            var unitCounter = 0;

            $("[id^=txtDepGuarantee]", context).each(function () {
                if (this.value == "")
                    blankCounter++;
                else {
                    var money = this.value.toLowerCase().indexOf("dinheiro") >= 0;
                    if (!money) {
                        var id = this.id.replace("txtDepGuarantee", "");
                        var unit = $("#txtDepUnitPrice" + id, context).val();
                        if (unit == "" || unit == "0,00")
                            unitCounter++;
                    }
                }
            });
            if (blankCounter > 0)
                feedback += "<li>" + blankCounter + " garantia" + (blankCounter == 1 ? "" : "s") + " em branco.</li>";
            if (unitCounter > 0)
                feedback += "<li>" + unitCounter + " valor" + (unitCounter == 1 ? "" : "es") + " unitário" + (unitCounter == 1 ? "" : "s") + " em branco.</li>";

            blankCounter = 0;
            $("[id^=txtDepQuantity]", context).each(function () {
                if (this.value == "0" || this.value == "0,00")
                    blankCounter++;
            });
            if (blankCounter > 0)
                feedback += "<li>" + blankCounter + " quantidade" + (blankCounter == 1 ? "" : "s") + " em branco.</li>";



        }
    }

    if ($("input[name=rbIsBovespa]:checked", context).length == 0) {
        feedback += "<li>Destino/Origem em branco.</li>";
    }

    if (feedback.length > 0)
        feedback = "<ul>" + feedback + "</ul>";

    return feedback;
}


function SetStockExchanges(context) {

    var clientCode = $("#txtClient", context).val();

    if (clientCode != "") {
        $.ajax({
            cache: false,
            type: "POST",
            dataType: "json",
            async: true,
            url: "/Market/LoadStockExchanges",
            data: { clientCode: clientCode },
            success: function (data) {
                $('#rbIsBovespa', context).each(function () {
                    var isBovespa = $(this).val() == 1;
                    if (isBovespa) {
                        $(this).attr("disabled", data.Bovespa ? "" : "disabled");
                        if (!data.Bovespa)
                            $(this).attr("checked", data.Bovespa);
                    }
                    else {
                        $(this).attr("disabled", data.Bmf ? "" : "disabled");
                        if (!data.Bmf)
                            $(this).attr("checked", data.Bmf);
                    }
                });
            }
        });
    }
}