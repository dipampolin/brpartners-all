﻿
$(document).ready(function () {

    $(".qx-modal-btn-fechar").unbind("click");
    $(".qx-modal-btn-fechar").bind("click", function () {
        Modal.Close();
        $("#subscriptionList #chkAll").attr('checked', false);
        $("#subscriptionFilterContent #btnSearch").click();
        return false;
    });
});

function Next(current) {
    var next = parseInt(current) + 1;
    $("#frmConfirmSubscriptionApprove #divConfirm-" + current.toString()).css({ 'display': 'none' });
    $("#frmConfirmSubscriptionApprove #divConfirm-" + next.toString()).css({ 'display': 'block' });
}

function Back(current) {
    var back = parseInt(current) - 1;
    $("#frmConfirmSubscriptionApprove #divConfirm-" + current.toString()).css({ 'display': 'none' });
    $("#frmConfirmSubscriptionApprove #divConfirm-" + back.toString()).css({ 'display': 'block' });
}

function Approve(index, requestId, isinStock, isinSubscription, company) {
    this.Submit(index, requestId, isinStock, isinSubscription, 'A', company);
}

function Reprove(index, requestId, isinStock, isinSubscription, company) {
    this.Submit(index, requestId, isinStock, isinSubscription, 'R', company);
}

function Submit(index, requestId, isinStock, isinSubscription, status, company) {

    var current = $("#frmConfirmSubscriptionApprove #divConfirm-" + index);
    var message = status == "A" ? "Aprovação com sucesso." : "Reprovação com sucesso.";

    $.ajax({
        url: "/Subscription/ChangeStatus",
        type: "POST",
        dataType: "json",
        data: { requestId: requestId, isinStock: isinStock, isinSubscription: isinSubscription, status: status, companyName: company },
        success: function (data) {
            if (data.Error == "") {
                var total = $("#frmConfirmSubscriptionApprove #hdfTotal").val();

                $("#divButtons-" + index).remove();
                $("#divMessage-" + index).html(message).css({ 'display': 'block' });

                if (total != index) {
                    var next = parseInt(index) + 1;
                    current.css({ 'display': 'none' });
                    $("#frmConfirmSubscriptionApprove #divConfirm-" + next.toString()).css({ 'display': 'block' });
                }

            }
            else {
                $("#divButtons-" + index).remove();
                $("#divMessage-" + index).html("Erro na atualização.").css({ 'display': 'block' });
            }
        }
    });

}