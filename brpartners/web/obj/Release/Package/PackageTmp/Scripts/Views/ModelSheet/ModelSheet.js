﻿$(function () {
    $("#modelsheet, #modelsheet2, #modelsheet3, #modelsheet4, #modelsheet5, #modelsheet6, #modelsheet7").datepicker();

    $("#filtroModelSheet").click(function () {
        $('#filtroModelSheetContent').slideToggle('fast');
        $('select').styleInputs();
        $(this).toggleClass('aberto');
    });
});