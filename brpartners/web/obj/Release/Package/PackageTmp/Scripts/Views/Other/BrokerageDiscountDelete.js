﻿$(function () {
    $("#btnDelete").unbind("click");
    $("#btnDelete").bind("click", function () {

        $.ajax({
            type: "POST",
            cache: false,
            async: false,
            url: '../Other/RemoveBrokerageDiscount',
            data: { id: $("#hdfRequestId").val() },
            dataType: 'json',
            beforeSend: function () {
                $("#loadingArea").show();
                $("#feedback").hide().find("span").html('');
                $("#feedbackError").hide().find("span").html('');
            },
            success: function (data) {
                $("#loadingArea").hide();

                if (data.Result) {
                    selector = "#feedback";
                    
                    oTableBroker.fnDraw(true);
                    
                }
                else {
                    selector = "#feedbackError";
                }
                $(selector).show().find("span").append(data.Message);

                Modal.Close();
            }
        });

    });
});