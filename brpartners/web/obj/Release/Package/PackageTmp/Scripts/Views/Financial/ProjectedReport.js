﻿/// <reference path="../../References.js" />
var oTable;
var emptyDataMessage = "Sua pesquisa não encontrou resultados válidos. Tente novamente.";

$(function () {
    $(document).ready(function () {
        var context = '#tableArea';
        var filterContext = '#ProjectedFilterContent';

        $("#txtTradingDate", filterContext).setMask("99/99/9999").datepicker().datepicker($.datepicker.regional['pt-BR']);
        $("#txtSettleDate", filterContext).setMask("99/99/9999").datepicker().datepicker($.datepicker.regional['pt-BR']);
        $.datepicker.setDefaults($.datepicker.regional['pt-BR']);

        $("#ProjectedFilter").click(function () {
            $('#ProjectedFilterContent').slideToggle('fast');
            $('select').styleInputs();
            $(this).toggleClass('aberto');
        });

        $(filterContext).bind('keydown', function (event) {
            if (event.which == 13) {
                $("#btnSearch", filterContext).click();
                return false;
            }
        });

        $("#btnSearch", filterContext).click(function () {

            var feedback = ValidateFilterFields(filterContext);

            if (feedback == "") {


                $("#emptyArea").hide();
                $("#tableArea").hide();
                $("#loadingArea").show();

                if ($(filterContext).css("display") != "none")
                    $("#ProjectedFilter").click();

                $("#filterFeedbackError", filterContext).hide();

                $.ajax({
                    cache: false,
                    type: "POST",
                    async: true,
                    url: "/Financial/LoadProjectedReport",
                    data: { Assessors: $("#txtAssessors", filterContext).val(), Clients: $("#txtClient", filterContext).val(), Type: $("#ddlType", filterContext).val(), TradingDate: $("#txtTradingDate", filterContext).val(), SettleDate: $("#txtSettleDate", filterContext).val() },
                    success: function () {
                        obj = {};
                        obj = {
                            "bSort": true,
                            "bAutoWidth": false,
                            "bInfo": true,
                            "bSort": true,
                            "iDisplayLength": 10,
                            "oLanguage": {
                                "sLengthMenu": "<span>Exibir:</span> _MENU_ <span>por página</span>",
                                "sZeroRecords": "Sua pesquisa não encontrou resultados válidos. Tente novamente.",
                                "oPaginate": {
                                    "sPrevious": "Anteriores",
                                    "sNext": "Próximos"
                                }
                            },
                            "sPaginationType": "full_numbers",
                            "sDom": 'rtip',
                            "aoColumns": [
                                                null,
                                                null,
                                                null,
                                                null,
                                                { "sClass": "direita" },
                                                null,
                                                { "sClass": "direita" },
                                                { "sClass": "direita" },
                                                { "sClass": "direita" },
                                                { "sClass": "direita" },
                                                null,
                                                { "sClass": "direita" },
                                                { "sClass": "direita" },
                                                { "sClass": "direita" }
                                    ],
							"fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
								var iCols = [4, 6, 7, 8, 9, 11, 12, 13];
								for (var i in iCols) {
									if (!isNaN(parseFloat(aData[iCols[i]]))) {
										var number = parseFloat(aData[iCols[i]].replace(/\./g, "").replace(/\,/g, "."));
										if (number > 0) {
											$(nRow).find("td:eq(" + iCols[i].toString() + ")").addClass("positivo");
										}
										else if (number < 0) {
											$(nRow).find("td:eq(" + iCols[i].toString() + ")").addClass("negativo");
										}
									}
								}
								return nRow;
							},
                            "fnDrawCallback": function () {
                                var totalRecords = this.fnSettings().fnRecordsDisplay();
                                var emptyArea = $("#emptyArea");
                                var tableArea = $("#tableArea");

                                $("#loadingArea").hide();

                                if (totalRecords == 0) {
                                    emptyArea.html(emptyDataMessage);
                                    emptyArea.show();
                                    tableArea.hide();
                                }
                                else {
                                    tableArea.show();
                                    emptyArea.hide();

                                    var totalPerPage = this.fnSettings()._iDisplayLength;

                                    var totalPages = Math.ceil(totalRecords / totalPerPage);

                                    var currentIndex = parseInt(this.fnSettings()._iDisplayStart);
                                    var currentPage = totalPages > 0 ? Math.ceil(1 + (currentIndex / totalPerPage)) : 0;

                                    $("#projectedList_info").html("Exibindo " + currentPage + " de " + totalPages);

                                    if (totalRecords > 0 && totalPages > 1) {
                                        $("#projectedList_info").css("display", "");
                                        $("#projectedList_length").css("display", "");
                                        $("#projectedList_paginate").css("display", "");
                                    }
                                    else {
                                        $("#projectedList_info").css("display", "none");
                                        $("#projectedList_length").css("display", "none");
                                        $("#projectedList_paginate").css("display", "none");
                                    }
                                    $('select', '#projectedList_length').styleInputs();
                                }
                                // Adiciona a classe 'ultimo' à última tr no corpo da tabela
                                $('table tbody tr:last-child').addClass('ultimo');

                                // Adiciona a classe 'ultimo' às últimas th e td no corpo da tabela
                                $('table tr td:last-child').addClass('ultimo');
                                $('table tr th:last-child').addClass('ultimo');

								$('table thead tr th').removeClass('direita');
                            },
                            "bProcessing": false,
                            "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                                $('#projectedList_first').css("visibility", "hidden");
                                $('#projectedList_last').css("visibility", "hidden");
                            },
                            "bRetrieve": true,
                            "bServerSide": true,
                            "sAjaxSource": "../DataTables/ProjectedReport_List",
                            "fnServerData": function (sSource, aoData, fnCallback) {
                                $.ajax({
                                    cache: false,
                                    dataType: 'json',
                                    type: "POST",
                                    url: sSource,
                                    data: aoData,
                                    success: fnCallback
                                });
                            }
                        };

                        oTable = $("#projectedList").dataTable(obj);
                        oTable.fnClearTable();
                        oTable.fnDraw();
                    }
                });
            }
            else {
                $("#filterFeedbackError", filterContext).html(feedback).show();
            }
            return false;
        });

        setTimeout(function () {
            $("#btnSearch", filterContext).click();
        }, 250);
    });

});

function ValidateFilterFields(context) {
    var messageError = "";
    if (!validateHelper.ValidateList($("#txtAssessors", context).val()))
        messageError += "<li>Faixa de assessores inválida.</li>";
    if (!validateHelper.ValidateList($("#txtClient", context).val()))
        messageError += "<li>Faixa de clientes inválida.</li>";

    if (messageError.length > 0)
        messageError = "<ul>" + messageError + "</ul>";

    return messageError;
}


function Export(pdf) {
    if ($("#tableArea").css("display") != "none") {
        var url = "/Financial/ConfirmProjectedExport/?isPDF=" + pdf;
        commonHelper.OpenModal(url, 330, ".content", "");
        $("#MsgBox").center();
    }
}

