﻿function showModal() {
    $(".table-tr-remove").remove();
    $("#representativeTable").trigger('reset');
    $('#dialog-form-rules').show();
}

function closeModal() {
    $('#dialog-form-rules').hide();
}

function addRepresentative() {
    createElementTable('.rep-field', 'representativeTable')
}

function createElementTable(css, table) {
    var inputs = document.querySelectorAll(css);
    var elementTr = document.createElement('tr');
    elementTr.classList.add('table-tr-remove');
    var table = document.querySelector('#' + table);
    table.appendChild(elementTr);

    inputs.forEach(function(input) {
        var newElem = document.createElement('td');

        if (input.type == "select-one") {
            newElem.textContent = input.options[input.selectedIndex].text;
            newElem.id = input.options[input.selectedIndex].value;
        }
        else {
            newElem.textContent = input.value;
            newElem.id = input.value;
        }

        newElem.classList.add('table-td');
        newElem.classList.add('table-td-remove');
        elementTr.appendChild(newElem);
    });

    var TdInput = document.createElement('td');
    TdInput.classList.add('table-td');

    var elementInput = document.createElement("a");
    elementInput.className = 'ico-excluir';
    elementInput.style.marginLeft = '21px';

    TdInput.appendChild(elementInput);
    elementTr.appendChild(TdInput);

    elementInput.addEventListener('click', function(event) {
        removeParent(event);
    });

    hiddenElementTable();
}

function hiddenElementTable() {
    var rows = document.getElementById('representativeTable').getElementsByTagName("tr").length
    if (rows > 0) {
        document.getElementById('representativeTable').style.display = '';
    }
    else
        document.getElementById('representativeTable').style.display = 'none';
}

function removeParent(event, value) {
    event.target.parentNode.parentNode.classList.add('fadeOut');

    document.getElementById("HdnRepresentativeDelete").value += (value + ";");

    setTimeout(function() {
        event.target.parentNode.parentNode.remove();
        hiddenElementTable();
    }, 500);
}

function saveRepresentatives() {
    var rows = document.getElementById('representativeTable').getElementsByTagName("tr");
    for (var i = 1; i < rows.length; i++) {
        var cells = rows[i].cells;

        if (i > 1)
            document.getElementById("HdnRepresentative").value += ";"

        for (var a = 0; a < (cells.length - 1); a++) {
            if (a > 0)
                document.getElementById("HdnRepresentative").value += ",";

            document.getElementById("HdnRepresentative").value += cells[a].id;
        }
    }

    document.getElementById('btnSave').click();
}

function visualizeInformation(value) {
    $("#representativeTable").trigger('reset');
    $(".table-tr-remove").remove();

    $.ajax({
        url: '/Account/GetRules/',
        data: { idDocument: value },
        type: 'GET',
        datatype: 'json',
        success: function (response) {
            $('#IdDocument').val(response.IdDocument);
            $('#StartValidateDocument').val(dataFormatada(response.StartValidateDocument));
            $('#FinalValidateDocument').val(dataFormatada(response.FinalValidateDocument));
            $('#IdDocumentType').val(response.IdDocumentType);

            response.Representatives.forEach(function (cont) {
                var elementTr = createRow();
                AddNewRowTable(cont.Id, cont.Id, elementTr, 'row-hidden');
                AddNewRowTable(cont.IdRepresentative, cont.NameRepresentative, elementTr);
                AddNewRowTable(cont.NumberDocument, cont.NumberDocument, elementTr);
                AddNewRowTable(cont.IdQualification, cont.DescriptionQualification, elementTr);
                createButtonAction(elementTr, cont.Id);
            });

            hiddenElementTable();
            $('#dialog-form-rules').show();
        }
    });
}

function dataFormatada(d) {
    if (d != null) {
        var dateFormat = d.replace('/Date(', '').replace(')/', '');
        var data = new Date(parseInt(dateFormat));
        var dia = data.getDate() < 10 ? '0' + data.getDate() : data.getDate();
        var mes = data.getMonth() + 1 < 10 ? '0' + (data.getMonth() + 1) : (data.getMonth() + 1);
        var ano = data.getFullYear();
        return [ano, mes, dia].join('-');
    }
}

function createRow(table) {
    var elementTr = document.createElement('tr');
    elementTr.classList.add('table-tr-remove');
    var table = document.querySelector('#representativeTable');
    table.appendChild(elementTr);

    return elementTr;
}

function AddNewRowTable(id, value, elementTr, nameClass) {
    var newElem = document.createElement('td');
    newElem.textContent = value;
    newElem.id = id;

    if (nameClass != undefined)
        newElem.classList.add(nameClass);

    newElem.classList.add('table-td');
    elementTr.appendChild(newElem);
}

function createButtonAction(element, value) {
    var TdInput = document.createElement('td');
    TdInput.classList.add('table-td');
    TdInput.style.width = "60px";

    var elementInput = document.createElement("a");
    elementInput.className = 'ico-excluir';
    elementInput.style.marginLeft = '21px';
    TdInput.appendChild(elementInput);
    element.appendChild(TdInput);

    elementInput.addEventListener('click', function (event) {
        removeParent(event, value);
    });
}