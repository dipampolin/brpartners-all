﻿(function (angular) {
    'use strict';

    angular.module('brp')
    .controller("ReportViewerIndexCtrl", ['$scope', 'ReportViewerService', function ($scope, ReportViewerService) {

        var $ctrl = this;
        this.loadData = function (state) {
            $ctrl.isLoading = true;
            ReportViewerService.list(state).then(function (result) {
                $ctrl.data = result.data;
                state.pagination.numberOfPages = result.pages;
                $ctrl.isLoading = false;
            });

        }
    }]);
})(window.angular);

