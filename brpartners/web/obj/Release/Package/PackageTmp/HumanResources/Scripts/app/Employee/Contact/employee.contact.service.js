﻿(function (angular) {
    'use strict';

    angular.module('brp')

    .factory("EmployeeContactService", ['$http', 'PaginateRequest', function ($http, PaginateRequest) {

        var create = function (data) {
            return $http.post('/RH/EmployeeContact/CreatePost', data)
                .then(function (res) {
                    return res.data;
                });
        }

        var list = function (id) {
            return $http.get('/RH/EmployeeContact/List', {
                params: {
                    employeeId: id
                }
            }).then(function (res) {
                return res.data;
            });
        }

        var remove = function (id) {
            return $http.get('/RH/EmployeeContact/Remove', {
                params: {
                    id: id
                }
            }).then(function (res) {
                return res.data;
            });
        }

        var findById = function (id) {
            return $http.get('/RH/EmployeeContact/Find', {
                params: {
                    employeeId: id
                }
            }).then(function (res) {
                return res.data;
            });
        }

        var getOptions = function () {
            return [{ Id: "Celular Corporativo", Value: "Celular Corporativo" }, { Id: "Celular Pessoal", Value: "Celular Pessoal" }, { Id: "E-mail Corporativo", Value: "E-mail Corporativo" }, { Id: "E-mail Pessoal", Value: "E-mail Pessoal" }, { Id: "Telefone Comercial", Value: "Telefone Comercial" }, { Id: "Telefone Residencial", Value: "Telefone Residencial" }, { Id: "Outros", Value: "Outros" }];
        }

        return {
            getOptions: getOptions,
            create: create,
            list: list,
            findById: findById,
            remove: remove
        }
    }]);

})(window.angular);