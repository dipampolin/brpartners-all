﻿(function (angular, employee) {
    'use strict';

    angular.module('brp')
    .controller("EmployeeHistoryCompanyCreateCtrl", ['$uibModalInstance', 'CompanyService', 'EmployeeHistoryCompanyService', 'data', 'employeeId', 'toastr', 'EmployeeCompanyService',
        function ($uibModalInstance, CompanyService, EmployeeHistoryCompanyService, data, employeeId, toastr, EmployeeCompanyService) {
            var $ctrl = this;

            this.data = data || {};
            this.data.EmployeeId = this.data.EmployeeId || employeeId;
            CompanyService.listAll().then(function (result) {
                $ctrl.companies = result;
            });

            $ctrl.ok = function () {
                EmployeeHistoryCompanyService.create($ctrl.data).then(function (result) {
                    EmployeeCompanyService.getActiveEmployeeCompany(employee.Id).then(function (result) {
                        if (result != "") {
                            employee.CompanyId = result.CompanyId;
                        }
                    });

                    toastr.success('Empresa salvo com sucesso.');
                    return $uibModalInstance.close(data);
                }).catch(function (err) {
                    if (err.status == 409) {
                        toastr.warning('Já existe uma Empresa ativa para este funcionário!');
                    } else {
                        console.log(2);
                        toastr.error('Falha ao salvar Empresa!');
                    }
                });
            };

            $ctrl.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        }])
    .controller("EmployeeHistoryCostCenterCreateCtrl", ['$uibModalInstance', 'CostCenterService', 'EmployeeHistoryCostCenterService', 'data', 'employeeId', 'toastr',
        function ($uibModalInstance, CostCenterService, EmployeeHistoryCostCenterService, data, employeeId, toastr) {
            var $ctrl = this;
            this.data = data || {
                
            };

            this.data.EmployeeId = this.data.EmployeeId || employeeId;

            CostCenterService.list().then(function (result) {
                $ctrl.costcenters = result;
            });

            $ctrl.ok = function () {
                EmployeeHistoryCostCenterService.create($ctrl.data).then(function (result) {
                    CostCenterService.getActiveEmployeeCostCenter(employee.Id).then(function (result) {
                        if (result != "") {
                            employee.CostCenterId = result.CostCenter.Id;
                        }
                    });
                    toastr.success('Centro de Custo salvo com sucesso.');
                    return $uibModalInstance.close(data);
                })
                .catch(function (err) {
                    if (err.status == 409) {
                        toastr.warning('Já existe um Centro de Custo ativo para este funcionário!');
                    } else {
                        toastr.error('Falha ao salvar Centro de Custo!');
                    }
                });
            };

            $ctrl.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        }])
    .controller("EmployeeHistoryRoleCreateCtrl", ['$uibModalInstance', 'RoleService', 'EmployeeHistoryRoleService', 'data', 'employeeId', 'toastr',
        function ($uibModalInstance, RoleService, EmployeeHistoryRoleService, data, employeeId, toastr) {
            var $ctrl = this;

            this.data = data || {};
            this.data.EmployeeId = this.data.EmployeeId || employeeId;
            RoleService.listAll().then(function (result) {
                $ctrl.roles = result;
            });

            $ctrl.ok = function () {
                EmployeeHistoryRoleService.create($ctrl.data).then(function (result) {
                    RoleService.getActiveEmployeeRole(employee.Id).then(function (result) {
                        if (result != "") {
                            employee.RoleId = result.Role.Id;
                        }
                    });

                    toastr.success('Cargo salvo com sucesso.');
                    return $uibModalInstance.close(data);
                }).catch(function (err) {
                    if (err.status == 409) {
                        toastr.warning('Já existe um Cargo ativo para este funcionário!');
                    } else {
                        toastr.error('Falha ao salvar Cargo!');
                    }
                });
            };

            $ctrl.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        }])
    .controller("EmployeeHistoryTypeCreateCtrl", ['$uibModalInstance', 'EmployeeHistoryTypeService', 'data', 'employeeId', 'toastr',
        function ($uibModalInstance, EmployeeHistoryTypeService, data, employeeId, toastr) {
            var $ctrl = this;
            this.data = data || {

            };

            this.data.EmployeeId = this.data.EmployeeId || employeeId;

            $ctrl.employeeTypes = EmployeeHistoryTypeService.getOptions();

            EmployeeHistoryTypeService.list().then(function (result) {
                $ctrl.costcenters = result;
            });

            $ctrl.ok = function () {
                EmployeeHistoryTypeService.create($ctrl.data).then(function (result) {
                    /* EmployeeHistoryTypeService.getActiveEmployeeType(employee.Id).then(function (result) {
                        if (result != "") {
                            employee.TypeId = result.EmployeeType.Id;
                        }
                    }); */
                    toastr.success('Tipo de Funcionário salvo com sucesso.');
                    return $uibModalInstance.close(data);
                })
                .catch(function (err) {
                    if (err.status == 409) {
                        toastr.warning('Já existe um Tipo ativo para este funcionário!');
                    } else {
                        toastr.error('Falha ao salvar o Tipo de Funcionário!');
                    }
                });
            };

            $ctrl.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        }]);

})(window.angular, window.employee);
