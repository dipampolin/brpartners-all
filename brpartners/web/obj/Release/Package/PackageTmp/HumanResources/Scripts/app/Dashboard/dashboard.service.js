﻿(function (angular) {
    'use strict';

    angular.module('brp')
    .factory("DashboardService", ['$http', function ($http) {

        var listCompanyDistribution = function () {
            return $http({
                    method: 'GET',
                    url: '/RH/Dashboard/ListCompanyDistribution'
                })
                .then(function (res) {
                    var months = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];

                    var companies = res.data.reduce(function (array, current) {
                        if (array.indexOf(current.CompanyName) == -1) {
                            array.push(current.CompanyName);
                        }
                        return array;
                    }, []);
                  
                    var data = [];
                    companies.forEach(function (company) {
                        var companyData = [];
                        months.forEach(function (month, i) {
                            var value = res.data.filter(function (current) {
                                return current.CompanyName === company && current.StartDate.getMonth() <= i && (!current.EndDate || current.EndDate.getMonth() >= i);
                            }).length;
                            companyData.push(value);
                        });
                        data.push(companyData);
                    });

                    return {
                        months: months,
                        companies: companies,
                        data: data
                    };
                });
        };

        var listEmployeesWithNearBirthdays = function () {
            return $http({
                method: 'GET',
                url: '/RH/Dashboard/ListEmployeesWithNearBirthdays'
            }).then(function (res) {
                return res.data;
            });
        };

        return {
            listCompanyDistribution: listCompanyDistribution,
            listEmployeesWithNearBirthdays : listEmployeesWithNearBirthdays
        };
    }]);

})(window.angular);