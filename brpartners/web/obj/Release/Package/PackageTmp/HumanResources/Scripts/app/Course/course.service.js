﻿(function (angular) {
    'use strict';

    angular.module('brp')

    .factory("CourseService", ['$http', 'PaginateRequest', function ($http, PaginateRequest) {

        var list = function (state) {
            return PaginateRequest.get('/RH/Course/List', state, 'Name')
                .then(function (res) {
                    return res.data;
                });
        };

        var listAll = function () {
            return $http({
                method: 'GET',
                url: '/RH/Course/ListAll/'
            }).then(function (res) {
                return res.data;
            });
        };

        var findById = function (id) {
            return $http({
                method: 'GET',
                url: '/RH/Course/Find/' + id
            }).then(function (res) {
                return res.data;
            });
        }

        var save = function (data) {
            return $http.post('/RH/Course/Save', data).then(function (res) {
                return res.data;
            });
        }

        var remove = function (id) {
            return $http.get('/RH/Course/Remove/' + id)
                .then(function (res) {
                    return res.data;
                });
        }

        return {
            list: list,
            findById: findById,
            save: save,
            listAll: listAll,
            remove: remove
        }

    }]);

})(window.angular);