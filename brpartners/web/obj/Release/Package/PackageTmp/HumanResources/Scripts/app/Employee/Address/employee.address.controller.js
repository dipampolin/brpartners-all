﻿
(function (angular, employee) {
    'use strict';

    angular.module('brp')
    .controller("EmployeeAddressCtrl", ['LocationService', 'EmployeeAddressService', 'toastr', function (LocationService, EmployeeAddressService, toastr) {

        var $ctrl = this;

        $ctrl.employee = employee;

        this.refresh = function () {
            if (employee.Id) {
                $ctrl.isLoading = true;
                EmployeeAddressService.list(employee.Id).then(function (result) {
                    $ctrl.data = result;
                    $ctrl.isLoading = false;
                });
            }
        }
        this.refresh();

        $ctrl.remove = function (id) {
            EmployeeAddressService.remove(id).then(function (result) {
                toastr.success('Endereço excluído com sucesso.');
                EmployeeAddressService.list(employee.Id).then(function (data) {
                    $ctrl.data = data;
                });
            }).catch(function (err) {
                toastr.error('Falha ao excluir Endereço!');
            });
        }
    }]);

})(window.angular, window.employee);