﻿(function (angular, employee) {
    'use strict';

    angular.module('brp')

    .controller("EmployeeBenefitCtrl", ['$scope', 'EmployeeService', 'EmployeeBenefitService', 'toastr', function ($scope, EmployeeService, EmployeeBenefitService, toastr) {

        var $ctrl = this;

        $ctrl.employee = employee;

        this.loadData = function () {
            $ctrl.isLoading = true;
            if (employee.Id) {
                $ctrl.isLoading = true;
                EmployeeBenefitService.list(employee.Id).then(function (result) {
                    $ctrl.data = result;
                    $ctrl.isLoading = false;
                });
            }
        }
        this.loadData();

        $ctrl.remove = function (id) {
            EmployeeBenefitService.remove(id).then(function (result) {
                toastr.success('Benefício excluído com sucesso.');
                EmployeeBenefitService.list(employee.Id).then(function (result) {
                    $ctrl.data = result;
                });
            }).catch(function (err) {
                toastr.error('Falha ao excluir Benefício!');
            });
        }
    }]);
})(window.angular, window.employee);
