﻿(function (angular) {
    'use strict';

    angular.module('brp')

        .directive("roleCreate", ['$uibModal', 'ModalService', function ($uibModal, ModalService) {
            return {
                restrict: "A",
                scope: {
                    refreshCallback: "&"
                },
                link: function (scope, elem, attrs) {

                    angular.element(elem).on('click', function (event) {
                        event.preventDefault();

                        ModalService.show({
                            id: attrs.roleCreate,
                            view: '/HumanResources/Static/Role/roleCreate.html',
                            controller: 'RoleCreateCtrl',
                            service: 'RoleService',
                            cb: scope.refreshCallback()
                        });
                    });
                }
            }
        }]);
})(window.angular);