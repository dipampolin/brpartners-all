﻿
(function (angular) {
    'use strict';

    angular.module('brp')
    .factory('dateParser', [function () {

        var parseDate = function (unparsedDate) {
            var date = moment(unparsedDate);
            if (date.isValid()) {
                return date.toDate();
            }
            return null;
        };

        var parseDateFromElement = function (element, properties) {
            for (var j = 0; j < properties.length; j++) {
                element[properties[j]] = parseDate(element[properties[j]]);
            }
        }

        var parse = function (data) {
            var dataArray = [].concat(data);
            for (var i = 0; i < dataArray.length; i++) {
                parseDateFromElement(dataArray[i], Array.prototype.slice.call(arguments, 1));
            }
            return data;
        };

        return {
            parse: parse
        }
    }]);

})(window.angular);
