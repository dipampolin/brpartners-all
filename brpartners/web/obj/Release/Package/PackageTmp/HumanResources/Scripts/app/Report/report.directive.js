﻿
(function (angular) {
    'use strict';

    angular.module('brp')

    .directive("reportCreate", ['$uibModal', 'ReportService', 'ModalService', function ($uibModal, ReportService, ModalService) {

        return {
            restrict: "A",
            scope: {
                refreshCallback: "&"
            },
            link: function (scope, elem, attrs) {

                angular.element(elem).on('click', function (event) {
                    event.preventDefault();

                    ModalService.show({
                        size: 'lg',
                        id: attrs.reportCreate,
                        view: '/HumanResources/Static/Report/reportCreate.html',
                        controller: 'ReportCreateCtrl',
                        service: 'ReportService',
                        cb: scope.refreshCallback()
                    });
                });

            }
        }

    }]);

})(window.angular);
