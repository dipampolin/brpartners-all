﻿
(function (angular) {
    'use strict';

    angular.module('brp')

    .controller("EmployeeContactCreateCtrl", ['$uibModalInstance', 'EmployeeContactService', 'data', 'employeeId', 'toastr', function ($uibModalInstance, EmployeeContactService, data, employeeId, toastr) {
        var $ctrl = this;

        this.data = data || {};
        this.data.EmployeeId = this.data.EmployeeId || employeeId;

        function InitialValues() {
            if (!$ctrl.data.Id) {
                $ctrl.data.Active = true;
            }
        }

        InitialValues();

        $ctrl.ok = function () {
            EmployeeContactService.create($ctrl.data).then(function (result) {
                toastr.success('Contato salvo com sucesso.');
                return $uibModalInstance.close(data);
            }).catch(function (err) {
                toastr.error('Falha ao salvar Contato!');
            });
        };

        $ctrl.contacts = EmployeeContactService.getOptions();

        $ctrl.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }]);

})(window.angular);
