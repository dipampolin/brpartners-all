﻿(function (angular) {
    'use strict';

    angular.module('brp')

        .factory("RoleService", ['$http', 'PaginateRequest', function ($http, PaginateRequest) {

        var listAll = function () {
            return $http({
                method: 'GET',
                url: '/RH/Role/ListAll'
            }).then(function (res) {
                return res.data;
            });
        };
        var getActiveEmployeeRole = function (id) {
            return $http({
                method: 'GET',
                url: '/RH/Role/GetActiveEmployeeRole?id=' +id
            }).then(function (res) {
                return res.data;
            });
        };

        var paginateList = function (state) {
            return PaginateRequest.get('/RH/Role/List', state, 'Name')
                .then(function (res) {
                    return res.data;
                });
        };

        var save = function (role) {
            return $http.post('/RH/Role/Save', role)
                .then(function (res) {
                    return res.data;
                });
        };

        var findById = function (id) {
            return $http({
                method: 'GET',
                url: '/RH/Role/Find/' + id
            }).then(function (res) {
                return res.data;
            });
        }

        var remove = function (id) {
            return $http.get('/RH/Role/Remove/' + id)
                    .then(function (res) {
                        return res.data;
                    });
        }

        return {
            listAll: listAll,
            paginateList: paginateList,
            getActiveEmployeeRole: getActiveEmployeeRole,
            save: save,
            findById: findById,
            remove: remove
        }

    }]);

})(window.angular);