﻿(function (angular) {
    'use strict';

    angular.module('brp')

    .directive("employeeAttachmentImport", ['$uibModal', 'EmployeeAttachmentService', 'ModalService', function ($uibModal, EmployeeAttachmentService, ModalService) {

        return {
            restrict: "A",
            scope: {
                refreshCallback: "&"
            },
            link: function (scope, elem, attrs) {
                angular.element(elem).on('click', function (event) {
                    event.preventDefault();
                    ModalService.show({
                        id: attrs.employeeAttachmentImport,
                        view: '/HumanResources/Static/Employee/Attachment/employeeAttachmentImport.html',
                        controller: 'EmployeeAttachmentImportCtrl',
                        service: 'EmployeeAttachmentService',
                        cb: scope.refreshCallback()
                    });
                });
            }
        }
    }]);

})(window.angular);