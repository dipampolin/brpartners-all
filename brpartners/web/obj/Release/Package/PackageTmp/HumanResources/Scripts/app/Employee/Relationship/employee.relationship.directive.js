﻿(function (angular) {
    'use strict';

    angular.module('brp')

    .directive("relationshipCreate", ['$uibModal', 'ModalService', function ($uibModal, ModalService) {
        return {
            restrict: "A",
            scope: {
                refreshCallback: "&",
                employeeId: '='
            },
            link: function (scope, elem, attrs) {

                angular.element(elem).on('click', function (event) {
                    event.preventDefault();
                    ModalService.show({
                        id: attrs.relationshipCreate,
                        view: '/HumanResources/Static/Employee/Relationship/relationshipCreate.html',
                        controller: 'EmployeeRelationshipCreateCtrl',
                        service: 'EmployeeRelationshipService',
                        resolve: {
                            employeeId: scope.employeeId
                        },
                        cb: scope.refreshCallback()
                    });

                });
            }
        }
    }]);

})(window.angular);