﻿(function (angular) {
    'use strict';

    angular.module('brp')

    .directive("costCenterCreate", ['$uibModal', 'ModalService', function ($uibModal, ModalService) {
        return {
            restrict: "A",
            scope: {
                refreshCallback: "&"
            },
            link: function (scope, elem, attrs) {

                angular.element(elem).on('click', function (event) {
                    event.preventDefault();

                    ModalService.show({
                        id: attrs.costCenterCreate,
                        view: '/HumanResources/Static/CostCenter/costCenterCreate.html',
                        controller: 'CostCenterCreateCtrl',
                        service: 'CostCenterService',
                        cb: scope.refreshCallback()
                    });
                });
            }
        }
    }]);
})(window.angular);
