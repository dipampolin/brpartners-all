﻿
(function (angular) {
    'use strict';

    angular.module('brp')

    .directive("benefitCreate", ['$uibModal', 'BenefitService', 'ModalService', function ($uibModal, BenefitService, ModalService) {

        return {
            restrict: "A",
            scope: {
                refreshCallback: "&"
            },
            link: function (scope, elem, attrs) {

                angular.element(elem).on('click', function (event) {
                    event.preventDefault();

                    ModalService.show({
                        id: attrs.benefitCreate,
                        view: '/HumanResources/Static/Benefit/benefitCreate.html',
                        controller: 'BenefitCreateCtrl',
                        service: 'BenefitService',
                        cb: scope.refreshCallback()
                    });
                });

            }
        }

    }]);

})(window.angular);
