﻿
(function (angular) {
    'use strict';

    angular.module('brp')

    .factory('PaginateRequest', ['$http', function ($http) {

        var get = function (uri, state, defaultOrder) {

            var params = {
                Start: state.pagination.start,
                Length: state.pagination.number,
                OrderColumn: state.sort.predicate || defaultOrder,
                OrderDirection: state.sort.reverse ? 'desc' : 'asc'
            }

            var search = state.search.predicateObject || {};
            var searchString = Object.keys(search).reduce(function (acc, key) {
                acc.push('search[' + key + ']=' + encodeURIComponent(search[key]));
                return acc;
            }, []).join('&');

            if (searchString) {
                uri = uri + '?' + searchString
            }

            return $http({
                method: 'GET',
                url: uri,
                params: params
            });
        }

        return {
            get: get
        }
    }]);

})(window.angular);
