﻿
(function (angular) {
    'use strict';

    angular.module('brp')

    .directive("employeeCourseCreate", ['$uibModal', 'ModalService', function ($uibModal, ModalService) {
        return {
            restrict: "A",
            scope: {
                refreshCallback: "&"
            },
            link: function (scope, elem, attrs) {
                
                angular.element(elem).on('click', function (event) {
                    event.preventDefault();
                    ModalService.show({
                        id: attrs.employeeCourseCreate,
                        view: '/HumanResources/Static/Employee/Course/employeeCourseCreate.html',
                        controller: 'EmployeeCourseCreateCtrl',
                        service: 'EmployeeCourseService',
                        cb: scope.refreshCallback()
                    });

                });
            }
        }
    }]);
})(window.angular);