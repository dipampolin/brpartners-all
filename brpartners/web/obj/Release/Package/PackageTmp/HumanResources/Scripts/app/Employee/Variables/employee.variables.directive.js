﻿(function (angular) {
    'use strict';

    angular.module('brp')

    .directive("variablesCreate", ['$uibModal', 'ModalService', function ($uibModal, ModalService) {
        return {
            restrict: "A",
            scope: {
                refreshCallback: "&",
            },
            link: function (scope, elem, attrs) {

                angular.element(elem).on('click', function (event) {
                    event.preventDefault();
                    ModalService.show({
                        id: attrs.variablesCreate,
                        view: '/HumanResources/Static/Employee/Variables/variablesCreate.html',
                        controller: 'EmployeeVariablesCreateCtrl',
                        service: 'EmployeeVariablesService',
                        cb: scope.refreshCallback
                    });

                });
            }
        }
    }]);

})(window.angular);