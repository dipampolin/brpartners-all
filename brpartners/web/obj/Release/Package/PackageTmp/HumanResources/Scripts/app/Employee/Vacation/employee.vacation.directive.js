﻿
(function (angular) {
    'use strict';

    angular.module('brp')

    .directive("vacationCreate", ['$uibModal', 'ModalService', function ($uibModal, ModalService) {
        return {
            restrict: "A",
            scope: {
                refreshCallback: "&",
            },
            link: function (scope, elem, attrs) {

                angular.element(elem).on('click', function (event) {
                    event.preventDefault();
                    ModalService.show({
                        id: attrs.vacationCreate,
                        view: '/HumanResources/Static/Employee/Vacation/vacationCreate.html',
                        controller: 'EmployeeVacationCreateCtrl',
                        cb: scope.refreshCallback,
                        service: 'EmployeeVacationService',
                    });

                });
            }
        }
    }]);

})(window.angular);