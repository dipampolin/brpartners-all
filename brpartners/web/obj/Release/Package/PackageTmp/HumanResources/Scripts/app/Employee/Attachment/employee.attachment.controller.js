﻿(function (angular, employee) {
    'use strict';

    angular.module('brp')

    .controller("EmployeeAttachmentCtrl", ['$scope', 'toastr', 'EmployeeAttachmentService', function ($scope, toastr, EmployeeAttachmentService) {
       
        var $ctrl = this;

        $ctrl.employee = employee;

        this.loadData = function () {
            $ctrl.isLoading = true;
            if (employee.Id) {
                $ctrl.isLoading = true;
                EmployeeAttachmentService.list(employee.Id).then(function (result) {
                    $ctrl.data = result;
                    $ctrl.isLoading = false;
                });
            }
        }
        this.loadData();

        $ctrl.remove = function (id) {
            EmployeeAttachmentService.remove(id).then(function (result) {
                toastr.success('Anexo excluído com sucesso.');
                EmployeeAttachmentService.list(employee.Id).then(function (data) {
                    $ctrl.data = data;
                });
            }).catch(function (err) {
                toastr.error('Falha ao excluir Anexo!');
            });
        }

    }]);

})(window.angular, window.employee);
