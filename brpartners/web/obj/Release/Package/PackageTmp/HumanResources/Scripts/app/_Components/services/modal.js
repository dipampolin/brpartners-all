﻿
(function (angular) {
    'use strict';

    angular.module('brp')

    .factory('ModalService', ['$uibModal', '$injector', function ($uibModal, $injector) {
        
        var show = function (opts) {

            opts.resolve = opts.resolve || {};

            opts.resolve = angular.extend(opts.resolve, {
                data: function () {

                    if (opts.data) {
                        return opts.data;
                    }

                    if (!opts.id || !opts.service) {
                        return null;
                    }

                    var service = $injector.get(opts.service);
                    return service.findById(opts.id);
                }
            });

            var modalInstance = $uibModal.open({
                templateUrl: opts.view,
                controller: opts.controller,
                backdrop: 'static',
                controllerAs: '$ctrl',
                size: opts.size || null,
                resolve: opts.resolve
            });

            modalInstance.result.then(function () {
                if (typeof opts.cb == 'function') {
                    opts.cb();
                }
            });

            return modalInstance.result;
        }

        return {
            show: show
        }
    }]);

})(window.angular);
