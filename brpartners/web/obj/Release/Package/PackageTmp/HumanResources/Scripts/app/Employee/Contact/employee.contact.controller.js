﻿
(function (angular, employee) {
    'use strict';

    angular.module('brp')
    .controller("EmployeeContactCtrl", ['EmployeeContactService', 'toastr', function (EmployeeContactService, toastr) {

        var $ctrl = this;

        $ctrl.employee = employee;

        this.refresh = function () {

            if (employee.Id) {
                $ctrl.isLoading = true;
                EmployeeContactService.list(employee.Id).then(function (result) {
                    $ctrl.data = result;
                    $ctrl.isLoading = false;
                });
            }
        }

        this.refresh();

        $ctrl.remove = function (id) {
            EmployeeContactService.remove(id).then(function (result) {
                toastr.success('Contato excluído com sucesso.');
                EmployeeContactService.list(employee.Id).then(function (result) {
                    $ctrl.data = result;
                });
            }).catch(function (err) {
                toastr.error('Falha ao excluir o Contato!');
            });
        }
    }]);

})(window.angular, window.employee);
