﻿
(function (angular) {
    'use strict';

    angular.module('brp')

    .directive("courseCreate", ['$uibModal', 'CourseService', 'ModalService', function ($uibModal, CourseService, ModalService) {

        return {
            restrict: "A",
            scope: {
                refreshCallback: "&"
            },
            link: function (scope, elem, attrs) {

                angular.element(elem).on('click', function (event) {
                    event.preventDefault();

                    ModalService.show({
                        id: attrs.courseCreate,
                        view: '/HumanResources/Static/Course/courseCreate.html',
                        controller: 'CourseCreateCtrl',
                        service: 'CourseService',
                        size: 'lg',
                        cb: scope.refreshCallback()
                    });
                });

            }
        }

    }]);

})(window.angular);