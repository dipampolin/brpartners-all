﻿(function (angular) {
    'use strict';

    angular.module('brp')

    .factory("ReportViewerService", ['$http', 'PaginateRequest', function ($http, PaginateRequest) {

        var list = function (state) {
            return PaginateRequest.get('/RH/ReportViewer/List', state, 'Name')
                .then(function (res) {
                    return res.data;
                });
        };

        var findAlias = function () {
            return $http({
                method: 'GET',
                url: '/RH/ReportViewer/GetAlias'
            }).then(function (res) {
                return res.data;
            });
        }
        var findById = function (id) {
            return $http({
                method: 'GET',
                url: '/RH/ReportViewer/Find/' + id
            }).then(function (res) {
                return res.data;
            });
        }
        return {
            list: list,
            findAlias: findAlias,
            findById: findById
        }
    }]);

})(window.angular);