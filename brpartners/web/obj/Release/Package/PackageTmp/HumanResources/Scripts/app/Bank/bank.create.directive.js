﻿(function (angular) {
    'use strict';

    angular.module('brp')

        .directive("bankCreate", ['$uibModal', 'ModalService', function ($uibModal, ModalService) {
            return {
                restrict: "A",
                scope: {
                    refreshCallback: "&"
                },
                link: function (scope, elem, attrs) {

                    angular.element(elem).on('click', function (event) {
                        event.preventDefault();

                        ModalService.show({
                            id: attrs.bankCreate,
                            view: '/HumanResources/Static/Bank/bankCreate.html',
                            controller: 'BankCreateCtrl',
                            service: 'BankService',
                            cb: scope.refreshCallback()
                        });
                    });
                }
            }
        }]);
})(window.angular);