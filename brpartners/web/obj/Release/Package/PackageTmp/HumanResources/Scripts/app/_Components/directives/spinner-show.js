﻿(function (angular) {
    'use strict';

    angular.module('brp')
    .directive('loading', ['$http', function ($http) {
        return {
            restrict: 'A',
            link: function (scope, elm, attrs) {
                scope.isLoading = function () {
                    return $http.pendingRequests.length > 0;
                };

                scope.$watch(scope.isLoading, function (v) {
                    if (v) {
                        elm.show();
                        scope.showed = true;
                    } else {
                        scope.showed = false;
                        elm.hide();
                    }
                }, true);
            }
        }
    }])
}(window.angular));
