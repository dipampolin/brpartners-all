﻿
(function (angular, employee) {
    'use strict';

    angular.module('brp')

    .controller("EmployeeAttachmentImportCtrl", ['$uibModalInstance', 'EmployeeAttachmentService', 'data', 'toastr', 'Upload', '$timeout', function ($uibModalInstance, EmployeeAttachmentService, data, toastr, Upload, $timeout) {
        var $ctrl = this;

        this.data = data || {};

        $ctrl.cancel = function () {
            $uibModalInstance.close();
        };
      

        $ctrl.onFileSelect = function (file) {
            if (!file) {
                toastr.error('Arquivo acima do limite');
            }
            
            $ctrl.data.FileContent = file;
        }

        $ctrl.ok = function () {

            if ($ctrl.data.FileContent) {
                $ctrl.data = {
                    Id: $ctrl.data.Id || undefined,
                    Description: $ctrl.data.Description || undefined,
                    FileName: $ctrl.data.FileContent.Name || $ctrl.data.FileName,
                    EmployeeId: employee.Id,
                    FileContent: $ctrl.data.FileContent,
                }

                EmployeeAttachmentService.create($ctrl.data).then(function (result) {
                    toastr.success('Anexo salvo com sucesso.');
                    $uibModalInstance.close(result);
                }).catch(function (err) {
                    toastr.error('Falha ao salvar o Anexo!');
                });
            }
        }

    }]);

})(window.angular, window.employee);
