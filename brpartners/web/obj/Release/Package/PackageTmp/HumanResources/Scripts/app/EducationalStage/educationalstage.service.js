﻿(function (angular) {
    'use strict';

    angular.module('brp')

    .factory("EducationalStageService", ['$http', function ($http) {

        var list = function () {
            return $http({
                method: 'GET',
                url: '/RH/EducationalStage/List'
            }).then(function (res) {
                return res.data;
            });
        }

        return {
            list: list
        }

    }]);

})(window.angular);