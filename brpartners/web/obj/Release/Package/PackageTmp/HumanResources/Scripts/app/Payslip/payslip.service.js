﻿(function (angular) {
    'use strict'

    angular.module('brp')
        .factory("PayslipService", ['$http', 'moment', 'PaginateRequest', function ($http, moment, PaginateRequest) {

            var list = function (state) {

                return PaginateRequest.get('/RH/Payslip/List', state, 'CompanyName')
                .then(function (res) {
                    res.data.data.map(function (item) {
                        item.Date = moment(item.Date).toDate();;
                    });
                    return res.data;
                });

            }

            var remove = function (item) {

                return $http.post('/RH/Payslip/DeletePayment', {
                    Date: item.Date,
                    CompanyCode: item.CompanyCode
                });
            }

            return {
                list: list,
                remove: remove
            }
        }]);


})(window.angular)