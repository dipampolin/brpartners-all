﻿(function (angular, employee) {
    'use strict';

    angular.module('brp')

    .controller("EmployeeVehicleCtrl", ['EmployeeVehicleService', 'toastr', function (EmployeeVehicleService, toastr) {

        var $ctrl = this;

        $ctrl.employee = employee;

        this.refresh = function () {
            if (employee.Id) {
                $ctrl.isLoading = true;
                EmployeeVehicleService.list(employee.Id).then(function (result) {
                    $ctrl.data = result;
                    $ctrl.isLoading = false;
                });
            }
        }

        this.refresh();

        $ctrl.remove = function (id) {
            EmployeeVehicleService.remove(id).then(function (result) {
                toastr.success('Veículo excluído com sucesso.');
                EmployeeVehicleService.list(employee.Id).then(function (result) {
                    $ctrl.data = result;
                });
            }).catch(function (err) {
                toastr.error('Falha ao excluir Veículo!');
            });
        }
    }]);
})(window.angular, window.employee);
