﻿
(function (angular) {
    'use strict';

    angular.module('brp')

        .controller("EmployeeIndexCtrl", ['$scope', 'EmployeeService', 'toastr', function ($scope, EmployeeService, toastr) {

        var $ctrl = this;

        this.loadData = function (state) {
            $ctrl.isLoading = true;

            console.log(state);

            EmployeeService.list(state).then(function (result) {
                $ctrl.data = result.data;
                state.pagination.numberOfPages = result.pages;
                $ctrl.isLoading = false;
            });
        }


        $ctrl.remove = function (item) {
            EmployeeService.remove(item.Id).then(function (result) {
                toastr.success('Funcionário excluído com sucesso.');

                var index = $ctrl.data.indexOf(item);
                $ctrl.data.splice(index, 1); 

            }).catch(function (err) {
                toastr.error('Falha ao excluir Endereço!');
            });
        }
    }])

    .controller("EmployeeCreateCtrl", ['$uibModalInstance', 'EmployeeService', 'companies', function ($uibModalInstance, EmployeeService, companies) {
        var $ctrl = this;

        this.data = {};

        $ctrl.companyList = companies;

        $ctrl.ok = function () {
            $ctrl.data.CompanyId = $ctrl.company.Id;
            EmployeeService.create($ctrl.data).then(function (data) {
                $uibModalInstance.close();
                top.location.href = "/RH/Employee/Create/" + data.Id;
            });
        };

        $ctrl.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }]);

})(window.angular);
