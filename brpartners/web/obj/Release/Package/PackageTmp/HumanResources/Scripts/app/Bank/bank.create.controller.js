﻿(function (angular) {
    'use strict';

    angular.module('brp')

        .controller("BankCreateCtrl", ['$uibModalInstance', 'data', 'toastr', 'BankService', function ($uibModalInstance, data, toastr, BankService) {
            var $ctrl = this;

            this.data = data ||
                {
                    Code: undefined,
                    Name: undefined,
                    Active: true
                };

            $ctrl.ok = function () {
                BankService.save($ctrl.data).then(function (result) {
                    toastr.success('Banco salvo com sucesso.');
                    $uibModalInstance.close();
                }).catch(function () {
                    toastr.error('Falha ao salvar Banco!');
                });
            };

            $ctrl.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        }]);
})(window.angular);