﻿(function (angular) {
    'use strict';

    angular.module('brp')
    .directive("selectCity", ['LocationService', function (LocationService) {
    return {
        restrict: "E",
        template: "<select class='form-control input-sm' ng-model='model' ng-options='item.Id as item.Value for item in cities'><option value=''/></select>",
        scope: {
            model: "=ngModel",
            state: "="
        },
        controller: ['$scope', function ($scope) {

            $scope.$watch('state', function (state) {

                if (!state) {
                    $scope.cities = [];
                    return;
                }

                LocationService.listCities(state).then(function (cities) {
                    $scope.cities = cities;
                });

            });
        }],
        link: function (scope, element, attrs, ctrl) {
        }
    }
    }]);
})(window.angular);