﻿
(function (angular) {
    'use strict';

    angular.module('brp')

        .controller("CompanyIndexCtrl", ['$scope', 'CompanyService', 'toastr', function ($scope, CompanyService, toastr) {

        var $ctrl = this;

        this.loadData = function (state) {
            $ctrl.isLoading = true;

            CompanyService.list(state).then(function (result) {
                $ctrl.data = result.data;
                state.pagination.numberOfPages = result.pages;

                $ctrl.isLoading = false;
            });
        }

        this.remove = function (id) {
            CompanyService.remove(id)
                .then(function (result) {
                    $ctrl.refresh();
                    toastr.success('Empresa excluída com sucesso.');
                })
                .catch(function (err) {
                    if (err.status == 409) {
                        toastr.warning('Empresa vinculada à um Funcionário!');
                    } else {
                        toastr.error('Falha ao excluir Empresa!');
                    }
                });
        }

    }])

    .controller("CompanyCreateCtrl", ['$uibModalInstance', 'CompanyService', 'data', 'toastr', function ($uibModalInstance, CompanyService, data, toastr) {
        var $ctrl = this;

        this.data = data || {};

        $ctrl.ok = function () {
            CompanyService.save($ctrl.data).then(function myfunction() {
                toastr.success('Empresa salva com sucesso.');
                $uibModalInstance.close();
            }).catch(function (err) {
                console.log(1);
                toastr.error('Falha ao salvar Empresa!');
            });
        };

        $ctrl.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }]);

})(window.angular);
