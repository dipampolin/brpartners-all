﻿(function (angular, employee) {
    'use strict';

    angular.module('brp')

        .controller("EmployeeVariablesCreateCtrl", ['$uibModalInstance', 'EmployeeVariablesService', 'EmployeeVariablesTypeService', 'data', 'toastr', 
            function ($uibModalInstance, EmployeeVariablesService, EmployeeVariablesTypeService, data, toastr) {
        var $ctrl = this;

        this.data = data || {};

        this.data = {
            Id: this.data.Id || null,
            EmployeeVariablesTypeId: this.data.EmployeeVariablesTypeId || null,
            Description: this.data.Description || null,
            Value: this.data.Value || null,
            Date: this.data.Date ? moment(this.data.Date).toDate() : null,
            EmployeeId: employee.Id || null
        }
        
        $ctrl.ok = function () {
            EmployeeVariablesService.create($ctrl.data).then(function (result) {
                toastr.success('Variável salva com sucesso.');
                $uibModalInstance.close(result)
            }).catch(function (err) {
                toastr.error('Falha ao salvar Variável!');
            })
        };

        $ctrl.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        EmployeeVariablesTypeService.listAll().then(
            function (data) {
                $ctrl.variablesTypes = data;
            });
        
    }]);

})(window.angular, window.employee);
