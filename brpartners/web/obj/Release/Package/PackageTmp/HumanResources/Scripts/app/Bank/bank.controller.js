﻿(function (angular) {
    'use strict';

    angular
        .module('brp')

        .controller("BankIndexCtrl", ["$scope", "BankService", "toastr", function ($scope, BankService, toastr) {

            var $ctrl = this;

            this.loadData = function (state) {
                $ctrl.isLoading = true;

                BankService.paginateList(state).then(function (result) {
                    $ctrl.data = result.data;
                    state.pagination.numberOfPages = result.pages;
                    $ctrl.isLoading = false;
                });
            }

            this.remove = function(id) {
                BankService.remove(id)
                    .then(function (result) {
                        $ctrl.refresh();
                        toastr.success('Banco excluído com sucesso.');
                    })
                    .catch(function (err) {
                        if (err.status == 409) {
                            toastr.warning('Banco vinculado à um Funcionário!');
                        } else {
                            toastr.error('Falha ao excluir Banco!');
                        }
                    });
            }

        }]);


})(window.angular);