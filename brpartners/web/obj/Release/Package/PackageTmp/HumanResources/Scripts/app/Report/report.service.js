﻿(function (angular) {
    'use strict';

    angular.module('brp')

    .factory("ReportService", ['$http', 'PaginateRequest', function ($http, PaginateRequest) {

        var list = function (state) {
            return PaginateRequest.get('/RH/Report/List', state, 'Name')
                .then(function (res) {
                    return res.data;
                });
        };

        var findAlias = function () {
            return $http({
                method: 'GET',
                url: '/RH/Report/GetAlias'
            }).then(function (res) {
                return res.data;
            });
        }

        var save = function (alias, report) {
            return $http.post('/RH/Report/SaveQuery', { alias: alias, report: report }).then(function (res) {
                return res.data;
            });
        }

        var findById = function (id) {
            return $http({
                method: 'GET',
                url: '/RH/Report/Find/' + id
            }).then(function (res) {
                return res.data;
            });
        }

        var showReport = function (id) {
            return $http({
                method: 'POST',
                url: '/RH/Report/ShowReport/' + id
            }).then(function (res) {
                return res.data;
            });
        }

        var remove = function (item) {
            return $http({
                method: 'POST',
                url: '/RH/Report/Remove?reportId=' + item.Id
            }).then(function (res) {
                return res.data;
            });
        }

        var findFiltrableFields = function (reportId) {
            return $http({
                method: 'GET',
                url: '/RH/Report/GetFiltrable?id=' + reportId
            }).then(function (res) {
                return res.data;
            });
        };

        var getDateRoles = function () {
            return $http({
                method: 'GET',
                url: '/RH/Report/GetDateRoles'
            }).then(function (res) {
                return res.data;
            });
        };

        var addFilterInQuery = function (filters, reportId) {
            return $http.post('/RH/Report/AddQueryFilter', { filters: filters, reportId: reportId }).then(function (res) {
                return res.data;
            });
        }
        
        var getDateInterval = function () {
            var forEach = Array.prototype.forEach;
            var list = [];
            getDateRoles().then(function (result) {
                forEach.call(result, function (item) {
                    list.push({Id: item.Key, Value: item.Label})
                });
            });
            return list;
        }

        var getBooleanChoose = function () {
            return [{ Id: 1, Value: "Sim" }, { Id: 0, Value: "Não" }];
        }
        
        return {
            list: list,
            findAlias: findAlias,
            save: save,
            findById: findById,
            showReport: showReport,
            remove: remove,
            findFiltrableFields: findFiltrableFields,
            addFilterInQuery: addFilterInQuery,
            getDateInterval: getDateInterval,
            getBooleanChoose: getBooleanChoose
        }
    }]);

})(window.angular);