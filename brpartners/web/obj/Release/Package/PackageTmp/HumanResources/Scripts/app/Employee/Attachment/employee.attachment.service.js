﻿(function (angular) {
    'use strict';
    angular.module('brp')
    .factory("EmployeeAttachmentService", ['$http', function ($http) {
        
        var list = function (employeeId) {
            return $http({
                method: 'GET',
                url: '/RH/EmployeeAttachment/List',
                params: { EmployeeId: employeeId }
            }).then(function (res) {
                return res.data;
            });
        };

        var findById = function (id) {
            return $http({
                method: 'GET',
                url: '/RH/EmployeeAttachment/Find/' + id
            }).then(function (res) {
                return res.data;
            });
        }

        var remove = function (id) {
            return $http.get('/RH/EmployeeAttachment/Remove', {
                params: {
                    id: id
                }
            }).then(function (res) {
                return res.data;
            });
        }

        var create = function (data) {
            var formData = new FormData();
            for (var p in data) {
                formData.append(p, data[p]);
            }

            return $http.post('/RH/EmployeeAttachment/Create', formData, {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined
                }
            }).then(function (res) {
                return res.data;
            });
        }
        return {
            list: list,
            findById: findById,
            create: create,
            remove: remove
        }
    }]);

})(window.angular);