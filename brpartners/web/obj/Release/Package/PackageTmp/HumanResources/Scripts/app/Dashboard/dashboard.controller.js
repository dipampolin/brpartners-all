﻿(function (angular, model) {
    'use strict';

    angular.module('brp')
    .controller("DashboardCtrl", ['$scope', 'DashboardService', function ($scope, DashboardService) {
        var $ctrl = this;

        this.model = model;

        $ctrl.chartDepartment = {
            labels : Object.keys(model.DepartmentDistribution),
            data: function () {
                var fields = [];
                angular.forEach(model.DepartmentDistribution, function (value) {
                    fields.push(value);
                });
                return fields;
            }(),
            options : {
                legend: {
                    display: Object.keys(model.DepartmentDistribution).length <= 10,
                    position: 'top'
                },
                animation: {
                    duration: 500
                }
            }
        };

        DashboardService.listEmployeesWithNearBirthdays().then(function (result) {
            $ctrl.Birthdays = result;
        });

        DashboardService.listCompanyDistribution().then(function (employeeCompanies) {
           
            $ctrl.chartCompany = {
                labels: employeeCompanies.months,
                data :employeeCompanies.data,
                series: employeeCompanies.companies,
                options: {
                    legend: {
                        display: employeeCompanies.companies.length <= 10
                    },
                    animation: {
                        duration: 500
                    },
                    scales: {
                        yAxes: [
                          {
                              id: 'y-axis-1',
                              type: 'linear',
                              min: 0,
                              display: true,
                              position: 'left'
                          }
                        ]
                    }
                }
            };
        });
    }]);

})(window.angular, window.rhmodel || {});
