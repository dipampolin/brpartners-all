﻿(function (angular) {
    'use strict';

    angular.module('brp')

        .controller("RoleCreateCtrl", ['$uibModalInstance', 'data', 'toastr', 'RoleService', function ($uibModalInstance, data, toastr, RoleService) {
            var $ctrl = this;

            this.data = data ||
                {
                    Name: undefined,
                    Active: true
                };

            $ctrl.ok = function () {
                RoleService.save($ctrl.data).then(function (result) {
                    toastr.success('Cargo salvo com sucesso.');
                    $uibModalInstance.close();
                }).catch(function () {
                    toastr.error('Falha ao salvar Cargo!');
                });
            };

            $ctrl.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        }]);
})(window.angular);