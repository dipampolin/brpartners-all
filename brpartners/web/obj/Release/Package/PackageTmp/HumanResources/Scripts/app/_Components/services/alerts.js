﻿
(function (angular) {
    'use strict';

    angular.module('brp')

    .factory('TegraAlerts', ['$uibModal', '$q', function ($uibModal, $q) {
        
        var confirm = function (message, title) {
            return $uibModal.open({
                templateUrl: '/HumanResources/Static/confirm.html',
                backdrop: 'static',
                controller: ['$uibModalInstance', function ($uibModalInstance) {
                    this.question = message || "Tem certeza";
                    this.title = title || "Confirmar";
                    this.ok = $uibModalInstance.close;
                    this.cancel = $uibModalInstance.dismiss;
                }],
                controllerAs: '$ctrl'
            }).result;
        }

        var alert = function (message, title) {
            return $uibModal.open({
                templateUrl: '/HumanResources/Static/alert.html',
                backdrop: 'static',
                controller: ['$uibModalInstance', function ($uibModalInstance) {
                    this.message = message || "";
                    this.title = title || "Alerta";
                    this.ok = $uibModalInstance.close;
                }],
                controllerAs: '$ctrl'
            }).result;
        }

        return {
            confirm: confirm,
            alert: alert
        }
    }]);

})(window.angular);
