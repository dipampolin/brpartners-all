﻿(function (angular, employee) {
    'use strict';

    angular.module('brp')

    .controller("EmployeeVacationCreateCtrl", ['$uibModalInstance', 'EmployeeVacationService', 'data', 'toastr', function ($uibModalInstance, EmployeeVacationService, data, toastr) {
        var $ctrl = this;

        this.data = data || {};
        this.data = {
            Id: $ctrl.data.Id || undefined,
            AquisitionStartDate: this.data.AquisitionStartDate ? moment(this.data.AquisitionStartDate).toDate() : undefined,
            AquisitionEndDate: this.data.AquisitionEndDate ? moment(this.data.AquisitionEndDate).toDate() : undefined,
            EnjoymentStartDate: this.data.EnjoymentStartDate ? moment(this.data.EnjoymentStartDate).toDate() : undefined,
            EnjoymentEndDate: this.data.EnjoymentEndDate ? moment(this.data.EnjoymentEndDate).toDate() : undefined,
            ReceiptDate: this.data.ReceiptDate ? moment(this.data.ReceiptDate).toDate() : undefined,
            Value: this.data.Value || undefined,
            EmployeeId: employee.Id || undefined
        };

        $ctrl.ok = function () {
            EmployeeVacationService.create($ctrl.data).then(function (result) {
                toastr.success('Férias salvo com sucesso.');
                $uibModalInstance.close($ctrl.data);
            }).catch(function (err) {
                toastr.error('Falha ao salvar as Férias!');
            });
        };

        $ctrl.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }]);

})(window.angular, window.employee);
