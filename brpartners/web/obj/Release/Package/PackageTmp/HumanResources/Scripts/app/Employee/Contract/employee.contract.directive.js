﻿(function (angular) {
    'use strict';

    angular.module('brp')

    .directive("employeeContractCreate", ['$uibModal', 'EmployeeContractService', 'ModalService', function ($uibModal, EmployeeContractService, ModalService) {

        return {
            restrict: "A",
            scope: {
                refreshCallback: "&"
            },
            link: function (scope, elem, attrs) {
                angular.element(elem).on('click', function (event) {
                    event.preventDefault();

                    ModalService.show({
                        id: attrs.employeeContractCreate,
                        view: '/HumanResources/Static/Employee/Contract/employeeContractCreate.html',
                        controller: 'EmployeeContractCreateCtrl',
                        service: 'EmployeeContractService',
                        cb: scope.refreshCallback,
                    }).then(function () {

                    })
                });
            }
        }
    }]);
})(window.angular);
