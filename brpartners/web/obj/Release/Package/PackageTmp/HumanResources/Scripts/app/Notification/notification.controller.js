﻿(function (angular) {
    'use strict';

    angular.module('brp')
    .controller("NotificationIndexCtrl", ['$scope', 'NotificationService', 'toastr', function ($scope, NotificationService, toastr) {
        var $ctrl = this;

        this.loadData = function (state) {
            $ctrl.isLoading = true;

            NotificationService.list(state).then(function (result) {

                result.data.forEach(function (item) {
                    NotificationService.getRoleDescritionByEnumValue(item.Frequency).then(function (data) {
                        item.Frequency = data;
                    });
                });

                $ctrl.data = result.data;
                state.pagination.numberOfPages = result.pages;

                $ctrl.isLoading = false;
            });
        }

        $ctrl.remove = function (item) {
            NotificationService.remove(item).then(function () {
                toastr.success('Notificação removida com sucesso!');
                $ctrl.refresh();
            }).catch(function (err) {
                toastr.error('Erro ao excluir uma notificação!');
            });
        }

        $ctrl.notify = function (id) {
            NotificationService.notify(id).then(function (result) {
                toastr.success('Notificação enviada com sucesso!');
                $ctrl.refresh();
            }).catch(function (err) {
                toastr.error('Erro ao enviar a notificação!');
            });
        }

    }]).controller("NotificationCreateCtrl", ['$uibModalInstance', '$uibModal', 'ModalService', 'NotificationService', 'data', 'toastr', 'ReportService', '$filter', function ($uibModalInstance, $uibModal, ModalService, NotificationService, data, toastr, ReportService, $filter) {
        var $ctrl = this;
        this.data = data || {
            Items: [],
            Destinations: []
        };

        $ctrl.isLoading = true;

        ReportService.findAlias().then(function (items) {
            items.find(removeSupervisor);
            $ctrl.data.Items = $ctrl.data.Items || [];
            $ctrl.data.Items.forEach(function (item) {
                var originalItem = items.filter(function (oitem) {
                    return oitem.Field == item.Field && oitem.Table == item.Table;
                })[0] || null;

                var index = items.indexOf(originalItem);

                if (index >= 0) {
                    items.splice(index, 1);
                }

            });

            $ctrl.options = {
                title: 'Selecione os Campos',
                orderProperty: 'Field',
                items: items,
                selectedItems: $ctrl.data.Items ? $ctrl.data.Items : [],
                groups: $filter('unique')(items, "Table")
            };
        });

        function removeSupervisor(item, index, array) {
            if (item) {
                if (item.Field == 'SupervisorId') {
                    array.splice(index, 1);
                }
            }
        }

        if (!$ctrl.data.Id) {
            $ctrl.data.Active = true;
        }

        $ctrl.addDestination = function () {
            ModalService.show({
                view: '/HumanResources/Static/Notification/notificationDestinationCreate.html',
                controller: 'DestinationCreateCtrl'
            }).then(function (item) {
                $ctrl.data.Destinations = $ctrl.data.Destinations || [];
                $ctrl.data.Destinations.push(item);
            })
        };

        $ctrl.removeDestination = function (index) {
            $ctrl.data.Destinations.splice(index, 1);
        }


        NotificationService.getRoles().then(function (res) {
            $ctrl.frequency = res;
        });

        $ctrl.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        $ctrl.ok = function () {
            NotificationService.create(this.data).then(function (data) {
                console.log(data);
                ModalService.show({
                    view: '/HumanResources/Static/Notification/notificationFilterCreate.html',
                    controller: 'NotificationFilterCreateCtrl',
                    resolve: {
                        notificationId: data
                    }
                });
                toastr.success('Notificação salvo com sucesso!');
                $uibModalInstance.close();

            }).catch(function (err) {
                console.log(err);
                toastr.error('Erro ao criar a notificação: ' + err.data + ".");
            });
        }
    }]).controller("DestinationCreateCtrl", ['$uibModalInstance', '$uibModal', 'data', function ($uibModalInstance, $uibModal, data) {
        var $ctrl = this;
        this.data = data || {};

        $ctrl.isLoading = true;

        $ctrl.ok = function () {
            $uibModalInstance.close($ctrl.data);
        }

        $ctrl.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }]).controller("NotificationFilterCreateCtrl", ['$uibModalInstance', '$uibModal', 'data', 'NotificationService', 'notificationId', 'CostCenterService', 'CompanyService', 'ReportService', 'EmployeeService', 'toastr', function ($uibModalInstance, $uibModal, data, NotificationService, notificationId, CostCenterService, CompanyService, ReportService, EmployeeService, toastr) {
        var $ctrl = this;
        this.data = data || {};

        NotificationService.findFiltrableFields(notificationId).then(function (res) {
            for (var i = 0; i < res.length; i++) {
                if (res[i].Type === "Boolean" && res[i].Value != null) {
                    res[i].Value = (res[i].Value - 0);
                }
                res[i].Model = null;
            }
            $ctrl.data.Filters = res;
        });

        $ctrl.addMask = function (item) {
            if (item.Type === 'DateTime') {
                return {
                    mask: '99/99/9999',
                    placeHolder: '__/__/____',
                    length: 10,
                    type: "Date"
                }
            }
            if (item.Type === 'String') {
                return {
                    mask: undefined,
                    placeHolder: undefined,
                    length: 50,
                    type: "String"
                }
            }
            if (item.Type === 'Boolean') {
                return {
                    mask: undefined,
                    placeHolder: undefined,
                    length: undefined,
                    type: "Boolean"
                }
            }
        };

        $ctrl.initialValues = function (item) {
            if (item.Field === 'SupervisorId') {
                var id = parseInt(item.Value);
                if (!isNaN(id)) {
                    EmployeeService.findById(id).then(function (result) {
                        item.Model = result.Name;
                    });
                }
            }
        }

        $ctrl.selectDateInterval = ReportService.getDateInterval();

        $ctrl.allBooleanChoose = ReportService.getBooleanChoose();

        this.getAutoCompleteValue = function (val, item) {
            if (item.Table === 'Employee') {
                return EmployeeService.findByName(val, $ctrl.data.Id || 0);
            }
        };

        $ctrl.verifySupervisor = function (item) {
            if (!item.Model) {
                item.Value = null;
            }
        };

        CostCenterService.listReport().then(function (result) {
            $ctrl.selectCostCenter = result;
        });

        CompanyService.findReport().then(function (result) {
            $ctrl.selectCompany = result;
        });

        this.onSelect = function ($item, $model, $label, item) {
            item.Value = $item.Id;
            item.Model = $item.Name;
        }

        this.onSupervisorSelect = function myfunction($item, $model, $label, item) {
            item.Value = $item.Id;
            item.Model = $item.Name;
        }

        $ctrl.ok = function () {
            NotificationService.addFilterInQuery($ctrl.data, notificationId).then(function (res) {
                toastr.success('Filtro de Notificação salvo com Sucesso!');
                $uibModalInstance.close();
            });
        }

        $ctrl.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

    }])
})(window.angular);

