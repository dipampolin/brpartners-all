﻿
(function (angular) {
    'use strict';

    angular.module('brp')
    .directive("notificationIndexCreate", ['$uibModal', 'NotificationService', 'ModalService', function ($uibModal, NotificationService, ModalService) {

        return {
            restrict: "A",
            scope: {
                refreshCallback: "&"
            },
            link: function (scope, elem, attrs) {

                angular.element(elem).on('click', function (event) {
                    event.preventDefault();

                    ModalService.show({
                        size: 'lg',
                        id: attrs.notificationIndexCreate,
                        view: '/HumanResources/Static/Notification/notificationIndexCreate.html',
                        controller: 'NotificationCreateCtrl',
                        service: 'NotificationService',
                        cb: scope.refreshCallback()
                    });
                });

            }
        }

    }]);

})(window.angular);
