﻿
(function (angular) {
    'use strict';

    angular.module('brp')
    .filter('cep', function () {
        return function (val) {
            if (!val) {
                return '';
            }
            return val.toString().replace(/^(\d{5})(\d{3})$/, '$1-$2');
        }
    });

})(window.angular);
