﻿(function (angular) {
    'use strict';

    angular.module('brp')

    .controller("EmployeeRelationshipCreateCtrl", ['$uibModalInstance', 'EmployeeRelationshipService', 'data', 'employeeId', 'toastr', '$scope', function ($uibModalInstance, EmployeeRelationshipService, data, employeeId, toastr, $scope) {
        var $ctrl = this;

        this.data = data || {
            UseHealthPlan: false,
            UseOdontologyPlan: false
        };

        this.data.EmployeeId = this.data.EmployeeId || employeeId;

        $ctrl.options = [
             { value: false, label: 'Não' },
             { value: true, label: 'Sim' },
        ];

        EmployeeRelationshipService.getHealthPlan().then(function (result) {
            $ctrl.healthplans = result;
        }).catch(function (err) {
            console.log(err);
        });

        EmployeeRelationshipService.getDentalPlan().then(function (result) {
            $ctrl.dentalplans = result;
        }).catch(function (err) {
            console.log(err);
        });

        $ctrl.ok = function () {
            EmployeeRelationshipService.create($ctrl.data).then(function (result) {
                toastr.success('Pessoa Relacionada salvo com sucesso.');
                return $uibModalInstance.close(data);
            }).catch(function (err) {
                toastr.error('Falha ao salvar Pessoa Relacionada!');
            });
        };

        $ctrl.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        $scope.$watch('$ctrl.data.Birthdate', function (val) {
            if (!val) {
                $ctrl.data.Birthdate = undefined;
            }
        });

        $ctrl.relationshipTypes = EmployeeRelationshipService.getRelationshipTypes();
    }]);

})(window.angular);
