﻿(function (angular) {
    'use strict';

    angular.module('brp')

    .factory("EmployeeSalaryService", ['$http', 'dateParser', function ($http, dateParser) {

        var parseDates = function (data) {
            return dateParser.parse(data, 'StartDate', 'EndDate');
        }

        var list = function (id) {
            return $http.get('/RH/SalaryHistory/List', {
                params: {
                    employeeId: id
                }
            }).then(function (res) {
                return res.data;
            });
        }

        var create = function (data) {
            return $http.post('/RH/SalaryHistory/Create', data)
                .then(function (res) {
                    return res.data;
                });
        }

        var findById = function (id) {
            return $http.get('/RH/SalaryHistory/Find', {
                params: {
                    id: id
                }
            }).then(function (res) {
                return parseDates(res.data);
            });
        }

        var getCategorySalaries = function () {
            return [{ Id: 'CLT', Value: 'CLT' }, { Id: 'Estágio', Value: 'Estágio' }, { Id: 'Pró-labore', Value: 'Pró-labore' }];
        }

        var getPeriodSalaries = function () {
            return [{ Id: 'Mensal', Value: 'Mensal' }, { Id: 'Trimestral', Value: 'Trimestral' }, { Id: 'Semanal', Value: 'Semanal' }, { Id: 'Quinzenal', Value: 'Quinzenal' }];
        }

        var remove = function (id) {
            return $http.get('/RH/SalaryHistory/Remove', {
                params: {
                    id: id
                }
            }).then(function (res) {
                return res.data;
            });
        }

        return {
            getCategorySalaries: getCategorySalaries,
            getPeriodSalaries: getPeriodSalaries,
            list: list,
            create: create,
            findById: findById,
            remove: remove
        }
    }]);

})(window.angular);