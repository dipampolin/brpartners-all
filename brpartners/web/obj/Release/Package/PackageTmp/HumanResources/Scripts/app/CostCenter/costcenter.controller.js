﻿(function (angular) {
    'use strict';

    angular.module('brp')

        .controller("CostCenterIndexCtrl", ['CostCenterService', 'toastr', function (CostCenterService, toastr) {

        var $ctrl = this;

        this.loadData = function (state) {
            $ctrl.isLoading = true;

            CostCenterService.listAll(state).then(function (result) {
                $ctrl.data = result.data;
                state.pagination.numberOfPages = result.pages;
                $ctrl.isLoading = false;
            });
        }

        this.remove = function (id) {
            CostCenterService.remove(id)
                .then(function (result) {
                    $ctrl.refresh();
                    toastr.success('Centro de Custo excluído com sucesso.');
                })
                .catch(function (err) {
                    if (err.status == 409) {
                        toastr.warning('Centro de Custo vinculado à um Funcionário!');
                    } else {
                        toastr.error('Falha ao excluir Centro de Custo!');
                    }
                });
        }

    }]);
})(window.angular);
