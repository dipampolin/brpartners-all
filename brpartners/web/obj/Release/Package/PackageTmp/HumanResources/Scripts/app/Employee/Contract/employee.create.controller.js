﻿(function (angular, employee) {
    'use strict';

    angular.module('brp')

    .controller("EmployeeContractCreateCtrl", ['$uibModalInstance', 'EmployeeContractService', 'data', '$scope', 'toastr', function ($uibModalInstance, EmployeeContractService, data, $scope, toastr) {
        var $ctrl = this;

        this.data = data || {};

        this.data = {
            Id: this.data.Id || undefined,
            BenefitId: this.data.BenefitId || undefined,
            StartDate: this.data.StartDate || undefined,
            EndDate: this.data.EndDate || undefined,
            EmployeeId: employee.Id || undefined,
            Approved: this.data.Approved || undefined,
            Description: this.data.Description || undefined
        }

        if (data) {
            this.data.StartDate = moment(this.data.StartDate).toDate();
            if (this.data.EndDate) {
                this.data.EndDate = moment(this.data.EndDate).toDate();
            }
        }

        $ctrl.ok = function () {

            EmployeeContractService.create($ctrl.data).then(function (result) {
                toastr.success('Vencimento contrato salvo com sucesso.');
                $uibModalInstance.close(result);
            }).catch(function (err) {
                toastr.error('Falha ao salvar Vencimento contrato!');
            });
        };

        $ctrl.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }]);

})(window.angular, window.employee);
