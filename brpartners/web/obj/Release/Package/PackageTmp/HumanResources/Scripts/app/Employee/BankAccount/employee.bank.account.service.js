﻿(function (angular) {
    'use strict';

    angular.module('brp')
    .factory("EmployeeBankAccountService", ['$http', function ($http) {

        var create = function (data) {
            return $http.post('/RH/BankAccount/Save', data)
                .then(function (res) {
                    return res.data;
                });
        }

        var list = function (id) {
            return $http.get('/RH/BankAccount/List', {
                params: {
                    employeeId: id
                }
            }).then(function (res) {
                return res.data;
            });
        }

        var remove = function (id) {
            return $http.get('/RH/BankAccount/Remove', {
                params: {
                    id: id
                }
            }).then(function (res) {
                return res.data;
            });
        }

        var findById = function (id) {
            return $http.get('/RH/BankAccount/Find', {
                params: {
                    id: id
                }
            }).then(function (res) {
                return res.data;
            });
        }

        return {
            create: create,
            list: list,
            findById: findById,
            remove: remove
        };
    }]);

})(window.angular);