﻿(function (angular, employee) {
    'use strict';

    angular.module('brp')

    .controller("EmployeeSalaryCtrl", ['$scope', 'EmployeeService', 'EmployeeSalaryService', 'toastr', function ($scope, EmployeeService, EmployeeSalaryService, toastr) {

        var $ctrl = this;
        $ctrl.employee = employee;
      
        this.loadData = function () {
            $ctrl.isLoading = true;
            if (employee.Id) {
                EmployeeSalaryService.list(employee.Id).then(function (result) {
                    $ctrl.data = result;
                    $ctrl.isLoading = false;
                });
            }
        }
        this.loadData();

        $ctrl.remove = function (id) {
            EmployeeSalaryService.remove(id).then(function (result) {
                toastr.success('Linha de Salário excluído com sucesso.');
                EmployeeSalaryService.list(employee.Id).then(function (result) {
                    $ctrl.data = result;
                });
            }).catch(function (err) {
                toastr.error('Falha ao excluir Linha de Salário!');
            });
        }
    }]);
})(window.angular, window.employee);