﻿
(function (angular) {
    'use strict';

    angular.module('brp')

        .controller("BenefitIndexCtrl", ['$scope', 'BenefitService', 'toastr', function ($scope, BenefitService, toastr) {

        var $ctrl = this;

        this.loadData = function (state) {

            $ctrl.isLoading = true;
            BenefitService.list(state).then(function (result) {
                $ctrl.data = result.data;
                state.pagination.numberOfPages = result.pages;
                $ctrl.isLoading = false;
            });

        }

        this.remove = function (id) {
            BenefitService.remove(id).then(function (result) {
                $ctrl.refresh();
                toastr.success('Benefício excluído com sucesso.');
            }).catch(function (err) {
                if (err.status == 409 ) {
                    toastr.warning('Benefício vinculado à um Funcionário!');
                } else {
                    toastr.error('Falha ao excluir Benefício!');
                }
            });
        }

    }])

        .controller("BenefitCreateCtrl", ['$uibModalInstance', 'BenefitService', 'data', 'toastr', 'BenefitTypeService',
            function ($uibModalInstance, BenefitService, data, toastr, BenefitTypeService) {
        var $ctrl = this;

        this.data = data || {};
        if ($ctrl.data.EndDate == "Invalid Date" || !$ctrl.data.EndDate) {
            $ctrl.data.EndDate = undefined;
        }

        $ctrl.ok = function () {
            BenefitService.save($ctrl.data).then(function (result) {
                toastr.success('Benefício salvo com sucesso.');
                $uibModalInstance.close();
            }).catch(function () {
                toastr.error('Falha ao salvar Benefício!');
            });
        };

        $ctrl.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        BenefitTypeService.listAll().then(function (result) {
            $ctrl.listBenefitType = result;
        });
    }]);

})(window.angular);
