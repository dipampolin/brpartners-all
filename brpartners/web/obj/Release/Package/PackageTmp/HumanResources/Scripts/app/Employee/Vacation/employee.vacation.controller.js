﻿(function (angular, employee) {
    'use strict';

    angular.module('brp')

    .controller("EmployeeVacationCtrl", ['$scope', 'EmployeeService', 'EmployeeVacationService', 'toastr', function ($scope, EmployeeService, EmployeeVacationService, toastr) {

        var $ctrl = this;

        $ctrl.employee = employee;
        this.loadData = function () {
            $ctrl.isLoading = true;
            if (employee.Id) {
                $ctrl.isLoading = true;
                EmployeeVacationService.list(employee.Id).then(function (result) {
                    $ctrl.data = result;
                    $ctrl.isLoading = false;
                });
            }
        }
        this.loadData();

        $ctrl.remove = function (id) {
            EmployeeVacationService.remove(id).then(function (result) {
                toastr.success('Férias excluída com sucesso.');
                EmployeeVacationService.list(employee.Id).then(function (result) {
                    $ctrl.data = result;
                });
            }).catch(function (err) {
                toastr.error('Falha ao excluir Férias!');
            });
        }

    }]);
})(window.angular, window.employee);