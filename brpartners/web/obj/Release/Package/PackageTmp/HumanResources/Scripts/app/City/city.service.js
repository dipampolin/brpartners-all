﻿(function (angular) {
    'use strict';
    angular.module('brp')

    .factory("CityService", ['$http', function ($http) {

        var findByName = function (name, stateId) {
            return $http({
                method: 'GET',
                url: '/RH/City/FindByName?name=' + name + '&' + "stateId=" + stateId
            }).then(function (res) {
                return res.data;
            });
        };

        var findById = function (id) {
            return $http({
                method: 'GET',
                url: '/RH/City/Find/' + id
            }).then(function (res) {
                return res.data;
            });
        }

        return {
            findByName: findByName,
            findById: findById,
        }

    }]);

})(window.angular);