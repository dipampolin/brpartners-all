﻿(function (angular) {
    'use strict';

    angular.module('brp')
    .directive('upperCase',function ($compile) {
        return {
            require: 'ngModel',
            link: function (scope, element, attr, ngModelCtrl) {
                function fromUser(text) {
                    return text.toUpperCase();
                }
                ngModelCtrl.$parsers.push(fromUser);
            }
        };
    });
})(window.angular);
