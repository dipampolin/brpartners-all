﻿using QX3.Portal.Contracts.DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QX3.Portal.WebSite.Models
{
    public static class StatementHelper
    {
        public static string GetStatementName(int id)
        {
            StatementTypes a = (StatementTypes)id;

            return a.GetDescription();

            //switch (id)
            //{
            //    case 1:
            //        name = "Renda Fixa";
            //        break;
            //    case 2:
            //        name = "NDF";
            //        break;
            //    case 3:
            //        name = "Swap";
            //        break;
            //    case 4:
            //        name = "CCB/Fiança";
            //        break;
            //    case 5:
            //        name = "Opções Flexíveis";
            //        break;
            //    case 6:
            //        name = "Conta Cliente";
            //        break;
            //    case 7:
            //        name = "Moeda Estrangeira";
            //        break;
            //    default:
            //        break;
            //}

            //return name;
        }

        public static string GetStatements(List<int> list)
        {
            string names = string.Empty;

            if (list != null && list.Count > 0)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    names = names + GetStatementName(list[i]);

                    if (i < list.Count - 1)
                        names = names + ", ";
                }
            }

            return names;
        }

        public static List<KeyValuePair<string, int>> GetStatmentsNameValue()
        {
            List<KeyValuePair<string, int>> lst = CommonHelper.GetEnumValuesAndDescriptions<StatementTypes>();

            return lst;
        }

    }
}