﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.WebSite.CommonService;
using QX3.Portal.WebSite.Models;
using QX3.Portal.WebSite.Models.Channel;

namespace QX3.Portal.WebSite.Helper
{
    public class BrokerageDiscountHelper
    {

        private IChannelProvider<ICommonContractChannel> cmm;
        protected IChannelProvider<ICommonContractChannel> CommonProvider
        {
            get
            {
                if (cmm == null)
                    cmm = ChannelProviderFactory<ICommonContractChannel>.Create("CommonClassName");
                return cmm;
            }
        }

        public string BrokerDiscountListObject(List<BrokerageDiscount> brokerList, ref FormCollection form, int iCount)
        {
            JsonModel dtJson = new JsonModel();

            dtJson.sEcho = Convert.ToInt32(form["sEcho"]);

            if (brokerList == null) brokerList = new List<BrokerageDiscount>();

            dtJson.iTotalRecords = iCount;
            dtJson.iTotalDisplayRecords = iCount;

            var tableValues = new List<string[]>();
            List<String> rowData = new List<String>();

            if (brokerList != null)
            {
                AccessControlHelper acHelper = new AccessControlHelper();
                foreach (BrokerageDiscount broker in brokerList)
                {
                    string situation = "";
                    switch (broker.Situation)
                    {
                        case "Aprovar":
                            {
                                situation = "A";
                                break;
                            }
                        case "Para Efetuar":
                            {
                                situation = "F";
                                break;
                            }
                        case "Efetuado":
                            {
                                situation = "E";
                                break;
                            }
                        case "Reprovado":
                            {
                                situation = "R";
                                break;
                            }
                        default: break;
                    }
                    if ((broker.Situation == "Aprovar" && acHelper.HasFuncionalityPermission("Other.BrokerageDiscount", "btnApprove"))
                        || (broker.Situation == "Para Efetuar" && acHelper.HasFuncionalityPermission("Other.BrokerageDiscount", "btnEffective")))
                    {
                        rowData.Add("<input type='hidden' class='hdf-situacao' value='" + situation + "'><input type='checkbox' class='chk-broker-discount' value='" + broker.RequestId + "|" + broker.ClientCode + "' "
                            + ((broker.Situation != "Aprovar" && broker.Situation != "Para Efetuar") ? "disabled='disabled'" : "") + " />");
                    }
                    else
                    {
                        rowData.Add("<input type='hidden' class='hdf-situacao' value='" + situation + "'><input type='checkbox' class='chk-broker-discount' value='" + broker.RequestId + "|" + broker.ClientCode + "' "
                        + "disabled='disabled'" + " />");
                    }
                    rowData.Add(broker.ClientCode.ToString() + "-" + broker.DV.ToString());
                    rowData.Add(broker.ClientName);
                    rowData.Add(broker.Assessor);
                    rowData.Add((broker.StockExchange == 1) ? "Bovespa" : "BM&amp;F");
                    rowData.Add(broker.MarketType);
                    rowData.Add(broker.Operation);
                    rowData.Add(broker.Vigency);
                    rowData.Add(broker.StockCode);
                    rowData.Add((broker.StockCode.ToLower() == "todos") ? "-" : broker.Qtty.ToString("n0"));
                    rowData.Add((broker.StockDiscountPercentual.HasValue) ? broker.StockDiscountPercentual.Value.ToString("N") + "%" : broker.StockDiscountValue.Value.ToString("n"));

                    rowData.Add((!String.IsNullOrEmpty(broker.Description))
                        ? (broker.Description.Length > 100)
                            ? "<div>" + HttpUtility.HtmlEncode(broker.Description.Substring(0, 100)) + "..." + "<div style='display:none' class='tooltip'><span class='texto'>" + HttpUtility.HtmlEncode(broker.Description) + "</span><span class='rodape'></span></div></div>"
                            : "<div>" + HttpUtility.HtmlEncode(broker.Description) + "</div>"
                        : "");

                    if ((broker.Situation == "Aprovar" && acHelper.HasFuncionalityPermission("Other.BrokerageDiscount", "btnApprove"))
                        || (broker.Situation == "Para Efetuar" && acHelper.HasFuncionalityPermission("Other.BrokerageDiscount", "btnEffective")))
                        rowData.Add("<a href='javascript:;' class='exibir-modal'>" + broker.Situation + "</a>");
                    else
                        rowData.Add(broker.Situation);


                    rowData.Add(
                        ((acHelper.HasFuncionalityPermission("Other.BrokerageDiscount", "btnDelete")) ? "<a href='javascript:;' class='ico-excluir exclude-brokerage'>excluir</a>" : "") +
                        ((acHelper.HasFuncionalityPermission("Other.BrokerageDiscount", "btnLog")) ? "<a class='ico-hist view-log-detail' href='javascript:;'>log</a>" : ""));

                    tableValues.Add(rowData.ToArray());
                    rowData = new List<string>();

                }
                dtJson.aaData = tableValues;
                return Newtonsoft.Json.JsonConvert.SerializeObject(dtJson);
            }
            else
            {
                return Newtonsoft.Json.JsonConvert.SerializeObject(dtJson);
            }
        }

        public List<KeyValuePair<string, string>> GetMarketList(int stock)
        {
            List<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();

            if (stock == 1)
            {
                list.Add(new KeyValuePair<string, string>("", "Todos"));
                list.Add(new KeyValuePair<string, string>("GET", "Geral"));
                list.Add(new KeyValuePair<string, string>("VIS", "À Vista"));
                list.Add(new KeyValuePair<string, string>("OPC", "Opção"));
                list.Add(new KeyValuePair<string, string>("TER", "Termo"));
            }
            else if (stock == 2)
            {
                list.Add(new KeyValuePair<string, string>("", "Todos"));
                list.Add(new KeyValuePair<string, string>("FUT", "Futuro"));
                list.Add(new KeyValuePair<string, string>("OPC", "Opção"));
                list.Add(new KeyValuePair<string, string>("TER", "Termo"));
            }
            return list;
        }

        public int GetBrokerageDiscountPendents()
        {
            return CommonHelper.GetPendents("Other.BrokerageDiscount");
        }

        public BrokerageDiscount CreateBrokerageDiscountFilter(FormCollection form)
        {
            /*TODO: Filtro de cliente e assessor*/
            var filter = new BrokerageDiscount()
            {
                StockExchange = Convert.ToInt32(form["ddlStockMarket"]),
                MarketType = form["ddlMarketType"],
                Operation = form["ddlOperation"],
                Vigency = form["ddlVigency"],
                Situation = form["ddlSituation"]
            };

            if (!string.IsNullOrEmpty(form["txtClient"]))
            {
                filter.ClientFilter = new MarketHelper().GetClientsRangesToFilter(form["txtClient"]);
                //filter.ClientFilter = filter.ClientFilter.Substring(0, filter.ClientFilter.LastIndexOf(';'));
            }


            filter.Assessor = (!String.IsNullOrEmpty(form["txtAssessor"])) ? new MarketHelper().GetAssessorsRangesToFilter(form["txtAssessor"]) : "";//new MarketHelper().GetAssessorsRangesToFilter(form["txtAssessor"]);

            if (form["rdbDiscountType"] == "P")
            {
                if (!String.IsNullOrEmpty(form["txtMinorDiscountPercentual"])) filter.StockDiscountMinorPercentual = Convert.ToInt32(form["txtMinorDiscountPercentual"]);
                if (!String.IsNullOrEmpty(form["txtMajorDiscountPercentual"])) filter.StockDiscountMajorPercentual = Convert.ToInt32(form["txtMajorDiscountPercentual"]);

                if (!String.IsNullOrEmpty(form["txtMinorDiscountPercentual"]) || !String.IsNullOrEmpty(form["txtMajorDiscountPercentual"]))
                {
                    /*Informar valor comm range imporvável.*/
                    filter.StockDiscountMinorValue = -99999;
                    filter.StockDiscountMajorValue = -99999;
                }
            }
            else
            {
                if (!String.IsNullOrEmpty(form["txtMinorDiscountValue"])) filter.StockDiscountMinorValue = Convert.ToDecimal(form["txtMinorDiscountValue"]);
                if (!String.IsNullOrEmpty(form["txtMajorDiscountValue"])) filter.StockDiscountMajorValue = Convert.ToDecimal(form["txtMajorDiscountValue"]);

                if (!String.IsNullOrEmpty(form["txtMinorDiscountValue"]) || (!String.IsNullOrEmpty(form["txtMajorDiscountValue"])))
                {
                    /*Informar valor comm range imporvável.*/
                    filter.StockDiscountMinorPercentual = -99999;
                    filter.StockDiscountMajorPercentual = -99999;
                }
            }

            return filter;
        }

        public void CreateBrokerageDiscountRequests(ref List<BrokerageDiscount> requests, BrokerageDiscount req)
        {
            var marketTypeOpt = new List<KeyValuePair<string, string>>();
            if (req.StockExchange == 1)//Bovespa
                marketTypeOpt.Add(new KeyValuePair<string, string>("VIS", "Vista"));
            else
                marketTypeOpt.Add(new KeyValuePair<string, string>("FUT", "FUTURO"));
            marketTypeOpt.Add(new KeyValuePair<string, string>("OPC", "Opção"));
            marketTypeOpt.Add(new KeyValuePair<string, string>("TER", "Termo"));

            var operation = new List<KeyValuePair<string, string>>();
            operation.Add(new KeyValuePair<string, string>("S", "Normal"));
            operation.Add(new KeyValuePair<string, string>("N", "Day Trade"));

            if (String.IsNullOrEmpty(req.MarketType) || String.IsNullOrEmpty(req.Operation))
            {
                if (String.IsNullOrEmpty(req.MarketType))
                {
                    /*O desconto será dado a todos os mercados*/
                    foreach (var m in marketTypeOpt)
                    {
                        var request = new BrokerageDiscount()
                        {
                            ClientCode = req.ClientCode,
                            StockExchange = req.StockExchange,
                            Vigency = req.Vigency,
                            Description = req.Description,
                            StockCode = req.StockCode,
                            Qtty = req.Qtty,
                            StockDiscountPercentual = req.StockDiscountPercentual,
                            StockDiscountValue = req.StockDiscountValue,
                            MarketType = m.Key
                        };

                        if (!String.IsNullOrEmpty(req.Operation))
                        {
                            request.Operation = req.Operation;
                            requests.Add(request);
                        }
                        else
                        {
                            /*Se operação = todos, criar duas requisições para cada mercado*/
                            foreach (var o in operation)
                            {
                                var requestOperation = new BrokerageDiscount()
                                {
                                    ClientCode = req.ClientCode,
                                    StockExchange = req.StockExchange,
                                    Vigency = req.Vigency,
                                    Description = req.Description,
                                    StockCode = req.StockCode,
                                    Qtty = req.Qtty,
                                    StockDiscountPercentual = req.StockDiscountPercentual,
                                    StockDiscountValue = req.StockDiscountValue,
                                    MarketType = m.Key,
                                    Operation = o.Key
                                };

                                requests.Add(requestOperation);
                            }
                        }
                    }

                }
                else if (String.IsNullOrEmpty(req.Operation))
                {
                    /*apenas operação = todos*/
                    foreach (var o in operation)
                    {
                        var requestOperation = new BrokerageDiscount()
                        {
                            ClientCode = req.ClientCode,
                            StockExchange = req.StockExchange,
                            Vigency = req.Vigency,
                            Description = req.Description,
                            StockCode = req.StockCode,
                            Qtty = req.Qtty,
                            StockDiscountPercentual = req.StockDiscountPercentual,
                            StockDiscountValue = req.StockDiscountValue,
                            MarketType = req.MarketType,
                            Operation = o.Key
                        };

                        requests.Add(requestOperation);
                    }
                }
            }
            else
            {
                requests.Add(req);
            }

        }
    }
}