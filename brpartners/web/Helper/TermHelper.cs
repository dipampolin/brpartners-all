﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using QX3.Portal.Contracts.DataContracts;
using System.Text;
using QX3.Portal.WebSite.TermService;
using QX3.Portal.WebSite.Models.Channel;
using QX3.Spinnex.Common.Services.Logging;

namespace QX3.Portal.WebSite.Helper
{
    [Serializable]
    public class ClientTerm : TermItem
    {
        public int Index { get; set; }
    }

    public class TermHelper
    {
        public static string[] TermActions = new string[] { "Solicitar", "Alterar", "Excluir", "Efetuar", "Exportar" };

        public string GetClientTermsTableBody(List<TermItem> terms)
        {
            StringBuilder html = new StringBuilder();
            bool even = false;
            int index = 0;

            List<ClientTerm> clientTerms = (from t in terms
                                            select new ClientTerm
                                            {
                                                ID = t.ID,
                                                Client = t.Client,
                                                Company = t.Company,
                                                Contract = t.Contract,
                                                AvailableQuantity = t.AvailableQuantity,
                                                SettleQuantity = t.SettleQuantity,
                                                TotalQuantity = t.TotalQuantity,
                                                Type = t.Type,
                                                Status = t.Status,
                                                SettleDate = t.SettleDate,
                                                DueDate = t.DueDate,
                                                RolloverDate = t.RolloverDate,
                                                Index = index++
                                            }).ToList();

            HttpContext.Current.Session["Term.ClientTerms"] = clientTerms;

            if (clientTerms != null)
            {
                foreach (ClientTerm t in clientTerms)
                {
                    string trModel = @"<tr class='{4}'>
                                            <td>{1}<input type='hidden' id='hdfTerm{0}' name='hdfTerm{0}' value='{6}' /></td>
                                            <td>{2}</td>
                                            <td><input type='text' alt='stockQuantity' value='0,00' id='txtTerm{0}' name='txtTerm{0}' /></td>
                                            <td><span id='totalQuantity{0}'>{3}</span></td>
                                            <td><span id='dueDate{0}'>{8}</span></td>
                                            <td><span id='dueDate{0}'>{9}</span></td>
                                            <td><select id='ddlTerm{0}' name='ddlTerm{0}'><option value='12' selected >LPD</option><option value='13'>LPDE</option><option value='11'>LA</option></select><input type='hidden' id='hdfBusinessDate{0}' name='hdfBusinessDate{0}' value='{7}' /></td>
                                        </tr>";

                    html.AppendFormat(trModel, 
                        t.Index, 
                        t.Company.Value,
                        t.Contract, 
                        t.AvailableQuantity.ToString("N0"), 
                        even ? "even" : "odd",
                        t.AvailableQuantity.ToString("N0"), 
                        t.ID, 
                        MarketHelper.GetBusinessDay(3),
                        t.DueDate.HasValue ? t.DueDate.Value.ToString("dd/MM/yyyy") : "--",
                        t.RolloverDate.HasValue ? t.RolloverDate.Value.ToString("dd/MM/yyyy") : "--");
                    even = !even;
                }
            }
            
            return html.ToString();
        }

        public int GetNumberOfPending()
        {
            return QX3.Portal.WebSite.Models.CommonHelper.GetPendents("Term.Index");
        }
    }
}