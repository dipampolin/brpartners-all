﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using QX3.Portal.Contracts.DataContracts;
using System.Text;
using QX3.Portal.WebSite.Models;
using System.Web.Mvc;

namespace QX3.Portal.WebSite.Helper
{
    public class OtherHelper
    {
        #region Common

        private void GenericSort<T>(ref List<T> list, string sOrder)
        {
            if (list.Count > 0)
            {
                Type t = list[0].GetType();
                string[] OrderCommand = sOrder.Split('_');
                string OrderByProperty = OrderCommand[0];
                string Direction = OrderCommand[1];


                if (t.GetProperty(OrderByProperty) != null)
                {
                    if (Direction == "asc")
                    {
                        list = list.OrderBy
                            (
                                a =>
                                {
                                    var l = t.InvokeMember(OrderByProperty, System.Reflection.BindingFlags.GetProperty, null, a,
                                                 null);
                                    return l;
                                }
                            ).ToList();
                    }
                    else
                    {
                        list = list.OrderByDescending
                            (
                                a =>
                                t.InvokeMember(OrderByProperty, System.Reflection.BindingFlags.GetProperty, null, a,
                                                null)
                            ).ToList();
                    }
                }

            }
        }

        private List<T> GenericPagingList<T>(FormCollection form, List<T> list)
        {
            List<T> _list = new List<T>();

            if (list.Count > 0)
            {
                if (!String.IsNullOrEmpty(form["iDisplayStart"]) && (!String.IsNullOrEmpty(form["iDisplayLength"])) && form["iDisplayLength"] != "-1")
                {
                    try
                    {
                        _list = list.AsEnumerable().Skip(Convert.ToInt32(form["iDisplayStart"])).Take(Convert.ToInt32(form["iDisplayLength"])).ToList();
                    }
                    catch (InvalidOperationException)
                    {
                        _list = list.AsEnumerable().Skip(0).Take(Convert.ToInt32(form["iDisplayLength"])).ToList();
                    }
                }
            }

            return _list;
        }

        private string SortListCommand(FormCollection form, String[] conlumn)
        {
            string sOrder = String.Empty;

            if (!String.IsNullOrEmpty(form["iSortCol_0"]) && !String.IsNullOrEmpty(form["iSortingCols"]))
            {
                int columnIndex = Convert.ToInt32(form["iSortCol_0"]);
                string columnName = conlumn[columnIndex];
                sOrder += columnName;
                sOrder += "_" + form["sSortDir_0"];
            }
            return sOrder;
        }

        #endregion

        public static string[] OrdersCorrectionsActions = new string[] { "Solicitar", "Alterar", "Excluir", "Efetuar", "Exportar" };

        public static string[] BrokerageDiscountActions = new string[] { "Solicitar", "Aprovar", "Excluir", "Efetuar", "Exportar", "Reprovar" };

        public static string[] CustodyTransferActions = new string[] { "Solicitar", "Alterar", "Excluir", "Efetuar", "Exportar" };

        public string GetOrdersCorrectionTableBody(List<OrdersCorrection> corrections)
        {
            StringBuilder html = new StringBuilder();

            if (corrections != null)
                foreach (OrdersCorrection c in corrections)
                {
                    bool toRealize = (c.Status.Value == "7");
                    string chkName = "chkCorrection" + c.ID;

                    AccessControlHelper acHelper = new AccessControlHelper();
                    bool canEdit = acHelper.HasFuncionalityPermission("Other.OrdersCorrection", "btnEdit");
                    bool canDelete = acHelper.HasFuncionalityPermission("Other.OrdersCorrection", "btnDelete");
                    bool canRealize = acHelper.HasFuncionalityPermission("Other.OrdersCorrection", "btnRealize");
                    bool canSeeLog = acHelper.HasFuncionalityPermission("Other.OrdersCorrection", "btnLog");

                    string td0 = string.Format("<input id=\"chkCorrection{0}\" type=\"checkbox\" value=\"{0}\" name=\"chkCorrection{0}\" {1} />", c.ID, toRealize ? "" : "disabled");
                    string td7 = (toRealize && canRealize) ? string.Format("<a href=\"javascript:void(0);\" id=\"lnkRealize{0}\" class=\"exibir-modal\">{1}</a>", c.ID, c.Status.Description) : c.Status.Description;
                    string blankImg = " <img class=\"espaco\" src=\"../../img/ico/icon-space-table.gif\" />";
                    string td8 = string.Format("<a class=\"ico-detalhes fechado\" href=\"javascript:void(0);\" id=\"buttonDetails{0}\">mostrar detalhes</a>{1}{3}{2}", c.ID,
                        (toRealize && canEdit) ? string.Format("<a class=\"ico-editar\" href=\"javascript:void(0);\" onclick=\"OpenRequest({0})\">editar</a>", c.ID) : blankImg,
                        (toRealize && canDelete) ? string.Format("<a class=\"ico-excluir\" onclick=\"DeleteCorrection({0});return false;\" href=\"javascript:void(0);\">excluir</a>", c.ID) : blankImg, canSeeLog ? string.Format("<a class=\"ico-hist\" onclick=\"OpenCorrectionHistory({0})\" href=\"javascript:void(0);\">log</a>", c.ID) : string.Empty);

                    StringBuilder subhtml = new StringBuilder();
                    subhtml.Append(string.Format("<tr class=\"detalhes\" style=\"display:none;\" id=\"tbDetails{0}\"><td></td><td  valign=\"middle\"><table class=\"secundaria\" cellpadding=\"0\" cellspacing=\"0\"><thead><tr><th>Papel</th><th>Qtde</th><th>Preço</th></tr></thead><tbody>", c.ID));
                    foreach (OrdersCorrectionDetail d in c.Details)
                        subhtml.Append(string.Format("<tr><td>{0}</td><td>{1}</td><td>{2}</td></tr>", d.StockCode, d.Quantity.ToString("N0"), d.Value.ToString("N2")));
                    subhtml.Append(string.Format("</tbody></table></td><td valign=\"middle\" colspan=\"7\"><p><strong>{0}</strong>{1}</p></td></tr>", c.Description.Description, c.Comments));

                    string trFormat = string.Format("<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td><td>{6}</td><td>{7}</td><td>{8}</td></tr>",
                                                    td0,
                                                    c.Type.Description,
                                                    c.Responsible.Description,
                                                    string.Format("{0} {1}", c.ClientFrom.Value, c.ClientFrom.Description),
                                                    c.Type.Value == "6" ? string.Format("{0} {1}", c.ClientTo.Value, c.ClientTo.Description) : "Conta Erro",
                                                    c.Date.ToString("dd/MM/yyyy"),
                                                    c.D1 ? "D-1" : "D0",
                                                    td7,
                                                    td8);
                    html.Append(trFormat);
                    html.Append(subhtml);
                }

            return html.ToString();
        }

        public string OperationsMapListObject(List<OperationsMap> items, FormCollection form)
        {
            JsonModel jm = new JsonModel();
                        
            if (items != null)
            {
                var NRCCompleteList = items;

                List<String> list = new List<String>();

                String[] column = { "ClientCode", "ClientName", "Volume", "Brokerage", "" };

                this.GenericSort(ref NRCCompleteList, this.SortListCommand(form, column));

                var NRCPartialList = GenericPagingList(form, NRCCompleteList);

                jm.sEcho = Convert.ToInt32(form["sEcho"]);
                jm.iTotalRecords = NRCPartialList.Count;
                jm.iTotalDisplayRecords = NRCCompleteList.Count;
                jm.aaData = new List<String[]>();

                for (int i = 0; i < NRCPartialList.Count; i++)
                {
                    OperationsMap nrc = (OperationsMap)NRCPartialList[i];

                    list.Add(nrc.ClientCode.ToString());
                    list.Add(CommonHelper.GetClientName(nrc.ClientCode));
                    list.Add(nrc.Volume.Value.ToString("N0"));
                    list.Add(nrc.BrokerageValue.Value.ToString("N2"));
                    list.Add("<a rel='" + nrc.ClientCode.ToString() + "' href='javascript:void(0);' class='ico-detalhes fechado'>mostrar detalhes</a>");

                    jm.aaData.Add(list.ToArray());
                    list = new List<String>();
                }

            }

            return Newtonsoft.Json.JsonConvert.SerializeObject(jm);
        }

        public string OperatorsListObject(List<Operator> items, FormCollection form)
        {
            JsonModel jm = new JsonModel();

            if (items != null)
            {
                var NRCCompleteList = items;

                List<String> list = new List<String>();

                String[] column = { "OperatorName", "BrokerageValue" };

                this.GenericSort(ref NRCCompleteList, this.SortListCommand(form, column));

                var NRCPartialList = GenericPagingList(form, NRCCompleteList);

                jm.sEcho = Convert.ToInt32(form["sEcho"]);
                jm.iTotalRecords = NRCPartialList.Count;
                jm.iTotalDisplayRecords = NRCCompleteList.Count;
                jm.aaData = new List<String[]>();

                for (int i = 0; i < NRCPartialList.Count; i++)
                {
                    Operator nrc = (Operator)NRCPartialList[i];

                    list.Add("<a href='javascript:;' rel='" + nrc.OperatorCode.ToString() + "' class='link-operador' >" + nrc.OperatorName + "</a>");
                    list.Add(nrc.BrokerageValue.Value.ToString("N2"));

                    jm.aaData.Add(list.ToArray());
                    list = new List<String>();
                }

            }

            return Newtonsoft.Json.JsonConvert.SerializeObject(jm);
        }

		public string OperationsListObject(List<OperationClient> list, ref FormCollection form, int iCount)
		{
			JsonModel dtJson = new JsonModel();

			dtJson.sEcho = Convert.ToInt32(form["sEcho"]);

			if (list == null) list = new List<OperationClient>();

			dtJson.iTotalRecords = iCount;
			dtJson.iTotalDisplayRecords = iCount;

			var tableValues = new List<string[]>();
			List<String> rowData = new List<String>();

			if (list != null)
			{
				AccessControlHelper acHelper = new AccessControlHelper();
				foreach (var item in list)
				{
					string id = item.TradingDate.Value.ToShortDateString().Replace("/", string.Empty) + item.ClientCode.ToString();
					string idValue = item.TradingDate.Value.ToString("dd-MM-yyyy") + "|" + item.ClientCode.ToString();

					rowData.Add(string.Format("<input class=\"chkOperation\" type=\"checkbox\" name=\"chkOperation-{0}\" id=\"chkOperation-{0}\" value=\"{1}\" />", id, idValue));
					rowData.Add(item.TradingDate.HasValue ? item.TradingDate.Value.ToShortDateString() : "-");
					rowData.Add(item.ClientCode.ToString());
					rowData.Add(item.ClientName);
					rowData.Add(item.AssessorName);
					rowData.Add(!String.IsNullOrEmpty(item.DescStatus) ? item.DescStatus : "-");
					
					// Ações

                    string details = "<a target='_blank' href='" + new UrlHelper(HttpContext.Current.Request.RequestContext).Action("OperationReport", "Other", new { clientCode = item.ClientCode, tradingDate = item.TradingDate }) + "' class='ico-visualizar'></a>";
                    string sendmail = (acHelper.HasFuncionalityPermission("Other.Operations", "btnSendReport")) ? "<a href='javascript:;' class='ico-email'></a>" : "";
                    string hist = "<a href='javascript:;' class='ico-hist'></a>";

					rowData.Add(details+sendmail+hist);
					
					tableValues.Add(rowData.ToArray());
					rowData = new List<string>();
				}
			}

			dtJson.aaData = tableValues;

			return Newtonsoft.Json.JsonConvert.SerializeObject(dtJson);
		}

        #region Informe de rendimentos
        public string IncomeReport_StockMarket(IncomeReport items, FormCollection form) 
        {
            JsonModel jm = new JsonModel();

            if (items != null)
            {
                var NRCCompleteList = items.StockMarketList;

                if (NRCCompleteList != null)
                {

                    List<String> list = new List<String>();

                    String[] column = { "StockCode", "AvailableQuantity", "Price", "TotalValue" };

                    CommonHelper.GenericSort(ref NRCCompleteList, CommonHelper.SortListCommand(form, column));

                    var NRCPartialList = CommonHelper.GenericPagingList(form, NRCCompleteList);

                    jm.sEcho = Convert.ToInt32(form["sEcho"]);
                    jm.iTotalRecords = NRCPartialList.Count;
                    jm.iTotalDisplayRecords = NRCCompleteList.Count;
                    jm.aaData = new List<String[]>();

                    for (int i = 0; i < NRCPartialList.Count; i++)
                    {
                        StockMarket nrc = (StockMarket)NRCPartialList[i];

                        list.Add(nrc.StockCode);
                        list.Add((nrc.AvailableQuantity.HasValue) ? nrc.AvailableQuantity.Value.ToString("N0") : "-");
                        list.Add((nrc.Price.HasValue) ? nrc.Price.Value.ToString("N2") : "-");
                        list.Add((nrc.TotalValue.HasValue) ? nrc.TotalValue.Value.ToString("N2") : "-");

                        jm.aaData.Add(list.ToArray());
                        list = new List<String>();
                    }
                }

            }

            return Newtonsoft.Json.JsonConvert.SerializeObject(jm);
        }

        public string IncomeReport_StockRent(IncomeReport items, FormCollection form)
        {
            JsonModel jm = new JsonModel();

            if (items != null)
            {
                var NRCCompleteList = items.StockRentList;

                if (NRCCompleteList != null)
                {

                    List<String> list = new List<String>();
                    
                    String[] column = { "StockCode", "Type", "OpenDate", "PaymentDate", "Quantity", "Tax", "Quote", "TotalValue" };

                    CommonHelper.GenericSort(ref NRCCompleteList, CommonHelper.SortListCommand(form, column));

                    var NRCPartialList = CommonHelper.GenericPagingList(form, NRCCompleteList);

                    jm.sEcho = Convert.ToInt32(form["sEcho"]);
                    jm.iTotalRecords = NRCPartialList.Count;
                    jm.iTotalDisplayRecords = NRCCompleteList.Count;
                    jm.aaData = new List<String[]>();

                    for (int i = 0; i < NRCPartialList.Count; i++)
                    {
                        StockRent nrc = (StockRent)NRCPartialList[i];

                        list.Add(nrc.StockCode);
                        list.Add(nrc.Type);
                        list.Add((nrc.OpenDate.HasValue) ? nrc.OpenDate.Value.ToString("dd/MM/yyyy") : "-");
                        list.Add((nrc.PaymentDate.HasValue) ? nrc.PaymentDate.Value.ToString("dd/MM/yyyy") : "-");
                        list.Add((nrc.Quantity.HasValue) ? nrc.Quantity.Value.ToString("N0") : "-");
                        list.Add((nrc.Tax.HasValue) ? nrc.Tax.Value.ToString("N2") : "-");
                        list.Add((nrc.Quote.HasValue) ? nrc.Quote.Value.ToString("N2") : "-");
                        list.Add((nrc.TotalValue.HasValue) ? nrc.TotalValue.Value.ToString("N2") : "-");

                        jm.aaData.Add(list.ToArray());
                        list = new List<String>();
                    }
                }

            }

            return Newtonsoft.Json.JsonConvert.SerializeObject(jm);
        }

        public string IncomeReport_Options(IncomeReport items, FormCollection form)
        {
            JsonModel jm = new JsonModel();

            if (items != null)
            {
                var NRCCompleteList = items.OptionsList;

                if (NRCCompleteList != null)
                {

                    List<String> list = new List<String>();

                    String[] column = { "StockCode", "OperationType", "Type", "PaymentDate", "AvailableQtty", "Price", "Volume", "VistaVolume" };

                    
                    CommonHelper.GenericSort(ref NRCCompleteList, CommonHelper.SortListCommand(form, column));

                    var NRCPartialList = CommonHelper.GenericPagingList(form, NRCCompleteList);

                    jm.sEcho = Convert.ToInt32(form["sEcho"]);
                    jm.iTotalRecords = NRCPartialList.Count;
                    jm.iTotalDisplayRecords = NRCCompleteList.Count;
                    jm.aaData = new List<String[]>();

                    for (int i = 0; i < NRCPartialList.Count; i++)
                    {
                        BMFOptions nrc = (BMFOptions)NRCPartialList[i];

                        list.Add(nrc.StockCode);
                        list.Add(nrc.OperationType);
                        list.Add(nrc.Type);
                        list.Add((nrc.PaymentDate.HasValue) ? nrc.PaymentDate.Value.ToString("dd/MM/yyyy") : "-");
                        list.Add((nrc.AvailableQtty.HasValue) ? nrc.AvailableQtty.Value.ToString("N0") : "-");
                        list.Add((nrc.Price.HasValue) ? nrc.Price.Value.ToString("N2") : "-");
                        list.Add((nrc.Volume.HasValue) ? nrc.Volume.Value.ToString("N2") : "-");
                        list.Add((nrc.VistaVolume.HasValue) ? nrc.VistaVolume.Value.ToString("N2") : "-");

                        jm.aaData.Add(list.ToArray());
                        list = new List<String>();
                    }
                }

            }

            return Newtonsoft.Json.JsonConvert.SerializeObject(jm);
        }

        public string IncomeReport_BMFOptionsFuture(IncomeReport items, FormCollection form)
        {
            JsonModel jm = new JsonModel();

            if (items != null)
            {
                var NRCCompleteList = items.BMFOptionsFutureList;

                if (NRCCompleteList != null)
                {

                    List<String> list = new List<String>();
                    
                    String[] column = { "StockCode", "Type", "PaymentDate", "AvailableQuantity", "Option", "CV", "Position" };

                    CommonHelper.GenericSort(ref NRCCompleteList, CommonHelper.SortListCommand(form, column));

                    var NRCPartialList = CommonHelper.GenericPagingList(form, NRCCompleteList);

                    jm.sEcho = Convert.ToInt32(form["sEcho"]);
                    jm.iTotalRecords = NRCPartialList.Count;
                    jm.iTotalDisplayRecords = NRCCompleteList.Count;
                    jm.aaData = new List<String[]>();

                    for (int i = 0; i < NRCPartialList.Count; i++)
                    {
                        BMFOptionsFuture nrc = (BMFOptionsFuture)NRCPartialList[i];

                        list.Add(nrc.StockCode);
                        list.Add(nrc.Type);
                        list.Add((nrc.PaymentDate.HasValue) ? nrc.PaymentDate.Value.ToString("dd/MM/yyyy") : "-");
                        list.Add((nrc.Price.HasValue) ? nrc.Price.Value.ToString("N2") : "-");
                        list.Add(nrc.CV);
                        list.Add((nrc.Position.HasValue) ? nrc.Position.Value.ToString("N2") : "-");

                        jm.aaData.Add(list.ToArray());
                        list = new List<String>();
                    }
                }

            }

            return Newtonsoft.Json.JsonConvert.SerializeObject(jm);
        }

        public string IncomeReport_BMFGold(IncomeReport items, FormCollection form)
        {
            JsonModel jm = new JsonModel();

            if (items != null)
            {
                var NRCCompleteList = items.BMFGoldList;

                if (NRCCompleteList != null)
                {

                    List<String> list = new List<String>();

                    String[] column = { "Pounds", "DefaultQuantity", "Quote", "TotalValue" };

                    CommonHelper.GenericSort(ref NRCCompleteList, CommonHelper.SortListCommand(form, column));

                    var NRCPartialList = CommonHelper.GenericPagingList(form, NRCCompleteList);

                    jm.sEcho = Convert.ToInt32(form["sEcho"]);
                    jm.iTotalRecords = NRCPartialList.Count;
                    jm.iTotalDisplayRecords = NRCCompleteList.Count;
                    jm.aaData = new List<String[]>();

                    for (int i = 0; i < NRCPartialList.Count; i++)
                    {
                        BMFGold nrc = (BMFGold)NRCPartialList[i];

                        list.Add((nrc.Pounds.HasValue) ? nrc.Pounds.Value.ToString("N2") : "-");
                        list.Add((nrc.DefaultQuantity.HasValue) ? nrc.DefaultQuantity.Value.ToString("N0") : "-");
                        list.Add((nrc.Quote.HasValue) ? nrc.Quote.Value.ToString("N2") : "-");
                        list.Add((nrc.TotalValue.HasValue) ? nrc.TotalValue.Value.ToString("N2") : "-");

                        jm.aaData.Add(list.ToArray());
                        list = new List<String>();
                    }
                }

            }

            return Newtonsoft.Json.JsonConvert.SerializeObject(jm);
        }

        public string IncomeReport_BMFSwap(IncomeReport items, FormCollection form)
        {
            JsonModel jm = new JsonModel();

            if (items != null)
            {
                var NRCCompleteList = items.BMFSwapList;

                if (NRCCompleteList != null)
                {
                    
                    List<String> list = new List<String>();

                    String[] column = { "Contract", "Number", "Base", "PaymentDate", "RegisterDate", "NetTotal" };

                    CommonHelper.GenericSort(ref NRCCompleteList, CommonHelper.SortListCommand(form, column));

                    var NRCPartialList = CommonHelper.GenericPagingList(form, NRCCompleteList);

                    jm.sEcho = Convert.ToInt32(form["sEcho"]);
                    jm.iTotalRecords = NRCPartialList.Count;
                    jm.iTotalDisplayRecords = NRCCompleteList.Count;
                    jm.aaData = new List<String[]>();

                    for (int i = 0; i < NRCPartialList.Count; i++)
                    {
                        BMFSwap nrc = (BMFSwap)NRCPartialList[i];

                        list.Add(nrc.Contract);
                        list.Add((nrc.Number.HasValue) ? nrc.Number.Value.ToString("N0") : "-");
                        list.Add((nrc.Base.HasValue) ? nrc.Base.Value.ToString("N0") : "-");
                        list.Add((nrc.PaymentDate.HasValue) ? nrc.PaymentDate.Value.ToString("dd/MM/yyyy") : "-");
                        list.Add((nrc.RegisterDate.HasValue) ? nrc.RegisterDate.Value.ToString("dd/MM/yyyy") : "-");
                        list.Add((nrc.NetTotal.HasValue) ? nrc.NetTotal.Value.ToString("N2") : "-");

                        jm.aaData.Add(list.ToArray());
                        list = new List<String>();
                    }
                }

            }

            return Newtonsoft.Json.JsonConvert.SerializeObject(jm);
        }

        public string IncomeReport_IncomeProceeds(IncomeReport items, FormCollection form)
        {
            JsonModel jm = new JsonModel();

            if (items != null)
            {
                var NRCCompleteList = items.IncomeProceedsList;

                if (NRCCompleteList != null)
                {
                    
                    List<String> list = new List<String>();

                    String[] column = { "StockCode", "Type", "AvailableQuantity", "GrossValue", "IRPerc", "IRValue", "NetValue", "PaymentDate" };

                    CommonHelper.GenericSort(ref NRCCompleteList, CommonHelper.SortListCommand(form, column));

                    var NRCPartialList = CommonHelper.GenericPagingList(form, NRCCompleteList);

                    jm.sEcho = Convert.ToInt32(form["sEcho"]);
                    jm.iTotalRecords = NRCPartialList.Count;
                    jm.iTotalDisplayRecords = NRCCompleteList.Count;
                    jm.aaData = new List<String[]>();

                    for (int i = 0; i < NRCPartialList.Count; i++)
                    {
                        IncomeProceeds nrc = (IncomeProceeds)NRCPartialList[i];

                        list.Add(nrc.StockCode);
                        list.Add(nrc.Type);
                        list.Add((nrc.AvailableQuantity.HasValue) ? nrc.AvailableQuantity.Value.ToString("N0") : "-");
                        list.Add((nrc.GrossValue.HasValue) ? nrc.GrossValue.Value.ToString("N2") : "-");
                        list.Add((nrc.IRPerc.HasValue) ? nrc.IRPerc.Value.ToString("N2") : "-");
                        list.Add((nrc.IRValue.HasValue) ? nrc.IRValue.Value.ToString("N2") : "-");
                        list.Add((nrc.NetValue.HasValue) ? nrc.NetValue.Value.ToString("N2") : "-");
                        list.Add((nrc.PaymentDate.HasValue) ? nrc.PaymentDate.Value.ToString("dd/MM/yyyy") : "-");

                        jm.aaData.Add(list.ToArray());
                        list = new List<String>();
                    }
                }

            }

            return Newtonsoft.Json.JsonConvert.SerializeObject(jm);
        }

        public string IncomeReport_IncomeTerms(IncomeReport items, FormCollection form)
        {
            JsonModel jm = new JsonModel();

            if (items != null)
            {
                var NRCCompleteList = items.IncomeTermsList;

                if (NRCCompleteList != null)
                {
                    
                    List<String> list = new List<String>();

                    String[] column = { "StockCode", "AvailableQuantity", "Price", "PaymentDate", "Total" };

                    CommonHelper.GenericSort(ref NRCCompleteList, CommonHelper.SortListCommand(form, column));

                    var NRCPartialList = CommonHelper.GenericPagingList(form, NRCCompleteList);

                    jm.sEcho = Convert.ToInt32(form["sEcho"]);
                    jm.iTotalRecords = NRCPartialList.Count;
                    jm.iTotalDisplayRecords = NRCCompleteList.Count;
                    jm.aaData = new List<String[]>();

                    for (int i = 0; i < NRCPartialList.Count; i++)
                    {
                        IncomeTerms nrc = (IncomeTerms)NRCPartialList[i];

                        list.Add(nrc.StockCode);
                        list.Add((nrc.AvailableQuantity.HasValue) ? nrc.AvailableQuantity.Value.ToString("N0") : "-");
                        list.Add((nrc.Price.HasValue) ? nrc.Price.Value.ToString("N2") : "-");
                        list.Add((nrc.PaymentDate.HasValue) ? nrc.PaymentDate.Value.ToString("dd/MM/yyyy") : "-");
                        list.Add((nrc.Total.HasValue) ? nrc.Total.Value.ToString("N2") : "-");
                        

                        jm.aaData.Add(list.ToArray());
                        list = new List<String>();
                    }
                }

            }

            return Newtonsoft.Json.JsonConvert.SerializeObject(jm);
        }

        public string IncomeReport_IRRFDayTrade(IncomeReport items, FormCollection form)
        {
            JsonModel jm = new JsonModel();

            if (items != null)
            {
                var NRCCompleteList = items.IRRFDayTradeList;

                if (NRCCompleteList != null)
                {
                    List<String> list = new List<String>();

                    jm.sEcho = Convert.ToInt32(form["sEcho"]);
                    jm.iTotalRecords = NRCCompleteList.Count;
                    jm.aaData = new List<String[]>();

                    for (int i = 0; i < NRCCompleteList.Count; i++)
                    {
                        IRPeriod nrc = (IRPeriod)NRCCompleteList[i];

                        list.Add(nrc.Month);
                        list.Add((nrc.GrossValue.HasValue) ? nrc.GrossValue.Value.ToString("N2") : "-");
                        list.Add((nrc.Retained.HasValue) ? nrc.Retained.Value.ToString("N2") : "-");

                        jm.aaData.Add(list.ToArray());
                        list = new List<String>();
                    }
                }

            }

            return Newtonsoft.Json.JsonConvert.SerializeObject(jm);
        }

        public string IncomeReport_IRRFOperations(IncomeReport items, FormCollection form)
        {
            JsonModel jm = new JsonModel();

            if (items != null)
            {
                var NRCCompleteList = items.IRRFOperationsList;

                if (NRCCompleteList != null)
                {
                    List<String> list = new List<String>();

                    jm.sEcho = Convert.ToInt32(form["sEcho"]);
                    jm.iTotalRecords = NRCCompleteList.Count;
                    jm.aaData = new List<String[]>();

                    for (int i = 0; i < NRCCompleteList.Count; i++)
                    {
                        IRPeriod nrc = (IRPeriod)NRCCompleteList[i];

                        list.Add(nrc.Month);
                        list.Add((nrc.GrossValue.HasValue) ? nrc.GrossValue.Value.ToString("N2") : "-");
                        list.Add((nrc.Retained.HasValue) ? nrc.Retained.Value.ToString("N2") : "-");

                        jm.aaData.Add(list.ToArray());
                        list = new List<String>();
                    }
                }

            }

            return Newtonsoft.Json.JsonConvert.SerializeObject(jm);
        }

        #endregion

        #region Private 

        public string Private_List(List<Private> items, FormCollection form, string refDate)
        {
            JsonModelPrivate jm = new JsonModelPrivate();

            if (items != null)
            {
                var NRCCompleteList = items;

                if (NRCCompleteList != null)
                {
                    List<String> list = new List<String>();

                    String[] column = { "CPFCGC", "ClientName", "Product", "BrokerageValue", "PositionValue", "AvailableValue", "CaptationValue", "ApplicationValue", "RedemptionValue" };
                    
                    Dictionary<string,Type> columnTypes = new Dictionary<string,Type>();
                    columnTypes.Add("CPFCGC", typeof(System.Int64));
                    columnTypes.Add("ClientName", typeof(System.String));
                    columnTypes.Add("Product", typeof(System.String));
                    columnTypes.Add("BrokerageValue", typeof(System.Decimal));
                    columnTypes.Add("PositionValue", typeof(System.Decimal));
                    columnTypes.Add("AvailableValue", typeof(System.Decimal));
                    columnTypes.Add("CaptationValue", typeof(System.Decimal));
                    columnTypes.Add("ApplicationValue", typeof(System.Decimal));
                    columnTypes.Add("RedemptionValue", typeof(System.Decimal));

                    CommonHelper.GenericSort(ref NRCCompleteList, CommonHelper.SortListCommand(form, column), columnTypes);

                    var NRCPartialList = CommonHelper.GenericPagingList(form, NRCCompleteList);

                    jm.sEcho = Convert.ToInt32(form["sEcho"]);
                    jm.iTotalRecords = NRCPartialList.Count;
                    jm.iTotalDisplayRecords = NRCCompleteList.Count;
                    jm.aaData = new List<String[]>();

                    for (int i = 0; i < NRCPartialList.Count; i++)
                    {
                        Private nrc = (Private)NRCPartialList[i];

                        list.Add(nrc.CPFCGC.ToString());
                        list.Add(nrc.ClientName);
                        list.Add(nrc.Product);
                        list.Add((nrc.BrokerageValue.HasValue) ? nrc.BrokerageValue.Value.ToString("N2") : "-");
                        list.Add((nrc.PositionValue.HasValue) ? nrc.PositionValue.Value.ToString("N2") : "-");
                        list.Add((nrc.AvailableValue.HasValue) ? nrc.AvailableValue.Value.ToString("N2") : "-");
                        list.Add((nrc.CaptationValue.HasValue) ? nrc.CaptationValue.Value.ToString("N2") : "-");
                        list.Add((nrc.ApplicationValue.HasValue) ? nrc.ApplicationValue.Value.ToString("N2") : "-");
                        list.Add((nrc.RedemptionValue.HasValue) ? nrc.RedemptionValue.Value.ToString("N2") : "-");

                        jm.aaData.Add(list.ToArray());
                        list = new List<String>();
                    }
                }
            }

            jm.RefDate = refDate;

            return Newtonsoft.Json.JsonConvert.SerializeObject(jm);
        }

        #endregion
    }
}