﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.WebSite.Models;

namespace QX3.Portal.WebSite.Helper
{
    public class CustodyTransferHelper
    {

        public string CustodyTransferListObject(List<CustodyTransfer> list, ref FormCollection form, int iCount) 
        {
            JsonModel dtJson = new JsonModel();

            dtJson.sEcho = Convert.ToInt32(form["sEcho"]);

            if (list == null) list = new List<CustodyTransfer>();

            dtJson.iTotalRecords = iCount;
            dtJson.iTotalDisplayRecords = iCount;

            var tableValues = new List<string[]>();
            List<String> rowData = new List<String>();

            
            AccessControlHelper acHelper = new AccessControlHelper();
            foreach (CustodyTransfer cst in list)
            {
                    
                rowData.Add("<input type='hidden' class='hdf-situacao' value='" + cst.Status + "'><input type='checkbox' class='chk-custody-transfer' value='" + cst.RequestId + "|" + cst.ClientCode + "' "
                    + ((cst.Status != "A") ? "disabled='disabled'" : "") + " />");
                rowData.Add(cst.ClientCode.ToString() + "-" + cst.DV.ToString());
                rowData.Add(cst.ClientName);
                rowData.Add(cst.Assessor);
                rowData.Add(cst.StockCode);
                rowData.Add(cst.Qtty.ToString("n0"));
                rowData.Add(cst.OrigBroker.ToString());
                rowData.Add(cst.OrigPortfolio.ToString());
                rowData.Add(cst.DestinyBroker.ToString());
                rowData.Add(cst.DestinyPortfolio.ToString());
                if (cst.Status == "A" && acHelper.HasFuncionalityPermission("Other.CustodyTransfer", "btnEffective"))
                    rowData.Add("<a href='javascript:;' class='exibir-modal'>" + cst.DescStatus + "</a>");
                else
                    rowData.Add(cst.DescStatus);

                rowData.Add(cst.RequestDate.ToShortDateString());

                rowData.Add((!String.IsNullOrEmpty(cst.Observations))
                    ? (cst.Observations.Length > 100)
                        ? "<div>" + HttpUtility.HtmlEncode(cst.Observations.Substring(0, 100)) + "..." + "<div style='display:none' class='tooltip'><span class='texto'>" + HttpUtility.HtmlEncode(cst.Observations) + "</span><span class='rodape'></span></div></div>"
                        : "<div>" + HttpUtility.HtmlEncode(cst.Observations) + "</div>"
                    : "");

                //rowData.Add("");
                rowData.Add(
                    ((acHelper.HasFuncionalityPermission("Other.CustodyTransfer", "btnEdit") && cst.Status == "A") ? "<a href='javascript:;' class='ico-editar'>editar</a>" : " <img class=\"espaco\" src=\"../../img/ico/icon-space-table.gif\" />") +
                    ((acHelper.HasFuncionalityPermission("Other.CustodyTransfer", "btnDelete") && cst.Status != "T") ? "<a href='javascript:;' class='ico-excluir'>excluir</a>" : " <img class=\"espaco\" src=\"../../img/ico/icon-space-table.gif\" />") +
                    ((acHelper.HasFuncionalityPermission("Other.CustodyTransfer", "btnLog") && cst.Status != "T") ? "<a class='ico-hist view-log-detail' href='javascript:;'>log</a>" : " <img class=\"espaco\" src=\"../../img/ico/icon-space-table.gif\" />"));

                tableValues.Add(rowData.ToArray());
                rowData = new List<string>();

            }
            

            dtJson.aaData = tableValues;

            return Newtonsoft.Json.JsonConvert.SerializeObject(dtJson);
        }

        public int GetCustodyTransferPendents() 
        {
            return CommonHelper.GetPendents("Other.CustodyTransfer");
        }

    }
}