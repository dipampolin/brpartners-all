﻿using iTextSharp.text;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.draw;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.Services.SagazWebService;
using QX3.Portal.WebSite.Helper;
using QX3.Portal.WebSite.Models.Channel;
using QX3.Portal.WebSite.OnlinePortfolioService;
using QX3.Spinnex.Common.Services.Formatting;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace QX3.Portal.WebSite.Models
{
    public class PDFHelper
    {
        static IFormatProvider cultureInfo = new System.Globalization.CultureInfo("en-US", true);
        public static bool DisclaimerPage = false;

        #region NonResidentTraders

        public MemoryStream CreateConfirmationToMemoryStream(Confirmation report)
        {
            iTextSharp.text.Document doc = new iTextSharp.text.Document(new Rectangle(970, 1371), 25, 25, 25, 45);
            MemoryStream memStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
            writer.CloseStream = false;

            writer.PageEvent = new PrintHeaderFooter { Title = "Confirmation" };

            doc.Open();

            Font accountOfFont = FontFactory.GetFont("Arial", 11, Font.BOLD, new BaseColor(111, 111, 111));
            Font youFont = FontFactory.GetFont("Arial", 14, Font.NORMAL, new BaseColor(111, 111, 111));
            Font stockFont = FontFactory.GetFont("Arial", 18, Font.BOLD, new BaseColor(111, 111, 111));
            Font stockInfoFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(111, 111, 111));

            #region Header

            Font lineFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(111, 111, 111));
            Font lineHeaderFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(255, 255, 255));
            Font lineFooterFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(255, 255, 255));


            string clientAddress = string.Format("{0}\nPhone: {1}\nFAX: {2}\nEmail: {3}", report.Address, report.PhoneNumber, report.FaxNumber, report.Email);
            string clientAccount = string.Format("Account Number: BV-{0}  Sales Rap: {1}", report.ClientCode, report.ClientCode);

            AddBvReportHeader(ref doc, "Confirmation", report.Header, report.ClientName, clientAddress, clientAccount);

            #endregion

            Paragraph warning1 = new Paragraph(" We confirm the trade (s) below subject  to the terms and conditions seth forthon the back of this confirmation.", accountOfFont);
            warning1.SpacingAfter = 25;
            doc.Add(warning1);

            #region Items

            List<ConfirmationItem> items = new List<ConfirmationItem>();
            items = report.Items;

            if (items.Count > 0)
            {
                PdfPTable tableItems = new PdfPTable(3);
                tableItems.WidthPercentage = 100;
                var colWidthPercentages = new[] { 425f, 40f, 425f };
                tableItems.HorizontalAlignment = Element.ALIGN_LEFT;
                tableItems.SetWidths(colWidthPercentages);

                int counter = 0;

                foreach (ConfirmationItem i in items)
                {
                    PdfPCell cellItem = new PdfPCell();
                    cellItem.Border = 0;
                    cellItem.BorderColor = new BaseColor(255, 255, 255);
                    cellItem.HorizontalAlignment = Element.ALIGN_LEFT;

                    PdfPTable tableSymbol = new PdfPTable(2);
                    tableSymbol.SpacingAfter = tableSymbol.SpacingBefore = 0;
                    tableSymbol.HorizontalAlignment = Element.ALIGN_LEFT;
                    tableSymbol.WidthPercentage = 100;

                    PdfPCell cellSymbol = new PdfPCell(new Paragraph(string.Format("You ({0})", i.OperationType == "B" ? "bought" : "Sold"), youFont));
                    cellSymbol.Colspan = 2;
                    cellSymbol.BorderColor = new BaseColor(255, 255, 255);
                    cellSymbol.Border = 0;
                    cellSymbol.PaddingBottom = 15;
                    cellSymbol.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cellSymbol.HorizontalAlignment = Element.ALIGN_LEFT;
                    tableSymbol.AddCell(cellSymbol);

                    //Symbol
                    cellSymbol = new PdfPCell(new Paragraph(string.Format("{0} - {1}", i.Symbol, this.ChangeCaps(i.CompanyName)), stockFont));
                    cellSymbol.BorderColor = new BaseColor(255, 255, 255);
                    cellSymbol.Border = 0;
                    cellSymbol.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cellSymbol.HorizontalAlignment = Element.ALIGN_LEFT;
                    tableSymbol.AddCell(cellSymbol);

                    //Trade ID
                    cellSymbol = new PdfPCell(new Paragraph(string.Format("Trade ID: {0}", i.TradeId), stockInfoFont));
                    cellSymbol.BorderColor = new BaseColor(255, 255, 255);
                    cellSymbol.Border = 0;
                    cellSymbol.VerticalAlignment = Element.ALIGN_BOTTOM;
                    cellSymbol.HorizontalAlignment = Element.ALIGN_LEFT;
                    tableSymbol.AddCell(cellSymbol);

                    //Quantity
                    cellSymbol = new PdfPCell(new Paragraph(string.Format("QTY: {0}", i.Quantity.ToString("N0", cultureInfo)), stockInfoFont));
                    cellSymbol.BorderColor = new BaseColor(255, 255, 255);
                    cellSymbol.Border = 0;
                    cellSymbol.VerticalAlignment = Element.ALIGN_BOTTOM;
                    cellSymbol.HorizontalAlignment = Element.ALIGN_LEFT;
                    tableSymbol.AddCell(cellSymbol);

                    //ISIN
                    cellSymbol = new PdfPCell(new Paragraph(string.Format("ISIN code: {0}", i.ISINCode), stockInfoFont));
                    cellSymbol.BorderColor = new BaseColor(255, 255, 255);
                    cellSymbol.Border = 0;
                    cellSymbol.VerticalAlignment = Element.ALIGN_BOTTOM;
                    cellSymbol.HorizontalAlignment = Element.ALIGN_LEFT;
                    cellSymbol.PaddingTop = 5;
                    tableSymbol.AddCell(cellSymbol);

                    //Unit Price
                    cellSymbol = new PdfPCell(new Paragraph(string.Format("Unit Price: {0}", i.TradeValue.ToString("N2", cultureInfo)), stockInfoFont));
                    cellSymbol.BorderColor = new BaseColor(255, 255, 255);
                    cellSymbol.Border = 0;
                    cellSymbol.VerticalAlignment = Element.ALIGN_TOP;
                    cellSymbol.HorizontalAlignment = Element.ALIGN_LEFT;
                    cellSymbol.PaddingBottom = 10;
                    tableSymbol.AddCell(cellSymbol);

                    //Currency
                    cellSymbol = new PdfPCell(new Paragraph(string.Format("Currency: {0}", i.CurrencyName), stockInfoFont));
                    cellSymbol.BorderColor = new BaseColor(255, 255, 255);
                    cellSymbol.Border = 0;
                    cellSymbol.VerticalAlignment = Element.ALIGN_TOP;
                    cellSymbol.HorizontalAlignment = Element.ALIGN_LEFT;
                    cellSymbol.PaddingBottom = 15;
                    tableSymbol.AddCell(cellSymbol);

                    cellItem.AddElement(tableSymbol);

                    //Adding the dotted line
                    Image grayLine = Image.GetInstance(System.Web.HttpContext.Current.Server.MapPath("~/Content/images/dotted-line.png"));
                    grayLine.Alignment = Image.ALIGN_CENTER;
                    grayLine.WidthPercentage = 100;
                    cellItem.AddElement(grayLine);

                    PdfPTable tableItemContent = new PdfPTable(2);
                    tableItemContent.SpacingBefore = 15;
                    tableItemContent.WidthPercentage = 100;
                    tableItemContent.HorizontalAlignment = Element.ALIGN_LEFT;

                    PdfPCell cellItemContent = new PdfPCell(new Phrase("Trade Date", stockInfoFont));
                    cellItemContent.BorderColor = new BaseColor(255, 255, 255);
                    cellItemContent.PaddingLeft = cellItemContent.PaddingBottom = 5;
                    cellItemContent.PaddingTop = 3;
                    cellItemContent.BackgroundColor = new BaseColor(238, 239, 239);
                    tableItemContent.AddCell(cellItemContent);
                    cellItemContent = new PdfPCell(new Phrase(i.TradeDate.ToString("MM/dd/yyyy", cultureInfo), stockInfoFont));
                    cellItemContent.PaddingLeft = cellItemContent.PaddingBottom = 5;
                    cellItemContent.PaddingTop = 3;
                    cellItemContent.BorderColor = new BaseColor(255, 255, 255);
                    cellItemContent.BackgroundColor = new BaseColor(238, 239, 239);
                    tableItemContent.AddCell(cellItemContent);
                    cellItemContent = new PdfPCell(new Phrase("Settlement Date", stockInfoFont));
                    cellItemContent.PaddingLeft = cellItemContent.PaddingBottom = 5;
                    cellItemContent.PaddingTop = 3;
                    cellItemContent.BorderColor = new BaseColor(255, 255, 255);
                    cellItemContent.BackgroundColor = new BaseColor(230, 231, 231);
                    tableItemContent.AddCell(cellItemContent);
                    cellItemContent = new PdfPCell(new Phrase(i.SettlementDate.ToString("MM/dd/yyyy", cultureInfo), stockInfoFont));
                    cellItemContent.PaddingLeft = cellItemContent.PaddingBottom = 5;
                    cellItemContent.PaddingTop = 3;
                    cellItemContent.BorderColor = new BaseColor(255, 255, 255);
                    cellItemContent.BackgroundColor = new BaseColor(230, 231, 231);
                    tableItemContent.AddCell(cellItemContent);
                    cellItemContent = new PdfPCell(new Phrase("Principal", stockInfoFont));
                    cellItemContent.PaddingLeft = cellItemContent.PaddingBottom = 5;
                    cellItemContent.PaddingTop = 3;
                    cellItemContent.BorderColor = new BaseColor(255, 255, 255);
                    cellItemContent.BackgroundColor = new BaseColor(238, 239, 239);
                    tableItemContent.AddCell(cellItemContent);
                    cellItemContent = new PdfPCell(new Phrase(i.PrincipalValue.ToString("N2", cultureInfo), stockInfoFont));
                    cellItemContent.PaddingRight = cellItemContent.PaddingBottom = 5;
                    cellItemContent.PaddingTop = 3;
                    cellItemContent.HorizontalAlignment = Element.ALIGN_RIGHT;
                    cellItemContent.BorderColor = new BaseColor(255, 255, 255);
                    cellItemContent.BackgroundColor = new BaseColor(238, 239, 239);
                    tableItemContent.AddCell(cellItemContent);
                    cellItemContent = new PdfPCell(new Phrase("Commission", stockInfoFont));
                    cellItemContent.PaddingLeft = cellItemContent.PaddingBottom = 5;
                    cellItemContent.PaddingTop = 3;
                    cellItemContent.BorderColor = new BaseColor(255, 255, 255);
                    cellItemContent.BackgroundColor = new BaseColor(230, 231, 231);
                    tableItemContent.AddCell(cellItemContent);
                    cellItemContent = new PdfPCell(new Phrase(i.CommissionValue.ToString("N2", cultureInfo), stockInfoFont));
                    cellItemContent.PaddingRight = cellItemContent.PaddingBottom = 5;
                    cellItemContent.HorizontalAlignment = Element.ALIGN_RIGHT;
                    cellItemContent.PaddingTop = 3;
                    cellItemContent.BorderColor = new BaseColor(255, 255, 255);
                    cellItemContent.BackgroundColor = new BaseColor(230, 231, 231);
                    tableItemContent.AddCell(cellItemContent);
                    cellItemContent = new PdfPCell(new Phrase("Transaction Fee", stockInfoFont));
                    cellItemContent.PaddingLeft = cellItemContent.PaddingBottom = 5;
                    cellItemContent.PaddingTop = 3;
                    cellItemContent.BorderColor = new BaseColor(255, 255, 255);
                    cellItemContent.BackgroundColor = new BaseColor(238, 239, 239);
                    tableItemContent.AddCell(cellItemContent);
                    cellItemContent = new PdfPCell(new Phrase(i.TransactionFeeValue.ToString("N2", cultureInfo), stockInfoFont));
                    cellItemContent.PaddingRight = cellItemContent.PaddingBottom = 5;
                    cellItemContent.HorizontalAlignment = Element.ALIGN_RIGHT;
                    cellItemContent.PaddingTop = 3;
                    cellItemContent.BorderColor = new BaseColor(255, 255, 255);
                    cellItemContent.BackgroundColor = new BaseColor(238, 239, 239);
                    tableItemContent.AddCell(cellItemContent);
                    cellItemContent = new PdfPCell(new Phrase("*Net Amount", stockInfoFont));
                    cellItemContent.PaddingLeft = cellItemContent.PaddingBottom = 5;
                    cellItemContent.PaddingTop = 3;
                    cellItemContent.BorderColor = new BaseColor(255, 255, 255);
                    cellItemContent.BackgroundColor = new BaseColor(230, 231, 231);
                    tableItemContent.AddCell(cellItemContent);
                    cellItemContent = new PdfPCell(new Phrase(i.NetAmmountValue.ToString("N2", cultureInfo), stockInfoFont));
                    cellItemContent.PaddingRight = cellItemContent.PaddingBottom = 5;
                    cellItemContent.HorizontalAlignment = Element.ALIGN_RIGHT;
                    cellItemContent.PaddingTop = 3;
                    cellItemContent.BorderColor = new BaseColor(255, 255, 255);
                    cellItemContent.BackgroundColor = new BaseColor(230, 231, 231);
                    tableItemContent.AddCell(cellItemContent);
                    cellItemContent = new PdfPCell(new Phrase("Custodian", stockInfoFont));
                    cellItemContent.PaddingLeft = cellItemContent.PaddingBottom = 5;
                    cellItemContent.PaddingTop = 3;
                    cellItemContent.BorderColor = new BaseColor(255, 255, 255);
                    cellItemContent.BackgroundColor = new BaseColor(238, 239, 239);
                    tableItemContent.AddCell(cellItemContent);
                    cellItemContent = new PdfPCell(new Phrase(i.CustodyCode.ToString("N0", cultureInfo), stockInfoFont));
                    cellItemContent.PaddingLeft = cellItemContent.PaddingBottom = 5;
                    cellItemContent.PaddingTop = 3;
                    cellItemContent.BorderColor = new BaseColor(255, 255, 255);
                    cellItemContent.BackgroundColor = new BaseColor(238, 239, 239);
                    tableItemContent.AddCell(cellItemContent);

                    cellItem.AddElement(tableItemContent);

                    if ((counter % 2) > 0)
                    {
                        Image blueLine = Image.GetInstance(System.Web.HttpContext.Current.Server.MapPath("~/Content/images/dotted-blueline.png"));
                        PdfPCell lineCell = new PdfPCell(blueLine);
                        lineCell.Border = 0;
                        lineCell.PaddingTop = 8;
                        lineCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        lineCell.BorderColor = new BaseColor(255, 255, 255);
                        tableItems.AddCell(lineCell);
                    }

                    tableItems.AddCell(cellItem);

                    if (counter > 0 && ((counter % 2) > 0))
                    {
                        cellItem = new PdfPCell();
                        cellItem.Colspan = 3;
                        cellItem.Border = 0;
                        cellItem.BorderColor = new BaseColor(255, 255, 255);
                        cellItem.PaddingTop = 15;
                        cellItem.AddElement(Chunk.NEWLINE);
                        tableItems.AddCell(cellItem);
                    }

                    counter++;
                }

                if (items.Count % 2 > 0)
                {
                    PdfPCell lineCell = new PdfPCell();
                    lineCell.AddElement(new Phrase(""));
                    lineCell.BorderColor = new BaseColor(255, 255, 255);
                    tableItems.AddCell(lineCell);

                    PdfPCell emptyCell = new PdfPCell();
                    emptyCell.Border = 0;
                    lineCell.BorderColor = new BaseColor(255, 255, 255);
                    tableItems.AddCell(emptyCell);
                }

                doc.Add(tableItems);

            }

            #endregion

            //Add warning
            Font warningFont = FontFactory.GetFont("Arial", 11, Font.NORMAL, new BaseColor(111, 111, 111));
            Paragraph warning = new Paragraph(@"THIS CONFIRMATION IS AN ADVICE NOT AN INVOICE. REMMITANCE OR SECURITIES ARE DUE ON OR BEFORE SETTLEMENT DATE.", warningFont);
            warning.Alignment = Element.ALIGN_CENTER;
            warning.SpacingBefore = 35;
            doc.Add(warning);
            warning = new Paragraph("See Terms and Conditions and explanation of coded symbols on the last page of this Confirmation.", warningFont);
            warning.Alignment = Element.ALIGN_CENTER;
            warning.SpacingBefore = 12;
            doc.Add(warning);
            warning = new Paragraph(@"On other than Round Lots (normally 100 shares) if a differencial has been charged above an 000-Lot differencial
									in connection with any transaction, the amount of such differencial will be furnished upon request.", warningFont);
            warning.Alignment = Element.ALIGN_CENTER;
            warning.SpacingBefore = 12;
            doc.Add(warning);
            warning = new Paragraph(@"All transactions are cleared in Brazil by Votorantim CTVM Ltda., an affiliate of Banco Votorantim Securities, Inc.", warningFont);
            warning.Alignment = Element.ALIGN_CENTER;
            warning.SpacingBefore = 12;
            doc.Add(warning);

            AddReportFooter(ref doc, report.Footer, ref writer);

            doc.Close();

            return memStream;
        }

        public MemoryStream CreateTradeBlotterToMemoryStream(TradeBlotter report)
        {
            iTextSharp.text.Document doc = new iTextSharp.text.Document(new Rectangle(970, 1371).Rotate(), 25, 25, 25, 45);
            MemoryStream memStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
            writer.CloseStream = false;

            writer.PageEvent = new PrintHeaderFooter { Title = "TradeBlotter" };

            doc.Open();

            Font tradeFont = FontFactory.GetFont("Arial", 30, Font.BOLD, new BaseColor(0, 135, 169));
            Font companyAddressFont = FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(111, 111, 111));
            Font dateFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(111, 111, 111));
            Font lineFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(111, 111, 111));
            Font lineHeaderFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(255, 255, 255));

            DateTime date = DateTime.Now;
            if (report.Items != null && report.Items.Count > 0)
                date = report.Items[0].TradeDate;

            AddBvReportHeader(ref doc, "Trade Blotter", report.Header, string.Empty, string.Empty, string.Empty, date, null);

            #region Items

            List<TradeBlotterItem> items = new List<TradeBlotterItem>();
            items = report.Items;

            if (items.Count > 0)
            {
                PdfPTable tableItems = new PdfPTable(13);
                tableItems.WidthPercentage = 100;
                tableItems.HorizontalAlignment = Element.ALIGN_LEFT;
                var colWidthPercentages = new[] { 67f, 84f, 83f, 135f, 100f, 87f, 87f, 158f, 114f, 158f, 114f, 70f, 150f };
                tableItems.SetWidths(colWidthPercentages);
                tableItems.SplitLate = false;
                tableItems.SplitRows = true;

                string[] headerItems = new string[13] { "Trade ID", "Trade Data", "Settle Date", "Isin", "Stock", "# Of Shares", "Share Price", "Customer Buy Amount", "Net Buy Amount", "Customer Sell Amount", "Net Sell Amount", "Currency", "Client/Broker" };
                AddListHeader(ref tableItems, headerItems);

                bool even = true;
                foreach (TradeBlotterItem i in items)
                {
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.TradeId.ToString(), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.TradeDate.ToString("MM/dd/yyyy", cultureInfo), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.SettlementDate.ToString("MM/dd/yyyy", cultureInfo), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.ISINCode, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.CompanyName, even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.NumberOfShares.ToString("N0", cultureInfo), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.SharePrice.ToString("N2", cultureInfo), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.CustomerBuyAmount.ToString("N2", cultureInfo), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.NetBuyAmount.ToString("N2", cultureInfo), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.CustomerSellAmount.ToString("N2", cultureInfo), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.NetSellAmount.ToString("N2", cultureInfo), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.CurrencyName, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.ClientName, even);
                    even = !even;
                }
                doc.Add(tableItems);
            }

            #endregion

            doc.Close();

            return memStream;
        }

        public MemoryStream CreateCustomerStatementToMemoryStream(CustomerStatement report)
        {
            iTextSharp.text.Document doc = new iTextSharp.text.Document(new Rectangle(970, 1371).Rotate(), 25, 25, 25, 45);
            MemoryStream memStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
            writer.CloseStream = false;

            DisclaimerPage = false;

            writer.PageEvent = new PrintHeaderFooter { Title = "CustomerStatement" };

            doc.Open();

            Font lineFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(111, 111, 111));
            Font lineHeaderFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(255, 255, 255));
            Font lineFooterFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(255, 255, 255));
            Font disclaimerHeaderFont = FontFactory.GetFont("Arial", 11, Font.BOLD, new BaseColor(111, 111, 111));

            string clientAddress = string.Format("{0}\nPhone: {1}\nFAX: {2}\nEmail: {3}", report.Address, report.PhoneNumber, report.FaxNumber, report.Email);
            string clientAccount = string.Format("Account Number: BV-{0}  Sales Rap: {1}", report.ClientCode, report.ClientCode);

            AddBvReportHeader(ref doc, "Customer Statement", report.Header, report.ClientName, clientAddress, clientAccount, report.BeginDate, report.EndDate);

            #region Items

            List<CustomerStatementItem> items = new List<CustomerStatementItem>();
            items = report.Items;

            if (items.Count > 0)
            {
                string currency = "BRL";

                PdfPTable tableItems = new PdfPTable(14);
                tableItems.WidthPercentage = 100;
                var colWidthPercentages = new[] { 98f, 78f, 78f, 62f, 127f, 69f, 58f, 127f, 86f, 128f, 69f, 131f, 128f, 69f };
                tableItems.HorizontalAlignment = Element.ALIGN_LEFT;
                tableItems.SpacingBefore = 15;
                tableItems.SetWidths(colWidthPercentages);

                string[] headerItems = new string[14] { "Security", "Trade Data", "Settle Date", "Buy/Sell", "Share Price", "Currency", "Shares", "Net Value", "Date Settled", "Settled Value", "Currency", "Outstanding Shares", "Outstanding Values", "Currency" };
                AddListHeader(ref tableItems, headerItems);

                bool even = true;

                foreach (CustomerStatementItem i in items)
                {
                    currency = i.CurrencyName;
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.CompanyName, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.TradeDate.ToString("MM/dd/yyyy", cultureInfo), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.SettlementDate.ToString("MM/dd/yyyy", cultureInfo), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.OperationCode, even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.SharePrice.ToString("N2", cultureInfo), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.CurrencyName, even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.Shares.ToString("N0", cultureInfo), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.NetValue.ToString("N2", cultureInfo), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.SettlementDate.ToString("MM/dd/yyyy"), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.SettleValue.ToString("N2", cultureInfo), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.CurrencyName, even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.OutstandingShares.ToString("N2", cultureInfo), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.OutstandingValues.ToString("N2", cultureInfo), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.CurrencyName, even);
                    even = !even;
                }

                #region Footer

                AddListFooterItem(ref tableItems, Element.ALIGN_LEFT, string.Format("# of trades: {0}", items.Count));
                AddBlankCells(ref tableItems, 6, new BaseColor(208, 208, 208), 1);
                AddListFooterItem(ref tableItems, Element.ALIGN_RIGHT, items.Sum(i => i.NetValue).ToString("N2", cultureInfo));
                AddBlankCells(ref tableItems, 1, new BaseColor(208, 208, 208), 1);
                AddListFooterItem(ref tableItems, Element.ALIGN_RIGHT, items.Sum(i => i.SettleValue).ToString("N2", cultureInfo));
                AddListFooterItem(ref tableItems, Element.ALIGN_LEFT, currency);
                AddBlankCells(ref tableItems, 2, new BaseColor(208, 208, 208), 1);
                AddListFooterItem(ref tableItems, Element.ALIGN_LEFT, currency);

                #endregion

                doc.Add(tableItems);
            }

            #endregion

            #region Disclaimer

            if (report.Footer != null)
                AddReportFooter(ref doc, report.Footer, ref writer);

            #endregion

            doc.Close();

            return memStream;
        }

        public MemoryStream CreateCustomerLedgerToMemoryStream(CustomerLedger report)
        {
            iTextSharp.text.Document doc = new iTextSharp.text.Document(new Rectangle(970, 1371).Rotate(), 25, 25, 25, 45);
            MemoryStream memStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
            writer.CloseStream = false;

            writer.PageEvent = new PrintHeaderFooter { Title = "CustomerLedger" };

            doc.Open();

            Font lineFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(111, 111, 111));
            Font lineHeaderFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(255, 255, 255));
            Font lineFooterFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(255, 255, 255));
            Font disclaimerHeaderFont = FontFactory.GetFont("Arial", 11, Font.BOLD, new BaseColor(111, 111, 111));
            Font disclaimerFont = FontFactory.GetFont("Arial", 11, Font.NORMAL, new BaseColor(111, 111, 111));

            string clientAccount = string.Format("Cust ID: {0}\nCust Name: {1}", report.ClientCode, report.ClientName);

            AddReportHeader(ref doc, "Customer Ledger", report.Header, report.ClientName, string.Empty, clientAccount, report.StartDate, report.EndDate);

            #region Items

            List<CustomerLedgerItem> items = new List<CustomerLedgerItem>();
            items = report.Items;

            if (items.Count > 0)
            {
                string currency = "BRL";

                PdfPTable tableItems = new PdfPTable(13);
                tableItems.WidthPercentage = 100;
                var colWidthPercentages = new[] { 72f, 232f, 78f, 78f, 61f, 85f, 65f, 117f, 66f, 107f, 107f, 137f, 137f };
                tableItems.HorizontalAlignment = Element.ALIGN_LEFT;
                tableItems.SpacingBefore = 15;
                tableItems.SetWidths(colWidthPercentages);

                string[] headerItems = new string[13] { "ConfirmID", "Security", "Trade Date", "Settle Date", "Buy/Sell", "Share Price", "Shares", "Net Value", "Currency", "Settled Shares", "Settled Value", "Outstanding Shares", "Outstanding Values" };
                AddListHeader(ref tableItems, headerItems);

                bool even = true;

                foreach (CustomerLedgerItem i in items)
                {
                    currency = i.CurrencyName;
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.ConfirmID.ToString(), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.Security, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.TradeDate.ToString("MM/dd/yyyy", cultureInfo), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.SettleDate.ToString("MM/dd/yyyy", cultureInfo), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.OperationCode, even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.SharePrice.ToString("N2", cultureInfo), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.Shares.ToString("N0", cultureInfo), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.NetValue.ToString("N2", cultureInfo), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.CurrencyName, even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.SettledShares.ToString("N0", cultureInfo), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.SettledValue.ToString("N2", cultureInfo), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.OutstandingShares.ToString("N2", cultureInfo), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.OutstandingValues.ToString("N2", cultureInfo), even);
                    even = !even;
                }

                doc.Add(tableItems);
            }

            #endregion

            doc.Close();

            return memStream;
        }

        public MemoryStream CreateStockRecordToMemoryStream(StockRecord report)
        {
            iTextSharp.text.Document doc = new iTextSharp.text.Document(new Rectangle(970, 1371), 25, 25, 25, 45);
            MemoryStream memStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
            writer.CloseStream = false;

            writer.PageEvent = new PrintHeaderFooter { Title = "StockRecord" };

            doc.Open();

            Font lineFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(111, 111, 111));
            Font lineStockFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(111, 111, 111));
            Font lineHeaderFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(255, 255, 255));
            Font lineFooterFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(255, 255, 255));
            Font disclaimerHeaderFont = FontFactory.GetFont("Arial", 11, Font.BOLD, new BaseColor(111, 111, 111));
            Font disclaimerFont = FontFactory.GetFont("Arial", 11, Font.NORMAL, new BaseColor(111, 111, 111));

            AddBvReportHeader(ref doc, "Daily Stock Record", report.Header, string.Empty, string.Empty, string.Empty, report.StartDate, report.EndDate);

            #region Items

            List<StockRecordItem> items = new List<StockRecordItem>();
            items = report.Items;

            if (items.Count > 0)
            {
                foreach (StockRecordItem item in items)
                {
                    Paragraph stockName = new Paragraph(item.CompanyName + "\n", lineStockFont);
                    stockName.SpacingBefore = 20;
                    //stockName.SpacingAfter = 15;
                    doc.Add(stockName);

                    Image grayLine = Image.GetInstance(System.Web.HttpContext.Current.Server.MapPath("~/Content/images/dotted-line3.png"));
                    grayLine.Alignment = Image.ALIGN_CENTER;
                    grayLine.WidthPercentage = 100;
                    grayLine.Alignment = Element.ALIGN_LEFT;
                    doc.Add(grayLine);

                    PdfPTable tableSubItems = new PdfPTable(5);
                    tableSubItems.WidthPercentage = 100;
                    tableSubItems.SpacingBefore = 10;
                    var colWidthPercentages = new[] { 85f, 550f, 85f, 100f, 100f };
                    tableSubItems.HorizontalAlignment = Element.ALIGN_LEFT;
                    tableSubItems.SetWidths(colWidthPercentages);

                    string[] headerItems = new string[5] { "ConfirmID", "Client/Broker", "Date Settled", "Long", "Short" };
                    AddListHeader(ref tableSubItems, headerItems);

                    bool even = true;

                    foreach (StockRecordSubItem si in item.SubItems)
                    {
                        AddListItem(ref tableSubItems, Element.ALIGN_LEFT, si.ConfirmID.ToString(), even);
                        AddListItem(ref tableSubItems, Element.ALIGN_LEFT, si.ClientBroker, even);
                        AddListItem(ref tableSubItems, Element.ALIGN_LEFT, si.DateSettled.ToString("MM/dd/yyyy", cultureInfo), even);
                        AddListItem(ref tableSubItems, Element.ALIGN_RIGHT, si.Long.ToString("N2", cultureInfo), even);
                        AddListItem(ref tableSubItems, Element.ALIGN_RIGHT, si.Short.ToString("N2", cultureInfo), even);
                        even = !even;
                    }
                    AddBlankCells(ref tableSubItems, 3, new BaseColor(208, 208, 208), 1);
                    AddListFooterItem(ref tableSubItems, Element.ALIGN_RIGHT, item.SubItems.Select(s => s.Long).Sum().ToString("N2", cultureInfo));
                    AddListFooterItem(ref tableSubItems, Element.ALIGN_RIGHT, item.SubItems.Select(s => s.Short).Sum().ToString("N2", cultureInfo));

                    doc.Add(tableSubItems);
                }
            }

            #endregion

            doc.Close();

            return memStream;
        }

        public MemoryStream CreateFailLedgerToMemoryStream(FailLedger report)
        {
            iTextSharp.text.Document doc = new iTextSharp.text.Document(new Rectangle(970, 1371), 25, 25, 25, 45);
            MemoryStream memStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
            writer.CloseStream = false;

            writer.PageEvent = new PrintHeaderFooter { Title = "FailLedger" };

            doc.Open();

            Font lineFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(111, 111, 111));
            Font lineHeaderFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(255, 255, 255));
            Font lineFooterFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(255, 255, 255));
            Font disclaimerHeaderFont = FontFactory.GetFont("Arial", 11, Font.BOLD, new BaseColor(111, 111, 111));
            Font disclaimerFont = FontFactory.GetFont("Arial", 11, Font.NORMAL, new BaseColor(111, 111, 111));
            Font dateFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(111, 111, 111));

            string clientAccount = string.Empty;//string.Format("Cust ID: {0}\nCust Name: {1}", report.ClientCode, report.ClientName);

            string reportType = string.Empty;
            switch (report.Type)
            {
                case FailLedgerType.FailToDeliver:
                    reportType = "Fail to Deliver";
                    break;
                case FailLedgerType.FailToReceive:
                    reportType = "Fail to Receive";
                    break;
                case FailLedgerType.APCustomer:
                    reportType = "A/P Customer";
                    break;
                case FailLedgerType.ARCustomer:
                    reportType = "AR Customer";
                    break;
            }

            DateTime date = DateTime.Now;
            if (report.Items != null && report.Items.Count > 0)
                date = report.Items[0].TradeDate;

            AddReportHeader(ref doc, string.Concat("Fail Ledger - ", reportType), report.Header, string.Empty, string.Empty, clientAccount, date, null);

            #region Items

            List<Customer> users = new List<Customer>();
            users = (from l in report.Items group l by new { l.ClientCode, l.ClientName } into g select new Customer { Code = g.Key.ClientCode, Name = g.Key.ClientName }).ToList();

            if (users.Count > 0)
            {
                Font accountOfFont = FontFactory.GetFont("Arial", 11, Font.BOLD, new BaseColor(111, 111, 111));
                Font accountOfValueFont = FontFactory.GetFont("Arial", 11, Font.NORMAL, new BaseColor(111, 111, 111));

                foreach (Customer c in users)
                {
                    Paragraph accParagraph1 = new Paragraph("For the Account of:", accountOfFont);
                    Paragraph accParagraph2 = new Paragraph();
                    Phrase codeLabel = new Phrase("Client ID: ", accountOfFont);
                    accParagraph2.Add(codeLabel);
                    Phrase codeValue = new Phrase(c.Code.ToString(), accountOfValueFont);
                    accParagraph2.Add(codeValue);
                    accParagraph2.Add("  ");
                    Phrase nameLabel = new Phrase("Name: ", accountOfFont);
                    accParagraph2.Add(nameLabel);
                    Phrase nameValue = new Phrase(c.Name, accountOfValueFont);
                    accParagraph2.Add(nameValue);
                    accParagraph2.ExtraParagraphSpace = 0;
                    doc.Add(accParagraph1);
                    doc.Add(accParagraph2);

                    List<FailLedgerItem> items = new List<FailLedgerItem>();
                    items = (from ci in report.Items where ci.ClientCode.Equals(c.Code) select ci).ToList();

                    if (items.Count > 0)
                    {
                        PdfPTable tableItems = new PdfPTable(9);
                        tableItems.WidthPercentage = 100;
                        var colWidthPercentages = new[] { 75f, 50f, 130f, 80f, 80f, 80f, 90f, 140f, 110f };
                        tableItems.HorizontalAlignment = Element.ALIGN_LEFT;
                        tableItems.SpacingBefore = 10;
                        tableItems.SetWidths(colWidthPercentages);

                        string[] headerItems = new string[9] { "Confirm ID", "B/S", "Security", "Trade Date", "Settle Date", "Shares", "Net Amount", "Market Value Per Share", "Market Value Total" };
                        AddListHeader(ref tableItems, headerItems);

                        bool even = true;

                        foreach (FailLedgerItem i in items)
                        {
                            AddListItem(ref tableItems, Element.ALIGN_LEFT, i.ConfirmID.ToString(), even);
                            AddListItem(ref tableItems, Element.ALIGN_LEFT, i.OperationCode, even);
                            AddListItem(ref tableItems, Element.ALIGN_LEFT, i.Security, even);
                            AddListItem(ref tableItems, Element.ALIGN_LEFT, i.TradeDate.ToString("MM/dd/yyyy", cultureInfo), even);
                            AddListItem(ref tableItems, Element.ALIGN_LEFT, i.SettleDate.ToString("MM/dd/yyyy", cultureInfo), even);
                            AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.Shares.ToString("N0", cultureInfo), even);
                            AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.NetAmount.ToString("N2", cultureInfo), even);
                            AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.MarketValuePerShare.ToString("N2", cultureInfo), even);
                            AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.MarketValueTotal.ToString("N2", cultureInfo), even);
                            even = !even;
                        }

                        #region Footer

                        AddBlankCells(ref tableItems, 8, new BaseColor(255, 255, 255), 0);
                        AddListFooterItem(ref tableItems, Element.ALIGN_RIGHT, string.Format("Record Count: {0}", items.Count.ToString("N0", cultureInfo)), FontFactory.GetFont("Arial", 12, Font.BOLDITALIC, new BaseColor(89, 89, 89)), new BaseColor(255, 255, 255));


                        #endregion


                        doc.Add(tableItems);
                    }
                }
            }

            #endregion

            doc.Close();

            return memStream;
        }

        public MemoryStream CreateBrokerageToMemoryStream(List<BrokerageTransfer> transfers, string startDate, string endDate, ReportHeader header)
        {
            MemoryStream memStream = new MemoryStream();
            bool pending = true;

            if (transfers.Count > 0)
            {
                string reportType = string.Empty;
                DateTime sDate = DateTime.Parse(startDate);
                DateTime? eDate = null;

                switch (transfers[0].Status)
                {
                    case BrokerageTransferStatus.Pending:
                        reportType = "Repasses Pendentes";
                        break;
                    case BrokerageTransferStatus.Settled:
                        reportType = "Repasses Liquidados";
                        eDate = DateTime.Parse(endDate);
                        pending = !pending;
                        break;
                }

                iTextSharp.text.Document doc = new iTextSharp.text.Document(new Rectangle(970, 1371).Rotate(), 25, 25, 25, 45);
                PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
                writer.CloseStream = false;

                writer.PageEvent = new PrintHeaderFooter { Title = "Brokerage", MinItems = pending };

                doc.Open();

                Font lineFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(111, 111, 111));
                Font lineHeaderFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(255, 255, 255));
                Font lineFooterFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(255, 255, 255));
                Font disclaimerHeaderFont = FontFactory.GetFont("Arial", 11, Font.BOLD, new BaseColor(111, 111, 111));
                Font disclaimerFont = FontFactory.GetFont("Arial", 11, Font.NORMAL, new BaseColor(111, 111, 111));
                Font dateFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(111, 111, 111));

                AddReportHeader(ref doc, reportType, header, string.Empty, string.Empty, string.Empty, sDate, eDate, true, true);

                #region Items

                PdfPTable tableItems = pending ? new PdfPTable(8) : new PdfPTable(10);
                tableItems.WidthPercentage = 100;
                var colWidthPercentages = pending ? new[] { 85f, 85f, 250f, 85f, 120f, 120f, 120f, 120f } : new[] { 75f, 95f, 200f, 100f, 115f, 115f, 115f, 115f, 115f, 120f };
                tableItems.HorizontalAlignment = Element.ALIGN_LEFT;
                tableItems.SpacingBefore = 10;
                tableItems.SetWidths(colWidthPercentages);

                string[] headerItems = pending ? new string[8] { "Código BV", "Código Sinacor", "Nome do Cliente", "Data Corretagem", "Corretagem Total", "% Repasse", "Valor Repasse R$", "Valor Repasse $" } : new string[10] { "Código BV", "Código Sinacor", "Nome do Cliente", "Data Corretagem", "Corretagem Total", "% Repasse", "Valor Repasse R$", "Valor Repasse $", "Data de Liquidação", "Login" };
                AddListHeader(ref tableItems, headerItems);

                bool even = true;

                foreach (BrokerageTransfer i in transfers)
                {
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.ClientBrokerCode.ToString(), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.ClientCode.ToString(), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.ClientName, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.BrokerageDate.ToString("dd/MM/yyyy"), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.BrokerageValue.ToString("N2"), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.TransferPercentage.ToString("N3"), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.TransferValue.ToString("N2"), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.TransferValueToDolar.ToString("N2"), even);
                    if (!pending)
                    {
                        AddListItem(ref tableItems, Element.ALIGN_LEFT, i.SettlementDate.Value.ToString("dd/MM/yyyy"), even);
                        AddListItem(ref tableItems, Element.ALIGN_LEFT, i.UserName, even);
                    }
                    even = !even;
                }

                #region Footer

                AddBlankCells(ref tableItems, 4, new BaseColor(255, 255, 255), 0);
                AddListFooterItem(ref tableItems, Element.ALIGN_RIGHT, transfers.Select(t => t.BrokerageValue).Sum().ToString("N2"), FontFactory.GetFont("Arial", 12, Font.BOLDITALIC, new BaseColor(89, 89, 89)), new BaseColor(255, 255, 255));
                AddListFooterItem(ref tableItems, Element.ALIGN_RIGHT, transfers.Select(t => t.TransferPercentage).Sum().ToString("N3"), FontFactory.GetFont("Arial", 12, Font.BOLDITALIC, new BaseColor(89, 89, 89)), new BaseColor(255, 255, 255));
                AddListFooterItem(ref tableItems, Element.ALIGN_RIGHT, transfers.Select(t => t.TransferValue).Sum().ToString("N2"), FontFactory.GetFont("Arial", 12, Font.BOLDITALIC, new BaseColor(89, 89, 89)), new BaseColor(255, 255, 255));
                AddListFooterItem(ref tableItems, Element.ALIGN_RIGHT, transfers.Select(t => t.TransferValueToDolar).Sum().ToString("N2"), FontFactory.GetFont("Arial", 12, Font.BOLDITALIC, new BaseColor(89, 89, 89)), new BaseColor(255, 255, 255));
                if (!pending)
                    AddBlankCells(ref tableItems, 2, new BaseColor(255, 255, 255), 0);

                #endregion

                doc.Add(tableItems);

                #endregion

                doc.Close();
            }
            return memStream;
        }

        #endregion

        #region Access Control

        public MemoryStream CreateGroupOfAssessorsToMemoryStream(List<GroupOfAssessorsListItem> groups)
        {
            iTextSharp.text.Document doc = new iTextSharp.text.Document(new Rectangle(970, 1371), 25, 25, 25, 45);
            MemoryStream memStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
            writer.CloseStream = false;

            writer.PageEvent = new PrintHeaderFooter { Title = "GroupsOfAssessors" };

            doc.Open();

            Font tradeFont = FontFactory.GetFont("Arial", 30, Font.BOLD, new BaseColor(0, 135, 169));
            Font companyAddressFont = FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(111, 111, 111));
            Font dateFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(111, 111, 111));
            Font lineFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(111, 111, 111));
            Font lineHeaderFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(255, 255, 255));

            AddReportHeader(ref doc, "Grupos de Assessores", null, string.Empty, string.Empty, string.Empty, null, null);

            if (groups.Count > 0)
            {
                PdfPTable tableItems = new PdfPTable(3);
                tableItems.WidthPercentage = 70;
                tableItems.HorizontalAlignment = Element.ALIGN_LEFT;
                var colWidthPercentages = new[] { 400f, 400f, 400f };
                tableItems.SetWidths(colWidthPercentages);
                tableItems.SplitLate = false;
                tableItems.SplitRows = true;
                tableItems.SpacingBefore = 25;

                string[] headerItems = new string[3] { "Grupos", "Assessores", "Usuários" };
                AddListHeader(ref tableItems, headerItems);

                bool even = true;
                foreach (GroupOfAssessorsListItem i in groups)
                {
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.Name, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.Assessors, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.UsersCount.ToString("N0"), even);
                    even = !even;
                }
                doc.Add(tableItems);
            }

            doc.Close();

            return memStream;
        }

        public MemoryStream CreateHistoryToMemoryStream(List<HistoryData> data, string targetTitle, string targetColumnTitle, bool hasAction, string extraTargetColumnTitle = null)
        {
            iTextSharp.text.Document doc = new iTextSharp.text.Document(new Rectangle(1371, 970), 25, 25, 25, 45);
            MemoryStream memStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
            writer.CloseStream = false;

            if (extraTargetColumnTitle != null)
            {
                writer.PageEvent = new PrintHeaderFooter { Title = "TermHistory", MinItems = !hasAction, ExtraTitle = HttpUtility.HtmlDecode(targetColumnTitle), ExtraExtraTitle = HttpUtility.HtmlDecode(extraTargetColumnTitle) };
            }
            else if (targetColumnTitle == null)
            {
                writer.PageEvent = new PrintHeaderFooter { Title = "History" };
            }
            else
            {
                writer.PageEvent = new PrintHeaderFooter { Title = "History", MinItems = !hasAction, ExtraTitle = HttpUtility.HtmlDecode(targetColumnTitle) };
            }

            doc.Open();

            Font tradeFont = FontFactory.GetFont("Arial", 30, Font.BOLD, new BaseColor(0, 135, 169));
            Font companyAddressFont = FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(111, 111, 111));
            Font dateFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(111, 111, 111));
            Font lineFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(111, 111, 111));
            Font lineHeaderFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(255, 255, 255));

            AddReportHeader(ref doc, HttpUtility.HtmlDecode(targetTitle), null, string.Empty, string.Empty, string.Empty, null, null);

            if (data.Count > 0)
            {
                string[] headerItems;
                PdfPTable tableItems;
                Single[] colWidthPercentages;

                if (extraTargetColumnTitle != null)
                {
                    tableItems = new PdfPTable(5);
                    tableItems.WidthPercentage = 100;
                    colWidthPercentages = new[] { 220f, 238f, 135f, 238f, 250f };
                    headerItems = new string[5] { "Data e Hora", "Responsável", "Ação", HttpUtility.HtmlDecode(targetColumnTitle), HttpUtility.HtmlDecode(extraTargetColumnTitle) };
                }
                else if (hasAction && targetColumnTitle != null && extraTargetColumnTitle == null)
                {
                    tableItems = new PdfPTable(4);
                    tableItems.WidthPercentage = 100;
                    colWidthPercentages = new[] { 125f, 238f, 135f, 238f };
                    headerItems = new string[4] { "Data e Hora", "Responsável", "Ação", HttpUtility.HtmlDecode(targetColumnTitle) };
                }
                else if (hasAction && targetColumnTitle == null)
                {
                    tableItems = new PdfPTable(3);
                    tableItems.WidthPercentage = 100;
                    colWidthPercentages = new[] { 125f, 292f, 132f };
                    headerItems = new string[3] { "Data e Hora", "Responsável", "Ação" };
                }
                else
                {
                    tableItems = new PdfPTable(3);
                    tableItems.WidthPercentage = 100;
                    colWidthPercentages = new[] { 125f, 292f, 132f };
                    headerItems = new string[3] { "Data e Hora", "Responsável", HttpUtility.HtmlDecode(targetColumnTitle) };
                }

                tableItems.HorizontalAlignment = Element.ALIGN_LEFT;
                tableItems.SetWidths(colWidthPercentages);
                tableItems.SplitLate = false;
                tableItems.SplitRows = true;
                tableItems.SpacingBefore = 35;

                AddListHeader(ref tableItems, headerItems);

                bool even = true;
                foreach (HistoryData i in data)
                {
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.Date.ToString("dd/MM/yyyy - HH:mm"), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.Responsible, even);
                    if (hasAction)
                        AddListItem(ref tableItems, Element.ALIGN_LEFT, i.Action, even);
                    if (targetColumnTitle != null)
                        AddListItem(ref tableItems, Element.ALIGN_LEFT, i.Target, even);
                    if (extraTargetColumnTitle != null)
                        AddListItem(ref tableItems, Element.ALIGN_LEFT, i.ExtraTarget, even);
                    even = !even;
                }
                doc.Add(tableItems);
            }

            doc.Close();

            return memStream;
        }

        //public MemoryStream CreateFundHistoryToMemoryStream(List<HistoryData> data, string targetTitle, string targetColumnTitle, bool hasAction, string extraTargetColumnTitle)
        //{
        //    List<QX3.Portal.Services.SagazWebService.Fund> funds = new List<Services.SagazWebService.Fund>();
        //    using (var service = QX3.Portal.WebSite.Models.Channel.ChannelProviderFactory<QX3.Portal.WebSite.FundService.IFundContractChannel>.Create("FundClassName").GetChannel().CreateChannel())
        //    {
        //        funds = service.FN_LoadFunds().Result;
        //    }

        //    Document doc = new Document(new Rectangle(1371, 970), 25, 25, 25, 45);
        //    MemoryStream memStream = new MemoryStream();
        //    PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
        //    writer.CloseStream = false;

        //    writer.PageEvent = new PrintHeaderFooter { Title = "Funds.History", MinItems = !hasAction, ExtraTitle = HttpUtility.HtmlDecode(targetColumnTitle), ExtraExtraTitle = HttpUtility.HtmlDecode(extraTargetColumnTitle) };

        //    doc.Open();

        //    Font tradeFont = FontFactory.GetFont("Arial", 30, Font.BOLD, new BaseColor(0, 135, 169));
        //    Font companyAddressFont = FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(111, 111, 111));
        //    Font dateFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(111, 111, 111));
        //    Font lineFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(111, 111, 111));
        //    Font lineHeaderFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(255, 255, 255));

        //    AddReportHeader(ref doc, HttpUtility.HtmlDecode(targetTitle), null, string.Empty, string.Empty, string.Empty, null, null);

        //    if (data.Count > 0)
        //    {
        //        string[] headerItems;
        //        PdfPTable tableItems;
        //        Single[] colWidthPercentages;

        //        tableItems = new PdfPTable(6);
        //        tableItems.WidthPercentage = 100;
        //        colWidthPercentages = new[] { 220f, 238f, 135f, 250f, 238f, 250f };
        //        headerItems = new string[6] { "Data e Hora", "Responsável", "Ação", "Cliente", HttpUtility.HtmlDecode(targetColumnTitle), HttpUtility.HtmlDecode(extraTargetColumnTitle) };


        //        tableItems.HorizontalAlignment = Element.ALIGN_LEFT;
        //        tableItems.SetWidths(colWidthPercentages);
        //        tableItems.SplitLate = false;
        //        tableItems.SplitRows = true;
        //        tableItems.SpacingBefore = 35;

        //        AddListHeader(ref tableItems, headerItems);

        //        bool even = true;
        //        foreach (HistoryData i in data)
        //        {
        //            AddListItem(ref tableItems, Element.ALIGN_LEFT, i.Date.ToString("dd/MM/yyyy - HH:mm"), even);
        //            AddListItem(ref tableItems, Element.ALIGN_LEFT, i.Responsible, even);
        //            AddListItem(ref tableItems, Element.ALIGN_LEFT, i.Action, even);
        //            AddListItem(ref tableItems, Element.ALIGN_LEFT, CommonHelper.GetClientName(i.ID), even);
        //            AddListItem(ref tableItems, Element.ALIGN_LEFT, funds.Where(a => a.Code == Convert.ToInt32(i.Target)).FirstOrDefault() == null
        //                            ? "-"
        //                            : funds.Where(a => a.Code == Convert.ToInt32(i.Target)).FirstOrDefault().Name, even);
        //            AddListItem(ref tableItems, Element.ALIGN_LEFT, i.ExtraTarget, even);

        //            even = !even;
        //        }
        //        doc.Add(tableItems);
        //    }

        //    doc.Close();

        //    return memStream;
        //}

        public MemoryStream CreateUsersToMemoryStream(List<UsersListItem> users)
        {
            iTextSharp.text.Document doc = new iTextSharp.text.Document(new Rectangle(970, 1371).Rotate(), 25, 25, 25, 45);
            MemoryStream memStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
            writer.CloseStream = false;

            writer.PageEvent = new PrintHeaderFooter { Title = "Users" };

            doc.Open();

            Font tradeFont = FontFactory.GetFont("Arial", 30, Font.BOLD, new BaseColor(0, 135, 169));
            Font companyAddressFont = FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(111, 111, 111));
            Font dateFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(111, 111, 111));
            Font lineFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(111, 111, 111));
            Font lineHeaderFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(255, 255, 255));

            AddReportHeader(ref doc, "Usuários", null, string.Empty, string.Empty, string.Empty, null, null);

            if (users.Count > 0)
            {
                PdfPTable tableItems = new PdfPTable(8);
                tableItems.WidthPercentage = 100;
                tableItems.HorizontalAlignment = Element.ALIGN_LEFT;
                var colWidthPercentages = new[] { 150f, 100f, 250f, 70f, 40f, 110f, 350f, 85f };
                tableItems.SetWidths(colWidthPercentages);
                tableItems.SplitLate = false;
                tableItems.SplitRows = true;
                tableItems.SpacingBefore = 25;

                string[] headerItems = new string[8] { "Usuário", "Login", "Email", "Cod. Ass.", "Tipo", "Perfil de Acesso", "Grupo de Assessores", "Assessores" };
                AddListHeader(ref tableItems, headerItems);

                bool even = true;
                foreach (UsersListItem i in users)
                {
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.Name, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.Login, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.Email, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.AssessorID, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.Type, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.ProfileName, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.AssessorGroup, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.AssessorInterval, even);
                    even = !even;
                }
                doc.Add(tableItems);
            }

            doc.Close();

            return memStream;
        }

        public MemoryStream CreateProfilesToMemoryStream(List<ProfilesListItem> profiles)
        {
            iTextSharp.text.Document doc = new iTextSharp.text.Document(new Rectangle(970, 1371), 25, 25, 25, 45);
            MemoryStream memStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
            writer.CloseStream = false;

            writer.PageEvent = new PrintHeaderFooter { Title = "Profiles" };

            doc.Open();

            Font tradeFont = FontFactory.GetFont("Arial", 30, Font.BOLD, new BaseColor(0, 135, 169));
            Font companyAddressFont = FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(111, 111, 111));
            Font dateFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(111, 111, 111));
            Font lineFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(111, 111, 111));
            Font lineHeaderFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(255, 255, 255));

            AddReportHeader(ref doc, "Perfis", null, string.Empty, string.Empty, string.Empty, null, null);

            if (profiles.Count > 0)
            {
                PdfPTable tableItems = new PdfPTable(2);
                tableItems.WidthPercentage = 70;
                tableItems.HorizontalAlignment = Element.ALIGN_LEFT;
                var colWidthPercentages = new[] { 400f, 400f };
                tableItems.SetWidths(colWidthPercentages);
                tableItems.SplitLate = false;
                tableItems.SplitRows = true;
                tableItems.SpacingBefore = 25;

                string[] headerItems = new string[2] { "Perfil", "Usuários" };
                AddListHeader(ref tableItems, headerItems);

                bool even = true;
                foreach (ProfilesListItem i in profiles)
                {
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.Name, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.UsersCount.ToString("N0"), even);
                    even = !even;
                }
                doc.Add(tableItems);
            }

            doc.Close();

            return memStream;
        }

        public MemoryStream CreateClientsRegisterToMemoryStream(List<FuncionalityClient> list)
        {
            iTextSharp.text.Document doc = new iTextSharp.text.Document(new Rectangle(970, 1371), 25, 25, 25, 45);
            MemoryStream memStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
            writer.CloseStream = false;

            writer.PageEvent = new PrintHeaderFooter { Title = "ClientsRegister" };

            doc.Open();

            Font tradeFont = FontFactory.GetFont("Arial", 30, Font.BOLD, new BaseColor(0, 135, 169));
            Font companyAddressFont = FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(111, 111, 111));
            Font dateFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(111, 111, 111));
            Font lineFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(111, 111, 111));
            Font lineHeaderFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(255, 255, 255));
            Font itemFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(89, 89, 89));

            AddReportHeader(ref doc, "Cadastro de Cliente por Funcionalidade", null, string.Empty, string.Empty, string.Empty);

            if (list.Count > 0)
            {
                PdfPTable tableItems = new PdfPTable(4);
                tableItems.WidthPercentage = 100;
                tableItems.SpacingAfter = 30;

                var colWidthPercentages = new[] { 6f, 8f, 10f, 10f };
                tableItems.SetWidths(colWidthPercentages);
                tableItems.SplitRows = false;

                string[] headerItems = new string[4] { "Código", "Nome", "E-mail", "Cópia para" };
                AddListHeader(ref tableItems, headerItems);

                bool even = true;
                foreach (var i in list)
                {
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, i.ClientCode.ToString(), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, i.ClientName, even);

                    PdfPCell cell = new PdfPCell();
                    cell.BorderWidthBottom = cell.BorderWidthLeft = cell.BorderWidthRight = cell.BorderWidthTop = 1;
                    cell.BorderColor = new BaseColor(255, 255, 255);
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell.PaddingLeft = cell.PaddingBottom = 7;
                    cell.PaddingTop = 0;
                    cell.BackgroundColor = even ? new BaseColor(238, 239, 239) : new BaseColor(230, 231, 231);
                    List<string> registeredEmails = new List<string>();
                    List<string> filteredList = i.Funcionalities.SelectMany(p => p.Emails.Select(t => t.Email)).ToList().Except(registeredEmails).ToList();
                    registeredEmails.AddRange(filteredList);
                    filteredList.ForEach(q => cell.AddElement(new Phrase(q, itemFont)));
                    tableItems.AddCell(cell);

                    PdfPCell cell1 = new PdfPCell();
                    cell1.BorderWidthBottom = cell1.BorderWidthLeft = cell1.BorderWidthRight = cell1.BorderWidthTop = 1;
                    cell1.BorderColor = new BaseColor(255, 255, 255);
                    cell1.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell1.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell1.PaddingLeft = cell1.PaddingBottom = 7;
                    cell1.PaddingTop = 0;
                    cell1.BackgroundColor = even ? new BaseColor(238, 239, 239) : new BaseColor(230, 231, 231);
                    List<string> registeredEmails1 = new List<string>();
                    List<string> filteredList1 = i.Funcionalities.SelectMany(p => p.CopyTo.Select(t => t.Email)).ToList().Except(registeredEmails1).ToList();
                    registeredEmails1.AddRange(filteredList1);
                    filteredList1.ForEach(q => cell1.AddElement(new Phrase(q, itemFont)));
                    tableItems.AddCell(cell1);

                    even = !even;
                }
                doc.Add(tableItems);
            }

            doc.Close();

            return memStream;
        }

        #endregion

        #region Other

        public MemoryStream CreateOrdersToMemoryStream(List<OrdersCorrection> orders)
        {
            iTextSharp.text.Document doc = new iTextSharp.text.Document(new Rectangle(970, 1371).Rotate(), 25, 25, 25, 45);
            MemoryStream memStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
            writer.CloseStream = false;

            writer.PageEvent = new PrintHeaderFooter { Title = "OrdersCorrection" };

            doc.Open();

            Font tradeFont = FontFactory.GetFont("Arial", 30, Font.BOLD, new BaseColor(0, 135, 169));
            Font companyAddressFont = FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(111, 111, 111));
            Font dateFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(111, 111, 111));
            Font lineFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(111, 111, 111));
            Font lineHeaderFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(255, 255, 255));

            AddReportHeader(ref doc, "Correções de Ordens", null, string.Empty, string.Empty, string.Empty, null, null);

            if (orders.Count > 0)
            {
                PdfPTable tableItems = new PdfPTable(7);
                tableItems.WidthPercentage = 100;
                tableItems.HorizontalAlignment = Element.ALIGN_LEFT;
                var colWidthPercentages = new[] { 130f, 312f, 312f, 312f, 85f, 60f, 110f };
                tableItems.SetWidths(colWidthPercentages);
                tableItems.SplitLate = false;
                tableItems.SplitRows = true;
                tableItems.SpacingBefore = 25;

                string[] headerItems = new string[7] { "Tipo", "Assessor", "Origem", "Destino", "Data Solic.", "Data", "Situação" };
                AddListHeader(ref tableItems, headerItems);

                bool even = false;
                foreach (OrdersCorrection i in orders)
                {
                    bool change = i.Type.Value == "6";
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.Type.Description, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.Responsible.Description, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, string.Format("{0} {1}", i.ClientFrom.Value, i.ClientFrom.Description), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, change ? string.Format("{0} {1}", i.ClientTo.Value, i.ClientTo.Description) : "Conta Erro", even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.Date.ToString("dd/MM/yyyy"), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.D1 ? "D-1" : "D0", even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.Status.Description, even);


                    PdfPCell cell = new PdfPCell();
                    cell.BorderWidthBottom = cell.BorderWidthLeft = cell.BorderWidthRight = cell.BorderWidthTop = 1;
                    cell.BorderColor = new BaseColor(255, 255, 255);
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.PaddingLeft = cell.PaddingBottom = 7;
                    cell.PaddingTop = 3;
                    cell.Colspan = 7;
                    cell.BackgroundColor = even ? new BaseColor(238, 239, 239) : new BaseColor(230, 231, 231);

                    PdfPTable tableSubItemsContainer = new PdfPTable(2);
                    colWidthPercentages = new[] { 35f, 65f };
                    tableSubItemsContainer.SetWidths(colWidthPercentages);
                    tableSubItemsContainer.SpacingBefore = 15;
                    tableSubItemsContainer.SpacingAfter = 15;
                    tableSubItemsContainer.WidthPercentage = 100;
                    PdfPCell cellSubItemTable = new PdfPCell();
                    cellSubItemTable.BorderColor = new BaseColor(0, 0, 0);
                    cellSubItemTable.Border = 0;
                    PdfPTable tableSubItems = new PdfPTable(3);
                    tableSubItems.WidthPercentage = 100;

                    tableSubItems.HorizontalAlignment = Element.ALIGN_LEFT;
                    colWidthPercentages = new[] { 80f, 80f, 80f };
                    tableSubItems.SetWidths(colWidthPercentages);
                    string[] headerSubItems = new string[3] { "Papel", "Quantidade", "Preço" };
                    AddListSubHeader(ref tableSubItems, headerSubItems);
                    foreach (OrdersCorrectionDetail d in i.Details)
                    {
                        AddListSubItem(ref tableSubItems, Element.ALIGN_LEFT, d.StockCode);
                        AddListSubItem(ref tableSubItems, Element.ALIGN_LEFT, d.Quantity.ToString("N0"));
                        AddListSubItem(ref tableSubItems, Element.ALIGN_RIGHT, d.Value.ToString("N4"));
                    }
                    cellSubItemTable.AddElement(tableSubItems);
                    cellSubItemTable.PaddingLeft = 15;
                    tableSubItemsContainer.AddCell(cellSubItemTable);

                    Paragraph description = new Paragraph();
                    description.Add(new Paragraph(i.Description.Description, dateFont));
                    description.Add(new Paragraph(i.Comments, lineFont));
                    cellSubItemTable = new PdfPCell(description);
                    cellSubItemTable.Border = 0;
                    cellSubItemTable.PaddingLeft = 15;
                    tableSubItemsContainer.AddCell(cellSubItemTable);

                    cell.AddElement(tableSubItemsContainer);
                    tableItems.AddCell(cell);
                }
                doc.Add(tableItems);
            }

            doc.Close();

            return memStream;
        }

        public MemoryStream CreateDiscountsToMemoryStream(List<BrokerageDiscount> discounts)
        {
            iTextSharp.text.Document doc = new iTextSharp.text.Document(new Rectangle(1371, 970), 25, 25, 25, 45);
            MemoryStream memStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
            writer.CloseStream = false;

            writer.PageEvent = new PrintHeaderFooter { Title = "BrokerageDiscunt" };

            doc.Open();

            Font tradeFont = FontFactory.GetFont("Arial", 30, Font.BOLD, new BaseColor(0, 135, 169));
            Font companyAddressFont = FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(111, 111, 111));
            Font dateFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(111, 111, 111));
            Font lineFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(111, 111, 111));
            Font lineHeaderFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(255, 255, 255));

            AddReportHeader(ref doc, "Desconto de Corretagem", null, string.Empty, string.Empty, string.Empty, null, null);

            if (discounts.Count > 0)
            {
                PdfPTable tableItems = new PdfPTable(12);
                tableItems.WidthPercentage = 100;
                var colWidthPercentages = new[] { 8f, 10f, 20f, 8f, 8f, 8f, 8f, 8f, 8f, 8f, 30f, 8f };
                tableItems.SetWidths(colWidthPercentages);
                tableItems.SplitRows = false;

                string[] headerItems = new string[12] { "Cod", "Cliente", "Assessor", "Bolsa", "Mercado", "Operação", "Vigência", "Ativo", "Quantidade", "Desconto", "Descrição", "Situação" };
                AddListHeader(ref tableItems, headerItems);

                bool even = true;
                foreach (BrokerageDiscount i in discounts)
                {
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.ClientCode.ToString() + "-" + i.DV.ToString(), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.ClientName, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.Assessor, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.StockExchange == 1 ? "Bovespa" : "BM&F", even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.MarketType, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.Operation, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.Vigency, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.StockCode, even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.Qtty.ToString(), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.StockDiscountPercentual.HasValue ? i.StockDiscountPercentual.Value.ToString() + "%" : i.StockDiscountValue.Value.ToString("n"), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.Description, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.Situation, even);
                    even = !even;
                }
                doc.Add(tableItems);
            }

            doc.Close();

            return memStream;
        }

        public MemoryStream CreateCustodyTransferToMemoryStream(List<CustodyTransfer> list)
        {
            iTextSharp.text.Document doc = new iTextSharp.text.Document(new Rectangle(1371, 970), 25, 25, 25, 45);
            MemoryStream memStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
            writer.CloseStream = false;

            writer.PageEvent = new PrintHeaderFooter { Title = "CustodyTransfer" };

            doc.Open();

            Font tradeFont = FontFactory.GetFont("Arial", 30, Font.BOLD, new BaseColor(0, 135, 169));
            Font companyAddressFont = FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(111, 111, 111));
            Font dateFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(111, 111, 111));
            Font lineFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(111, 111, 111));
            Font lineHeaderFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(255, 255, 255));

            AddReportHeader(ref doc, "Transferência de Custódia", null, string.Empty, string.Empty, string.Empty, null, null);

            if (list.Count > 0)
            {
                PdfPTable tableItems = new PdfPTable(12);
                tableItems.WidthPercentage = 100;
                var colWidthPercentages = new[] { 8f, 10f, 20f, 8f, 8f, 12f, 12f, 12f, 8f, 9f, 10f, 8f };
                tableItems.SetWidths(colWidthPercentages);
                tableItems.SplitRows = false;

                string[] headerItems = new string[12] { "Código", "Cliente", "Assessor", "Ativo", "Quantidade", "Corretora Origem", "Carteira Origem", "Corretora Destino", "Carteira Destino", "Status", "Data da Movimentação", "Observação" };
                AddListHeader(ref tableItems, headerItems);

                bool even = true;
                foreach (CustodyTransfer i in list)
                {
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.ClientCode + "-" + i.DV, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.ClientName, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.Assessor, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.StockCode, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.Qtty.ToString(), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.OrigBroker, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.OrigPortfolio.ToString(), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.DestinyBroker, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.DestinyPortfolio.ToString(), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.DescStatus, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.RequestDate.ToShortDateString(), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.Observations, even);

                    even = !even;
                }
                doc.Add(tableItems);
            }

            doc.Close();

            return memStream;
        }

        public MemoryStream CreateCustodyTransferReportToMemoryStream(CustodyTransferReport report, Byte[] reportChart)
        {
            iTextSharp.text.Document doc = new iTextSharp.text.Document(new Rectangle(970, 1371), 25, 25, 25, 45);
            MemoryStream memStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
            writer.CloseStream = false;

            writer.PageEvent = new PrintHeaderFooter();

            doc.Open();

            AddReportHeader(ref doc, "Relatório de Transferência de Custódia", null, string.Empty, string.Empty, string.Empty, null, null);

            Font fontTitle = FontFactory.GetFont("Arial", 14, Font.BOLD, new BaseColor(89, 89, 89));
            Font fontDefault = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(89, 89, 89));

            #region TÍTULO ORIGEM X DESTINO
            PdfPTable tableTitulo = new PdfPTable(2);
            tableTitulo.WidthPercentage = 100;
            tableTitulo.SpacingAfter = 20;

            PdfPCell cellTitulo = new PdfPCell();
            cellTitulo.BorderColor = new BaseColor(167, 174, 182);
            cellTitulo.BorderWidth = 1;
            cellTitulo.Padding = 15;

            cellTitulo.Phrase = new Phrase("Origem X Destino", fontTitle);
            cellTitulo.Border = PdfPCell.TOP_BORDER | PdfPCell.LEFT_BORDER | PdfPCell.BOTTOM_BORDER;
            cellTitulo.HorizontalAlignment = 0;
            tableTitulo.AddCell(cellTitulo);

            cellTitulo.Phrase = new Phrase("Período " + report.FromDate.ToShortDateString() + " - " + report.ToDate.ToShortDateString(), fontDefault);
            cellTitulo.Border = PdfPCell.TOP_BORDER | PdfPCell.RIGHT_BORDER | PdfPCell.BOTTOM_BORDER;
            cellTitulo.HorizontalAlignment = 2;
            tableTitulo.AddCell(cellTitulo);

            doc.Add(tableTitulo);
            #endregion

            #region GRÁFICO
            if (reportChart != null)
            {
                PdfPTable tableChart = new PdfPTable(1);
                tableChart.WidthPercentage = 100;
                tableChart.SpacingAfter = 20;

                PdfPCell cellChart = new PdfPCell();
                cellChart.BorderWidth = 0;
                cellChart.Padding = 0;

                Image imagemDoTopo = Image.GetInstance(reportChart);
                imagemDoTopo.Border = Rectangle.BOX;
                imagemDoTopo.BorderColor = new BaseColor(167, 174, 182);
                imagemDoTopo.BorderWidth = 1;

                cellChart.AddElement(imagemDoTopo);
                tableChart.AddCell(cellChart);
                doc.Add(tableChart);
            }
            #endregion

            #region TABELA DE TOPS
            PdfPTable tableTops = new PdfPTable(3);
            tableTops.WidthPercentage = 100;
            tableTops.SpacingAfter = 20;

            var colWidthPercentages = new[] { 50f, 3, 50f };
            tableTops.SetWidths(colWidthPercentages);

            PdfPCell cellTopsEspacamento = new PdfPCell();
            cellTopsEspacamento.BorderWidth = 0;
            cellTopsEspacamento.Phrase = new Phrase(" ");

            /* Top Origem */
            PdfPCell cellTops = new PdfPCell();
            cellTops.BorderColor = new BaseColor(167, 174, 182);
            cellTops.BorderWidth = 1;
            cellTops.Padding = 15;
            cellTops.AddElement(AddChartTopHeader("Top Origem", report.BrokerInTop.Sum(p => p.Amount), report.FromDate, report.ToDate));
            if (report.BrokerInTop.Count > 0)
            {
                cellTops.AddElement(AddChartTopList(report.BrokerInTop));
            }
            else
            {
                cellTops.AddElement(new Phrase("Não houve aportes durante o período selecionado", fontDefault));
            }
            tableTops.AddCell(cellTops);

            /* COLUNA DE ESPAÇAMENTO */
            tableTops.AddCell(cellTopsEspacamento);

            /* Top Destino */
            cellTops = new PdfPCell();
            cellTops.BorderColor = new BaseColor(167, 174, 182);
            cellTops.BorderWidth = 1;
            cellTops.Padding = 15;
            cellTops.AddElement(AddChartTopHeader("Top Destino", report.BrokerOutTop.Sum(p => p.Amount), report.FromDate, report.ToDate));
            if (report.BrokerOutTop.Count > 0)
            {
                cellTops.AddElement(AddChartTopList(report.BrokerOutTop));
            }
            else
            {
                cellTops.AddElement(new Phrase("Não houve saídas durante o período selecionado", fontDefault));
            }
            tableTops.AddCell(cellTops);

            /* LINHA DE ESPAÇAMENTO */
            if (report.BrokerInTop.Count > 0 || report.BrokerOutTop.Count > 0)
            {
                tableTops.AddCell(cellTopsEspacamento);
                tableTops.AddCell(cellTopsEspacamento);
                tableTops.AddCell(cellTopsEspacamento);
            }

            /* Assessores com mais aportes */
            if (report.BrokerInTop.Count > 0)
            {
                cellTops = new PdfPCell();
                cellTops.BorderColor = new BaseColor(167, 174, 182);
                cellTops.BorderWidth = 1;
                cellTops.Padding = 15;
                cellTops.AddElement(AddChartTopHeader("Assessores com mais aportes", report.AssessorInTop.Sum(p => p.Amount), report.FromDate, report.ToDate));
                cellTops.AddElement(AddChartTopList(report.AssessorInTop));
                tableTops.AddCell(cellTops);
            }
            else
            {
                tableTops.AddCell(cellTopsEspacamento);
            }

            /* COLUNA DE ESPAÇAMENTO */
            tableTops.AddCell(cellTopsEspacamento);

            /* Assessores com mais saídas */
            if (report.BrokerOutTop.Count > 0)
            {
                cellTops = new PdfPCell();
                cellTops.BorderColor = new BaseColor(167, 174, 182);
                cellTops.BorderWidth = 1;
                cellTops.Padding = 15;
                cellTops.AddElement(AddChartTopHeader("Assessores com mais saídas", report.AssessorOutTop.Sum(p => p.Amount), report.FromDate, report.ToDate));
                cellTops.AddElement(AddChartTopList(report.AssessorOutTop));
                tableTops.AddCell(cellTops);
            }
            else
            {
                tableTops.AddCell(cellTopsEspacamento);
            }

            doc.Add(tableTops);
            #endregion

            doc.Close();

            return memStream;
        }

        private PdfPTable AddChartBar(decimal percentage)
        {
            float sobra = 100 - (float)percentage;

            PdfPTable barraPorcentagem = new PdfPTable(2);
            barraPorcentagem.HorizontalAlignment = 0;
            barraPorcentagem.WidthPercentage = 100;
            barraPorcentagem.SpacingBefore = 0;
            barraPorcentagem.SpacingAfter = 0;

            barraPorcentagem.SetWidths(new[] { (float)percentage, sobra });

            PdfPCell cellPorcentagem = new PdfPCell();
            cellPorcentagem.BackgroundColor = new BaseColor(200, 220, 194);
            cellPorcentagem.Padding = 0;
            cellPorcentagem.Border = PdfPCell.TOP_BORDER | PdfPCell.LEFT_BORDER | PdfPCell.BOTTOM_BORDER;
            cellPorcentagem.BorderColor = new BaseColor(104, 143, 93);
            cellPorcentagem.BorderWidth = 1;
            cellPorcentagem.HorizontalAlignment = 0;
            cellPorcentagem.Phrase = new Phrase(" ");
            barraPorcentagem.AddCell(cellPorcentagem);

            cellPorcentagem.BackgroundColor = new BaseColor(187, 196, 205);
            cellPorcentagem.Border = PdfPCell.TOP_BORDER | PdfPCell.RIGHT_BORDER | PdfPCell.BOTTOM_BORDER;
            barraPorcentagem.AddCell(cellPorcentagem);

            return barraPorcentagem;
        }

        private PdfPTable AddChartTopList(List<CustodyTransferTop> top)
        {
            PdfPTable tableTop1 = new PdfPTable(4);
            tableTop1.WidthPercentage = 100;
            tableTop1.SpacingBefore = 0;
            tableTop1.SpacingAfter = 0;

            tableTop1.SetWidths(new[] { 46, 19, 8, 27 });

            Font fontTitle = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(89, 89, 89));
            Font fontAmount = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(89, 89, 89));
            Font fontPorcentage = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(104, 143, 93));

            foreach (CustodyTransferTop topItem in top)
            {
                PdfPCell cellTopTexto = new PdfPCell();
                cellTopTexto.BorderWidth = 0;
                cellTopTexto.Padding = 4;

                /* Título */
                cellTopTexto.Phrase = new Phrase(topItem.Title, fontTitle);
                tableTop1.AddCell(cellTopTexto);

                /* Barra */
                PdfPCell cellTopBarra = new PdfPCell();
                cellTopBarra.BorderWidth = 0;
                cellTopBarra.Padding = 4;
                cellTopBarra.HorizontalAlignment = 0;
                cellTopBarra.AddElement(AddChartBar(topItem.Percentage));
                tableTop1.AddCell(cellTopBarra);

                /* Porcentagem */
                cellTopTexto.HorizontalAlignment = 2;
                cellTopTexto.Phrase = new Phrase(Math.Round(topItem.Percentage, 0) + "%", fontPorcentage);
                tableTop1.AddCell(cellTopTexto);

                /* Valor */
                cellTopTexto.Phrase = new Phrase(string.Format("{0:C}", topItem.Amount), fontAmount);
                tableTop1.AddCell(cellTopTexto);
            }

            return tableTop1;
        }

        private PdfPTable AddChartTopHeader(string title, decimal amount, DateTime fromDate, DateTime toDate)
        {
            PdfPTable table1 = new PdfPTable(2);
            table1.WidthPercentage = 100;
            table1.SpacingBefore = 0;
            table1.SpacingAfter = 10;

            Font fontTitle = FontFactory.GetFont("Arial", 14, Font.BOLD, new BaseColor(89, 89, 89));
            Font fontAmountPositive = FontFactory.GetFont("Arial", 16, Font.BOLD, new BaseColor(11, 110, 1));
            Font fontAmountNegative = FontFactory.GetFont("Arial", 16, Font.BOLD, new BaseColor(255, 0, 0));
            Font fontDate = FontFactory.GetFont("Arial", 11, Font.NORMAL, new BaseColor(155, 155, 155));

            PdfPCell cellTitle = new PdfPCell();
            cellTitle.Padding = 0;
            cellTitle.BorderWidth = 0;
            cellTitle.Phrase = new Phrase(title, fontTitle);
            table1.AddCell(cellTitle);

            PdfPCell cellAmount = new PdfPCell();
            cellAmount.Padding = 0;
            cellAmount.BorderWidth = 0;
            cellAmount.HorizontalAlignment = 2;
            if (amount > 0)
            {
                cellAmount.Phrase = new Phrase(string.Format("{0:C}", amount), fontAmountPositive);
            }
            else
            {
                cellAmount.Phrase = new Phrase(string.Format("{0:C}", amount), fontAmountNegative);
            }
            table1.AddCell(cellAmount);

            PdfPCell cellDate = new PdfPCell();
            cellDate.Padding = 0;
            cellDate.PaddingTop = 6;
            cellDate.PaddingBottom = 12;
            cellDate.BorderWidth = 0;
            cellDate.BorderWidthBottom = 1;
            cellDate.BorderColorBottom = new BaseColor(210, 213, 217);
            cellDate.Phrase = new Phrase(fromDate.ToShortDateString() + " - " + toDate.ToShortDateString(), fontDate);
            table1.AddCell(cellDate);
            cellDate.Phrase = new Phrase();
            table1.AddCell(cellDate);

            return table1;
        }

        public MemoryStream ExtractSWPPDFReportToMemoryStream(ExtractSWPResponse list)
        {
            iTextSharp.text.Document doc = new iTextSharp.text.Document(new Rectangle(1371, 1500).Rotate(), 30, 30, 30, 50);
            MemoryStream memStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
            writer.CloseStream = false;

            writer.PageEvent = new PrintHeaderFooter { Title = "Extrato de Movimentação de SWP" };

            doc.Open();

            AddReportHeader(ref doc, "Extrato de Movimentação de SWP", null, string.Empty, string.Empty, string.Empty, null, null);


            Font tradeFont = FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(0, 135, 169));
            Font companyAddressFont = FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(111, 111, 111));
            Font dateFont = FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(111, 111, 111));
            Font lineFont = FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(111, 111, 111));
            Font lineHeaderFont = FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(255, 255, 255));

            //var logo = Image.GetInstance(System.Web.HttpContext.Current.Server.MapPath("~/img/logo-brpartners.png"));
            //doc.Add(logo);

            Paragraph pg = new Paragraph();
            pg.Add(Chunk.NEWLINE);

            doc.Add(pg);

            PdfPTable table = new PdfPTable(4);
            table.WidthPercentage = 100;

            table.SetWidths(new[] { 35, 25, 20, 20 });

            PdfPTable tbR = new PdfPTable(1);
            tbR.DefaultCell.BorderWidth = 0.0f;

            PdfPCell cellCab = new PdfPCell();
            cellCab.Border = Rectangle.TOP_BORDER;
            cellCab.BorderWidthTop = 0;
            cellCab.BorderColorTop = new BaseColor(0, 0, 0);

            PdfPTable tb = new PdfPTable(1);
            tb.DefaultCell.Border = Rectangle.NO_BORDER;

            tb.AddCell(new Phrase("BR Partners Banco de Investimento", new Font(FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(0, 0, 0)))));
            tb.AddCell("Av Brigadeiro Faria Lima, 3355 - 26º andar");
            tb.AddCell("CEP: 04538-133 - SÃO PAULO - SP");

            cellCab.Table = tb;
            table.AddCell(cellCab);

            tb = new PdfPTable(1);
            tb.DefaultCell.Border = Rectangle.NO_BORDER;

            tb.AddCell("Tel.: +55 11 3704-1000");
            tb.AddCell("Email: brpartnersctvm@brap.com.br");
            tb.AddCell("CNPJ: 16.695.922/0001-09");

            cellCab.Table = tb;
            table.AddCell(cellCab);

            tb = new PdfPTable(1);
            tb.DefaultCell.Border = Rectangle.NO_BORDER;
            tb.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_CENTER;
            tb.DefaultCell.VerticalAlignment = Rectangle.ALIGN_MIDDLE;

            tb.AddCell(new Phrase("Cliente:", new Font(FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(0, 0, 0)))));
            tb.AddCell(list.Cnpjcpf);

            tbR.AddCell(tb);

            cellCab.Table = tbR;
            table.AddCell(cellCab);

            tb = new PdfPTable(1);
            tb.DefaultCell.Border = Rectangle.NO_BORDER;
            tb.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_CENTER;
            tb.DefaultCell.VerticalAlignment = Rectangle.ALIGN_MIDDLE;

            tb.AddCell(new Phrase("Período:", new Font(FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(0, 0, 0)))));
            tb.AddCell(list.DtRef);

            tbR = new PdfPTable(1);
            tbR.DefaultCell.BorderWidth = 0.0f;
            tbR.AddCell(tb);

            cellCab.Table = tbR;
            table.AddCell(cellCab);

            doc.Add(table);

            doc.Add(pg);

            PdfPTable tableItems = new PdfPTable(15);
            tableItems.WidthPercentage = 100;
            var colWidthPercentages = new[] { 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f };
            tableItems.SetWidths(colWidthPercentages);
            string[] headerItems = new string[15] { "Operação", "Data Operação", "Data Vencto", "Valor Inicial", "Indexador Banco", "Percentual Banco", "Taxa Banco", "Indexador Cliente", "Percentual Cliente", "Taxa Cliente", "Curva Banco", "Curva Cliente", "Ajuste Bruto", "Imposto", "Ajuste Líquido" };
            AddListHeader(ref tableItems, headerItems);


            if (list.ExtratoSWP.Count() > 0)
            {
                foreach (var s in list.ExtratoSWP)
                {

                    bool even = false;

                    AddListItem(ref tableItems, Element.ALIGN_LEFT, s.tp_operacao.ToString(), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, s.dt_operacao.ToString(), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, s.dt_vencimento.ToString(), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, s.vl_base.ToString("N2"), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, s.dc_index_dado.ToString(), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, s.vl_perc_index_data.ToString("N2"), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, s.vl_taxa_dada.ToString("N2"), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, s.dc_index_tomado.ToString(), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, s.vl_perc_index_tomado.ToString("N2"), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, s.vl_taxa_tomado.ToString("N2"), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, s.vl_curva_dado.ToString("N2"), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, s.vl_curva_tomado.ToString("N2"), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, s.vl_bruto.ToString("N2"), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, s.vl_ir.ToString("N2"), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, s.vl_liquido.ToString("N2"), even);

                    // celula com colunas mescladas
                    PdfPCell cell = new PdfPCell();
                    cell.BorderWidthBottom = cell.BorderWidthLeft = cell.BorderWidthRight = cell.BorderWidthTop = 1;
                    cell.BorderColor = new BaseColor(255, 255, 255);
                    cell.PaddingLeft = cell.PaddingTop = cell.PaddingRight = 10;
                    cell.PaddingBottom = 15;
                    cell.Colspan = 10;
                    cell.BackgroundColor = even ? new BaseColor(238, 239, 239) : new BaseColor(230, 231, 231);

                }

                doc.Add(tableItems);
            }

            doc.Close();

            return memStream;
        }



        public MemoryStream ExtractNDFPDFReportToMemoryStream(ExtractNDFResponse list)
        {
            iTextSharp.text.Document doc = new iTextSharp.text.Document(new Rectangle(1371, 1500).Rotate(), 30, 30, 30, 50);
            MemoryStream memStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
            writer.CloseStream = false;

            writer.PageEvent = new PrintHeaderFooter { Title = "Extrato de Movimentação de NDF" };

            doc.Open();

            AddReportHeader(ref doc, "Extrato de Movimentação de NDF", null, string.Empty, string.Empty, string.Empty, null, null);


            Font tradeFont = FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(0, 135, 169));
            Font companyAddressFont = FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(111, 111, 111));
            Font dateFont = FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(111, 111, 111));
            Font lineFont = FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(111, 111, 111));
            Font lineHeaderFont = FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(255, 255, 255));

            //var logo = Image.GetInstance(System.Web.HttpContext.Current.Server.MapPath("~/img/logo-brpartners.png"));
            //doc.Add(logo);

            Paragraph pg = new Paragraph();
            pg.Add(Chunk.NEWLINE);

            doc.Add(pg);

            PdfPTable table = new PdfPTable(4);
            table.WidthPercentage = 100;

            table.SetWidths(new[] { 35, 25, 20, 20 });

            PdfPTable tbR = new PdfPTable(1);
            tbR.DefaultCell.BorderWidth = 0.0f;

            PdfPCell cellCab = new PdfPCell();
            cellCab.Border = Rectangle.TOP_BORDER;
            cellCab.BorderWidthTop = 0;
            cellCab.BorderColorTop = new BaseColor(0, 0, 0);

            PdfPTable tb = new PdfPTable(1);
            tb.DefaultCell.Border = Rectangle.NO_BORDER;

            tb.AddCell(new Phrase("BR Partners Banco de Investimento", new Font(FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(0, 0, 0)))));
            tb.AddCell("Av Brigadeiro Faria Lima, 3355 - 26º andar");
            tb.AddCell("CEP: 04538-133 - SÃO PAULO - SP");

            cellCab.Table = tb;
            table.AddCell(cellCab);

            tb = new PdfPTable(1);
            tb.DefaultCell.Border = Rectangle.NO_BORDER;

            tb.AddCell("Tel.: +55 11 3704-1000");
            tb.AddCell("Email: brpartnersctvm@brap.com.br");
            tb.AddCell("CNPJ: 16.695.922/0001-09");

            cellCab.Table = tb;
            table.AddCell(cellCab);

            tb = new PdfPTable(1);
            tb.DefaultCell.Border = Rectangle.NO_BORDER;
            tb.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_CENTER;
            tb.DefaultCell.VerticalAlignment = Rectangle.ALIGN_MIDDLE;

            tb.AddCell(new Phrase("Cliente:", new Font(FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(0, 0, 0)))));
            tb.AddCell(list.Cnpjcpf);

            tbR.AddCell(tb);

            cellCab.Table = tbR;
            table.AddCell(cellCab);

            tb = new PdfPTable(1);
            tb.DefaultCell.Border = Rectangle.NO_BORDER;
            tb.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_CENTER;
            tb.DefaultCell.VerticalAlignment = Rectangle.ALIGN_MIDDLE;

            tb.AddCell(new Phrase("Período:", new Font(FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(0, 0, 0)))));
            var date = new DateTime(list.Ano, list.Mes, 1);
            tb.AddCell(date.ToShortDateString());

            tbR = new PdfPTable(1);
            tbR.DefaultCell.BorderWidth = 0.0f;
            tbR.AddCell(tb);

            cellCab.Table = tbR;
            table.AddCell(cellCab);

            doc.Add(table);

            doc.Add(pg);

            PdfPTable tableItems = new PdfPTable(11);
            tableItems.WidthPercentage = 100;
            var colWidthPercentages = new[] { 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f };
            tableItems.SetWidths(colWidthPercentages);
            string[] headerItems = new string[11] { "Operação", "Data Operação", "Data Vencto", "Vlr Base Moeda Estr", "Moeda Referência", "Moeda Cotada", "Cotação Vencto", "Cotação Termo", "Ajuste Bruto", "Impostos", "Ajuste Líquido" };
            AddListHeader(ref tableItems, headerItems);


            if (list.ExtratoNDF.Count() > 0)
            {
                foreach (var s in list.ExtratoNDF)
                {

                    bool even = false;

                    AddListItem(ref tableItems, Element.ALIGN_LEFT, s.tp_operacao.ToString(), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, s.dt_operacao.ToString(), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, s.dt_vencimento.ToString(), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, s.vl_base_moeda_estr.ToString("N2"), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, s.tp_moeda_ref.ToString(), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, s.tp_moeda_cot.ToString(), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, s.dc_criterio_termo.ToString(), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, s.vl_cotacao.ToString("N2"), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, s.vl_bruto.ToString("N2"), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, s.vl_ir.ToString("N2"), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, s.vl_liquido.ToString("N2"), even);

                    // celula com colunas mescladas
                    PdfPCell cell = new PdfPCell();
                    cell.BorderWidthBottom = cell.BorderWidthLeft = cell.BorderWidthRight = cell.BorderWidthTop = 1;
                    cell.BorderColor = new BaseColor(255, 255, 255);
                    cell.PaddingLeft = cell.PaddingTop = cell.PaddingRight = 10;
                    cell.PaddingBottom = 15;
                    cell.Colspan = 10;
                    cell.BackgroundColor = even ? new BaseColor(238, 239, 239) : new BaseColor(230, 231, 231);

                }

                doc.Add(tableItems);
            }

            doc.Close();

            return memStream;
        }


        public MemoryStream ExtractPDFReportToMemoryStream(ExtractRendaResponse list)
        {
            iTextSharp.text.Document doc = new iTextSharp.text.Document(new Rectangle(1371, 1500).Rotate(), 30, 30, 30, 50);
            MemoryStream memStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
            writer.CloseStream = false;

            writer.PageEvent = new PrintHeaderFooter { Title = "Extrato de Movimentação" };

            doc.Open();

            AddReportHeader(ref doc, "Extrato de Movimentação", null, string.Empty, string.Empty, string.Empty, null, null);


            Font tradeFont = FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(0, 135, 169));
            Font companyAddressFont = FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(111, 111, 111));
            Font dateFont = FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(111, 111, 111));
            Font lineFont = FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(111, 111, 111));
            Font lineHeaderFont = FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(255, 255, 255));

            //var logo = Image.GetInstance(System.Web.HttpContext.Current.Server.MapPath("~/img/logo-brpartners.png"));
            //doc.Add(logo);

            Paragraph pg = new Paragraph();
            pg.Add(Chunk.NEWLINE);

            doc.Add(pg);

            PdfPTable table = new PdfPTable(4);
            table.WidthPercentage = 100;

            table.SetWidths(new[] { 35, 25, 20, 20 });

            PdfPTable tbR = new PdfPTable(1);
            tbR.DefaultCell.BorderWidth = 0.0f;

            PdfPCell cellCab = new PdfPCell();
            cellCab.Border = Rectangle.TOP_BORDER;
            cellCab.BorderWidthTop = 0;
            cellCab.BorderColorTop = new BaseColor(0, 0, 0);

            PdfPTable tb = new PdfPTable(1);
            tb.DefaultCell.Border = Rectangle.NO_BORDER;

            tb.AddCell(new Phrase("BR Partners Banco de Investimento", new Font(FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(0, 0, 0)))));
            tb.AddCell("Av Brigadeiro Faria Lima, 3355 - 26º andar");
            tb.AddCell("CEP: 04538-133 - SÃO PAULO - SP");

            cellCab.Table = tb;
            table.AddCell(cellCab);

            tb = new PdfPTable(1);
            tb.DefaultCell.Border = Rectangle.NO_BORDER;

            tb.AddCell("Tel.: +55 11 3704-1000");
            tb.AddCell("Email: brpartnersctvm@brap.com.br");
            tb.AddCell("CNPJ: 16.695.922/0001-09");

            cellCab.Table = tb;
            table.AddCell(cellCab);

            tb = new PdfPTable(1);
            tb.DefaultCell.Border = Rectangle.NO_BORDER;
            tb.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_CENTER;
            tb.DefaultCell.VerticalAlignment = Rectangle.ALIGN_MIDDLE;

            tb.AddCell(new Phrase("Cliente:", new Font(FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(0, 0, 0)))));

            var cpf = GeneralFormatter.FormatCPF(list.Cnpjcpf);

            if (cpf.Length == 11)
            {
                tb.AddCell(Convert.ToUInt64(cpf).ToString(@"000\.000\.000\-00"));
            }
            else
            {
                tb.AddCell(Convert.ToUInt64(cpf).ToString(@"00\.000\.000\/0000\-00"));
            }

            tbR.AddCell(tb);

            cellCab.Table = tbR;
            table.AddCell(cellCab);

            tb = new PdfPTable(1);
            tb.DefaultCell.Border = Rectangle.NO_BORDER;
            tb.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_CENTER;
            tb.DefaultCell.VerticalAlignment = Rectangle.ALIGN_MIDDLE;

            tb.AddCell(new Phrase("Período:", new Font(FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(0, 0, 0)))));
            tb.AddCell(list.DtRefDe + " Até " + list.DtRefAte);

            tbR = new PdfPTable(1);
            tbR.DefaultCell.BorderWidth = 0.0f;
            tbR.AddCell(tb);

            cellCab.Table = tbR;
            table.AddCell(cellCab);

            doc.Add(table);

            doc.Add(pg);

            PdfPTable tableItems = new PdfPTable(12);
            tableItems.WidthPercentage = 100;
            var colWidthPercentages = new[] { 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f };
            tableItems.SetWidths(colWidthPercentages);
            string[] headerItems = new string[12] { "Data Operação", "Tipo Operação", "Papel", "Vencimento", "Tipo", "Indexador", "Taxa", "Valor Aplicado", "Valor Bruto", "IOF", "IR", "Valor Líquido" };
            AddListHeader(ref tableItems, headerItems);


            if (list.Extrato.Count() > 0)
            {
                foreach (var s in list.Extrato)
                {

                    bool even = false;

                    AddListItem(ref tableItems, Element.ALIGN_LEFT, s.dt_operacao.ToString(), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, s.dc_evento.ToString(), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, s.dc_grupo_papel.ToString(), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, s.dt_vencimento.ToString(), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, s.dc_indexador.ToString(), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, s.tx_operacao.ToString("N2"), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, s.tx_percentual.ToString("N2"), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, s.vl_operacao.ToString("N2"), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, s.vl_bruto.ToString("N2"), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, s.tx_iof.ToString("N2"), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, s.tx_ir.ToString("N2"), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, s.vl_liquido.ToString("N2"), even);

                    // celula com colunas mescladas
                    PdfPCell cell = new PdfPCell();
                    cell.BorderWidthBottom = cell.BorderWidthLeft = cell.BorderWidthRight = cell.BorderWidthTop = 1;
                    cell.BorderColor = new BaseColor(255, 255, 255);
                    cell.PaddingLeft = cell.PaddingTop = cell.PaddingRight = 10;
                    cell.PaddingBottom = 15;
                    cell.Colspan = 10;
                    cell.BackgroundColor = even ? new BaseColor(238, 239, 239) : new BaseColor(230, 231, 231);

                }

                doc.Add(tableItems);

            }

            Paragraph paragraph3 = new Paragraph();
            paragraph3.SpacingBefore = 20;
            paragraph3.SpacingAfter = 0;
            paragraph3.Alignment = Element.ALIGN_LEFT;
            Phrase phr3 = new Phrase("* Liquidação antecipada de títulos publicos e privados estão sujeitas as condições de resgate pactuadas nas datas de compra, bem como as taxas de mercado praticadas no dia.");
            paragraph3.Add(phr3);
            doc.Add(paragraph3);

            doc.Close();

            return memStream;
        }

        public MemoryStream CreateOperationsMapToMemoryStream(List<OperationsMap> list)
        {
            iTextSharp.text.Document doc = new iTextSharp.text.Document(new Rectangle(970, 1371), 25, 25, 25, 45);
            MemoryStream memStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
            writer.CloseStream = false;

            writer.PageEvent = new PrintHeaderFooter { Title = "OperationsMap" };

            doc.Open();

            Font tradeFont = FontFactory.GetFont("Arial", 30, Font.BOLD, new BaseColor(0, 135, 169));
            Font companyAddressFont = FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(111, 111, 111));
            Font dateFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(111, 111, 111));
            Font lineFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(111, 111, 111));
            Font lineHeaderFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(255, 255, 255));

            AddReportHeader(ref doc, "Mapa de Operações", null, string.Empty, string.Empty, string.Empty, null, null);

            if (list.Count > 0)
            {
                /*** Operadores ***/
                List<Operator> operatorList = new List<Operator>();
                List<int> auxOperatorList = list.Select(a => a.OperatorCode).Distinct().ToList();
                operatorList = (from a in auxOperatorList
                                select new Operator
                                {
                                    OperatorCode = a,
                                    OperatorName = list.Where(b => b.OperatorCode == a).FirstOrDefault().Operator,
                                    BrokerageValue = list.Where(b => b.OperatorCode == a).Sum(c => c.BrokerageValue)
                                }).ToList();

                int iNewPage = 0;

                foreach (var i in operatorList) // lista de operadores
                {
                    AddSimpleHeader(ref doc, i.OperatorName);

                    PdfPTable tableItems = new PdfPTable(4);
                    tableItems.WidthPercentage = 100;
                    var colWidthPercentages = new[] { 10f, 10f, 10f, 10f };
                    tableItems.SetWidths(colWidthPercentages);
                    string[] headerItems = new string[4] { "Código", "Cliente", "Volume", "Corretagem" };
                    AddListHeader(ref tableItems, headerItems);

                    bool even = false;

                    /*** Clientes ***/
                    List<int> itemListClientCodes = list.Where(a => a.OperatorCode == i.OperatorCode).Select(a => a.ClientCode).Distinct().ToList();
                    List<OperationsMap> clientsList = (from a in itemListClientCodes
                                                       select new OperationsMap()
                                                       {
                                                           ClientCode = a,
                                                           Volume = list.Where(b => b.ClientCode == a).Sum(c => c.Volume),
                                                           BrokerageValue = list.Where(b => b.ClientCode == a).Sum(c => c.BrokerageValue),
                                                       }).ToList();

                    foreach (var i1 in clientsList) // lista de clientes do operador 
                    {
                        AddListItem(ref tableItems, Element.ALIGN_LEFT, i1.ClientCode.ToString(), even);
                        AddListItem(ref tableItems, Element.ALIGN_LEFT, CommonHelper.GetClientName(i1.ClientCode), even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, i1.Volume.HasValue ? i1.Volume.Value.ToString("N0") : string.Empty, even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, i1.BrokerageValue.HasValue ? i1.BrokerageValue.Value.ToString("N2") : string.Empty, even);

                        // celula com colunas mescladas
                        PdfPCell cell = new PdfPCell();
                        cell.BorderWidthBottom = cell.BorderWidthLeft = cell.BorderWidthRight = cell.BorderWidthTop = 1;
                        cell.BorderColor = new BaseColor(255, 255, 255);
                        cell.PaddingLeft = cell.PaddingTop = cell.PaddingRight = 15;
                        cell.PaddingBottom = 25;
                        cell.Colspan = 4;
                        cell.BackgroundColor = even ? new BaseColor(238, 239, 239) : new BaseColor(230, 231, 231);

                        // tabela com as operações do cliente do operador
                        PdfPTable tableSubItems = new PdfPTable(6);
                        tableSubItems.HorizontalAlignment = Element.ALIGN_LEFT;
                        tableSubItems.SplitLate = true;
                        tableSubItems.SplitRows = true;
                        colWidthPercentages = new[] { 10f, 10f, 10f, 10f, 10f, 10f };
                        tableSubItems.SetWidths(colWidthPercentages);
                        string[] headerSubItems = new string[6] { "C/V", "Ativo", "Quantidade", "Preço", "Volume", "Corretagem" };
                        AddListSubHeader(ref tableSubItems, headerSubItems);

                        /*** Operações do cliente ***/
                        List<OperationsMap> clientOperationsList = list.Where(p => p.ClientCode == i1.ClientCode).ToList();

                        foreach (var i2 in clientOperationsList)
                        {
                            AddListSubItem(ref tableSubItems, Element.ALIGN_LEFT, i2.OperationDesc);
                            AddListSubItem(ref tableSubItems, Element.ALIGN_LEFT, i2.StockCode);
                            AddListSubItem(ref tableSubItems, Element.ALIGN_RIGHT, i2.TradingQtty.HasValue ? i2.TradingQtty.Value.ToString("N0") : string.Empty);
                            AddListSubItem(ref tableSubItems, Element.ALIGN_RIGHT, i2.TradingValue.HasValue ? i2.TradingValue.Value.ToString("N2") : string.Empty);
                            AddListSubItem(ref tableSubItems, Element.ALIGN_RIGHT, i2.Volume.HasValue ? i2.Volume.Value.ToString("N0") : string.Empty);
                            AddListSubItem(ref tableSubItems, Element.ALIGN_RIGHT, i2.BrokerageValue.HasValue ? i2.BrokerageValue.Value.ToString("N2") : string.Empty);
                        }
                        cell.AddElement(tableSubItems);
                        tableItems.AddCell(cell);
                        even = !even;
                    }
                    doc.Add(tableItems);

                    if (++iNewPage < operatorList.Count)
                    {
                        DisclaimerPage = true;
                        doc.NewPage();
                    }
                    DisclaimerPage = false;
                }
            }

            doc.Close();

            return memStream;
        }

        public MemoryStream CreateOperationsToMemoryStream(List<OperationClient> list)
        {
            iTextSharp.text.Document doc = new iTextSharp.text.Document(new Rectangle(1371, 970).Rotate(), 25, 25, 25, 45);
            MemoryStream memStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
            writer.CloseStream = false;

            writer.PageEvent = new PrintHeaderFooter { Title = "Operations" };

            doc.Open();

            Font tradeFont = FontFactory.GetFont("Arial", 30, Font.BOLD, new BaseColor(0, 135, 169));
            Font companyAddressFont = FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(111, 111, 111));
            Font dateFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(111, 111, 111));
            Font lineFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(111, 111, 111));
            Font lineHeaderFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(255, 255, 255));

            AddReportHeader(ref doc, "Operações", null, string.Empty, string.Empty, string.Empty);

            if (list.Count > 0)
            {
                PdfPTable tableItems = new PdfPTable(5);
                tableItems.WidthPercentage = 100;
                tableItems.SpacingAfter = 30;

                var colWidthPercentages = new[] { 6f, 6f, 15f, 15f, 6f };
                tableItems.SetWidths(colWidthPercentages);
                tableItems.SplitRows = false;

                string[] headerItems = new string[5] { "Pregão", "Código", "Cliente", "Assessor", "Situação" };
                AddListHeader(ref tableItems, headerItems);

                bool even = true;
                foreach (var i in list)
                {
                    AddListItem(ref tableItems, Element.ALIGN_CENTER, i.TradingDate.HasValue ? i.TradingDate.Value.ToShortDateString() : "-", even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.ClientCode.ToString(), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.ClientName, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.AssessorName, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.DescStatus, even);
                    even = !even;
                }
                doc.Add(tableItems);
            }

            doc.Close();

            return memStream;
        }

        public MemoryStream CreateOperationsReportToMemoryStream(OperationReport report, CPClientInformation client)
        {
            iTextSharp.text.Document doc = new iTextSharp.text.Document(new Rectangle(1371, 970).Rotate(), 25, 25, 25, 45);
            MemoryStream memStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
            writer.CloseStream = false;

            writer.PageEvent = new PrintHeaderFooter { Title = "OperationsReport" };

            doc.Open();

            Font font1Normal = FontFactory.GetFont("Arial", 14, Font.NORMAL, new BaseColor(89, 89, 89));
            Font font1NormalVermelha = FontFactory.GetFont("Arial", 14, Font.NORMAL, new BaseColor(215, 6, 1));
            Font font1NormalVerde = FontFactory.GetFont("Arial", 14, Font.NORMAL, new BaseColor(30, 128, 0));
            Font font1Bold = FontFactory.GetFont("Arial", 14, Font.BOLD, new BaseColor(89, 89, 89));
            Font font1BoldVermelha = FontFactory.GetFont("Arial", 14, Font.BOLD, new BaseColor(215, 6, 1));
            Font font1BoldVerde = FontFactory.GetFont("Arial", 14, Font.BOLD, new BaseColor(30, 128, 0));
            Font font1BoldAzul = FontFactory.GetFont("Arial", 14, Font.BOLD, new BaseColor(0, 135, 169));
            Font font2Bold = FontFactory.GetFont("Arial", 20, Font.BOLD, new BaseColor(89, 89, 89));
            Font font2BoldVermelha = FontFactory.GetFont("Arial", 20, Font.BOLD, new BaseColor(215, 6, 1));
            Font font2BoldAzul = FontFactory.GetFont("Arial", 20, Font.BOLD, new BaseColor(0, 135, 169));

            PdfPCell cellEmpty = new PdfPCell();
            cellEmpty.Border = 0;
            cellEmpty.BorderColor = new BaseColor(255, 255, 255);
            cellEmpty.BorderWidthTop = cellEmpty.BorderWidthRight = cellEmpty.BorderWidthBottom = cellEmpty.BorderWidthLeft = 0;
            cellEmpty.Padding = 0;
            cellEmpty.VerticalAlignment = Element.ALIGN_MIDDLE;

            #region SEPARADOR

            PdfPTable separador = new PdfPTable(1);
            separador.SpacingBefore = 0;
            separador.SpacingAfter = 0;
            separador.WidthPercentage = 100;
            PdfPCell cellseparador = new PdfPCell();
            cellseparador.BorderWidthLeft = cellseparador.BorderWidthRight = cellseparador.BorderWidthBottom = 0;
            cellseparador.BorderWidthTop = 1;
            cellseparador.BorderColor = new BaseColor(187, 187, 187);
            cellseparador.Padding = 0;
            separador.AddCell(cellseparador);

            #endregion

            Image logo = Image.GetInstance(System.Web.HttpContext.Current.Server.MapPath("~/img/votorantim-corretora-topo.png"));
            doc.Add(logo);

            #region Cabeçalho

            if (client != null && report != null)
            {
                PdfPTable table1 = new PdfPTable(3);
                table1.WidthPercentage = 100;
                table1.SpacingBefore = 30;
                table1.SpacingAfter = 30;
                var colWidthPercentages = new[] { 15f, 50f, 30f };
                table1.SetWidths(colWidthPercentages);

                PdfPCell cell1 = new PdfPCell();
                cell1.Border = 0;
                cell1.Padding = 0;
                cell1.AddElement(new Phrase("De: ", font1Bold));
                table1.AddCell(cell1);

                cell1 = new PdfPCell();
                cell1.Border = 0;
                cell1.Padding = 0;
                cell1.AddElement(new Phrase(SessionHelper.CurrentUser.Name, font1Normal));
                table1.AddCell(cell1);

                cell1 = new PdfPCell();
                cell1.Border = 0;
                cell1.PaddingTop = cell1.PaddingRight = cell1.PaddingBottom = 0;
                cell1.PaddingLeft = 10;
                cell1.AddElement(new Phrase("Observações: ", font1Bold));
                table1.AddCell(cell1);

                cell1 = new PdfPCell();
                cell1.Border = 0;
                cell1.Padding = 0;
                cell1.AddElement(new Phrase("Data do Pregão: ", font1Bold));
                table1.AddCell(cell1);

                cell1 = new PdfPCell();
                cell1.Border = 0;
                cell1.Padding = 0;
                cell1.AddElement(new Phrase(report.TradeDate.ToShortDateString(), font1Normal));
                table1.AddCell(cell1);

                cell1 = new PdfPCell();
                cell1.Border = 0;
                cell1.PaddingTop = cell1.PaddingRight = cell1.PaddingBottom = 0;
                cell1.PaddingLeft = 10;
                cell1.Rowspan = 3;
                table1.AddCell(cell1);

                cell1 = new PdfPCell();
                cell1.Border = 0;
                cell1.Padding = 0;
                cell1.AddElement(new Phrase("Cliente: ", font1Bold));
                table1.AddCell(cell1);

                cell1 = new PdfPCell();
                cell1.Border = 0;
                cell1.Padding = 0;
                cell1.AddElement(new Phrase(client.Client.Description, font1BoldAzul));
                table1.AddCell(cell1);

                cell1 = new PdfPCell();
                cell1.Border = 0;
                cell1.Padding = 0;
                cell1.AddElement(new Phrase("Código: ", font1Bold));
                table1.AddCell(cell1);

                cell1 = new PdfPCell();
                cell1.Border = 0;
                cell1.Padding = 0;
                cell1.AddElement(new Phrase(client.Client.Value, font1Normal));
                table1.AddCell(cell1);

                doc.Add(table1);
            }

            #endregion

            AddReportHeader(ref doc, "Relatório de Operações Efetuadas", null, string.Empty, string.Empty, string.Empty, null, null, true, false, false);

            if (report != null && report.ReportDetails != null)
            {
                var market = report.ReportDetails.Select(a => a.MarketName).Distinct().ToList();

                foreach (var m in market)
                {
                    var detailsMarket = report.ReportDetails.Where(a => a.MarketName.Equals(m)).ToList();

                    foreach (var mkt in detailsMarket.Select(a => a.TradeType).Distinct().ToList())
                    {
                        if (detailsMarket.FirstOrDefault() != null)
                        {
                            #region data liquidação e mercado

                            Paragraph pgh1 = new Paragraph();
                            pgh1.SpacingAfter = 10;
                            pgh1.Font = font1Bold;
                            if (detailsMarket.FirstOrDefault() != null)
                            {
                                pgh1.Add("Data de Liquidação: " + detailsMarket.FirstOrDefault().SettlementDate.ToShortDateString());
                            }
                            pgh1.Add("   " + m);
                            pgh1.Add("   " + detailsMarket.Where(a => a.TradeType == mkt).FirstOrDefault().TradeType);
                            doc.Add(pgh1);

                            #endregion
                        }

                        #region Detalhes

                        foreach (var d in detailsMarket.Where(a => a.TradeType == mkt).Select(a => a.Header).Distinct().ToList())
                        {
                            var details = detailsMarket.Where(a => a.Header.Equals(d)).ToList();

                            if (details != null && details.Count > 0)
                            {
                                var det = details.FirstOrDefault();

                                PdfPTable table2 = new PdfPTable(5);
                                table2.WidthPercentage = 100;
                                table2.SpacingBefore = 20;
                                table2.SpacingAfter = 15;
                                table2.SplitRows = false;
                                table2.SplitLate = false;
                                table2.KeepTogether = true;
                                var colWidthPercentages2 = new[] { 20f, 20f, 20f, 20f, 20f };
                                table2.SetWidths(colWidthPercentages2);

                                #region Compra TER279 B2W VAREJO ON NM BRBTOWACNOR8

                                Paragraph paragraph2 = new Paragraph();
                                paragraph2.SpacingBefore = 0;
                                paragraph2.SpacingAfter = 10;
                                Phrase phrase1 = new Phrase(String.Format("{0}{1}",
                                    det.MarketType.Equals("TER") ? det.MarketType : det.MarketTypeDesc,
                                    det.MarketType.Equals("TER") ? det.TermDueDateType.ToString() : string.Empty), font1Normal);
                                paragraph2.Add(phrase1);
                                var fontTmp = det.OperationType.Equals("C") ? font1BoldVerde : font1BoldVermelha;
                                Phrase phrase2 = new Phrase(det.OperationTypeDesc.ToUpper(), fontTmp);
                                paragraph2.Add("           ");
                                paragraph2.Add(phrase2);

                                PdfPCell cellInfo = new PdfPCell(cellEmpty);
                                cellInfo.Colspan = 5;
                                cellInfo.AddElement(paragraph2);
                                table2.AddCell(cellInfo);

                                Paragraph paragraph3 = new Paragraph();
                                paragraph3.SpacingBefore = 0;
                                paragraph3.SpacingAfter = 10;
                                Phrase phr3 = new Phrase(String.Format("{0}  {1}", det.CompanyName, det.SpecificationCode), font2BoldAzul);
                                Phrase phr4 = new Phrase(det.IsinCode, font2Bold);
                                paragraph3.Add(phr3);
                                paragraph3.Add("           ");
                                paragraph3.Add(phr4);

                                PdfPCell cellInfo1 = new PdfPCell(cellEmpty);
                                cellInfo1.Colspan = 5;
                                cellInfo1.AddElement(paragraph3);
                                table2.AddCell(cellInfo1);

                                #endregion

                                #region header dos detalhes

                                Paragraph paragraph4 = new Paragraph();
                                paragraph4.Alignment = Element.ALIGN_CENTER;
                                paragraph4.Font = font1Bold;
                                paragraph4.Add("Ativo");
                                PdfPCell cell2 = new PdfPCell();
                                cell2.PaddingBottom = 8;
                                cell2.BorderWidth = 2;
                                cell2.BorderColor = new BaseColor(255, 255, 255);
                                cell2.BackgroundColor = new BaseColor(196, 214, 218);
                                cell2.VerticalAlignment = Element.ALIGN_MIDDLE;
                                cell2.AddElement(paragraph4);
                                table2.AddCell(cell2);

                                paragraph4 = new Paragraph();
                                paragraph4.Alignment = Element.ALIGN_CENTER;
                                paragraph4.Font = font1Bold;
                                paragraph4.Add("Quantidade");
                                cell2 = new PdfPCell();
                                cell2.PaddingBottom = 8;
                                cell2.BorderWidth = 2;
                                cell2.BorderColor = new BaseColor(255, 255, 255);
                                cell2.BackgroundColor = new BaseColor(196, 214, 218);
                                cell2.VerticalAlignment = Element.ALIGN_MIDDLE;
                                cell2.AddElement(paragraph4);
                                table2.AddCell(cell2);

                                paragraph4 = new Paragraph();
                                paragraph4.Alignment = Element.ALIGN_CENTER;
                                paragraph4.Font = font1Bold;
                                paragraph4.Add("Preço");
                                cell2 = new PdfPCell();
                                cell2.PaddingBottom = 8;
                                cell2.BorderWidth = 2;
                                cell2.BorderColor = new BaseColor(255, 255, 255);
                                cell2.BackgroundColor = new BaseColor(196, 214, 218);
                                cell2.VerticalAlignment = Element.ALIGN_MIDDLE;
                                cell2.AddElement(paragraph4);
                                table2.AddCell(cell2);

                                paragraph4 = new Paragraph();
                                paragraph4.Alignment = Element.ALIGN_CENTER;
                                paragraph4.Font = font1Bold;
                                paragraph4.Add("Valor");
                                cell2 = new PdfPCell();
                                cell2.PaddingBottom = 8;
                                cell2.BorderWidth = 2;
                                cell2.BorderColor = new BaseColor(255, 255, 255);
                                cell2.BackgroundColor = new BaseColor(196, 214, 218);
                                cell2.VerticalAlignment = Element.ALIGN_MIDDLE;
                                cell2.AddElement(paragraph4);
                                table2.AddCell(cell2);

                                paragraph4 = new Paragraph();
                                paragraph4.Alignment = Element.ALIGN_CENTER;
                                paragraph4.Font = font1Bold;
                                paragraph4.Add("Custos");
                                cell2 = new PdfPCell();
                                cell2.PaddingBottom = 8;
                                cell2.BorderWidth = 2;
                                cell2.BorderColor = new BaseColor(255, 255, 255);
                                cell2.BackgroundColor = new BaseColor(196, 214, 218);
                                cell2.VerticalAlignment = Element.ALIGN_MIDDLE;
                                cell2.AddElement(paragraph4);
                                table2.AddCell(cell2);

                                #endregion

                                #region linhas de detalhes

                                bool even = true;

                                foreach (var dt in details)
                                {
                                    paragraph4 = new Paragraph();
                                    paragraph4.Font = font1Normal;
                                    paragraph4.Add(dt.TradeCode);
                                    cell2 = new PdfPCell();
                                    cell2.PaddingBottom = 8;
                                    cell2.PaddingLeft = cell2.PaddingRight = 5;
                                    cell2.BorderWidth = 2;
                                    cell2.BorderColor = new BaseColor(255, 255, 255);
                                    cell2.BackgroundColor = even ? new BaseColor(231, 231, 231) : new BaseColor(239, 239, 239);
                                    cell2.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    cell2.AddElement(paragraph4);
                                    table2.AddCell(cell2);

                                    paragraph4 = new Paragraph();
                                    paragraph4.Alignment = Element.ALIGN_RIGHT;
                                    paragraph4.Font = dt.AvailableQuantity < 0 ? font1NormalVermelha : font1Normal;
                                    paragraph4.Add(dt.AvailableQuantity.ToString("N0"));
                                    cell2 = new PdfPCell();
                                    cell2.PaddingBottom = 8;
                                    cell2.PaddingLeft = cell2.PaddingRight = 5;
                                    cell2.BorderWidth = 2;
                                    cell2.BorderColor = new BaseColor(255, 255, 255);
                                    cell2.BackgroundColor = even ? new BaseColor(231, 231, 231) : new BaseColor(239, 239, 239);
                                    cell2.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    cell2.AddElement(paragraph4);
                                    table2.AddCell(cell2);

                                    paragraph4 = new Paragraph();
                                    paragraph4.Alignment = Element.ALIGN_RIGHT;
                                    paragraph4.Font = dt.TradeValue < 0 ? font1NormalVermelha : font1Normal;
                                    paragraph4.Add(dt.TradeValue.ToString("C2"));
                                    cell2 = new PdfPCell();
                                    cell2.PaddingBottom = 8;
                                    cell2.PaddingLeft = cell2.PaddingRight = 5;
                                    cell2.BorderWidth = 2;
                                    cell2.BorderColor = new BaseColor(255, 255, 255);
                                    cell2.BackgroundColor = even ? new BaseColor(231, 231, 231) : new BaseColor(239, 239, 239);
                                    cell2.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    cell2.AddElement(paragraph4);
                                    table2.AddCell(cell2);

                                    paragraph4 = new Paragraph();
                                    paragraph4.Alignment = Element.ALIGN_RIGHT;
                                    paragraph4.Font = dt.TotalNet < 0 ? font1NormalVermelha : font1Normal;
                                    paragraph4.Add(dt.TotalNet.ToString("C2"));
                                    cell2 = new PdfPCell();
                                    cell2.PaddingBottom = 8;
                                    cell2.PaddingLeft = cell2.PaddingRight = 5;
                                    cell2.BorderWidth = 2;
                                    cell2.BorderColor = new BaseColor(255, 255, 255);
                                    cell2.BackgroundColor = even ? new BaseColor(231, 231, 231) : new BaseColor(239, 239, 239);
                                    cell2.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    cell2.AddElement(paragraph4);
                                    table2.AddCell(cell2);

                                    paragraph4 = new Paragraph();
                                    paragraph4.Alignment = Element.ALIGN_RIGHT;
                                    paragraph4.Font = dt.BrokerageValue < 0 ? font1NormalVermelha : font1Normal;
                                    paragraph4.Add(dt.BrokerageValue.ToString("C2"));
                                    cell2 = new PdfPCell();
                                    cell2.PaddingBottom = 8;
                                    cell2.PaddingLeft = cell2.PaddingRight = 5;
                                    cell2.BorderWidth = 2;
                                    cell2.BorderColor = new BaseColor(255, 255, 255);
                                    cell2.BackgroundColor = even ? new BaseColor(231, 231, 231) : new BaseColor(239, 239, 239);
                                    cell2.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    cell2.AddElement(paragraph4);
                                    table2.AddCell(cell2);

                                    even = !even;
                                }

                                #endregion

                                #region footer dos detalhes - soma

                                table2.AddCell(cellEmpty);

                                paragraph4 = new Paragraph();
                                paragraph4.Alignment = Element.ALIGN_RIGHT;
                                paragraph4.Font = details.Sum(a => a.AvailableQuantity) < 0 ? font1BoldVermelha : font1Bold;
                                paragraph4.Add(details.Sum(a => a.AvailableQuantity).ToString("N0"));
                                cell2 = new PdfPCell();
                                cell2.PaddingBottom = 8;
                                cell2.PaddingLeft = cell2.PaddingRight = 5;
                                cell2.BorderWidth = 2;
                                cell2.BorderColor = new BaseColor(255, 255, 255);
                                cell2.BackgroundColor = new BaseColor(208, 208, 208);
                                cell2.VerticalAlignment = Element.ALIGN_MIDDLE;
                                cell2.AddElement(paragraph4);
                                table2.AddCell(cell2);

                                paragraph4 = new Paragraph();
                                paragraph4.Alignment = Element.ALIGN_RIGHT;
                                paragraph4.Font = details.Sum(a => a.TradeValue) < 0 ? font1BoldVermelha : font1Bold;
                                paragraph4.Add(details.Sum(a => a.TradeValue).ToString("C2"));
                                cell2 = new PdfPCell();
                                cell2.PaddingBottom = 8;
                                cell2.PaddingLeft = cell2.PaddingRight = 5;
                                cell2.BorderWidth = 2;
                                cell2.BorderColor = new BaseColor(255, 255, 255);
                                cell2.BackgroundColor = new BaseColor(208, 208, 208);
                                cell2.VerticalAlignment = Element.ALIGN_MIDDLE;
                                cell2.AddElement(paragraph4);
                                table2.AddCell(cell2);

                                paragraph4 = new Paragraph();
                                paragraph4.Alignment = Element.ALIGN_RIGHT;
                                paragraph4.Font = details.Sum(a => a.TotalNet) < 0 ? font1BoldVermelha : font1Bold;
                                paragraph4.Add(details.Sum(a => a.TotalNet).ToString("C2"));
                                cell2 = new PdfPCell();
                                cell2.PaddingBottom = 8;
                                cell2.PaddingLeft = cell2.PaddingRight = 5;
                                cell2.BorderWidth = 2;
                                cell2.BorderColor = new BaseColor(255, 255, 255);
                                cell2.BackgroundColor = new BaseColor(208, 208, 208);
                                cell2.VerticalAlignment = Element.ALIGN_MIDDLE;
                                cell2.AddElement(paragraph4);
                                table2.AddCell(cell2);

                                paragraph4 = new Paragraph();
                                paragraph4.Alignment = Element.ALIGN_RIGHT;
                                paragraph4.Font = details.Sum(a => a.BrokerageValue) < 0 ? font1BoldVermelha : font1Bold;
                                paragraph4.Add(details.Sum(a => a.BrokerageValue).ToString("C2"));
                                cell2 = new PdfPCell();
                                cell2.PaddingBottom = 8;
                                cell2.PaddingLeft = cell2.PaddingRight = 5;
                                cell2.BorderWidth = 2;
                                cell2.BorderColor = new BaseColor(255, 255, 255);
                                cell2.BackgroundColor = new BaseColor(208, 208, 208);
                                cell2.VerticalAlignment = Element.ALIGN_MIDDLE;
                                cell2.AddElement(paragraph4);
                                table2.AddCell(cell2);

                                #endregion

                                #region footer dos detalhes - total

                                table2.AddCell(cellEmpty);

                                table2.AddCell(cellEmpty);

                                paragraph4 = new Paragraph();
                                paragraph4.Alignment = Element.ALIGN_RIGHT;
                                paragraph4.Font = font1Bold;
                                paragraph4.Add("Total:");
                                cell2 = new PdfPCell(cellEmpty);
                                cell2.PaddingBottom = 8;
                                cell2.PaddingLeft = cell2.PaddingRight = 5;
                                cell2.VerticalAlignment = Element.ALIGN_MIDDLE;
                                cell2.AddElement(paragraph4);
                                table2.AddCell(cell2);

                                paragraph4 = new Paragraph();
                                paragraph4.Alignment = Element.ALIGN_RIGHT;
                                paragraph4.Font = details.Sum(a => a.TotalNet) + details.Sum(a => a.BrokerageValue) < 0 ? font1BoldVermelha : font1Bold;
                                paragraph4.Add((details.Sum(a => a.TotalNet) + details.Sum(a => a.BrokerageValue)).ToString("C2"));
                                cell2 = new PdfPCell();
                                cell2.PaddingBottom = 8;
                                cell2.PaddingLeft = cell2.PaddingRight = 5;
                                cell2.BorderWidth = 2;
                                cell2.BorderColor = new BaseColor(255, 255, 255);
                                cell2.VerticalAlignment = Element.ALIGN_MIDDLE;
                                cell2.AddElement(paragraph4);
                                table2.AddCell(cell2);

                                table2.AddCell(cellEmpty);

                                #endregion

                                doc.Add(table2);

                                if (d != detailsMarket.Where(a => a.TradeType == mkt).Select(a => a.Header).Distinct().ToList().LastOrDefault())
                                {
                                    doc.Add(separador);
                                }
                            }
                        }

                        #endregion

                        #region Resumo

                        var resumo = report.ReportFinancial.Where(a => a.TradeType == mkt).FirstOrDefault();
                        if (resumo != null)
                        {
                            PdfPTable table1 = new PdfPTable(1);
                            table1.WidthPercentage = 100;
                            table1.SpacingBefore = 20;
                            table1.SpacingAfter = 0;
                            table1.SplitRows = false;
                            table1.SplitLate = false;
                            table1.KeepTogether = true;

                            PdfPCell cell4 = new PdfPCell();
                            cell4.BorderWidthTop = cell4.BorderWidthRight = cell4.BorderWidthBottom = cell4.BorderWidthLeft = 1;
                            cell4.BorderColor = new BaseColor(187, 187, 187);
                            cell4.Padding = 25;

                            PdfPTable table5 = new PdfPTable(3);
                            table5.WidthPercentage = 100;
                            table5.SpacingBefore = 0;
                            table5.SpacingAfter = 0;
                            table5.SplitRows = false;
                            table5.SplitLate = false;
                            table5.KeepTogether = true;
                            var colWidthPercentages5 = new[] { 49f, 2f, 49f };
                            table5.SetWidths(colWidthPercentages5);

                            #region título Resumo

                            Paragraph paragraph3 = new Paragraph();
                            paragraph3.SpacingBefore = 0;
                            paragraph3.SpacingAfter = 0;
                            paragraph3.Font = font1Bold;
                            paragraph3.Add("Resumo");
                            PdfPCell cell1 = new PdfPCell(cellEmpty);
                            cell1.VerticalAlignment = Element.ALIGN_TOP;
                            cell1.PaddingTop = -6;
                            cell1.PaddingLeft = 0;
                            cell1.AddElement(paragraph3);
                            table5.AddCell(cell1);

                            #endregion

                            table5.AddCell(cellEmpty);

                            #region data da liquidação

                            paragraph3 = new Paragraph();
                            paragraph3.SpacingBefore = 0;
                            paragraph3.SpacingAfter = 0;
                            paragraph3.Alignment = Element.ALIGN_RIGHT;
                            paragraph3.Font = font2Bold;
                            paragraph3.Add(resumo.SettlementDate.ToShortDateString());
                            cell1 = new PdfPCell(cellEmpty);
                            cell1.VerticalAlignment = Element.ALIGN_TOP;
                            cell1.PaddingTop = -2;
                            cell1.PaddingRight = 1;
                            cell1.AddElement(paragraph3);
                            table5.AddCell(cell1);

                            #endregion

                            #region primeira tabela de resumo

                            PdfPTable table3 = new PdfPTable(2);
                            table3.WidthPercentage = 100;
                            table3.SpacingBefore = 20;
                            table3.SpacingAfter = 20;
                            table3.SplitRows = false;
                            table3.SplitLate = false;
                            table3.KeepTogether = true;
                            table3.HorizontalAlignment = Element.ALIGN_LEFT;
                            var colWidthPercentages3 = new[] { 50f, 50f };
                            table3.SetWidths(colWidthPercentages3);

                            #region Total Compras

                            bool even = true;

                            Paragraph paragraph4 = new Paragraph();
                            paragraph4.Font = font1Normal;
                            paragraph4.Add("Total Compras");
                            PdfPCell cell2 = new PdfPCell();
                            cell2.PaddingBottom = 8;
                            cell2.PaddingLeft = cell2.PaddingRight = 5;
                            cell2.BorderWidth = 2;
                            cell2.BorderColor = new BaseColor(255, 255, 255);
                            cell2.BackgroundColor = even ? new BaseColor(231, 231, 231) : new BaseColor(239, 239, 239);
                            cell2.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cell2.AddElement(paragraph4);
                            table3.AddCell(cell2);

                            paragraph4 = new Paragraph();
                            paragraph4.Alignment = Element.ALIGN_RIGHT;
                            paragraph4.Font = resumo.TotalBuying < 0 ? font1NormalVermelha : font1Normal;
                            paragraph4.Add(resumo.TotalBuying.ToString("C2"));
                            cell2 = new PdfPCell();
                            cell2.PaddingBottom = 8;
                            cell2.PaddingLeft = cell2.PaddingRight = 5;
                            cell2.BorderWidth = 2;
                            cell2.BorderColor = new BaseColor(255, 255, 255);
                            cell2.BackgroundColor = even ? new BaseColor(231, 231, 231) : new BaseColor(239, 239, 239);
                            cell2.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cell2.AddElement(paragraph4);
                            table3.AddCell(cell2);

                            #endregion

                            #region Total Vendas

                            even = !even;

                            paragraph4 = new Paragraph();
                            paragraph4.Font = font1Normal;
                            paragraph4.Add("Total Vendas");
                            cell2 = new PdfPCell();
                            cell2.PaddingBottom = 8;
                            cell2.PaddingLeft = cell2.PaddingRight = 5;
                            cell2.BorderWidth = 2;
                            cell2.BorderColor = new BaseColor(255, 255, 255);
                            cell2.BackgroundColor = even ? new BaseColor(231, 231, 231) : new BaseColor(239, 239, 239);
                            cell2.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cell2.AddElement(paragraph4);
                            table3.AddCell(cell2);

                            paragraph4 = new Paragraph();
                            paragraph4.Alignment = Element.ALIGN_RIGHT;
                            paragraph4.Font = resumo.TotalSelling < 0 ? font1NormalVermelha : font1Normal;
                            paragraph4.Add(resumo.TotalSelling.ToString("C2"));
                            cell2 = new PdfPCell();
                            cell2.PaddingBottom = 8;
                            cell2.PaddingLeft = cell2.PaddingRight = 5;
                            cell2.BorderWidth = 2;
                            cell2.BorderColor = new BaseColor(255, 255, 255);
                            cell2.BackgroundColor = even ? new BaseColor(231, 231, 231) : new BaseColor(239, 239, 239);
                            cell2.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cell2.AddElement(paragraph4);
                            table3.AddCell(cell2);

                            #endregion

                            #region Total Termo

                            even = !even;

                            paragraph4 = new Paragraph();
                            paragraph4.Font = font1Normal;
                            paragraph4.Add("Total Termo");
                            cell2 = new PdfPCell();
                            cell2.PaddingBottom = 8;
                            cell2.PaddingLeft = cell2.PaddingRight = 5;
                            cell2.BorderWidth = 2;
                            cell2.BorderColor = new BaseColor(255, 255, 255);
                            cell2.BackgroundColor = even ? new BaseColor(231, 231, 231) : new BaseColor(239, 239, 239);
                            cell2.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cell2.AddElement(paragraph4);
                            table3.AddCell(cell2);

                            paragraph4 = new Paragraph();
                            paragraph4.Alignment = Element.ALIGN_RIGHT;
                            paragraph4.Font = resumo.TotalBuying < 0 ? font1NormalVermelha : font1Normal;
                            paragraph4.Add(mkt.Equals("TER") ? resumo.TotalBuying.ToString("C2") : "R$ 0,00");
                            cell2 = new PdfPCell();
                            cell2.PaddingBottom = 8;
                            cell2.PaddingLeft = cell2.PaddingRight = 5;
                            cell2.BorderWidth = 2;
                            cell2.BorderColor = new BaseColor(255, 255, 255);
                            cell2.BackgroundColor = even ? new BaseColor(231, 231, 231) : new BaseColor(239, 239, 239);
                            cell2.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cell2.AddElement(paragraph4);
                            table3.AddCell(cell2);

                            #endregion

                            #region Total Ajuste Diário

                            even = !even;

                            paragraph4 = new Paragraph();
                            paragraph4.Font = font1Normal;
                            paragraph4.Add("Total Ajuste Diário");
                            cell2 = new PdfPCell();
                            cell2.PaddingBottom = 8;
                            cell2.PaddingLeft = cell2.PaddingRight = 5;
                            cell2.BorderWidth = 2;
                            cell2.BorderColor = new BaseColor(255, 255, 255);
                            cell2.BackgroundColor = even ? new BaseColor(231, 231, 231) : new BaseColor(239, 239, 239);
                            cell2.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cell2.AddElement(paragraph4);
                            table3.AddCell(cell2);

                            paragraph4 = new Paragraph();
                            paragraph4.Alignment = Element.ALIGN_RIGHT;
                            paragraph4.Font = 0 == 0 ? font1NormalVermelha : font1Normal;
                            paragraph4.Add("R$ 0,00");
                            cell2 = new PdfPCell();
                            cell2.PaddingBottom = 8;
                            cell2.PaddingLeft = cell2.PaddingRight = 5;
                            cell2.BorderWidth = 2;
                            cell2.BorderColor = new BaseColor(255, 255, 255);
                            cell2.BackgroundColor = even ? new BaseColor(231, 231, 231) : new BaseColor(239, 239, 239);
                            cell2.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cell2.AddElement(paragraph4);
                            table3.AddCell(cell2);

                            #endregion

                            #region Total Negócios

                            even = !even;

                            paragraph4 = new Paragraph();
                            paragraph4.Font = font1Normal;
                            paragraph4.Add("Total Negócios");
                            cell2 = new PdfPCell();
                            cell2.PaddingBottom = 8;
                            cell2.PaddingLeft = cell2.PaddingRight = 5;
                            cell2.BorderWidth = 2;
                            cell2.BorderColor = new BaseColor(255, 255, 255);
                            cell2.BackgroundColor = even ? new BaseColor(231, 231, 231) : new BaseColor(239, 239, 239);
                            cell2.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cell2.AddElement(paragraph4);
                            table3.AddCell(cell2);

                            paragraph4 = new Paragraph();
                            paragraph4.Alignment = Element.ALIGN_RIGHT;
                            paragraph4.Font = resumo.TotalTrade < 0 ? font1NormalVermelha : font1Normal;
                            paragraph4.Add(resumo.TotalTrade.ToString("C2"));
                            cell2 = new PdfPCell();
                            cell2.PaddingBottom = 8;
                            cell2.PaddingLeft = cell2.PaddingRight = 5;
                            cell2.BorderWidth = 2;
                            cell2.BorderColor = new BaseColor(255, 255, 255);
                            cell2.BackgroundColor = even ? new BaseColor(231, 231, 231) : new BaseColor(239, 239, 239);
                            cell2.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cell2.AddElement(paragraph4);
                            table3.AddCell(cell2);

                            #endregion

                            #region Custos

                            even = !even;

                            paragraph4 = new Paragraph();
                            paragraph4.Font = font1Normal;
                            paragraph4.Add("Custos");
                            cell2 = new PdfPCell();
                            cell2.PaddingBottom = 8;
                            cell2.PaddingLeft = cell2.PaddingRight = 5;
                            cell2.BorderWidth = 2;
                            cell2.BorderColor = new BaseColor(255, 255, 255);
                            cell2.BackgroundColor = even ? new BaseColor(231, 231, 231) : new BaseColor(239, 239, 239);
                            cell2.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cell2.AddElement(paragraph4);
                            table3.AddCell(cell2);

                            paragraph4 = new Paragraph();
                            paragraph4.Alignment = Element.ALIGN_RIGHT;
                            paragraph4.Font = resumo.BrokerageValue < 0 ? font1NormalVermelha : font1Normal;
                            paragraph4.Add(resumo.BrokerageValue.ToString("C2"));
                            cell2 = new PdfPCell();
                            cell2.PaddingBottom = 8;
                            cell2.PaddingLeft = cell2.PaddingRight = 5;
                            cell2.BorderWidth = 2;
                            cell2.BorderColor = new BaseColor(255, 255, 255);
                            cell2.BackgroundColor = even ? new BaseColor(231, 231, 231) : new BaseColor(239, 239, 239);
                            cell2.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cell2.AddElement(paragraph4);
                            table3.AddCell(cell2);

                            #endregion

                            PdfPCell cell3 = new PdfPCell(cellEmpty);
                            cell3.AddElement(table3);
                            table5.AddCell(cell3);

                            #endregion

                            table5.AddCell(cellEmpty);

                            #region segunda tabela de resumo

                            PdfPTable table4 = new PdfPTable(2);
                            table4.WidthPercentage = 100;
                            table4.SpacingBefore = 0;
                            table4.SpacingAfter = 0;
                            table4.SplitRows = false;
                            table4.SplitLate = false;
                            table4.KeepTogether = true;
                            table4.HorizontalAlignment = Element.ALIGN_RIGHT;
                            var colWidthPercentages4 = new[] { 50f, 50f };
                            table4.SetWidths(colWidthPercentages4);

                            #region Taxas Oper. CBLC

                            even = true;

                            paragraph4 = new Paragraph();
                            paragraph4.Font = font1Normal;
                            paragraph4.Add("Taxas Oper. CBLC");
                            cell2 = new PdfPCell();
                            cell2.PaddingBottom = 8;
                            cell2.PaddingLeft = cell2.PaddingRight = 5;
                            cell2.BorderWidth = 2;
                            cell2.BorderColor = new BaseColor(255, 255, 255);
                            cell2.BackgroundColor = even ? new BaseColor(231, 231, 231) : new BaseColor(239, 239, 239);
                            cell2.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cell2.AddElement(paragraph4);
                            table4.AddCell(cell2);

                            paragraph4 = new Paragraph();
                            paragraph4.Alignment = Element.ALIGN_RIGHT;
                            paragraph4.Font = resumo.OperationalTaxCBLC < 0 ? font1NormalVermelha : font1Normal;
                            paragraph4.Add(resumo.OperationalTaxCBLC.ToString("C2"));
                            cell2 = new PdfPCell();
                            cell2.PaddingBottom = 8;
                            cell2.PaddingLeft = cell2.PaddingRight = 5;
                            cell2.BorderWidth = 2;
                            cell2.BorderColor = new BaseColor(255, 255, 255);
                            cell2.BackgroundColor = even ? new BaseColor(231, 231, 231) : new BaseColor(239, 239, 239);
                            cell2.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cell2.AddElement(paragraph4);
                            table4.AddCell(cell2);

                            #endregion

                            #region Taxas Oper. BOVESPA

                            even = !even;

                            paragraph4 = new Paragraph();
                            paragraph4.Font = font1Normal;
                            paragraph4.Add("Taxas Oper. BOVESPA");
                            cell2 = new PdfPCell();
                            cell2.PaddingBottom = 8;
                            cell2.PaddingLeft = cell2.PaddingRight = 5;
                            cell2.BorderWidth = 2;
                            cell2.BorderColor = new BaseColor(255, 255, 255);
                            cell2.BackgroundColor = even ? new BaseColor(231, 231, 231) : new BaseColor(239, 239, 239);
                            cell2.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cell2.AddElement(paragraph4);
                            table4.AddCell(cell2);

                            paragraph4 = new Paragraph();
                            paragraph4.Alignment = Element.ALIGN_RIGHT;
                            paragraph4.Font = resumo.OperationalTaxBovespa < 0 ? font1NormalVermelha : font1Normal;
                            paragraph4.Add(resumo.OperationalTaxBovespa.ToString("C2"));
                            cell2 = new PdfPCell();
                            cell2.PaddingBottom = 8;
                            cell2.PaddingLeft = cell2.PaddingRight = 5;
                            cell2.BorderWidth = 2;
                            cell2.BorderColor = new BaseColor(255, 255, 255);
                            cell2.BackgroundColor = even ? new BaseColor(231, 231, 231) : new BaseColor(239, 239, 239);
                            cell2.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cell2.AddElement(paragraph4);
                            table4.AddCell(cell2);

                            #endregion

                            #region Taxas Operacionais

                            even = !even;

                            paragraph4 = new Paragraph();
                            paragraph4.Font = font1Normal;
                            paragraph4.Add("Taxas Operacionais");
                            cell2 = new PdfPCell();
                            cell2.PaddingBottom = 8;
                            cell2.PaddingLeft = cell2.PaddingRight = 5;
                            cell2.BorderWidth = 2;
                            cell2.BorderColor = new BaseColor(255, 255, 255);
                            cell2.BackgroundColor = even ? new BaseColor(231, 231, 231) : new BaseColor(239, 239, 239);
                            cell2.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cell2.AddElement(paragraph4);
                            table4.AddCell(cell2);

                            paragraph4 = new Paragraph();
                            paragraph4.Alignment = Element.ALIGN_RIGHT;
                            paragraph4.Font = resumo.OperationalTax < 0 ? font1NormalVermelha : font1Normal;
                            paragraph4.Add(resumo.OperationalTax.ToString("C2"));
                            cell2 = new PdfPCell();
                            cell2.PaddingBottom = 8;
                            cell2.PaddingLeft = cell2.PaddingRight = 5;
                            cell2.BorderWidth = 2;
                            cell2.BorderColor = new BaseColor(255, 255, 255);
                            cell2.BackgroundColor = even ? new BaseColor(231, 231, 231) : new BaseColor(239, 239, 239);
                            cell2.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cell2.AddElement(paragraph4);
                            table4.AddCell(cell2);

                            #endregion

                            #region Outras Despesas

                            even = !even;

                            paragraph4 = new Paragraph();
                            paragraph4.Font = font1Normal;
                            paragraph4.Add("Outras Despesas");
                            cell2 = new PdfPCell();
                            cell2.PaddingBottom = 8;
                            cell2.PaddingLeft = cell2.PaddingRight = 5;
                            cell2.BorderWidth = 2;
                            cell2.BorderColor = new BaseColor(255, 255, 255);
                            cell2.BackgroundColor = even ? new BaseColor(231, 231, 231) : new BaseColor(239, 239, 239);
                            cell2.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cell2.AddElement(paragraph4);
                            table4.AddCell(cell2);

                            paragraph4 = new Paragraph();
                            paragraph4.Alignment = Element.ALIGN_RIGHT;
                            paragraph4.Font = resumo.OtherExpenses < 0 ? font1NormalVermelha : font1Normal;
                            paragraph4.Add(resumo.OtherExpenses.ToString("C2"));
                            cell2 = new PdfPCell();
                            cell2.PaddingBottom = 8;
                            cell2.PaddingLeft = cell2.PaddingRight = 5;
                            cell2.BorderWidth = 2;
                            cell2.BorderColor = new BaseColor(255, 255, 255);
                            cell2.BackgroundColor = even ? new BaseColor(231, 231, 231) : new BaseColor(239, 239, 239);
                            cell2.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cell2.AddElement(paragraph4);
                            table4.AddCell(cell2);

                            #endregion

                            #region IR s/ Day Trade

                            even = !even;

                            paragraph4 = new Paragraph();
                            paragraph4.Font = font1Normal;
                            paragraph4.Add("IR s/ Day Trade");
                            cell2 = new PdfPCell();
                            cell2.PaddingBottom = 8;
                            cell2.PaddingLeft = cell2.PaddingRight = 5;
                            cell2.BorderWidth = 2;
                            cell2.BorderColor = new BaseColor(255, 255, 255);
                            cell2.BackgroundColor = even ? new BaseColor(231, 231, 231) : new BaseColor(239, 239, 239);
                            cell2.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cell2.AddElement(paragraph4);
                            table4.AddCell(cell2);

                            paragraph4 = new Paragraph();
                            paragraph4.Alignment = Element.ALIGN_RIGHT;
                            paragraph4.Font = resumo.IRRFDaytrade < 0 ? font1NormalVermelha : font1Normal;
                            paragraph4.Add(resumo.IRRFDaytrade.ToString("C2"));
                            cell2 = new PdfPCell();
                            cell2.PaddingBottom = 8;
                            cell2.PaddingLeft = cell2.PaddingRight = 5;
                            cell2.BorderWidth = 2;
                            cell2.BorderColor = new BaseColor(255, 255, 255);
                            cell2.BackgroundColor = even ? new BaseColor(231, 231, 231) : new BaseColor(239, 239, 239);
                            cell2.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cell2.AddElement(paragraph4);
                            table4.AddCell(cell2);

                            #endregion

                            #region IR s/ Operações

                            even = !even;

                            paragraph4 = new Paragraph();
                            paragraph4.Font = font1Normal;
                            paragraph4.Add("IR s/ Operações");
                            cell2 = new PdfPCell();
                            cell2.PaddingBottom = 8;
                            cell2.PaddingLeft = cell2.PaddingRight = 5;
                            cell2.BorderWidth = 2;
                            cell2.BorderColor = new BaseColor(255, 255, 255);
                            cell2.BackgroundColor = even ? new BaseColor(231, 231, 231) : new BaseColor(239, 239, 239);
                            cell2.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cell2.AddElement(paragraph4);
                            table4.AddCell(cell2);

                            paragraph4 = new Paragraph();
                            paragraph4.Alignment = Element.ALIGN_RIGHT;
                            paragraph4.Font = resumo.IRRFOperations < 0 ? font1NormalVermelha : font1Normal;
                            paragraph4.Add(resumo.IRRFOperations.ToString("C2"));
                            cell2 = new PdfPCell();
                            cell2.PaddingBottom = 8;
                            cell2.PaddingLeft = cell2.PaddingRight = 5;
                            cell2.BorderWidth = 2;
                            cell2.BorderColor = new BaseColor(255, 255, 255);
                            cell2.BackgroundColor = even ? new BaseColor(231, 231, 231) : new BaseColor(239, 239, 239);
                            cell2.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cell2.AddElement(paragraph4);
                            table4.AddCell(cell2);

                            #endregion

                            cell3 = new PdfPCell(cellEmpty);
                            cell3.AddElement(table4);
                            table5.AddCell(cell3);

                            #endregion

                            cell4.AddElement(table5);

                            cell4.AddElement(separador);

                            #region total líquido

                            paragraph3 = new Paragraph();
                            paragraph3.SpacingBefore = 20;
                            paragraph3.SpacingAfter = 0;
                            paragraph3.Alignment = Element.ALIGN_RIGHT;
                            Phrase phr3 = new Phrase("Total Líquido (R$)", font2Bold);
                            paragraph3.Add(phr3);
                            paragraph3.Add("       ");
                            Phrase phr4 = new Phrase(resumo.TotalNet > 0 ? "Crédito" : "Débito", resumo.TotalNet < 0 ? font2BoldVermelha : font2Bold);
                            paragraph3.Add(phr4);
                            paragraph3.Add("       ");
                            Phrase phr5 = new Phrase(resumo.TotalNet.ToString("C2"), resumo.TotalNet < 0 ? font2BoldVermelha : font2Bold);
                            paragraph3.Add(phr5);
                            cell4.AddElement(paragraph3);

                            #endregion

                            table1.AddCell(cell4);
                            doc.Add(table1);
                        }

                        #endregion
                    }
                }
            }

            doc.Close();

            return memStream;
        }

        public MemoryStream CreateIncomeReportPositionToMemoryStream(IncomeReport item, Dictionary<string, bool> checks)
        {
            iTextSharp.text.Document doc = new iTextSharp.text.Document(new Rectangle(1371, 970).Rotate(), 25, 25, 25, 45);
            MemoryStream memStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
            writer.CloseStream = false;

            writer.PageEvent = new PrintHeaderFooter { Title = "Informe de Rendimentos" };

            doc.Open();

            Font tradeFont = FontFactory.GetFont("Arial", 30, Font.BOLD, new BaseColor(0, 135, 169));
            Font companyAddressFont = FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(111, 111, 111));
            Font dateFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(111, 111, 111));
            Font lineFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(111, 111, 111));
            Font lineHeaderFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(255, 255, 255));

            #region Posição

            bool hasPosition = false;

            if (checks["Posicao"] &&
                (item.StockMarketList.Count > 0 ||
                item.StockRentList.Count > 0 ||
                item.OptionsList.Count > 0 ||
                item.BMFOptionsFutureList.Count > 0 ||
                item.BMFGoldList.Count > 0 ||
                item.BMFSwapList.Count > 0 ||
                item.IncomeProceedsList.Count > 0 ||
                item.IncomeTermsList.Count > 0))
            {
                hasPosition = true;

                //AddReportHeader(ref doc, "Informe de Rendimentos - Posição", null, string.Format("{0} - {1}", item.Client.Client.Value, item.Client.Client.Description), string.Empty, string.Empty); 
                AddReportHeader(ref doc, "Informe de Rendimentos - Posição", null, string.Empty, string.Empty, string.Empty);

                //AddSimpleText(ref doc, "Saldo em conta corrente: " + item.Balance.Value.ToString("C2"), 20); 

                #region A Vista

                if (item.StockMarketList.Count > 0)
                {
                    AddSimpleHeader(ref doc, "A Vista");

                    PdfPTable tableItems = new PdfPTable(4);
                    tableItems.WidthPercentage = 100;
                    tableItems.SpacingAfter = 30;

                    //var colWidthPercentages = new[] { 25f, 25f, 25f, 25f };
                    //tableItems.SetWidths(colWidthPercentages);
                    tableItems.SplitRows = false;

                    string[] headerItems = new string[4] { "Ativo", "Qtde.", "Preço", "Valor Total" };
                    AddListHeader(ref tableItems, headerItems);

                    bool even = true;

                    foreach (var i in item.StockMarketList)
                    {
                        AddListItem(ref tableItems, Element.ALIGN_LEFT, i.StockCode, even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.AvailableQuantity.HasValue ? i.AvailableQuantity.Value.ToString("N0") : "-", even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.Price.HasValue ? i.Price.Value.ToString("C2") : "-", even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.TotalValue.HasValue ? i.TotalValue.Value.ToString("C2") : "-", even);
                        even = !even;
                    }

                    doc.Add(tableItems);

                    doc.NewPage();
                }

                #endregion

                #region Aluguel de Ações

                if (item.StockRentList.Count > 0)
                {
                    AddSimpleHeader(ref doc, "Aluguel de Ações");

                    PdfPTable tableItems = new PdfPTable(8);
                    tableItems.WidthPercentage = 100;
                    tableItems.SpacingAfter = 30;

                    //var colWidthPercentages = new[] { 10f, 10f, 10f, 10f };
                    //tableItems.SetWidths(colWidthPercentages);
                    tableItems.SplitRows = false;

                    string[] headerItems = new string[8] { "Ativo", "Tipo", "Abertura", "Vencimento", "Qtde.", "Taxa", "Cotação", "Valor Total" };
                    AddListHeader(ref tableItems, headerItems);

                    bool even = true;

                    foreach (var i in item.StockRentList)
                    {
                        AddListItem(ref tableItems, Element.ALIGN_LEFT, i.StockCode, even);
                        AddListItem(ref tableItems, Element.ALIGN_LEFT, i.Type, even);
                        AddListItem(ref tableItems, Element.ALIGN_CENTER, i.OpenDate.HasValue ? i.OpenDate.Value.ToString("dd/MM/yyyy") : "-", even);
                        AddListItem(ref tableItems, Element.ALIGN_CENTER, i.PaymentDate.HasValue ? i.PaymentDate.Value.ToString("dd/MM/yyyy") : "-", even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.Quantity.HasValue ? i.Quantity.Value.ToString("N0") : "-", even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.Tax.HasValue ? i.Tax.Value.ToString("C2") : "-", even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.Quote.HasValue ? i.Quote.Value.ToString("C2") : "-", even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.TotalValue.HasValue ? i.TotalValue.Value.ToString("C2") : "-", even);
                        even = !even;
                    }

                    doc.Add(tableItems);

                    doc.NewPage();
                }

                #endregion

                #region Opções

                if (item.OptionsList.Count > 0)
                {
                    AddSimpleHeader(ref doc, "Opções");

                    PdfPTable tableItems = new PdfPTable(8);
                    tableItems.WidthPercentage = 100;
                    tableItems.SpacingAfter = 30;

                    //var colWidthPercentages = new[] { 10f, 10f, 10f, 10f };
                    //tableItems.SetWidths(colWidthPercentages);
                    tableItems.SplitRows = false;

                    string[] headerItems = new string[8] { "Ativo", "C/V", "Tipo", "Exercício", "Qtde.", "Preço Médio", "Volume", "Volume Vista" };

                    AddListHeader(ref tableItems, headerItems);

                    bool even = true;

                    foreach (var i in item.OptionsList)
                    {
                        AddListItem(ref tableItems, Element.ALIGN_LEFT, i.StockCode, even);
                        AddListItem(ref tableItems, Element.ALIGN_LEFT, i.OperationType, even);
                        AddListItem(ref tableItems, Element.ALIGN_LEFT, i.Type, even);
                        AddListItem(ref tableItems, Element.ALIGN_CENTER, i.PaymentDate.HasValue ? i.PaymentDate.Value.ToString("dd/MM/yyyy") : "-", even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.AvailableQtty.HasValue ? i.AvailableQtty.Value.ToString("N0") : "-", even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.Price.HasValue ? i.Price.Value.ToString("C2") : "-", even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.Volume.HasValue ? i.Volume.Value.ToString("C2") : "-", even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.VistaVolume.HasValue ? i.VistaVolume.Value.ToString("C2") : "-", even);
                        even = !even;
                    }

                    doc.Add(tableItems);

                    doc.NewPage();
                }

                #endregion

                #region BM&F - Futuro e Opções

                if (item.BMFOptionsFutureList.Count > 0)
                {
                    AddSimpleHeader(ref doc, "BM&F - Futuro e Opções");

                    PdfPTable tableItems = new PdfPTable(6);
                    tableItems.WidthPercentage = 100;
                    tableItems.SpacingAfter = 30;

                    //var colWidthPercentages = new[] { 10f, 10f, 10f, 10f };
                    //tableItems.SetWidths(colWidthPercentages);
                    tableItems.SplitRows = false;

                    string[] headerItems = new string[6] { "Ativo", "Tipo", "Vencimento", "Exercício", "C/V", "Posição Atual" };
                    AddListHeader(ref tableItems, headerItems);

                    bool even = true;

                    foreach (var i in item.BMFOptionsFutureList)
                    {
                        AddListItem(ref tableItems, Element.ALIGN_LEFT, i.StockCode, even);
                        AddListItem(ref tableItems, Element.ALIGN_LEFT, i.Type, even);
                        AddListItem(ref tableItems, Element.ALIGN_CENTER, i.PaymentDate.HasValue ? i.PaymentDate.Value.ToString("dd/MM/yyyy") : "-", even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.Price.HasValue ? i.Price.Value.ToString("N2") : "-", even);
                        AddListItem(ref tableItems, Element.ALIGN_LEFT, i.CV, even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.Position.HasValue ? i.Position.Value.ToString("C2") : "-", even);
                        even = !even;
                    }

                    doc.Add(tableItems);

                    doc.NewPage();
                }

                #endregion

                #region BM&F - Ouro

                if (item.BMFGoldList.Count > 0)
                {
                    AddSimpleHeader(ref doc, "BM&F - Ouro");

                    PdfPTable tableItems = new PdfPTable(4);
                    tableItems.WidthPercentage = 100;
                    tableItems.SpacingAfter = 30;

                    //var colWidthPercentages = new[] { 10f, 10f, 10f, 10f };
                    //tableItems.SetWidths(colWidthPercentages);
                    tableItems.SplitRows = false;

                    string[] headerItems = new string[4] { "Gramas", "Qtde. Padrão", "Cotação", "Valor Total" };
                    AddListHeader(ref tableItems, headerItems);

                    bool even = true;

                    foreach (var i in item.BMFGoldList)
                    {
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.Pounds.HasValue ? i.Pounds.Value.ToString("N2") : "-", even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.DefaultQuantity.HasValue ? i.DefaultQuantity.Value.ToString("N2") : "-", even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.Quote.HasValue ? i.Quote.Value.ToString("C2") : "-", even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.TotalValue.HasValue ? i.TotalValue.Value.ToString("C2") : "-", even);
                        even = !even;
                    }

                    doc.Add(tableItems);

                    doc.NewPage();
                }

                #endregion

                #region BM&F - Swap e Opções Flexíveis

                if (item.BMFSwapList.Count > 0)
                {
                    AddSimpleHeader(ref doc, "BM&F - Swap e Opções Flexíveis");

                    PdfPTable tableItems = new PdfPTable(6);
                    tableItems.WidthPercentage = 100;
                    tableItems.SpacingAfter = 30;

                    //var colWidthPercentages = new[] { 10f, 10f, 10f, 10f };
                    //tableItems.SetWidths(colWidthPercentages);
                    tableItems.SplitRows = false;

                    string[] headerItems = new string[6] { "Contrato", "Número", "Tamanho Base", "Vencimento", "Registro", "Valor Líquido" };
                    AddListHeader(ref tableItems, headerItems);

                    bool even = true;

                    foreach (var i in item.BMFSwapList)
                    {
                        AddListItem(ref tableItems, Element.ALIGN_LEFT, i.Contract, even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.Number.HasValue ? i.Number.Value.ToString("N0") : "-", even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.Base.HasValue ? i.Base.Value.ToString("N0") : "-", even);
                        AddListItem(ref tableItems, Element.ALIGN_CENTER, i.PaymentDate.HasValue ? i.PaymentDate.Value.ToString("dd/MM/yyyy") : "-", even);
                        AddListItem(ref tableItems, Element.ALIGN_CENTER, i.RegisterDate.HasValue ? i.RegisterDate.Value.ToString("dd/MM/yyyy") : "-", even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.NetTotal.HasValue ? i.NetTotal.Value.ToString("C2") : "-", even);
                        even = !even;
                    }

                    doc.Add(tableItems);

                    doc.NewPage();
                }

                #endregion

                #region Proventos Pagos

                if (item.IncomeProceedsList.Count > 0)
                {
                    AddSimpleHeader(ref doc, "Proventos Pagos");

                    PdfPTable tableItems = new PdfPTable(8);
                    tableItems.WidthPercentage = 100;
                    tableItems.SpacingAfter = 30;

                    //var colWidthPercentages = new[] { 10f, 10f, 10f, 10f };
                    //tableItems.SetWidths(colWidthPercentages);
                    tableItems.SplitRows = false;

                    string[] headerItems = new string[8] { "Ativo", "Tipo", "Quantidade", "Valor Bruto", "% IR", "Valor IR", "Valor Líquido", "Pagamento" };
                    AddListHeader(ref tableItems, headerItems);

                    bool even = true;

                    foreach (var i in item.IncomeProceedsList)
                    {
                        AddListItem(ref tableItems, Element.ALIGN_LEFT, i.StockCode, even);
                        AddListItem(ref tableItems, Element.ALIGN_LEFT, i.Type, even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.AvailableQuantity.HasValue ? i.AvailableQuantity.Value.ToString("N0") : "-", even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.GrossValue.HasValue ? i.GrossValue.Value.ToString("C2") : "-", even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.IRPerc.HasValue ? i.IRPerc.Value.ToString("N2") : "-", even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.IRValue.HasValue ? i.IRValue.Value.ToString("C2") : "-", even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.NetValue.HasValue ? i.NetValue.Value.ToString("C2") : "-", even);
                        AddListItem(ref tableItems, Element.ALIGN_CENTER, i.PaymentDate.HasValue ? i.PaymentDate.Value.ToString("dd/MM/yyyy") : "-", even);
                        even = !even;
                    }

                    doc.Add(tableItems);

                    doc.NewPage();
                }

                #endregion

                #region Termo

                if (item.IncomeTermsList.Count > 0)
                {
                    AddSimpleHeader(ref doc, "Termo");

                    PdfPTable tableItems = new PdfPTable(5);
                    tableItems.WidthPercentage = 100;
                    tableItems.SpacingAfter = 30;

                    //var colWidthPercentages = new[] { 10f, 10f, 10f, 10f };
                    //tableItems.SetWidths(colWidthPercentages);
                    tableItems.SplitRows = false;

                    string[] headerItems = new string[5] { "Ativo", "Quantidade", "Preço", "Vencimento", "Valor Total" };
                    AddListHeader(ref tableItems, headerItems);

                    bool even = true;

                    foreach (var i in item.IncomeTermsList)
                    {
                        AddListItem(ref tableItems, Element.ALIGN_LEFT, i.StockCode, even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.AvailableQuantity.HasValue ? i.AvailableQuantity.Value.ToString("N0") : "-", even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.Price.HasValue ? i.Price.Value.ToString("C2") : "-", even);
                        AddListItem(ref tableItems, Element.ALIGN_CENTER, i.PaymentDate.HasValue ? i.PaymentDate.Value.ToString("dd/MM/yyyy") : "-", even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.Total.HasValue ? i.Total.Value.ToString("C2") : "-", even);
                        even = !even;
                    }

                    doc.Add(tableItems);

                    doc.NewPage();
                }

                #endregion
            }

            #endregion

            #region IRRF Day Trade

            if (checks["DayTrade"] && item.IRRFDayTradeList.Count > 0)
            {
                if (hasPosition)
                {
                    AddReportHeader(ref doc, "Informe de Rendimentos - IRRF Day Trade", null, string.Empty, string.Empty, string.Empty, null, null, true, false, false);
                }
                else
                {
                    AddReportHeader(ref doc, "Informe de Rendimentos - IRRF Day Trade", null, string.Empty, string.Empty, string.Empty);
                }

                PdfPTable tableItems = new PdfPTable(3);
                tableItems.WidthPercentage = 100;
                tableItems.SpacingAfter = 30;

                //var colWidthPercentages = new[] { 25f, 25f, 25f, 25f };
                //tableItems.SetWidths(colWidthPercentages);
                tableItems.SplitRows = false;

                string[] headerItems = new string[3] { "Mês", "Total Bruto", "IR Retido" };
                AddListHeader(ref tableItems, headerItems);

                bool even = true;

                foreach (var i in item.IRRFDayTradeList)
                {
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.Month, even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.GrossValue.HasValue ? i.GrossValue.Value.ToString("C2") : "-", even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.Retained.HasValue ? i.Retained.Value.ToString("C2") : "-", even);
                    even = !even;
                }

                doc.Add(tableItems);

                doc.NewPage();
            }

            #endregion

            #region IRRF Operações

            if (checks["Operacoes"] && item.IRRFOperationsList.Count > 0)
            {
                if (hasPosition || (checks["DayTrade"] && item.IRRFDayTradeList.Count > 0))
                {
                    AddReportHeader(ref doc, "Informe de Rendimentos - IRRF Operações", null, string.Empty, string.Empty, string.Empty, null, null, true, false, false);
                }
                else
                {
                    AddReportHeader(ref doc, "Informe de Rendimentos - IRRF Day Trade", null, string.Empty, string.Empty, string.Empty);
                }

                PdfPTable tableItems = new PdfPTable(3);
                tableItems.WidthPercentage = 100;
                tableItems.SpacingAfter = 30;

                //var colWidthPercentages = new[] { 25f, 25f, 25f, 25f };
                //tableItems.SetWidths(colWidthPercentages);
                tableItems.SplitRows = false;

                string[] headerItems = new string[3] { "Mês", "Total Bruto", "IR Retido" };
                AddListHeader(ref tableItems, headerItems);

                bool even = true;

                foreach (var i in item.IRRFOperationsList)
                {
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.Month, even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.GrossValue.HasValue ? i.GrossValue.Value.ToString("C2") : "-", even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.Retained.HasValue ? i.Retained.Value.ToString("C2") : "-", even);
                    even = !even;
                }

                doc.Add(tableItems);

                doc.NewPage();
            }

            #endregion

            doc.Close();

            return memStream;
        }

        public MemoryStream CreateConsolidatedPositionToMemoryStream(ConsolidatedPositionResponse item)
        {
            iTextSharp.text.Document doc = new iTextSharp.text.Document(new Rectangle(1371, 970), 25, 25, 25, 45);
            MemoryStream memStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
            writer.CloseStream = false;

            writer.PageEvent = new PrintHeaderFooter { Title = "Posição Consolidada" };

            doc.Open();

            Font footerFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(89, 89, 89));

            PdfPCell cellEmpty = new PdfPCell();
            cellEmpty.Border = 0;
            cellEmpty.BorderColor = new BaseColor(255, 255, 255);
            cellEmpty.BorderWidthTop = cellEmpty.BorderWidthRight = cellEmpty.BorderWidthBottom = cellEmpty.BorderWidthLeft = 0;
            cellEmpty.Padding = 0;
            cellEmpty.VerticalAlignment = Element.ALIGN_MIDDLE;

            #region Resume

            AddReportHeader(ref doc, "Posição Consolidada", null, string.Empty, string.Empty, string.Empty);

            PdfPTable tableItems1 = new PdfPTable(4);
            tableItems1.WidthPercentage = 100;
            tableItems1.SpacingAfter = 30;

            tableItems1.SetWidths(new[] { 40f, 20f, 20f, 20f });
            tableItems1.SplitRows = false;

            AddListHeader(ref tableItems1, new string[4] { "", "CI", "CN", "Total" });

            bool even = true;
            foreach (var i in item.Result.Resume)
            {
                AddListItem(ref tableItems1, Element.ALIGN_LEFT, i.Label, even);
                AddListItem(ref tableItems1, Element.ALIGN_RIGHT, i.InvestmentAccount.ToString("C2"), even);
                AddListItem(ref tableItems1, Element.ALIGN_RIGHT, i.NormalAccount.ToString("C2"), even);
                AddListItem(ref tableItems1, Element.ALIGN_RIGHT, i.Total.ToString("C2"), even);
                even = !even;
            }

            /*** RODAPÉ ***/
            AddListFooterItem(ref tableItems1, Element.ALIGN_LEFT, "Total C/C", footerFont, new BaseColor(255, 255, 255));
            AddListFooterItem(ref tableItems1, Element.ALIGN_RIGHT, item.Result.Resume.Sum(p => p.InvestmentAccount).ToString("C2"));
            AddListFooterItem(ref tableItems1, Element.ALIGN_RIGHT, item.Result.Resume.Sum(p => p.NormalAccount).ToString("C2"));
            AddListFooterItem(ref tableItems1, Element.ALIGN_RIGHT, item.Result.Resume.Sum(p => p.Total).ToString("C2"));

            doc.Add(tableItems1);

            doc.NewPage();

            #endregion

            #region TotalStock - Ações Total

            AddReportHeader(ref doc, "Posição Consolidada - Ações Total", null, string.Empty, string.Empty, string.Empty, null, null, true, false, false);

            PdfPTable tableItems2 = new PdfPTable(17);
            tableItems2.WidthPercentage = 100;
            tableItems2.SpacingAfter = 30;

            tableItems2.SetWidths(new[] { 8f, 6f, 6f, 9f, 9f, 9f, 9f, 9f, 9f, 9f, 10f, 11f, 10f, 11f, 11f, 7f, 11f });
            tableItems2.SplitRows = false;

            if (item.Result.TotalStock.TotalStockList != null)
            {
                AddListHeader(ref tableItems2, new string[17] { "Papel", "Cart.", "Dist.", "Qtde. Disponível", "Pendência", "Bloqueada", "Liquidar D+0", "Bloqueada D+1", "Bloqueada D+2", "Bloqueada D+3", "VAC Quantidade", "VAC Valor", "Saldo Qtde.", "Preço Atual", "Valor Atual", "Deságio %", "Deságio $" });

                even = true;



                foreach (var i in item.Result.TotalStock.TotalStockList)
                {
                    AddListItem(ref tableItems2, Element.ALIGN_LEFT, i.Stock, even, 10);
                    AddListItem(ref tableItems2, Element.ALIGN_LEFT, i.Portfolio, even, 10);
                    AddListItem(ref tableItems2, Element.ALIGN_LEFT, i.Distribution, even, 10);
                    AddListItem(ref tableItems2, Element.ALIGN_RIGHT, i.AvailableQuantity.ToString("N0"), even, 10, true);
                    AddListItem(ref tableItems2, Element.ALIGN_RIGHT, i.PendingQuantity.ToString("N0"), even, 10, true);
                    AddListItem(ref tableItems2, Element.ALIGN_RIGHT, i.BlockedQuantity.ToString("N0"), even, 10, true);
                    AddListItem(ref tableItems2, Element.ALIGN_RIGHT, i.SettleInD0.ToString("N0"), even, 10, true);
                    AddListItem(ref tableItems2, Element.ALIGN_RIGHT, i.BlockedD1.ToString("N0"), even, 10, true);
                    AddListItem(ref tableItems2, Element.ALIGN_RIGHT, i.BlockedD2.ToString("N0"), even, 10, true);
                    AddListItem(ref tableItems2, Element.ALIGN_RIGHT, i.BlockedD3.ToString("N0"), even, 10, true);
                    AddListItem(ref tableItems2, Element.ALIGN_RIGHT, i.VACQuantity.ToString("N0"), even, 10, true);
                    AddListItem(ref tableItems2, Element.ALIGN_RIGHT, i.VACValue.ToString("N2"), even, 10, true);
                    AddListItem(ref tableItems2, Element.ALIGN_RIGHT, i.BalanceQuantity.ToString("N0"), even, 10, true);
                    AddListItem(ref tableItems2, Element.ALIGN_RIGHT, i.CurrentPrice.ToString("N2"), even, 10, true);
                    AddListItem(ref tableItems2, Element.ALIGN_RIGHT, i.CurrentValue.ToString("N2"), even, 10, true);
                    AddListItem(ref tableItems2, Element.ALIGN_RIGHT, i.DiscountPercentage.ToString("N2") + "%", even, 10, true);
                    AddListItem(ref tableItems2, Element.ALIGN_RIGHT, i.DiscountValue.ToString("N2"), even, 10, true);
                    even = !even;
                }


                /*** RODAPÉ ***/
                tableItems2.AddCell(cellEmpty);
                tableItems2.AddCell(cellEmpty);
                tableItems2.AddCell(cellEmpty);
                tableItems2.AddCell(cellEmpty);
                tableItems2.AddCell(cellEmpty);
                tableItems2.AddCell(cellEmpty);
                tableItems2.AddCell(cellEmpty);
                tableItems2.AddCell(cellEmpty);
                tableItems2.AddCell(cellEmpty);
                AddListFooterItem(ref tableItems2, Element.ALIGN_RIGHT, "Total", footerFont, new BaseColor(255, 255, 255));
                AddListFooterItem(ref tableItems2, Element.ALIGN_RIGHT, item.Result.TotalStock.TotalStockList.Sum(p => p.VACQuantity).ToString("N0"), 10, true);
                AddListFooterItem(ref tableItems2, Element.ALIGN_RIGHT, item.Result.TotalStock.TotalStockList.Sum(p => p.VACValue).ToString("N2"), 10, true);
                AddListFooterItem(ref tableItems2, Element.ALIGN_RIGHT, item.Result.TotalStock.TotalStockList.Sum(p => p.BalanceQuantity).ToString("N0"), 10, true);
                tableItems2.AddCell(cellEmpty);
                AddListFooterItem(ref tableItems2, Element.ALIGN_RIGHT, item.Result.TotalStock.TotalStockList.Sum(p => p.CurrentValue).ToString("N2"), 10, true);
                tableItems2.AddCell(cellEmpty);
                AddListFooterItem(ref tableItems2, Element.ALIGN_RIGHT, item.Result.TotalStock.TotalStockList.Sum(p => p.DiscountValue).ToString("N2"), 10, true);

                doc.Add(tableItems2);

            }
            AddSimpleHeader(ref doc, "Posição Aberta do Dia");

            PdfPTable tableItems3 = new PdfPTable(11);
            tableItems3.WidthPercentage = 100;
            tableItems3.SpacingAfter = 30;

            //tableItems3.SetWidths(new[] { 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f });
            tableItems3.SplitRows = false;

            AddListHeader(ref tableItems3, new string[11] { "Companhia", "C/V", "Mercado", "Qtde.", "Custo", "Preço do Negócio", "Preço Atual", "Lucro/Prejuízo %", "Lucro/Prejuízo $", "Porta", "Operador" });

            even = true;

            if (item.Result.TotalStock.OpeningPositionList != null)
            {

                foreach (var i in item.Result.TotalStock.OpeningPositionList)
                {
                    AddListItem(ref tableItems3, Element.ALIGN_LEFT, i.Company, even);
                    AddListItem(ref tableItems3, Element.ALIGN_LEFT, i.OperationType, even);
                    AddListItem(ref tableItems3, Element.ALIGN_LEFT, i.MarketType, even);
                    AddListItem(ref tableItems3, Element.ALIGN_RIGHT, i.Quantity.ToString("N0"), even);
                    AddListItem(ref tableItems3, Element.ALIGN_RIGHT, i.NegotiationValue.ToString("N2"), even);
                    AddListItem(ref tableItems3, Element.ALIGN_RIGHT, i.Price.ToString("N2"), even);
                    AddListItem(ref tableItems3, Element.ALIGN_RIGHT, i.CurrentPrice.ToString("N2"), even);
                    AddListItem(ref tableItems3, Element.ALIGN_RIGHT, i.ProfitGainLossPercentual.ToString("N2"), even);
                    AddListItem(ref tableItems3, Element.ALIGN_RIGHT, i.ProfitGainLossValue.ToString("N2"), even);
                    AddListItem(ref tableItems3, Element.ALIGN_LEFT, i.Gate, even);
                    AddListItem(ref tableItems3, Element.ALIGN_LEFT, i.Operator, even);
                    even = !even;
                }

            }

            doc.Add(tableItems3);

            doc.NewPage();

            #endregion

            #region Option - Opções

            AddReportHeader(ref doc, "Posição Consolidada - Opções", null, string.Empty, string.Empty, string.Empty, null, null, true, false, false);

            PdfPTable tableItems4 = new PdfPTable(18);
            tableItems4.WidthPercentage = 100;
            tableItems4.SpacingAfter = 30;

            tableItems4.SetWidths(new[] { 8f, 10f, 6f, 6f, 9f, 9f, 9f, 9f, 9f, 9f, 9f, 10f, 11f, 10f, 11f, 11f, 7f, 11f });
            tableItems4.SplitRows = false;

            AddListHeader(ref tableItems4, new string[18] { "Papel", "Vencimento", "Cart.", "Dist.", "Qtde. Disponível", "Pendência", "Bloqueada", "Liquidar D+0", "Bloqueada D+1", "Bloqueada D+2", "Bloqueada D+3", "VAC Quantidade", "VAC Valor", "Saldo Qtde.", "Preço Atual", "Valor Atual", "Deságio %", "Deságio $" });

            even = true;
            foreach (var i in item.Result.Option)
            {
                AddListItem(ref tableItems4, Element.ALIGN_LEFT, i.Stock, even, 10);
                AddListItem(ref tableItems4, Element.ALIGN_CENTER, i.DueDate, even, 10);
                AddListItem(ref tableItems4, Element.ALIGN_LEFT, i.Portfolio, even, 10);
                AddListItem(ref tableItems4, Element.ALIGN_LEFT, i.Distribution, even, 10);
                AddListItem(ref tableItems4, Element.ALIGN_RIGHT, i.AvailableQuantity.ToString("N0"), even, 10, true);
                AddListItem(ref tableItems4, Element.ALIGN_RIGHT, i.PendingQuantity.ToString("N0"), even, 10, true);
                AddListItem(ref tableItems4, Element.ALIGN_RIGHT, i.BlockedQuantity.ToString("N0"), even, 10, true);
                AddListItem(ref tableItems4, Element.ALIGN_RIGHT, i.SettleInD0.ToString("N0"), even, 10, true);
                AddListItem(ref tableItems4, Element.ALIGN_RIGHT, i.BlockedD1.ToString("N0"), even, 10, true);
                AddListItem(ref tableItems4, Element.ALIGN_RIGHT, i.BlockedD2.ToString("N0"), even, 10, true);
                AddListItem(ref tableItems4, Element.ALIGN_RIGHT, i.BlockedD3.ToString("N0"), even, 10, true);
                AddListItem(ref tableItems4, Element.ALIGN_RIGHT, i.VACQuantity.ToString("N0"), even, 10, true);
                AddListItem(ref tableItems4, Element.ALIGN_RIGHT, i.VACValue.ToString("N2"), even, 10, true);
                AddListItem(ref tableItems4, Element.ALIGN_RIGHT, i.BalanceQuantity.ToString("N0"), even, 10, true);
                AddListItem(ref tableItems4, Element.ALIGN_RIGHT, i.CurrentPrice.ToString("N2"), even, 10, true);
                AddListItem(ref tableItems4, Element.ALIGN_RIGHT, i.CurrentValue.ToString("N2"), even, 10, true);
                AddListItem(ref tableItems4, Element.ALIGN_RIGHT, i.DiscountPercentage.ToString("N2") + "%", even, 10, true);
                AddListItem(ref tableItems4, Element.ALIGN_RIGHT, i.DiscountValue.ToString("N2"), even, 10, true);
                even = !even;
            }

            doc.Add(tableItems4);

            doc.NewPage();

            #endregion

            #region Term - Termo

            AddReportHeader(ref doc, "Posição Consolidada - Termo", null, string.Empty, string.Empty, string.Empty, null, null, true, false, false);

            PdfPTable tableItems5 = new PdfPTable(17);
            tableItems5.WidthPercentage = 100;
            tableItems5.SpacingAfter = 30;

            tableItems5.SetWidths(new[] { 7f, 6f, 6f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 7f, 10f });
            tableItems5.SplitRows = false;

            AddListHeader(ref tableItems5, new string[17] { "Papel", "Cart.", "Dist.", "Disponível", "Pendência", "Bloqueada", "Liquidar D+0", "Bloq. D+1", "Bloq. D+2", "Bloq. D+3", "VAC Qtde.", "VAC Valor", "Saldo Qtde.", "Preço Atual", "Valor", "Deságio %", "Deságio $" });

            even = true;

            if (item.Result.Terms != null)
            {

                foreach (var i in item.Result.Terms.Terms)
                {
                    AddListItem(ref tableItems5, Element.ALIGN_LEFT, i.Stock, even, 10);
                    AddListItem(ref tableItems5, Element.ALIGN_LEFT, i.Portfolio, even, 10);
                    AddListItem(ref tableItems5, Element.ALIGN_LEFT, i.Distribution, even, 10);
                    AddListItem(ref tableItems5, Element.ALIGN_RIGHT, i.AvailableQuantity.ToString("N0"), even, 10, true);
                    AddListItem(ref tableItems5, Element.ALIGN_RIGHT, i.PendingQuantity.ToString("N0"), even, 10, true);
                    AddListItem(ref tableItems5, Element.ALIGN_RIGHT, i.BlockedQuantity.ToString("N0"), even, 10, true);
                    AddListItem(ref tableItems5, Element.ALIGN_RIGHT, i.SettleInD0.ToString("N0"), even, 10, true);
                    AddListItem(ref tableItems5, Element.ALIGN_RIGHT, i.BlockedD1.ToString("N0"), even, 10, true);
                    AddListItem(ref tableItems5, Element.ALIGN_RIGHT, i.BlockedD2.ToString("N0"), even, 10, true);
                    AddListItem(ref tableItems5, Element.ALIGN_RIGHT, i.BlockedD3.ToString("N0"), even, 10, true);
                    AddListItem(ref tableItems5, Element.ALIGN_RIGHT, i.VACQuantity.ToString("N0"), even, 10, true);
                    AddListItem(ref tableItems5, Element.ALIGN_RIGHT, i.VACValue.ToString("N2"), even, 10, true);
                    AddListItem(ref tableItems5, Element.ALIGN_RIGHT, i.BalanceQuantity.ToString("N0"), even, 10, true);
                    AddListItem(ref tableItems5, Element.ALIGN_RIGHT, i.CurrentPrice.ToString("N2"), even, 10, true);
                    AddListItem(ref tableItems5, Element.ALIGN_RIGHT, i.CurrentValue.ToString("N2"), even, 10, true);
                    AddListItem(ref tableItems5, Element.ALIGN_RIGHT, i.DiscountPercentage.ToString("N2"), even, 10, true);
                    AddListItem(ref tableItems5, Element.ALIGN_RIGHT, i.DiscountValue.ToString("N2"), even, 10, true);
                    even = !even;
                }

            }

            doc.Add(tableItems5);


            AddSimpleHeader(ref doc, "Descrição dos Termos Registrados");

            PdfPTable tableItems6 = new PdfPTable(12);
            tableItems6.WidthPercentage = 100;
            tableItems6.SpacingAfter = 30;

            //tableItems6.SetWidths(new[] { 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f });
            tableItems6.SplitRows = false;

            AddListHeader(ref tableItems6, new string[12] { "Operação", "Papel", "Contrato", "Abertura", "Vencimento", "Qtde. Orig.", "Preço Orig.", "Valor Orig.", "Preço Atual", "Valor Atual", "Lucro/Prejuízo %", "Lucro/Prejuízo $" });

            even = true;

            if (item.Result.Terms != null)
            {

                foreach (var i in item.Result.Terms.RegisteredTerms)
                {
                    AddListItem(ref tableItems6, Element.ALIGN_LEFT, i.Operation, even, 10);
                    AddListItem(ref tableItems6, Element.ALIGN_LEFT, i.Stock, even, 10);
                    AddListItem(ref tableItems6, Element.ALIGN_LEFT, i.Contract, even, 10);
                    AddListItem(ref tableItems6, Element.ALIGN_CENTER, i.Opening, even, 10);
                    AddListItem(ref tableItems6, Element.ALIGN_CENTER, i.DueDate, even, 10);
                    AddListItem(ref tableItems6, Element.ALIGN_RIGHT, i.OriginalQuantity.ToString("N0"), even, 10, true);
                    AddListItem(ref tableItems6, Element.ALIGN_RIGHT, i.OriginalPrice.ToString("N2"), even, 10, true);
                    AddListItem(ref tableItems6, Element.ALIGN_RIGHT, i.OriginalValue.ToString("N2"), even, 10, true);
                    AddListItem(ref tableItems6, Element.ALIGN_RIGHT, i.CurrentPrice.ToString("N2"), even, 10, true);
                    AddListItem(ref tableItems6, Element.ALIGN_RIGHT, i.CurrentValue.ToString("N2"), even, 10, true);
                    AddListItem(ref tableItems6, Element.ALIGN_RIGHT, i.ProfitLossPercentage.ToString("N2"), even, 10, true);
                    AddListItem(ref tableItems6, Element.ALIGN_RIGHT, i.ProfitLossValue.ToString("N2"), even, 10, true);
                    even = !even;
                }
            }
            doc.Add(tableItems6);

            doc.NewPage();

            #endregion

            #region SettledTerms - Termos Liquidados

            AddReportHeader(ref doc, "Posição Consolidada - Termos Liquidados", null, string.Empty, string.Empty, string.Empty, null, null, true, false, false);

            PdfPTable tableItems7 = new PdfPTable(10);
            tableItems7.WidthPercentage = 100;
            tableItems7.SpacingAfter = 30;

            //tableItems7.SetWidths(new[] { 8f, 10f, 6f, 6f, 9f, 9f, 9f, 9f, 9f, 9f, 9f, 10f, 11f, 10f, 11f, 11f, 7f, 11f });
            tableItems7.SplitRows = false;

            AddListHeader(ref tableItems7, new string[10] { "Liquidação", "Papel", "Tipo", "Nº Negócio", "C/V", "Quantidade", "Preço Negócio", "Valor Contrato Liquidado", "Valor Contrato Revertido", "Lucro/Prejuízo $" });

            even = true;

            if (item.Result.SettledTerms != null)
            {

                foreach (var i in item.Result.SettledTerms)
                {
                    AddListItem(ref tableItems7, Element.ALIGN_CENTER, i.LiquidationDate, even);
                    AddListItem(ref tableItems7, Element.ALIGN_LEFT, i.Stock, even);
                    AddListItem(ref tableItems7, Element.ALIGN_LEFT, i.LiquidationType, even);
                    AddListItem(ref tableItems7, Element.ALIGN_LEFT, i.BusinessNumber.ToString(), even);
                    AddListItem(ref tableItems7, Element.ALIGN_LEFT, i.CV, even);
                    AddListItem(ref tableItems7, Element.ALIGN_RIGHT, i.Quantity.ToString("N0"), even);
                    AddListItem(ref tableItems7, Element.ALIGN_RIGHT, i.BusinessPrice.ToString("N2"), even);
                    AddListItem(ref tableItems7, Element.ALIGN_RIGHT, i.SettledContractValue.ToString("N2"), even);
                    AddListItem(ref tableItems7, Element.ALIGN_RIGHT, i.SettledContractReversed.ToString("N2"), even);
                    AddListItem(ref tableItems7, Element.ALIGN_RIGHT, i.ProfitLoss.ToString("N2"), even);
                    even = !even;
                }

            }

            doc.Add(tableItems7);

            doc.NewPage();

            #endregion

            #region SettledTermsToday - Termos Liquidados Hoje

            AddReportHeader(ref doc, "Posição Consolidada - Termos Liquidados Hoje", null, string.Empty, string.Empty, string.Empty, null, null, true, false, false);

            PdfPTable tableItems8 = new PdfPTable(10);
            tableItems8.WidthPercentage = 100;
            tableItems8.SpacingAfter = 30;

            //tableItems8.SetWidths(new[] { 8f, 10f, 6f, 6f, 9f, 9f, 9f, 9f, 9f, 9f, 9f, 10f, 11f, 10f, 11f, 11f, 7f, 11f });
            tableItems8.SplitRows = false;

            AddListHeader(ref tableItems8, new string[10] { "Liquidação", "Papel", "Tipo", "Nº Negócio", "C/V", "Quantidade", "Preço Negócio", "Valor Contrato Liquidado", "Valor Contrato Revertido", "Lucro/Prejuízo $" });

            even = true;

            if (item.Result.SettledTermsToday != null)
            {

                foreach (var i in item.Result.SettledTermsToday)
                {
                    AddListItem(ref tableItems8, Element.ALIGN_CENTER, i.LiquidationDate, even);
                    AddListItem(ref tableItems8, Element.ALIGN_LEFT, i.Stock, even);
                    AddListItem(ref tableItems8, Element.ALIGN_LEFT, i.LiquidationType, even);
                    AddListItem(ref tableItems8, Element.ALIGN_LEFT, i.BusinessNumber.ToString(), even);
                    AddListItem(ref tableItems8, Element.ALIGN_LEFT, i.CV, even);
                    AddListItem(ref tableItems8, Element.ALIGN_RIGHT, i.Quantity.ToString("N0"), even);
                    AddListItem(ref tableItems8, Element.ALIGN_RIGHT, i.BusinessPrice.ToString("N2"), even);
                    AddListItem(ref tableItems8, Element.ALIGN_RIGHT, i.SettledContractValue.ToString("N2"), even);
                    AddListItem(ref tableItems8, Element.ALIGN_RIGHT, i.SettledContractReversed.ToString("N2"), even);
                    AddListItem(ref tableItems8, Element.ALIGN_RIGHT, i.ProfitLoss.ToString("N2"), even);
                    even = !even;
                }

            }

            doc.Add(tableItems8);

            doc.NewPage();

            #endregion

            #region RentDonor - Aluguel Doador

            AddReportHeader(ref doc, "Posição Consolidada - Aluguel Doador", null, string.Empty, string.Empty, string.Empty, null, null, true, false, false);

            PdfPTable tableItems9 = new PdfPTable(14);
            tableItems9.WidthPercentage = 100;
            tableItems9.SpacingAfter = 30;

            //tableItems9.SetWidths(new[] { 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f });
            tableItems9.SplitRows = false;

            AddListHeader(ref tableItems9, new string[14] { "Companhia", "ISIN", "Carteira", "Origem", "Abertura", "Vencimento", "Carência", "Quantidade", "Preço Médio", "Taxa de Remuneração", "Taxa de Comissão", "Valor Líquido", "Preço Atual", "Valor Atual" });

            even = true;

            if (item.Result.RentDonor != null)
            {

                foreach (var i in item.Result.RentDonor)
                {
                    AddListItem(ref tableItems9, Element.ALIGN_LEFT, i.Company, even, 10);
                    AddListItem(ref tableItems9, Element.ALIGN_LEFT, i.ISIN, even, 10);
                    AddListItem(ref tableItems9, Element.ALIGN_LEFT, i.Portfolio, even, 10);
                    AddListItem(ref tableItems9, Element.ALIGN_CENTER, i.Origin, even, 10);
                    AddListItem(ref tableItems9, Element.ALIGN_CENTER, i.OpeningDate, even, 10);
                    AddListItem(ref tableItems9, Element.ALIGN_CENTER, i.DueDate, even, 10);
                    AddListItem(ref tableItems9, Element.ALIGN_CENTER, i.LiquidationGraceDate, even, 10);
                    AddListItem(ref tableItems9, Element.ALIGN_RIGHT, i.Quantity.ToString("N0"), even, 10, true);
                    AddListItem(ref tableItems9, Element.ALIGN_RIGHT, i.AveragePrice.ToString("N2"), even, 10, true);
                    AddListItem(ref tableItems9, Element.ALIGN_RIGHT, i.RemunerationRate.ToString("N2"), even, 10, true);
                    AddListItem(ref tableItems9, Element.ALIGN_RIGHT, i.CommisionRate.ToString("N2"), even, 10, true);
                    AddListItem(ref tableItems9, Element.ALIGN_RIGHT, i.NetValue.ToString("N2"), even, 10, true);
                    AddListItem(ref tableItems9, Element.ALIGN_RIGHT, i.CurrentPrice.ToString("N2"), even, 10, true);
                    AddListItem(ref tableItems9, Element.ALIGN_RIGHT, i.CurrentValue.ToString("N2"), even, 10, true);
                    even = !even;
                }

                /*** RODAPÉ ***/
                tableItems9.AddCell(cellEmpty);
                tableItems9.AddCell(cellEmpty);
                tableItems9.AddCell(cellEmpty);
                tableItems9.AddCell(cellEmpty);
                tableItems9.AddCell(cellEmpty);
                tableItems9.AddCell(cellEmpty);
                AddListFooterItem(ref tableItems9, Element.ALIGN_RIGHT, "Total", footerFont, new BaseColor(255, 255, 255));
                AddListFooterItem(ref tableItems9, Element.ALIGN_RIGHT, item.Result.RentDonor.Sum(p => p.Quantity).ToString("N0"), 10, true);
                tableItems9.AddCell(cellEmpty);
                tableItems9.AddCell(cellEmpty);
                tableItems9.AddCell(cellEmpty);
                AddListFooterItem(ref tableItems9, Element.ALIGN_RIGHT, item.Result.RentDonor.Sum(p => p.NetValue).ToString("N2"), 10, true);
                tableItems9.AddCell(cellEmpty);
                AddListFooterItem(ref tableItems9, Element.ALIGN_RIGHT, item.Result.RentDonor.Sum(p => p.CurrentValue).ToString("N2"), 10, true);

            }
            doc.Add(tableItems9);

            doc.NewPage();

            #endregion

            #region RentTaker - Aluguel Tomador

            AddReportHeader(ref doc, "Posição Consolidada - Aluguel Tomador", null, string.Empty, string.Empty, string.Empty, null, null, true, false, false);

            PdfPTable tableItems10 = new PdfPTable(14);
            tableItems10.WidthPercentage = 100;
            tableItems10.SpacingAfter = 30;

            //tableItems10.SetWidths(new[] { 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f });
            tableItems10.SplitRows = false;

            AddListHeader(ref tableItems10, new string[14] { "Companhia", "ISIN", "Carteira", "Origem", "Abertura", "Vencimento", "Carência", "Quantidade", "Preço Médio", "Taxa de Remuneração", "Taxa de Comissão", "Valor Líquido", "Preço Atual", "Valor Atual" });

            even = true;

            if (item.Result.RentTaker != null)
            {

                foreach (var i in item.Result.RentTaker)
                {
                    AddListItem(ref tableItems10, Element.ALIGN_LEFT, i.Company, even, 10);
                    AddListItem(ref tableItems10, Element.ALIGN_LEFT, i.ISIN, even, 10);
                    AddListItem(ref tableItems10, Element.ALIGN_LEFT, i.Portfolio, even, 10);
                    AddListItem(ref tableItems10, Element.ALIGN_CENTER, i.Origin, even, 10);
                    AddListItem(ref tableItems10, Element.ALIGN_CENTER, i.OpeningDate, even, 10);
                    AddListItem(ref tableItems10, Element.ALIGN_CENTER, i.DueDate, even, 10);
                    AddListItem(ref tableItems10, Element.ALIGN_CENTER, i.LiquidationGraceDate, even, 10);
                    AddListItem(ref tableItems10, Element.ALIGN_RIGHT, i.Quantity.ToString("N0"), even, 10, true);
                    AddListItem(ref tableItems10, Element.ALIGN_RIGHT, i.AveragePrice.ToString("N2"), even, 10, true);
                    AddListItem(ref tableItems10, Element.ALIGN_RIGHT, i.RemunerationRate.ToString("N2"), even, 10, true);
                    AddListItem(ref tableItems10, Element.ALIGN_RIGHT, i.CommisionRate.ToString("N2"), even, 10, true);
                    AddListItem(ref tableItems10, Element.ALIGN_RIGHT, i.NetValue.ToString("N2"), even, 10, true);
                    AddListItem(ref tableItems10, Element.ALIGN_RIGHT, i.CurrentPrice.ToString("N2"), even, 10, true);
                    AddListItem(ref tableItems10, Element.ALIGN_RIGHT, i.CurrentValue.ToString("N2"), even, 10, true);
                    even = !even;
                }

                /*** RODAPÉ ***/
                tableItems10.AddCell(cellEmpty);
                tableItems10.AddCell(cellEmpty);
                tableItems10.AddCell(cellEmpty);
                tableItems10.AddCell(cellEmpty);
                tableItems10.AddCell(cellEmpty);
                tableItems10.AddCell(cellEmpty);
                AddListFooterItem(ref tableItems10, Element.ALIGN_RIGHT, "Total", footerFont, new BaseColor(255, 255, 255));
                AddListFooterItem(ref tableItems10, Element.ALIGN_RIGHT, item.Result.RentTaker.Sum(p => p.Quantity).ToString("N0"), 10, true);
                tableItems10.AddCell(cellEmpty);
                tableItems10.AddCell(cellEmpty);
                tableItems10.AddCell(cellEmpty);
                AddListFooterItem(ref tableItems10, Element.ALIGN_RIGHT, item.Result.RentTaker.Sum(p => p.NetValue).ToString("N2"), 10, true);
                tableItems10.AddCell(cellEmpty);
                AddListFooterItem(ref tableItems10, Element.ALIGN_RIGHT, item.Result.RentTaker.Sum(p => p.CurrentValue).ToString("N2"), 10, true);
            }
            doc.Add(tableItems10);

            doc.NewPage();

            #endregion

            #region BovespaGuarantees - Garantias Bovespa

            AddReportHeader(ref doc, "Posição Consolidada - Garantias Bovespa", null, string.Empty, string.Empty, string.Empty, null, null, true, false, false);

            PdfPTable tableItems11 = new PdfPTable(10);
            tableItems11.WidthPercentage = 100;
            tableItems11.SpacingAfter = 30;

            //tableItems11.SetWidths(new[] { 8f, 10f, 6f, 6f, 9f, 9f, 9f, 9f, 9f, 9f, 9f, 10f, 11f, 10f, 11f, 11f, 7f, 11f });
            tableItems11.SplitRows = false;

            AddListHeader(ref tableItems11, new string[10] { "Ativo", "Depósito", "Banco", "Origem", "Qtde.", "Valor", "Vencimento", "Guarda-Chuva", "Preço", "Valor Total" });

            even = true;

            if (item.Result.BovespaGuarantees != null)
            {

                foreach (var i in item.Result.BovespaGuarantees)
                {
                    AddListItem(ref tableItems11, Element.ALIGN_LEFT, i.Stock, even);
                    AddListItem(ref tableItems11, Element.ALIGN_CENTER, i.DepositDate, even);
                    AddListItem(ref tableItems11, Element.ALIGN_LEFT, i.Bank, even);
                    AddListItem(ref tableItems11, Element.ALIGN_LEFT, i.Origin, even);
                    AddListItem(ref tableItems11, Element.ALIGN_RIGHT, i.Quantity.ToString("N0"), even);
                    AddListItem(ref tableItems11, Element.ALIGN_RIGHT, i.GuaranteeValue.ToString("N2"), even);
                    AddListItem(ref tableItems11, Element.ALIGN_CENTER, i.DueDate, even);
                    AddListItem(ref tableItems11, Element.ALIGN_LEFT, i.Umbrella, even);
                    AddListItem(ref tableItems11, Element.ALIGN_RIGHT, i.CurrentPrice.ToString("N2"), even);
                    AddListItem(ref tableItems11, Element.ALIGN_RIGHT, i.TotalValue.ToString("N2"), even);
                    even = !even;
                }

            }
            doc.Add(tableItems11);



            doc.NewPage();

            #endregion

            #region BMF - BM&F Total

            AddReportHeader(ref doc, "Posição Consolidada - BM&F Total", null, string.Empty, string.Empty, string.Empty, null, null, true, false, false);

            AddSimpleHeader(ref doc, "BM&F: Abertura");

            PdfPTable tableItems12 = new PdfPTable(6);
            tableItems12.WidthPercentage = 100;
            tableItems12.SpacingAfter = 30;

            //tableItems12.SetWidths(new[] { 10f, 10f, 10f, 10f, 10f, 10f });
            tableItems12.SplitRows = false;

            AddListHeader(ref tableItems12, new string[6] { "Mercadoria", "Quantidade", "Fechamento", "Atual", "Lucro/Prejuízo %", "Lucro/Prejuízo $" });

            even = true;

            if (item.Result.BMF != null && item.Result.BMF.BMFOpenList != null)
            {

                foreach (var i in item.Result.BMF.BMFOpenList)
                {
                    AddListItem(ref tableItems12, Element.ALIGN_LEFT, i.Commodity, even);
                    AddListItem(ref tableItems12, Element.ALIGN_RIGHT, i.Quantity.ToString("N0"), even);
                    AddListItem(ref tableItems12, Element.ALIGN_RIGHT, i.Closing.ToString("N2"), even);
                    AddListItem(ref tableItems12, Element.ALIGN_RIGHT, i.Current.ToString("N2"), even);
                    AddListItem(ref tableItems12, Element.ALIGN_RIGHT, i.ProfitLossPercentage.ToString("N2") + "%", even);
                    AddListItem(ref tableItems12, Element.ALIGN_RIGHT, i.ProfitLoss.ToString("N2"), even);
                    even = !even;
                }



                /*** RODAPÉ ***/
                tableItems12.AddCell(cellEmpty);
                AddListFooterItem(ref tableItems12, Element.ALIGN_RIGHT, "Total", footerFont, new BaseColor(255, 255, 255));
                AddListFooterItem(ref tableItems12, Element.ALIGN_RIGHT, item.Result.BMF.BMFOpenList.Sum(p => p.Closing).ToString("N2"));
                AddListFooterItem(ref tableItems12, Element.ALIGN_RIGHT, item.Result.BMF.BMFOpenList.Sum(p => p.Current).ToString("N2"));
                AddListFooterItem(ref tableItems12, Element.ALIGN_RIGHT, item.Result.BMF.BMFOpenList.Sum(p => p.ProfitLossPercentage).ToString("N2") + "%");
                AddListFooterItem(ref tableItems12, Element.ALIGN_RIGHT, item.Result.BMF.BMFOpenList.Sum(p => p.ProfitLoss).ToString("N2"));

            }

            doc.Add(tableItems12);


            AddSimpleHeader(ref doc, "BM&F: Pregão Atual");

            PdfPTable tableItems13 = new PdfPTable(12);
            tableItems13.WidthPercentage = 100;
            tableItems13.SpacingAfter = 30;

            //tableItems13.SetWidths(new[] { 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f });
            tableItems13.SplitRows = false;

            AddListHeader(ref tableItems13, new string[12] { "Mercadoria", "Nº Negócio", "Hora", "C/V", "After Market", "Quantidade", "Preço Negócio", "Valor Total Negócio", "Preço Atual", "Lucro/Prejuízo %", "Lucro/Prejuízo $", "Tipo" });

            even = true;

            if (item.Result.BMF != null && item.Result.BMF.BMFCurrentList != null)
            {

                foreach (var i in item.Result.BMF.BMFCurrentList)
                {
                    AddListItem(ref tableItems13, Element.ALIGN_LEFT, i.Commodity, even);
                    AddListItem(ref tableItems13, Element.ALIGN_LEFT, i.TradeNumber, even);
                    AddListItem(ref tableItems13, Element.ALIGN_LEFT, i.TradeHour, even);
                    AddListItem(ref tableItems13, Element.ALIGN_LEFT, i.OperationType, even);
                    AddListItem(ref tableItems13, Element.ALIGN_LEFT, i.AfterMarket, even);
                    AddListItem(ref tableItems13, Element.ALIGN_RIGHT, i.Quantity.ToString("N0"), even);
                    AddListItem(ref tableItems13, Element.ALIGN_RIGHT, i.Price.ToString("N2"), even);
                    AddListItem(ref tableItems13, Element.ALIGN_RIGHT, i.TotalPrice.ToString("N2"), even);
                    AddListItem(ref tableItems13, Element.ALIGN_RIGHT, i.CurrentPrice.ToString("N2"), even);
                    AddListItem(ref tableItems13, Element.ALIGN_RIGHT, i.ProfitLossPercentage.ToString("N2") + "%", even);
                    AddListItem(ref tableItems13, Element.ALIGN_RIGHT, i.ProfitLoss.ToString("N2"), even);
                    AddListItem(ref tableItems13, Element.ALIGN_LEFT, i.Type, even);
                    even = !even;
                }

                /*** RODAPÉ ***/
                tableItems13.AddCell(cellEmpty);
                tableItems13.AddCell(cellEmpty);
                tableItems13.AddCell(cellEmpty);
                tableItems13.AddCell(cellEmpty);
                AddListFooterItem(ref tableItems13, Element.ALIGN_RIGHT, "Total", footerFont, new BaseColor(255, 255, 255));
                AddListFooterItem(ref tableItems13, Element.ALIGN_RIGHT, item.Result.BMF.BMFCurrentList.Sum(p => p.Quantity).ToString("N0"));
                tableItems13.AddCell(cellEmpty);
                AddListFooterItem(ref tableItems13, Element.ALIGN_RIGHT, item.Result.BMF.BMFCurrentList.Sum(p => p.TotalPrice).ToString("N2"));
                AddListFooterItem(ref tableItems13, Element.ALIGN_RIGHT, item.Result.BMF.BMFCurrentList.Sum(p => p.CurrentPrice).ToString("N2"));
                AddListFooterItem(ref tableItems13, Element.ALIGN_RIGHT, item.Result.BMF.BMFCurrentList.Sum(p => p.ProfitLossPercentage).ToString("N2") + "%");
                AddListFooterItem(ref tableItems13, Element.ALIGN_RIGHT, item.Result.BMF.BMFCurrentList.Sum(p => p.ProfitLoss).ToString("N2"));
                tableItems13.AddCell(cellEmpty);

            }

            doc.Add(tableItems13);


            AddSimpleHeader(ref doc, "BM&F: Total (Posição de Abertura + Pregão Atual)");

            PdfPTable tableItems14 = new PdfPTable(3);
            tableItems14.WidthPercentage = 100;
            tableItems14.SpacingAfter = 30;

            //tableItems14.SetWidths(new[] { 10f, 10f, 10f });
            tableItems14.SplitRows = false;

            AddListHeader(ref tableItems14, new string[3] { "Mercadoria", "Quantidade", "Lucro/Prejuízo $" });

            even = true;

            if (item.Result.BMF != null && item.Result.BMF.BMFConsolidated != null)
            {

                foreach (var i in item.Result.BMF.BMFConsolidated)
                {
                    AddListItem(ref tableItems14, Element.ALIGN_LEFT, i.Commodity, even);
                    AddListItem(ref tableItems14, Element.ALIGN_RIGHT, i.Quantity.ToString("N0"), even);
                    AddListItem(ref tableItems14, Element.ALIGN_RIGHT, i.ProfitLoss.ToString("N2"), even);
                    even = !even;
                }

            }
            doc.Add(tableItems14);

            doc.NewPage();

            #endregion

            #region Gold - Ouro

            AddReportHeader(ref doc, "Posição Consolidada - Ouro", null, string.Empty, string.Empty, string.Empty, null, null, true, false, false);

            PdfPTable tableItems15 = new PdfPTable(5);
            tableItems15.WidthPercentage = 100;
            tableItems15.SpacingAfter = 30;

            //tableItems15.SetWidths(new[] { 8f, 10f, 6f, 6f, 9f, 9f, 9f, 9f, 9f, 9f, 9f, 10f, 11f, 10f, 11f, 11f, 7f, 11f });
            tableItems15.SplitRows = false;

            AddListHeader(ref tableItems15, new string[5] { "Qtde./Gramas", "Quantidade Padrão", "Valor Custódia", "Provisão Taxa de Custódia", "Provisão Mês Anterior" });

            even = true;

            if (item.Result.Gold != null)
            {

                foreach (var i in item.Result.Gold)
                {
                    AddListItem(ref tableItems15, Element.ALIGN_RIGHT, i.Quantity.ToString("N2"), even);
                    AddListItem(ref tableItems15, Element.ALIGN_RIGHT, i.DefaultQuantity.ToString("N2"), even);
                    AddListItem(ref tableItems15, Element.ALIGN_RIGHT, i.CustodyValue.ToString("N2"), even);
                    AddListItem(ref tableItems15, Element.ALIGN_RIGHT, i.ProvisionCustodyRatio.ToString("N2"), even);
                    AddListItem(ref tableItems15, Element.ALIGN_RIGHT, i.ProvisionLastMonth.ToString("N2"), even);
                    even = !even;
                }

            }

            doc.Add(tableItems15);

            doc.NewPage();

            #endregion

            #region BMFGuarantees - Garantias BM&F

            AddReportHeader(ref doc, "Posição Consolidada - Garantias BM&F", null, string.Empty, string.Empty, string.Empty, null, null, true, false, false);

            PdfPTable tableItems16 = new PdfPTable(8);
            tableItems16.WidthPercentage = 100;
            tableItems16.SpacingAfter = 30;

            //tableItems16.SetWidths(new[] { 8f, 10f, 6f, 6f, 9f, 9f, 9f, 9f, 9f, 9f, 9f, 10f, 11f, 10f, 11f, 11f, 7f, 11f });
            tableItems16.SplitRows = false;

            AddListHeader(ref tableItems16, new string[8] { "Total R$ Garantias", "Margem Requerida", "Fiança Própria", "Fiança Externa", "Dinheiro", "Ativos", "Custódia", "Ações" });

            even = true;

            if (item.Result.BMFGuarantees != null && item.Result.BMFGuarantees.OpeningPosition != null)
            {

                foreach (var i in item.Result.BMFGuarantees.OpeningPosition)
                {
                    AddListItem(ref tableItems16, Element.ALIGN_RIGHT, i.TotalGuarantees.ToString("N2"), even);
                    AddListItem(ref tableItems16, Element.ALIGN_RIGHT, i.RequiredMargin.ToString("N2"), even);
                    AddListItem(ref tableItems16, Element.ALIGN_RIGHT, i.OwnGuarantees.ToString("N2"), even);
                    AddListItem(ref tableItems16, Element.ALIGN_RIGHT, i.ExternalGuarantees.ToString("N2"), even);
                    AddListItem(ref tableItems16, Element.ALIGN_RIGHT, i.Money.ToString("N2"), even);
                    AddListItem(ref tableItems16, Element.ALIGN_RIGHT, i.Stocks.ToString("N2"), even);
                    AddListItem(ref tableItems16, Element.ALIGN_RIGHT, i.Custody.ToString("N2"), even);
                    AddListItem(ref tableItems16, Element.ALIGN_RIGHT, i.Assets.ToString("N2"), even);
                    even = !even;
                }
            }
            doc.Add(tableItems16);


            AddSimpleHeader(ref doc, "Composição Garantias");

            PdfPTable tableItems17 = new PdfPTable(7);
            tableItems17.WidthPercentage = 100;
            tableItems17.SpacingAfter = 30;

            //tableItems17.SetWidths(new[] { 8f, 10f, 6f, 6f, 9f, 9f, 9f, 9f, 9f, 9f, 9f, 10f, 11f, 10f, 11f, 11f, 7f, 11f });
            tableItems17.SplitRows = false;

            AddListHeader(ref tableItems17, new string[7] { "Ativo", "Nº Ativo", "Vencimento", "Quantidade", "Instituição Emitente", "Valor Garantia", "Guarda-chuva" });

            even = true;

            if (item.Result.BMFGuarantees != null && item.Result.BMFGuarantees.GuaranteesComposition != null)
            {

                foreach (var i in item.Result.BMFGuarantees.GuaranteesComposition)
                {
                    AddListItem(ref tableItems17, Element.ALIGN_LEFT, i.Stock, even);
                    AddListItem(ref tableItems17, Element.ALIGN_LEFT, i.StockNumber.ToString(), even);
                    AddListItem(ref tableItems17, Element.ALIGN_CENTER, i.DueDate, even);
                    AddListItem(ref tableItems17, Element.ALIGN_RIGHT, i.Quantity.ToString("N0"), even);
                    AddListItem(ref tableItems17, Element.ALIGN_LEFT, i.InstitutionIssuer, even);
                    AddListItem(ref tableItems17, Element.ALIGN_RIGHT, i.Value.ToString("N2"), even);
                    AddListItem(ref tableItems17, Element.ALIGN_LEFT, i.Umbrella, even);
                    even = !even;
                }
            }
            doc.Add(tableItems17);

            doc.NewPage();

            #endregion

            #region CDBFinal - CDB Final

            AddReportHeader(ref doc, "Posição Consolidada - CDB Final", null, string.Empty, string.Empty, string.Empty, null, null, true, false, false);

            PdfPTable tableItems18 = new PdfPTable(10);
            tableItems18.WidthPercentage = 100;
            tableItems18.SpacingAfter = 30;

            //tableItems18.SetWidths(new[] { 8f, 10f, 6f, 6f, 9f, 9f, 9f, 9f, 9f, 9f, 9f, 10f, 11f, 10f, 11f, 11f, 7f, 11f });
            tableItems18.SplitRows = false;

            AddListHeader(ref tableItems18, new string[10] { "Título", "Emissão", "Vencimento", "Taxa", "Operação", "Resgate", "Qtde.", "Total", "IR/IOF", "Valor Líquido" });

            even = true;

            if (item.Result.CDBFinal != null)
            {

                foreach (var i in item.Result.CDBFinal)
                {
                    AddListItem(ref tableItems18, Element.ALIGN_LEFT, i.Title, even);
                    AddListItem(ref tableItems18, Element.ALIGN_CENTER, i.IssueDate, even);
                    AddListItem(ref tableItems18, Element.ALIGN_CENTER, i.DueDate, even);
                    AddListItem(ref tableItems18, Element.ALIGN_RIGHT, i.Ratio.ToString("N2"), even);
                    AddListItem(ref tableItems18, Element.ALIGN_CENTER, i.OperationDate, even);
                    AddListItem(ref tableItems18, Element.ALIGN_CENTER, i.RedemptionDate, even);
                    AddListItem(ref tableItems18, Element.ALIGN_RIGHT, i.Quantity.ToString("N0"), even);
                    AddListItem(ref tableItems18, Element.ALIGN_RIGHT, i.GrossValue.ToString("N2"), even);
                    AddListItem(ref tableItems18, Element.ALIGN_RIGHT, i.IRIOF.ToString("N2"), even);
                    AddListItem(ref tableItems18, Element.ALIGN_RIGHT, i.NetValue.ToString("N2"), even);
                    even = !even;
                }
            }
            doc.Add(tableItems18);

            doc.NewPage();

            #endregion

            #region CommitedCDB - Compromissada em CDB

            AddReportHeader(ref doc, "Posição Consolidada - Compromissada em CDB", null, string.Empty, string.Empty, string.Empty, null, null, true, false, false);

            PdfPTable tableItems19 = new PdfPTable(10);
            tableItems19.WidthPercentage = 100;
            tableItems19.SpacingAfter = 30;

            //tableItems19.SetWidths(new[] { 8f, 10f, 6f, 6f, 9f, 9f, 9f, 9f, 9f, 9f, 9f, 10f, 11f, 10f, 11f, 11f, 7f, 11f });
            tableItems19.SplitRows = false;

            AddListHeader(ref tableItems19, new string[10] { "Título", "Emissão", "Vencimento", "Taxa", "Operação", "Resgate", "Qtde.", "Valor Bruto", "IR/IOF", "Valor Líquido" });

            even = true;

            if (item.Result.CommitedCDB != null)
            {

                foreach (var i in item.Result.CommitedCDB)
                {
                    AddListItem(ref tableItems19, Element.ALIGN_LEFT, i.Title, even);
                    AddListItem(ref tableItems19, Element.ALIGN_CENTER, i.IssueDate, even);
                    AddListItem(ref tableItems19, Element.ALIGN_CENTER, i.DueDate, even);
                    AddListItem(ref tableItems19, Element.ALIGN_RIGHT, i.Ratio.ToString("N2"), even);
                    AddListItem(ref tableItems19, Element.ALIGN_CENTER, i.OperationDate, even);
                    AddListItem(ref tableItems19, Element.ALIGN_CENTER, i.RedemptionDate, even);
                    AddListItem(ref tableItems19, Element.ALIGN_RIGHT, i.Quantity.ToString("N0"), even);
                    AddListItem(ref tableItems19, Element.ALIGN_RIGHT, i.GrossValue.ToString("N2"), even);
                    AddListItem(ref tableItems19, Element.ALIGN_RIGHT, i.IRIOF.ToString("N2"), even);
                    AddListItem(ref tableItems19, Element.ALIGN_RIGHT, i.NetValue.ToString("N2"), even);
                    even = !even;
                }

            }

            doc.Add(tableItems19);

            doc.NewPage();

            #endregion

            #region PublicTitles - Títulos Públicos

            AddReportHeader(ref doc, "Posição Consolidada - Títulos Públicos", null, string.Empty, string.Empty, string.Empty, null, null, true, false, false);

            PdfPTable tableItems20 = new PdfPTable(5);
            tableItems20.WidthPercentage = 100;
            tableItems20.SpacingAfter = 30;

            //tableItems20.SetWidths(new[] { 10f, 10f, 10f, 10f, 10f, 10f });
            tableItems20.SplitRows = false;

            AddListHeader(ref tableItems20, new string[5] { "Título", "Quantidade", "Vencimento", "Valor Bruto", "Valor Líquido" });

            even = true;

            if (item.Result.PublicTitles != null)
            {

                foreach (var i in item.Result.PublicTitles)
                {
                    AddListItem(ref tableItems20, Element.ALIGN_LEFT, i.Title, even);
                    AddListItem(ref tableItems20, Element.ALIGN_RIGHT, i.Quantity.ToString("N0"), even);
                    AddListItem(ref tableItems20, Element.ALIGN_CENTER, i.DueDate, even);
                    AddListItem(ref tableItems20, Element.ALIGN_RIGHT, i.GrossValue.ToString("N2"), even);
                    AddListItem(ref tableItems20, Element.ALIGN_RIGHT, i.NetValue.ToString("N2"), even);
                    even = !even;
                }

                /*** RODAPÉ ***/
                AddListFooterItem(ref tableItems20, Element.ALIGN_RIGHT, "Total", footerFont, new BaseColor(255, 255, 255));
                AddListFooterItem(ref tableItems20, Element.ALIGN_RIGHT, item.Result.PublicTitles.Sum(p => p.Quantity).ToString("N0"));
                tableItems20.AddCell(cellEmpty);
                AddListFooterItem(ref tableItems20, Element.ALIGN_RIGHT, item.Result.PublicTitles.Sum(p => p.GrossValue).ToString("N2"));
                AddListFooterItem(ref tableItems20, Element.ALIGN_RIGHT, item.Result.PublicTitles.Sum(p => p.NetValue).ToString("N2"));
            }
            doc.Add(tableItems20);

            doc.NewPage();

            #endregion

            #region Clubs - Clubes

            AddReportHeader(ref doc, "Posição Consolidada - Clubes", null, string.Empty, string.Empty, string.Empty, null, null, true, false, false);

            PdfPTable tableItems21 = new PdfPTable(8);
            tableItems21.WidthPercentage = 100;
            tableItems21.SpacingAfter = 30;

            //tableItems21.SetWidths(new[] { 10f, 10f, 10f, 10f, 10f, 10f });
            tableItems21.SplitRows = false;

            AddListHeader(ref tableItems21, new string[8] { "Fundo/Clube", "Aplicação", "Qtde. Quota", "Valor da Quota", "Valor Bruto", "IR", "IOF", "Valor Líquido" });

            even = true;

            if (item.Result.Clubs != null)
            {

                foreach (var i in item.Result.Clubs)
                {
                    AddListItem(ref tableItems21, Element.ALIGN_LEFT, i.ClubFund, even);
                    AddListItem(ref tableItems21, Element.ALIGN_CENTER, i.ApplicationDate, even);
                    AddListItem(ref tableItems21, Element.ALIGN_RIGHT, i.QuotaQuantity.ToString("N0"), even);
                    AddListItem(ref tableItems21, Element.ALIGN_RIGHT, i.QuotaValue.ToString("N2"), even);
                    AddListItem(ref tableItems21, Element.ALIGN_RIGHT, i.GrossValue.ToString("N2"), even);
                    AddListItem(ref tableItems21, Element.ALIGN_RIGHT, i.IR.ToString("N2"), even);
                    AddListItem(ref tableItems21, Element.ALIGN_RIGHT, i.IOF.ToString("N2"), even);
                    AddListItem(ref tableItems21, Element.ALIGN_RIGHT, i.NetValue.ToString("N2"), even);
                    even = !even;
                }

                /*** RODAPÉ ***/
                tableItems21.AddCell(cellEmpty);
                AddListFooterItem(ref tableItems21, Element.ALIGN_RIGHT, "Total", footerFont, new BaseColor(255, 255, 255));
                AddListFooterItem(ref tableItems21, Element.ALIGN_RIGHT, item.Result.Clubs.Sum(p => p.QuotaQuantity).ToString("N0"));
                AddListFooterItem(ref tableItems21, Element.ALIGN_RIGHT, item.Result.Clubs.Sum(p => p.QuotaValue).ToString("N2"));
                AddListFooterItem(ref tableItems21, Element.ALIGN_RIGHT, item.Result.Clubs.Sum(p => p.GrossValue).ToString("N2"));
                AddListFooterItem(ref tableItems21, Element.ALIGN_RIGHT, item.Result.Clubs.Sum(p => p.IR).ToString("N2"));
                AddListFooterItem(ref tableItems21, Element.ALIGN_RIGHT, item.Result.Clubs.Sum(p => p.IOF).ToString("N2"));
                tableItems21.AddCell(cellEmpty);
            }
            doc.Add(tableItems21);

            doc.NewPage();

            #endregion

            #region TotalCheckingAccount - Total Conta Corrente

            AddReportHeader(ref doc, "Posição Consolidada - Total Conta Corrente", null, string.Empty, string.Empty, string.Empty, null, null, true, false, false);

            PdfPTable tableItems22 = new PdfPTable(7);
            tableItems22.WidthPercentage = 100;
            tableItems22.SpacingAfter = 30;

            //tableItems22.SetWidths(new[] { 10f, 10f, 10f, 10f, 10f, 10f });
            tableItems22.SplitRows = false;

            AddListHeader(ref tableItems22, new string[7] { "Valor Disponível", "Valor Projetado", "Valor Total (Disponível + Projetado)", "Projetado Pregão", "Total (incluindo Projetado Pregão)", "Dias Devedor Mês", "Dias Devedor Total" });

            even = true;

            if (item.Result.TotalCheckingAccount != null)
            {

                var balance = item.Result.TotalCheckingAccount.CheckingAccountBalance;
                //foreach (var i in item.Result.TotalCheckingAccount.CheckingAccountBalance)
                //{
                AddListItem(ref tableItems22, Element.ALIGN_RIGHT, balance.AmountAvailable.ToString("N2"), even);
                AddListItem(ref tableItems22, Element.ALIGN_RIGHT, balance.AmountProjected.ToString("N2"), even);
                AddListItem(ref tableItems22, Element.ALIGN_RIGHT, balance.AmountTotal.ToString("N2"), even);
                AddListItem(ref tableItems22, Element.ALIGN_RIGHT, balance.TradingProjected.ToString("N2"), even);
                AddListItem(ref tableItems22, Element.ALIGN_RIGHT, balance.Total.ToString("N2"), even);
                AddListItem(ref tableItems22, Element.ALIGN_LEFT, balance.DebtDaysMounth.ToString(), even);
                AddListItem(ref tableItems22, Element.ALIGN_LEFT, balance.DebtDaysTotal.ToString(), even);
                //    even = !even;
                //}
            }
            doc.Add(tableItems22);


            AddSimpleHeader(ref doc, "Descrição Projetada");

            PdfPTable tableItems23 = new PdfPTable(4);
            tableItems23.WidthPercentage = 100;
            tableItems23.SpacingAfter = 30;

            //tableItems23.SetWidths(new[] { 10f, 10f, 10f, 10f, 10f, 10f });
            tableItems23.SplitRows = false;

            AddListHeader(ref tableItems23, new string[4] { "Data de Liquidação", "Valor Projetado", "Valor Projetado (Pregão Atual)", "Total" });

            even = true;

            if (item.Result.TotalCheckingAccount != null && item.Result.TotalCheckingAccount.DescriptionProjected != null)
            {

                var projected = item.Result.TotalCheckingAccount.DescriptionProjected;
                //foreach (var i in item.Result.TotalCheckingAccount.CheckingAccountBalance)
                //{
                AddListItem(ref tableItems23, Element.ALIGN_CENTER, projected.SettlementDate, even);
                AddListItem(ref tableItems23, Element.ALIGN_RIGHT, projected.AmountProjected.ToString("N2"), even);
                AddListItem(ref tableItems23, Element.ALIGN_RIGHT, projected.ProjectedAmountCurrentTrading.ToString("N2"), even);
                AddListItem(ref tableItems23, Element.ALIGN_RIGHT, projected.Total.ToString("N2"), even);
                //    even = !even;
                //}
            }
            doc.Add(tableItems23);

            doc.NewPage();

            #endregion

            doc.Close();

            return memStream;
        }

        public MemoryStream CreatePrivateReportToMemoryStream(List<Private> list, string refDate)
        {
            iTextSharp.text.Document doc = new iTextSharp.text.Document(new Rectangle(1371, 970), 25, 25, 25, 45);
            MemoryStream memStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
            writer.CloseStream = false;

            writer.PageEvent = new PrintHeaderFooter { Title = "PrivateReport" };

            doc.Open();

            Font tradeFont = FontFactory.GetFont("Arial", 30, Font.BOLD, new BaseColor(0, 135, 169));
            Font companyAddressFont = FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(111, 111, 111));
            Font dateFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(111, 111, 111));
            Font lineFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(111, 111, 111));
            Font lineHeaderFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(255, 255, 255));

            AddReportHeader(ref doc, "Relatório Private", null, string.Empty, string.Empty, string.Empty);

            //PdfPTable tableSubscriptionHeader = new PdfPTable(1);
            //tableSubscriptionHeader.WidthPercentage = 100;
            //tableSubscriptionHeader.HorizontalAlignment = 0;
            //tableSubscriptionHeader.SpacingBefore = 20;
            //tableSubscriptionHeader.SpacingAfter = 30;

            //PdfPCell cellTitulo = new PdfPCell();
            //cellTitulo.BorderWidth = 0;
            //cellTitulo.PaddingBottom = 5;
            //Font fontTitle = FontFactory.GetFont("Arial", 16, Font.BOLD, new BaseColor(0, 135, 169));

            //cellTitulo.Phrase = new Phrase("Referência: " + refDate, fontTitle);
            //tableSubscriptionHeader.AddCell(cellTitulo);

            //doc.Add(tableSubscriptionHeader);

            if (list.Count > 0)
            {
                PdfPTable tableItems = new PdfPTable(9);
                tableItems.WidthPercentage = 100;
                var colWidthPercentages = new[] { 8f, 20f, 10f, 8f, 8f, 12f, 12f, 12f, 8f };
                tableItems.SetWidths(colWidthPercentages);
                tableItems.SplitRows = false;

                string[] headerItems = new string[9] { "CPF/CGC", "Cliente", "Produto", "Corretagem", "Posição", "Disponível", "Captação", "Aplicação", "Resgate" };
                AddListHeader(ref tableItems, headerItems);

                bool even = true;
                foreach (Private i in list)
                {
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.CPFCGC.ToString(), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.ClientName, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.Product, even);

                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.BrokerageValue.HasValue ? i.BrokerageValue.Value.ToString("N2") : "-", even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.PositionValue.HasValue ? i.PositionValue.Value.ToString("N2") : "-", even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.AvailableValue.HasValue ? i.AvailableValue.Value.ToString("N2") : "-", even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.CaptationValue.HasValue ? i.CaptationValue.Value.ToString("N2") : "-", even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.ApplicationValue.HasValue ? i.ApplicationValue.Value.ToString("N2") : "-", even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.RedemptionValue.HasValue ? i.RedemptionValue.Value.ToString("N2") : "-", even);

                    even = !even;
                }
                doc.Add(tableItems);
            }

            doc.Close();

            return memStream;
        }

        #endregion

        #region Financial

        public MemoryStream CreateRedemptionRequestItemsToMemoryStream(List<RedemptionRequestListItem> list)
        {
            iTextSharp.text.Document doc = new iTextSharp.text.Document(new Rectangle(1371, 970), 25, 25, 25, 45);
            MemoryStream memStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
            writer.CloseStream = false;

            writer.PageEvent = new PrintHeaderFooter { Title = "RedemptionRequestItem" };

            doc.Open();

            Font tradeFont = FontFactory.GetFont("Arial", 30, Font.BOLD, new BaseColor(0, 135, 169));
            Font companyAddressFont = FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(111, 111, 111));
            Font dateFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(111, 111, 111));
            Font lineFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(111, 111, 111));
            Font lineHeaderFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(255, 255, 255));

            AddReportHeader(ref doc, "Solicitação de Resgate", null, string.Empty, string.Empty, string.Empty, null, null);

            if (list.Count > 0)
            {
                PdfPTable tableItems = new PdfPTable(8);
                tableItems.WidthPercentage = 100;
                var colWidthPercentages = new[] { 12f, 12f, 12f, 12f, 12f, 12f, 12f, 12f };
                tableItems.SetWidths(colWidthPercentages);
                tableItems.SplitRows = false;

                Font headerFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(89, 89, 89));

                string[] headerItems = new string[6] { "Solicitado em", "Código", "Cliente", "Destino", "Valor", "Status" };
                int[] headerRowspans = new int[6] { 2, 2, 2, 0, 2, 2 };
                int[] headerColspans = new int[6] { 0, 0, 0, 3, 0, 0 };
                AddListHeader(ref tableItems, headerItems, headerRowspans, headerColspans);

                string[] headerItems1 = new string[3] { "Banco", "Agência", "Conta" };
                int[] headerRowspans1 = new int[3] { 0, 0, 0 };
                int[] headerColspans1 = new int[3] { 0, 0, 0 };
                AddListHeader(ref tableItems, headerItems1, headerRowspans1, headerColspans1);

                bool even = true;
                foreach (RedemptionRequestListItem i in list)
                {
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.RequestedDate, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.ClientCode, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.ClientName, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.Bank, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.Agency, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.Account, even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.Amount, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.StatusDescription, even);
                    even = !even;
                }
                doc.Add(tableItems);
            }

            doc.Close();

            return memStream;
        }

        public MemoryStream CreateDailyFinancialReportToMemoryStream(List<DailyFinancial> list)
        {
            iTextSharp.text.Document doc = new iTextSharp.text.Document(new Rectangle(1371, 970), 25, 25, 25, 45);
            MemoryStream memStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
            writer.CloseStream = false;

            writer.PageEvent = new PrintHeaderFooter { Title = "DailyFinancialReportItem" };

            doc.Open();

            Font tradeFont = FontFactory.GetFont("Arial", 30, Font.BOLD, new BaseColor(0, 135, 169));
            Font companyAddressFont = FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(111, 111, 111));
            Font dateFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(111, 111, 111));
            Font lineFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(111, 111, 111));
            Font lineHeaderFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(255, 255, 255));

            AddReportHeader(ref doc, "Relatório Financeiro Diário", null, string.Empty, string.Empty, string.Empty, null, null);

            if (list.Count > 0)
            {
                PdfPTable tableItems = new PdfPTable(12);
                tableItems.WidthPercentage = 100;
                var colWidthPercentages = new[] { 6, 9, 15, 10, 10, 10, 10, 10, 10, 10, 10, 12 };
                tableItems.SetWidths(colWidthPercentages);
                tableItems.SplitRows = false;

                Font headerFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(89, 89, 89));

                string[] headerItems = new string[12] { "Código", "Cliente", "Assessor", "Saldo Anterior", "Disponível C/C", "Crédito", "Débito", "Liquidado", "Saldo Atual", "Projetado", "Saldo Final", "Taxa de Custódia" };
                AddListHeader(ref tableItems, headerItems);

                bool even = true;
                foreach (DailyFinancial i in list)
                {
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.ClientID.ToString() + "-" + i.DV.ToString(), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.ClientName, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.AssessorName, even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.AnteriorBalance.ToString("N"), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.AccountBalance.ToString("N"), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.Credit.ToString("N"), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.Debt.ToString("N"), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.Finished.ToString("N"), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.LiquidResult.ToString("N"), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.ProjectedBalance.ToString("N"), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.FinalBalance.ToString("N"), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, (i.CustodyTax.HasValue) ? "Sim" : "Não", even);
                    even = !even;
                }
                doc.Add(tableItems);
            }

            doc.Close();

            return memStream;
        }

        #endregion

        #region OnlinePortfolio

        public MemoryStream CreateAdminClientsToMemoryStream(List<PortfolioClientListItem> adminClients)
        {
            iTextSharp.text.Document doc = new iTextSharp.text.Document(new Rectangle(970, 1371), 25, 25, 25, 45);
            MemoryStream memStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
            writer.CloseStream = false;

            writer.PageEvent = new PrintHeaderFooter { Title = "OnlinePortfolio" };

            doc.Open();

            Font tradeFont = FontFactory.GetFont("Arial", 30, Font.BOLD, new BaseColor(0, 135, 169));
            Font companyAddressFont = FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(111, 111, 111));
            Font dateFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(111, 111, 111));
            Font lineFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(111, 111, 111));
            Font lineHeaderFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(255, 255, 255));

            AddReportHeader(ref doc, "Carteira Online - Clientes", null, string.Empty, string.Empty, string.Empty, null, null);

            if (adminClients.Count > 0)
            {
                PdfPTable tableItems = new PdfPTable(4);
                tableItems.WidthPercentage = 100;
                tableItems.HorizontalAlignment = Element.ALIGN_LEFT;
                var colWidthPercentages = new[] { 100f, 400f, 270f, 150f };
                tableItems.SetWidths(colWidthPercentages);
                tableItems.SplitLate = false;
                tableItems.SplitRows = true;
                tableItems.SpacingBefore = 25;

                string[] headerItems = new string[4] { "Código", "Nome", "Operador", "Apuração" };
                AddListHeader(ref tableItems, headerItems);

                bool even = true;
                foreach (PortfolioClientListItem i in adminClients)
                {
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.ClientCode.ToString(), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.ClientName, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.Operators, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.CalculationDescription, even);


                    PdfPCell cell = new PdfPCell();
                    cell.BorderWidthBottom = cell.BorderWidthLeft = cell.BorderWidthRight = cell.BorderWidthTop = 1;
                    cell.BorderColor = new BaseColor(255, 255, 255);
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.PaddingLeft = cell.PaddingBottom = 7;
                    cell.PaddingTop = 3;
                    cell.Colspan = 7;
                    cell.BackgroundColor = even ? new BaseColor(238, 239, 239) : new BaseColor(230, 231, 231);
                    even = !even;
                }
                doc.Add(tableItems);
            }

            doc.Close();

            return memStream;
        }

        #endregion

        #region Proceeds

        public MemoryStream CreateProceedsToMemoryStream(List<QX3.Portal.Contracts.DataContracts.Proceeds> list, string groupBy)
        {
            iTextSharp.text.Document doc = new iTextSharp.text.Document(new Rectangle(970, 1371).Rotate(), 25, 25, 25, 45);
            MemoryStream memStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
            writer.CloseStream = false;

            writer.PageEvent = new PrintHeaderFooter { Title = "Proceeds" };

            doc.Open();

            Font font = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(111, 111, 111));

            AddReportHeader(ref doc, "Proventos", null, string.Empty, string.Empty, string.Empty, null, null);

            List<string> groupItems = new List<string>();

            switch (groupBy)
            {
                case "C":
                    groupItems = (from l in list select l.ClientCode.ToString()).Distinct().ToList();
                    break;
                case "A":
                    groupItems = (from l in list select l.AssessorCode.ToString()).Distinct().ToList();
                    break;
                case "P":
                    groupItems = (from l in list select l.StockCode).Distinct().ToList();
                    break;
            }

            if (groupItems != null && groupItems.Count > 0)
            {
                List<QX3.Portal.Contracts.DataContracts.Proceeds> subGroupItems = new List<QX3.Portal.Contracts.DataContracts.Proceeds>();

                switch (groupBy)
                {
                    case "C":
                        foreach (string code in groupItems)
                        {
                            PdfPTable tableItems = new PdfPTable(12);
                            tableItems.WidthPercentage = 100;
                            tableItems.HorizontalAlignment = Element.ALIGN_LEFT;
                            var colWidthPercentages = new[] { 70f, 100f, 50f, 210f, 70f, 70f, 70f, 70f, 70f, 70f, 70f, 70f };
                            tableItems.SetWidths(colWidthPercentages);
                            tableItems.SplitRows = false;
                            tableItems.KeepTogether = true;
                            tableItems.SpacingBefore = 15;
                            tableItems.SpacingAfter = 15;

                            subGroupItems = (from g in list where g.ClientCode.Equals(Int32.Parse(code)) select g).ToList();
                            var item = subGroupItems[0];
                            Paragraph itemHeader = new Paragraph();
                            itemHeader.Add(new Phrase("Cód. Cliente: " + item.ClientCode, font));
                            itemHeader.Add(Chunk.NEWLINE);
                            itemHeader.Add(new Phrase("Nome do Cliente: " + item.ClientName, font));

                            PdfPCell cell = new PdfPCell(itemHeader);
                            cell.Colspan = 12;
                            cell.Border = 0;
                            cell.PaddingBottom = 10;
                            tableItems.AddCell(cell);

                            string[] headerItems = new string[12] { "Papel", "ISIN", "Cod.", "Assessor", "Provento", "Qtde.", "Valor Bruto", "% IR", "Valor IR", "Valor Líq.", "Data Pgto.", "Carteira" };
                            AddListHeader(ref tableItems, headerItems);

                            bool even = true;

                            foreach (QX3.Portal.Contracts.DataContracts.Proceeds p in subGroupItems)
                            {
                                AddListItem(ref tableItems, Element.ALIGN_LEFT, p.StockCode, even);
                                AddListItem(ref tableItems, Element.ALIGN_LEFT, p.ISIN, even);
                                AddListItem(ref tableItems, Element.ALIGN_LEFT, p.AssessorCode.ToString(), even);
                                AddListItem(ref tableItems, Element.ALIGN_LEFT, p.AssessorName, even);
                                AddListItem(ref tableItems, Element.ALIGN_LEFT, p.Proceed, even);
                                AddListItem(ref tableItems, Element.ALIGN_RIGHT, p.Quantity.ToString("N4") + "%", even);
                                AddListItem(ref tableItems, Element.ALIGN_RIGHT, p.GrossValue.ToString("N2"), even);
                                AddListItem(ref tableItems, Element.ALIGN_RIGHT, p.IR.ToString("N2") + "%", even);
                                AddListItem(ref tableItems, Element.ALIGN_RIGHT, p.IRValue.ToString("N2"), even);
                                AddListItem(ref tableItems, Element.ALIGN_RIGHT, p.NetValue.ToString("N2"), even);
                                AddListItem(ref tableItems, Element.ALIGN_LEFT, p.PaymentDate.ToString("dd/MM/yyyy"), even);
                                AddListItem(ref tableItems, Element.ALIGN_LEFT, p.Portfolio.ToString(), even);
                                even = !even;
                            }
                            doc.Add(tableItems);

                        }
                        break;
                    case "A":
                        foreach (string code in groupItems)
                        {
                            PdfPTable tableItems = new PdfPTable(12);
                            tableItems.WidthPercentage = 100;
                            tableItems.HorizontalAlignment = Element.ALIGN_LEFT;
                            var colWidthPercentages = new[] { 70f, 100f, 50f, 210f, 70f, 70f, 70f, 70f, 70f, 70f, 70f, 70f };
                            tableItems.SetWidths(colWidthPercentages);
                            tableItems.SplitRows = false;
                            tableItems.KeepTogether = true;
                            tableItems.SpacingBefore = 15;
                            tableItems.SpacingAfter = 15;

                            subGroupItems = (from g in list where g.AssessorCode.Equals(Int32.Parse(code)) select g).ToList();
                            var item = subGroupItems[0];

                            Paragraph itemHeader = new Paragraph();
                            itemHeader.Add(new Phrase("Cód. Assessor: " + item.AssessorCode, font));
                            itemHeader.Add(Chunk.NEWLINE);
                            itemHeader.Add(new Phrase("Nome do Assessor: " + item.AssessorName, font));
                            itemHeader.SpacingAfter = 10;

                            PdfPCell cell = new PdfPCell(itemHeader);
                            cell.Colspan = 12;
                            cell.Border = 0;
                            cell.PaddingBottom = 10;
                            tableItems.AddCell(cell);

                            string[] headerItems = new string[12] { "Papel", "ISIN", "Cod. Clie.", "Nome do Cliente", "Provento", "Qtde.", "Valor Bruto", "% IR", "Valor IR", "Valor Líq.", "Data Pgto.", "Carteira" };
                            AddListHeader(ref tableItems, headerItems);

                            bool even = true;

                            foreach (QX3.Portal.Contracts.DataContracts.Proceeds p in subGroupItems)
                            {
                                AddListItem(ref tableItems, Element.ALIGN_LEFT, p.StockCode, even);
                                AddListItem(ref tableItems, Element.ALIGN_LEFT, p.ISIN, even);
                                AddListItem(ref tableItems, Element.ALIGN_LEFT, p.ClientCode.ToString(), even);
                                AddListItem(ref tableItems, Element.ALIGN_LEFT, p.ClientName, even);
                                AddListItem(ref tableItems, Element.ALIGN_LEFT, p.Proceed, even);
                                AddListItem(ref tableItems, Element.ALIGN_RIGHT, p.Quantity.ToString("N4") + "%", even);
                                AddListItem(ref tableItems, Element.ALIGN_RIGHT, p.GrossValue.ToString("N2"), even);
                                AddListItem(ref tableItems, Element.ALIGN_RIGHT, p.IR.ToString("N2") + "%", even);
                                AddListItem(ref tableItems, Element.ALIGN_RIGHT, p.IRValue.ToString("N2"), even);
                                AddListItem(ref tableItems, Element.ALIGN_RIGHT, p.NetValue.ToString("N2"), even);
                                AddListItem(ref tableItems, Element.ALIGN_LEFT, p.PaymentDate.ToString("dd/MM/yyyy"), even);
                                AddListItem(ref tableItems, Element.ALIGN_LEFT, p.Portfolio.ToString(), even);
                                even = !even;
                            }
                            doc.Add(tableItems);

                        }
                        break;
                    case "P":
                        foreach (string code in groupItems)
                        {
                            PdfPTable tableItems = new PdfPTable(12);
                            tableItems.WidthPercentage = 100;
                            tableItems.HorizontalAlignment = Element.ALIGN_LEFT;
                            var colWidthPercentages = new[] { 50f, 210f, 60f, 210f, 70f, 70f, 70f, 70f, 70f, 70f, 70f, 60f };
                            tableItems.SetWidths(colWidthPercentages);
                            tableItems.SplitRows = false;
                            tableItems.KeepTogether = true;
                            tableItems.SpacingBefore = 15;
                            tableItems.SpacingAfter = 15;

                            subGroupItems = (from g in list where g.StockCode.Equals(code) select g).ToList();
                            var item = subGroupItems[0];

                            Paragraph itemHeader = new Paragraph();
                            itemHeader.Add(new Phrase("Papel: " + item.StockCode, font));
                            itemHeader.Add(Chunk.NEWLINE);
                            itemHeader.Add(new Phrase("ISIN: " + item.ISIN, font));
                            itemHeader.SpacingAfter = 10;

                            PdfPCell cell = new PdfPCell(itemHeader);
                            cell.Colspan = 12;
                            cell.Border = 0;
                            cell.PaddingBottom = 10;
                            tableItems.AddCell(cell);

                            string[] headerItems = new string[12] { "Cod.", "Assessor", "Cod. Clie.", "Nome do Cliente", "Provento", "Qtde.", "Valor Bruto", "% IR", "Valor IR", "Valor Líq.", "Data Pgto.", "Carteira" };
                            AddListHeader(ref tableItems, headerItems);

                            bool even = true;

                            foreach (QX3.Portal.Contracts.DataContracts.Proceeds p in subGroupItems)
                            {
                                AddListItem(ref tableItems, Element.ALIGN_LEFT, p.AssessorCode.ToString(), even);
                                AddListItem(ref tableItems, Element.ALIGN_LEFT, p.AssessorName, even);
                                AddListItem(ref tableItems, Element.ALIGN_LEFT, p.ClientCode.ToString(), even);
                                AddListItem(ref tableItems, Element.ALIGN_LEFT, p.ClientName, even);
                                AddListItem(ref tableItems, Element.ALIGN_LEFT, p.Proceed, even);
                                AddListItem(ref tableItems, Element.ALIGN_RIGHT, p.Quantity.ToString("N4") + "%", even);
                                AddListItem(ref tableItems, Element.ALIGN_RIGHT, p.GrossValue.ToString("N2"), even);
                                AddListItem(ref tableItems, Element.ALIGN_RIGHT, p.IR.ToString("N2") + "%", even);
                                AddListItem(ref tableItems, Element.ALIGN_RIGHT, p.IRValue.ToString("N2"), even);
                                AddListItem(ref tableItems, Element.ALIGN_RIGHT, p.NetValue.ToString("N2"), even);
                                AddListItem(ref tableItems, Element.ALIGN_LEFT, p.PaymentDate.ToString("dd/MM/yyyy"), even);
                                AddListItem(ref tableItems, Element.ALIGN_LEFT, p.Portfolio.ToString(), even);
                                even = !even;
                            }
                            doc.Add(tableItems);

                        }
                        break;
                }
            }
            doc.Close();

            return memStream;
        }

        #endregion

        #region Term

        public MemoryStream CreateTermToMemoryStream(List<TermListItem> list, string groupBy)
        {
            iTextSharp.text.Document doc = new iTextSharp.text.Document(new Rectangle(970, 1371).Rotate(), 25, 25, 25, 45);
            MemoryStream memStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
            writer.CloseStream = false;

            writer.PageEvent = new PrintHeaderFooter { Title = "Term" };

            doc.Open();

            Font font = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(111, 111, 111));

            AddReportHeader(ref doc, "Termo", null, string.Empty, string.Empty, string.Empty, null, null);

            List<string> groupItems = new List<string>();

            switch (groupBy)
            {
                case "C":
                    groupItems = (from l in list select l.Code).Distinct().ToList();
                    break;
                case "A":
                    groupItems = (from l in list select l.Assessor).Distinct().ToList();
                    break;
                case "P":
                    groupItems = (from l in list select l.Stock).Distinct().ToList();
                    break;
            }

            if (groupItems != null && groupItems.Count > 0)
            {
                List<TermListItem> subGroupItems = new List<TermListItem>();

                switch (groupBy)
                {
                    case "C":
                        foreach (string code in groupItems)
                        {
                            PdfPTable tableItems = new PdfPTable(11);
                            tableItems.WidthPercentage = 100;
                            tableItems.HorizontalAlignment = Element.ALIGN_LEFT;
                            var colWidthPercentages = new[] { 200f, 90f, 90f, 90f, 80f, 80f, 80f, 90f, 90f, 90f, 90f };
                            tableItems.SetWidths(colWidthPercentages);
                            tableItems.SplitRows = false;
                            //tableItems.KeepTogether = true;
                            tableItems.SpacingBefore = 15;
                            tableItems.SpacingAfter = 15;

                            subGroupItems = (from g in list where g.Code.Equals(code) select g).ToList();
                            var item = subGroupItems[0];
                            Paragraph itemHeader = new Paragraph();
                            itemHeader.Add(new Phrase("Código: " + item.Code, font));
                            itemHeader.Add(Chunk.NEWLINE);
                            itemHeader.Add(new Phrase("Nome do Cliente: " + item.Client, font));

                            PdfPCell cell = new PdfPCell(itemHeader);
                            cell.Colspan = 11;
                            cell.Border = 0;
                            cell.PaddingBottom = 10;
                            tableItems.AddCell(cell);

                            string[] headerItems = new string[11] { "Assessor", "Papel", "Empresa", "Contrato", "Qtde. Disponível", "Qtde. Total", "Qtde. a Liquidar", "Tipo", "Data", "Situação", "Data de Rolagem" };
                            AddListHeader(ref tableItems, headerItems);

                            bool even = true;

                            foreach (TermListItem p in subGroupItems)
                            {
                                AddListItem(ref tableItems, Element.ALIGN_LEFT, p.Assessor, even);
                                AddListItem(ref tableItems, Element.ALIGN_LEFT, p.Stock, even);
                                AddListItem(ref tableItems, Element.ALIGN_LEFT, p.Company.ToString(), even);
                                AddListItem(ref tableItems, Element.ALIGN_LEFT, p.Contract.ToString("N0"), even);
                                AddListItem(ref tableItems, Element.ALIGN_RIGHT, p.AvailableQuantity.ToString("N0"), even);
                                AddListItem(ref tableItems, Element.ALIGN_RIGHT, p.TotalQuantity.ToString("N0"), even);
                                AddListItem(ref tableItems, Element.ALIGN_RIGHT, p.SettleQuantity.ToString("N0"), even);
                                AddListItem(ref tableItems, Element.ALIGN_LEFT, p.TypeDescription, even);
                                AddListItem(ref tableItems, Element.ALIGN_LEFT, p.SettleDate.HasValue ? p.SettleDate.Value.ToString("dd/MM/yyyy") : "-", even);
                                AddListItem(ref tableItems, Element.ALIGN_LEFT, p.StatusDescription, even);
                                AddListItem(ref tableItems, Element.ALIGN_LEFT, p.RolloverDate.HasValue ? p.RolloverDate.Value.ToString("dd/MM/yyyy") : "-", even);

                                even = !even;
                            }
                            doc.Add(tableItems);

                        }
                        break;
                    case "A":
                        foreach (string code in groupItems)
                        {
                            PdfPTable tableItems = new PdfPTable(12);
                            tableItems.WidthPercentage = 100;
                            tableItems.HorizontalAlignment = Element.ALIGN_LEFT;
                            var colWidthPercentages = new[] { 230f, 90f, 90f, 90f, 90f, 100f, 100f, 100f, 90f, 90f, 90f, 90f };
                            tableItems.SetWidths(colWidthPercentages);
                            tableItems.SplitRows = false;
                            //tableItems.KeepTogether = true;
                            tableItems.SpacingBefore = 15;
                            tableItems.SpacingAfter = 15;

                            subGroupItems = (from g in list where g.Assessor.Equals(code) select g).ToList();
                            var item = subGroupItems[0];

                            Paragraph itemHeader = new Paragraph();
                            itemHeader.Add(new Phrase("Código: " + item.AssessorCode, font));
                            itemHeader.Add(Chunk.NEWLINE);
                            itemHeader.Add(new Phrase("Nome do Assessor: " + item.Assessor, font));
                            itemHeader.SpacingAfter = 10;

                            PdfPCell cell = new PdfPCell(itemHeader);
                            cell.Colspan = 12;
                            cell.Border = 0;
                            cell.PaddingBottom = 10;
                            tableItems.AddCell(cell);

                            string[] headerItems = new string[12] { "Cliente", "Código", "Papel", "Empresa", "Contrato", "Qtde. Disponível", "Qtde. Total", "Qtde. a Liquidar", "Tipo", "Data", "Situação", "Data de Rolagem" };
                            AddListHeader(ref tableItems, headerItems);

                            bool even = true;

                            foreach (TermListItem p in subGroupItems)
                            {
                                AddListItem(ref tableItems, Element.ALIGN_LEFT, p.Client, even);
                                AddListItem(ref tableItems, Element.ALIGN_LEFT, p.Code, even);
                                AddListItem(ref tableItems, Element.ALIGN_LEFT, p.Stock, even);
                                AddListItem(ref tableItems, Element.ALIGN_LEFT, p.Company.ToString(), even);
                                AddListItem(ref tableItems, Element.ALIGN_RIGHT, p.Contract.ToString("N0"), even);
                                AddListItem(ref tableItems, Element.ALIGN_RIGHT, p.AvailableQuantity.ToString("N0"), even);
                                AddListItem(ref tableItems, Element.ALIGN_RIGHT, p.TotalQuantity.ToString("N0"), even);
                                AddListItem(ref tableItems, Element.ALIGN_RIGHT, p.SettleQuantity.ToString("N0"), even);
                                AddListItem(ref tableItems, Element.ALIGN_LEFT, p.TypeDescription, even);
                                AddListItem(ref tableItems, Element.ALIGN_LEFT, p.SettleDate.HasValue ? p.SettleDate.Value.ToString("dd/MM/yyyy") : "-", even);
                                AddListItem(ref tableItems, Element.ALIGN_LEFT, p.StatusDescription, even);
                                AddListItem(ref tableItems, Element.ALIGN_LEFT, p.RolloverDate.HasValue ? p.RolloverDate.Value.ToString("dd/MM/yyyy") : "-", even);

                                even = !even;
                            }

                            doc.Add(tableItems);

                        }
                        break;
                    case "P":
                        foreach (string code in groupItems)
                        {
                            PdfPTable tableItems = new PdfPTable(11);
                            tableItems.WidthPercentage = 100;
                            tableItems.HorizontalAlignment = Element.ALIGN_LEFT;
                            var colWidthPercentages = new[] { 230f, 90f, 230f, 90f, 100f, 100f, 100f, 90f, 90f, 90f, 90f };
                            tableItems.SetWidths(colWidthPercentages);
                            tableItems.SplitRows = false;
                            //tableItems.KeepTogether = true;
                            tableItems.SpacingBefore = 15;
                            tableItems.SpacingAfter = 15;

                            subGroupItems = (from g in list where g.Stock.Equals(code) select g).ToList();
                            var item = subGroupItems[0];

                            Paragraph itemHeader = new Paragraph();
                            itemHeader.Add(new Phrase("Papel: " + item.Stock, font));
                            itemHeader.Add(Chunk.NEWLINE);
                            itemHeader.Add(new Phrase("Empresa: " + item.Company, font));
                            itemHeader.SpacingAfter = 10;

                            PdfPCell cell = new PdfPCell(itemHeader);
                            cell.Colspan = 11;
                            cell.Border = 0;
                            cell.PaddingBottom = 10;
                            tableItems.AddCell(cell);

                            string[] headerItems = new string[11] { "Cliente", "Código", "Assessor", "Contrato", "Qtde. Disponível", "Qtde. Total", "Qtde. a Liquidar", "Tipo", "Data", "Situação", "Data de Rolagem" };
                            AddListHeader(ref tableItems, headerItems);

                            bool even = true;

                            foreach (TermListItem p in subGroupItems)
                            {
                                AddListItem(ref tableItems, Element.ALIGN_LEFT, p.Client, even);
                                AddListItem(ref tableItems, Element.ALIGN_LEFT, p.Code, even);
                                AddListItem(ref tableItems, Element.ALIGN_LEFT, p.Assessor, even);
                                AddListItem(ref tableItems, Element.ALIGN_LEFT, p.Contract.ToString("N0"), even);
                                AddListItem(ref tableItems, Element.ALIGN_RIGHT, p.AvailableQuantity.ToString("N0"), even);
                                AddListItem(ref tableItems, Element.ALIGN_RIGHT, p.TotalQuantity.ToString("N0"), even);
                                AddListItem(ref tableItems, Element.ALIGN_RIGHT, p.SettleQuantity.ToString("N0"), even);
                                AddListItem(ref tableItems, Element.ALIGN_LEFT, p.TypeDescription, even);
                                AddListItem(ref tableItems, Element.ALIGN_LEFT, p.SettleDate.HasValue ? p.SettleDate.Value.ToString("dd/MM/yyyy") : "-", even);
                                AddListItem(ref tableItems, Element.ALIGN_LEFT, p.StatusDescription, even);
                                AddListItem(ref tableItems, Element.ALIGN_LEFT, p.RolloverDate.HasValue ? p.RolloverDate.Value.ToString("dd/MM/yyyy") : "-", even);

                                even = !even;
                            }

                            doc.Add(tableItems);

                        }
                        break;
                }
            }
            doc.Close();

            return memStream;
        }

        #endregion

        #region Risk

        public MemoryStream CreateGuaranteesToMemoryStream(List<GuaranteeListItem> list)
        {
            iTextSharp.text.Document doc = new iTextSharp.text.Document(new Rectangle(970, 1371).Rotate(), 25, 25, 25, 45);
            MemoryStream memStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
            writer.CloseStream = false;

            writer.PageEvent = new PrintHeaderFooter { Title = "Guarantees" };

            doc.Open();

            Font font = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(111, 111, 111));

            AddReportHeader(ref doc, "Garantias", null, string.Empty, string.Empty, string.Empty, null, null);

            foreach (GuaranteeListItem item in list)
            {
                PdfPTable tableItems = new PdfPTable(10);
                tableItems.WidthPercentage = 100;
                tableItems.HorizontalAlignment = Element.ALIGN_LEFT;
                var colWidthPercentages = new[] { 90f, 490f, 90f, 90f, 90f, 90f, 90f, 90f, 90f, 90f };
                tableItems.SetWidths(colWidthPercentages);
                tableItems.SplitRows = false;
                tableItems.KeepTogether = true;
                tableItems.SpacingBefore = 15;
                tableItems.SpacingAfter = 15;

                Paragraph itemHeader = new Paragraph();
                itemHeader.Add(new Phrase(string.Concat(item.Client.Value, " ", item.Client.Description), font));
                itemHeader.Add(Chunk.NEWLINE);
                itemHeader.Add(new Phrase(item.Assessor, font));

                PdfPCell cell = new PdfPCell(itemHeader);
                cell.Colspan = 10;
                cell.Border = 0;
                cell.PaddingBottom = 10;
                tableItems.AddCell(cell);

                string[] headerItems = new string[10] { "Solicitação", "Garantia", "Mercado", "Atual.", "Qtde.", "Valor Unit.", "Total", "Deságio", "Solicitado", "Situação" };
                AddListHeader(ref tableItems, headerItems);

                bool even = true;

                foreach (GuaranteeListSubItem sItem in item.Items)
                {
                    bool inGuarantee = sItem.Status.Value == "-1";
                    bool isMoney = sItem.Guarantee.ToLower().IndexOf("dinheiro") >= 0;
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, inGuarantee ? "Em garantia" : sItem.IsWithdrawal ? "Retirada" : "Depósito", even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, sItem.Guarantee, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, isMoney ? "-" : sItem.IsBovespa ? "Bovespa" : "BM&F", even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, sItem.UpdateDate.ToString("dd/MM/yyyy"), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, isMoney ? "-" : sItem.Quantity.ToString("N0"), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, isMoney ? "-" : sItem.UnitPrice.ToString("N2"), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, sItem.Total.ToString("N2"), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, sItem.Discount.ToString("N2") + "%", even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, sItem.RequestedValue.ToString("N2"), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, sItem.Status.Description, even);

                    even = !even;
                }
                doc.Add(tableItems);
            }
            doc.Close();

            return memStream;
        }

        #endregion

        #region Online Portfolio

        public MemoryStream CreatePortfolioContactsToMemoryStream(PortfolioContact clientContacts)
        {
            iTextSharp.text.Document doc = new iTextSharp.text.Document(new Rectangle(970, 1371).Rotate(), 25, 25, 25, 45);
            MemoryStream memStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
            writer.CloseStream = false;

            writer.PageEvent = new PrintHeaderFooter { Title = "PortfolioContacts" };

            doc.Open();

            Font nfont = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(111, 111, 111));
            Font font = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(111, 111, 111));

            AddReportHeader(ref doc, "Carteira Online - Contatos", null, string.Empty, string.Empty, string.Empty, null, null);

            IChannelProvider<IOnlinePortfolioContractChannel> portfolioProvider = ChannelProviderFactory<IOnlinePortfolioContractChannel>.Create("OnlinePortfolioClassName");
            var portfolioService = portfolioProvider.GetChannel().CreateChannel();
            portfolioService.Open();

            if (clientContacts != null)
            {
                PdfPTable tableItems = new PdfPTable(3);
                tableItems.WidthPercentage = 100;
                tableItems.HorizontalAlignment = Element.ALIGN_LEFT;
                var colWidthPercentages = new[] { 200f, 200f, 520f };
                tableItems.SetWidths(colWidthPercentages);
                tableItems.SplitRows = false;
                tableItems.KeepTogether = true;
                tableItems.SpacingBefore = 15;
                tableItems.SpacingAfter = 15;

                Paragraph itemHeader = new Paragraph(new Phrase(string.Concat(clientContacts.Client.Value, "     ", clientContacts.Client.Description), font));
                PdfPCell cell = new PdfPCell(itemHeader);
                cell.Colspan = 3;
                cell.Border = 0;
                tableItems.AddCell(cell);

                itemHeader = new Paragraph(new Phrase(string.Concat(clientContacts.ClientPhone == "()" ? "-" : clientContacts.ClientPhone, "     ", clientContacts.ClientEmail), nfont));
                cell = new PdfPCell(itemHeader);
                cell.Colspan = 3;
                cell.Border = 0;
                cell.PaddingBottom = 10;
                tableItems.AddCell(cell);

                string[] headerItems = new string[3] { "Contato em", "Operador", "Mensagem" };
                AddListHeader(ref tableItems, headerItems);

                bool even = true;

                foreach (PortfolioContactItem i in clientContacts.Contacts)
                {
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.ContactDate, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.Operator.Description, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.Contact, even);
                    even = !even;
                }
                doc.Add(tableItems);
            }
            portfolioService.Close();

            doc.Close();

            return memStream;
        }

        public MemoryStream CreatePortfolioPositionToMemoryStream(PortfolioPositionClientContent clientPosition)
        {
            iTextSharp.text.Document doc = new iTextSharp.text.Document(new Rectangle(970, 1371).Rotate(), 25, 25, 25, 45);
            MemoryStream memStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
            writer.CloseStream = false;

            writer.PageEvent = new PrintHeaderFooter { Title = "PortfolioPosition" };

            doc.Open();

            Font nfont = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(111, 111, 111));
            Font font = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(111, 111, 111));

            AddReportHeader(ref doc, "Carteira Online - Posição", null, string.Empty, string.Empty, string.Empty, null, null);

            if (clientPosition != null)
            {
                PdfPTable tableItems = new PdfPTable(12);
                tableItems.WidthPercentage = 100;
                tableItems.HorizontalAlignment = Element.ALIGN_LEFT;
                var colWidthPercentages = new[] { 60f, 75f, 75f, 75f, 80f, 65f, 75f, 75f, 78f, 81f, 75f, 190f };
                tableItems.SetWidths(colWidthPercentages);
                tableItems.SpacingBefore = 15;
                tableItems.SpacingAfter = 15;

                AddClientHeader(ref tableItems, clientPosition.ClientName, clientPosition.ClientEmail, clientPosition.ClientPhone, clientPosition.ClientStartDate, clientPosition.ClientStartValue);

                bool even = true;

                AddPortfolioPositionBoxes(ref tableItems, clientPosition);

                foreach (PortfolioPositionMarket market in clientPosition.Markets)
                {
                    int columnsSize = (market.Name.ToLower() == "a vista" || market.Name.ToLower() == "short" || market.Name.ToLower() == "l&s") ? 3 : (market.Name.ToLower() == "termo") ? 1 : 2;

                    AddMarketHeader(ref tableItems, market.Name.ToUpper(), columnsSize);

                    string[] headerItems;

                    switch (columnsSize)
                    {
                        case 3: //A Vista ou Short
                            headerItems = new string[10] { "Papel", "Data Compra", "Quantidade", "Preço Compra", "Compra Líquido", "Preço Venda", "Venda Líquido", "Lucro / Prejuízo", "%", "Setor" };
                            break;
                        case 1:
                            headerItems = new string[12] { "Papel", "Data Compra", "Quantidade", "Preço Compra", "Compra Líquido", "Preço Venda", "Venda Líquido", "Lucro / Prejuízo", "%", "Vencimento", "Rolagem", "Setor" };
                            break;
                        default: //Financiamento e BTC
                            headerItems = new string[11] { "Papel", "Data Compra", "Quantidade", "Preço Compra", "Compra Líquido", "Preço Venda", "Venda Líquido", "Lucro / Prejuízo", "%", "Vencimento", "Setor" };
                            break;

                    }

                    AddPortfolioListHeader(ref tableItems, headerItems, columnsSize);



                    foreach (PortfolioPositionItem i in market.Items)
                    {
                        AddListItem(ref tableItems, Element.ALIGN_LEFT, i.StockCode, even);
                        AddListItem(ref tableItems, Element.ALIGN_LEFT, i.BuyDate, even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.Quantity.ToString("N0"), even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.BuyValue.ToString("N2"), even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.BuyNetValue.ToString("N2"), even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.Price.ToString("N2"), even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.SellValue.ToString("N2"), even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.ProfitLoss.ToString("N2"), even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, string.Concat(i.Percentage.ToString("N2"), "%"), even);
                        if (columnsSize != 3)
                            AddListItem(ref tableItems, Element.ALIGN_LEFT, i.DueDate, even);
                        if (columnsSize == 1)
                            AddListItem(ref tableItems, Element.ALIGN_LEFT, i.RollOverDate, even);
                        AddPortfolioListItem(ref tableItems, Element.ALIGN_LEFT, i.Sector, even, columnsSize);
                        even = !even;
                    }
                }
                doc.Add(tableItems);
                if (clientPosition.Chart != null)
                    AddSectorChart(ref doc, clientPosition.Chart);
            }


            doc.Close();

            return memStream;
        }

        public MemoryStream CreatePortfolioHistoryToMemoryStream(PortfolioHistoryClientContent clientPosition)
        {
            iTextSharp.text.Document doc = new iTextSharp.text.Document(new Rectangle(970, 1371).Rotate(), 25, 25, 25, 45);
            MemoryStream memStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
            writer.CloseStream = false;

            writer.PageEvent = new PrintHeaderFooter { Title = "PortfolioHistory" };

            doc.Open();

            Font nfont = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(111, 111, 111));
            Font font = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(111, 111, 111));

            AddReportHeader(ref doc, "Carteira Online - Histórico", null, string.Empty, string.Empty, string.Empty, null, null);

            if (clientPosition != null)
            {
                PdfPTable tableItems = new PdfPTable(12);
                tableItems.WidthPercentage = 100;
                tableItems.HorizontalAlignment = Element.ALIGN_LEFT;
                var colWidthPercentages = new[] { 85f, 85f, 400f, 100f, 105f, 95f, 90f, 90f, 120f, 110f, 110f, 75f };
                tableItems.SetWidths(colWidthPercentages);
                tableItems.SpacingBefore = 15;
                tableItems.SpacingAfter = 15;

                AddClientHeader(ref tableItems, string.Concat(clientPosition.ClientCode, " ", clientPosition.ClientName), clientPosition.ClientEmail, clientPosition.ClientPhone);

                bool even = true;

                foreach (PortfolioHistoryPeriod period in clientPosition.Items)
                {
                    string[] headerItems = new string[12] { "Papel", "Mercado", "Setor", "Data Compra", "Preço Compra", "Preço Venda", "Data Venda", "Quantidade", "Financeiro Líquido", "Venda Líquido", "Lucro/Prejuízo", "%" };
                    AddListHeader(ref tableItems, headerItems);

                    foreach (PortfolioPositionItem i in period.Items)
                    {
                        AddListItem(ref tableItems, Element.ALIGN_LEFT, i.StockCode, even);
                        AddListItem(ref tableItems, Element.ALIGN_LEFT, i.MarketGroup, even);
                        AddListItem(ref tableItems, Element.ALIGN_LEFT, i.Sector, even);
                        AddListItem(ref tableItems, Element.ALIGN_LEFT, i.BuyDate, even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.BuyValue.ToString("N2"), even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.Price.ToString("N2"), even);
                        AddListItem(ref tableItems, Element.ALIGN_LEFT, i.HistoryDate.ToShortDateString(), even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.Quantity.ToString("N0"), even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.BuyNetValue.ToString("N2"), even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.SellNetValue.ToString("N2"), even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.ProfitLoss.ToString("N2"), even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, string.Concat(i.Percentage.ToString("N2"), "%"), even);
                        even = !even;
                    }

                    AddHistoryFooter(ref tableItems, period.Period, period.Balance);
                }
                doc.Add(tableItems);
            }
            doc.Close();

            return memStream;
        }

        public MemoryStream CreateAdminPositionToMemoryStream(PortfolioPosition clientPosition)
        {
            iTextSharp.text.Document doc = new iTextSharp.text.Document(new Rectangle(970, 1371).Rotate(), 25, 25, 25, 45);
            MemoryStream memStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
            writer.CloseStream = false;

            writer.PageEvent = new PrintHeaderFooter { Title = "PortfolioPosition" };

            doc.Open();

            Font nfont = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(111, 111, 111));
            Font font = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(111, 111, 111));

            AddReportHeader(ref doc, "Carteira Online - Posição Admin", null, string.Empty, string.Empty, string.Empty, null, null);

            if (clientPosition != null)
            {
                PdfPTable tableItems = new PdfPTable(11);
                tableItems.WidthPercentage = 100;
                tableItems.HorizontalAlignment = Element.ALIGN_LEFT;
                var colWidthPercentages = new[] { 100f, 100f, 100f, 95f, 95f, 100f, 142f, 98f, 130f, 117f, 110f };
                tableItems.SetWidths(colWidthPercentages);
                tableItems.SpacingBefore = 15;
                tableItems.SpacingAfter = 15;

                bool even = true;

                string[] headerItems = new string[11] { "Ativo", "Operação", "Mercado", "L & S", "Quantidade", "Preço Bruto", "Preço Líquido", "Carteira", "Data Posição", "Data Venc. do Termo", "Data Rolagem do Termo" };
                AddListHeader(ref tableItems, headerItems);

                foreach (PortfolioPositionItem item in clientPosition.Details)
                {
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, item.StockCode, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, item.Operation, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, item.MarketType, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, item.LongShort ? "L & S" : "-", even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, item.Quantity.ToString("N0"), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, item.Price.ToString("N2"), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, item.NetPrice.ToString("N2"), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, item.PortfolioNumber.Equals(0) ? item.PortfolioNumber.ToString() : "-", even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, item.PositionDate.ToShortDateString(), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, item.DueDate, even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, item.RollOverDate, even);
                    even = !even;

                }
                doc.Add(tableItems);
            }

            doc.Close();

            return memStream;
        }

        protected void AddMarketHeader(ref PdfPTable table, string marketName, int colspan)
        {
            Font marketFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(4, 103, 164));

            PdfPCell marketCell = new PdfPCell(new Phrase(marketName, marketFont));
            marketCell.Colspan = 12;
            marketCell.HorizontalAlignment = Element.ALIGN_CENTER;
            marketCell.BorderWidthLeft = marketCell.BorderWidthRight = marketCell.BorderWidthTop = 1;
            marketCell.PaddingBottom = 10;
            marketCell.BorderColor = new BaseColor(177, 178, 177);
            marketCell.BackgroundColor = new BaseColor(213, 215, 214);
            table.AddCell(marketCell);
        }

        protected void AddPortfolioPositionBoxes(ref PdfPTable tableItems, PortfolioPositionClientContent clientPosition)
        {
            PdfPCell resumeCell = new PdfPCell();

            BaseColor negative = new BaseColor(144, 39, 39);
            BaseColor positive = new BaseColor(52, 131, 30);

            PdfPTable resumeTable = new PdfPTable(3);
            var colWidthPercentages = new[] { 140f, 220f, 220f };
            resumeTable.SetWidths(colWidthPercentages);
            resumeTable.WidthPercentage = 100;
            resumeTable.HorizontalAlignment = Element.ALIGN_LEFT;

            Font nfont = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(111, 111, 111));
            Font font = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(111, 111, 111));
            Font subTitleFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(0, 135, 169));

            //Financial Box
            var financial = clientPosition.Financial;
            PdfPTable finTable = new PdfPTable(2);
            finTable.WidthPercentage = 100;
            finTable.HorizontalAlignment = Element.ALIGN_LEFT;
            PdfPCell finCell = new PdfPCell(new Phrase("Financeiro", subTitleFont));
            finCell.Colspan = 2;
            finCell.BorderWidthTop = finCell.BorderWidthRight = finCell.BorderWidthLeft = 1;
            finCell.BorderWidthBottom = 0;
            finCell.PaddingLeft = finCell.PaddingTop = finCell.PaddingBottom = 10;
            finCell.BorderColor = new BaseColor(168, 175, 182);
            finTable.AddCell(finCell);
            finCell = new PdfPCell(new Phrase("Posição:", font));
            finCell.BorderWidthRight = finCell.BorderWidthTop = finCell.BorderWidthBottom = 0;
            finCell.BorderWidthLeft = 1;
            finCell.PaddingLeft = 10;
            finCell.BorderColor = new BaseColor(168, 175, 182);
            finTable.AddCell(finCell);
            finCell = new PdfPCell(new Phrase(financial.Position, nfont));
            finCell.BorderWidthLeft = finCell.BorderWidthTop = finCell.BorderWidthBottom = 0;
            finCell.BorderWidthRight = 1;
            finCell.PaddingRight = 10;
            finCell.BorderColor = new BaseColor(168, 175, 182);
            finTable.AddCell(finCell);
            finCell = new PdfPCell(new Phrase("Caixa:", font));
            finCell.BorderWidthRight = finCell.BorderWidthTop = finCell.BorderWidthBottom = 0;
            finCell.BorderWidthLeft = 1;
            finCell.PaddingLeft = 10;
            finCell.BorderColor = new BaseColor(168, 175, 182);
            finTable.AddCell(finCell);
            finCell = new PdfPCell(new Phrase(financial.Cash, nfont));
            finCell.BorderWidthLeft = finCell.BorderWidthTop = finCell.BorderWidthBottom = 0;
            finCell.BorderWidthRight = 1;
            finCell.PaddingRight = 10;
            finCell.BorderColor = new BaseColor(168, 175, 182);
            finTable.AddCell(finCell);
            finCell = new PdfPCell(new Phrase("TOTAL:", font));
            finCell.BorderWidthRight = finCell.BorderWidthTop = 0;
            finCell.BorderWidthLeft = finCell.BorderWidthBottom = 1;
            finCell.PaddingLeft = finCell.PaddingBottom = 10;
            finCell.BorderColor = new BaseColor(168, 175, 182);
            finCell.PaddingBottom = 45;
            finTable.AddCell(finCell);
            finCell = new PdfPCell(new Phrase(financial.Total, nfont));
            finCell.BorderWidthRight = finCell.BorderWidthBottom = 1;
            finCell.BorderWidthLeft = finCell.BorderWidthTop = 0;
            finCell.PaddingBottom = finCell.PaddingRight = 10;
            finCell.BorderColor = new BaseColor(168, 175, 182);
            finCell.PaddingBottom = 45;
            finTable.AddCell(finCell);

            resumeCell.Border = 0;
            resumeCell.PaddingRight = 10;
            resumeCell.AddElement(finTable);
            resumeTable.AddCell(resumeCell);

            //IBOV Box
            var ibov = clientPosition.Ibov;
            PdfPTable ibovTable = new PdfPTable(3);
            var ibovPercentages = new[] { 100f, 70f, 50f };
            ibovTable.SetWidths(ibovPercentages);
            ibovTable.WidthPercentage = 100;
            ibovTable.HorizontalAlignment = Element.ALIGN_LEFT;
            ibovTable.SplitRows = false;
            ibovTable.KeepTogether = true;
            PdfPCell ibovCell = new PdfPCell(new Phrase("IBOV", subTitleFont));
            ibovCell.BorderWidthTop = ibovCell.BorderWidthRight = ibovCell.BorderWidthLeft = 1;
            ibovCell.BorderWidthBottom = 0;
            ibovCell.PaddingLeft = ibovCell.PaddingTop = ibovCell.PaddingBottom = 10;
            ibovCell.BorderColor = new BaseColor(168, 175, 182);
            ibovCell.Colspan = 3;
            ibovTable.AddCell(ibovCell);
            ibovCell = new PdfPCell();
            ibovCell.BorderWidthBottom = ibovCell.BorderWidthTop = ibovCell.BorderWidthRight = 0;
            ibovCell.BorderWidthLeft = 1;
            ibovCell.BorderColor = new BaseColor(168, 175, 182);
            ibovTable.AddCell(ibovCell);
            ibovCell = new PdfPCell(new Phrase("Pontos", font));
            ibovCell.Border = 0;
            ibovTable.AddCell(ibovCell);
            ibovCell = new PdfPCell(new Phrase("Variação", font));
            ibovCell.BorderWidthLeft = ibovCell.BorderWidthTop = ibovCell.BorderWidthBottom = 0;
            ibovCell.BorderWidthRight = 1;
            ibovCell.BorderColor = new BaseColor(168, 175, 182);
            ibovTable.AddCell(ibovCell);
            ibovCell = new PdfPCell(new Phrase(string.Format("Hoje ({0}):", DateTime.Now.ToShortDateString()), font));
            ibovCell.BorderWidthBottom = ibovCell.BorderWidthTop = ibovCell.BorderWidthRight = 0;
            ibovCell.BorderWidthLeft = 1;
            ibovCell.PaddingLeft = 10;
            ibovCell.BorderColor = new BaseColor(168, 175, 182);
            ibovTable.AddCell(ibovCell);
            ibovCell = new PdfPCell(new Phrase(ibov.TodayPoints, nfont));
            ibovCell.Border = 0;
            ibovTable.AddCell(ibovCell);
            ibovCell = new PdfPCell(new Phrase(ibov.TodayVariation, FontFactory.GetFont("Arial", 12, Font.NORMAL, ibov.TodayVariation.IndexOf("-") >= 0 ? negative : positive)));

            ibovCell.BorderWidthLeft = ibovCell.BorderWidthTop = ibovCell.BorderWidthBottom = 0;
            ibovCell.HorizontalAlignment = Element.ALIGN_RIGHT;
            ibovCell.PaddingRight = 25;
            ibovCell.BorderWidthRight = 1;
            ibovCell.BorderColor = new BaseColor(168, 175, 182);
            ibovTable.AddCell(ibovCell);
            ibovCell = new PdfPCell(new Phrase(string.Format("12M ({0}):", ibov.YearDate), font));
            ibovCell.BorderWidthLeft = ibovCell.BorderWidthBottom = 1;
            ibovCell.BorderWidthRight = ibovCell.BorderWidthTop = 0;
            ibovCell.BorderColor = new BaseColor(168, 175, 182);
            ibovCell.PaddingLeft = 10;
            ibovCell.PaddingBottom = 45;
            ibovTable.AddCell(ibovCell);

            ibovCell = new PdfPCell(new Phrase(ibov.YearPoints, nfont));
            ibovCell.BorderWidthLeft = ibovCell.BorderWidthTop = ibovCell.BorderWidthRight = 0;
            ibovCell.BorderWidthBottom = 1;
            ibovCell.PaddingBottom = 45;
            ibovCell.BorderColor = new BaseColor(168, 175, 182);
            ibovTable.AddCell(ibovCell);
            ibovCell = new PdfPCell(new Phrase(ibov.YearVariation, FontFactory.GetFont("Arial", 12, Font.NORMAL, ibov.YearVariation.IndexOf("-") >= 0 ? negative : positive)));

            ibovCell.BorderWidthLeft = ibovCell.BorderWidthTop = 0;
            ibovCell.HorizontalAlignment = Element.ALIGN_RIGHT;
            ibovCell.PaddingRight = 25;
            ibovCell.BorderWidthRight = ibovCell.BorderWidthBottom = 1;
            ibovCell.PaddingBottom = 45;
            ibovCell.BorderColor = new BaseColor(168, 175, 182);
            ibovTable.AddCell(ibovCell);

            resumeCell = new PdfPCell();
            resumeCell.Border = 0;
            resumeCell.PaddingRight = 10;
            resumeCell.AddElement(ibovTable);
            resumeTable.AddCell(resumeCell);

            //CC Box
            var cc = clientPosition.Discount;
            PdfPTable ccTable = new PdfPTable(4);
            ccTable.WidthPercentage = 100;
            var ccPercentages = new[] { 45f, 40f, 70f, 55f };
            ccTable.SetWidths(ccPercentages);
            ccTable.HorizontalAlignment = Element.ALIGN_RIGHT;
            ccTable.SplitRows = false;
            ccTable.KeepTogether = true;
            PdfPCell ccCell = new PdfPCell(new Phrase("C/C", subTitleFont));
            ccCell.BorderWidthTop = ccCell.BorderWidthLeft = 1;
            ccCell.BorderWidthBottom = ccCell.BorderWidthRight = 0;
            ccCell.Colspan = 2;
            ccCell.PaddingLeft = ccCell.PaddingTop = 10;
            ccCell.BorderColor = new BaseColor(168, 175, 182);
            ccTable.AddCell(ccCell);

            ccCell = new PdfPCell(new Phrase("Desconto DayTrade", font));
            ccCell.HorizontalAlignment = Element.ALIGN_RIGHT;
            ccCell.PaddingRight = 5;
            ccCell.BorderWidthTop = 1;
            ccCell.PaddingTop = 10;
            ccCell.BorderWidthRight = ccCell.BorderWidthLeft = ccCell.BorderWidthBottom = 0;
            ccCell.BorderColor = new BaseColor(168, 175, 182);
            ccTable.AddCell(ccCell);

            ccCell = new PdfPCell(new Phrase(cc.DayTrade, nfont));
            ccCell.BorderWidthRight = ccCell.BorderWidthTop = 1;
            ccCell.BorderWidthLeft = ccCell.BorderWidthBottom = 0;
            ccCell.PaddingTop = 10;
            ccCell.HorizontalAlignment = Element.ALIGN_RIGHT;
            ccCell.PaddingRight = 15;
            ccCell.BorderColor = new BaseColor(168, 175, 182);
            ccTable.AddCell(ccCell);

            PdfPCell blankCell = new PdfPCell();
            blankCell.BorderWidthLeft = 1;
            blankCell.BorderWidthRight = blankCell.BorderWidthBottom = blankCell.BorderWidthTop = 0;
            blankCell.Colspan = 2;
            blankCell.BorderColor = new BaseColor(168, 175, 182);
            ccTable.AddCell(blankCell);

            ccCell = new PdfPCell(new Phrase("Disponível", font));
            ccCell.HorizontalAlignment = Element.ALIGN_RIGHT;
            ccCell.PaddingRight = 5;
            ccCell.Border = 0;
            ccCell.BorderColor = new BaseColor(168, 175, 182);
            ccTable.AddCell(ccCell);
            ccCell = new PdfPCell(new Phrase(cc.Available, nfont));
            ccCell.BorderWidthRight = 1;
            ccCell.HorizontalAlignment = Element.ALIGN_RIGHT;
            ccCell.PaddingRight = 15;
            ccCell.BorderWidthLeft = ccCell.BorderWidthBottom = ccCell.BorderWidthTop = 0;
            ccCell.BorderColor = new BaseColor(168, 175, 182);
            ccTable.AddCell(ccCell);

            ccTable.AddCell(blankCell);

            ccCell = new PdfPCell(new Phrase("Operações do Dia", font));
            ccCell.HorizontalAlignment = Element.ALIGN_RIGHT;
            ccCell.PaddingRight = 5;
            ccCell.Border = 0;
            ccCell.BorderColor = new BaseColor(168, 175, 182);
            ccTable.AddCell(ccCell);
            ccCell = new PdfPCell(new Phrase(cc.DailyOperations, nfont));
            ccCell.BorderWidthRight = 1;
            ccCell.HorizontalAlignment = Element.ALIGN_RIGHT;
            ccCell.PaddingRight = 15;
            ccCell.BorderWidthLeft = ccCell.BorderWidthBottom = ccCell.BorderWidthTop = 0;
            ccCell.BorderColor = new BaseColor(168, 175, 182);
            ccTable.AddCell(ccCell);

            ccTable.AddCell(blankCell);

            ccCell = new PdfPCell(new Phrase("D+1", font));
            ccCell.HorizontalAlignment = Element.ALIGN_RIGHT;
            ccCell.PaddingRight = 5;
            ccCell.Border = 0;
            ccCell.BorderColor = new BaseColor(168, 175, 182);
            ccTable.AddCell(ccCell);
            ccCell = new PdfPCell(new Phrase(cc.D1, nfont));
            ccCell.BorderWidthRight = 1;
            ccCell.HorizontalAlignment = Element.ALIGN_RIGHT;
            ccCell.PaddingRight = 15;
            ccCell.BorderWidthLeft = ccCell.BorderWidthBottom = ccCell.BorderWidthTop = 0;
            ccCell.BorderColor = new BaseColor(168, 175, 182);
            ccTable.AddCell(ccCell);

            ccTable.AddCell(blankCell);

            ccCell = new PdfPCell(new Phrase("D+2", font));
            ccCell.HorizontalAlignment = Element.ALIGN_RIGHT;
            ccCell.PaddingRight = 5;
            ccCell.Border = 0;
            ccCell.BorderColor = new BaseColor(168, 175, 182);
            ccTable.AddCell(ccCell);
            ccCell = new PdfPCell(new Phrase(cc.D2, nfont));
            ccCell.BorderWidthRight = 1;
            ccCell.HorizontalAlignment = Element.ALIGN_RIGHT;
            ccCell.PaddingRight = 15;
            ccCell.BorderWidthLeft = ccCell.BorderWidthBottom = ccCell.BorderWidthTop = 0;
            ccCell.BorderColor = new BaseColor(168, 175, 182);
            ccTable.AddCell(ccCell);

            ccCell = new PdfPCell(new Phrase("Desconto", font));
            ccCell.HorizontalAlignment = Element.ALIGN_LEFT;
            ccCell.PaddingLeft = 10;
            ccCell.PaddingBottom = 21;
            ccCell.BorderWidthLeft = ccCell.BorderWidthBottom = 1;
            ccCell.BorderWidthRight = ccCell.BorderWidthTop = 0;
            ccCell.BorderColor = new BaseColor(168, 175, 182);
            ccTable.AddCell(ccCell);
            ccCell = new PdfPCell(new Phrase(string.Format("{0}%", clientPosition.CCDiscount), nfont));
            ccCell.BorderWidthBottom = 1;
            ccCell.PaddingLeft = 10;
            ccCell.PaddingBottom = 21;
            ccCell.HorizontalAlignment = Element.ALIGN_RIGHT;
            ccCell.PaddingRight = 15;
            ccCell.BorderWidthLeft = ccCell.BorderWidthRight = ccCell.BorderWidthTop = 0;
            ccCell.BorderColor = new BaseColor(168, 175, 182);
            ccTable.AddCell(ccCell);


            ccCell = new PdfPCell(new Phrase("D+3", font));
            ccCell.HorizontalAlignment = Element.ALIGN_RIGHT;
            ccCell.BorderWidthBottom = 1;
            ccCell.BorderWidthRight = ccCell.BorderWidthLeft = ccCell.BorderWidthTop = 0;
            ccCell.BorderColor = new BaseColor(168, 175, 182);
            ccCell.PaddingBottom = 21;
            ccCell.PaddingRight = 5;
            ccTable.AddCell(ccCell);
            ccCell = new PdfPCell(new Phrase(cc.D3, nfont));
            ccCell.BorderWidthLeft = ccCell.BorderWidthTop = 0;
            ccCell.BorderWidthRight = ccCell.BorderWidthBottom = 1;
            ccCell.PaddingBottom = 21;
            ccCell.HorizontalAlignment = Element.ALIGN_RIGHT;
            ccCell.PaddingRight = 15;
            ccCell.BorderColor = new BaseColor(168, 175, 182);
            ccTable.AddCell(ccCell);

            resumeCell = new PdfPCell();
            resumeCell.Border = 0;
            resumeCell.HorizontalAlignment = Element.ALIGN_RIGHT;
            resumeCell.AddElement(ccTable);
            resumeTable.AddCell(resumeCell);

            resumeCell = new PdfPCell();
            resumeCell.Colspan = 12;
            resumeCell.HorizontalAlignment = Element.ALIGN_CENTER;
            resumeCell.Border = 0;
            resumeCell.PaddingBottom = 20;
            resumeCell.AddElement(resumeTable);

            tableItems.AddCell(resumeCell);
        }

        protected void AddSectorChart(ref iTextSharp.text.Document doc, List<PortfolioPositionChartItem> chartItems)
        {
            PdfPTable chartTable = new PdfPTable(4);
            chartTable.WidthPercentage = 60;
            chartTable.SplitRows = false;
            chartTable.KeepTogether = true;
            var colWidthPercentages = new[] { 300f, 120f, 90f, 60f };
            chartTable.SetWidths(colWidthPercentages);

            Font font = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(111, 111, 111));
            Font subTitleFont = FontFactory.GetFont("Arial", 16, Font.BOLD, new BaseColor(0, 102, 164));
            Font headerFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(0, 102, 164));


            PdfPCell sectorCell = new PdfPCell(new Phrase("Setores", subTitleFont));
            sectorCell.Colspan = 4;
            sectorCell.BorderWidthTop = sectorCell.BorderWidthRight = sectorCell.BorderWidthLeft = 1;
            sectorCell.BorderWidthBottom = 0;
            sectorCell.PaddingLeft = sectorCell.PaddingTop = sectorCell.PaddingBottom = 10;
            sectorCell.BorderColor = new BaseColor(168, 175, 182);
            chartTable.AddCell(sectorCell);

            sectorCell = new PdfPCell();
            sectorCell.Colspan = 2;
            sectorCell.BorderWidthTop = sectorCell.BorderWidthRight = sectorCell.BorderWidthBottom = 0;
            sectorCell.BorderWidthLeft = 1;
            sectorCell.BorderColor = new BaseColor(168, 175, 182);
            chartTable.AddCell(sectorCell);

            sectorCell = new PdfPCell(new Phrase("Alocação", headerFont));
            sectorCell.Border = 0;
            sectorCell.PaddingLeft = 5;
            sectorCell.PaddingBottom = 5;
            sectorCell.BorderColor = new BaseColor(168, 175, 182);
            chartTable.AddCell(sectorCell);
            sectorCell = new PdfPCell(new Phrase("Variação", headerFont));
            sectorCell.BorderWidthTop = sectorCell.BorderWidthLeft = sectorCell.BorderWidthBottom = 0;
            sectorCell.BorderWidthRight = 1;
            sectorCell.PaddingLeft = 5;
            sectorCell.PaddingBottom = 5;
            sectorCell.BorderColor = new BaseColor(168, 175, 182);
            chartTable.AddCell(sectorCell);

            bool even = true;
            foreach (PortfolioPositionChartItem item in chartItems)
            {

                sectorCell = new PdfPCell(new Phrase(item.SectorName, font));
                sectorCell.BorderWidthBottom = sectorCell.BorderWidthTop = sectorCell.BorderWidthRight = 0;
                sectorCell.BorderWidthLeft = 1;
                sectorCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                sectorCell.BorderColor = new BaseColor(168, 175, 182);
                sectorCell.PaddingLeft = 10;
                sectorCell.BackgroundColor = even ? new BaseColor(238, 239, 239) : new BaseColor(230, 231, 231);
                sectorCell.BorderColor = new BaseColor(168, 175, 182);
                chartTable.AddCell(sectorCell);

                //Chart
                sectorCell = new PdfPCell();
                sectorCell.Border = 0;
                sectorCell.BackgroundColor = even ? new BaseColor(238, 239, 239) : new BaseColor(230, 231, 231);
                PdfPTable chartItem = new PdfPTable(1);
                chartItem.WidthPercentage = (float)item.Percentage;
                PdfPCell chartCell = new PdfPCell();
                chartCell.Border = 1;
                chartCell.FixedHeight = 15.0f;
                chartCell.BorderColor = new BaseColor(97, 121, 130);
                chartCell.BackgroundColor = new BaseColor(123, 152, 162);
                chartItem.AddCell(chartCell);
                chartItem.HorizontalAlignment = Element.ALIGN_LEFT;
                sectorCell.AddElement(chartItem);
                sectorCell.PaddingTop = sectorCell.PaddingBottom = sectorCell.PaddingRight = sectorCell.PaddingLeft = 5;
                sectorCell.HorizontalAlignment = Element.ALIGN_LEFT;
                sectorCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                chartTable.AddCell(sectorCell);

                sectorCell = new PdfPCell(new Phrase(item.Allocation, font));
                sectorCell.Border = 0;
                sectorCell.BackgroundColor = even ? new BaseColor(238, 239, 239) : new BaseColor(230, 231, 231);
                sectorCell.PaddingLeft = 5;
                chartTable.AddCell(sectorCell);
                sectorCell = new PdfPCell(new Phrase(string.Concat(item.Variation, "%"), font));
                sectorCell.BorderWidthBottom = sectorCell.BorderWidthTop = sectorCell.BorderWidthLeft = 0;
                sectorCell.BorderWidthRight = 1;
                sectorCell.PaddingLeft = 5;
                sectorCell.BorderColor = new BaseColor(168, 175, 182);
                sectorCell.BackgroundColor = even ? new BaseColor(238, 239, 239) : new BaseColor(230, 231, 231);
                chartTable.AddCell(sectorCell);
                even = !even;
            }
            sectorCell = new PdfPCell();
            sectorCell.Colspan = 4;
            sectorCell.BorderWidthBottom = sectorCell.BorderWidthRight = sectorCell.BorderWidthLeft = 1;
            sectorCell.BorderWidthTop = 0;
            sectorCell.BorderColor = new BaseColor(168, 175, 182);
            chartTable.AddCell(sectorCell);

            chartTable.SpacingBefore = 10;
            chartTable.HorizontalAlignment = Element.ALIGN_LEFT;

            doc.Add(chartTable);
        }

        protected void AddHistoryFooter(ref PdfPTable tableItems, string period, PortfolioHistoryPeriodBalance balance)
        {
            Font monthFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(4, 103, 164));
            Font itemFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(89, 89, 89));

            PdfPCell footerCell = new PdfPCell(new Phrase(period, monthFont));
            footerCell.Colspan = 7;
            footerCell.HorizontalAlignment = Element.ALIGN_RIGHT;
            footerCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            footerCell.Border = 0;
            footerCell.PaddingRight = 5;
            footerCell.BackgroundColor = new BaseColor(255, 255, 255);
            tableItems.AddCell(footerCell);

            footerCell = new PdfPCell(new Phrase(balance.Quantity.ToString("N2"), itemFont));
            footerCell.HorizontalAlignment = Element.ALIGN_RIGHT;
            footerCell.Padding = 7;
            footerCell.BorderWidthTop = footerCell.BorderWidthLeft = footerCell.BorderWidthRight = footerCell.BorderWidthBottom = 1;
            footerCell.BorderColor = new BaseColor(255, 255, 255);
            footerCell.BackgroundColor = new BaseColor(216, 226, 228);
            tableItems.AddCell(footerCell);

            footerCell = new PdfPCell(new Phrase(balance.BuyNetValue.ToString("N2"), itemFont));
            footerCell.HorizontalAlignment = Element.ALIGN_RIGHT;
            footerCell.Padding = 7;
            footerCell.BorderWidthTop = footerCell.BorderWidthLeft = footerCell.BorderWidthRight = footerCell.BorderWidthBottom = 1;
            footerCell.BorderColor = new BaseColor(255, 255, 255);
            footerCell.BackgroundColor = new BaseColor(216, 226, 228);
            tableItems.AddCell(footerCell);

            footerCell = new PdfPCell(new Phrase(balance.SellNetValue.ToString("N2"), itemFont));
            footerCell.HorizontalAlignment = Element.ALIGN_RIGHT;
            footerCell.Padding = 7;
            footerCell.BorderWidthTop = footerCell.BorderWidthLeft = footerCell.BorderWidthRight = footerCell.BorderWidthBottom = 1;
            footerCell.BorderColor = new BaseColor(255, 255, 255);
            footerCell.BackgroundColor = new BaseColor(216, 226, 228);
            tableItems.AddCell(footerCell);

            footerCell = new PdfPCell(new Phrase(balance.ProfitLoss.ToString("N2"), itemFont));
            footerCell.HorizontalAlignment = Element.ALIGN_RIGHT;
            footerCell.Padding = 7;
            footerCell.BorderWidthTop = footerCell.BorderWidthLeft = footerCell.BorderWidthRight = footerCell.BorderWidthBottom = 1;
            footerCell.BorderColor = new BaseColor(255, 255, 255);
            footerCell.BackgroundColor = new BaseColor(216, 226, 228);
            tableItems.AddCell(footerCell);

            footerCell = new PdfPCell(new Phrase(balance.Percentage.ToString("N2"), itemFont));
            footerCell.HorizontalAlignment = Element.ALIGN_RIGHT;
            footerCell.Padding = 7;
            footerCell.BorderWidthTop = footerCell.BorderWidthLeft = footerCell.BorderWidthRight = footerCell.BorderWidthBottom = 1;
            footerCell.BorderColor = new BaseColor(255, 255, 255);
            footerCell.BackgroundColor = new BaseColor(216, 226, 228);
            tableItems.AddCell(footerCell);

            footerCell = new PdfPCell();
            footerCell.Colspan = 12;
            footerCell.BorderWidthTop = footerCell.BorderWidthLeft = footerCell.BorderWidthRight = footerCell.BorderWidthBottom = 0;
            footerCell.BackgroundColor = new BaseColor(255, 255, 255);
            footerCell.PaddingTop = 10;
            tableItems.AddCell(footerCell);
        }

        #endregion

        #region Financial

        public MemoryStream CreateProjectedReportToMemoryStream(List<ProjectedReportItem> list, string groupBy)
        {
            iTextSharp.text.Document doc = new iTextSharp.text.Document(new Rectangle(970, 1371).Rotate(), 25, 25, 25, 45);
            MemoryStream memStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
            writer.CloseStream = false;

            writer.PageEvent = new PrintHeaderFooter { Title = "ProjectedReport" };

            doc.Open();

            Font font = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(111, 111, 111));

            AddReportHeader(ref doc, "Relatório Projetado", null, string.Empty, string.Empty, string.Empty, null, null);

            List<string> groupItems = new List<string>();

            switch (groupBy)
            {
                case "A":
                    groupItems = (from l in list select l.Assessor).Distinct().ToList();
                    break;
                default:
                    groupItems = null;
                    break;
            }

            BaseColor negative = new BaseColor(144, 39, 39);
            BaseColor positive = new BaseColor(52, 131, 30);

            if (groupItems != null)
            {
                List<ProjectedReportItem> subGroupItems = new List<ProjectedReportItem>();

                foreach (string name in groupItems)
                {
                    PdfPTable tableItems = new PdfPTable(13);
                    tableItems.WidthPercentage = 100;
                    tableItems.HorizontalAlignment = Element.ALIGN_LEFT;
                    var colWidthPercentages = new[] { 85f, 85f, 150f, 60f, 60f, 90f, 90f, 90f, 90f, 90f, 80f, 80f, 110f };
                    tableItems.SetWidths(colWidthPercentages);
                    tableItems.SplitRows = false;
                    tableItems.SpacingBefore = 15;
                    tableItems.SpacingAfter = 15;

                    subGroupItems = (from g in list where g.Assessor.Equals(name) select g).ToList();
                    var item = subGroupItems[0];
                    Paragraph itemHeader = new Paragraph();
                    itemHeader.Add(new Phrase("Assessor: " + name, font));

                    PdfPCell cell = new PdfPCell(itemHeader);
                    cell.Colspan = 13;
                    cell.Border = 0;
                    cell.PaddingBottom = 10;
                    tableItems.AddCell(cell);

                    string[] headerItems = new string[13] { "Pregão de", "Cód.", "Nome", "%", "Nota", "Volume", "Corretagem", "Reg. Bolsas", "Emolumentos", "Liquidação", "A.N.A", "IRRF", "A Receber/Pagar" };
                    AddListHeader(ref tableItems, headerItems);

                    bool even = true;

                    foreach (ProjectedReportItem p in subGroupItems)
                    {
                        AddListItem(ref tableItems, Element.ALIGN_LEFT, p.TradingDate.ToShortDateString(), even);
                        AddListItem(ref tableItems, Element.ALIGN_LEFT, p.ClientID.ToString(), even);
                        AddListItem(ref tableItems, Element.ALIGN_LEFT, p.ClientName.ToString(), even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, p.Percentage.ToString("N2"), even);
                        AddListItem(ref tableItems, Element.ALIGN_LEFT, p.Note.ToString(), even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, p.Volume.ToString("N2"), even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, p.Brokerage.ToString("N2"), even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, p.Record.ToString("N2"), even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, p.Fees.ToString("N2"), even);
                        AddListItem(ref tableItems, Element.ALIGN_LEFT, p.SettlementDate.ToShortDateString(), even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, p.ANA.ToString("N2"), even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, p.IRRF.ToString("N2"), even);
                        AddListValueItem(ref tableItems, Element.ALIGN_RIGHT, p.NetValue.ToString("N2"), even);
                        even = !even;
                    }
                    doc.Add(tableItems);
                }
            }
            else
            {
                PdfPTable tableItems = new PdfPTable(14);
                tableItems.WidthPercentage = 100;
                tableItems.HorizontalAlignment = Element.ALIGN_LEFT;
                var colWidthPercentages = new[] { 300f, 75f, 75f, 100f, 50f, 55f, 90f, 90f, 90f, 90f, 75f, 55f, 55f, 110f };
                tableItems.SetWidths(colWidthPercentages);
                tableItems.SplitRows = false;
                tableItems.SpacingBefore = 15;
                tableItems.SpacingAfter = 15;

                string[] headerItems = new string[14] { "Assessor", "Pregão de", "Cód.", "Nome", "%", "Nota", "Volume", "Corretagem", "Reg. Bolsas", "Emolumentos", "Liquidação", "A.N.A", "IRRF", "A Receber/Pagar" };
                AddListHeader(ref tableItems, headerItems);

                bool even = true;

                foreach (ProjectedReportItem p in list)
                {
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, p.Assessor, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, p.TradingDate.ToShortDateString(), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, p.ClientID.ToString(), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, p.ClientName.ToString(), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, p.Percentage.ToString("N2"), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, p.Note.ToString(), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, p.Volume.ToString("N2"), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, p.Brokerage.ToString("N2"), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, p.Record.ToString("N2"), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, p.Fees.ToString("N2"), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, p.SettlementDate.ToShortDateString(), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, p.ANA.ToString("N2"), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, p.IRRF.ToString("N2"), even);
                    AddListValueItem(ref tableItems, Element.ALIGN_RIGHT, p.NetValue.ToString("N2"), even);
                    even = !even;
                }
                doc.Add(tableItems);
            }

            doc.Close();

            return memStream;
        }

        #endregion

        #region ClientRegistration

        public MemoryStream CreateStatusViewerToMemoryStream(List<Client> list)
        {
            iTextSharp.text.Document doc = new iTextSharp.text.Document(new Rectangle(1371, 970), 25, 25, 25, 45);
            MemoryStream memStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
            writer.CloseStream = false;

            writer.PageEvent = new PrintHeaderFooter { Title = "StatusViewer" };

            doc.Open();

            Font tradeFont = FontFactory.GetFont("Arial", 30, Font.BOLD, new BaseColor(0, 135, 169));
            Font companyAddressFont = FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(111, 111, 111));
            Font dateFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(111, 111, 111));
            Font lineFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(111, 111, 111));
            Font lineHeaderFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(255, 255, 255));

            AddReportHeader(ref doc, "Cadastro de Status", null, string.Empty, string.Empty, string.Empty);

            //PdfPTable tableSubscriptionHeader = new PdfPTable(1);
            //tableSubscriptionHeader.WidthPercentage = 100;
            //tableSubscriptionHeader.HorizontalAlignment = 0;
            //tableSubscriptionHeader.SpacingBefore = 20;
            //tableSubscriptionHeader.SpacingAfter = 30;

            //PdfPCell cellTitulo = new PdfPCell();
            //cellTitulo.BorderWidth = 0;
            //cellTitulo.PaddingBottom = 5;

            //doc.Add(tableSubscriptionHeader);

            if (list.Count > 0)
            {
                PdfPTable tableItems = new PdfPTable(7);
                tableItems.WidthPercentage = 100;
                var colWidthPercentages = new[] { 8f, 6f, 22f, 8f, 22f, 6f, 6f };
                tableItems.SetWidths(colWidthPercentages);
                tableItems.SplitRows = false;

                string[] headerItems = new string[7] { "Cadastro", "Código", "Cliente", "CPF/CGC", "Assessor", "BM&F", "Bovespa" };
                AddListHeader(ref tableItems, headerItems);

                bool even = true;
                foreach (Client i in list)
                {
                    AddListItem(ref tableItems, Element.ALIGN_CENTER, i.AccessionDate.HasValue ? i.AccessionDate.Value.ToShortDateString() : "-", even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.ClientCode.ToString(), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.ClientName, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.CPFCGC.ToString(), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.AssessorName.ToString(), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.BMFActive.HasValue ? i.BMFActive.Value ? "Ativo" : "Inativo" : "-", even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.BovespaActive.HasValue ? i.BovespaActive.Value ? "Ativo" : "Inativo" : "-", even);

                    even = !even;
                }
                doc.Add(tableItems);
            }

            doc.Close();

            return memStream;
        }

        public MemoryStream CreateOverdueAndDueToMemoryStream(List<OverdueAndDue> list)
        {
            iTextSharp.text.Document doc = new iTextSharp.text.Document(new Rectangle(1371, 970), 25, 25, 25, 45);
            MemoryStream memStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
            writer.CloseStream = false;

            writer.PageEvent = new PrintHeaderFooter { Title = "OverdueAndDue" };

            doc.Open();

            Font tradeFont = FontFactory.GetFont("Arial", 30, Font.BOLD, new BaseColor(0, 135, 169));
            Font companyAddressFont = FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(111, 111, 111));
            Font dateFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(111, 111, 111));
            Font lineFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(111, 111, 111));
            Font lineHeaderFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(255, 255, 255));

            AddReportHeader(ref doc, "Cadastro de Vencimentos", null, string.Empty, string.Empty, string.Empty);

            if (list.Count > 0)
            {
                PdfPTable tableItems = new PdfPTable(8);
                tableItems.WidthPercentage = 100;
                var colWidthPercentages = new[] { 8f, 10f, 10f, 16f, 6f, 6f, 6f, 6f };
                tableItems.SetWidths(colWidthPercentages);
                tableItems.SplitRows = false;

                string[] headerItems = new string[8] { "Código", "Cliente", "CPF/CGC", "Assessor", "Documento", "Situação", "Vencimento", "Sinacor" };
                AddListHeader(ref tableItems, headerItems);

                MarketHelper marketHelper = new MarketHelper();

                bool even = true;
                foreach (OverdueAndDue i in list)
                {
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.ClientCode.ToString(), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.ClientName, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.CPFCGC.ToString(), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, marketHelper.GetAssessorName(i.AssessorCode), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.RegisterType, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.SituationDesc, even);
                    AddListItem(ref tableItems, Element.ALIGN_CENTER, i.RegisterDate != null && i.RegisterDate != DateTime.MinValue ? i.RegisterDate.ToShortDateString() : "-", even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.SinacorDesc, even);
                    even = !even;
                }
                doc.Add(tableItems);
            }

            doc.Close();

            return memStream;
        }

        public MemoryStream CreateTransferBovespaToMemoryStream(List<TransferBovespaItem> list)
        {
            iTextSharp.text.Document doc = new iTextSharp.text.Document(new Rectangle(1371, 970), 25, 25, 25, 45);
            MemoryStream memStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
            writer.CloseStream = false;

            writer.PageEvent = new PrintHeaderFooter { Title = "TransferBovespa" };

            doc.Open();

            Font tradeFont = FontFactory.GetFont("Arial", 30, Font.BOLD, new BaseColor(0, 135, 169));
            Font companyAddressFont = FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(111, 111, 111));
            Font dateFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(111, 111, 111));
            Font lineFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(111, 111, 111));
            Font lineHeaderFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(255, 255, 255));

            AddReportHeader(ref doc, "Repasse Bovespa", null, string.Empty, string.Empty, string.Empty);

            if (list.Count > 0)
            {
                PdfPTable tableItems = new PdfPTable(3);
                tableItems.WidthPercentage = 100;
                var colWidthPercentages = new[] { 8f, 10f, 10f };
                tableItems.SetWidths(colWidthPercentages);
                tableItems.SplitRows = false;

                string[] headerItems = new string[3] { "Código", "Cliente", "CNPJ" };
                AddListHeader(ref tableItems, headerItems);

                MarketHelper marketHelper = new MarketHelper();

                bool even = true;
                foreach (TransferBovespaItem i in list)
                {
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.Client.Value, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.Client.Description, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.CNPJ, even);
                    even = !even;
                }
                doc.Add(tableItems);
            }

            doc.Close();

            return memStream;
        }

        public MemoryStream CreateTransferBMFToMemoryStream(List<TransferBMFItem> list)
        {
            iTextSharp.text.Document doc = new iTextSharp.text.Document(new Rectangle(1371, 970).Rotate(), 25, 25, 25, 45);
            MemoryStream memStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
            writer.CloseStream = false;

            writer.PageEvent = new PrintHeaderFooter { Title = "TransferBMF" };

            doc.Open();

            Font tradeFont = FontFactory.GetFont("Arial", 30, Font.BOLD, new BaseColor(0, 135, 169));
            Font companyAddressFont = FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(111, 111, 111));
            Font dateFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(111, 111, 111));
            Font lineFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(111, 111, 111));
            Font lineHeaderFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(255, 255, 255));

            AddReportHeader(ref doc, "Repasse BMF", null, string.Empty, string.Empty, string.Empty);

            if (list.Count > 0)
            {
                PdfPTable tableItems = new PdfPTable(6);
                tableItems.WidthPercentage = 100;
                var colWidthPercentages = new[] { 3f, 12f, 3f, 12f, 5f, 3f };
                tableItems.SetWidths(colWidthPercentages);
                tableItems.SplitRows = false;

                string[] headerItems = new string[6] { "Vínculo", "Corretora", "Código", "Cliente", "CPF/CNPJ", "Tipo" };
                AddListHeader(ref tableItems, headerItems);

                bool even = true;
                foreach (var i in list)
                {
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.BondId.ToString(), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.BrokerName, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.ClientCode == 0 ? "-" : i.ClientCode.ToString(), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, string.IsNullOrEmpty(i.ClientName) ? "-" : i.ClientName, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, string.IsNullOrEmpty(i.CPFCGC) ? "-" : i.CPFCGC, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.TypeDesc, even);
                    even = !even;
                }
                doc.Add(tableItems);
            }

            doc.Close();

            return memStream;
        }

        #endregion

        #region Subscription

        public MemoryStream CreateSubscriptionsToMemoryStream(List<Subscription> list)
        {
            iTextSharp.text.Document doc = new iTextSharp.text.Document(new Rectangle(1371, 970), 25, 25, 25, 45);
            MemoryStream memStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
            writer.CloseStream = false;

            writer.PageEvent = new PrintHeaderFooter { Title = "Subscription" };

            doc.Open();

            Font tradeFont = FontFactory.GetFont("Arial", 30, Font.BOLD, new BaseColor(0, 135, 169));
            Font companyAddressFont = FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(111, 111, 111));
            Font dateFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(111, 111, 111));
            Font lineFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(111, 111, 111));
            Font lineHeaderFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(255, 255, 255));

            AddReportHeader(ref doc, "Consulta de Subscrições", null, string.Empty, string.Empty, string.Empty, null, null);

            if (list.Count > 0)
            {
                PdfPTable tableItems = new PdfPTable(9);
                tableItems.WidthPercentage = 100;

                var colWidthPercentages = new[] { 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f };
                tableItems.SetWidths(colWidthPercentages);
                tableItems.SplitRows = false;

                string[] headerItems = new string[5] { "Empresa", "Ativo", "Cadastro", "Cronograma", "Situação" };
                int[] headerRowspans = new int[5] { 2, 2, 2, 0, 2 };
                int[] headerColspans = new int[5] { 0, 0, 0, 5, 0 };
                AddListHeader(ref tableItems, headerItems, headerRowspans, headerColspans);

                string[] headerItems1 = new string[5] { "Início", "Data Com", "Corretora", "Bolsa", "Data Empresa" };
                int[] headerRowspans1 = new int[5] { 0, 0, 0, 0, 0 };
                int[] headerColspans1 = new int[5] { 0, 0, 0, 0, 0 };
                AddListHeader(ref tableItems, headerItems1, headerRowspans1, headerColspans1);

                bool even = true;
                foreach (Subscription i in list)
                {
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.Company, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.StockCode, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.RequestId.ToString(), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.InitialDate.HasValue ? i.InitialDate.Value.ToShortDateString() : "-", even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.COMDate.HasValue ? i.COMDate.Value.ToShortDateString() : "-", even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, (i.BrokerDate.HasValue ? i.BrokerDate.Value.ToShortDateString() : "-") + (i.BrokerHour.HasValue ? " - " + i.BrokerHour.Value.ToShortTimeString() : string.Empty), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.StockExchangeDate.HasValue ? i.StockExchangeDate.Value.ToShortDateString() : "-", even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.CompanyDate.HasValue ? i.CompanyDate.Value.ToShortDateString() : "-", even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.DescStatus, even);
                    even = !even;
                }
                doc.Add(tableItems);
            }

            doc.Close();

            return memStream;
        }

        public MemoryStream CreateRequestRightsToMemoryStream(Subscription subscription, List<SubscriptionRights> list)
        {
            iTextSharp.text.Document doc = new iTextSharp.text.Document(new Rectangle(1371, 970), 25, 25, 25, 45);
            MemoryStream memStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
            writer.CloseStream = false;

            writer.PageEvent = new PrintHeaderFooter { Title = "RequestRights" };

            doc.Open();

            Font tradeFont = FontFactory.GetFont("Arial", 30, Font.BOLD, new BaseColor(0, 135, 169));
            Font companyAddressFont = FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(111, 111, 111));
            Font dateFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(111, 111, 111));
            Font lineFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(111, 111, 111));
            Font lineHeaderFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(255, 255, 255));

            AddReportHeader(ref doc, "Solicitações de Direitos", null, string.Empty, string.Empty, string.Empty, null, null);

            // Cabeçalho da Subscrição
            Font fontTitle = FontFactory.GetFont("Arial", 16, Font.BOLD, new BaseColor(0, 135, 169));
            Font fontDefault = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(89, 89, 89));

            PdfPTable tableSubscriptionHeader = new PdfPTable(1);
            tableSubscriptionHeader.WidthPercentage = 100;
            tableSubscriptionHeader.HorizontalAlignment = 0;
            tableSubscriptionHeader.SpacingBefore = 20;
            tableSubscriptionHeader.SpacingAfter = 30;

            PdfPCell cellTitulo = new PdfPCell();
            cellTitulo.BorderWidth = 0;
            cellTitulo.PaddingBottom = 5;

            cellTitulo.Phrase = new Phrase(subscription.Company + " - " + subscription.StockCode + " (" + subscription.RightPercentual.Value.ToString("N2") + "%)", fontTitle);
            tableSubscriptionHeader.AddCell(cellTitulo);

            cellTitulo.Phrase = new Phrase(subscription.ISINStock + "     " + subscription.InitialDate.Value.ToShortDateString() + " até " + subscription.StockExchangeDate.Value.ToShortDateString(), fontDefault);
            tableSubscriptionHeader.AddCell(cellTitulo);

            doc.Add(tableSubscriptionHeader);


            if (list.Count > 0)
            {
                PdfPTable tableItems = new PdfPTable(12);
                tableItems.WidthPercentage = 100;

                var colWidthPercentages = new[] { 17f, 5f, 18f, 7f, 6f, 6f, 8f, 8f, 6f, 6f, 6f, 7f };
                tableItems.SetWidths(colWidthPercentages);
                tableItems.SplitRows = false;

                string[] headerItems = new string[12] { "Cliente", "Código", "Assessor", "Qtde. Direito", "Solicitado", "Disponível", "Preço Unitário", "Total $", "Data Limite", "Sobras", "Notificação", "Situação" };
                AddListHeader(ref tableItems, headerItems);

                bool even = true;
                foreach (SubscriptionRights i in list)
                {
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.ClientName, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.ClientCode.ToString(), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.AssessorName, even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.RightsQtty.Value.ToString("N0"), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.RequestedQtty.Value.ToString("N0"), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.AvailableQtty.Value.ToString("N0"), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.RightValue.Value.ToString("N2"), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.TotalValue.Value.ToString("N2"), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.StockExchangeDate.Value.ToShortDateString(), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.DescScraps, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.DescWarning, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.DescStatus, even);
                    even = !even;
                }
                doc.Add(tableItems);
            }

            doc.Close();

            return memStream;
        }

        #endregion

        #region Funds

        //public MemoryStream CreateFundsBalanceToMemoryStream(
        //    FundClient client,
        //    FundBalance balance,
        //    List<FundBalancePendingApplicationItem> pending,
        //    List<FundBalanceChartItemResponse> chart,
        //    byte[] chartImage)
        //{
        //    Document doc = new Document(new Rectangle(1371, 970), 25, 25, 25, 45);
        //    MemoryStream memStream = new MemoryStream();
        //    PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
        //    writer.CloseStream = false;

        //    writer.PageEvent = new PrintHeaderFooter { Title = "FundBalance" };

        //    doc.Open();

        //    Font tradeFont = FontFactory.GetFont("Arial", 30, Font.BOLD, new BaseColor(0, 135, 169));
        //    Font companyAddressFont = FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(111, 111, 111));
        //    Font dateFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(111, 111, 111));
        //    Font lineFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(111, 111, 111));
        //    Font lineHeaderFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(255, 255, 255));

        //    AddReportHeader(ref doc, "Fundos de Investimento - Saldo", null, string.Empty, string.Empty, string.Empty);

        //    #region Fundos - Fundo selecionado

        //    if (balance.Items.Length > 0)
        //    {
        //        AddSimpleHeader(ref doc, balance.FundName);

        //        PdfPTable tableItems = new PdfPTable(10);
        //        tableItems.WidthPercentage = 100;
        //        tableItems.SpacingAfter = 30;

        //        var colWidthPercentages = new[] { 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f };
        //        tableItems.SetWidths(colWidthPercentages);
        //        tableItems.SplitRows = false;

        //        string[] headerItems = new string[10] { "No da Cautela", "No de Cotas", "Data da Aplicação", "Data da Conversão", "Valor da Aplicação", "Valor corrigido", "Previsão de IOF", "Previsão de IR", "Valor do Resgate", "Rendimento" };
        //        AddListHeader(ref tableItems, headerItems);

        //        bool even = true;
        //        foreach (var i in balance.Items)
        //        {
        //            AddListItem(ref tableItems, Element.ALIGN_LEFT, i.CautionNumber, even);
        //            AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.QuantityOfQuota.ToString("N9"), even);
        //            AddListItem(ref tableItems, Element.ALIGN_CENTER, i.ApplicationDate.ToShortDateString(), even);
        //            AddListItem(ref tableItems, Element.ALIGN_CENTER, i.ConversionDate.ToShortDateString(), even);
        //            AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.ApplicationValue.ToString("N2"), even);
        //            AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.CorrectedValue.ToString("N2"), even);
        //            AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.ProvisionIOF.ToString("N2"), even);
        //            AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.ProvisionIRRF.ToString("N2"), even);
        //            AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.RedemptionValue.ToString("N2"), even);
        //            AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.Income.ToString("N2"), even);
        //            even = !even;
        //        }
        //        doc.Add(tableItems);
        //    }

        //    #endregion

        //    #region Fundos - Movimentações pendentes de confirmação

        //    if (pending.Count > 0)
        //    {
        //        AddSimpleHeader(ref doc, "Movimentações Pendentes de Confirmação");

        //        PdfPTable tableItems = new PdfPTable(3);
        //        tableItems.WidthPercentage = 50;
        //        tableItems.SpacingAfter = 30;
        //        tableItems.HorizontalAlignment = 0;

        //        var colWidthPercentages = new[] { 50f, 25f, 25f };
        //        tableItems.SetWidths(colWidthPercentages);
        //        tableItems.SplitRows = false;

        //        string[] headerItems = new string[3] { "Fundo", "Valor da Aplicação", "Tipo" };
        //        AddListHeader(ref tableItems, headerItems);

        //        bool even = true;
        //        foreach (var i in pending)
        //        {
        //            AddListItem(ref tableItems, Element.ALIGN_LEFT, i.FundName, even);
        //            AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.Value.ToString("N2"), even);
        //            AddListItem(ref tableItems, Element.ALIGN_LEFT, i.Type, even);
        //            even = !even;
        //        }
        //        doc.Add(tableItems);
        //    }

        //    #endregion

        //    #region Fundos - Distribuição dos Investimentos

        //    if (chart.Count > 0)
        //    {
        //        doc.NewPage();
        //        AddReportHeader(ref doc, "Fundos de Investimento - Saldo", null, string.Empty, string.Empty, string.Empty);

        //        decimal chartTotal = chart.Sum(p => p.Value);

        //        AddSimpleHeader(ref doc, "Distribuição dos Investimentos");

        //        PdfPTable tableItems = new PdfPTable(4);
        //        tableItems.WidthPercentage = 50;
        //        tableItems.SpacingAfter = 30;
        //        tableItems.HorizontalAlignment = 0;

        //        var colWidthPercentages = new[] { 55f, 15f, 15f, 15f };
        //        tableItems.SetWidths(colWidthPercentages);
        //        tableItems.SplitRows = false;

        //        string[] headerItems = new string[4] { "Fundo", "Composição", "% da Carteira", "Investimento" };
        //        AddListHeader(ref tableItems, headerItems);

        //        bool even = true;
        //        foreach (var i in chart)
        //        {
        //            AddListItemChart(ref tableItems, Element.ALIGN_LEFT, i.FundName, i.RGBColor, even);
        //            AddListItem(ref tableItems, Element.ALIGN_LEFT, i.Composition, even);
        //            AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.Percentage, even);
        //            AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.Value.ToString("N2"), even);
        //            even = !even;
        //        }

        //        if (chartImage != null && chartImage.Length > 0)
        //        {
        //            tableItems.WidthPercentage = 100;

        //            //AddSimpleHeader(ref doc, "Gráfico de Distribuição dos Investimentos");
        //            Image image = Image.GetInstance(chartImage);
        //            image.SpacingAfter = 0;
        //            image.SpacingBefore = 0;
        //            //doc.Add(image);

        //            PdfPTable table1 = new PdfPTable(2);
        //            table1.SetWidths(new[] { 50f, 50f });
        //            table1.WidthPercentage = 100;
        //            table1.SpacingBefore = 0;
        //            table1.SpacingAfter = 0;

        //            PdfPCell cell1 = new PdfPCell();
        //            cell1.Border = 0;
        //            cell1.Padding = 0;
        //            cell1.AddElement(tableItems);

        //            PdfPCell cell2 = new PdfPCell();
        //            cell2.Border = 0;
        //            cell2.Padding = 0;
        //            cell2.AddElement(image);

        //            table1.AddCell(cell1);
        //            table1.AddCell(cell2);

        //            doc.Add(table1);
        //        }
        //        else
        //        {
        //            doc.Add(tableItems);
        //        }
        //    }

        //    #endregion

        //    doc.Close();

        //    return memStream;
        //}

        public MemoryStream CreateFundsAnalyticalStatementToMemoryStream(FundClient client, List<FundStatementItem> statement)
        {
            iTextSharp.text.Document doc = new iTextSharp.text.Document(new Rectangle(1371, 970), 25, 25, 25, 45);
            MemoryStream memStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
            writer.CloseStream = false;

            writer.PageEvent = new PrintHeaderFooter { Title = "FundAnalyticalStatement" };

            doc.Open();

            Font tradeFont = FontFactory.GetFont("Arial", 30, Font.BOLD, new BaseColor(0, 135, 169));
            Font companyAddressFont = FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(111, 111, 111));
            Font dateFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(111, 111, 111));
            Font lineFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(111, 111, 111));
            Font lineHeaderFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(255, 255, 255));

            AddReportHeader(ref doc, "Fundos de Investimento - Extrato Analítico", null, string.Empty, string.Empty, string.Empty);

            if (statement.Count > 0)
            {
                PdfPTable tableItems = new PdfPTable(10);
                tableItems.WidthPercentage = 100;
                tableItems.SpacingAfter = 30;

                var colWidthPercentages = new[] { 12f, 12f, 14f, 8f, 8f, 8f, 8f, 9f, 9f, 8f };
                tableItems.SetWidths(colWidthPercentages);
                tableItems.SplitRows = false;

                string[] headerItems = new string[10] { "Data de Aplicação", "Data de Movimentação", "Lançamento", "Nº da Cautela", "Qtde de Cotas", "Valor Original", "Valor Bruto", "IOF", "IRRF", "Valor" };
                AddListHeader(ref tableItems, headerItems);

                bool even = true;
                foreach (var i in statement)
                {
                    AddListItem(ref tableItems, Element.ALIGN_CENTER, i.ApplicationDate.ToShortDateString(), even);
                    AddListItem(ref tableItems, Element.ALIGN_CENTER, i.MovementDate.ToShortDateString(), even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.MovementType, even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.CautionNumber, even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.QuantityOfQuota.ToString("N9"), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.OriginalValue.ToString("C2"), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.GrossValue.ToString("C2"), even);
                    //AddListItem(ref tableItems, Element.ALIGN_CENTER, QX3.Portal.WebSite.Helper.FundHelper.TaxFormatter(i.IOF, i.OriginalValue), even);
                    //AddListItem(ref tableItems, Element.ALIGN_CENTER, QX3.Portal.WebSite.Helper.FundHelper.TaxFormatter(i.IRRF, i.OriginalValue), even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.Value.ToString("C2"), even);
                    even = !even;
                }
                doc.Add(tableItems);
            }

            doc.Close();

            return memStream;
        }

        //public MemoryStream CreateFundsSyntheticStatementToMemoryStream(FundClient client, List<FundStatementItem> statement)
        //{
        //    Document doc = new Document(new Rectangle(970, 1371), 25, 25, 25, 45);
        //    MemoryStream memStream = new MemoryStream();
        //    PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
        //    writer.CloseStream = false;

        //    writer.PageEvent = new PrintHeaderFooter { Title = "FundSyntheticStatement" };

        //    doc.Open();

        //    Font tradeFont = FontFactory.GetFont("Arial", 30, Font.BOLD, new BaseColor(0, 135, 169));
        //    Font companyAddressFont = FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(111, 111, 111));
        //    Font dateFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(111, 111, 111));
        //    Font lineFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(111, 111, 111));
        //    Font lineHeaderFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(255, 255, 255));

        //    AddReportHeader(ref doc, "Fundos de Investimento - Extrato Sintético", null, string.Empty, string.Empty, string.Empty);

        //    if (statement.Count > 0)
        //    {
        //        PdfPTable tableItems = new PdfPTable(6);
        //        tableItems.WidthPercentage = 100;
        //        tableItems.SpacingAfter = 30;

        //        var colWidthPercentages = new[] { 7f, 20f, 5f, 10f, 10f, 10f };
        //        tableItems.SetWidths(colWidthPercentages);
        //        tableItems.SplitRows = false;

        //        string[] headerItems = new string[6] { "Data", "Lançamento", "Cota", "IOF", "IRRF", "Valor" };
        //        AddListHeader(ref tableItems, headerItems);

        //        DateTime currentDate = DateTime.MinValue;
        //        decimal sum = 0;

        //        bool even = true;
        //        foreach (var i in statement)
        //        {
        //            if (currentDate == DateTime.MinValue)
        //            {
        //                AddFundsSyntheticStatementItem(ref tableItems, Element.ALIGN_CENTER, i.ApplicationDate.AddDays(-1).ToShortDateString(), 0);
        //                AddFundsSyntheticStatementItem(ref tableItems, Element.ALIGN_LEFT, "Saldo Anterior", 4);
        //                AddFundsSyntheticStatementItem(ref tableItems, Element.ALIGN_RIGHT, 0.ToString("C2"), 0);
        //            }

        //            if (currentDate != DateTime.MinValue && currentDate != i.ApplicationDate)
        //            {
        //                AddFundsSyntheticStatementItem(ref tableItems, Element.ALIGN_CENTER, currentDate.ToShortDateString(), 0);
        //                AddFundsSyntheticStatementItem(ref tableItems, Element.ALIGN_LEFT, "Saldo", 4);
        //                AddFundsSyntheticStatementItem(ref tableItems, Element.ALIGN_RIGHT, sum.ToString("C2"), 0);
        //            }

        //            AddListItem(ref tableItems, Element.ALIGN_CENTER, i.ApplicationDate.ToShortDateString(), even);
        //            AddListItem(ref tableItems, Element.ALIGN_LEFT, i.MovementType, even);
        //            AddListItem(ref tableItems, Element.ALIGN_LEFT, "D+0", even);
        //            AddListItem(ref tableItems, Element.ALIGN_CENTER, QX3.Portal.WebSite.Helper.FundHelper.TaxFormatter(i.IOF, i.OriginalValue), even);
        //            AddListItem(ref tableItems, Element.ALIGN_CENTER, QX3.Portal.WebSite.Helper.FundHelper.TaxFormatter(i.IRRF, i.OriginalValue), even);
        //            AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.Value.ToString("C2"), even);

        //            if (i.MovementType.ToLower().IndexOf("resgate") >= 0)
        //            {
        //                sum -= i.Value;
        //            }
        //            else
        //            {
        //                sum += i.Value;
        //            }

        //            currentDate = i.ApplicationDate;

        //            if (i.CautionNumber == statement[statement.Count - 1].CautionNumber)
        //            {
        //                AddFundsSyntheticStatementItem(ref tableItems, Element.ALIGN_CENTER, currentDate.ToShortDateString(), 0);
        //                AddFundsSyntheticStatementItem(ref tableItems, Element.ALIGN_LEFT, "Saldo", 4);
        //                AddFundsSyntheticStatementItem(ref tableItems, Element.ALIGN_RIGHT, sum.ToString("C2"), 0);
        //            }

        //            even = !even;
        //        }
        //        doc.Add(tableItems);
        //    }

        //    doc.Close();

        //    return memStream;
        //}

        //public MemoryStream CreateFundDetailToMemoryStream(FundDetails fundDetails)
        //{
        //    Document doc = new Document(new Rectangle(970, 1371), 25, 25, 25, 45);
        //    MemoryStream memStream = new MemoryStream();
        //    PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
        //    writer.CloseStream = false;

        //    writer.PageEvent = new PrintHeaderFooter { Title = "FundDetails" };

        //    doc.Open();

        //    Font tradeFont = FontFactory.GetFont("Arial", 30, Font.BOLD, new BaseColor(0, 135, 169));
        //    Font companyAddressFont = FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(111, 111, 111));
        //    Font dateFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(111, 111, 111));
        //    Font lineFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(111, 111, 111));
        //    Font lineHeaderFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(255, 255, 255));

        //    AddReportHeader(ref doc, "Fundos de Investimento - Informações do Fundo", null, string.Empty, string.Empty, string.Empty);
        //    AddSimpleHeader(ref doc, fundDetails.Name, 20);
        //    AddSimpleHeader(ref doc, "Informações Gerais");
        //    AddSimpleHeader(ref doc, "Objetivos", 12);
        //    AddSimpleText(ref doc, fundDetails.Goals, 12);
        //    AddSimpleHeader(ref doc, "Público-alvo", 12);
        //    AddSimpleText(ref doc, fundDetails.TargetAudience, 12);

        //    PdfPTable tableItems = new PdfPTable(2);
        //    tableItems.WidthPercentage = 40;
        //    tableItems.HorizontalAlignment = 0;
        //    tableItems.SpacingAfter = 30;

        //    var colWidthPercentages = new[] { 60f, 40f };
        //    tableItems.SetWidths(colWidthPercentages);
        //    tableItems.SplitRows = false;

        //    bool even = true;

        //    AddListItem(ref tableItems, Element.ALIGN_LEFT, "Aplicação inicial mínima", even);
        //    AddListItem(ref tableItems, Element.ALIGN_RIGHT, fundDetails.InitialInvestment.ToString("C2"), even);

        //    AddListItem(ref tableItems, Element.ALIGN_LEFT, "Investimento adicional", !even);
        //    AddListItem(ref tableItems, Element.ALIGN_RIGHT, fundDetails.AdditionalInvestment.ToString("C2"), !even);

        //    AddListItem(ref tableItems, Element.ALIGN_LEFT, "Mínimo para permanência no fundo", even);
        //    AddListItem(ref tableItems, Element.ALIGN_RIGHT, fundDetails.MinimunInvestment.ToString("C2"), even);

        //    AddListItem(ref tableItems, Element.ALIGN_LEFT, "Prazo médio da carteira", !even);
        //    AddListItem(ref tableItems, Element.ALIGN_LEFT, fundDetails.InvestmentHorizon, !even);

        //    AddListItem(ref tableItems, Element.ALIGN_LEFT, "Taxa de administração", even);
        //    AddListItem(ref tableItems, Element.ALIGN_RIGHT, fundDetails.AdministrationFee.ToString("N2") + "%", even);

        //    AddListItem(ref tableItems, Element.ALIGN_LEFT, "Taxa de performance", !even);
        //    AddListItem(ref tableItems, Element.ALIGN_RIGHT, fundDetails.PerfomanceFee.ToString("N2") + "%", !even);

        //    AddListItem(ref tableItems, Element.ALIGN_LEFT, "Taxa de entrada", even);
        //    AddListItem(ref tableItems, Element.ALIGN_RIGHT, fundDetails.EntryFee.ToString("N2") + "%", even);

        //    AddListItem(ref tableItems, Element.ALIGN_LEFT, "Taxa de saída", !even);
        //    AddListItem(ref tableItems, Element.ALIGN_RIGHT, fundDetails.ExitFee.ToString("N2") + "%", !even);

        //    AddListItem(ref tableItems, Element.ALIGN_LEFT, "Cotização de entrada", even);
        //    AddListItem(ref tableItems, Element.ALIGN_RIGHT, "D+" + fundDetails.QuotaEntry.ToString(), even);

        //    AddListItem(ref tableItems, Element.ALIGN_LEFT, "Cotização de saída", !even);
        //    AddListItem(ref tableItems, Element.ALIGN_RIGHT, "D+" + fundDetails.QuotaExit.ToString(), !even);

        //    AddListItem(ref tableItems, Element.ALIGN_LEFT, "Financeiro de entrada", even);
        //    AddListItem(ref tableItems, Element.ALIGN_RIGHT, "D+" + fundDetails.FinancialEntry.ToString(), even);

        //    AddListItem(ref tableItems, Element.ALIGN_LEFT, "Financeiro de saída", !even);
        //    AddListItem(ref tableItems, Element.ALIGN_RIGHT, "D+" + fundDetails.FinancialExit.ToString(), !even);

        //    AddListItem(ref tableItems, Element.ALIGN_LEFT, "Carência", even);
        //    AddListItem(ref tableItems, Element.ALIGN_LEFT, String.IsNullOrEmpty(fundDetails.PeriodOfGrace) ? "-" : fundDetails.PeriodOfGrace, even);

        //    AddListItem(ref tableItems, Element.ALIGN_LEFT, "Liquidez", !even);
        //    AddListItem(ref tableItems, Element.ALIGN_LEFT, fundDetails.Liquidity, !even);

        //    doc.Add(tableItems);

        //    AddSimpleHeader(ref doc, "Informações Complementares");

        //    PdfPTable tableItems1 = new PdfPTable(2);
        //    tableItems1.WidthPercentage = 70;
        //    tableItems1.HorizontalAlignment = 0;
        //    tableItems1.SpacingAfter = 30;

        //    var colWidthPercentages1 = new[] { 35f, 65f };
        //    tableItems1.SetWidths(colWidthPercentages1);
        //    tableItems1.SplitRows = false;

        //    even = true;

        //    AddListItem(ref tableItems1, Element.ALIGN_LEFT, "Razão social", even);
        //    AddListItem(ref tableItems1, Element.ALIGN_LEFT, fundDetails.Name, even);

        //    AddListItem(ref tableItems1, Element.ALIGN_LEFT, "CNPJ", !even);
        //    AddListItem(ref tableItems1, Element.ALIGN_LEFT, String.Format(@"{0:00\.000\.000\/0000\-00}", Convert.ToInt64(fundDetails.CNPJ)), !even);

        //    AddListItem(ref tableItems1, Element.ALIGN_LEFT, "Administrador", even);
        //    AddListItem(ref tableItems1, Element.ALIGN_LEFT, fundDetails.Administration, even);

        //    AddListItem(ref tableItems1, Element.ALIGN_LEFT, "Gestor", !even);
        //    AddListItem(ref tableItems1, Element.ALIGN_LEFT, fundDetails.Management, !even);

        //    AddListItem(ref tableItems1, Element.ALIGN_LEFT, "Regime de cotas", even);
        //    AddListItem(ref tableItems1, Element.ALIGN_LEFT, fundDetails.QuotaSystem, even);

        //    AddListItem(ref tableItems1, Element.ALIGN_LEFT, "Benchmark", !even);
        //    AddListItem(ref tableItems1, Element.ALIGN_LEFT, fundDetails.Benchmark, !even);

        //    AddListItem(ref tableItems1, Element.ALIGN_LEFT, "Código Anbima", even);
        //    AddListItem(ref tableItems1, Element.ALIGN_LEFT, fundDetails.AnbimaCode, even);

        //    AddListItem(ref tableItems1, Element.ALIGN_LEFT, "Classificação Anbima", !even);
        //    AddListItem(ref tableItems1, Element.ALIGN_LEFT, fundDetails.AnbimaClassification, !even);

        //    AddListItem(ref tableItems1, Element.ALIGN_LEFT, "Classificação CVM", even);
        //    AddListItem(ref tableItems1, Element.ALIGN_LEFT, fundDetails.CVMClassification, even);

        //    AddListItem(ref tableItems1, Element.ALIGN_LEFT, "Fundo para investidores qualificados?", !even);
        //    AddListItem(ref tableItems1, Element.ALIGN_LEFT, fundDetails.QualifiedInvestor ? "Sim" : "Não", !even);

        //    AddListItem(ref tableItems1, Element.ALIGN_LEFT, "Horário de movimentação", even);
        //    AddListItem(ref tableItems1, Element.ALIGN_LEFT, fundDetails.MovementEndTime, even);

        //    AddListItem(ref tableItems1, Element.ALIGN_LEFT, "Alavancagem", !even);
        //    AddListItem(ref tableItems1, Element.ALIGN_LEFT, fundDetails.Leverage ? "Sim" : "Não", !even);

        //    AddListItem(ref tableItems1, Element.ALIGN_LEFT, "Permite ativos no exterior?", even);
        //    AddListItem(ref tableItems1, Element.ALIGN_LEFT, fundDetails.AssetsAbroad ? "Sim" : "Não", even);

        //    AddListItem(ref tableItems1, Element.ALIGN_LEFT, "Perda de patrimônio", !even);
        //    AddListItem(ref tableItems1, Element.ALIGN_LEFT, String.IsNullOrEmpty(fundDetails.LossOfProperty) ? "-" : fundDetails.LossOfProperty, !even);

        //    doc.Add(tableItems1);

        //    doc.Close();

        //    return memStream;
        //}

        #endregion

        #region Common

        protected void AddClientHeader(ref PdfPTable tableItems, string client, string email, string phone)
        {
            AddClientHeader(ref tableItems, client, email, phone, string.Empty, string.Empty);
        }

        protected void AddClientHeader(ref PdfPTable tableItems, string client, string email, string phone, string startDate, string startValue)
        {
            Font clientNameFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(0, 135, 169));
            Font contentFont = FontFactory.GetFont("Arial", 11, Font.NORMAL, new BaseColor(111, 111, 111));
            Font titleFont = FontFactory.GetFont("Arial", 11, Font.BOLD, new BaseColor(111, 111, 111));

            PdfPTable clientCell = new PdfPTable(2);
            clientCell.WidthPercentage = 100;
            var colWidthPercentages = new[] { 150, 770 };
            clientCell.SetWidths(colWidthPercentages);

            PdfPCell cell = new PdfPCell();
            cell.Border = 0;
            cell.Colspan = 2;
            cell.AddElement(new Phrase(client, clientNameFont));
            clientCell.AddCell(cell);

            Paragraph parInfo = new Paragraph();
            parInfo.Add(new Phrase("Tel.: ", titleFont));
            parInfo.Add(new Phrase(phone, contentFont));
            cell = new PdfPCell();
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.Border = 0;
            cell.AddElement(parInfo);
            clientCell.AddCell(cell);

            parInfo = new Paragraph();
            parInfo.Add(new Phrase("Email: ", titleFont));
            parInfo.Add(new Phrase(email, contentFont));
            cell = new PdfPCell();
            cell.Border = 0;
            cell.AddElement(parInfo);
            clientCell.AddCell(cell);

            if (!string.IsNullOrEmpty(startDate))
            {
                parInfo = new Paragraph();
                parInfo.Add(new Phrase("Data de Início: ", titleFont));
                parInfo.Add(new Phrase(startDate, contentFont));
                cell = new PdfPCell();
                cell.Border = 0;
                cell.AddElement(parInfo);
                clientCell.AddCell(cell);
            }

            if (!string.IsNullOrEmpty(startValue))
            {
                parInfo = new Paragraph();
                parInfo.Add(new Phrase("Valor Inicial: ", titleFont));
                parInfo.Add(new Phrase(startValue, contentFont));
                cell = new PdfPCell();
                cell.Border = 0;
                cell.AddElement(parInfo);
                clientCell.AddCell(cell);
            }

            PdfPCell tableCell = new PdfPCell(clientCell);
            tableCell.Colspan = tableItems.NumberOfColumns;
            tableCell.Border = 0;
            tableCell.PaddingBottom = 20;
            tableItems.AddCell(tableCell);
        }

        protected void AddReportFooter(ref iTextSharp.text.Document report, ReportFooter footer, ref PdfWriter writer)
        {
            if (footer != null && !string.IsNullOrEmpty(footer.Disclaimer))
            {
                DisclaimerPage = true;

                report.NewPage();

                StringReader disclaimerReader = new StringReader(HttpUtility.HtmlDecode(footer.Disclaimer));
                StyleSheet disclaimerSheet = new StyleSheet();
                disclaimerSheet.LoadTagStyle(HtmlTags.TABLE, HtmlTags.COLOR, "#6F6F6F");
                disclaimerSheet.LoadTagStyle(HtmlTags.TD, HtmlTags.VALIGN, "top");
                disclaimerSheet.LoadTagStyle(HtmlTags.P, HtmlTags.COLOR, "#6F6F6F");
                disclaimerSheet.LoadTagStyle(HtmlTags.TABLE, HtmlTags.FONTSIZE, "11");
                disclaimerSheet.LoadTagStyle(HtmlTags.P, HtmlTags.FONTSIZE, "11");
                //disclaimerSheet.LoadTagStyle(HtmlTags.TD, HtmlTags.COLOR, "#FF0000");
                //disclaimerSheet.LoadTagStyle(HtmlTags.TD, HtmlTags.BORDER, "2//");

                List<IElement> disclaimerObjects = HTMLWorker.ParseToList(disclaimerReader, disclaimerSheet);

                for (int k = 0; k < disclaimerObjects.Count; ++k)
                    report.Add((IElement)disclaimerObjects[k]);
            }
        }

        protected void AddBvReportHeader(ref iTextSharp.text.Document report, string reportTitle, ReportHeader headerInfo, string clientName, string clientAddress, string clientAccount)
        {
            AddReportHeader(ref report, reportTitle, headerInfo, clientName, clientAddress, clientAccount, null, null, false, true);
        }

        protected void AddBvReportHeader(ref iTextSharp.text.Document report, string reportTitle, ReportHeader headerInfo, string clientName, string clientAddress, string clientAccount, DateTime? startDate, DateTime? endDate)
        {
            AddReportHeader(ref report, reportTitle, headerInfo, clientName, clientAddress, clientAccount, startDate, endDate, false, true);
        }

        protected void AddReportHeader(ref iTextSharp.text.Document report, string reportTitle, ReportHeader headerInfo, string clientName, string clientAddress, string clientAccount)
        {
            AddReportHeader(ref report, reportTitle, headerInfo, clientName, clientAddress, clientAccount, null, null, false, false);
        }

        protected void AddReportHeader(ref iTextSharp.text.Document report, string reportTitle, ReportHeader headerInfo, string clientName, string clientAddress, string clientAccount, DateTime? startDate, DateTime? endDate)
        {
            AddReportHeader(ref report, reportTitle, headerInfo, clientName, clientAddress, clientAccount, startDate, endDate, false, false);
        }

        protected void AddReportHeader(ref iTextSharp.text.Document report, string reportTitle, ReportHeader headerInfo, string clientName, string clientAddress, string clientAccount, DateTime? startDate, DateTime? endDate, bool portuguese, bool isBvSecurities, bool? hasLogo = true)
        {
            Font reportNameFont = FontFactory.GetFont("Arial", 30, Font.BOLD, new BaseColor(0, 135, 169));
            Font companyAddressFont = FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(111, 111, 111));
            Font traderNameFont = FontFactory.GetFont("Arial", 16, Font.BOLD, new BaseColor(111, 111, 111));
            Font traderFont = FontFactory.GetFont("Arial", 11, Font.NORMAL, new BaseColor(111, 111, 111));
            Font accountOfFont = FontFactory.GetFont("Arial", 11, Font.BOLD, new BaseColor(111, 111, 111));
            Font accountOfValueFont = FontFactory.GetFont("Arial", 11, Font.NORMAL, new BaseColor(111, 111, 111));

            if (hasLogo.HasValue && hasLogo.Value)
            {
                var logo = Image.GetInstance(System.Web.HttpContext.Current.Server.MapPath(isBvSecurities ? "~/Content/images/logo_rel_confirmation.jpg" : "~/img/logo-brpartners.png"));
                report.Add(logo);
            }

            PdfPTable header = new PdfPTable(2);
            header.WidthPercentage = 100;
            PdfPCell cellHeader = new PdfPCell();
            cellHeader.HorizontalAlignment = Element.ALIGN_RIGHT;
            cellHeader.Border = 0;
            if (!string.IsNullOrEmpty(clientAccount))
                cellHeader.Colspan = 2;
            cellHeader.BorderColor = new BaseColor(255, 255, 255);

            var colWidthPercentages = new[] { 30, 70 };
            header.SetWidths(colWidthPercentages);

            //Empresa
            ReportHeader address = headerInfo;
            if (address != null)
            {
                string addressContent = ChangeCaps(string.Format("{0}, {1} - {2}\n{3} - {4}", address.AddressLine1, address.Number, address.AddressLine2, address.Quarter, address.City));
                Paragraph companyParagraph = new Paragraph(addressContent, companyAddressFont);
                companyParagraph.ExtraParagraphSpace = 0;
                cellHeader.AddElement(companyParagraph);
            }

            //Dados do Cliente
            if (!string.IsNullOrEmpty(clientAddress))
            {
                Paragraph pClientName = new Paragraph(ChangeCaps(clientName), traderNameFont);
                pClientName.SpacingBefore = 15;
                pClientName.SpacingAfter = 5;
                Paragraph addressParagraph = new Paragraph(clientAddress, traderFont);
                addressParagraph.ExtraParagraphSpace = 0;
                cellHeader.AddElement(pClientName);
                cellHeader.AddElement(addressParagraph);
            }
            else
                cellHeader.AddElement(Chunk.NEWLINE);

            header.AddCell(cellHeader);

            if (!string.IsNullOrEmpty(clientAccount))
            {
                Paragraph accParagraph1 = new Paragraph("For the Account of:", accountOfFont);
                Paragraph accParagraph2 = new Paragraph(clientAccount, accountOfValueFont);
                accParagraph2.ExtraParagraphSpace = 0;
                cellHeader = new PdfPCell();
                cellHeader.HorizontalAlignment = Element.ALIGN_LEFT;
                cellHeader.Border = 0;
                cellHeader.PaddingTop = 10;
                cellHeader.BorderColor = new BaseColor(255, 255, 255);
                cellHeader.AddElement(accParagraph1);
                cellHeader.AddElement(accParagraph2);
                header.AddCell(cellHeader);
            }

            cellHeader = new PdfPCell();
            cellHeader.HorizontalAlignment = Element.ALIGN_RIGHT;
            cellHeader.Border = 0;
            cellHeader.VerticalAlignment = Element.ALIGN_BOTTOM;
            cellHeader.BorderColor = new BaseColor(255, 255, 255);

            PdfPTable reportNameTable = new PdfPTable(1);
            reportNameTable.WidthPercentage = 100;
            Paragraph reportNameParagraph = new Paragraph(reportTitle, reportNameFont);
            reportNameParagraph.Alignment = Element.ALIGN_RIGHT;
            PdfPCell cellReportName = new PdfPCell(reportNameParagraph);
            cellReportName.HorizontalAlignment = Element.ALIGN_RIGHT;
            cellReportName.Border = 0;
            reportNameTable.AddCell(cellReportName);

            if (startDate.HasValue)
            {
                string dateText = endDate.HasValue ? string.Format(portuguese ? "de {0} à {1}" : "from {0} to {1}", startDate.Value.ToString("MM/dd/yyyy", cultureInfo), endDate.Value.ToString("MM/dd/yyyy", cultureInfo)) : startDate.Value.ToString("MM/dd/yyyy", cultureInfo);
                Paragraph periodParagraph = new Paragraph(new Phrase(dateText, accountOfFont));
                periodParagraph.ExtraParagraphSpace = 0;
                PdfPCell cellReportPeriod = new PdfPCell(periodParagraph);
                cellReportPeriod.HorizontalAlignment = Element.ALIGN_RIGHT;
                cellReportPeriod.Border = 0;
                cellReportPeriod.PaddingTop = 7;
                reportNameTable.AddCell(cellReportPeriod);
            }
            cellHeader.AddElement(reportNameTable);
            header.AddCell(cellHeader);

            report.Add(header);

            AddLineSeparator(ref report);
        }

        public void AddListHeader(ref PdfPTable table, string[] headerItems)
        {
            Font headerFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(89, 89, 89));

            foreach (string item in headerItems)
            {
                PdfPCell headerCell = new PdfPCell(new Phrase(item, headerFont));
                headerCell.BorderWidthBottom = headerCell.BorderWidthLeft = headerCell.BorderWidthRight = headerCell.BorderWidthTop = 1;
                headerCell.BorderColor = new BaseColor(255, 255, 255);
                headerCell.BackgroundColor = new BaseColor(196, 213, 218);
                headerCell.PaddingLeft = headerCell.PaddingBottom = 7;
                headerCell.PaddingTop = 4;
                headerCell.VerticalAlignment = Element.ALIGN_CENTER;
                headerCell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(headerCell);
            }
        }

        public void AddPortfolioListHeader(ref PdfPTable table, string[] headerItems, int colspan)
        {
            Font headerFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(89, 89, 89));

            foreach (string item in headerItems)
            {
                PdfPCell headerCell = new PdfPCell(new Phrase(item, headerFont));
                headerCell.BorderWidthBottom = headerCell.BorderWidthLeft = headerCell.BorderWidthRight = headerCell.BorderWidthTop = 1;
                headerCell.BorderColor = new BaseColor(255, 255, 255);
                headerCell.BackgroundColor = new BaseColor(196, 213, 218);
                headerCell.PaddingLeft = headerCell.PaddingBottom = 7;
                headerCell.PaddingTop = 4;
                if (item == "Setor" && colspan > 0)
                    headerCell.Colspan = colspan;
                headerCell.VerticalAlignment = Element.ALIGN_CENTER;
                headerCell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(headerCell);
            }
        }

        public void AddListHeader(ref PdfPTable table, string[] headerItems, int[] rowspans, int[] colspans)
        {
            Font headerFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(89, 89, 89));

            int i = 0;

            foreach (string item in headerItems)
            {
                PdfPCell headerCell = new PdfPCell(new Phrase(item, headerFont));

                if (rowspans[i] > 0)
                {
                    headerCell.Rowspan = rowspans[i];
                }
                if (colspans[i] > 0)
                {
                    headerCell.Colspan = colspans[i];
                }
                i++;

                headerCell.BorderWidthBottom = headerCell.BorderWidthLeft = headerCell.BorderWidthRight = headerCell.BorderWidthTop = 1;
                headerCell.BorderColor = new BaseColor(255, 255, 255);
                headerCell.BackgroundColor = new BaseColor(196, 213, 218);
                headerCell.PaddingLeft = headerCell.PaddingBottom = 7;
                headerCell.PaddingTop = 4;
                headerCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                headerCell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(headerCell);
            }
        }

        public void AddListSubHeader(ref PdfPTable table, string[] headerItems)
        {
            Font headerFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(0, 135, 169));

            foreach (string item in headerItems)
            {
                PdfPCell headerCell = new PdfPCell(new Phrase(item, headerFont));
                headerCell.BorderWidthBottom = headerCell.BorderWidthLeft = headerCell.BorderWidthRight = headerCell.BorderWidthTop = 1;
                headerCell.BorderColor = new BaseColor(255, 255, 255);
                headerCell.BackgroundColor = new BaseColor(255, 255, 255);
                headerCell.PaddingLeft = headerCell.PaddingBottom = 7;
                headerCell.PaddingTop = 4;
                headerCell.VerticalAlignment = Element.ALIGN_CENTER;
                headerCell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(headerCell);
            }
        }

        protected void AddListFooterItem(ref PdfPTable table, int hAlignment, string value, int? fontSize = 12, bool? noWrap = false)
        {
            Font footerFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(89, 89, 89));
            if (fontSize.Value != 12)
            {
                footerFont = FontFactory.GetFont("Arial", fontSize.Value, Font.BOLD, new BaseColor(89, 89, 89));
            }
            this.AddListFooterItem(ref table, hAlignment, value, footerFont, noWrap);
        }

        protected void AddListFooterItem(ref PdfPTable table, int hAlignment, string value, Font footerFont, BaseColor backgroundColor, bool? noWrap = false)
        {
            PdfPCell footerCell = new PdfPCell(new Phrase(value, footerFont));
            footerCell.BorderWidthBottom = footerCell.BorderWidthLeft = footerCell.BorderWidthRight = footerCell.BorderWidthTop = 1;
            footerCell.BackgroundColor = backgroundColor;
            footerCell.BorderColor = new BaseColor(255, 255, 255);
            footerCell.PaddingLeft = footerCell.PaddingBottom = 7;
            footerCell.PaddingTop = 3;
            if (hAlignment == Element.ALIGN_RIGHT)
                footerCell.PaddingRight = 7;
            footerCell.VerticalAlignment = Element.ALIGN_CENTER;
            footerCell.HorizontalAlignment = hAlignment;
            if (noWrap.Value)
                footerCell.NoWrap = true;
            table.AddCell(footerCell);
        }

        protected void AddListFooterItem(ref PdfPTable table, int hAlignment, string value, Font footerFont, bool? noWrap = false)
        {
            BaseColor bColor = new BaseColor(208, 208, 208);
            AddListFooterItem(ref table, hAlignment, value, footerFont, bColor, noWrap);
        }

        protected void AddListItem(ref PdfPTable table, int hAlignment, string value, bool even, int? fontSize = 12, bool? noWrap = false)
        {
            Font itemFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(89, 89, 89));
            if (fontSize != null && fontSize.HasValue && fontSize.Value != 12)
            {
                itemFont = FontFactory.GetFont("Arial", fontSize.Value, Font.NORMAL, new BaseColor(89, 89, 89));
            }

            PdfPCell cell = new PdfPCell(new Phrase(value, itemFont));
            cell.BorderWidthBottom = cell.BorderWidthLeft = cell.BorderWidthRight = cell.BorderWidthTop = 1;
            cell.BorderColor = new BaseColor(255, 255, 255);
            cell.HorizontalAlignment = hAlignment;
            cell.PaddingLeft = cell.PaddingBottom = 7;
            cell.PaddingTop = 3;
            if (hAlignment == Element.ALIGN_RIGHT)
                cell.PaddingRight = 7;
            cell.BackgroundColor = even ? new BaseColor(238, 239, 239) : new BaseColor(230, 231, 231);
            if (noWrap.Value)
                cell.NoWrap = true;
            table.AddCell(cell);
        }

        protected void AddListItem(ref PdfPTable table, int hAlignment, int vAlignment, string value, bool even)
        {
            Font itemFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(89, 89, 89));

            PdfPCell cell = new PdfPCell(new Phrase(value, itemFont));
            cell.BorderWidthBottom = cell.BorderWidthLeft = cell.BorderWidthRight = cell.BorderWidthTop = 1;
            cell.BorderColor = new BaseColor(255, 255, 255);
            cell.HorizontalAlignment = hAlignment;
            cell.VerticalAlignment = vAlignment;
            cell.PaddingLeft = cell.PaddingBottom = 7;
            cell.PaddingTop = 3;
            if (hAlignment == Element.ALIGN_RIGHT)
                cell.PaddingRight = 7;
            cell.BackgroundColor = even ? new BaseColor(238, 239, 239) : new BaseColor(230, 231, 231);
            table.AddCell(cell);
        }

        protected void AddListItemChart(ref PdfPTable table, int hAlignment, string value, string color, bool even)
        {
            Font itemFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(89, 89, 89));

            PdfPCell cell = new PdfPCell();
            cell.BorderWidthBottom = cell.BorderWidthLeft = cell.BorderWidthRight = cell.BorderWidthTop = 1;
            cell.BorderColor = new BaseColor(255, 255, 255);
            cell.HorizontalAlignment = hAlignment;
            cell.PaddingLeft = cell.PaddingBottom = 7;
            cell.PaddingTop = 5;
            if (hAlignment == Element.ALIGN_RIGHT)
                cell.PaddingRight = 7;
            cell.BackgroundColor = even ? new BaseColor(238, 239, 239) : new BaseColor(230, 231, 231);
            cell.PaddingBottom = 0;

            PdfPCell cellChart = new PdfPCell();
            cellChart.Border = 0;
            System.Drawing.Color color1 = System.Drawing.ColorTranslator.FromHtml(color);
            cellChart.BackgroundColor = new BaseColor(color1.R, color1.G, color1.B, color1.A);

            PdfPCell cellText = new PdfPCell(new Phrase(value, itemFont));
            cellText.Border = 0;
            cellText.PaddingTop = -2;
            cellText.PaddingLeft = 10;

            PdfPTable tableChartLegend = new PdfPTable(2);
            tableChartLegend.SetWidths(new[] { 5f, 95f });
            tableChartLegend.WidthPercentage = 100;
            tableChartLegend.SpacingAfter = 0;
            tableChartLegend.SpacingBefore = 0;
            tableChartLegend.AddCell(cellChart);
            tableChartLegend.AddCell(cellText);

            cell.AddElement(tableChartLegend);
            table.AddCell(cell);
        }

        protected void AddPortfolioListItem(ref PdfPTable table, int hAlignment, string value, bool even, int colspan)
        {
            Font itemFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(89, 89, 89));

            PdfPCell cell = new PdfPCell(new Phrase(value, itemFont));
            cell.BorderWidthBottom = cell.BorderWidthLeft = cell.BorderWidthRight = cell.BorderWidthTop = 1;
            cell.BorderColor = new BaseColor(255, 255, 255);
            cell.HorizontalAlignment = hAlignment;
            cell.PaddingLeft = cell.PaddingBottom = 7;
            cell.PaddingTop = 3;
            if (hAlignment == Element.ALIGN_RIGHT)
                cell.PaddingRight = 7;
            if (colspan > 1)
                cell.Colspan = colspan;
            cell.BackgroundColor = even ? new BaseColor(238, 239, 239) : new BaseColor(230, 231, 231);
            table.AddCell(cell);
        }

        protected void AddSectorItem(ref PdfPTable table, int hAlignment, string value, bool even, int colspan)
        {
            Font itemFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(89, 89, 89));

            PdfPCell cell = new PdfPCell(new Phrase(value, itemFont));
            cell.BorderWidthBottom = cell.BorderWidthLeft = cell.BorderWidthRight = cell.BorderWidthTop = 1;
            cell.BorderColor = new BaseColor(255, 255, 255);
            cell.HorizontalAlignment = hAlignment;
            cell.PaddingLeft = cell.PaddingBottom = 7;
            if (colspan > 0)
                cell.Colspan = colspan;
            cell.PaddingTop = 3;
            if (hAlignment == Element.ALIGN_RIGHT)
                cell.PaddingRight = 7;
            cell.BackgroundColor = even ? new BaseColor(238, 239, 239) : new BaseColor(230, 231, 231);
            table.AddCell(cell);
        }

        protected void AddFundsSyntheticStatementItem(ref PdfPTable table, int hAlignment, string value, int colspan)
        {
            Font itemFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(89, 89, 89));

            PdfPCell cell = new PdfPCell(new Phrase(value, itemFont));
            cell.BorderWidthBottom = cell.BorderWidthLeft = cell.BorderWidthRight = cell.BorderWidthTop = 1;
            cell.BorderColor = new BaseColor(255, 255, 255);
            cell.HorizontalAlignment = hAlignment;
            cell.PaddingLeft = cell.PaddingBottom = 7;
            if (colspan > 0)
                cell.Colspan = colspan;
            cell.PaddingTop = 3;
            if (hAlignment == Element.ALIGN_RIGHT)
                cell.PaddingRight = 7;
            cell.BackgroundColor = new BaseColor(227, 246, 252);
            table.AddCell(cell);
        }

        protected void AddListValueItem(ref PdfPTable table, int hAlignment, string value, bool even)
        {
            BaseColor negative = new BaseColor(144, 39, 39);
            BaseColor positive = new BaseColor(52, 131, 30);
            Font itemFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, value.IndexOf("-") >= 0 ? negative : positive);

            PdfPCell cell = new PdfPCell(new Phrase(value, itemFont));
            cell.BorderWidthBottom = cell.BorderWidthLeft = cell.BorderWidthRight = cell.BorderWidthTop = 1;
            cell.BorderColor = new BaseColor(255, 255, 255);
            cell.HorizontalAlignment = hAlignment;
            cell.PaddingLeft = cell.PaddingBottom = 7;
            cell.PaddingTop = 3;
            if (hAlignment == Element.ALIGN_RIGHT)
                cell.PaddingRight = 7;
            cell.BackgroundColor = even ? new BaseColor(238, 239, 239) : new BaseColor(230, 231, 231);

            table.AddCell(cell);
        }

        protected void AddListSubItem(ref PdfPTable table, int hAlignment, string value)
        {
            Font itemFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(89, 89, 89));

            PdfPCell cell = new PdfPCell(new Phrase(value, itemFont));
            cell.BorderWidthBottom = cell.BorderWidthLeft = cell.BorderWidthRight = cell.BorderWidthTop = 1;
            cell.BorderColor = new BaseColor(255, 255, 255);
            cell.HorizontalAlignment = hAlignment;
            cell.PaddingLeft = cell.PaddingBottom = 7;
            cell.PaddingTop = 3;
            if (hAlignment == Element.ALIGN_RIGHT)
                cell.PaddingRight = 7;
            cell.BackgroundColor = new BaseColor(241, 242, 242);
            table.AddCell(cell);
        }

        protected void AddLineSeparator(ref iTextSharp.text.Document document)
        {
            PdfPTable tableLines = new PdfPTable(1);
            tableLines.WidthPercentage = 100;
            tableLines.HorizontalAlignment = Element.ALIGN_LEFT;
            tableLines.SpacingBefore = 15;
            tableLines.SpacingAfter = 10;

            LineSeparator line1 = new LineSeparator();
            line1.LineColor = new BaseColor(153, 153, 153);
            line1.LineWidth = 2f;
            line1.Percentage = 100;
            PdfPCell cellLines = new PdfPCell();
            cellLines.Border = 1;
            cellLines.BorderColor = new BaseColor(255, 255, 255);
            cellLines.HorizontalAlignment = Element.ALIGN_LEFT;
            cellLines.AddElement(line1);
            tableLines.AddCell(cellLines);

            LineSeparator line2 = new LineSeparator();
            line2.LineColor = new BaseColor(102, 183, 203);
            line2.LineWidth = 2f;
            line2.Percentage = 100;
            cellLines = new PdfPCell();
            cellLines.Border = 1;
            cellLines.BorderColor = new BaseColor(255, 255, 255);
            cellLines.HorizontalAlignment = Element.ALIGN_LEFT;
            cellLines.AddElement(line2);
            tableLines.AddCell(cellLines);

            document.Add(tableLines);
        }

        protected void AddBlankCells(ref PdfPTable table, int numberOfCells, BaseColor backgroundColor, float border)
        {
            while (numberOfCells > 0)
            {
                PdfPCell blankCell = new PdfPCell(new Phrase(""));
                blankCell.BorderWidthBottom = blankCell.BorderWidthLeft = blankCell.BorderWidthRight = blankCell.BorderWidthTop = border;
                blankCell.BackgroundColor = backgroundColor;
                blankCell.BorderColor = new BaseColor(255, 255, 255);
                table.AddCell(blankCell);
                numberOfCells--;
            }
        }

        protected void AddBlankCells(ref PdfPTable table, int numberOfCells, BaseColor backgroundColor)
        {
            AddBlankCells(ref table, numberOfCells, backgroundColor, 0);
        }

        protected string ChangeCaps(string text)
        {
            string changedText = string.Empty;

            string[] items = text.Split(new Char[] { ' ' });

            if (items.Length > 0)
            {
                StringBuilder sb = new StringBuilder();

                foreach (string item in items)
                {
                    string newWord = string.Empty;

                    if (!string.IsNullOrEmpty(item))
                    {
                        newWord = char.ToUpper(item[0]) + (item.Length > 1 ? item.Substring(1).ToLower() : "");
                        sb.Append(string.Concat(newWord, ' '));
                    }
                }
                changedText = sb.ToString();
            }

            return changedText;
        }

        protected Paragraph PrepareDisclaimerContent(string disclaimerText)
        {
            Paragraph disclaimer = new Paragraph();
            disclaimer.Alignment = Element.ALIGN_JUSTIFIED;

            string[] paragraphs = Regex.Split(disclaimerText, "<br />");

            int i = 0;
            if (paragraphs.Length > 0)
            {
                foreach (string text in paragraphs)
                {
                    Paragraph paragraph = new Paragraph();
                    if (!string.IsNullOrEmpty(paragraphs[i]))
                        PrepareDisclaimerParagraph(paragraphs[i], ref paragraph);
                    else
                        paragraph.Add(Chunk.NEWLINE);
                    disclaimer.Add(paragraph);
                    i++;
                }
            }

            return disclaimer;
        }

        protected void PrepareDisclaimerParagraph(string paragraphContent, ref Paragraph currentParagraph)
        {
            List<Phrase> phrases = new List<Phrase>();
            string tempPhrase = string.Empty;

            for (int i = 0; i < paragraphContent.Length; i++)
            {
                if (i < (paragraphContent.Length - 2) && paragraphContent[i] == '<' && paragraphContent[i + 2] == '>')
                {
                    if (tempPhrase.Length > 0)
                    {
                        Phrase normalPhrase = new Phrase(tempPhrase, FontFactory.GetFont("Arial", 11, Font.NORMAL, new BaseColor(111, 111, 111)));
                        phrases.Add(normalPhrase);
                        tempPhrase = string.Empty;
                    }
                    var tag = paragraphContent.Substring(i, 3);
                    string tags = "<b><u><i>";
                    if (((i + 5) <= paragraphContent.Length) && tags.IndexOf(paragraphContent.Substring(i + 3, 3)) >= 0)
                    {
                        tag += paragraphContent.Substring(i + 3, 3);
                        if (((i + 8) <= paragraphContent.Length) && tags.IndexOf(paragraphContent.Substring(i + 6, 3)) >= 0)
                            tag += paragraphContent.Substring(i + 6, 3);
                    }
                    string text = ExtractStringFromTag(paragraphContent.Substring(i, paragraphContent.Length - i), tag);
                    Font font = FontFactory.GetFont("Arial", 11, GetTagFontStyle(tag), new BaseColor(111, 111, 111));
                    Phrase phrase = new Phrase(text, font);
                    phrases.Add(phrase);
                    i = i + (tag.Length - 1) + text.Length + tag.Length + (tag.Length / 3);
                }
                else
                    tempPhrase += paragraphContent[i];
            }
            if (tempPhrase.Length > 0)
            {
                Phrase normalPhrase = new Phrase(tempPhrase, FontFactory.GetFont("Arial", 11, Font.NORMAL, new BaseColor(111, 111, 111)));
                phrases.Add(normalPhrase);
                tempPhrase = string.Empty;
            }

            foreach (Phrase p in phrases)
                currentParagraph.Add(p);
        }

        protected string ExtractStringFromTag(string paragraphContent, string tag)
        {
            int startIndex = paragraphContent.IndexOf(tag) + tag.Length;
            string endTag = tag.Replace("<", "</");
            if (endTag.Length == 8)
                endTag = endTag.Substring(4, 4) + endTag.Substring(0, 4);
            int endIndex = paragraphContent.IndexOf(endTag, startIndex);
            return paragraphContent.Substring(startIndex, endIndex - startIndex);
        }

        protected Int32 GetTagFontStyle(string tag)
        {
            int fontStyle = Font.NORMAL;
            switch (tag)
            {
                case "<u>":
                    fontStyle = Font.BOLD;
                    break;
                case "<i>":
                    fontStyle = Font.ITALIC;
                    break;
                case "<u><i>":
                case "<i><u>":
                    fontStyle = Font.BOLDITALIC;
                    break;
            }
            return fontStyle;
        }

        protected void AddSimpleHeader(ref iTextSharp.text.Document document, string headerText, int? fontSize = 18)
        {
            Font font = FontFactory.GetFont("Arial", fontSize.Value, Font.BOLD, new BaseColor(0, 135, 169));
            Paragraph p = new Paragraph(headerText, font);
            p.SpacingAfter = 20;
            document.Add(p);
        }

        protected void AddSimpleText(ref iTextSharp.text.Document document, string headerText, int? fontSize = 18)
        {
            Font font = FontFactory.GetFont("Arial", fontSize.Value, Font.BOLD, new BaseColor(89, 89, 89));
            Paragraph p = new Paragraph(headerText, font);
            p.SpacingAfter = 20;
            document.Add(p);
        }

        #endregion

        #region BMF

        public MemoryStream CreateBrokerageReportsToMemoryStream(BrokerageTransferInfo model, List<int> clientList, List<CPClientInformation> clientInfo)
        {
            iTextSharp.text.Document doc = new iTextSharp.text.Document(new Rectangle(970, 1371).Rotate(), 25, 25, 25, 45);
            MemoryStream memStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
            writer.CloseStream = false;

            writer.PageEvent = new PrintHeaderFooter { Title = "Corretagem" };

            doc.Open();

            Font font = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(111, 111, 111));

            AddReportHeader(ref doc, "Corretagem", null, string.Empty, string.Empty, string.Empty, null, null);

            foreach (var i in clientList)
            {
                var client = clientInfo.Where(a => a != null).Where(a => a.ClientCode == i).FirstOrDefault();
                var bovespaList = (model.BovespaInfo == null) ? null : model.BovespaInfo.Where(a => a.ClientCode == i).ToList();
                var bmfList = (model.BMFInfo == null) ? null : model.BMFInfo.Where(a => a.ClientCode == i).ToList();

                bool addedHeader = false;

                Paragraph itemHeader = new Paragraph();

                if (client != null)
                {
                    itemHeader.Add(new Phrase(string.Concat(client.Client.Value, " ", client.Client.Description), font));
                    itemHeader.Add(Chunk.NEWLINE);
                    itemHeader.Add(new Phrase(client.AssessorName, font));
                }
                else
                {
                    itemHeader.Add(new Phrase(i.ToString(), font));
                    itemHeader.Add(Chunk.NEWLINE);
                    //itemHeader.Add(new Phrase("", font));
                }

                //BMF

                if (bmfList != null && bmfList.Count > 0)
                {
                    PdfPTable tableItems = new PdfPTable(6);
                    tableItems.WidthPercentage = 100;
                    tableItems.HorizontalAlignment = Element.ALIGN_LEFT;
                    var colWidthPercentages = new[] { 220f, 490f, 90f, 90f, 90f, 90f };
                    tableItems.SetWidths(colWidthPercentages);
                    tableItems.SplitRows = false;
                    tableItems.KeepTogether = true;
                    tableItems.SpacingBefore = 15;
                    tableItems.SpacingAfter = 15;

                    if (!addedHeader)
                    {
                        PdfPCell cell = new PdfPCell(itemHeader);
                        cell.Colspan = 6;
                        cell.Border = 0;
                        cell.PaddingBottom = 10;
                        tableItems.AddCell(cell);

                        addedHeader = true;
                    }

                    string[] headerItems = new string[5] { "Corretora", "Produto", "Corretagem Bruta", "Repasse de Corretagem", "Receita em R$ (Bruta - Repasse)" };
                    int[] headerRowspans = new int[5] { 2, 2, 2, 0, 2 };
                    int[] headerColspans = new int[5] { 0, 0, 0, 2, 0 };
                    AddListHeader(ref tableItems, headerItems, headerRowspans, headerColspans);

                    string[] headerItems1 = new string[2] { "R$", "%" };
                    int[] headerRowspans1 = new int[2] { 0, 0 };
                    int[] headerColspans1 = new int[2] { 0, 0 };
                    AddListHeader(ref tableItems, headerItems1, headerRowspans1, headerColspans1);

                    bool even = true;

                    foreach (var bmf in bmfList)
                    {
                        AddListItem(ref tableItems, Element.ALIGN_LEFT, bmf.Broker, even);
                        AddListItem(ref tableItems, Element.ALIGN_LEFT, bmf.Product, even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, (bmf.GrossBrokerage.HasValue) ? bmf.GrossBrokerage.Value.ToString("N2") : "-", even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, (bmf.TransferValue.HasValue) ? bmf.TransferValue.Value.ToString("N2") : "-", even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, (bmf.TransferPercentual.HasValue) ? bmf.TransferPercentual.Value.ToString("N2") : "-", even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, (bmf.Revenue.HasValue) ? bmf.Revenue.Value.ToString("N2") : "-", even);

                        even = !even;
                    }


                    AddBlankCells(ref tableItems, 1, new BaseColor(208, 208, 208), 1);
                    AddListFooterItem(ref tableItems, Element.ALIGN_RIGHT, "Total: ");
                    AddListFooterItem(ref tableItems, Element.ALIGN_RIGHT, bmfList.Sum(a => a.GrossBrokerage).HasValue ? bmfList.Sum(a => a.GrossBrokerage).Value.ToString("N2") : "-");
                    AddListFooterItem(ref tableItems, Element.ALIGN_RIGHT, bmfList.Sum(a => a.TransferValue).HasValue ? bmfList.Sum(a => a.TransferValue).Value.ToString("N2") : "-");
                    AddListFooterItem(ref tableItems, Element.ALIGN_RIGHT, bmfList.Sum(a => a.TransferPercentual).HasValue ? bmfList.Sum(a => a.TransferPercentual).Value.ToString("N2") : "-");
                    AddListFooterItem(ref tableItems, Element.ALIGN_RIGHT, bmfList.Sum(a => a.Revenue).HasValue ? bmfList.Sum(a => a.Revenue).Value.ToString("N2") : "-");

                    doc.Add(tableItems);
                }

                //BOVESPA
                if (bovespaList != null && bovespaList.Count > 0)
                {
                    PdfPTable tableItems = new PdfPTable(3);
                    tableItems.WidthPercentage = 100;
                    tableItems.HorizontalAlignment = Element.ALIGN_LEFT;
                    var colWidthPercentages = new[] { 90f, 90f, 90f };
                    tableItems.SetWidths(colWidthPercentages);
                    tableItems.SplitRows = false;
                    tableItems.KeepTogether = true;
                    tableItems.SpacingBefore = 15;
                    tableItems.SpacingAfter = 15;

                    if (!addedHeader)
                    {
                        PdfPCell cell = new PdfPCell(itemHeader);
                        cell.Colspan = 3;
                        cell.Border = 0;
                        cell.PaddingBottom = 10;
                        tableItems.AddCell(cell);

                        addedHeader = true;
                    }

                    string[] headerItems = new string[3] { "Corretagem Bruta (R$)", "Corretagem Líquida (R$)", "Desconto em R$ (Bruta - Líquida)" };
                    AddListHeader(ref tableItems, headerItems);

                    bool even = true;

                    foreach (var bovespa in bovespaList)
                    {
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, (bovespa.GrossBrokerage.HasValue) ? bovespa.GrossBrokerage.Value.ToString("N2") : "-", even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, (bovespa.NetBrokerage.HasValue) ? bovespa.NetBrokerage.Value.ToString("N2") : "-", even);
                        AddListItem(ref tableItems, Element.ALIGN_RIGHT, (bovespa.DiscountValue.HasValue) ? bovespa.DiscountValue.Value.ToString("N2") : "-", even);
                    }

                    doc.Add(tableItems);


                }
            }

            doc.Close();

            return memStream;
        }

        public MemoryStream CreateBrokerageRangeToMemoryStream(BrokerageRange model)
        {
            iTextSharp.text.Document doc = new iTextSharp.text.Document(new Rectangle(970, 1371).Rotate(), 25, 25, 25, 45);
            MemoryStream memStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
            writer.CloseStream = false;

            writer.PageEvent = new PrintHeaderFooter { Title = "Faixa de corretagem" };

            doc.Open();

            Font font = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(111, 111, 111));

            AddReportHeader(ref doc, "Faixa de corretagem", null, string.Empty, string.Empty, string.Empty, null, null);

            bool addedHeader = false;

            Paragraph itemHeader = new Paragraph();

            itemHeader.Add(new Phrase(string.Concat(model.ClientCode, " ", model.ClientName), font));
            itemHeader.Add(Chunk.NEWLINE);
            itemHeader.Add(new Phrase(model.AssessorName, font));

            //BOVESPA
            if (model.Bovespa != null && model.Bovespa.Count > 0)
            {
                PdfPTable tableItems = new PdfPTable(3);
                tableItems.WidthPercentage = 100;
                tableItems.HorizontalAlignment = Element.ALIGN_LEFT;
                var colWidthPercentages = new[] { 90f, 90f, 90f };
                tableItems.SetWidths(colWidthPercentages);
                tableItems.SplitRows = false;
                tableItems.KeepTogether = true;
                tableItems.SpacingBefore = 15;
                tableItems.SpacingAfter = 15;

                if (!addedHeader)
                {
                    PdfPCell cell = new PdfPCell(itemHeader);
                    cell.Colspan = 3;
                    cell.Border = 0;
                    cell.PaddingBottom = 10;
                    tableItems.AddCell(cell);

                    addedHeader = true;
                }

                string[] headerItems = new string[3] { "Mercado", "Normal", "Day Trade" };
                AddListHeader(ref tableItems, headerItems);

                bool even = true;

                foreach (var bovespa in model.Bovespa)
                {
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, !String.IsNullOrEmpty(bovespa.MarketType) ? bovespa.MarketType : "-", even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, (bovespa.NormalOperationRate.HasValue) ? bovespa.NormalOperationRate.Value.ToString("N2") : "-", even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, (bovespa.DayTradeRate.HasValue) ? bovespa.DayTradeRate.Value.ToString("N2") : "-", even);
                }

                doc.Add(tableItems);
            }

            //BMF

            if (model.BMF != null && model.BMF.Count > 0)
            {
                PdfPTable tableItems = new PdfPTable(5);
                tableItems.WidthPercentage = 100;
                tableItems.HorizontalAlignment = Element.ALIGN_LEFT;
                var colWidthPercentages = new[] { 90f, 90f, 90f, 90f, 90f };
                tableItems.SetWidths(colWidthPercentages);
                tableItems.SplitRows = false;
                tableItems.KeepTogether = true;
                tableItems.SpacingBefore = 15;
                tableItems.SpacingAfter = 15;

                if (!addedHeader)
                {
                    PdfPCell cell = new PdfPCell(itemHeader);
                    cell.Colspan = 5;
                    cell.Border = 0;
                    cell.PaddingBottom = 10;
                    tableItems.AddCell(cell);

                    addedHeader = true;
                }

                string[] headerItems = new string[5] { "Ativo", "Contratos", "Mercado", "Normal", "Day Trade" };

                AddListHeader(ref tableItems, headerItems);

                bool even = true;

                var itemListStockCodes = model.BMF.Select(a => a.Stock).Distinct().ToList();

                var BrokerageRangeBMFList = (from a in itemListStockCodes
                                             select new BrokerageBMF()
                                             {
                                                 Stock = a,
                                                 Quantity = model.BMF.Where(b => b.Stock == a).FirstOrDefault().Quantity,
                                                 MarketType = model.BMF.Where(b => b.Stock == a).FirstOrDefault().MarketType,
                                                 NormalOperationRate = model.BMF.Where(b => b.Stock == a).Sum(c => c.NormalOperationRate),
                                                 DayTradeRate = model.BMF.Where(b => b.Stock == a).Sum(c => c.DayTradeRate)
                                             }).ToList();

                foreach (var bmf in BrokerageRangeBMFList)
                {
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, bmf.Stock, even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, (bmf.Quantity.HasValue) ? bmf.Quantity.Value.ToString("N2") : "-", even);
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, bmf.MarketType, even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, (bmf.NormalOperationRate.HasValue) ? bmf.NormalOperationRate.Value.ToString("N2") : "-", even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, (bmf.DayTradeRate.HasValue) ? bmf.DayTradeRate.Value.ToString("N2") : "-", even);

                    var detailList = model.BMF.Where(a => a.Stock == bmf.Stock).ToList();

                    if (detailList != null && detailList.Count > 0)
                    {

                        PdfPCell cell = new PdfPCell();
                        cell.BorderWidthBottom = cell.BorderWidthLeft = cell.BorderWidthRight = cell.BorderWidthTop = 1;
                        cell.BorderColor = new BaseColor(255, 255, 255);
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.PaddingLeft = cell.PaddingBottom = 7;
                        cell.PaddingTop = 3;
                        cell.Colspan = 7;
                        cell.BackgroundColor = even ? new BaseColor(238, 239, 239) : new BaseColor(230, 231, 231);

                        PdfPTable tableSubItems = new PdfPTable(4);
                        tableSubItems.WidthPercentage = 80;

                        tableSubItems.HorizontalAlignment = Element.ALIGN_MIDDLE;
                        colWidthPercentages = new[] { 80f, 80f, 80f, 80f };
                        tableSubItems.SetWidths(colWidthPercentages);

                        string[] headerSubItems = new string[4] { "Vencimento (mês)", "Faixa de Contrato (R$)", "Normal", "Day Trade" };

                        AddListSubHeader(ref tableSubItems, headerSubItems);
                        foreach (BrokerageBMF d in detailList)
                        {
                            //TODO: Mudar
                            AddListSubItem(ref tableSubItems, Element.ALIGN_LEFT, "-");
                            AddListSubItem(ref tableSubItems, Element.ALIGN_LEFT, "-");
                            AddListSubItem(ref tableSubItems, Element.ALIGN_LEFT, d.NormalOperationRate.HasValue ? d.NormalOperationRate.Value.ToString("N2") : "-");
                            AddListSubItem(ref tableSubItems, Element.ALIGN_RIGHT, d.DayTradeRate.HasValue ? d.DayTradeRate.Value.ToString("N2") : "-");
                        }

                        cell.AddElement(tableSubItems);
                        tableItems.AddCell(cell);
                    }
                    even = !even;
                }

                doc.Add(tableItems);
            }


            doc.Close();

            return memStream;
        }

        #endregion

        #region Guarantee

        public MemoryStream CreateGuaranteeToMemoryStream(GuaranteeClient guarantee)
        {
            iTextSharp.text.Document doc = new iTextSharp.text.Document(new Rectangle(1371, 970).Rotate(), 25, 25, 25, 45);
            MemoryStream memStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
            writer.CloseStream = false;

            writer.PageEvent = new PrintHeaderFooter { Title = "GuaranteeClient" };

            doc.Open();

            Font tradeFont = FontFactory.GetFont("Arial", 30, Font.BOLD, new BaseColor(0, 135, 169));
            Font companyAddressFont = FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(111, 111, 111));
            Font dateFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(111, 111, 111));
            Font lineFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(111, 111, 111));
            Font lineHeaderFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(255, 255, 255));

            AddReportHeader(ref doc, "Garantia - " + guarantee.ActivitTypeDesc, null, string.Empty, string.Empty, string.Empty);

            // Cabeçalho
            Font fontTitle = FontFactory.GetFont("Arial", 16, Font.BOLD, new BaseColor(0, 135, 169));
            Font fontDefault = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(89, 89, 89));

            PdfPTable tableGuaranteeHeader = new PdfPTable(1);
            tableGuaranteeHeader.HorizontalAlignment = 0;
            tableGuaranteeHeader.SpacingBefore = 20;
            tableGuaranteeHeader.SpacingAfter = 20;

            PdfPCell cellTitulo = new PdfPCell();
            cellTitulo.BorderWidth = 0;
            cellTitulo.PaddingBottom = 5;

            cellTitulo.Phrase = new Phrase(guarantee.ClientCode + " - " + guarantee.ClientName, fontTitle);
            tableGuaranteeHeader.AddCell(cellTitulo);

            cellTitulo.Phrase = new Phrase(guarantee.AssessorName, fontDefault);
            tableGuaranteeHeader.AddCell(cellTitulo);

            doc.Add(tableGuaranteeHeader);


            // Totais
            PdfPTable tableTotais = new PdfPTable(3);
            tableTotais.HorizontalAlignment = 2;
            tableTotais.SpacingAfter = 30;

            AddListItem(ref tableTotais, Element.ALIGN_CENTER, "Total de garantias: " + guarantee.TotalValue.ToString("C2"), true, null, true);
            AddListItem(ref tableTotais, Element.ALIGN_CENTER, "Chamada de margem: " + guarantee.MarginCall.ToString("C2"), false, null, true);
            AddListItem(ref tableTotais, Element.ALIGN_CENTER, "Saldo livre: " + guarantee.BalanceValue.ToString("C2"), true, null, true);

            doc.Add(tableTotais);


            if (guarantee.Activits.Count > 0)
            {
                PdfPTable tableItems = new PdfPTable(3);
                tableItems.HorizontalAlignment = 0;
                tableItems.SpacingAfter = 20;
                tableItems.SplitRows = false;

                string[] headerItems = new string[3] { "Tipo", "Vencimento", "Total" };
                AddListHeader(ref tableItems, headerItems);

                bool even = true;
                foreach (var i in guarantee.Activits)
                {
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, i.Type, even);
                    AddListItem(ref tableItems, Element.ALIGN_CENTER, i.DueDate != null && i.DueDate != DateTime.MinValue ? i.DueDate.ToShortDateString() : "-", even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, i.TotalValue.ToString("C2"), even);
                    even = !even;
                }

                if (guarantee.Stocks.Count > 0)
                {
                    AddListItem(ref tableItems, Element.ALIGN_LEFT, "Ativos", even);
                    AddListItem(ref tableItems, Element.ALIGN_CENTER, "-", even);
                    AddListItem(ref tableItems, Element.ALIGN_RIGHT, guarantee.Stocks.Sum(p => p.Value).ToString("C2"), even);
                }

                doc.Add(tableItems);
            }
            guarantee.Stocks.AddRange(guarantee.Stocks);
            if (guarantee.Stocks.Count > 0)
            {
                AddSimpleHeader(ref doc, "Ativos");

                PdfPTable tableItems1 = new PdfPTable(3);
                tableItems1.HorizontalAlignment = 0;
                tableItems1.SplitRows = false;

                string[] headerItems1 = new string[3] { "Ativo", "Quantidade", "Valor" };
                AddListHeader(ref tableItems1, headerItems1);

                bool even = true;
                foreach (var i in guarantee.Stocks)
                {
                    AddListItem(ref tableItems1, Element.ALIGN_LEFT, i.Stock, even);
                    AddListItem(ref tableItems1, Element.ALIGN_RIGHT, i.Quantity.ToString("N0"), even);
                    AddListItem(ref tableItems1, Element.ALIGN_RIGHT, i.Value.ToString("C2"), even);
                    even = !even;
                }

                doc.Add(tableItems1);
            }

            doc.Close();

            return memStream;
        }

        #endregion

        #region Cadastro

        public MemoryStream GeneratePDFForIndividualPerson(IndividualPerson model, RegistrationType type)
        {

            ContractInfo PDFInfo = new ContractInfo();
            //PDFInfo.BrokerName = ConfigurationManager.AppSettings["BrokerName"].Trim();



            PDFInfo.PDFPath = ConfigurationManager.AppSettings["PDFGeneratedFilePath"];
            PDFInfo.FileName = "Ficha_cadastral";

            if (!System.IO.Directory.Exists(PDFInfo.PDFPath))
            {
                System.IO.Directory.CreateDirectory(PDFInfo.PDFPath);
            }

            if (type == RegistrationType.PF)
            {
                PDFInfo.PDFModelPath = ConfigurationManager.AppSettings["PDFIndividualModelPF1"].Trim();
                this.CreatePDFIndividual(model, ref PDFInfo);
            }
            else if (type == RegistrationType.PF2)
            {
                PDFInfo.PDFModelPath = ConfigurationManager.AppSettings["PDFIndividualModelPF2"].Trim();
                this.CreatePDFIndividualPF2(model, ref PDFInfo);
            }
            else
            {
                PDFInfo.PDFModelPath = ConfigurationManager.AppSettings["PDFIndividualModelPFCVTM"].Trim();
                this.CreatePDFIndividual(model, ref PDFInfo);
                this.CreatePDFIndividualPF2(model, ref PDFInfo);
            }

            MemoryStream m = new MemoryStream(PDFGenerator.GeneratePDF(PDFInfo));

            return m;

        }

        public MemoryStream GeneratePDFForLegalPerson(LegalPerson model, RegistrationType type)
        {

            ContractInfo PDFInfo = new ContractInfo();

            PDFInfo.PDFPath = ConfigurationManager.AppSettings["PDFGeneratedFilePath"];
            PDFInfo.FileName = "Ficha_cadastral";

            if (!System.IO.Directory.Exists(PDFInfo.PDFPath))
            {
                System.IO.Directory.CreateDirectory(PDFInfo.PDFPath);
            }

            if (type == RegistrationType.PJ)
            {
                PDFInfo.PDFModelPath = ConfigurationManager.AppSettings["PDFIndividualModelPJ1"].Trim();
                this.CreatePDFIndividualPJ1(model, ref PDFInfo);
            }
            else if (type == RegistrationType.PJ2)
            {
                PDFInfo.PDFModelPath = ConfigurationManager.AppSettings["PDFIndividualModelPJ2"].Trim();
                this.CreatePDFIndividualPJ2(model, ref PDFInfo);
            }
            else
            {
                PDFInfo.PDFModelPath = ConfigurationManager.AppSettings["PDFIndividualModelPJCVTM"].Trim();
                this.CreatePDFIndividualPJ1(model, ref PDFInfo);
                this.CreatePDFIndividualPJ2(model, ref PDFInfo);
            }

            MemoryStream m = new MemoryStream(PDFGenerator.GeneratePDF(PDFInfo));

            return m;

        }

        public void CreatePDFIndividual(IndividualPerson model, ref ContractInfo PDFData)
        {
            PDFData.FileName = PDFData.FileName + "_" + model.CPF.Replace(".", "").Replace("-", "");

            List<ContractField> PDFFields = new List<ContractField>();

            PDFFields.Add(new ContractField() { Field = "rdbIsRenew", FieldContent = model.IsRenew ? "S" : "N" });

            PDFFields.Add(new ContractField() { Field = "txtCPF", FieldContent = model.CPF });
            PDFFields.Add(new ContractField() { Field = "txtName", FieldContent = model.Name });
            PDFFields.Add(new ContractField() { Field = "txtBirthDate", FieldContent = model.BirthDate.HasValue ? model.BirthDate.Value.ToShortDateString() : "" });
            PDFFields.Add(new ContractField() { Field = "txtGender", FieldContent = model.Gender.HasValue ? model.Gender.Value == Gender.Male ? "M" : "F" : "" });
            PDFFields.Add(new ContractField() { Field = "txtNationality", FieldContent = model.Nationality });
            PDFFields.Add(new ContractField() { Field = "txtNaturality", FieldContent = model.Naturality });
            PDFFields.Add(new ContractField() { Field = "txtFatherName", FieldContent = model.FatherName });
            PDFFields.Add(new ContractField() { Field = "txtMotherName", FieldContent = model.MotherName });

            if (model.Document != null)
            {
                PDFFields.Add(new ContractField() { Field = "rdbDocumentType", FieldContent = model.Document.DocumentType.HasValue ? ((int)model.Document.DocumentType).ToString() : "" });

                PDFFields.Add(new ContractField() { Field = "txtDocumentNumber", FieldContent = model.Document.DocumentNumber });
                PDFFields.Add(new ContractField() { Field = "txtEmissorDate", FieldContent = model.Document.EmissorDate.HasValue ? model.Document.EmissorDate.Value.ToShortDateString() : "" });
                PDFFields.Add(new ContractField() { Field = "txtEmissor", FieldContent = model.Document.Emissor });

                PDFFields.Add(new ContractField() { Field = "txtDocumentState", FieldContent = model.Document.DocumentState.HasValue ? model.Document.DocumentState.ToString() : "" });

            }

            if (model.CivilState != null)
            {
                PDFFields.Add(new ContractField() { Field = "rdbCivilState", FieldContent = model.CivilState.CivilStateType.HasValue ? ((int)model.CivilState.CivilStateType).ToString() : "" });
                PDFFields.Add(new ContractField() { Field = "rdbPropertyRegime", FieldContent = model.CivilState.PropertyRegime.HasValue ? ((int)model.CivilState.PropertyRegime).ToString() : "" });

                PDFFields.Add(new ContractField() { Field = "txtSpouseName", FieldContent = model.CivilState.SpouseName });
                PDFFields.Add(new ContractField() { Field = "txtSpouseCPF", FieldContent = model.CivilState.SpouseCPF });
            }

            PDFFields.Add(new ContractField() { Field = "rdbPPE", FieldContent = model.PPE.HasValue ? model.PPE.Value ? "S" : "N" : "" });
            PDFFields.Add(new ContractField() { Field = "rdbQualifiedInvestor", FieldContent = model.QualifiedInvestor.HasValue ? model.QualifiedInvestor.Value ? "S" : "N" : "" });

            var residentialAddress = model.Addresses != null ? model.Addresses.Where(a => a.Type == AddressType.Residential).FirstOrDefault() : null;
            if (residentialAddress != null) AddAddress(residentialAddress, ref PDFFields, AddressType.Residential);

            var comercialAddress = model.Addresses != null ? model.Addresses.Where(a => a.Type == AddressType.Comercial).FirstOrDefault() : null;
            if (comercialAddress != null) AddAddress(comercialAddress, ref PDFFields, AddressType.Comercial);

            var otherAddress = model.Addresses != null ? model.Addresses.Where(a => a.Type == AddressType.Other).FirstOrDefault() : null;
            if (otherAddress != null) AddAddress(otherAddress, ref PDFFields, AddressType.Other);

            PDFFields.Add(new ContractField() { Field = "rdbAddressChoice", FieldContent = model.PersonalAddressType.HasValue ? ((int)model.PersonalAddressType.Value).ToString() : "" });

            var residentialPhone = model.Phones != null ? model.Phones.Where(a => a.ContactType == ContactType.ResidentialPhone).FirstOrDefault() : null;
            if (residentialPhone != null) AddPhone(residentialPhone, ref PDFFields, ContactType.ResidentialPhone);

            var comercialPhone = model.Phones != null ? model.Phones.Where(a => a.ContactType == ContactType.ComercialPhone).FirstOrDefault() : null;
            if (comercialPhone != null) AddPhone(comercialPhone, ref PDFFields, ContactType.ComercialPhone);

            var faxphone = model.Phones != null ? model.Phones.Where(a => a.ContactType == ContactType.FaxPhone).FirstOrDefault() : null;
            if (faxphone != null) AddPhone(faxphone, ref PDFFields, ContactType.FaxPhone);

            var cellularPhone = model.Phones != null ? model.Phones.Where(a => a.ContactType == ContactType.CellularPhone).FirstOrDefault() : null;
            if (cellularPhone != null) AddPhone(cellularPhone, ref PDFFields, ContactType.CellularPhone);

            PDFFields.Add(new ContractField() { Field = "txtPersonalMail", FieldContent = model.PersonalMail });
            PDFFields.Add(new ContractField() { Field = "txtProfessionalMail", FieldContent = model.ProfessionalMail });

            PDFFields.Add(new ContractField() { Field = "rdbPersonalContactChoice", FieldContent = model.PersonalChoiceContact.HasValue ? ((int)model.PersonalChoiceContact.Value).ToString() : "" });

            PDFFields.Add(new ContractField() { Field = "txtBestProfessional", FieldContent = model.Occupation != null ? model.Occupation.BestProfessionalOption.HasValue ? (((int)model.Occupation.BestProfessionalOption.Value) + 1).ToString() : "" : "" });

            PDFFields.Add(new ContractField() { Field = "txtOccupation", FieldContent = model.Occupation != null ? model.Occupation.Occupation : "" });
            PDFFields.Add(new ContractField() { Field = "txtCompany", FieldContent = model.Occupation != null ? model.Occupation.Role : "" });
            PDFFields.Add(new ContractField() { Field = "txtRole", FieldContent = model.Occupation != null ? model.Occupation.Company : "" });
            PDFFields.Add(new ContractField() { Field = "txtCompanyCNPJ", FieldContent = model.Occupation != null ? model.Occupation.PropertyCNPJ : "" });

            if (model.DepositAccounts != null)
            {
                for (var i = 0; i < model.DepositAccounts.Count && i <= 2; i++)
                {
                    var acc = model.DepositAccounts[i].Account;

                    PDFFields.Add(new ContractField() { Field = "txtBank" + (i + 1).ToString(), FieldContent = acc != null ? acc.BankCode : "" });
                    PDFFields.Add(new ContractField() { Field = "txtAgency" + (i + 1).ToString(), FieldContent = acc != null ? acc.AgencyCode : "" });
                    PDFFields.Add(new ContractField() { Field = "txtAccount" + (i + 1).ToString(), FieldContent = acc != null ? acc.Account : "" });
                    PDFFields.Add(new ContractField() { Field = "txtBankCoTitular" + (i + 1).ToString(), FieldContent = model.DepositAccounts[i].CoTitularName });
                    PDFFields.Add(new ContractField() { Field = "txtBankCoCPF" + (i + 1).ToString(), FieldContent = model.DepositAccounts[i].CoCPF });

                }
            }

            if (model.References != null)
            {
                for (var i = 0; i < model.References.Count && i <= 2; i++)
                {
                    var refer = model.References[i];

                    var referType = refer != null ? refer.ReferType.HasValue ? ((int)refer.ReferType.Value).ToString() : "" : "";

                    PDFFields.Add(new ContractField() { Field = "rdbReferType" + (i + 1).ToString(), FieldContent = referType });
                    PDFFields.Add(new ContractField() { Field = "txtReferName" + (i + 1).ToString(), FieldContent = refer != null ? refer.Name : "" });
                    PDFFields.Add(new ContractField() { Field = "txtReferDDDCode" + (i + 1).ToString(), FieldContent = refer != null && refer.PhoneCode != 0 ? refer.PhoneCode.ToString() : "" });
                    PDFFields.Add(new ContractField() { Field = "txtReferPhone" + (i + 1).ToString(), FieldContent = refer != null ? refer.PhoneNumber : "" });
                }
            }

            PDFFields.Add(new ContractField() { Field = "txtTotalPatrimony", FieldContent = model.TotalPatrimony.HasValue ? model.TotalPatrimony.Value.ToString("N2") : "" });

            if (model.EstimatedPatrimony != null)
            {
                PDFFields.Add(new ContractField() { Field = "rdbPatrimonial", FieldContent = ((int)model.EstimatedPatrimony.Yield).ToString() });
                PDFFields.Add(new ContractField() { Field = "txtEstimatedPatrimony", FieldContent = model.EstimatedPatrimony.Other.HasValue ? model.EstimatedPatrimony.Other.Value.ToString("N2") : "" });
            }

            if (model.EstimatedPatrimonyComposition != null)
            {
                if (model.EstimatedPatrimonyComposition.ApplicationPerc.HasValue)
                {
                    PDFFields.Add(new ContractField() { Field = "txtApplicationPerc", FieldContent = model.EstimatedPatrimonyComposition.ApplicationPerc.Value.ToString("N2") });
                    PDFFields.Add(new ContractField() { Field = "rdbApplicationPerc", FieldContent = "S" });
                }

                if (model.EstimatedPatrimonyComposition.CorporatePerc.HasValue)
                {
                    PDFFields.Add(new ContractField() { Field = "txtCorporatePerc", FieldContent = model.EstimatedPatrimonyComposition.CorporatePerc.Value.ToString("N2") });
                    PDFFields.Add(new ContractField() { Field = "rdbCorporatePerc", FieldContent = "S" });
                }

                if (model.EstimatedPatrimonyComposition.HabitationPerc.HasValue)
                {
                    PDFFields.Add(new ContractField() { Field = "txtHabitationPerc", FieldContent = model.EstimatedPatrimonyComposition.HabitationPerc.Value.ToString("N2") });
                    PDFFields.Add(new ContractField() { Field = "rdbHabitationPerc", FieldContent = "S" });
                }

                if (model.EstimatedPatrimonyComposition.LandPerc.HasValue)
                {
                    PDFFields.Add(new ContractField() { Field = "txtLandPerc", FieldContent = model.EstimatedPatrimonyComposition.LandPerc.Value.ToString("N2") });
                    PDFFields.Add(new ContractField() { Field = "rdbLandPerc", FieldContent = "S" });
                }

                if (model.EstimatedPatrimonyComposition.OtherPerc.HasValue)
                {
                    PDFFields.Add(new ContractField() { Field = "txtOtherPerc", FieldContent = model.EstimatedPatrimonyComposition.OtherPerc.Value.ToString("N2") });
                    PDFFields.Add(new ContractField() { Field = "rdbOtherPerc", FieldContent = "S" });
                }
            }

            if (model.PatrimonyOrigins != null)
            {
                foreach (var p in model.PatrimonyOrigins)
                {
                    PDFFields.Add(new ContractField() { Field = "rdbPatrimonyOrigin" + p.ToString(), FieldContent = ((int)p).ToString() });
                }
            }

            PDFFields.Add(new ContractField() { Field = "txtPatrimonyOrigin", FieldContent = model.OtherOrigins });


            if (model.Liquidity != null)
            {
                PDFFields.Add(new ContractField() { Field = "rdbLiquidity", FieldContent = ((int)model.Liquidity.Value).ToString() });
                PDFFields.Add(new ContractField() { Field = "txtLiquidity", FieldContent = model.Liquidity.Other.HasValue ? model.Liquidity.Other.Value.ToString("N2") : "" });
            }

            if (model.Investment != null)
            {
                PDFFields.Add(new ContractField() { Field = "rdbEstimatedInvestments", FieldContent = ((int)model.Investment.Value).ToString() });
                PDFFields.Add(new ContractField() { Field = "txtEstimatedInvestments", FieldContent = model.Investment.Other.HasValue ? model.Investment.Other.Value.ToString("N2") : "" });
            }


            if (model.AnnualIncome != null)
            {
                PDFFields.Add(new ContractField() { Field = "rdbAnnualIncome", FieldContent = ((int)model.AnnualIncome.Value).ToString() });
                PDFFields.Add(new ContractField() { Field = "txtAnnualIncome", FieldContent = model.AnnualIncome.Other.HasValue ? model.AnnualIncome.Other.Value.ToString("N2") : "" });
            }

            if (model.Transaction != null)
            {
                PDFFields.Add(new ContractField() { Field = "rdbTransaction", FieldContent = ((int)model.Transaction.Value).ToString() });
                PDFFields.Add(new ContractField() { Field = "txtTransaction", FieldContent = model.Transaction.Other.HasValue ? model.Transaction.Other.Value.ToString("N2") : "" });
            }

            if (PDFData.ContractFields == null || PDFData.ContractFields.Count == 0)
                PDFData.ContractFields = PDFFields;
            else
                PDFData.ContractFields.AddRange(PDFFields);
        }

        public void CreatePDFIndividualPF2(IndividualPerson model, ref ContractInfo PDFData)
        {
            PDFData.FileName = PDFData.FileName + "_" + model.CPF.Replace(".", "").Replace("-", "");

            List<ContractField> PDFFields = new List<ContractField>();

            PDFFields.Add(new ContractField() { Field = "txtCPF", FieldContent = model.CPF });
            PDFFields.Add(new ContractField() { Field = "txtName", FieldContent = model.Name });

            PDFFields.Add(new ContractField() { Field = "txtAdministrator", FieldContent = model.Administrator });
            PDFFields.Add(new ContractField() { Field = "txtCVM", FieldContent = model.CVM });
            PDFFields.Add(new ContractField() { Field = "txtRDE", FieldContent = model.RDE });
            PDFFields.Add(new ContractField() { Field = "txtOriginCountry", FieldContent = model.OriginCountry });
            PDFFields.Add(new ContractField() { Field = "txtCustodian", FieldContent = model.Custodian });
            PDFFields.Add(new ContractField() { Field = "txtLegalRepresentative", FieldContent = model.LegalRepresentative });
            PDFFields.Add(new ContractField() { Field = "txtTributeRepresentative", FieldContent = model.TributeRepresentative });
            PDFFields.Add(new ContractField() { Field = "txtCoLegalRepresentative", FieldContent = model.CoLegalRepresentative });
            PDFFields.Add(new ContractField() { Field = "txtCollectiveAccountTitular", FieldContent = model.CollectiveAccountTitular });

            PDFFields.Add(new ContractField() { Field = "rdbCollectiveAccount", FieldContent = model.CollectiveAccount.HasValue ? model.CollectiveAccount.Value == true ? "S" : "N" : "" });

            if (model.ImobileGoods != null)
            {
                for (var i = 0; i < model.ImobileGoods.Count && i < 3; i++)
                {
                    var address = model.ImobileGoods[i].Address;
                    PDFFields.Add(new ContractField() { Field = "txtImobileType" + (i + 1).ToString(), FieldContent = model.ImobileGoods[i].Type.HasValue ? model.ImobileGoods[i].Type.GetDescription() : "" });
                    PDFFields.Add(new ContractField() { Field = "txtImobileValue" + (i + 1).ToString(), FieldContent = model.ImobileGoods[i].Value.HasValue ? model.ImobileGoods[i].Value.Value.ToString("N2") : "" });
                    PDFFields.Add(new ContractField() { Field = "txtImobileAddress" + (i + 1).ToString(), FieldContent = address != null ? address.AddressLine1 : "" });
                    PDFFields.Add(new ContractField() { Field = "txtImobileQuarter" + (i + 1).ToString(), FieldContent = address != null ? address.Quarter : "" });
                    PDFFields.Add(new ContractField() { Field = "txtImobileCity" + (i + 1).ToString(), FieldContent = address != null ? address.City : "" });
                    PDFFields.Add(new ContractField() { Field = "txtImobileState" + (i + 1).ToString(), FieldContent = address != null ? address.State.HasValue ? address.State.Value.ToString() : "" : "" });
                    PDFFields.Add(new ContractField() { Field = "txtImobilePostalCode" + (i + 1).ToString(), FieldContent = address != null ? address.PostalCode : "" });
                    PDFFields.Add(new ContractField() { Field = "txtImobileCountry" + (i + 1).ToString(), FieldContent = address != null ? address.StateCountry : "" });
                }
            }

            if (model.MobileGoods != null)
            {
                for (var i = 0; i < model.MobileGoods.Count && i < 4; i++)
                {
                    PDFFields.Add(new ContractField() { Field = "rdbMobileType" + (i + 1).ToString(), FieldContent = model.MobileGoods[i].Type.HasValue ? ((int)model.MobileGoods[i].Type).ToString() : "" });
                    PDFFields.Add(new ContractField() { Field = "txtMobileValue" + (i + 1).ToString(), FieldContent = model.MobileGoods[i].Value.HasValue ? model.MobileGoods[i].Value.Value.ToString("N2") : "" });
                    PDFFields.Add(new ContractField() { Field = "txtMobileDescription" + (i + 1).ToString(), FieldContent = model.MobileGoods[i].Description });

                }
            }

            var mensal = model.MensalRendiments.Where(a => a.Type == MensalRendimentType.ProLabore || a.Type == MensalRendimentType.Salary).FirstOrDefault();
            if (mensal != null)
            {
                PDFFields.Add(new ContractField() { Field = "txtMensalType1", FieldContent = mensal.Type.GetDescription().ToString() });
                PDFFields.Add(new ContractField() { Field = "txtMensalValue1", FieldContent = mensal.Value.HasValue ? mensal.Value.Value.ToString("N2") : "" });
            }

            mensal = model.MensalRendiments.Where(a => a.Type == MensalRendimentType.Other).FirstOrDefault();
            if (mensal != null)
            {
                PDFFields.Add(new ContractField() { Field = "txtMensalType2", FieldContent = mensal.Description });
                PDFFields.Add(new ContractField() { Field = "txtMensalValue2", FieldContent = mensal.Value.HasValue ? mensal.Value.Value.ToString("N2") : "" });
            }

            PDFFields.Add(new ContractField() { Field = "rdbOperatesOnItsOwn", FieldContent = model.OperatesOnItsOwn.HasValue ? model.OperatesOnItsOwn.Value == true ? "S" : "N" : "" });
            PDFFields.Add(new ContractField() { Field = "rdbAuthorizesTransmissionOrders", FieldContent = model.AuthorizesTransmissionOrders.HasValue ? model.AuthorizesTransmissionOrders.Value == true ? "S" : "N" : "" });

            if (model.AuthorizedMembers != null)
            {
                for (var i = 0; i < 4 && i < model.AuthorizedMembers.Count; i++)
                {
                    var member = model.AuthorizedMembers[i];

                    PDFFields.Add(new ContractField() { Field = "txtAuthorizedMemberName" + (i + 1).ToString(), FieldContent = member.Name });
                    PDFFields.Add(new ContractField() { Field = "txtAuthorizedMemberCPF" + (i + 1).ToString(), FieldContent = member.CPF });
                    PDFFields.Add(new ContractField() { Field = "txtAuthorizedMemberEmail" + (i + 1).ToString(), FieldContent = member.Email });
                    PDFFields.Add(new ContractField() { Field = "txtAuthorizedMemberRG" + (i + 1).ToString(), FieldContent = member.RG });
                    PDFFields.Add(new ContractField() { Field = "txtAuthorizedMemberBirthDate" + (i + 1).ToString(), FieldContent = member.BirthDate.HasValue ? member.BirthDate.Value.ToShortDateString() : "" });
                }
            }

            if (model.AuthorizedAccounts != null)
            {
                for (var i = 0; i < 2 && i < model.AuthorizedAccounts.Count; i++)
                {
                    var account = model.AuthorizedAccounts[i];

                    PDFFields.Add(new ContractField() { Field = "txtAuthorizedAccountTitular" + (i + 1).ToString(), FieldContent = account.TitularName });
                    PDFFields.Add(new ContractField() { Field = "txtAuthorizedAccountCPF" + (i + 1).ToString(), FieldContent = account.CPF });
                    PDFFields.Add(new ContractField() { Field = "txtAuthorizedAccountCoTitular" + (i + 1).ToString(), FieldContent = account.CoTitularName });
                    PDFFields.Add(new ContractField() { Field = "txtAuthorizedAccountCoCPF" + (i + 1).ToString(), FieldContent = account.CoCPF });
                    PDFFields.Add(new ContractField() { Field = "txtAuthorizedAccountBank" + (i + 1).ToString(), FieldContent = account.Account != null ? account.Account.BankCode : "" });
                    PDFFields.Add(new ContractField() { Field = "txtAuthorizedAccountAgency" + (i + 1).ToString(), FieldContent = account.Account != null ? account.Account.AgencyCode : "" });
                    PDFFields.Add(new ContractField() { Field = "txtAuthorizedAccount" + (i + 1).ToString(), FieldContent = account.Account != null ? account.Account.Account : "" });
                }
            }

            PDFFields.Add(new ContractField() { Field = "rdbBrokerBound", FieldContent = model.BrokerBound.HasValue ? model.BrokerBound.Value == true ? "S" : "N" : "" });
            PDFFields.Add(new ContractField() { Field = "rdbApprovalTerm", FieldContent = model.ApprovalTerm.HasValue ? ((int)model.ApprovalTerm.Value).ToString() : "" });
            PDFFields.Add(new ContractField() { Field = "txtTotalPatrimony", FieldContent = model.TotalPatrimony.HasValue ? model.TotalPatrimony.Value.ToString("N2") : "" });


            if (PDFData.ContractFields == null || PDFData.ContractFields.Count == 0)
                PDFData.ContractFields = PDFFields;
            else
                PDFData.ContractFields.AddRange(PDFFields);
        }

        public void CreatePDFIndividualPJ1(LegalPerson model, ref ContractInfo PDFData)
        {
            PDFData.FileName = PDFData.FileName + "_" + model.CNPJ.Replace(".", "").Replace("-", "").Replace("/", "");

            List<ContractField> PDFFields = new List<ContractField>();

            PDFFields.Add(new ContractField() { Field = "rdbIsRenew", FieldContent = model.IsRenew ? "S" : "N" });

            PDFFields.Add(new ContractField() { Field = "txtCNPJ", FieldContent = model.CNPJ });
            PDFFields.Add(new ContractField() { Field = "txtCorporateName", FieldContent = model.CorporateName });
            PDFFields.Add(new ContractField() { Field = "txtFancyName", FieldContent = model.FancyName });
            PDFFields.Add(new ContractField() { Field = "txtNIRE", FieldContent = model.NIRE });
            PDFFields.Add(new ContractField() { Field = "txtStateInscription", FieldContent = model.StateInscription });
            PDFFields.Add(new ContractField() { Field = "txtPrincipalActivity", FieldContent = model.PrincipalActivity });
            PDFFields.Add(new ContractField() { Field = "txtConstitutionDate", FieldContent = model.ConstitutionDate.HasValue ? model.ConstitutionDate.Value.ToShortDateString() : "" });

            PDFFields.Add(new ContractField() { Field = "rdbConstitutionForm", FieldContent = model.ConstitutionForm.HasValue ? ((int)model.ConstitutionForm.Value).ToString() : "" });

            if (model.ConstitutionForm.HasValue && model.ConstitutionForm.Value == ConstitutionForm.Other)
                PDFFields.Add(new ContractField() { Field = "txtConstitutionFormOtherDescription", FieldContent = model.ConstitutionFormOtherDescription });

            PDFFields.Add(new ContractField() { Field = "rdbOpenCapital", FieldContent = model.OpenCapital.HasValue ? model.OpenCapital.Value ? "S" : "N" : "" });
            PDFFields.Add(new ContractField() { Field = "rdbStockControlType", FieldContent = model.StockControlType.HasValue ? ((int)model.StockControlType.Value).ToString() : "" });
            PDFFields.Add(new ContractField() { Field = "rdbCapitalType", FieldContent = model.CapitalType.HasValue ? ((int)model.CapitalType.Value).ToString() : "" });

            PDFFields.Add(new ContractField() { Field = "rdbQualifiedInvestor", FieldContent = model.QualifiedInvestor.HasValue ? model.QualifiedInvestor.Value ? "S" : "N" : "" });

            if (model.Address != null)
            {
                PDFFields.Add(new ContractField() { Field = "txtAddress", FieldContent = model.Address.AddressLine1 });
                PDFFields.Add(new ContractField() { Field = "txtQuarter", FieldContent = model.Address.Quarter });
                PDFFields.Add(new ContractField() { Field = "txtCity", FieldContent = model.Address.City });
                PDFFields.Add(new ContractField() { Field = "txtState", FieldContent = model.Address.State.HasValue ? model.Address.State.Value.ToString() : "" });
                PDFFields.Add(new ContractField() { Field = "txtPostalCode", FieldContent = model.Address.PostalCode });
                PDFFields.Add(new ContractField() { Field = "txtStateCountry", FieldContent = model.Address.StateCountry });
                PDFFields.Add(new ContractField() { Field = "txtSite", FieldContent = model.Address.AddressLine2 });
            }

            if (model.Contacts != null)
            {
                var comercialPhone = model.Contacts != null ? model.Contacts.Where(a => a.ContactType == ContactType.ComercialPhone).FirstOrDefault() : null;
                if (comercialPhone != null) AddPhone(comercialPhone, ref PDFFields, ContactType.ComercialPhone);

                var faxPhone = model.Contacts != null ? model.Contacts.Where(a => a.ContactType == ContactType.FaxPhone).FirstOrDefault() : null;
                if (faxPhone != null) AddPhone(faxPhone, ref PDFFields, ContactType.FaxPhone);
            }

            PDFFields.Add(new ContractField() { Field = "txtEmail1", FieldContent = model.Email1 });
            PDFFields.Add(new ContractField() { Field = "txtEmail2", FieldContent = model.Email2 });

            if (model.BaseData.HasValue)
            {
                PDFFields.Add(new ContractField() { Field = "txtDataBaseDia", FieldContent = model.BaseData.Value.Day.ToString() });
                PDFFields.Add(new ContractField() { Field = "txtDataBaseMes", FieldContent = model.BaseData.Value.Month.ToString() });
                PDFFields.Add(new ContractField() { Field = "txtDataBaseAno", FieldContent = model.BaseData.Value.Year.ToString() });
            }

            PDFFields.Add(new ContractField() { Field = "txtCapital", FieldContent = model.Capital.HasValue ? model.Capital.Value.ToString("N2") : "" });
            PDFFields.Add(new ContractField() { Field = "txtIntegratedCapital", FieldContent = model.IntegratedCapital.HasValue ? model.IntegratedCapital.Value.ToString("N2") : "" });
            PDFFields.Add(new ContractField() { Field = "txtNetWorth", FieldContent = model.NetWorth.HasValue ? model.NetWorth.Value.ToString("N2") : "" });

            PDFFields.Add(new ContractField() { Field = "txtAccountingContactName", FieldContent = model.AccountingContactName });
            PDFFields.Add(new ContractField() { Field = "txtAccountingContactPhoneDDDCode", FieldContent = !(String.IsNullOrEmpty(model.AccountingContactPhone)) ? model.AccountingContactPhone.Substring(1, 2) : "" });
            PDFFields.Add(new ContractField() { Field = "txtAccountingContactPhone", FieldContent = !(String.IsNullOrEmpty(model.AccountingContactPhone)) ? model.AccountingContactPhone.Substring(5, 9) : "" });

            if (model.StockControls != null)
            {
                for (var i = 0; i < model.StockControls.Count && i <= 4; i++)
                {
                    var stc = model.StockControls[i];

                    PDFFields.Add(new ContractField() { Field = "txtStockControlName" + (i + 1).ToString(), FieldContent = stc.Name });
                    PDFFields.Add(new ContractField() { Field = "txtStockControlCPF" + (i + 1).ToString(), FieldContent = stc.CPFCNPJ });
                    PDFFields.Add(new ContractField() { Field = "txtStockControlEntryDate" + (i + 1).ToString(), FieldContent = stc.EntryDate.HasValue ? stc.EntryDate.Value.ToShortDateString() : "" });
                    PDFFields.Add(new ContractField() { Field = "txtStockControlParticipation" + (i + 1).ToString(), FieldContent = stc.Participation.HasValue ? stc.Participation.Value.ToString("N2") : "" });
                }
            }

            if (model.ControlledLegalPerson != null)
            {
                for (var i = 0; i < model.ControlledLegalPerson.Count && i <= 2; i++)
                {
                    var stc = model.ControlledLegalPerson[i];

                    PDFFields.Add(new ContractField() { Field = "txtControlledLegalPerson" + (i + 1).ToString(), FieldContent = stc.CorporateName });
                    PDFFields.Add(new ContractField() { Field = "txtControlledLegalPersonCNPJ" + (i + 1).ToString(), FieldContent = stc.CPFCNPJ });
                }
            }

            if (model.AffiliatedLegalPerson != null)
            {
                for (var i = 0; i < model.AffiliatedLegalPerson.Count && i <= 2; i++)
                {
                    var stc = model.AffiliatedLegalPerson[i];

                    PDFFields.Add(new ContractField() { Field = "txtAffiliatedLegalPerson" + (i + 1).ToString(), FieldContent = stc.CorporateName });
                    PDFFields.Add(new ContractField() { Field = "txtAffiliatedLegalPersonCNPJ" + (i + 1).ToString(), FieldContent = stc.CPFCNPJ });
                }
            }

            if (model.AttorneyLegalPerson != null)
            {
                for (var i = 0; i < model.AttorneyLegalPerson.Count && i <= 4; i++)
                {
                    var stc = model.AttorneyLegalPerson[i];

                    PDFFields.Add(new ContractField() { Field = "txtAttorneyLegalPerson" + (i + 1).ToString(), FieldContent = stc.CorporateName });
                    PDFFields.Add(new ContractField() { Field = "txtAttorneyLegalPersonCNPJ" + (i + 1).ToString(), FieldContent = stc.CPFCNPJ });
                    PDFFields.Add(new ContractField() { Field = "txtAttorneyLegalPersonBirthDate" + (i + 1).ToString(), FieldContent = stc.BirthDate.HasValue ? stc.BirthDate.Value.ToShortDateString() : "" });
                    PDFFields.Add(new ContractField() { Field = "txtAttorneyLegalPersonRole" + (i + 1).ToString(), FieldContent = stc.Role });
                }
            }

            if (model.ReferenceAccounts != null)
            {
                for (var i = 0; i < model.ReferenceAccounts.Count && i <= 3; i++)
                {
                    var acc = model.ReferenceAccounts[i].Account;

                    PDFFields.Add(new ContractField() { Field = "txtBank" + (i + 1).ToString(), FieldContent = acc != null ? acc.BankCode : "" });
                    PDFFields.Add(new ContractField() { Field = "txtAgency" + (i + 1).ToString(), FieldContent = acc != null ? acc.AgencyCode : "" });
                    PDFFields.Add(new ContractField() { Field = "txtAccount" + (i + 1).ToString(), FieldContent = acc != null ? acc.Account : "" });
                }
            }

            if (model.References != null)
            {
                for (var i = 0; i < model.References.Count && i <= 3; i++)
                {
                    var refer = model.References[i];

                    PDFFields.Add(new ContractField() { Field = "rdbReferType" + (i + 1).ToString(), FieldContent = refer != null ? refer.ReferType.HasValue ? ((int)refer.ReferType.Value).ToString() : "" : "" });
                    PDFFields.Add(new ContractField() { Field = "txtReferName" + (i + 1).ToString(), FieldContent = refer != null ? refer.Name : "" });
                    PDFFields.Add(new ContractField() { Field = "txtReferPhone" + (i + 1).ToString(), FieldContent = refer != null && refer.PhoneCode != 0 ? "(" + refer.PhoneCode + ")" + refer.PhoneNumber : "" });
                }
            }

            if (model.AuthorizedAccountsSame != null)
            {
                for (var i = 0; i < model.AuthorizedAccountsSame.Count && i <= 3; i++)
                {
                    var acc = model.AuthorizedAccountsSame[i].Account;

                    PDFFields.Add(new ContractField() { Field = "txtBankAccount" + (i + 1).ToString(), FieldContent = acc != null ? acc.BankCode : "" });
                    PDFFields.Add(new ContractField() { Field = "txtAgencyAccount" + (i + 1).ToString(), FieldContent = acc != null ? acc.AgencyCode : "" });
                    PDFFields.Add(new ContractField() { Field = "txtAccountAccount" + (i + 1).ToString(), FieldContent = acc != null ? acc.Account : "" });
                }
            }

            if (model.AuthorizedAccountsOthers != null)
            {
                for (var i = 0; i < model.AuthorizedAccountsOthers.Count && i <= 3; i++)
                {
                    PDFFields.Add(new ContractField() { Field = "txtAuthorizedAccountTitular" + (i + 1).ToString(), FieldContent = model.AuthorizedAccountsOthers[i].TitularName });
                    PDFFields.Add(new ContractField() { Field = "txtAuthorizedAccountCPF" + (i + 1).ToString(), FieldContent = model.AuthorizedAccountsOthers[i].CPF });
                    PDFFields.Add(new ContractField() { Field = "txtAuthorizedAccountCoTitular" + (i + 1).ToString(), FieldContent = model.AuthorizedAccountsOthers[i].CoTitularName });
                    PDFFields.Add(new ContractField() { Field = "txtAuthorizedAccountCoCPF" + (i + 1).ToString(), FieldContent = model.AuthorizedAccountsOthers[i].CoCPF });

                    var acc = model.AuthorizedAccountsOthers[i].Account;

                    PDFFields.Add(new ContractField() { Field = "txtAuthorizedAccountBank" + (i + 1).ToString(), FieldContent = acc != null ? acc.BankCode : "" });
                    PDFFields.Add(new ContractField() { Field = "txtAuthorizedAccountAgency" + (i + 1).ToString(), FieldContent = acc != null ? acc.AgencyCode : "" });
                    PDFFields.Add(new ContractField() { Field = "txtAuthorizedAccount" + (i + 1).ToString(), FieldContent = acc != null ? acc.Account : "" });
                }
            }

            if (PDFData.ContractFields == null || PDFData.ContractFields.Count == 0)
                PDFData.ContractFields = PDFFields;
            else
                PDFData.ContractFields.AddRange(PDFFields);
        }

        public void CreatePDFIndividualPJ2(LegalPerson model, ref ContractInfo PDFData)
        {
            PDFData.FileName = PDFData.FileName + "_" + model.CNPJ.Replace(".", "").Replace("-", "").Replace("/", "");

            List<ContractField> PDFFields = new List<ContractField>();

            PDFFields.Add(new ContractField() { Field = "txtCNPJ", FieldContent = model.CNPJ });
            PDFFields.Add(new ContractField() { Field = "txtCorporateName", FieldContent = model.CorporateName });

            PDFFields.Add(new ContractField() { Field = "txtAdministrator", FieldContent = model.Administrator });
            PDFFields.Add(new ContractField() { Field = "txtCVM", FieldContent = model.CVM });
            PDFFields.Add(new ContractField() { Field = "txtRDE", FieldContent = model.RDE });
            PDFFields.Add(new ContractField() { Field = "txtOriginCountry", FieldContent = model.OriginCountry });
            PDFFields.Add(new ContractField() { Field = "txtCustodian", FieldContent = model.Custodian });
            PDFFields.Add(new ContractField() { Field = "txtLegalRepresentative", FieldContent = model.LegalRepresentative });
            PDFFields.Add(new ContractField() { Field = "txtTributeRepresentative", FieldContent = model.TributeRepresentative });
            PDFFields.Add(new ContractField() { Field = "txtCoLegalRepresentative", FieldContent = model.CoLegalRepresentative });
            PDFFields.Add(new ContractField() { Field = "txtCollectiveAccountTitular", FieldContent = model.CollectiveAccountTitular });

            PDFFields.Add(new ContractField() { Field = "rdbCollectiveAccount", FieldContent = model.CollectiveAccount.HasValue ? model.CollectiveAccount.Value == true ? "S" : "N" : "" });

            if (model.ImobileGoods != null)
            {
                for (var i = 0; i < model.ImobileGoods.Count && i < 3; i++)
                {
                    var address = model.ImobileGoods[i].Address;
                    PDFFields.Add(new ContractField() { Field = "txtImobileType" + (i + 1).ToString(), FieldContent = model.ImobileGoods[i].Type.HasValue ? model.ImobileGoods[i].Type.GetDescription() : "" });
                    PDFFields.Add(new ContractField() { Field = "txtImobileValue" + (i + 1).ToString(), FieldContent = model.ImobileGoods[i].Value.HasValue ? model.ImobileGoods[i].Value.Value.ToString("N2") : "" });
                    PDFFields.Add(new ContractField() { Field = "txtImobileAddress" + (i + 1).ToString(), FieldContent = address != null ? address.AddressLine1 : "" });
                    PDFFields.Add(new ContractField() { Field = "txtImobileQuarter" + (i + 1).ToString(), FieldContent = address != null ? address.Quarter : "" });
                    PDFFields.Add(new ContractField() { Field = "txtImobileCity" + (i + 1).ToString(), FieldContent = address != null ? address.City : "" });
                    PDFFields.Add(new ContractField() { Field = "txtImobileState" + (i + 1).ToString(), FieldContent = address != null ? address.State.HasValue ? address.State.Value.ToString() : "" : "" });
                    PDFFields.Add(new ContractField() { Field = "txtImobilePostalCode" + (i + 1).ToString(), FieldContent = address != null ? address.PostalCode : "" });
                    PDFFields.Add(new ContractField() { Field = "txtImobileCountry" + (i + 1).ToString(), FieldContent = address != null ? address.StateCountry : "" });
                }
            }

            if (model.MobileGoods != null)
            {
                for (var i = 0; i < model.MobileGoods.Count && i < 4; i++)
                {
                    PDFFields.Add(new ContractField() { Field = "rdbMobileType" + (i + 1).ToString(), FieldContent = model.MobileGoods[i].Type.HasValue ? ((int)model.MobileGoods[i].Type).ToString() : "" });
                    PDFFields.Add(new ContractField() { Field = "txtMobileValue" + (i + 1).ToString(), FieldContent = model.MobileGoods[i].Value.HasValue ? model.MobileGoods[i].Value.Value.ToString("N2") : "" });
                    PDFFields.Add(new ContractField() { Field = "txtMobileDescription" + (i + 1).ToString(), FieldContent = model.MobileGoods[i].Description });

                }
            }

            if (model.MensalRendiments != null)
            {
                var mensal = model.MensalRendiments[0];
                if (mensal != null)
                {
                    PDFFields.Add(new ContractField() { Field = "txtMensalType1", FieldContent = mensal.PJType });
                    PDFFields.Add(new ContractField() { Field = "txtMensalValue1", FieldContent = mensal.Value.HasValue ? mensal.Value.Value.ToString("N2") : "" });
                }

                if (model.MensalRendiments.Count > 1)
                {

                    mensal = model.MensalRendiments[1];
                    if (mensal != null)
                    {
                        PDFFields.Add(new ContractField() { Field = "txtMensalType2", FieldContent = mensal.PJType });
                        PDFFields.Add(new ContractField() { Field = "txtMensalValue2", FieldContent = mensal.Value.HasValue ? mensal.Value.Value.ToString("N2") : "" });
                    }
                }
            }

            if (!String.IsNullOrEmpty(model.FinancialPositionDate) && model.FinancialPositionDate.Length == 10)
            {
                PDFFields.Add(new ContractField() { Field = "txtFinancialPositionDateDia", FieldContent = model.FinancialPositionDate.Substring(0, 2) });
                PDFFields.Add(new ContractField() { Field = "txtFinancialPositionDateMes", FieldContent = model.FinancialPositionDate.Substring(3, 2) });
                PDFFields.Add(new ContractField() { Field = "txtFinancialPositionDateAno", FieldContent = model.FinancialPositionDate.Substring(6, 4) });
            }

            PDFFields.Add(new ContractField() { Field = "txtFinancialPositionValue", FieldContent = model.PL.HasValue ? model.PL.Value.ToString("N2") : "" });
            PDFFields.Add(new ContractField() { Field = "txtTotalValue", FieldContent = model.TotalPatrimony.HasValue ? model.TotalPatrimony.Value.ToString("N2") : "" });

            PDFFields.Add(new ContractField() { Field = "rdbOperatesOnItsOwn", FieldContent = model.OperatesOnItsOwn.HasValue ? model.OperatesOnItsOwn.Value == true ? "S" : "N" : "" });
            PDFFields.Add(new ContractField() { Field = "rdbAuthorizesTransmissionOrders", FieldContent = model.AuthorizesTransmissionOrders.HasValue ? model.AuthorizesTransmissionOrders.Value == true ? "S" : "N" : "" });

            if (model.AuthorizedMembers != null)
            {
                for (var i = 0; i < 4 && i < model.AuthorizedMembers.Count; i++)
                {
                    var member = model.AuthorizedMembers[i];

                    PDFFields.Add(new ContractField() { Field = "txtAuthorizedMemberName" + (i + 1).ToString(), FieldContent = member.Name });
                    PDFFields.Add(new ContractField() { Field = "txtAuthorizedMemberCPF" + (i + 1).ToString(), FieldContent = member.CPF });
                    PDFFields.Add(new ContractField() { Field = "txtAuthorizedMemberEmail" + (i + 1).ToString(), FieldContent = member.Email });
                    PDFFields.Add(new ContractField() { Field = "txtAuthorizedMemberRG" + (i + 1).ToString(), FieldContent = member.RG });
                    PDFFields.Add(new ContractField() { Field = "txtAuthorizedMemberBirthDate" + (i + 1).ToString(), FieldContent = member.BirthDate.HasValue ? member.BirthDate.Value.ToShortDateString() : "" });
                }
            }

            PDFFields.Add(new ContractField() { Field = "rdbBrokerBound", FieldContent = model.BrokerBound.HasValue ? model.BrokerBound.Value == true ? "S" : "N" : "" });
            PDFFields.Add(new ContractField() { Field = "rdbApprovalTerm", FieldContent = model.ApprovalTerm.HasValue ? ((int)model.ApprovalTerm.Value).ToString() : "" });

            PDFFields.Add(new ContractField() { Field = "rdbPPE", FieldContent = model.PPE.HasValue ? model.PPE.Value ? "S" : "N" : "" });

            if (model.PoliticalMembers != null)
            {
                for (var i = 0; i < 2 && i < model.PoliticalMembers.Count; i++)
                {
                    var member = model.PoliticalMembers[i];

                    PDFFields.Add(new ContractField() { Field = "txtPoliticalMemberName" + (i + 1).ToString(), FieldContent = member.Name });
                    PDFFields.Add(new ContractField() { Field = "txtPoliticalMemberCPF" + (i + 1).ToString(), FieldContent = member.CPF });
                }
            }

            if (PDFData.ContractFields == null || PDFData.ContractFields.Count == 0)
                PDFData.ContractFields = PDFFields;
            else
                PDFData.ContractFields.AddRange(PDFFields);
        }

        public void AddAddress(PersonAddress address, ref List<ContractField> PDFFields, AddressType type)
        {
            var adType = type.ToString();

            PDFFields.Add(new ContractField() { Field = "txt" + adType + "Address", FieldContent = address.AddressLine1 });
            PDFFields.Add(new ContractField() { Field = "txt" + adType + "Quarter", FieldContent = address.Quarter });
            PDFFields.Add(new ContractField() { Field = "txt" + adType + "City", FieldContent = address.City });
            PDFFields.Add(new ContractField() { Field = "txt" + adType + "State", FieldContent = address.State.HasValue ? address.State.ToString() : "" });
            PDFFields.Add(new ContractField() { Field = "txt" + adType + "PostalCode", FieldContent = address.PostalCode });
            PDFFields.Add(new ContractField() { Field = "txt" + adType + "Country", FieldContent = address.StateCountry });
        }

        public void AddPhone(Phone phone, ref List<ContractField> PDFFields, ContactType type)
        {
            var adType = type.ToString();

            PDFFields.Add(new ContractField() { Field = "txt" + adType.Replace("Phone", "DDDCode"), FieldContent = phone.DDDCode.ToString() });
            PDFFields.Add(new ContractField() { Field = "txt" + adType, FieldContent = phone.PhoneNumber });
        }

        #endregion

        #region Demonstrativo

        public MemoryStream CreateBrokerageNoteToMemoryStream(BrokerageNoteResponse note)
        {
            iTextSharp.text.Document doc = new iTextSharp.text.Document(new Rectangle(1371, 970).Rotate(), 55, 55, 55, 75);
            MemoryStream memStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(doc, memStream);
            writer.CloseStream = false;

            writer.PageEvent = new PrintHeaderFooter { Title = "Nota de Corretagem" };

            doc.Open();

            Font tradeFont = FontFactory.GetFont("Arial", 30, Font.BOLD, new BaseColor(0, 135, 169));
            Font companyAddressFont = FontFactory.GetFont("Arial", 10, Font.NORMAL, new BaseColor(111, 111, 111));
            Font dateFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(111, 111, 111));
            Font lineFont = FontFactory.GetFont("Arial", 12, Font.NORMAL, new BaseColor(111, 111, 111));
            Font lineHeaderFont = FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(255, 255, 255));

            var logo = Image.GetInstance(System.Web.HttpContext.Current.Server.MapPath("~/img/logo-brpartners.png"));
            doc.Add(logo);

            Paragraph pg = new Paragraph();
            pg.Add(Chunk.NEWLINE);

            doc.Add(pg);

            PdfPTable table = new PdfPTable(4);
            table.WidthPercentage = 100;

            table.SetWidths(new[] { 35, 25, 20, 20 });

            PdfPTable tbR = new PdfPTable(1);
            tbR.DefaultCell.BorderWidth = 0.5f;

            PdfPCell cellCab = new PdfPCell();
            cellCab.Border = Rectangle.TOP_BORDER;
            cellCab.BorderWidthTop = 1;
            cellCab.BorderColorTop = new BaseColor(0, 0, 0);

            PdfPTable tb = new PdfPTable(1);
            tb.DefaultCell.Border = Rectangle.NO_BORDER;

            tb.AddCell(new Phrase("BR Partners Banco de Investimento", new Font(FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(0, 0, 0)))));
            tb.AddCell("Av Brigadeiro Faria Lima, 3355 - 26º andar");
            tb.AddCell("CEP: 04538-133 - SÃO PAULO - SP");

            cellCab.Table = tb;
            table.AddCell(cellCab);

            tb = new PdfPTable(1);
            tb.DefaultCell.Border = Rectangle.NO_BORDER;

            tb.AddCell("Tel.: +55 11 3704-1000");
            tb.AddCell("Email: brpartnersctvm@brap.com.br");
            tb.AddCell("CNPJ: 16.695.922/0001-09");

            cellCab.Table = tb;
            table.AddCell(cellCab);

            tb = new PdfPTable(1);
            tb.DefaultCell.Border = Rectangle.NO_BORDER;
            tb.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_CENTER;
            tb.DefaultCell.VerticalAlignment = Rectangle.ALIGN_MIDDLE;

            tb.AddCell(new Phrase("Nº Nota", new Font(FontFactory.GetFont("Arial", 13, Font.BOLD, new BaseColor(0, 0, 0)))));
            tb.AddCell(note.Cabecalho.NrNota.ToString());

            tbR.AddCell(tb);

            cellCab.Table = tbR;
            table.AddCell(cellCab);

            tb = new PdfPTable(1);
            tb.DefaultCell.Border = Rectangle.NO_BORDER;
            tb.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_CENTER;
            tb.DefaultCell.VerticalAlignment = Rectangle.ALIGN_MIDDLE;

            tb.AddCell(new Phrase("Data Pregão", new Font(FontFactory.GetFont("Arial", 13, Font.BOLD, new BaseColor(0, 0, 0)))));
            tb.AddCell(note.Cabecalho.DtPregao.ToString("dd/MM/yyyy"));

            tbR = new PdfPTable(1);
            tbR.DefaultCell.BorderWidth = 0.5f;
            tbR.AddCell(tb);

            cellCab.Table = tbR;
            table.AddCell(cellCab);

            doc.Add(table);

            ////////////////////////////////////////////////////////////////

            table = new PdfPTable(3);
            table.SpacingBefore = 15;
            table.WidthPercentage = 100;
            table.SetWidths(new[] { 60, 20, 20 });

            cellCab = new PdfPCell();

            tb = new PdfPTable(1);
            tb.DefaultCell.Border = Rectangle.NO_BORDER;

            tb.AddCell(new Phrase(note.Cabecalho.NomeCliente, new Font(FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(0, 0, 0)))));
            tb.AddCell(string.Format("{0}, {1} {2} - {3}", note.Cabecalho.Logradouro, note.Cabecalho.NrLogradouro, note.Cabecalho.Complemento, note.Cabecalho.Bairro));
            tb.AddCell(note.Cabecalho.Cep);
            tb.AddCell(string.Format("{0} - {1}", note.Cabecalho.Cidade, note.Cabecalho.Uf));

            tbR = new PdfPTable(1);
            tbR.DefaultCell.Border = Rectangle.NO_BORDER;
            tbR.AddCell(tb);

            cellCab.Table = tbR;
            table.AddCell(cellCab);

            tb = new PdfPTable(1);
            tb.DefaultCell.Border = Rectangle.NO_BORDER;
            tb.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_CENTER;
            tb.DefaultCell.VerticalAlignment = Rectangle.ALIGN_MIDDLE;

            tb.AddCell(new Phrase("CPF / CNPJ / CVM / COB", new Font(FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(0, 0, 0)))));
            tb.AddCell(note.Cabecalho.Cpf.ToString().Length < 12 ? string.Format(@"{0:000\.000\.000\-00}", note.Cabecalho.Cpf) : string.Format(@"{0:00\.000\.000\/0000\-00}", note.Cabecalho.Cpf));

            tbR = new PdfPTable(1);
            tbR.DefaultCell.Border = Rectangle.NO_BORDER;
            tbR.AddCell(tb);

            cellCab.Table = tbR;
            table.AddCell(cellCab);

            tb = new PdfPTable(1);
            tb.DefaultCell.Border = Rectangle.NO_BORDER;
            tb.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_CENTER;
            tb.DefaultCell.VerticalAlignment = Rectangle.ALIGN_MIDDLE;

            tb.AddCell(new Phrase("CÓDIGO CLIENTE", new Font(FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(0, 0, 0)))));
            tb.AddCell(note.Cabecalho.CdCliente.ToString());

            tbR = new PdfPTable(1);
            tbR.DefaultCell.Border = Rectangle.NO_BORDER;
            tbR.AddCell(tb);

            cellCab.Table = tbR;
            table.AddCell(cellCab);

            doc.Add(table);

            //////////////////////////////////////

            /////// NEGOCIOS REALIZADOS //////////////

            table = new PdfPTable(10);
            table.WidthPercentage = 100;
            table.SpacingBefore = 15;
            table.SetWidths(new[] { 4, 10, 4, 15, 25, 10, 10, 7, 12, 4 });


            cellCab = new PdfPCell();
            cellCab.Colspan = 10;
            cellCab.Padding = 5;
            cellCab.HorizontalAlignment = Rectangle.ALIGN_CENTER;
            cellCab.Phrase = new Phrase("NEGÓCIOS REALIZADOS", new Font(FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(0, 0, 0))));
            table.AddCell(cellCab);

            table.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_CENTER;
            table.DefaultCell.VerticalAlignment = Rectangle.ALIGN_MIDDLE;
            table.DefaultCell.Padding = 5;
            table.AddCell(new Phrase("Q", new Font(FontFactory.GetFont("Arial", 11, Font.BOLD, new BaseColor(0, 0, 0)))));
            table.AddCell(new Phrase("Negociação", new Font(FontFactory.GetFont("Arial", 11, Font.BOLD, new BaseColor(0, 0, 0)))));
            table.AddCell(new Phrase("C/V", new Font(FontFactory.GetFont("Arial", 11, Font.BOLD, new BaseColor(0, 0, 0)))));
            table.AddCell(new Phrase("Tipo de Mercado", new Font(FontFactory.GetFont("Arial", 11, Font.BOLD, new BaseColor(0, 0, 0)))));
            table.AddCell(new Phrase("Especificação do Título", new Font(FontFactory.GetFont("Arial", 11, Font.BOLD, new BaseColor(0, 0, 0)))));
            table.AddCell(new Phrase("Observação", new Font(FontFactory.GetFont("Arial", 11, Font.BOLD, new BaseColor(0, 0, 0)))));
            table.AddCell(new Phrase("Quantidade", new Font(FontFactory.GetFont("Arial", 11, Font.BOLD, new BaseColor(0, 0, 0)))));
            table.AddCell(new Phrase("Preço\n /Ajuste", new Font(FontFactory.GetFont("Arial", 11, Font.BOLD, new BaseColor(0, 0, 0)))));
            table.AddCell(new Phrase("Valor\n /Ajuste", new Font(FontFactory.GetFont("Arial", 11, Font.BOLD, new BaseColor(0, 0, 0)))));
            table.AddCell(new Phrase("D/C", new Font(FontFactory.GetFont("Arial", 11, Font.BOLD, new BaseColor(0, 0, 0)))));


            foreach (BrokerageNoteBody b in note.Corpo)
            {


                table.DefaultCell.Padding = 4;
                table.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_CENTER;
                table.AddCell(FontStyle(b.Qualificado, "Arial", 10, Font.NORMAL, new BaseColor(0, 0, 0)));

                table.AddCell(FontStyle(b.Negociacao, "Arial", 10, Font.NORMAL, new BaseColor(0, 0, 0)));
                table.AddCell(FontStyle(b.NatOp, "Arial", 10, Font.NORMAL, new BaseColor(0, 0, 0)));
                table.AddCell(FontStyle(b.TipoMercado, "Arial", 10, Font.NORMAL, new BaseColor(0, 0, 0)));
                table.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_LEFT;
                table.AddCell(FontStyle(b.Especificacao, "Arial", 10, Font.NORMAL, new BaseColor(0, 0, 0)));
                table.AddCell(FontStyle(b.Obs, "Arial", 10, Font.NORMAL, new BaseColor(0, 0, 0)));
                table.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
                table.AddCell(FontStyle(b.Quatidade.ToString("N0"), "Arial", 10, Font.NORMAL, new BaseColor(0, 0, 0)));
                table.AddCell(FontStyle(b.Preco.ToString("N2"), "Arial", 10, Font.NORMAL, new BaseColor(0, 0, 0)));
                table.AddCell(FontStyle(b.Valor.ToString("N2"), "Arial", 10, Font.NORMAL, new BaseColor(0, 0, 0)));
                table.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_CENTER;
                table.AddCell(FontStyle(b.DC, "Arial", 10, Font.NORMAL, new BaseColor(0, 0, 0)));

            }

            // doc.Add(table);

            //if (note.Corpo.Count > 22 && note.Corpo.Count < 53) // verificar quantidade de linhas e quebra a página para não bugar as tabelas.
            //{
            //    doc.NewPage();
            //}

            PdfPTable TableMaster = table;

            /////////////////////////////////////////////


            table = new PdfPTable(3);

            table.WidthPercentage = 100;
            table.SpacingBefore = 15;
            table.DefaultCell.Border = Rectangle.NO_BORDER;
            table.DefaultCell.BorderWidth = 0;

            table.SetWidths(new[] { 49, 2, 49 });

            PdfPTable tbAux = new PdfPTable(1);
            tbAux.DefaultCell.Border = Rectangle.NO_BORDER;
            tbAux.DefaultCell.BorderWidth = 0;

            ///// RESUMO NEGOCIOS /////
            tbR = new PdfPTable(2);
            tbR.SplitLate = true;
            tbR.SplitRows = false;
            tbR.SetWidths(new[] { 60, 40 });
            tbR.DefaultCell.Padding = 5;

            cellCab = new PdfPCell();
            cellCab.Colspan = 2;
            cellCab.HorizontalAlignment = Rectangle.ALIGN_LEFT;
            cellCab.Padding = 5;
            cellCab.Phrase = new Phrase("RESUMO DOS NEGÓCIOS", new Font(FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(0, 0, 0))));
            tbR.AddCell(cellCab);

            tbR.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_LEFT;
            tbR.AddCell("Debêntures");
            tbR.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
            tbR.AddCell(note.Resumo.VlDebentures.ToString("N2"));

            tbR.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_LEFT;
            tbR.AddCell("Vendas à vista");
            tbR.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
            tbR.AddCell(note.Resumo.VlVendasVista.ToString("N2"));

            tbR.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_LEFT;
            tbR.AddCell("Compras à vista");
            tbR.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
            tbR.AddCell(note.Resumo.VlComprasVista.ToString("N2"));

            tbR.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_LEFT;
            tbR.AddCell("Opções - Vendas");
            tbR.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
            tbR.AddCell(note.Resumo.VlVendasOpc.ToString("N2"));

            tbR.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_LEFT;
            tbR.AddCell("Opções - Compras");
            tbR.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
            tbR.AddCell(note.Resumo.VlComprasOpc.ToString("N2"));

            tbR.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_LEFT;
            tbR.AddCell("Operações à Termo");
            tbR.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
            tbR.AddCell(note.Resumo.VlTermo.ToString("N2"));

            tbR.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_LEFT;
            tbR.AddCell("Valor das Oper. com Tít. Púb. (V. Nom.)");
            tbR.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
            tbR.AddCell(note.Resumo.VlTitulos.ToString("N2"));

            tbR.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_LEFT;
            tbR.AddCell("Valor das Operações");
            tbR.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
            tbR.AddCell(note.Resumo.VlOperacoes.ToString("N2"));

            tbAux.AddCell(tbR);

            ////////////////////////////////////////////

            /////////////////// ESPECIFICAÇOES DIVERSAS ////////////////////

            tbR = new PdfPTable(1);
            tbR.SetWidths(new[] { 100 });
            tbR.DefaultCell.Padding = 5;
            tbR.SpacingBefore = 15;

            tbR.AddCell(new Phrase("ESPECIFICAÇÕES DIVERSAS", new Font(FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(0, 0, 0)))));
            tbR.AddCell("A coluna Q indica liquidação no Agente do Qualificado");

            if (!string.IsNullOrEmpty(note.Resumo.EspecDivEstorno.Trim()))
                tbR.AddCell(note.Resumo.EspecDivEstorno);

            if (note.Resumo.VlBaseIrDT > 0 || note.Resumo.VlBaseIrOper > 0 || note.Resumo.VlBaseIrDT < 0 || note.Resumo.VlBaseIrOper < 0)
            {
                var projecao = Math.Truncate(note.Resumo.VlBaseIrDT) / 100;
                tbR.AddCell(string.Format("IRRF Day Trade:  Base R$ {0} Projeção: R$ {1}\n\nO Valor do IRRF s/ Day-trade já está descontado do Líquido da Nota", note.Resumo.VlBaseIrDT.ToString("N2"), (projecao < 0 ? 0 : projecao)));
            }

            tbAux.AddCell(tbR);

            //////////////////////////////////////////////////////////////////

            /////////// OBSERVAÇÕES ////////////////////////////

            tbR = new PdfPTable(2);
            tbR.SetWidths(new[] { 60, 40 });
            tbR.DefaultCell.Padding = 5;
            tbR.SpacingBefore = 15;

            tbR.DefaultCell.Colspan = 2;
            tbR.AddCell(new Phrase("OBSERVAÇÕES(*)", new Font(FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(0, 0, 0)))));

            tbR.DefaultCell.Colspan = 0;
            tbR.AddCell("1 - As operações a termo não são computadas no líquido da nota");
            tbR.AddCell("I - POP ");

            tbR.AddCell("2 - Corretora ou pessoa vinculada atuou na contra parte");
            tbR.AddCell("# - Negócio direto");

            tbR.AddCell("8 - Liquidação Institucional");
            tbR.AddCell("D - Day-Trade");

            tbR.AddCell("F - Cobertura");
            tbR.AddCell("B - Debêntures");

            tbR.AddCell("C - Clubes e Fundos de Ações");
            tbR.AddCell("A - Posição Futuro");

            tbR.AddCell("H - Home Broker");
            tbR.AddCell("X - Box");

            tbR.AddCell("P - Carteira Própria");
            tbR.AddCell("Y - Desmanche de Box");

            tbR.AddCell("L - Precatório");
            tbR.AddCell("T - Liquidação pelo Bruto");

            tbAux.AddCell(tbR);

            ////////////////////////////////////////////////////

            table.AddCell(tbAux);



            ////////////////////////////////////////////////////////////////

            ////////////////////
            table.AddCell("");
            ////////////////////

            tbAux = new PdfPTable(1);
            tbAux.DefaultCell.Border = Rectangle.NO_BORDER;
            tbAux.DefaultCell.BorderWidth = 0;

            //////// RESUMO FINANCEIRO ////////////////////
            tbR = new PdfPTable(2);
            tbR.SetWidths(new[] { 75, 25 });
            tbR.DefaultCell.Padding = 5;

            tbR.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_LEFT;
            tbR.AddCell(new Phrase("RESUMO FINANCEIRO", new Font(FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(0, 0, 0)))));

            tbR.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
            tbR.AddCell(new Phrase("D/C", new Font(FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(0, 0, 0)))));

            tbAux.AddCell(tbR);
            /////////////////////////////////////////////////

            //////// CBLC ////////////////////
            tbR = new PdfPTable(2);
            tbR.SetWidths(new[] { 75, 25 });
            tbR.DefaultCell.Padding = 5;
            tbR.SpacingBefore = 15;

            tbR.DefaultCell.Colspan = 2;
            tbR.AddCell(new Phrase("CBLC", new Font(FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(0, 0, 0)))));

            tbR.DefaultCell.Colspan = 0;

            tbR.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_LEFT;
            tbR.AddCell("Valor Líquido das Operações");
            tbR.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
            tbR.AddCell(note.Resumo.VlLiquidoOperacoes.ToString("N2") + " " + note.Resumo.TotalCBLCDC);

            tbR.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_LEFT;
            tbR.AddCell("Taxa de Liquidação");
            tbR.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
            tbR.AddCell(note.Resumo.TaxaLiquidacao.ToString("N2") + " " + note.Resumo.TaxaLiquidacaoDC);

            tbR.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_LEFT;
            tbR.AddCell("Taxa de Registro");
            tbR.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
            tbR.AddCell(note.Resumo.TaxaRegistro.ToString("N2") + " " + note.Resumo.TaxaRegistroDC);

            tbR.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_LEFT;
            tbR.AddCell("Total CBLC");
            tbR.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
            tbR.AddCell(note.Resumo.TotalCBLC.ToString("N2") + " " + note.Resumo.TotalCBLCDC);
            tbAux.AddCell(tbR);
            /////////////////////////////////////////////////

            //////// BOVESPA/SOMA ////////////////////
            tbR = new PdfPTable(2);
            tbR.SetWidths(new[] { 75, 25 });
            tbR.DefaultCell.Padding = 5;
            tbR.SpacingBefore = 15;

            tbR.DefaultCell.Colspan = 2;
            tbR.AddCell(new Phrase("BOVESPA / SOMA", new Font(FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(0, 0, 0)))));

            tbR.DefaultCell.Colspan = 0;
            tbR.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_LEFT;
            tbR.AddCell("Taxa de Termo / Opções");
            tbR.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
            tbR.AddCell(note.Resumo.TaxaTermoOpc.ToString("N2") + " D");

            tbR.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_LEFT;
            tbR.AddCell("Taxa A.N.A");
            tbR.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
            tbR.AddCell(note.Resumo.TaxaANA.ToString("N2") + " D");

            tbR.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_LEFT;
            tbR.AddCell("Emolumentos");
            tbR.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
            tbR.AddCell(note.Resumo.Emolumentos.ToString("N2") + " D");

            tbR.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_LEFT;
            tbR.AddCell("Total Bovespa / Soma");
            tbR.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
            tbR.AddCell(note.Resumo.TotalBovespaSoma.ToString("N2") + " D");
            tbAux.AddCell(tbR);
            /////////////////////////////////////////////////

            //////// CORRETAGEM / DESPESAS ////////////////////
            tbR = new PdfPTable(2);
            tbR.SetWidths(new[] { 75, 25 });
            tbR.DefaultCell.Padding = 5;
            tbR.SpacingBefore = 15;

            tbR.DefaultCell.Colspan = 2;
            tbR.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_LEFT;
            tbR.AddCell(new Phrase("CORRETAGEM / DESPESAS", new Font(FontFactory.GetFont("Arial", 12, Font.BOLD, new BaseColor(0, 0, 0)))));

            tbR.DefaultCell.Colspan = 0;
            tbR.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_LEFT;
            tbR.AddCell("Corretagem");
            tbR.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
            tbR.AddCell(note.Resumo.Corretagem.ToString("N2") + " D");

            tbR.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_LEFT;
            tbR.AddCell("ISS (SÃO PAULO)");
            tbR.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
            tbR.DefaultCell.PaddingRight = 17.5f;
            tbR.AddCell(note.Resumo.ISS.ToString("N2"));

            tbR.DefaultCell.Padding = 5;
            tbR.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_LEFT;
            tbR.AddCell("I.R.R.F. s/ operações. Base R$ " + note.Resumo.VlBaseIR.ToString("N2"));
            tbR.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
            tbR.AddCell(note.Resumo.IRRF.ToString("N2") + " D");

            tbR.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_LEFT;
            tbR.AddCell("Outras");
            tbR.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
            tbR.AddCell(note.Resumo.Outras.ToString("N2") + " D");

            tbR.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_LEFT;
            tbR.AddCell("Total Corretagem / Despesas");
            tbR.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
            tbR.AddCell(note.Resumo.TotalCorretagem.ToString("N2") + " D");

            tbR.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_LEFT;
            tbR.AddCell("Líquido " + (!string.IsNullOrEmpty(note.Resumo.LiquidoDt) ? string.Format(" para {0}/{1}/{2}", note.Resumo.LiquidoDt.Substring(6, 2), note.Resumo.LiquidoDt.Substring(4, 2), note.Resumo.LiquidoDt.Substring(0, 4)) : ""));
            tbR.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
            tbR.AddCell(note.Resumo.Liquido.ToString("N2") + " " + note.Resumo.LiquidoDC);

            tbAux.AddCell(tbR);
            /////////////////////////////////////////////////

            table.AddCell(tbAux);

            ///////////////////////////////////////////////
            tbR = new PdfPTable(1);
            tbR.SetWidths(new[] { 100 });
            tbR.DefaultCell.Padding = 1;
            tbR.DefaultCell.Border = Rectangle.NO_BORDER;
            tbR.SpacingBefore = 15;
            tbR.AddCell(" ");

            tbAux.AddCell(tbR);
            ///////////////////////////////////////////////

            table.AddCell(tbAux);


            // Colocando essas tabelas dentro de uma célula para evitar que a mesma seja splitada entre as páginas.

            table.WidthPercentage = 100;
            PdfPCell tempcell = new PdfPCell(table);
            tempcell.Colspan = 10;
            tempcell.Border = 0;
            TableMaster.AddCell(tempcell);
            doc.Add(TableMaster);
            /////////////////////////////////

            table = new PdfPTable(1);
            table.WidthPercentage = 100;
            table.SpacingBefore = 15;
            table.DefaultCell.BorderWidth = 1f;
            table.DefaultCell.Border = Rectangle.TOP_BORDER;
            table.AddCell("OUVIDORIA EMAIL: ouvidoria@brap.com.br TEL.: 0800-778 3355");


            doc.Add(table);

            doc.Close();

            return memStream;
        }

        private Phrase FontStyle(string texto, string fontName, float fontSize, int style, BaseColor basecolor)
        {

            return new Phrase(texto, new Font(FontFactory.GetFont(fontName, fontSize, style, basecolor)));

        }

        #endregion
    }

    public class PrintHeaderFooter : PdfPageEventHelper
    {
        private PdfContentByte pdfContent;
        private PdfTemplate pageNumberTemplate;
        private BaseFont baseFont;
        private DateTime printTime;

        public string Title { get; set; }
        public bool MinItems { get; set; }
        public string ExtraTitle { get; set; }
        public string ExtraExtraTitle { get; set; }

        public override void OnOpenDocument(PdfWriter writer, iTextSharp.text.Document document)
        {
            printTime = DateTime.Now;
            baseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            pdfContent = writer.DirectContent;
            pageNumberTemplate = pdfContent.CreateTemplate(50, 50);
        }

        public override void OnEndPage(PdfWriter writer, iTextSharp.text.Document document)
        {
            base.OnEndPage(writer, document);

            int pageN = writer.PageNumber;
            string text = pageN + " - ";
            float len = baseFont.GetWidthPoint(text, 10);

            Rectangle pageSize = document.PageSize;
            pdfContent = writer.DirectContent;
            pdfContent.SetRGBColorFill(100, 100, 100);

            pdfContent.BeginText();
            pdfContent.SetFontAndSize(baseFont, 10);
            pdfContent.SetTextMatrix(pageSize.Width / 2, pageSize.GetBottom(30));
            pdfContent.ShowText(text);
            pdfContent.EndText();

            pdfContent.AddTemplate(pageNumberTemplate, (pageSize.Width / 2) + len, pageSize.GetBottom(30));

            pdfContent.BeginText();
            pdfContent.SetFontAndSize(baseFont, 10);
            pdfContent.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, printTime.ToString(), pageSize.GetRight(40), pageSize.GetBottom(30), 0);
            pdfContent.EndText();
        }

        public override void OnStartPage(PdfWriter writer, iTextSharp.text.Document document)
        {
            //Adding extra headers to the document
            if (writer.PageNumber > 1)
            {
                switch (this.Title)
                {
                    case "TradeBlotter":
                        this.CreateTradeBlotterHeader(ref document);
                        break;
                    case "CustomerStatement":
                        if (!PDFHelper.DisclaimerPage)
                            this.CreateCustomerStatementHeader(ref document);
                        break;
                    case "FailLedger":
                        this.CreateFailLedgerHeader(ref document);
                        break;
                    case "Users":
                        this.CreateUsersHeader(ref document);
                        break;
                    case "History":
                        this.CreateHistoryHeader(ref document, this.MinItems, this.ExtraTitle);
                        break;
                    case "TermHistory":
                        this.CreateHistoryHeader(ref document, this.MinItems, this.ExtraTitle, this.ExtraExtraTitle);
                        break;
                    case "GroupsOfAssessors":
                        this.CreateGroupsOfAssessorsHeader(ref document);
                        break;
                    case "OrdersCorrection":
                        this.CreateOrdersCorrectionHeader(ref document);
                        break;
                    case "BrokerageDiscount":
                        this.CreateBrokerageDiscountHeader(ref document);
                        break;
                    case "RedemptionRequestItem":
                        this.CreateRedemptionRequestItemsHeader(ref document);
                        break;
                    case "CustodyTransfer":
                        this.CreateCustodyTransferHeader(ref document);
                        break;
                    case "DailyFinancialReport":
                        this.CreateDailyFinancialReportHeader(ref document);
                        break;
                    case "RequestRights":
                        this.CreateRequestRightsReportHeader(ref document);
                        break;
                    case "AnalyticalStatement":
                        this.CreateFundsAnalyticalStatementHeader(ref document);
                        break;
                    case "SyntheticStatement":
                        this.CreateFundsSyntheticStatementHeader(ref document);
                        break;
                    case "Funds.History":
                        this.CreateFundHistoryHeader(ref document, this.MinItems, this.ExtraTitle, this.ExtraExtraTitle);
                        break;
                    case "ClientsRegister":
                        this.CreateClientsRegisterHeader(ref document);
                        break;
                    case "OperationsMap":
                        if (!PDFHelper.DisclaimerPage)
                            this.CreateOperationsMapHeader(ref document);
                        break;
                    case "Operations":
                        this.CreateOperationsHeader(ref document);
                        break;
                    case "PrivateReport":
                        this.CreatePrivateReportHeader(ref document);
                        break;
                    case "StatusViewer":
                        this.CreateStatusViewerHeader(ref document);
                        break;
                    case "OverdueAndDue":
                        this.CreateOverdueAndDueHeader(ref document);
                        break;
                    case "GuaranteeClient":
                        this.CreateGuaranteeClientHeader(ref document);
                        break;
                    case "TransferBMF":
                        this.CreateTransferBMFHeader(ref document);
                        break;
                }
            }

            base.OnStartPage(writer, document);
        }

        public override void OnCloseDocument(PdfWriter writer, iTextSharp.text.Document document)
        {
            base.OnCloseDocument(writer, document);

            pageNumberTemplate.BeginText();
            pageNumberTemplate.SetFontAndSize(baseFont, 10);
            pageNumberTemplate.SetTextMatrix(0, 0);
            pageNumberTemplate.ShowText(string.Empty + (writer.PageNumber - 1));
            pageNumberTemplate.EndText();
        }

        private void CreateTradeBlotterHeader(ref iTextSharp.text.Document document)
        {
            PDFHelper helper = new PDFHelper();

            PdfPTable tableHeader = new PdfPTable(13);
            tableHeader.WidthPercentage = 100;
            tableHeader.HorizontalAlignment = Element.ALIGN_LEFT;
            var colWidthPercentages = new[] { 67f, 84f, 83f, 135f, 100f, 87f, 87f, 158f, 114f, 158f, 114f, 70f, 150f };
            tableHeader.SetWidths(colWidthPercentages);
            tableHeader.SpacingBefore = 10;

            string[] headerItems = new string[13] { "Trade ID", "Trade Data", "Settle Date", "Isin", "Stock", "# Of Shares", "Share Price", "Customer Buy Amount", "Net Buy Amount", "Customer Sell Amount", "Net Sell Amount", "Currency", "Client/Broker" };
            helper.AddListHeader(ref tableHeader, headerItems);
            document.Add(tableHeader);
        }

        private void CreateCustomerStatementHeader(ref iTextSharp.text.Document document)
        {
            PDFHelper helper = new PDFHelper();

            PdfPTable tableHeader = new PdfPTable(14);
            tableHeader.WidthPercentage = 100;
            var colWidthPercentages = new[] { 98f, 78f, 78f, 62f, 127f, 69f, 58f, 127f, 86f, 128f, 69f, 131f, 128f, 69f };
            tableHeader.HorizontalAlignment = Element.ALIGN_LEFT;
            tableHeader.SpacingBefore = 15;
            tableHeader.SetWidths(colWidthPercentages);

            string[] headerItems = new string[14] { "Security", "Trade Data", "Settle Date", "Buy/Sell", "Share Price", "Currency", "Shares", "Net Value", "Date Settled", "Settled Value", "Currency", "Outstanding Shares", "Outstanding Values", "Currency" };
            helper.AddListHeader(ref tableHeader, headerItems);
            document.Add(tableHeader);
        }

        private void CreateCustomerLedgerHeader(ref iTextSharp.text.Document document)
        {
            PDFHelper helper = new PDFHelper();

            PdfPTable tableHeader = new PdfPTable(13);
            tableHeader.WidthPercentage = 100;
            var colWidthPercentages = new[] { 72f, 232f, 78f, 78f, 61f, 85f, 65f, 117f, 66f, 107f, 107f, 137f, 137f };
            tableHeader.HorizontalAlignment = Element.ALIGN_LEFT;
            tableHeader.SpacingBefore = 15;
            tableHeader.SetWidths(colWidthPercentages);

            string[] headerItems = new string[13] { "Confirm ID", "Security", "Trade Data", "Settle Date", "Buy/Sell", "Share Price", "Shares", "Net Value", "Currency", "Settled Shares", "Settled Value", "Outstanding Shares", "Outstanding Values" };
            helper.AddListHeader(ref tableHeader, headerItems);
            document.Add(tableHeader);
        }

        private void CreateFailLedgerHeader(ref iTextSharp.text.Document document)
        {
            PDFHelper helper = new PDFHelper();

            PdfPTable tableHeader = new PdfPTable(11);
            tableHeader.WidthPercentage = 100;
            var colWidthPercentages = new[] { 72f, 124f, 78f, 90f, 58f, 90f, 58f, 90f, 70f, 90f, 96f };
            tableHeader.HorizontalAlignment = Element.ALIGN_LEFT;
            tableHeader.SpacingBefore = 15;
            tableHeader.SetWidths(colWidthPercentages);

            string[] headerItems = new string[11] { "Confirm ID", "B/S", "Security", "Trade Date", "Settle Date", "Date Settled", "Shares", "Net Amount", "Currency", "Market Value Per Share", "Market Value Total" };
            helper.AddListHeader(ref tableHeader, headerItems);
            document.Add(tableHeader);
        }

        private void CreateOrdersCorrectionHeader(ref iTextSharp.text.Document document)
        {
            PDFHelper helper = new PDFHelper();

            PdfPTable tableHeader = new PdfPTable(7);
            tableHeader.WidthPercentage = 100;
            var colWidthPercentages = new[] { 130f, 312f, 312f, 312f, 85f, 60f, 110f };
            tableHeader.HorizontalAlignment = Element.ALIGN_LEFT;
            tableHeader.SpacingBefore = 15;
            tableHeader.SetWidths(colWidthPercentages);

            string[] headerItems = new string[7] { "Tipo", "Assessor", "Origem", "Destino", "Data Solic.", "Data", "Situação" };
            helper.AddListHeader(ref tableHeader, headerItems);
            document.Add(tableHeader);
        }

        private void CreateBrokerageDiscountHeader(ref iTextSharp.text.Document document)
        {
            PDFHelper helper = new PDFHelper();

            PdfPTable tableHeader = new PdfPTable(12);
            tableHeader.WidthPercentage = 100;
            tableHeader.SpacingBefore = 15;

            string[] headerItems = new string[12] { "Cod", "Cliente", "Assessor", "Bolsa", "Mercado", "Operação", "Vigência", "Ativo", "Quantidade", "Desconto", "Descrição", "Situação" };
            helper.AddListHeader(ref tableHeader, headerItems);
            document.Add(tableHeader);
        }

        private void CreateGroupsOfAssessorsHeader(ref iTextSharp.text.Document document)
        {
            PDFHelper helper = new PDFHelper();

            PdfPTable tableHeader = new PdfPTable(3);
            tableHeader.WidthPercentage = 70;
            var colWidthPercentages = new[] { 400f, 400f, 400f };
            tableHeader.HorizontalAlignment = Element.ALIGN_LEFT;
            tableHeader.SpacingBefore = 15;
            tableHeader.SetWidths(colWidthPercentages);

            string[] headerItems = new string[3] { "Grupos", "Assessores", "Usuários" };
            helper.AddListHeader(ref tableHeader, headerItems);
            document.Add(tableHeader);
        }

        private void CreateProfilesHeader(ref iTextSharp.text.Document document)
        {
            PDFHelper helper = new PDFHelper();

            PdfPTable tableHeader = new PdfPTable(2);
            tableHeader.WidthPercentage = 70;
            var colWidthPercentages = new[] { 400f, 400f };
            tableHeader.HorizontalAlignment = Element.ALIGN_LEFT;
            tableHeader.SpacingBefore = 15;
            tableHeader.SetWidths(colWidthPercentages);

            string[] headerItems = new string[2] { "Perfil", "Usuários" };
            helper.AddListHeader(ref tableHeader, headerItems);
            document.Add(tableHeader);
        }

        private void CreateUsersHeader(ref iTextSharp.text.Document document)
        {
            PDFHelper helper = new PDFHelper();

            PdfPTable tableHeader = new PdfPTable(6);
            tableHeader.WidthPercentage = 100;
            var colWidthPercentages = new[] { 150f, 70f, 40f, 110f, 460f, 85f };
            tableHeader.HorizontalAlignment = Element.ALIGN_LEFT;
            tableHeader.SpacingBefore = 15;
            tableHeader.SetWidths(colWidthPercentages);

            string[] headerItems = new string[6] { "Usuário", "Cod. Ass.", "Tipo", "Perfil de Acesso", "Grupo de Assessores", "Assessores" };
            helper.AddListHeader(ref tableHeader, headerItems);
            document.Add(tableHeader);
        }

        private void CreateHistoryHeader(ref iTextSharp.text.Document document, bool minItems, string extraTitle, string extraExtraTitle = null)
        {
            PDFHelper helper = new PDFHelper();
            PdfPTable tableHeader;
            Single[] colWidthPercentages;
            string[] headerItems;

            if (extraExtraTitle != null)
            {
                tableHeader = new PdfPTable(5);
                tableHeader.WidthPercentage = 100;
                colWidthPercentages = new[] { 220f, 238f, 135f, 238f, 250f };
                headerItems = new string[5] { "Data e Hora", "Responsável", "Ação", extraTitle, extraExtraTitle };
            }
            else if (!minItems && extraExtraTitle == null)
            {
                tableHeader = new PdfPTable(4);
                tableHeader.WidthPercentage = 100;
                colWidthPercentages = new[] { 125f, 238f, 135f, 238f };
                headerItems = new string[4] { "Data e Hora", "Responsável", "Ação", extraTitle };
            }
            else
            {
                tableHeader = new PdfPTable(3);
                tableHeader.WidthPercentage = 100;
                colWidthPercentages = new[] { 125f, 292f, 132f };
                headerItems = new string[3] { "Data e Hora", "Responsável", extraTitle };
            }

            tableHeader.HorizontalAlignment = Element.ALIGN_LEFT;
            tableHeader.SpacingBefore = 15;
            tableHeader.SetWidths(colWidthPercentages);

            helper.AddListHeader(ref tableHeader, headerItems);
            document.Add(tableHeader);
        }

        private void CreateFundHistoryHeader(ref iTextSharp.text.Document document, bool minItems, string extraTitle, string extraExtraTitle)
        {
            PDFHelper helper = new PDFHelper();
            PdfPTable tableHeader;
            Single[] colWidthPercentages;
            string[] headerItems;

            tableHeader = new PdfPTable(6);
            tableHeader.WidthPercentage = 100;
            colWidthPercentages = new[] { 220f, 238f, 135f, 250f, 238f, 250f };
            headerItems = new string[6] { "Data e Hora", "Responsável", "Ação", "Cliente", extraTitle, extraExtraTitle };

            tableHeader.HorizontalAlignment = Element.ALIGN_LEFT;
            tableHeader.SpacingBefore = 15;
            tableHeader.SetWidths(colWidthPercentages);

            helper.AddListHeader(ref tableHeader, headerItems);
            document.Add(tableHeader);
        }

        private void CreateRedemptionRequestItemsHeader(ref iTextSharp.text.Document document)
        {
            PDFHelper helper = new PDFHelper();

            PdfPTable tableHeader = new PdfPTable(8);
            tableHeader.WidthPercentage = 100;
            tableHeader.SpacingBefore = 15;

            var colWidthPercentages = new[] { 12f, 12f, 12f, 12f, 12f, 12f, 12f, 12f };
            tableHeader.SetWidths(colWidthPercentages);

            string[] headerItems = new string[8] { "Solicitado em", "Código", "Cliente", "Banco", "Agência", "Conta", "Valor", "Status" };
            helper.AddListHeader(ref tableHeader, headerItems);
            document.Add(tableHeader);
        }

        private void CreateCustodyTransferHeader(ref iTextSharp.text.Document document)
        {
            PDFHelper helper = new PDFHelper();

            PdfPTable tableHeader = new PdfPTable(12);
            tableHeader.WidthPercentage = 100;
            tableHeader.SpacingBefore = 15;

            var colWidthPercentages = new[] { 8f, 10f, 20f, 8f, 8f, 12f, 12f, 12f, 8f, 9f, 10f, 8f };
            tableHeader.SetWidths(colWidthPercentages);

            string[] headerItems = new string[12] { "Código", "Cliente", "Assessor", "Ativo", "Quantidade", "Corretora Origem", "Carteira Origem", "Corretora Destino", "Carteira Destino", "Status", "Data da Movimentação", "Observação" };
            helper.AddListHeader(ref tableHeader, headerItems);
            document.Add(tableHeader);
        }

        private void CreatePrivateReportHeader(ref iTextSharp.text.Document document)
        {
            PDFHelper helper = new PDFHelper();

            PdfPTable tableHeader = new PdfPTable(9);
            tableHeader.WidthPercentage = 100;
            tableHeader.SpacingBefore = 15;

            var colWidthPercentages = new[] { 8f, 20f, 10f, 8f, 8f, 12f, 12f, 12f, 8f };
            tableHeader.SetWidths(colWidthPercentages);

            string[] headerItems = new string[9] { "CPF/CGC", "Cliente", "Produto", "Corretagem", "Posição", "Disponível", "Captação", "Aplicação", "Resgate" };
            helper.AddListHeader(ref tableHeader, headerItems);
            document.Add(tableHeader);
        }

        private void CreateStatusViewerHeader(ref iTextSharp.text.Document document)
        {
            PDFHelper helper = new PDFHelper();

            PdfPTable tableItems = new PdfPTable(7);
            tableItems.WidthPercentage = 100;
            var colWidthPercentages = new[] { 8f, 6f, 22f, 8f, 22f, 6f, 6f };
            tableItems.SetWidths(colWidthPercentages);
            tableItems.SplitRows = false;

            string[] headerItems = new string[7] { "Cadastro", "Código", "Cliente", "CPF/CGC", "Assessor", "BM&F", "Bovespa" };
            helper.AddListHeader(ref tableItems, headerItems);

            document.Add(tableItems);
        }

        private void CreateDailyFinancialReportHeader(ref iTextSharp.text.Document document)
        {
            PDFHelper helper = new PDFHelper();

            PdfPTable tableHeader = new PdfPTable(12);
            tableHeader.WidthPercentage = 100;
            tableHeader.SpacingBefore = 15;

            var colWidthPercentages = new[] { 6, 9, 15, 10, 10, 10, 10, 10, 10, 10, 10, 12 };
            tableHeader.SetWidths(colWidthPercentages);

            string[] headerItems = new string[12] { "Assessor", "Código", "Cliente", "Saldo Anterior", "Disponível C/C", "Crédito", "Débito", "Liquidado", "Saldo Atual", "Projetado", "Saldo Final", "Taxa de Custódia" };
            helper.AddListHeader(ref tableHeader, headerItems);
            document.Add(tableHeader);
        }

        private void CreateRequestRightsReportHeader(ref iTextSharp.text.Document document)
        {
            PDFHelper helper = new PDFHelper();

            PdfPTable tableHeader = new PdfPTable(12);
            tableHeader.WidthPercentage = 100;
            tableHeader.SpacingBefore = 15;

            var colWidthPercentages = new[] { 17f, 5f, 18f, 7f, 6f, 6f, 8f, 8f, 6f, 6f, 6f, 7f };
            tableHeader.SetWidths(colWidthPercentages);

            string[] headerItems = new string[12] { "Cliente", "Código", "Assessor", "Qtde. Direito", "Solicitado", "Disponível", "Preço Unitário", "Total $", "Data Limite", "Sobras", "Notificação", "Situação" };
            helper.AddListHeader(ref tableHeader, headerItems);
            document.Add(tableHeader);
        }

        private void CreateFundsAnalyticalStatementHeader(ref iTextSharp.text.Document document)
        {
            PDFHelper helper = new PDFHelper();

            PdfPTable tableHeader = new PdfPTable(12);
            tableHeader.WidthPercentage = 100;
            tableHeader.SpacingBefore = 30;

            var colWidthPercentages = new[] { 12f, 12f, 14f, 8f, 8f, 8f, 8f, 9f, 9f, 8f };
            tableHeader.SetWidths(colWidthPercentages);

            string[] headerItems = new string[10] { "Data de Aplicação", "Data de Movimentação", "Lançamento", "Nº da Cautela", "Qtde de Cotas", "Valor Original", "Valor Bruto", "IOF", "IRRF", "Valor" };
            helper.AddListHeader(ref tableHeader, headerItems);
            document.Add(tableHeader);
        }

        private void CreateFundsSyntheticStatementHeader(ref iTextSharp.text.Document document)
        {
            PDFHelper helper = new PDFHelper();

            PdfPTable tableHeader = new PdfPTable(6);
            tableHeader.WidthPercentage = 100;
            tableHeader.SpacingBefore = 30;

            var colWidthPercentages = new[] { 7f, 20f, 5f, 10f, 10f, 10f };
            tableHeader.SetWidths(colWidthPercentages);

            string[] headerItems = new string[6] { "Data", "Lançamento", "Cota", "IOF", "IRRF", "Valor" };
            helper.AddListHeader(ref tableHeader, headerItems);
            document.Add(tableHeader);
        }

        private void CreateClientsRegisterHeader(ref iTextSharp.text.Document document)
        {
            PDFHelper helper = new PDFHelper();

            PdfPTable tableHeader = new PdfPTable(4);
            tableHeader.WidthPercentage = 100;
            tableHeader.SpacingBefore = 30;

            var colWidthPercentages = new[] { 6f, 8f, 10f, 10f };
            tableHeader.SetWidths(colWidthPercentages);

            string[] headerItems = new string[4] { "Código", "Nome", "E-mail", "Cópia para" };
            helper.AddListHeader(ref tableHeader, headerItems);
            document.Add(tableHeader);
        }

        private void CreateOperationsMapHeader(ref iTextSharp.text.Document document)
        {
            PDFHelper helper = new PDFHelper();
            PdfPTable tableHeader = new PdfPTable(4);
            tableHeader.WidthPercentage = 100;
            var colWidthPercentages = new[] { 10f, 10f, 10f, 10f };
            tableHeader.SetWidths(colWidthPercentages);
            string[] headerItems = new string[4] { "Código", "Cliente", "Volume", "Corretagem" };
            helper.AddListHeader(ref tableHeader, headerItems);
            document.Add(tableHeader);
        }

        private void CreateOperationsHeader(ref iTextSharp.text.Document document)
        {
            PDFHelper helper = new PDFHelper();
            PdfPTable tableHeader = new PdfPTable(5);
            tableHeader.WidthPercentage = 100;
            var colWidthPercentages = new[] { 6f, 6f, 15f, 15f, 6f };
            tableHeader.SetWidths(colWidthPercentages);
            string[] headerItems = new string[5] { "Pregão", "Código", "Cliente", "Assessor", "Situação" };
            helper.AddListHeader(ref tableHeader, headerItems);
            document.Add(tableHeader);
        }

        private void CreateOverdueAndDueHeader(ref iTextSharp.text.Document document)
        {
            PDFHelper helper = new PDFHelper();
            PdfPTable tableHeader = new PdfPTable(8);
            tableHeader.WidthPercentage = 100;
            var colWidthPercentages = new[] { 8f, 10f, 10f, 16f, 6f, 6f, 6f, 6f };
            tableHeader.SetWidths(colWidthPercentages);
            string[] headerItems = new string[8] { "Código", "Cliente", "CPF/CGC", "Assessor", "Documento", "Situação", "Vencimento", "Sinacor" };
            helper.AddListHeader(ref tableHeader, headerItems);
            document.Add(tableHeader);
        }

        private void CreateGuaranteeClientHeader(ref iTextSharp.text.Document document)
        {
            PDFHelper helper = new PDFHelper();
            PdfPTable tableHeader = new PdfPTable(3);
            tableHeader.HorizontalAlignment = 0;
            string[] headerItems = new string[3] { "Ativo", "Quantidade", "Valor" };
            helper.AddListHeader(ref tableHeader, headerItems);
            document.Add(tableHeader);
        }

        private void CreateTransferBMFHeader(ref iTextSharp.text.Document document)
        {
            PDFHelper helper = new PDFHelper();
            PdfPTable tableHeader = new PdfPTable(6);
            tableHeader.WidthPercentage = 100;
            var colWidthPercentages = new[] { 3f, 12f, 3f, 12f, 5f, 3f };
            tableHeader.SetWidths(colWidthPercentages);
            string[] headerItems = new string[6] { "Vínculo", "Corretora", "Código", "Cliente", "CPF/CNPJ", "Tipo" };
            helper.AddListHeader(ref tableHeader, headerItems);
            document.Add(tableHeader);
        }









    }
}