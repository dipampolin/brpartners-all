﻿using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.WebSite.CommonService;
using QX3.Portal.WebSite.Helper;
using QX3.Portal.WebSite.Models.Channel;
using QX3.Spinnex.Common.Services.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace QX3.Portal.WebSite.Models
{
    public static class CommonHelper
    {
        public static List<SelectListItem> LoadSelectTypes(string moduleName, string specification, string selectedValue)
        {
            return LoadSelectTypes(moduleName, specification, selectedValue, null);
        }

        public static List<SelectListItem> LoadSelectTypes(string moduleName, string specification, string selectedValue, CommonType genericType)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            if (genericType != null)
                items.Add(new SelectListItem { Text = genericType.Description, Value = genericType.Value, Selected = selectedValue == genericType.Value });

            var commonTypes = LoadTypes(moduleName, specification);

            items.AddRange((from t in commonTypes
                            select new SelectListItem
                            {
                                Text = t.Description,
                                Value = t.Value,
                                Selected = t.Value == selectedValue
                            }).OrderBy(s => s.Text).ToList());


            return items;
        }

        public static List<CommonType> LoadTypes(string moduleName, string specification)
        {
            List<CommonType> items = new List<CommonType>();

            try
            {
                IChannelProvider<ICommonContractChannel> channelProvider = ChannelProviderFactory<ICommonContractChannel>.Create("CommonClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();
                var commonTypes = service.LoadTypes(moduleName, specification).Result;
                service.Close();

                if (commonTypes != null)
                    items = commonTypes.ToList();

            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }

            return items;
        }

        public static string GetMonthName(int month, bool abbreviate, IFormatProvider provider)
        {
            DateTimeFormatInfo info = DateTimeFormatInfo.GetInstance(provider);
            if (abbreviate) return info.GetAbbreviatedMonthName(month);
            return info.GetMonthName(month);
        }

        public static string GetMonthName(int month, bool abbreviate)
        {
            return GetMonthName(month, abbreviate, null);
        }

        public static string GetMonthName(int month, IFormatProvider provider)
        {
            return GetMonthName(month, false, provider);
        }

        public static string GetMonthName(int month)
        {
            return GetMonthName(month, false, null);
        }

        public static bool IsCultureBrazilian()
        {
            CultureInfo currentCulture = CultureInfo.CurrentCulture;
            return currentCulture.Name == "pt-BR";
        }

        public static IQueryable<DateTime> GetHolidaysBrazil()
        {
            List<DateTime> holidaysBrasil = new List<DateTime>();

            holidaysBrasil.Add(new DateTime(2012, 1, 1));	// I - 1º de janeiro, Confraternização Universal (feriado nacional);
            holidaysBrasil.Add(new DateTime(2012, 4, 21));	// VI - 21 de abril, Tiradentes (feriado nacional);
            holidaysBrasil.Add(new DateTime(2012, 5, 1));	// VII - 1º de maio, Dia Mundial do Trabalho (feriado nacional);
            holidaysBrasil.Add(new DateTime(2012, 9, 7));	// IX - 7 de setembro, Independência do Brasil (feriado nacional);
            holidaysBrasil.Add(new DateTime(2012, 10, 12));	// X - 12 de outubro, Nossa Senhora Aparecida (feriado nacional);
            holidaysBrasil.Add(new DateTime(2012, 11, 2));	// XII- 2 de novembro, Finados (feriado nacional);
            holidaysBrasil.Add(new DateTime(2012, 11, 15));	// XIII - 15 de novembro, Proclamação da República (feriado nacional);
            holidaysBrasil.Add(new DateTime(2012, 12, 25));	// XV - 25 de dezembro, Natal (feriado nacional);

            return holidaysBrasil.AsQueryable();
        }

        public static DateTime GetFirstWeekday(int month, int year)
        {
            DateTime datetime = new DateTime(year, month, 1);

            while (datetime.DayOfWeek == DayOfWeek.Saturday || datetime.DayOfWeek == DayOfWeek.Sunday || GetHolidaysBrazil().Contains(datetime))
            {
                datetime = datetime.AddDays(1);
            }

            return datetime;
        }

        public static int GetPendents(string areaName)
        {
            int result = 0;
            var CommonProvider = ChannelProviderFactory<ICommonContractChannel>.Create("CommonClassName");

            using (var service = CommonProvider.GetChannel().CreateChannel())
            {
                service.Open();

                string assessor = String.Empty;
                /*if (SessionHelper.CurrentUser.Assessors != null)
                    foreach (int i in SessionHelper.CurrentUser.Assessors)
                        assessor += i.ToString() + ";";*/

                var list = service.GetWarningList(SessionHelper.CurrentUser, areaName, assessor, null, GetLoggedUserGroupId()).Result;
                result = (list != null) ? list.Sum(a => a.Count) : 0;

                service.Close();
            }

            return result;
        }

        public static List<T> PrepareObjectToSerialize<T>(IEnumerable<T> list) where T : class
        {
            List<T> returnList = new List<T>();

            foreach (T item in list)
            {
                T obj = System.Activator.CreateInstance<T>();

                foreach (System.Reflection.PropertyInfo prop in obj.GetType().GetProperties())
                {
                    if (prop.PropertyType.FullName == "System.String")
                    {
                        prop.SetValue(obj, System.Web.HttpUtility.HtmlEncode(prop.GetValue(item, null)), null);
                    }
                    else
                    {
                        prop.SetValue(obj, prop.GetValue(item, null), null);
                    }
                }

                returnList.Add(obj);
            }
            return returnList;
        }

        public static void WriteLogInFile(string fileName, string message)
        {
            try
            {
                string filePath = ConfigurationManager.AppSettings["FileLoggerPath"] + fileName + ".txt";
                FileStream f;
                if (!File.Exists(filePath))
                    f = File.Create(filePath);
                else
                    f = File.Open(filePath, FileMode.Append, FileAccess.Write);
                using (StreamWriter strw = new StreamWriter(f))
                {

                    strw.WriteLine("----------------" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + "----------------");

                    /*Caso haja usuário logado.*/

                    if (SessionHelper.CurrentUser != null)
                    {
                        Type t = SessionHelper.CurrentUser.GetType();
                        PropertyInfo[] props = t.GetProperties();
                        foreach (var p in props)
                        {
                            object obj = p.GetValue(SessionHelper.CurrentUser, null);
                            if (obj != null)
                            {
                                strw.WriteLine(p.Name + ": " + obj.ToString());
                            }
                        }

                    }

                    strw.WriteLine("");
                    strw.WriteLine(message);

                    strw.Close();
                }
                f.Close();
            }
            catch { }
            //TODO: Refatorar
        }

        public static void WriteLogInFile(string fileName, DateTime date, string message)
        {
            CommonHelper.WriteLogInFile(fileName + "_" + date.Day + "_" + date.Month + "_" + date.Year, message);
        }

        public static int? GetLoggedUserGroupId()
        {
            /*var group = SessionHelper.CurrentUser.GroupOfAssessor;
            
            if (group == null) return null;
            else return group.ID;*/

            return Convert.ToInt32(SessionHelper.CurrentUser.ID);
        }

        public static string GetClientName(int code)
        {
            var nonservicetrader =
    QX3.Portal.WebSite.Models.Channel.ChannelProviderFactory<QX3.Portal.WebSite.NonResidentTradersService.INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName").GetChannel().CreateChannel();
            nonservicetrader.Open();

            var assessor = string.Empty;
            if (SessionHelper.CurrentUser.Assessors != null)
                foreach (int i in SessionHelper.CurrentUser.Assessors)
                    assessor += i.ToString() + ";";

            var name = nonservicetrader.GetCustomerName(code, assessor, CommonHelper.GetLoggedUserGroupId()).Result;

            nonservicetrader.Close();

            return name;
        }

        public static void GenericSort<T>(ref List<T> list, string sOrder)
        {
            if (list.Count > 0)
            {
                Type t = list[0].GetType();
                string[] OrderCommand = sOrder.Split('_');
                string OrderByProperty = OrderCommand[0];
                string Direction = OrderCommand[1];


                if (t.GetProperty(OrderByProperty) != null)
                {
                    if (Direction == "asc")
                    {
                        list = list.OrderBy
                            (
                                a =>
                                {
                                    var l = t.InvokeMember(OrderByProperty, System.Reflection.BindingFlags.GetProperty, null, a,
                                                 null);
                                    return l;
                                }
                            ).ToList();
                    }
                    else
                    {
                        list = list.OrderByDescending
                            (
                                a =>
                                t.InvokeMember(OrderByProperty, System.Reflection.BindingFlags.GetProperty, null, a,
                                                null)
                            ).ToList();
                    }
                }

            }
        }

        public static void GenericSort<T>(ref List<T> list, string sOrder, Dictionary<string, Type> columnTypes)
        {
            if (list.Count > 0)
            {
                Type t = list[0].GetType();
                string[] OrderCommand = sOrder.Split('_');
                string OrderByProperty = OrderCommand[0];
                string Direction = OrderCommand[1];

                var j = columnTypes[OrderByProperty];


                if (t.GetProperty(OrderByProperty) != null)
                {
                    if (Direction == "asc")
                    {
                        list = list.OrderBy
                            (
                                a =>
                                {
                                    var l = t.InvokeMember(OrderByProperty, System.Reflection.BindingFlags.GetProperty, null, a,
                                                  null);
                                    if (l.GetType() == j)
                                        return l;
                                    else
                                        return System.ComponentModel.TypeDescriptor.GetConverter(j).ConvertFrom(l);
                                }
                            ).ToList();
                    }
                    else
                    {
                        list = list.OrderByDescending
                            (
                                a =>
                                {
                                    var l = t.InvokeMember(OrderByProperty, System.Reflection.BindingFlags.GetProperty, null, a,
                                                  null);
                                    if (l.GetType() == j)
                                        return l;
                                    else
                                        return System.ComponentModel.TypeDescriptor.GetConverter(j).ConvertFrom(l);
                                }
                            ).ToList();
                    }
                }

            }
        }

        public static List<T> GenericPagingList<T>(FormCollection form, List<T> list)
        {
            List<T> _list = new List<T>();

            if (list.Count > 0)
            {
                if (!String.IsNullOrEmpty(form["iDisplayStart"]) && (!String.IsNullOrEmpty(form["iDisplayLength"])) && form["iDisplayLength"] != "-1")
                {
                    try
                    {
                        _list = list.AsEnumerable().Skip(Convert.ToInt32(form["iDisplayStart"])).Take(Convert.ToInt32(form["iDisplayLength"])).ToList();
                    }
                    catch (InvalidOperationException)
                    {
                        _list = list.AsEnumerable().Skip(0).Take(Convert.ToInt32(form["iDisplayLength"])).ToList();
                    }
                }
            }

            return _list;
        }

        public static string SortListCommand(FormCollection form, String[] conlumn)
        {
            string sOrder = String.Empty;

            if (!String.IsNullOrEmpty(form["iSortCol_0"]) && !String.IsNullOrEmpty(form["iSortingCols"]))
            {
                int columnIndex = Convert.ToInt32(form["iSortCol_0"]);
                string columnName = conlumn[columnIndex];
                sOrder += columnName;
                sOrder += "_" + form["sSortDir_0"];
            }
            return sOrder;
        }

        public static string GetValueColorClass(decimal value = 0)
        {
            if (value == 0)
            {
                return string.Empty;
            }
            else if (value < 0)
            {
                return "negativo";
            }
            else if (value > 0)
            {
                return "positivo";
            }
            return string.Empty;
        }

        public static string GetValueColorClass(int value)
        {
            return GetValueColorClass(Convert.ToDecimal(value));
        }

        public static string CreateRandomPassword(int passwordLength)
        {
            string randomPassword = string.Empty;

            string allowedChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            Random random = new Random();
            var result = new string(
                Enumerable.Repeat(allowedChars, passwordLength)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());
            randomPassword = string.Concat(random.Next(0, 9), result, allowedChars[random.Next(0, 25)]);

            return result;
        }

        public static string CleanupCpfCnpj(string text)
        {
            if (string.IsNullOrWhiteSpace(text))
                return string.Empty;
            var regex = new Regex(@"[^\d]");
            return regex.Replace(text, "");
        }

        public static string FormatCpfCnpj(long value)
        {
            if (value >= 100000000000)
                return value.ToString(@"00\.000\.000\/0000\-00");
            else
                return value.ToString(@"000\.000\.000\-00");
        }

        public static string FormatCpfCnpj(string value)
        {
            value = value.Replace(".", "").Replace("-", "").Replace("/", "");

            long v = Convert.ToInt64(value);
            return FormatCpfCnpj(v);
        }

        private static string _portalVersion;

        public static string GetPortalVersion()
        {
            if (_portalVersion != null)
                return _portalVersion;

            Assembly asm = typeof(CommonHelper).Assembly;
            var version = asm.GetName().Version;

            string appVersion = version.ToString();

            _portalVersion = string.Format("v{0}<br />{1:d}", appVersion, GetBuildDate(asm));
            return _portalVersion;

        }

        private static DateTime GetBuildDate(Assembly asm)
        {
            const int peHeaderOffset = 60;
            const int linkerTimestampOffset = 8;
            var b = new byte[2048];
            System.IO.FileStream s = null;
            try
            {
                s = new System.IO.FileStream(asm.Location, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                s.Read(b, 0, 2048);
            }
            finally
            {
                if (s != null)
                    s.Close();
            }
            var dt = new System.DateTime(1970, 1, 1, 0, 0, 0).AddSeconds(System.BitConverter.ToInt32(b, System.BitConverter.ToInt32(b, peHeaderOffset) + linkerTimestampOffset));
            return dt.AddHours(System.TimeZone.CurrentTimeZone.GetUtcOffset(dt).Hours);
        }

        public static List<SelectListItem> MontarRetornoSemValorBranco(List<Parameters> parameters)
        {
            var lstParameters = new List<SelectListItem>();

            foreach (var item in parameters)
            {
                lstParameters.Add(new SelectListItem { Text = item.Descricao, Value = item.Valor });
            }

            return lstParameters;
        }

        public static int GetDaysInMonth(int month, int year)
        {
            return DateTime.DaysInMonth(year, month);
        }

        public static string FormatFirstDayOfMonth(int month, int year)
        {
            return string.Format("01/{0}/{1}", month.ToString().PadLeft(2,'0'), year.ToString());
        }

        public static string FormatLastDayOfMonth(int month, int year)
        {
            return string.Format("{0}/{1}/{2}", GetDaysInMonth(month,year).ToString(), month.ToString().PadLeft(2, '0'), year.ToString());
        }

        public static string GetMonthsName(int month)
        {
            string name = string.Empty;

            switch (month)
            {
                case 1:
                    name = "Janeiro";
                    break;
                case 2:
                    name = "Fevereiro";
                    break;
                case 3:
                    name = "Março";
                    break;
                case 4:
                    name = "Abril";
                    break;
                case 5:
                    name = "Maio";
                    break;
                case 6:
                    name = "Junho";
                    break;
                case 7:
                    name = "Julho";
                    break;
                case 8:
                    name = "Agosto";
                    break;
                case 9:
                    name = "Setembro";
                    break;
                case 10:
                    name = "Outubro";
                    break;
                case 11:
                    name = "Novembro";
                    break;
                case 12:
                    name = "Dezembro";
                    break;
                default:
                    break;
            }

            return name;
        }

        public static string FormatBoolean(bool? obj)
        {

            return obj.HasValue ? (obj.Value ? "Sim" : "Não") : "-";
        }

        public static List<KeyValuePair<string, int>> GetEnumValuesAndDescriptions<T>()
        {
            Type enumType = typeof(T);

            if (enumType.BaseType != typeof(Enum))
                throw new ArgumentException("T is not System.Enum");

            List<KeyValuePair<string, int>> enumValList = new List<KeyValuePair<string, int>>();

            foreach (var e in Enum.GetValues(typeof(T)))
            {
                var fi = e.GetType().GetField(e.ToString());
                var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

                enumValList.Add(new KeyValuePair<string, int>((attributes.Length > 0) ? attributes[0].Description : e.ToString(), (int)e));
            }

            return enumValList;
        }
    }
}