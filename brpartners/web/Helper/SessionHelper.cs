﻿using System;
using System.Web;
using QX3.Portal.Contracts.DataContracts;
using System.Web.Security;
using QX3.Portal.WebSite.Models;

namespace QX3.Portal.WebSite.Helper
{
    public static class SessionHelper
    {
        public class SessionItemNullException : Exception { }

        public static UserInformation CurrentUser
        {
            get
            {
                if (HttpContext.Current.Session["CurrentUser"] == null)
                {
                    var currentUser = new UserInformation();
                    HttpContext.Current.Session["CurrentUser"] = currentUser;
                    return currentUser;
                }
                return HttpContext.Current.Session["CurrentUser"] as UserInformation;
            }
            set
            {
                HttpContext.Current.Session["CurrentUser"] = value;
            }
        }

        public static bool CheckUserSession()
        {
            var currentUser = CurrentUser;

            if (currentUser == null || string.IsNullOrEmpty(currentUser.Email))
            {
                LogHelper.SaveAuditingLog("Account.SessionTimeOut");
                HttpContext.Current.Session.Abandon();
                return false;
            }

            return true;
        }


        public static int? ThirdPartySuitabilityClient // ID_CLIENTE
        {
            get
            {
                int? client = HttpContext.Current.Session["ThirdPartySuitabilityClient"] as int?;
                return client;
            }
            set
            {
                HttpContext.Current.Session["ThirdPartySuitabilityClient"] = value;
            }
        }

        public static int? ThirdPartySuitabilityClientId // ID
        {
            get
            {
                int? client = HttpContext.Current.Session["ThirdPartySuitabilityClientId"] as int?;
                return client;
            }
            set
            {
                HttpContext.Current.Session["ThirdPartySuitabilityClientId"] = value;
            }
        }

    }
}