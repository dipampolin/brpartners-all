﻿using QX3.Spinnex.Common.Services.Logging;
using QX3.Portal.Contracts.DataContracts;
using System;
using QX3.Portal.WebSite.Helper;
using System.Xml.Linq;
using QX3.Portal.WebSite.LogService;
using QX3.Portal.WebSite.Models.Channel;
using System.Collections.Generic;
using System.Linq;

namespace QX3.Portal.WebSite.Models
{
	public static class LogHelper
	{
		public static void SaveAuditingLog(string moduleName)
		{
			SaveAuditingLog(moduleName, string.Empty);
		}

		public static void SaveAuditingLog(string moduleName, string logData)
		{
			LogManager.InitLog(string.Concat("Auditing.", moduleName));
            
			LogManager.WriteLog(XElement.Parse(string.Format("<auditing>{0}</auditing>", logData)));
		}

		public static void SaveHistoryLog(string moduleName, HistoryData data)
		{
			try
			{
				UserInformation currentUser = SessionHelper.CurrentUser;

				HistoryLog log = new HistoryLog();
				log.ModuleName = moduleName;

				data.Date = DateTime.Now;
				data.ResponsibleID = (int)currentUser.ID;
				log.Data = data;

				IChannelProvider<ILogContractChannel> channelProvider = ChannelProviderFactory<ILogContractChannel>.Create("LogClassName");
				var service = channelProvider.GetChannel().CreateChannel();

				service.Open();
				service.InsertHistory(log);
				service.Close();
			}
			catch (Exception ex)
			{
				LogManager.WriteError(ex);
			}
		}

        public static List<HistoryData> LoadHistory(HistoryFilter filter) 
        {
            AccessControlHelper helper = new AccessControlHelper();
            List<HistoryData> data = new List<HistoryData>();
            IChannelProvider<ILogContractChannel> channelProvider = ChannelProviderFactory<ILogContractChannel>.Create("LogClassName");
            var service = channelProvider.GetChannel().CreateChannel();

            service.Open();
            var history = service.LoadHistory(filter).Result;
            service.Close();

            if (history != null && history.Count() > 0)
            {
                foreach (HistoryData hd in history)
                {
                    if (string.IsNullOrEmpty(hd.Responsible))
                        hd.Responsible = helper.GetUserName(hd.ResponsibleID);
                }
                data = history.ToList();
            }

            service.Close();

            return history != null ? data : new List<HistoryData>();
        }
	}
}