﻿using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.WebSite.Models.Channel;
using QX3.Spinnex.Common.Services.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace QX3.Portal.WebSite.Helper
{
    public static class WebSiteHelper
    {
        #region Funcionalities Tree View

        public static bool insertModel;

        public static HtmlString FuncionalitiesTreeView(this HtmlHelper html, string treeId, List<ACElement> funcionalities, string emptyContent, bool isInsert)
        {
            StringBuilder sb = new StringBuilder();

            insertModel = isInsert;

            try
            {
                var rootItems = FuncionalityChildrenElements(0, funcionalities);

                sb.AppendFormat("<ul id='{0}' class='filetree ignore'>", treeId);

                if (rootItems.Count() == 0)
                {
                    sb.AppendFormat("<li>{0}</li>", emptyContent);
                }

                foreach (ACElement item in rootItems)
                {
                    item.Elements = FuncionalityChildrenElements(item.ID, funcionalities);
                    RenderFuncionalityLi(sb, item);
                    funcionalities.Remove(item);
                    AppendFuncionalityChildren(sb, item, funcionalities);
                }

                sb.AppendLine("</ul>");
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }

            return new HtmlString(sb.ToString());
        }

        private static void AppendFuncionalityChildren(StringBuilder sb, ACElement element, List<ACElement> funcionalities)
        {
            if (element.Elements.Count() == 0)
            {
                sb.AppendLine("</li>");
                return;
            }

            sb.AppendLine(string.Format("<ul class='ignore {0}'>", element.Elements.Any(e => e.IsPage.Equals(false)) ? "funcionalidades" : string.Empty));

            foreach (ACElement item in element.Elements)
            {
                item.Elements = FuncionalityChildrenElements(item.ID, funcionalities);
                RenderFuncionalityLi(sb, item);
                funcionalities.Remove(item);
                AppendFuncionalityChildren(sb, item, funcionalities);
            }

            sb.AppendLine("</ul></li>");
        }

        private static void RenderFuncionalityLi(StringBuilder sb, ACElement item)
        {
            sb.AppendFormat("<li id='fItem{0}' class='ignore {1}' >{5}<span id='alias{0}' class='{2} ignore'>{3}{4}</span><div id='lnkOpenEdit{0}' style='display:none;' class='ico-editar ignore'>editar</div>", item.ID, GetFuncionalityLiClass(item), GetFuncionalitySpanClass(item), GetFuncionalityLiContent(item), GetFuncionalityEditBox(item), GetFuncionalityCheckBox(item));
        }

        private static string GetFuncionalityCheckBox(ACElement item)
        {
            if (insertModel)
                return string.Format("<input type='checkbox' id='chk{0}' />", item.ID);

            return string.Empty;
        }

        private static string GetFuncionalityLiClass(ACElement item)
        {
            if (item.IsPage && item.Elements.Count > 0 && !item.Elements[0].IsPage)
                return "closed";
            return string.Empty;
        }

        private static string GetFuncionalitySpanClass(ACElement item)
        {
            if (item.IsPage)
                return item.Elements.Count > 0 ? "folder" : "file";
            else
                return "file";
        }

        private static string GetFuncionalityLiContent(ACElement item)
        {
            if (item.IsPage)
            {
                string url = GetElementUrl(item.Name);
                return string.Format("<a href='{0}' id='lbl{1}' class='ignore'>{2}</a>", string.IsNullOrEmpty(url) ? "javascript:void(0);" : url, item.ID, item.Alias);
            }
            else
                return string.Format("<label id='lbl{0}'>{1}</label>", item.ID, item.Alias);
        }

        private static string GetFuncionalityEditBox(ACElement item)
        {
            return string.Format("<div id='editBox{0}' style='display:none;' class='linha-frm ignore'><input type='text' class='ignore' id='txtAlias{0}' maxlength='150' value='{1}' /><a href='#' id='btnChangeAlias{0}' class='btn-padrao ignore'><span class='ignore'>salvar</span></a></div>", item.ID, item.Alias);
        }

        private static List<ACElement> FuncionalityChildrenElements(int elementID, List<ACElement> funcionalities)
        {
            var children = (from e in funcionalities where e.ParentID.Equals(elementID) select e);
            if (children != null && children.Count() > 0)
                return children.OrderBy(c => c.Name).ToList();
            return new List<ACElement>();
        }

        #endregion

        #region Breadcrumbs

        public static HtmlString PortalBreadcrumbs(this HtmlHelper html)
        {
            AccessControlHelper helper = new AccessControlHelper();
            var currentUser = SessionHelper.CurrentUser;
            var permissions = (currentUser != null && currentUser.Profile != null) ? SessionHelper.CurrentUser.Profile.Permissions : new List<ACElement>();

            StringBuilder sb = new StringBuilder();

            try
            {
                var selectedItem = (from e in permissions where e.Name.Equals(GetCurrentPageName()) select e).FirstOrDefault();

                if (selectedItem == null || selectedItem.ID == 0)
                {
                    sb.Append("<ul><li class='selecionado ultimo'><span>Portal</span></li></ul>");
                }
                else
                {
                    sb.AppendFormat("<li class='selecionado ultimo'><span>{0}</span></li></ul>", selectedItem.Alias);

                    if (selectedItem.ParentID != 0)
                    {
                        bool endOfBreadCrumb = false;
                        int currentNodeID = selectedItem.ParentID;
                        while (!endOfBreadCrumb)
                            BreadCrumbsParent(sb, ref currentNodeID, ref permissions, ref endOfBreadCrumb);

                    }
                    sb.Insert(0, "<ul>");

                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }
            return new HtmlString(sb.ToString());
        }

        private static void BreadCrumbsParent(StringBuilder sb, ref int currentNodeID, ref List<ACElement> permissions, ref bool endOfBreadCrumb)
        {
            var parentID = currentNodeID;
            var currentNode = permissions.Where(p => p.ID.Equals(parentID)).FirstOrDefault();
            if (currentNode == null)
                return;

            string url = GetElementUrl(currentNode.Name);
            sb.Insert(0, string.IsNullOrEmpty(url) ? string.Format("<li><span>{0}</span></li>", currentNode.Alias) : string.Format("<li><a href='{0}'><span>{1}</span></a></li>", url, currentNode.Alias));

            currentNodeID = currentNode.ParentID;
            if (currentNode.ParentID == 0)
                endOfBreadCrumb = true;
        }

        #endregion

        #region Menu

        public static HtmlString NewPortalMenu(this HtmlHelper html)
        {
            StringBuilder sbMenu = new StringBuilder();

            try
            {
                var permissions = AccessControlHelper.AllowedElements;

                var rootItems = MenuChildrenElements(0, permissions).OrderBy(e => e.Alias);

                int rootItemsCounter = rootItems.Count();

                if (rootItemsCounter > 0)
                {
                    int LastUlCounter = (int)Math.Round((decimal)(rootItemsCounter / 2));
                    int auxCounter = rootItemsCounter;

                    foreach (ACElement item in rootItems)
                    {
                        if (auxCounter == rootItemsCounter)
                            sbMenu.AppendLine("<div class='caixa-secoes'><ul class='secoes'>");
                        else
                            if (auxCounter == LastUlCounter)
                            sbMenu.AppendLine("</ul></div><div class='caixa-secoes'><ul class='secoes'>");

                        item.Elements = MenuChildrenElements(item.ID, permissions);
                        RenderMenuLi(sbMenu, item);
                        AppendMenuChildren(sbMenu, item, permissions);

                        if (auxCounter == 1)
                            sbMenu.AppendLine("</ul></div>");

                        auxCounter--;
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }
            return new HtmlString(sbMenu.ToString());
        }

        public static HtmlString PortalMenu(this HtmlHelper html)
        {
            StringBuilder sbMenu = new StringBuilder();

            try
            {
                var permissions = AccessControlHelper.AllowedElements;

                var rootItems = MenuChildrenElements(0, permissions);

                sbMenu.Append("<ul class='secoes'>");

                if (rootItems.Count() > 0)
                {
                    foreach (ACElement item in rootItems)
                    {
                        item.Elements = MenuChildrenElements(item.ID, permissions);
                        RenderMenuLi(sbMenu, item);
                        AppendMenuChildren(sbMenu, item, permissions);
                    }
                }

                sbMenu.AppendLine("</ul>");
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }

            return new HtmlString(sbMenu.ToString());

        }

        private static void AppendMenuChildren(StringBuilder sbMenu, ACElement element, List<ACElement> permissions)
        {
            if (element.Elements.Count() == 0)
            {
                sbMenu.AppendLine("</li>");
                return;
            }

            bool show = false;
            sbMenu.AppendFormat("<ul style='display:{0};'>", CheckMenuUlVisibility(ref show, element.Elements, permissions) ? "block" : "none");

            foreach (ACElement item in element.Elements)
            {
                item.Elements = MenuChildrenElements(item.ID, permissions);
                RenderMenuLi(sbMenu, item);
                AppendMenuChildren(sbMenu, item, permissions);
            }

            sbMenu.AppendLine("</ul></li>");
        }

        private static bool CheckMenuUlVisibility(ref bool show, List<ACElement> children, List<ACElement> permissions)
        {
            string currentPageName = GetCurrentPageName();

            foreach (ACElement e in children)
            {
                if (e.Name == currentPageName)
                    show = true;
                else
                {
                    var eChildren = (from p in permissions where p.ParentID.Equals(e.ID) && p.IsPage.Equals(true) select p).ToList();
                    if (eChildren != null && eChildren.Count > 0)
                        CheckMenuUlVisibility(ref show, eChildren, permissions);
                }
            }
            return show;
        }

        private static void RenderMenuLi(StringBuilder sbMenu, ACElement item)
        {


            if (item.ParentID == 0 && item.Name.IndexOf('.') < 0)
                sbMenu.AppendFormat("<li><span id='{1}' class='clicavelNo0' onclick='OpenSubMenu(this)'>{0}</span>", item.Alias, item.ID);
            else
            {
                string url = GetElementUrl(item.Name);
                if (string.IsNullOrEmpty(url))
                    sbMenu.AppendFormat("<li><span class='clicavel item-fechado' id='{1}' onclick='(this)'>{0}</span>", item.Alias, item.ID);
                else
                {
                    if (item.ParentID == 0)
                        sbMenu.AppendFormat("<li class='sem-filhos'><span><a href='{0}'>{1}</a></span>", url, item.Alias);
                    else
                    {
                        if (item.Visible)
                        {
                            sbMenu.AppendFormat("<li><a href='{0}'>{1}</a>", url, item.Alias);
                        }
                    }
                }
            }


        }

        private static List<ACElement> MenuChildrenElements(int elementID, List<ACElement> funcionalities)
        {
            var children = (from e in funcionalities where e.ParentID.Equals(elementID) && e.IsPage.Equals(true) select e);
            if (children != null && children.Count() > 0)
                return children.OrderBy(c => c.Alias).ToList();
            return new List<ACElement>();
        }

        #endregion

        private static string GetCurrentPageName()
        {
            try
            {
                string url = HttpContext.Current.Request.Url.AbsolutePath;
                string[] urlItems = url.Split('/').Where(s => s != "").ToArray();
                int urlCount = urlItems.Count();

                if (urlCount == 0)
                {
                    return "";
                }
                else if (urlItems[0].Equals("RH"))
                {
                    var sd = string.Format("{0}.{1}.{2}", urlItems[0], urlItems[1], urlCount > 2 ? urlItems[2] : "Index");
                    return string.Format("{0}.{1}.{2}", urlItems[0], urlItems[1], urlCount > 2 ? urlItems[2] : "Index");
                }

                return string.Format("{0}.{1}", urlItems[0], urlCount > 1 ? urlItems[1] : "Index");
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return "";
            }
        }

        private static string GetElementUrl(string elementName)
        {

            if (!elementName.Contains('.'))
            {
                return string.Empty;
            }

            string elementUrl = string.Empty;
            try
            {
                string[] elemItems = elementName.Split('.');
                int elemCount = elemItems.Count();

                elementUrl = "/" + string.Join("/", elemItems);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }
            return elementUrl;
        }

        public static string AbsoluteAction(this UrlHelper url, string action, string controller)
        {
            Uri requestUrl = url.RequestContext.HttpContext.Request.Url;

            string absoluteAction = string.Format("{0}://{1}{2}",
                                                  requestUrl.Scheme,
                                                  requestUrl.Authority,
                                                  url.Action(action, controller));

            return absoluteAction;
        }

        public static string AbsoluteAction(this UrlHelper url, string content)
        {
            Uri requestUrl = url.RequestContext.HttpContext.Request.Url;

            string absoluteAction = string.Format("{0}://{1}{2}",
                                                  requestUrl.Scheme,
                                                  requestUrl.Authority,
                                                  VirtualPathUtility.ToAbsolute(content));

            return absoluteAction;
        }

        #region Ranges

        public static string ConvertNumListToRangeStr(List<int> numList)
        {
            return ConvertNumListToPossiblyDegenerateRanges(numList).Select(r => PrettyRange(r)).Intersperse(",");
        }

        public static List<int> ConvertRangeStrToNumList(string rangeStr)
        {
            List<int> list = new List<int>();

            foreach (char c in rangeStr)
                if ("0123456789,-".IndexOf(c) < 0)
                    return null;

            if (string.IsNullOrEmpty(rangeStr))
                return null;

            string[] assParams = rangeStr.Split(',');
            foreach (string ap in assParams)
            {
                if (string.IsNullOrEmpty(ap))
                    continue;
                string[] assParams2 = ap.Split('-');
                if (assParams2.Length > 2)
                    return null;
                else
                {
                    int start = Int32.Parse(assParams2[0]);
                    list.Add(start);
                    if (assParams2.Length == 2)
                    {
                        int end = Int32.Parse(assParams2[1]);
                        for (int i = start + 1; i <= end; i++)
                            list.Add(i);
                    }
                }
            }

            return list;
        }

        public static List<int> GetFullListOfAssessors()
        {
            List<int> list = new List<int>();

            return list;
        }

        public static string RemoveSpecialChars(string word)
        {
            string newWord = word;
            newWord = Regex.Replace(newWord, "[áàâãª]", "a");
            newWord = Regex.Replace(newWord, "[ÁÀÂÃ]", "A");
            newWord = Regex.Replace(newWord, "[éèê]", "e");
            newWord = Regex.Replace(newWord, "[ÉÈÊ]", "e");
            newWord = Regex.Replace(newWord, "[íìî]", "i");
            newWord = Regex.Replace(newWord, "[ÍÌÎ]", "I");
            newWord = Regex.Replace(newWord, "[óòôõº]", "o");
            newWord = Regex.Replace(newWord, "[ÓÒÔÕ]", "O");
            newWord = Regex.Replace(newWord, "[úùû]", "u");
            newWord = Regex.Replace(newWord, "[ÚÙÛ]", "U");
            newWord = Regex.Replace(newWord, "[ç]", "c");
            newWord = Regex.Replace(newWord, "[Ç]", "C");
            return newWord;
        }

        /// <summary>
        /// e.g. 1,3,5,6,7,8,9,10,12
        /// becomes
        /// (1,1),(3,3),(5,10),(12,12)
        /// </summary>
        public static IEnumerable<Tuple<int, int>> ConvertNumListToPossiblyDegenerateRanges(IEnumerable<int> numList)
        {
            Tuple<int, int> currentRange = null;
            foreach (var num in numList)
            {
                if (currentRange == null)
                {
                    currentRange = Tuple.Create(num, num);
                }
                else if (currentRange.Item2 == num - 1)
                {
                    currentRange = Tuple.Create(currentRange.Item1, num);
                }
                else
                {
                    yield return currentRange;
                    currentRange = Tuple.Create(num, num);
                }
            }
            if (currentRange != null)
            {
                yield return currentRange;
            }
        }

        /// <summary>
        /// e.g. (1,1) becomes "1"
        /// (1,3) becomes "1-3"
        /// </summary>
        /// <param name="range"></param>
        /// <returns></returns>
        public static string PrettyRange(Tuple<int, int> range)
        {
            if (range.Item1 == range.Item2)
            {
                return range.Item1.ToString();
            }
            return string.Format("{0}-{1}", range.Item1, range.Item2);
        }

        public static string Intersperse(this IEnumerable<string> items, string interspersand)
        {
            var currentInterspersand = "";
            var result = new StringBuilder();
            foreach (var item in items)
            {
                result.Append(currentInterspersand);
                result.Append(item);
                currentInterspersand = interspersand;
            }
            return result.ToString();

        }

        #endregion

        #region EnumDropDown

        private static Type GetNonNullableModelType(ModelMetadata modelMetadata)
        {
            Type realModelType = modelMetadata.ModelType;

            Type underlyingType = Nullable.GetUnderlyingType(realModelType);
            if (underlyingType != null)
            {
                realModelType = underlyingType;
            }
            return realModelType;
        }

        private static Type GetNonNullableModelType(object realModelType)
        {

            Type underlyingType = Nullable.GetUnderlyingType(realModelType.GetType());
            if (underlyingType != null)
            {
                return underlyingType;
            }
            return realModelType.GetType();
        }

        private static readonly SelectListItem[] SingleEmptyItem = new[] { new SelectListItem { Text = "--Selecione--", Value = "" } };

        public static string GetEnumDescription<TEnum>(TEnum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            var value1 = (int)fi.GetValue(value);

            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if ((attributes != null) && (attributes.Length > 0))
                return attributes[0].Description;
            else
                return value.ToString();
        }

        public static int GetEnumValue<TEnum>(TEnum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            return (int)fi.GetValue(value);
        }

        public static MvcHtmlString EnumDropDownListFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum>> expression)
        {
            return EnumDropDownListFor(htmlHelper, expression, null);
        }
        /// <summary>
        /// Monta uma drop list (select) a partir de uma instância de um enumerador. É preferível que os valores da enumeração tenham o atributo "Description".
        /// </summary>
        /// <typeparam name="TEnum">Tipo da enumeração. Definido através de uma instância</typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="name">Nome do campo.</param>
        /// <param name="enumeration">Instância de uma enumeração</param>
        /// <param name="htmlAttributes">Opcional: atributos html opcionais para o campo select retornado</param>
        /// <returns></returns>
        public static MvcHtmlString EnumDropDownList<TEnum>(this HtmlHelper htmlHelper, string name, TEnum enumeration, int? selected, object htmlAttributes)
        {

            IEnumerable<TEnum> values = Enum.GetValues(enumeration.GetType()).Cast<TEnum>();

            IEnumerable<SelectListItem> items = from value in values
                                                select new SelectListItem
                                                {
                                                    Text = GetEnumDescription(value),
                                                    Value = GetEnumValue(value).ToString()
                                                    ,
                                                    Selected = selected.HasValue ? GetEnumValue(value).Equals(selected.Value) : false
                                                };

            return htmlHelper.DropDownList(name, items, "--", htmlAttributes);
        }

        public static MvcHtmlString EnumDropDownList<TEnum>(this HtmlHelper htmlHelper, string name, TEnum enumeration, int? selected)
        {
            return EnumDropDownList(htmlHelper, name, enumeration, selected, null);
        }

        public static MvcHtmlString EnumDropDownListFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum>> expression, object htmlAttributes)
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            Type enumType = GetNonNullableModelType(metadata);
            IEnumerable<TEnum> values = Enum.GetValues(enumType).Cast<TEnum>();

            IEnumerable<SelectListItem> items = from value in values
                                                select new SelectListItem
                                                {
                                                    Text = GetEnumDescription(value),
                                                    Value = GetEnumValue(value).ToString(),
                                                    Selected = value.Equals(metadata.Model)
                                                };

            if (metadata.IsNullableValueType)
                items = SingleEmptyItem.Concat(items);

            //return htmlHelper.DropDownListFor(expression, items, htmlAttributes);
            return htmlHelper.DropDownList(htmlAttributes.GetType().GetProperty("id").GetValue(htmlAttributes, null).ToString(), items, null, htmlAttributes);
        }

        #endregion

        #region EnumRadioGroup

        private static MvcHtmlString _internalRadioEnumList<TEnum>(IEnumerable<TEnum> values, object htmlAttributes, int? selectedValue)
        {
            StringBuilder stb = new StringBuilder();

            Random r = new Random();

            var sName = (htmlAttributes != null)
                    ? htmlAttributes.GetType().GetProperty("name") != null
                        ? htmlAttributes.GetType().GetProperty("name").GetValue(htmlAttributes, null)
                        : "rdbRadio_" + r.Next(10000)
                    : "rdbRadio_" + r.Next(10000);

            foreach (var value in values)
            {
                string sId = (htmlAttributes != null)
                        ? htmlAttributes.GetType().GetProperty("id") != null
                            ? htmlAttributes.GetType().GetProperty("id").GetValue(htmlAttributes, null) + "_" + r.Next(10000)
                            : "rdbRadioId_" + r.Next(10000)
                        : "rdbRadioId_" + r.Next(10000);

                string sReturn = (htmlAttributes != null)
                        ? htmlAttributes.GetType().GetProperty("wordwrap") != null
                            ? (bool)htmlAttributes.GetType().GetProperty("wordwrap").GetValue(htmlAttributes, null) == false
                                ? ""
                                : "<span class='quebrar'></span>"
                            : "<span class='quebrar'></span>"
                        : "<span class='quebrar'></span>";

                bool bChecked = GetEnumValue(value).Equals(selectedValue);

                string s = String.Format("<input type=\"{0}\" name=\"{1}\" id=\"{2}\" value=\"{3}\" {4} />{5}{6}", "radio", sName, sId, GetEnumValue(value).ToString(), bChecked ? "checked='checked'" : "", GetEnumDescription(value), sReturn);
                stb.Append(s);
            }

            return MvcHtmlString.Create(stb.ToString());
        }

        private static MvcHtmlString _internalCheckEnumList<TEnum>(IEnumerable<TEnum> values, object htmlAttributes, List<int> selectedValues)
        {
            StringBuilder stb = new StringBuilder();

            Random r = new Random();

            var sName = (htmlAttributes != null)
                    ? htmlAttributes.GetType().GetProperty("name") != null
                        ? htmlAttributes.GetType().GetProperty("name").GetValue(htmlAttributes, null)
                        : "rdbRadio_" + r.Next(10000)
                    : "rdbRadio_" + r.Next(10000);

            foreach (var value in values)
            {
                string sId = (htmlAttributes != null)
                        ? htmlAttributes.GetType().GetProperty("id") != null
                            ? htmlAttributes.GetType().GetProperty("id").GetValue(htmlAttributes, null) + "_" + r.Next(10000)
                            : "rdbRadioId_" + r.Next(10000)
                        : "rdbRadioId_" + r.Next(10000);

                string sReturn = (htmlAttributes != null)
                        ? htmlAttributes.GetType().GetProperty("wordwrap") != null
                            ? (bool)htmlAttributes.GetType().GetProperty("wordwrap").GetValue(htmlAttributes, null) == false
                                ? ""
                                : "<span class='quebrar'></span>"
                            : "<span class='quebrar'></span>"
                        : "<span class='quebrar'></span>";

                bool bChecked = (selectedValues != null) && selectedValues.Exists(a => a == GetEnumValue(value));

                string s = String.Format("<input type=\"{0}\" name=\"{1}\" id=\"{2}\" value=\"{3}\" {4} />{5}{6}", "checkbox", sName, sId, GetEnumValue(value).ToString(), bChecked ? "checked='checked'" : "", GetEnumDescription(value), sReturn);
                stb.Append(s);
            }

            return MvcHtmlString.Create(stb.ToString());
        }

        public static MvcHtmlString EnumRadioGroupFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum>> expression, object htmlAttributes)
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            Type enumType = GetNonNullableModelType(metadata);
            IEnumerable<TEnum> values = Enum.GetValues(enumType).Cast<TEnum>();

            int? selected = null; if (metadata.Model != null) selected = (int)metadata.Model;
            return _internalRadioEnumList(values, htmlAttributes, selected);
        }

        public static MvcHtmlString EnumRadioGroup<TEnum>(this HtmlHelper htmlHelper, TEnum enumeration, int? selectedValue, object htmlAttributes)
        {
            IEnumerable<TEnum> values = Enum.GetValues(enumeration.GetType()).Cast<TEnum>();

            return _internalRadioEnumList(values, htmlAttributes, selectedValue);

        }
        #endregion

        #region EnumCheckBox

        /*public static MvcHtmlString EnumCheckBoxFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum>> expression, object htmlAttributes)
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            Type enumType = GetNonNullableModelType(metadata);
            IEnumerable<TEnum> values = Enum.GetValues(enumType).Cast<TEnum>();

            //List int? selected = null; if (metadata.Model != null) selected = (int)metadata.Model;
            return _internalCheckEnumList(values, htmlAttributes, null);
        }*/

        public static MvcHtmlString EnumCheckBox<TEnum>(this HtmlHelper htmlHelper, TEnum enumeration, List<int> selectedValues, object htmlAttributes)
        {
            IEnumerable<TEnum> values = Enum.GetValues(enumeration.GetType()).Cast<TEnum>();

            return _internalCheckEnumList(values, htmlAttributes, selectedValues);

        }
        #endregion

    }
}