﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using QX3.Portal.WebSite.Models;
using System.IO;
using iTextSharp.text.pdf;

namespace QX3.Portal.WebSite.Helper
{
    public class PDFGenerator
    {
        /// <summary>
        /// Gera o PDF
        /// </summary>
        /// <param name="data"></param>
        /*public static Byte[] GeneratePDF(ContractInfo PDFInfo)
        {
            int count = 0;

            try
            {
                if (PDFInfo != null)
                {
                    string NewPDF = PDFInfo.PDFPath + PDFInfo.FileName + ".pdf";

                    if (File.Exists(NewPDF))
                    {
                        File.GetAccessControl(NewPDF);
                        File.Delete(NewPDF);
                    }

                    File.Copy(PDFInfo.PDFModelPath, NewPDF);

                    Exception ex = new Exception("PDFModelPath: " + PDFInfo.PDFModelPath);

                    PdfReader reader = new PdfReader(PDFInfo.PDFModelPath);
                    PdfStamper stamper = new PdfStamper(reader, new FileStream(NewPDF, FileMode.Create));
                    //MemoryStream m = new MemoryStream();
                    //PdfStamper stamper = new PdfStamper(reader, m);
                    AcroFields fields = stamper.AcroFields;

                    //preenchendo os campos do PDF
                    foreach (ContractField cf in PDFInfo.ContractFields)
                    {
                        count++;
                        fields.SetField(cf.Field, (cf.FieldContent != null ? cf.FieldContent.ToUpper() : cf.FieldContent));
                    }

                    stamper.FormFlattening = true;
                    stamper.Close();

                    FileStream fs = File.OpenRead(NewPDF);
                    Byte[] PDF = new Byte[fs.Length];
                    fs.Read(PDF, 0, PDF.Length);
                    fs.Close();
                    
                    return PDF;

                }
            }
            catch (Exception ex)
            {
                int teste = count;
            }

            return null;
        }*/

        public static Byte[] GeneratePDF(ContractInfo PDFInfo)
        {
            int count = 0;

            try
            {
                if (PDFInfo != null)
                {

                    Exception ex = new Exception("PDFModelPath: " + PDFInfo.PDFModelPath);

                    PdfReader reader = new PdfReader(PDFInfo.PDFModelPath);

                    MemoryStream m = new MemoryStream();
                    PdfStamper stamper = new PdfStamper(reader, m);
                    AcroFields fields = stamper.AcroFields;

                    foreach (ContractField cf in PDFInfo.ContractFields)
                    {
                        count++;
                        fields.SetField(cf.Field, (cf.FieldContent != null ? cf.FieldContent.ToUpper() : cf.FieldContent));
                    }

                    stamper.FormFlattening = true;
                    stamper.Close();

                    reader.Close();
                    m.Flush();

                    Byte[] PDF = m.ToArray();
                    m.Close();
                    return PDF;

                }
            }
            catch (Exception ex)
            {
                int teste = count;
            }

            return null;
        }
    }
}