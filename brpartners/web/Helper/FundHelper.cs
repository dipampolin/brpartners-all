﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.Services.SagazWebService;
using QX3.Portal.WebSite.FundService;
using QX3.Portal.WebSite.Models;
using QX3.Portal.WebSite.Models.Channel;
using QX3.Portal.WebSite.NonResidentTradersService;
using QX3.Portal.WebSite.OtherService;

namespace QX3.Portal.WebSite.Helper
{
    public class FundHelper
    {
        private static IChannelProvider<IFundContractChannel> fndChn;
        protected static IChannelProvider<IFundContractChannel> FundProvider
        {
            get
            {
                if (fndChn == null)
                    fndChn = ChannelProviderFactory<IFundContractChannel>.Create("FundClassName");
                return fndChn;
            }

        }

        public string FundBalanceResponse(FundBalance response, ref FormCollection form, int iCount)
        {
            FundBalanceResponse dtJson = new FundBalanceResponse();

            dtJson.sEcho = Convert.ToInt32(form["sEcho"]);

            if (response == null) response = new FundBalance();

            dtJson.iTotalRecords = iCount;
            dtJson.iTotalDisplayRecords = iCount;

            var tableValues = new List<string[]>();
            List<String> rowData = new List<String>();

            if (response != null)
            {
                foreach (FundBalanceItem sub in response.Items)
                {
                    rowData.Add(sub.CautionNumber);
                    rowData.Add(sub.QuantityOfQuota.ToString("N9"));
                    rowData.Add(sub.ApplicationDate.ToShortDateString());
                    rowData.Add(sub.ConversionDate.ToShortDateString());
                    rowData.Add(sub.ApplicationValue.ToString("N2"));
                    rowData.Add(sub.CorrectedValue.ToString("N2"));
                    rowData.Add(sub.ProvisionIOF.ToString("N2"));
                    rowData.Add(sub.ProvisionIRRF.ToString("N2"));
                    rowData.Add(sub.RedemptionValue.ToString("N2"));
                    rowData.Add(sub.Income.ToString("N2"));

                    tableValues.Add(rowData.ToArray());
                    rowData = new List<string>();
                }
            }

            dtJson.aaData = tableValues;

            dtJson.GrossValue = response.GrossValue.ToString("N2");
            dtJson.NetValue = response.NetValue.ToString("N2");
            dtJson.BalanceDate = response.Date.ToString("dd/MM/yyyy");
			dtJson.FundName = response.FundName;

            return Newtonsoft.Json.JsonConvert.SerializeObject(dtJson);
        }

        public static decimal? GetUserBalance(FundClient client)
        {
            decimal? result = null;
            FinancialPositionDataItem data = null;
            
            using (var service = FundProvider.GetChannel().CreateChannel()) 
            {
                data = service.FN_GetUserBalance(client).Result;
                if (data != null)
                {
                    decimal balance = data.CurrentAccountInitialBalance + data.SettledBalanceToDate;
                    result = balance;
                }
                else
                    result = 0;
            }

            return result;
        }

        public string BalancePositionResponse(ref FundClient client)
        {

            decimal availableBalance = 0;
            decimal? balance = FundHelper.GetUserBalance(client);

            if (balance.HasValue)
            {
                client.PositionBalance = balance.Value;
                availableBalance = balance.Value;

                using (var service = FundProvider.GetChannel().CreateChannel())
                {
                    decimal blocked = service.FN_LoadBlockedBalance(client.ClientCode).Result;
                    if (blocked > 0)
                    {
                        client.BlockedBalance = blocked;

                        decimal available = balance.Value - blocked;
                        client.AvailableBalance = available;
                        availableBalance = available;
                    }
                }
            }

            return Newtonsoft.Json.JsonConvert.SerializeObject(client);
        }

        public FundClient GetFundClient(int clientCode) 
        {
            var provider = ChannelProviderFactory<INonResidentTradersServiceContractChannel>.Create("NonResidentTradersClassName");
            var otherService = ChannelProviderFactory<IOtherContractChannel>.Create("OtherClassName").GetChannel().CreateChannel();

            using (var service = provider.GetChannel().CreateChannel())
            {
                var customer = service.LoadCustomerName(clientCode).Result;//CommonHelper.GetClientName(clientCode);

                if (!String.IsNullOrEmpty(customer))
                {
                    otherService.Open();

                    var obj = otherService.LoadClientInformation(clientCode.ToString(), "", CommonHelper.GetLoggedUserGroupId());

                    if (obj.Result == null)
                        return null;

                    var client = new FundClient()
                    {
                        DateBalance = DateTime.Now,
                        ClientCode = Convert.ToInt32(clientCode),
                        ClientName = customer,
                        ClientEmail = service.LoadClientEmail(clientCode).Result,
                        FormattedClientCode = (obj.Result != null && obj.Result.Client != null) ? obj.Result.Client.Value.PadLeft(11, '0') : clientCode.ToString().PadLeft(11, '0')
                    };

                    this.BalancePositionResponse(ref client);

                    otherService.Close();

                   return client;
                }
                else { return null; }
            }            
        }

        public static string TaxFormatter(decimal tax, decimal value)
        {
            string taxText = string.Format("{0}%", tax.ToString("N2"));
            string taxValue = (value * (tax / 100)).ToString("N2");
            return string.Format("{0} / R$ {1}", taxText, taxValue);
        }

        public static string HourMinuteFormatter(string timeLimit) 
        {
            timeLimit = timeLimit.Trim();

            if (timeLimit.Length < 3)
                return string.Concat(timeLimit, "h");

            return timeLimit.Replace("00", "").Replace(":", "h");
        }
    }
}