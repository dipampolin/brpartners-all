﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QX3.Portal.WebSite.Helper
{
    public class RedirectHelper : RedirectResult
    {

        public RedirectHelper(string url)
            : base(url)
        {

        }

        public override void ExecuteResult(ControllerContext context)
        {
            /*Caso a requisição seja em Ajax, executa um JavaScriptResult, direcionando para a página escolhida.
              Senão, executa o método original.
             */

            if (context.RequestContext.HttpContext.Request.IsAjaxRequest())
            {
                UrlHelper h = new UrlHelper(context.RequestContext);
                
                string destinationUrl = h.Content(Url);

                JavaScriptResult result = new JavaScriptResult()
                {
                    Script = String.Format("window.location='{0}';", destinationUrl)
                };
                result.ExecuteResult(context);
            }
            else
                base.ExecuteResult(context);
        }
    }
}