﻿using System.Collections.Generic;
using ICSharpCode.SharpZipLib.Zip;
using System.IO;
using QX3.Spinnex.Common.Services.Logging;
using System;
using Newtonsoft.Json;
using System.Xml.Linq;

namespace QX3.Portal.WebSite.Models
{

	public class ZipHelper
	{
		public byte[] GenerateZipFile(List<ZipItem> filesToZip, string folderPath)
		{
			string tempPath = Path.GetTempPath();
			string filesPath = string.Concat(tempPath, folderPath);
			LogManager.WriteLog("filesPath:" + filesPath + "</filesPath>");

			this.DeleteFiles(filesPath);

			foreach (ZipItem z in filesToZip)
				this.SaveMemoryStream(z, filesPath);

			MemoryStream outputMemStream = new MemoryStream();
			ZipOutputStream zos = new ICSharpCode.SharpZipLib.Zip.ZipOutputStream(outputMemStream);
			zos.IsStreamOwner = false;
		    zos.UseZip64 = UseZip64.Off;


			string[] files = Directory.GetFiles(filesPath);
			LogManager.WriteLog("<zipFilesDirectoryGetFiles>" + JsonConvert.SerializeObject(files) + "</zipFilesDirectoryGetFiles>");
			byte[] buffer = new byte[4096];
			foreach (string f in files)
			{
				LogManager.WriteLog("<file>" + f.Length + "</file>");

				FileInfo fi = new FileInfo(f);
				if (!fi.Exists)
					continue;

				string entryName = f;
				entryName = ZipEntry.CleanName(entryName);
				var entries = entryName.Split('/');
				if (entries.Length > 0)
					entryName = entries[entries.Length - 1];

				ZipEntry ze = new ZipEntry(entryName);

				zos.PutNextEntry(ze);

				var fs = fi.OpenRead();
				while (fs.Position < fs.Length)
				{
					int bytesRead = fs.Read(buffer, 0, buffer.Length);
					zos.Write(buffer, 0, bytesRead);
				}
				fs.Close();
			}
			zos.Finish();

			byte[] buf = new byte[outputMemStream.Position];
			outputMemStream.Position = 0;
			outputMemStream.Read(buf, 0, buf.Length);
			outputMemStream.Flush();
			outputMemStream.Close();

			//this.DeleteFiles(filesPath);

			LogManager.WriteLog("<buffer>" + buf.Length + "</buffer>");

			return buf;
		}

		private void SaveMemoryStream(ZipItem item, string filePath)
		{
			try
			{
				bool directoryExists = Directory.Exists(filePath);
				if (!directoryExists)
					directoryExists = Directory.CreateDirectory(filePath).Exists;

				LogManager.WriteLog("directoryExists:" + directoryExists);

				if (directoryExists)
				{
					string file = string.Concat(filePath, item.FileName);
					FileStream fileStream = new FileStream(file, FileMode.OpenOrCreate);
					item.FileStream.WriteTo(fileStream);
					fileStream.Flush();
					fileStream.Close();
				}
			}
			catch (Exception ex)
			{
				LogManager.WriteError(ex);
			}
		}

		private void DeleteFiles(string filePath)
		{
			try
			{
				var files = Directory.GetFiles(filePath);
				foreach (string f in files)
					File.Delete(f);
			}
			catch (Exception ex)
			{
				LogManager.WriteError(ex);
			}
		}
	}
}