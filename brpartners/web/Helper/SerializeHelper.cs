﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.IO;
using System.ComponentModel;

namespace QX3.Portal.WebSite.Helper
{
    public class SerializeHelper
    {
        private static string FilePath 
        {
            get 
            {
                return Path.GetTempPath();   
                //return @"C:\\teste\\";
            }
        }

        public static void SerializeObject(object obj, string filename) 
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(obj.GetType());

                using (FileStream file = new FileStream(SerializeHelper.FilePath + filename, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite))
                {
                    serializer.Serialize(file, obj);
                }
            }
            catch
            {
                HttpContext.Current.Session["ObjectSerialized_" + filename] = obj;   
            }            
        }

        public static object DeserializeObject(string filename, Type type) 
        {
            object obj;
            try
            {               
                XmlSerializer serializer = new XmlSerializer(type);

                using (FileStream file = new FileStream(SerializeHelper.FilePath + filename, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    obj = serializer.Deserialize(file);
                }
                return obj;
            }
            catch
            {
                return null;
            }
        }
    }
}