﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using QX3.Portal.Contracts.DataContracts;
using System.Web.Caching;
using QX3.Portal.WebSite.Models.Channel;
using QX3.Portal.WebSite.MarketService;
using QX3.Spinnex.Common.Services.Logging;
using System.Web.Mvc;
using QX3.Portal.WebSite.CommonService;

namespace QX3.Portal.WebSite.Helper
{
    public class MarketHelper
    {
        protected static int StocksCache
        {
            get
            {
                int cache = 24;
                Int32.TryParse(ConfigurationManager.AppSettings["Stocks.Cache"], out cache);
                return cache;
            }
        }

        public static Dictionary<string, string> CompanyFilter
        {
            get
            {
                var company = new Dictionary<string, string>();
                company.Add("T", "--selecione--");
                company.Add("P", "Cod. Papel");
                company.Add("E", "Empresa");
                return company;
            }
        }

        public List<Stock> LoadStocks()
        {
            if (HttpContext.Current.Cache["Market.Stocks"] == null || ((List<Stock>)HttpContext.Current.Cache["Market.Stocks"]).Count <= 0)
            {
                WcfResponse<Stock[]> response = new WcfResponse<Stock[]>();
                try
                {
                    IChannelProvider<IMarketContractChannel> channelProvider = ChannelProviderFactory<IMarketContractChannel>.Create("MarketClassName");
                    var service = channelProvider.GetChannel().CreateChannel();

                    service.Open();
                    response = service.LoadStocks();
                    service.Close();
                }
                catch (Exception ex)
                {
                    LogManager.WriteError(ex);
                }

                HttpContext.Current.Cache.Insert("Market.Stocks", response.Result == null ? new List<Stock>() : response.Result.ToList(), null, Cache.NoAbsoluteExpiration, TimeSpan.FromHours(StocksCache));
            }
            return (List<Stock>)HttpContext.Current.Cache["Market.Stocks"];
        }

        public string GetAssessorsRangesToFilter(string assessors)
        {
            /*string list = string.Empty;
            var userList = SessionHelper.CurrentUser.Assessors;
            List<int> ids = new List<int>();

            if (!String.IsNullOrEmpty(assessors))
            {
                ids = WebSiteHelper.ConvertRangeStrToNumList(assessors);
                if (userList != null && userList.Count > 0 && !userList.Contains(0))
                    ids = (from i in ids.OfType<int>() where userList.Contains(i) select i).ToList();
            }
            else
                ids = userList;

            string assessorsList;

            if (ids.Count > 0)
                assessorsList = ids.Count > 0 ? (String.Join(";", ids) + ';') : "";
            else if (ids.Count == 0 && userList.Count > 0)
                //
                assessorsList = "-1;";
            else
                assessorsList = "";

            return assessorsList;*/

            string list = string.Empty;
            List<int> ids = new List<int>();

            if (!String.IsNullOrEmpty(assessors))
            {
                ids = WebSiteHelper.ConvertRangeStrToNumList(assessors);
            }

            string assessorsList;

            if (ids.Count > 0)
                assessorsList = ids.Count > 0 ? (String.Join(";", ids) + ';') : "";
            else
                assessorsList = "";

            return assessorsList;
        }
               

        public string GetClientsRangesToFilter(string clients)
        {
            List<int> ids = WebSiteHelper.ConvertRangeStrToNumList(clients);
            return (ids != null && ids.Count > 0) ? (String.Join(";", ids) + ';') : "";
        }

        public string GetAssessorName(int code)
        {
            try
            {

                IChannelProvider<IMarketContractChannel> channelProvider = ChannelProviderFactory<IMarketContractChannel>.Create("MarketClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                var response = service.LoadAssessorName(code);

                service.Close();

                return response.Result;
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return string.Empty;
            }
        }

        public List<int> GetUpdatedAssessorsList(List<int> assessors)
        {
            try
            {

                IChannelProvider<ICommonContractChannel> channelProvider = ChannelProviderFactory<ICommonContractChannel>.Create("CommonClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();
                var list = service.GetFullListOfAssessors().Result;
                service.Close();

                if (list != null && assessors != null)
                    return list.Intersect(assessors).ToList();

                return assessors;
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return new List<int>();
            }
        }

        public static string GetBusinessDay(int numberOfDaysToAdd)
        {
            return GetBusinessDay(numberOfDaysToAdd, string.Empty);
        }

        public static string GetBusinessDay(int numberOfDaysToAdd, string targetDate)
        {
            DateTime businessDay = DateTime.Now.AddDays(numberOfDaysToAdd);

            try
            {
                if (string.IsNullOrEmpty(targetDate))
                    targetDate = DateTime.Now.ToString("dd/MM/yyyy");

                IChannelProvider<IMarketContractChannel> channelProvider = ChannelProviderFactory<IMarketContractChannel>.Create("MarketClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();
                var response = service.GetNextBusinessDay(numberOfDaysToAdd, targetDate).Result;
                service.Close();

                DateTime.TryParse(response, out businessDay);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return string.Empty;
            }
            return businessDay.ToString("dd/MM/yyyy");
        }

        public int GetNumberOfPending(string tableName, int pendingStatusId)
        {
            int pending = 0;

            try
            {
                IChannelProvider<IMarketContractChannel> marketProvider = ChannelProviderFactory<IMarketContractChannel>.Create("MarketClassName");
                var marketService = marketProvider.GetChannel().CreateChannel();

                marketService.Open();
                pending = marketService.GetNumberOfPending(tableName, pendingStatusId, GetAssessorsRangesToFilter(string.Empty)).Result;
                marketService.Close();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }
            return pending;
        }
    }
}