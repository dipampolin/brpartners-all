﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.WebSite.Models;
using QX3.Portal.WebSite.Models.Channel;
using QX3.Portal.WebSite.SubscriptionService;

namespace QX3.Portal.WebSite.Helper
{
    public class SubscriptionHelper
    {
        public static string[] SubscriptionActions = new string[] { "Solicitar", "Editar", "Excluir", "Aprovar", "Reprovar" };
        public static string[] RequestRightsActions = new string[] { "Solicitar", "Efetuar", "Notificar", "Editar", "Excluir" };

        public string SubscriptionListObject(List<Subscription> subscriptionList, ref FormCollection form, int iCount)
        {
            JsonModel dtJson = new JsonModel();

            dtJson.sEcho = Convert.ToInt32(form["sEcho"]);

            if (subscriptionList == null) subscriptionList = new List<Subscription>();

            dtJson.iTotalRecords = iCount;
            dtJson.iTotalDisplayRecords = iCount;

            var tableValues = new List<string[]>();
            List<String> rowData = new List<String>();

            if (subscriptionList != null)
            {
                AccessControlHelper acHelper = new AccessControlHelper();
                foreach (Subscription sub in subscriptionList)
                {

                    string disabled = sub.Status.Equals('P') || sub.Status.Equals('S') ? "" : "disabled=\"disabled\"";
                    rowData.Add(string.Format(String.Format("<input type=\"checkbox\" name=\"chkSubscription-{0}\" id=\"chkSubscription-{0}\" value=\"{1}\" {2} />", sub.RequestId.ToString(), sub.Status.Value.ToString(), disabled)) + "<input type='hidden' class='hidden-req' value='" + sub.RequestId + "' />");
                    rowData.Add(string.Format("<a class=\"lnk-detail\" title=\"detalhes\" href=\"javascript:void(0);\">{0}</a>", sub.Company));
                    rowData.Add(sub.StockCode);
                    rowData.Add(sub.RequestId.ToString());
                    rowData.Add(sub.InitialDate.Value.ToShortDateString());
                    rowData.Add(sub.COMDate.Value.ToShortDateString());
                    rowData.Add((sub.BrokerDate.HasValue ? sub.BrokerDate.Value.ToShortDateString() : "-") + (sub.BrokerHour.HasValue ? " - " + sub.BrokerHour.Value.ToShortTimeString() : string.Empty));
                    rowData.Add(sub.StockExchangeDate.Value.ToShortDateString());
                    rowData.Add(sub.CompanyDate.Value.ToShortDateString());

                    if (sub.Status.Equals('P'))
                        rowData.Add(string.Format("<a href='javascript:void(0);' onclick=\"ChangeStatus(false,this,'{0}','{2}','{3}');return false;\">{1}</a>", sub.RequestId, sub.DescStatus, sub.ISINStock, sub.ISINSubscription));
                    else
                        rowData.Add(sub.DescStatus);

                    // actions
                    string details = "<a class=\"ico-visualizar\" title=\"detalhes\" href=\"javascript:void(0);\">detalhes</a>";
                    string edit = !sub.Status.Equals('F') && acHelper.HasFuncionalityPermission("Subscription.Index", "btnEdit") ? "<a href=\"javascript:void(0);\" title=\"editar\" class=\"ico-editar\">editar</a>" : "<img class=\"espaco\" src=\"../../img/ico/icon-space-table.gif\" />";
                    string delete = !sub.Status.Equals('F') && acHelper.HasFuncionalityPermission("Subscription.Index", "btnDelete") ? "<a class=\"ico-excluir\" title=\"excluir\" href=\"javascript:void(0);\">excluir</a>" : "<img class=\"espaco\" src=\"../../img/ico/icon-space-table.gif\" />";
                    string log = acHelper.HasFuncionalityPermission("Subscription.Index", "btnLog") ? string.Format("<a href=\"javascript:void(0);\" title=\"visualizar\" class=\"ico-hist\" onclick=\"OpenHistoryDetail({0});return false;\">histórico</a>", sub.RequestId.ToString()) : string.Empty;
                    rowData.Add(string.Format("{0}{1}{2}{3}", details, edit, delete, log));

                    tableValues.Add(rowData.ToArray());
                    rowData = new List<string>();
                }
            }

            dtJson.aaData = tableValues;

            return Newtonsoft.Json.JsonConvert.SerializeObject(dtJson);
        }

        public int GetSubscriptionPendents()
        {
            return CommonHelper.GetPendents("Subscription.Index");
        }

        public Subscription CreateSubscriptionFilter(FormCollection form)
        {
            /*
             pCD_NEGOCIO         in  Varchar2,
            pDT_INICIO          in  Varchar2,
            pDT_CORRETORA       in  Varchar2,
            pIN_STATUS          in  char,
            pCURSOR
             */
            var filter = new Subscription()
            {
                ISINStock = form["txtStockCode"],
                InitialDate = Convert.ToDateTime(form["txtInitialDate"]),
                BrokerDate = Convert.ToDateTime(form["txtBrokerDate"]),
                Status = form["ddlStatus"].ToCharArray()[0]
            };

            return filter;
        }

        public string RequestRightsListObject(List<SubscriptionRights> list, ref FormCollection form, int iCount, int? totalSubscripion = null, bool? FlagChangeFilterSubscription = null)
        {
            JsonModelSubscription dtJson = new JsonModelSubscription();

            dtJson.sEcho = Convert.ToInt32(form["sEcho"]);

            if (list == null) list = new List<SubscriptionRights>();

            var columns = new string[] { ""
                , "ClientCode"
                , "AssessorName"
                , "RightsQtty"
                , "RequestedQtty"
                , "AvailableQtty"
                , "RightValue"
                , "TotalValue"
                , "StockExchangeDate"
                , "DescScraps"
                , "DescWarning"
                , "DescStatus"
                , ""
            };
            /*Sort*/
            CommonHelper.GenericSort(ref list, CommonHelper.SortListCommand(form, columns));

            var NRCPartialList = CommonHelper.GenericPagingList(form, list);

            dtJson.iTotalRecords = iCount;
            dtJson.iTotalDisplayRecords = iCount;

            var tableValues = new List<string[]>();
            List<String> rowData = new List<String>();

            if (list != null)
            {
                AccessControlHelper acHelper = new AccessControlHelper();
                foreach (SubscriptionRights item in NRCPartialList)
                {
                    string disabled = (item.Status.HasValue) ? item.Status.Equals('P') || item.Status.Equals('A') ? "" : "disabled=\"disabled\"" : "";
                    rowData.Add(
                        string.Format(String.Format("<input type=\"checkbox\" name=\"chkRequestRights-{0}\" id=\"chkRequestRights-{0}\" value=\"{1}\" {2} />", item.RequestId.ToString(), item.Status.Value.ToString(), disabled))
                        + "<input type='hidden' class='lnk-client' value='" + item.ClientCode.ToString() + "' />"
                        + "<input type='hidden' class='lnk-status' value='" + item.Status.ToString() + "' />"
                        );
                    rowData.Add(string.Format(item.ClientName));

                    rowData.Add(item.ClientCode.ToString());
                    rowData.Add(item.AssessorName);
                    rowData.Add((item.RightsQtty.HasValue ? item.RightsQtty.Value.ToString("N0") : "-"));
					rowData.Add((item.RequestedQtty.HasValue ? item.RequestedQtty.Value.ToString("N0") : "-"));
					rowData.Add((item.AvailableQtty.HasValue ? item.AvailableQtty.Value.ToString("N0") : "-"));
                    rowData.Add((item.RightValue.HasValue ? item.RightValue.Value.ToString("N2") : "-"));
                    rowData.Add((item.TotalValue.HasValue ? item.TotalValue.Value.ToString("N2") : "-"));
                    rowData.Add((item.StockExchangeDate.HasValue ? item.StockExchangeDate.Value.ToShortDateString() : "-"));
                    rowData.Add(item.DescScraps);
                    rowData.Add(string.Format("<a href='javascript:void(0);' onclick=\"ChangeNotification(false,this,'{0}','{1}');return false;\">{2}</a>", item.RequestId, item.ClientCode.ToString(), !string.IsNullOrEmpty(item.DescWarning) ? item.DescWarning : "Pendente" ));
                    if (item.Status.Equals('P'))
                        rowData.Add(string.Format("<a href='javascript:void(0);' onclick=\"ChangeStatus(false,this,'{0}','{1}');return false;\">{2}</a>", item.RequestId, item.ClientCode.ToString(), item.DescStatus));
                    else
                        rowData.Add(item.DescStatus);

                    // actions
                    string edit = !item.Status.Equals('A') && item.AvailableQtty > 0 && acHelper.HasFuncionalityPermission("Subscription.RequestRights", "btnEdit") ? "<a href=\"javascript:void(0);\" title=\"editar\" class=\"ico-editar\">editar</a>" : "<img class=\"espaco\" src=\"../../img/ico/icon-space-table.gif\" />";
                    string delete = item.Status.Equals('P') && acHelper.HasFuncionalityPermission("Subscription.RequestRights", "btnDelete") ? "<a class=\"ico-excluir\" title=\"excluir\" href=\"javascript:void(0);\">excluir</a>" : "<img class=\"espaco\" src=\"../../img/ico/icon-space-table.gif\" />";
                    string log = acHelper.HasFuncionalityPermission("Subscription.RequestRights", "btnLog") ? string.Format("<a href=\"javascript:void(0);\" title=\"visualizar\" class=\"ico-hist\" onclick=\"OpenHistoryDetail({0}, {1});return false;\">histórico</a>", item.RequestId.ToString(), item.ClientCode.ToString()) : string.Empty;
                    rowData.Add(string.Format("{0}{1}{2}", edit, delete, (item.Status != 'A' || (item.Status == 'A' && !string.IsNullOrEmpty(item.DescWarning))) ? log : "<img class=\"espaco\" src=\"../../img/ico/icon-space-table.gif\" />"));

                    tableValues.Add(rowData.ToArray());
                    rowData = new List<string>();
                }
            }

            dtJson.aaData = tableValues;
            dtJson.TotalSubscription = totalSubscripion == null ? 0 : totalSubscripion.Value;
            dtJson.FlagChangeFilterSubscription = FlagChangeFilterSubscription == null ? false : FlagChangeFilterSubscription.Value;

            return Newtonsoft.Json.JsonConvert.SerializeObject(dtJson);
        }
    }
}