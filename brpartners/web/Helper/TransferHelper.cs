﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.WebSite.Models;
using System.Web.Mvc;

namespace QX3.Portal.WebSite.Helper
{
    public class TransferHelper
    {
        public string GetOrdersCorrectionTableBody(List<TransferBovespaItem> transferBovespaList, ref FormCollection form, int iCount)
        {
            JsonModel dtJson = new JsonModel();

            dtJson.sEcho = Convert.ToInt32(form["sEcho"]);

            if (transferBovespaList == null) transferBovespaList = new List<TransferBovespaItem>();

            dtJson.iTotalRecords = iCount;
            dtJson.iTotalDisplayRecords = iCount;

            var tableValues = new List<string[]>();
            List<String> rowData = new List<String>();

            if (transferBovespaList != null)
            {
                AccessControlHelper acHelper = new AccessControlHelper();
                foreach (TransferBovespaItem item in transferBovespaList)
                {
                    rowData.Add(item.Client.Value);
                    rowData.Add(item.Client.Description);
                    rowData.Add(item.CNPJ);
                    tableValues.Add(rowData.ToArray());
                    rowData = new List<string>();
                }
            }

            dtJson.aaData = tableValues;

            return Newtonsoft.Json.JsonConvert.SerializeObject(dtJson);
        }
    }
}