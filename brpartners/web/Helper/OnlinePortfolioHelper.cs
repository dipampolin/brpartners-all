﻿using System;
using System.Collections.Generic;
using QX3.Portal.Contracts.DataContracts;
using System.Text;
using QX3.Portal.WebSite.Models;
using QX3.Portal.WebSite.Models.Channel;
using QX3.Portal.WebSite.OnlinePortfolioService;
using System.Web;
using System.Linq;
using QX3.Portal.WebSite.Helper;

namespace QX3.Portal.WebSite.Helper
{
    public class OnlinePortfolioHelper
    {
        public void GetClientContactsContent(List<PortfolioClientContactsListItem> contacts, ref PortfolioContactsClientContent content)
        {
            StringBuilder htmlItems = new StringBuilder();

            if (contacts != null)
                foreach (PortfolioClientContactsListItem c in contacts)
                {
                    string message = c.Message.Length > 100 ? string.Format("<span id=\"halfComment{1}\" class=\"comentario-tabela\" style=\"display:block; width:686px;\">{0}...</span><span id=\"fullComment{1}\" class=\"comentario-tabela\" style=\"display:none; width:686px;\">{2}</span>", c.Message.Substring(0, 100), c.Index, c.Message) : c.Message;
                    string link = c.Message.Length > 100 ? string.Format("<a id=\"lnkContactDetails{0}\" class=\"ico-detalhes fechado\" onclick=\"OpenFullComment(this,{0});return false;\" href=\"javascript:void(0);\">expandir</a>", c.Index) : string.Empty;
                    string trFormat = string.Format("<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td></tr>", c.Date, c.Operator, message, link);
                    htmlItems.Append(trFormat);
                }

            content.HtmlItems = htmlItems.ToString();
        }

        public static Int32[] GetOperatorClients(PortfolioOperatorFilter filter)
        {
            try
            {
                IChannelProvider<IOnlinePortfolioContractChannel> portfolioProvider = ChannelProviderFactory<IOnlinePortfolioContractChannel>.Create("OnlinePortfolioClassName");
                var portfolioService = portfolioProvider.GetChannel().CreateChannel();

                portfolioService.Open();
                var clients = portfolioService.LoadOperatorClients(Int32.Parse(SessionHelper.CurrentUser.ID.ToString()), (Int32)filter).Result;
                portfolioService.Close();
                return clients;
            }
            catch { }

            return null;
        }

        public static string GetOperatorClientsToFilterList()
        {
            string list = string.Empty;

            try
            {
                var session = HttpContext.Current.Session["OnlinePortfolio.Operator.Clients"];

                if (session == null)
                {
                    IChannelProvider<IOnlinePortfolioContractChannel> portfolioProvider = ChannelProviderFactory<IOnlinePortfolioContractChannel>.Create("OnlinePortfolioClassName");
                    var portfolioService = portfolioProvider.GetChannel().CreateChannel();

                    portfolioService.Open();
                    var clients = portfolioService.LoadOperatorClients(Int32.Parse(SessionHelper.CurrentUser.ID.ToString()), (Int32)PortfolioOperatorFilter.None).Result;
                    portfolioService.Close();
                    HttpContext.Current.Session["OnlinePortfolio.Operator.Clients"] = clients;
                    session = clients;
                }

                if (session != null && ((int[])session).Length > 0)
                    list = String.Concat(String.Join(";", (int[])session), ";");
            }
            catch { }

            return list;
        }

        public PortfolioPositionClientContent GetClientPosition(PortfolioPosition clientPosition, decimal currentDiscount, bool first)
        {
            PortfolioPositionClientContent content = new PortfolioPositionClientContent();
            content.ClientCode = clientPosition.Client.Value;
            content.ClientName = string.Format("{0} {1}", clientPosition.Client.Value, clientPosition.Client.Description);
            content.ClientEmail = string.IsNullOrEmpty(clientPosition.Email) ? "-" : clientPosition.Email;
            content.ClientPhone = clientPosition.Phone == "()" ? "-" : clientPosition.Phone;
            content.ClientStartDate = clientPosition.StartDate.ToShortDateString();
            content.ClientStartValue = clientPosition.StartValue.ToString("N2");
            content.ClientBound = clientPosition.Bound;
            content.Comments = clientPosition.Comments;

            StringBuilder htmlTableItems = new StringBuilder();

            List<PortfolioPositionItem> recalculatedItems = new List<PortfolioPositionItem>();

            if (clientPosition != null)
            {
                decimal finalValue = 0;

                //List<string> markets = (from l in clientPosition.Details select l.MarketGroup).Distinct().OrderBy(m => m).ToList();

                List<string> markets = (from l in clientPosition.Details select l.MarketGroup).Distinct().OrderBy(
                    m => (m == "A VISTA") ? 1 :
                         (m == "OPÇÕES") ? 2 :
                         (m == "TERMO") ? 3 :
                         (m == "L&S") ? 4 :
                         (m == "SHORT") ? 5 :
                         (m == "BTC TOMADOR") ? 6 :
                         (m == "BTC DOADOR") ? 7 :
                         (m == "Financiamento") ? 8 : 9).ThenBy(m=>m).ToList();

                decimal discount = currentDiscount;

                List<PortfolioPositionMarket> marketsList = new List<PortfolioPositionMarket>();

                //Carregando ultimas cotações
                List<string> symbols = new List<string>();

                List<string> listOfSymbols = clientPosition.Details.Count > 0 ? clientPosition.Details.Select(i => i.StockCode.Trim()).Distinct().ToList() : new List<string>();
                List<string> listOfTermSymbols = clientPosition.Details.Count > 0 ? (from d in clientPosition.Details where !string.IsNullOrEmpty(d.TermStockCode) select d.TermStockCode.Trim()).Distinct().ToList() : new List<string>();

                symbols.Add("IBOV");
                if (listOfSymbols.Count > 0)
                    symbols.AddRange(listOfSymbols);
                if (listOfTermSymbols.Count > 0)
                    symbols.AddRange(listOfTermSymbols);

                List<StockPrice> stocks = new List<StockPrice>();
                SolutionTechHelper stHelper = new SolutionTechHelper();
                stocks = stHelper.LoadQuotes(symbols.ToArray());

                content.StockPrices = String.Join(";", stocks.Select(s => s.Price).ToList());
                content.Errors = String.Join(";", stocks.Select(s => s.Symbol).ToList());

                foreach (string market in markets)
                {
                    List<PortfolioPositionItem> marketItems = new List<PortfolioPositionItem>();

                    int columnsSize = (market.ToLower() == "a vista" || market.ToLower() == "short" || market.ToLower() == "l&s") ? 1 : market.ToLower() == "termo" ? 3 : 2;

                    switch (columnsSize)
                    {
                        case 1: //A Vista ou Short
                            string dateText = market.ToLower() == "short" ? "Data Venda" : "Data Compra";
                            htmlTableItems.Append(string.Format("<tr class=\"primario\"><th scope=\"col\" colspan=\"12\" style=\"font-size:16px;\">{0}</th></tr><tr class=\"secundario\"><th scope=\"col\">Papel</th><th scope=\"col\">{1}</th><th scope=\"col\">Quantidade</th><th scope=\"col\">Preço Compra</th><th scope=\"col\">Compra Líquido</th><th scope=\"col\">Preço Venda</th><th scope=\"col\">Venda líquido</th><th scope=\"col\">Lucro/Prejuizo</th><th scope=\"col\" width='64px'>%</th><th scope=\"col\" colspan=\"3\">Setor</th></tr>", market.ToUpper(), dateText));
                            break;
                        case 3:
                            htmlTableItems.Append(string.Format("<tr class=\"primario\"><th scope=\"col\" colspan=\"12\" style=\"font-size:16px;\">{0}</th></tr><tr class=\"secundario\"><th scope=\"col\">Papel</th><th scope=\"col\">Data Compra</th><th scope=\"col\">Quantidade</th><th scope=\"col\">Preço Compra</th><th scope=\"col\">Compra Líquido</th><th scope=\"col\">Preço Venda</th><th scope=\"col\">Venda líquido</th><th scope=\"col\">Lucro/Prejuizo</th><th scope=\"col\" width='64px'>%</th><th scope=\"col\">Vencimento</th><th scope=\"col\">Rolagem</th><th scope=\"col\">Setor</th></tr>", market.ToUpper()));
                            break;
                        default: //Financiamento e BTC
                            htmlTableItems.Append(string.Format("<tr class=\"primario\"><th scope=\"col\" colspan=\"12\" style=\"font-size:16px;\">{0}</th></tr><tr class=\"secundario\"><th scope=\"col\">Papel</th><th scope=\"col\">Data Venda</th><th scope=\"col\">Quantidade</th><th scope=\"col\">Preço Compra</th><th scope=\"col\">Compra Líquido</th><th scope=\"col\">Preço Venda</th><th scope=\"col\">Venda líquido</th><th scope=\"col\">Lucro/Prejuizo</th><th scope=\"col\" width='64px'>%</th><th scope=\"col\">Vencimento</th><th scope=\"col\" colspan=\"2\">Setor</th></tr>", market.ToUpper()));
                            break;
                    }

                    var items = (from m in clientPosition.Details where m.MarketGroup.Equals(market) select m).ToList();

                    foreach (PortfolioPositionItem i in items)
                    {
                        PortfolioPositionItem recalculatedItem = i;

                        string symbol = i.MarketType.ToLower() != "termo" ? i.StockCode.Trim() : i.TermStockCode.Trim();
                        var stock = stocks.Where(s => s.Symbol.Equals(symbol)).FirstOrDefault();
                        double stockPrice = stock == null ? 0 : stock.Price;

                        RecalculatePositionDetail(ref recalculatedItem, discount, stockPrice);

                        string trFormat = string.Empty;

                        switch (columnsSize)
                        {
                            case 1: //A Vista ou Short
                                trFormat = string.Format("<tr><td>{0}</td><td>{2}</td><td class=\"direita {3}\">{4}</td><td class=\"direita {16}\">{15}</td><td class=\"direita {5}\">{6}</td><td class=\"direita {7}\">{8}</td><td class=\"direita {9}\">{10}</td><td class=\"direita {11}\">{12}</td><td class=\"direita {13}\">{14}</td><td colspan=\"3\">{1}</td></tr>",
                                                        recalculatedItem.StockCode,
                                                        recalculatedItem.Sector,
                                                        recalculatedItem.BuyDate,
                                                        CommonHelper.GetValueColorClass(recalculatedItem.Quantity),
                                                        recalculatedItem.Quantity.ToString("N0"),
                                                        CommonHelper.GetValueColorClass(recalculatedItem.BuyNetValue),
                                                        recalculatedItem.BuyNetValue.ToString("N2"),
                                                        CommonHelper.GetValueColorClass(recalculatedItem.SellValue),
                                                        recalculatedItem.SellValue.ToString("N2"),
                                                        CommonHelper.GetValueColorClass(recalculatedItem.SellNetValue),
                                                        recalculatedItem.SellNetValue.ToString("N2"),
                                                        CommonHelper.GetValueColorClass(recalculatedItem.ProfitLoss),
                                                        recalculatedItem.ProfitLoss.ToString("N2"),
                                                        CommonHelper.GetValueColorClass(recalculatedItem.Percentage),
                                                        string.Concat(recalculatedItem.Percentage.ToString("N2"), "%"),
                                                        recalculatedItem.BuyValue.ToString("N2"),
                                                        CommonHelper.GetValueColorClass(recalculatedItem.BuyValue)
                                                        );
                                break;
                            case 3:
                                trFormat = string.Format("<tr><td>{0}</td><td>{2}</td><td class=\"direita {5}\">{6}</td><td class=\"direita {18}\">{17}</td><td class=\"direita {7}\">{8}</td><td class=\"direita {9}\">{10}</td><td class=\"direita {11}\">{12}</td><td class=\"direita {13}\">{14}</td><td class=\"direita {15}\">{16}</td><td>{3}</td><td>{4}</td><td>{1}</td></tr>",
                                                            recalculatedItem.StockCode,
                                                            recalculatedItem.Sector,
                                                            recalculatedItem.BuyDate,
                                                            recalculatedItem.DueDate,
                                                            recalculatedItem.RollOverDate,
                                                            CommonHelper.GetValueColorClass(recalculatedItem.Quantity), recalculatedItem.Quantity.ToString("N0"),
                                                            CommonHelper.GetValueColorClass(recalculatedItem.BuyNetValue),
                                                            recalculatedItem.BuyNetValue.ToString("N2"),
                                                            CommonHelper.GetValueColorClass(recalculatedItem.SellValue), recalculatedItem.SellValue.ToString("N2"),
                                                            CommonHelper.GetValueColorClass(recalculatedItem.SellNetValue), recalculatedItem.SellNetValue.ToString("N2"),
                                                            CommonHelper.GetValueColorClass(recalculatedItem.ProfitLoss), recalculatedItem.ProfitLoss.ToString("N2"),
                                                            CommonHelper.GetValueColorClass(recalculatedItem.Percentage), string.Concat(recalculatedItem.Percentage.ToString("N2"), "%"),
                                                            recalculatedItem.BuyValue.ToString("N2"),
                                                            CommonHelper.GetValueColorClass(recalculatedItem.BuyValue)
                                                            );
                                break;
                            default: //Financiamento e BTC
                                trFormat = string.Format("<tr><td>{0}</td><td>{2}</td><td class=\"direita {4}\">{5}</td><td class=\"direita {17}\">{16}</td><td class=\"direita {6}\">{7}</td><td class=\"direita {8}\">{9}</td><td class=\"direita {10}\">{11}</td><td class=\"direita {12}\">{13}</td><td class=\"direita {14}\">{15}</td><td>{3}</td><td colspan=\"2\">{1}</td></tr>",
                                                            recalculatedItem.StockCode,
                                                            recalculatedItem.Sector,
                                                            recalculatedItem.BuyDate,
                                                            recalculatedItem.DueDate,
                                                            CommonHelper.GetValueColorClass(recalculatedItem.Quantity),
                                                            recalculatedItem.Quantity.ToString("N0"),
                                                            CommonHelper.GetValueColorClass(recalculatedItem.BuyNetValue),
                                                            recalculatedItem.BuyNetValue.ToString("N2"),
                                                            CommonHelper.GetValueColorClass(recalculatedItem.SellValue),
                                                            recalculatedItem.SellValue.ToString("N2"),
                                                            CommonHelper.GetValueColorClass(recalculatedItem.SellNetValue),
                                                            recalculatedItem.SellNetValue.ToString("N2"),
                                                            CommonHelper.GetValueColorClass(recalculatedItem.ProfitLoss),
                                                            recalculatedItem.ProfitLoss.ToString("N2"),
                                                            CommonHelper.GetValueColorClass(recalculatedItem.Percentage),
                                                            string.Concat(recalculatedItem.Percentage.ToString("N2"), "%"),
                                                            recalculatedItem.BuyValue.ToString("N2"),
                                                            CommonHelper.GetValueColorClass(recalculatedItem.BuyValue)
                                                            );
                                break;
                        }

                        htmlTableItems.Append(trFormat);
                        recalculatedItems.Add(recalculatedItem);
                        marketItems.Add(recalculatedItem);
                    }
                    marketsList.Add(new PortfolioPositionMarket { Name = market, Items = marketItems });
                }

                content.Markets = marketsList;
                HttpContext.Current.Session["OnlinePortfolio.Position.ChartItems"] = recalculatedItems;
                HttpContext.Current.Session["OnlinePortfolio.Position.ClientPosition"] = recalculatedItems;

                content.HtmlTableItems = htmlTableItems.ToString();

                //Financial
                PortfolioPositionFinancialContent financialContent = new PortfolioPositionFinancialContent();
                financialContent.Position = finalValue.ToString("N2");
                financialContent.Cash = clientPosition.AvailableValue.ToString("N2");
                financialContent.Total = (finalValue + clientPosition.AvailableValue).ToString("N2");
                content.Financial = financialContent;

                //IBOV
                PortfolioPositionIbovContent ibovContent = new PortfolioPositionIbovContent();
                var ibov = stocks.Where(s => s.Symbol.Equals("IBOV")).FirstOrDefault();
                decimal ibovQuotation = Convert.ToDecimal(ibov != null ? ibov.Price : 0);
                ibovContent.TodayPoints = ibovQuotation.ToString("N0");
                ibovContent.TodayVariation = ibovQuotation == 0 ? "0,00%" : string.Concat((((clientPosition.IbovValueD1 - ibovQuotation) / ibovQuotation) * 100).ToString("N2"), "%");
                ibovContent.YearDate = clientPosition.IbovDate.ToShortDateString();
                ibovContent.YearPoints = clientPosition.IbovValue.ToString("N0");
                ibovContent.YearVariation = ibovQuotation == 0 ? "0,00%" : string.Concat((((clientPosition.IbovValue - ibovQuotation) / ibovQuotation) * 100).ToString("N2"), "%");
                content.Ibov = ibovContent;

                //C/C
                PortfolioPositionDiscountContent discountContent = new PortfolioPositionDiscountContent();
                discountContent.DayTrade = string.Concat(clientPosition.DayTradeDiscount.ToString("N2"), "%");
                discountContent.Available = clientPosition.AvailableValue.ToString("N2");
                discountContent.DailyOperations = clientPosition.TradesValue.ToString("N2");
                discountContent.D1 = clientPosition.ProjectedValueD1.ToString("N2");
                discountContent.D2 = clientPosition.ProjectedValueD2.ToString("N2");
                discountContent.D3 = clientPosition.ProjectedValueD3.ToString("N2");
                content.Discount = discountContent;

                content.CCDiscount = discount.ToString("N2");
            }

            HttpContext.Current.Session["OnlinePortfolio.Position.ClientPosition"] = content;

            return content;
        }

        public void RecalculatePositionDetail(ref PortfolioPositionItem item, decimal discount, double stockPrice)
        {
            decimal finalValue = 0;

            if (string.IsNullOrEmpty(item.Sector))
                item.Sector = "NÃO DEFINIDO";

            decimal grossValue = item.Quotation == 0 ? item.Quotation : ((item.Quantity * Convert.ToDecimal(stockPrice)) / item.Quotation);
            decimal grossBrokerage = (grossValue * item.Rate) - item.AdditionalValue;
            decimal discountValue = grossBrokerage * (discount/100);
            decimal netBrokerage = grossBrokerage - discountValue;
            decimal ibovFee = grossValue * item.IbovFee;
            decimal cblcFee = grossValue * item.CblcFee;

            decimal costs = (netBrokerage + ibovFee + cblcFee);
            decimal currentStockPrice = Convert.ToDecimal(stockPrice);

            if (item.TradeType == 'V')
            {
                finalValue = grossValue + costs;
                item.BuyNetValue = finalValue;
                item.BuyValue = currentStockPrice;
                item.ProfitLoss = item.SellNetValue - item.BuyNetValue;
            }
            else
            {
                finalValue = grossValue - costs;
                item.SellNetValue = finalValue;
                item.SellValue = currentStockPrice;
                item.ProfitLoss = item.SellNetValue - item.BuyNetValue;
            }

            item.Percentage = item.BuyNetValue == 0 ? 0 : (item.ProfitLoss / Math.Abs(item.BuyNetValue)) * 100;

        }

        public string GetClientPositionChartHtml(List<PortfolioPositionItem> chartItems)
        {
            StringBuilder chartHtmlItems = new StringBuilder();

            var items = GetClientPositionChart(chartItems).OrderByDescending(c => c.Percentage);
            foreach (PortfolioPositionChartItem i in items)
            {
                if (i.Percentage < 1)
                    i.Percentage = 1;
                chartHtmlItems.Append(string.Format("<tr><td>{0}</td><td><div class=\"grafico\"><span style=\"width:{1}%;\"></span></div></td><td class=\"direita\">{2}</td><td class=\"direita\">{3} %</td></tr>", i.SectorName, Math.Round(i.Percentage), i.Allocation, i.Variation));
            }

            return chartHtmlItems.ToString();
        }

        protected List<PortfolioPositionChartItem> GetClientPositionChart(List<PortfolioPositionItem> items)
        {
            List<PortfolioPositionChartItem> chartItems = new List<PortfolioPositionChartItem>();

            decimal total = (from c in items where c.MarketType.Equals("Short") select c.BuyNetValue).Sum();
            total += (from c in items where c.MarketType != "Short" select c.SellNetValue).Sum();

            List<string> sectors = (from s in items select s.Sector).Distinct().OrderBy(m => m).ToList();

            foreach (string sector in sectors)
            {
                PortfolioPositionChartItem item = new PortfolioPositionChartItem();
                decimal sum = (from c in items where c.Sector.Equals(sector) && c.MarketType.Equals("Short") select c.BuyNetValue).Sum();
                sum += (from c in items where c.Sector.Equals(sector) && c.MarketType != "Short" select c.SellNetValue).Sum();
                item.Allocation = sum.ToString("N2");
                item.Percentage = Math.Round((sum / total) * 100, 2);
                item.Variation = item.Percentage.ToString("N2");
                if (item.Percentage < 1)
                    item.Percentage = 1;
                item.SectorName = sector.Trim();
                chartItems.Add(item);
            }

            if (chartItems != null && chartItems.Count > 0)
            {
                var currentContent = (PortfolioPositionClientContent)HttpContext.Current.Session["OnlinePortfolio.Position.ClientPosition"];
                currentContent.Chart = chartItems.OrderByDescending(c => c.Percentage).ToList();
                HttpContext.Current.Session["OnlinePortfolio.Position.ClientPosition"] = currentContent;
            }
            return chartItems;
        }

        public PortfolioPositionClientContent GetAdminClientPosition(PortfolioPosition obj)
        {
            PortfolioPositionClientContent content = new PortfolioPositionClientContent();

            if (obj.Client == null)
            {
                content.ClientName = "";
                return content;
            }

            content.ClientName = obj.Client.Description;
            content.ClientEmail = !string.IsNullOrEmpty(obj.Email) ? obj.Email : "-";
            content.ClientPhone = !string.IsNullOrEmpty(obj.Phone) && !obj.Phone.Equals("()") ? obj.Phone : "-";

            AccessControlHelper acHelper = new AccessControlHelper();
            bool canPartition = acHelper.HasFuncionalityPermission("OnlinePortfolio.AdminPosition", "btnPartition");
            bool canEdit = acHelper.HasFuncionalityPermission("OnlinePortfolio.AdminPosition", "btnEdit");
            bool canDelete = acHelper.HasFuncionalityPermission("OnlinePortfolio.AdminPosition", "btnDelete");

            string tr = string.Empty;

            string actions = string.Empty;
            actions += canPartition ? "<a href=\"javascript:;\" title=\"particionar\" class=\"ico-particionar\" onclick=\"OpenPartition({0});return false;\">particionar</a>" : string.Empty;
            actions += canEdit ? "<a href=\"javascript:;\" title=\"editar\" class=\"ico-editar\" onclick=\"OpenRequest({0});return false;\">editar</a>" : string.Empty;
            actions += canDelete ? "<a href=\"javascript:;\" title=\"excluir\" class=\"ico-excluir\" onclick=\"DeleteRequest({0});return false;\">excluir</a>" : string.Empty;

            if (obj.Details != null && obj.Details.Count > 0)
            {
                foreach (PortfolioPositionItem item in obj.Details)
                {
                    tr += string.Format("<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td class=\"direita\">{4}</td><td class=\"direita\">{5}</td><td class=\"direita\">{6}</td><td>{7}</td><td>{8}</td><td>{9}</td><td>{10}</td><td>{11}</td></tr>",
                                       item.StockCode,
                                       item.Operation,
                                       item.MarketType,
                                       item.LongShort ? "L & S" : "-",
                                       item.Quantity.ToString("N0"),
                                       item.Price.ToString("N2"),
                                       item.NetPrice.ToString("N2"),
                                       !item.PortfolioNumber.Equals(0) ? item.PortfolioNumber.ToString() : "-",
                                       item.PositionDate.ToShortDateString(),
                                       !string.IsNullOrEmpty(item.DueDate) ? item.DueDate : "-",
                                       !string.IsNullOrEmpty(item.RollOverDate) ? item.RollOverDate : "-",
                                       string.Format(actions, item.Id.ToString())
                                       );
                }
            }

            content.HtmlTableItems = tr;
            HttpContext.Current.Session["OnlinePortfolio.AdminPosition"] = obj;

            return content;
        }

        public PortfolioHistoryClientContent GetClientHistory(PortfolioPosition clientPosition)
        {
            PortfolioHistoryClientContent content = new PortfolioHistoryClientContent();
            content.ClientCode = clientPosition.Client.Value;
            content.ClientName = string.Format("{0} {1}", clientPosition.Client.Value, clientPosition.Client.Description);
            content.ClientEmail = string.IsNullOrEmpty(clientPosition.Email) ? "-" : clientPosition.Email;
            content.ClientPhone = clientPosition.Phone == "()" ? "-" : clientPosition.Phone;

            StringBuilder htmlTableItems = new StringBuilder();

            List<PortfolioHistoryPeriod> periods = new List<PortfolioHistoryPeriod>();

            if (clientPosition != null && clientPosition.Details != null && clientPosition.Details.Count > 0)
            {
                List<string> months = (from l in clientPosition.Details select l.BuyDate.Remove(0, 3)).Distinct().OrderBy(m => m).ToList();

                htmlTableItems.Append("<table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"classificar historico\"><tbody>");

                foreach (string month in months)
                {
                    string monthName = GetMonthName(month);
                    PortfolioHistoryPeriod period = new PortfolioHistoryPeriod();
                    period.Period = monthName;
                    period.Items = new List<PortfolioPositionItem>();


                    htmlTableItems.Append("<tr class=\"primaria\"><th scope=\"col\">Papel</th><th scope=\"col\">Mercado</th><th scope=\"col\">Setor</th><th scope=\"col\">Data Compra</th><th scope=\"col\">Preço Compra</th><th scope=\"col\">Preço Venda</th><th scope=\"col\">Data Venda</th><th scope=\"col\">Quantidade</th><th scope=\"col\">Financeiro Líquido</th><th scope=\"col\">Venda Líquido</th><th scope=\"col\">Lucro/Prejuizo</th><th scope=\"col\" width='64px'>%</th></tr>");

                    var items = (from m in clientPosition.Details where m.BuyDate.Remove(0, 3).Equals(month) orderby m.BuyDate select m).ToList();
                    bool even = true;

                    foreach (PortfolioPositionItem i in items)
                    {
                        PortfolioPositionItem recalculatedItem = i;
                        //RecalculateHistoryDetail(ref recalculatedItem, 1);
                        period.Items.Add(recalculatedItem);

                        string percentageClass = recalculatedItem.Percentage < 0 ? "negativo" : "positivo";
                        string profitLossClass = recalculatedItem.ProfitLoss < 0 ? "negativo" : "positivo";

                        string trFormat = string.Format("<tr class=\"{14}\"><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td class=\"direita\">{4}</td><td class=\"direita\">{5}</td><td>{6}</td><td>{7}</td><td class=\"direita\">{8}</td><td class=\"direita\">{9}</td><td class=\"direita {10}\">{11}</td><td class=\"direita {12}\">{13}</td></tr>",
                                                        recalculatedItem.StockCode,
                                                        recalculatedItem.MarketGroup,
                                                        recalculatedItem.Sector,
                                                        recalculatedItem.BuyDate,
                                                        recalculatedItem.BuyValue.ToString("N2"),
                                                        recalculatedItem.Price.ToString("N2"),
                                                        recalculatedItem.HistoryDate.ToShortDateString(),
                                                        recalculatedItem.Quantity.ToString("N0"),
                                                        recalculatedItem.BuyNetValue.ToString("N2"),
                                                        recalculatedItem.SellNetValue.ToString("N2"),
                                                        profitLossClass,
                                                        recalculatedItem.ProfitLoss.ToString("N2"),
                                                        percentageClass,
                                                        string.Concat(recalculatedItem.Percentage.ToString("N2"), "%"),
                                                        even ? "even" : "odd"
                                                        );
                        htmlTableItems.Append(trFormat);
                        even = !even;
                    }

                    PortfolioHistoryPeriodBalance balance = new PortfolioHistoryPeriodBalance();
                    balance.ProfitLoss = items.Sum(s => s.ProfitLoss);
                    balance.Percentage = items.Sum(s => s.Percentage);
                    balance.Quantity = items.Sum(s => s.Quantity);
                    balance.BuyNetValue = items.Sum(s => s.BuyNetValue);
                    balance.SellNetValue = items.Sum(s => s.SellNetValue);

                    htmlTableItems.Append(string.Format("<tr class=\"resultado\"><td colspan=\"7\" class=\"zerada\" scope=\"row\">{0}</td><td class=\"direita\">{1}</td><td class=\"direita\">{2}</td><td class=\"direita\">{3}</td><td class=\"direita {6}\">{4}</td><td class=\"direita {7}\">{5}</td></tr>", monthName, balance.Quantity.ToString("N0"), balance.BuyNetValue.ToString("N2"), balance.SellNetValue.ToString("N2"), balance.ProfitLoss.ToString("N2"), string.Concat(balance.Percentage.ToString("N2"), "%"), (balance.ProfitLoss >= 0 ? "positivo" : "negativo"), (balance.Percentage >= 0 ? "positivo" : "negativo")));

                    period.Balance = balance;
                    periods.Add(period);
                }

                htmlTableItems.Append("</tbody></table>");

                content.Items = periods;
                content.HtmlTableItems = htmlTableItems.ToString();

            }
            else
                content.HtmlTableItems = string.Empty;

            HttpContext.Current.Session["OnlinePortfolio.History.ClientHistory"] = content;

            return content;
        }

        protected string GetMonthName(string monthYear)
        {
            string name = monthYear;
            int month = int.Parse(monthYear.Substring(0, 2));
            name = string.Concat(CommonHelper.GetMonthName(month), monthYear.Substring(2, 5));
            return name.First().ToString().ToUpper() + String.Join("", name.Skip(1)); ;
        }
    }
}