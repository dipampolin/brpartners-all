﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using QX3.Portal.Contracts.DataContracts;
using System.Text;
using QX3.Portal.WebSite.Models;
using QX3.Portal.WebSite.Models.Channel;
using QX3.Portal.WebSite.RiskService;
using QX3.Spinnex.Common.Services.Logging;
using System.Web.Mvc;

namespace QX3.Portal.WebSite.Helper
{
    public class FinancialHelper
    {
        public string DailyFinancialReportListObject(List<DailyFinancial> list, ref FormCollection form, int iCount)
        {
            JsonModel dtJson = new JsonModel();

            dtJson.sEcho = Convert.ToInt32(form["sEcho"]);

            if (list == null) list = new List<DailyFinancial>();

            dtJson.iTotalRecords = iCount;
            dtJson.iTotalDisplayRecords = iCount;

            var tableValues = new List<string[]>();
            List<String> rowData = new List<String>();

            if (list != null)
            {
                AccessControlHelper acHelper = new AccessControlHelper();
                foreach (DailyFinancial daily in list)
                {
                    /*
                        <th>Cod</th>
                        <th>Cliente</th>
                        <th>Assessor</th>
                        <th>Saldo Anterior</th>
                        <th>Disponível C/C</th>
                        <th>Crédito</th>
                        <th>Débito</th>
                        <th>Liquidado</th>
                        <th>Saldo Atual</th>
                        <th>Projetado</th>
                        <th class="ultimo">Saldo Final</th>
                     */

                    rowData.Add(daily.ClientID.ToString() + "-" + daily.DV.ToString());
                    rowData.Add(daily.ClientName);
                    rowData.Add(daily.AssessorName);
                    rowData.Add(daily.AnteriorBalance.ToString("n")); 
                    rowData.Add(daily.AccountBalance.ToString("n"));
                    rowData.Add(daily.Credit.ToString("n"));
                    rowData.Add(daily.Debt.ToString("n"));
                    rowData.Add(daily.Finished.ToString("n"));
                    rowData.Add(daily.LiquidResult.ToString("n"));
                    rowData.Add(daily.ProjectedBalance.ToString("n"));
                    rowData.Add(daily.FinalBalance.ToString("n"));
                    rowData.Add((daily.CustodyTax.HasValue) ? "Sim" : "Não");               

                    tableValues.Add(rowData.ToArray());
                    rowData = new List<string>();

                }
            }

            dtJson.aaData = tableValues;

            return Newtonsoft.Json.JsonConvert.SerializeObject(dtJson);
        }
    }
}