﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using QX3.Portal.Contracts.DataContracts;
using System.Text;
using QX3.Portal.WebSite.TermService;
using QX3.Portal.WebSite.Models.Channel;
using QX3.Spinnex.Common.Services.Logging;
using System.Configuration;
using QX3.Portal.WebSite.QuotesService;

namespace QX3.Portal.WebSite.Helper
{

    public class SolutionTechHelper
    {
        public static string SolutionTechToken = ConfigurationManager.AppSettings["SolutionTech.Token"] ?? "";

        public List<StockPrice> LoadQuotes(string[] stocks)
        {
            List<StockPrice> prices = new List<StockPrice>();

            try
            {
                IChannelProvider<IQuotesContractChannel> channelProvider = ChannelProviderFactory<IQuotesContractChannel>.Create("QuotesClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();
                var result = service.LoadQuotes(stocks).Result;
                if (result != null)
                    prices = result.ToList();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }

            return prices;
        }
    }
}