﻿using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.Services.SagazWebService;
using QX3.Portal.WebSite.Helper;
using QX3.Portal.WebSite.Models.Channel;
using QX3.Portal.WebSite.OnlinePortfolioService;
using QX3.Portal.WebSite.Properties;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;

namespace QX3.Portal.WebSite.Models
{
    public class ExcelHelper
    {
        #region NonResidentTraders

        static IFormatProvider cultureInfo = new System.Globalization.CultureInfo("en-US", true);

        public MemoryStream CreateConfirmationToMemoryStream(Confirmation report)
        {
            MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(CreateConfirmationXml(report)));
            msExcel.Position = 0;
            msExcel.Flush();

            return msExcel;
        }

        public MemoryStream CreateTradeBlotterToMemoryStream(TradeBlotter report)
        {
            MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(CreateTradeBlotterXml(report)));
            msExcel.Position = 0;
            msExcel.Flush();

            return msExcel;
        }

        public MemoryStream CreateCustomerStatementToMemoryStream(CustomerStatement report)
        {
            MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(CreateCustomerStatementXml(report)));
            msExcel.Position = 0;
            msExcel.Flush();

            return msExcel;
        }

        public MemoryStream CreateCustomerLedgerToMemoryStream(CustomerLedger report)
        {
            MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(CreateCustomerLedgerXml(report)));
            msExcel.Position = 0;
            msExcel.Flush();

            return msExcel;
        }

        public MemoryStream CreateStockRecordToMemoryStream(StockRecord report)
        {
            MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(CreateStockRecordXml(report)));
            msExcel.Position = 0;
            msExcel.Flush();

            return msExcel;
        }

        public MemoryStream CreateFailLedgerToMemoryStream(FailLedger report)
        {
            MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(CreateFailLedgerXml(report)));
            msExcel.Position = 0;
            msExcel.Flush();

            return msExcel;
        }

        public MemoryStream CreateBrokerageToMemoryStream(List<BrokerageTransfer> report, bool pending, string startDate, string endDate)
        {
            MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(CreateBrokerageXml(report, pending, startDate, endDate)));
            msExcel.Position = 0;
            msExcel.Flush();

            return msExcel;
        }

        private string CreateConfirmationXml(Confirmation report)
        {
            string headerXml = string.Empty;
            string itemsXml = string.Empty;
            string footerXml = string.Empty;

            if (report.Items.Count > 0)
            {
                headerXml = ReportsResources.ConfirmationHeader;

                headerXml = headerXml.Replace("#Author#", "VotoWeb");
                headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", cultureInfo));

                headerXml = headerXml.Replace("#ClientCode#", report.ClientCode.ToString());
                headerXml = headerXml.Replace("#ClientName#", HttpUtility.HtmlEncode(report.ClientName));
                headerXml = headerXml.Replace("#ClientAddress#", HttpUtility.HtmlEncode(report.Address));
                headerXml = headerXml.Replace("#ClientEmail#", report.Email);
                headerXml = headerXml.Replace("#ClientPhone#", report.PhoneNumber);
                headerXml = headerXml.Replace("#ClientFax#", report.FaxNumber);

                bool even = true;
                itemsXml = string.Empty;

                var list = report.Items;

                StringBuilder sbItems = new StringBuilder();

                foreach (ConfirmationItem i in list)
                {
                    string itemXml = even ? ReportsResources.ConfirmationItemEven : ReportsResources.ConfirmationItemOdd;
                    itemXml = string.Format(itemXml, i.OperationType == "B" ? "Bougth" : "Sold",
                    i.Symbol,
                    HttpUtility.HtmlEncode(i.CompanyName),
                    i.ISINCode,
                    i.Quantity.ToString("N0", cultureInfo),
                    i.CurrencyName,
                    i.TradeValue.ToString("N2", cultureInfo),
                    i.TradeDate.ToString("MM/dd/yyyy", cultureInfo),
                    i.SettlementDate.ToString("MM/dd/yyyy", cultureInfo),
                    i.PrincipalValue.ToString("N2", cultureInfo),
                    i.CommissionValue.ToString("N2", cultureInfo),
                    i.TransactionFeeValue.ToString("N2", cultureInfo),
                    i.NetAmmountValue.ToString("N2", cultureInfo),
                    i.CustodyCode.ToString("N0", cultureInfo));
                    sbItems.Append(itemXml);
                    even = !even;
                }
                itemsXml = sbItems.ToString();

                footerXml = ReportsResources.ConfirmationFooter;
            }

            string xml = string.Concat(headerXml, itemsXml, footerXml);
            XmlDocument xmlReport = new XmlDocument();
            xmlReport.LoadXml(xml);

            return xml;
        }

        private string CreateTradeBlotterXml(TradeBlotter report)
        {
            string headerXml = string.Empty;
            string itemsXml = string.Empty;
            string footerXml = string.Empty;

            if (report.Items.Count > 0)
            {
                headerXml = ReportsResources.TradeBlotterHeader;

                headerXml = headerXml.Replace("#Author#", "VotoWeb");
                headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", cultureInfo));

                DateTime date = DateTime.Now;
                if (report.Items != null && report.Items.Count > 0)
                    date = report.Items[0].TradeDate;

                headerXml = headerXml.Replace("#StartDate#", date.ToString("MM/dd/yyyy", cultureInfo));

                bool even = true;
                itemsXml = string.Empty;

                var list = report.Items;

                StringBuilder sbItems = new StringBuilder();

                foreach (TradeBlotterItem i in list)
                {
                    string itemXml = even ? ReportsResources.TradeBlotterItemEven : ReportsResources.TradeBlotterItemOdd;
                    itemXml = string.Format(itemXml, i.TradeId,
                    i.TradeDate.ToString("MM/dd/yyyy", cultureInfo),
                    i.SettlementDate.ToString("MM/dd/yyyy", cultureInfo),
                    i.ISINCode,
                    HttpUtility.HtmlEncode(i.CompanyName),
                    i.NumberOfShares.ToString("N0", cultureInfo),
                    i.SharePrice.ToString("N2", cultureInfo),
                    i.CustomerBuyAmount.ToString("N2", cultureInfo),
                    i.NetBuyAmount.ToString("N2", cultureInfo),
                    i.CustomerSellAmount.ToString("N2", cultureInfo),
                    i.NetSellAmount.ToString("N2", cultureInfo),
                    i.CurrencyName,
                    HttpUtility.HtmlEncode(i.ClientName));
                    sbItems.Append(itemXml);
                    even = !even;
                }
                itemsXml = sbItems.ToString();

                footerXml = ReportsResources.TradeBlotterFooter;
            }

            string xml = string.Concat(headerXml, itemsXml, footerXml);
            XmlDocument xmlReport = new XmlDocument();
            xmlReport.LoadXml(xml);

            return xml;
        }

        private string CreateCustomerStatementXml(CustomerStatement report)
        {
            string headerXml = string.Empty;
            string itemsXml = string.Empty;
            string footerXml = string.Empty;

            if (report.Items.Count > 0)
            {
                headerXml = ReportsResources.CustomerStatementHeader;

                headerXml = headerXml.Replace("#Author#", "VotoWeb");
                headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"));

                headerXml = headerXml.Replace("#StartDate#", report.BeginDate.ToString("MM/dd/yyyy"));
                headerXml = headerXml.Replace("#EndDate#", report.EndDate.ToString("MM/dd/yyyy"));

                headerXml = headerXml.Replace("#ClientCode#", report.ClientCode.ToString());
                headerXml = headerXml.Replace("#ClientName#", HttpUtility.HtmlEncode(report.ClientName));
                headerXml = headerXml.Replace("#ClientAddress#", HttpUtility.HtmlEncode(report.Address));
                headerXml = headerXml.Replace("#ClientEmail#", report.Email);
                headerXml = headerXml.Replace("#ClientPhone#", report.PhoneNumber);
                headerXml = headerXml.Replace("#ClientFax#", report.FaxNumber);

                bool even = true;
                itemsXml = string.Empty;

                var list = report.Items;

                StringBuilder sbItems = new StringBuilder();

                foreach (CustomerStatementItem item in list)
                {
                    string itemXml = even ? ReportsResources.CustomerStatementItemEven : ReportsResources.CustomerStatementItemOdd;
                    itemXml = string.Format(itemXml,
                                            HttpUtility.HtmlEncode(item.CompanyName),
                                            item.TradeDate.ToString("MM/dd/yyyy", cultureInfo),
                                            item.SettlementDate.ToString("MM/dd/yyyy", cultureInfo),
                                            item.OperationCode,
                                            item.SharePrice.ToString("N2", cultureInfo),
                                            item.CurrencyName,
                                            item.Shares.ToString("N0", cultureInfo),
                                            item.NetValue.ToString("N2", cultureInfo),
                                            item.SettlementDate.ToString("MM/dd/yyyy", cultureInfo),
                                            item.SettleValue.ToString("N2", cultureInfo),
                                            item.CurrencyName,
                                            item.OutstandingShares.ToString("N2", cultureInfo),
                                            item.OutstandingValues.ToString("N2", cultureInfo),
                                            item.CurrencyName);
                    sbItems.Append(itemXml);
                    even = !even;
                }
                itemsXml = sbItems.ToString();

                footerXml = ReportsResources.CustomerStatementFooter;
                footerXml = string.Format(footerXml,
                                            report.Items.Count,
                                            list.Sum(i => i.NetValue).ToString("N2", cultureInfo),
                                            list.Sum(i => i.SettleValue).ToString("N2", cultureInfo),
                                            report.Items[0].CurrencyName,
                                            report.Items[0].CurrencyName);
            }

            string xml = string.Concat(headerXml, itemsXml, footerXml);
            XmlDocument xmlReport = new XmlDocument();
            xmlReport.LoadXml(xml);

            return xml;
        }

        private string CreateCustomerLedgerXml(CustomerLedger report)
        {
            string headerXml = string.Empty;
            string itemsXml = string.Empty;
            string footerXml = string.Empty;

            if (report.Items.Count > 0)
            {
                headerXml = ReportsResources.CustomerLedgerHeader;

                headerXml = headerXml.Replace("#Author#", "VotoWeb");
                headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", cultureInfo));

                headerXml = headerXml.Replace("#StartDate#", report.StartDate.ToString("MM/dd/yyyy", cultureInfo));
                headerXml = headerXml.Replace("#EndDate#", report.EndDate.ToString("MM/dd/yyyy", cultureInfo));

                headerXml = headerXml.Replace("#ClientCode#", report.ClientCode.ToString());
                headerXml = headerXml.Replace("#ClientName#", HttpUtility.HtmlEncode(report.ClientName));

                bool even = true;
                itemsXml = string.Empty;

                var list = report.Items;

                StringBuilder sbItems = new StringBuilder();

                foreach (CustomerLedgerItem item in list)
                {
                    string itemXml = even ? ReportsResources.CustomerLedgerItemEven : ReportsResources.CustomerLedgerItemOdd;
                    itemXml = string.Format(itemXml,
                        item.ConfirmID,
                        item.Security,
                        item.TradeDate.ToString("MM/dd/yyyy", cultureInfo),
                        item.SettleDate.ToString("MM/dd/yyyy", cultureInfo),
                        item.OperationCode,
                        item.SharePrice.ToString("N2", cultureInfo),
                        item.Shares.ToString("N0", cultureInfo),
                        item.NetValue.ToString("N2", cultureInfo),
                        item.CurrencyName,
                        item.SettledShares.ToString("N0", cultureInfo),
                        item.SettledValue.ToString("N2", cultureInfo),
                        item.OutstandingShares.ToString("N2", cultureInfo),
                        item.OutstandingValues.ToString("N2", cultureInfo));
                    sbItems.Append(itemXml);
                    even = !even;
                }
                itemsXml = sbItems.ToString();

                footerXml = ReportsResources.CustomerLedgerFooter;
            }

            string xml = string.Concat(headerXml, itemsXml, footerXml);
            XmlDocument xmlReport = new XmlDocument();
            xmlReport.LoadXml(xml);

            return xml;
        }

        private string CreateStockRecordXml(StockRecord report)
        {
            string headerXml = string.Empty;
            string itemsXml = string.Empty;
            string footerXml = string.Empty;

            if (report.Items.Count > 0)
            {
                headerXml = ReportsResources.StockRecordHeader;

                headerXml = headerXml.Replace("#Author#", "VotoWeb");
                headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", cultureInfo));

                headerXml = headerXml.Replace("#StartDate#", report.StartDate.ToString("MM/dd/yyyy", cultureInfo));
                headerXml = headerXml.Replace("#EndDate#", report.EndDate.ToString("MM/dd/yyyy", cultureInfo));

                bool even = true;
                itemsXml = string.Empty;

                var list = report.Items;

                StringBuilder sbItems = new StringBuilder();

                foreach (StockRecordItem item in list)
                {
                    sbItems.Append(ReportsResources.StockRecordItem.Replace("#CompanyName#", item.CompanyName));

                    foreach (StockRecordSubItem sItem in item.SubItems)
                    {
                        string sItemXml = even ? ReportsResources.StockRecordSubItemEven : ReportsResources.StockRecordSubItemOdd;
                        sItemXml = string.Format(sItemXml, sItem.ConfirmID, sItem.ClientBroker, sItem.DateSettled.ToString("MM/dd/yyyy", cultureInfo), sItem.Long.ToString("N2", cultureInfo), sItem.Short.ToString("N2", cultureInfo));
                        sbItems.Append(sItemXml);
                        even = !even;
                    }
                    sbItems.AppendFormat(ReportsResources.StockRecordSubItemSum, item.SubItems.Select(s => s.Long).Sum().ToString("N2", cultureInfo), item.SubItems.Select(s => s.Short).Sum().ToString("N2", cultureInfo));
                }
                itemsXml = sbItems.ToString();
                footerXml = ReportsResources.StockRecordFooter;
            }

            string xml = string.Concat(headerXml, itemsXml, footerXml);
            XmlDocument xmlReport = new XmlDocument();
            xmlReport.LoadXml(xml);

            return xml;
        }

        private string CreateFailLedgerXml(FailLedger report)
        {
            string headerXml = string.Empty;
            string itemsXml = string.Empty;
            string footerXml = string.Empty;

            if (report.Items.Count > 0)
            {
                headerXml = ReportsResources.FailLedgerHeader;

                headerXml = headerXml.Replace("#Author#", "VotoWeb");
                headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", cultureInfo));

                string reportType = string.Empty;
                switch (report.Type)
                {
                    case FailLedgerType.FailToDeliver:
                        reportType = "Fail to Deliver";
                        break;
                    case FailLedgerType.FailToReceive:
                        reportType = "Fail to Receive";
                        break;
                    case FailLedgerType.APCustomer:
                        reportType = "A/P Customer";
                        break;
                    case FailLedgerType.ARCustomer:
                        reportType = "AR Customer";
                        break;
                }
                headerXml = headerXml.Replace("#ReportName#", reportType);

                bool even = true;
                itemsXml = string.Empty;

                var list = report.Items;

                bool first = true;

                StringBuilder sbItems = new StringBuilder();

                if (list.Count > 0)
                {
                    if (first)
                        headerXml = headerXml.Replace("#TradeDate#", list[0].TradeDate.ToString("MM/dd/yyyy", cultureInfo));

                    var users = (from l in list group l by new { l.ClientCode, l.ClientName } into g select new Customer { Code = g.Key.ClientCode, Name = g.Key.ClientName }).ToList();

                    foreach (Customer c in users)
                    {
                        var userItems = (from ci in list where ci.ClientCode.Equals(c.Code) select ci).ToList();
                        sbItems.Append(ReportsResources.FailLedgerUserItem.Replace("#ClientID#", c.Code.ToString()).Replace("#ClientName#", HttpUtility.HtmlEncode(c.Name)));

                        foreach (FailLedgerItem item in userItems)
                        {
                            string itemXml = even ? ReportsResources.FailLedgerItemEven : ReportsResources.FailLedgerItemOdd;
                            itemXml = string.Format(itemXml,
                                item.ConfirmID,
                                item.OperationCode,
                                item.Security,
                                item.TradeDate.ToString("MM/dd/yyyy", cultureInfo),
                                item.SettleDate.ToString("MM/dd/yyyy", cultureInfo),
                                item.Shares.ToString("N0", cultureInfo),
                                item.NetAmount.ToString("N2", cultureInfo),
                                item.MarketValuePerShare.ToString("N2", cultureInfo),
                                item.MarketValueTotal.ToString("N2", cultureInfo));
                            sbItems.Append(itemXml);
                            even = !even;
                        }
                        sbItems.Append(ReportsResources.FailLedgerItemCount.Replace("#RecordCount#", userItems.Count.ToString("N0", cultureInfo)));
                    }
                    first = !first;
                }
                itemsXml = sbItems.ToString();
                footerXml = ReportsResources.FailLedgerFooter;
            }

            string xml = string.Concat(headerXml, itemsXml, footerXml);
            XmlDocument xmlReport = new XmlDocument();
            xmlReport.LoadXml(xml);

            return xml;
        }

        private string CreateBrokerageXml(List<BrokerageTransfer> report, bool pending, string startDate, string endDate)
        {
            string headerXml = string.Empty;
            string itemsXml = string.Empty;
            string footerXml = string.Empty;

            if (report.Count > 0)
            {
                headerXml = pending ? ReportsResources.BrokeragePendingHeader : ReportsResources.BrokerageSettledHeader;

                headerXml = headerXml.Replace("#Author#", "VotoWeb");
                headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", cultureInfo));

                bool even = true;
                itemsXml = string.Empty;


                headerXml = headerXml.Replace("#StartDate#", startDate);
                if (!pending)
                    headerXml = headerXml.Replace("#EndDate#", endDate);

                StringBuilder sbItems = new StringBuilder();

                foreach (BrokerageTransfer item in report)
                {
                    string itemXml = even ? (pending ? ReportsResources.BrokeragePendingItemEven : ReportsResources.BrokerageSettledItemEven) : (pending ? ReportsResources.BrokeragePendingItemOdd : ReportsResources.BrokerageSettledItemOdd);
                    if (pending)
                        itemXml = string.Format(itemXml,
                                                item.ClientBrokerCode,
                                                item.ClientCode,
                                                HttpUtility.HtmlEncode(item.ClientName),
                                                item.BrokerageDate.ToString("dd/MM/yyyy"),
                                                item.BrokerageValue.ToString("N2", cultureInfo),
                                                item.TransferPercentage.ToString("N3", cultureInfo),
                                                item.TransferValue.ToString("N2", cultureInfo),
                                                item.TransferValueToDolar.ToString("N2", cultureInfo));
                    else
                        itemXml = string.Format(itemXml,
                                                item.ClientBrokerCode,
                                                item.ClientCode,
                                                HttpUtility.HtmlEncode(item.ClientName),
                                                item.BrokerageDate.ToString("dd/MM/yyyy"),
                                                item.BrokerageValue.ToString("N2", cultureInfo),
                                                item.TransferPercentage.ToString("N3", cultureInfo),
                                                item.TransferValue.ToString("N2", cultureInfo),
                                                item.TransferValueToDolar.ToString("N2", cultureInfo),
                                                item.SettlementDate.Value.ToString("dd/MM/yyyy"),
                                                item.UserName);
                    sbItems.Append(itemXml);
                    even = !even;
                }
                itemsXml = sbItems.ToString();
                footerXml = string.Format(pending ? ReportsResources.BrokeragePendingFooter : ReportsResources.BrokerageSettledFooter,
                                            report.Select(t => t.BrokerageValue).Sum().ToString("N2", cultureInfo),
                                            report.Select(t => t.TransferPercentage).Sum().ToString("N3", cultureInfo),
                                            report.Select(t => t.TransferValue).Sum().ToString("N2", cultureInfo),
                                            report.Select(t => t.TransferValueToDolar).Sum().ToString("N2", cultureInfo));
            }

            string xml = string.Concat(headerXml, itemsXml, footerXml);
            XmlDocument xmlReport = new XmlDocument();
            xmlReport.LoadXml(xml);

            return xml;
        }

        #endregion

        #region AccessControl

        public MemoryStream CreateGroupsOfAssessorsToMemoryStream(List<GroupOfAssessorsListItem> groups)
        {
            string headerXml = string.Empty;
            string itemsXml = string.Empty;
            string footerXml = string.Empty;

            if (groups.Count > 0)
            {
                headerXml = AccessControlResources.ExcelGroupsOfAssessorsHeader;
                headerXml = headerXml.Replace("#Author#", "VotoWeb");
                headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", cultureInfo));
                headerXml = headerXml.Replace("#ReportName#", "Grupos de Assessores");

                bool even = true;
                itemsXml = string.Empty;

                StringBuilder sbItems = new StringBuilder();

                foreach (GroupOfAssessorsListItem i in groups)
                {
                    string itemXml = even ? AccessControlResources.ExcelGroupsOfAssessorsItemEven : AccessControlResources.ExcelGroupsOfAssessorsItemOdd;
                    itemXml = string.Format(itemXml,
                        HttpUtility.HtmlEncode(i.Name),
                        i.Assessors,
                        i.UsersCount.ToString("N0"));
                    sbItems.Append(itemXml);
                    even = !even;
                }
                itemsXml = sbItems.ToString();
                footerXml = AccessControlResources.ExcelGroupsOfAssessorsFooter;
            }

            string xml = string.Concat(headerXml, itemsXml, footerXml);
            XmlDocument xmlReport = new XmlDocument();
            xmlReport.LoadXml(xml);

            MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(xml));
            msExcel.Position = 0;
            msExcel.Flush();

            return msExcel;
        }

        public MemoryStream CreateHistoryToMemoryStream(List<HistoryData> data, string targetTitle, string targetColumnTitle, bool hasAction, string extraTargetColumnTitle = null)
        {
            string headerXml = string.Empty;
            string itemsXml = string.Empty;
            string footerXml = string.Empty;

            if (data.Count > 0)
            {
                if (extraTargetColumnTitle != null)
                {
                    headerXml = TermHistoryResources.WorksheetHeader;
                    headerXml = headerXml.Replace("#ExtraTargetColumnTitle#", extraTargetColumnTitle);
                }
                else if (hasAction && targetColumnTitle != null && extraTargetColumnTitle == null)
                {
                    headerXml = AccessControlResources.ExcelHistoryHeader;
                }
                else if (hasAction && targetColumnTitle == null)
                {
                    headerXml = AccessControlResources.ExcelHistoryHeaderNoTargetColumn;
                }
                else
                {
                    headerXml = AccessControlResources.ExcelHistoryHeaderLite;
                }

                headerXml = headerXml.Replace("#Author#", "VotoWeb");
                headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", cultureInfo));
                headerXml = headerXml.Replace("#ReportName#", targetTitle);
                headerXml = headerXml.Replace("#TargetColumnTitle#", targetColumnTitle);

                bool even = true;
                itemsXml = string.Empty;

                StringBuilder sbItems = new StringBuilder();

                foreach (HistoryData hd in data)
                {
                    string itemXml;

                    if (extraTargetColumnTitle != null)
                    {
                        itemXml = even ? TermHistoryResources.WorksheetItemEven : TermHistoryResources.WorksheetItemOdd;
                        itemXml = string.Format(itemXml, hd.Date.ToString("dd/MM/yyyy - HH:mm"), HttpUtility.HtmlEncode(hd.Responsible), HttpUtility.HtmlEncode(hd.Action), HttpUtility.HtmlEncode(hd.Target), HttpUtility.HtmlEncode(hd.ExtraTarget));
                    }
                    else if (hasAction && targetColumnTitle != null && extraTargetColumnTitle == null)
                    {
                        itemXml = even ? AccessControlResources.ExcelHistoryItemEven : AccessControlResources.ExcelHistoryItemOdd;
                        itemXml = string.Format(itemXml, hd.Date.ToString("dd/MM/yyyy - HH:mm"), HttpUtility.HtmlEncode(hd.Responsible), HttpUtility.HtmlEncode(hd.Action), HttpUtility.HtmlEncode(hd.Target));
                    }
                    else if (hasAction && targetColumnTitle == null)
                    {
                        itemXml = even ? AccessControlResources.ExcelHistoryItemEvenLite : AccessControlResources.ExcelHistoryItemOddLite;
                        itemXml = string.Format(itemXml, hd.Date.ToString("dd/MM/yyyy - HH:mm"), HttpUtility.HtmlEncode(hd.Responsible), HttpUtility.HtmlEncode(hd.Action), HttpUtility.HtmlEncode(hd.Target));
                    }
                    else
                    {
                        itemXml = even ? AccessControlResources.ExcelHistoryItemEvenLite : AccessControlResources.ExcelHistoryItemOddLite;
                        itemXml = string.Format(itemXml, hd.Date.ToString("dd/MM/yyyy - HH:mm"), HttpUtility.HtmlEncode(hd.Responsible), HttpUtility.HtmlEncode(hd.Action));
                    }

                    sbItems.Append(itemXml);
                    even = !even;
                }
                itemsXml = sbItems.ToString();

                if (extraTargetColumnTitle != null)
                {
                    footerXml = TermHistoryResources.WorksheetFooter;
                }
                else if (hasAction && targetColumnTitle != null && extraTargetColumnTitle == null)
                {
                    footerXml = AccessControlResources.ExcelHistoryFooter;
                }
                else
                {
                    footerXml = AccessControlResources.ExcelHistoryFooterLite;
                }
            }

            string xml = string.Concat(headerXml, itemsXml, footerXml);
            XmlDocument xmlReport = new XmlDocument();
            xmlReport.LoadXml(xml);

            MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(xml));
            msExcel.Position = 0;
            msExcel.Flush();

            return msExcel;
        }

        //public MemoryStream CreateFundHistoryToMemoryStream(List<HistoryData> data
        //    , string targetTitle
        //    , string targetColumnTitle
        //    , bool hasAction
        //    , string extraTargetColumnTitle)
        //{
        //    string headerXml = string.Empty;
        //    string itemsXml = string.Empty;
        //    string footerXml = string.Empty;

        //    if (data.Count > 0)
        //    {
        //        List<QX3.Portal.Services.SagazWebService.Fund> funds = new List<Services.SagazWebService.Fund>();
        //        using (var service = QX3.Portal.WebSite.Models.Channel.ChannelProviderFactory<QX3.Portal.WebSite.FundService.IFundContractChannel>.Create("FundClassName").GetChannel().CreateChannel())
        //        {
        //            funds = service.FN_LoadFunds().Result;
        //        }

        //        headerXml = FundsResources.FundHistoryHeader;

        //        headerXml = headerXml.Replace("#Author#", "VotoWeb");
        //        headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", cultureInfo));
        //        headerXml = headerXml.Replace("#ReportName#", targetTitle);
        //        headerXml = headerXml.Replace("#TargetColumnTitle#", targetColumnTitle);
        //        headerXml = headerXml.Replace("#ExtraTargetColumnTitle#", extraTargetColumnTitle);

        //        bool even = true;
        //        itemsXml = string.Empty;

        //        StringBuilder sbItems = new StringBuilder();

        //        foreach (HistoryData hd in data)
        //        {
        //            string itemXml = String.Empty;

        //            //if (extraTargetColumnTitle != null)
        //            {
        //                itemXml = even ? FundsResources.FundItemEven : FundsResources.FundItemOdd;
        //                itemXml = string.Format(itemXml, hd.Date.ToString("dd/MM/yyyy - HH:mm")
        //                    , HttpUtility.HtmlEncode(hd.Responsible)
        //                    , HttpUtility.HtmlEncode(hd.Action)
        //                    , HttpUtility.HtmlEncode(CommonHelper.GetClientName(hd.ID))
        //                    , HttpUtility.HtmlEncode(funds.Where(a => a.Code == Convert.ToInt32(hd.Target)).FirstOrDefault() == null
        //                            ? "-"
        //                            : funds.Where(a => a.Code == Convert.ToInt32(hd.Target)).FirstOrDefault().Name)
        //                    , HttpUtility.HtmlEncode(hd.ExtraTarget));
        //            }

        //            sbItems.Append(itemXml);
        //            even = !even;
        //        }
        //        itemsXml = sbItems.ToString();


        //        footerXml = FundsResources.FundHistoryFooter;

        //    }

        //    string xml = string.Concat(headerXml, itemsXml, footerXml);
        //    XmlDocument xmlReport = new XmlDocument();
        //    xmlReport.LoadXml(xml);

        //    MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(xml));
        //    msExcel.Position = 0;
        //    msExcel.Flush();

        //    return msExcel;
        //}

        public MemoryStream CreateUsersToMemoryStream(List<UsersListItem> users)
        {
            string headerXml = string.Empty;
            string itemsXml = string.Empty;
            string footerXml = string.Empty;

            if (users.Count > 0)
            {
                headerXml = AccessControlResources.ExcelUsersHeader;
                headerXml = headerXml.Replace("#Author#", "VotoWeb");
                headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", cultureInfo));
                headerXml = headerXml.Replace("#ReportName#", "Usu&#225;rios");

                bool even = true;
                itemsXml = string.Empty;

                StringBuilder sbItems = new StringBuilder();

                foreach (UsersListItem i in users)
                {
                    string itemXml = even ? AccessControlResources.ExcelUsersItemEven : AccessControlResources.ExcelUsersItemOdd;
                    itemXml = string.Format(itemXml,
                        HttpUtility.HtmlEncode(i.Name),
                        HttpUtility.HtmlEncode(i.Login),
                        HttpUtility.HtmlEncode(i.Email),
                        i.AssessorID,
                        i.Type,
                        HttpUtility.HtmlEncode(i.ProfileName),
                        HttpUtility.HtmlEncode(i.AssessorGroup),
                        i.AssessorInterval);
                    sbItems.Append(itemXml);
                    even = !even;
                }
                itemsXml = sbItems.ToString();
                footerXml = AccessControlResources.ExcelUsersFooter;
            }

            string xml = string.Concat(headerXml, itemsXml, footerXml);
            XmlDocument xmlReport = new XmlDocument();
            xmlReport.LoadXml(xml);

            MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(xml));
            msExcel.Position = 0;
            msExcel.Flush();

            return msExcel;
        }

        public MemoryStream CreateProfilesToMemoryStream(List<ProfilesListItem> profiles)
        {
            string headerXml = string.Empty;
            string itemsXml = string.Empty;
            string footerXml = string.Empty;

            if (profiles.Count > 0)
            {
                headerXml = AccessControlResources.ExcelProfilesHeader;
                headerXml = headerXml.Replace("#Author#", "VotoWeb");
                headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", cultureInfo));
                headerXml = headerXml.Replace("#ReportName#", "Perfis");

                bool even = true;
                itemsXml = string.Empty;

                StringBuilder sbItems = new StringBuilder();

                foreach (ProfilesListItem i in profiles)
                {
                    string itemXml = even ? AccessControlResources.ExcelProfilesItemEven : AccessControlResources.ExcelProfilesItemOdd;
                    itemXml = string.Format(itemXml, i.Name,
                    i.UsersCount.ToString("N0"));
                    sbItems.Append(itemXml);
                    even = !even;
                }
                itemsXml = sbItems.ToString();
                footerXml = AccessControlResources.ExcelProfilesFooter;
            }

            string xml = string.Concat(headerXml, itemsXml, footerXml);
            XmlDocument xmlReport = new XmlDocument();
            xmlReport.LoadXml(xml);

            MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(xml));
            msExcel.Position = 0;
            msExcel.Flush();

            return msExcel;
        }

        public MemoryStream CreateClientsRegisterToMemoryStream(List<FuncionalityClient> list)
        {
            string headerXml = string.Empty;
            string footerXml = string.Empty;

            StringBuilder sbItems = new StringBuilder();

            headerXml = AccessControlResources.ClientsRegisterHeader;
            headerXml = headerXml.Replace("#Author#", "VotoWeb");
            headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", cultureInfo));
            sbItems.Append(headerXml);

            bool even = true;

            if (list.Count > 0)
            {
                foreach (var li in list)
                {
                    string emails = string.Empty;
                    List<string> registeredEmails = new List<string>();
                    List<string> filteredList = li.Funcionalities.SelectMany(p => p.Emails.Select(t => t.Email)).ToList().Except(registeredEmails).ToList();
                    registeredEmails.AddRange(filteredList);
                    registeredEmails.ForEach(q => emails += (q + "&#10;"));
                    emails = emails.Length > 0 ? emails.Substring(0, emails.Length - 5) : emails;
                    int alturaLinha = 0;
                    registeredEmails.ForEach(q => alturaLinha++);

                    string copiaPara = string.Empty;
                    List<string> registeredEmails1 = new List<string>();
                    List<string> filteredList1 = li.Funcionalities.SelectMany(p => p.CopyTo.Select(t => t.Email)).ToList().Except(registeredEmails1).ToList();
                    registeredEmails1.AddRange(filteredList1);
                    registeredEmails1.ForEach(q => copiaPara += (q + "&#10;"));
                    copiaPara = copiaPara.Length > 0 ? copiaPara.Substring(0, copiaPara.Length - 5) : copiaPara;
                    int alturaLinha1 = 0;
                    registeredEmails1.ForEach(q => alturaLinha1++);

                    string item = string.Format(even ? AccessControlResources.ClientsRegisterItemEven : AccessControlResources.ClientsRegisterItemOdd,
                        alturaLinha1 > alturaLinha ? alturaLinha1 * 15 : alturaLinha * 15,
                        HttpUtility.HtmlEncode(li.ClientCode),
                        HttpUtility.HtmlEncode(li.ClientName),
                        emails,
                        copiaPara
                    );
                    sbItems.Append(item);
                    even = !even;
                }
            }

            sbItems.Append(AccessControlResources.ClientsRegisterFooter);

            string xml = sbItems.ToString();
            XmlDocument xmlReport = new XmlDocument();
            xmlReport.LoadXml(xml);

            MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(xml));
            msExcel.Position = 0;
            msExcel.Flush();

            return msExcel;
        }

        #endregion

        #region Other

        public MemoryStream CreateOrdersToMemoryStream(List<OrdersCorrection> list)
        {
            string headerXml = string.Empty;
            string footerXml = string.Empty;

            StringBuilder sbItems = new StringBuilder();

            if (list.Count > 0)
            {
                headerXml = OtherResources.OrdersCorrectionHeader;
                headerXml = headerXml.Replace("#Author#", "VotoWeb");
                headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", cultureInfo));

                sbItems.Append(headerXml);

                foreach (OrdersCorrection order in list)
                {
                    string item = string.Format(OtherResources.OrdersCorrectionItem, HttpUtility.HtmlEncode(order.Type.Description), HttpUtility.HtmlEncode(order.Responsible.Description), string.Format("{0} {1}", order.ClientFrom.Value, HttpUtility.HtmlEncode(order.ClientFrom.Description)), (order.Type.Value == "6") ? string.Format("{0} {1}", order.ClientTo.Value, HttpUtility.HtmlEncode(order.ClientTo.Description)) : "Conta Erro", order.Date.ToString("dd/MM/yyyy"), order.D1 ? "D-1" : "D0", HttpUtility.HtmlEncode(order.Status.Description));
                    sbItems.Append(item);

                    int counter = order.Details.Count;

                    if (order.Details != null && counter > 0)
                    {
                        sbItems.Append(OtherResources.OrdersCorrectionSubItemHeader);
                        foreach (OrdersCorrectionDetail detail in order.Details)
                        {
                            string subitem = string.Empty;
                            if (counter == 1)
                                subitem = string.Format(OtherResources.OrderCorrectionSubItemLast,
                                                        detail.StockCode,
                                                        detail.Quantity.ToString("N0"),
                                                        detail.Value.ToString("N4"),
                                                        HttpUtility.HtmlEncode(order.Description.Description),
                                                        HttpUtility.HtmlEncode(order.Comments));
                            else
                                subitem = string.Format(OtherResources.OrderCorrectionSubItem,
                                                        detail.StockCode,
                                                        detail.Quantity.ToString("N0"),
                                                        detail.Value.ToString("N4"));
                            sbItems.Append(subitem);
                            counter--;
                        }

                    }

                }

                sbItems.Append(OtherResources.OrdersCorrectionFooter);
            }

            string xml = sbItems.ToString();
            XmlDocument xmlReport = new XmlDocument();
            xmlReport.LoadXml(xml);

            MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(xml));
            msExcel.Position = 0;
            msExcel.Flush();

            return msExcel;
        }

        public MemoryStream CreateDiscountsToMemoryStream(List<BrokerageDiscount> list)
        {
            string headerXml = string.Empty;
            string footerXml = string.Empty;

            StringBuilder sbItems = new StringBuilder();

            if (list.Count > 0)
            {
                headerXml = OtherResources.BrokerageDiscountHeader;
                headerXml = headerXml.Replace("#Author#", "VotoWeb");
                headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", cultureInfo));

                sbItems.Append(headerXml);

                foreach (BrokerageDiscount discount in list)
                {
                    string item = string.Format(OtherResources.BrokerageDiscountItem,
                                            HttpUtility.HtmlEncode(discount.ClientCode + "-" + discount.DV),
                                            HttpUtility.HtmlEncode(discount.ClientName),
                                            HttpUtility.HtmlEncode(discount.Assessor),
                                            HttpUtility.HtmlEncode((discount.StockExchange == 1) ? "Bovespa" : "BM&F"),
                                            HttpUtility.HtmlEncode(discount.MarketType),
                                            HttpUtility.HtmlEncode(discount.Operation),
                                            HttpUtility.HtmlEncode(discount.Vigency),
                                            HttpUtility.HtmlEncode(discount.StockCode),
                                            HttpUtility.HtmlEncode(discount.Qtty),
                                            HttpUtility.HtmlEncode((discount.StockDiscountPercentual.HasValue) ? discount.StockDiscountPercentual.Value.ToString() + "%" : discount.StockDiscountValue.Value.ToString("n")),
                                            HttpUtility.HtmlEncode(discount.Description),
                                            HttpUtility.HtmlEncode(discount.Situation)
                    );
                    sbItems.Append(item);
                }

                sbItems.Append(OtherResources.BrokerageDiscountFooter);
            }

            string xml = sbItems.ToString();
            XmlDocument xmlReport = new XmlDocument();
            xmlReport.LoadXml(xml);

            MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(xml));
            msExcel.Position = 0;
            msExcel.Flush();

            return msExcel;
        }

        public MemoryStream CreateCustodyTransferToMemoryStream(List<CustodyTransfer> list)
        {
            string headerXml = string.Empty;
            string footerXml = string.Empty;

            StringBuilder sbItems = new StringBuilder();

            if (list.Count > 0)
            {
                headerXml = OtherResources.CustodyTransferHeader;
                headerXml = headerXml.Replace("#Author#", "VotoWeb");
                headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", cultureInfo));

                sbItems.Append(headerXml);

                foreach (CustodyTransfer listItem in list)
                {
                    string item = string.Format(OtherResources.CustodyTransferItem,
                                            HttpUtility.HtmlEncode(listItem.ClientCode + "-" + listItem.DV),
                                            HttpUtility.HtmlEncode(listItem.ClientName),
                                            HttpUtility.HtmlEncode(listItem.Assessor),
                                            HttpUtility.HtmlEncode(listItem.StockCode),
                                            HttpUtility.HtmlEncode(listItem.Qtty),
                                            HttpUtility.HtmlEncode(listItem.OrigBroker),
                                            HttpUtility.HtmlEncode(listItem.OrigPortfolio),
                                            HttpUtility.HtmlEncode(listItem.DestinyBroker),
                                            HttpUtility.HtmlEncode(listItem.DestinyPortfolio),
                                            HttpUtility.HtmlEncode(listItem.DescStatus),
                                            HttpUtility.HtmlEncode(listItem.RequestDate.ToShortDateString()),
                                            HttpUtility.HtmlEncode(listItem.Observations)
                    );
                    sbItems.Append(item);
                }

                sbItems.Append(OtherResources.CustodyTransferFooter);
            }

            string xml = sbItems.ToString();
            XmlDocument xmlReport = new XmlDocument();
            xmlReport.LoadXml(xml);

            MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(xml));
            msExcel.Position = 0;
            msExcel.Flush();

            return msExcel;
        }

        public MemoryStream CreateOperationsMapToMemoryStream(List<OperationsMap> list)
        {
            string headerXml = string.Empty;
            string footerXml = string.Empty;

            StringBuilder sbItems = new StringBuilder();

            headerXml = OtherResources.MapHeader;
            headerXml = headerXml.Replace("#Author#", "VotoWeb");
            headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", cultureInfo));
            sbItems.Append(headerXml);

            bool even1 = true;

            /*** Operadores ***/
            List<Operator> operatorList = new List<Operator>();
            if (list != null)
            {
                List<int> auxOperatorList = list.Select(a => a.OperatorCode).Distinct().ToList();
                operatorList = (from a in auxOperatorList
                                select new Operator
                                {
                                    OperatorCode = a,
                                    OperatorName = list.Where(b => b.OperatorCode == a).FirstOrDefault().Operator,
                                    BrokerageValue = list.Where(b => b.OperatorCode == a).Sum(c => c.BrokerageValue)
                                }).ToList();
            }

            if (list.Count > 0) // se existe operadores
            {
                if (operatorList.Count > 1) // se existe mais de um operador
                {
                    sbItems.Append(OtherResources.MapResumeHeader);

                    foreach (Operator li in operatorList) // passa por todos os operadores para o resumo
                    {
                        string item = string.Format(even1 ? OtherResources.MapResumeEven : OtherResources.MapResumeOdd,
                                                    HttpUtility.HtmlEncode(li.OperatorName),
                                                    HttpUtility.HtmlEncode(li.BrokerageValue.HasValue ? FormatStringToExcelNumber(li.BrokerageValue.Value.ToString()) : string.Empty)
                        );
                        sbItems.Append(item);
                        even1 = !even1;
                    }

                    sbItems.Append(OtherResources.MapSheetFooter);
                }

                foreach (Operator li in operatorList) // passa por todos os operadores para a planilha individual
                {
                    sbItems.Append(OtherResources.MapOperatorsHeader.Replace("#Operador#", li.OperatorName.Length > 30 ? li.OperatorName.Substring(0, 30) : li.OperatorName));

                    List<OperationsMap> list1 = list.Where(p => p.OperatorCode == li.OperatorCode).ToList();

                    even1 = true;

                    foreach (var li1 in list1) // passa por todas as operações do operador no ponteito atual
                    {
                        string item = string.Format(even1 ? OtherResources.MapOperatorsEven : OtherResources.MapOperatorsOdd,
                                                    HttpUtility.HtmlEncode(li1.ClientCode),
                                                    HttpUtility.HtmlEncode(CommonHelper.GetClientName(li1.ClientCode)),
                                                    HttpUtility.HtmlEncode(li1.TradingDate.HasValue ? li1.TradingDate.Value.ToShortDateString() : "-"),
                                                    HttpUtility.HtmlEncode(li1.OperationDesc),
                                                    HttpUtility.HtmlEncode(li1.StockCode),
                                                    HttpUtility.HtmlEncode(li1.TradingQtty.HasValue ? li1.TradingQtty.Value.ToString() : string.Empty),
                                                    HttpUtility.HtmlEncode(li1.TradingValue.HasValue ? FormatStringToExcelNumber(li1.TradingValue.Value.ToString()) : string.Empty),
                                                    HttpUtility.HtmlEncode(li1.Volume.HasValue ? li1.Volume.Value.ToString() : string.Empty),
                                                    HttpUtility.HtmlEncode(li1.BrokerageValue.HasValue ? FormatStringToExcelNumber(li1.BrokerageValue.Value.ToString()) : string.Empty)
                        );
                        sbItems.Append(item);
                        even1 = !even1;
                    }

                    sbItems.Append(OtherResources.MapSheetFooter);
                }
            }

            sbItems.Append(OtherResources.MapFooter);

            string xml = sbItems.ToString();
            XmlDocument xmlReport = new XmlDocument();
            xmlReport.LoadXml(xml);

            MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(xml));
            msExcel.Position = 0;
            msExcel.Flush();

            return msExcel;
        }

        public MemoryStream CreateOperationsToMemoryStream(List<OperationClient> list)
        {
            string headerXml = string.Empty;
            string footerXml = string.Empty;

            StringBuilder sbItems = new StringBuilder();

            headerXml = OtherResources.OperationsHeader;
            headerXml = headerXml.Replace("#Author#", "VotoWeb");
            headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", cultureInfo));
            sbItems.Append(headerXml);

            bool even1 = true;

            if (list.Count > 0)
            {
                foreach (var i in list)
                {
                    string item = string.Format(even1 ? OtherResources.OperationsEven : OtherResources.OperationsOdd,
                                                HttpUtility.HtmlEncode(i.TradingDate.HasValue ? i.TradingDate.Value.ToShortDateString() : "-"),
                                                HttpUtility.HtmlEncode(i.ClientCode),
                                                HttpUtility.HtmlEncode(i.ClientName),
                                                HttpUtility.HtmlEncode(i.AssessorName),
                                                HttpUtility.HtmlEncode(!String.IsNullOrEmpty(i.DescStatus) ? i.DescStatus : "-")
                    );
                    sbItems.Append(item);
                    even1 = !even1;
                }
            }

            sbItems.Append(OtherResources.OperationsFooter);

            string xml = sbItems.ToString();
            XmlDocument xmlReport = new XmlDocument();
            xmlReport.LoadXml(xml);

            MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(xml));
            msExcel.Position = 0;
            msExcel.Flush();

            return msExcel;
        }

        public MemoryStream CreateIncomeReportPositionToMemoryStream(IncomeReport item, Dictionary<string, bool> checks)
        {
            string headerXml = string.Empty;
            string footerXml = string.Empty;

            StringBuilder sbItems = new StringBuilder();

            headerXml = OtherIncomeReportResources.Header;
            headerXml = headerXml.Replace("#Author#", "VotoWeb");
            headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", cultureInfo));
            sbItems.Append(headerXml);

            #region Posição

            if (checks["Posicao"] &&
                (item.StockMarketList.Count > 0 ||
                item.StockRentList.Count > 0 ||
                item.OptionsList.Count > 0 ||
                item.BMFOptionsFutureList.Count > 0 ||
                item.BMFGoldList.Count > 0 ||
                item.BMFSwapList.Count > 0 ||
                item.IncomeProceedsList.Count > 0 ||
                item.IncomeTermsList.Count > 0))
            {
                #region A Vista

                if (item.StockMarketList.Count > 0)
                {
                    bool even = true;

                    sbItems.Append(OtherIncomeReportResources.HeaderAVista);

                    foreach (var i in item.StockMarketList)
                    {
                        string linha = string.Format(even ? OtherIncomeReportResources.ItemAVistaEven : OtherIncomeReportResources.ItemAVistaOdd,
                                                    HttpUtility.HtmlEncode(i.StockCode),
                                                    HttpUtility.HtmlEncode(i.AvailableQuantity.HasValue ? FormatStringToExcelNumber(i.AvailableQuantity.Value.ToString()) : string.Empty),
                                                    HttpUtility.HtmlEncode(i.Price.HasValue ? FormatStringToExcelNumber(i.Price.Value.ToString()) : string.Empty),
                                                    HttpUtility.HtmlEncode(i.TotalValue.HasValue ? FormatStringToExcelNumber(i.TotalValue.Value.ToString()) : string.Empty)
                        );
                        sbItems.Append(linha);
                        even = !even;
                    }

                    sbItems.Append(OtherIncomeReportResources.SheetFooter);
                }

                #endregion

                #region Aluguel de Ações

                if (item.StockRentList.Count > 0)
                {
                    bool even = true;

                    sbItems.Append(OtherIncomeReportResources.HeaderAluguel);

                    foreach (var i in item.StockRentList)
                    {
                        string linha = string.Format(even ? OtherIncomeReportResources.ItemAluguelEven : OtherIncomeReportResources.ItemAluguelOdd,
                                                    HttpUtility.HtmlEncode(i.StockCode),
                                                    HttpUtility.HtmlEncode(i.Type),
                                                    HttpUtility.HtmlEncode(i.OpenDate.HasValue ? i.OpenDate.Value.ToString("dd/MM/yyyy") : "-"),
                                                    HttpUtility.HtmlEncode(i.PaymentDate.HasValue ? i.PaymentDate.Value.ToString("dd/MM/yyyy") : "-"),
                                                    HttpUtility.HtmlEncode(i.Quantity.HasValue ? FormatStringToExcelNumber(i.Quantity.Value.ToString()) : string.Empty),
                                                    HttpUtility.HtmlEncode(i.Tax.HasValue ? FormatStringToExcelNumber(i.Tax.Value.ToString()) : string.Empty),
                                                    HttpUtility.HtmlEncode(i.Quote.HasValue ? FormatStringToExcelNumber(i.Quote.Value.ToString()) : string.Empty),
                                                    HttpUtility.HtmlEncode(i.TotalValue.HasValue ? FormatStringToExcelNumber(i.TotalValue.Value.ToString()) : string.Empty)
                        );
                        sbItems.Append(linha);
                        even = !even;
                    }

                    sbItems.Append(OtherIncomeReportResources.SheetFooter);
                }

                #endregion

                #region Opções

                if (item.OptionsList.Count > 0)
                {
                    bool even = true;

                    sbItems.Append(OtherIncomeReportResources.HeaderOpcoes);

                    foreach (var i in item.OptionsList)
                    {
                        string linha = string.Format(even ? OtherIncomeReportResources.ItemOpcoesEven : OtherIncomeReportResources.ItemOpcoesOdd,
                                                    HttpUtility.HtmlEncode(i.StockCode),
                                                    HttpUtility.HtmlEncode(i.OperationType),
                                                    HttpUtility.HtmlEncode(i.Type),
                                                    HttpUtility.HtmlEncode(i.PaymentDate.HasValue ? i.PaymentDate.Value.ToString("dd/MM/yyyy") : "-"),
                                                    HttpUtility.HtmlEncode(i.AvailableQtty.HasValue ? FormatStringToExcelNumber(i.AvailableQtty.Value.ToString()) : string.Empty),
                                                    HttpUtility.HtmlEncode(i.Price.HasValue ? FormatStringToExcelNumber(i.Price.Value.ToString()) : string.Empty),
                                                    HttpUtility.HtmlEncode(i.Volume.HasValue ? FormatStringToExcelNumber(i.Volume.Value.ToString()) : string.Empty),
                                                    HttpUtility.HtmlEncode(i.VistaVolume.HasValue ? FormatStringToExcelNumber(i.VistaVolume.Value.ToString()) : string.Empty)
                        );
                        sbItems.Append(linha);
                        even = !even;
                    }

                    sbItems.Append(OtherIncomeReportResources.SheetFooter);
                }

                #endregion

                #region BM&F - Futuro e Opções

                if (item.BMFOptionsFutureList.Count > 0)
                {
                    bool even = true;

                    sbItems.Append(OtherIncomeReportResources.HeaderFuturo);

                    foreach (var i in item.BMFOptionsFutureList)
                    {
                        string linha = string.Format(even ? OtherIncomeReportResources.ItemFuturoEven : OtherIncomeReportResources.ItemFuturoOdd,
                                                    HttpUtility.HtmlEncode(i.StockCode),
                                                    HttpUtility.HtmlEncode(i.Type),
                                                    HttpUtility.HtmlEncode(i.PaymentDate.HasValue ? i.PaymentDate.Value.ToString("dd/MM/yyyy") : "-"),
                                                    HttpUtility.HtmlEncode(i.Price.HasValue ? FormatStringToExcelNumber(i.Price.Value.ToString()) : string.Empty),
                                                    HttpUtility.HtmlEncode(i.CV),
                                                    HttpUtility.HtmlEncode(i.Position.HasValue ? FormatStringToExcelNumber(i.Position.Value.ToString()) : string.Empty)
                        );
                        sbItems.Append(linha);
                        even = !even;
                    }

                    sbItems.Append(OtherIncomeReportResources.SheetFooter);
                }

                #endregion

                #region BM&F - Ouro

                if (item.BMFGoldList.Count > 0)
                {
                    bool even = true;

                    sbItems.Append(OtherIncomeReportResources.HeaderOuro);

                    foreach (var i in item.BMFGoldList)
                    {
                        string linha = string.Format(even ? OtherIncomeReportResources.ItemOuroEven : OtherIncomeReportResources.ItemOuroOdd,
                                                    HttpUtility.HtmlEncode(i.Pounds.HasValue ? FormatStringToExcelNumber(i.Pounds.Value.ToString()) : string.Empty),
                                                    HttpUtility.HtmlEncode(i.DefaultQuantity.HasValue ? FormatStringToExcelNumber(i.DefaultQuantity.Value.ToString()) : string.Empty),
                                                    HttpUtility.HtmlEncode(i.Quote.HasValue ? FormatStringToExcelNumber(i.Quote.Value.ToString()) : string.Empty),
                                                    HttpUtility.HtmlEncode(i.TotalValue.HasValue ? FormatStringToExcelNumber(i.TotalValue.Value.ToString()) : string.Empty)
                        );
                        sbItems.Append(linha);
                        even = !even;
                    }

                    sbItems.Append(OtherIncomeReportResources.SheetFooter);
                }

                #endregion

                #region Swap e Opções Flexíveis

                if (item.BMFSwapList.Count > 0)
                {
                    bool even = true;

                    sbItems.Append(OtherIncomeReportResources.HeaderSwap);

                    foreach (var i in item.BMFSwapList)
                    {
                        string linha = string.Format(even ? OtherIncomeReportResources.ItemSwapEven : OtherIncomeReportResources.ItemSwapOdd,
                                                    HttpUtility.HtmlEncode(i.Contract),
                                                    HttpUtility.HtmlEncode(i.Number.HasValue ? FormatStringToExcelNumber(i.Number.Value.ToString()) : string.Empty),
                                                    HttpUtility.HtmlEncode(i.Base.HasValue ? FormatStringToExcelNumber(i.Base.Value.ToString()) : string.Empty),
                                                    HttpUtility.HtmlEncode(i.PaymentDate.HasValue ? i.PaymentDate.Value.ToString("dd/MM/yyyy") : "-"),
                                                    HttpUtility.HtmlEncode(i.RegisterDate.HasValue ? i.RegisterDate.Value.ToString("dd/MM/yyyy") : "-"),
                                                    HttpUtility.HtmlEncode(i.NetTotal.HasValue ? FormatStringToExcelNumber(i.NetTotal.Value.ToString()) : string.Empty)
                        );
                        sbItems.Append(linha);
                        even = !even;
                    }

                    sbItems.Append(OtherIncomeReportResources.SheetFooter);
                }

                #endregion

                #region Proventos Pagos

                if (item.IncomeProceedsList.Count > 0)
                {
                    bool even = true;

                    sbItems.Append(OtherIncomeReportResources.HeaderProventos);

                    foreach (var i in item.IncomeProceedsList)
                    {
                        string linha = string.Format(even ? OtherIncomeReportResources.ItemProventosEven : OtherIncomeReportResources.ItemProventosOdd,
                                                    HttpUtility.HtmlEncode(i.StockCode),
                                                    HttpUtility.HtmlEncode(i.Type),
                                                    HttpUtility.HtmlEncode(i.AvailableQuantity.HasValue ? FormatStringToExcelNumber(i.AvailableQuantity.Value.ToString()) : string.Empty),
                                                    HttpUtility.HtmlEncode(i.GrossValue.HasValue ? FormatStringToExcelNumber(i.GrossValue.Value.ToString()) : string.Empty),
                                                    HttpUtility.HtmlEncode(i.IRPerc.HasValue ? FormatStringToExcelNumber(i.IRPerc.Value.ToString()) : string.Empty),
                                                    HttpUtility.HtmlEncode(i.IRValue.HasValue ? FormatStringToExcelNumber(i.IRValue.Value.ToString()) : string.Empty),
                                                    HttpUtility.HtmlEncode(i.NetValue.HasValue ? FormatStringToExcelNumber(i.NetValue.Value.ToString()) : string.Empty),
                                                    HttpUtility.HtmlEncode(i.PaymentDate.HasValue ? i.PaymentDate.Value.ToString("dd/MM/yyyy") : "-")
                        );
                        sbItems.Append(linha);
                        even = !even;
                    }

                    sbItems.Append(OtherIncomeReportResources.SheetFooter);
                }

                #endregion

                #region Termo

                if (item.IncomeTermsList.Count > 0)
                {
                    bool even = true;

                    sbItems.Append(OtherIncomeReportResources.HeaderTermo);

                    foreach (var i in item.IncomeTermsList)
                    {
                        string linha = string.Format(even ? OtherIncomeReportResources.ItemTermoEven : OtherIncomeReportResources.ItemTermoOdd,
                                                    HttpUtility.HtmlEncode(i.StockCode),
                                                    HttpUtility.HtmlEncode(i.AvailableQuantity.HasValue ? FormatStringToExcelNumber(i.AvailableQuantity.Value.ToString()) : string.Empty),
                                                    HttpUtility.HtmlEncode(i.Price.HasValue ? FormatStringToExcelNumber(i.Price.Value.ToString()) : string.Empty),
                                                    HttpUtility.HtmlEncode(i.PaymentDate.HasValue ? i.PaymentDate.Value.ToString("dd/MM/yyyy") : "-"),
                                                    HttpUtility.HtmlEncode(i.Total.HasValue ? FormatStringToExcelNumber(i.Total.Value.ToString()) : string.Empty)
                        );
                        sbItems.Append(linha);
                        even = !even;
                    }

                    sbItems.Append(OtherIncomeReportResources.SheetFooter);
                }

                #endregion
            }

            #endregion

            #region IRRF Day Trade

            if (checks["DayTrade"] && item.IRRFDayTradeList.Count > 0)
            {
                bool even = true;

                sbItems.Append(OtherIncomeReportResources.HeaderDayTrade);

                foreach (var i in item.IRRFDayTradeList)
                {
                    string linha = string.Format(even ? OtherIncomeReportResources.ItemDayTradeEven : OtherIncomeReportResources.ItemDayTradeOdd,
                                                HttpUtility.HtmlEncode(i.Month),
                                                HttpUtility.HtmlEncode(i.GrossValue.HasValue ? FormatStringToExcelNumber(i.GrossValue.Value.ToString()) : string.Empty),
                                                HttpUtility.HtmlEncode(i.Retained.HasValue ? FormatStringToExcelNumber(i.Retained.Value.ToString()) : string.Empty)
                    );
                    sbItems.Append(linha);
                    even = !even;
                }

                sbItems.Append(OtherIncomeReportResources.SheetFooter);
            }

            #endregion

            #region Operações

            if (checks["Operacoes"] && item.IRRFOperationsList.Count > 0)
            {
                bool even = true;

                sbItems.Append(OtherIncomeReportResources.HeaderOperacoes);

                foreach (var i in item.IRRFOperationsList)
                {
                    string linha = string.Format(even ? OtherIncomeReportResources.ItemOperacoesEven : OtherIncomeReportResources.ItemOperacoesOdd,
                                                HttpUtility.HtmlEncode(i.Month),
                                                HttpUtility.HtmlEncode(i.GrossValue.HasValue ? FormatStringToExcelNumber(i.GrossValue.Value.ToString()) : string.Empty),
                                                HttpUtility.HtmlEncode(i.Retained.HasValue ? FormatStringToExcelNumber(i.Retained.Value.ToString()) : string.Empty)
                    );
                    sbItems.Append(linha);
                    even = !even;
                }

                sbItems.Append(OtherIncomeReportResources.SheetFooter);
            }

            #endregion

            sbItems.Append(OtherIncomeReportResources.Footer);

            string xml = sbItems.ToString();
            XmlDocument xmlReport = new XmlDocument();
            xmlReport.LoadXml(xml);

            MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(xml));
            msExcel.Position = 0;
            msExcel.Flush();

            return msExcel;
        }

        public MemoryStream CreatePrivateReportToMemoryStream(List<Private> list, string refDate)
        {
            string headerXml = string.Empty;
            string footerXml = string.Empty;

            StringBuilder sbItems = new StringBuilder();

            if (list.Count > 0)
            {
                int counter = 0;
                headerXml = OtherResources.PrivateHeader;
                headerXml = headerXml.Replace("#Author#", "VotoWeb");
                headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", cultureInfo));
                headerXml = headerXml.Replace("#Ref#", "Refer&#234;ncia&#58;" + refDate);

                sbItems.Append(headerXml);

                foreach (Private listItem in list)
                {
                    string model = counter % 2 != 0 ? OtherResources.PrivateItem : OtherResources.PrivateAltItem;
                    string item = string.Format(model,
                                            HttpUtility.HtmlEncode(listItem.CPFCGC),
                                            HttpUtility.HtmlEncode(listItem.ClientName),
                                            HttpUtility.HtmlEncode(listItem.Product),
                                            HttpUtility.HtmlEncode(listItem.BrokerageValue.HasValue ? listItem.BrokerageValue.Value.ToString("N2") : "-"),
                                            HttpUtility.HtmlEncode(listItem.PositionValue.HasValue ? listItem.PositionValue.Value.ToString("N2") : "-"),
                                            HttpUtility.HtmlEncode(listItem.AvailableValue.HasValue ? listItem.AvailableValue.Value.ToString("N2") : "-"),
                                            HttpUtility.HtmlEncode(listItem.CaptationValue.HasValue ? listItem.CaptationValue.Value.ToString("N2") : "-"),
                                            HttpUtility.HtmlEncode(listItem.ApplicationValue.HasValue ? listItem.ApplicationValue.Value.ToString("N2") : "-"),
                                            HttpUtility.HtmlEncode(listItem.RedemptionValue.HasValue ? listItem.RedemptionValue.Value.ToString("N2") : "-")
                    );
                    sbItems.Append(item);
                    counter++;
                }

                sbItems.Append(OtherResources.PrivateFooter);
            }

            string xml = sbItems.ToString();
            XmlDocument xmlReport = new XmlDocument();
            xmlReport.LoadXml(xml);

            MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(xml));
            msExcel.Position = 0;
            msExcel.Flush();

            return msExcel;
        }

        public MemoryStream CreateBBRentToMemoryStream(BBRent item)
        {
            string headerXml = string.Empty;
            string footerXml = string.Empty;

            StringBuilder sbItems = new StringBuilder();

            headerXml = BBRentResources.Header;
            headerXml = headerXml.Replace("#Author#", "VotoWeb");
            headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", cultureInfo));
            sbItems.Append(headerXml);

            if (item.Settlements.Count > 0)
            {
                bool even = true;

                sbItems.Append(BBRentResources.HeaderSettlement);

                foreach (var i in item.Settlements)
                {
                    string linha = string.Format(even ? BBRentResources.ItemSettlementEven : BBRentResources.ItemSettlementOdd,
                        HttpUtility.HtmlEncode(i.Date.ToShortDateString()),
                        HttpUtility.HtmlEncode(i.ClientCode.ToString()),
                        HttpUtility.HtmlEncode(i.ByAccount.HasValue ? i.ByAccount.Value.ToString() : "-"),
                        HttpUtility.HtmlEncode(i.Contract.ToString()),
                        HttpUtility.HtmlEncode(i.Stock),
                        HttpUtility.HtmlEncode(FormatStringToExcelNumber(i.Quantity.ToString())),
                        HttpUtility.HtmlEncode(FormatStringToExcelNumber(i.Gross.ToString())),
                        HttpUtility.HtmlEncode(FormatStringToExcelNumber(i.IR.ToString())),
                        HttpUtility.HtmlEncode(FormatStringToExcelNumber(i.NetValue.ToString())),
                        HttpUtility.HtmlEncode(FormatStringToExcelNumber(i.TotalCommission.ToString())),
                        HttpUtility.HtmlEncode(FormatStringToExcelNumber(i.NetCommission.ToString()))
                    );
                    sbItems.Append(linha);
                    even = !even;
                }

                sbItems.Append(string.Format(BBRentResources.FooterSettlement,
                    HttpUtility.HtmlEncode(FormatStringToExcelNumber(item.Settlements.Sum(p => p.Gross).ToString())),
                    HttpUtility.HtmlEncode(FormatStringToExcelNumber(item.Settlements.Sum(p => p.IR).ToString())),
                    HttpUtility.HtmlEncode(FormatStringToExcelNumber(item.Settlements.Sum(p => p.NetValue).ToString())),
                    HttpUtility.HtmlEncode(FormatStringToExcelNumber(item.Settlements.Sum(p => p.TotalCommission).ToString())),
                    HttpUtility.HtmlEncode(FormatStringToExcelNumber(item.Settlements.Sum(p => p.NetCommission).ToString()))
                ));
            }


            if (item.Transfers.Count > 0)
            {
                bool even = true;

                sbItems.Append(BBRentResources.HeaderTransfer);

                foreach (var i in item.Transfers)
                {
                    string linha = string.Format(even ? BBRentResources.ItemTransferEven : BBRentResources.ItemTransferOdd,
                        HttpUtility.HtmlEncode(i.Date.ToShortDateString()),
                        HttpUtility.HtmlEncode(i.ClientCode.ToString()),
                        HttpUtility.HtmlEncode(i.ByAccount.HasValue ? i.ByAccount.Value.ToString() : "-"),
                        HttpUtility.HtmlEncode(i.Contract.ToString()),
                        HttpUtility.HtmlEncode(i.Stock),
                        HttpUtility.HtmlEncode(FormatStringToExcelNumber(i.Quantity.ToString())),
                        HttpUtility.HtmlEncode(FormatStringToExcelNumber(i.Gross.ToString())),
                        HttpUtility.HtmlEncode(FormatStringToExcelNumber(i.IR.ToString())),
                        HttpUtility.HtmlEncode(FormatStringToExcelNumber(i.NetValue.ToString())),
                        HttpUtility.HtmlEncode(FormatStringToExcelNumber(i.TotalCommission.ToString())),
                        HttpUtility.HtmlEncode(FormatStringToExcelNumber(i.NetCommission.ToString())),
                        HttpUtility.HtmlEncode(FormatStringToExcelNumber(i.Transfers.ToString()))
                    );
                    sbItems.Append(linha);
                    even = !even;
                }

                sbItems.Append(string.Format(BBRentResources.FooterTransfer,
                    HttpUtility.HtmlEncode(FormatStringToExcelNumber(item.Transfers.Sum(p => p.Gross).ToString())),
                    HttpUtility.HtmlEncode(FormatStringToExcelNumber(item.Transfers.Sum(p => p.IR).ToString())),
                    HttpUtility.HtmlEncode(FormatStringToExcelNumber(item.Transfers.Sum(p => p.NetValue).ToString())),
                    HttpUtility.HtmlEncode(FormatStringToExcelNumber(item.Transfers.Sum(p => p.TotalCommission).ToString())),
                    HttpUtility.HtmlEncode(FormatStringToExcelNumber(item.Transfers.Sum(p => p.NetCommission).ToString())),
                    HttpUtility.HtmlEncode(FormatStringToExcelNumber(item.Transfers.Sum(p => p.Transfers).ToString()))
                ));
            }


            if (item.Proceeds.Count > 0)
            {
                bool even = true;

                sbItems.Append(BBRentResources.HeaderProceeds);

                foreach (var i in item.Proceeds)
                {
                    string linha = string.Format(even ? BBRentResources.ItemProceedsEven : BBRentResources.ItemProceedsOdd,
                        HttpUtility.HtmlEncode(i.Date.ToShortDateString()),
                        HttpUtility.HtmlEncode(i.ClientCode.ToString()),
                        HttpUtility.HtmlEncode(i.ByAccount.HasValue ? i.ByAccount.Value.ToString() : "-"),
                        HttpUtility.HtmlEncode(i.Stock),
                        HttpUtility.HtmlEncode(FormatStringToExcelNumber(i.Quantity.ToString())),
                        HttpUtility.HtmlEncode(FormatStringToExcelNumber(i.NetValue.ToString())),
                        HttpUtility.HtmlEncode(i.Type)
                    );
                    sbItems.Append(linha);
                    even = !even;
                }

                sbItems.Append(string.Format(BBRentResources.FooterProceeds,
                    HttpUtility.HtmlEncode(FormatStringToExcelNumber(item.Proceeds.Sum(p => p.NetValue).ToString()))
                ));
            }


            sbItems.Append(BBRentResources.Footer);

            string xml = sbItems.ToString();
            XmlDocument xmlReport = new XmlDocument();
            xmlReport.LoadXml(xml);

            MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(xml));
            msExcel.Position = 0;
            msExcel.Flush();

            return msExcel;
        }

        public MemoryStream CreateVAMToMemoryStream(List<VAMItem> list)
        {
            string headerXml = string.Empty;
            string footerXml = string.Empty;

            StringBuilder sbItems = new StringBuilder();

            headerXml = OtherResources.VAMHeader;
            headerXml = headerXml.Replace("#Author#", "VotoWeb");
            headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", cultureInfo));
            sbItems.Append(headerXml);

            bool even = true;

            foreach (VAMItem i in list)
            {
                string linha = string.Format(even ? OtherResources.VAMItemEven : OtherResources.VAMItemOdd,
                    i.ClientCode,
                    i.ClientDigit,
                    HttpUtility.HtmlEncode(i.ClientName.Trim()),
                    i.AssessorCode,
                    HttpUtility.HtmlEncode(i.AssessorName.Trim()),
                    HttpUtility.HtmlEncode(i.Market),
                    HttpUtility.HtmlEncode(i.MarketDescription),
                    HttpUtility.HtmlEncode(i.Portfolio),
                    HttpUtility.HtmlEncode(i.PortfolioDescription.Trim()),
                    HttpUtility.HtmlEncode(FormatStringToExcelNumber(i.Price.ToString())),
                    HttpUtility.HtmlEncode(FormatStringToExcelNumber(i.TotalQuantity.ToString())),
                    HttpUtility.HtmlEncode(FormatStringToExcelNumber(i.AvailableQuantity.ToString())),
                    HttpUtility.HtmlEncode(FormatStringToExcelNumber(i.D1Quantity.ToString())),
                    HttpUtility.HtmlEncode(FormatStringToExcelNumber(i.D2Quantity.ToString())),
                    HttpUtility.HtmlEncode(FormatStringToExcelNumber(i.D3Quantity.ToString())),
                    HttpUtility.HtmlEncode(FormatStringToExcelNumber(i.BloquedQuantity.ToString())),
                    HttpUtility.HtmlEncode(FormatStringToExcelNumber(i.PendingQuantity.ToString())),
                    HttpUtility.HtmlEncode(FormatStringToExcelNumber(i.QuantityToSettle.ToString())),
                    HttpUtility.HtmlEncode(FormatStringToExcelNumber(i.NegotiableQuantity.ToString())),
                    HttpUtility.HtmlEncode(i.DueDate)
                );
                sbItems.Append(linha);
                even = !even;
            }


            sbItems.Append(OtherResources.VAMFooter);

            string xml = sbItems.ToString();
            XmlDocument xmlReport = new XmlDocument();
            xmlReport.LoadXml(xml);

            MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(xml));
            msExcel.Position = 0;
            msExcel.Flush();

            return msExcel;
        }

        #endregion

        #region Financial

        public MemoryStream CreateRedemptionRequestItemsToMemoryStream(List<RedemptionRequestListItem> list)
        {
            string headerXml = string.Empty;
            string footerXml = string.Empty;

            StringBuilder sbItems = new StringBuilder();

            if (list.Count > 0)
            {
                headerXml = FinancialResources.RedemptionRequestHeader;
                headerXml = headerXml.Replace("#Author#", "VotoWeb");
                headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", cultureInfo));

                sbItems.Append(headerXml);

                foreach (RedemptionRequestListItem i in list)
                {
                    string item = string.Format(FinancialResources.RedemptionRequestItem,
                                                HttpUtility.HtmlEncode(i.RequestedDate),
                                                HttpUtility.HtmlEncode(i.ClientCode),
                                                HttpUtility.HtmlEncode(i.ClientName),
                                                HttpUtility.HtmlEncode(i.Bank),
                                                HttpUtility.HtmlEncode(i.Agency),
                                                HttpUtility.HtmlEncode(i.Account),
                                                HttpUtility.HtmlEncode(i.Amount.Replace(".", "").Replace(",", ".")),
                                                HttpUtility.HtmlEncode(i.StatusDescription)
                    );
                    sbItems.Append(item);
                }

                sbItems.Append(FinancialResources.RedemptionRequestFooter);
            }

            string xml = sbItems.ToString();
            XmlDocument xmlReport = new XmlDocument();
            xmlReport.LoadXml(xml);

            MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(xml));
            msExcel.Position = 0;
            msExcel.Flush();

            return msExcel;
        }

        public MemoryStream CreateProjectedReportToMemoryStream(List<ProjectedReportItem> list, string groupBy)
        {
            string headerXml = string.Empty;
            string itemsXml = string.Empty;
            string footerXml = string.Empty;

            if (list.Count > 0)
            {
                headerXml = FinancialResources.ProjectedReportHeader;
                headerXml = headerXml.Replace("#Author#", "VotoWeb");
                headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", cultureInfo));

                bool even = true;
                itemsXml = string.Empty;

                if (groupBy.Equals("A"))
                {
                    List<GroupByItem> groupItems = (from l in list group l by new { l.Assessor } into g select new GroupByItem { Name = g.Key.Assessor }).ToList();

                    List<ProjectedReportItem> subGroupItems = new List<ProjectedReportItem>();

                    int countAssessor = 1;

                    foreach (GroupByItem i in groupItems)
                    {
                        string worksheetHeader = string.Format(FinancialResources.ProjectedReportSheetHeader, "Preg&#227;o de", "C&#243;digo", "Nome", "%", "Nota", "Volume", "Corretagem", "Reg. Bolsas", "Emolumentos", "Liquida&#231;&#227;o", "A.N.A.", "IRPF", "A receber/pagar");

                        string name = (i.Name).Length > 25 ? i.Name.Substring(0, 25) : i.Name;
                        name = HttpUtility.HtmlEncode(name);
                        itemsXml += worksheetHeader.Replace("#MainName#", name).Replace("#SheetName#", string.Format("{0} - {1}", countAssessor, name));

                        countAssessor++;

                        subGroupItems = (from g in list where g.Assessor.Equals(i.Name) select g).ToList();

                        foreach (ProjectedReportItem p in subGroupItems)
                        {
                            string itemXml = even ? FinancialResources.ProjectedReportSheetItemEven : FinancialResources.ProjectedReportSheetItemOdd;
                            itemXml = string.Format(itemXml,
                                p.TradingDate.ToShortDateString(),
                                p.ClientID,
                                HttpUtility.HtmlEncode(p.ClientName),
                                p.Percentage.ToString().Replace(',', '.'),
                                p.Note,
                                p.Volume.ToString().Replace(',', '.'),
                                p.Brokerage.ToString().Replace(',', '.'),
                                p.Record.ToString().Replace(',', '.'),
                                p.Fees.ToString().Replace(',', '.'),
                                p.SettlementDate.ToShortDateString(),
                                p.ANA.ToString().Replace(',', '.'),
                                p.IRRF.ToString().Replace(',', '.'),
                                p.NetValue.ToString().Replace(',', '.')
                                );
                            itemsXml += itemXml;
                            even = !even;
                        }
                        even = true;
                        itemsXml += FinancialResources.ProjectedReportSheetFooter;
                    }
                }
                else
                {
                    itemsXml += string.Format(FinancialResources.ProjectedReportSheetHeaderCombined, "Assessor", "Preg&#227;o de", "C&#243;digo", "Nome", "%", "Nota", "Volume", "Corretagem", "Reg. Bolsas", "Emolumentos", "Liquida&#231;&#227;o", "A.N.A.", "IRPF", "A receber/pagar");

                    list = (from tb in list orderby tb.Assessor select tb).ToList();

                    foreach (ProjectedReportItem p in list)
                    {
                        string itemXml = even ? FinancialResources.ProjectedReportSheetItemEvenCombined : FinancialResources.ProjectedReportSheetItemOddCombined;
                        itemXml = string.Format(itemXml,
                            HttpUtility.HtmlEncode(p.Assessor),
                            p.TradingDate.ToShortDateString(),
                            p.ClientID,
                            HttpUtility.HtmlEncode(p.ClientName),
                            p.Percentage.ToString().Replace(',', '.'),
                            p.Note,
                            p.Volume.ToString().Replace(',', '.'),
                            p.Brokerage.ToString().Replace(',', '.'),
                            p.Record.ToString().Replace(',', '.'),
                            p.Fees.ToString().Replace(',', '.'),
                            p.SettlementDate.ToShortDateString(),
                            p.ANA.ToString().Replace(',', '.'),
                            p.IRRF.ToString().Replace(',', '.'),
                            p.NetValue.ToString().Replace(',', '.')
                            );
                        itemsXml += itemXml;
                        even = !even;
                    }

                    itemsXml += FinancialResources.ProjectedReportSheetFooter;
                }
                footerXml = FinancialResources.ProjectedReportFooter;
            }

            string xml = string.Concat(headerXml, itemsXml, footerXml);
            XmlDocument xmlReport = new XmlDocument();
            xmlReport.LoadXml(xml);

            MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(xml));
            msExcel.Position = 0;
            msExcel.Flush();

            return msExcel;
        }

        public MemoryStream CreateDailyFinancialReportToMemoryStream(List<DailyFinancial> list)
        {
            string headerXml = string.Empty;
            string footerXml = string.Empty;

            StringBuilder sbItems = new StringBuilder();

            if (list.Count > 0)
            {
                headerXml = FinancialResources.DailyFinancialReportHeader;
                headerXml = headerXml.Replace("#Author#", "VotoWeb");
                headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", cultureInfo));

                sbItems.Append(headerXml);

                foreach (DailyFinancial i in list)
                {
                    string item = string.Format(FinancialResources.DailyFinancialReportItem,
                                                HttpUtility.HtmlEncode(i.ClientID.ToString() + "-" + i.DV.ToString()),
                                                HttpUtility.HtmlEncode(i.ClientName),
                                                HttpUtility.HtmlEncode(i.AssessorName),
                                                HttpUtility.HtmlEncode(i.AnteriorBalance.ToString().Replace(",", ".")),
                                                HttpUtility.HtmlEncode(i.AccountBalance.ToString().Replace(",", ".")),
                                                HttpUtility.HtmlEncode(i.Credit.ToString().Replace(",", ".")),
                                                HttpUtility.HtmlEncode(i.Debt.ToString().Replace(",", ".")),
                                                HttpUtility.HtmlEncode(i.Finished.ToString().Replace(",", ".")),
                                                HttpUtility.HtmlEncode(i.LiquidResult.ToString().Replace(",", ".")),
                                                HttpUtility.HtmlEncode(i.ProjectedBalance.ToString().Replace(",", ".")),
                                                HttpUtility.HtmlEncode(i.FinalBalance.ToString().Replace(",", ".")),
                                                HttpUtility.HtmlEncode((i.CustodyTax.HasValue) ? (i.CustodyTax.Value) ? "Sim" : "Não" : "")
                    );
                    sbItems.Append(item);
                }

                sbItems.Append(FinancialResources.DailyFinancialReportFooter);
            }

            string xml = sbItems.ToString();
            XmlDocument xmlReport = new XmlDocument();
            xmlReport.LoadXml(xml);

            MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(xml));
            msExcel.Position = 0;
            msExcel.Flush();

            return msExcel;
        }

        #endregion

        #region OnlinePortfolio

        public MemoryStream CreatePortfolioPositionToMemoryStream(PortfolioPositionClientContent clientPosition)
        {
            string headerXml = string.Empty;
            string footerXml = string.Empty;

            StringBuilder sbItems = new StringBuilder();

            if (clientPosition != null)
            {
                headerXml = OnlinePortfolioResources.PositionHeader;

                List<KeyValuePair<string, string>> headerValues = new List<KeyValuePair<string, string>>();
                headerValues.Add(new KeyValuePair<string, string>("Client", string.Concat(clientPosition.ClientCode, " ", HttpUtility.HtmlEncode(clientPosition.ClientName))));
                headerValues.Add(new KeyValuePair<string, string>("Phone", clientPosition.ClientPhone));
                headerValues.Add(new KeyValuePair<string, string>("Email", clientPosition.ClientEmail));
                headerValues.Add(new KeyValuePair<string, string>("StartDate", clientPosition.ClientStartDate));
                headerValues.Add(new KeyValuePair<string, string>("StartValue", FormatStringToExcelNumber(clientPosition.ClientStartValue)));

                headerValues.Add(new KeyValuePair<string, string>("FinPosition", FormatStringToExcelNumber(clientPosition.Financial.Position)));
                headerValues.Add(new KeyValuePair<string, string>("FinCash", FormatStringToExcelNumber(clientPosition.Financial.Cash)));
                headerValues.Add(new KeyValuePair<string, string>("FinTotal", FormatStringToExcelNumber(clientPosition.Financial.Total)));

                headerValues.Add(new KeyValuePair<string, string>("IbovToday", FormatStringToExcelNumber(clientPosition.Ibov.TodayPoints)));
                headerValues.Add(new KeyValuePair<string, string>("IbovYear", FormatStringToExcelNumber(clientPosition.Ibov.YearPoints)));
                headerValues.Add(new KeyValuePair<string, string>("IbovTodayVar", FormatStringToExcelNumber(clientPosition.Ibov.TodayVariation)));
                headerValues.Add(new KeyValuePair<string, string>("IbovYearVar", FormatStringToExcelNumber(clientPosition.Ibov.YearVariation)));
                headerValues.Add(new KeyValuePair<string, string>("IbovTodayDate", DateTime.Now.ToShortDateString()));
                headerValues.Add(new KeyValuePair<string, string>("IbovYearDate", clientPosition.Ibov.YearDate));

                headerValues.Add(new KeyValuePair<string, string>("CCDiscount", clientPosition.CCDiscount));
                headerValues.Add(new KeyValuePair<string, string>("CCDayTrade", FormatStringToExcelNumber(clientPosition.Discount.DayTrade)));
                headerValues.Add(new KeyValuePair<string, string>("CCAvailable", FormatStringToExcelNumber(clientPosition.Discount.Available)));
                headerValues.Add(new KeyValuePair<string, string>("CCOperations", FormatStringToExcelNumber(clientPosition.Discount.DailyOperations)));
                headerValues.Add(new KeyValuePair<string, string>("CCD1", FormatStringToExcelNumber(clientPosition.Discount.D1)));
                headerValues.Add(new KeyValuePair<string, string>("CCD2", FormatStringToExcelNumber(clientPosition.Discount.D2)));
                headerValues.Add(new KeyValuePair<string, string>("CCD3", FormatStringToExcelNumber(clientPosition.Discount.D3)));

                sbItems.Append(ReplaceValues(headerValues, OnlinePortfolioResources.PositionHeader));
                bool even = true;

                if (clientPosition.Markets != null && clientPosition.Markets.Count > 0)
                {
                    foreach (PortfolioPositionMarket market in clientPosition.Markets)
                    {
                        sbItems.Append(OnlinePortfolioResources.PositionMarketHeader.Replace("#Market#", HttpUtility.HtmlEncode(market.Name.ToUpper())));
                        foreach (PortfolioPositionItem i in market.Items)
                        {
                            bool notOnlySector = market.Name.ToLower() != "a vista" && market.Name.ToLower() != "short" && market.Name.ToLower() != "l&s";

                            string item = string.Format(even ? OnlinePortfolioResources.PositionMarketEven : OnlinePortfolioResources.PositionMarketOdd,
                                                            i.StockCode,
                                                            i.BuyDate,
                                                            i.Quantity.ToString("N0", cultureInfo),
                                                            i.BuyValue.ToString("N2", cultureInfo),
                                                            i.BuyNetValue.ToString("N2", cultureInfo),
                                                            i.SellValue.ToString("N2", cultureInfo),
                                                            i.SellNetValue.ToString("N2", cultureInfo),
                                                            i.ProfitLoss.ToString("N2", cultureInfo),
                                                            i.Percentage.ToString("N2", cultureInfo),
                                                            notOnlySector ? i.DueDate : "",
                                                            (notOnlySector && (market.Name.ToLower() != "financiamento" && market.Name.ToLower() != "opções")) ? i.RollOverDate : "",
                                                             HttpUtility.HtmlEncode(i.Sector),
                                                             market.Name.ToLower() != "short" ? i.BuyValue.ToString("N2", cultureInfo) : i.SellValue.ToString("N2", cultureInfo));
                            sbItems.Append(item);
                            even = !even;
                        }
                    }
                }
                sbItems.Append(OnlinePortfolioResources.PositionMarketFooter);

                even = true;

                if (clientPosition.Chart != null)
                {
                    sbItems.Append(OnlinePortfolioResources.PositionSectorHeader);

                    foreach (PortfolioPositionChartItem c in clientPosition.Chart)
                    {
                        string item = string.Format(even ? OnlinePortfolioResources.PositionSectorEven : OnlinePortfolioResources.PositionSectorOdd,
                                                   HttpUtility.HtmlEncode(c.SectorName),
                                                        FormatStringToExcelNumber(c.Allocation),
                                                        FormatStringToExcelNumber(c.Variation));
                        sbItems.Append(item);
                        even = !even;
                    }
                    sbItems.Append(OnlinePortfolioResources.PositionSectorFooter);
                }

                sbItems.Append(OnlinePortfolioResources.PositionFooter);

            }

            string xml = sbItems.ToString();
            XmlDocument xmlReport = new XmlDocument();
            xmlReport.LoadXml(xml);

            MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(xml));
            msExcel.Position = 0;
            msExcel.Flush();

            return msExcel;
        }

        public MemoryStream CreatePortfolioContactsToMemoryStream(PortfolioContact clientContacts)
        {
            string headerXml = string.Empty;
            StringBuilder sb = new StringBuilder();

            if (clientContacts != null)
            {
                headerXml = OnlinePortfolioResources.ContactHeader;
                headerXml = headerXml.Replace("#Author#", "VotoWeb");
                headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", cultureInfo));

                bool even = true;


                sb.Append(headerXml);

                sb.Append(string.Format(OnlinePortfolioResources.ContactWorksheetHeader, string.Concat(clientContacts.Client.Value, "  ", HttpUtility.HtmlEncode(clientContacts.Client.Description)), string.Concat(clientContacts.ClientPhone == "()" ? "-" : clientContacts.ClientPhone, "  ", clientContacts.ClientEmail)));
                foreach (PortfolioContactItem i in clientContacts.Contacts)
                {
                    string itemXml = even ? OnlinePortfolioResources.ContactWorksheetItemEven : OnlinePortfolioResources.ContactWorksheetItemOdd;
                    itemXml = string.Format(itemXml, i.ContactDate, HttpUtility.HtmlEncode(i.Operator.Description), HttpUtility.HtmlEncode(i.Contact));
                    sb.Append(itemXml);
                    even = !even;
                }
                sb.Append(OnlinePortfolioResources.ContactWorksheetFooter);

                sb.Append(OnlinePortfolioResources.ContactFooter);
            }

            MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(sb.ToString()));
            msExcel.Position = 0;
            msExcel.Flush();

            return msExcel;
        }

        public MemoryStream CreateAdminPositionToMemoryStream(PortfolioPosition position)
        {
            StringBuilder sbExcel = new StringBuilder();

            if (position != null)
            {
                string headerXml = OnlinePortfolioResources.AdminPositionHeader;
                headerXml = headerXml.Replace("#Author#", "VotoWeb");
                headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"));

                bool even = true;

                sbExcel.Append(headerXml);

                foreach (PortfolioPositionItem item in position.Details)
                {
                    string itemXml = even ? OnlinePortfolioResources.AdminPositionItemEven : OnlinePortfolioResources.AdminPositionItemOdd;

                    itemXml = string.Format(itemXml,
                                            item.StockCode,
                                            item.Operation,
                                            item.MarketType,
                                            item.LongShort ? "L & S" : "-",
                                            item.Quantity.ToString("N0", cultureInfo),
                                            item.Price.ToString("N2", cultureInfo),
                                            item.NetPrice.ToString("N2", cultureInfo),
                                            !item.PortfolioNumber.Equals(0) ? item.PortfolioNumber.ToString() : "-",
                                            item.PositionDate.ToShortDateString(),
                                            !string.IsNullOrEmpty(item.DueDate) ? item.DueDate : "-",
                                            !string.IsNullOrEmpty(item.RollOverDate) ? item.RollOverDate : "-");
                    sbExcel.Append(itemXml);
                    even = !even;

                    //sbExcel.Append(GuaranteesResources.WorksheetFooter);
                }
                sbExcel.Append(OnlinePortfolioResources.AdminPositionFooter);
            }
            MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(sbExcel.ToString()));
            msExcel.Position = 0;
            msExcel.Flush();

            return msExcel;
        }

        public MemoryStream CreateAdminClientsToMemoryStream(List<PortfolioClientListItem> list)
        {
            string headerXml = string.Empty;
            string footerXml = string.Empty;
            var even = true;

            StringBuilder sbItems = new StringBuilder();

            if (list.Count > 0)
            {
                headerXml = OnlinePortfolioResources.AdminClientsHeader;
                headerXml = headerXml.Replace("#Author#", "VotoWeb");
                headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", cultureInfo));

                sbItems.Append(headerXml);

                foreach (PortfolioClientListItem client in list)
                {

                    string item = even ? OnlinePortfolioResources.AdminClientsItemEven : OnlinePortfolioResources.AdminClientsItemOdd;
                    item = string.Format(item,
                        client.ClientCode,
                        HttpUtility.HtmlEncode(client.ClientName),
                        HttpUtility.HtmlEncode(client.Operators),
                        HttpUtility.HtmlEncode(client.CalculationDescription));
                    sbItems.Append(item);
                    even = !even;
                }

                sbItems.Append(OnlinePortfolioResources.AdminClientsFooter);
            }

            string xml = sbItems.ToString();
            XmlDocument xmlReport = new XmlDocument();
            xmlReport.LoadXml(xml);

            MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(xml));
            msExcel.Position = 0;
            msExcel.Flush();

            return msExcel;
        }

        public MemoryStream CreatePortfolioHistoryToMemoryStream(PortfolioHistoryClientContent clientPosition)
        {
            string headerXml = string.Empty;

            StringBuilder sbItems = new StringBuilder();

            if (clientPosition != null)
            {
                headerXml = OnlinePortfolioResources.HistoryHeader;

                List<KeyValuePair<string, string>> headerValues = new List<KeyValuePair<string, string>>();
                headerValues.Add(new KeyValuePair<string, string>("Client", string.Concat(clientPosition.ClientCode, " ", HttpUtility.HtmlEncode(clientPosition.ClientName))));
                headerValues.Add(new KeyValuePair<string, string>("Phone", clientPosition.ClientPhone));
                headerValues.Add(new KeyValuePair<string, string>("Email", clientPosition.ClientEmail));

                sbItems.Append(ReplaceValues(headerValues, headerXml));
                bool even = true;

                foreach (PortfolioHistoryPeriod period in clientPosition.Items)
                {
                    foreach (PortfolioPositionItem i in period.Items)
                    {
                        string item = string.Format(even ? OnlinePortfolioResources.HistoryItemEven : OnlinePortfolioResources.HistoryItemOdd,
                                                        i.StockCode,
                                                        HttpUtility.HtmlEncode(i.MarketGroup),
                                                        HttpUtility.HtmlEncode(i.Sector),
                                                        i.BuyDate,
                                                        i.BuyValue.ToString("N2", cultureInfo),
                                                        i.Price.ToString("N2", cultureInfo),
                                                        i.HistoryDate.ToShortDateString(),
                                                        i.Quantity.ToString("N0", cultureInfo),
                                                        i.BuyNetValue.ToString("N2", cultureInfo),
                                                        i.SellNetValue.ToString("N2", cultureInfo),
                                                        i.ProfitLoss.ToString("N2", cultureInfo),
                                                        i.Percentage.ToString("N2", cultureInfo));
                        sbItems.Append(item);
                        even = !even;
                    }
                    sbItems.Append(string.Format(OnlinePortfolioResources.HistoryMonthSum,
                                                 period.Period,
                                                 period.Balance.Quantity.ToString("N0", cultureInfo),
                                                 period.Balance.BuyNetValue.ToString("N2", cultureInfo),
                                                 period.Balance.SellNetValue.ToString("N2", cultureInfo),
                                                 period.Balance.ProfitLoss.ToString("N2", cultureInfo),
                                                 period.Balance.Percentage.ToString("N2", cultureInfo)));
                }

                sbItems.Append(ReplaceValues(headerValues, OnlinePortfolioResources.HistoryFooter));
            }
            string xml = sbItems.ToString();
            XmlDocument xmlReport = new XmlDocument();
            xmlReport.LoadXml(xml);

            MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(xml));
            msExcel.Position = 0;
            msExcel.Flush();

            return msExcel;

        }

        #endregion

        #region Proceeds

        public MemoryStream CreateProceedsToMemoryStream(List<QX3.Portal.Contracts.DataContracts.Proceeds> list, string groupBy)
        {
            string headerXml = string.Empty;
            string itemsXml = string.Empty;
            string footerXml = string.Empty;

            if (list.Count > 0)
            {
                headerXml = MarketResources.ProceedsHeader;
                headerXml = headerXml.Replace("#Author#", "VotoWeb");
                headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", cultureInfo));

                bool even = true;
                itemsXml = string.Empty;

                List<GroupByItem> groupItems = new List<GroupByItem>();

                switch (groupBy)
                {
                    case "C":
                        groupItems = (from l in list group l by new { l.ClientCode, l.ClientName } into g select new GroupByItem { Code = g.Key.ClientCode.ToString(), Name = g.Key.ClientName }).ToList();
                        break;
                    case "A":
                        groupItems = (from l in list group l by new { l.AssessorCode, l.AssessorName } into g select new GroupByItem { Code = g.Key.AssessorCode.ToString(), Name = g.Key.AssessorName }).ToList();
                        break;
                    case "P":
                        groupItems = (from l in list group l by new { l.StockCode, l.CompanyName } into g select new GroupByItem { Code = g.Key.StockCode, Name = g.Key.CompanyName }).ToList();
                        break;
                }

                List<QX3.Portal.Contracts.DataContracts.Proceeds> subGroupItems = new List<QX3.Portal.Contracts.DataContracts.Proceeds>();

                StringBuilder sbItems = new StringBuilder();

                switch (groupBy)
                {
                    case "C":
                        foreach (GroupByItem i in groupItems)
                        {
                            string worksheetHeader = string.Format(MarketResources.ProceedsWorksheetHeader, "Papel", "ISIN", "Cod.", "Assessor");
                            itemsXml += worksheetHeader.Replace("#MainName#", string.Concat(i.Code, " - ", HttpUtility.HtmlEncode(i.Name)));

                            subGroupItems = (from g in list where g.ClientCode.Equals(Int32.Parse(i.Code)) select g).ToList();

                            foreach (QX3.Portal.Contracts.DataContracts.Proceeds p in subGroupItems)
                            {
                                string itemXml = even ? MarketResources.ProceedsWorksheetItemEven : MarketResources.ProceedsWorksheetItemOdd;
                                itemXml = string.Format(itemXml,
                                                        p.StockCode,
                                                        p.ISIN,
                                                        p.AssessorCode,
                                                        HttpUtility.HtmlEncode(p.AssessorName),
                                                        HttpUtility.HtmlEncode(p.Proceed),
                                                        p.Quantity.ToString("N4", cultureInfo),
                                                        p.GrossValue.ToString("N2", cultureInfo),
                                                        p.IR.ToString("N2", cultureInfo),
                                                        p.IRValue.ToString("N2", cultureInfo),
                                                        p.NetValue.ToString("N2", cultureInfo),
                                                        p.PaymentDate.ToString("dd/MM/yyyy"),
                                                        p.Portfolio);
                                itemsXml += itemXml;
                                even = !even;
                            }

                            itemsXml += MarketResources.ProceedsWorksheetFooter;
                        }
                        break;
                    case "A":
                        foreach (GroupByItem i in groupItems)
                        {
                            string worksheetHeader = string.Format(MarketResources.ProceedsWorksheetHeader, "Papel", "ISIN", "Cod. Clie.", "Nome do Cliente");
                            itemsXml += worksheetHeader.Replace("#MainName#", string.Concat(i.Code, " - ", HttpUtility.HtmlEncode(i.Name)));

                            subGroupItems = (from g in list where g.AssessorCode.Equals(Int32.Parse(i.Code)) select g).ToList();

                            foreach (QX3.Portal.Contracts.DataContracts.Proceeds p in subGroupItems)
                            {
                                string itemXml = even ? MarketResources.ProceedsWorksheetItemEven : MarketResources.ProceedsWorksheetItemOdd;
                                itemXml = string.Format(itemXml,
                                                        p.StockCode,
                                                        p.ISIN,
                                                        p.AssessorCode,
                                                        HttpUtility.HtmlEncode(p.AssessorName),
                                                        p.Proceed,
                                                        p.Quantity.ToString("N4", cultureInfo),
                                                        p.GrossValue.ToString("N2", cultureInfo),
                                                        p.IR.ToString("N2", cultureInfo),
                                                        p.IRValue.ToString("N2", cultureInfo),
                                                        p.NetValue.ToString("N2", cultureInfo),
                                                        p.PaymentDate.ToString("dd/MM/yyyy"),
                                                        p.Portfolio);
                                itemsXml += itemXml;
                                even = !even;
                            }

                            itemsXml += MarketResources.ProceedsWorksheetFooter;
                        }
                        break;
                    case "P":
                        foreach (GroupByItem i in groupItems)
                        {
                            string worksheetHeader = string.Format(MarketResources.ProceedsWorksheetHeader, "Cod.", "Assessor", "Cod. Clie.", "Nome do Cliente");
                            itemsXml += worksheetHeader.Replace("#MainName#", i.Code);

                            subGroupItems = (from g in list where g.StockCode.Equals(i.Code) select g).ToList();

                            foreach (QX3.Portal.Contracts.DataContracts.Proceeds p in subGroupItems)
                            {
                                string itemXml = even ? MarketResources.ProceedsWorksheetItemEven : MarketResources.ProceedsWorksheetItemOdd;
                                itemXml = string.Format(itemXml,
                                                        p.StockCode,
                                                        p.ISIN,
                                                        p.AssessorCode,
                                                        HttpUtility.HtmlEncode(p.AssessorName),
                                                        p.Proceed,
                                                        p.Quantity.ToString("N4", cultureInfo),
                                                        p.GrossValue.ToString("N2", cultureInfo),
                                                        p.IR.ToString("N2", cultureInfo),
                                                        p.IRValue.ToString("N2", cultureInfo),
                                                        p.NetValue.ToString("N2", cultureInfo),
                                                        p.PaymentDate.ToString("dd/MM/yyyy"),
                                                        p.Portfolio);
                                itemsXml += itemXml;
                                even = !even;
                            }

                            itemsXml += MarketResources.ProceedsWorksheetFooter;
                        }
                        break;
                }
                footerXml = MarketResources.ProceedsFooter;
            }

            string xml = string.Concat(headerXml, itemsXml, footerXml);
            XmlDocument xmlReport = new XmlDocument();
            xmlReport.LoadXml(xml);

            MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(xml));
            msExcel.Position = 0;
            msExcel.Flush();

            return msExcel;
        }

        #endregion

        #region Term

        public MemoryStream CreateTermToMemoryStream(List<TermListItem> list, string groupBy)
        {
            string headerXml = string.Empty;
            string itemsXml = string.Empty;
            string footerXml = string.Empty;

            if (list.Count > 0)
            {
                headerXml = TermResources.Header;
                headerXml = headerXml.Replace("#Author#", "VotoWeb");
                headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", cultureInfo));

                bool even = true;
                itemsXml = string.Empty;

                List<GroupByItem> groupItems = new List<GroupByItem>();

                switch (groupBy)
                {
                    case "C":
                        groupItems = (from l in list group l by new { l.Code, l.Client } into g select new GroupByItem { Code = g.Key.Code.ToString(), Name = g.Key.Client }).ToList();
                        break;
                    case "A":
                        groupItems = (from l in list group l by new { l.AssessorCode, l.Assessor } into g select new GroupByItem { Code = g.Key.AssessorCode.ToString(), Name = g.Key.Assessor }).ToList();
                        break;
                    case "P":
                        groupItems = (from l in list group l by new { l.Stock } into g select new GroupByItem { Code = g.Key.Stock }).ToList();
                        break;
                }

                List<TermListItem> subGroupItems = new List<TermListItem>();

                StringBuilder sbItems = new StringBuilder();

                switch (groupBy)
                {
                    case "C":
                        foreach (GroupByItem i in groupItems)
                        {
                            string worksheetHeader = string.Format(TermResources.WorksheetHeader, "Assessor", "C&#243;digo", "Papel", "Empresa");

                            string name = string.Concat(i.Code, " - ", i.Name);
                            name = name.Length > 25 ? name.Substring(0, 25) : name;
                            itemsXml += worksheetHeader.Replace("#MainName#", HttpUtility.HtmlEncode(name));

                            subGroupItems = (from g in list where g.Code.Equals(i.Code) select g).ToList();

                            foreach (TermListItem p in subGroupItems)
                            {
                                string itemXml = even ? TermResources.WorksheetItemEven : TermResources.WorksheetItemOdd;
                                itemXml = string.Format(itemXml,
                                                        HttpUtility.HtmlEncode(p.Assessor),
                                                        p.AssessorCode,
                                                        p.Stock,
                                                        HttpUtility.HtmlEncode(p.Company),
                                                        p.Contract,
                                                        p.AvailableQuantity.ToString(cultureInfo),
                                                        p.TotalQuantity.ToString(cultureInfo),
                                                        p.SettleQuantity.ToString(cultureInfo),
                                                        p.TypeDescription,
                                                        p.SettleDate.HasValue ? p.SettleDate.Value.ToString("dd/MM/yyyy") : "-",
                                                        p.StatusDescription,
                                                        p.RolloverDate.HasValue ? p.RolloverDate.Value.ToString("dd/MM/yyyy") : "-");
                                itemsXml += itemXml;
                                even = !even;
                            }

                            itemsXml += TermResources.WorksheetFooter;
                        }
                        break;
                    case "A":
                        foreach (GroupByItem i in groupItems)
                        {
                            string worksheetHeader = string.Format(TermResources.WorksheetHeader, "Cliente", "C&#243;digo", "Papel", "Empresa");

                            string name = string.Concat(i.Code, " - ", i.Name);
                            name = name.Length > 25 ? name.Substring(0, 25) : name;
                            itemsXml += worksheetHeader.Replace("#MainName#", HttpUtility.HtmlEncode(name));

                            subGroupItems = (from g in list where g.AssessorCode.Equals(i.Code) select g).ToList();

                            foreach (TermListItem p in subGroupItems)
                            {
                                string itemXml = even ? TermResources.WorksheetItemEven : TermResources.WorksheetItemOdd;
                                itemXml = string.Format(itemXml,
                                                        HttpUtility.HtmlEncode(p.Client),
                                                        p.Code,
                                                        p.Stock,
                                                        HttpUtility.HtmlEncode(p.Company),
                                                        p.Contract,
                                                        p.AvailableQuantity.ToString(cultureInfo),
                                                        p.TotalQuantity.ToString(cultureInfo),
                                                        p.SettleQuantity.ToString(cultureInfo),
                                                        p.TypeDescription,
                                                        p.SettleDate.HasValue ? p.SettleDate.Value.ToString("dd/MM/yyyy") : "-",
                                                        p.StatusDescription,
                                                        p.RolloverDate.HasValue ? p.RolloverDate.Value.ToString("dd/MM/yyyy") : "-");
                                itemsXml += itemXml;
                                even = !even;
                            }

                            itemsXml += TermResources.WorksheetFooter;
                        }
                        break;
                    case "P":
                        foreach (GroupByItem i in groupItems)
                        {
                            string worksheetHeader = string.Format(TermResources.WorksheetHeader, "Cliente", "C&#243;digo", "Assessor", "C&#243;d. Assessor");
                            itemsXml += worksheetHeader.Replace("#MainName#", i.Code);

                            subGroupItems = (from g in list where g.Stock.Equals(i.Code) select g).ToList();

                            foreach (TermListItem p in subGroupItems)
                            {
                                string itemXml = even ? TermResources.WorksheetItemEven : TermResources.WorksheetItemOdd;
                                itemXml = string.Format(itemXml,
                                                        HttpUtility.HtmlEncode(p.Client),
                                                        p.Code,
                                                        HttpUtility.HtmlEncode(p.Assessor),
                                                        p.AssessorCode,
                                                        p.Contract,
                                                        p.AvailableQuantity.ToString(cultureInfo),
                                                        p.TotalQuantity.ToString(cultureInfo),
                                                        p.SettleQuantity.ToString(cultureInfo),
                                                        p.TypeDescription,
                                                        p.SettleDate.HasValue ? p.SettleDate.Value.ToString("dd/MM/yyyy") : "-",
                                                        p.StatusDescription,
                                                        p.RolloverDate.HasValue ? p.RolloverDate.Value.ToString("dd/MM/yyyy") : "-");
                                itemsXml += itemXml;
                                even = !even;
                            }

                            itemsXml += TermResources.WorksheetFooter;
                        }
                        break;
                }
                footerXml = TermResources.Footer;
            }

            string xml = string.Concat(headerXml, itemsXml, footerXml);
            XmlDocument xmlReport = new XmlDocument();
            xmlReport.LoadXml(xml);

            MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(xml));
            msExcel.Position = 0;
            msExcel.Flush();

            return msExcel;
        }

        #endregion

        #region Risk

        public MemoryStream CreateGuaranteesToMemoryStream(List<GuaranteeListItem> list)
        {
            StringBuilder sbExcel = new StringBuilder();

            if (list.Count > 0)
            {
                string headerXml = GuaranteesResources.Header;
                headerXml = headerXml.Replace("#Author#", "VotoWeb");
                headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"));

                bool even = true;

                sbExcel.Append(headerXml);

                if (list.Count > 40)
                    list = list.Take(40).ToList();

                foreach (GuaranteeListItem i in list)
                {
                    string worksheetHeader = GuaranteesResources.WorksheetHeader.Replace("#Client#", string.Concat(i.Client.Value, " ", i.Client.Description)).Replace("#Assessor#", i.Assessor);
                    sbExcel.Append(worksheetHeader);

                    foreach (GuaranteeListSubItem s in i.Items)
                    {
                        bool inGuarantee = s.Status.Value == "-1";
                        bool isMoney = s.Guarantee.ToLower().IndexOf("dinheiro") >= 0;

                        string itemXml = even ? GuaranteesResources.WorksheetItemEven : GuaranteesResources.WorksheetItemOdd;
                        itemXml = string.Format(itemXml,
                                                inGuarantee ? "Em garantia" : s.IsWithdrawal ? "Retirada" : "Dep&#243;sito",
                                                HttpUtility.HtmlEncode(s.Guarantee),
                                                isMoney ? "-" : s.IsBovespa ? "Bovespa" : "BM&#38;F",
                                                s.UpdateDate.ToString("dd/MM/yyyy"),
                                                isMoney ? "" : s.Quantity.ToString("N0", cultureInfo),
                                                isMoney ? "" : s.UnitPrice.ToString("N2", cultureInfo),
                                                s.Total.ToString("N2", cultureInfo),
                                                s.Discount.ToString("N2", cultureInfo),
                                                s.RequestedValue.ToString("N2", cultureInfo),
                                                s.Status.Description);
                        sbExcel.Append(itemXml);
                        even = !even;
                    }
                    sbExcel.Append(GuaranteesResources.WorksheetFooter);
                }
                sbExcel.Append(GuaranteesResources.Footer);
            }
            MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(sbExcel.ToString()));
            msExcel.Position = 0;
            msExcel.Flush();

            return msExcel;
        }

        #endregion

        #region Subscription

        public MemoryStream CreateSubscriptionsToMemoryStream(List<Subscription> list)
        {
            string headerXml = string.Empty;
            string footerXml = string.Empty;

            StringBuilder sbItems = new StringBuilder();

            if (list.Count > 0)
            {
                headerXml = SubscriptionResources.SubscriptionHeader;
                headerXml = headerXml.Replace("#Author#", "VotoWeb");
                headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", cultureInfo));

                sbItems.Append(headerXml);

                foreach (Subscription li in list)
                {
                    string item = string.Format(SubscriptionResources.SubscriptionItem,
                                                HttpUtility.HtmlEncode(li.Company),
                                                HttpUtility.HtmlEncode(li.StockCode),
                                                HttpUtility.HtmlEncode(li.RequestId.ToString()),
                                                HttpUtility.HtmlEncode(li.InitialDate.HasValue ? li.InitialDate.Value.ToShortDateString() : "-"),
                                                HttpUtility.HtmlEncode(li.COMDate.HasValue ? li.COMDate.Value.ToShortDateString() : "-"),
                                                HttpUtility.HtmlEncode((li.BrokerDate.HasValue ? li.BrokerDate.Value.ToShortDateString() : "-") + (li.BrokerHour.HasValue ? " - " + li.BrokerHour.Value.ToShortTimeString() : "")),
                                                HttpUtility.HtmlEncode(li.StockExchangeDate.HasValue ? li.StockExchangeDate.Value.ToShortDateString() : "-"),
                                                HttpUtility.HtmlEncode(li.CompanyDate.HasValue ? li.CompanyDate.Value.ToShortDateString() : "-"),
                                                HttpUtility.HtmlEncode(li.DescStatus)
                    );
                    sbItems.Append(item);
                }

                sbItems.Append(SubscriptionResources.SubscriptionFooter);
            }

            string xml = sbItems.ToString();
            XmlDocument xmlReport = new XmlDocument();
            xmlReport.LoadXml(xml);

            MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(xml));
            msExcel.Position = 0;
            msExcel.Flush();

            return msExcel;
        }

        public MemoryStream CreateRequestRightsToMemoryStream(Subscription subscription, List<SubscriptionRights> list)
        {
            string headerXml = string.Empty;
            string headerSubscriptionXml = string.Empty;
            string footerXml = string.Empty;

            StringBuilder sbItems = new StringBuilder();

            if (list.Count > 0)
            {
                headerXml = SubscriptionResources.RequestRightsHeader;
                headerXml = headerXml.Replace("#Author#", "VotoWeb");
                headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", cultureInfo));

                sbItems.Append(headerXml);

                headerSubscriptionXml = SubscriptionResources.RequestRightsSubscriptionHeader;
                headerSubscriptionXml = string.Format(headerSubscriptionXml,
                    subscription.Company,
                    subscription.StockCode,
                    subscription.RightPercentual.Value.ToString("N2"),
                    subscription.ISINStock,
                    subscription.InitialDate.Value.ToShortDateString(),
                    subscription.StockExchangeDate.Value.ToShortDateString());

                sbItems.Append(headerSubscriptionXml);

                foreach (SubscriptionRights li in list)
                {
                    string item = string.Format(SubscriptionResources.RequestRightsItem,
                                                HttpUtility.HtmlEncode(li.ClientName),
                                                HttpUtility.HtmlEncode(li.ClientCode),
                                                HttpUtility.HtmlEncode(li.AssessorName),
                                                HttpUtility.HtmlEncode(li.RightsQtty.Value),
                                                HttpUtility.HtmlEncode(li.RequestedQtty.Value),
                                                HttpUtility.HtmlEncode(li.AvailableQtty.Value),
                                                HttpUtility.HtmlEncode(li.RightValue.Value.ToString().Replace(",", ".")),
                                                HttpUtility.HtmlEncode(li.TotalValue.Value.ToString().Replace(",", ".")),
                                                HttpUtility.HtmlEncode(li.StockExchangeDate.Value.ToShortDateString()),
                                                HttpUtility.HtmlEncode(li.DescScraps),
                                                HttpUtility.HtmlEncode(li.DescWarning),
                                                HttpUtility.HtmlEncode(li.DescStatus)
                    );
                    sbItems.Append(item);
                }

                sbItems.Append(SubscriptionResources.RequestRightsFooter);
            }

            string xml = sbItems.ToString();
            XmlDocument xmlReport = new XmlDocument();
            xmlReport.LoadXml(xml);

            MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(xml));
            msExcel.Position = 0;
            msExcel.Flush();

            return msExcel;
        }

        #endregion

        #region Funds

        //public MemoryStream CreateFundsBalanceToMemoryStream(
        //    FundClient client,
        //    FundBalance balance,
        //    List<FundBalancePendingApplicationItem> pending,
        //    List<FundBalanceChartItemResponse> chart)
        //{
        //    string headerXml = string.Empty;
        //    string footerXml = string.Empty;

        //    StringBuilder sbItems = new StringBuilder();

        //    headerXml = FundsResources.FundsBalanceHeader;
        //    headerXml = headerXml.Replace("#Author#", "VotoWeb");
        //    headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", cultureInfo));
        //    sbItems.Append(headerXml);

        //    #region Fundos - Fundo selecionado

        //    string balanceXml = string.Empty;
        //    bool even1 = true;

        //    if (balance.Items.Count() > 0)
        //    {
        //        balanceXml = FundsResources.FundsBalanceSelectedHeader;
        //        balanceXml = balanceXml.Replace("#SheetFundName#", balance.FundName.Length > 31 ? balance.FundName.Substring(31) : balance.FundName);
        //        balanceXml = balanceXml.Replace("#FundName#", balance.FundName);
        //        sbItems.Append(balanceXml);

        //        foreach (var li in balance.Items)
        //        {
        //            string item = string.Format(even1 ? FundsResources.FundsBalanceSelectedItemEven : FundsResources.FundsBalanceSelectedItemOdd,
        //                                        HttpUtility.HtmlEncode(li.CautionNumber),
        //                                        HttpUtility.HtmlEncode(FormatStringToExcelNumber(li.QuantityOfQuota.ToString())),
        //                                        HttpUtility.HtmlEncode(li.ApplicationDate.ToShortDateString()),
        //                                        HttpUtility.HtmlEncode(li.ConversionDate.ToShortDateString()),
        //                                        HttpUtility.HtmlEncode(FormatStringToExcelNumber(li.ApplicationValue.ToString())),
        //                                        HttpUtility.HtmlEncode(FormatStringToExcelNumber(li.CorrectedValue.ToString())),
        //                                        HttpUtility.HtmlEncode(FormatStringToExcelNumber(li.ProvisionIOF.ToString())),
        //                                        HttpUtility.HtmlEncode(FormatStringToExcelNumber(li.ProvisionIRRF.ToString())),
        //                                        HttpUtility.HtmlEncode(FormatStringToExcelNumber(li.RedemptionValue.ToString())),
        //                                        HttpUtility.HtmlEncode(FormatStringToExcelNumber(li.Income.ToString()))
        //            );
        //            sbItems.Append(item);
        //            even1 = !even1;
        //        }

        //        sbItems.Append(FundsResources.FundsBalanceSheetFooter);
        //    }

        //    #endregion

        //    #region Fundos - Movimentações pendentes de confirmação

        //    string pendingXml = string.Empty;
        //    bool even2 = true;

        //    if (pending.Count() > 0)
        //    {
        //        pendingXml = FundsResources.FundsBalancePendingHeader;
        //        sbItems.Append(pendingXml);

        //        foreach (var li in pending)
        //        {
        //            string item = string.Format(even2 ? FundsResources.FundsBalancePendingItemEven : FundsResources.FundsBalancePendingItemOdd,
        //                HttpUtility.HtmlEncode(li.FundName),
        //                HttpUtility.HtmlEncode(FormatStringToExcelNumber(li.Value.ToString())),
        //                HttpUtility.HtmlEncode(li.Type)
        //            );
        //            sbItems.Append(item);
        //            even2 = !even2;
        //        }

        //        sbItems.Append(FundsResources.FundsBalanceSheetFooter);
        //    }

        //    #endregion

        //    #region Fundos - Distribuição dos Investimentos

        //    string chartXml = string.Empty;
        //    bool even3 = true;

        //    if (chart.Count() > 0)
        //    {
        //        chartXml = FundsResources.FundsBalanceChartHeader;
        //        sbItems.Append(chartXml);

        //        foreach (var li in chart)
        //        {
        //            string item = string.Format(even3 ? FundsResources.FundsBalanceChartItemEven : FundsResources.FundsBalanceChartItemOdd,
        //                HttpUtility.HtmlEncode(li.FundName),
        //                HttpUtility.HtmlEncode(li.Composition),
        //                HttpUtility.HtmlEncode(FormatStringToExcelNumber(li.Percentage)),
        //                HttpUtility.HtmlEncode(FormatStringToExcelNumber(li.Value.ToString()))
        //            );
        //            sbItems.Append(item);
        //            even3 = !even3;
        //        }

        //        sbItems.Append(FundsResources.FundsBalanceSheetFooter);
        //    }

        //    #endregion

        //    sbItems.Append(FundsResources.FundsBalanceFooter);

        //    string xml = sbItems.ToString();
        //    XmlDocument xmlReport = new XmlDocument();
        //    xmlReport.LoadXml(xml);

        //    MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(xml));
        //    msExcel.Position = 0;
        //    msExcel.Flush();

        //    return msExcel;
        //}

        //public MemoryStream CreateFundsAnalyticalStatementToMemoryStream(FundClient client, List<FundStatementItem> statement)
        //{
        //    string headerXml = string.Empty;
        //    string footerXml = string.Empty;

        //    StringBuilder sbItems = new StringBuilder();

        //    headerXml = FundsResources.FundsAnalyticalStatementHeader;
        //    headerXml = headerXml.Replace("#Author#", "VotoWeb");
        //    headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", cultureInfo));
        //    sbItems.Append(headerXml);

        //    bool even1 = true;

        //    if (statement.Count > 0)
        //    {
        //        foreach (var li in statement)
        //        {
        //            string item = string.Format(even1 ? FundsResources.FundsAnalyticalStatementItemEven : FundsResources.FundsAnalyticalStatementItemOdd,
        //                                        HttpUtility.HtmlEncode(li.ApplicationDate.ToShortDateString()),
        //                                        HttpUtility.HtmlEncode(li.MovementDate.ToShortDateString()),
        //                                        HttpUtility.HtmlEncode(li.MovementType),
        //                                        HttpUtility.HtmlEncode(li.CautionNumber),
        //                                        HttpUtility.HtmlEncode(FormatStringToExcelNumber(li.QuantityOfQuota.ToString())),
        //                                        HttpUtility.HtmlEncode(FormatStringToExcelNumber(li.OriginalValue.ToString())),
        //                                        HttpUtility.HtmlEncode(FormatStringToExcelNumber(li.GrossValue.ToString())),
        //                                        HttpUtility.HtmlEncode(QX3.Portal.WebSite.Helper.FundHelper.TaxFormatter(li.IOF, li.OriginalValue)),
        //                                        HttpUtility.HtmlEncode(QX3.Portal.WebSite.Helper.FundHelper.TaxFormatter(li.IRRF, li.OriginalValue)),
        //                                        HttpUtility.HtmlEncode(FormatStringToExcelNumber(li.Value.ToString()))
        //            );
        //            sbItems.Append(item);
        //            even1 = !even1;
        //        }
        //    }

        //    sbItems.Append(FundsResources.FundsAnalyticalStatementFooter);

        //    string xml = sbItems.ToString();
        //    XmlDocument xmlReport = new XmlDocument();
        //    xmlReport.LoadXml(xml);

        //    MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(xml));
        //    msExcel.Position = 0;
        //    msExcel.Flush();

        //    return msExcel;
        //}

        //public MemoryStream CreateFundsSyntheticStatementToMemoryStream(FundClient client, List<FundStatementItem> statement)
        //{
        //    string headerXml = string.Empty;
        //    string footerXml = string.Empty;

        //    StringBuilder sbItems = new StringBuilder();

        //    headerXml = FundsResources.FundsSyntheticStatementHeader;
        //    headerXml = headerXml.Replace("#Author#", "VotoWeb");
        //    headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", cultureInfo));
        //    sbItems.Append(headerXml);

        //    DateTime currentDate = DateTime.MinValue;
        //    decimal sum = 0;

        //    bool even1 = true;

        //    if (statement.Count > 0)
        //    {
        //        foreach (var li in statement)
        //        {
        //            if (currentDate == DateTime.MinValue)
        //            {
        //                string item = string.Format(FundsResources.FundsSyntheticStatementItemBalance,
        //                                            HttpUtility.HtmlEncode(li.ApplicationDate.AddDays(-1).ToShortDateString()),
        //                                            HttpUtility.HtmlEncode("Saldo Anterior"),
        //                                            HttpUtility.HtmlEncode(FormatStringToExcelNumber(0.ToString()))
        //                );
        //                sbItems.Append(item);
        //            }

        //            //if (currentDate != DateTime.MinValue && currentDate != li.ApplicationDate)
        //            //{
        //            //    string item1 = string.Format(FundsResources.FundsSyntheticStatementItemBalance,
        //            //                                HttpUtility.HtmlEncode(currentDate.ToShortDateString()),
        //            //                                HttpUtility.HtmlEncode("Saldo"),
        //            //                                HttpUtility.HtmlEncode(FormatStringToExcelNumber(sum.ToString()))
        //            //    );
        //            //    sbItems.Append(item1);
        //            //}

        //            //string item2 = string.Format(even1 ? FundsResources.FundsSyntheticStatementItemEven : FundsResources.FundsSyntheticStatementItemOdd,
        //            //                            HttpUtility.HtmlEncode(li.ApplicationDate.ToShortDateString()),
        //            //                            HttpUtility.HtmlEncode(li.MovementType),
        //            //                            HttpUtility.HtmlEncode("D+0"),
        //            //                            HttpUtility.HtmlEncode(QX3.Portal.WebSite.Helper.FundHelper.TaxFormatter(li.IOF, li.OriginalValue)),
        //            //                            HttpUtility.HtmlEncode(QX3.Portal.WebSite.Helper.FundHelper.TaxFormatter(li.IRRF, li.OriginalValue)),
        //            //                            HttpUtility.HtmlEncode(FormatStringToExcelNumber(li.Value.ToString()))
        //            //);
        //            sbItems.Append(item2);

        //            if (li.MovementType.ToLower().IndexOf("resgate") >= 0)
        //            {
        //                sum -= li.Value;
        //            }
        //            else
        //            {
        //                sum += li.Value;
        //            }

        //            currentDate = li.ApplicationDate;

        //            if (li.CautionNumber == statement[statement.Count - 1].CautionNumber)
        //            {
        //                string item3 = string.Format(FundsResources.FundsSyntheticStatementItemBalance,
        //                    HttpUtility.HtmlEncode(currentDate.ToShortDateString()),
        //                    HttpUtility.HtmlEncode("Saldo"),
        //                    HttpUtility.HtmlEncode(FormatStringToExcelNumber(sum.ToString()))
        //                );
        //                sbItems.Append(item3);
        //            }

        //            even1 = !even1;
        //        }
        //    }

        //    sbItems.Append(FundsResources.FundsSyntheticStatementFooter);

        //    string xml = sbItems.ToString();
        //    XmlDocument xmlReport = new XmlDocument();
        //    xmlReport.LoadXml(xml);

        //    MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(xml));
        //    msExcel.Position = 0;
        //    msExcel.Flush();

        //    return msExcel;
        //}

        #endregion

        #region BMF

        public MemoryStream CreateBrokerageReportsToMemoryStream(BrokerageTransferInfo model, List<int> clientList, List<CPClientInformation> clientInfo)
        {
            StringBuilder sbExcel = new StringBuilder();

            if ((model.BMFInfo != null && model.BMFInfo.Count > 0)
                || (model.BovespaInfo != null && model.BovespaInfo.Count > 0))
            {
                string headerXml = BMFResources.Header;
                headerXml = headerXml.Replace("#Author#", "VotoWeb");
                headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"));

                bool even = true;

                sbExcel.Append(headerXml);

                foreach (var i in clientList)
                {
                    var client = clientInfo.Where(a => a != null).Where(a => a.ClientCode == i).FirstOrDefault();
                    var bovespaList = (model.BovespaInfo == null) ? null : model.BovespaInfo.Where(a => a.ClientCode == i).ToList();
                    var bmfList = (model.BMFInfo == null) ? null : model.BMFInfo.Where(a => a.ClientCode == i).ToList();

                    string worksheetHeader = BMFResources.HeaderWorksheet;
                    if (client != null && client.Client != null)
                        worksheetHeader = BMFResources.HeaderWorksheet.Replace("#Client#",
                                string.Concat(HttpUtility.HtmlEncode(client.Client.Value), " ", HttpUtility.HtmlEncode(client.Client.Description)))
                                .Replace("#Assessor#", HttpUtility.HtmlEncode(client.AssessorName));
                    else
                        worksheetHeader = worksheetHeader.Replace("#Client#", i.ToString()).Replace("#Assessor#", "");
                    sbExcel.Append(worksheetHeader);

                    //BMF

                    if (bmfList != null && bmfList.Count > 0)
                    {
                        sbExcel.Append(BMFResources.HeaderBMF);

                        foreach (var bmf in bmfList)
                        {
                            string itemXml = even ? BMFResources.ItemEvenBMF : BMFResources.ItemOddBMF;

                            itemXml = string.Format(itemXml,
                                       HttpUtility.HtmlEncode(bmf.Broker),
                                       HttpUtility.HtmlEncode(bmf.Product),
                                       HttpUtility.HtmlEncode((bmf.GrossBrokerage.HasValue) ? bmf.GrossBrokerage.Value.ToString("N2") : "-"),
                                       HttpUtility.HtmlEncode((bmf.TransferValue.HasValue) ? bmf.TransferValue.Value.ToString("N2") : "-"),
                                       HttpUtility.HtmlEncode((bmf.TransferPercentual.HasValue) ? bmf.TransferPercentual.Value.ToString("N2") : "-"),
                                       HttpUtility.HtmlEncode((bmf.Revenue.HasValue) ? bmf.Revenue.Value.ToString("N2") : "-")
                                );
                            sbExcel.Append(itemXml);
                            even = !even;
                        }

                        var footerBMF = BMFResources.FooterBMF;

                        footerBMF = string.Format(footerBMF,
                             HttpUtility.HtmlEncode(bmfList.Sum(a => a.GrossBrokerage).HasValue ? bmfList.Sum(a => a.GrossBrokerage).Value.ToString("N2") : "-"),
                             HttpUtility.HtmlEncode(bmfList.Sum(a => a.TransferValue).HasValue ? bmfList.Sum(a => a.TransferValue).Value.ToString("N2") : "-"),
                             HttpUtility.HtmlEncode(bmfList.Sum(a => a.TransferPercentual).HasValue ? bmfList.Sum(a => a.TransferPercentual).Value.ToString("N2") : "-"),
                             HttpUtility.HtmlEncode(bmfList.Sum(a => a.Revenue).HasValue ? bmfList.Sum(a => a.Revenue).Value.ToString("N2") : "-"));

                        sbExcel.Append(footerBMF);
                    }

                    //BOVESPA

                    if (bovespaList != null && bovespaList.Count > 0)
                    {
                        sbExcel.Append(BMFResources.HeaderBovespa);

                        foreach (var bovespa in bovespaList)
                        {
                            string itemXml = even ? BMFResources.ItemEvenBovespa : BMFResources.ItemOddBovespa;

                            itemXml = string.Format(itemXml,
                                       HttpUtility.HtmlEncode((bovespa.GrossBrokerage.HasValue) ? bovespa.GrossBrokerage.Value.ToString("N2") : "-"),
                                       HttpUtility.HtmlEncode((bovespa.NetBrokerage.HasValue) ? bovespa.NetBrokerage.Value.ToString("N2") : "-"),
                                       HttpUtility.HtmlEncode((bovespa.DiscountValue.HasValue) ? bovespa.DiscountValue.Value.ToString("N2") : "-")

                                );
                            sbExcel.Append(itemXml);
                            even = !even;
                        }
                    }

                    sbExcel.Append(BMFResources.WorksheetFooter);
                }
                sbExcel.Append(GuaranteesResources.Footer);
            }
            MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(sbExcel.ToString()));
            msExcel.Position = 0;
            msExcel.Flush();

            return msExcel;
        }

        public MemoryStream CreateBrokerageRangeToMemoryStream(BrokerageRange model)
        {
            StringBuilder sbExcel = new StringBuilder();

            string headerXml = BMFResources.BrokerageRangeHeader;
            headerXml = headerXml.Replace("#Author#", "VotoWeb");
            headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"));

            bool even = true;

            headerXml = headerXml.Replace("#Client#", HttpUtility.HtmlEncode(model.ClientCode + " " + model.ClientName)).Replace("#Assessor#", HttpUtility.HtmlEncode(model.AssessorName));

            sbExcel.Append(headerXml);

            //BOVESPA

            if (model.Bovespa != null && model.Bovespa.Count > 0)
            {
                sbExcel.Append(BMFResources.BrokerageRangeBovespaHeader);

                foreach (var bovespa in model.Bovespa)
                {
                    string itemXml = even ? BMFResources.BrokerageRangeBovespaItemEven : BMFResources.BrokerageRangeBovespaItemOdd;

                    itemXml = string.Format(itemXml,
                                !String.IsNullOrEmpty(bovespa.MarketType) ? HttpUtility.HtmlEncode(bovespa.MarketType) : "-",
                                HttpUtility.HtmlEncode((bovespa.NormalOperationRate.HasValue) ? bovespa.NormalOperationRate.Value.ToString("N2") : "-"),
                                HttpUtility.HtmlEncode((bovespa.DayTradeRate.HasValue) ? bovespa.DayTradeRate.Value.ToString("N2") : "-")

                        );
                    sbExcel.Append(itemXml);
                    even = !even;
                }
            }

            //BMF

            if (model.BMF != null && model.BMF.Count > 0)
            {
                sbExcel.Append(BMFResources.BrokerageRangeBMFHeader);

                var itemListStockCodes = model.BMF.Select(a => a.Stock).Distinct().ToList();

                var BrokerageRangeBMFList = (from a in itemListStockCodes
                                             select new BrokerageBMF()
                                             {
                                                 Stock = a,
                                                 Quantity = model.BMF.Where(b => b.Stock == a).FirstOrDefault().Quantity,
                                                 MarketType = model.BMF.Where(b => b.Stock == a).FirstOrDefault().MarketType,
                                                 NormalOperationRate = model.BMF.Where(b => b.Stock == a).Sum(c => c.NormalOperationRate),
                                                 DayTradeRate = model.BMF.Where(b => b.Stock == a).Sum(c => c.DayTradeRate)
                                             }).ToList();

                foreach (var bmf in BrokerageRangeBMFList)
                {
                    string itemXml = even ? BMFResources.BrokerageRangeBMFItemEven : BMFResources.BrokerageRangeBMFItemOdd;

                    itemXml = string.Format(itemXml,
                                HttpUtility.HtmlEncode(bmf.Stock),
                                HttpUtility.HtmlEncode((bmf.Quantity.HasValue) ? bmf.Quantity.Value.ToString("N2") : "-"),
                                HttpUtility.HtmlEncode(bmf.MarketType),
                                HttpUtility.HtmlEncode((bmf.NormalOperationRate.HasValue) ? bmf.NormalOperationRate.Value.ToString("N2") : "-"),
                                HttpUtility.HtmlEncode((bmf.DayTradeRate.HasValue) ? bmf.DayTradeRate.Value.ToString("N2") : "-")
                        );
                    sbExcel.Append(itemXml);


                    var detailList = model.BMF.Where(a => a.Stock == bmf.Stock).ToList();

                    if (detailList != null && detailList.Count > 0)
                    {
                        sbExcel.Append(BMFResources.BrokerageRangeBMFSubItemHeader);

                        bool even2 = true;

                        foreach (var d in detailList)
                        {
                            string subItemXML = BMFResources.BrokerageRangeBMFSubItemOdd;//even2 ? BMFResources.BrokerageRangeBMFSubItemEven : BMFResources.BrokerageRangeBMFSubItemOdd;
                            //TODO: Mudar
                            subItemXML = string.Format(subItemXML,
                                    HttpUtility.HtmlEncode("-"),
                                    HttpUtility.HtmlEncode("-"),
                                    HttpUtility.HtmlEncode(d.NormalOperationRate.HasValue ? d.NormalOperationRate.Value.ToString("N2") : "-"),
                                    HttpUtility.HtmlEncode(d.DayTradeRate.HasValue ? d.DayTradeRate.Value.ToString("N2") : "-")
                            );

                            sbExcel.Append(subItemXML);
                        }
                    }

                    even = !even;
                }
            }

            sbExcel.Append(BMFResources.BrokerageRangeFooter);

            MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(sbExcel.ToString()));
            msExcel.Position = 0;
            msExcel.Flush();

            return msExcel;
        }

        #endregion

        #region ClientRegistration

        public MemoryStream CreateStatusViewerToMemoryStream(List<Client> list)
        {
            string headerXml = string.Empty;
            string footerXml = string.Empty;

            StringBuilder sbItems = new StringBuilder();

            if (list.Count > 0)
            {
                int counter = 0;
                headerXml = ClientRegistrationResources.StatusViewerHeader;
                headerXml = headerXml.Replace("#Author#", "VotoWeb");
                headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", cultureInfo));

                sbItems.Append(headerXml);

                foreach (Client listItem in list)
                {
                    string model = counter % 2 != 0 ? ClientRegistrationResources.StatusViewerItemOdd : ClientRegistrationResources.StatusViewerItemEven;
                    string item = string.Format(model,
                        HttpUtility.HtmlEncode(listItem.AccessionDate.HasValue ? listItem.AccessionDate.Value.ToShortDateString() : "-"),
                                            HttpUtility.HtmlEncode(listItem.ClientCode),
                                            HttpUtility.HtmlEncode(listItem.ClientName),
                                            HttpUtility.HtmlEncode(listItem.CPFCGC),
                                            HttpUtility.HtmlEncode(listItem.AssessorName),
                                            HttpUtility.HtmlEncode(listItem.BMFActive.HasValue ? listItem.BMFActive.Value ? "Ativo" : "Inativo" : "-"),
                                            HttpUtility.HtmlEncode(listItem.BovespaActive.HasValue ? listItem.BovespaActive.Value ? "Ativo" : "Inativo" : "-")
                            );
                    sbItems.Append(item);
                    counter++;
                }

                sbItems.Append(ClientRegistrationResources.StatusViewerFooter);
            }

            string xml = sbItems.ToString();
            XmlDocument xmlReport = new XmlDocument();
            xmlReport.LoadXml(xml);

            MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(xml));
            msExcel.Position = 0;
            msExcel.Flush();

            return msExcel;
        }

        public MemoryStream CreateOverdueAndDueToMemoryStream(List<OverdueAndDue> list)
        {
            string headerXml = string.Empty;
            string footerXml = string.Empty;

            StringBuilder sbItems = new StringBuilder();

            if (list.Count > 0)
            {
                int counter = 0;
                headerXml = ClientRegistrationResources.OverdueAndDueHeader;
                headerXml = headerXml.Replace("#Author#", "VotoWeb");
                headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", cultureInfo));

                sbItems.Append(headerXml);

                MarketHelper helper = new MarketHelper();

                foreach (OverdueAndDue listItem in list)
                {
                    string model = counter % 2 != 0 ? ClientRegistrationResources.OverdueAndDueItemOdd : ClientRegistrationResources.OverdueAndDueItemEven;
                    string item = string.Format(model,
                        HttpUtility.HtmlEncode(listItem.ClientCode),
                        HttpUtility.HtmlEncode(listItem.ClientName),
                        HttpUtility.HtmlEncode(listItem.CPFCGC),
                        HttpUtility.HtmlEncode(helper.GetAssessorName(listItem.AssessorCode)),
                        HttpUtility.HtmlEncode(listItem.RegisterType),
                        HttpUtility.HtmlEncode(listItem.SituationDesc),
                        HttpUtility.HtmlEncode(listItem.RegisterDate != null && listItem.RegisterDate != DateTime.MinValue ? listItem.RegisterDate.ToShortDateString() : "-"),
                        HttpUtility.HtmlEncode(listItem.SinacorDesc)
                    );
                    sbItems.Append(item);
                    counter++;
                }

                sbItems.Append(ClientRegistrationResources.OverdueAndDueFooter);
            }

            string xml = sbItems.ToString();
            XmlDocument xmlReport = new XmlDocument();
            xmlReport.LoadXml(xml);

            MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(xml));
            msExcel.Position = 0;
            msExcel.Flush();

            return msExcel;
        }

        public MemoryStream CreateTransferBovespaToMemoryStream(List<TransferBovespaItem> list)
        {
            string headerXml = string.Empty;
            string footerXml = string.Empty;

            StringBuilder sbItems = new StringBuilder();

            if (list.Count > 0)
            {
                int counter = 0;
                headerXml = ClientRegistrationResources.TransferBovespaHeader;
                headerXml = headerXml.Replace("#Author#", "VotoWeb");
                headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", cultureInfo));

                sbItems.Append(headerXml);

                MarketHelper helper = new MarketHelper();

                foreach (TransferBovespaItem listItem in list)
                {
                    string model = counter % 2 != 0 ? ClientRegistrationResources.TransferBovespaItemOdd : ClientRegistrationResources.TransferBovespaItemEven;
                    string item = string.Format(model,
                        HttpUtility.HtmlEncode(listItem.Client.Value),
                        HttpUtility.HtmlEncode(listItem.Client.Description),
                        HttpUtility.HtmlEncode(listItem.CNPJ)
                    );
                    sbItems.Append(item);
                    counter++;
                }

                sbItems.Append(ClientRegistrationResources.TransferBovespaFooter);
            }

            string xml = sbItems.ToString();
            XmlDocument xmlReport = new XmlDocument();
            xmlReport.LoadXml(xml);

            MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(xml));
            msExcel.Position = 0;
            msExcel.Flush();

            return msExcel;
        }

        public MemoryStream CreateTransferBMFToMemoryStream(List<TransferBMFItem> list)
        {
            string headerXml = string.Empty;
            string footerXml = string.Empty;

            StringBuilder sbItems = new StringBuilder();

            if (list.Count > 0)
            {
                int counter = 0;
                headerXml = ClientRegistrationResources.TransferBMFHeader;
                headerXml = headerXml.Replace("#Author#", "VotoWeb");
                headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", cultureInfo));

                sbItems.Append(headerXml);

                MarketHelper helper = new MarketHelper();

                foreach (var listItem in list)
                {
                    string model = counter % 2 != 0 ? ClientRegistrationResources.TransferBMFEven : ClientRegistrationResources.TransferBMFOdd;
                    string item = string.Format(model,
                        HttpUtility.HtmlEncode(listItem.BondId.ToString()),
                        HttpUtility.HtmlEncode(listItem.BrokerName),
                        HttpUtility.HtmlEncode(listItem.ClientCode == 0 ? "-" : listItem.ClientCode.ToString()),
                        HttpUtility.HtmlEncode(string.IsNullOrEmpty(listItem.ClientName) ? "-" : listItem.ClientName),
                        HttpUtility.HtmlEncode(string.IsNullOrEmpty(listItem.CPFCGC) ? "-" : listItem.CPFCGC),
                        HttpUtility.HtmlEncode(listItem.TypeDesc)
                    );
                    sbItems.Append(item);
                    counter++;
                }

                sbItems.Append(ClientRegistrationResources.TransferBMFFooter);
            }

            string xml = sbItems.ToString();
            XmlDocument xmlReport = new XmlDocument();
            xmlReport.LoadXml(xml);

            MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(xml));
            msExcel.Position = 0;
            msExcel.Flush();

            return msExcel;
        }

        #endregion

        #region Guarantee

        public MemoryStream CreateGuaranteeToMemoryStream(GuaranteeClient guarantee)
        {
            string headerXml = string.Empty;
            string footerXml = string.Empty;

            StringBuilder sbItems = new StringBuilder();

            headerXml = GuaranteeClientResources.GuaranteeHeader;
            headerXml = headerXml.Replace("#Author#", "VotoWeb");
            headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", cultureInfo));

            headerXml = string.Format(headerXml,
                guarantee.ClientCode,
                HttpUtility.HtmlEncode(guarantee.ClientName),
                HttpUtility.HtmlEncode(guarantee.AssessorName),
                FormatStringToExcelNumber(guarantee.TotalValue.ToString()),
                FormatStringToExcelNumber(guarantee.MarginCall.ToString()),
                FormatStringToExcelNumber(guarantee.BalanceValue.ToString()),
                HttpUtility.HtmlEncode(guarantee.ActivitTypeDesc)
            );

            sbItems.Append(headerXml);

            int counter = 0;
            foreach (var listItem in guarantee.Activits)
            {
                string model = counter % 2 != 0 ? GuaranteeClientResources.GuaranteeActivitEven : GuaranteeClientResources.GuaranteeActivitOdd;
                string item = string.Format(model,
                    listItem.Type,
                    listItem.DueDate.ToShortDateString(),
                    FormatStringToExcelNumber(listItem.TotalValue.ToString())
                );
                sbItems.Append(item);
                counter++;
            }

            if (guarantee.Stocks.Count > 0)
            {
                string model1 = counter % 2 != 0 ? GuaranteeClientResources.GuaranteeActivitEven : GuaranteeClientResources.GuaranteeActivitOdd;
                string item1 = string.Format(model1,
                    "Ativos",
                    "-",
                    FormatStringToExcelNumber(guarantee.Stocks.Sum(p => p.Value).ToString())
                );

                sbItems.Append(item1);

                sbItems.Append(GuaranteeClientResources.GuaranteeSheetFooter);

                sbItems.Append(GuaranteeClientResources.GuaranteeStockHeader);

                counter = 0;
                foreach (var listItem in guarantee.Stocks)
                {
                    string model2 = counter % 2 != 0 ? GuaranteeClientResources.GuaranteeStockEven : GuaranteeClientResources.GuaranteeStockOdd;
                    string item2 = string.Format(model2,
                        listItem.Stock,
                        FormatStringToExcelNumber(listItem.Quantity.ToString()),
                        FormatStringToExcelNumber(listItem.Value.ToString())
                    );
                    sbItems.Append(item2);
                    counter++;
                }

                sbItems.Append(GuaranteeClientResources.GuaranteeSheetFooter);
            }
            else
            {
                sbItems.Append(GuaranteeClientResources.GuaranteeSheetFooter);
            }

            sbItems.Append(GuaranteeClientResources.GuaranteeFooter);

            string xml = sbItems.ToString();
            XmlDocument xmlReport = new XmlDocument();
            xmlReport.LoadXml(xml);

            MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(xml));
            msExcel.Position = 0;
            msExcel.Flush();

            return msExcel;
        }

        #endregion

        #region Position

        public MemoryStream CreateConsolidatedPositionReportToMemoryStream(ConsolidatedPositionResponse2 list, List<ConsolidatedPositionDetails> ListDetails)
        {
            string headerXml = string.Empty;
            string footerXml = string.Empty;

            StringBuilder sbItems = new StringBuilder();

            headerXml = Position.Header;
            headerXml = headerXml.Replace("#Author#", "BrPartners");
            headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", cultureInfo));
            sbItems.Append(headerXml);

            if (list.ConsolidatedPosition.Count() > 0)
            {

                foreach (var s in list.ConsolidatedPosition)
                {

                    bool even = true;

                    sbItems.Append(string.Format(Position.HeaderConsolidatedPosition, HttpUtility.HtmlEncode(s.DescTipoMercado), list.CdBolsa.ToString()));
                    var grupoDet = from x in ListDetails where x.TipoMercado == s.TipoMercado select x;
                    foreach (var i in (grupoDet))
                    {
                        string linha = string.Format(even ? Position.ItemConsolidatedPositionEven : Position.ItemConsolidatedPositionOdd,
                                                        HttpUtility.HtmlEncode(i.Ativo),
                                                        HttpUtility.HtmlEncode(FormatStringToExcelNumber(i.QtdDisponivel.ToString())),
                                                        HttpUtility.HtmlEncode(FormatStringToExcelNumber(i.Preco.ToString())),
                                                        HttpUtility.HtmlEncode(FormatStringToExcelNumber(i.ValorAtual.ToString()))
                        );
                        sbItems.Append(linha);
                        even = !even;
                    }

                    string linhaTotal = string.Format(Position.FooterTotal, HttpUtility.HtmlEncode(FormatStringToExcelNumber(s.ValorBruto.ToString()))
                        );
                    sbItems.Append(linhaTotal);
                    sbItems.Append(Position.SheetFooter);
                }


            }


            sbItems.Append(Position.Footer);

            string xml = sbItems.ToString();
            XmlDocument xmlReport = new XmlDocument();
            xmlReport.LoadXml(xml);

            MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(xml));
            msExcel.Position = 0;
            msExcel.Flush();

            return msExcel;


        }

        public MemoryStream CreateCustodyPositionReportToMemoryStream(CustodyPositionResponse cpr, string nomeCustodia)
        {
            List<CustodyPosition> list = new List<CustodyPosition>();
            list = cpr.CustodyPosition;

            string headerXml = string.Empty;
            string footerXml = string.Empty;

            StringBuilder sbItems = new StringBuilder();

            headerXml = Position.Header;
            headerXml = headerXml.Replace("#Author#", "BrPartners");
            headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", cultureInfo));
            sbItems.Append(headerXml);

            if (list.Count() > 0)
            {
                sbItems.Append(string.Format(Position.HeaderCustodyPosition, HttpUtility.HtmlEncode(cpr.DescTipoMercado), cpr.CdBolsa.ToString()));

                bool even = true;

                foreach (var s in list)
                {

                    string linha = string.Format(even ? Position.ItemCustodyPositionEven : Position.ItemCustodyPositionOdd,
                                                    HttpUtility.HtmlEncode(s.Ativo),
                                                    HttpUtility.HtmlEncode(s.NomeCarteira),
                                                    HttpUtility.HtmlEncode(FormatStringToExcelNumber(s.NumeroCarteira.ToString())),
                                                    HttpUtility.HtmlEncode(FormatStringToExcelNumber(s.QtdDisponivel.ToString("N0"))),
                                                    HttpUtility.HtmlEncode(FormatStringToExcelNumber(s.QtdOpLiquidar.ToString("N0"))),
                                                    HttpUtility.HtmlEncode(FormatStringToExcelNumber(s.QtdTotal.ToString("N0"))),
                                                    HttpUtility.HtmlEncode(FormatStringToExcelNumber(s.QtdBloqueada.ToString("N0"))),
                                                    HttpUtility.HtmlEncode(FormatStringToExcelNumber(s.QtdComprasExec.ToString("N0"))),
                                                    HttpUtility.HtmlEncode(FormatStringToExcelNumber(s.QtdComprasAbertas.ToString("N0"))),
                                                    HttpUtility.HtmlEncode(FormatStringToExcelNumber(s.QtdVendasExec.ToString("N0"))),
                                                    HttpUtility.HtmlEncode(FormatStringToExcelNumber(s.QtdVendasAbertas.ToString("N0"))),
                                                    HttpUtility.HtmlEncode(FormatStringToExcelNumber(s.ValorAtual.ToString("N2")))
                    );
                    sbItems.Append(linha);
                    even = !even;
                }

                var vltotal = (from x in list select x).Sum(x => x.ValorAtual);

                string linhaTotal = string.Format(Position.FooterCustodyPositionTotal, HttpUtility.HtmlEncode(FormatStringToExcelNumber(vltotal.ToString())));
                sbItems.Append(linhaTotal);
                sbItems.Append(Position.SheetFooter);
            }



            sbItems.Append(Position.Footer);

            string xml = sbItems.ToString();
            XmlDocument xmlReport = new XmlDocument();
            xmlReport.LoadXml(xml);

            MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(xml));
            msExcel.Position = 0;
            msExcel.Flush();

            return msExcel;


        }

        public MemoryStream CreateFinancialPositionReportToMemoryStream(FinancialPositionResponse list)
        {
            string headerXml = string.Empty;
            string footerXml = string.Empty;

            StringBuilder sbItems = new StringBuilder();

            headerXml = Position.Header;
            headerXml = headerXml.Replace("#Author#", "BrPartners");
            headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", cultureInfo));
            sbItems.Append(headerXml);

            sbItems.Append(string.Format(Position.HeaderFinancialPosition, HttpUtility.HtmlEncode("Posição Financeira"), list.CdBolsa.ToString()));

            //SALDO
            sbItems.Append(string.Format(Position.HeaderBalance));
            sbItems.Append(string.Format(Position.ItemBalance, "Inicial", FormatStringToExcelNumber(list.FinancialPosition.Saldo.Inicial.ToString())));
            sbItems.Append(string.Format(Position.ItemBalance, HttpUtility.HtmlEncode("Movimentação do dia"), FormatStringToExcelNumber(list.FinancialPosition.Saldo.MovimentoDia.ToString())));
            sbItems.Append(string.Format(Position.ItemBalance, "Final", FormatStringToExcelNumber(list.FinancialPosition.Saldo.Final.ToString())));

            //DISPONIVEL
            sbItems.Append(string.Format(Position.HeaderAvaible));

            foreach (var a in list.FinancialPosition.Disponivel)
            {
                sbItems.Append(string.Format(Position.ItemAvaible, HttpUtility.HtmlEncode(a.DescMercado), FormatStringToExcelNumber(a.ValorDisponivel.ToString())));
            }

            //ORDENS
            sbItems.Append(string.Format(Position.HeaderOrders));
            sbItems.Append(string.Format(Position.ItemOrders, "Compra", FormatStringToExcelNumber(list.FinancialPosition.Ordens.ValorComprasExec.ToString()), FormatStringToExcelNumber(list.FinancialPosition.Ordens.ValorComprasEmAberto.ToString())));
            sbItems.Append(string.Format(Position.ItemOrders, "Vendas", FormatStringToExcelNumber(list.FinancialPosition.Ordens.ValorVendasExec.ToString()), FormatStringToExcelNumber(list.FinancialPosition.Ordens.ValorVendasEmAberto.ToString())));
            sbItems.Append(string.Format(Position.ItemOrders, "Total", FormatStringToExcelNumber((list.FinancialPosition.Ordens.ValorComprasExec - list.FinancialPosition.Ordens.ValorComprasExec).ToString()), FormatStringToExcelNumber((list.FinancialPosition.Ordens.ValorVendasEmAberto - list.FinancialPosition.Ordens.ValorComprasEmAberto).ToString())));

            //PROJETADO
            sbItems.Append(string.Format(Position.HeaderProjected));
            sbItems.Append(string.Format(Position.ItemProjected, "D+1", FormatStringToExcelNumber(list.FinancialPosition.Projetado.ProjetadoD1.ToString())));
            sbItems.Append(string.Format(Position.ItemProjected, "D+2", FormatStringToExcelNumber(list.FinancialPosition.Projetado.ProjetadoD2.ToString())));
            sbItems.Append(string.Format(Position.ItemProjected, "D+3", FormatStringToExcelNumber(list.FinancialPosition.Projetado.ProjetadoD3.ToString())));
            sbItems.Append(string.Format(Position.ItemProjected, "Total Projetado", FormatStringToExcelNumber(list.FinancialPosition.Projetado.ProjetadoTotal.ToString())));

            sbItems.Append(Position.SheetFooter);


            sbItems.Append(Position.Footer);

            string xml = sbItems.ToString();
            XmlDocument xmlReport = new XmlDocument();
            xmlReport.LoadXml(xml);

            MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(xml));
            msExcel.Position = 0;
            msExcel.Flush();

            return msExcel;


        }

        #endregion

        #region Demonstrative

        public MemoryStream CreateExtractReportToMemoryStream(ExtractRendaResponse list)
        {
            string headerXml = string.Empty;
            string footerXml = string.Empty;

            StringBuilder sbItems = new StringBuilder();

            headerXml = Demonstrative.Header;
            headerXml = headerXml.Replace("#Author#", "BrPartners");
            headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", cultureInfo));
            sbItems.Append(headerXml);

            var periodoDe = Convert.ToDateTime(list.DtRefDe).ToString("dd/MM/yyyy");
            var periodoAte = Convert.ToDateTime(list.DtRefAte);

            if (list.Extrato.Count() > 0)
            {
                sbItems.Append(string.Format(Demonstrative.HeaderExtract, periodoDe, periodoAte, list.Cnpjcpf));

                bool even = true;

                foreach (var s in list.Extrato)
                {

                    string linha = string.Format(even ? Demonstrative.ItemExtractEven : Demonstrative.ItemExtractOdd,

                                                    HttpUtility.HtmlEncode(s.dt_operacao),
                                                    HttpUtility.HtmlEncode(s.dc_evento),
                                                    HttpUtility.HtmlEncode(s.dc_grupo_papel),
                                                    HttpUtility.HtmlEncode(s.dt_vencimento),
                                                    HttpUtility.HtmlEncode(s.dc_indexador),
                                                    HttpUtility.HtmlEncode(FormatStringToExcelNumber(s.tx_operacao.ToString())),
                                                    HttpUtility.HtmlEncode(FormatStringToExcelNumber(s.tx_percentual.ToString())),
                                                    HttpUtility.HtmlEncode(FormatStringToExcelNumber(s.vl_operacao.ToString())),
                                                    HttpUtility.HtmlEncode(FormatStringToExcelNumber(s.vl_bruto.ToString())),
                                                    HttpUtility.HtmlEncode(FormatStringToExcelNumber(s.tx_iof.ToString())),
                                                    HttpUtility.HtmlEncode(FormatStringToExcelNumber(s.tx_ir.ToString())),
                                                    HttpUtility.HtmlEncode(FormatStringToExcelNumber(s.vl_liquido.ToString()))
                    );
                    sbItems.Append(linha);
                    even = !even;
                }

                sbItems.Append(Demonstrative.SheetFooter);
            }

            sbItems.Append(Demonstrative.Footer);

            string xml = sbItems.ToString();
            XmlDocument xmlReport = new XmlDocument();
            xmlReport.LoadXml(xml);

            MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(xml));
            msExcel.Position = 0;
            msExcel.Flush();

            return msExcel;
        }

        public MemoryStream CreateProceedsReportToMemoryStream(ProceedsDemonstrativeResponse list)
        {
            string headerXml = string.Empty;
            string footerXml = string.Empty;

            StringBuilder sbItems = new StringBuilder();

            headerXml = Demonstrative.Header;
            headerXml = headerXml.Replace("#Author#", "BrPartners");
            headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", cultureInfo));
            sbItems.Append(headerXml);

            if (list.Proventos.Count() > 0)
            {
                sbItems.Append(string.Format(Demonstrative.HeaderProceeds, list.Tipo == "S" ? "Pagos" : "Provisionados", list.dtInicial.ToString("dd/MM/yyyy"), list.dtFinal.ToString("dd/MM/yyyy"), list.CdBolsa));

                bool even = true;

                foreach (var s in list.Proventos)
                {

                    string linha = string.Format(even ? Demonstrative.ItemProceedsEven : Demonstrative.ItemProceedsOdd,
                                                    HttpUtility.HtmlEncode(s.Pagamento.ToString("dd/MM/yyyy")),
                                                    HttpUtility.HtmlEncode(s.Ativo),
                                                    HttpUtility.HtmlEncode(s.Empresa),
                                                    HttpUtility.HtmlEncode(s.Provento),
                                                    HttpUtility.HtmlEncode(FormatStringToExcelNumber(s.ValorBruto.ToString())),
                                                    HttpUtility.HtmlEncode(FormatStringToExcelNumber(s.PercIr.ToString())),
                                                    HttpUtility.HtmlEncode(FormatStringToExcelNumber(s.ValorLiquido.ToString()))
                    );
                    sbItems.Append(linha);
                    even = !even;
                }

                sbItems.Append(Demonstrative.SheetFooter);
            }



            sbItems.Append(Demonstrative.Footer);

            string xml = sbItems.ToString();
            XmlDocument xmlReport = new XmlDocument();
            xmlReport.LoadXml(xml);

            MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(xml));
            msExcel.Position = 0;
            msExcel.Flush();

            return msExcel;


        }

        public MemoryStream CreateTradingSummaryReportToMemoryStream(TradingSummaryResponse list)
        {
            string headerXml = string.Empty;
            string footerXml = string.Empty;

            StringBuilder sbItems = new StringBuilder();

            headerXml = Demonstrative.Header;
            headerXml = headerXml.Replace("#Author#", "BrPartners");
            headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", cultureInfo));
            sbItems.Append(headerXml);

            if (list.ResumoNegociacao.Count() > 0)
            {
                sbItems.Append(string.Format(Demonstrative.HeaderTradingSummary, list.Mes, list.CdBolsa));

                bool even = true;

                foreach (var s in list.ResumoNegociacao)
                {

                    string linha = string.Format(even ? Demonstrative.ItemTradingSummaryEven : Demonstrative.ItemTradingSummaryOdd,
                                                    HttpUtility.HtmlEncode(s.Data.ToString("dd/MM/yyyy")),
                                                    HttpUtility.HtmlEncode(FormatStringToExcelNumber(s.QtdCompra.ToString())),
                                                    HttpUtility.HtmlEncode(FormatStringToExcelNumber(s.QtdVenda.ToString())),
                                                    HttpUtility.HtmlEncode(FormatStringToExcelNumber(s.Preco.ToString())),
                                                    HttpUtility.HtmlEncode(FormatStringToExcelNumber(s.TotalFinCompra.ToString())),
                                                    HttpUtility.HtmlEncode(FormatStringToExcelNumber(s.TotalFinVenda.ToString())),
                                                    HttpUtility.HtmlEncode(FormatStringToExcelNumber(s.FinanceiroLiquido.ToString()))
                    );
                    sbItems.Append(linha);
                    even = !even;
                }

                sbItems.Append(string.Format(Demonstrative.ItemTradingSummaryTotal, FormatStringToExcelNumber((from x in list.ResumoNegociacao select x.TotalFinCompra).Sum().ToString()), FormatStringToExcelNumber((from x in list.ResumoNegociacao select x.TotalFinVenda).Sum().ToString()), FormatStringToExcelNumber((from x in list.ResumoNegociacao select x.FinanceiroLiquido).Sum().ToString())));

                sbItems.Append(Demonstrative.SheetFooter);
            }



            sbItems.Append(Demonstrative.Footer);

            string xml = sbItems.ToString();
            XmlDocument xmlReport = new XmlDocument();
            xmlReport.LoadXml(xml);

            MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(xml));
            msExcel.Position = 0;
            msExcel.Flush();

            return msExcel;


        }

        public MemoryStream ExtractNDFExcelReportToMemoryStream(ExtractNDFResponse list)
        {
            string headerXml = string.Empty;
            string footerXml = string.Empty;

            StringBuilder sbItems = new StringBuilder();

            headerXml = Demonstrative.Header;
            headerXml = headerXml.Replace("#Author#", "BrPartners");
            headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", cultureInfo));
            sbItems.Append(headerXml);

            if (list.ExtratoNDF.Count() > 0)
            {
                sbItems.Append(string.Format(Demonstrative.HeaderExtractNDF, list.Ano, list.Mes, list.Cnpjcpf));

                bool even = true;

                foreach (var s in list.ExtratoNDF)
                {

                    string linha = string.Format(even ? Demonstrative.ItemExtractEvenNDF : Demonstrative.ItemExtractOddNDF,
                                                  HttpUtility.HtmlEncode(s.tp_operacao),
                                                 HttpUtility.HtmlEncode(s.dt_operacao),
                                                 HttpUtility.HtmlEncode(s.dt_vencimento),
                                                 HttpUtility.HtmlEncode(FormatStringToExcelNumber(s.vl_base_moeda_estr.ToString())),
                                                 HttpUtility.HtmlEncode(s.tp_moeda_ref),
                                                 HttpUtility.HtmlEncode(s.tp_moeda_cot),
                                                 HttpUtility.HtmlEncode(s.dc_criterio_termo),
                                                 HttpUtility.HtmlEncode(FormatStringToExcelNumber(s.vl_cotacao.ToString())),
                                                 HttpUtility.HtmlEncode(FormatStringToExcelNumber(s.vl_bruto.ToString())),
                                                 HttpUtility.HtmlEncode(FormatStringToExcelNumber(s.vl_ir.ToString())),
                                                 HttpUtility.HtmlEncode(FormatStringToExcelNumber(s.vl_liquido.ToString()))

                    );
                    sbItems.Append(linha);
                    even = !even;
                }

                sbItems.Append(Demonstrative.SheetFooter);
            }

            sbItems.Append(Demonstrative.Footer);

            string xml = sbItems.ToString();
            XmlDocument xmlReport = new XmlDocument();
            xmlReport.LoadXml(xml);

            MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(xml));
            msExcel.Position = 0;
            msExcel.Flush();

            return msExcel;
        }

        public MemoryStream ExtractSWPExcelReportToMemoryStream(ExtractSWPResponse list)
        {
            string headerXml = string.Empty;
            string footerXml = string.Empty;

            StringBuilder sbItems = new StringBuilder();

            headerXml = Demonstrative.Header;
            headerXml = headerXml.Replace("#Author#", "BrPartners");
            headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", cultureInfo));
            sbItems.Append(headerXml);

            if (list.ExtratoSWP.Count() > 0)
            {
                sbItems.Append(string.Format(Demonstrative.HeaderExtractSWP, list.DtRef, list.Cnpjcpf));

                bool even = true;

                foreach (var s in list.ExtratoSWP)
                {

                    string linha = string.Format(even ? Demonstrative.ItemExtractEvenSWP : Demonstrative.ItemExtractOddSWP,
                                                 HttpUtility.HtmlEncode(s.tp_operacao),
                                                 HttpUtility.HtmlEncode(s.dt_operacao),
                                                 HttpUtility.HtmlEncode(s.dt_vencimento),
                                                 HttpUtility.HtmlEncode(FormatStringToExcelNumber(s.vl_base.ToString())),
                                                 HttpUtility.HtmlEncode(s.dc_index_dado),
                                                 HttpUtility.HtmlEncode(FormatStringToExcelNumber(s.vl_perc_index_data.ToString())),
                                                 HttpUtility.HtmlEncode(FormatStringToExcelNumber(s.vl_taxa_dada.ToString())),
                                                 HttpUtility.HtmlEncode(s.dc_index_tomado),
                                                 HttpUtility.HtmlEncode(FormatStringToExcelNumber(s.vl_perc_index_tomado.ToString())),
                                                 HttpUtility.HtmlEncode(FormatStringToExcelNumber(s.vl_taxa_tomado.ToString())),
                                                 HttpUtility.HtmlEncode(FormatStringToExcelNumber(s.vl_curva_dado.ToString())),
                                                 HttpUtility.HtmlEncode(FormatStringToExcelNumber(s.vl_curva_tomado.ToString())),
                                                 HttpUtility.HtmlEncode(FormatStringToExcelNumber(s.vl_bruto.ToString())),
                                                 HttpUtility.HtmlEncode(FormatStringToExcelNumber(s.vl_ir.ToString())),
                                                 HttpUtility.HtmlEncode(FormatStringToExcelNumber(s.vl_liquido.ToString()))

                    );
                    sbItems.Append(linha);
                    even = !even;
                }

                sbItems.Append(Demonstrative.SheetFooter);
            }

            sbItems.Append(Demonstrative.Footer);

            string xml = sbItems.ToString();
            XmlDocument xmlReport = new XmlDocument();
            xmlReport.LoadXml(xml);

            MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(xml));
            msExcel.Position = 0;
            msExcel.Flush();

            return msExcel;
        }

        public MemoryStream CreateBrokerageNoteReportToMemoryStream(BrokerageNoteResponse list)
        {
            string headerXml = string.Empty;
            string footerXml = string.Empty;

            StringBuilder sbItems = new StringBuilder();

            headerXml = Demonstrative.Header;
            headerXml = headerXml.Replace("#Author#", "BrPartners");
            headerXml = headerXml.Replace("#Date#", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", cultureInfo));
            sbItems.Append(headerXml);

            if (list.NotasCorretagem.Count() > 0)
            {
                sbItems.Append(string.Format(Demonstrative.HeaderBrokerageNote, list.dtInicial.ToString("dd/MM/yyyy"), list.dtFinal.ToString("dd/MM/yyyy"), list.CdBolsa));

                bool even = true;

                foreach (var s in list.NotasCorretagem)
                {

                    string linha = string.Format(even ? Demonstrative.ItemBrokerageNoteEven : Demonstrative.ItemBrokerageNoteOdd,
                                                    HttpUtility.HtmlEncode(s.DtNota.ToString("dd/MM/yyyy")),
                                                    HttpUtility.HtmlEncode(FormatStringToExcelNumber(s.NrNota.ToString()))
                    );
                    sbItems.Append(linha);
                    even = !even;
                }

                sbItems.Append(Demonstrative.SheetFooter);
            }



            sbItems.Append(Demonstrative.Footer);

            string xml = sbItems.ToString();
            XmlDocument xmlReport = new XmlDocument();
            xmlReport.LoadXml(xml);

            MemoryStream msExcel = new MemoryStream(Encoding.Default.GetBytes(xml));
            msExcel.Position = 0;
            msExcel.Flush();

            return msExcel;


        }

        #endregion

        protected string ReplaceValues(List<KeyValuePair<string, string>> values, string model)
        {
            values.Add(new KeyValuePair<string, string>("Author", "Votoweb"));
            values.Add(new KeyValuePair<string, string>("Date", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")));

            string content = model;
            foreach (KeyValuePair<string, string> pair in values)
                content = content.Replace(string.Concat("#", pair.Key, "#"), pair.Value);

            return content;
        }

        private string FormatStringToExcelNumber(string oldValue)
        {
            return oldValue.Replace(".", string.Empty).Replace(",", ".").Replace("%", string.Empty);
        }
    }
}