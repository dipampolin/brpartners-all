﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using QX3.Portal.Contracts.DataContracts;
using System.Text;
using QX3.Portal.WebSite.Models;
using QX3.Portal.WebSite.Models.Channel;
using QX3.Portal.WebSite.RiskService;
using QX3.Spinnex.Common.Services.Logging;

namespace QX3.Portal.WebSite.Helper
{
    public class GuaranteesHelper
    {
        public static string[] GuaranteesActions = new string[] { "Solicitar", "Efetuar", "Excluir", "Alterar", "Exportar" };


        public GuaranteeClientContent GetClientGuaranteeContent(GuaranteeListItem guarantee, bool inGuarantee)
        {
            GuaranteeClientContent clientContent = new GuaranteeClientContent();
            clientContent.Client = string.Concat(guarantee.Client.Value, " ", guarantee.Client.Description);
            clientContent.Assessor = guarantee.Assessor;

            if (guarantee.Balance != null)
            {
                var balance = guarantee.Balance;
                string balanceItems = "<tr><td class=\"{0}\"><strong>Total Garantias:</strong><span>{1}</span></td><td class=\"even {2}\"><strong>Usados:</strong><span>{3}</span></td><td class=\"{4}\"><strong>Livres:</strong><span>{5}</span></td><td class=\"even {6}\"><strong>Após Efetuações:</strong><span>{7}</span></td></tr>";
                balanceItems = string.Format(balanceItems,
                                             balance.Total >= 0 ? "positivo" : "negativo",
                                             balance.Total.ToString("N2"),
                                             balance.Used >= 0 ? "positivo" : "negativo",
                                             balance.Used.ToString("N2"),
                                             balance.Free >= 0 ? "positivo" : "negativo",
                                             balance.Free.ToString("N2"),
                                             balance.AfterRealize >= 0 ? "positivo" : "negativo",
                                             balance.AfterRealize.ToString("N2"));
                clientContent.HtmlBalanceItems = balanceItems;
            }

            clientContent.Index = guarantee.Index;
            clientContent.InGuarantee = inGuarantee;

            StringBuilder htmlItems = new StringBuilder();

            if (guarantee != null)
                foreach (GuaranteeListSubItem g in guarantee.Items)
                {
                    bool toRealize = (g.Status.Value == "15");

                    AccessControlHelper acHelper = new AccessControlHelper();
                    bool canEdit = acHelper.HasFuncionalityPermission("Risk.Guarantees", "btnEdit");
                    bool canDelete = acHelper.HasFuncionalityPermission("Risk.Guarantees", "btnDelete");
                    bool canRealize = acHelper.HasFuncionalityPermission("Risk.Guarantees", "btnRealize");
                    bool canSeeLog = acHelper.HasFuncionalityPermission("Risk.Guarantees", "btnLog");

                    string td0 = string.Format("<input id=\"chkGuarantee{0}\" type=\"checkbox\" value=\"{0}\" name=\"chkGuarantee{0}\" {1} />", g.ID, toRealize ? "" : "disabled");
                    string td7 = (toRealize && canRealize) ? string.Format("<a href=\"javascript:void(0);\" id=\"lnkRealize{0}\" class=\"exibir-modal\">{1}</a>", g.ID, g.Status.Description) : g.Status.Description;
                    string blankImg = " <img class=\"espaco\" src=\"../../img/ico/icon-space-table.gif\" />";
                    string td8 = string.Format("{1}{3}{2}", g.ID,
                        (toRealize && canEdit) ? string.Format("<a class=\"ico-editar\" href=\"javascript:;\" onclick=\"OpenRequest({0})\">editar</a>", g.ID) : blankImg,
                        (toRealize && canDelete) ? string.Format("<a class=\"ico-excluir\" onclick=\"DeleteGuarantee({0});return false;\" href=\"javascript:void(0);\">excluir</a>", g.ID) : blankImg, (canSeeLog && g.Status.Value != "-1") ? string.Format("<a class=\"ico-hist\" onclick=\"OpenGuaranteeHistory({0})\" href=\"javascript:void(0);\">log</a>", g.ID) : string.Empty);

                    bool isMoney =  g.Guarantee.ToLower().IndexOf("dinheiro") >= 0;

                    string trFormat = string.Format("<tr>{0}{1}<td>{2}</td><td>{3}</td><td>{4}</td>{5}{6}{7}{8}<td class=\"direita\">{9}</td><td>{10}</td>{11}</tr>",
                                                    string.Format("<td {1}>{0}</td>", td0, inGuarantee ? "class='InGuaranteeColumn'" : ""),
                                                    string.Format("<td {1}>{0}</td>", g.ID == 0 ? "-" : g.IsWithdrawal ? "Retirada" : "Depósito", inGuarantee ? "class='InGuaranteeColumn'" : ""),
                                                    g.Guarantee,
                                                    isMoney ? "-" : g.IsBovespa ? "Bovespa" : "BM&F",
                                                    g.UpdateDate.ToString("dd/MM/yyyy"),
                                                    string.Format("<td class='{0} direita'>{1}</td>", isMoney ? "" : g.Quantity >= 0 ? "positivo" : "negativo", isMoney ? "-" : g.Quantity.ToString("N0")),
                                                    string.Format("<td class='{0} direita'>{1}</td>", isMoney ? "" : g.UnitPrice >= 0 ? "positivo" : "negativo", isMoney ? "-" : g.UnitPrice.ToString("N2")),
                                                    string.Format("<td class='{0} direita'>{1}</td>", g.Total >= 0 ? "positivo" : "negativo", g.Total.ToString("N2")),
                                                    string.Format("<td class='{0} direita'>{1}</td>", g.Discount >= 0 ? "positivo" : "negativo", string.Concat(g.Discount.ToString("N2"), "%")),
                                                    inGuarantee ? "-" : g.RequestedValue.ToString("N2"),
                                                    td7,
                                                    string.Format("<td {1}>{0}</td>", td8, inGuarantee ? "class='InGuaranteeColumn'" : ""));
                    htmlItems.Append(trFormat);
                }

            clientContent.HtmlItems = htmlItems.ToString();

            return clientContent;
        }

        public int GetNumberOfPending()
        {
            return CommonHelper.GetPendents("Risk.Guarantees");

            /*int pending = 0;

            try
            {
                MarketHelper helper = new MarketHelper();

                IChannelProvider<IRiskContractChannel> riskProvider = ChannelProviderFactory<IRiskContractChannel>.Create("RiskClassName");
                var riskService = riskProvider.GetChannel().CreateChannel();

                riskService.Open();
                pending = riskService.GetNumberOfPending(helper.GetAssessorsRangesToFilter(string.Empty)).Result;
                riskService.Close();
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
            }
            return pending;*/
        }
        
    }
}