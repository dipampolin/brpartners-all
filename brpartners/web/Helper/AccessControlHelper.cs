﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.WebSite.Models.Channel;
using QX3.Portal.WebSite.AccessControlService;
using QX3.Spinnex.Common.Services.Logging;
using System.Web.Mvc;
using QX3.Portal.WebSite.AuthenticationService;
using System.Text.RegularExpressions;
using QX3.Portal.WebSite.Models;
using System.Text;
using QX3.Spinnex.Common.Services.Security;
using System.Web.Security;

namespace QX3.Portal.WebSite.Helper
{
    public class AccessControlHelper
    {
        public class SessionItemNullException : Exception { }

        public static List<ACElement> AllowedElements
        {
            get
            {
                var profile = SessionHelper.CurrentUser.Profile;
                if (profile != null && profile.Permissions != null)
                    return profile.Permissions;
                return new List<ACElement>();
            }
        }

        public static string[] GroupOfAssessorsActions = new string[] { "Criar Grupo", "Exportar", "Alterar Grupo", "Ver Detalhe", "Excluir Grupo" };
        public static string[] UserActions = new string[] { "Criar Usuário", "Alterar Usuário", "Associar Usuário a um Grupo", "Associar Usuário a um Perfil", "Excluir Usuário", "Exportar" };
        public static string[] ProfilesActions = new string[] { "Criar Perfil", "Exportar", "Alterar Perfil", "Ver Detalhes", "Excluir Perfil" };

        public bool HasPagePermission(string pageName)
        {
            bool allowed = false;

            var elements = AllowedElements;
            if (elements != null)
                allowed = elements.Where(e => e.Name.ToLower().Equals(WebSiteHelper.RemoveSpecialChars(pageName.ToLower()))).Any();
            return allowed;
        }

        public bool HasFuncionalityPermission(string pageName, string funcName)
        {
            var pages = AllowedElements;

            if (pages != null)
            {
                var page = pages.Where(p => p.Name.ToLower().Equals(WebSiteHelper.RemoveSpecialChars(pageName.ToLower()))).FirstOrDefault();
                if (page != null)
                {
                    var elements = pages.Where(p => p.ParentID.Equals(page.ID));
                    if (elements != null)
                        return elements.Where(e => e.Name.Equals(funcName)).Any();
                }
            }
            return false;
        }

        public List<ACElement> LoadElements(bool active)
        {
            string sessionName = string.Concat("AccessControl.ACElements", active ? ".Active" : "");

            //if (HttpContext.Current.Session[sessionName] == null || ((List<ACElement>)HttpContext.Current.Session[sessionName]).Count <= 0)
            //{
                List<ACElement> elements = new List<ACElement>();

                try
                {
                    IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
                    var service = channelProvider.GetChannel().CreateChannel();

                    service.Open();
                    elements = service.LoadACElements(active).Result.ToList();
                    service.Close();
                }
                catch (Exception ex)
                {
                    LogManager.WriteError(ex);
                }
                HttpContext.Current.Session[sessionName] = elements;
            //}
            return (List<ACElement>)HttpContext.Current.Session[sessionName];
        }

        public void ResetElementsSession(bool active)
        {
            string sessionName = string.Concat("AccessControl.ACElements", active ? ".Active" : "");
            HttpContext.Current.Session[sessionName] = null;
        }

        public List<ACUser> LoadUsers()
        {
            string sessionName = "AccessControl.ACUsers";

            if (HttpContext.Current.Session[sessionName] == null || ((List<ACUser>)HttpContext.Current.Session[sessionName]).Count <= 0)
            {
                List<ACUser> users = new List<ACUser>();

                try
                {
                    IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
                    var service = channelProvider.GetChannel().CreateChannel();

                    service.Open();
                    users = service.LoadUsers().Result.ToList();
                    service.Close();
                }
                catch (Exception ex)
                {
                    LogManager.WriteError(ex);
                }
                HttpContext.Current.Session[sessionName] = users;
            }
            return (List<ACUser>)HttpContext.Current.Session[sessionName];
        }

        #region Users


        public bool EmailIsValid(string txtUserEmail, int userID)
        {
            try
            {
                IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();
                var user = service.LoadUsers().Result.Where(u => u.Email == txtUserEmail);
                service.Close();

                return (user.Count() == 0) || (user.Count() == 1 && user.First().ID == userID);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return false;
            }
        }

        public bool UsernameIsValid(string userName, int userID)
        {
            try
            {
                IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();
                var user = service.LoadUsers().Result.Where(u => u.Login.ToLower() == userName.ToLower());
                service.Close();

                return (user.Count() == 0) || (user.Count() == 1 && user.First().ID == userID);
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return false;
            }
        }

        public string SendWelcomeEmail(string email, string name)
        {
            string feedback = string.Empty;

            IChannelProvider<IAuthenticationContractChannel> channelProvider = ChannelProviderFactory<IAuthenticationContractChannel>.Create("AuthenticationClassName");
            var service = channelProvider.GetChannel().CreateChannel();

            service.Open();

            var response = service.CreateGuidToSend(new UserInformation { Email = email });

            if (response.Result)
            {
                if (!service.SendWelcomeEmail(response.Data, email, name).Result)
                    feedback = "Ocorreu um erro ao enviar o e-mail de redefinição de senha.";
            }
            else
                feedback = "Ocorreu um erro ao criar o e-mail de redefinição de senha.";

            service.Close();

            return feedback;
        }

        #endregion

        #region Clientes Online

        public bool CpfCadastrado(decimal? cpf)
        {
            IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
            var service = channelProvider.GetChannel().CreateChannel();

            service.Open();
            var user = service.LoadClient(cpf.ToString()).Result;
            service.Close();

            if (user.CpfCnpj != null)
                return true;

            return false;
        }

        public bool EmailCadastrado(string email)
        {
            IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
            var service = channelProvider.GetChannel().CreateChannel();

            service.Open();
            var user = service.LoadUserProfile(email).Result;
            service.Close();

            if (user.ID != 0)
                return true;

            return false;
        }

        public bool EmailCadastrado(decimal? cpf)
        {
            IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
            var service = channelProvider.GetChannel().CreateChannel();

            service.Open();
            var user = service.LoadClient(cpf.ToString()).Result;
            service.Close();

            if (user.CpfCnpj != null)
                return true;

            return false;
        }

        #endregion

        #region Group Of Assessors

        public List<ACGroupOfAssessors> LoadGroupsOfAssessors()
        {
            string sessionName = "AccessControl.ACGroupOfAssessors";

            if (HttpContext.Current.Session[sessionName] == null || ((List<ACGroupOfAssessors>)HttpContext.Current.Session[sessionName]).Count <= 0)
            {
                List<ACGroupOfAssessors> groups = new List<ACGroupOfAssessors>();

                try
                {
                    IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
                    var service = channelProvider.GetChannel().CreateChannel();

                    service.Open();
                    groups = service.LoadGroupOfAssessors(true, 0, string.Empty).Result.ToList();
                    service.Close();
                }
                catch (Exception ex)
                {
                    LogManager.WriteError(ex);
                }
                HttpContext.Current.Session[sessionName] = groups;
            }
            return (List<ACGroupOfAssessors>)HttpContext.Current.Session[sessionName];
        }

        public List<ACProfile> LoadProfiles()
        {
            string sessionName = "AccessControl.ACProfiles";

            if (HttpContext.Current.Session[sessionName] == null || ((List<ACProfile>)HttpContext.Current.Session[sessionName]).Count <= 0)
            {
                List<ACProfile> profiles = new List<ACProfile>();

                try
                {
                    IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
                    var service = channelProvider.GetChannel().CreateChannel();

                    service.Open();
                    profiles = service.LoadProfiles().Result.ToList();
                    service.Close();
                }
                catch (Exception ex)
                {
                    LogManager.WriteError(ex);
                }
                HttpContext.Current.Session[sessionName] = profiles;
            }
            return (List<ACProfile>)HttpContext.Current.Session[sessionName];
        }

        public List<int> GetAssessorsIds(string assessors)
        {
            return WebSiteHelper.ConvertRangeStrToNumList(assessors);
        }

        public string GetAssessorsRangesAndConvertToString(string assessors)
        {
            string list = string.Empty;
            var userList = SessionHelper.CurrentUser.Assessors;
            List<int> ids = WebSiteHelper.ConvertRangeStrToNumList(assessors);

            if (userList != null && userList.Count > 0)
                ids = userList.Contains(0) ? ids : (from i in ids.OfType<int>() where userList.Contains(i) select i).ToList();

            return ids.Count > 0 ? (String.Join(";", ids) + ';') : "";
        }

        public string GetClientsRangesAndConvertToString(string clients)
        {
            List<int> ids = WebSiteHelper.ConvertRangeStrToNumList(clients);
            return ids.Count > 0 ? (String.Join(";", ids) + ';') : "";
        }

        public string ConvertClientRangesToStartEndFormat(string clients)
        {
            string list = string.Empty;
            List<int> ids = WebSiteHelper.ConvertRangeStrToNumList(clients);
            if (ids.Count > 0)
            {
                int min = (from id in ids.OfType<int>() select id).Min();
                int max = (from id in ids.OfType<int>() select id).Max();
                list = string.Concat(min, ";", max);
            }
            return list;
        }

        public bool GroupNameIsValid(string txtGroupName, int id)
        {
            try
            {
                IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                var response = service.ValidateGroupName(txtGroupName, id);

                service.Close();

                return response.Result;

            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return false;
            }
        }

        public bool AssessorsListIsValid(int groupId, string txtAssessors, string clientsInclude, string clientsExclude)
        {
            try
            {
                List<int> list = GetAssessorsIds(txtAssessors);
                List<int> distinctList = list.Distinct().ToList();

                if (list.Count != distinctList.Count)
                    return false;

                IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                //var response = service.ValidateAssessorsList(txtAssessors, groupId);
                var response = service.ValidateAssessorsAndClientsList(groupId, txtAssessors, clientsInclude, clientsExclude);

                service.Close();

                return response.Result;
            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return false;
            }
        }

        #endregion

        public string GetUserName(int userID)
        {
            var users = this.LoadUsers();
            var user = users.Where(u => u.ID.Equals(userID)).FirstOrDefault();
            return user == null ? string.Empty : user.Name;
        }

        public string GetFuncionalityAlias(int funcID)
        {
            var funcs = this.LoadElements(false);
            var funcName = (from f in funcs where f.ID.Equals(funcID) select f.Alias).FirstOrDefault();
            return funcName ?? string.Empty;
        }

        public string GetGroupName(int groupID)
        {
            var groups = this.LoadGroupsOfAssessors();
            var groupName = (from g in groups where g.ID.Equals(groupID) select g.Name).FirstOrDefault();
            return groupName ?? string.Empty;
        }

        public string GetProfileName(int profileID)
        {
            var profiles = this.LoadProfiles();
            var profileName = (from g in profiles where g.ID.Equals(profileID) select g.Name).FirstOrDefault();
            return profileName ?? string.Empty;
        }

        public bool ProfileNameIsValid(string profileName, int profileID)
        {
            try
            {
                IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                var response = service.ValidateProfileName(profileName, profileID);

                service.Close();

                return response.Result;

            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return false;
            }
        }

        public bool ProfileElementsAreValid(List<ACElement> elements, int profileID)
        {
            try
            {
                IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
                var service = channelProvider.GetChannel().CreateChannel();

                service.Open();

                var response = service.ValidateProfileElements(elements.ToArray(), profileID);

                service.Close();

                return response.Result;

            }
            catch (Exception ex)
            {
                LogManager.WriteError(ex);
                return false;
            }
        }

        public List<ACUser> LoadUsersWithoutProfile()
        {
            string sessionName = "AccessControl.ACUsersWithoutProfile";

            if (HttpContext.Current.Session[sessionName] == null || ((List<ACUser>)HttpContext.Current.Session[sessionName]).Count <= 0)
            {
                List<ACUser> users = new List<ACUser>();

                try
                {
                    IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
                    var service = channelProvider.GetChannel().CreateChannel();

                    service.Open();
                    users = service.LoadUsersWithoutProfile().Result.ToList();
                    service.Close();
                }
                catch (Exception ex)
                {
                    LogManager.WriteError(ex);
                }
                HttpContext.Current.Session[sessionName] = users;
            }
            return (List<ACUser>)HttpContext.Current.Session[sessionName];
        }

        #region Login

        public LoginModel Login(string txtLogin, string txtPassword, string ipClient, string sessionId)
        {
            var login = new LoginModel();

            IChannelProvider<IAuthenticationContractChannel> channelProvider = ChannelProviderFactory<IAuthenticationContractChannel>.Create("AuthenticationClassName");
            var service = channelProvider.GetChannel().CreateChannel();
            service.Open();

            PasswordGenerator pg = new PasswordGenerator();
            string encryptedPassword = pg.EncodePassword(txtPassword);

            var response = service.AuthenticateUser(txtLogin.ToLower(), encryptedPassword, true); 

            service.Close();

            if (response.Result != null && response.Data.CanLogIn)
            {
                FormsAuthentication.SetAuthCookie(txtLogin.ToLower(), true);
                SessionHelper.CurrentUser = response.Result;
                SessionHelper.ThirdPartySuitabilityClient = null;

                IChannelProvider<IAccessControlContractChannel> channelProvider2 = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
                using (var service2 = channelProvider2.GetChannel().CreateChannel())
                {
                    var verifySession = service2.Verify_SESSION_Login(txtLogin.ToLower()).Result;

                    if (verifySession)
                    {
                        var resDel = service2.Delete_SESSION_Login(txtLogin.ToLower()).Result;
                    }

                    var resInsert = service2.Insert_SESSION(txtLogin.ToLower(), Convert.ToString(ipClient), DateTime.Now, sessionId).Result;
                }

                login.Element = SessionHelper.CurrentUser.Profile.Permissions;
                login.Profile = SessionHelper.CurrentUser.Profile;
                login.IdClient = response.Result.IdClient;
            }
            else
            {
                login.LoginError = new LoginError
                {
                    Message = response.Message,
                    ErrorType = response.Data.ErrorType == 1 || response.Data.ErrorType == 4 || response.Data.ErrorType == 5 ? string.Empty : "erro"
                };
            }

            return login;
        }

        public void UpdateUserPermissions()
        {
            try
            {
                IChannelProvider<IAccessControlContractChannel> channelProvider = ChannelProviderFactory<IAccessControlContractChannel>.Create("AccessControlClassName");
                var service = channelProvider.GetChannel().CreateChannel();
                service.Open();
                var response = service.LoadUserProfile(SessionHelper.CurrentUser.Email);
                service.Close();
                if (response != null && response.Result != null)
                    SessionHelper.CurrentUser.Profile = response.Result;
            }
            catch (Exception ex) { LogManager.WriteError(ex); }
        }

        public bool CheckIfUserExistsInLDAP(string filterValue, LDAPSearchFilter filterName)
        {
            IChannelProvider<IAuthenticationContractChannel> channelProvider = ChannelProviderFactory<IAuthenticationContractChannel>.Create("AuthenticationClassName");
            var service = channelProvider.GetChannel().CreateChannel();

            service.Open();
            var response = service.CheckIfUserExists(filterValue, filterName);
            service.Close();

            return response.Result;
        }

        public bool ValidateLDAPLogin(string login)
        {
            int loginLength = login.Length;
            if (string.IsNullOrEmpty(login) || loginLength < 4 || loginLength > 12 || !IsAlphanumeric(login) || CheckIfUserExistsInLDAP(login, LDAPSearchFilter.SAMAccountName))
                return false;
            return true;
        }

        public bool ValidateLDAPPassword(string password, string login)
        {
            return !(string.IsNullOrEmpty(password) || password.Length < 8 || !IsAlphanumeric(password) || CheckIfPasswordContainsLogin(password, login));
        }

        private bool CheckIfPasswordContainsLogin(string password, string login)
        {
            if (!string.IsNullOrEmpty(login) && !string.IsNullOrEmpty(password))
                return password.ToLower().IndexOf(login.ToLower()) >= 0 || password.ToLower().IndexOf(InvertString(login).ToLower()) >= 0;
            return true;
        }

        private string InvertString(string text)
        {
            char[] ArrayChar = text.ToCharArray();
            Array.Reverse(ArrayChar);
            return new string(ArrayChar);
        }

        private bool IsAlphanumeric(string text)
        {
            //TODO: expressão para permitir também ponto
            string alphatext = text.Replace(".", "");
            Regex pattern = new Regex("[^0-9a-zA-Z]");
            return !pattern.IsMatch(alphatext);
        }

        #endregion

        #region ClientsRegister

        public string ClientsRegisterReportListObject(List<FuncionalityClient> list, ref FormCollection form, int iCount)
        {
            JsonModel dtJson = new JsonModel();

            dtJson.sEcho = Convert.ToInt32(form["sEcho"]);

            if (list == null) list = new List<FuncionalityClient>();

            dtJson.iTotalRecords = iCount;
            dtJson.iTotalDisplayRecords = iCount;

            var tableValues = new List<string[]>();
            List<String> rowData = new List<String>();

            if (list != null)
            {   
                AccessControlHelper acHelper = new AccessControlHelper();
                foreach (FuncionalityClient functionalityClient in list)
                {
                    /*
                        <th>Codigo</th>
                        <th>Nome</th>
                        <th>Email</th>
                        <th>Email</th>
                     */
                    var sbEmail = new StringBuilder();
                    var sbCopy = new StringBuilder();

                    var registeredEmails = new List<string>();
                    var registeredCopy = new List<string>();

                    List<string> completeList = functionalityClient.Funcionalities.SelectMany(p => p.Emails.Select(t => t.Email)).ToList(); //Lista Completa 
                    List<string> filteredList = completeList.Except(registeredEmails).ToList(); //Remove-se os emails repetidos, somente para a listagem

                    registeredEmails.AddRange(filteredList);
                    registeredEmails.ForEach(p => sbEmail.Append("<span>" + p + "</span>"));

                    List<string> completeCopy = functionalityClient.Funcionalities.SelectMany(p => p.CopyTo.Select(t => t.Email)).ToList(); //Lista Completa de copia
                    List<string> filteredCopy = completeCopy.Except(registeredCopy).ToList(); //Remove-se os emails repetidos, somente para a listagem

                    registeredCopy.AddRange(filteredCopy);
                    registeredCopy.ForEach(p => sbCopy.Append("<span>" + p + "</span>"));

                    rowData.Add(functionalityClient.ClientCode.ToString());
                    rowData.Add(functionalityClient.ClientName);
                    rowData.Add(sbEmail.ToString());
                    rowData.Add(sbCopy.ToString());
                    rowData.Add(
                        string.Format(
                            "<a href=\"javascript:void(0);\" id=\"linkId{0}_Check\" title=\"editar cliente\" onclick=\"EditClient({0});return false;\" class=\"ico-editar\">editar</a><a href=\"javascript:void(0);\" rel=\"{0}\" title=\"visualizar\" class=\"ico-visualizar\">Visualizar</a><a href=\"javascript:void(0);\" rel=\"{0}\" title=\"excluir usuário\" class=\"ico-excluir\">excluir</a>", functionalityClient.ClientCode.ToString()));

                    tableValues.Add(rowData.ToArray());
                    rowData = new List<string>();
                }
            }
            dtJson.aaData = tableValues;
            return Newtonsoft.Json.JsonConvert.SerializeObject(dtJson);
        }

        #endregion
    }

    public class AccessControlAuthorizeAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (!httpContext.User.Identity.IsAuthenticated)
                return false;

            string[] path = httpContext.Request.Url.AbsolutePath.Split('/');
            int pathCount = path.Count();

            if (pathCount < 2)
                return false;

            string requestedElement = string.Format("{0}.{1}", path[1], pathCount > 2 ? path[2] : "Index").Trim();

            List<ACElement> allowedElements = AccessControlHelper.AllowedElements;

            if (allowedElements != null)
                 if (allowedElements.Count > 0 && allowedElements.Exists(delegate(ACElement e) { return e.Name.ToUpper() == requestedElement.ToUpper(); }))
                     return true;

            return false;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            #region Para usar o ReturnUrl no login
            //if (!filterContext.HttpContext.User.Identity.IsAuthenticated)
            //{
            //    base.HandleUnauthorizedRequest(filterContext);
            //    return;
            //}
            #endregion

            string redirect = "~/Account/Login";

            if (filterContext.HttpContext.User.Identity.IsAuthenticated && !string.IsNullOrEmpty(SessionHelper.CurrentUser.UserName))
            {
                filterContext.Controller.TempData["ErrorMessage"] = "Você não possui permissão.";
                redirect = "~/Home/";
            }

            filterContext.Result = new RedirectResult(redirect);
        }
    }
}