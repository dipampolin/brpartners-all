﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.269
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace QX3.Portal.WebSite.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class BBRentResources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal BBRentResources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("QX3.Portal.WebSite.Properties.BBRentResources", typeof(BBRentResources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;/Workbook&gt;.
        /// </summary>
        internal static string Footer {
            get {
                return ResourceManager.GetString("Footer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;Row ss:AutoFitHeight=&quot;0&quot;&gt;
        ///    &lt;Cell ss:StyleID=&quot;s98&quot;&gt;&lt;Data ss:Type=&quot;String&quot;&gt;Total&lt;/Data&gt;&lt;/Cell&gt;
        ///    &lt;Cell ss:StyleID=&quot;s91&quot;/&gt;
        ///    &lt;Cell ss:StyleID=&quot;s91&quot;/&gt;
        ///    &lt;Cell ss:StyleID=&quot;s91&quot;/&gt;
        ///    &lt;Cell ss:StyleID=&quot;s91&quot;/&gt;
        ///    &lt;Cell ss:StyleID=&quot;s92&quot;&gt;&lt;Data ss:Type=&quot;Number&quot;&gt;{0}&lt;/Data&gt;&lt;/Cell&gt;
        ///    &lt;Cell ss:StyleID=&quot;s91&quot;/&gt;
        ///   &lt;/Row&gt;
        ///  &lt;/Table&gt;
        /// &lt;/Worksheet&gt;.
        /// </summary>
        internal static string FooterProceeds {
            get {
                return ResourceManager.GetString("FooterProceeds", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;Row ss:AutoFitHeight=&quot;0&quot;&gt;
        ///    &lt;Cell ss:StyleID=&quot;s98&quot;&gt;&lt;Data ss:Type=&quot;String&quot;&gt;Total&lt;/Data&gt;&lt;/Cell&gt;
        ///    &lt;Cell ss:StyleID=&quot;s98&quot;/&gt;
        ///    &lt;Cell ss:StyleID=&quot;s98&quot;/&gt;
        ///    &lt;Cell ss:StyleID=&quot;s98&quot;/&gt;
        ///    &lt;Cell ss:StyleID=&quot;s98&quot;/&gt;
        ///    &lt;Cell ss:StyleID=&quot;s98&quot;/&gt;
        ///    &lt;Cell ss:StyleID=&quot;s92&quot;&gt;&lt;Data ss:Type=&quot;Number&quot;&gt;{0}&lt;/Data&gt;&lt;/Cell&gt;
        ///    &lt;Cell ss:StyleID=&quot;s92&quot;&gt;&lt;Data ss:Type=&quot;Number&quot;&gt;{1}&lt;/Data&gt;&lt;/Cell&gt;
        ///    &lt;Cell ss:StyleID=&quot;s92&quot;&gt;&lt;Data ss:Type=&quot;Number&quot;&gt;{2}&lt;/Data&gt;&lt;/Cell&gt;
        ///    &lt;Cell ss:StyleID=&quot;s92&quot;&gt;&lt;Data ss:Type=&quot;Number&quot;&gt;{3}&lt;/D [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string FooterSettlement {
            get {
                return ResourceManager.GetString("FooterSettlement", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;Row ss:AutoFitHeight=&quot;0&quot;&gt;
        ///    &lt;Cell ss:StyleID=&quot;s98&quot;&gt;&lt;Data ss:Type=&quot;String&quot;&gt;Total&lt;/Data&gt;&lt;/Cell&gt;
        ///    &lt;Cell ss:StyleID=&quot;s98&quot;/&gt;
        ///    &lt;Cell ss:StyleID=&quot;s98&quot;/&gt;
        ///    &lt;Cell ss:StyleID=&quot;s98&quot;/&gt;
        ///    &lt;Cell ss:StyleID=&quot;s98&quot;/&gt;
        ///    &lt;Cell ss:StyleID=&quot;s98&quot;/&gt;
        ///    &lt;Cell ss:StyleID=&quot;s92&quot;&gt;&lt;Data ss:Type=&quot;Number&quot;&gt;{0}&lt;/Data&gt;&lt;/Cell&gt;
        ///    &lt;Cell ss:StyleID=&quot;s92&quot;&gt;&lt;Data ss:Type=&quot;Number&quot;&gt;{1}&lt;/Data&gt;&lt;/Cell&gt;
        ///    &lt;Cell ss:StyleID=&quot;s92&quot;&gt;&lt;Data ss:Type=&quot;Number&quot;&gt;{2}&lt;/Data&gt;&lt;/Cell&gt;
        ///    &lt;Cell ss:StyleID=&quot;s92&quot;&gt;&lt;Data ss:Type=&quot;Number&quot;&gt;{3}&lt;/D [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string FooterTransfer {
            get {
                return ResourceManager.GetString("FooterTransfer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;?xml version=&quot;1.0&quot;?&gt;
        ///&lt;?mso-application progid=&quot;Excel.Sheet&quot;?&gt;
        ///&lt;Workbook xmlns=&quot;urn:schemas-microsoft-com:office:spreadsheet&quot;
        /// xmlns:o=&quot;urn:schemas-microsoft-com:office:office&quot;
        /// xmlns:x=&quot;urn:schemas-microsoft-com:office:excel&quot;
        /// xmlns:ss=&quot;urn:schemas-microsoft-com:office:spreadsheet&quot;
        /// xmlns:html=&quot;http://www.w3.org/TR/REC-html40&quot;&gt;
        /// &lt;DocumentProperties xmlns=&quot;urn:schemas-microsoft-com:office:office&quot;&gt;
        ///  &lt;Author&gt;#Author#&lt;/Author&gt;
        ///  &lt;Created&gt;#Date#&lt;/Created&gt;
        /// &lt;/DocumentProperties&gt;
        /// &lt;Styles&gt;
        ///  &lt;Style s [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string Header {
            get {
                return ResourceManager.GetString("Header", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;Worksheet ss:Name=&quot;Proventos&quot;&gt;
        ///  &lt;Table x:FullColumns=&quot;1&quot; x:FullRows=&quot;1&quot; ss:StyleID=&quot;s62&quot; ss:DefaultRowHeight=&quot;15&quot;&gt;
        ///   &lt;Column ss:StyleID=&quot;s62&quot; ss:AutoFitWidth=&quot;0&quot; ss:Width=&quot;69.75&quot;/&gt;
        ///   &lt;Column ss:StyleID=&quot;s62&quot; ss:AutoFitWidth=&quot;0&quot; ss:Width=&quot;65.25&quot; ss:Span=&quot;1&quot;/&gt;
        ///   &lt;Column ss:Index=&quot;4&quot; ss:StyleID=&quot;s62&quot; ss:AutoFitWidth=&quot;0&quot; ss:Width=&quot;54&quot;/&gt;
        ///   &lt;Column ss:StyleID=&quot;s62&quot; ss:Width=&quot;66&quot;/&gt;
        ///   &lt;Column ss:StyleID=&quot;s62&quot; ss:AutoFitWidth=&quot;0&quot; ss:Width=&quot;84.75&quot;/&gt;
        ///   &lt;Column ss:StyleID=&quot;s62&quot; ss:AutoFitWidth=&quot;0&quot; ss:Wid [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string HeaderProceeds {
            get {
                return ResourceManager.GetString("HeaderProceeds", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;Worksheet ss:Name=&quot;Liquida&amp;#231;&amp;#227;o&quot;&gt;
        ///  &lt;Table x:FullColumns=&quot;1&quot; x:FullRows=&quot;1&quot; ss:StyleID=&quot;s62&quot; ss:DefaultRowHeight=&quot;15&quot;&gt;
        ///   &lt;Column ss:StyleID=&quot;s62&quot; ss:AutoFitWidth=&quot;0&quot; ss:Width=&quot;69.75&quot;/&gt;
        ///   &lt;Column ss:StyleID=&quot;s62&quot; ss:AutoFitWidth=&quot;0&quot; ss:Width=&quot;65.25&quot; ss:Span=&quot;2&quot;/&gt;
        ///   &lt;Column ss:Index=&quot;5&quot; ss:StyleID=&quot;s62&quot; ss:AutoFitWidth=&quot;0&quot; ss:Width=&quot;54&quot;/&gt;
        ///   &lt;Column ss:StyleID=&quot;s62&quot; ss:Width=&quot;66&quot;/&gt;
        ///   &lt;Column ss:StyleID=&quot;s62&quot; ss:Width=&quot;81&quot; ss:Span=&quot;2&quot;/&gt;
        ///   &lt;Column ss:Index=&quot;10&quot; ss:StyleID=&quot;s62&quot; ss:Width=&quot;82 [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string HeaderSettlement {
            get {
                return ResourceManager.GetString("HeaderSettlement", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;Worksheet ss:Name=&quot;Repasse&quot;&gt;
        ///  &lt;Table x:FullColumns=&quot;1&quot; x:FullRows=&quot;1&quot; ss:StyleID=&quot;s62&quot; ss:DefaultRowHeight=&quot;15&quot;&gt;
        ///   &lt;Column ss:StyleID=&quot;s62&quot; ss:AutoFitWidth=&quot;0&quot; ss:Width=&quot;69.75&quot;/&gt;
        ///   &lt;Column ss:StyleID=&quot;s62&quot; ss:AutoFitWidth=&quot;0&quot; ss:Width=&quot;65.25&quot; ss:Span=&quot;2&quot;/&gt;
        ///   &lt;Column ss:Index=&quot;5&quot; ss:StyleID=&quot;s62&quot; ss:AutoFitWidth=&quot;0&quot; ss:Width=&quot;54&quot;/&gt;
        ///   &lt;Column ss:StyleID=&quot;s62&quot; ss:Width=&quot;66&quot;/&gt;
        ///   &lt;Column ss:StyleID=&quot;s62&quot; ss:Width=&quot;81&quot; ss:Span=&quot;2&quot;/&gt;
        ///   &lt;Column ss:Index=&quot;10&quot; ss:StyleID=&quot;s62&quot; ss:Width=&quot;82.5&quot;/&gt;
        ///   &lt;Co [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string HeaderTransfer {
            get {
                return ResourceManager.GetString("HeaderTransfer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;Row ss:AutoFitHeight=&quot;0&quot;&gt;
        ///    &lt;Cell ss:StyleID=&quot;s70&quot;&gt;&lt;Data ss:Type=&quot;String&quot;&gt;{0}&lt;/Data&gt;&lt;/Cell&gt;
        ///    &lt;Cell ss:StyleID=&quot;s71&quot;&gt;&lt;Data ss:Type=&quot;Number&quot;&gt;{1}&lt;/Data&gt;&lt;/Cell&gt;
        ///    &lt;Cell ss:StyleID=&quot;s71&quot;&gt;&lt;Data ss:Type=&quot;Number&quot;&gt;{2}&lt;/Data&gt;&lt;/Cell&gt;
        ///    &lt;Cell ss:StyleID=&quot;s72&quot;&gt;&lt;Data ss:Type=&quot;String&quot;&gt;{3}&lt;/Data&gt;&lt;/Cell&gt;
        ///    &lt;Cell ss:StyleID=&quot;s73&quot;&gt;&lt;Data ss:Type=&quot;Number&quot;&gt;{4}&lt;/Data&gt;&lt;/Cell&gt;
        ///    &lt;Cell ss:StyleID=&quot;s74&quot;&gt;&lt;Data ss:Type=&quot;Number&quot;&gt;{5}&lt;/Data&gt;&lt;/Cell&gt;
        ///    &lt;Cell ss:StyleID=&quot;s72&quot;&gt;&lt;Data ss:Type=&quot;String&quot;&gt;{6}&lt;/Data&gt;&lt;/Cell&gt;
        ///  [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string ItemProceedsEven {
            get {
                return ResourceManager.GetString("ItemProceedsEven", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;Row ss:AutoFitHeight=&quot;0&quot;&gt;
        ///    &lt;Cell ss:StyleID=&quot;s75&quot;&gt;&lt;Data ss:Type=&quot;String&quot;&gt;{0}&lt;/Data&gt;&lt;/Cell&gt;
        ///    &lt;Cell ss:StyleID=&quot;s76&quot;&gt;&lt;Data ss:Type=&quot;Number&quot;&gt;{1}&lt;/Data&gt;&lt;/Cell&gt;
        ///    &lt;Cell ss:StyleID=&quot;s76&quot;&gt;&lt;Data ss:Type=&quot;Number&quot;&gt;{2}&lt;/Data&gt;&lt;/Cell&gt;
        ///    &lt;Cell ss:StyleID=&quot;s77&quot;&gt;&lt;Data ss:Type=&quot;String&quot;&gt;{3}&lt;/Data&gt;&lt;/Cell&gt;
        ///    &lt;Cell ss:StyleID=&quot;s78&quot;&gt;&lt;Data ss:Type=&quot;Number&quot;&gt;{4}&lt;/Data&gt;&lt;/Cell&gt;
        ///    &lt;Cell ss:StyleID=&quot;s79&quot;&gt;&lt;Data ss:Type=&quot;Number&quot;&gt;{5}&lt;/Data&gt;&lt;/Cell&gt;
        ///    &lt;Cell ss:StyleID=&quot;s77&quot;&gt;&lt;Data ss:Type=&quot;String&quot;&gt;{6}&lt;/Data&gt;&lt;/Cell&gt;
        ///  [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string ItemProceedsOdd {
            get {
                return ResourceManager.GetString("ItemProceedsOdd", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;Row ss:AutoFitHeight=&quot;0&quot;&gt;
        ///    &lt;Cell ss:StyleID=&quot;s70&quot;&gt;&lt;Data ss:Type=&quot;String&quot;&gt;{0}&lt;/Data&gt;&lt;/Cell&gt;
        ///    &lt;Cell ss:StyleID=&quot;s71&quot;&gt;&lt;Data ss:Type=&quot;Number&quot;&gt;{1}&lt;/Data&gt;&lt;/Cell&gt;
        ///    &lt;Cell ss:StyleID=&quot;s71&quot;&gt;&lt;Data ss:Type=&quot;Number&quot;&gt;{2}&lt;/Data&gt;&lt;/Cell&gt;
        ///    &lt;Cell ss:StyleID=&quot;s71&quot;&gt;&lt;Data ss:Type=&quot;Number&quot;&gt;{3}&lt;/Data&gt;&lt;/Cell&gt;
        ///    &lt;Cell ss:StyleID=&quot;s72&quot;&gt;&lt;Data ss:Type=&quot;String&quot;&gt;{4}&lt;/Data&gt;&lt;/Cell&gt;
        ///    &lt;Cell ss:StyleID=&quot;s73&quot;&gt;&lt;Data ss:Type=&quot;Number&quot;&gt;{5}&lt;/Data&gt;&lt;/Cell&gt;
        ///    &lt;Cell ss:StyleID=&quot;s74&quot;&gt;&lt;Data ss:Type=&quot;Number&quot;&gt;{6}&lt;/Data&gt;&lt;/Cell&gt;
        ///  [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string ItemSettlementEven {
            get {
                return ResourceManager.GetString("ItemSettlementEven", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;Row ss:AutoFitHeight=&quot;0&quot;&gt;
        ///    &lt;Cell ss:StyleID=&quot;s75&quot;&gt;&lt;Data ss:Type=&quot;String&quot;&gt;{0}&lt;/Data&gt;&lt;/Cell&gt;
        ///    &lt;Cell ss:StyleID=&quot;s76&quot;&gt;&lt;Data ss:Type=&quot;Number&quot;&gt;{1}&lt;/Data&gt;&lt;/Cell&gt;
        ///    &lt;Cell ss:StyleID=&quot;s76&quot;&gt;&lt;Data ss:Type=&quot;Number&quot;&gt;{2}&lt;/Data&gt;&lt;/Cell&gt;
        ///    &lt;Cell ss:StyleID=&quot;s76&quot;&gt;&lt;Data ss:Type=&quot;Number&quot;&gt;{3}&lt;/Data&gt;&lt;/Cell&gt;
        ///    &lt;Cell ss:StyleID=&quot;s77&quot;&gt;&lt;Data ss:Type=&quot;String&quot;&gt;{4}&lt;/Data&gt;&lt;/Cell&gt;
        ///    &lt;Cell ss:StyleID=&quot;s78&quot;&gt;&lt;Data ss:Type=&quot;Number&quot;&gt;{5}&lt;/Data&gt;&lt;/Cell&gt;
        ///    &lt;Cell ss:StyleID=&quot;s79&quot;&gt;&lt;Data ss:Type=&quot;Number&quot;&gt;{6}&lt;/Data&gt;&lt;/Cell&gt;
        ///  [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string ItemSettlementOdd {
            get {
                return ResourceManager.GetString("ItemSettlementOdd", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;Row ss:AutoFitHeight=&quot;0&quot;&gt;
        ///    &lt;Cell ss:StyleID=&quot;s70&quot;&gt;&lt;Data ss:Type=&quot;String&quot;&gt;{0}&lt;/Data&gt;&lt;/Cell&gt;
        ///    &lt;Cell ss:StyleID=&quot;s71&quot;&gt;&lt;Data ss:Type=&quot;Number&quot;&gt;{1}&lt;/Data&gt;&lt;/Cell&gt;
        ///    &lt;Cell ss:StyleID=&quot;s71&quot;&gt;&lt;Data ss:Type=&quot;Number&quot;&gt;{2}&lt;/Data&gt;&lt;/Cell&gt;
        ///    &lt;Cell ss:StyleID=&quot;s71&quot;&gt;&lt;Data ss:Type=&quot;Number&quot;&gt;{3}&lt;/Data&gt;&lt;/Cell&gt;
        ///    &lt;Cell ss:StyleID=&quot;s72&quot;&gt;&lt;Data ss:Type=&quot;String&quot;&gt;{4}&lt;/Data&gt;&lt;/Cell&gt;
        ///    &lt;Cell ss:StyleID=&quot;s73&quot;&gt;&lt;Data ss:Type=&quot;Number&quot;&gt;{5}&lt;/Data&gt;&lt;/Cell&gt;
        ///    &lt;Cell ss:StyleID=&quot;s74&quot;&gt;&lt;Data ss:Type=&quot;Number&quot;&gt;{6}&lt;/Data&gt;&lt;/Cell&gt;
        ///  [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string ItemTransferEven {
            get {
                return ResourceManager.GetString("ItemTransferEven", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;Row ss:AutoFitHeight=&quot;0&quot;&gt;
        ///    &lt;Cell ss:StyleID=&quot;s75&quot;&gt;&lt;Data ss:Type=&quot;String&quot;&gt;{0}&lt;/Data&gt;&lt;/Cell&gt;
        ///    &lt;Cell ss:StyleID=&quot;s76&quot;&gt;&lt;Data ss:Type=&quot;Number&quot;&gt;{1}&lt;/Data&gt;&lt;/Cell&gt;
        ///    &lt;Cell ss:StyleID=&quot;s76&quot;&gt;&lt;Data ss:Type=&quot;Number&quot;&gt;{2}&lt;/Data&gt;&lt;/Cell&gt;
        ///    &lt;Cell ss:StyleID=&quot;s76&quot;&gt;&lt;Data ss:Type=&quot;Number&quot;&gt;{3}&lt;/Data&gt;&lt;/Cell&gt;
        ///    &lt;Cell ss:StyleID=&quot;s77&quot;&gt;&lt;Data ss:Type=&quot;String&quot;&gt;{4}&lt;/Data&gt;&lt;/Cell&gt;
        ///    &lt;Cell ss:StyleID=&quot;s78&quot;&gt;&lt;Data ss:Type=&quot;Number&quot;&gt;{5}&lt;/Data&gt;&lt;/Cell&gt;
        ///    &lt;Cell ss:StyleID=&quot;s79&quot;&gt;&lt;Data ss:Type=&quot;Number&quot;&gt;{6}&lt;/Data&gt;&lt;/Cell&gt;
        ///  [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string ItemTransferOdd {
            get {
                return ResourceManager.GetString("ItemTransferOdd", resourceCulture);
            }
        }
    }
}
