﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("QX3.Portal.WebSite")]
[assembly: AssemblyDescription("BrPartners - Portal Corretora")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("BrPartners")]
[assembly: AssemblyProduct("QX3.Portal.WebSite")]
[assembly: AssemblyCopyright("Copyright © BrPartners 2013")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("077cf377-9636-43a1-8ba5-561874200f60")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("1.5.*")]

// Versão 1.2.5172 - Ajustes na página de suitability do cliente
// - Criação do perfil para responder o suitability
// - Criação da Home para o perfil suitabilty
// - Ajuste no cálculo do questionário
// - Mensagem para o cliente desenquadrado

// Versão 1.2.5197 - 24/03/2014 - Suitability / Nota de corretagem
// - Ajuste no cálculo do questionário
// - Página de resultado não estava exibindo
// - Nota de corretagem com imposto negativo

// Versão 1.2.5198 - 26/03/2014 - Nota de corretagem
// - Nota de corretagem com imposto negativo

// Versão 1.2.5199 - 27/03/2014 - Nota de corretagem
// - Nota de corretagem Projeção se negativo, precisa zerar

// Versão 1.2.5200 - 28/03/2014 - Suitability 
// - Ajuste no script do cálculo do questionário

// Versão 1.2.5203 - 31/03/2014 - Suitability 
// - Ajuste no Desenquadramento

// Versão 1.2.5204 - 01/04/2014 - Suitability 
// - Ajuste no Formato da Data do Desenquadramento

// Versão 1.2.5205 - 01/04/2014 - Suitability Compliance
// - Inclusão da coluna nome.