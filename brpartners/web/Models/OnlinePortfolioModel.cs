﻿using System;
using System.Collections.Generic;
using QX3.Portal.Contracts.DataContracts;

namespace QX3.Portal.WebSite.Models
{
    [Serializable]
    public class PortfolioContactsClientContent
    {
        public int Index { get; set; }
        public string ClientCode { get; set; }
        public string ClientName { get; set; }
        public string ClientPhone { get; set; }
        public string ClientEmail { get; set; }
        public string HtmlItems { get; set; }
        public int Length { get; set; }
        public string Legend { get; set; }
    }


    [Serializable]
    public class PortfolioClientContactsListItem
    {
        public int Index { get; set; }
        public string Date { get; set; }
        public string Operator { get; set; }
        public string Message { get; set; }
    }

    [Serializable]
    public class PortfolioClientListItem
    {
        public int ClientCode { get; set; }
        public string ClientName { get; set; }
        public int CalculationType { get; set; }
        public string CalculationDescription { get; set; }
        public bool SendHistory { get; set; }
        public string SendAttachType { get; set; }
        public string StartDate { get; set; }
        public decimal InitialValue { get; set; }
        public string Operators { get; set; }
    }

    #region Portfolio Position

    [Serializable]
    public class PortfolioPositionClientContent
    {
        public int Index { get; set; }
        public string ClientCode { get; set; }
        public string ClientName { get; set; }
        public string ClientPhone { get; set; }
        public string ClientEmail { get; set; }
        public string ClientStartDate { get; set; }
        public string ClientStartValue { get; set; }
        public string ClientBound { get; set; }
        public string Comments { get; set; }
        public string HtmlTableItems { get; set; }
        public PortfolioPositionFinancialContent Financial { get; set; }
        public PortfolioPositionIbovContent Ibov { get; set; }
        public PortfolioPositionDiscountContent Discount { get; set; }
        public List<PortfolioPositionChartItem> Chart { get; set; }
        public List<PortfolioPositionMarket> Markets { get; set; }
        public string CCDiscount { get; set; }
        public int Length { get; set; }
        public string Legend { get; set; }
        public string StockPrices { get; set; }
        public string Errors { get; set; }
    }

    [Serializable]
    public class PortfolioPositionMarket
    {
        public string Name { get; set; }
        public List<PortfolioPositionItem> Items { get; set; }
    }

    [Serializable]
    public class PortfolioPositionFinancialContent
    {
        public string Cash { get; set; }
        public string Position { get; set; }
        public string Total { get; set; }
    }

    [Serializable]
    public class PortfolioPositionIbovContent
    {
        public string TodayDate { get; set; }
        public string TodayPoints { get; set; }
        public string TodayVariation { get; set; }
        public string Yesterday { get; set; }
        public string YearDate { get; set; }
        public string YearPoints { get; set; }
        public string YearVariation { get; set; }
    }

    [Serializable]
    public class PortfolioPositionDiscountContent
    {
        public string Discount { get; set; }
        public string DayTrade { get; set; }
        public string Available { get; set; }
        public string DailyOperations { get; set; }
        public string D1 { get; set; }
        public string D2 { get; set; }
        public string D3 { get; set; }
    }

    [Serializable]
    public class PortfolioPositionChartItem
    {
        public decimal Percentage { get; set; }
        public string SectorName { get; set; }
        public string Variation { get; set; }
        public string Allocation { get; set; }
    }

    #endregion

    [Serializable]
    public class PortfolioAdminPosition
    {
        public string ClientCode { get; set; }
        public string ClientName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public List<PortfolioAdminPositionListItem> Position { get; set; }
    }

    [Serializable]
    public class PortfolioAdminPositionListItem
    {
        public string Id { get; set; }

        public string StockCode { get; set; }
        public string Operation { get; set; }
        public string MarketType { get; set; }
        public string PortfolioNumber { get; set; }
        public string Quantity { get; set; }

        public string Price { get; set; }
        public string NetPrice { get; set; }
        public string PositionDate { get; set; }

        public string DueDate { get; set; }
        public string RollOverDate { get; set; }
        public string LongShort { get; set; }
    }

    [Serializable]
    public class PortfolioHistoryClientContent
    {
        public int Index { get; set; }
        public string ClientCode { get; set; }
        public string ClientName { get; set; }
        public string ClientPhone { get; set; }
        public string ClientEmail { get; set; }
        public string HtmlTableItems { get; set; }
        public int Length { get; set; }
        public string Legend { get; set; }
        public List<PortfolioHistoryPeriod> Items { get; set; }
    }

    [Serializable]
    public class PortfolioHistoryPeriod
    {
        public string Period { get; set; }
        public List<PortfolioPositionItem> Items { get; set; }
        public PortfolioHistoryPeriodBalance Balance { get; set; }
    }

    [Serializable]
    public class PortfolioHistoryPeriodBalance
    {
        public decimal Quantity { get; set; }
        public decimal BuyNetValue { get; set; }
        public decimal SellNetValue { get; set; }
        public decimal ProfitLoss { get; set; }
        public decimal Percentage { get; set; }
    }
}