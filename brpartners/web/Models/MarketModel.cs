﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Security;

namespace QX3.Portal.WebSite.Models
{
	[Serializable]
     public class GroupByItem
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }

}
