﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using QX3.Portal.Contracts.DataContracts;
using QX3.Portal.WebSite.Helper;

namespace QX3.Portal.WebSite.Models
{
    public class ContractInfo
    {
        #region Propriedades

        //public CustomerType CustomerType { get; set; }
        public List<ContractField> ContractFields { get; set; }
        public string PDFPath { get; set; }
        public string PDFModelPath { get; set; }
        public string ContractCode { get; set; }
        public string FileName { get; set; }
        public string BrokerName { get; set; }

        #endregion
    }

    public class ContractField
    {
        #region Propriedades

        public string Field { get; set; }
        public string FieldContent { get; set; }

        #endregion
    }

    public abstract class PDFController
    {
        public abstract void CreateRegistrationPF(IndividualPerson searchUser, ref ContractInfo PDFData);
    }
}