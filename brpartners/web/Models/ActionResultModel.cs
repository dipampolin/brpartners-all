﻿using System;

namespace QX3.Portal.WebSite.Models
{
	[Serializable]
    public class ActionResultModel
    {
        public string Status { get; set; }
        public String[] Messages { get; set; }
    }   
}