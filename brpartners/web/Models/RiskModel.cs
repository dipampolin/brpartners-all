﻿using System;
using QX3.Portal.Contracts.DataContracts;
using System.Collections.Generic;

namespace QX3.Portal.WebSite.Models
{
    [Serializable]
    public class GuaranteeListItem
    {
        public CommonType Client { get; set; }
        public string Assessor { get; set; }
        public List<GuaranteeListSubItem> Items { get; set; }
        public string Total { get; set; }
        public string Used { get; set; }
        public string Free { get; set; }
        public string AfterRealize { get; set; }
        public int Index { get; set; }
        public GuaranteeBalance Balance { get; set; }
    }

    [Serializable]
    public class GuaranteeListSubItem : GuaranteeItem
    {
        public int Index { get; set; }
    }

    [Serializable]
    public class GuaranteeClientContent
    {
        public int Index { get; set; }
        public string Client { get; set; }
        public string Assessor { get; set; }
        public string HtmlBalanceItems { get; set; }
        public string HtmlItems { get; set; }
        public int Length { get; set; }
        public string Legend { get; set; }
        public bool InGuarantee { get; set; }
    }

    [Serializable]
    public class GuaranteeSuggest
    {
        public string Guarantee { get; set; }
        public string Available { get; set; }
        public string Total { get; set; }
        public string UnitPrice { get; set; }
        public string IsQuantity { get; set; }
    }


}
