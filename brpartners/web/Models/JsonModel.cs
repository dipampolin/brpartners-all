﻿using System.Collections.Generic;
using System;

namespace QX3.Portal.WebSite.Models
{
	[Serializable]
    public class JsonModel
    {
        public int sEcho { get; set; }
        public int iTotalRecords { get; set; }
        public int iTotalDisplayRecords { get; set; }
        public List<string[]> aaData { get; set; }
    }

	[Serializable]
	public class JsonModelSubscription : JsonModel
	{
		public int TotalSubscription { get; set; }
		public bool FlagChangeFilterSubscription { get; set; }
	}

    [Serializable]
    public class JsonModelPrivate : JsonModel
    {
        public string RefDate { get; set; }
    }
}