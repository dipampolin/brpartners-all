﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QX3.Portal.WebSite.Models
{
    [Serializable]
    public class OrdersListItem
    {
        public string ID { get; set; }
        public string ClientFrom { get; set; }
        public string ClientTo { get; set; }
        public string Comments { get; set; }
        public string Date { get; set; }
        public string Description { get; set; }
        public string Details { get; set; }
        public string Responsible { get; set; }
        public string Status { get; set; }
        public string Type { get; set; }
        public string UserID { get; set; }
    }
}