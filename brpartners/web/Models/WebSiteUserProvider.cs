﻿using System;
using System.Collections.Generic;
using QX3.Spinnex.Common.Services.Logging;
using QX3.Portal.WebSite.Helper;

namespace QX3.Portal.WebSite.Models
{
	[Serializable]
	public class ConcordiaUserProvider : IUserInformationProvider
	{
		#region IUserInformationProvider Members

		public string UserID
		{
			get
			{
				var user = SessionHelper.CurrentUser;
				if (user == null)
					return null;
				return user.ID.ToString();
			}
		}

		public string UserName
		{
			get
			{

				var user = SessionHelper.CurrentUser;
				if (user == null)
					return null;
				return user.UserName;
			}
		}

		public string DisplayName
		{
			get
			{
				var user = SessionHelper.CurrentUser;
				if (user == null)
					return null;
				return user.Name;
			}
		}

		#endregion

		#region IUserInformationProvider Members


		public KeyValuePair<string, string>[] Properties
		{
			get { throw new NotImplementedException(); }
		}

		#endregion
	}
}