﻿using System;
using System.Configuration;

namespace QX3.Portal.WebSite.Models.Channel
{
    public class ChannelProviderFactory<T>
    {
        public static IChannelProvider<T> Create(string channelProviderClassName)
        {
            Type type = Type.GetType(ConfigurationManager.AppSettings[channelProviderClassName]);
            return (IChannelProvider<T>)Activator.CreateInstance(type);
        }
    }
}