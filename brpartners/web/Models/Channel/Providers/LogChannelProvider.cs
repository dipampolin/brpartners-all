﻿using System;
using System.ServiceModel;
using System.Configuration;
using QX3.Portal.WebSite.LogService;

namespace QX3.Portal.WebSite.Models.Channel.Providers
{
	public class LogChannelProvider : IChannelProvider<ILogContractChannel>
    {
        public ChannelFactory<ILogContractChannel> GetChannel()
        {
            try
            {
                var service = new LogContractClient();
                var cnn = new ChannelFactory<ILogContractChannel>("WSHttpBinding_ILogContract");
                cnn.Credentials.UserName.UserName = ConfigurationManager.AppSettings["AppUserName"];
                cnn.Credentials.UserName.Password = ConfigurationManager.AppSettings["AppPassword"];

                return cnn;
            }
            catch
            {
                throw new Exception();
            }
        }
    }
}