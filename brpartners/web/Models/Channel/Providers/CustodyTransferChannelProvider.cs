﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using QX3.Portal.WebSite.CustodyTransferService;
using System.ServiceModel;
using System.Configuration;

namespace QX3.Portal.WebSite.Models.Channel.Providers
{
    public class CustodyTransferChannelProvider: IChannelProvider<ICustodyTransferContractChannel>
    {
        public ChannelFactory<ICustodyTransferContractChannel> GetChannel()
        {
            try
            {
                var cnn = new ChannelFactory<ICustodyTransferContractChannel>("WSHttpBinding_ICustodyTransferContract");
                cnn.Credentials.UserName.UserName = ConfigurationManager.AppSettings["AppUserName"];
                cnn.Credentials.UserName.Password = ConfigurationManager.AppSettings["AppPassword"];

                return cnn;
            }
            catch
            {
                throw new Exception();
            }
        }
    }
}