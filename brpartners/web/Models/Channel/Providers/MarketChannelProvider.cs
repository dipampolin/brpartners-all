﻿using System;
using System.ServiceModel;
using System.Configuration;
using QX3.Portal.WebSite.MarketService;

namespace QX3.Portal.WebSite.Models.Channel.Providers
{
	public class MarketChannelProvider : IChannelProvider<IMarketContractChannel>
    {
        public ChannelFactory<IMarketContractChannel> GetChannel()
        {
            try
            {
				var service = new MarketContractClient();
				var cnn = new ChannelFactory<IMarketContractChannel>("WSHttpBinding_IMarketContract");
                cnn.Credentials.UserName.UserName = ConfigurationManager.AppSettings["AppUserName"];
                cnn.Credentials.UserName.Password = ConfigurationManager.AppSettings["AppPassword"];

                return cnn;
            }
            catch
            {
                throw new Exception();
            }
        }
    }
}