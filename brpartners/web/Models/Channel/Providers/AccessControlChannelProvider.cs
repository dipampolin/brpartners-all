﻿using System;
using System.ServiceModel;
using System.Configuration;
using QX3.Portal.WebSite.AccessControlService;

namespace QX3.Portal.WebSite.Models.Channel.Providers
{
	public class AccessControlChannelProvider : IChannelProvider<IAccessControlContractChannel>
    {
        public ChannelFactory<IAccessControlContractChannel> GetChannel()
        {
            try
            {
				var service = new AccessControlContractClient();
				var cnn = new ChannelFactory<IAccessControlContractChannel>("WSHttpBinding_IAccessControlContract");
                cnn.Credentials.UserName.UserName = ConfigurationManager.AppSettings["AppUserName"];
                cnn.Credentials.UserName.Password = ConfigurationManager.AppSettings["AppPassword"];

                return cnn;
            }
            catch
            {
                throw new Exception();
            }
        }
    }
}