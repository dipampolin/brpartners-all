﻿using System;
using System.ServiceModel;
using System.Configuration;
using QX3.Portal.WebSite.PositionService;


namespace QX3.Portal.WebSite.Models.Channel.Providers
{
    public class PositionChannelProvider : IChannelProvider<IPositionContractChannel>
    {
        public ChannelFactory<IPositionContractChannel> GetChannel()
        {
            try
            {
                var cnn = new ChannelFactory<IPositionContractChannel>("WSHttpBinding_IPositionContract");
                cnn.Credentials.UserName.UserName = ConfigurationManager.AppSettings["AppUserName"];
                cnn.Credentials.UserName.Password = ConfigurationManager.AppSettings["AppPassword"];

                return cnn;
            }
            catch
            {
                throw new Exception();
            }
        }
    }
}