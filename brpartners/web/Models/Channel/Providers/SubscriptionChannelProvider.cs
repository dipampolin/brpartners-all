﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;
using System.Configuration;
using QX3.Portal.WebSite.SubscriptionService;

namespace QX3.Portal.WebSite.Models.Channel.Providers
{
    public class SubscriptionChannelProvider: IChannelProvider<ISubscriptionContractChannel>
    {
        public ChannelFactory<ISubscriptionContractChannel> GetChannel() 
        {
            try
            {                
                var cnn = new ChannelFactory<ISubscriptionContractChannel>("WSHttpBinding_ISubscriptionContract");
                cnn.Credentials.UserName.UserName = ConfigurationManager.AppSettings["AppUserName"];
                cnn.Credentials.UserName.Password = ConfigurationManager.AppSettings["AppPassword"];
                return cnn;
            }
            catch
            {
                throw new Exception();
            }
        }
    }
}