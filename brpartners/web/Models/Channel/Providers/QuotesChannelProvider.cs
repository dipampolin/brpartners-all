﻿using System;
using System.ServiceModel;
using System.Configuration;
using QX3.Portal.WebSite.LogService;
using QX3.Portal.WebSite.QuotesService;

namespace QX3.Portal.WebSite.Models.Channel.Providers
{
	public class QuotesChannelProvider : IChannelProvider<IQuotesContractChannel>
    {
        public ChannelFactory<IQuotesContractChannel> GetChannel()
        {
            try
            {
                var service = new QuotesContractClient();
                var cnn = new ChannelFactory<IQuotesContractChannel>("WSHttpBinding_IQuotesContract");
                cnn.Credentials.UserName.UserName = ConfigurationManager.AppSettings["AppUserName"];
                cnn.Credentials.UserName.Password = ConfigurationManager.AppSettings["AppPassword"];

                return cnn;
            }
            catch
            {
                throw new Exception();
            }
        }
    }
}