﻿using System;
using System.ServiceModel;
using System.Configuration;
using QX3.Portal.WebSite.GuaranteeService;

namespace QX3.Portal.WebSite.Models.Channel.Providers
{
    public class GuaranteeChannelProvider : IChannelProvider<IGuaranteeContractChannel>
    {
        public ChannelFactory<IGuaranteeContractChannel> GetChannel()
        {
            try
            {
                var cnn = new ChannelFactory<IGuaranteeContractChannel>("WSHttpBinding_IGuaranteeContract");
                cnn.Credentials.UserName.UserName = ConfigurationManager.AppSettings["AppUserName"];
                cnn.Credentials.UserName.Password = ConfigurationManager.AppSettings["AppPassword"];

                return cnn;
            }
            catch
            {
                throw new Exception();
            }
        }
    }
}