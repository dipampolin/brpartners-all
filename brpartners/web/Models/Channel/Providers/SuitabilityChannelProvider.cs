﻿using System;
using System.ServiceModel;
using System.Configuration;
using QX3.Portal.WebSite.SuitabilityService;


namespace QX3.Portal.WebSite.Models.Channel.Providers
{
    public class SuitabilityChannelProvider : IChannelProvider<ISuitabilityContractChannel>
    {
        public ChannelFactory<ISuitabilityContractChannel> GetChannel()
        {
            try
            {
                var cnn = new ChannelFactory<ISuitabilityContractChannel>("WSHttpBinding_ISuitabilityContract");
                cnn.Credentials.UserName.UserName = ConfigurationManager.AppSettings["AppUserName"];
                cnn.Credentials.UserName.Password = ConfigurationManager.AppSettings["AppPassword"];

                return cnn;
            }
            catch
            {
                throw new Exception();
            }
        }
    }
}

