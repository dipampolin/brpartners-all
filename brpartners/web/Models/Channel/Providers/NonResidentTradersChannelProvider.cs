﻿using System;
using System.ServiceModel;
using System.Configuration;
using QX3.Portal.WebSite.NonResidentTradersService;

namespace QX3.Portal.WebSite.Models.Channel.Providers
{
    public class NonResidentTradersChannelProvider : IChannelProvider<INonResidentTradersServiceContractChannel>
    {
        public ChannelFactory<INonResidentTradersServiceContractChannel> GetChannel()
        {
            try
            {
                var service = new NonResidentTradersServiceContractClient();
                var cnn = new ChannelFactory<INonResidentTradersServiceContractChannel>("WSHttpBinding_INonResidentTradersServiceContract");
                cnn.Credentials.UserName.UserName = ConfigurationManager.AppSettings["AppUserName"];
                cnn.Credentials.UserName.Password = ConfigurationManager.AppSettings["AppPassword"];

                return cnn;
            }
            catch
            {
                throw new Exception();
            }
        }
    }
}