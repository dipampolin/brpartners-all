﻿using System;
using System.ServiceModel;
using System.Configuration;
using QX3.Portal.WebSite.ClientRegistrationService;

namespace QX3.Portal.WebSite.Models.Channel.Providers
{
    public class ClientRegistrationChannelProvider : IChannelProvider<IClientRegistrationContractChannel>
    {
        public ChannelFactory<IClientRegistrationContractChannel> GetChannel()
        {
            try
            {
                var cnn = new ChannelFactory<IClientRegistrationContractChannel>("WSHttpBinding_IClientRegistrationContract");
                cnn.Credentials.UserName.UserName = ConfigurationManager.AppSettings["AppUserName"];
                cnn.Credentials.UserName.Password = ConfigurationManager.AppSettings["AppPassword"];

                return cnn;
            }
            catch
            {
                throw new Exception();
            }
        }
    }
}