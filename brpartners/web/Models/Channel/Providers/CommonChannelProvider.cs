﻿using System;
using System.ServiceModel;
using System.Configuration;
using QX3.Portal.WebSite.CommonService;

namespace QX3.Portal.WebSite.Models.Channel.Providers
{
    public class CommonChannelProvider : IChannelProvider<ICommonContractChannel>
    {
        public ChannelFactory<ICommonContractChannel> GetChannel()
        {
            try
            {
                var service = new CommonContractClient();
                var cnn = new ChannelFactory<ICommonContractChannel>("WSHttpBinding_ICommonContract");
                cnn.Credentials.UserName.UserName = ConfigurationManager.AppSettings["AppUserName"];
                cnn.Credentials.UserName.Password = ConfigurationManager.AppSettings["AppPassword"];

                return cnn;
            }
            catch
            {
                throw new Exception();
            }
        }
    }
}