﻿using System;
using System.ServiceModel;
using System.Configuration;
using QX3.Portal.WebSite.MarketService;
using QX3.Portal.WebSite.OtherService;

namespace QX3.Portal.WebSite.Models.Channel.Providers
{
    public class OtherChannelProvider : IChannelProvider<IOtherContractChannel>
    {
        public ChannelFactory<IOtherContractChannel> GetChannel()
        {
            try
            {
                var service = new OtherContractClient();
                var cnn = new ChannelFactory<IOtherContractChannel>("WSHttpBinding_IOtherContract");
                cnn.Credentials.UserName.UserName = ConfigurationManager.AppSettings["AppUserName"];
                cnn.Credentials.UserName.Password = ConfigurationManager.AppSettings["AppPassword"];

                return cnn;
            }
            catch
            {
                throw new Exception();
            }
        }
    }
}