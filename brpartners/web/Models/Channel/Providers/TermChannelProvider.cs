﻿using System;
using System.ServiceModel;
using System.Configuration;
using QX3.Portal.WebSite.TermService;

namespace QX3.Portal.WebSite.Models.Channel.Providers
{
    public class TermChannelProvider : IChannelProvider<ITermContractChannel>
    {
        public ChannelFactory<ITermContractChannel> GetChannel()
        {
            try
            {
                var service = new TermContractClient();
                var cnn = new ChannelFactory<ITermContractChannel>("WSHttpBinding_ITermContract");
                cnn.Credentials.UserName.UserName = ConfigurationManager.AppSettings["AppUserName"];
                cnn.Credentials.UserName.Password = ConfigurationManager.AppSettings["AppPassword"];

                return cnn;
            }
            catch
            {
                throw new Exception();
            }
        }
    }
}