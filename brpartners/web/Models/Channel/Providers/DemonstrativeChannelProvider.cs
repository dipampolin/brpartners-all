﻿using System;
using System.ServiceModel;
using System.Configuration;
using QX3.Portal.WebSite.DemonstrativeService;


namespace QX3.Portal.WebSite.Models.Channel.Providers
{
    public class DemonstrativeChannelProvider : IChannelProvider<IDemonstrativeContractChannel>
    {
        public ChannelFactory<IDemonstrativeContractChannel> GetChannel()
        {
            try
            {
                var cnn = new ChannelFactory<IDemonstrativeContractChannel>("WSHttpBinding_IDemonstrativeContract");
                cnn.Credentials.UserName.UserName = ConfigurationManager.AppSettings["AppUserName"];
                cnn.Credentials.UserName.Password = ConfigurationManager.AppSettings["AppPassword"];
                return cnn;
            }
            catch
            {
                throw new Exception();
            }
        }
    }
}

