﻿using System;
using System.ServiceModel;
using System.Configuration;
using QX3.Portal.WebSite.FinancialService;

namespace QX3.Portal.WebSite.Models.Channel.Providers
{
    public class FinancialChannelProvider : IChannelProvider<IFinancialContractChannel>
    {
        public ChannelFactory<IFinancialContractChannel> GetChannel()
        {
            try
            {
                var service = new FinancialContractClient();
                var cnn = new ChannelFactory<IFinancialContractChannel>("WSHttpBinding_IFinancialContract");
                cnn.Credentials.UserName.UserName = ConfigurationManager.AppSettings["AppUserName"];
                cnn.Credentials.UserName.Password = ConfigurationManager.AppSettings["AppPassword"];

                return cnn;
            }
            catch
            {
                throw new Exception();
            }
        }
    }
}