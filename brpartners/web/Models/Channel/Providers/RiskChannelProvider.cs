﻿using System;
using System.ServiceModel;
using System.Configuration;
using QX3.Portal.WebSite.RiskService;

namespace QX3.Portal.WebSite.Models.Channel.Providers
{
    public class RiskChannelProvider : IChannelProvider<IRiskContractChannel>
    {
        public ChannelFactory<IRiskContractChannel> GetChannel()
        {
            try
            {
                var cnn = new ChannelFactory<IRiskContractChannel>("WSHttpBinding_IRiskContract");
                cnn.Credentials.UserName.UserName = ConfigurationManager.AppSettings["AppUserName"];
                cnn.Credentials.UserName.Password = ConfigurationManager.AppSettings["AppPassword"];

                return cnn;
            }
            catch
            {
                throw new Exception();
            }
        }
    }
}