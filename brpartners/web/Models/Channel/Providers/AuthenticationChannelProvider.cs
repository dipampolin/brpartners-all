﻿using System;
using System.ServiceModel;
using System.Configuration;
using QX3.Portal.WebSite.AuthenticationService;

namespace QX3.Portal.WebSite.Models.Channel.Providers
{
    public class AuthenticationChannelProvider : IChannelProvider<IAuthenticationContractChannel>
    {
        public ChannelFactory<IAuthenticationContractChannel> GetChannel()
        {
            try
            {
                var service = new AuthenticationContractClient();
                var cnn = new ChannelFactory<IAuthenticationContractChannel>("WSHttpBinding_IAuthenticationContract");
                cnn.Credentials.UserName.UserName = ConfigurationManager.AppSettings["AppUserName"];
                cnn.Credentials.UserName.Password = ConfigurationManager.AppSettings["AppPassword"];

                return cnn;
            }
            catch
            {
                throw new Exception();
            }
        }
    }
}