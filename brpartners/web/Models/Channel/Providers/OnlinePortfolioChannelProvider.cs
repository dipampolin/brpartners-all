﻿using System;
using System.ServiceModel;
using System.Configuration;
using QX3.Portal.WebSite.OnlinePortfolioService;

namespace QX3.Portal.WebSite.Models.Channel.Providers
{
    public class OnlinePortfolioChannelProvider : IChannelProvider<IOnlinePortfolioContractChannel>
    {
        public ChannelFactory<IOnlinePortfolioContractChannel> GetChannel()
        {
            try
            {
                var service = new OnlinePortfolioContractClient();
                var cnn = new ChannelFactory<IOnlinePortfolioContractChannel>("WSHttpBinding_IOnlinePortfolioContract");
                cnn.Credentials.UserName.UserName = ConfigurationManager.AppSettings["AppUserName"];
                cnn.Credentials.UserName.Password = ConfigurationManager.AppSettings["AppPassword"];

                return cnn;
            }
            catch
            {
                throw new Exception();
            }
        }
    }
}