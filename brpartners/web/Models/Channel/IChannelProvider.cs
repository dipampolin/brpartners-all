﻿using System.ServiceModel;

namespace QX3.Portal.WebSite.Models.Channel
{
    public interface IChannelProvider<T>
    {
        ChannelFactory<T> GetChannel();
    }
}