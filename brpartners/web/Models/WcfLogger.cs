﻿using System;
using QX3.Spinnex.Common.Services.Logging;
using QX3.Portal.WebSite.Models.Channel;
using QX3.Portal.WebSite.LogService;
using System.Xml;
using System.IO;
using System.Text;

namespace QX3.Portal.WebSite.Models
{
	public class WcfLogger : ILogger
	{
		public void Init(string config)
		{

		}

		public bool PersistLog(LogContent content)
		{
			if (content == null)
				return false;

			try
			{
				var data = content.GetLogData();
				if (data == null)
					return false;

				var doc = new XmlDocument();
				MemoryStream ms = new MemoryStream();
				XmlWriter xw = new XmlTextWriter(ms, Encoding.Default);
				data.Save(xw);
				xw.Flush();
				ms.Position = 0;
				doc.Load(ms);
				xw.Close();
				ms.Close();

				IChannelProvider<ILogContractChannel> channelProvider = ChannelProviderFactory<ILogContractChannel>.Create("LogClassName");
				var service = channelProvider.GetChannel().CreateChannel();

				StringWriter sw = new StringWriter();
				XmlTextWriter tx = new XmlTextWriter(sw);
				doc.WriteTo(tx);

				service.Open();
				service.CreateTraceRecord(sw.ToString());
				service.Close();

				return true;
			}
			catch (Exception ex)
			{
				content.AddError(ex);
				return false;
			}

		}
	}
}
