﻿using QX3.Portal.Contracts.DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QX3.Portal.WebSite.Models
{
    public class LoginModel
    {
        public LoginModel()
        {
            Element = new List<ACElement>();
        }

        public List<ACElement> Element { get; set; }
        public ACProfile Profile { get; set; }
        public LoginError LoginError { get; set; }
        public string IdClient { get; set; }
    }

    public class LoginError
    {
        public string Message { get; set; }
        public string ErrorType { get; set; }
    }
}