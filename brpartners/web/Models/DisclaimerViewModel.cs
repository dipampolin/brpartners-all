﻿using System;
using System.Web.Mvc;

namespace QX3.Portal.WebSite.Models
{
	[Serializable]
	public class DisclaimerViewModel
	{
		public Int32 ReportId { get; set; }

		public string Description { get; set; }

		public Boolean HasDisclaimer { get; set; }

		[AllowHtml]
		public string DisclaimerContent { get; set; }
	}
}