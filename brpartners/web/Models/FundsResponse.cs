﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using QX3.Portal.Services.SagazWebService;

namespace QX3.Portal.WebSite.Models
{
    public class FundsStatementResponse: JsonModel
    {
        public string Date { get; set; }
        public string MovementDate { get; set; }
        public string MovementType { get; set; }
        public string Quota { get; set; }
        public string QuantityOfQuota { get; set; }
        public string IOF { get; set; }
        public string IRRF { get; set; }
        public string Value { get; set; }
        public string CautionNumber { get; set; }
        public string OriginalValue { get; set; }
        public string GrossValue { get; set; }
    }

    public class FundDetailsResponse : JsonModel
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string InitialInvestment { get; set; }
        public string ApplicationTimeLimit { get; set; }
        public string FundManagerLink { get; set; }
        public string AdditionalInvestment { get; set; }
        public string ElectronicsSignature { get; set; }
        public string Accession { get; set; }
        public string Documents { get; set; }
        public string UserHasAccepted { get; set; }
        public string AccessionDocID { get; set; }

        public string UserHasBalance { get; set; }
        public string UserHasApplication { get; set; }

        public string FundsSuggested { get; set; }
        public string AmountToTransfer { get; set; }
    }

    public class FundApplicationResponse : JsonModel
    {
        public string Result { get; set; }
        public string Data { get; set; }
    }

    public class FundBalanceResponse : JsonModel
    {
        public string GrossValue { get; set; }
        public string NetValue { get; set; }
        public string BalanceDate { get; set; }
        public string FundName { get; set; }
    }

    public class UserBalanceResponse : JsonModel
    {
        public string AvailableBalance { get; set; }
        public string BlockedBalance { get; set; }
        public string PositionBalance { get; set; }
    }

	[Serializable]
    public class FundBalanceChartItemResponse : FundBalanceChartItem 
    {
        public string RGBColor { get; set; }
        public string Percentage { get; set; }
    }
}