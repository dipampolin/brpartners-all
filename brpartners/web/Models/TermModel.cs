﻿
using System;
namespace QX3.Portal.WebSite.Models
{
	[Serializable]
	public class TermListItem
	{
		public int ID { get; set; }
        public string Client { get; set; }
        public string Code { get; set; }
        public string Assessor { get; set; }
        public string AssessorCode { get; set; }
        public string Stock { get; set; }
        public string Company { get; set; }
        public int Contract { get; set; }
        public Int64 AvailableQuantity { get; set; }
        public Int64 TotalQuantity { get; set; }
        public Int64 SettleQuantity { get; set; }
        public string TypeId { get; set; }
        public string TypeDescription { get; set; }
        public DateTime? SettleDate { get; set; }
        public string StatusId { get; set; }
        public string StatusDescription { get; set; }
        public int Index { get; set; }
        public DateTime? DueDate { get; set; }
        public DateTime? RolloverDate { get; set; }
	}
}
