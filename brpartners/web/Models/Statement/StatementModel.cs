﻿using QX3.Portal.Contracts.DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QX3.Portal.WebSite.Models.Statement
{
    public class FixedIncomeDTO
    {
        public string Name { get; set; }
        public string CpfCnpj { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public List<FixedIncomeDetails> Movement { get; set; }
        public List<FixedIncomeDetails> OpenPosition { get; set; }
    }

    public class NdfDTO
    {
        public string Name { get; set; }
        public string CpfCnpj { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public string Type { get; set; }
        public List<NdfDetails> Movement { get; set; }
        public List<NdfDetails> OpenPosition { get; set; }
    }

    public class SwapDTO
    {
        public string Name { get; set; }
        public string CpfCnpj { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public string Type { get; set; }
        public List<SwapDetails> Movement { get; set; }
        public List<SwapDetails> OpenPosition { get; set; }
    }
}