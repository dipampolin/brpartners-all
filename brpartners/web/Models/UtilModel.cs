﻿using QX3.Portal.WebSite.Properties;
using System.IO;
using System;

namespace QX3.Portal.WebSite.Models
{
	[Serializable]
	public class Error
	{
		private string _DisplayMode = "block";
		public string DisplayMode
		{
			get
			{
				return _DisplayMode;
			}
			set
			{
				_DisplayMode = value;
			}
		}
		private string _Message = Resources.DefaultFormErrorMessage;
		public string Message
		{
			get
			{
				return _Message;
			}
			set
			{
				_Message = value;
			}
		}

		private bool _IsList = false;
		public bool IsList
		{
			get
			{
				return _IsList;
			}
			set
			{
				_IsList = value;
			}
		}


	}

	[Serializable]
	public class ZipItem
	{
		public MemoryStream FileStream { get; set; }
		public string FileName { get; set; }
	}
}