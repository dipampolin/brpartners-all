﻿
using System;
namespace QX3.Portal.WebSite.Models
{
	[Serializable]
	public class UserListModel
	{
		public string ID { get; set; }
		public string Name { get; set; }
		public string Email { get; set; }
		public string Type { get; set; }
		public string ProfileName { get; set; }
		public string AssessorID { get; set; }
		public string AssessorGroup { get; set; }
		public string AssessorInterval { get; set; }
	}

	[Serializable]
	public class GroupOfAssessorsListItem
	{
		public int ID { get; set; }
		public string Name { get; set; }
		public string Assessors { get; set; }
		public int UsersCount { get; set; }
	}

	[Serializable]
	public class UsersListItem
	{
		public string ID { get; set; }
        public int ClientID { get; set; }
		public string Name { get; set; }
		public string Type { get; set; }
		public string ProfileName { get; set; }
		public string AssessorID { get; set; }
		public string AssessorGroup { get; set; }
		public string AssessorInterval { get; set; }
		public string Blocked { get; set; }
        public string Email { get; set; }
        public string Login { get; set; }

        public long? CpfCnpj { get; set; }
	}

	[Serializable]
	public class ProfilesListItem
	{
		public int ID { get; set; }
		public string Name { get; set; }
		public int UsersCount { get; set; }
	}

}
