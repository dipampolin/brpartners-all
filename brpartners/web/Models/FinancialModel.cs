﻿
using System;
namespace QX3.Portal.WebSite.Models
{
	[Serializable]
	public class RedemptionRequestListItem
	{
        public string Id { get; set; }
        public string RequestedDate { get; set; }
        public string ClientCode { get; set; }
        public string ClientName { get; set; }
        public string Bank { get; set; }
        public string Agency { get; set; }
        public string Account { get; set; }
        public string Amount { get; set; }
        public string StatusId { get; set; }
        public string StatusDescription { get; set; }
        public string SettllementDate { get; set; }
        public string Observation { get; set; }

        public int Index { get; set; }

        public RedemptionRequestBalanceItem Balance { get; set; }
	}

    [Serializable]
    public class RedemptionRequestBalanceItem
    {
        public string Available { get; set; }
        public string AvailableD1 { get; set; }
        public string AvailableD2 { get; set; }
        public string AvailableD3 { get; set; }
        public string Total { get; set; }
        public string Intended { get; set; }
    }
}
