﻿using Tegra.DDD.Repository;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Domain.Repository
{
    public interface IEmployeeCourseRepository : IRepository<CourseEmployee, int>
    {
        CourseEmployee GetByEmployee(int employeeId);
    }
}
