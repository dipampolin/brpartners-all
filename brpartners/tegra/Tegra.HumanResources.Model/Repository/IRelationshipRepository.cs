﻿using Tegra.DDD.Repository;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Domain.Repository
{
    public interface IRelationshipRepository : IRepository<EmployeeRelationship, int>
    {
        EmployeeRelationship GetByEmployee(int employeeId);
    }
}
