﻿using Tegra.DDD.Repository;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Domain.Repository
{
    public interface IEmployeeContractRepository : IRepository<EmployeeContract, int>
    {
        EmployeeContract GetByEmployee(int employeeId);
    }
}
