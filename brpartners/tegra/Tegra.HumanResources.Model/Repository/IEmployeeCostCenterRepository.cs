﻿using Tegra.DDD.Repository;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Domain.Repository
{
    public interface IEmployeeCostCenterRepository : IRepository<EmployeeCostCenter, int>
    {
        EmployeeCostCenter GetByEmployee(int employeeId);
    }
}
