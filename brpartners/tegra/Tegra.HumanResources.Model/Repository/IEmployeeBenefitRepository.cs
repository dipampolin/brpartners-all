﻿using Tegra.DDD.Repository;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Domain.Repository
{
    public interface IEmployeeBenefitRepository : IRepository<EmployeeBenefit, int>
    {
        EmployeeBenefit GetByEmployee(int employeeId);
    }
}
