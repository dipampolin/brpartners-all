﻿using Tegra.DDD.Repository;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Domain.Repository
{
    public interface IEmployeeVacationRepository : IRepository<EmployeeVacation, int>
    {
        EmployeeVacation GetByEmployee(int employeeId);
    }
}
