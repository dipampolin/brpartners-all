﻿using Tegra.DDD.Repository;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Domain.Repository
{
    public interface IVehicleRepository : IRepository<EmployeeVehicle, int>
    {
        EmployeeVehicle GetByEmployee(int employeeId);
    }
}
