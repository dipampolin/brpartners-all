﻿using System.Collections.Generic;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Representations;

namespace Tegra.HumanResources.Domain.Repository
{
    public interface IReportRepository : DDD.Repository.IRepository<Report, int>
    {
        List<ReportByAlias> GetReportAlias();
    }
}
