﻿using Tegra.DDD.Repository;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Domain.Repository
{
    public interface IEmployeeCompanyRepository : IRepository<EmployeeCompany, int>
    {
        EmployeeCompany GetByEmployee(int id);
    }
}
