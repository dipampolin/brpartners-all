﻿using Tegra.DDD.Repository;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Domain.Repository
{
    public interface IAddressRepository : IRepository<EmployeeAddress, int>
    {
        EmployeeAddress GetByEmployee(int employeeId);
    }
}
