﻿using Tegra.DDD.Repository;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Domain.Repository
{
    public interface IEmployeeVariablesRepository : IRepository<EmployeeVariables, int>
    {
        EmployeeVariables GetByEmployee(int employeeId);
    }
}
