﻿using Tegra.DDD.Repository;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Domain.Repository
{
    public interface IEmployeeAttachmentRepository : IRepository<EmployeeAttachment, int>
    {
        EmployeeAttachment GetByEmployee(int employeeId);
    }
}
