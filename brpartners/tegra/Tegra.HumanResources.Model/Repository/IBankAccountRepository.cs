﻿using Tegra.DDD.Repository;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Domain.Repository
{
    public interface IBankAccountRepository : IRepository<EmployeeBankAccount, int>
    {
        EmployeeBankAccount GetByEmployee(int employeeId);
    }
}
