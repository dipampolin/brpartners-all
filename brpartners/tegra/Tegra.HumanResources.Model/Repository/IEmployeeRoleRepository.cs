﻿using Tegra.DDD.Repository;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Domain.Repository
{
    public interface IEmployeeRoleRepository : IRepository<EmployeeRole, int>
    {
        EmployeeRole GetByEmployee(int employeeId);
    }
}
