﻿using Tegra.DDD.Repository;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Domain.Repository
{
    public interface ISalaryHistoryRepository : IRepository<EmployeeSalary, int>
    {
        EmployeeSalary GetByEmployee(int employeeId);
    }
}
