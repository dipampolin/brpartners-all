﻿using Tegra.DDD.Repository;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Domain.Repository
{
    public interface INotificationDestinationRepository : IRepository<NotificationDestination, int>
    {
    }
}
