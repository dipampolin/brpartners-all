﻿using Tegra.DDD.Repository;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Domain.Repository
{
    public interface ICostCenterRepository : IRepository<CostCenter, int>
    {
    }
}
