﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tegra.DDD.Repository;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Domain.Repository
{
    public interface IEmployeeVariablesTypeRepository : IRepository<EmployeeVariablesType, int>
    {
    }
}
