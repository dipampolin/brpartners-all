﻿using Tegra.DDD.Repository;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Domain.Repository
{
    public interface IEmployeeTypeRepository : IRepository<EmployeeType, int>
    {
        EmployeeType GetByEmployee(int employeeId);
    }
}
