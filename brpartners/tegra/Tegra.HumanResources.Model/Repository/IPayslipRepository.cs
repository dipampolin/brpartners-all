﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Tegra.DDD.Pagination;
using Tegra.DDD.Repository;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Representations;

namespace Tegra.HumanResources.Domain.Repository
{
    public interface IPayslipRepository : IRepository<Payslip, int>
    {
        Payslip GetToImport(int year, int month, string employeeCode);

        PagedResult<PayslipByMonth> List(PaginationSettings settings);

        List<PayslipByExcel> GetPayslipToReport(Expression<Func<Payslip, bool>> predicate);
    }
}
