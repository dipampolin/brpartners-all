﻿using System.Linq;
using Tegra.DDD.Repository;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Domain.Repository
{
    public interface INotificationRepository : IRepository<Notification, int>
    {
        IQueryable<Notification> GetNotification(int id);
    }
}
