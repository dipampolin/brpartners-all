﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Repository;

namespace Tegra.HumanResources.Domain
{


    public class Conversions
    {

        public enum ConvertType
        {
            DATE,
            CPF,
            CNPJ,
            MOEDA,
            CARRO_PLACA,
            CEP,
            DEFAULT_BOOL,
            OPTIONAL_DATE
        }

        private Dictionary<ConvertType, Expression<Func<object, string>>> _maps;

        private Dictionary<string, FormatAttribute> _attributeMap;

        private readonly ICivilStatusRepository _civilStatusRepository;

        public Conversions(ICivilStatusRepository civilStatusRepository = null)
        {

            this._civilStatusRepository = civilStatusRepository;
            _attributeMap = new Dictionary<string, FormatAttribute>();
            var classes = Assembly.GetExecutingAssembly().GetTypes().ToList();

            foreach (var c in classes)
            {
                var className = c.Name;

                var classNameAttribute = c.GetCustomAttributes(typeof(DescriptionAttribute), true).FirstOrDefault();

                if (classNameAttribute != null)
                {
                    className = ((DescriptionAttribute)classNameAttribute).Description;
                }

                var proper = c.GetProperties().Where(f => ((FormatAttribute)f.GetCustomAttributes(typeof(FormatAttribute), true).FirstOrDefault()) != null).ToList();

                foreach (var p in proper)
                {
                    var attribute = (FormatAttribute)p.GetCustomAttributes(typeof(FormatAttribute), true).Single();
                    var attributeName = p.Name;

                    var propAttribute = p.GetCustomAttributes(typeof(DescriptionAttribute), true).FirstOrDefault();

                    if (propAttribute != null)
                    {
                        attributeName = ((DescriptionAttribute)propAttribute).Description;
                    }

                    var alias = className + " - " + attributeName;

                    if (!_attributeMap.ContainsKey(alias))
                        _attributeMap.Add(alias, attribute);
                }
            }

            this._maps = new Dictionary<ConvertType, Expression<Func<object, string>>>
            {
                { ConvertType.DATE, x => ((DateTime)x).ToString("dd/MM/yyyy") },
                { ConvertType.OPTIONAL_DATE, x => x == null ? "" : ((DateTime?)x).Value.ToString("dd/MM/yyyy")  },
                { ConvertType.CPF, x => x == null ? "" : Convert.ToUInt64(x).ToString(@"000\.000\.000\-00") },
                { ConvertType.CNPJ, x => x == null ? "" : Convert.ToUInt64(x).ToString(@"00\.000\.000\/0000\-00") },
                { ConvertType.MOEDA, x => x == null ? "" : string.Format("{0:N}", x) },
                { ConvertType.CARRO_PLACA, x => x == null ? "" : string.Format("{0}-{1}", x.ToString().Substring(0,3), x.ToString().Substring(3))},
                { ConvertType.CEP, x => x == null ? "" : Convert.ToUInt64(x).ToString(@"00000\-000") },
                { ConvertType.DEFAULT_BOOL, x => x == null ? "" : x.Equals(true) ? "Sim" : "Não" }
            };
        }

        public FormatAttribute GetByAttribute(string name)
        {

            if (!_attributeMap.Keys.Contains(name))
            {
                return null;
            }

            return _attributeMap[name];
        }

        public Expression<Func<object, string>> Get(ConvertType type)
        {
            return this._maps[type];
        }
    }
}
