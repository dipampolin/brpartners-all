﻿using System;
using System.Linq.Expressions;
using static Tegra.HumanResources.Domain.Conversions;

namespace Tegra.HumanResources.Domain.Entities
{
    public class FormatAttribute : Attribute
    {
        public ConvertType Type { get; set; }

        public FormatAttribute(ConvertType type)
        {
            this.Type = type;
        }

        public Expression<Func<object, string>> GetExpression()
        {
            return (new Conversions()).Get(Type);
        }
    }
}