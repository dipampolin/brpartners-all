﻿using CodeEngine.Framework.QueryBuilder;
using CodeEngine.Framework.QueryBuilder.Enums;
using System.Collections.Generic;
using System.Linq;

namespace Tegra.HumanResources.Domain.Join
{
    public class RoleJoin
    {
        public static SelectQueryBuilder GetRoleJoin(IEnumerable<string> tables, SelectQueryBuilder query)
        {
            if (tables.Contains("Role") || tables.Contains("EmployeeRole"))
            {
                query.AddJoin(JoinType.LeftJoin,
                    "rh.EmployeeRole", "EmployeeId",
                    Comparison.Equals,
                    "Employee", "Id");

                query.AddJoin(JoinType.LeftJoin,
                   "rh.Role", "Id",
                   Comparison.Equals,
                   "EmployeeRole", "RoleId");
            }
            return query;
        }
    }
}
