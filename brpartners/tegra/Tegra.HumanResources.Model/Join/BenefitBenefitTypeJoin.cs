﻿using CodeEngine.Framework.QueryBuilder;
using CodeEngine.Framework.QueryBuilder.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tegra.HumanResources.Domain.Join
{
    public static class BenefitBenefitTypeJoin
    {
        public static SelectQueryBuilder GetBenefitBenefitTypeJoin(IEnumerable<string> tables, SelectQueryBuilder query)
        {
            if(tables.Contains("BenefitType"))
            {

                if (!tables.Contains("Benefit")) {
                    query.AddJoin(JoinType.LeftJoin,
                        "rh.EmployeeBenefit", "EmployeeId",
                        Comparison.Equals,
                        "Employee", "Id");

                    query.AddJoin(JoinType.LeftJoin,
                        "rh.Benefit", "Id",
                        Comparison.Equals,
                        "EmployeeBenefit", "BenefitId");
                }

                query.AddJoin(JoinType.LeftJoin,
                        "rh.BenefitType", "Id",
                        Comparison.Equals,
                        "Benefit", "BenefitTypeId");
            }
            return query;
        }
    }
}
