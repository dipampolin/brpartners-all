﻿using CodeEngine.Framework.QueryBuilder;
using CodeEngine.Framework.QueryBuilder.Enums;
using System.Collections.Generic;
using System.Linq;

namespace Tegra.HumanResources.Domain.Join
{
    public static class PayslipJoin
    {
        public static SelectQueryBuilder GetPayslipJoin(IEnumerable<string> tables, SelectQueryBuilder query)
        {
            if (tables.Contains("Payslip"))
            {
                query.AddJoin(JoinType.LeftJoin,
                    "rh.Payslip", "EmployeeId",
                    Comparison.Equals,
                    "Employee", "Id");

                if (tables.Contains("PayslipItem"))
                {
                    query.AddJoin(JoinType.LeftJoin,
                        "rh.PayslipItem", "PayslipId",
                        Comparison.Equals,
                        "Payslip", "Id");
                }

                if (tables.Contains("CostCenter"))
                {
                    query.AddJoin(JoinType.LeftJoin,
                       "rh.CostCenter", "Id",
                       Comparison.Equals,
                       "Payslip", "CostCenterId");
                }
            }

            if (!tables.Contains("Payslip") && tables.Contains("CostCenter"))
            {
                query.AddJoin(JoinType.LeftJoin,
                    "rh.Payslip", "EmployeeId",
                    Comparison.Equals,
                    "Employee", "Id");

                if (tables.Contains("PayslipItem"))
                {
                    query.AddJoin(JoinType.LeftJoin,
                        "rh.PayslipItem", "PayslipId",
                        Comparison.Equals,
                        "Payslip", "Id");
                }

                if (tables.Contains("CostCenter"))
                {
                    query.AddJoin(JoinType.LeftJoin,
                       "rh.CostCenter", "Id",
                       Comparison.Equals,
                       "Employee", "CostCenterId");
                }
            }

            if (!tables.Contains("Payslip") && tables.Contains("PayslipItem"))
            {
                query.AddJoin(JoinType.LeftJoin,
                  "rh.Payslip", "EmployeeId",
                  Comparison.Equals,
                  "Employee", "Id");

                query.AddJoin(JoinType.LeftJoin,
                        "rh.PayslipItem", "PayslipId",
                        Comparison.Equals,
                        "Payslip", "Id");
            }

            return query;
        }
    }
}
