﻿using CodeEngine.Framework.QueryBuilder;
using CodeEngine.Framework.QueryBuilder.Enums;
using System.Collections.Generic;
using System.Linq;

namespace Tegra.HumanResources.Domain.Join
{
    public class EmployeeVariablesJoin
    {
        public static SelectQueryBuilder GetEmployeeVariablesJoin(IEnumerable<string> tables, SelectQueryBuilder query)
        {
            if (tables.Contains("EmployeeVariables"))
            {
                query.AddJoin(JoinType.LeftJoin,
                        "rh.EmployeeVariables", "EmployeeId",
                        Comparison.Equals,
                        "Employee", "Id");
            }
            return query;
        }
    }
}
