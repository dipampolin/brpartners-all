﻿using CodeEngine.Framework.QueryBuilder;
using CodeEngine.Framework.QueryBuilder.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tegra.HumanResources.Domain.Join
{
    public class EmployeeVariablesTypeJoin
    {
        public static SelectQueryBuilder GetEmployeeVariablesType(IEnumerable<string> tables, SelectQueryBuilder query)
        {
            if (tables.Contains("EmployeeVariablesType"))
            {

                if (!tables.Contains("EmployeeVariables"))
                {
                    query.AddJoin(JoinType.LeftJoin,
                        "rh.EmployeeVariables", "EmployeeId",
                        Comparison.Equals,
                        "Employee", "Id");
                }
                
                query.AddJoin(JoinType.LeftJoin,
                        "rh.EmployeeVariablesType", "Id",
                        Comparison.Equals,
                        "EmployeeVariables", "EmployeeVariablesTypeId");
            }
            return query;
        }
    }
}
