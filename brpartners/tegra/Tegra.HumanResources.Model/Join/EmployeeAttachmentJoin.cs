﻿using CodeEngine.Framework.QueryBuilder;
using CodeEngine.Framework.QueryBuilder.Enums;
using System.Collections.Generic;
using System.Linq;

namespace Tegra.HumanResources.Domain.Join
{
    public class EmployeeAttachmentJoin
    {
        public static SelectQueryBuilder GetEmployeeAttachmentJoin(IEnumerable<string> tables, SelectQueryBuilder query)
        {
            if (tables.Contains("EmployeeAttachment"))
            {
                query.AddJoin(JoinType.LeftJoin,
                        "rh.EmployeeAttachment", "EmployeeId",
                        Comparison.Equals,
                        "Employee", "Id");
            }
            return query;
        }
    }
}
