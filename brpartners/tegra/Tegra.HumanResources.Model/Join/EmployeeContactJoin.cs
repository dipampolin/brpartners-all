﻿using CodeEngine.Framework.QueryBuilder;
using CodeEngine.Framework.QueryBuilder.Enums;
using System.Collections.Generic;
using System.Linq;

namespace Tegra.HumanResources.Domain.Join
{
    public class EmployeeContactJoin
    {
        public static SelectQueryBuilder GetEmployeeContactJoin(IEnumerable<string> tables, SelectQueryBuilder query)
        {
            if (tables.Contains("EmployeeContact"))
            {
                query.AddJoin(JoinType.LeftJoin,
                    "rh.EmployeeContact", "EmployeeId",
                    Comparison.Equals,
                    "Employee", "Id");
            }
            return query;
        }
    }
}
