﻿using CodeEngine.Framework.QueryBuilder;
using CodeEngine.Framework.QueryBuilder.Enums;
using System.Collections.Generic;
using System.Linq;

namespace Tegra.HumanResources.Domain.Join
{
    public static class CompanyJoin
    {
        public static SelectQueryBuilder GetCompanyJoin(IEnumerable<string> tables, SelectQueryBuilder query)
        {
            if (tables.Contains("Company") || tables.Contains("EmployeeCompany"))
            {
                query.AddJoin(JoinType.LeftJoin,
                   "rh.EmployeeCompany", "EmployeeId",
                   Comparison.Equals,
                   "Employee", "Id");

                query.AddJoin(JoinType.LeftJoin,
                    "rh.Company", "Id",
                    Comparison.Equals,
                    "EmployeeCompany", "CompanyId");
            }
            return query;
        }
    }
}
