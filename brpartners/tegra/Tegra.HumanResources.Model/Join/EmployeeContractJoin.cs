﻿using CodeEngine.Framework.QueryBuilder;
using CodeEngine.Framework.QueryBuilder.Enums;
using System.Collections.Generic;
using System.Linq;

namespace Tegra.HumanResources.Domain.Join
{
    public class EmployeeContractJoin
    {
        public static SelectQueryBuilder GetEmployeeContractJoin(IEnumerable<string> tables, SelectQueryBuilder query)
        {
            if (tables.Contains("EmployeeContract"))
            {
                query.AddJoin(JoinType.LeftJoin,
                    "rh.EmployeeContract", "EmployeeId",
                    Comparison.Equals,
                    "Employee", "Id");
            }
            return query;
        }
    }
}
