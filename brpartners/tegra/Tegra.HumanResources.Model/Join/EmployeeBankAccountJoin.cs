﻿using CodeEngine.Framework.QueryBuilder;
using CodeEngine.Framework.QueryBuilder.Enums;
using System.Collections.Generic;
using System.Linq;

namespace Tegra.HumanResources.Domain.Join
{
    public class EmployeeBankAccountJoin
    {
        public static SelectQueryBuilder GetEmployeeBankAccountJoin(IEnumerable<string> tables, SelectQueryBuilder query)
        {
            if (tables.Contains("EmployeeBankAccount"))
            {
                query.AddJoin(JoinType.LeftJoin,
                    "rh.EmployeeBankAccount", "EmployeeId",
                    Comparison.Equals,
                    "Employee", "Id");
            }
            return query;
        }
    }
}
