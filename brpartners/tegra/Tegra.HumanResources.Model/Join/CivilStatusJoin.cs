﻿using CodeEngine.Framework.QueryBuilder;
using CodeEngine.Framework.QueryBuilder.Enums;
using System.Collections.Generic;
using System.Linq;

namespace Tegra.HumanResources.Domain.Join
{
    public static class CivilStatusJoin
    {
        public static SelectQueryBuilder GetCivilStatusJoin(IEnumerable<string> tables, SelectQueryBuilder query)
        {
            if (tables.Contains("CivilStatus"))
            {
                query.AddJoin(JoinType.LeftJoin,
                        "rh.CivilStatus", "Id",
                        Comparison.Equals,
                        "Employee", "CivilStatusId");
            }
            return query;
        }
    }
}
