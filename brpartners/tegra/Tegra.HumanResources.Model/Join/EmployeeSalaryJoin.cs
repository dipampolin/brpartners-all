﻿using CodeEngine.Framework.QueryBuilder;
using CodeEngine.Framework.QueryBuilder.Enums;
using System.Collections.Generic;
using System.Linq;

namespace Tegra.HumanResources.Domain.Join
{
    public class EmployeeSalaryJoin
    {
        public static SelectQueryBuilder GetEmployeeSalaryJoin(IEnumerable<string> tables, SelectQueryBuilder query)
        {
            if (tables.Contains("EmployeeSalary"))
            {
                query.AddJoin(JoinType.LeftJoin,
                        "rh.EmployeeSalary", "EmployeeId",
                        Comparison.Equals,
                        "Employee", "Id");
            }
            return query;
        }
    }
}
