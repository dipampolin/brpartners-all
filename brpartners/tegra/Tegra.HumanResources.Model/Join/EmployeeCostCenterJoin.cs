﻿using CodeEngine.Framework.QueryBuilder;
using CodeEngine.Framework.QueryBuilder.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tegra.HumanResources.Domain.Join
{
    public class EmployeeCostCenterJoin
    {
        public static SelectQueryBuilder GetEmployeeCostCenterJoin(IEnumerable<string> tables, SelectQueryBuilder query)
        {
            if (tables.Contains("EmployeeCostCenter"))
            {
                query.AddJoin(JoinType.LeftJoin,
                        "rh.EmployeeCostCenter", "EmployeeId",
                        Comparison.Equals,
                        "Employee", "Id");
            }
            return query;
        }
    }
}
