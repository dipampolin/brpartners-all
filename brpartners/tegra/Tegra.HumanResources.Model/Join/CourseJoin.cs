﻿using CodeEngine.Framework.QueryBuilder;
using CodeEngine.Framework.QueryBuilder.Enums;
using System.Collections.Generic;
using System.Linq;

namespace Tegra.HumanResources.Domain.Join
{
    public static class CourseJoin
    {
        public static SelectQueryBuilder GetCourseJoin(IEnumerable<string> tables, SelectQueryBuilder query)
        {
            if (tables.Contains("CourseEmployee") || tables.Contains("Course"))
            {
                query.AddJoin(JoinType.LeftJoin,
                    "rh.CourseEmployee", "EmployeeId",
                    Comparison.Equals,
                    "Employee", "Id");

                query.AddJoin(JoinType.LeftJoin,
                     "rh.Course", "Id",
                     Comparison.Equals,
                     "CourseEmployee", "CourseId");
            }

            return query;
        }
    }
}
