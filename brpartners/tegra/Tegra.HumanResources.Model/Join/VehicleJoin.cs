﻿using CodeEngine.Framework.QueryBuilder;
using CodeEngine.Framework.QueryBuilder.Enums;
using System.Collections.Generic;
using System.Linq;

namespace Tegra.HumanResources.Domain.Join
{
    public static class VehicleJoin
    {
        public static SelectQueryBuilder GetVehicleJoin(IEnumerable<string> tables, SelectQueryBuilder query)
        {
            if (tables.Contains("EmployeeVehicle"))
            {
                query.AddJoin(JoinType.LeftJoin,
                    "rh.EmployeeVehicle", "EmployeeId",
                    Comparison.Equals,
                    "Employee", "Id");
            }
            return query;
        }
    }
}
