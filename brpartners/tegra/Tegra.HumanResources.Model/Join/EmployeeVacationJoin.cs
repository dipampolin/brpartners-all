﻿using CodeEngine.Framework.QueryBuilder;
using CodeEngine.Framework.QueryBuilder.Enums;
using System.Collections.Generic;
using System.Linq;

namespace Tegra.HumanResources.Domain.Join
{
    public class EmployeeVacationJoin
    {
        public static SelectQueryBuilder GetEmployeeVacationJoin(IEnumerable<string> tables, SelectQueryBuilder query)
        {
            if (tables.Contains("EmployeeVacation"))
            {
                query.AddJoin(JoinType.LeftJoin,
                        "rh.EmployeeVacation", "EmployeeId",
                        Comparison.Equals,
                        "Employee", "Id");
            }
            return query;
        }
    }
}
