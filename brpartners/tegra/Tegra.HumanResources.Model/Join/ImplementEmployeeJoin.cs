﻿using CodeEngine.Framework.QueryBuilder;
using System.Collections.Generic;
using System.Linq;

namespace Tegra.HumanResources.Domain.Join
{
    public static class ImplementEmployeeJoin
    {
        public static SelectQueryBuilder GetEmployeeJoin(IEnumerable<string> tables, SelectQueryBuilder query)
        {
            if (tables.Contains("Employee"))
            {
                CompanyJoin.GetCompanyJoin(tables, query);

                PayslipJoin.GetPayslipJoin(tables, query);

                CourseJoin.GetCourseJoin(tables, query);

                VehicleJoin.GetVehicleJoin(tables, query);

                CivilStatusJoin.GetCivilStatusJoin(tables, query);

                EducationalStageJoin.GetEducationalStageJoin(tables, query);

                EmployeeAddressJoin.GetAddressJoin(tables, query);

                EmployeeBankAccountJoin.GetEmployeeBankAccountJoin(tables, query);

                EmployeeBenefitJoin.GetEmployeeBenefitJoin(tables, query);

                RoleJoin.GetRoleJoin(tables, query);

                EmployeeVariablesJoin.GetEmployeeVariablesJoin(tables, query);

                EmployeeSalaryJoin.GetEmployeeSalaryJoin(tables, query);

                EmployeeContractJoin.GetEmployeeContractJoin(tables, query);

                EmployeeVacationJoin.GetEmployeeVacationJoin(tables, query);

                EmployeeContactJoin.GetEmployeeContactJoin(tables, query);

                EmployeeRelationshipJoin.GetEmployeeRelationshipJoin(tables, query);

                EmployeeAttachmentJoin.GetEmployeeAttachmentJoin(tables, query);

                EmployeeSuperiorJoin.GetEmployeeSuperiorJoin(tables, query);

                BenefitBenefitTypeJoin.GetBenefitBenefitTypeJoin(tables, query);

                EmployeeVariablesTypeJoin.GetEmployeeVariablesType(tables, query);

                EmployeeCostCenterJoin.GetEmployeeCostCenterJoin(tables, query);
            }
            return query;
        }
    }
}