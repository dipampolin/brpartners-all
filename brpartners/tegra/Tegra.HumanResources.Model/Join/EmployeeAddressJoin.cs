﻿using CodeEngine.Framework.QueryBuilder;
using CodeEngine.Framework.QueryBuilder.Enums;
using System.Collections.Generic;
using System.Linq;

namespace Tegra.HumanResources.Domain.Join
{
    public static class EmployeeAddressJoin
    {
        public static SelectQueryBuilder GetAddressJoin(IEnumerable<string> tables, SelectQueryBuilder query)
        {

            if (!tables.Contains("EmployeeAddress") && tables.Contains("City"))
            {
                query.AddJoin(JoinType.LeftJoin,
                    "rh.EmployeeAddress", "EmployeeId",
                    Comparison.Equals,
                    "Employee", "Id");

                query.AddJoin(JoinType.LeftJoin,
                    "rh.City", "Id",
                    Comparison.Equals,
                    "EmployeeAddress", "CityId");

                if (tables.Contains("State"))
                    query.AddJoin(JoinType.LeftJoin,
                        "rh.State", "Id",
                        Comparison.Equals,
                        "City", "StateId");
            }

            if (tables.Contains("EmployeeAddress") && !tables.Contains("City"))
            {
                query.AddJoin(JoinType.LeftJoin,
                    "rh.EmployeeAddress", "EmployeeId",
                    Comparison.Equals,
                    "Employee", "Id");

                if (tables.Contains("State"))
                {
                    query.AddJoin(JoinType.LeftJoin,
                        "rh.City", "Id",
                        Comparison.Equals,
                        "EmployeeAddress", "CityId");

                    query.AddJoin(JoinType.LeftJoin,
                        "rh.State", "Id",
                        Comparison.Equals,
                        "City", "StateId");
                }
            }

            if (!tables.Contains("EmployeeAddress") && !tables.Contains("City") && tables.Contains("State"))
            {
                query.AddJoin(JoinType.LeftJoin,
                    "rh.EmployeeAddress", "EmployeeId",
                    Comparison.Equals,
                    "Employee", "Id");

                query.AddJoin(JoinType.LeftJoin,
                    "rh.City", "Id",
                    Comparison.Equals,
                    "EmployeeAddress", "CityId");

                query.AddJoin(JoinType.LeftJoin,
                    "rh.State", "Id",
                    Comparison.Equals,
                    "City", "StateId");
            }

            if (tables.Contains("EmployeeAddress") && tables.Contains("City") && tables.Contains("State"))
            {
                query.AddJoin(JoinType.LeftJoin,
                    "rh.EmployeeAddress", "EmployeeId",
                    Comparison.Equals,
                    "Employee", "Id");

                query.AddJoin(JoinType.LeftJoin,
                    "rh.City", "Id",
                    Comparison.Equals,
                    "EmployeeAddress", "CityId");

                query.AddJoin(JoinType.LeftJoin,
                    "rh.State", "Id",
                    Comparison.Equals,
                    "City", "StateId");
            }
            return query;
        }
    }
}
