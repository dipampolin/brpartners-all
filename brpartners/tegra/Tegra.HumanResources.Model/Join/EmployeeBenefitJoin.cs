﻿using CodeEngine.Framework.QueryBuilder;
using CodeEngine.Framework.QueryBuilder.Enums;
using System.Collections.Generic;
using System.Linq;

namespace Tegra.HumanResources.Domain.Join
{
    public class EmployeeBenefitJoin
    {
        public static SelectQueryBuilder GetEmployeeBenefitJoin(IEnumerable<string> tables, SelectQueryBuilder query)
        {
            if (tables.Contains("EmployeeBenefit") || tables.Contains("Benefit"))
            {
                query.AddJoin(JoinType.LeftJoin,
                    "rh.EmployeeBenefit", "EmployeeId",
                    Comparison.Equals,
                    "Employee", "Id");

                query.AddJoin(JoinType.LeftJoin,
                    "rh.Benefit", "Id",
                    Comparison.Equals,
                    "EmployeeBenefit", "BenefitId");
            }
            return query;
        }
    }
}
