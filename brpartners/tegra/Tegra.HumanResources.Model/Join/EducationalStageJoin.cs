﻿using CodeEngine.Framework.QueryBuilder;
using CodeEngine.Framework.QueryBuilder.Enums;
using System.Collections.Generic;
using System.Linq;

namespace Tegra.HumanResources.Domain.Join
{
    public static class EducationalStageJoin
    {
        public static SelectQueryBuilder GetEducationalStageJoin(IEnumerable<string> tables, SelectQueryBuilder query)
        {
            if (tables.Contains("EducationalStage"))
            {
                query.AddJoin(JoinType.LeftJoin,
                    "rh.EducationalStage", "Id",
                    Comparison.Equals,
                    "Employee", "EducationalStageId");
            }
            return query;
        }
    }
}
