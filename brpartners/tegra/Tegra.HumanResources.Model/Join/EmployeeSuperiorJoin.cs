﻿using CodeEngine.Framework.QueryBuilder;
using CodeEngine.Framework.QueryBuilder.Enums;
using System.Collections.Generic;
using System.Linq;

namespace Tegra.HumanResources.Domain.Join
{
    public class EmployeeSuperiorJoin
    {
        public static SelectQueryBuilder GetEmployeeSuperiorJoin(IEnumerable<string> tables, SelectQueryBuilder query)
        {
            if (tables.Contains("EmployeeSuperior"))
            {
                query.AddJoin(JoinType.LeftJoin,
                        "EmployeeSuperior", "Id",
                        Comparison.Equals,
                        "Employee", "SupervisorId");
            }
            return query;
        }
    }
}
