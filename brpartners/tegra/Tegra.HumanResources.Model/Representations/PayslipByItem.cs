﻿namespace Tegra.HumanResources.Domain.Representations
{
    public class PayslipByItem
    {
        public int IdPayslipItem { get; set; }

        public int Rubric { get; set; }

        public string RubricDescription { get; set; }

        public string RubricType { get; set; }

        public decimal Reference { get; set; }

        public decimal Value { get; set; }
    }
}
