﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Tegra.HumanResources.Domain.Representations
{
    public class NotificationRole
    {
        public string Key { get; set; }

        public string Label { get; set; }

        public static List<NotificationRole> GetEnum()
        {
            DescriptionAttribute attribute = null;
            var list = new List<NotificationRole>();
            foreach (var c in Enum.GetValues(typeof(Frequency)))
            {
                attribute = c.GetType().GetField(c.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false).SingleOrDefault() as DescriptionAttribute;
                list.Add(new NotificationRole { Key = c.ToString(), Label = attribute.Description });
            }
            return list;
        }

        public static string GetEnumDescription(string value)
        {
            DescriptionAttribute attribute = null;
            var list = new List<NotificationRole>();
            string description = "";
            foreach (var c in Enum.GetValues(typeof(Frequency)))
            {
                attribute = c.GetType().GetField(c.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false).SingleOrDefault() as DescriptionAttribute;
                if (value == c.ToString())
                {
                    description = attribute.Description;
                }
            }
            return description;
        }
    }

    public enum Frequency
    {
        [Description("Todo Dia")]
        EVERY_DAY,
        [Description("A Cada 3 Dias")]
        EVERY_THREE_DAYS,
        [Description("A Cada Semana")]
        EVERY_WEEK,
        [Description("A Cada 15 Dias")]
        EVERY_FIFTEEN_DAYS,
        [Description("Todo Mês")]
        EVERY_MONTH
    }
}
