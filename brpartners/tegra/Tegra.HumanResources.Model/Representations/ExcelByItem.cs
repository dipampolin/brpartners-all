﻿using Excel;
using System;
using System.Diagnostics;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Domain.Representations
{
    public class ExcelByItem
    {
        public static readonly string HEADER_ROW_COD_EMP = "COD_EMP";
        public static readonly string HEADER_ROW_NOME_EMPRESA = "NOME EMPRESA";
        public static readonly string HEADER_ROW_REGISTRO = "REGISTRO";
        public static readonly string HEADER_ROW_ANO = "ANO";
        public static readonly string HEADER_ROW_MES = "MES";
        public static readonly string HEADER_ROW_RUBRICA = "RUBRICA";
        public static readonly string HEADER_ROW_DESCR_RUBRICA = "DESCR RUBRICA";
        public static readonly string HEADER_ROW_TIPO_RUB = "TIPO_RUB";
        public static readonly string HEADER_ROW_REFERENC = "REFERENC";
        public static readonly string HEADER_ROW_VALOR = "VALOR";
        public static readonly string HEADER_ROW_CCUSTO = "CCUSTO";
        public static readonly string HEADER_ROW_DESCR_CCUSTO = "DESCR CCUSTO";
        public static readonly string HEADER_ROW_CARGO = "CARGO";
        public static readonly string HEADER_ROW_DESCR_CARGO = "DESCR CARGO";

        public static int COD_EMPRESA = 0;
        public static int NOME_EMPRESA = 1;
        public static int REGISTRO = 2;
        public static int ANO = 3;
        public static int MES = 4;
        public static int RUBRICA = 5;
        public static int DESC_RUBRICA = 6;
        public static int TYPE_RUBRICA = 7;
        public static int REFERENCIA = 8;
        public static int VALOR = 9;
        public static int CENTRO_CUSTO = 10;
        public static int CENTRO_CUSTO_DESCRIPTION = 11;
        public static int CARGO = 12;
        public static int DESC_CARGO = 13;

        public static PayslipItem CreatePayslipItem(IExcelDataReader excelReader, Payslip payslip)
        {
            return new PayslipItem(payslip, excelReader.GetDecimal(VALOR))
            {
                Rubric = excelReader.GetInt32(RUBRICA),
                RubricDescription = excelReader.GetString(DESC_RUBRICA),
                RubricType = excelReader.GetString(TYPE_RUBRICA),
                Reference = excelReader.GetDecimal(REFERENCIA)
            };
        }

        public static Payslip CreatePayslip(IExcelDataReader excelReader, int employeeId, CostCenter ceterCost, Company company, DateTime date)
        {
            try
            {
                return new Payslip(date, employeeId, ceterCost.Id, company.Id)
                {
                    CostCenterCode = excelReader.GetInt32(CENTRO_CUSTO),
                    CostCenterDescription = excelReader.GetString(CENTRO_CUSTO_DESCRIPTION),
                    RoleCode = excelReader.GetInt32(CARGO),
                    RoleDescription = excelReader.GetString(DESC_CARGO),
                    CompanyCode = excelReader.GetInt32(COD_EMPRESA),
                    CompanyName = excelReader.GetString(NOME_EMPRESA)
                };
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                throw;
            }
        }
    }
}
