﻿using System;
using System.Collections.Generic;

namespace Tegra.HumanResources.Domain.Representations
{
    public class PayslipByExcel
    {
        public int IdPayslip { get; set; }

        public DateTime Date { get; set; }

        public int CompanyCode { get; set; }

        public string CompanyName { get; set; }

        public string CostCenterCode { get; set; }

        public string CostCenterDescription { get; set; }

        public int RoleCode { get; set; }

        public string RoleDescription { get; set; }

        public List<PayslipByItem> Items { get; set; }

        public int IdCustomer { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }
    }
}
