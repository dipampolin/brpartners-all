﻿using System.Collections.Generic;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Domain.Representations
{
    public class ReportByAlias
    {
        public string Table { get; set; }
        public string TableAlias { get; set; }
        public string FieldAlias { get; set; }
        public string Field { get; set; }
        public string Value { get; set; }
        public string Type { get; set; }

        public static List<ReportItem> Factory(List<ReportByAlias> alias)
        {
            var reportList = new List<ReportItem>();

            alias.ForEach(c =>
            {
                reportList.Add(new ReportItem
                {
                    FieldAlias = c.FieldAlias,
                    Field = c.Field,
                    Table = c.Table,
                    TableAlias = c.TableAlias,
                    Type = c.Type
                });

            });
            return reportList;
        }

        public static List<ReportFilter> FactoryFilter(List<ReportByAlias> alias)
        {
            var reportList = new List<ReportFilter>();

            alias.ForEach(c =>
            {
                reportList.Add(new ReportFilter
                {
                    FieldAlias = c.FieldAlias,
                    Field = c.Field,
                    Table = c.Table,
                    TableAlias = c.TableAlias,
                    Value = c.Value,
                    Type = c.Type
                });

            });
            return reportList;
        }
    }
}