﻿namespace Tegra.HumanResources.Domain.Representations
{
    public class SendEmail
    {
        public string Subject { get; set; }

        public string From { get; set; }

        public string To { get; set; }

        public dynamic Body { get; set; }
    }
}
