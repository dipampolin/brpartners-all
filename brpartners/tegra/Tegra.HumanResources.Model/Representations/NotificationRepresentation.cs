﻿using System.Collections.Generic;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Domain.Representations
{
    public class NotificationRepresentation
    {
        public string Table { get; set; }
        public string TableAlias { get; set; }
        public string FieldAlias { get; set; }
        public string Field { get; set; }
        public string Value { get; set; }
        public string Type { get; set; }

        public static List<NotificationFilter> FactoryFilter(List<NotificationRepresentation> alias)
        {
            var reportList = new List<NotificationFilter>();

            alias.ForEach(c =>
            {
                reportList.Add(new NotificationFilter
                {
                    FieldAlias = c.FieldAlias,
                    Field = c.Field,
                    Table = c.Table,
                    TableAlias = c.TableAlias,
                    Value = c.Value,
                    Type = c.Type
                });

            });
            return reportList;
        }
    }
}
