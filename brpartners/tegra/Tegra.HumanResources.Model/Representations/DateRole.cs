﻿using System.Collections.Generic;
using System.Linq;

namespace Tegra.HumanResources.Domain.Representations
{
    public class DateRole
    {
        public string Key { get; set; }

        public string Label { get; set; }

        public string Command { get; set; }

        public static List<DateRole> GetDictionaryRoles()
        {
            var dateRole = new List<DateRole>
            {
                new DateRole { Key="LAST_M", Label = "Mês Anterior", Command = string.Format("DATEADD(MM, DATEDIFF(MM, 0, GETDATE()) -1, 0) AND EOMONTH(GETDATE(), -1)")},
                new DateRole { Key="CUR_M", Label = "Mês Corrente", Command = string.Format("DATEADD(MM, DATEDIFF(MM, 0, GETDATE()), 0) AND EOMONTH(GETDATE())")},
                new DateRole { Key="NEX_M", Label = "Mês Seguinte", Command = string.Format("DATEADD(MM, DATEDIFF(MM, 0, GETDATE()) + 1, 0) AND EOMONTH(GETDATE(), +1)")},
                new DateRole { Key="CUR_TRI", Label = "Últimos 90 Dias", Command = string.Format("DATEADD(DD, DATEDIFF(DD, 0, GETDATE()) - 90, 0) AND DATEADD(DD, DATEDIFF(DD, 0, GETDATE()), 0)")},
                new DateRole { Key="CUR_SEM", Label = "Últimos 180 Dias", Command = string.Format("DATEADD(DD, DATEDIFF(DD, 0, GETDATE()) - 180, 0) AND DATEADD(DD, DATEDIFF(DD, 0, GETDATE()), 0)")},
                new DateRole { Key="CUR_YEA", Label = "Últimos 365 Dias", Command = string.Format("DATEADD(DD, DATEDIFF(DD, 0, GETDATE()) - 365, 0) AND DATEADD(DD, DATEDIFF(DD, 0, GETDATE()), 0)")},
                new DateRole { Key="NEX_TRI", Label = "Próximos 90 Dias", Command = string.Format("DATEADD(DD, DATEDIFF(DD, 0, GETDATE()), 0) AND DATEADD(DD, DATEDIFF(DD, 0, GETDATE()) + 90, 0)")},
                new DateRole { Key="NEX_SEM", Label = "Próximos 180 Dias", Command = string.Format("DATEADD(DD, DATEDIFF(DD, 0, GETDATE()), 0) AND DATEADD(DD, DATEDIFF(DD, 0, GETDATE()) + 180, 0)")},
                new DateRole { Key="NEX_YEA", Label = "Próximos 365 Dias", Command = string.Format("DATEADD(DD, DATEDIFF(DD, 0, GETDATE()), 0) AND DATEADD(DD, DATEDIFF(DD, 0, GETDATE()) + 365, 0)")}
            };

            return dateRole;
        }


        public static DateRole GetDateRoles(string value)
        {
            var roleDate = GetDictionaryRoles();
            return roleDate.FirstOrDefault(c => c.Key == value);
        }
    }
}
