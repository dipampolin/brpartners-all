﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tegra.HumanResources.Domain.Representations
{
    public enum StatusHttp
    {
        CONFLICT = 409,
        PRECONDITION_FAILED = 412,
        INTERNAL_SERVER_ERROR = 500
    }
}
