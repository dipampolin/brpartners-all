﻿using System;

namespace Tegra.HumanResources.Domain.Representations
{
    public class PayslipByMonth
    {

        public int Count { get; set; }

        public string CompanyName { get; set; }

        public int CompanyCode { get; set; }

        public DateTime Date { get; set; }

    }
}
