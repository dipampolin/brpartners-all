﻿using System;
using System.Collections.Generic;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Domain.Representations
{
    public class PayslipByException
    {
        public PayslipByException()
        {
            Payslips = new List<Payslip>();
            Exceptions = new List<Exception>();
        }

        public List<Payslip> Payslips { get; set; }

        public List<Exception> Exceptions { get; set; }
    }
}
