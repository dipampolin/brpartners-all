﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tegra.HumanResources.Domain.Representations
{
    public enum AbaRepresentation
    {
        ABA_GERAL,
        ABA_ENDERECO,
        ABA_CONTATOS,
        ABA_VEICULO,
        ABA_CONTA_BANCARIA,
        ABA_PESSOA_RELACIONADA,
        ABA_CURSO,
        ABA_HISTORICO,
        ABA_LINHA_SALARIO,
        ABA_BENEFICIO,
        ABA_VARIAVEIS,
        ABA_FERIAS,
        ABA_ANEXOS,
        ABA_VENCIMENTO_CONTRATO
    }
}
