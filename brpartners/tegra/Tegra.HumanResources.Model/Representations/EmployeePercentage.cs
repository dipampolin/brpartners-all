﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tegra.HumanResources.Domain.Representations
{
    public class EmployeePercentage
    {
        public AbaRepresentation Aba { get; set; }
        public int Quantity { get; set; }
        public int Weight { get; set; }
    }
}
