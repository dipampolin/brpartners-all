﻿using System;
using System.Collections.Generic;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Domain.Service
{
    public interface IPayslipService
    {
        bool DeleteAll(List<Payslip> payslips);

        bool Remove(DateTime date, int employeeId);

        bool RemoveByCompany(DateTime date, int CompanyCode);

        bool InsertAll(List<Payslip> payslips);

        void VerifyIfExistThenDelete(List<Payslip> payslips);
    }
}
