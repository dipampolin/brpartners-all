﻿using Tegra.HumanResources.Domain.Entities;
namespace Tegra.HumanResources.Services
{
    public interface IEmployeeService
    {
        Employee Create(Employee entity);

        int CalculateByPercentage(Employee employee, int employeeId = 0);
    }
}