﻿using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Exceptions;
using Tegra.HumanResources.Domain.Repository;

namespace Tegra.HumanResources.Services
{
    public class BenefitService : IBenefitService
    {
        private readonly IBenefitRepository _benefitRepository;
        private readonly IEmployeeBenefitRepository _employeeBenefitRepository;
        private readonly IRelationshipRepository _relationshipRepository;

        public BenefitService(IBenefitRepository benefitRepository, IEmployeeBenefitRepository employeeBenefitRepository, IRelationshipRepository relationshipRepository)
        {
            this._benefitRepository = benefitRepository;
            this._employeeBenefitRepository = employeeBenefitRepository;
            this._relationshipRepository = relationshipRepository;
        }

        public Benefit Save(Benefit data)
        {
            if (data.Id == 0)
            {
                return this._benefitRepository.Add(data);
            }

            var entity = _benefitRepository.Find(data.Id);
            entity.BenefitTypeId = data.BenefitTypeId;
            entity.Description = data.Description;
            entity.Value = data.Value;
            return _benefitRepository.Update(entity);
        }

        public void Remove(int id)
        {

            var qtdEmployeeBenefit = _employeeBenefitRepository.SelectBy(employeeBenefit => employeeBenefit.BenefitId == id).Count;
            var qtdEmployeeRelationshipHealthPlan = _relationshipRepository.SelectBy(r => r.HealthPlan.Id == id).Count;
            var qtdEmployeeRelationshipOdontologyPlan = _relationshipRepository.SelectBy(r => r.OdontologyPlan.Id == id).Count;

            if ((qtdEmployeeBenefit + qtdEmployeeRelationshipHealthPlan  + qtdEmployeeRelationshipOdontologyPlan) > 0)
            {
                throw new ForeignKeyEmployeeException();
            }

            var entity = _benefitRepository.Find(id);
            _benefitRepository.Remove(entity, false);
        }
    }
}