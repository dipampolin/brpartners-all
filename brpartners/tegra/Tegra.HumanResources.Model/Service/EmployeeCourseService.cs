﻿using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Repository;

namespace Tegra.HumanResources.Domain.Service
{
    public class EmployeeCourseService : IEmployeeCourseService
    {
        private readonly IEmployeeCourseRepository _repository;

        public EmployeeCourseService(IEmployeeCourseRepository repository)
        {
            this._repository = repository;
        }

        public CourseEmployee Create(CourseEmployee data)
        {
            if (data.Id == 0)
            {
                return this._repository.Add(data);
            }

            var entity = _repository.Find(data.Id);
            entity.CourseId = data.CourseId;
            entity.Deadline = data.Deadline;
            entity.EmployeeId = data.EmployeeId;
            entity.EndDate = data.EndDate;
            entity.Required = data.Required;
            entity.StartDate = data.StartDate;
            return _repository.Update(entity);
        }

        public bool Remove(int id, bool logic = true)
        {
            return _repository.Remove(id, logic);
        }
    }
}
