﻿using System.IO;
using System.Linq;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Repository;

namespace Tegra.HumanResources.Domain.Service
{
    public class EmployeeAttachmentService : IEmployeeAttachmentService
    {
        private readonly IEmployeeAttachmentRepository _repository;

        public EmployeeAttachmentService(IEmployeeAttachmentRepository repository)
        {
            this._repository = repository;
        }

        public bool Remove(int id, bool logic)
        {
            return _repository.Remove(id, logic);
        }

        public EmployeeAttachment Save(EmployeeAttachment data)
        {
            if (data.Id == 0)
            {
                return this._repository.Add(data);
            }

            var entity = _repository.Query(c => c.Id == data.Id).FirstOrDefault();

            entity.EmployeeId = data.EmployeeId;
            entity.Description = data.Description;
            entity.Id = data.Id;
            entity.FileContent = data.FileContent;
            entity.FileName = data.FileName;
            return _repository.Update(entity);
        }

        public byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
    }
}
