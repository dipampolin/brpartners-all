﻿using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Repository;

namespace Tegra.HumanResources.Services
{
    public class AddressService : IAddressService
    {
        private readonly IAddressRepository _addressRepository;

        public AddressService(IAddressRepository addressRepository)
        {
            this._addressRepository = addressRepository;
        }

        public EmployeeAddress Save(EmployeeAddress data)
        {
            return _addressRepository.InsertOrUpdate(data, data.Id);
        }

        public bool Remove(int id, bool logic = true)
        {
            return _addressRepository.Remove(id, logic);
        }
    }
}