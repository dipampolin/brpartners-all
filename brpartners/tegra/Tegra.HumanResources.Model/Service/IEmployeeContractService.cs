﻿using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Domain.Repository
{
    public interface IEmployeeContractService
    {
        EmployeeContract Create(EmployeeContract employeeBenefit);

        bool Remove(int id, bool logic);
    }
}
