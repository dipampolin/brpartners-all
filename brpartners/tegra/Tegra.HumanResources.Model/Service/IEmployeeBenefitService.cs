﻿using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Domain.Service
{
    public interface IEmployeeBenefitService
    {
        EmployeeBenefit Create(EmployeeBenefit employeeBenefit);

        bool Remove(int id, bool logic);
    }
}
