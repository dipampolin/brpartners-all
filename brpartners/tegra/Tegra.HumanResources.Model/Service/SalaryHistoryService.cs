﻿using System.Linq;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Repository;

namespace Tegra.HumanResources.Domain.Service
{
    public class SalaryHistoryService : ISalaryHistoryService
    {
        private readonly ISalaryHistoryRepository _repository;

        public SalaryHistoryService(ISalaryHistoryRepository repository)
        {
            this._repository = repository;
        }

        public bool Remove(int id, bool logic = true)
        {
            return _repository.Remove(id, logic);
        }

        public EmployeeSalary Save(EmployeeSalary data)
        {
            if (data.Id == 0)
            {
                return this._repository.Add(data);
            }

            var entity = _repository.Query(c => c.Id == data.Id).FirstOrDefault();

            entity.EmployeeId = data.EmployeeId;
            entity.Description = data.Description;
            entity.Id = data.Id;
            entity.Category = data.Category;
            entity.EndDate = data.EndDate;
            entity.ExtraHour = data.ExtraHour;
            entity.Gratification = data.Gratification;
            entity.Salary = data.Salary;
            entity.SalaryPeriod = data.SalaryPeriod;
            entity.StartDate = data.StartDate;
            entity.Triennium = data.Triennium;
            entity.WorkLoad = data.WorkLoad;

            return _repository.Update(entity);
        }
    }
}
