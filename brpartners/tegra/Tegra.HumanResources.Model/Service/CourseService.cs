﻿using System.Data.Entity;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Repository;
using System.Linq;
using System;
using Tegra.HumanResources.Domain.Exceptions;

namespace Tegra.HumanResources.Services
{
    public class CourseService : ICourseService
    {

        private readonly ICourseRepository _courseRepository;

        private readonly IEmployeeCourseRepository _employeeCourseRepository;

        public CourseService(ICourseRepository courseRepository, IEmployeeCourseRepository employeeCourseRepository)
        {
            this._courseRepository = courseRepository;
            this._employeeCourseRepository = employeeCourseRepository;
        }

        public Course Save(Course data)
        {
            if (data.Id == 0)
            {
                return this._courseRepository.Add(data);
            }

            var entity = _courseRepository.Query(x => x.Id == data.Id)
                .Include(x => x.CourseEmployees)
                .Single();

            entity.Name = data.Name;
            entity.Active = data.Active;

            entity.CourseEmployees.RemoveAll(x => !data.CourseEmployees.Exists(y => y.EmployeeId == x.EmployeeId));

            data.CourseEmployees.ForEach(x =>
            {

                var ce = entity.CourseEmployees.FirstOrDefault(y => y.EmployeeId == x.EmployeeId);

                if (ce == null)
                {
                    entity.CourseEmployees.Add(x);
                } else
                {
                    ce.Deadline = x.Deadline;
                    ce.Required = x.Required;
                }
            });

            return _courseRepository.Update(entity);
        }

        public void Remove(int id)
        {
            var qtdEmployeeCourse = _employeeCourseRepository.SelectBy(employeeCourse => employeeCourse.CourseId == id).Count;

            if (qtdEmployeeCourse > 0)
            {
                throw new ForeignKeyEmployeeException();
            }

            var entity = _courseRepository.Find(id);
            _courseRepository.Remove(entity, false);
        }
    }
}
