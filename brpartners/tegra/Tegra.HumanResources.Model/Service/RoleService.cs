﻿using System;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Exceptions;
using Tegra.HumanResources.Domain.Repository;

namespace Tegra.HumanResources.Domain.Service
{
    public class RoleService : IRoleService
    {

        private readonly IRoleRepository _roleRepository;

        private readonly IEmployeeRoleRepository _employeeRoleRepository;

        public RoleService(IRoleRepository roleRepository, IEmployeeRoleRepository employeeRoleRepository)
        {
            this._roleRepository = roleRepository;
            this._employeeRoleRepository = employeeRoleRepository;
        }

        public Role Save(Role role)
        {
            if (role.Id == 0)
            {
                role.CreatedDate = DateTime.Now;
                return this._roleRepository.Add(role);
            }

            var entity = _roleRepository.Find(role.Id);
            entity.Name = role.Name;
            entity.Active = role.Active;
            return _roleRepository.Update(entity);
        }

        public void Remove(int id)
        {
            var qtdEmployeeRole = _employeeRoleRepository.SelectBy(employeeRole => employeeRole.RoleId == id).Count;
            
            if (qtdEmployeeRole > 0)
            {
                throw new ForeignKeyEmployeeException();
            }

            var entity = _roleRepository.Find(id);
            _roleRepository.Remove(entity, false);
        }
    }
}
