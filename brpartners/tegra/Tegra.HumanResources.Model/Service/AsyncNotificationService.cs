﻿using System;
using System.Data.Entity;
using System.Linq;
using Tegra.HumanResources.Domain.Repository;
using Tegra.HumanResources.Domain.Representations;
using Tegra.HumanResources.Domain.Util;

namespace Tegra.HumanResources.Domain.Service
{
    public class AsyncNotificationService : IAsyncNotificationService
    {
        private readonly INotificationService _notificationService;
        private readonly INotificationDestinationRepository _destinationRepository;
        private readonly ITemplateGenerator _generator;

        public AsyncNotificationService(INotificationService notificationService, INotificationDestinationRepository repository, ITemplateGenerator generator)
        {
            this._notificationService = notificationService;
            this._destinationRepository = repository;
            this._generator = generator;
        }

        public void SendNotifications()
        {
            if (DateTime.Now.Hour < 9 && DateTime.Now.Hour > 18)
            {
                return;
            }
            _destinationRepository.Query(destination =>
                destination.Notification.Active &&
                    (destination.LastSent == null ||
                    (destination.Notification.Frequency == Frequency.EVERY_DAY.ToString() && DbFunctions.DiffDays(destination.LastSent, DateTime.Today) >= 1) ||
                    (destination.Notification.Frequency == Frequency.EVERY_MONTH.ToString() && destination.LastSent.Value.Month != DateTime.Today.Month) ||
                    (destination.Notification.Frequency == Frequency.EVERY_THREE_DAYS.ToString() && DbFunctions.DiffDays(destination.LastSent, DateTime.Today) >= 3) ||
                    (destination.Notification.Frequency == Frequency.EVERY_FIFTEEN_DAYS.ToString() && DbFunctions.DiffDays(destination.LastSent, DateTime.Today) >= 15) ||
                    (destination.Notification.Frequency == Frequency.EVERY_WEEK.ToString() && DbFunctions.DiffDays(destination.LastSent, DateTime.Today) >= 7))
           ).Include(d => d.Notification).ToList().ForEach(destination =>
            {
                var model = _notificationService.GenerateNotification(destination.Notification.Query);

                if (model.Rows.Count == 0)
                {
                    return;
                }

                var emailBody = _generator.Generate(model);

                var email = new SendEmail
                {
                    To = destination.Email,
                    Body = emailBody,
                    From = "site.qx3@qx3.com.br",
                    Subject = destination.Notification.Name
                };

                try
                {
                    _notificationService.Send(email);
                    destination.LastSent = DateTime.Now;
                    _destinationRepository.Update(destination);
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message);

                }
            });
        }
    }
}
