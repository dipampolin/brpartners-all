﻿using System;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Exceptions;
using Tegra.HumanResources.Domain.Repository;

namespace Tegra.HumanResources.Domain.Service
{
    public class CostCenterService : ICostCenterService
    {
        private readonly ICostCenterRepository _costCenterRepository;

        private readonly IEmployeeRepository _employeeRepository;

        public CostCenterService(ICostCenterRepository costCenterRepository, IEmployeeRepository employeeRepository)
        {
            this._costCenterRepository = costCenterRepository;
            this._employeeRepository = employeeRepository;
        }

        public CostCenter Save(CostCenter data)
        {
            if (data.Id == 0)
            {
                return this._costCenterRepository.Add(data);
            }

            var entity = _costCenterRepository.Find(data.Id);
            entity.Name = data.Name;
            entity.Code = data.Code;
            return _costCenterRepository.Update(entity);
        }

        public void Remove(int id = 0)
        {
            var qtdEmployeeCostCenter = _employeeRepository.SelectBy(employee => employee.CostCenterId == id).Count;

            if (qtdEmployeeCostCenter > 0)
            {
                throw new ForeignKeyEmployeeException();
            }

            var entity = _costCenterRepository.Find(id);
            _costCenterRepository.Remove(entity, false);
        }
    }
}
