﻿using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Domain.Service
{
    public interface IEmployeeVariablesService
    {
        EmployeeVariables Create(EmployeeVariables employeeVariables);

        bool Remove(int id, bool logic);
    }
}
