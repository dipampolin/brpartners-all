﻿using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Repository;

namespace Tegra.HumanResources.Domain.Service
{
    public class EmployeeVacationService : IEmployeeVacationService
    {
        private readonly IEmployeeVacationRepository _repository;

        public EmployeeVacationService(IEmployeeVacationRepository repository)
        {
            this._repository = repository;
        }

        public EmployeeVacation Create(EmployeeVacation data)
        {
            if (data.Id == 0)
            {
                return this._repository.Add(data);
            }

            var entity = _repository.Find(data.Id);
            entity.AquisitionEndDate = data.AquisitionEndDate;
            entity.EmployeeId = data.EmployeeId;
            entity.AquisitionStartDate = data.AquisitionStartDate;
            entity.EmployeeId = data.EmployeeId;
            entity.EnjoymentEndDate = data.EnjoymentEndDate;
            entity.EnjoymentStartDate = data.EnjoymentStartDate;
            entity.Id = data.Id;
            entity.ReceiptDate = data.ReceiptDate;
            entity.Value = data.Value;

            return _repository.Update(entity);
        }

        public bool Remove(int id, bool logic = true)
        {
            return _repository.Remove(id, logic);
        }
    }
}
