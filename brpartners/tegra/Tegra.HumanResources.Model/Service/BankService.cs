﻿using System;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Exceptions;
using Tegra.HumanResources.Domain.Repository;

namespace Tegra.HumanResources.Domain.Service
{
    public class BankService : IBankService
    {

        private readonly IBankRepository _bankRepository;

        public BankService(IBankRepository bankRepository)
        {
            this._bankRepository = bankRepository;
        }

        public Bank Save(Bank bank)
        {
            if (bank.Id == 0)
            {
                bank.CreatedDate = DateTime.Now;
                return this._bankRepository.Add(bank);
            }

            var entity = _bankRepository.Find(bank.Id);
            entity.Code = bank.Code;
            entity.Name = bank.Name;
            entity.Active = bank.Active;

            return _bankRepository.Update(entity);
        }

        public void Remove(int id)
        {
            var entity = _bankRepository.Find(id);
            _bankRepository.Remove(entity, true);
        }
    }
}
