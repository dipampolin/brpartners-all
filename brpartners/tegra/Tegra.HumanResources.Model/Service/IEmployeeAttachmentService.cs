﻿using System.IO;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Domain.Service
{
    public interface IEmployeeAttachmentService
    {
        EmployeeAttachment Save(EmployeeAttachment data);

        bool Remove(int id, bool logic);

        byte[] ReadFully(Stream input);
    }
}
