﻿using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Services
{
    public interface ICompanyService
    {
        Company Save(Company entity);

        void Remove(int id);
    }
}