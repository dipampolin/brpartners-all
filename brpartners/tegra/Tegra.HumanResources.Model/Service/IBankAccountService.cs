﻿using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Services
{
    public interface IBankAccountService
    {
        EmployeeBankAccount Save(EmployeeBankAccount entity);

        bool Remove(int id, bool logic);
    }
}