﻿using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Services
{
    public interface ICourseService
    {
        Course Save(Course entity);

        void Remove(int id);
    }
}