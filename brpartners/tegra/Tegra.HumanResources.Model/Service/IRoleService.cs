﻿using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Domain.Service
{
    public interface IRoleService
    {
        Role Save(Role role);

        void Remove(int id);
    }
}
