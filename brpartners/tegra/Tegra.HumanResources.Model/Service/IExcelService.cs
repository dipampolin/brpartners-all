﻿using Excel;
using System.Collections.Generic;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Representations;

namespace Tegra.HumanResources.Domain.Service
{
    public interface IExcelService
    {
        PayslipByException ProcessExcel(IExcelDataReader excelReader);
    }
}
