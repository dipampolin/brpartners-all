﻿using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Services
{
    public interface IBenefitService
    {
        Benefit Save(Benefit entity);

        void Remove(int id);
    }
}