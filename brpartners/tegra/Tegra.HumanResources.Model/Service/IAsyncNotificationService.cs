﻿namespace Tegra.HumanResources.Domain.Service
{
    public interface IAsyncNotificationService
    {
        void SendNotifications();
    }
}
