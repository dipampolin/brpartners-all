﻿using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Domain.Service
{
    public interface IEmployeeCourseService
    {
        CourseEmployee Create(CourseEmployee employeeBenefit);

        bool Remove(int id, bool logic);
    }
}
