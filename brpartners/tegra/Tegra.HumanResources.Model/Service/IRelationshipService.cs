﻿using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Services
{
    public interface IRelationshipService
    {
        EmployeeRelationship Save(EmployeeRelationship entity);

        bool Remove(int id, bool logic);
    }
}