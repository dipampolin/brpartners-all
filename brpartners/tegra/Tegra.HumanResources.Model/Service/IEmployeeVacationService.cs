﻿using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Domain.Service
{
    public interface IEmployeeVacationService
    {
        EmployeeVacation Create(EmployeeVacation employeeBenefit);

        bool Remove(int id, bool logic);
    }
}
