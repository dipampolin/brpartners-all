﻿using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Domain.Service
{
    public interface IBankService
    {
        Bank Save(Bank bank);

        void Remove(int id);
    }
}
