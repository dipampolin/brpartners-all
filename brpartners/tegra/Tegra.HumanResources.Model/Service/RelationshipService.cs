﻿using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Repository;

namespace Tegra.HumanResources.Services
{
    public class RelationshipService : IRelationshipService
    {
        private readonly IRelationshipRepository _repository;

        public RelationshipService(IRelationshipRepository repository)
        {
            this._repository = repository;
        }

        public EmployeeRelationship Save(EmployeeRelationship data)
        {
            return _repository.InsertOrUpdate(data, data.Id);
        }

        public bool Remove(int id, bool logic = true)
        {
            return _repository.Remove(id, logic);
        }
    }
}