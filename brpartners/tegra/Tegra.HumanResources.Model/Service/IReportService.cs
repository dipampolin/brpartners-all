﻿using System.Collections.Generic;
using System.Data;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Representations;

namespace Tegra.HumanResources.Domain.Service
{
    public interface IReportService
    {
        string GenerateQuery(Report report);

        bool ExecuteQuery(string query);

        Report Save(Report data);

        DataTable GenerateReport(Report report);

        List<ReportByAlias> GetReportAlias(string nameSpace = "Tegra.HumanResources.Domain.Entities");

        List<ReportByAlias> GetReportFilter(List<string> names, int id, string nameSpace = "Tegra.HumanResources.Domain.Entities");

        Report CreateFilters(List<ReportFilter> reportItem, int reportId);

        bool Remove(int reportId);

        Report UpdateQuery(Report data, List<ReportFilter> items);
    }
}
