﻿using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Repository;

namespace Tegra.HumanResources.Services
{
    public class VehicleService : IVehicleService
    {
        private readonly IVehicleRepository _repository;

        public VehicleService(IVehicleRepository repository)
        {
            this._repository = repository;
        }

        public EmployeeVehicle Save(EmployeeVehicle data)
        {
            if (data.Id == 0)
            {
                return this._repository.Add(data);
            }

            var entity = _repository.Find(data.Id);
            entity.Brand = data.Brand;
            entity.Model = data.Model;
            entity.Color = data.Color;
            entity.CarPlate = data.CarPlate;
            entity.Active = data.Active;
            return _repository.Update(entity);
        }

        public bool Remove(int id, bool logic = true)
        {
            return _repository.Remove(id, logic);
        }
    }
}