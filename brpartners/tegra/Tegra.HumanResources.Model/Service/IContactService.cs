﻿using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Services
{
    public interface IContactService
    {
        EmployeeContact Save(EmployeeContact entity);

        bool Remove(int id, bool logic);
    }
}