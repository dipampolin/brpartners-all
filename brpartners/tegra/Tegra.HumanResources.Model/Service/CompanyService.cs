﻿using System;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Exceptions;
using Tegra.HumanResources.Domain.Repository;

namespace Tegra.HumanResources.Services
{
    public class CompanyService : ICompanyService
    {
        private readonly ICompanyRepository _companyRepository;

        private readonly IEmployeeCompanyRepository _employeeCompanyRepository;

        public CompanyService(ICompanyRepository companyRepository, IEmployeeCompanyRepository employeeRepository)
        {
            this._companyRepository = companyRepository;
            this._employeeCompanyRepository = employeeRepository;
        }

        public Company Save(Company data)
        {
            if (data.Id == 0)
            {
                return this._companyRepository.Add(data);
            }

            var entity = _companyRepository.Find(data.Id);
            entity.Name = data.Name;
            entity.Cnpj = data.Cnpj;
            entity.Code = data.Code;
            return _companyRepository.Update(entity);
        }

        public void Remove(int id)
        {
            var qtdEmployeeCompany = _employeeCompanyRepository.SelectBy(employeeCompany => employeeCompany.CompanyId == id).Count;

            if (qtdEmployeeCompany > 0)
            {
                throw new ForeignKeyEmployeeException();
            }

            var entity = _companyRepository.Find(id);
            _companyRepository.Remove(entity, false);
        }
    }
}
