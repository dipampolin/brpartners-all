﻿using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Repository;

namespace Tegra.HumanResources.Services
{
    public class ContactService : IContactService
    {

        private readonly IContactRepository _repository;

        public ContactService(IContactRepository repository)
        {
            this._repository = repository;
        }

        public bool Remove(int id, bool logic = true)
        {
            return _repository.Remove(id, logic);
        }

        public EmployeeContact Save(EmployeeContact data)
        {
            return _repository.InsertOrUpdate(data, data.Id);
        }

    }
}