﻿using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Repository;

namespace Tegra.HumanResources.Domain.Service
{
    public class EmployeeBenefitService : IEmployeeBenefitService
    {
        private readonly IEmployeeBenefitRepository _repository;

        public EmployeeBenefitService(IEmployeeBenefitRepository repository)
        {
            this._repository = repository;
        }

        public EmployeeBenefit Create(EmployeeBenefit data)
        {
            if (data.Id == 0)
            {
                return this._repository.Add(data);
            }

            var entity = _repository.Find(data.Id);
            entity.BenefitId = data.BenefitId;
            entity.EmployeeId = data.EmployeeId;
            entity.StartDate = data.StartDate;
            entity.EndDate = data.EndDate;
            entity.CustomValue = data.CustomValue;
            entity.CardNumber = data.CardNumber;

            return _repository.Update(entity);
        }

        public bool Remove(int id, bool logic = true)
        {
            return _repository.Remove(id, logic);
        }
    }
}
