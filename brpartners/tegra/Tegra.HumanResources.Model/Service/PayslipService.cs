﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Repository;

namespace Tegra.HumanResources.Domain.Service
{
    public class PayslipService : IPayslipService
    {
        private readonly IPayslipRepository _repository;

        public PayslipService(IPayslipRepository repository)
        {
            this._repository = repository;
        }

        public bool DeleteAll(List<Payslip> payslips)
        {

            using (var transaction = _repository.BeginTransaction())
            {
                try
                {
                    _repository.RemoveAll(payslips);
                    transaction.Commit();
                    return true;
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    throw e;
                }
            }


        }

        public bool InsertAll(List<Payslip> payslips)
        {
            using (var transaction = _repository.BeginTransaction())
            {
                try
                {
                    _repository.AddAll(payslips);
                    transaction.Commit();
                    return true;
                }

                catch (Exception e)
                {
                    transaction.Rollback();
                    throw e;
                }
            }
        }

        public bool Remove(DateTime date, int employeeId)
        {
            var payslip = _repository.Query(x => x.Date == date && x.EmployeeId == employeeId).SingleOrDefault();

            if (payslip == null)
            {
                return false;
            }

            return _repository.Remove(payslip, false);
        }

        public void VerifyIfExistThenDelete(List<Payslip> payslips)
        {
            payslips.ForEach(c =>
            {
                var tempPayslips = _repository.SelectBy(d => d.CompanyCode == c.CompanyCode && d.Employee.Id == c.EmployeeId);
                _repository.RemoveAll(tempPayslips);

            });
        }

        public bool RemoveByCompany(DateTime date, int CompanyCode)
        {
            var payslips = _repository.Query(x => x.CompanyCode == CompanyCode && x.Date == date).ToList();
            return _repository.RemoveAll(payslips);
        }

    }
}
