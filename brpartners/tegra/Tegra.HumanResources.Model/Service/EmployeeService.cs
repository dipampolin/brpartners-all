﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Repository;
using Tegra.HumanResources.Domain.Representations;

namespace Tegra.HumanResources.Services
{
    public class EmployeeService : IEmployeeService
    {

        private readonly IEmployeeRepository _employeeRepository;
        private readonly IRelationshipRepository _relationshipRepository;
        private readonly IBankAccountRepository _bankAccountRepository;
        private readonly IContactRepository _contactRepository;
        private readonly ISalaryHistoryRepository _salaryHistoryRepository;
        private readonly IEmployeeBenefitRepository _employeeBenefitRepository;
        private readonly IEmployeeVariablesRepository _employeeVariablesRepository;
        private readonly IEmployeeVacationRepository _employeeVacationRepository;
        private readonly IVehicleRepository _vehicleRepository;
        private readonly IEmployeeCostCenterRepository _employeeCostCenterRepository;
        private readonly IAddressRepository _addressRepository;
        private readonly IEmployeeCourseRepository _courseEmployeeRepository;
        private readonly IEmployeeCompanyRepository _employeeCompanyRepository;
        private readonly IEmployeeRoleRepository _employeeRoleRepository;
        private readonly IEmployeeAttachmentRepository _employeeAttachmentRepository;
        private readonly IEmployeeContractRepository _employeeContractRepository;

        public EmployeeService(IEmployeeRepository employeeRepository,
                            IRelationshipRepository relationshipRepository,
                            IBankAccountRepository bankAccountRepository,
                            IContactRepository contactRepository,
                            ISalaryHistoryRepository salaryHistoryRepository,
                            IEmployeeBenefitRepository employeeBenefitRepository,
                            IEmployeeVariablesRepository employeeVariablesRepository,
                            IEmployeeVacationRepository employeeVacationRepository,
                            IVehicleRepository vehicleRepository,
                            IEmployeeCostCenterRepository employeeCostCenterRepository,
                            IAddressRepository addressRepository,
                            IEmployeeCourseRepository courseEmployeeRepository,
                            IEmployeeCompanyRepository employeeCompanyRepository,
                            IEmployeeRoleRepository employeeRoleRepository,
                            IEmployeeAttachmentRepository employeeAttachmentRepository,
                            IEmployeeContractRepository employeeContractRepository)
        {
            this._employeeRepository = employeeRepository;
            this._relationshipRepository = relationshipRepository;
            this._bankAccountRepository = bankAccountRepository;
            this._contactRepository = contactRepository;
            this._salaryHistoryRepository = salaryHistoryRepository;
            this._employeeBenefitRepository = employeeBenefitRepository;
            this._employeeVariablesRepository = employeeVariablesRepository;
            this._employeeVacationRepository = employeeVacationRepository;
            this._vehicleRepository = vehicleRepository;
            this._employeeCostCenterRepository = employeeCostCenterRepository;
            this._addressRepository = addressRepository;
            this._courseEmployeeRepository = courseEmployeeRepository;
            this._employeeCompanyRepository = employeeCompanyRepository;
            this._employeeRoleRepository = employeeRoleRepository;
            this._employeeAttachmentRepository = employeeAttachmentRepository;
            this._employeeContractRepository = employeeContractRepository;
        }

        public Employee Create(Employee data)
        {
            if (string.IsNullOrEmpty(data.Picture))
            {
                data.Picture = _employeeRepository.SelectBy(employee => employee.Id == data.Id,
                                    employee => employee.Picture).SingleOrDefault();
            }

            return _employeeRepository.InsertOrUpdate(data, data.Id);
        }

        public int CalculateByPercentage(Employee employee, int employeeId = 0)
        {
            var entity = _employeeRepository.Find(employee != null ? employee.Id : employeeId);
            entity.Percentage = FilledEmployeePercentage(employee ?? entity).Where(c => c.Quantity > 0).Sum(c => c.Weight);
            this.Create(entity);
            return entity.Id;
        }


        public int CalculateAbaGeralPercentage(Employee entity)
        {
            int filled = 0;
            var properties = GetProperties(entity);

            foreach (var p in properties)
            {
                var value = p.GetValue(entity, null);

                if (value != null)
                {
                    filled = filled + 1;
                }
            }

            if (filled >= 40)
            {
                filled = 40;
            }

            if (entity.Nationality == null && filled >= 37)
            {
                filled = 40;
            }

            return 40 - filled;
        }

        private static PropertyInfo[] GetProperties(object obj)
        {
            return obj.GetType().GetProperties();
        }

        private List<EmployeePercentage> FilledEmployeePercentage(Employee employee)
        {
            var employeeAdrressPercentage = _addressRepository.GetByEmployee(employee.Id) != null ? 1 : 0;
            var employeeContactPercentage = _contactRepository.GetByEmployee(employee.Id) != null ? 1 : 0;
            var employeeVehiclePercentage = _vehicleRepository.GetByEmployee(employee.Id) != null ? 1 : 0;
            var employeeBankAccountPercentage = _bankAccountRepository.GetByEmployee(employee.Id) != null ? 1 : 0;
            var employeePeopleRelationshioPercentage = _relationshipRepository.GetByEmployee(employee.Id) != null ? 1 : 0;
            var employeeCoursePercentage = _courseEmployeeRepository.GetByEmployee(employee.Id) != null ? 1 : 0;
            var employeeCompanyPercentage = _employeeCompanyRepository.GetByEmployee(employee.Id) != null ? 1 : 0;
            var employeeCostCenterPercentage = _employeeCostCenterRepository.GetByEmployee(employee.Id) != null ? 1 : 0;
            var employeeRolePencentage = _employeeRoleRepository.GetByEmployee(employee.Id) != null ? 1 : 0;
            var employeeSalaryHistoryPercentage = _salaryHistoryRepository.GetByEmployee(employee.Id) != null ? 1 : 0;
            var employeeBenefitPercentage = _employeeBenefitRepository.GetByEmployee(employee.Id) != null ? 1 : 0;
            var employeeVariablesPercentage = _employeeVariablesRepository.GetByEmployee(employee.Id) != null ? 1 : 0;
            var employeeVacationPercentage = _employeeVacationRepository.GetByEmployee(employee.Id) != null ? 1 : 0;
            var employeeAttachmentPencentage = _employeeAttachmentRepository.GetByEmployee(employee.Id) != null ? 1 : 0;
            var employeeContractPencentage = _employeeContractRepository.GetByEmployee(employee.Id) != null ? 1 : 0;


            var percent = new List<EmployeePercentage>
            {
                  new EmployeePercentage
                 {
                      Aba = AbaRepresentation.ABA_GERAL,
                      Quantity = 1,
                      Weight = 34 - CalculateAbaGeralPercentage(employee)
                  },
                 new EmployeePercentage
                 {
                      Aba = AbaRepresentation.ABA_ENDERECO,
                      Quantity = employeeAdrressPercentage,
                      Weight = 5
                 },
                 new EmployeePercentage
                 {
                      Aba = AbaRepresentation.ABA_CONTATOS,
                      Quantity = employeeContactPercentage,
                      Weight = 5
                 },
                 new EmployeePercentage
                 {
                      Aba = AbaRepresentation.ABA_VEICULO,
                      Quantity = employeeVehiclePercentage,
                      Weight = 5
                 },
                 new EmployeePercentage
                 {
                      Aba = AbaRepresentation.ABA_CONTA_BANCARIA,
                      Quantity = employeeBankAccountPercentage,
                      Weight = 5
                 },
                 new EmployeePercentage
                 {
                      Aba = AbaRepresentation.ABA_PESSOA_RELACIONADA,
                      Quantity = employeePeopleRelationshioPercentage,
                      Weight = 5
                 },
                 new EmployeePercentage
                 {
                      Aba = AbaRepresentation.ABA_CURSO,
                      Quantity = employeeCoursePercentage,
                      Weight = 5
                 },
                 new EmployeePercentage
                 {
                      Aba = AbaRepresentation.ABA_HISTORICO,
                      Quantity = employeeCompanyPercentage,
                      Weight = 2
                 },
                 new EmployeePercentage
                 {
                      Aba = AbaRepresentation.ABA_HISTORICO,
                      Quantity = employeeCostCenterPercentage,
                      Weight = 2
                 },
                 new EmployeePercentage
                 {
                      Aba = AbaRepresentation.ABA_HISTORICO,
                      Quantity = employeeRolePencentage,
                      Weight = 2
                 },
                 new EmployeePercentage
                 {
                      Aba = AbaRepresentation.ABA_LINHA_SALARIO,
                      Quantity = employeeSalaryHistoryPercentage,
                      Weight = 5
                 },
                 new EmployeePercentage
                 {
                      Aba = AbaRepresentation.ABA_BENEFICIO,
                      Quantity = employeeBenefitPercentage,
                      Weight = 5
                 },
                 new EmployeePercentage
                 {
                      Aba =  AbaRepresentation.ABA_VARIAVEIS,
                      Quantity = employeeVariablesPercentage,
                      Weight = 5
                 },
                 new EmployeePercentage
                 {
                      Aba = AbaRepresentation.ABA_FERIAS,
                      Quantity = employeeVacationPercentage,
                      Weight = 5
                 },
                 new EmployeePercentage
                 {
                      Aba = AbaRepresentation.ABA_ANEXOS,
                      Quantity = employeeAttachmentPencentage,
                      Weight = 5
                 },
                 new EmployeePercentage
                 {
                      Aba =  AbaRepresentation.ABA_CONTATOS,
                      Quantity = employeeContractPencentage,
                      Weight = 5
                 }
            };

            return percent;
        }
    }
}
