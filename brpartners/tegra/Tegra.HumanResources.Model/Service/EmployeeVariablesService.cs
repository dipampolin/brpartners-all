﻿using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Repository;

namespace Tegra.HumanResources.Domain.Service
{
    public class EmployeeVariablesService : IEmployeeVariablesService
    {
        private IEmployeeVariablesRepository _repository;

        public EmployeeVariablesService(IEmployeeVariablesRepository repository)
        {
            this._repository = repository;
        }

        public EmployeeVariables Create(EmployeeVariables data)
        {

            if (data.Id == 0)
            {
                return this._repository.Add(data);
            }

            var entity = _repository.Find(data.Id);
            entity.Date = data.Date;
            entity.EmployeeId = data.EmployeeId;
            entity.Description = data.Description;
            entity.EmployeeId = data.EmployeeId;
            entity.Id = data.Id;
            entity.EmployeeVariablesTypeId = data.EmployeeVariablesTypeId;
            entity.Value = data.Value;

            return _repository.Update(entity);
        }

        public bool Remove(int id, bool logic = true)
        {
            return _repository.Remove(id, logic);
        }
    }
}
