﻿using Excel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Repository;
using Tegra.HumanResources.Domain.Representations;

namespace Tegra.HumanResources.Domain.Service
{
    public class ExcelService : IExcelService
    {
        private readonly ICompanyRepository _companyRepository;
        private readonly IEmployeeRepository _employeeRepository;
        private readonly ICostCenterRepository _centerCostRepository;

        public ExcelService(
            ICompanyRepository companyRepository,
            IEmployeeRepository employeeRepository,
            ICostCenterRepository centerCostRepository
        )
        {
            this._companyRepository = companyRepository;
            this._employeeRepository = employeeRepository;
            this._centerCostRepository = centerCostRepository;
        }

        public PayslipByException ProcessExcel(IExcelDataReader excelReader)
        {
            var payslips = new PayslipByException();
            if (excelReader != null && excelReader.IsValid)
            {
                if (!IsValidDafaultExcel(excelReader))
                {
                    var exception = new Exception("Arquivo inválido. Reveja o layout do arquivo.");

                    if (isNotContains(payslips.Exceptions, exception.Message))
                    {
                        payslips.Exceptions.Add(exception);
                    }
                }

                Payslip payslip = null;
                var currentEmployee = "";
                while (excelReader.Read())
                {
                    var companyCode = excelReader.GetString(ExcelByItem.COD_EMPRESA);
                    var company = _companyRepository.SelectBy(x => x.Code == companyCode).SingleOrDefault();

                    if (company == null)
                    {
                        var companyException = new Exception("Nenhuma Empresa com código " + companyCode + " foi encontrada");

                        if (isNotContains(payslips.Exceptions, companyException.Message))
                        {
                            payslips.Exceptions.Add(companyException);
                        }
                    }

                    var costCenterCode = excelReader.GetString(ExcelByItem.CENTRO_CUSTO);

                    CostCenter costCenter = null;

                    if (costCenterCode != null)
                    {
                        costCenter = _centerCostRepository.SelectBy(c => c.Code == costCenterCode).SingleOrDefault();
                    }

                    if (costCenter == null)
                    {
                        var costCenterException = new Exception("Nenhum Centro de Custo com código " + costCenterCode + " foi encontrado");

                        if (isNotContains(payslips.Exceptions, costCenterException.Message))
                        {
                            payslips.Exceptions.Add(costCenterException);
                        }
                    }

                    var employeeCode = excelReader.GetString(ExcelByItem.REGISTRO);

                    if (currentEmployee != employeeCode)
                    {
                        currentEmployee = employeeCode;

                        var employee = _employeeRepository.SelectBy(x => x.Initials == employeeCode, c => new { Id = c.Id }).FirstOrDefault();

                        if (employee == null)
                        {
                            var employeeeException = new Exception("Nenhum Funcionário com a sigla " + employeeCode + " foi encontrado");

                            if (isNotContains(payslips.Exceptions, employeeeException.Message))
                            {
                                payslips.Exceptions.Add(employeeeException);
                            }
                        }
                        else
                        {
                            if (company != null && costCenter != null)
                            {
                                var year = excelReader.GetInt32(ExcelByItem.ANO);
                                var month = excelReader.GetInt32(ExcelByItem.MES);

                                var date = new DateTime(year, month, 1);
                                payslip = ExcelByItem.CreatePayslip(excelReader, employee.Id, costCenter, company, date);
                                payslips.Payslips.Add(payslip);
                            }
                          
                        }
                    }

                    if (payslip != null)
                    {
                        PayslipItem item = ExcelByItem.CreatePayslipItem(excelReader, payslip);
                        payslip.Items.Add(item);
                    }
                }
                return payslips;
            }
            var fileException = new Exception("Arquivo inválido");

            if (isNotContains(payslips.Exceptions, fileException.Message))
            {
                payslips.Exceptions.Add(fileException);
            }

            return payslips;
        }

        private static bool isNotContains(List<Exception> Exceptions, string error)
        {
            return !Exceptions.Any(c => c.Message.Contains(error));
        }

        private bool IsValidDafaultExcel(IExcelDataReader excelReader)
        {
            excelReader.Read();
            int cont = 0;
            bool result = false;
            foreach (var item in GetListHeader())
            {
                result = item.Equals(excelReader.GetValue(cont));
                if (!result)
                    return result;
                cont++;
            }
            return result;
        }

        private IList GetListHeader()
        {
            IList excelList = new List<string>();
            excelList.Add(ExcelByItem.HEADER_ROW_COD_EMP);
            excelList.Add(ExcelByItem.HEADER_ROW_NOME_EMPRESA);
            excelList.Add(ExcelByItem.HEADER_ROW_REGISTRO);
            excelList.Add(ExcelByItem.HEADER_ROW_ANO);
            excelList.Add(ExcelByItem.HEADER_ROW_MES);
            excelList.Add(ExcelByItem.HEADER_ROW_RUBRICA);
            excelList.Add(ExcelByItem.HEADER_ROW_DESCR_RUBRICA);
            excelList.Add(ExcelByItem.HEADER_ROW_TIPO_RUB);
            excelList.Add(ExcelByItem.HEADER_ROW_REFERENC);
            excelList.Add(ExcelByItem.HEADER_ROW_VALOR);
            excelList.Add(ExcelByItem.HEADER_ROW_CCUSTO);
            excelList.Add(ExcelByItem.HEADER_ROW_DESCR_CCUSTO);
            excelList.Add(ExcelByItem.HEADER_ROW_CARGO);
            excelList.Add(ExcelByItem.HEADER_ROW_DESCR_CARGO);
            return excelList;
        }
    }
}
