﻿using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Domain.Service
{
    public interface ICostCenterService
    {
        CostCenter Save(CostCenter costCenter);

        void Remove(int id);
    }
}
