﻿using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Domain.Repository
{
    public class EmployeeContractService : IEmployeeContractService
    {
        private readonly IEmployeeContractRepository _repository;

        public EmployeeContractService(IEmployeeContractRepository repository)
        {
            this._repository = repository;
        }

        public EmployeeContract Create(EmployeeContract data)
        {
            if (data.Id == 0)
            {
                return this._repository.Add(data);
            }

            var entity = _repository.Find(data.Id);
            entity.Description = data.Description;
            entity.EmployeeId = data.EmployeeId;
            entity.StartDate = data.StartDate;
            entity.EndDate = data.EndDate;
            entity.Approved = data.Approved;
            entity.Id = data.Id;

            return _repository.Update(entity);
        }

        public bool Remove(int id, bool logic = true)
        {
            return _repository.Remove(id, logic);
        }
    }
}
