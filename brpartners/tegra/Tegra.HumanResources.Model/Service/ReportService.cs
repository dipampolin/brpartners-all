﻿using CodeEngine.Framework.QueryBuilder;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using Tegra.HumanResources.Domain.Attributes;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Join;
using Tegra.HumanResources.Domain.Repository;
using Tegra.HumanResources.Domain.Representations;

namespace Tegra.HumanResources.Domain.Service
{
    public class ReportService : IReportService
    {
        private readonly IReportRepository _repository;
        private readonly IReportItemRepository _reportItemRepository;
        private readonly IReportFilterRepository _reportFilterRepository;

        public ReportService(IReportRepository repository, IReportItemRepository reportItemRepository, IReportFilterRepository reportFilterRepository)
        {
            this._repository = repository;
            this._reportItemRepository = reportItemRepository;
            this._reportFilterRepository = reportFilterRepository;
        }

        private string GenerateFields(Report report)
        {
            string query = "";
            foreach (var item in report.Items.OrderByDescending(c => c.Table.Equals("Employee")))
            {
                query += string.Format(" {0}.{1} as '{2}'{3}", item.Table, item.Field, item.FieldAlias, ",");
            }

            return query;
        }

        public string GenerateQuery(Report report)
        {
            SelectQueryBuilder query = new SelectQueryBuilder();
            var selectedTable = report.Items.Select(c => c.Table)
                                    .OrderByDescending(c => c.Equals("Employee"))
                                        .FirstOrDefault();

            query.SelectFromTable(string.Format("rh.{0}", selectedTable));
            query.SelectColumns(GenerateFields(report).Split(','));

            ImplementEmployeeJoin.GetEmployeeJoin(report.Items.Select(c => c.Table).Distinct(), query);

            var queryString = query.BuildQuery();
            queryString = queryString.Replace("EmployeeSuperior ON", "rh.Employee as EmployeeSuperior ON");
            return queryString;
        }

        public List<ReportByAlias> GetReportAlias(string nameSpace)
        {
            var typelist =
              GetTypesInNamespace(Assembly.GetExecutingAssembly(), nameSpace)
            .Where(c => ((DescriptionAttribute[])c.GetCustomAttributes(typeof(DescriptionAttribute), true)).Where(g => g.Description != null) != null).ToList();

            var reportList = new List<ReportByAlias>();
            typelist.ForEach(c =>
            {
                ReportByAlias report = null;
                var pro = c.GetCustomAttributes(typeof(DescriptionAttribute), true).FirstOrDefault();

                if (pro != null && !string.IsNullOrEmpty(((DescriptionAttribute)pro).Description))
                {
                    var proper = c.GetProperties().Where(f => ((DescriptionAttribute)f.GetCustomAttributes(typeof(DescriptionAttribute), true).FirstOrDefault()) != null);
                    proper.ToList().ForEach(h =>
                    {
                        report = new ReportByAlias();
                        report.Table = c.Name;
                        report.Field = h.Name;
                        report.Type = GetType(h);
                        report.TableAlias = ((DescriptionAttribute)pro).Description;
                        var y = h.GetCustomAttributes(typeof(DescriptionAttribute), true).FirstOrDefault();
                        report.FieldAlias = report.TableAlias + " - " + ((DescriptionAttribute)y).Description.ToString();
                        reportList.Add(report);
                    });
                }
            });

            return reportList;
        }

        public List<ReportByAlias> GetReportFilter(List<string> names, int id, string nameSpace)
        {
            var typelist =
            GetTypesInNamespace(Assembly.GetExecutingAssembly(), nameSpace)
          .Where(c => ((DescriptionAttribute[])c.GetCustomAttributes(typeof(DescriptionAttribute), true)).Where(g => g.Description != null) != null).ToList();

            var filterList = typelist.Where(c => names.Contains(c.Name)).ToList();

            var reportList = new List<ReportByAlias>();

            filterList.ForEach(c =>
            {
                ReportByAlias report = null;
                var pro = c.GetCustomAttributes(typeof(DescriptionAttribute), true).FirstOrDefault();

                if (pro != null && !string.IsNullOrEmpty(((DescriptionAttribute)pro).Description))
                {
                    var proper = c.GetProperties().Where(f => ((FilterableAttribute)f.GetCustomAttributes(typeof(FilterableAttribute), true).FirstOrDefault()) != null);
                    proper.ToList().ForEach(h =>
                    {
                        report = new ReportByAlias();
                        report.Table = c.Name;
                        report.Field = h.Name;
                        report.Type = GetType(h);
                        report.Value = GetValueField(c, h, pro, id);
                        report.TableAlias = ((DescriptionAttribute)pro).Description;
                        var y = h.GetCustomAttributes(typeof(DescriptionAttribute), true).FirstOrDefault();
                        report.FieldAlias = report.TableAlias + " - " + ((DescriptionAttribute)y).Description.ToString();
                        reportList.Add(report);
                    });
                }
            });

            return reportList;
        }

        private string GetValueField(Type c, PropertyInfo h, object pro, int id)
        {
            var report = _repository.Query(f => f.Id > 0).SelectMany(s => s.Filters).FirstOrDefault(fl => fl.Table == c.Name && fl.ReportId == id && fl.TableAlias == ((DescriptionAttribute)pro).Description && fl.Field == h.Name);
            if (report == null)
                return null;
            return report.Value;
        }

        private static string GetType(PropertyInfo p)
        {
            if (p.PropertyType.IsGenericType && p.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                return (p.PropertyType.GetGenericArguments()[0]).Name;
            }
            return p.PropertyType.Name;
        }

        public bool ExecuteQuery(string query)
        {
            SqlDataReader reader;
            using (SqlConnection connection = GetCurrentConnection())
            {
                var cmd = connection.CreateCommand();
                cmd.CommandText = query;
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 1800;

                connection.Open();

                try
                {
                    reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        for (int cont = 0; cont < reader.FieldCount; cont++)
                        {
                            var comlumn = reader.GetName(cont);
                        }
                    }
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return reader != null;
        }

        private static SqlConnection GetCurrentConnection()
        {
            return new SqlConnection(ConfigurationManager.ConnectionStrings["RH"].ConnectionString);
        }

        public DataTable GenerateReport(Report report)
        {
            using (SqlConnection connection = GetCurrentConnection())
            {
                var cmd = connection.CreateCommand();
                cmd.CommandText = report.Query;
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 1800;

                using (SqlDataAdapter adaptador = new SqlDataAdapter(cmd))
                {
                    using (DataTable tabela = new DataTable())
                    {
                        adaptador.Fill(tabela);
                        return tabela;
                    }
                }
            }
        }

        private static Type[] GetTypesInNamespace(Assembly assembly, string nameSpace)
        {
            return assembly.GetTypes().Where(t => string.Equals(t.Namespace, nameSpace, StringComparison.Ordinal)).ToArray();
        }

        public Report Save(Report data)
        {
            if (data.Id == 0)
            {
                return this._repository.Add(data);
            }

            var entity = _repository.Query(c => c.Id == data.Id).Include(c => c.Items).FirstOrDefault();

            _reportItemRepository.RemoveAll(entity.Items.ToList());

            entity.Query = data.Query;
            entity.Name = data.Name;
            entity.Id = data.Id;
            entity.Active = data.Active;
            entity.Description = data.Description;
            entity.Items = data.Items;
            return _repository.Update(entity);
        }

        public Report UpdateQuery(Report data, List<ReportFilter> filters)
        {
            if (!this.ExecuteQuery(data.Query))
                return null;

            if (data.Id == 0)
                return this._repository.Add(data);

            var entity = _repository.Query(c => c.Id == data.Id).Include(c => c.Filters).FirstOrDefault();

            _reportFilterRepository.RemoveAll(entity.Filters.ToList());

            entity.Query = data.Query;
            entity.Filters = filters;
            return _repository.Update(entity);
        }

        public bool Remove(int reportId)
        {
            return _repository.Remove(reportId, false);
        }

        public Report CreateFilters(List<ReportFilter> reportItem, int reportId)
        {
            var report = _repository.Find(reportId);

            var query = report.Query;

            if (query.ToLower().Contains("where"))
            {
                query = query.Remove(query.LastIndexOf("where", StringComparison.CurrentCultureIgnoreCase));
            }
            string filters = "";

            reportItem.Where(c => !string.IsNullOrEmpty(c.Value)).ToList().ForEach(c =>
                    {
                        if (filters.Length == 0)
                            filters += "WHERE ";
                        if (c.Type.Equals("String"))
                        {
                            filters += string.Format("{0}.{1}='{2}' AND ", c.Table, c.Field, c.Value);
                        }
                        if (c.Type.Equals("DateTime"))
                        {
                            filters += string.Format("{0}.{1} BETWEEN {2} AND ", c.Table, c.Field, DateRole.GetDateRoles(c.Value).Command);
                        }
                        else
                            filters += string.Format("{0}.{1}={2} AND ", c.Table, c.Field, c.Value);
                    });

            if (filters.ToLower().LastIndexOf("and ") > 0)
            {
                report.Query = string.Format("{0} {1}", query, filters.Remove(filters.LastIndexOf("and", StringComparison.CurrentCultureIgnoreCase)));
                return report;
            }

            report.Query = string.Format("{0} {1}", query, filters.Length > 0 ? filters.Remove(filters.LastIndexOf("and", StringComparison.CurrentCultureIgnoreCase)) : "");
            return report;
        }
    }
}
