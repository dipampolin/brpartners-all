﻿using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Services
{
    public interface IVehicleService
    {
        EmployeeVehicle Save(EmployeeVehicle entity);

        bool Remove(int id, bool logic);
    }
}