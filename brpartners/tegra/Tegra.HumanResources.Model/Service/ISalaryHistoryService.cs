﻿using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Domain.Service
{
    public interface ISalaryHistoryService
    {
        EmployeeSalary Save(EmployeeSalary entity);

        bool Remove(int id, bool logic);
    }
}
