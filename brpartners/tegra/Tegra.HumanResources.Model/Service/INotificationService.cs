﻿using System.Collections.Generic;
using System.Data;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Representations;
using Tegra.HumanResources.Domain.Util;

namespace Tegra.HumanResources.Domain.Service
{
    public interface INotificationService
    {
        void Send(SendEmail message);

        bool ExecuteQuery(string query);

        DataTable GenerateNotification(string query);

        string GenerateQuery(Notification notification);

        Notification Save(Notification data);

        bool Remove(Notification entity);

        List<NotificationRepresentation> GetNotificationFilter(List<string> list, int id, string nameSpace = "Tegra.HumanResources.Domain.Entities");

        void SendScheduledNotifications(ITemplateGenerator generator);

        Notification CreateFilters(List<NotificationFilter> notificationItem, int reportId);

        Notification UpdateQuery(Notification notification, List<NotificationFilter> notificationQuery);

        List<NotificationRole> GetNotificationRoles();

        string GetRoleDescritionByEnumValue(string value);
    }
}
