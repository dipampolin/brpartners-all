﻿using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Services
{
    public interface IAddressService
    {
        EmployeeAddress Save(EmployeeAddress entity);

        bool Remove(int id, bool logic);
    }
}