﻿using CodeEngine.Framework.QueryBuilder;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using Tegra.HumanResources.Domain.Attributes;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Join;
using Tegra.HumanResources.Domain.Repository;
using Tegra.HumanResources.Domain.Representations;
using Tegra.HumanResources.Domain.Util;

namespace Tegra.HumanResources.Domain.Service
{
    public class NotificationService : INotificationService
    {
        private readonly SmtpClient _client = new SmtpClient();

        private readonly INotificationRepository _repository;

        private readonly INotificationFilterRepository _notificationtFilterRepository;

        private readonly INotificationItemRepository _notificationItemRepository;

        private readonly INotificationDestinationRepository _notificationDestinationRepository;

        public NotificationService(INotificationRepository repository, INotificationFilterRepository notificationtFilterRepository, INotificationItemRepository notificationItemRepository, INotificationDestinationRepository notificationDestinationRepository)
        {
            this._repository = repository;
            this._notificationtFilterRepository = notificationtFilterRepository;
            this._notificationItemRepository = notificationItemRepository;
            this._notificationDestinationRepository = notificationDestinationRepository;
        }

        public void Send(SendEmail message)
        {
            try
            {
                var smtpMessage = new MailMessage()
                {
                    IsBodyHtml = true,
                    Body = message.Body,
                    Subject = message.Subject,
                    Priority = MailPriority.High,
                    BodyEncoding = Encoding.GetEncoding("iso-8859-1")
                };

                if (!string.IsNullOrEmpty(message.From))
                    smtpMessage.From = new MailAddress(message.From);

                smtpMessage.IsBodyHtml = true;
                smtpMessage.To.Add(message.To);
                _client.EnableSsl = true;
                _client.Send(smtpMessage);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool ExecuteQuery(string query)
        {
            SqlDataReader reader;
            using (SqlConnection connection = GetCurrentConnection())
            {
                var cmd = connection.CreateCommand();
                cmd.CommandText = query;
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 1800;

                connection.Open();

                try
                {
                    reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        for (int cont = 0; cont < reader.FieldCount; cont++)
                        {
                            var comlumn = reader.GetName(cont);
                        }
                    }
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return reader != null;
        }

        private static SqlConnection GetCurrentConnection()
        {
            return new SqlConnection(ConfigurationManager.ConnectionStrings["RH"].ConnectionString);
        }

        public DataTable GenerateNotification(string query)
        {
            using (SqlConnection connection = GetCurrentConnection())
            {
                var cmd = connection.CreateCommand();
                cmd.CommandText = query;
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 1800;

                using (SqlDataAdapter adaptador = new SqlDataAdapter(cmd))
                {
                    using (DataTable tabela = new DataTable())
                    {
                        adaptador.Fill(tabela);
                        return tabela;
                    }
                }
            }
        }

        private string GenerateFields(Notification notification)
        {
            string query = "";
            foreach (var item in notification.Items.OrderByDescending(c => c.Table.Equals("Employee")))
            {
                query += string.Format(" {0}.{1} as '{2}'{3}", item.Table, item.Field, item.FieldAlias, ",");
            }

            return query;
        }

        public string GenerateQuery(Notification notification)
        {
            SelectQueryBuilder query = new SelectQueryBuilder();
            var selectedTable = notification.Items.Select(c => c.Table)
                                    .OrderByDescending(c => c.Equals("Employee"))
                                        .FirstOrDefault();

            query.SelectFromTable(string.Format("rh.{0}", selectedTable));
            query.SelectColumns(GenerateFields(notification).Split(','));


            ImplementEmployeeJoin.GetEmployeeJoin(notification.Items.Select(c => c.Table).Distinct(), query);

            var queryString = query.BuildQuery();

            if (!this.ExecuteQuery(queryString))
            {
                var erro = notification.Items.Select(c => c.TableAlias).Distinct().Aggregate((before, last) => String.Format("{0} e {1}", before, last));

                throw new Exception("Não foi possível relacioanar as tabelas: " + erro);
            }

            queryString = queryString.Replace("EmployeeSuperior ON", "rh.Employee as EmployeeSuperior ON");
            return queryString;
        }

        public Notification Save(Notification data)
        {
            if (data.Id == 0)
            {
                return this._repository.Add(data);
            }

            var entity = _repository.Query(c => c.Id == data.Id).Include(c => c.Items).Include(c => c.Destinations).FirstOrDefault();

            data.Destinations.Where(d => d.Id > 0).ToDictionary(d => d.Id, d => d).ToList().ForEach(d =>
               d.Value.LastSent = entity.Destinations.FirstOrDefault(dest => dest.Id == d.Key).LastSent
            );
            _notificationItemRepository.RemoveAll(entity.Items.ToList());
            _notificationDestinationRepository.RemoveAll(entity.Destinations.ToList());

            entity.Active = data.Active;
            entity.Frequency = data.Frequency;
            entity.Id = data.Id;
            entity.Items = data.Items;
            entity.Name = data.Name;
            entity.Query = data.Query;
            entity.Destinations = data.Destinations;
            return _repository.Update(entity);
        }

        public bool Remove(Notification entity)
        {
            return _repository.Remove(entity.Id, false);
        }

        public void SendScheduledNotifications(ITemplateGenerator generator)
        {
            var notifications = _repository.Query(n => n.Active).Include(c => c.Destinations).ToList();

            notifications.ForEach(n =>
            {
                List<SendEmail> email = new List<SendEmail>();
                var model = GenerateNotification(n.Query);

                System.Diagnostics.Debug.WriteLine(model);
                n.Destinations.ToList().ForEach(c =>
                {
                    email.Add(new SendEmail
                    {
                        To = c.Email,
                        Body = generator.Generate(model),
                        From = "site.qx3@qx3.com.br",
                        Subject = n.Name
                    });
                });

                email.ForEach(c =>
                {
                    System.Diagnostics.Debug.WriteLine(c.Body);
                    Send(c);
                });
            });
        }

        private static Type[] GetTypesInNamespace(Assembly assembly, string nameSpace)
        {
            return assembly.GetTypes().Where(t => string.Equals(t.Namespace, nameSpace, StringComparison.Ordinal)).ToArray();
        }

        public List<NotificationRepresentation> GetNotificationFilter(List<string> list, int id, string nameSpace)
        {
            var typelist =
           GetTypesInNamespace(Assembly.GetExecutingAssembly(), nameSpace)
         .Where(c => ((DescriptionAttribute[])c.GetCustomAttributes(typeof(DescriptionAttribute), true)).Where(g => g.Description != null) != null).ToList();

            var filterList = typelist.Where(c => list.Contains(c.Name)).ToList();

            var reportList = new List<NotificationRepresentation>();

            filterList.ForEach(c =>
            {
                NotificationRepresentation notification = null;
                var pro = c.GetCustomAttributes(typeof(DescriptionAttribute), true).FirstOrDefault();

                if (pro != null && !string.IsNullOrEmpty(((DescriptionAttribute)pro).Description))
                {
                    var proper = c.GetProperties().Where(f => ((FilterableAttribute)f.GetCustomAttributes(typeof(FilterableAttribute), true).FirstOrDefault()) != null);
                    proper.ToList().ForEach(h =>
                    {
                        notification = new NotificationRepresentation();
                        notification.Table = c.Name;
                        notification.Field = h.Name;
                        notification.Type = GetType(h);
                        notification.Value = GetValueField(c, h, pro, id);
                        notification.TableAlias = ((DescriptionAttribute)pro).Description;
                        var y = h.GetCustomAttributes(typeof(DescriptionAttribute), true).FirstOrDefault();
                        notification.FieldAlias = notification.TableAlias + " - " + ((DescriptionAttribute)y).Description.ToString();
                        reportList.Add(notification);
                    });
                }
            });

            return reportList;
        }

        private string GetValueField(Type c, PropertyInfo h, object pro, int id)
        {
            var notification = _repository.Query(f => f.Id > 0).SelectMany(s => s.Filters).FirstOrDefault(fl => fl.Table == c.Name && fl.NotificationId == id && fl.TableAlias == ((DescriptionAttribute)pro).Description && fl.Field == h.Name);
            if (notification == null)
                return null;
            return notification.Value;
        }

        private static string GetType(PropertyInfo p)
        {
            if (p.PropertyType.IsGenericType && p.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                return (p.PropertyType.GetGenericArguments()[0]).Name;
            }
            return p.PropertyType.Name;
        }

        public Notification CreateFilters(List<NotificationFilter> reportItem, int reportId)
        {
            var notification = _repository.Find(reportId);

            var query = notification.Query;

            if (query.ToLower().Contains("where"))
            {
                query = query.Remove(query.LastIndexOf("where", StringComparison.CurrentCultureIgnoreCase));
            }
            string filters = "";

            reportItem.Where(c => !string.IsNullOrEmpty(c.Value)).ToList().ForEach(c =>
            {
                if (filters.Length == 0)
                    filters += "WHERE ";
                if (c.Type.Equals("String"))
                {
                    filters += string.Format("{0}.{1}='{2}' AND ", c.Table, c.Field, c.Value);
                }
                if (c.Type.Equals("DateTime"))
                {
                    filters += string.Format("{0}.{1} BETWEEN {2} AND ", c.Table, c.Field, DateRole.GetDateRoles(c.Value).Command);
                }
                else
                    filters += string.Format("{0}.{1}={2} AND ", c.Table, c.Field, c.Value);
            });

            if (filters.ToLower().LastIndexOf("and ") > 0)
            {
                notification.Query = string.Format("{0} {1}", query, filters.Remove(filters.LastIndexOf("and", StringComparison.CurrentCultureIgnoreCase)));
                return notification;
            }

            notification.Query = string.Format("{0} {1}", query, filters.Length > 0 ? filters.Remove(filters.LastIndexOf("and", StringComparison.CurrentCultureIgnoreCase)) : "");
            return notification;
        }

        public Notification UpdateQuery(Notification data, List<NotificationFilter> filters)
        {
            if (!this.ExecuteQuery(data.Query))
                return null;

            if (data.Id == 0)
                return this._repository.Add(data);

            var entity = _repository.Query(c => c.Id == data.Id).Include(c => c.Filters).FirstOrDefault();

            _notificationtFilterRepository.RemoveAll(entity.Filters.ToList());

            entity.Query = data.Query;
            entity.Filters = filters;
            return _repository.Update(entity);
        }

        public List<NotificationRole> GetNotificationRoles()
        {
            return NotificationRole.GetEnum();
        }

        public string GetRoleDescritionByEnumValue(string value)
        {
            return NotificationRole.GetEnumDescription(value);
        }
    }
}
