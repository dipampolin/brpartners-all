﻿using System.Collections.Generic;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Repository;

namespace Tegra.HumanResources.Domain.Service
{
    public class ReportItemService : IReportItemService
    {
        private readonly IReportItemRepository _repository;

        public ReportItemService(IReportItemRepository repository)
        {
            this._repository = repository;
        }

        public bool DeleteAll(List<ReportItem> items)
        {
            return _repository.RemoveAll(items);
        }
    }
}
