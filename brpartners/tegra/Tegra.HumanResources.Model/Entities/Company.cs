﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq.Expressions;
using Tegra.DDD;
using Tegra.HumanResources.Domain.Attributes;

namespace Tegra.HumanResources.Domain.Entities
{
    [Description("Empresa")]
    public class Company : Entity, IFiltrableEntity<Company>
    {

        public Expression<Func<Company, bool>> GetFilter(string name, string value)
        {

            var filters = new Dictionary<string, Expression<Func<Company, bool>>>()
            {
                { "Name", x => x.Name.Contains(value) },
                { "Cnpj", x => x.Cnpj.Contains(value) },
                { "Code", x => x.Code.Contains(value) }
            };

            return filters[name];
        }

        public Company() { }

        public Company(string code, string name, string cnpj = "")
        {
            this.Code = code;
            this.Name = name;
            this.Cnpj = cnpj;
        }

        public int Id { get; set; }

        [Description("Código")]
        [Filterable]
        public string Code { get; set; }

        [Description("Nome")]
        public string Name { get; set; }

        [Description("CNPJ")]
        [Format(Conversions.ConvertType.CNPJ)]
        public string Cnpj { get; set; }
    }
}
