﻿using System;
using Tegra.DDD;

namespace Tegra.HumanResources.Domain.Entities
{
    public class NotificationDestination : Entity
    {
        public int Id { get; set; }

        public string Email { get; set; }

        public DateTime? LastSent { get; set; }

        public int NotificationId { get; set; }
        public virtual Notification Notification { get; set; }
    }
}
