﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;
using Tegra.DDD;

namespace Tegra.HumanResources.Domain.Entities
{
    public class Report : Entity, IFiltrableEntity<Report>
    {
        public Expression<Func<Report, bool>> GetFilter(string name, string value)
        {

            var filters = new Dictionary<string, Expression<Func<Report, bool>>>()
            {
                { "Name", x => x.Name.Contains(value) }
            };

            return filters[name];
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        [MaxLength]
        public string Query { get; set; }

        public bool Active { get; set; }

        public virtual ICollection<ReportItem> Items { get; set; }

        public virtual ICollection<ReportFilter> Filters { get; set; }
    }
}
