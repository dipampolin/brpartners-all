﻿using System.ComponentModel;
using Tegra.DDD;

namespace Tegra.HumanResources.Domain.Entities
{
    [Description("Veículo")]
    public class EmployeeVehicle : Entity
    {
        public EmployeeVehicle() { }
        public EmployeeVehicle(int employeeId, string brand, string model, string color, string carplate, bool active = true)
        {
            this.EmployeeId = employeeId;
            this.Brand = brand;
            this.Model = model;
            this.Color = color;
            this.CarPlate = carplate;
            this.Active = active;
        }

        public int Id { get; set; }

        [Description("Marca")]
        public string Brand { get; set; }

        [Description("Modelo")]
        public string Model { get; set; }

        [Description("Cor")]
        public string Color { get; set; }

        [Description("Placa")]
        [Format(Conversions.ConvertType.CARRO_PLACA)]
        public string CarPlate { get; set; }

        public bool Active { get; set; }

        // FKs
        public int EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }

    }
}