﻿using Tegra.DDD;

namespace Tegra.HumanResources.Domain.Entities
{
    public class ReportItem : Entity
    {
        public int Id { get; set; }

        public string Field { get; set; }

        public string FieldAlias { get; set; }

        public string Table { get; set; }

        public string TableAlias { get; set; }

        public int ReportId { get; set; }

        public string Type { get; set; }

        public virtual Report Report { get; set; }
    }
}

