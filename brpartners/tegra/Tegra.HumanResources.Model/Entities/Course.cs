﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq.Expressions;
using Tegra.DDD;

namespace Tegra.HumanResources.Domain.Entities
{
    [Description("Funcionário Curso")]
    public class Course : Entity, IFiltrableEntity<Course>
    {

        public Expression<Func<Course, bool>> GetFilter(string name, string value)
        {

            bool boolValue = false;
            bool.TryParse(value, out boolValue);

            var filters = new Dictionary<string, Expression<Func<Course, bool>>>()
            {
                { "Name", x => x.Name.Contains(value) },
                { "Active", x => x.Active.Equals(boolValue) }
            };

            return filters[name];
        }


        public Course() { }

        public Course(string name, bool active = true)
        {

            this.Name = name;
            this.Active = active;
        }

        public int Id { get; set; }

        [Description("Nome")]
        public string Name { get; set; }

        public bool Active { get; set; }

        // Reverse FK
        public virtual List<CourseEmployee> CourseEmployees { get; set; }

    }
}
