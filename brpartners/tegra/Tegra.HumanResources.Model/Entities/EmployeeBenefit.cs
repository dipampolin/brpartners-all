﻿using System;
using System.ComponentModel;
using Tegra.DDD;

namespace Tegra.HumanResources.Domain.Entities
{
    [Description("Péríodo Benefício")]
    public class EmployeeBenefit : Entity
    {
        public int Id { get; set; }

        public int BenefitId { get; set; }
        public virtual Benefit Benefit { get; set; }

        public int EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }

        [Description("DataInÍcio")]
        [Format(Conversions.ConvertType.DATE)]
        public DateTime StartDate { get; set; }

        [Description("DataFinal")]
        [Format(Conversions.ConvertType.DATE)]
        public DateTime? EndDate { get; set; }

        [Description("Valor Customizado")]
        public decimal? CustomValue { get; set; }

        [Description("Número do Cartão")]
        public string CardNumber { get; set; }
    }
}
