﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;
using System.Web.Script.Serialization;
using Tegra.DDD;
using Tegra.HumanResources.Domain.Attributes;

namespace Tegra.HumanResources.Domain.Entities
{
    [Serializable]
    [Description("Funcionário")]
    public class Employee : Entity, IFiltrableEntity<Employee>
    {
        public Expression<Func<Employee, bool>> GetFilter(string name, string value)
        {
            bool boolValue = false;
            bool.TryParse(value, out boolValue);

            var filters = new Dictionary<string, Expression<Func<Employee, bool>>>()
            {
                { "Name", x => x.Name.Contains(value) },
                { "Initials", x => x.Initials.Contains(value) },
                { "Active", x => x.Active.Equals(boolValue) }
            };

            return filters[name];
        }

        public Employee()
        {
            Expression<Func<object, string>> www = x => ((DateTime)x).ToString("dd/MM/YYYY");
            this.Payslips = new List<Payslip>();
        }

        public Employee(string initials, string name)
        {
            this.Payslips = new List<Payslip>();
            this.Initials = initials;
            this.Name = name;
            EmployeeCostCenters = new Collection<EmployeeCostCenter>();
        }

        public int Id { get; set; }

        [Description("Nome")]
        public string Name { get; set; }

        [Description("Sigla")]
        public string Initials { get; set; }

        [Description("Matrícula")]
        public string Registration { get; set; }

        [Description("Crachá")]
        public string IdentificationNumber { get; set; }

        [Description("Sexo")]
        public string Gender { get; set; }

        [Description("Aniversário")]
        [Format(Conversions.ConvertType.DATE)]
        public DateTime Birthdate { get; set; }

        [Description("Estrangeiro")]
        [Format(Conversions.ConvertType.DEFAULT_BOOL)]
        public bool Foreigner { get; set; }

        [Description("Nacionalidade")]
        public string Nationality { get; set; }

        [Description("Nome da Mãe")]
        public string MotherName { get; set; }

        [Description("Data de nascimento da Mãe")]
        [Format(Conversions.ConvertType.DATE)]
        public DateTime? MotherBirthdate { get; set; }

        [Description("Nome da Pai")]
        public string FatherName { get; set; }

        [Description("Data de nascimento do Pai")]
        [Format(Conversions.ConvertType.DATE)]
        public DateTime? FatherBirthdate { get; set; }

        [Description("Telefone-Principal")]
        public string MainPhone { get; set; }

        [MaxLength]
        public string Picture { get; set; }

        [Description("CPF")]
        [Format(Conversions.ConvertType.CPF)]
        public string Cpf { get; set; }

        [Description("RG")]
        public string Rg { get; set; }

        [Description("RG-Expedidor")]
        public string RgDispatcher { get; set; }

        [Description("RG-Data Expedição")]
        [Format(Conversions.ConvertType.DATE)]
        public DateTime? RgDateExpedition { get; set; }

        [Description("RNE")]
        public string Rne { get; set; }

        [Description("RNE-Expedidor")]
        public string RneDispatcher { get; set; }

        [Description("RNE-Data Expedição")]
        [Format(Conversions.ConvertType.DATE)]
        public DateTime? RneDateExpedition { get; set; }

        [Description("RNE-Data Expiração")]
        [Format(Conversions.ConvertType.DATE)]
        [Filterable]
        public DateTime? RneDateExpiration { get; set; }

        [Description("Título-Eleitor-Número")]
        public string VoterNumberCard { get; set; }

        [Description("Título-Eleitor-Sessão")]
        public string VoterSession { get; set; }

        [Description("Título-Eleitor-Zona")]
        public string VoterZone { get; set; }

        [Description("Carteira de Trabalho-Número")]
        public string EmploymentBooklet { get; set; }

        [Description("Carteira de Trabalho-Serie")]
        public string EmploymentBookletSerie { get; set; }

        [Description("Carteira de Trabalho-UF")]
        public string EmploymentBookletUf { get; set; }

        [Description("PIS")]
        public string Pis { get; set; }

        [Description("Passaporte")]
        public string Passport { get; set; }

        [Description("Passaporte-Data Expedição")]
        [Format(Conversions.ConvertType.DATE)]
        [Filterable]
        public DateTime? PassportDateExpedition { get; set; }

        [Description("Passaporte-Data Expiração")]
        [Format(Conversions.ConvertType.DATE)]
        [Filterable]
        public DateTime? PassportDateExpiration { get; set; }

        [Description("Passaporte-Expedidor")]
        public string PassportDispatcher { get; set; }

        [Description("Passaporte-UF")]
        public string PassportUf { get; set; }

        [Description("Reservista")]
        public string Reservist { get; set; }

        [Description("Categoria de Reservista")]
        public string ReservistCategory { get; set; }

        [Description("Ativo")]
        [Filterable]
        [Format(Conversions.ConvertType.DEFAULT_BOOL)]
        public bool Active { get; set; }

        [Description("Data Admissão")]
        [Format(Conversions.ConvertType.OPTIONAL_DATE)]
        [Filterable]
        public DateTime? AdmissionDate { get; set; }

        [Description("Data Demissão")]
        [Format(Conversions.ConvertType.DATE)]
        [Filterable]
        public DateTime? ResignationDate { get; set; }

        public int Percentage { get; set; }

        [Description("CNH")]
        public string Cnh { get; set; }

        [Description("CNH-Categoria")]
        public string CnhCategory { get; set; }

        [Description("CNH-Data Expedição")]
        [Format(Conversions.ConvertType.DATE)]
        [Filterable]
        public DateTime? CnhDateExpedition { get; set; }

        [Description("CNH-Data Expiração")]
        [Format(Conversions.ConvertType.DATE)]
        [Filterable]
        public DateTime? CnhDateExpiration { get; set; }

        [Description("CNH-Data Expiração")]
        [Format(Conversions.ConvertType.DATE)]
        [Filterable]
        public DateTime? CnhDateFirst { get; set; }

        [Description("Naturalizado")]
        public bool? Naturalized { get; set; }

        [Description("País de Nascimento")]
        public string CountryOfBirth { get; set; }

        [Description("Cidade de Nascimento")]
        public string CityOfBirth { get; set; }


        // FKs/
        public int? CompanyId { get; set; }
        public virtual Company Company { get; set; }

        public int? CivilStatusId { get; set; }
        public virtual CivilStatus CivilStatus { get; set; }

        public int? CityId { get; set; }
        public virtual City City { get; set; }

        public int? EducationalStageId { get; set; }
        public virtual EducationalStage EducationalStage { get; set; }

        public int? CostCenterId { get; set; }
        public virtual CostCenter CostCenter { get; set; }

        [Description("Superior Imediato")]
        [Filterable]
        public int? SupervisorId { get; set; }
        public virtual Employee Supervisor { get; set; }

        public int? PassportCityId { get; set; }
        public virtual City PassportCity { get; set; }


        // Reverse FK
        public virtual ICollection<EmployeeCostCenter> EmployeeCostCenters { get; set; }
        public virtual ICollection<CourseEmployee> CourseEmployees { get; set; }
        public virtual ICollection<EmployeeRole> EmployeeRoles { get; set; }
        public virtual ICollection<EmployeeAddress> EmployeeAddress { get; set; }
        public virtual ICollection<Payslip> Payslips { get; set; }
        public virtual ICollection<EmployeeVehicle> Vehicles { get; set; }
        public virtual ICollection<EmployeeAttachment> EmployeeAttachments { get; set; }
        public virtual ICollection<EmployeeContract> EmployeeContracts { get; set; }
        public virtual ICollection<EmployeeCompany> EmployeeCompanies { get; set; }
    }
}
