﻿using System;
using System.ComponentModel;
using Tegra.DDD;

namespace Tegra.HumanResources.Domain.Entities
{
    [Description("Período Cargo")]
    public class EmployeeRole : Entity
    {
        public int Id { get; set; }

        [Description("Data Início")]
        [Format(Conversions.ConvertType.DATE)]
        public DateTime StartDate { get; set; }

        [Description("Data Final")]
        [Format(Conversions.ConvertType.DATE)]
        public DateTime? EndDate { get; set; }

        public int EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }

        public int RoleId { get; set; }
        public virtual Role Role { get; set; }

        public bool IsActive()
        {
            return StartDate <= DateTime.Now && (EndDate == null || EndDate >= DateTime.Now);
        }

    }
}
