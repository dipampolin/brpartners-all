﻿using System;
using System.ComponentModel;
using Tegra.DDD;

namespace Tegra.HumanResources.Domain.Entities
{
    [Description("Período Empresa")]
    public class EmployeeCompany : Entity
    {
        public int Id { get; set; }

        [Description("Data InÍcio")]
        [Format(Conversions.ConvertType.DATE)]
        public DateTime StartDate { get; set; }

        [Description("Data Final")]
        [Format(Conversions.ConvertType.DATE)]
        public DateTime? EndDate { get; set; }

        public int CompanyId { get; set; }
        public virtual Company Company { get; set; }

        public int EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }

        public bool IsActive()
        {
            return StartDate <= DateTime.Now && (EndDate == null || EndDate >= DateTime.Now);
        }
    }
}
