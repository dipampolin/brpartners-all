﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using Tegra.DDD;

namespace Tegra.HumanResources.Domain.Entities
{
    [Description("Holerite")]
    public class Payslip : Entity
    {
        public Payslip()
        {
            this.Items = new List<PayslipItem>();
        }

        public Payslip(DateTime date, int employeeId, int costCenterId, int companyId)
        {
            this.Items = new List<PayslipItem>();
            this.Date = new DateTime(date.Year, date.Month, 1);
            this.EmployeeId = employeeId;
            this.CostCenterId = costCenterId;
            this.CompanyId = companyId;
        }

        public int Id { get; set; }

        public DateTime Date { get; set; }

        public int CompanyCode { get; set; }

        public string CompanyName { get; set; }

        public int CostCenterCode { get; set; }

        public string CostCenterDescription { get; set; }

        [Description("Código Cargo")]
        public int RoleCode { get; set; }

        [Description("Descrição Cargo")]
        public string RoleDescription { get; set; }

        // Methods
        public Payslip AddItem(decimal value)
        {
            this.Items.Add(new PayslipItem(this, value));
            return this;
        }

        // FKs
        public int EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }

        public int CompanyId { get; set; }

        [ForeignKey("CompanyId")]
        public virtual Company Company { get; set; }

        public int CostCenterId { get; set; }
        public virtual CostCenter CostCenter { get; set; }

        // Reverse FK
        public virtual ICollection<PayslipItem> Items { get; set; }

    }
}
