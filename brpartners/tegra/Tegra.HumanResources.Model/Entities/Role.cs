﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq.Expressions;
using Tegra.DDD;

namespace Tegra.HumanResources.Domain.Entities
{
    [Description("Funcionário Cargo")]
    public class Role : Entity, IFiltrableEntity<Role>
    {

        public Expression<Func<Role, bool>> GetFilter(string name, string value)
        {
            bool boolValue = false;
            bool.TryParse(value, out boolValue);

            var filters = new Dictionary<string, Expression<Func<Role, bool>>>()
            {
                { "Name", x => x.Name.Contains(value) },
                { "Active", x => x.Active.Equals(boolValue) }
            };

            return filters[name];
        }

        public int Id { get; set; }

        [Description("Nome")]
        public string Name { get; set; }
    
        public bool Active { get; set; }

        [Format(Conversions.ConvertType.DATE)]
        public DateTime? CreatedDate { get; set; }
    }
}
