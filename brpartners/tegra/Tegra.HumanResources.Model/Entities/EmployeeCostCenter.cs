﻿using System;
using System.ComponentModel;
using Tegra.DDD;

namespace Tegra.HumanResources.Domain.Entities
{
    [Description("Período Centro de Custo")]
    public class EmployeeCostCenter : Entity
    {
        public int Id { get; set; }

        [Description("Data Início")]
        [Format(Conversions.ConvertType.DATE)]
        public DateTime StartDate { get; set; }

        [Description("Data Final")]
        [Format(Conversions.ConvertType.DATE)]
        public DateTime? EndDate { get; set; }

        public int EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }

        public int CostCenterId { get; set; }
        public virtual CostCenter CostCenter { get; set; }

        public bool IsActive()
        {
            return StartDate <= DateTime.Now && (EndDate == null || EndDate >= DateTime.Now);
        }
    }
}
