﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq.Expressions;
using Tegra.DDD;
using Tegra.HumanResources.Domain.Attributes;

namespace Tegra.HumanResources.Domain.Entities
{
    [Description("Centro de Custo")]
    public class CostCenter : Entity, IFiltrableEntity<CostCenter>
    {

        public CostCenter() { }

        public CostCenter(string code, string name)
        {
            this.Code = code;
            this.Name = name;
        }

        public int Id { get; set; }

        [Description("Código")]
        [Filterable]
        public string Code { get; set; }

        [Description("Nome")]
        public string Name { get; set; }

        public Expression<Func<CostCenter, bool>> GetFilter(string name, string value)
        {
            var filters = new Dictionary<string, Expression<Func<CostCenter, bool>>>()
            {
                { "Name", x => x.Name.Contains(value) },
            };

            return filters[name];
        }
    }
}
