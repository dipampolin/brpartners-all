﻿using System.ComponentModel;
using Tegra.DDD;

namespace Tegra.HumanResources.Domain.Entities
{
    [Description("Contato")]
    public class EmployeeContact : Entity
    {
        public int Id { get; set; }

        [Description("Tipo")]
        public string Type { get; set; }

        [Description("Contato")]
        public string Value { get; set; }

        public bool Active { get; set; }

        public int EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }
    }
}
