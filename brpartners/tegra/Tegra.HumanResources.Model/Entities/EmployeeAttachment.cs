﻿using System.ComponentModel;
using Tegra.DDD;

namespace Tegra.HumanResources.Domain.Entities
{
    [Description("Funcionário Anexos")]
    public class EmployeeAttachment : Entity
    {
        public int Id { get; set; }

        [Description("Descrição")]
        public string Description { get; set; }

        [Description("Arquivo")]
        public byte[] FileContent { get; set; }

        public string FileName { get; set; }

        public int EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }
    }
}
