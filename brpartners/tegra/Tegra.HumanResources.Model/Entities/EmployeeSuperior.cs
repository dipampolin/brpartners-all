﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace Tegra.HumanResources.Domain.Entities
{
    [NotMapped]
    [Description("Superior")]
    public class EmployeeSuperior
    {
        [Description("Nome")]
        public string Name { get; set; }

        [Description("Sigla")]
        public string Initials { get; set; }
    }
}
