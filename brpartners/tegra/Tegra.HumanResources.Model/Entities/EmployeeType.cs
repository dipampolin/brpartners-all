﻿using System;
using System.ComponentModel;
using Tegra.DDD;

namespace Tegra.HumanResources.Domain.Entities
{
    [Description("Tipo de Funcionário")]
    public class EmployeeType : Entity
    {
        public int Id { get; set; }

        [Description("Data Início")]
        [Format(Conversions.ConvertType.DATE)]
        public DateTime StartDate { get; set; }

        [Description("Data Final")]
        [Format(Conversions.ConvertType.DATE)]
        public DateTime? EndDate { get; set; }

        public int EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }

        public int TypeId { get; set; }

        public bool IsActive()
        {
            return StartDate <= DateTime.Now && (EndDate == null || EndDate >= DateTime.Now);
        }

        public string TypeDescription()
        {
            string t = string.Empty;

            switch (TypeId)
            {
                case 1:
                    t = "Pró-Labore";
                    break;
                case 2:
                    t = "Estagiário";
                    break;
                case 3:
                    t = "Aprendiz";
                    break;
                case 4:
                    t = "Funcionário";
                    break;
                case 5:
                    t = "Prestador de Serviço";
                    break;
                default:
                    t = "";
                    break;
            }

            return t;
        }
    }
}
