﻿using System.ComponentModel;
using Tegra.DDD;

namespace Tegra.HumanResources.Domain.Entities
{
    [Description("Estado Civil")]
    public class CivilStatus : Entity
    {

        public CivilStatus() { }

        public CivilStatus(string value)
        {
            Value = value;
        }

        public int Id { get; set; }

        [Description("Tipo")]
        public string Value { get; set; }

    }
}
