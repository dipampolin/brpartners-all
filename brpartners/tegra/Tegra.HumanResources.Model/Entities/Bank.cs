﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq.Expressions;
using Tegra.DDD;

namespace Tegra.HumanResources.Domain.Entities
{
    [Description("Banco")]
    public class Bank : Entity, IFiltrableEntity<Bank>
    {

        public Expression<Func<Bank, bool>> GetFilter(string name, string value)
        {
            bool boolValue = false;
            bool.TryParse(value, out boolValue);

            var filters = new Dictionary<string, Expression<Func<Bank, bool>>>()
            {
                { "Code", x => x.Code.Contains(value) },
                { "Name", x => x.Name.Contains(value) },
                { "Active", x => x.Active.Equals(boolValue) }
            };

            return filters[name];
        }

        public int Id { get; set; }

        [Description("Código")]
        public string Code { get; set; }

        [Description("Nome")]
        public string Name { get; set; }

        public bool Active { get; set; }

        [Format(Conversions.ConvertType.DATE)]
        public DateTime? CreatedDate { get; set; }
    }
}
