﻿using System.ComponentModel;
using Tegra.DDD;

namespace Tegra.HumanResources.Domain.Entities
{
    [Description("Cidade")]
    public class City : Entity
    {

        public City() { }

        public City(string name, State state = null)
        {
            this.State = state;
            this.Name = name;
        }

        public int Id { get; set; }

        [Description("Nome")]
        public string Name { get; set; }

        public virtual State State { get; set; }

        public int StateId { get; set; }

    }
}
