﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq.Expressions;
using Tegra.DDD;

namespace Tegra.HumanResources.Domain.Entities
{
    [Description("Funcionário Benefício")]
    public class Benefit : Entity, IFiltrableEntity<Benefit>
    {
        public Expression<Func<Benefit, bool>> GetFilter(string name, string value)
        {

            bool boolValue = false;
            bool.TryParse(value, out boolValue);

            var filters = new Dictionary<string, Expression<Func<Benefit, bool>>>()
            {
                { "Type", x => x.Type.Name.Contains(value) },
                { "Description", x => x.Description.Contains(value) },
            };

            return filters[name];
        }

        public Benefit() { }

        public Benefit(int benefitTypeId, string description, decimal value)
        {
            this.BenefitTypeId = benefitTypeId;
            this.Value = value;
        }

        public int Id { get; set; }

        public virtual BenefitType Type { get; set; }

        public int BenefitTypeId { get; set; }

        [Description("Descrição")]
        public string Description { get; set; }

        [Description("Valor")]
        public decimal Value { get; set; }

    }
}
