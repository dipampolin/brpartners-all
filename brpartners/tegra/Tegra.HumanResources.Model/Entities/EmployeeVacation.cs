﻿using System;
using System.ComponentModel;
using Tegra.DDD;
using Tegra.HumanResources.Domain.Attributes;

namespace Tegra.HumanResources.Domain.Entities
{
    [Description("Férias")]
    public class EmployeeVacation : Entity
    {
        public int Id { get; set; }

        public DateTime AquisitionStartDate { get; set; }

        public DateTime AquisitionEndDate { get; set; }

        [Description("Data Início")]
        [Format(Conversions.ConvertType.DATE)]
        [Filterable]
        public DateTime EnjoymentStartDate { get; set; }

        [Description("Data Final")]
        [Format(Conversions.ConvertType.DATE)]
        [Filterable]
        public DateTime EnjoymentEndDate { get; set; }

        [Description("Data Recebimento")]
        [Format(Conversions.ConvertType.DATE)]
        public DateTime ReceiptDate { get; set; }

        [Description("Valor")]
        public decimal Value { get; set; }

        public int EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }

    }
}
