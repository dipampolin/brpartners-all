﻿using Tegra.DDD;

namespace Tegra.HumanResources.Domain.Entities
{
    public class NotificationFilter : Entity
    {
        public int Id { get; set; }

        public string Field { get; set; }

        public string FieldAlias { get; set; }

        public string Table { get; set; }

        public string TableAlias { get; set; }

        public string Value { get; set; }

        public int NotificationId { get; set; }

        public string Type { get; set; }

        public virtual Notification Notification { get; set; }
    }
}
