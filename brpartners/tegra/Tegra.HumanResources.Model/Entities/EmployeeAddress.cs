﻿using System.ComponentModel;
using Tegra.DDD;

namespace Tegra.HumanResources.Domain.Entities
{
    [Description("Endereço")]
    public class EmployeeAddress : Entity
    {
        public EmployeeAddress() { }

        public int Id { get; set; }

        [Description("Tipo")]
        public string Type { get; set; }

        [Description("Rua")]
        public string Street { get; set; }

        [Description("Cep")]
        [Format(Conversions.ConvertType.CEP)]
        public string CEP { get; set; }

        [Description("Número")]
        public string Number { get; set; }

        [Description("Complemento")]
        public string Complement { get; set; }

        [Description("Bairro")]
        public string Neighborhood { get; set; }

        public bool Active { get; set; }

        // FKs
        public int EmployeeId { get; set; }
        public Employee Employee { get; set; }
        public int CityId { get; set; }
        public City City { get; set; }
    }
}