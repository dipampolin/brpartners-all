﻿using System;
using System.ComponentModel;
using Tegra.DDD;

namespace Tegra.HumanResources.Domain.Entities
{
    [Description("Funcionário Variáveis")]
    public class EmployeeVariables : Entity
    {
        public int Id { get; set; }

        public int? EmployeeVariablesTypeId { get; set; }
        public virtual EmployeeVariablesType Type { get; set; }

        [Description("Descrição")]
        public string Description { get; set; }

        [Description("Valor")]
        [Format(Conversions.ConvertType.MOEDA)]
        public decimal Value { get; set; }

        [Description("Data")]
        [Format(Conversions.ConvertType.DATE)]
        public DateTime Date { get; set; }

        public int EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }
    }
}
