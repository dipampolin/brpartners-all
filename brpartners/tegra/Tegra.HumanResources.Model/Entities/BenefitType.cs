﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.Text;
using Tegra.DDD;
using System.Linq.Expressions;

namespace Tegra.HumanResources.Domain.Entities
{
    [Serializable]
    [Description("Funcionário Benefício (Tipo)")]
    public class BenefitType : Entity
    {
        public int Id { get; set; }

        [Description("Tipo")]
        public string Name { set; get; }

        public BenefitCategory? Category { get; set; }

    }

    public enum BenefitCategory
    {
        ODONTOLOGICO,
        SAUDE
    }
}
