﻿using System.ComponentModel;
using Tegra.DDD;

namespace Tegra.HumanResources.Domain.Entities
{
    [Description("Escolaridade")]
    public class EducationalStage : Entity
    {
        public EducationalStage() { }

        public EducationalStage(string value)
        {
            Value = value;
        }

        public int Id { get; set; }

        [Description("Tipo")]
        public string Value { get; set; }

    }
}
