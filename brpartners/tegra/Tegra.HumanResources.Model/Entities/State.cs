﻿using System.Collections.Generic;
using System.ComponentModel;
using Tegra.DDD;

namespace Tegra.HumanResources.Domain.Entities
{
    [Description("Estado")]
    public class State : Entity
    {

        public State() { }

        public State(string UF, string name)
        {
            this.Name = name;
            this.UF = UF;
            this.Cities = new List<City>();
        }

        public int Id { get; set; }

        public string UF { get; set; }

        [Description("Nome")]
        public string Name { get; set; }

        public virtual IList<City> Cities { get; set; }

        public State AddCity(City c)
        {
            c.State = this;
            this.Cities.Add(c);
            return this;
        }

    }
}
