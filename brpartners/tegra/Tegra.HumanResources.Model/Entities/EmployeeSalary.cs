﻿using System;
using System.ComponentModel;
using Tegra.DDD;

namespace Tegra.HumanResources.Domain.Entities
{
    [Description("Salário")]
    public class EmployeeSalary : Entity
    {
        public int Id { get; set; }

        [Description("Descrição")]
        public string Description { get; set; }

        [Description("Categoria")]
        public string Category { get; set; }

        [Description("Período")]
        public string SalaryPeriod { get; set; }

        [Description("Carga Horária")]
        public string WorkLoad { get; set; }

        [Description("Valor")]
        public string Salary { get; set; }

        [Description("Hora Extra")]
        public string ExtraHour { get; set; }

        [Description("Gratificação")]
        public string Gratification { get; set; }

        [Description("Triennium")]
        public string Triennium { get; set; }

        [Description("Data Início")]
        [Format(Conversions.ConvertType.DATE)]
        public DateTime? StartDate { get; set; }

        [Description("Data Final")]
        [Format(Conversions.ConvertType.DATE)]
        public DateTime? EndDate { get; set; }

        public int EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }
    }
}
