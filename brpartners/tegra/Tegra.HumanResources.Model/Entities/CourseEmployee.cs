﻿using System;
using System.ComponentModel;
using Tegra.DDD;

namespace Tegra.HumanResources.Domain.Entities
{
    [Description("Período Curso")]
    public class CourseEmployee : Entity
    {
        public int Id { get; set; }

        public int? CourseId { get; set; }
        public virtual Course Course { get; set; }

        public int EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }

        public decimal? Value { get; set; }

        [Description("Prazo Máximo")]
        [Format(Conversions.ConvertType.DATE)]
        public DateTime? Deadline { get; set; }

        [Description("Data InÍcio")]
        [Format(Conversions.ConvertType.DATE)]
        public DateTime? StartDate { get; set; }

        [Description("Data Final")]
        [Format(Conversions.ConvertType.DATE)]
        public DateTime? EndDate { get; set; }

        public bool Required { get; set; }

    }
}
