﻿using System.ComponentModel;
using Tegra.DDD;
using Tegra.HumanResources.Domain.Attributes;

namespace Tegra.HumanResources.Domain.Entities
{
    [Description("Dados Bancários")]
    public class EmployeeBankAccount : Entity
    {
        public int Id { get; set; }

        public bool Main { get; set; }

        [Description("Nome")]
        public string Bank { get; set; }

        [Description("Agência")]
        [Filterable]
        public string Agency { get; set; }

        [Description("Conta")]
        public string Account { get; set; }

        public int EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }

        public int? BankId { get; set; }
        public virtual Bank BankObj { get; set; }
    }
}
