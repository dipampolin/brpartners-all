﻿using System;
using System.ComponentModel;
using Tegra.DDD;

namespace Tegra.HumanResources.Domain.Entities
{
    [Description("Contrato")]
    public class EmployeeContract : Entity
    {
        public int Id { get; set; }

        [Description("Descrição")]
        public string Description { get; set; }

        [Description("Data Início")]
        [Format(Conversions.ConvertType.DATE)]
        public DateTime StartDate { get; set; }

        [Description("Data Final")]
        [Format(Conversions.ConvertType.DATE)]
        public DateTime? EndDate { get; set; }

        [Description("Aprovado")]
        [Format(Conversions.ConvertType.DEFAULT_BOOL)]
        public bool Approved { get; set; }

        public int EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }
    }
}
