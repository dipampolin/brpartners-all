﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;
using Tegra.DDD;

namespace Tegra.HumanResources.Domain.Entities
{
    public class Notification : Entity, IFiltrableEntity<Notification>
    {
        public Expression<Func<Notification, bool>> GetFilter(string name, string value)
        {

            var filters = new Dictionary<string, Expression<Func<Notification, bool>>>()
            {
                { "Name", x => x.Name.Contains(value) }
            };

            return filters[name];
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public string Frequency { get; set; }

        public bool Active { get; set; }

        [MaxLength]
        public string Query { get; set; }

        public virtual ICollection<NotificationItem> Items { get; set; }

        public virtual ICollection<NotificationDestination> Destinations { get; set; }

        public virtual ICollection<NotificationFilter> Filters { get; set; }
    }
}
