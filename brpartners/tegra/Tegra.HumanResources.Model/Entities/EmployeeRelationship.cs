﻿using System;
using System.ComponentModel;
using Tegra.DDD;

namespace Tegra.HumanResources.Domain.Entities
{
    [Description("Pessoas Relacionadas")]
    public class EmployeeRelationship : Entity
    {
        public int Id { get; set; }

        [Description("Parentesco")]
        public string Relationship { get; set; }

        [Description("Nome")]
        public string Name { get; set; }

        [Description("CPF")]
        [Format(Conversions.ConvertType.CPF)]
        public string Cpf { get; set; }

        [Description("Usa Plano de Saúde")]
        [Format(Conversions.ConvertType.DEFAULT_BOOL)]
        public bool UseHealthPlan { get; set; }

        public Nullable<int> HealthPlanId { get; set; }

        public virtual Benefit HealthPlan { get; set; }

        [Description("Usa Plano Odontológico")]
        [Format(Conversions.ConvertType.DEFAULT_BOOL)]
        public bool UseOdontologyPlan { get; set; }

        public Nullable<int> OdontologyPlanId { get; set; }

        public virtual Benefit OdontologyPlan { get; set; }

        [Description("IR")]
        [Format(Conversions.ConvertType.DEFAULT_BOOL)]
        public bool IR { get; set; }

        [Description("Data Início")]
        [Format(Conversions.ConvertType.DATE)]
        public DateTime StartDate { get; set; }

        [Description("Data Final")]
        [Format(Conversions.ConvertType.DATE)]
        public DateTime? EndDate { get; set; }

        [Description("Data de Nascimento")]
        [Format(Conversions.ConvertType.DATE)]
        public DateTime? Birthdate { get; set; }

        public int EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }
    }
}
