﻿using System.ComponentModel;
using Tegra.DDD;

namespace Tegra.HumanResources.Domain.Entities
{
    [Description("Vencimentos")]
    public class PayslipItem : Entity
    {
        public PayslipItem() { }

        public PayslipItem(Payslip payslip, decimal value)
        {
            this.Payslip = payslip;
            this.Value = value;
        }

        public int Id { get; set; }

        [Description("Rubrica")]
        public int Rubric { get; set; }

        [Description("Descrição Rubrica")]
        public string RubricDescription { get; set; }

        [Description("Tipo Rubrica")]
        public string RubricType { get; set; }

        [Description("Referência")]
        [Format(Conversions.ConvertType.MOEDA)]
        public decimal Reference { get; set; }

        [Description("Valor")]
        [Format(Conversions.ConvertType.MOEDA)]
        public decimal Value { get; set; }

        // FKs
        public int PayslipId { get; set; }
        public virtual Payslip Payslip { get; set; }

    }
}
