﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Tegra.DDD;

namespace Tegra.HumanResources.Domain.Entities
{
    [Description("Funcionário Variáveis (Tipo)")]
    public class EmployeeVariablesType : Entity
    {
        public int Id { get; set; }

        [Description("Tipo")]
        public string Name { get; set; }
    }
}
