﻿namespace Tegra.HumanResources.Domain.Util
{
    public interface ITemplateGenerator
    {
        string Generate(object model);
    }
}
