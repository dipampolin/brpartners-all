﻿using System;

namespace Tegra.DDD
{
    public interface IEntity
    {
        DateTime? DeletedDate { get; }

        void Delete();
    }
}
