﻿using System.Data.Entity;

namespace Tegra.DDD
{
    public interface IContext
    {
        void EnableAutoDetectChanges();

        void DisableAutoDetectChanges();

        DbContextTransaction BeginTransaction();
    }
}
