﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Tegra.DDD
{
    public abstract class Entity : IEntity
    {

        public Entity()
        {
            this.CreatedDate = DateTime.Now;
        }

        [Index]
        public DateTime? DeletedDate { get; protected set; }

        [Required]
        public DateTime CreatedDate { get; set; }

        public void Delete()
        {
            this.DeletedDate = DateTime.Now;
        }
    }
}
