﻿using System;
using System.Linq.Expressions;

namespace Tegra.DDD
{
    public interface IFiltrableEntity<TEntity> : IEntity
    {

        Expression<Func<TEntity, bool>> GetFilter(string name, string value);

    }
}
