﻿namespace Tegra.DDD.Repository
{
    public class BaseRepository<TContext>
    {

        protected TContext _context;

        public TContext getContext()
        {
            return this._context;
        }

        public void SetContext(TContext context)
        {
            _context = context;
        }
    }
}
