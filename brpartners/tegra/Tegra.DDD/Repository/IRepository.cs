﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Tegra.DDD.Pagination;

namespace Tegra.DDD.Repository
{
    public interface IRepository<TEntity, TKey>
        where TEntity : Entity
        where TKey : IComparable
    {

        TEntity Find(TKey id);

        List<TEntity> All();

        List<TEntity> AllWithInclude(params Expression<Func<TEntity, object>>[] includeSelector);

        TEntity Add(TEntity entity);

        TEntity InsertOrUpdate(TEntity entity, TKey id);

        IEnumerable<TEntity> AddAll(IEnumerable<TEntity> entities);

        TEntity Update(TEntity entity);

        bool RemoveAll(List<TEntity> entities);

        bool Remove(TKey id, bool logic = true);

        bool Remove(TEntity entity, bool logic = true);

        List<U> SelectBy<U>(Expression<Func<TEntity, bool>> exp, Expression<Func<TEntity, U>> columns);

        List<TEntity> SelectBy(Expression<Func<TEntity, bool>> exp);

        List<U> Select<U>(Expression<Func<TEntity, U>> columns);

        PagedResult<U> SelectPagedBy<U>(PaginationSettings settings, Expression<Func<TEntity, bool>> exp, Expression<Func<TEntity, U>> columns);

        PagedResult<TEntity> SelectPagedBy(PaginationSettings settings, Expression<Func<TEntity, bool>> exp);

        PagedResult<U> SelectPaged<U>(PaginationSettings settings, Expression<Func<TEntity, U>> columns);

        IQueryable<TEntity> Query(Expression<Func<TEntity, bool>> exp = null);

        IQueryable<U> Query<U>(Expression<Func<TEntity, U>> columns);

        IQueryable<TResult> QueryBy<TResult>(Expression<Func<TEntity, bool>> exp, Expression<Func<TEntity, TResult>> columns);

        TEntity FindWithInclude(TKey id, params Expression<Func<TEntity, object>>[] includeSelector);

        DbContextTransaction BeginTransaction();

    }
}
