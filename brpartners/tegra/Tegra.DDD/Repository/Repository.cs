﻿using LinqKit;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using Tegra.DDD.Extensions;
using Tegra.DDD.Pagination;

namespace Tegra.DDD.Repository
{
    public abstract class Repository<TContext, TEntity, TKey> : BaseRepository<TContext>, IRepository<TEntity, TKey>
        where TContext : DbContext, IContext
        where TEntity : Entity, new()
        where TKey : IComparable
    {


        public virtual TEntity Find(TKey id)
        {
            return _context.Set<TEntity>().Find(id);
        }

        public virtual List<TEntity> All()
        {
            return _context.Set<TEntity>()
                .ToList();
        }

        public virtual List<TEntity> AllWithInclude(params Expression<Func<TEntity, object>>[] includeSelector)
        {
            return includeSelector.Aggregate(_context.Set<TEntity>().AsQueryable(), IncludeInternal)
                .ToList();
        }

        public virtual TEntity Add(TEntity entity)
        {
            _context.Set<TEntity>().Add(entity);
            _context.SaveChanges();

            return entity;
        }

        public virtual TEntity InsertOrUpdate(TEntity entity, TKey id)
        {
            var isNew = id == null || id.CompareTo(0) == 0;
            _context.Entry(entity).State = isNew ? EntityState.Added : EntityState.Modified;
            if (!isNew)
            {
                entity.CreatedDate = GetCreatedDate(id);
            }
            _context.SaveChanges();
            return entity;
        }

        private DateTime GetCreatedDate(TKey id)
        {
            var entity = Find(id);
            if (entity != null)
            {
                return entity.CreatedDate;
            }
            return new DateTime();
        }

        public virtual IEnumerable<TEntity> AddAll(IEnumerable<TEntity> entities)
        {
            _context.Set<TEntity>().AddRange(entities);
            _context.SaveChanges();

            return entities;
        }

        public virtual TEntity Update(TEntity entity)
        {
            try
            {
                _context.EnableAutoDetectChanges();
                _context.SaveChanges();
            }
            finally
            {
                _context.DisableAutoDetectChanges();
            }

            return entity;
        }

        public virtual bool Remove(TKey id, bool logic = true)
        {
            var entity = Find(id);

            if (entity == null)
            {
                throw new ArgumentException("Entity not found");
            }

            return Remove(entity, logic);
        }

        public virtual bool RemoveAll(List<TEntity> entities)
        {
            _context.EnableAutoDetectChanges();
            _context.Set<TEntity>().RemoveRange(entities);
            _context.DisableAutoDetectChanges();
            return _context.SaveChanges() > 0;
        }

        public virtual bool Remove(TEntity entity, bool logic = true)
        {

            try
            {
                if (logic)
                {
                    entity.Delete();
                    Update(entity);
                    return true;
                }

                _context.EnableAutoDetectChanges();
                _context.Set<TEntity>().Remove(entity);
                _context.DisableAutoDetectChanges();
                return _context.SaveChanges() > 0;
            }
            catch (Exception e)
            {
                throw new ArgumentException("Falha ao excluir entidade", e);
            }

        }

        public virtual List<TResult> SelectBy<TResult>(Expression<Func<TEntity, bool>> exp, Expression<Func<TEntity, TResult>> columns)
        {
            return QueryBy(exp, columns).ToList();
        }

        public virtual List<TEntity> SelectBy(Expression<Func<TEntity, bool>> exp)
        {
            var query = _context.Set<TEntity>().AsQueryable();

            query = AddFixedConditions(query);

            return query.Where(exp).ToList();
        }

        public virtual List<TResult> Select<TResult>(Expression<Func<TEntity, TResult>> columns)
        {
            return Query(columns).ToList();
        }

        public virtual IQueryable<TResult> QueryBy<TResult>(Expression<Func<TEntity, bool>> exp, Expression<Func<TEntity, TResult>> columns)
        {
            var query = _context.Set<TEntity>().AsQueryable();

            query = AddFixedConditions(query);

            return query.Where(exp).Select<TEntity, TResult>(columns);
        }

        public virtual IQueryable<TEntity> Query(Expression<Func<TEntity, bool>> exp = null)
        {
            var query = _context.Set<TEntity>().AsQueryable();
            query = AddFixedConditions(query);

            if (exp != null)
            {
                query = query.Where(exp);
            }

            return query;
        }

        public virtual IQueryable<U> Query<U>(Expression<Func<TEntity, U>> columns)
        {
            var query = _context.Set<TEntity>().AsQueryable();
            query = AddFixedConditions(query);
            return query.Select<TEntity, U>(columns);
        }

        public virtual PagedResult<TEntity> SelectPagedBy(PaginationSettings settings, Expression<Func<TEntity, bool>> exp)
        {
            var query = _context.Set<TEntity>().AsQueryable();
            query = AddFixedConditions(query);

            if (exp != null)
            {
                query = query.Where(exp);
            }


            var total = query.Count();

            var orderDirection = settings.OrderDirection.ToString().Equals("Desc") ? "Descending" : "Ascending";

            var data = query.OrderBy<TEntity>(settings.OrderColumn, orderDirection)
                         .Skip(settings.Start)
                         .Take(settings.Length)
                         .ToList();

            return new PagedResult<TEntity>(settings.Start, total, settings.Length, data);
        }

        public virtual PagedResult<TResult> SelectPagedBy<TResult>(PaginationSettings settings, Expression<Func<TEntity, bool>> exp, Expression<Func<TEntity, TResult>> columns)
        {
            var query = _context.Set<TEntity>().AsQueryable();
            query = AddFixedConditions(query);

            if (exp != null)
            {
                query = query.AsExpandable().Where(exp);
            }

            if (typeof(IFiltrableEntity<TEntity>).IsAssignableFrom(typeof(TEntity)) && settings.Search != null && settings.Search.Count > 0)
            {
                var predicate = PredicateBuilder.True<TEntity>();
                settings.Search.Keys.ToList().ForEach(key =>
                {
                    var expFilter = (new TEntity() as IFiltrableEntity<TEntity>).GetFilter(key, settings.Search[key]);
                    predicate = predicate.And(expFilter);
                });

                query = query.AsExpandable().Where(predicate);
            }

            var total = query.AsQueryable().Count();

            var orderDirection = settings.OrderDirection.ToString().Equals("Desc") ? "Descending" : "Ascending";

            var data = query.OrderBy<TEntity>(settings.OrderColumn, orderDirection)
                            .Select<TEntity, TResult>(columns)
                            .Skip(settings.Start)
                            .Take(settings.Length)
                            .ToList();

            return new PagedResult<TResult>(settings.Start, total, settings.Length, data);
        }


        public virtual PagedResult<TResult> SelectPaged<TResult>(PaginationSettings settings, Expression<Func<TEntity, TResult>> columns)
        {

            var query = _context.Set<TEntity>().AsQueryable();
            query = AddFixedConditions(query);

            if (typeof(IFiltrableEntity<TEntity>).IsAssignableFrom(typeof(TEntity)) && settings.Search != null && settings.Search.Count > 0)
            {
                var predicate = PredicateBuilder.True<TEntity>();
                settings.Search.Keys.ToList().ForEach(key =>
                {
                    var exp = (new TEntity() as IFiltrableEntity<TEntity>).GetFilter(key, settings.Search[key]);
                    predicate = predicate.And(exp);
                });

                query = query.AsExpandable().Where(predicate);
            }

            var total = query.Count();

            var orderDirection = settings.OrderDirection.ToString().Equals("Desc") ? "Descending" : "Ascending";

            query = query.OrderBy<TEntity>(settings.OrderColumn, orderDirection);

            var data = query.Select<TEntity, TResult>(columns)
                         .Skip(settings.Start)
                         .Take(settings.Length)
                         .ToList();

            return new PagedResult<TResult>(settings.Start, total, settings.Length, data);
        }

        protected virtual IQueryable<TEntity> AddFixedConditions(IQueryable<TEntity> query)
        {
            query.Where(x => x.DeletedDate == null);
            return query;
        }

        protected virtual IQueryable<TEntity> FindByIdQuery(IQueryable<TEntity> query, TKey id)
        {
            throw new NotImplementedException();
        }

        public virtual TEntity FindWithInclude(TKey id, params Expression<Func<TEntity, object>>[] includeSelector)
        {
            var query = FindByIdQuery(_context.Set<TEntity>(), id);

            query = includeSelector.Aggregate(query, IncludeInternal);

            return query.FirstOrDefault();
        }

        public DbContextTransaction BeginTransaction()
        {
            return _context.BeginTransaction();
        }

        protected virtual IQueryable<TEntity> FindWithInclude(IQueryable<TEntity> query, params Expression<Func<TEntity, object>>[] includeSelector)
        {
            return includeSelector.Aggregate(query, IncludeInternal);
        }

        private IQueryable<T> IncludeInternal<T>(IQueryable<T> query, Expression<Func<TEntity, object>> selector)
        {
            string propertyName = FuncToString(selector);
            return query.Include(propertyName);
        }

        private string GetPropertyName<T>(Expression<Func<T, object>> expression)
        {
            MemberExpression memberExpr = expression.Body as MemberExpression;
            if (memberExpr == null)
                throw new ArgumentException("Expression body must be a member expression");
            return memberExpr.Member.Name;
        }

        private static string FuncToString(Expression selector)
        {
            switch (selector.NodeType)
            {
                case ExpressionType.Lambda:

                    var lambdaBody = ((LambdaExpression)selector).Body;

                    return lambdaBody.NodeType == ExpressionType.Call
                        ? FuncToString(lambdaBody)              // That`s meant for expressions like "x=> x.Terms.Select(q=> q.Logo) > Terms.Logo"
                        : ExtractFuncFromString(lambdaBody);    // That`s meant for expressions like "x=> x.Template.Logo"

                case ExpressionType.MemberAccess:
                    return ((selector as MemberExpression).Member as System.Reflection.PropertyInfo).Name;
                case ExpressionType.Call:
                    var method = selector as MethodCallExpression;
                    return FuncToString(method.Arguments[0]) + "." + FuncToString(method.Arguments[1]);
                case ExpressionType.Quote:
                    return FuncToString(((selector as UnaryExpression).Operand as LambdaExpression).Body);
            }
            throw new InvalidOperationException();
        }

        private static string ExtractFuncFromString(Expression lambdaBody)
        {
            var match = new Regex(@"[^.]\.(?<include>\S+)").Match(lambdaBody.ToString());
            return match.Groups["include"].Value.TrimEnd();
        }
    }
}
