﻿using System.Collections.Generic;

namespace Tegra.DDD.Pagination
{

    public class PaginationSettings
    {

        public enum enSortOrder
        {
            Asc,
            Desc
        }

        public PaginationSettings()
        {

        }

        public PaginationSettings(string orderColumn)
        {
            Start = 0;
            Length = 10;
            OrderDirection = enSortOrder.Asc;
            OrderColumn = orderColumn;
        }

        public PaginationSettings(string orderColumn, int pageSize, int page)
        {
            Length = pageSize;
            OrderColumn = orderColumn;
            OrderDirection = enSortOrder.Asc;
        }

        public int Start { get; set; }

        public int Length { get; set; }

        public Dictionary<string, string> Search { get; set; }

        public string OrderColumn { get; set; }

        public enSortOrder OrderDirection { get; set; }
    }
}
