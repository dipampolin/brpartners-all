﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Tegra.DDD.Pagination
{
    public class PagedResult<U>
    {

        public PagedResult(int page, int totalRecords, int pageSize, List<U> data)
        {
            this.page = page;
            this.recordsTotal = totalRecords;
            this.pageSize = pageSize;
            this.data = data;
        }

        public int recordsTotal { get; set; }

        public int recordsFiltered
        {
            get
            {
                return data.Count;
            }
            protected set { /*nothing*/ }
        }

        public int page { get; set; }

        [JsonProperty(PropertyName = "pageSize")]
        public int pageSize { get; set; }

        [JsonProperty(PropertyName = "pages")]
        public int pages
        {
            get
            {
                return (int)Math.Ceiling((double)recordsTotal / (double)pageSize);
            }
            protected set { /*nothing*/ }
        }

        public List<U> data { get; set; }

    }
}
