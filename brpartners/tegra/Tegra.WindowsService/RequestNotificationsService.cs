﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.ServiceProcess;
using System.Text;

namespace Tegra.WindowsService
{
    public partial class RequestNotificationsService : ServiceBase
    {

        private System.Timers.Timer timer;
        public RequestNotificationsService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            // Set up a timer to trigger every hour.
            timer = new System.Timers.Timer(60000);
            timer.AutoReset = true;
            timer.Elapsed += new System.Timers.ElapsedEventHandler(this.OnTimer);
            timer.Start();
        }

        public void OnTimer(object sender, System.Timers.ElapsedEventArgs args)
        {
#pragma warning disable CS0618 // Type or member is obsolete
            var settings = ConfigurationSettings.AppSettings;
#pragma warning restore CS0618 // Type or member is obsolete
            var endpoint = settings["notificationEndpoint"];
            Debug.WriteLine(endpoint);
            var request = WebRequest.Create(endpoint);
            request.Method = "POST";
            request.ContentType = "application/json";
            request.ContentLength = 0;
            try
            {
                using (var response = request.GetResponse())
                {
                    var encoding = ASCIIEncoding.ASCII;
                    using (var reader = new StreamReader(response.GetResponseStream(), encoding))
                    {
                        string responseText = reader.ReadToEnd();
                        Debug.WriteLine("Server Response: " + responseText);
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("ERRO: " + e.Message);
            }

        }

        protected override void OnStop()
        {
            this.timer.Stop();
        }
    }
}
