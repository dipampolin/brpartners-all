﻿using Tegra.DDD.Repository;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Repository;

namespace Tegra.HumanResources.Infra.Repository
{
    public class NotificationItemRepository : Repository<RHContext, NotificationItem, int>, INotificationItemRepository
    {
    }
}
