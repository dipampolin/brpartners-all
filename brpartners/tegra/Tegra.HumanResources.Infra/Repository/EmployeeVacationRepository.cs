﻿using System.Linq;
using Tegra.DDD.Repository;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Repository;

namespace Tegra.HumanResources.Infra.Repository
{
    public class EmployeeVacationRepository : Repository<RHContext, EmployeeVacation, int>, IEmployeeVacationRepository
    {
        public EmployeeVacation GetByEmployee(int employeeId)
        {
            return this.Query(c => c.EmployeeId == employeeId).FirstOrDefault();
        }
    }
}
