﻿using Tegra.DDD.Repository;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Repository;

namespace Tegra.HumanResources.Infra.Repository
{
    public class ReportItemRepository : Repository<RHContext, ReportItem, int>, IReportItemRepository
    {
    }
}
