﻿using System.Linq;
using Tegra.DDD.Repository;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Repository;

namespace Tegra.HumanResources.Infra.Repository
{
    public class EmployeeContractRepository : Repository<RHContext, EmployeeContract, int>, IEmployeeContractRepository
    {
        public EmployeeContract GetByEmployee(int employeeId)
        {
            return this.Query(c => c.EmployeeId == employeeId).FirstOrDefault();
        }
    }
}
