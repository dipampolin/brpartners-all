﻿using Tegra.DDD.Repository;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Infra;

namespace Tegra.HumanResources.Domain.Repository
{
    public class CourseRepository : Repository<RHContext, Course, int>, ICourseRepository
    {

    }
}
