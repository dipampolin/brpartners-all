﻿using System.Linq;
using Tegra.DDD.Repository;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Repository;

namespace Tegra.HumanResources.Infra.Repository
{
    public class EmployeeCourseRepository : Repository<RHContext, CourseEmployee, int>, IEmployeeCourseRepository
    {
        public CourseEmployee GetByEmployee(int employeeId)
        {
            return this.Query(c => c.EmployeeId == employeeId).FirstOrDefault();
        }
    }
}
