﻿using Tegra.DDD.Repository;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Infra;
using System.Linq;

namespace Tegra.HumanResources.Domain.Repository
{
    public class RelationshipRepository : Repository<RHContext, EmployeeRelationship, int>, IRelationshipRepository
    {
        public EmployeeRelationship GetByEmployee(int employeeId)
        {
            return this.Query(c => c.EmployeeId == employeeId).FirstOrDefault();
        }
    }
}
