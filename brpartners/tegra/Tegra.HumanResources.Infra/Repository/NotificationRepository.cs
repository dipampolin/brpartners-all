﻿using System.Data.Entity;
using System.Linq;
using Tegra.DDD.Repository;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Repository;

namespace Tegra.HumanResources.Infra.Repository
{
    public class NotificationRepository : Repository<RHContext, Notification, int>, INotificationRepository
    {
        public IQueryable<Notification> GetNotification(int id)
        {
            if (id > 0)
                return this.Query(x => x.Id == id).Include(c => c.Items).Include(c => c.Destinations);
            return this.Query(c => c.Id > 0).Include(c => c.Items).Include(c => c.Destinations);
        }
    }
}
