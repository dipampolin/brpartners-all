﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Tegra.DDD.Extensions;
using Tegra.DDD.Pagination;
using Tegra.DDD.Repository;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Repository;
using Tegra.HumanResources.Domain.Representations;

namespace Tegra.HumanResources.Infra.Repository
{
    public class PayslipRepository : Repository<RHContext, Payslip, int>, IPayslipRepository
    {
        public Payslip GetToImport(int year, int month, string employeeCode)
        {
            var date = new DateTime(year, month, 1);

            return this.Query(x => x.Date.Equals(date) && x.Employee.Initials == employeeCode)
                .Include(x => x.Items)
                .SingleOrDefault();
        }

        public PagedResult<PayslipByMonth> List(PaginationSettings settings)
        {
            var query = this.Query().AsQueryable();

            if (settings.Search != null)
            {

                if (settings.Search.Keys.Contains("CompanyName"))
                {
                    var name = settings.Search["CompanyName"];
                    query = query.Where(x => x.CompanyName.Contains(name));
                }

                if (settings.Search.Keys.Contains("Year"))
                {
                    var year = int.Parse(settings.Search["Year"]);
                    query = query.Where(x => x.Date.Year == year);
                }

                if (settings.Search.Keys.Contains("Month"))
                {
                    var month = int.Parse(settings.Search["Month"]);
                    query = query.Where(x => x.Date.Month == month);
                }

            }

            var groupQuery = query.GroupBy(x => new
            {
                companyName = x.CompanyName,
                month = x.Date.Month,
                year = x.Date.Year,
                companyCode = x.CompanyCode
            }).Select(g => new
            {
                Count = g.Count(),
                CompanyName = g.Key.companyName,
                Month = g.Key.month,
                Year = g.Key.year,
                CompanyCode = g.Key.companyCode
            });



            var total = groupQuery.Count();

            var data = groupQuery
                .OrderBy(settings.OrderColumn, settings.OrderDirection.ToString())
                .Skip(settings.Start)
                .Take(settings.Length)
                .ToList()
                .Select(x => new PayslipByMonth
                {
                    CompanyName = x.CompanyName,
                    Count = x.Count,
                    CompanyCode = x.CompanyCode,
                    Date = new DateTime(x.Year, x.Month, 1)
                })
                .ToList();

            return new PagedResult<PayslipByMonth>(settings.Start, total, settings.Length, data);
        }

        public List<PayslipByExcel> GetPayslipToReport(Expression<Func<Payslip, bool>> predicate)
        {
            return this.SelectBy(predicate, p => new PayslipByExcel
            {
                CompanyCode = p.CompanyCode,
                CompanyName = p.CompanyName,
                CostCenterCode = p.CostCenter.Code,
                CostCenterDescription = p.CostCenter.Name,
                Date = p.Date,
                IdCustomer = p.Employee.Id,
                Code = p.Employee.Initials,
                Name = p.Employee.Name,
                IdPayslip = p.Id,
                RoleCode = p.RoleCode,
                RoleDescription = p.RoleDescription,
                Items = p.Items.Select(pi => new PayslipByItem
                {
                    IdPayslipItem = pi.Id,
                    Reference = pi.Reference,
                    Rubric = pi.Rubric,
                    RubricDescription = pi.RubricDescription,
                    RubricType = pi.RubricType,
                    Value = pi.Value
                }).ToList()
            }).ToList();
        }
    }
}
