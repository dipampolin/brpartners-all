﻿using System.Linq;
using Tegra.DDD.Repository;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Infra;

namespace Tegra.HumanResources.Domain.Repository
{
    public class BankAccountRepository : Repository<RHContext, EmployeeBankAccount, int>, IBankAccountRepository
    {
        public EmployeeBankAccount GetByEmployee(int employeeId)
        {
            return this.Query(c => c.EmployeeId == employeeId).FirstOrDefault();
        }
    }
}
