﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tegra.DDD.Repository;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Repository;

namespace Tegra.HumanResources.Infra.Repository
{
    public class EmployeeVariablesTypeRepository : Repository<RHContext, EmployeeVariablesType, int>, IEmployeeVariablesTypeRepository
    {

    }
}
