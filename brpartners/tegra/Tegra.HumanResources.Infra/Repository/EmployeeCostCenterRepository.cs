﻿using System;
using System.Linq;
using Tegra.DDD.Repository;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Repository;

namespace Tegra.HumanResources.Infra.Repository
{
    public class EmployeeCostCenterRepository : Repository<RHContext, EmployeeCostCenter, int>, IEmployeeCostCenterRepository
    {
        public EmployeeCostCenter GetByEmployee(int employeeId)
        {
            return this.Query(c => c.EmployeeId == employeeId).FirstOrDefault();
        }
    }
}
