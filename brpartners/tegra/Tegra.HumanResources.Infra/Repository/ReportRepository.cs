﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using Tegra.DDD.Repository;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Domain.Repository;
using Tegra.HumanResources.Domain.Representations;

namespace Tegra.HumanResources.Infra.Repository
{
    public class ReportRepository : Repository<RHContext, Report, int>, IReportRepository
    {
        public bool CreateQueryReport(Report report, string select)
        {
            return true;
        }

        public List<ReportByAlias> GetReportAlias()
        {
            string @namespace = "Tegra.HumanResources.Domain.Entities";
            var allClass = AppDomain.CurrentDomain.GetAssemblies()
                        .SelectMany(t => t.GetTypes())
                        .Where(t => t.IsClass && t.Namespace == @namespace);
            var reportList = new List<ReportByAlias>();
            if (allClass.Count() > 0)
            {
                var typelist =
                 allClass.Where(attr => ((DescriptionAttribute[])attr.GetCustomAttributes(typeof(DescriptionAttribute), true)).Where(g => g.Description != null) != null).ToList();


                typelist.ForEach(c =>
                {
                    ReportByAlias report = null;
                    var pro = c.GetCustomAttributes(typeof(DescriptionAttribute), true).FirstOrDefault();

                    if (pro != null && !string.IsNullOrEmpty(((DescriptionAttribute)pro).Description))
                    {
                        var proper = c.GetProperties().Where(f => ((DescriptionAttribute)f.GetCustomAttributes(typeof(DescriptionAttribute), true).FirstOrDefault()) != null);
                        proper.ToList().ForEach(h =>
                        {
                            report = new ReportByAlias();
                            report.Table = c.Name;
                            report.TableAlias = ((DescriptionAttribute)pro).Description;
                            var y = h.GetCustomAttributes(typeof(DescriptionAttribute), true).FirstOrDefault();
                            report.FieldAlias = ((DescriptionAttribute)y).Description.ToString();
                            report.Field = h.Name;
                            reportList.Add(report);
                        });
                    }
                });
            }

            return reportList;
        }

        private static Type[] GetTypesInNamespace(Assembly assembly, string nameSpace)
        {
            return assembly.GetTypes().Where(t => string.Equals(t.Namespace, nameSpace, StringComparison.Ordinal)).ToArray();
        }
    }
}
