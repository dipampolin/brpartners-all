namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_new_fields_for_crud : DbMigration
    {
        public override void Up()
        {
            AddColumn("rh.Role", "Active", c => c.Boolean(nullable: false, defaultValue: true));
        }
        
        public override void Down()
        {
            DropColumn("rh.Role", "Active");
        }
    }
}
