namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class remove_and_insert_new_civil_state_and_educational_stages : DbMigration
    {
        public override void Up()
        {
            Sql("delete from rh.Employee");
            Sql("delete from rh.CivilStatus");
            Sql("delete from rh.EducationalStage");
        }
        
        public override void Down()
        {
            Sql("delete from rh.Employee");
            Sql("delete from rh.CivilStatus");
            Sql("delete from rh.EducationalStage");
        }
    }
}
