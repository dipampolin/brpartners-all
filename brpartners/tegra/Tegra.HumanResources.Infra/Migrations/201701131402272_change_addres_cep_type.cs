namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class change_addres_cep_type : DbMigration
    {
        public override void Up()
        {
            AlterColumn("rh.EmployeeAddress", "CEP", c => c.String(nullable: false, maxLength: 256));
        }
        
        public override void Down()
        {
            AlterColumn("rh.EmployeeAddress", "CEP", c => c.Int(nullable: false));
        }
    }
}
