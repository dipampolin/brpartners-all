namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class remove_value_notification_item : DbMigration
    {
        public override void Up()
        {
            DropColumn("rh.NotificationItem", "Value");
        }
        
        public override void Down()
        {
            AddColumn("rh.NotificationItem", "Value", c => c.String(maxLength: 256));
        }
    }
}
