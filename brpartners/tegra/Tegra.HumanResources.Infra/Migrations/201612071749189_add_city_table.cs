namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_city_table : DbMigration
    {
        public override void Up()
        {
            AddColumn("rh.Employee", "CityId", c => c.Int());
            CreateIndex("rh.Employee", "CityId");
            AddForeignKey("rh.Employee", "CityId", "rh.City", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("rh.Employee", "CityId", "rh.City");
            DropIndex("rh.Employee", new[] { "CityId" });
            DropColumn("rh.Employee", "CityId");
        }
    }
}
