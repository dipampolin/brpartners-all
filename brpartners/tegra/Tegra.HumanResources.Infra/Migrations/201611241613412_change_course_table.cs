namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class change_course_table : DbMigration
    {
        public override void Up()
        {
            AddColumn("rh.CourseEmployee", "StartDate", c => c.DateTime(nullable: false));
            AddColumn("rh.CourseEmployee", "EndDate", c => c.DateTime());
            DropColumn("rh.CourseEmployee", "Completed");
        }
        
        public override void Down()
        {
            AddColumn("rh.CourseEmployee", "Completed", c => c.Boolean());
            DropColumn("rh.CourseEmployee", "EndDate");
            DropColumn("rh.CourseEmployee", "StartDate");
        }
    }
}
