namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_employee_porcentage_field : DbMigration
    {
        public override void Up()
        {
            AddColumn("rh.Employee", "Percentage", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("rh.Employee", "Percentage");
        }
    }
}
