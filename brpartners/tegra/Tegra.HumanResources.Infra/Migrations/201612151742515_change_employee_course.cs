namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class change_employee_course : DbMigration
    {
        public override void Up()
        {
            AlterColumn("rh.CourseEmployee", "StartDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("rh.CourseEmployee", "StartDate", c => c.DateTime(nullable: false));
        }
    }
}
