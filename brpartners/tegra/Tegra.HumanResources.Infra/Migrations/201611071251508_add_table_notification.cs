namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_table_notification : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "rh.Notification",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Query = c.String(maxLength: 256),
                        Email = c.String(maxLength: 256),
                        DeletedDate = c.DateTime(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.DeletedDate);
            
        }
        
        public override void Down()
        {
            DropIndex("rh.Notification", new[] { "DeletedDate" });
            DropTable("rh.Notification");
        }
    }
}
