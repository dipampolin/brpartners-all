namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_createad_data : DbMigration
    {
        public override void Up()
        {
            AddColumn("rh.NotificationFilter", "DeletedDate", c => c.DateTime());
            AddColumn("rh.NotificationFilter", "CreatedDate", c => c.DateTime(nullable: false));
            CreateIndex("rh.NotificationFilter", "DeletedDate");
        }
        
        public override void Down()
        {
            DropIndex("rh.NotificationFilter", new[] { "DeletedDate" });
            DropColumn("rh.NotificationFilter", "CreatedDate");
            DropColumn("rh.NotificationFilter", "DeletedDate");
        }
    }
}
