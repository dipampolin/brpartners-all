namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_contrains_nullable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("rh.EmployeeRelationship", "OdontologyPlanId", c => c.Int(nullable: true));
            AlterColumn("rh.EmployeeRelationship", "HealthPlanId", c => c.Int(nullable: true));
        }
        
        public override void Down()
        {
        }
    }
}
