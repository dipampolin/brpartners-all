namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_last_sent_date_notification_dest : DbMigration
    {
        public override void Up()
        {
            AddColumn("rh.NotificationDestination", "LastSent", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("rh.NotificationDestination", "LastSent");
        }
    }
}
