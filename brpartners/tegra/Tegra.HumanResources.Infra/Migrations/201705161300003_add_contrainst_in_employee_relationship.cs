namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_contrainst_in_employee_relationship : DbMigration
    {
        public override void Up()
        {
            AddColumn("rh.EmployeeRelationship", "HealthPlanId", c => c.Int(nullable: false));
            AddColumn("rh.EmployeeRelationship", "OdontologyPlanId", c => c.Int(nullable: false));
            CreateIndex("rh.EmployeeRelationship", "HealthPlanId");
            CreateIndex("rh.EmployeeRelationship", "OdontologyPlanId");
            AddForeignKey("rh.EmployeeRelationship", "HealthPlanId", "rh.BenefitType", "Id", cascadeDelete: false);
            AddForeignKey("rh.EmployeeRelationship", "OdontologyPlanId", "rh.BenefitType", "Id", cascadeDelete: false);
            DropColumn("rh.EmployeeRelationship", "HealthPlan");
            DropColumn("rh.EmployeeRelationship", "OdontologyPlan");
        }
        
        public override void Down()
        {
            AddColumn("rh.EmployeeRelationship", "OdontologyPlan", c => c.String(maxLength: 256));
            AddColumn("rh.EmployeeRelationship", "HealthPlan", c => c.String(maxLength: 256));
            DropForeignKey("rh.EmployeeRelationship", "OdontologyPlanId", "rh.BenefitType");
            DropForeignKey("rh.EmployeeRelationship", "HealthPlanId", "rh.BenefitType");
            DropIndex("rh.EmployeeRelationship", new[] { "OdontologyPlanId" });
            DropIndex("rh.EmployeeRelationship", new[] { "HealthPlanId" });
            DropColumn("rh.EmployeeRelationship", "OdontologyPlanId");
            DropColumn("rh.EmployeeRelationship", "HealthPlanId");
        }
    }
}
