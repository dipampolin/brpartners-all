namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class change_column_employee : DbMigration
    {
        public override void Up()
        {
            AddColumn("rh.Employee", "FatherId", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("rh.Employee", "FatherId");
        }
    }
}
