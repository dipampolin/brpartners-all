namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_category_opetional_in_benefitytype : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("rh.EmployeeRelationship", "HealthPlanId", "rh.Benefit");
            DropForeignKey("rh.EmployeeRelationship", "OdontologyPlanId", "rh.Benefit");
            DropIndex("rh.EmployeeRelationship", new[] { "HealthPlanId" });
            DropIndex("rh.EmployeeRelationship", new[] { "OdontologyPlanId" });
            AlterColumn("rh.EmployeeRelationship", "HealthPlanId", c => c.Int());
            AlterColumn("rh.EmployeeRelationship", "OdontologyPlanId", c => c.Int());
            CreateIndex("rh.EmployeeRelationship", "HealthPlanId");
            CreateIndex("rh.EmployeeRelationship", "OdontologyPlanId");
            AddForeignKey("rh.EmployeeRelationship", "HealthPlanId", "rh.Benefit", "Id");
            AddForeignKey("rh.EmployeeRelationship", "OdontologyPlanId", "rh.Benefit", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("rh.EmployeeRelationship", "OdontologyPlanId", "rh.Benefit");
            DropForeignKey("rh.EmployeeRelationship", "HealthPlanId", "rh.Benefit");
            DropIndex("rh.EmployeeRelationship", new[] { "OdontologyPlanId" });
            DropIndex("rh.EmployeeRelationship", new[] { "HealthPlanId" });
            AlterColumn("rh.EmployeeRelationship", "OdontologyPlanId", c => c.Int(nullable: false));
            AlterColumn("rh.EmployeeRelationship", "HealthPlanId", c => c.Int(nullable: false));
            CreateIndex("rh.EmployeeRelationship", "OdontologyPlanId");
            CreateIndex("rh.EmployeeRelationship", "HealthPlanId");
            AddForeignKey("rh.EmployeeRelationship", "OdontologyPlanId", "rh.Benefit", "Id", cascadeDelete: true);
            AddForeignKey("rh.EmployeeRelationship", "HealthPlanId", "rh.Benefit", "Id", cascadeDelete: true);
        }
    }
}
