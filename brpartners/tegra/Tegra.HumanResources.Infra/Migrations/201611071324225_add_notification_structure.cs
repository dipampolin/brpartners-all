namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_notification_structure : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "rh.NotificationDestination",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 256),
                        Email = c.String(maxLength: 256),
                        Query = c.String(maxLength: 256),
                        NotificationItemId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("rh.NotificationItem", t => t.NotificationItemId, cascadeDelete: true)
                .Index(t => t.NotificationItemId);
            
            CreateTable(
                "rh.NotificationItem",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Table = c.String(maxLength: 256),
                        Field = c.String(maxLength: 256),
                        Value = c.String(maxLength: 256),
                        NotificationId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("rh.Notification", t => t.NotificationId, cascadeDelete: true)
                .Index(t => t.NotificationId);
            
            AddColumn("rh.Notification", "Name", c => c.String(maxLength: 256));
            AddColumn("rh.Notification", "Frequency", c => c.String(maxLength: 256));
            DropColumn("rh.Notification", "Query");
            DropColumn("rh.Notification", "Email");
        }
        
        public override void Down()
        {
            AddColumn("rh.Notification", "Email", c => c.String(maxLength: 256));
            AddColumn("rh.Notification", "Query", c => c.String(maxLength: 256));
            DropForeignKey("rh.NotificationDestination", "NotificationItemId", "rh.NotificationItem");
            DropForeignKey("rh.NotificationItem", "NotificationId", "rh.Notification");
            DropIndex("rh.NotificationItem", new[] { "NotificationId" });
            DropIndex("rh.NotificationDestination", new[] { "NotificationItemId" });
            DropColumn("rh.Notification", "Frequency");
            DropColumn("rh.Notification", "Name");
            DropTable("rh.NotificationItem");
            DropTable("rh.NotificationDestination");
        }
    }
}
