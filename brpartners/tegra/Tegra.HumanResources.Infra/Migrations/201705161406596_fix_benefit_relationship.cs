namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fix_benefit_relationship : DbMigration
    {
        public override void Up()
        {
            AddForeignKey("rh.EmployeeRelationship", "HealthPlanId", "rh.Benefit", "Id", cascadeDelete: false);
            AddForeignKey("rh.EmployeeRelationship", "OdontologyPlanId", "rh.Benefit", "Id", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("rh.EmployeeRelationship", "OdontologyPlanId", "rh.BenefitType");
            DropForeignKey("rh.EmployeeRelationship", "HealthPlanId", "rh.BenefitType");
        }
    }
}
