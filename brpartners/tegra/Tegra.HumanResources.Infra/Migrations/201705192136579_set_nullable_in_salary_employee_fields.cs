namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class set_nullable_in_salary_employee_fields : DbMigration
    {
        public override void Up()
        {
            AlterColumn("rh.EmployeeSalary", "Salary", c => c.Decimal(precision: 10, scale: 2));
            AlterColumn("rh.EmployeeSalary", "ExtraHour", c => c.Decimal(precision: 10, scale: 2));
            AlterColumn("rh.EmployeeSalary", "Gratification", c => c.Decimal(precision: 10, scale: 2));
            AlterColumn("rh.EmployeeSalary", "Triennium", c => c.Decimal(precision: 10, scale: 2));
            AlterColumn("rh.EmployeeSalary", "StartDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("rh.EmployeeSalary", "StartDate", c => c.DateTime(nullable: false));
            AlterColumn("rh.EmployeeSalary", "Triennium", c => c.Decimal(nullable: false, precision: 10, scale: 2));
            AlterColumn("rh.EmployeeSalary", "Gratification", c => c.Decimal(nullable: false, precision: 10, scale: 2));
            AlterColumn("rh.EmployeeSalary", "ExtraHour", c => c.Decimal(nullable: false, precision: 10, scale: 2));
            AlterColumn("rh.EmployeeSalary", "Salary", c => c.Decimal(nullable: false, precision: 10, scale: 2));
        }


    }
}
