namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update_value_column_vacation : DbMigration
    {
        public override void Up()
        {
            AlterColumn("rh.EmployeeVacation", "Value", c => c.Decimal(nullable: false, precision: 10, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("rh.EmployeeVacation", "Value", c => c.String(maxLength: 256));
        }
    }
}
