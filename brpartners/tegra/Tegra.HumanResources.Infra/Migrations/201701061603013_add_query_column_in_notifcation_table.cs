namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_query_column_in_notifcation_table : DbMigration
    {
        public override void Up()
        {
            AddColumn("rh.Notification", "Query", c => c.String());
            DropColumn("rh.NotificationDestination", "Query");
        }
        
        public override void Down()
        {
            AddColumn("rh.NotificationDestination", "Query", c => c.String());
            DropColumn("rh.Notification", "Query");
        }
    }
}
