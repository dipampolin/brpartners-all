namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class change_tables : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("rh.Employee", "CompanyId", "rh.Company");
            DropIndex("rh.Employee", new[] { "CompanyId" });
            DropColumn("rh.Employee", "CompanyId");
        }
        
        public override void Down()
        {
            AddColumn("rh.Employee", "CompanyId", c => c.Int(nullable: false));
            CreateIndex("rh.Employee", "CompanyId");
            AddForeignKey("rh.Employee", "CompanyId", "rh.Company", "Id");
        }
    }
}
