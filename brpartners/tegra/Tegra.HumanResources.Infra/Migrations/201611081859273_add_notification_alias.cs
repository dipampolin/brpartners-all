namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_notification_alias : DbMigration
    {
        public override void Up()
        {
            AddColumn("rh.NotificationItem", "FieldAlias", c => c.String(maxLength: 256));
            AddColumn("rh.NotificationItem", "TableAlias", c => c.String(maxLength: 256));
        }
        
        public override void Down()
        {
            DropColumn("rh.NotificationItem", "TableAlias");
            DropColumn("rh.NotificationItem", "FieldAlias");
        }
    }
}
