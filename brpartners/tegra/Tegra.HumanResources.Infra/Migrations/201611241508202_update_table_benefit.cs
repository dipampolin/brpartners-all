namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update_table_benefit : DbMigration
    {
        public override void Up()
        {
            AlterColumn("rh.EmployeeBenefit", "EndDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("rh.EmployeeBenefit", "EndDate", c => c.DateTime(nullable: false));
        }
    }
}
