namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update_employee_table : DbMigration
    {
        public override void Up()
        {
            DropIndex("rh.Employee", new[] { "CompanyId" });
            AlterColumn("rh.Employee", "CompanyId", c => c.Int());
            CreateIndex("rh.Employee", "CompanyId");
        }
        
        public override void Down()
        {
            DropIndex("rh.Employee", new[] { "CompanyId" });
            AlterColumn("rh.Employee", "CompanyId", c => c.Int(nullable: false));
            CreateIndex("rh.Employee", "CompanyId");
        }
    }
}
