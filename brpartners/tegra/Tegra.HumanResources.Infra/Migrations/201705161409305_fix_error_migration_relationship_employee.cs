namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fix_error_migration_relationship_employee : DbMigration
    {
        public override void Up()
        {
            Sql("ALTER TABLE [rh].[EmployeeRelationship] DROP CONSTRAINT [FK_rh.EmployeeRelationship_rh.BenefitType_HealthPlanId]");
            Sql("ALTER TABLE [rh].[EmployeeRelationship] DROP CONSTRAINT [FK_rh.EmployeeRelationship_rh.BenefitType_OdontologyPlanId]");
        }
        
        public override void Down()
        {
           
        }
    }
}
