namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class set_fields_nullable_in_employee_vehicle : DbMigration
    {
        public override void Up()
        {
            AlterColumn("rh.EmployeeVehicle", "Brand", c => c.String(maxLength: 50));
            AlterColumn("rh.EmployeeVehicle", "Model", c => c.String(maxLength: 200));
            AlterColumn("rh.EmployeeVehicle", "Color", c => c.String(maxLength: 60));
            AlterColumn("rh.EmployeeVehicle", "CarPlate", c => c.String(maxLength: 14));
        }
        
        public override void Down()
        {
            AlterColumn("rh.EmployeeVehicle", "CarPlate", c => c.String(nullable: false, maxLength: 14));
            AlterColumn("rh.EmployeeVehicle", "Color", c => c.String(nullable: false, maxLength: 60));
            AlterColumn("rh.EmployeeVehicle", "Model", c => c.String(nullable: false, maxLength: 200));
            AlterColumn("rh.EmployeeVehicle", "Brand", c => c.String(nullable: false, maxLength: 50));
        }
    }
}
