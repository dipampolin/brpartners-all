namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class change_Employee_table : DbMigration
    {
        public override void Up()
        {
            AddColumn("rh.Employee", "Foreigner", c => c.Boolean(nullable: false));
            AddColumn("rh.Employee", "Nacionality", c => c.String(maxLength: 100));
            AddColumn("rh.Employee", "MotherName", c => c.String(maxLength: 200));
            AddColumn("rh.Employee", "FatherName", c => c.String(maxLength: 200));
            AddColumn("rh.Employee", "MainPhone", c => c.String(maxLength: 20));
            AddColumn("rh.Employee", "Rg", c => c.String(maxLength: 20));
            AddColumn("rh.Employee", "RgDispatcher", c => c.String(maxLength: 20));
            AddColumn("rh.Employee", "RgDateExpedition", c => c.DateTime());
            AddColumn("rh.Employee", "Rne", c => c.String(maxLength: 20));
            AddColumn("rh.Employee", "RneDispatcher", c => c.String(maxLength: 20));
            AddColumn("rh.Employee", "RneDateExpedition", c => c.DateTime());
            AddColumn("rh.Employee", "RneDateExpiration", c => c.DateTime());
            AddColumn("rh.Employee", "VoteNumberCard", c => c.String(maxLength: 20));
            AddColumn("rh.Employee", "VoterSession", c => c.String(maxLength: 20));
            AddColumn("rh.Employee", "VoterZone", c => c.String(maxLength: 20));
            AddColumn("rh.Employee", "EmployeementBooklet", c => c.String(maxLength: 20));
            AddColumn("rh.Employee", "Pis", c => c.String(maxLength: 20));
            AddColumn("rh.Employee", "Passaport", c => c.String(maxLength: 20));
            AddColumn("rh.Employee", "Reservist", c => c.String(maxLength: 20));
            AddColumn("rh.Employee", "Active", c => c.Boolean(nullable: false));
            AddColumn("rh.Employee", "ResignationDate", c => c.DateTime());
            AddColumn("rh.Employee", "Registration", c => c.String(maxLength: 45));
            AddColumn("rh.Employee", "IdentificationNumber", c => c.String(maxLength: 45));
            AddColumn("rh.CourseEmployee", "Completed", c => c.Boolean());
            AlterColumn("rh.Employee", "Code", c => c.String(nullable: false, maxLength: 45));
            AlterColumn("rh.Employee", "Cpf", c => c.String(maxLength: 20));
            AlterColumn("rh.Employee", "Gender", c => c.String(nullable: false, maxLength: 20));
        }
        
        public override void Down()
        {
            AlterColumn("rh.Employee", "Gender", c => c.String(nullable: false, maxLength: 1));
            AlterColumn("rh.Employee", "Cpf", c => c.String(maxLength: 11));
            AlterColumn("rh.Employee", "Code", c => c.Int(nullable: false));
            DropColumn("rh.CourseEmployee", "Completed");
            DropColumn("rh.Employee", "IdentificationNumber");
            DropColumn("rh.Employee", "Registration");
            DropColumn("rh.Employee", "ResignationDate");
            DropColumn("rh.Employee", "Active");
            DropColumn("rh.Employee", "Reservist");
            DropColumn("rh.Employee", "Passaport");
            DropColumn("rh.Employee", "Pis");
            DropColumn("rh.Employee", "EmployeementBooklet");
            DropColumn("rh.Employee", "VoterZone");
            DropColumn("rh.Employee", "VoterSession");
            DropColumn("rh.Employee", "VoteNumberCard");
            DropColumn("rh.Employee", "RneDateExpiration");
            DropColumn("rh.Employee", "RneDateExpedition");
            DropColumn("rh.Employee", "RneDispatcher");
            DropColumn("rh.Employee", "Rne");
            DropColumn("rh.Employee", "RgDateExpedition");
            DropColumn("rh.Employee", "RgDispatcher");
            DropColumn("rh.Employee", "Rg");
            DropColumn("rh.Employee", "MainPhone");
            DropColumn("rh.Employee", "FatherName");
            DropColumn("rh.Employee", "MotherName");
            DropColumn("rh.Employee", "Nacionality");
            DropColumn("rh.Employee", "Foreigner");
        }
    }
}
