namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_column_picture_in_employee_table : DbMigration
    {
        public override void Up()
        {
            AddColumn("rh.Employee", "Picture", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("rh.Employee", "Picture");
        }
    }
}
