namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class change_table_employeeDepartment_to_costCenterDepartment : DbMigration
    {
        public override void Up()
        {
            Sql("DELETE FROM rh.EmployeeDepartment");
            DropForeignKey("rh.EmployeeDepartment", "DepartmentId", "rh.Department");
            RenameTable(name: "rh.EmployeeDepartment", newName: "EmployeeCostCenter");
            DropForeignKey("rh.CourseEmployee", "EmployeeId", "rh.Employee");
            DropIndex("rh.EmployeeCostCenter", new[] { "DepartmentId" });
            AddColumn("rh.EmployeeCostCenter", "CostCenterId", c => c.Int(nullable: false));
            CreateIndex("rh.EmployeeCostCenter", "CostCenterId");
            AddForeignKey("rh.EmployeeCostCenter", "CostCenterId", "rh.CostCenter", "Id", cascadeDelete: true);
            AddForeignKey("rh.CourseEmployee", "EmployeeId", "rh.Employee", "Id", cascadeDelete: true);
            DropColumn("rh.EmployeeCostCenter", "DepartmentId");
        }
        
        public override void Down()
        {
            Sql("DELETE FROM rh.EmployeeDepartment");
            AddColumn("rh.EmployeeCostCenter", "DepartmentId", c => c.Int(nullable: false));
            DropForeignKey("rh.CourseEmployee", "EmployeeId", "rh.Employee");
            DropForeignKey("rh.EmployeeCostCenter", "CostCenterId", "rh.CostCenter");
            DropIndex("rh.EmployeeCostCenter", new[] { "CostCenterId" });
            DropColumn("rh.EmployeeCostCenter", "CostCenterId");
            CreateIndex("rh.EmployeeCostCenter", "DepartmentId");
            AddForeignKey("rh.CourseEmployee", "EmployeeId", "rh.Employee", "Id");
            AddForeignKey("rh.EmployeeDepartment", "DepartmentId", "rh.Department", "Id", cascadeDelete: true);
            RenameTable(name: "rh.EmployeeCostCenter", newName: "EmployeeDepartment");
        }
    }
}
