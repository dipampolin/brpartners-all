namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class remove_repartment_table : DbMigration
    {
        public override void Up()
        {
            DropIndex("rh.Department", new[] { "DeletedDate" });
            DropTable("rh.Department");
        }
        
        public override void Down()
        {
            CreateTable(
                "rh.Department",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 256),
                        DeletedDate = c.DateTime(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("rh.Department", "DeletedDate");
        }
    }
}
