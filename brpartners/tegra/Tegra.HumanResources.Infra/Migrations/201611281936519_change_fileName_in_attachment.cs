namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class change_fileName_in_attachment : DbMigration
    {
        public override void Up()
        {
            AddColumn("rh.EmployeeAttachment", "FileContent", c => c.Binary());
            DropColumn("rh.EmployeeAttachment", "File");
        }
        
        public override void Down()
        {
            AddColumn("rh.EmployeeAttachment", "File", c => c.Binary());
            DropColumn("rh.EmployeeAttachment", "FileContent");
        }
    }
}
