namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class change_supervisor_to_not_null : DbMigration
    {
        public override void Up()
        {
            DropIndex("rh.Employee", new[] { "SupervisorId" });
            AlterColumn("rh.Employee", "SupervisorId", c => c.Int());
            CreateIndex("rh.Employee", "SupervisorId");
        }
        
        public override void Down()
        {
            DropIndex("rh.Employee", new[] { "SupervisorId" });
            AlterColumn("rh.Employee", "SupervisorId", c => c.Int(nullable: false));
            CreateIndex("rh.Employee", "SupervisorId");
        }
    }
}
