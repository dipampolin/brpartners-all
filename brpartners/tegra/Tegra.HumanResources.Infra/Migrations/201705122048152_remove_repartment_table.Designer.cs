// <auto-generated />
namespace Tegra.HumanResources.Infra.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class remove_repartment_table : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(remove_repartment_table));
        
        string IMigrationMetadata.Id
        {
            get { return "201705122048152_remove_repartment_table"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
