namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fix_table_notification_filter : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("rh.NotificationFilter", "Notification_Id", "rh.Notification");
            DropIndex("rh.NotificationFilter", new[] { "Notification_Id" });
            RenameColumn(table: "rh.NotificationFilter", name: "Notification_Id", newName: "NotificationId");
            AlterColumn("rh.NotificationFilter", "NotificationId", c => c.Int(nullable: false));
            CreateIndex("rh.NotificationFilter", "NotificationId");
            AddForeignKey("rh.NotificationFilter", "NotificationId", "rh.Notification", "Id", cascadeDelete: true);
            DropColumn("rh.NotificationFilter", "ReportId");
        }
        
        public override void Down()
        {
            AddColumn("rh.NotificationFilter", "ReportId", c => c.Int(nullable: false));
            DropForeignKey("rh.NotificationFilter", "NotificationId", "rh.Notification");
            DropIndex("rh.NotificationFilter", new[] { "NotificationId" });
            AlterColumn("rh.NotificationFilter", "NotificationId", c => c.Int());
            RenameColumn(table: "rh.NotificationFilter", name: "NotificationId", newName: "Notification_Id");
            CreateIndex("rh.NotificationFilter", "Notification_Id");
            AddForeignKey("rh.NotificationFilter", "Notification_Id", "rh.Notification", "Id");
        }
    }
}
