namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_category_nullable_IN_benefity_type : DbMigration
    {
        public override void Up()
        {
            AlterColumn("rh.BenefitType", "Category", c => c.Int(nullable: true));
        }
        
        public override void Down()
        {
        }
    }
}
