namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class change_all_tables : DbMigration
    {
        public override void Up()
        {
            AlterColumn("rh.EmployeeCompany", "EndDate", c => c.DateTime());
            AlterColumn("rh.EmployeeContract", "EndDate", c => c.DateTime());
            AlterColumn("rh.EmployeeDepartment", "EndDate", c => c.DateTime());
            AlterColumn("rh.EmployeeRelationship", "EndDate", c => c.DateTime());
            AlterColumn("rh.EmployeeRole", "EndDate", c => c.DateTime());
            AlterColumn("rh.EmployeeSalary", "EndDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("rh.EmployeeSalary", "EndDate", c => c.DateTime(nullable: false));
            AlterColumn("rh.EmployeeRole", "EndDate", c => c.DateTime(nullable: false));
            AlterColumn("rh.EmployeeRelationship", "EndDate", c => c.DateTime(nullable: false));
            AlterColumn("rh.EmployeeDepartment", "EndDate", c => c.DateTime(nullable: false));
            AlterColumn("rh.EmployeeContract", "EndDate", c => c.DateTime(nullable: false));
            AlterColumn("rh.EmployeeCompany", "EndDate", c => c.DateTime(nullable: false));
        }
    }
}
