namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update_report : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "rh.ReportFilter",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Field = c.String(maxLength: 256),
                        FieldAlias = c.String(maxLength: 256),
                        Table = c.String(maxLength: 256),
                        TableAlias = c.String(maxLength: 256),
                        Value = c.String(maxLength: 256),
                        ReportId = c.Int(nullable: false),
                        Type = c.String(maxLength: 256),
                        DeletedDate = c.DateTime(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("rh.Report", t => t.ReportId, cascadeDelete: true)
                .Index(t => t.ReportId)
                .Index(t => t.DeletedDate);
            
        }
        
        public override void Down()
        {
            DropForeignKey("rh.ReportFilter", "ReportId", "rh.Report");
            DropIndex("rh.ReportFilter", new[] { "DeletedDate" });
            DropIndex("rh.ReportFilter", new[] { "ReportId" });
            DropTable("rh.ReportFilter");
        }
    }
}
