namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_active_column : DbMigration
    {
        public override void Up()
        {
            AddColumn("rh.Notification", "Active", c => c.Boolean(nullable: false));
            AlterColumn("rh.Report", "Active", c => c.Boolean());
        }
        
        public override void Down()
        {
            AlterColumn("rh.Report", "Active", c => c.Boolean(nullable: false));
            DropColumn("rh.Notification", "Active");
        }
    }
}
