namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeFieldNameFromCodeToInitials : DbMigration
    {
        public override void Up()
        {
            RenameColumn("rh.Employee", "Code", "Initials");
        }
        
        public override void Down()
        {
            RenameColumn("rh.Employee", "Code", "Initials");
        }
    }
}
