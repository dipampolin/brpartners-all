namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class change_employee_table_columns : DbMigration
    {
        public override void Up()
        {
            AddColumn("rh.Employee", "Nationality", c => c.String(maxLength: 100));
            AddColumn("rh.Employee", "VoterNumberCard", c => c.String(maxLength: 20));
            AddColumn("rh.Employee", "EmploymentBooklet", c => c.String(maxLength: 20));
            AddColumn("rh.Employee", "Passport", c => c.String(maxLength: 20));
            DropColumn("rh.Employee", "Nacionality");
            DropColumn("rh.Employee", "VoteNumberCard");
            DropColumn("rh.Employee", "EmployeementBooklet");
            DropColumn("rh.Employee", "Passaport");
        }
        
        public override void Down()
        {
            AddColumn("rh.Employee", "Passaport", c => c.String(maxLength: 20));
            AddColumn("rh.Employee", "EmployeementBooklet", c => c.String(maxLength: 20));
            AddColumn("rh.Employee", "VoteNumberCard", c => c.String(maxLength: 20));
            AddColumn("rh.Employee", "Nacionality", c => c.String(maxLength: 100));
            DropColumn("rh.Employee", "Passport");
            DropColumn("rh.Employee", "EmploymentBooklet");
            DropColumn("rh.Employee", "VoterNumberCard");
            DropColumn("rh.Employee", "Nationality");
        }
    }
}
