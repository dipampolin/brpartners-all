namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class change_table_column : DbMigration
    {
        public override void Up()
        {
            AddColumn("rh.Employee", "EmploymentBookletUf", c => c.String(maxLength: 256));
        }
        
        public override void Down()
        {
            DropColumn("rh.Employee", "EmploymentBookletUf");
        }
    }
}
