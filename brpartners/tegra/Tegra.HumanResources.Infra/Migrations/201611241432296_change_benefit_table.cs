namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class change_benefit_table : DbMigration
    {
        public override void Up()
        {
            AddColumn("rh.Benefit", "Description", c => c.String(nullable: false, maxLength: 100));
            DropColumn("rh.Benefit", "PlanOperator");
            DropColumn("rh.Benefit", "Plan");
        }
        
        public override void Down()
        {
            AddColumn("rh.Benefit", "Plan", c => c.String(maxLength: 100));
            AddColumn("rh.Benefit", "PlanOperator", c => c.String(maxLength: 100));
            DropColumn("rh.Benefit", "Description");
        }
    }
}
