namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modify_notification_table : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("rh.NotificationDestination", "NotificationItemId", "rh.NotificationItem");
            DropIndex("rh.NotificationDestination", new[] { "NotificationItemId" });
            AddColumn("rh.NotificationDestination", "NotificationId", c => c.Int(nullable: false));
            CreateIndex("rh.NotificationDestination", "NotificationId");
            AddForeignKey("rh.NotificationDestination", "NotificationId", "rh.Notification", "Id", cascadeDelete: true);
            DropColumn("rh.NotificationDestination", "NotificationItemId");
        }
        
        public override void Down()
        {
            AddColumn("rh.NotificationDestination", "NotificationItemId", c => c.Int(nullable: false));
            DropForeignKey("rh.NotificationDestination", "NotificationId", "rh.Notification");
            DropIndex("rh.NotificationDestination", new[] { "NotificationId" });
            DropColumn("rh.NotificationDestination", "NotificationId");
            CreateIndex("rh.NotificationDestination", "NotificationItemId");
            AddForeignKey("rh.NotificationDestination", "NotificationItemId", "rh.NotificationItem", "Id", cascadeDelete: true);
        }
    }
}
