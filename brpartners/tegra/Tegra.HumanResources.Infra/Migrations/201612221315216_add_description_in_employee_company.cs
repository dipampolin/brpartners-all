namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_description_in_employee_company : DbMigration
    {
        public override void Up()
        {
            AlterColumn("rh.Benefit", "Description", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("rh.Benefit", "Description", c => c.String(nullable: false, maxLength: 100));
        }
    }
}
