namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update_employee_course_entity : DbMigration
    {
        public override void Up()
        {
            DropIndex("rh.CourseEmployee", new[] { "CourseId" });
            DropPrimaryKey("rh.CourseEmployee");
            AddColumn("rh.CourseEmployee", "Id", c => c.Int(nullable: false, identity: true));
            AlterColumn("rh.CourseEmployee", "CourseId", c => c.Int());
            AddPrimaryKey("rh.CourseEmployee", "Id");
            CreateIndex("rh.CourseEmployee", "CourseId");
        }
        
        public override void Down()
        {
            DropIndex("rh.CourseEmployee", new[] { "CourseId" });
            DropPrimaryKey("rh.CourseEmployee");
            AlterColumn("rh.CourseEmployee", "CourseId", c => c.Int(nullable: false));
            DropColumn("rh.CourseEmployee", "Id");
            AddPrimaryKey("rh.CourseEmployee", new[] { "CourseId", "EmployeeId" });
            CreateIndex("rh.CourseEmployee", "CourseId");
        }
    }
}
