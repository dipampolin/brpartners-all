namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_benefity_category_column : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("rh.EmployeeVariables", "EmployeeVariablesTypeId", "rh.EmployeeVariablesType");
            DropIndex("rh.EmployeeVariables", new[] { "EmployeeVariablesTypeId" });
            AddColumn("rh.BenefitType", "Category", c => c.Int(nullable: false));
            AlterColumn("rh.EmployeeVariables", "EmployeeVariablesTypeId", c => c.Int());
            CreateIndex("rh.EmployeeVariables", "EmployeeVariablesTypeId");
            AddForeignKey("rh.EmployeeVariables", "EmployeeVariablesTypeId", "rh.EmployeeVariablesType", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("rh.EmployeeVariables", "EmployeeVariablesTypeId", "rh.EmployeeVariablesType");
            DropIndex("rh.EmployeeVariables", new[] { "EmployeeVariablesTypeId" });
            AlterColumn("rh.EmployeeVariables", "EmployeeVariablesTypeId", c => c.Int(nullable: false));
            DropColumn("rh.BenefitType", "Category");
            CreateIndex("rh.EmployeeVariables", "EmployeeVariablesTypeId");
            AddForeignKey("rh.EmployeeVariables", "EmployeeVariablesTypeId", "rh.EmployeeVariablesType", "Id", cascadeDelete: true);
        }
    }
}
