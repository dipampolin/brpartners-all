namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class remove_value_in_reportItem : DbMigration
    {
        public override void Up()
        {
            DropColumn("rh.ReportItem", "Value");
        }
        
        public override void Down()
        {
            AddColumn("rh.ReportItem", "Value", c => c.String(maxLength: 100));
        }
    }
}
