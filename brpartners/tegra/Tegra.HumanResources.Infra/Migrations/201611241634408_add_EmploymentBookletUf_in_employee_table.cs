namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_EmploymentBookletUf_in_employee_table : DbMigration
    {
        public override void Up()
        {
            AddColumn("rh.Employee", "EmploymentBookletSerie", c => c.String(maxLength: 20));
        }
        
        public override void Down()
        {
            DropColumn("rh.Employee", "EmploymentBookletSerie");
        }
    }
}
