// <auto-generated />
namespace Tegra.HumanResources.Infra.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class adjusts_date : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(adjusts_date));
        
        string IMigrationMetadata.Id
        {
            get { return "201701062012407_adjusts_date"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
