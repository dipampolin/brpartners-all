namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class adjusts_date : DbMigration
    {
        public override void Up()
        {
            AlterColumn("rh.NotificationDestination", "LastSent", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("rh.NotificationDestination", "LastSent", c => c.DateTime(nullable: false));
        }
    }
}
