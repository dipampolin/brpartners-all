namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_table_notification_filter : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "rh.NotificationFilter",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Field = c.String(maxLength: 256),
                        FieldAlias = c.String(maxLength: 256),
                        Table = c.String(maxLength: 256),
                        TableAlias = c.String(maxLength: 256),
                        Value = c.String(maxLength: 256),
                        ReportId = c.Int(nullable: false),
                        Type = c.String(maxLength: 256),
                        Notification_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("rh.Notification", t => t.Notification_Id)
                .Index(t => t.Notification_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("rh.NotificationFilter", "Notification_Id", "rh.Notification");
            DropIndex("rh.NotificationFilter", new[] { "Notification_Id" });
            DropTable("rh.NotificationFilter");
        }
    }
}
