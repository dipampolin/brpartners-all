namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class change_keyss : DbMigration
    {
        public override void Up()
        {
            AddColumn("rh.Employee", "SupervisorId", c => c.Int(nullable: false));
            CreateIndex("rh.Employee", "SupervisorId");
            AddForeignKey("rh.Employee", "SupervisorId", "rh.Employee", "Id");
            DropColumn("rh.Employee", "FatherId");
        }
        
        public override void Down()
        {
            AddColumn("rh.Employee", "FatherId", c => c.Int());
            DropForeignKey("rh.Employee", "SupervisorId", "rh.Employee");
            DropIndex("rh.Employee", new[] { "SupervisorId" });
            DropColumn("rh.Employee", "SupervisorId");
        }
    }
}
