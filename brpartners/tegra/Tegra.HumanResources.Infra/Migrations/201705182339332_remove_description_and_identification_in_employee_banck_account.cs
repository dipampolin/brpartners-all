namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class remove_description_and_identification_in_employee_banck_account : DbMigration
    {
        public override void Up()
        {
            DropColumn("rh.EmployeeBankAccount", "Identification");
            DropColumn("rh.EmployeeBankAccount", "Description");
        }
        
        public override void Down()
        {
            AddColumn("rh.EmployeeBankAccount", "Description", c => c.String(maxLength: 256));
            AddColumn("rh.EmployeeBankAccount", "Identification", c => c.String(maxLength: 256));
        }
    }
}
