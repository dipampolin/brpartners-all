namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_table_type_to_employee_variables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "rh.EmployeeVariablesType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 100),
                        DeletedDate = c.DateTime(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.DeletedDate);
            
            AddColumn("rh.EmployeeVariables", "EmployeeVariablesTypeId", c => c.Int(nullable: true));
            CreateIndex("rh.EmployeeVariables", "EmployeeVariablesTypeId");
            AddForeignKey("rh.EmployeeVariables", "EmployeeVariablesTypeId", "rh.EmployeeVariablesType", "Id", cascadeDelete: false);
            DropColumn("rh.EmployeeVariables", "Type");
        }
        
        public override void Down()
        {
            AddColumn("rh.EmployeeVariables", "Type", c => c.String(maxLength: 100));
            DropForeignKey("rh.EmployeeVariables", "EmployeeVariablesTypeId", "rh.EmployeeVariablesType");
            DropIndex("rh.EmployeeVariablesType", new[] { "DeletedDate" });
            DropIndex("rh.EmployeeVariables", new[] { "EmployeeVariablesTypeId" });
            DropColumn("rh.EmployeeVariables", "EmployeeVariablesTypeId");
            DropTable("rh.EmployeeVariablesType");
        }
    }
}
