namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_benefit_type_table : DbMigration
    {
        public override void Up()
        {

            

            CreateTable(
                "rh.BenefitType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 256),
                        DeletedDate = c.DateTime(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.DeletedDate);

            Sql("delete from rh.Benefit");
            AddColumn("rh.Benefit", "BenefitTypeId", c => c.Int(nullable: false));
            CreateIndex("rh.Benefit", "BenefitTypeId");
            AddForeignKey("rh.Benefit", "BenefitTypeId", "rh.BenefitType", "Id");
            DropColumn("rh.Benefit", "Type");
        }
        
        public override void Down()
        {
            AddColumn("rh.Benefit", "Type", c => c.String(nullable: false, maxLength: 100));
            DropForeignKey("rh.Benefit", "BenefitTypeId", "rh.BenefitType");
            DropIndex("rh.BenefitType", new[] { "DeletedDate" });
            DropIndex("rh.Benefit", new[] { "BenefitTypeId" });
            DropColumn("rh.Benefit", "BenefitTypeId");
            DropTable("rh.BenefitType");
        }
    }
}
