namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_value_in_employee_course : DbMigration
    {
        public override void Up()
        {
            AddColumn("rh.CourseEmployee", "Value", c => c.Decimal(precision: 10, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("rh.CourseEmployee", "Value");
        }
    }
}
