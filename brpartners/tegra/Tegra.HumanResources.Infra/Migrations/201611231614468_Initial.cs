namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "rh.EmployeeAddress",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.String(nullable: false, maxLength: 25),
                        Street = c.String(nullable: false, maxLength: 100),
                        CEP = c.Int(nullable: false),
                        Number = c.String(nullable: false, maxLength: 8),
                        Complement = c.String(maxLength: 100),
                        Neighborhood = c.String(maxLength: 50),
                        Active = c.Boolean(nullable: false),
                        EmployeeId = c.Int(nullable: false),
                        CityId = c.Int(nullable: false),
                        DeletedDate = c.DateTime(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("rh.City", t => t.CityId, cascadeDelete: true)
                .ForeignKey("rh.Employee", t => t.EmployeeId, cascadeDelete: true)
                .Index(t => t.EmployeeId)
                .Index(t => t.CityId)
                .Index(t => t.DeletedDate);
            
            CreateTable(
                "rh.City",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        StateId = c.Int(nullable: false),
                        DeletedDate = c.DateTime(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("rh.State", t => t.StateId)
                .Index(t => t.StateId)
                .Index(t => t.DeletedDate);
            
            CreateTable(
                "rh.State",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UF = c.String(nullable: false, maxLength: 2),
                        Name = c.String(nullable: false, maxLength: 100),
                        DeletedDate = c.DateTime(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.DeletedDate);
            
            CreateTable(
                "rh.Employee",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 150),
                        Cpf = c.String(maxLength: 11),
                        Gender = c.String(nullable: false, maxLength: 1),
                        Birthdate = c.DateTime(nullable: false),
                        AdmissionDate = c.DateTime(),
                        CompanyId = c.Int(nullable: false),
                        CivilStatusId = c.Int(),
                        EducationalStageId = c.Int(),
                        CostCenterId = c.Int(),
                        DeletedDate = c.DateTime(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("rh.CivilStatus", t => t.CivilStatusId)
                .ForeignKey("rh.Company", t => t.CompanyId)
                .ForeignKey("rh.CostCenter", t => t.CostCenterId)
                .ForeignKey("rh.EducationalStage", t => t.EducationalStageId)
                .Index(t => t.CompanyId)
                .Index(t => t.CivilStatusId)
                .Index(t => t.EducationalStageId)
                .Index(t => t.CostCenterId)
                .Index(t => t.DeletedDate);
            
            CreateTable(
                "rh.CivilStatus",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Value = c.String(nullable: false, maxLength: 30),
                        DeletedDate = c.DateTime(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.DeletedDate);
            
            CreateTable(
                "rh.Company",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 20),
                        Name = c.String(nullable: false, maxLength: 100),
                        Cnpj = c.String(nullable: false, maxLength: 20),
                        DeletedDate = c.DateTime(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.DeletedDate);
            
            CreateTable(
                "rh.CostCenter",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 20),
                        Name = c.String(nullable: false, maxLength: 100),
                        DeletedDate = c.DateTime(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.DeletedDate);
            
            CreateTable(
                "rh.CourseEmployee",
                c => new
                    {
                        CourseId = c.Int(nullable: false),
                        EmployeeId = c.Int(nullable: false),
                        Deadline = c.DateTime(),
                        Required = c.Boolean(),
                        DeletedDate = c.DateTime(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => new { t.CourseId, t.EmployeeId })
                .ForeignKey("rh.Course", t => t.CourseId)
                .ForeignKey("rh.Employee", t => t.EmployeeId)
                .Index(t => t.CourseId)
                .Index(t => t.EmployeeId)
                .Index(t => t.DeletedDate);
            
            CreateTable(
                "rh.Course",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Active = c.Boolean(nullable: false),
                        DeletedDate = c.DateTime(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.DeletedDate);
            
            CreateTable(
                "rh.EducationalStage",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Value = c.String(nullable: false, maxLength: 30),
                        DeletedDate = c.DateTime(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.DeletedDate);
            
            CreateTable(
                "rh.Payslip",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        CompanyCode = c.Int(nullable: false),
                        CompanyName = c.String(nullable: false, maxLength: 255),
                        CostCenterCode = c.Int(nullable: false),
                        CostCenterDescription = c.String(nullable: false, maxLength: 255),
                        RoleCode = c.Int(nullable: false),
                        RoleDescription = c.String(nullable: false, maxLength: 100),
                        EmployeeId = c.Int(nullable: false),
                        CompanyId = c.Int(nullable: false),
                        CostCenterId = c.Int(nullable: false),
                        DeletedDate = c.DateTime(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("rh.Company", t => t.CompanyId, cascadeDelete: true)
                .ForeignKey("rh.CostCenter", t => t.CostCenterId)
                .ForeignKey("rh.Employee", t => t.EmployeeId)
                .Index(t => new { t.EmployeeId, t.Date, t.DeletedDate }, unique: true, name: "un_payslip_date")
                .Index(t => t.CompanyId)
                .Index(t => t.CostCenterId);
            
            CreateTable(
                "rh.PayslipItem",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Rubric = c.Int(nullable: false),
                        RubricDescription = c.String(nullable: false, maxLength: 255),
                        RubricType = c.String(nullable: false, maxLength: 1),
                        Reference = c.Decimal(nullable: false, precision: 10, scale: 2),
                        Value = c.Decimal(nullable: false, precision: 10, scale: 2),
                        PayslipId = c.Int(nullable: false),
                        DeletedDate = c.DateTime(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("rh.Payslip", t => t.PayslipId, cascadeDelete: true)
                .Index(t => t.PayslipId)
                .Index(t => t.DeletedDate);
            
            CreateTable(
                "rh.EmployeeVehicle",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Brand = c.String(nullable: false, maxLength: 25),
                        Model = c.String(nullable: false, maxLength: 100),
                        Color = c.String(nullable: false, maxLength: 30),
                        CarPlate = c.String(nullable: false, maxLength: 7),
                        Active = c.Boolean(nullable: false),
                        EmployeeId = c.Int(nullable: false),
                        DeletedDate = c.DateTime(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("rh.Employee", t => t.EmployeeId, cascadeDelete: true)
                .Index(t => t.EmployeeId)
                .Index(t => t.DeletedDate);
            
            CreateTable(
                "rh.Benefit",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.String(nullable: false, maxLength: 100),
                        PlanOperator = c.String(maxLength: 100),
                        Plan = c.String(maxLength: 100),
                        Value = c.Decimal(precision: 10, scale: 2),
                        DeletedDate = c.DateTime(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.DeletedDate);
            
            CreateTable(
                "rh.Department",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 256),
                        DeletedDate = c.DateTime(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.DeletedDate);
            
            CreateTable(
                "rh.EmployeeAttachment",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(maxLength: 256),
                        File = c.Binary(),
                        FileName = c.String(maxLength: 256),
                        EmployeeId = c.Int(nullable: false),
                        DeletedDate = c.DateTime(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("rh.Employee", t => t.EmployeeId, cascadeDelete: true)
                .Index(t => t.EmployeeId)
                .Index(t => t.DeletedDate);
            
            CreateTable(
                "rh.EmployeeBankAccount",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Main = c.Boolean(nullable: false),
                        Identification = c.String(maxLength: 256),
                        Description = c.String(maxLength: 256),
                        Bank = c.String(maxLength: 256),
                        Agency = c.String(maxLength: 256),
                        Account = c.String(maxLength: 256),
                        EmployeeId = c.Int(nullable: false),
                        DeletedDate = c.DateTime(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("rh.Employee", t => t.EmployeeId, cascadeDelete: true)
                .Index(t => t.EmployeeId)
                .Index(t => t.DeletedDate);
            
            CreateTable(
                "rh.EmployeeBenefit",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BenefitId = c.Int(nullable: false),
                        EmployeeId = c.Int(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        DeletedDate = c.DateTime(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("rh.Benefit", t => t.BenefitId, cascadeDelete: true)
                .ForeignKey("rh.Employee", t => t.EmployeeId, cascadeDelete: true)
                .Index(t => t.BenefitId)
                .Index(t => t.EmployeeId)
                .Index(t => t.DeletedDate);
            
            CreateTable(
                "rh.EmployeeCompany",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        CompanyId = c.Int(nullable: false),
                        EmployeeId = c.Int(nullable: false),
                        DeletedDate = c.DateTime(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("rh.Company", t => t.CompanyId, cascadeDelete: true)
                .ForeignKey("rh.Employee", t => t.EmployeeId, cascadeDelete: true)
                .Index(t => t.CompanyId)
                .Index(t => t.EmployeeId)
                .Index(t => t.DeletedDate);
            
            CreateTable(
                "rh.EmployeeContact",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.String(maxLength: 256),
                        Value = c.String(maxLength: 256),
                        Active = c.Boolean(nullable: false),
                        EmployeeId = c.Int(nullable: false),
                        DeletedDate = c.DateTime(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("rh.Employee", t => t.EmployeeId, cascadeDelete: true)
                .Index(t => t.EmployeeId)
                .Index(t => t.DeletedDate);
            
            CreateTable(
                "rh.EmployeeContract",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(maxLength: 256),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        Approved = c.Boolean(nullable: false),
                        EmployeeId = c.Int(nullable: false),
                        DeletedDate = c.DateTime(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("rh.Employee", t => t.EmployeeId, cascadeDelete: true)
                .Index(t => t.EmployeeId)
                .Index(t => t.DeletedDate);
            
            CreateTable(
                "rh.EmployeeDepartment",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        EmployeeId = c.Int(nullable: false),
                        DepartmentId = c.Int(nullable: false),
                        DeletedDate = c.DateTime(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("rh.Department", t => t.DepartmentId, cascadeDelete: true)
                .ForeignKey("rh.Employee", t => t.EmployeeId, cascadeDelete: true)
                .Index(t => t.EmployeeId)
                .Index(t => t.DepartmentId)
                .Index(t => t.DeletedDate);
            
            CreateTable(
                "rh.EmployeeRelationship",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Relationship = c.String(maxLength: 256),
                        Name = c.String(maxLength: 256),
                        Cpf = c.String(maxLength: 256),
                        UseHealthPlan = c.Boolean(nullable: false),
                        HealthPlan = c.String(maxLength: 256),
                        UseOdontologyPlan = c.Boolean(nullable: false),
                        OdontologyPlan = c.String(maxLength: 256),
                        IR = c.Boolean(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        EmployeeId = c.Int(nullable: false),
                        DeletedDate = c.DateTime(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("rh.Employee", t => t.EmployeeId, cascadeDelete: true)
                .Index(t => t.EmployeeId)
                .Index(t => t.DeletedDate);
            
            CreateTable(
                "rh.EmployeeRole",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        EmployeeId = c.Int(nullable: false),
                        RoleId = c.Int(nullable: false),
                        DeletedDate = c.DateTime(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("rh.Employee", t => t.EmployeeId, cascadeDelete: true)
                .ForeignKey("rh.Role", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.EmployeeId)
                .Index(t => t.RoleId)
                .Index(t => t.DeletedDate);
            
            CreateTable(
                "rh.Role",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 256),
                        DeletedDate = c.DateTime(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.DeletedDate);
            
            CreateTable(
                "rh.EmployeeSalary",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(maxLength: 256),
                        Category = c.String(maxLength: 256),
                        SalaryPeriod = c.String(maxLength: 256),
                        WorkLoad = c.String(maxLength: 256),
                        Salary = c.Decimal(nullable: false, precision: 10, scale: 2),
                        ExtraHour = c.Decimal(nullable: false, precision: 10, scale: 2),
                        Gratification = c.Decimal(nullable: false, precision: 10, scale: 2),
                        Triennium = c.Decimal(nullable: false, precision: 10, scale: 2),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        EmployeeId = c.Int(nullable: false),
                        DeletedDate = c.DateTime(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("rh.Employee", t => t.EmployeeId, cascadeDelete: true)
                .Index(t => t.EmployeeId)
                .Index(t => t.DeletedDate);
            
            CreateTable(
                "rh.EmployeeVacation",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AquisitionStartDate = c.DateTime(nullable: false),
                        AquisitionEndDate = c.DateTime(nullable: false),
                        EnjoymentStartDate = c.DateTime(nullable: false),
                        EnjoymentEndDate = c.DateTime(nullable: false),
                        ReceiptDate = c.DateTime(nullable: false),
                        Value = c.String(maxLength: 256),
                        EmployeeId = c.Int(nullable: false),
                        DeletedDate = c.DateTime(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("rh.Employee", t => t.EmployeeId, cascadeDelete: true)
                .Index(t => t.EmployeeId)
                .Index(t => t.DeletedDate);
            
            CreateTable(
                "rh.EmployeeVariables",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.String(maxLength: 256),
                        Description = c.String(maxLength: 256),
                        Value = c.Decimal(nullable: false, precision: 10, scale: 2),
                        Date = c.DateTime(nullable: false),
                        EmployeeId = c.Int(nullable: false),
                        DeletedDate = c.DateTime(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("rh.Employee", t => t.EmployeeId, cascadeDelete: true)
                .Index(t => t.EmployeeId)
                .Index(t => t.DeletedDate);
            
            CreateTable(
                "rh.NotificationDestination",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 256),
                        Email = c.String(maxLength: 256),
                        Query = c.String(),
                        NotificationId = c.Int(nullable: false),
                        DeletedDate = c.DateTime(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("rh.Notification", t => t.NotificationId, cascadeDelete: true)
                .Index(t => t.NotificationId)
                .Index(t => t.DeletedDate);
            
            CreateTable(
                "rh.Notification",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 256),
                        Frequency = c.String(maxLength: 256),
                        Active = c.Boolean(nullable: false),
                        DeletedDate = c.DateTime(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.DeletedDate);
            
            CreateTable(
                "rh.NotificationItem",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Table = c.String(maxLength: 256),
                        Field = c.String(maxLength: 256),
                        Value = c.String(maxLength: 256),
                        FieldAlias = c.String(maxLength: 256),
                        TableAlias = c.String(maxLength: 256),
                        NotificationId = c.Int(nullable: false),
                        DeletedDate = c.DateTime(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("rh.Notification", t => t.NotificationId, cascadeDelete: true)
                .Index(t => t.NotificationId)
                .Index(t => t.DeletedDate);
            
            CreateTable(
                "rh.ReportItem",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Field = c.String(nullable: false, maxLength: 80),
                        FieldAlias = c.String(nullable: false, maxLength: 80),
                        Table = c.String(nullable: false, maxLength: 80),
                        TableAlias = c.String(maxLength: 256),
                        Value = c.String(maxLength: 100),
                        ReportId = c.Int(nullable: false),
                        Type = c.String(maxLength: 80),
                        DeletedDate = c.DateTime(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("rh.Report", t => t.ReportId, cascadeDelete: true)
                .Index(t => t.ReportId)
                .Index(t => t.DeletedDate);
            
            CreateTable(
                "rh.Report",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 80),
                        Description = c.String(maxLength: 255),
                        Query = c.String(),
                        Active = c.Boolean(),
                        DeletedDate = c.DateTime(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.DeletedDate);
            
        }
        
        public override void Down()
        {
            DropForeignKey("rh.ReportItem", "ReportId", "rh.Report");
            DropForeignKey("rh.NotificationItem", "NotificationId", "rh.Notification");
            DropForeignKey("rh.NotificationDestination", "NotificationId", "rh.Notification");
            DropForeignKey("rh.EmployeeVariables", "EmployeeId", "rh.Employee");
            DropForeignKey("rh.EmployeeVacation", "EmployeeId", "rh.Employee");
            DropForeignKey("rh.EmployeeSalary", "EmployeeId", "rh.Employee");
            DropForeignKey("rh.EmployeeRole", "RoleId", "rh.Role");
            DropForeignKey("rh.EmployeeRole", "EmployeeId", "rh.Employee");
            DropForeignKey("rh.EmployeeRelationship", "EmployeeId", "rh.Employee");
            DropForeignKey("rh.EmployeeDepartment", "EmployeeId", "rh.Employee");
            DropForeignKey("rh.EmployeeDepartment", "DepartmentId", "rh.Department");
            DropForeignKey("rh.EmployeeContract", "EmployeeId", "rh.Employee");
            DropForeignKey("rh.EmployeeContact", "EmployeeId", "rh.Employee");
            DropForeignKey("rh.EmployeeCompany", "EmployeeId", "rh.Employee");
            DropForeignKey("rh.EmployeeCompany", "CompanyId", "rh.Company");
            DropForeignKey("rh.EmployeeBenefit", "EmployeeId", "rh.Employee");
            DropForeignKey("rh.EmployeeBenefit", "BenefitId", "rh.Benefit");
            DropForeignKey("rh.EmployeeBankAccount", "EmployeeId", "rh.Employee");
            DropForeignKey("rh.EmployeeAttachment", "EmployeeId", "rh.Employee");
            DropForeignKey("rh.EmployeeAddress", "EmployeeId", "rh.Employee");
            DropForeignKey("rh.EmployeeVehicle", "EmployeeId", "rh.Employee");
            DropForeignKey("rh.PayslipItem", "PayslipId", "rh.Payslip");
            DropForeignKey("rh.Payslip", "EmployeeId", "rh.Employee");
            DropForeignKey("rh.Payslip", "CostCenterId", "rh.CostCenter");
            DropForeignKey("rh.Payslip", "CompanyId", "rh.Company");
            DropForeignKey("rh.Employee", "EducationalStageId", "rh.EducationalStage");
            DropForeignKey("rh.CourseEmployee", "EmployeeId", "rh.Employee");
            DropForeignKey("rh.CourseEmployee", "CourseId", "rh.Course");
            DropForeignKey("rh.Employee", "CostCenterId", "rh.CostCenter");
            DropForeignKey("rh.Employee", "CompanyId", "rh.Company");
            DropForeignKey("rh.Employee", "CivilStatusId", "rh.CivilStatus");
            DropForeignKey("rh.EmployeeAddress", "CityId", "rh.City");
            DropForeignKey("rh.City", "StateId", "rh.State");
            DropIndex("rh.Report", new[] { "DeletedDate" });
            DropIndex("rh.ReportItem", new[] { "DeletedDate" });
            DropIndex("rh.ReportItem", new[] { "ReportId" });
            DropIndex("rh.NotificationItem", new[] { "DeletedDate" });
            DropIndex("rh.NotificationItem", new[] { "NotificationId" });
            DropIndex("rh.Notification", new[] { "DeletedDate" });
            DropIndex("rh.NotificationDestination", new[] { "DeletedDate" });
            DropIndex("rh.NotificationDestination", new[] { "NotificationId" });
            DropIndex("rh.EmployeeVariables", new[] { "DeletedDate" });
            DropIndex("rh.EmployeeVariables", new[] { "EmployeeId" });
            DropIndex("rh.EmployeeVacation", new[] { "DeletedDate" });
            DropIndex("rh.EmployeeVacation", new[] { "EmployeeId" });
            DropIndex("rh.EmployeeSalary", new[] { "DeletedDate" });
            DropIndex("rh.EmployeeSalary", new[] { "EmployeeId" });
            DropIndex("rh.Role", new[] { "DeletedDate" });
            DropIndex("rh.EmployeeRole", new[] { "DeletedDate" });
            DropIndex("rh.EmployeeRole", new[] { "RoleId" });
            DropIndex("rh.EmployeeRole", new[] { "EmployeeId" });
            DropIndex("rh.EmployeeRelationship", new[] { "DeletedDate" });
            DropIndex("rh.EmployeeRelationship", new[] { "EmployeeId" });
            DropIndex("rh.EmployeeDepartment", new[] { "DeletedDate" });
            DropIndex("rh.EmployeeDepartment", new[] { "DepartmentId" });
            DropIndex("rh.EmployeeDepartment", new[] { "EmployeeId" });
            DropIndex("rh.EmployeeContract", new[] { "DeletedDate" });
            DropIndex("rh.EmployeeContract", new[] { "EmployeeId" });
            DropIndex("rh.EmployeeContact", new[] { "DeletedDate" });
            DropIndex("rh.EmployeeContact", new[] { "EmployeeId" });
            DropIndex("rh.EmployeeCompany", new[] { "DeletedDate" });
            DropIndex("rh.EmployeeCompany", new[] { "EmployeeId" });
            DropIndex("rh.EmployeeCompany", new[] { "CompanyId" });
            DropIndex("rh.EmployeeBenefit", new[] { "DeletedDate" });
            DropIndex("rh.EmployeeBenefit", new[] { "EmployeeId" });
            DropIndex("rh.EmployeeBenefit", new[] { "BenefitId" });
            DropIndex("rh.EmployeeBankAccount", new[] { "DeletedDate" });
            DropIndex("rh.EmployeeBankAccount", new[] { "EmployeeId" });
            DropIndex("rh.EmployeeAttachment", new[] { "DeletedDate" });
            DropIndex("rh.EmployeeAttachment", new[] { "EmployeeId" });
            DropIndex("rh.Department", new[] { "DeletedDate" });
            DropIndex("rh.Benefit", new[] { "DeletedDate" });
            DropIndex("rh.EmployeeVehicle", new[] { "DeletedDate" });
            DropIndex("rh.EmployeeVehicle", new[] { "EmployeeId" });
            DropIndex("rh.PayslipItem", new[] { "DeletedDate" });
            DropIndex("rh.PayslipItem", new[] { "PayslipId" });
            DropIndex("rh.Payslip", new[] { "CostCenterId" });
            DropIndex("rh.Payslip", new[] { "CompanyId" });
            DropIndex("rh.Payslip", "un_payslip_date");
            DropIndex("rh.EducationalStage", new[] { "DeletedDate" });
            DropIndex("rh.Course", new[] { "DeletedDate" });
            DropIndex("rh.CourseEmployee", new[] { "DeletedDate" });
            DropIndex("rh.CourseEmployee", new[] { "EmployeeId" });
            DropIndex("rh.CourseEmployee", new[] { "CourseId" });
            DropIndex("rh.CostCenter", new[] { "DeletedDate" });
            DropIndex("rh.Company", new[] { "DeletedDate" });
            DropIndex("rh.CivilStatus", new[] { "DeletedDate" });
            DropIndex("rh.Employee", new[] { "DeletedDate" });
            DropIndex("rh.Employee", new[] { "CostCenterId" });
            DropIndex("rh.Employee", new[] { "EducationalStageId" });
            DropIndex("rh.Employee", new[] { "CivilStatusId" });
            DropIndex("rh.Employee", new[] { "CompanyId" });
            DropIndex("rh.State", new[] { "DeletedDate" });
            DropIndex("rh.City", new[] { "DeletedDate" });
            DropIndex("rh.City", new[] { "StateId" });
            DropIndex("rh.EmployeeAddress", new[] { "DeletedDate" });
            DropIndex("rh.EmployeeAddress", new[] { "CityId" });
            DropIndex("rh.EmployeeAddress", new[] { "EmployeeId" });
            DropTable("rh.Report");
            DropTable("rh.ReportItem");
            DropTable("rh.NotificationItem");
            DropTable("rh.Notification");
            DropTable("rh.NotificationDestination");
            DropTable("rh.EmployeeVariables");
            DropTable("rh.EmployeeVacation");
            DropTable("rh.EmployeeSalary");
            DropTable("rh.Role");
            DropTable("rh.EmployeeRole");
            DropTable("rh.EmployeeRelationship");
            DropTable("rh.EmployeeDepartment");
            DropTable("rh.EmployeeContract");
            DropTable("rh.EmployeeContact");
            DropTable("rh.EmployeeCompany");
            DropTable("rh.EmployeeBenefit");
            DropTable("rh.EmployeeBankAccount");
            DropTable("rh.EmployeeAttachment");
            DropTable("rh.Department");
            DropTable("rh.Benefit");
            DropTable("rh.EmployeeVehicle");
            DropTable("rh.PayslipItem");
            DropTable("rh.Payslip");
            DropTable("rh.EducationalStage");
            DropTable("rh.Course");
            DropTable("rh.CourseEmployee");
            DropTable("rh.CostCenter");
            DropTable("rh.Company");
            DropTable("rh.CivilStatus");
            DropTable("rh.Employee");
            DropTable("rh.State");
            DropTable("rh.City");
            DropTable("rh.EmployeeAddress");
        }
    }
}
