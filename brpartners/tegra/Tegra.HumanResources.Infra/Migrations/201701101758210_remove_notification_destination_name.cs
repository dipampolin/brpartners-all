namespace Tegra.HumanResources.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class remove_notification_destination_name : DbMigration
    {
        public override void Up()
        {
            DropColumn("rh.NotificationDestination", "Name");
        }
        
        public override void Down()
        {
            AddColumn("rh.NotificationDestination", "Name", c => c.String(maxLength: 256));
        }
    }
}
