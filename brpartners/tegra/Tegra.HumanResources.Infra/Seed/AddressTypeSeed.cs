﻿using System.Linq;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Domain.Seed
{
    class AddressTypeSeed
    {

        public static void Seed(RHContext context)
        {

            if (context.AddressTypes.Count() > 0)
            {
                return;
            }

            context.AddressTypes.Add(AddressType.Create("Residência"));
            context.AddressTypes.Add(AddressType.Create("Comercial"));
            context.AddressTypes.Add(AddressType.Create("Verâneio"));
            context.AddressTypes.Add(AddressType.Create("Entrega"));
            context.AddressTypes.Add(AddressType.Create("Cobrança"));
            context.AddressTypes.Add(AddressType.Create("Expedição"));
            context.AddressTypes.Add(AddressType.Create("Atendimento"));
            context.AddressTypes.Add(AddressType.Create("Recebimento"));
            context.AddressTypes.Add(AddressType.Create("Correspondência"));

            context.SaveChanges();
        }

    }
}
