﻿using Newtonsoft.Json;
using System.IO;
using System.Linq;
using System.Reflection;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Domain.Seed
{
    class CBOSeed
    {

        public static void Seed(RHContext context)
        {

            if (context.CBOs.Count() > 0)
            {
                return;
            }

            var assembly = Assembly.GetExecutingAssembly();

            using (var stream = assembly.GetManifestResourceStream("Tegra.HumanResources.Domain.SeedResources.CBO.json"))
            using (var reader = new StreamReader(stream))
            {
                var json = reader.ReadToEnd();
                dynamic cbos = JsonConvert.DeserializeObject(json);

                foreach (var item in cbos)
                {
                    context.CBOs.Add(CBO.Create(int.Parse((string)item.Codigo), (string)item.Titulo));
                }
            }

            context.SaveChanges();
        }

    }
}
