﻿using Tegra.HumanResources.Domain.Entities;
using System.Linq;
using Newtonsoft.Json;
using System.IO;
using System.Reflection;

namespace Tegra.HumanResources.Infra.Seed
{
    class CivilStatusSeed
    {

        public static void Seed(RHContext context)
        {

            if (context.CivilStatuses.Count() > 0)
            {
                return;
            }

            var assembly = Assembly.GetExecutingAssembly();

            using (var stream = assembly.GetManifestResourceStream("Tegra.HumanResources.Infra.SeedResources.CivilStatuses.json"))
            using (var reader = new StreamReader(stream))
            {
                var json = reader.ReadToEnd();
                dynamic statuses = JsonConvert.DeserializeObject(json);

                foreach (var status in statuses)
                {
                    context.CivilStatuses.Add(new CivilStatus((string)status.Value));
                }
            }
            context.SaveChanges();
        }

    }
}