﻿using Newtonsoft.Json;
using System.IO;
using System.Linq;
using System.Reflection;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Infra.Seed
{
    class CitySeed
    {

        public static void Seed(RHContext context)
        {

            if (context.States.Count() > 0)
            {
                return;
            }

            var assembly = Assembly.GetExecutingAssembly();

            var ufs = context.States.Select(x => x).ToList();

            using (var stream = assembly.GetManifestResourceStream("Tegra.HumanResources.Infra.SeedResources.Cities.json"))
            using (var reader = new StreamReader(stream))
            {
                var json = reader.ReadToEnd();
                dynamic cities = JsonConvert.DeserializeObject(json);

                foreach (var city in cities)
                {
                    var uf = ufs.SingleOrDefault(x => x.UF.Equals((string)city.Uf));

                    if (uf == null)
                    {
                        uf = new State((string)city.Uf, (string)city.Estado);
                        ufs.Add(uf);
                        context.States.Add(uf);
                    }

                    uf.AddCity(new City((string)city.Nome));
                }
            }

            context.SaveChanges();
        }

    }
}
