﻿using Newtonsoft.Json;
using System.IO;
using System.Linq;
using System.Reflection;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Domain.Seed
{
    class CountrySeed
    {

        public static void Seed(RHContext context)
        {

            if (context.Countries.Count() > 0)
            {
                return;
            }

            var assembly = Assembly.GetExecutingAssembly();

            using (var stream = assembly.GetManifestResourceStream("Tegra.HumanResources.Domain.SeedResources.Countries.json"))
            using (var reader = new StreamReader(stream))
            {
                var json = reader.ReadToEnd();
                dynamic countries = JsonConvert.DeserializeObject(json);

                foreach (var country in countries)
                {
                    context.Countries.Add(Country.Create((string)country.sigla, (string)country.nome_pais));
                }
            }

            context.SaveChanges();
        }

    }
}
