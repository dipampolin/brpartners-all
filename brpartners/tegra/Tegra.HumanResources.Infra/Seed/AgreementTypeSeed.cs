﻿using Newtonsoft.Json;
using System.IO;
using System.Linq;
using System.Reflection;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Domain.Seed
{
    class AgreementTypeSeed
    {

        public static void Seed(RHContext context)
        {

            if (context.AgreementTypes.Count() > 0)
            {
                return;
            }

            context.AgreementTypes.Add(AgreementType.Create("Farmácia"));
            context.AgreementTypes.Add(AgreementType.Create("Livraria"));
            context.AgreementTypes.Add(AgreementType.Create("Acadêmia"));

            context.SaveChanges();
        }

    }
}
