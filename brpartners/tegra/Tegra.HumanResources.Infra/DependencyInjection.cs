﻿using SimpleInjector;
using Tegra.DDD.Repository;
using Tegra.HumanResources.Domain.Repository;
using Tegra.HumanResources.Domain.Service;
using Tegra.HumanResources.Infra.Asynchronous;
using Tegra.HumanResources.Infra.Repository;
using Tegra.HumanResources.Services;

namespace Tegra.HumanResources.Infra
{
    public class DependencyInjection
    {

        public static void Register(Container container)
        {

            // Inject the context into the repository
            container.RegisterInitializer<BaseRepository<RHContext>>(repository =>
            {
                repository.SetContext(container.GetInstance<RHContext>());
            });

            // Context mapping
            container.Register<RHContext>(Lifestyle.Scoped);

            // Map all the repositories
            container.Register<IAddressRepository, AddressRepository>(Lifestyle.Scoped);
            container.Register<IBankAccountRepository, BankAccountRepository>(Lifestyle.Scoped);
            container.Register<IBenefitRepository, BenefitRepository>(Lifestyle.Scoped);
            container.Register<ICivilStatusRepository, CivilStatusRepository>(Lifestyle.Scoped);
            container.Register<ICityRepository, CityRepository>(Lifestyle.Scoped);
            container.Register<ICompanyRepository, CompanyRepository>(Lifestyle.Scoped);
            container.Register<IContactRepository, ContactRepository>(Lifestyle.Scoped);
            container.Register<ICourseRepository, CourseRepository>(Lifestyle.Scoped);
            container.Register<IEducationalStageRepository, EducationalStageRepository>(Lifestyle.Scoped);
            container.Register<IEmployeeRepository, EmployeeRepository>(Lifestyle.Scoped);
            container.Register<IPayslipRepository, PayslipRepository>(Lifestyle.Scoped);
            container.Register<ICostCenterRepository, CostCenterRepository>(Lifestyle.Scoped);
            container.Register<IStateRepository, StateRepository>(Lifestyle.Scoped);
            container.Register<IReportRepository, ReportRepository>(Lifestyle.Scoped);
            container.Register<IReportItemRepository, ReportItemRepository>(Lifestyle.Scoped);
            container.Register<IReportFilterRepository, ReportFilterRepository>(Lifestyle.Scoped);
            container.Register<IRelationshipRepository, RelationshipRepository>(Lifestyle.Scoped);
            container.Register<IVehicleRepository, VehicleRepository>(Lifestyle.Scoped);
            container.Register<INotificationRepository, NotificationRepository>(Lifestyle.Scoped);
            container.Register<INotificationItemRepository, NotificationItemRepository>(Lifestyle.Scoped);
            container.Register<INotificationDestinationRepository, NotificationDestinationRepository>(Lifestyle.Scoped);
            container.Register<IEmployeeAttachmentRepository, EmployeeAttachmentRepository>(Lifestyle.Scoped);
            container.Register<IEmployeeContractRepository, EmployeeContractRepository>(Lifestyle.Scoped);
            container.Register<IRoleRepository, RoleRepository>(Lifestyle.Scoped);
            container.Register<IEmployeeCompanyRepository, EmployeeCompanyRepository>(Lifestyle.Scoped);
            container.Register<IEmployeeCostCenterRepository, EmployeeCostCenterRepository>(Lifestyle.Scoped);
            container.Register<IEmployeeRoleRepository, EmployeeRoleRepository>(Lifestyle.Scoped);
            container.Register<IEmployeeBenefitRepository, EmployeeBenefitRepository>(Lifestyle.Scoped);
            container.Register<IEmployeeVariablesRepository, EmployeeVariablesRepository>(Lifestyle.Scoped);
            container.Register<IEmployeeVariablesTypeRepository, EmployeeVariablesTypeRepository>(Lifestyle.Scoped);
            container.Register<IEmployeeVacationRepository, EmployeeVacationRepository>(Lifestyle.Scoped);
            container.Register<IEmployeeCourseRepository, EmployeeCourseRepository>(Lifestyle.Scoped);
            container.Register<ISalaryHistoryRepository, SalaryHistoryRepository>(Lifestyle.Scoped);
            container.Register<INotificationFilterRepository, NotificationFilterRepository>(Lifestyle.Scoped);
            container.Register<IBenefitTypeRepository, BenefitTypeRepository>(Lifestyle.Scoped);
            container.Register<IEmployeeTypeRepository, EmployeeTypeRepository>(Lifestyle.Scoped);
            container.Register<IBankRepository, BankRepository>(Lifestyle.Scoped);

            // Map all the services
            container.Register<IAddressService, AddressService>(Lifestyle.Scoped);
            container.Register<IBankAccountService, BankAccountService>(Lifestyle.Scoped);
            container.Register<IBenefitService, BenefitService>(Lifestyle.Scoped);
            container.Register<ICompanyService, CompanyService>(Lifestyle.Scoped);
            container.Register<IContactService, ContactService>(Lifestyle.Scoped);
            container.Register<ICourseService, CourseService>(Lifestyle.Scoped);
            container.Register<IEmployeeService, EmployeeService>(Lifestyle.Scoped);
            container.Register<IPayslipService, PayslipService>(Lifestyle.Scoped);
            container.Register<IExcelService, ExcelService>(Lifestyle.Scoped);
            container.Register<IReportService, ReportService>(Lifestyle.Scoped);
            container.Register<IReportItemService, ReportItemService>(Lifestyle.Scoped);
            container.Register<IRelationshipService, RelationshipService>(Lifestyle.Scoped);
            container.Register<IVehicleService, VehicleService>(Lifestyle.Scoped);
            container.Register<INotificationService, NotificationService>(Lifestyle.Scoped);
            container.Register<INotificationItemService, NotificationItemService>(Lifestyle.Scoped);
            container.Register<INotificationDestinationService, NotificationDestinationService>(Lifestyle.Scoped);
            container.Register<IEmployeeAttachmentService, EmployeeAttachmentService>(Lifestyle.Scoped);
            container.Register<IEmployeeContractService, EmployeeContractService>(Lifestyle.Scoped);
            container.Register<IDepartmentService, DepartmentService>(Lifestyle.Scoped);
            container.Register<IRoleService, RoleService>(Lifestyle.Scoped);
            container.Register<IEmployeeCostCenterService, EmployeeCostCenterService>(Lifestyle.Scoped);
            container.Register<IEmployeeRoleService, EmployeeRoleService>(Lifestyle.Scoped);
            container.Register<IEmployeeBenefitService, EmployeeBenefitService>(Lifestyle.Scoped);
            container.Register<IEmployeeVariablesService, EmployeeVariablesService>(Lifestyle.Scoped);
            container.Register<IEmployeeVacationService, EmployeeVacationService>(Lifestyle.Scoped);
            container.Register<IEmployeeCourseService, EmployeeCourseService>(Lifestyle.Scoped);
            container.Register<ISalaryHistoryService, SalaryHistoryService>(Lifestyle.Scoped);
            container.Register<ICostCenterService, CostCenterService>(Lifestyle.Scoped);
            container.Register<INotificationFilterService, NotificationFilterService>(Lifestyle.Scoped);
            container.Register<IEmployeeTypeService, EmployeeTypeService>(Lifestyle.Scoped);
            container.Register<IBankService, BankService>(Lifestyle.Scoped);

            container.Register<IAsyncNotificationService, AsyncNotificationService>(Lifestyle.Scoped);
            container.RegisterDecorator<IAsyncNotificationService, LifetimeScopeCommandHandlerProxy>(Lifestyle.Singleton);
            container.RegisterDecorator<IAsyncNotificationService, AsyncCommandHandlerDecorator>(Lifestyle.Singleton);
        }
    }
}
