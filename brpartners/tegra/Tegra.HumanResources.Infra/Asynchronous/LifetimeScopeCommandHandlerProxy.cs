﻿using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Tegra.HumanResources.Domain.Service;

namespace Tegra.HumanResources.Infra.Asynchronous
{
    public class LifetimeScopeCommandHandlerProxy : IAsyncNotificationService
    {
        private readonly Container container;
        private readonly Func<IAsyncNotificationService> decorateeFactory;

        public LifetimeScopeCommandHandlerProxy(Container container,
            Func<IAsyncNotificationService> decorateeFactory)
        {
            this.container = container;
            this.decorateeFactory = decorateeFactory;
        }

        public void SendNotifications()
        {
            // Create the decorateeFactory within the scope.
            //ThreadPool.QueueUserWorkItem(state =>
            {
                using (container.BeginLifetimeScope())
                // Start a new scope.
                {
                    try
                    {
                        // Create new handler in this thread.
                        IAsyncNotificationService handler = this.decorateeFactory.Invoke();
                        handler.SendNotifications();
                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Debug.Write(ex.StackTrace);
                    }
                }
            }
            //);
            //IAsyncNotificationService handler = this.decorateeFactory.Invoke();
            //handler.SendNotifications();
        }
    }
}
