﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Tegra.HumanResources.Domain.Service;

namespace Tegra.HumanResources.Infra.Asynchronous
{
    public class AsyncCommandHandlerDecorator : IAsyncNotificationService
    {
        private readonly Func<IAsyncNotificationService> decorateeFactory;

        public AsyncCommandHandlerDecorator(Func<IAsyncNotificationService> decorateeFactory)
        {
            this.decorateeFactory = decorateeFactory;
        }

        public void SendNotifications()
        {
            // Execute on different thread.
            ThreadPool.QueueUserWorkItem(state => {
                try
                {
                    // Create new handler in this thread.
                    IAsyncNotificationService handler = this.decorateeFactory.Invoke();
                    handler.SendNotifications();
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.Write(ex.StackTrace);
                }
            });
        }

    }
}
