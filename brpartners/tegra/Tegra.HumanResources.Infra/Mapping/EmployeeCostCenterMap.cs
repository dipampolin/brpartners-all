﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Infra.Mapping
{
    public class EmployeeCostCenterMap
    {
        public static void Map(EntityTypeConfiguration<EmployeeCostCenter> model)
        {
            // PK
            model.HasKey(c => c.Id)
                .ToTable("EmployeeCostCenter");

            // Properties
            model.Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();

            model.Property(x => x.StartDate)
               .IsRequired();

            model.Property(x => x.EndDate)
                .IsOptional();

            // FKs
            model.HasRequired(x => x.Employee)
                .WithMany()
                .HasForeignKey(x => x.EmployeeId)
                .WillCascadeOnDelete(false);

            // FKs
            model.HasRequired(x => x.CostCenter)
                .WithMany()
                .HasForeignKey(x => x.CostCenterId)
                .WillCascadeOnDelete(false);
        }
    }
}
