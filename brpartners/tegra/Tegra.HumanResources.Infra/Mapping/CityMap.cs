﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Infra.Mapping
{
    public class CityMap
    {

        public static void Map(EntityTypeConfiguration<City> model)
        {

            // PK
            model.HasKey(c => c.Id)
                .ToTable("City");

            // Properties
            model.Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();

            model.Property(x => x.Name)
                .HasMaxLength(100)
                .IsRequired();

            // FKs
            model.HasRequired(x => x.State)
                .WithMany(p => p.Cities)
                .HasForeignKey(x => x.StateId)
                .WillCascadeOnDelete(false);

        }
    }
}
