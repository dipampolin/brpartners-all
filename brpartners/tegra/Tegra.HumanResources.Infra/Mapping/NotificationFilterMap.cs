﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Infra.Mapping
{
    public class NotificationFilterMap
    {
        public static void Map(EntityTypeConfiguration<NotificationFilter> model)
        {
            //pk
            model.HasKey(c => c.Id)
                    .ToTable("NotificationItemFilter");

            //fields
            model.Property(x => x.Id)
               .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
               .IsRequired();

            model.Property(c => c.Field)
                  .IsRequired()
                      .HasMaxLength(80);

            model.Property(c => c.FieldAlias)
                 .IsRequired()
                     .HasMaxLength(80);

            model.Property(c => c.Table)
                .IsRequired()
                    .HasMaxLength(80);

            model.Property(c => c.FieldAlias)
              .IsRequired()
                  .HasMaxLength(80);

            model.Property(c => c.Value)
                    .IsOptional()
                        .HasMaxLength(100);

            model.Property(c => c.Type)
              .IsOptional()
                  .HasMaxLength(80);

            //fk
            model.HasRequired(x => x.Notification)
              .WithMany(x => x.Filters)
              .HasForeignKey(x => x.NotificationId)
              .WillCascadeOnDelete(true);
        }
    }
}
