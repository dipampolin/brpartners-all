﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Infra.Mapping
{
    public class CivilStatusMap
    {

        public static void Map(EntityTypeConfiguration<CivilStatus> model)
        {
            // PK
            model.HasKey(c => c.Id)
                .ToTable("CivilStatus");

            // Properties
            model.Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();

            model.Property(x => x.Value)
                .HasMaxLength(30)
                .IsRequired();
        }
    }
}
