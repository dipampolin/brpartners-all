﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Infra.Mapping
{
    public class EmployeeCompanyMap
    {
        public static void Map(EntityTypeConfiguration<EmployeeCompany> model)
        {
            // PK
            model.HasKey(c => c.Id)
                .ToTable("EmployeeCompany");

            // Properties
            model.Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();

            model.Property(x => x.StartDate)
               .IsOptional();

            model.Property(x => x.EndDate)
                .IsOptional();

            // FKs
            model.HasRequired(x => x.Employee)
                .WithMany()
                .HasForeignKey(x => x.EmployeeId)
                .WillCascadeOnDelete(false);

            // FKs
            model.HasRequired(x => x.Company)
                .WithMany()
                .HasForeignKey(x => x.CompanyId)
                .WillCascadeOnDelete(false);
        }
    }
}
