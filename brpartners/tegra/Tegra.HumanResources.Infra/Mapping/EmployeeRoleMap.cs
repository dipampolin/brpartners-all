﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Infra.Mapping
{
    public class EmployeeRoleMap
    {
        public static void Map(EntityTypeConfiguration<EmployeeRole> model)
        {
            // PK
            model.HasKey(c => c.Id)
                .ToTable("EmployeeRole");

            // Properties
            model.Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();

            model.Property(x => x.StartDate)
               .IsRequired();

            model.Property(x => x.EndDate)
                .IsOptional();

            // FKs
            model.HasRequired(x => x.Employee)
                .WithMany()
                .HasForeignKey(x => x.EmployeeId)
                .WillCascadeOnDelete(false);

            // FKs
            model.HasRequired(x => x.Role)
                .WithMany()
                .HasForeignKey(x => x.RoleId)
                .WillCascadeOnDelete(false);
        }
    }
}
