﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Infra.Mapping
{
    public class EducationalStageMap
    {

        public static void Map(EntityTypeConfiguration<EducationalStage> model)
        {
            // PK
            model.HasKey(c => c.Id)
                .ToTable("EducationalStage");

            // Properties
            model.Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();

            model.Property(x => x.Value)
                .HasMaxLength(30)
                .IsRequired();
        }
    }
}
