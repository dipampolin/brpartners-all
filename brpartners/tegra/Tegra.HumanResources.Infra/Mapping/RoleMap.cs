﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Infra.Mapping
{
    public class RoleMap
    {
        public static void Map(EntityTypeConfiguration<Role> model)
        {
            // PK
            model.HasKey(c => c.Id)
                .ToTable("Role");

            // Properties
            model.Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();

            model.Property(x => x.Name)
                .IsRequired()
                .HasMaxLength(100);

        }
    }
}
