﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Infra.Mapping
{
    public class VehicleMap
    {
        public static void Map(EntityTypeConfiguration<EmployeeVehicle> model)
        {
            // PK
            model.HasKey(c => c.Id)
                .ToTable("EmployeeVehicle");

            // Properties
            model.Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();

            model.Property(x => x.Brand)
                .HasMaxLength(25)
                .IsOptional();

            model.Property(x => x.Model)
                .HasMaxLength(100)
                .IsOptional();

            model.Property(x => x.Color)
                .HasMaxLength(30)
                .IsOptional();

            model.Property(x => x.CarPlate)
                .HasMaxLength(7)
                .IsOptional();

            model.Property(x => x.Active)
                .IsRequired();

            // FKs
            model.HasRequired(x => x.Employee)
                .WithMany(p => p.Vehicles)
                .HasForeignKey(x => x.EmployeeId)
                .WillCascadeOnDelete(true);
        }
    }
}