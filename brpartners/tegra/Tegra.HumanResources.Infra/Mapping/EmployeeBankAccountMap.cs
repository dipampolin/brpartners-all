﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Infra.Mapping
{
    public class EmployeeBankAccountMap
    {
        public static void Map(EntityTypeConfiguration<EmployeeBankAccount> model)
        {
            // PK
            model.HasKey(c => c.Id)
                .ToTable("EmployeeBankAccount");

            // Properties
            model.Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();

            model.Property(x => x.Main)
                .IsRequired();

            model.Property(x => x.Bank)
             .IsRequired()
             .HasMaxLength(100);

            model.Property(x => x.Agency)
             .IsRequired()
             .HasMaxLength(20);

            model.Property(x => x.Account)
             .IsRequired()
             .HasMaxLength(20);

            // FKs
            model.HasRequired(x => x.Employee)
                .WithMany()
                .HasForeignKey(x => x.EmployeeId)
                .WillCascadeOnDelete(false);
        }
    }
}
