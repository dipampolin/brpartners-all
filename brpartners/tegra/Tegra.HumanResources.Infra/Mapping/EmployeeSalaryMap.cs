﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Infra.Mapping
{
    public class EmployeeSalaryMap
    {
        public static void Map(EntityTypeConfiguration<EmployeeSalary> model)
        {
            // PK
            model.HasKey(c => c.Id)
                .ToTable("EmployeeSalary");

            // Properties
            model.Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();

            model.Property(x => x.Description)
                .IsOptional()
                .HasMaxLength(100);

            model.Property(x => x.Category)
                .IsOptional()
                .HasMaxLength(100);

            model.Property(x => x.SalaryPeriod)
                .IsOptional()
                .HasMaxLength(100);

            model.Property(x => x.WorkLoad)
                .IsOptional()
                .HasMaxLength(100);


            model.Property(x => x.Salary)
                .IsOptional();

            model.Property(x => x.ExtraHour)
                .IsOptional();

            model.Property(x => x.Gratification)
               .IsOptional();

            model.Property(x => x.Triennium)
                .IsOptional();

            model.Property(x => x.StartDate)
               .IsOptional();

            model.Property(x => x.EndDate)
              .IsOptional();

            // FKs
            model.HasRequired(x => x.Employee)
                .WithMany()
                .HasForeignKey(x => x.EmployeeId)
                .WillCascadeOnDelete(false);
        }
    }
}
