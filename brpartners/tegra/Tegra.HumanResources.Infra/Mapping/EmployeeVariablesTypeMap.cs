﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Infra.Mapping
{
    public class EmployeeVariablesTypeMap
    {
        public static void Map(EntityTypeConfiguration<EmployeeVariablesType> model)
        {

            // PK
            model.HasKey(c => c.Id)
                .ToTable("EmployeeVariablesType");

            model.Property(x => x.Name)
                .HasMaxLength(100);
        }
    }
}
