﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Infra.Mapping
{
    public class NotificationDestinationMap
    {
        public static void Map(EntityTypeConfiguration<NotificationDestination> model)
        {
            // PK
            model.HasKey(c => c.Id)
                .ToTable("NotificationDestination");

            // Properties
            model.Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();

            model.Property(x => x.Email)
              .HasMaxLength(100)
              .IsRequired();

            model.Property(x => x.LastSent)
              .IsOptional();

            // FKs
            model.HasRequired(x => x.Notification)
                .WithMany(x => x.Destinations)
                .HasForeignKey(x => x.NotificationId)
                .WillCascadeOnDelete(true);
        }
    }
}
