﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Infra.Mapping
{
    public class PayslipItemMap
    {

        public static void Map(EntityTypeConfiguration<PayslipItem> model)
        {

            // PK
            model.HasKey(c => c.Id)
                .ToTable("PayslipItem");

            // Properties
            model.Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();

            model.Property(x => x.Reference)
                .HasPrecision(10, 2)
                .IsRequired();

            model.Property(x => x.Rubric)
                .IsRequired();

            model.Property(x => x.RubricDescription)
                .HasMaxLength(255)
                .IsRequired();

            model.Property(x => x.RubricType)
                .HasMaxLength(1)
                .IsRequired();

            model.Property(x => x.Value)
                .HasPrecision(10, 2)
                .IsRequired();

            // FKs
            model.HasRequired(x => x.Payslip)
                .WithMany(x => x.Items)
                .HasForeignKey(x => x.PayslipId)
                .WillCascadeOnDelete(true);

        }
    }
}
