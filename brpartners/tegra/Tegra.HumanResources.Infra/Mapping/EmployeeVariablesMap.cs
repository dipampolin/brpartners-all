﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Infra.Mapping
{
    public class EmployeeVariablesMap
    {
        public static void Map(EntityTypeConfiguration<EmployeeVariables> model)
        {

            // PK
            model.HasKey(c => c.Id)
                .ToTable("EmployeeVariables");

            // Properties
            model.Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();

            model.Property(x => x.Description)
                .HasMaxLength(100);

            model.Property(x => x.Description)
                .IsOptional();

            model.Property(x => x.Date)
               .IsOptional();

            // FKs
            model.HasRequired(x => x.Employee)
                .WithMany()
                .HasForeignKey(x => x.EmployeeId)
                .WillCascadeOnDelete(false);

            model.HasRequired(x => x.Type)
                .WithMany()
                .HasForeignKey(x => x.EmployeeVariablesTypeId)
                .WillCascadeOnDelete(false);
        }
    }
}
