﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Infra.Mapping
{
    public class EmployeeAddressMap
    {

        public static void Map(EntityTypeConfiguration<EmployeeAddress> model)
        {

            // PK
            model.HasKey(c => c.Id)
                .ToTable("EmployeeAddress");

            // Properties
            model.Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();

            model.Property(x => x.Type)
                .HasMaxLength(25)
                .IsRequired();

            model.Property(x => x.Street)
                .HasMaxLength(100)
                .IsRequired();

            model.Property(x => x.CEP)
                .IsRequired();

            model.Property(x => x.Number)
                .HasMaxLength(8)
                .IsRequired();

            model.Property(x => x.Complement)
                .HasMaxLength(100)
                .IsOptional();

            model.Property(x => x.Neighborhood)
                .HasMaxLength(50)
                .IsOptional();

            model.Property(x => x.Active)
                .IsRequired();

            // FKs
            model.HasRequired(x => x.Employee)
                .WithMany(p => p.EmployeeAddress)
                .HasForeignKey(x => x.EmployeeId)
                .WillCascadeOnDelete(true);

            model.HasRequired(x => x.City)
                .WithMany()
                .HasForeignKey(x => x.CityId)
                .WillCascadeOnDelete(true);

        }
    }
}