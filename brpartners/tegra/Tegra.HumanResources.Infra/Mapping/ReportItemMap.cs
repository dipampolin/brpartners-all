﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Infra.Mapping
{
    public class ReportItemMap
    {
        public static void Map(EntityTypeConfiguration<ReportItem> model)
        {
            //pk

            model.HasKey(c => c.Id)
                    .ToTable("ReportItem");

            //fields
            model.Property(x => x.Id)
               .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
               .IsRequired();

            model.Property(c => c.Field)
                  .IsRequired()
                      .HasMaxLength(80);

            model.Property(c => c.FieldAlias)
                 .IsRequired()
                     .HasMaxLength(80);

            model.Property(c => c.Table)
                .IsRequired()
                    .HasMaxLength(80);

            model.Property(c => c.FieldAlias)
              .IsRequired()
                  .HasMaxLength(80);

            model.Property(c => c.Type)
              .IsOptional()
                  .HasMaxLength(80);

            //fk
            model.HasRequired(x => x.Report)
              .WithMany(x => x.Items)
              .HasForeignKey(x => x.ReportId)
              .WillCascadeOnDelete(true);
        }

    }
}
