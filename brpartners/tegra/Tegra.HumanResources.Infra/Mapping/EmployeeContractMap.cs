﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Infra.Mapping
{
    public class EmployeeContractMap
    {
        public static void Map(EntityTypeConfiguration<EmployeeContract> model)
        {
            // PK
            model.HasKey(c => c.Id)
                .ToTable("EmployeeContract");

            // Properties
            model.Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();

            model.Property(x => x.Description)
                .IsOptional()
                .HasMaxLength(100);

            model.Property(x => x.Approved)
              .IsRequired();

            model.Property(x => x.StartDate)
               .IsRequired();

            model.Property(x => x.EndDate)
              .IsOptional();

            // FKs
            model.HasRequired(x => x.Employee)
                .WithMany()
                .HasForeignKey(x => x.EmployeeId)
                .WillCascadeOnDelete(false);
        }
    }
}
