﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Infra.Mapping
{
    public class NotificationItemMap
    {
        public static void Map(EntityTypeConfiguration<NotificationItem> model)
        {
            // PK
            model.HasKey(c => c.Id)
                .ToTable("NotificationItem");

            // Properties
            model.Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();

            model.Property(x => x.Table)
              .HasMaxLength(80)
              .IsRequired();

            model.Property(x => x.Field)
            .HasMaxLength(80)
            .IsRequired();

            model.Property(x => x.TableAlias)
            .HasMaxLength(100)
            .IsRequired();

            model.Property(x => x.FieldAlias)
            .HasMaxLength(100)
            .IsRequired();

            // FKs
            model.HasRequired(x => x.Notification)
                .WithMany(x => x.Items)
                .HasForeignKey(x => x.NotificationId)
                .WillCascadeOnDelete(true);
        }
    }
}
