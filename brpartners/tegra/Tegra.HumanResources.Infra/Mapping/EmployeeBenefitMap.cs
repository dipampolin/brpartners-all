﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Infra.Mapping
{
    public class EmployeeBenefitMap
    {
        public static void Map(EntityTypeConfiguration<EmployeeBenefit> model)
        {
            // PK
            model.HasKey(c => c.Id)
                .ToTable("EmployeeBenefit");

            // Properties
            model.Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();

            model.Property(x => x.StartDate)
               .IsRequired();

            model.Property(x => x.EndDate)
                .IsOptional();

            model.Property(x => x.CustomValue)
                .IsOptional();

            // FKs
            model.HasRequired(x => x.Employee)
                .WithMany()
                .HasForeignKey(x => x.EmployeeId)
                .WillCascadeOnDelete(false);

            // FKs
            model.HasRequired(x => x.Benefit)
                .WithMany()
                .HasForeignKey(x => x.BenefitId)
                .WillCascadeOnDelete(false);
        }
    }
}
