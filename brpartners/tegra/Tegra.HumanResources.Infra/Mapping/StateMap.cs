﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Infra.Mapping
{
    public class StateMap
    {

        public static void Map(EntityTypeConfiguration<State> model)
        {

            // PK
            model.HasKey(c => c.Id)
                .ToTable("State");

            // Properties
            model.Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();

            model.Property(x => x.UF)
               .HasMaxLength(2)
               .IsRequired();

            model.Property(x => x.Name)
                .HasMaxLength(100)
                .IsRequired();

        }
    }
}
