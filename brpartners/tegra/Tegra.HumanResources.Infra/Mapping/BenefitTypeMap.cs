﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Infra.Mapping
{
    public class BenefitTypeMap
    {
        public static void Map(EntityTypeConfiguration<BenefitType> model)
        {

            // PK
            model.HasKey(c => c.Id)
                .ToTable("BenefitType");

            // Properties
            model.Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();

            model.Property(x => x.Name)
                .HasMaxLength(100)
                .IsRequired();

            model.Property(x => x.Category)
                .IsOptional();

        }
    }
}
