﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Infra.Mapping
{
    public class PayslipMap
    {

        public static void Map(EntityTypeConfiguration<Payslip> model)
        {

            // PK
            model.HasKey(c => c.Id)
                .ToTable("Payslip");

            // Properties
            model.Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();

            model.Property(x => x.CompanyCode)
                .IsRequired();

            model.Property(x => x.CompanyName)
                .HasMaxLength(255)
                .IsRequired();

            model.Property(x => x.CostCenterCode)
                .IsRequired();

            model.Property(x => x.CostCenterDescription)
                .HasMaxLength(255)
                .IsRequired();

            model.Property(x => x.RoleCode)
                .IsRequired();

            model.Property(x => x.RoleDescription)
                .HasMaxLength(100)
                .IsRequired();

            model.Property(x => x.Date)
                .HasColumnAnnotation("Index",
                    new IndexAnnotation(new IndexAttribute("un_payslip_date", 2)
                    {
                        IsUnique = true
                    })
                )
                .IsRequired();

            model.Property(x => x.EmployeeId)
               .HasColumnAnnotation("Index",
                    new IndexAnnotation(new IndexAttribute("un_payslip_date", 1)
                    {
                        IsUnique = true
                    })
                )
               .IsRequired();

            model.Property(x => x.DeletedDate)
               .HasColumnAnnotation("Index",
                    new IndexAnnotation(new IndexAttribute("un_payslip_date", 3)
                    {
                        IsUnique = true
                    })
                );

            // FKs
            model.HasRequired(x => x.Employee)
                .WithMany(x => x.Payslips)
                .HasForeignKey(x => x.EmployeeId)
                .WillCascadeOnDelete(false);

            model.HasRequired(x => x.CostCenter)
                .WithMany()
                .HasForeignKey(x => x.CostCenterId)
                .WillCascadeOnDelete(false);

        }
    }
}
