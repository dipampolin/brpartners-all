﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Infra.Mapping
{
    public class NotificationMap
    {
        public static void Map(EntityTypeConfiguration<Notification> model)
        {
            // PK
            model.HasKey(c => c.Id)
                .ToTable("Notification");

            // Properties
            model.Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();

            model.Property(x => x.Frequency)
              .HasMaxLength(40)
              .IsRequired();

            model.Property(x => x.Name)
                 .HasMaxLength(110)
                 .IsRequired();

            model.Property(x => x.Query)
                  .HasMaxLength(4000);

            model.Property(x => x.Active)
             .IsOptional();
        }
    }
}
