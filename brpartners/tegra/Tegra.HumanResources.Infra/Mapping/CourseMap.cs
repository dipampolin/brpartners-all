﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Infra.Mapping
{
    public class CourseMap
    {

        public static void Map(EntityTypeConfiguration<Course> model)
        {

            // PK
            model.HasKey(c => c.Id)
                .ToTable("Course");

            // Properties
            model.Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();
            
            model.Property(x => x.Name)
                .HasMaxLength(100)
                .IsRequired();

            model.Property(x => x.Active)
                .IsRequired();

        }
    }
}
