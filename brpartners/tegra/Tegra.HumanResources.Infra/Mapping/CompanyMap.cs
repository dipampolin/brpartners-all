﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Infra.Mapping
{
    public class CompanyMap
    {

        public static void Map(EntityTypeConfiguration<Company> model)
        {

            // PK
            model.HasKey(c => c.Id)
                .ToTable("Company");

            // Properties
            model.Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();

            model.Property(x => x.Code)
                .HasMaxLength(20)
                .IsRequired();

            model.Property(x => x.Name)
                .HasMaxLength(100)
                .IsRequired();

            model.Property(x => x.Cnpj)
                .HasMaxLength(20)
                .IsRequired();

        }
    }
}
