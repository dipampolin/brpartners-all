﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Infra.Mapping
{
    public class EmployeeContactMap
    {
        public static void Map(EntityTypeConfiguration<EmployeeContact> model)
        {
            // PK
            model.HasKey(c => c.Id)
                .ToTable("EmployeeContact");

            // Properties
            model.Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();

            model.Property(x => x.Type)
                .IsOptional()
                .HasMaxLength(20);

            model.Property(x => x.Value)
              .IsRequired()
              .HasMaxLength(100);

            model.Property(x => x.Active)
               .IsRequired();

            // FKs
            model.HasRequired(x => x.Employee)
                .WithMany()
                .HasForeignKey(x => x.EmployeeId)
                .WillCascadeOnDelete(false);
        }
    }
}
