﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Infra.Mapping
{
    public class ReportMap
    {
        public static void Map(EntityTypeConfiguration<Report> model)
        {
            //pk
            model.HasKey(c => c.Id)
                    .ToTable("Report");

            //fields
            model.Property(c => c.Id)
                    .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                        .IsRequired();

            model.Property(c => c.Name)
                    .IsRequired()
                        .HasMaxLength(80);

            model.Property(c => c.Query)
                   .IsMaxLength();

            model.Property(c => c.Description)
                    .IsOptional()
                        .HasMaxLength(255);

            model.Property(x => x.Active)
               .IsOptional();
        }
    }
}
