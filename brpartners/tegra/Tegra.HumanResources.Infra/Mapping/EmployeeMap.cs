﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Infra.Mapping
{
    public class EmployeeMap
    {

        public static void Map(EntityTypeConfiguration<Employee> model)
        {
            // PK
            model.HasKey(c => c.Id)
                .ToTable("Employee");

            // Properties
            model.Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();

            model.Property(x => x.Name)
                .HasMaxLength(150)
                .IsRequired();

            model.Property(x => x.Initials)
                .HasMaxLength(45)
                .IsRequired();

            model.Property(x => x.Gender)
                 .HasMaxLength(20)
                 .IsRequired();

            model.Property(x => x.Birthdate)
               .IsRequired();

            model.Property(x => x.EmploymentBookletUf)
              .IsOptional();


            model.Property(x => x.Foreigner)
                .IsRequired();

            model.Property(x => x.Nationality)
                .IsOptional()
                .HasMaxLength(100);

            model.Property(x => x.MotherName)
                .IsOptional()
                .HasMaxLength(200);

            model.Property(x => x.FatherName)
              .IsOptional()
              .HasMaxLength(200);

            model.Property(x => x.MainPhone)
             .IsOptional()
             .HasMaxLength(20);

            model.Property(x => x.Picture)
                .IsOptional();

            model.Property(x => x.Cpf)
               .HasMaxLength(20)
               .IsOptional();

            model.Property(x => x.Rg)
               .HasMaxLength(20)
               .IsOptional();

            model.Property(x => x.RgDispatcher)
             .HasMaxLength(20)
             .IsOptional();

            model.Property(x => x.RgDateExpedition)
                .IsOptional();

            model.Property(x => x.Rne)
                .HasMaxLength(20)
                .IsOptional();

            model.Property(x => x.RneDispatcher)
               .HasMaxLength(20)
               .IsOptional();

            model.Property(x => x.RneDateExpedition)
                .IsOptional();

            model.Property(x => x.RneDateExpiration)
               .IsOptional();

            model.Property(x => x.VoterNumberCard)
               .HasMaxLength(20)
               .IsOptional();

            model.Property(x => x.VoterSession)
               .HasMaxLength(20)
               .IsOptional();

            model.Property(x => x.VoterZone)
              .HasMaxLength(20)
              .IsOptional();

            model.Property(x => x.EmploymentBooklet)
                 .HasMaxLength(20)
                 .IsOptional();

            model.Property(x => x.EmploymentBookletSerie)
                .HasMaxLength(20)
                .IsOptional();

            model.Property(x => x.Pis)
                .HasMaxLength(20)
                .IsOptional();

            model.Property(x => x.Passport)
                .HasMaxLength(20)
                .IsOptional();

            model.Property(x => x.Reservist)
                .HasMaxLength(20)
                .IsOptional();

            model.Property(x => x.Active)
             .IsRequired();

            model.Property(x => x.AdmissionDate)
               .IsOptional();

            model.Property(x => x.ResignationDate)
             .IsOptional();

            model.Property(x => x.Registration)
                .HasMaxLength(45)
                .IsOptional();

            model.Property(x => x.IdentificationNumber)
              .HasMaxLength(45)
              .IsOptional();

            model.Property(x => x.Percentage)
          .IsOptional();

            // FKs

            model.HasOptional(x => x.Supervisor)
                 .WithMany()
                 .HasForeignKey(x => x.SupervisorId)
                 .WillCascadeOnDelete(false);

            model.HasOptional(x => x.City)
             .WithMany()
             .HasForeignKey(x => x.CityId)
             .WillCascadeOnDelete(false);

            model.HasOptional(x => x.Company)
                .WithMany()
                .HasForeignKey(x => x.CompanyId)
                .WillCascadeOnDelete(false);

            model.HasOptional(x => x.CivilStatus)
                .WithMany()
                .HasForeignKey(x => x.CivilStatusId)
                .WillCascadeOnDelete(false);

            model.HasOptional(x => x.EducationalStage)
                .WithMany()
                .HasForeignKey(x => x.EducationalStageId)
                .WillCascadeOnDelete(false);
        }
    }
}
