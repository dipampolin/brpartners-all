﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Infra.Mapping
{
    public class CourseEmployeeMap
    {

        public static void Map(EntityTypeConfiguration<CourseEmployee> model)
        {
            // PK
            model.HasKey(c => c.Id)
                .ToTable("CourseEmployee");

            // Properties
            model.Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();

            model.Property(x => x.Deadline)
                .IsOptional();

            model.Property(x => x.Required)
                .IsOptional();

            model.Property(x => x.Value)
                .IsOptional();

            model.Property(x => x.StartDate)
                .IsOptional();

            model.Property(x => x.EndDate)
                .IsOptional();


            //FK
            model.HasRequired(x => x.Employee)
                .WithMany(p => p.CourseEmployees)
                .HasForeignKey(x => x.EmployeeId)
                .WillCascadeOnDelete(true);

            model.HasOptional(x => x.Course)
                .WithMany(p => p.CourseEmployees)
                .HasForeignKey(x => x.CourseId)
                .WillCascadeOnDelete(false);
        }
    }
}
