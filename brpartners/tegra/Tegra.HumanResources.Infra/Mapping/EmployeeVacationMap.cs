﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Infra.Mapping
{
    public class EmployeeVacationMap
    {
        public static void Map(EntityTypeConfiguration<EmployeeVacation> model)
        {
            // PK
            model.HasKey(c => c.Id)
                .ToTable("EmployeeVacation");

            // Properties
            model.Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();

            model.Property(x => x.AquisitionStartDate)
                    .IsOptional();

            model.Property(x => x.AquisitionEndDate)
                  .IsOptional();

            model.Property(x => x.EnjoymentStartDate)
                  .IsOptional();

            model.Property(x => x.EnjoymentEndDate)
                 .IsOptional();

            model.Property(x => x.ReceiptDate)
                .IsOptional();

            model.Property(x => x.Value)
                .IsOptional();

            // FKs
            model.HasRequired(x => x.Employee)
                .WithMany()
                .HasForeignKey(x => x.EmployeeId)
                .WillCascadeOnDelete(false);
        }
    }
}
