﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Infra.Mapping
{
    public class EmployeeAttachmentMap
    {
        public static void Map(EntityTypeConfiguration<EmployeeAttachment> model)
        {
            // PK
            model.HasKey(c => c.Id)
                .ToTable("EmployeeAttachment");

            // Properties
            model.Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();

            model.Property(x => x.Description)
                .IsRequired()
                .HasMaxLength(100);

            model.Property(x => x.FileName)
               .IsRequired()
               .HasMaxLength(100);

            model.Property(x => x.FileContent)
                .IsRequired();

            // FKs
            model.HasRequired(x => x.Employee)
                .WithMany()
                .HasForeignKey(x => x.EmployeeId)
                .WillCascadeOnDelete(false);
        }
    }
}
