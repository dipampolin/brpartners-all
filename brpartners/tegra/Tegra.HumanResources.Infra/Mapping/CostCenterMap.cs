﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Infra.Mapping
{
    public class CostCenterMap
    {

        public static void Map(EntityTypeConfiguration<CostCenter> model)
        {

            // PK
            model.HasKey(c => c.Id)
                .ToTable("CostCenter");

            // Properties
            model.Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();

            model.Property(x => x.Code)
                .HasMaxLength(20)
                .IsRequired();

            model.Property(x => x.Name)
                .HasMaxLength(100)
                .IsRequired();

        }
    }
}