﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Infra.Mapping
{
    public class EmployeeRelationshipMap
    {
        public static void Map(EntityTypeConfiguration<EmployeeRelationship> model)
        {
            // PK
            model.HasKey(c => c.Id)
                .ToTable("EmployeeRelationship");

            // Properties
            model.Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();

            model.Property(x => x.Relationship)
                    .IsOptional()
                    .HasMaxLength(100);

            model.Property(x => x.Name)
                   .IsOptional()
                   .HasMaxLength(100);

            model.Property(x => x.Cpf)
                   .IsOptional()
                   .HasMaxLength(20);

            model.Property(x => x.UseHealthPlan)
                  .IsRequired();

            model.Property(x => x.UseOdontologyPlan)
                .IsRequired();

            model.Property(x => x.IR)
               .IsRequired();

            model.Property(x => x.StartDate)
               .IsRequired();

            model.Property(x => x.EndDate)
              .IsOptional();

            // FKs
            model.HasRequired(x => x.Employee)
                .WithMany()
                .HasForeignKey(x => x.EmployeeId)
                .WillCascadeOnDelete(false);

            model.HasOptional(x => x.HealthPlan)
                .WithMany()
                .HasForeignKey(x => x.HealthPlanId)
                .WillCascadeOnDelete(false);

            model.HasOptional(x => x.OdontologyPlan)
                .WithMany()
                .HasForeignKey(x => x.OdontologyPlanId)
                .WillCascadeOnDelete(false);
        }
    }
}
