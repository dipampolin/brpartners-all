﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Tegra.HumanResources.Domain.Entities;

namespace Tegra.HumanResources.Infra.Mapping
{
    public class BenefitMap
    {

        public static void Map(EntityTypeConfiguration<Benefit> model)
        {

            // PK
            model.HasKey(c => c.Id)
                .ToTable("Benefit");

            // Properties
            model.Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();

            model.Property(x => x.Description)
                .HasMaxLength(100)
                .IsOptional();

            model.Property(x => x.Value)
                .IsOptional();

            // FKs
            model.HasRequired(x => x.Type)
                .WithMany()
                .HasForeignKey(x => x.BenefitTypeId)
                .WillCascadeOnDelete(false);

        }
    }
}