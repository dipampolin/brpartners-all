﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Tegra.DDD;
using Tegra.HumanResources.Domain.Entities;
using Tegra.HumanResources.Infra.Mapping;

namespace Tegra.HumanResources.Infra
{
    public class RHContext : DbContext, IContext
    {
        // Constructor
        public RHContext() : base("RH")
        {
            this.Configuration.LazyLoadingEnabled = false;
            Database.SetInitializer(new Initializer());
        }

        // Entities
        public DbSet<EmployeeAddress> Address { get; set; }
        public DbSet<Benefit> Benefits { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<CivilStatus> CivilStatuses { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<CostCenter> CostCenters { get; set; }
        public DbSet<Course> Course { get; set; }
        public DbSet<CourseEmployee> CourseEmployee { get; set; }
        public DbSet<EducationalStage> EducationalStages { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Payslip> Payslips { get; set; }
        public DbSet<PayslipItem> PayslipItem { get; set; }
        public DbSet<State> States { get; set; }
        public DbSet<EmployeeVehicle> Vehicles { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<NotificationItem> NotificationItems { get; set; }
        public DbSet<NotificationDestination> NotificationDestinations { get; set; }
        public DbSet<EmployeeVariables> EmployeeVariables { get; set; }
        public DbSet<EmployeeSalary> EmployeeSalaries { get; set; }
        public DbSet<EmployeeRelationship> EmployeeRelationships { get; set; }
        public DbSet<EmployeeVacation> EmployeeVacations { get; set; }
        public DbSet<EmployeeContract> EmployeeContracts { get; set; }
        public DbSet<EmployeeBankAccount> EmployeeBankAccounts { get; set; }
        public DbSet<EmployeeContact> EmployeeContacts { get; set; }
        public DbSet<EmployeeAttachment> EmployeeAttachments { get; set; }
        public DbSet<EmployeeBenefit> EmployeeBenefits { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<EmployeeRole> EmployeeRoles { get; set; }
        public DbSet<EmployeeCompany> EmployeeCompanies { get; set; }
        public DbSet<EmployeeCostCenter> EmployeeDepartment { get; set; }
        public DbSet<EmployeeType> EmployeeTypes { get; set; }
        public DbSet<Bank> Banks { get; set; }

        // Mapping
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // Default configuration
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Properties<string>().Configure(p => p.HasMaxLength(256));

            modelBuilder.Conventions.Remove<DecimalPropertyConvention>();
            modelBuilder.Conventions.Add(new DecimalPropertyConvention(10, 2));

            modelBuilder.HasDefaultSchema("rh");

            // Mappings
            EmployeeAddressMap.Map(modelBuilder.Entity<EmployeeAddress>());
            BenefitMap.Map(modelBuilder.Entity<Benefit>());
            CityMap.Map(modelBuilder.Entity<City>());
            CivilStatusMap.Map(modelBuilder.Entity<CivilStatus>());
            CompanyMap.Map(modelBuilder.Entity<Company>());
            CostCenterMap.Map(modelBuilder.Entity<CostCenter>());
            CourseEmployeeMap.Map(modelBuilder.Entity<CourseEmployee>());
            CourseMap.Map(modelBuilder.Entity<Course>());
            EducationalStageMap.Map(modelBuilder.Entity<EducationalStage>());
            EmployeeMap.Map(modelBuilder.Entity<Employee>());
            PayslipItemMap.Map(modelBuilder.Entity<PayslipItem>());
            PayslipMap.Map(modelBuilder.Entity<Payslip>());
            ReportItemMap.Map(modelBuilder.Entity<ReportItem>());
            ReportMap.Map(modelBuilder.Entity<Report>());
            StateMap.Map(modelBuilder.Entity<State>());
            VehicleMap.Map(modelBuilder.Entity<EmployeeVehicle>());
        }

        public void EnableAutoDetectChanges()
        {
            this.Configuration.AutoDetectChangesEnabled = true;
        }

        public void DisableAutoDetectChanges()
        {
            this.Configuration.AutoDetectChangesEnabled = false;
        }

        public DbContextTransaction BeginTransaction()
        {
            return this.Database.BeginTransaction();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
    }
}
