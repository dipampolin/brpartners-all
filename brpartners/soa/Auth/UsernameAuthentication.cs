﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.IdentityModel;
using System.IdentityModel.Selectors;
using System.IdentityModel.Tokens;

namespace QX3.Portal.WCF.Auth
{
    public class UsernameAuthentication : UserNamePasswordValidator
    {
        public override void Validate(string userName, string password)
        {
            if(null == userName && null == password)
                throw new NotImplementedException();

            if (!(userName == ConfigurationManager.AppSettings["AppUserName"] && password == ConfigurationManager.AppSettings["AppPassword"])) 
            {
                throw new SecurityTokenException("##Authentication failed##");
            }
        }
    }
}